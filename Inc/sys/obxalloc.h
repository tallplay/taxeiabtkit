#ifndef __OBXALLOC_H
#define __OBXALLOC_H
/****************************************************************************
 *
 * File:          obxalloc.h
 *
 * Description:   This file contains defines the globally accessable RAM
 *                structures and their access functions. The RAM may be
 *                allocated in any file, from the individual OBEX source
 *                code files, to a global allocation file.
 * 
 * Created:       February 12, 2000
 *
 * Version:       MTObex 3.4
 *
 * Copyright 2000-2005 Extended Systems, Inc.  ALL RIGHTS RESERVED.
 *
 * Unpublished Confidential Information of Extended Systems, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any means, 
 * or stored in a database or retrieval system, without prior written 
 * permission of Extended Systems, Inc.
 * 
 * Use of this work is governed by a license granted by Extended Systems, Inc.  
 * This work contains confidential and proprietary information of Extended 
 * Systems, Inc. which is protected by copyright, trade secret, trademark and 
 * other intellectual property rights.
 *
 ****************************************************************************/
#include <obconfig.h>
#include <sys/obstack.h>

#if OBEX_ROLE_SERVER == XA_ENABLED
extern void ProcessGeneral(ObexServerApp *ObexApp, ObexParserEvent event);
extern void ProcessPut(ObexServerApp *ObexApp, ObexParserEvent event);
extern void ProcessGet(ObexServerApp *ObexApp, ObexParserEvent event);
#if OBEX_SESSION_SUPPORT == XA_ENABLED 
extern void ProcessSession(ObexServerApp *ObexApp, ObexParserEvent event);
#endif /* OBEX_SESSION_SUPPORT == XA_ENABLED */
#endif /* OBEX_ROLE_SERVER == XA_ENABLED */

#if OBEX_ROLE_CLIENT == XA_ENABLED
extern void ProcessSimpleOpEvents(ObexClientApp *ObexApp, ObexParserEvent event);
extern void ProcessPutEvents(ObexClientApp *ObexApp, ObexParserEvent event);
extern void ProcessGetEvents(ObexClientApp *ObexApp, ObexParserEvent event);
#if OBEX_SESSION_SUPPORT == XA_ENABLED
extern void ProcessSessionEvents(ObexClientApp *ObexApp, ObexParserEvent event);
#endif /* OBEX_SESSION_SUPPORT == XA_ENABLED */
#endif /* OBEX_ROLE_CLIENT == XA_ENABLED */

/*---------------------------------------------------------------------------
 * OBEX ROM
 */
#if OBEX_ROLE_SERVER == XA_ENABLED
extern const ServerProcessEvent ServerEventHandlerTable[];
#endif /* OBEX_ROLE_SERVER == XA_ENABLED */

#if OBEX_ROLE_CLIENT == XA_ENABLED
extern const ClientProcessEvent ClientEventHandlerTable[];
#endif /* OBEX_ROLE_CLIENT == XA_ENABLED */

#if OBEX_SESSION_SUPPORT == XA_ENABLED
#if OBEX_ROLE_CLIENT == XA_ENABLED
extern const U8 ObClntRxSessionParmsTable[];
#endif /* OBEX_ROLE_CLIENT == XA_ENABLED */

#if OBEX_ROLE_SERVER == XA_ENABLED
extern const U8 ObServRxSessionParmsTable[];
#endif /* OBEX_ROLE_SERVER == XA_ENABLED */
#endif /* OBEX_SESSION_SUPPORT == XA_ENABLED */

/*---------------------------------------------------------------------------
 * OBEX RAM
 *
 * All of the OBEX components; Client, Server and Transports, are located
 * via the RAM pointer(s) or structure(s) defined in this section.
 */
#if XA_CONTEXT_PTR == XA_DISABLED
/*
 * Each OBEX modules data is stored in a global structure. This structure
 * is allocated in static RAM which is reserved in the executable.
 */
extern ObInternalData     ObexData;    /* Defined in obstack.h */

#else /* XA_CONTEXT_PTR == XA_DISABLED */
/*
 * The module data is accessed through a global pointer.
 */
extern ObInternalData     *ObexData;

#endif /* XA_CONTEXT_PTR == XA_DISABLED */

/*---------------------------------------------------------------------------
 * OBEX Context Access macros
 *
 * These macros convert to point to or dereference the actual memory.
 */
#if XA_CONTEXT_PTR == XA_DISABLED
#define OBD(f)      (ObexData.f)
#else
#define OBD(f)      (ObexData->f)
#endif /* XA_CONTEXT_PTR == XA_DISABLED */

/*---------------------------------------------------------------------------
 * OBEX Customizations
 *
 * Since this file is included by all source files after all the include
 * files, it is an ideal place to perform module wide customizations that
 * are not performed in the overide.h file.
 * 
 */

#endif /* __OBXALLOC_H */
