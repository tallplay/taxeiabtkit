/****************************************************************************
 *
 * File:
 *     $Workfile:HCI3W.h$ for iAnywhere Blue SDK, Version 2.2
 *     $Revision:14$
 *
 * Description: This file is part of an HCI 3-Wire implementation.  It 
 * provides header information used by the code that coordinates and controls 
 * the lower layers.  
 *
 * Created:     October 31, 2006
 *
 * Copyright 2006-2007 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions,  
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#ifndef __HCI3W_H
#define __HCI3W_H

#include "sys/HCI3WDat.h"
#include "sys/HCI3WUtl.h"

/****************************************************************************
 *
 * Types and Constants
 *
 ****************************************************************************/

/* Link buffers should be large enough to hold 3 payload 
 * bytes.  Just to be generous, we round up to an even 
 * power of 2 (8), and then double that for good measure.
 */ 
#define LINK_BUFF_SIZE 0x10

typedef enum {
    LM_SYNC,
    LM_SYNC_RESPONSE,
    LM_CONF,
    LM_CONF_RESPONSE,
    LM_WAKEUP,
    LM_WOKEN,
    LM_SLEEP
} LINK_MESSEGES_ENUM;

enum {
    LAST_LINK_MSG = LM_SLEEP,
    NUM_LINK_MESSAGES
};

typedef enum
{
    DI_DATA_NONE,
    DI_DATA_PARTIAL,
    DI_LINK_UNINIT,
    DI_LINK_INIT,
    DI_LINK_ACTIVE
} DATA_INIT_ENUM;

typedef enum {
    DS_UNINIT,
    DS_PARTIAL_INIT,
    DS_LINK_UNINIT,
    DS_LINK_INIT,
    DS_LINK_ACTIVE,
    DS_PARTIAL_FREE
} DATA_STATE_ENUM;

typedef enum
{
    DF_WORKING,
    DF_PARTIAL_FREE,
    DF_FREE
} DATA_FREE_ENUM;

typedef enum {
    RE_HOST,
    RE_CNTRLR_AT_ONCE,
    RE_CNTRLR_DELAYED
} ROLE_ENUM;

typedef enum {
    TE_NOT_TRIGGERED,
    TE_JUST_TRIGGERED,
    TE_PREV_TRIGGERED
} TRIGGER_ENUM;

typedef enum {
    LP_ASLEEP,
    LP_AWAKE
} LOW_POWER_ENUM;

/****************************************************************************
 *
 * Data Structures
 *
 ****************************************************************************/

typedef struct {
    U8 octets[2];
} LinkMsgStruct;

typedef struct {
    BOOL             crc;
    SLIDING_WIN_ENUM slidingWindow;
} ConfigOptionsStruct;

typedef struct
{
    BOOL            activeTriggered;
    BOOL            configFieldTransferred;
    BOOL            configResponseReceived;
    BOOL            delayUntilSYNCRX;
    BOOL            linkMsgNeeded;
    BOOL            privAfterLinkAllowOOF;
    BOOL            synchReceived;
    BOOL            waitingForWoken;
    BuffUseStruct   msgConfig;
    BuffUseStruct   msgRX;
    BuffUseStruct   msgSync;
    BuffUseStruct   msgTX;
    BuffUseStruct   rawRX;
    DATA_STATE_ENUM dataState;
    EvmTimer        timer;
    Hci3WCrcStruct  crcObj;
    Hci3WDatStruct  datObj;
    Hci3WEvtStruct  evtObj;
    Hci3WHdrStruct  hdrObj;
    Hci3WMemStruct  memObj;
    Hci3WRxQStruct  datRXQObj;
    Hci3WSlpStruct  slpObj;
    Hci3WTxQStruct  txqObj;
    Hci3WUtlStruct  utlObj;
    LinkMsgStruct   linkMsgs[NUM_LINK_MESSAGES];
    FrameHdrStruct *hdrInfoP;
    U8              bufConfig[8];
    U8              bufSync[8];
    U8              bufTX[LINK_BUFF_SIZE];
    U8              busy;
    void          (*h3CallBack)(H3W_EVENT_ENUM, U8);
} Hci3WStruct;

/****************************************************************************
 *
 * Function Reference
 *
 ****************************************************************************/

/*---------------------------------------------------------------------------
 * h3wInit()
 *     Initializes the HCI 3-wire coordination and control module.  This 
 *     function must be invoked before any of the other HCI 3-wire 
 *     coordination and control functions will work correctly.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire coordination and control module context 
 *         structure.  
 *     memPoolP - This parameter must point to a buffer that can be used as a 
 *         memory pool.  
 *     memPoolSize - This parameter must contain the size of the given memory 
 *         pool.  
 *     hciSynchFlowControl - This parameter must be TRUE if the upper HCI 
 *         layer is performing HCI synchronous flow control, or FALSE 
 *         otherwise.  
 *     role - This parameter must contain an enumeration corresponding to 
 *         whether the role is the host, a controller that delays until it 
 *         receives a message, or a controller that immediately begins 
 *         sending messages.  
 *     allowed - This parameter must contain a configuration options 
 *         structure indicating what features are allowed as long as the 
 *         other device also supports them.  
 *     afterLinkAllowOOF - This parameter must be TRUE if out of frame 
 *         software flow control is both desired and allowed by this device.  
 *     callBack - This parameter must be set to the address of a function 
 *         that can be called to signal the upper layers of events of 
 *         interest.  
*/
void h3wInit(Hci3WStruct        *cP, 
             U8                 *memPoolP,
             U32                 memPoolSize,
             BOOL                hciSynchFlowControl,
             ROLE_ENUM           role,
             ConfigOptionsStruct allowed,
             BOOL                afterLinkAllowOOF,
             void              (*callBack)(H3W_EVENT_ENUM, U8));

/*---------------------------------------------------------------------------
 * h3wDataFree()
 *     If the freeing process has not previously been started, this function 
 *     signals the lower data frame layer that it should begin freeing the 
 *     slip layer.  In any event, this function sets the coordination and 
 *     control layer state data to the appropriate values.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire coordination and control module context 
 *         structure.  
 *     dataFreeP - This parameter must point to a location where an 
 *         enumeration corresponding to the data free state can be written.  
 *
 * Returns:
 *     This function returns zero if it successfully checked (and began 
 *     freeing if needed) the free state.  Otherwise, it returns an error 
 *     number.  
 */
U32  h3wDataFree(Hci3WStruct *cP, DATA_FREE_ENUM *dataFreeP);

/*---------------------------------------------------------------------------
 * h3wDataInit()
 *     If the initializing process has not previously been started, this 
 *     function signals the lower data frame layer that it should begin 
 *     initializing the slip layer at a given transfer rate.  In any event, 
 *     this function sets the coordination and control layer state data to 
 *     the appropriate values.  This function obtains the configuration 
 *     options put in use if the state indicates that the HCI 3-wire linking 
 *     process has completed successfully and has gone active.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire coordination and control module context 
 *         structure.  
 *     dataInitP - This parameter must point to a location where a data frame 
 *         initialization enumeration can be written.  
 *     configuredP - This parameter must point to a location where a 
 *         configuration options structure can be written.  
 *     uartBPSSpeed - This parameter must be set the transport speed at 
 *         which the HCI 3-wire code is to operate.  
 *
 * Returns:
 *     This function returns zero if it successfully checked (and initiated 
 *     if needed) the initialization state.  Otherwise, it returns an error 
 *     number.  
 */
U32  h3wDataInit(Hci3WStruct         *cP,
                 DATA_INIT_ENUM      *dataInitP,
                 ConfigOptionsStruct *configuredP,
                 U32                  uartBPSSpeed);

/*---------------------------------------------------------------------------
 * h3wDataSetSpeed()
 *     This function redefines the speed at which the Data Layer is to 
 *     operate.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire coordination and control module context 
 *         structure.  
 *     bps - This is the new bits per second rate that is to be used.
 *
 * Returns:
 *     This function returns TRUE if it was successfully able to set the Data 
 *     Layer data transfer speed, or FALSE otherwise.  
 */
U32 h3wDataSetSpeed(Hci3WStruct *cP, U32 bps);

/*---------------------------------------------------------------------------
 * h3wGetConfigOptions()
 *     This function obtains the configuration options that are in use.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire coordination and control module context 
 *         structure.  
 *     configOptionsP - This parameter must point to a location where a 
 *         configuration options structure can be written.  
 *
 * Returns:
 *     This function returns zero if it successfully obtained the 
 *     configuration options in use.  Otherwise, it returns an error number.  
 */
U32  h3wGetConfigOptions(Hci3WStruct         *cP, 
                         ConfigOptionsStruct *configOptionsP);

/*---------------------------------------------------------------------------
 * h3wGetMaxPayloadBytes()
 *     This function obtains the maximum number of payload bytes allowed.  
 *     (Note that this value will be 4095 unless the code was instructed to 
 *     reduce this default value.)
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire coordination and control module context 
 *         structure.  
 *     maxPayloadBytesP - This parameter must point to a location where the 
 *         maximum number of payload bytes can be written.  
 *
 * Returns:
 *     This function returns zero if it successfully obtained the maximum 
 *     number of payload bytes value.  Otherwise, it returns an error number.  
 */
U32  h3wGetMaxPayloadBytes(Hci3WStruct *cP, U16 *maxPayloadBytesP);

/*---------------------------------------------------------------------------
 * h3wReceive()
 *     This function receives HCI 3-wire data messages from the lower levels.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire coordination and control module context 
 *         structure.  
 *     buff - This parameter must point to a buffer where the received data 
 *         can be written.  
 *     usedP - This parameter must point to a location where the size of the 
 *         received data can be written.  
 *     maxSize - This is the maximum size of the given buffer.  
 *     packetTypeP - This parameter must point to a location where an 
 *         enumeration corresponding to the HCI 3-wire packet type can be 
 *         written.  
 *
 * Returns:
 *     This function returns zero if it successfully received a message.  
 *     Otherwise, it returns an error number.  
 */
U32  h3wReceive(Hci3WStruct      *cP,
                U8               *buff,
                U16              *usedP,
                U16               maxSize,
                PACKET_TYPE_ENUM *packetTypeP);

/*---------------------------------------------------------------------------
 * h3wSendReliably()
 *     This function sends a realiable message.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire coordination and control module context 
 *         structure.  
 *     handleP - This parameter must point to a location where an HCI 3-wire 
 *         reliable transmission handle can be written.  
 *     buff - This parameter must point to a buffer containing the message 
 *         that is to be sent.  
 *     size - This parameter must contain the size of the given buffer.  
 *     packetType - This parameter must be set to an enumeration 
 *         corresponding to the HCI 3-wire packet type.  
 *     includeCRC - This parameter must be TRUE if a cyclic redundancy check 
 *         is to be appended to the end of the message when it is framed.  
 *
 * Returns:
 *     This function returns zero if it successfully sent a message.  
 *     Otherwise, it returns an error number.  (Note that because the message 
 *     is being sent reliably, the sending is not truly complete until it is 
 *     acknowledged.)  
 */
U32  h3wSendReliably(Hci3WStruct     *cP,
                     U8              *handleP,
                     U8              *buff,
                     U16              size,
                     PACKET_TYPE_ENUM packetType,
                     BOOL             includeCRC);

/*---------------------------------------------------------------------------
 * h3wSendUnreliably()
 *     This function sends an unrealiable message.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire coordination and control module context 
 *         structure.  
 *     buff - This parameter must point to a buffer containing the message 
 *         that is to be sent.  
 *     size - This parameter must contain the size of the given buffer.  
 *     packetType - This parameter must be set to an enumeration 
 *         corresponding to the HCI 3-wire packet type.  
 *     includeCRC - This parameter must be TRUE if a cyclic redundancy check 
 *         is to be appended to the end of the message when it is framed.  
 *
 * Returns:
 *     This function returns zero if it successfully (yet unreliably) sent a 
 *     message.  Otherwise, it returns an error number.  
 */
U32  h3wSendUnreliably(Hci3WStruct     *cP,
                       U8              *buff,
                       U16              size,
                       PACKET_TYPE_ENUM packetType,
                       BOOL             includeCRC);

/*---------------------------------------------------------------------------
 * h3wSetMaxPayloadBytes()
 *     This is an optional function that can be called at initialization time 
 *     to reduce the maximum payload size in messages.  Note that while this 
 *     will reduce memory requirements, it will also increase the overhead of 
 *     high volume message streams.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire coordination and control module context 
 *         structure.  
 *     maxPayloadBytes - The new value for the maximum number of payload 
 *         bytes.  Note that this value should range from about 10 on up to 
 *         no more than 4095.  
 *
 * Returns:
 *     This function returns zero if it successfully redefined the maximum 
 *     payload size.  Otherwise, it returns an error number.  
 */
U32  h3wSetMaxPayloadBytes(Hci3WStruct *cP, U16 maxPayloadBytes);

/*---------------------------------------------------------------------------
 * h3wSetUARTClockMultiplier()
 *     Sets the value of the UART clock multiplier.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire coordination and control module context 
 *         structure.  
 *     uartClockMultiplier - An enumeration corresponding to the amount the 
 *         UART speeds are overclocked.  
 *
 * Returns:
 *     This function returns zero if it successfully set the value of the 
 *     UART clock multiplier.  Otherwise, it returns an error number.  
 */
U32 h3wSetUARTClockMultiplier(Hci3WStruct   *cP, 
                              OVERCLOCK_ENUM uartClockMultiplier);

/*---------------------------------------------------------------------------
 * h3wSleep()
 *     This is an optional function that informs the other device that this 
 *     software is entering its sleep mode state.  It should not be used unless 
 *     the device making the call has a low power sleep mode, and unless no 
 *     HCI 3-wire communication is needed for the time being.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire coordination and control module context 
 *         structure.  
 *
 * Returns:
 *     This function returns zero if it successfully sent the other device a 
 *     SLEEP message.  Otherwise, it returns an error number.  Any further 
 *     action such as entering a low power mode is up to the upper layers to 
 *     carry out.  
 */
U32  h3wSleep(Hci3WStruct *cP);

/*---------------------------------------------------------------------------
 * h3wUARTEncodingToBPS()
 *     Converts a given encoding expected by the lower UART layer software 
 *     into an bits per second transfer rate.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire coordination and control module context 
 *         structure.  
 *     bpsP - This parameter must point to a location where the bits per 
 *         second rate can be written.  
 *     uartSpeedEncoding - This parameter must containt the UART speed 
 *         encoded as expected by the lower UART software.  
 *
 * Returns:
 *     This function returns zero if it successfully converted the given 
 *     encoding expected by the lower UART layer software into an bits per 
 *     second transfer rate.  Otherwise, it returns an error number.  
 */
U32  h3wUARTEncodingToBPS(Hci3WStruct *cP, U32 *bpsP, U16 uartSpeedEncoding);

/*---------------------------------------------------------------------------
 * h3wWakeup()
 *     This function sends a wakeup message to the other device.  This 
 *     function is not needed if the other device does not have a SLEEP mode.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire coordination and control module context 
 *         structure.  
 *     lowPowerStateP - This parameter must point to a location where an 
 *         enumeration corresponding to the assumed low power state can be 
 *         written.
 *
 * Returns:
 *     This function returns zero if it successfully sent the other device a 
 *     WAKEUP message.  Otherwise, it returns an error number.  
 */
U32  h3wWakeup(Hci3WStruct *cP, LOW_POWER_ENUM *lowPowerStateP);

/*---------------------------------------------------------------------------
 * h3wEventCallback()
 *     This function is called by the event module when an event needs to be 
 *     handled.  If it has a valid context, it invokes the event handling 
 *     routine.  
 * 
 * Parameters:
 *     context - This parameter must be set to the address of an HCI 3-wire 
 *         coordination and control context structure.  
 *     event - This is an event structure containing information about the 
 *         event to be handled.  
 */
void h3wEventCallback(void *context, EventStruct event);

/*---------------------------------------------------------------------------
 * h3wTimerLnkEventCallback()
 *     This function is invoked when the coordination and control timer fires.  
 *     If it is given a valid context, it invokes the coordination and 
 *     control timer event code.  
 * 
 * Parameters:
 *     evmTimerP - The event manager structure corresponding to the timer that 
 *         just fired.  
 */
void h3wTimerLnkEventCallback(struct _EvmTimer *evmTimerP);

/*---------------------------------------------------------------------------
 * h3wTerm()
 *     This function destroys a given coordination and control module, 
 *     cleaning up any allocated resources.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire coordination and control module context 
 *         structure.  
 */
void h3wTerm(Hci3WStruct *cP);

#endif /* __HCI3W_H */ 
