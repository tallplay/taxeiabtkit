/****************************************************************************
 *
 * File:
 *     $Workfile:HCI3WMem.h$ for iAnywhere Blue SDK, Version 2.2
 *     $Revision:8$
 *
 * Description: This file is part of an HCI 3-Wire implementation.  It 
 * provides header information used by the code that coordinates the use of 
 * the memory pool.  
 *
 * Created:     October 31, 2006
 *
 * Copyright 2006-2007 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions,  
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#ifndef __HCI3WMem_h
#define __HCI3WMem_h

#include "sys/hci3wutl.h"

/****************************************************************************
 *
 * Types and Constants
 *
 ****************************************************************************/

typedef enum
{
    BS_FREE,
    BS_IN_USE
} BUFF_STATUS_ENUM;

/****************************************************************************
 *
 * Data Structures
 *
 ****************************************************************************/

typedef struct {
    BOOL valid;
    U32  ndx;
} NdxStruct;

typedef struct {
    BUFF_STATUS_ENUM status;
    NdxStruct        next;
} BuffHdrStruct;

typedef struct
{
    BOOL initialized;
    U8  *privMemPoolP;
    U32  privMemPoolSize;
} Hci3WMemStruct;

/****************************************************************************
 *
 * Function Reference
 *
 ****************************************************************************/

/*---------------------------------------------------------------------------
 * h3wMemInit()
 *     Initializes the HCI 3-wire memory module.  This function must be 
 *     invoked before any of the other HCI 3-wire memory functions will work 
 *     correctly.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire memory module context structure.  
 *     memPoolP - This parameter must point to a buffer that will be used as 
 *         the memory pool.
 *     memPoolSize - This parameter must contain the size of the given memory 
 *         pool.  
 */
void h3wMemInit(Hci3WMemStruct *cP, 
                U8             *memPoolP, 
                U32             memPoolSize);

/*---------------------------------------------------------------------------
 * h3wMemFree()
 *     This function frees up a previously allocated memory buffer.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire memory module context structure.  
 *     buff - This parameter must point to a buffer that was previously 
 *         allocated by this module.  
 *     errObjP - A pointer to an error context structure.
 *
 * Returns:
 *     This function returns TRUE if it was successfully able to deallocate 
 *     the given memory buffer, or FALSE otherwise.  
 */
BOOL h3wMemFree(Hci3WMemStruct *cP,
                U8             *buff, 
                Hci3WErrStruct *errObjP);

/*---------------------------------------------------------------------------
 * h3wMemGet()
 *     Allocates memory from the memory pool.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire memory module context structure.  
 *     buffP - This parameter must point to a location where the address of a 
 *         buffer can be written.  
 *     size - The size of the buffer to be allocated.  
 *     errObjP - A pointer to an error context structure.
 *
 * Returns:
 *     This function returns TRUE if it was successfully able to allocate a 
 *     buffer of the desired size, or FALSE otherwise.  
 */
BOOL h3wMemGet(Hci3WMemStruct *cP, 
               U8            **buffP, 
               U32             size, 
               Hci3WErrStruct *errObjP);

/*---------------------------------------------------------------------------
 * h3wMemTerm()
 *     This function destroys a given memory module, cleaning up any 
 *     allocated resources.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire memory module context structure.  
 */
void h3wMemTerm(Hci3WMemStruct *cP);

#endif /* __HCI3WMem_h */ 
