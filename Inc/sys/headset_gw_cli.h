/****************************************************************************
 *
 * File:
 *     $Workfile:headset_gw_cli.h$ for XTNDAccess Blue SDK, Version 1.3
 *     $Revision:31$
 *
 * Description: This file contains a test application for the headset profile
 *             
 * Created:     September 12, 2000
 *
 * Copyright 2000-2002 Extended Systems, Inc.  ALL RIGHTS RESERVED.
 *
 * Unpublished Confidential Information of Extended Systems, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any means, 
 * or stored in a database or retrieval system, without prior written 
 * permission of Extended Systems, Inc.
 * 
 * Use of this work is governed by a license granted by Extended Systems, Inc.  
 * This work contains confidential and proprietary information of Extended 
 * Systems, Inc. which is protected by copyright, trade secret, trademark and 
 * other intellectual property rights.
 *
 ****************************************************************************/
#ifndef __GATEWAY_CLI_H_
#define __GATEWAY_CLI_H_


#include <hs_gw.h>
#include <sys/hci.h>
#include <parseopts.h>
#include <sniffer_ui.h>

/****************************************************************************
 *
 * Menu information
 *
 ****************************************************************************/
/* Application Main Menu options */
#define M_AGOUTGOING                M_MENUBASE+0
#define M_AGINCOMING                M_MENUBASE+1
#define M_AGDISCONNECT              M_MENUBASE+2
#define M_AGSPECIFY_ADDR_DONE       M_MENUBASE+3
#define M_AGLOGGER                  M_MENUBASE+4
#define M_AGBDADDR                  M_MENUBASE+5
#define M_AGSECURITY                M_MENUBASE+6

/* Volume Buttons */
#define M_AGMVDOWN                   M_MENUBASE+7
#define M_AGMVUP                     M_MENUBASE+8
#define M_AGSVDOWN                   M_MENUBASE+9
#define M_AGSVUP                     M_MENUBASE+10

#if AG_SECURITY == XA_ENABLED
#define M_PIN_ENTERED                M_MENUBASE+20
#endif

/****************************************************************************
 *
 * Application functions required by main
 *
 ****************************************************************************/
BOOL APP_Init(void);
void APP_Deinit(void);
void APP_PrintTitle(void);
BOOL APP_Thread(void);

/****************************************************************************
 *
 * Application functions required by textmenu
 *
 ****************************************************************************/
void App_Report(char *format,...);

#endif /* __GATEWAY_CLI_H_ */
