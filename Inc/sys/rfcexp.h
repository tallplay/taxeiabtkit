#ifndef __RFCEXP_H
#define __RFCEXP_H

/***************************************************************************
 *
 * File:
 *     $Workfile:rfcexp.h$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:18$
 *
 * Description:
 *     Function exports to lower layer (ME).
 *
 * Created:
 *     Sept 13, 1999
 *
 * Copyright 1999-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/


/*---------------------------------------------------------------------------
 * Prototype:     BtStatus RF_Init(void)
 *
 * Description:   Initialize RFCOMM.  Register with L2CAP, initialize state and
 *                other variables.
 *            
 * Parameters:    void
 *
 * Returns:       BT_STATUS_SUCCESS - initialization successful.
 *
 *                BT_STATUS_FAILED - initialization failed.   
 */
BtStatus RF_Init(void);

/*---------------------------------------------------------------------------
 * Prototype:     RF_Deinit()
 *
 * Description:   Deinitialize RFCOMM.  Deregister with L2CAP.
 *            
 * Parameters:    void
 *
 * Returns:       void   
 */
void RF_Deinit(void);

#endif /* __RFCEXP_H */

