#ifndef __HIDCTLCON_H_
#define __HIDCTLCON_H_

#include "l2cap.h"

/* Connection Flags */
#define HID_FLAG_LINK_ACL       0x01

/* Conn Events */
#define HID_CONN_EVENT_IN_CONNECTED     1
#define HID_CONN_EVENT_OUT_CONNECTED     2
#define HID_CONN_EVENT_CONNECT_IND 3
#define HID_CONN_EVENT_DISCONNECT  4
#define HID_CONN_EVENT_DATA_SENT   5
#define HID_CONN_EVENT_DATA_IND    6

/* Connection States */
#define HID_CONN_NUM_STATES     6

#define HID_STATE_DISCONNECTED   0
#define HID_STATE_CONN_PENDING   1
#define HID_STATE_CONN_INCOMING  2
#define HID_STATE_DISC_PENDING   3
#define HID_STATE_DISC_INCOMING  4
#define HID_STATE_CONNECTED      5

/* Forward references */
typedef struct _HidConnCallbackParms HidConnCallbackParms;
typedef struct _HidConn HidConn;

/* State machine function prototype */                     
typedef void (*HidConnState)(HidConn *Conn, L2capCallbackParms *Parms);

/* Conn callback function */
typedef void (*HidConnCallback)(HidConn *Conn, HidConnCallbackParms *Parms);

/* Conn calback parameters */
struct _HidConnCallbackParms {
    U8         event;
    U16        dataLen;
    union {
        BtRemoteDevice *remDev;
        U8             *data;
    } ptrs;
};

/* Connection State */
#define CONN_BT_PACKET_NUM 10
struct _HidConn {
    L2capChannelId      l2ChannelId;
    U8                  state;
    HidConnCallback    callback;
    BtRemoteDevice     *remDev;
    U8                 btPacketIdx;
    U8                 btPacketNum;
    BtPacket          btPacket[CONN_BT_PACKET_NUM];
};

/* State machine functions */
void HidConnInitStateMachine(void);
void HidL2IntCallback(L2capChannelId L2ChannelId, L2capCallbackParms *Parms);
void HidL2CtlCallback(L2capChannelId L2ChannelId, L2capCallbackParms *Parms);


/* Function Prototypes */
void HidInitConn(HidConn *conn, HidConnCallback callback);
BtStatus HidIntConnect(HidConn *conn, BtRemoteDevice *RemDev);
BtStatus HidCtlConnect(HidConn *conn, BtRemoteDevice *RemDev);
BtStatus HidConnectRsp(HidConn *Conn, BOOL Accept);
BtStatus HidDisconnect(HidConn *conn);
BOOL HidIsConnected(HidConn *conn);
BtStatus HidSendData(HidConn *conn, U16 datalen,  U8 *data);

#endif /* __HIDCON_H_ */


