/****************************************************************************
 *
 * File:
 *     $Workfile:HCI3WRXQ.h$ for iAnywhere Blue SDK, Version 2.2
 *     $Revision:7$
 *
 * Description: This file is part of an HCI 3-Wire implementation.  It 
 * provides header information used by the code that deals with messages 
 * stored and retrieved from a receive queue.  
 *
 * Created:     October 31, 2006
 *
 * Copyright 2006-2007 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions,  
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#ifndef __HCI3WRXQ_H
#define __HCI3WRXQ_H

#include "sys/HCI3WMem.h"
#include "sys/HCI3WUtl.h"

/****************************************************************************
 *
 * Types and Constants
 *
 ****************************************************************************/

/****************************************************************************
 *
 * Data Structures
 *
 ****************************************************************************/

typedef struct {
    BOOL hasNext;
    U8  *nextPtr;
    U16  size;
} RxQNodeHdrStruct;

typedef struct
{
    BOOL            rootValid;
    BuffPtrsStruct  bufPtrs;
    Hci3WMemStruct *privMemObjP;
    Hci3WUtlStruct *privUtlObjP;
    U8             *rootPtr;
} Hci3WRxQStruct;

/****************************************************************************
 *
 * Function Reference
 *
 ****************************************************************************/

/*---------------------------------------------------------------------------
 * h3wRXQInit()
 *     Initializes the HCI 3-wire receive queue module.  This function must 
 *     be invoked before any of the other HCI 3-wire receive queue functions 
 *     will work correctly.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire receive queue module context structure.  
 *     memObjP - Pointer to an HCI 3-wire memory module context structure.  
 *     utlObjP - Pointer to an HCI 3-wire utilities module context structure.  
 */
void h3wRXQInit(Hci3WRxQStruct *cP, 
                Hci3WMemStruct *memObjP, 
                Hci3WUtlStruct *utlObjP);

/*---------------------------------------------------------------------------
 * h3wRXQGet()
 *     This function returns the node pointed at by the root of the receive 
 *     queue.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire receive queue module context structure.  
 *     dstP - A pointer to a buffer use structure, where information about 
 *         the newly popped node can be written.  
 *     errObjP - A pointer to an error context structure.
 *
 * Returns:
 *     This routine returns TRUE if the receive queue was emptry or if it was 
 *     successfully able to get a node from the receive queue.  Otherwise, it 
 *     returns FALSE.  
 */
BOOL h3wRXQGet(Hci3WRxQStruct *cP, 
               BuffUseStruct  *dstP, 
               Hci3WErrStruct *errObjP);

/*---------------------------------------------------------------------------
 * h3wRXQGetAll()
 *     This function returns the all of the data from the receive queue.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire receive queue module context structure.  
 *     dstP - A pointer to a buffer use structure, where information about 
 *         the receive queue can be written.  
 *     errObjP - A pointer to an error context structure.
 *
 * Returns:
 *     This routine returns TRUE if the receive queue was emptry or if it was 
 *     successfully able to get all the nodes from the receive queue.  
 *     Otherwise, it returns FALSE.  
 */
BOOL h3wRXQGetAll(Hci3WRxQStruct *cP, 
                  BuffUseStruct  *dstP, 
                  Hci3WErrStruct *errObjP);

/*---------------------------------------------------------------------------
 * h3wRXQIsEmpty()
 *     Indicates whether the receive queue is empty.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire receive queue module context structure.  
 *
 * Returns:
 *     This function returns TRUE if the receive queue is empty, or FALSE 
 *     otherwise.  
 */
BOOL h3wRXQIsEmpty(Hci3WRxQStruct *cP);

/*---------------------------------------------------------------------------
 * h3wRXQPut()
 *     This function allocates memory for the new node, pushes the given data 
 *     onto the tail end of the receive queue, and copies the given data into 
 *     the new node.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire receive queue module context structure.  
 *     buf - This parameter must point to a buffer that is to be pushed onto 
 *         the recieve queue.
 *     size - This parameter must contain the size of the given buffer.  
 *     errObjP - A pointer to an error context structure.
 *
 * Returns:
 *     This function returns TRUE if it was successfully able to push the 
 *     given buffer onto the receive queue, or FALSE otherwise.  
 */
BOOL h3wRXQPut(Hci3WRxQStruct *cP,
               U8             *buf, 
               U16             size, 
               Hci3WErrStruct *errObjP);

/*---------------------------------------------------------------------------
 * h3wRXQFree()
 *     This function frees up all the nodes in the receive queue.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire receive queue module context structure.  
 *     errObjP - A pointer to an error context structure.
 *
 * Returns:
 *     This function returns TRUE if it was successfully able to free up all 
 *     of the nodes in the receive queue, or FALSE otherwise.  
 */
void h3wRXQFree(Hci3WRxQStruct *cP, Hci3WErrStruct *errObjP);

/*---------------------------------------------------------------------------
 * h3wRXQTerm()
 *     This function destroys a given receive queue module, cleaning up any 
 *     allocated resources.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire receive queue module context structure.  
 */
void h3wRXQTerm(Hci3WRxQStruct *cP);

#endif /* __HCI3WRXQ_H */ 
