/****************************************************************************
 * Created: Jan 9 2012
 * Description: This file contains internal struct of HID profile
 ****************************************************************************/

#ifndef __HIDI_H_
#define __HIDI_H_
#include "hid.h"

/* The HidContext structure contains the global context of HIDP. */
typedef struct _BtHidContext {
    SdpRecord       SdpRecord;
    U16             ChannelCount;
    hid_protocol_type  hid_prot_type;

} BtHidContext;

#if XA_CONTEXT_PTR == XA_ENABLED
#if 0
extern BtHidContext *hidContext;
#define HID(s) (hidContext->s)
#endif
#else /* XA_CONTEXT_PTR == XA_ENABLED */
extern BtHidContext hidContext;
#define HID(s) (hidContext.s)
#endif /* XA_CONTEXT_PTR */

BOOL HidAlloc(void);
BtStatus HidRegisterSdp(HidChannel *chnl);
BtStatus HidDeregisterSdp(HidChannel *chnl);
void HidReportFailedConnect(HidChannel *chnl, BtRemoteDevice *btrem);

BtStatus DidRegisterSdp(U8 ansi_iso_jis); //0, 1, 2
BtStatus DidDeregisterSdp();

#endif

