/****************************************************************************
 *
 * File:
 *     $Workfile:HCI3WDat.h$ for iAnywhere Blue SDK, Version 2.2
 *     $Revision:10$
 *
 * Description: This file is part of an HCI 3-Wire implementation.  It 
 * provides header information used by the code that handles the conversion 
 * of data to and from the framed format.  
 *
 * Created:     October 31, 2006
 *
 * Copyright 2006-2007 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions,  
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#ifndef __HCI3WDat_H
#define __HCI3WDat_H

#include "sys/HCI3WHdr.h"
#include "sys/HCI3WMem.h"
#include "sys/HCI3WSlp.h"
#include "sys/HCI3WTXQ.h"
#include "sys/HCI3WUtl.h"

/****************************************************************************
 *
 * Types and Constants
 *
 ****************************************************************************/

typedef enum {
    SS_UNINIT,
    SS_PARTIAL_INIT,
    SS_LINKING,
    SS_ACTIVE,
    SS_PARTIAL_FREE
} SLIP_STATE_ENUM;

/****************************************************************************
 *
 * Data Structures
 *
 ****************************************************************************/

typedef struct {
    BuffUseStruct *dP;
    BuffUseStruct *hP;
} HciBuffStruct;

typedef struct 
{
    Hci3WCrcStruct *privCRCObjP;
    Hci3WEvtStruct *privEvtObjP;
    Hci3WHdrStruct *privHdrObjP;
    Hci3WMemStruct *privMemObjP;
    Hci3WSlpStruct *privSlpObjP;
    Hci3WTxQStruct *privTXQObjP;
    Hci3WUtlStruct *privUtlObjP;
    SLIP_STATE_ENUM slipState;
} Hci3WDatStruct;

/****************************************************************************
 *
 * Function Reference
 *
 ****************************************************************************/

/*---------------------------------------------------------------------------
 * h3wDatInit()
 *     Initializes the HCI 3-wire data frame module.  This function must be 
 *     invoked before any of the other HCI 3-wire data frame functions will 
 *     work correctly.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire data frame module context structure.  
 *     crcObjP - Pointer to an HCI 3-wire cyclic redundancy check module 
 *         context structure.  
 *     evtObjP - Pointer to an HCI 3-wire event handling module context 
 *         structure.  
 *     hdrObjP - Pointer to an HCI 3-wire frame header module context 
 *         structure.  
 *     memObjP - Pointer to an HCI 3-wire memory module context structure.  
 *     slpObjP - Pointer to an HCI 3-wire slip module context structure.  
 *     txqObjP - Pointer to an HCI 3-wire transmit queue module context 
 *         structure.  
 *     utlObjP - Pointer to an HCI 3-wire utilities module context structure.  
 */
void h3wDatInit(Hci3WDatStruct *cP,
                Hci3WCrcStruct *crcObjP,
                Hci3WEvtStruct *evtObjP, 
                Hci3WHdrStruct *hdrObjP, 
                Hci3WMemStruct *memObjP, 
                Hci3WSlpStruct *slpObjP, 
                Hci3WTxQStruct *txqObjP,
                Hci3WUtlStruct *utlObjP);

/*---------------------------------------------------------------------------
 * h3wDatPureAck()
 *     This function transmits a pure acknowledgement message, as long as the 
 *     out of frame software flow control is not choked off.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire data frame module context structure.  
 *     errObjP - A pointer to an error context structure.  
 *
 * Returns:
 *     If MemCopy actually had a return I'd list it here. Otherwise this 
 *     section would not exist. Return values and descriptions would be
 *     listed much like the "Parameters" section.
 */
BOOL h3wDatPureAck(Hci3WDatStruct *cP, Hci3WErrStruct *errObjP);

/*---------------------------------------------------------------------------
 * h3wDatReceive()
 *     This function checks for conditions that prevent data reception, gets 
 *     enough memory to hold an HCI 3-wire framed message, invokes the lower 
 *     layer slip receive routine, creates an unframed version of the HCI 
 *     3-wire framed message, and then frees up the memory it just allocated.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire data frame module context structure.  
 *     dataBuffP - This parameter must point to a location where a buffer use 
 *         structure can be written.  
 *     hdrNewP - This parameter must point to a location where a frame header 
 *         structure can be written.  
 *     errObjP - A pointer to an error context structure.  
 *
 * Returns:
 *     If MemCopy actually had a return I'd list it here. Otherwise this 
 *     section would not exist. Return values and descriptions would be
 *     listed much like the "Parameters" section.
 */
BOOL h3wDatReceive(Hci3WDatStruct  *cP, 
                   BuffUseStruct   *dataBuffP,
                   FrameHdrStruct  *hdrNewP,
                   Hci3WErrStruct  *errObjP);

/*---------------------------------------------------------------------------
 * h3wDatSend()
 *     This function checks for conditions that prevent transmissions, gets 
 *     enough memory to frame the given HCI 3-wire message, creates an HCI 
 *     3-wire framed version of the message, invokes the lower layer slip 
 *     transmission routine, notes that this latest transmission has 
 *     acknowledged all of the outstanding received messages, adds the newly 
 *     transmitted message to the retransmit queue (if it was reliable), then 
 *     frees up the memory it just allocated.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire data frame module context structure.  
 *     handleP - a pointer to a location where a data frame handle can be 
 *         written.  
 *     isLowPowerMsg - This parameter must be TRUE if the message is a low 
 *         power message, or FALSE otherwise.  
 *     dataBuffP - This parameter must point to an HCI 3-wire message.  
 *     packetType - This parameter must contain an enumeration corresponding 
 *         to the HCI 3-wire packet type.  
 *     sendReliably - This parameter must be TRUE if the HCI 3-wire message 
 *         is relaiable, or FALSE otherwise.  
 *     includeCRC - This parameter must be TRUE if the HCI 3-wire message is 
 *         to have a CRC value appended to the end of the payload, or FALSE 
 *         otherwise.  
 *     errObjP - A pointer to an error context structure.  
 *
 * Returns:
 *     This function returns TRUE if it was successfully able to transmit the 
 *     message, or FALSE otherwise.  
 */
BOOL h3wDatSend(Hci3WDatStruct  *cP, 
                U8              *handleP, 
                BOOL             isLowPowerMsg,
                BuffUseStruct   *dataBuffP,
                PACKET_TYPE_ENUM packetType,
                BOOL             sendReliably, 
                BOOL             includeCRC,
                Hci3WErrStruct  *errObjP);

/*---------------------------------------------------------------------------
 * h3wDatSlipFree()
 *     This function signals the lower slip layer that it should begin 
 *     freeing the UART, sets the data layer state data to the appropriate 
 *     values, and frees up the transmit queue and the frame header objects.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire data frame module context structure.  
 *     slipStateP - This parameter must point to a location where a slip 
 *         state enumeration can be written.  
 *     errObjP - A pointer to an error context structure.  
 */
BOOL h3wDatSlipFree(Hci3WDatStruct  *cP, 
                    SLIP_STATE_ENUM *slipStateP, 
                    Hci3WErrStruct  *errObjP);

/*---------------------------------------------------------------------------
 * h3wDatSlipSetSpeed()
 *     This function redefines the speed at which the Slip Layer is to 
 *     operate.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire data frame module context structure.  
 *     bps - This is the new bits per second rate that is to be used.
 *     errObjP - A pointer to an error context structure.  
 *
 * Returns:
 *     This function returns TRUE if it was successfully able to set the Slip 
 *     Layer data transfer speed, or FALSE otherwise.  
 */
BOOL h3wDatSlipSetSpeed(Hci3WDatStruct *cP, U32 bps, Hci3WErrStruct *errObjP);

/*---------------------------------------------------------------------------
 * h3wDatSlipActivated()
 *     When an HCI 3-wire session finishes linking successfully, invoking 
 *     this function sets the data layer state data to the appropriate values.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire data frame module context structure.  
 *     slipStateP - This parameter must point to a location where a slip 
 *         state enumeration can be written.  
 */
void h3wDatSlipActivated(Hci3WDatStruct *cP, SLIP_STATE_ENUM *slipStateP);

/*---------------------------------------------------------------------------
 * h3wDatSlipInit()
 *     This function signals the lower slip layer that it should begin 
 *     initializing the UART, and sets the data layer state data to the 
 *     appropriate values.
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire data frame module context structure.  
 *     slipStateP - This parameter must point to a location where a slip 
 *         state enumeration can be written.  
 */
void h3wDatSlipInit(Hci3WDatStruct  *cP, 
                    SLIP_STATE_ENUM *slipStateP);

/*---------------------------------------------------------------------------
 * h3wDatSlipLinking()
 *     When an HCI 3-wire link operation commences, invoking this function 
 *     sets the data layer state data to the appropriate values.  This 
 *     functions signals the lower slip layer that linking has commenced.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire data frame module context structure.  
 *     slipStateP - This parameter must point to a location where a slip 
 *         state enumeration can be written.  
 */
void h3wDatSlipLinking(Hci3WDatStruct *cP, SLIP_STATE_ENUM *slipStateP);

/*---------------------------------------------------------------------------
 * h3wDatSlipResetLink()
 *     When an active HCI 3-wire session is reset, invoking this function 
 *     sets the data layer state data to the appropriate values.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire data frame module context structure.  
 *     slipStateP - This parameter must point to a location where a slip 
 *         state enumeration can be written.  
 */
void h3wDatSlipResetLink(Hci3WDatStruct *cP, SLIP_STATE_ENUM *slipStateP);

/*---------------------------------------------------------------------------
 * h3wDatSlipTerminated()
 *     When an HCI 3-wire link is terminated, invoking this function sets the 
 *     data layer state data to the appropriate values.  This functions 
 *     signals the lower slip layer that link has terminated.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire data frame module context structure.  
 *     slipStateP - This parameter must point to a location where a slip 
 *         state enumeration can be written.  
 */
void h3wDatSlipTerminated(Hci3WDatStruct *cP, SLIP_STATE_ENUM *slipStateP);

/*---------------------------------------------------------------------------
 * h3wDatTerm()
 *     This function destroys a given slip module, cleaning up any allocated 
 *     resources.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire slip module context structure.  
 */
void h3wDatTerm(Hci3WDatStruct *cP);

#endif /* __HCI3W_H */
