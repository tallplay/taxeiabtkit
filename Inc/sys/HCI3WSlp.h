/****************************************************************************
 *
 * File:
 *     $Workfile:HCI3WSlp.h$ for iAnywhere Blue SDK, Version 2.2
 *     $Revision:9$
 *
 * Description: This file is part of an HCI 3-Wire implementation.  It 
 * provides header information used by the code that handles the conversion 
 * of data between the framed format and the enslipped format, and interacts 
 * with the UART port.  
 *
 * Created:     October 31, 2006
 *
 * Copyright 2006-2007 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions,  
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#ifndef __HCI3WSlp_H
#define __HCI3WSlp_H

#include "UART.h"
#include "sys/HCI3WHdr.h"
#include "sys/HCI3WRXQ.h"

/****************************************************************************
 *
 * Types and Constants
 *
 ****************************************************************************/

typedef enum
{
    SD_UNKNOWN,
    SD_DELIMITER,
    SD_START_PACKET,
    SD_PACKET_DATA,
    SD_ESCAPE_SEQUENCE1,
    SD_ESCAPE_SEQUENCE2,
    SD_GARBLED_SEQUENCE,
    SD_END_PACKET
} DESLIP_STATE_ENUM;

typedef enum {
    US_UNINIT,
    US_PARTIAL_INIT,
    US_WORKING,
    US_PARTIAL_FREE
} UART_STATE_ENUM;

typedef enum {
    DS_CHAR_WAS_DECODED,
    DS_CONTINUE_TO_NEXT,
    DS_END_OF_PACKET
} DECODING_SLIP_ENUM;

/****************************************************************************
 *
 * Data Structures
 *
 ****************************************************************************/

typedef struct {
    BuffUseStruct *hP;
    BuffUseStruct *sP;
} SlipBuffStruct;

typedef struct {
    UART_STATE_ENUM state;
    BOOL            allowDualEvent;
} UartStruct;

typedef struct 
{
    DESLIP_STATE_ENUM deslipState;
    Hci3WEvtStruct   *privEvtObjP;
    Hci3WHdrStruct   *privHdrObjP;
    Hci3WMemStruct   *privMemObjP;
    Hci3WRxQStruct    slpRXQObj;
    Hci3WUtlStruct   *privUtlObjP;
    UartStruct        uart;
} Hci3WSlpStruct;

/****************************************************************************
 *
 * Function Reference
 *
 ****************************************************************************/

/*---------------------------------------------------------------------------
 * h3wSlpInit()
 *     Initializes the HCI 3-wire slip module.  This function must be invoked 
 *     before any of the other HCI 3-wire slip functions will work correctly.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire slip module context structure.  
 *     evtObjP - Pointer to an HCI 3-wire event handling module context 
 *         structure.  
 *     hdrObjP - Pointer to an HCI 3-wire frame header module context 
 *         structure.  
 *     memObjP - Pointer to an HCI 3-wire memory module context structure.  
 *     utlObjP - Pointer to an HCI 3-wire utilities module context structure.  
 */
void h3wSlpInit(Hci3WSlpStruct *cP,
                Hci3WEvtStruct *evtObjP,
                Hci3WHdrStruct *hdrObjP,
                Hci3WMemStruct *memObjP,
                Hci3WUtlStruct *utlObjP);

/*---------------------------------------------------------------------------
 * h3wSlpReceive()
 *     This function allocates memory for the received data, reads any left 
 *     over fragments from before, reads any newly arrived UART data, creates 
 *     a deslipped version of the slip encoded message, and then frees up the 
 *     newly allocated memory.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire slip module context structure.  
 *     buP - This parameter must point to a location where a buffer use 
 *         structure can be written.  
 *     errObjP - A pointer to an error context structure.
 *
 * Returns:
 *     This function returns TRUE if it was successfully able to receive a 
 *     message, or FALSE otherwise.  
 */
BOOL h3wSlpReceive(Hci3WSlpStruct *cP, 
                   BuffUseStruct  *buP, 
                   Hci3WErrStruct *errObjP);

/*---------------------------------------------------------------------------
 * h3wSlpResend()
 *     This function verifies that the semaphore is already held, sends the 
 *     given data, and clears the number of messages needing acknowledgements.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire slip module context structure.  
 *     h3wBuff - This parameter must point to a buffer to be transmitted.  
 *     h3wSize - This parameter must contain the size of the given buffer.  
 *     errObjP - A pointer to an error context structure.
 *
 * Returns:
 *     This function returns TRUE if it was successfully able to retransmit 
 *     the given message, or FALSE otherwise.  
 */
BOOL h3wSlpResend(Hci3WSlpStruct *cP, 
                  U8             *h3wBuff, 
                  U16             h3wSize, 
                  Hci3WErrStruct *errObjP);

/*---------------------------------------------------------------------------
 * h3wSlpSend()
 *     This function checks for conditionss that prevent transmissions, gets 
 *     enough memory to slip encode the given framed HCI 3-wire message, 
 *     creates a slip encoded version the message, invokes the UART write 
 *     routine, and then frees up the memory it just allocated.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire slip module context structure.  
 *     isLowPowerMsg - This parameter must be TRUE if the message is a low 
 *         power message, or FALSE otherwise.  
 *     h3wBuff - This parameter must point to an HCI 3-wire framed message.  
 *     h3wSize - This parameter must contain the size of the given buffer.  
 *     errObjP - A pointer to an error context structure.
 *
 * Returns:
 *     This function returns TRUE if it was successfully able to transmit the 
 *     message, or FALSE otherwise.  
 */
BOOL h3wSlpSend(Hci3WSlpStruct *cP, 
                BOOL            isLowPowerMsg,
                U8             *h3wBuff, 
                U16             h3wSize, 
                Hci3WErrStruct *errObjP);

/*---------------------------------------------------------------------------
 * h3wSlpUARTSetSpeed()
 *     This function redefines the speed at which the UART is to operate.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire slip module context structure.  
 *     bps - The new bits per second rate at which the UART is to operate.  
 *     errObjP - A pointer to an error context structure.
 *
 * Returns:
 *     This function returns TRUE if it was successfully able to set the UART 
 *     transmission speed, or FALSE otherwise.  
 */
BOOL h3wSlpUARTSetSpeed(Hci3WSlpStruct *cP, U32 bps, Hci3WErrStruct *errObjP);

/*---------------------------------------------------------------------------
 * h3wSlpUARTEventCallback()
 *     This function is invoked by the UART code when an event occurs that may 
 *     be of interest.  Unfortunately, we need some context information.  
 *     Since the UART software does not currently support this notion, we are 
 *     forced to work around it by using context data we store in a RAM 
 *     location.   Note that this workaround will only work in systems using a 
 *     single UART port.  If multiple UART ports are in use, then the UART 
 *     code will have to be modified to provide context information.  Once 
 *     this routine has the context data, it invokes the UART event handling 
 *     routine.  
 * 
 * Parameters:
 *     event - An enumeration corresponding to the event that occurred.  
 */
void h3wSlpUARTEventCallback(UartEvent event);

/*---------------------------------------------------------------------------
 * h3wSlpUARTFree()
 *     This function invokes the UART software shutdown routine.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire slip module context structure.  
 *     uartStateP - This parameter must point to a location where the 
 *         resulting UART state can be written.  
 */
void h3wSlpUARTFree(Hci3WSlpStruct *cP, UART_STATE_ENUM *uartStateP);

/*---------------------------------------------------------------------------
 * h3wSlpUARTInit()
 *     This function invokes the UART software initialization routine.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire slip module context structure.  
 *     uartStateP - This parameter must point to a location where the 
 *         resulting UART state can be written.  
 */
void h3wSlpUARTInit(Hci3WSlpStruct  *cP,
                    UART_STATE_ENUM *uartStateP);

/*---------------------------------------------------------------------------
 * h3wSlpUARTInitialized()
 *     This function invokes the routine that indicates the UART has been 
 *     successfully initialized.  It reports any resulting errors.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire slip module context structure.  
 *     uartStateP - This parameter must point to a location where the 
 *         resulting UART state can be written.  
 */
void h3wSlpUARTInitialized(Hci3WSlpStruct  *cP, 
                           UART_STATE_ENUM *uartStateP);

/*---------------------------------------------------------------------------
 * h3wSlpUARTTerminated()
 *     This function invokes the routine that indicates the UART has been 
 *     successfully shutdown.  It reports any resulting errors.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire slip module context structure.  
 *     uartStateP - This parameter must point to a location where the 
 *         resulting UART state can be written.  
 */
void h3wSlpUARTTerminated(Hci3WSlpStruct *cP, UART_STATE_ENUM *uartStateP);

/*---------------------------------------------------------------------------
 * h3wSlpTerm()
 *     This function destroys a given slip module, cleaning up any allocated 
 *     resources.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire slip module context structure.  
 */
void h3wSlpTerm(Hci3WSlpStruct *cP);

#endif /* __HCI3WSlp_H */
