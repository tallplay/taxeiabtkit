/****************************************************************************
 *
 * File:
 *     $Workfile:HCI3WTrn.h$ for iAnywhere Blue SDK, Version 2.2
 *     $Revision:12$
 *
 * Description: This file is part of an HCI 3-Wire implementation.  It 
 * provides header information used by the code that provides the transport 
 * layer interface between the upper HCI layer and the lower HCI 3-wire 
 * layer.  
 *
 * Created:     October 31, 2006
 *
 * Copyright 2006-2007 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions,  
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#ifndef __HCI3WTRN_H
#define __HCI3WTRN_H

#include "config.h"
#include "bttypes.h"
#include "hcitrans.h"
#include "sys/HCI3W.h"

/****************************************************************************
 *
 * Types and Constants
 *
 ****************************************************************************/

#define ACTION_Q_MAX (256)

#define MAX_HCI_FRAGMENTS (4)

/* This define must allow one slot for 
 * receives, transmits, and shutdowns.  
 */ 
#define MAX_WAKE_PENDING (3)

typedef enum {
    HR_INITIALIZE,
    HR_INITIALIZE_DONE,
    HR_LINK_UNEXPECTED,
    HR_WAKE,
    HR_RX,
    HR_HCI_RX_BUFF_AVAIL,
    HR_TX,
    HR_TX_FREE,
    HR_TERMINATE,
    HR_TERMINATE_DONE
} H3WMGR_REQ_ENUM;

typedef enum {
    HS_UNINITIALIZED,
    HS_MAX_PAYLOAD,
    HS_UART_MULT,
    HS_INITIALIZED,
    HS_LINKING,
    HS_ACTIVE,
    HS_WAKING,
    HS_NEED_HCI_RX_BUFF,
    HS_GIVE_RX_TO_HCI,
    HS_SHUTTING_DOWN
} H3WMGR_STATE_ENUM;

typedef enum {
    HE_FINISHED_SUCCESSFULLY,
    HE_PENDING,
    HE_BUSY,
    HE_ALREADY_INITIALIZED,
    HE_PAYLOAD_DEFINED,
    HE_UART_MULT_FAILURE,
    HE_UART_MULT_DEFINED,
    HE_GET_CONFIG_FAILURE,
    HE_NOT_LINKING,
    HE_ALREADY_LINKED,
    HE_NOT_ACTIVE,
    HE_NOT_SHUTDOWN,
    HE_PAYLOAD_MAX_FAILURE,
    HE_LINK_FAILURE,
    HE_BPS_FAILURE,
    HE_WAKE_FAILURE,
    HE_NOT_WAKING,
    HE_RX_FAILURE,
    HE_HCIGETBUF,
    HE_NO_BUFFER_NEEDED,
    HE_RX_TRUNCATED_ACL,
    HE_RX_TRUNCATED_SYNCH,
    HE_RX_BAD_PACKET_TYPE,
    HE_NOT_RETURNING_BUFF,
    HE_TX_PAYLOAD_TOO_LARGE,
    HE_TX_FAILURE,
    HE_TX_ACK_NOT_FOUND,
    HE_NOT_SHUTTING_DOWN,
    HE_SHUTDOWN_FAILURE
} H3WMGR_ERROR_ENUM;

/****************************************************************************
 *
 * Data Structures
 *
 ****************************************************************************/

typedef union {
    U8         hci3wHandle;
    HciPacket *hciPacket;
} DATA_UNION;

typedef struct {
    DATA_UNION      data;
    H3WMGR_REQ_ENUM req;
} ActionStruct;

typedef struct {
    HciPacket *hciPacket;
    U8         cNdx; /* An index to the child node */ 
    U8         hci3wHandle;
    U8         pNdx; /* An index to the parent node */ 
} TxNodeStruct;

typedef struct {
    H3WMGR_REQ_ENUM req;
    DATA_UNION      data;
} WakeStruct;

/*---------------------------------------------------------------------------
 * HCI3W_Context structure
 *
 *     The HCI 3-wire transport driver context contains 
 *     information for the HCI-ABCSP transport driver.
 */
typedef struct _HCI3W_Context 
{
    ActionStruct        actionQ[ACTION_Q_MAX];
    BOOL                cntrlrAsleep;
    BOOL                hciSynchronousFlowControl;
    BOOL                rxAwaitingHCIBuffer;
    ConfigOptionsStruct configured;
    H3WMGR_STATE_ENUM   h3wMgrState;
    Hci3WStruct         hci3wObj;
    ListEntry           txQPending;
    PACKET_TYPE_ENUM    rxPacketType;
    RxBuffHandle        rxBuff;
    EvmTimer            retryTX;
    EvmTimer            signalHCI;
    TranCallback        callback; /* Callback Entry point */ 
    TranEntry           tranEntry; /* Callback Entry point */ 
    TxNodeStruct        txQ[HCI3W_TXQ_MAX];
    U8                  aqrNdx;
    U8                  aqwNdx;
    U8                  h3wmgrBusy;
    U8                  hci3wRXBuff[HCI3W_MAX_PAYLOAD];
    U8                  hci3wTXBuff[HCI3W_MAX_PAYLOAD];
    U32                 reserved; //tallplay add
    U8                  memPool[HCI3W_MEM_POOL_SIZE];
    U8                  pendingReq;
    U8                  slidingWindowUsed;
    U8                  txRootFree;
    U8                  txRootToBeAcknowledged;
    U16                 rxBuffUsed;
    U16                 txBuffUsed;
    WakeStruct          wake[MAX_WAKE_PENDING];
} HCI3W_Context;

/* Define the appropriate external reference to the transport context,
 * if necessary.
 */
#if (XA_CONTEXT_PTR == XA_ENABLED)

extern HCI3W_Context *h3wxprt;
#define HCI3W(s) (h3wxprt->s)

#elif (XA_CONTEXT_PTR == XA_DISABLED)

extern HCI3W_Context h3wxprt;
#define HCI3W(s) (h3wxprt.s)

#endif /* XA_CONTEXT_PTR */ 

/****************************************************************************
 *
 * Function Reference
 *
 ****************************************************************************/

/*---------------------------------------------------------------------------
 * H3W_Init()
 *     This function is invoked by the upper HCI layer whenever the transport 
 *     layer needs to initialize the HCI 3-Wire transport and UART hardware 
 *     drivers.  
 * 
 * Parameters:
 *     tranCallback - This is the routine that should be invoked to signal the 
 *     upper HCI layers when a successful initialization or shutdown event has 
 *     occurred.  
 *
 * Returns:
 *     This function returns a Bluetooth Status enumeration indicating things 
 *     such as whether the operation succeeded, whether the operation is 
 *     currently pending, or whether the operation encountered some type of 
 *     error.  
 */
BtStatus H3W_Init(TranCallback tranCallback);

/*---------------------------------------------------------------------------
 * H3W_Shutdown()
 *     This function is invoked by the upper HCI layer whenever the transport 
 *     layer needs to be shut down.
 * 
 * Returns:
 *     This function returns a Bluetooth Status enumeration indicating things 
 *     such as whether the operation succeeded, whether the operation is 
 *     currently pending, or whether the operation encountered some type of 
 *     error.  
 */
BtStatus H3W_Shutdown(void);

#endif /* __HCI3WTRN_H */ 
