/****************************************************************************
 *
 * File:
 *     $Workfile:HCI3WUtl.h$ for iAnywhere Blue SDK, Version 2.2
 *     $Revision:14$
 *
 * Description: This file is part of an HCI 3-Wire implementation.  It 
 * provides header information for the HCI 3-wire utilities, including the 
 * shared data code, the CRC code, the event handling code, and the error 
 * handling code.  
 *
 * Created:     October 31, 2006
 *
 * Copyright 2006-2007 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions,  
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#ifndef __HCI3WUtl_H
#define __HCI3WUtl_H

#include "osapi.h"

/****************************************************************************
 *
 * Types and Constants
 *
 ****************************************************************************/

/* ####################################################################### */ 
/* #### The following defines are related to the utility functions.  ##### */ 
/* ####################################################################### */ 

#define UART_BITS_PER_DATA_OCTET 10

/* ####################################################################### */ 
/* ################## The following defines are related ################## */ 
/* ################## to the event handling functions.  ################## */ 
/* ####################################################################### */ 

#define EVENTQ_SIZE 256

/* ####################################################################### */ 
/* ################# The following enumerations are     ################## */ 
/* ################# related to the utility functions.  ################## */ 
/* ####################################################################### */ 

typedef enum
{
    SW_LOWEST = 0x0100,
    SW_LOWER          ,
    SW_LOW            ,
    SW_MID            ,
    SW_HIGH           ,
    SW_HIGHER         ,
    SW_HIGHEST
} SLIDING_WIN_ENUM ;

typedef enum 
{
    FL_OUT_OF_FRAME_SOFTWARE,
    FL_HCI_SYNCHRONOUS
} FLOW_LAYER_ENUM;

enum
{
    LAST_FLOW_LAYER = FL_HCI_SYNCHRONOUS,
    NUM_FLOW_LAYERS
};

typedef enum
{
    FT_CHOKE_ABILITY,
    FT_CURRENT_STATE
} FLOW_TYPE_ENUM;

enum
{
    LAST_FLOW_TYPE = FT_CURRENT_STATE,
    NUM_FLOW_TYPES
};

typedef enum
{
    SE_0, 
    SE_1, 
    SE_2, 
    SE_3, 
    SE_4, 
    SE_5, 
    SE_6, 
    SE_7  
} SEQ_ENUM;

typedef enum {
    H3_ACTIVATED,
    H3_LINK_LOST,
    H3_DATA_RECEIVED,
    H3_TRANSMIT_COMPLETE,
    H3_LOW_POWER_SLEEP,
    H3_LOW_POWER_WOKEN,
    H3_RELIABLY_SENT,
    H3_TERMINATED
} H3W_EVENT_ENUM;

typedef enum {
    HR_HOST,
    HR_CONTROLLER
} HCI3W_ROLE_ENUM;

typedef enum {
    OR_1,
    OR_2,
    OR_4,
    OR_8
} OVERCLOCK_ENUM;

/* ####################################################################### */ 
/* ############### The following enumerations are related ################ */ 
/* ############### to the event handling functions.       ################ */ 
/* ####################################################################### */ 

typedef enum {
    ET_UART_INIT,
    ET_LINK_TX_LINK,
    ET_LINK_TX_SYNC_RESP,
    ET_LINK_INIT,
    ET_LINK_TX_NOW,
    ET_LINK_INIT_TX_CONF_RESP,
    ET_LINK_ACTIVE,
    ET_LINK_RESET,
    ET_LINK_ACTIVE_TX_CONF_RESP,
    ET_LINK_ACTIVATED,
    ET_LINK_TERMINATED,
    ET_TX_RESEND_RELIABLE,
    ET_TX_PURE_ACK,
    ET_TX_WRITE_COMPLETE,
    ET_RX_UART,
    ET_RX_APP,
    ET_LOW_POWER_TX_WOKEN,
    ET_LOW_POWER_TX_WAKEUP,
    ET_LOW_POWER_SLEEP,
    ET_LOW_POWER_WOKEN,
    ET_UART_FREE
} EVENT_TYPE_ENUM;

enum {
    LAST_EVENT = ET_UART_FREE,
    NUM_EVENTS
};

/* ####################################################################### */ 
/* ############### The following enumerations are related ################ */ 
/* ############### to the error handling functions.       ################ */ 
/* ####################################################################### */ 

typedef enum
{
    ES_DAT_DEHCI = 1,
    ES_DAT_DEHCIBYTES,
    ES_DAT_ENHCI,
    ES_DAT_RECEIVE,
    ES_DAT_SEND,
    ES_H3W_CLOCKMULT,
    ES_H3W_DATAFREE,
    ES_H3W_DATAINIT,
    ES_H3W_GETCONFIG,
    ES_H3W_GETCONFIGE,
    ES_H3W_GETPLD,
    ES_H3W_HANDLEACTIVERX,
    ES_H3W_INIT,
    ES_H3W_RECEIVE,
    ES_H3W_RECEIVE_E,
    ES_H3W_SEND_E,
    ES_H3W_SENDREL,
    ES_H3W_SENDUNREL,
    ES_H3W_SETPLD,
    ES_H3W_SETSPEED,
    ES_H3W_SLEEP,
    ES_H3W_UARTTOBPS,
    ES_H3W_WAKEUP,
    ES_HDR_GETHDR,
    ES_HDR_SETHDR,
    ES_MEM_GETBUF,
    ES_SLP_DESLIP,
    ES_SLP_SEND,
    ES_SLP_UARTFREE,
    ES_SLP_UARTINIT,
    ES_SLP_UARTINITIALIZED,
    ES_UTL_BPSENCODE,
    ES_UTL_SEQITOE,
    ES_UTL_SETPLD
} ERR_SRC_ENUM;

typedef enum
{
    ER_ALREADY_INIT = 1,
    ER_BAD_HDR_CS,
    ER_BAD_PKT_RELIABILITY,
    ER_BAD_PKT_ROLE,
    ER_BPS_UNREPRESENTABLE,
    ER_BUF_TOO_SMALL,
    ER_BUSY,
    ER_CRC_ERROR,
    ER_INSUFFICIENT_MEMORY,
    ER_LINK_PROHIBITS_CRCS,
    ER_NOT_ACTIVE,
    ER_NOT_AWAKE,
    ER_NOT_INIT,
    ER_OUT_OF_RANGE,
    ER_OUT_OF_SEQUENCE,
    ER_PAYLOAD_INI,
    ER_PAYLOAD_SIZE_MISMATCH,
    ER_PAYLOAD_TOO_LARGE,
    ER_PAYLOAD_TOO_SMALL,
    ER_SENT_TOO_FEW,
    ER_SENT_TOO_MANY,
    ER_SLIDING_WINDOW_FULL,
    ER_TRUNCATED_HCI_MSG,
    ER_UART_FAILURE,
    ER_UART_SPEED,
    ER_UNKNOWN_PKT_TYPE,
    ER_XOFF
} ERR_ENUM;

/****************************************************************************
 *
 * Data Structures
 *
 ****************************************************************************/

/* ####################################################################### */ 
/* ### The following structures are related to the utility functions.  ### */ 
/* ####################################################################### */ 

typedef struct {
    U8 *buf;
    U16 used;
    U16 maxSize;
} BuffUseStruct;

typedef struct {
    U8 *src;
    U8 *dst;
} BuffPtrsStruct;

typedef struct {
    BOOL     valid;
    SEQ_ENUM ackNum;
} AckStruct;

typedef struct 
{
    AckStruct       lastRX;
    BOOL            afterIni;
    BOOL            flowControl[NUM_FLOW_LAYERS][NUM_FLOW_TYPES];
    BOOL            privMayUseCRC;
    BOOL            privOtherAssumedAwake;
    HCI3W_ROLE_ENUM privRole;
    TimeT           halfWrap;
    U8              privSlidingWindowSize;
    U8              privUARTClockMultiplier;
    U16             privMaxPayloadBytes;
    U32             privUARTBPS;
} Hci3WUtlStruct;

/* ####################################################################### */ 
/* ######## The following structures are related to the functions ######## */ 
/* ######## that perform cyclic redundancy check calculations.    ######## */ 
/* ####################################################################### */ 

typedef struct 
{
    U16 crcTable[256];
} Hci3WCrcStruct;

/* ####################################################################### */ 
/* ################ The following structures are related ################# */ 
/* ################ to the event handling functions.     ################# */ 
/* ####################################################################### */ 

typedef struct {
    EVENT_TYPE_ENUM type;
    U8              data;
} EventStruct;

typedef struct {
    void (*callBack)(void *, EventStruct);
    void *context;
} EcbStruct;

typedef struct {
    BOOL queueMultiple;
    BOOL currentlyQueued;
} QStateStruct;

typedef struct 
{
    EventStruct  eventQ[EVENTQ_SIZE];
    EcbStruct    privECB;
    QStateStruct qState[NUM_EVENTS];
    U8           rdNdx;
    U8           semaphoreCnt;
    U8           wrNdx;
} Hci3WEvtStruct;

/* ####################################################################### */ 
/* ################ The following structures are related ################# */ 
/* ################ to the error handling functions.     ################# */ 
/* ####################################################################### */ 

typedef struct 
{
    U32 msbErr;
    U32 lsbErr;
} Hci3WErrStruct;

/****************************************************************************
 *
 * Function Reference
 *
 ****************************************************************************/

/* ####################################################################### */ 
/* ################# The following public prototypes are ################# */ 
/* ################# related to the utility functions.   ################# */ 
/* ####################################################################### */ 

/*---------------------------------------------------------------------------
 * h3wUtlInit()
 *     Initializes the HCI 3-wire utilities module.  This function must be 
 *     invoked before any of the other HCI 3-wire utility functions will 
 *     work correctly.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire utilities module context structure.  
 *     allowCRC - This parameter indicates whether the HCI 3-wire layer 
 *         wants to support cyclic redundancy check calculations.  Whether 
 *         CRCs actually end up being used depends on whether the other side 
 *         also supports them.
 *     hciSynchFC - This parameter must be set to TRUE if HCI Flow Control is 
 *         in use.  
 *     role - Indicates whether the code is being run by an HCI 3-Wire Host 
 *         or by an HCI 3-Wire Controller.
 */
void            h3wUtlInit(Hci3WUtlStruct  *cP,
                           BOOL             allowCRC,
                           BOOL             hciSynchFC,
                           HCI3W_ROLE_ENUM  role,
                           SLIDING_WIN_ENUM slidingWindow);

/*---------------------------------------------------------------------------
 * h3wUtlBPStoUARTEncoding()
 *     Converts a given bits per second transfer rate into an encoding 
 *     expected by the lower UART layer software.
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire utilities module context structure.  
 *     uartSpeedEncodingP - A pointer to a location where the UART speed 
 *         encoding can be written.
 *     bps - The bits per second transfer rate.
 *     errObjP - A pointer to an error context structure.
 *
 * Returns:
 *     This function returns TRUE if the give bits per second rate were 
 *     successfully converted into a UART speed encoding, or FALSE otherwise.  
 */
BOOL            h3wUtlBPStoUARTEncoding(Hci3WUtlStruct *cP, 
                                        U16            *uartSpeedEncodingP,
                                        U32             bps,
                                        Hci3WErrStruct *errObjP);

/*---------------------------------------------------------------------------
 * h3wUtlGetFlowControl()
 *     Determines a flow control value for a given layer and type.
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire utilities module context structure.  
 *     layer - An enumeration corresponding either to the HCI flow control or 
 *         to the Out-Of-Frame Software flow control.
 *     type - An enumeration corresponding either to the choke ability or to 
 *         the current state of the flow control.  
 *
 * Returns:
 *     This function returns TRUE if the value for the given flow control 
 *     layer and type is enabled, or FALSE otherwise.  
 */
BOOL            h3wUtlGetFlowControl(Hci3WUtlStruct *cP,
                                     FLOW_LAYER_ENUM layer, 
                                     FLOW_TYPE_ENUM  type);

/*---------------------------------------------------------------------------
 * h3wUtlGetMayUseCRC()
 *     Determines whether cyclic redundancy checks can be used in HCI 3-wire 
 *     messages.
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire utilities module context structure.  
 *
 * Returns:
 *     This function returns TRUE if cyclic redundancy checks can be used in 
 *     HCI 3-wire messages, or FALSE otherwise.
 */
BOOL            h3wUtlGetMayUseCRC(Hci3WUtlStruct *cP);

/*---------------------------------------------------------------------------
 * h3wUtlGetOtherAssumedAwake()
 *     This function determines whether that the HCI 3-wire device being 
 *     communicated with is assumed to be awake.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire utilities module context structure.  
 *
 * Returns:
 *     This function returns TRUE if the HCI 3-wire device being communicated 
 *     with is assumed to be awake, or FALSE otherwise.  
 */
BOOL            h3wUtlGetOtherAssumedAwake(Hci3WUtlStruct *cP);

/*---------------------------------------------------------------------------
 * h3wUtlSeqU8ToE()
 *     Converts a numeric HCI 3-wire sequence number to its corresponding 
 *     enumeration value.
 * 
 * Parameters:
 *     asEnumP - This parameter must point to a sequence enumeration location 
 *         where the converted value can be written.  
 *     asU8 - This parameter must contain a numeric version of an HCI 3-wire 
 *         sequence number.  
 *     errObjP - A pointer to an error context structure.
 *
 * Returns:
 *     This function returns TRUE if it was successfully able to convert the 
 *     given numeric HCI 3-wire sequence number into an enumeration value, or 
 *     FALSE otherwise.  
 */
BOOL            h3wUtlSeqU8ToE(SEQ_ENUM       *asEnumP, 
                               U8              asU8, 
                               Hci3WErrStruct *errObjP);

/*---------------------------------------------------------------------------
 * h3wUtlSetMaxPayloadBytes()
 *     Sets the maximum number of payload bytes that can be transmitted in an 
 *     HCI 3-wire message before framing and slip encoding.
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire utilities module context structure.  
 *     maxPayloadBytes - The new value to be assigned to the maximum number of 
 *         payload bytes.
 *     errObjP - A pointer to an error context structure.
 *
 * Returns:
 *     This function returns TRUE if the maximum number of payload bytes were 
 *     successfully redefined, or FALSE otherwise.
 */
BOOL            h3wUtlSetMaxPayloadBytes(Hci3WUtlStruct *cP, 
                                         U16             maxPayloadBytes,
                                         Hci3WErrStruct *errObjP);

/*---------------------------------------------------------------------------
 * h3wUtlGetRole()
 *     Determines the HCI 3-wire role.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire utilities module context structure.  
 *
 * Returns:
 *     This function returns an enumeration corresponding either to the HCI 
 *     3-wire Host or to the HCI 3-wire Controller, depending on the current 
 *     role.  
 */
HCI3W_ROLE_ENUM h3wUtlGetRole(Hci3WUtlStruct *cP);

/*---------------------------------------------------------------------------
 * h3wUtlGetNextSeq()
 *     Given a current sequence number enumeration, this function determines 
 *     the next sequence number enumeration value.
 * 
 * Parameters:
 *     seqNum - An enumeration corresponding to an HCI 3-wire sequence number.  
 *
 * Returns:
 *     This function returns the next sequence number enumeration value for 
 *     the given sequence number.  
 */
SEQ_ENUM        h3wUtlGetNextSeq(SEQ_ENUM seqNum);

/*---------------------------------------------------------------------------
 * h3wUtlElapsed()
 *     Determines the number of clock ticks that have elapsed since a given 
 *     starting time.  This function correctly handles clock wraps.
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire utilities module context structure.  
 *     startTicks - The starting time from which the elapsed time is to be 
 *         determined.
 *
 * Returns:
 *     The number of clock ticks that have elapsed since the given starting 
 *     time..
 */
TimeT           h3wUtlElapsed(Hci3WUtlStruct *cP, TimeT startTicks);

/*---------------------------------------------------------------------------
 * h3wUtlGetSlidingWindowSize()
 *     Determines the HCI 3-wire sliding window size.
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire utilities module context structure.  
 *
 * Returns:
 *     This function returns the HCI 3-wire sliding window size.  
 */
U8              h3wUtlGetSlidingWindowSize(Hci3WUtlStruct *cP);

/*---------------------------------------------------------------------------
 * h3wUtlSeqEToU8()
 *     Converts a enumeration corresponding to an HCI 3-wire sequence number 
 *     to a numeric value.  
 * 
 * Parameters:
 *     seqNum - An enumeration corresponding to an HCI 3-wire sequence number.  
 *
 * Returns:
 *     This function returns a numeric HCI 3-wire sequence number 
 *     corresponding to the given enumeration value.  
 */
U8              h3wUtlSeqEToU8(SEQ_ENUM seqNum);

/*---------------------------------------------------------------------------
 * h3wUtlGetMaxPayloadBytes()
 *     Provides the maximum number of payload bytes that can be used in an HCI 
 *     3-wire message before framing and slip encoding.
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire utilities module context structure.  
 *
 * Returns:
 *     This function returns the maximum number of payload bytes that can be 
 *     used in an HCI 3-wire message before framing and slip encoding.
 */
U16             h3wUtlGetMaxPayloadBytes(Hci3WUtlStruct *cP);

/*---------------------------------------------------------------------------
 * h3wUtlUARTEncodingToBPS()
 *     Converts a given encoding expected by the lower UART layer software 
 *     into an bits per second transfer rate.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire utilities module context structure.  
 *     uartSpeedEncoding - The UART speed encoding that is to be converted 
 *         into a BPS rate.  
 *
 * Returns:
 *     This function returns the bits per second transfer rate derived from 
 *     the given encoding expected by the lower UART layer software.  
 */
U32             h3wUtlUARTEncodingToBPS(Hci3WUtlStruct *cP, 
                                        U16             uartSpeedEncoding);

/*---------------------------------------------------------------------------
 * h3wUtlGetTimeOneChar()
 *     Determines the time required to transmit a byte of data.
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire utilities module context structure.  
 *
 * Returns:
 *     This function returns the value of Tmax in milliseconds, which is 
 *     normally calculated from the data transfer rate.  However, this 
 *     function never returns a value less than 1.
 */
U32             h3wUtlGetTimeOneChar(Hci3WUtlStruct *cP);

/*---------------------------------------------------------------------------
 * h3wUtlGetTmax()
 *     Determines the value of Tmax.
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire utilities module context structure.  
 *
 * Returns:
 *     This function returns the value of Tmax in milliseconds, which is 
 *     calculated as per the HCI 3-wire specification from the maximum 
 *     payload size and the data transfer rate.  
 */
U32             h3wUtlGetTmax(Hci3WUtlStruct *cP);

/*---------------------------------------------------------------------------
 * h3wUtlGetUARTBPS()
 *     Determines the bits per second transfer rate.
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire utilities module context structure.  
 *
 * Returns:
 *     This function returns the number of bits that can be transferred per 
 *     second.  Note that this does not correspond to the amount of data that 
 *     can be transferred, becuase transfers include the UART start and stop 
 *     bits, and also include the overhead for HCI 3-wire framing and slip 
 *     encoding.  
 */
U32             h3wUtlGetUARTBPS(Hci3WUtlStruct *cP);

/*---------------------------------------------------------------------------
 * h3wUtlCopyBytes()
 *     Given a buffer pointers structure, this function copies bytes from the 
 *     source to the destination.
 * 
 * Parameters:
 *     bpP - Pointer to buffer pointers structure containing the source and 
 *         destination addresses.
 *     size - The number of bytes to copy
 */
void            h3wUtlCopyBytes(BuffPtrsStruct *bpP, U32 size);

/*---------------------------------------------------------------------------
 * h3wUtlGetLastRXAck()
 *     Determines the last acknowledgement number received since the last time 
 *     the HCI 3-wire link was established.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire utilities module context structure.  
 *     ackP - This parameter must point to an acknowledgement structure where 
 *         the current acknowledgement data can be written.  
 */
void            h3wUtlGetLastRXAck(Hci3WUtlStruct *cP, AckStruct *ackP);

/*---------------------------------------------------------------------------
 * h3wUtlSetFlowControl()
 *     Sets the flow control value for the given layer and type.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire utilities module context structure.  
 *     layer - An enumeration corresponding either to the HCI flow control or 
 *         to the Out-Of-Frame Software flow control.
 *     type - An enumeration corresponding either to the choke ability or to 
 *         the current state of the flow control.  
 */
void            h3wUtlSetFlowControl(Hci3WUtlStruct *cP,
                                     FLOW_LAYER_ENUM layer, 
                                     FLOW_TYPE_ENUM  type, 
                                     BOOL            value);

/*---------------------------------------------------------------------------
 * h3wUtlSetLastRXAck()
 *     Defines the last acknowledgement number to have been received.
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire utilities module context structure.  
 *     valid - This parameter must be TRUE if an acknowledgement number has 
 *         been received since the last time the HCI 3-wire link was 
 *         established, or FALSE otherwise.  
 *     ackNum - If an acknowledgement number has been received since the last 
 *         time the HCI 3-wire link was established, this parameter must 
 *         contain the last received acknowledgement number.  Otherwise, this 
 *         parameter has no meaning.  
 */
void            h3wUtlSetLastRXAck(Hci3WUtlStruct *cP, 
                                   BOOL            valid, 
                                   SEQ_ENUM        ackNum);

/*---------------------------------------------------------------------------
 * h3wUtlSetMayUseCRC()
 *     Sets the permission to use cyclic redundancy checks in HCI 3-wire 
 *     messages.
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire utilities module context structure.  
 *     mayUseCRC - The new permission value to be assigned.
 */
void            h3wUtlSetMayUseCRC(Hci3WUtlStruct *cP, BOOL mayUseCRC);

/*---------------------------------------------------------------------------
 * h3wUtlSetOtherAwake()
 *     Indicates whether the HCI 3-wire device being communicated with is to 
 *     be assumed to be awake or not.
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire utilities module context structure.  
 *     otherAssumedAwake - This parameter must be set to TRUE if the HCI 
 *         3-wire device being communicated with is to be assumed to be awake, 
 *         or FALSE otherwise.
 */
void            h3wUtlSetOtherAwake(Hci3WUtlStruct *cP, 
                                    BOOL            otherAssumedAwake);

/*---------------------------------------------------------------------------
 * h3wUtlSetSlidingWindow()
 *     Defines the HCI 3-wire sliding window size.
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire utilities module context structure.  
 *     slidingWindow - An enumeration value corresponding to the new sliding 
 *         window size.
 */
void            h3wUtlSetSlidingWindow(Hci3WUtlStruct  *cP, 
                                       SLIDING_WIN_ENUM slidingWindow);

/*---------------------------------------------------------------------------
 * h3wUtlSetUARTBPSSpeed()
 *     Defines the number of bits the UART can transfer in one second.  Note 
 *     that this does not correspond to the amount of data that can be 
 *     transferred, becuase transfers include the UART start and stop bits, 
 *     and also include the overhead for HCI 3-wire framing and slip encoding.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire utilities module context structure.  
 *     uartBPSSpeed - Number of bytes to copy
 */
void            h3wUtlSetUARTBPSSpeed(Hci3WUtlStruct *cP, 
                                      U32             uartBPSSpeed);

/*---------------------------------------------------------------------------
 * h3wUtlSetUARTMultiplier()
 *     Sets the value of the UART clock multiplier.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire utilities module context structure.  
 *     uartClockMultiplier - An enumeration corresponding to the amount the 
 *         UART speeds are overclocked.  
 */
void            h3wUtlSetUARTMultiplier(Hci3WUtlStruct *cP, 
                                        OVERCLOCK_ENUM  uartClockMultiplier);

/*---------------------------------------------------------------------------
 * h3wUtlTerm()
 *     This function destroys a given utilities module, cleaning up any 
 *     allocated resources.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire utilities module context structure.  
 */
void            h3wUtlTerm(Hci3WUtlStruct *cP);

/* ####################################################################### */ 
/* #### The following public prototypes are related to the functions ##### */ 
/* #### that perform cyclic redundancy check calculations.           ##### */ 
/* ####################################################################### */ 

/*---------------------------------------------------------------------------
 * h3wCRCInit()
 *     Initializes the HCI 3-wire cyclic redundancy check module.  This 
 *     function must be invoked before any of the other HCI 3-wire cyclic 
 *     redundancy check functions will work correctly.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire cyclic redundancy check module context 
 *         structure.  
 */
void h3wCRCInit(Hci3WCrcStruct *cP);

/*---------------------------------------------------------------------------
 * h3wCRCCalc()
 *     Calculates the HCI 3-wire cyclic redundancy value for a given data 
 *     buffer.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire cyclic redundancy check module context 
 *         structure.  
 *     dataP - Must point to a valid buffer containing an HCI 3-wire framed 
 *         message.  
 *     numOctets - Must contain the size of the given HCI 3-wire framed 
 *         buffer.  
 *
 * Returns:
 *     This function returns the cyclic redundancy value corresponding to the 
 *     given buffer.  Note that there are many different "flavors" of CRC 
 *     algorithms.  The value returned by this function corresponds to the 
 *     16-bit CCITT-CRC algorithm specified in the HCI 3-wire specification.  
 */
U16  h3wCRCCalc(Hci3WCrcStruct *cP, U8 *dataP, U16 numOctets);

/*---------------------------------------------------------------------------
 * h3wCRCTerm()
 *     This function destroys a given cyclic redundancy check module, cleaning 
 *     up any allocated resources.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire cyclic redundancy check module context 
 *         structure.  
 */
void h3wCRCTerm(Hci3WCrcStruct *cP);

/* ####################################################################### */ 
/* ############## The following public prototypes are       ############## */ 
/* ############## related to the event handling functions.  ############## */ 
/* ####################################################################### */ 

/*---------------------------------------------------------------------------
 * h3wEvtInit()
 *     Initializes the HCI 3-wire event handling module.  This function must 
 *     be invoked before any of the other HCI 3-wire event handling functions 
 *     will work correctly.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire event handling module context structure.  
 *     ecb - This parameter must contain a valid event callback structure.  
 */
void h3wEvtInit(Hci3WEvtStruct *cP, EcbStruct ecb);

/*---------------------------------------------------------------------------
 * h3wEvtGetSemaphore()
 *     This function attempts to obtain the event semaphore used to protect 
 *     the event handling queue.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire event handling module context structure.  
 *
 * Returns:
 *     This function returns TRUE if it was successfully able to obtain 
 *     ownership of the event queue semaphore, or FALSE otherwise.  
 */
BOOL h3wEvtGetSemaphore(Hci3WEvtStruct *cP);

/*---------------------------------------------------------------------------
 * h3wEvtProcess()
 *     This function processes the events on the event handling queue.  This 
 *     function must not be called unless the event handling semaphore is 
 *     owned by the invoking code.  After the pending events are processed, 
 *     this function releases the semaphore.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire event handling module context structure.  
 */
void h3wEvtProcess(Hci3WEvtStruct *cP);

/*---------------------------------------------------------------------------
 * h3wEvtQueue()
 *     This function pushes an event on the event queue.  If it is able to 
 *     obtain the semaphore protecting the queue, it then invokes the function 
 *     that processes the queued up events.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire event handling module context structure.  
 *     event - This parameter must contain the event structure that is to be 
 *         queued up.  
 */
void h3wEvtQueue(Hci3WEvtStruct *cP, EventStruct event);

/*---------------------------------------------------------------------------
 * h3wEvtTerm()
 *     This function destroys a given event handling module, cleaning up any 
 *     allocated resources.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire event handling module context structure.  
 */
void h3wEvtTerm(Hci3WEvtStruct *cP);

/* ####################################################################### */ 
/* ############## The following public prototypes are       ############## */ 
/* ############## related to the error handling functions.  ############## */ 
/* ####################################################################### */ 

/*---------------------------------------------------------------------------
 * h3wErrInit()
 *     Initializes the HCI 3-wire error handling module.  This function must 
 *     be invoked before any of the other HCI 3-wire error handling functions 
 *     will work correctly.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire error handling module context structure.  
 */
void h3wErrInit(Hci3WErrStruct *cP);

/*---------------------------------------------------------------------------
 * h3wErrHasNone()
 *     Determines whether an error has been set.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire error handling module context structure.  
 *
 * Returns:
 *     This function returns TRUE if no errors have been set for the given 
 *     error context, or FALSE otherwise.  
 */
BOOL h3wErrHasNone(Hci3WErrStruct *cP);

/*---------------------------------------------------------------------------
 * h3wErrGet()
 *     This function determines the numeric value corresponding to the error 
 *     state of the given error context.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire error handling module context structure.  
 *
 * Returns:
 *     This function returns zero if no errors have been set for the given 
 *     error context.  Otherwise, it returns a non-zero numeric encoding of 
 *     the error value.  
 */
U32  h3wErrGet(Hci3WErrStruct *cP);

/*---------------------------------------------------------------------------
 * h3wErrReportUnsolicited()
 *     This function generates reports for errors detected by code that was 
 *     not directly solicited by the upper layers.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire error handling module context structure.  
 *     errOnly - This parameter must be TRUE if only errors are to be 
 *         reported, or FALSE if both errors and non-error conditions are to 
 *         be reported.  
 *     operationStr - This parameter must point to a short string describing 
 *         the non-solicited operation.  
 */
void h3wErrReportUnsolicited(Hci3WErrStruct *cP, 
                             BOOL            errOnly, 
                             char           *operationStr);

/*---------------------------------------------------------------------------
 * h3wErrSet()
 *     Sets an error for the given error context.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire error handling module context structure.  
 *     errSrc - An enumeration that must correspond to the location where the 
 *         error was detected.  
 *     error - An enumeration that must correspond to the error condition.  
 */
void h3wErrSet(Hci3WErrStruct *cP, 
               ERR_SRC_ENUM    errSrc, 
               ERR_ENUM        error);

/*---------------------------------------------------------------------------
 * h3wErrTerm()
 *     This function destroys a given error handling module, cleaning up any 
 *     allocated resources.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire error handling module context structure.  
 */
void h3wErrTerm(Hci3WErrStruct *cP);

#endif /* __HCI3WUtl_H */ 
