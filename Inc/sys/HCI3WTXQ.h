/****************************************************************************
 *
 * File:
 *     $Workfile:HCI3WTXQ.h$ for iAnywhere Blue SDK, Version 2.2
 *     $Revision:12$
 *
 * Description: This file is part of an HCI 3-Wire implementation.  It 
 * provides header information used by the code that deals with messages 
 * stored and retrieved from a transmit queue, including reliable message 
 * retransmissions.  
 *
 * Created:     October 31, 2006
 *
 * Copyright 2006-2007 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions,  
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#ifndef __HCI3WTXQ_H
#define __HCI3WTXQ_H

#include "osapi.h"
#include "sys/HCI3WHdr.h"
#include "sys/HCI3WMem.h"
#include "sys/HCI3WSlp.h"
#include "sys/HCI3WUtl.h"

/****************************************************************************
 *
 * Types and Constants
 *
 ****************************************************************************/

/****************************************************************************
 *
 * Data Structures
 *
 ****************************************************************************/

typedef struct {
    BOOL  ptrIsValid;
    TimeT enqueuedAtTicks;
    U8    handle;
    U8   *ptr;
} TxQNodeHdrStruct;

typedef struct
{
    BuffPtrsStruct   bufPtrs;
    EvmTimer         timer;
    Hci3WCrcStruct  *privCRCObjP;
    Hci3WEvtStruct  *privEvtObjP;
    Hci3WHdrStruct  *privHdrObjP;
    Hci3WMemStruct  *privMemObjP;
    Hci3WSlpStruct  *privSlpObjP;
    Hci3WUtlStruct  *privUtlObjP;
    TxQNodeHdrStruct root;
    U8               handleCntr;
} Hci3WTxQStruct;

/****************************************************************************
 *
 * Function Reference
 *
 ****************************************************************************/

/*---------------------------------------------------------------------------
 * h3wTXQInit()
 *     Initializes the HCI 3-wire transmit queue module.  This function must 
 *     be invoked before any of the other HCI 3-wire transmit queue functions 
 *     will work correctly.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire transmit queue module context structure.  
 *     crcObjP - Pointer to an HCI 3-wire cyclic redundancy check module 
 *         context structure.  
 *     evtObjP - Pointer to an HCI 3-wire event handling module context 
 *         structure.  
 *     hdrObjP - Pointer to an HCI 3-wire frame header module context 
 *         structure.  
 *     memObjP - Pointer to an HCI 3-wire memory module context structure.  
 *     slpObjP - Pointer to an HCI 3-wire slip module context structure.  
 *     utlObjP - Pointer to an HCI 3-wire utilities module context structure.  
 */
void h3wTXQInit(Hci3WTxQStruct *cP,
                Hci3WCrcStruct *crcObjP,
                Hci3WEvtStruct *evtObjP, 
                Hci3WHdrStruct *hdrObjP, 
                Hci3WMemStruct *memObjP, 
                Hci3WSlpStruct *slpObjP,
                Hci3WUtlStruct *utlObjP);

/*---------------------------------------------------------------------------
 * h3wTXQPush()
 *     Pushes an message onto the transmit queue.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire transmit queue module context structure.  
 *     handleP - a pointer to a location where a transmit queue handle can be 
 *         written.  
 *     h3wBuff - a pointer to the message buffer that is to be pushed on the 
 *         queue.  
 *     size - the size of the given message buffer.  
 *     errObjP - A pointer to an error context structure.
 *
 * Returns:
 *     This function returns TRUE if it was successfully able to push the 
 *     transmit message onto the transmit queue, or FALSE otherwise.  
 */
BOOL h3wTXQPush(Hci3WTxQStruct *cP,
                U8             *handleP, 
                U8             *h3wBuff, 
                U16             size, 
                Hci3WErrStruct *errObjP);

/*---------------------------------------------------------------------------
 * h3wTXQResendReliable()
 *     This function loops through the transmit queue, freeing messages that 
 *     have been acknowledged, and retransmitting messages that have not.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire transmit queue module context structure.  
 *     ack - An acknowledgement structure that must contain the most recently 
 *         received acknowledgement information.  
 *     h3CallBack - This must point at the callback routine that is to be 
 *         invoked anytime a message has been successfully acknowledged and 
 *         removed from the transmit queue.  
 *     errObjP - A pointer to an error context structure.
 *
 * Returns:
 *     This function returns TRUE if it was successfully able to resend or 
 *     free the nodes on the transmit queue, or FALSE otherwise.  
 */
BOOL h3wTXQResendReliable(Hci3WTxQStruct *cP, 
                          AckStruct       ack, 
                          void          (*h3CallBack)(H3W_EVENT_ENUM, 
                                                      U8), 
                          Hci3WErrStruct *errObjP);

/*---------------------------------------------------------------------------
 * h3wTXQNumNodesInQ()
 *     Determines how many messages are currently in the transmit queue.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire transmit queue module context structure.  
 *
 * Returns:
 *     This function returns the number of messages in the transmit queue.  
 */
U8   h3wTXQNumNodesInQ(Hci3WTxQStruct *cP);

/*---------------------------------------------------------------------------
 * h3wTXQFree()
 *     Frees up the resources associated with the transmit queue.  Any 
 *     messages on the queue are dropped.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire transmit queue module context structure.  
 *     errObjP - A pointer to an error context structure.
 */
void h3wTXQFree(Hci3WTxQStruct *cP, Hci3WErrStruct *errObjP);

/*---------------------------------------------------------------------------
 * h3wTXQTerm()
 *     This function destroys a given transmit queue module, cleaning up any 
 *     allocated resources.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire transmit queue module context structure.  
 */
void h3wTXQTerm(Hci3WTxQStruct *cP);

/*---------------------------------------------------------------------------
 * h3wTXQTimerEventCallback()
 *     This function is invoked when the transmit queue timer fires.  If it 
 *     is given a valid context, it invokes the transmit queue timer event 
 *     code.  
 * 
 * Parameters:
 *     evmTimer - The event manager structure corresponding to the timer that 
 *         just fired.  
 */
void h3wTXQTimerEventCallback(struct _EvmTimer *evmTimerP);

#endif /* __HCI3WTXQ_H */ 
