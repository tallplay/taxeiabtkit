/****************************************************************************
 *
 * File:
 *     $Workfile:HCI3WHdr.h$ for iAnywhere Blue SDK, Version 2.2
 *     $Revision:10$
 *
 * Description: This file is part of an HCI 3-Wire implementation.  It 
 * provides header information used by the code that handles the frame header 
 * information.  
 *
 * Created:     October 31, 2006
 *
 * Copyright 2006-2007 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions,  
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#ifndef __HCI3WHdr_h
#define __HCI3WHdr_h

#include "eventmgr.h"
#include "sys/HCI3WUtl.h"

/****************************************************************************
 *
 * Types and Constants
 *
 ****************************************************************************/

typedef enum {
    PT_ACK,
    PT_CMD,
    PT_ACL,
    PT_SYNCH,
    PT_EVENT,
    PT_VENDOR,
    PT_LINK
} PACKET_TYPE_ENUM;

/****************************************************************************
 *
 * Data Structures
 *
 ****************************************************************************/

typedef struct {
    BOOL             includesCRC;
    BOOL             sentReliably;
    U16              payldLen;
    PACKET_TYPE_ENUM packetType;
    SEQ_ENUM         seqNum;
    SEQ_ENUM         ackNum;
    U8               hdrChecksum;
} FrameHdrStruct;

typedef struct 
{
    EvmTimer        timer;
    Hci3WEvtStruct *privEvtObjP;
    Hci3WUtlStruct *privUtlObjP;
    U16             numRXNeedingACK;
    SEQ_ENUM        rxSequenceNum;
    SEQ_ENUM        txSequenceNum;
    U8              maxValue[7];
    U8              andMask[8][8];
} Hci3WHdrStruct;

/****************************************************************************
 *
 * Function Reference
 *
 ****************************************************************************/

/*---------------------------------------------------------------------------
 * h3wHdrInit()
 *     Initializes the HCI 3-wire frame header module.  This function must 
 *     be invoked before any of the other HCI 3-wire frame header functions 
 *     will work correctly.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire frame header module context structure.  
 *     evtObjP - Pointer to an HCI 3-wire event handling module context 
 *         structure.  
 *     utlObjP - Pointer to an HCI 3-wire utilities module context structure.  
 */
void h3wHdrInit(Hci3WHdrStruct *cP, 
                Hci3WEvtStruct *evtObjP, 
                Hci3WUtlStruct *utlObjP);

/*---------------------------------------------------------------------------
 * h3wHdrAllAcknowledged()
 *     This function indicates whether there are messages needing 
 *     acknowledgement or not.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire frame header module context structure.  
 *
 * Returns:
 *     This function returns TRUE if there are no messages needing 
 *     acknowledgement, or FALSE otherwise.  
 */
BOOL h3wHdrAllAcknowledged(Hci3WHdrStruct *cP);

/*---------------------------------------------------------------------------
 * h3wHdrAtSlidingLimit()
 *     This function determines whether the sliding window limit has been 
 *     reached.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire frame header module context structure.  
 *     numNodesInTXQ - This parameter must be set to the number of nodes the 
 *         the transmit queue.  
 *
 * Returns:
 *     This function returns TRUE if the sliding window limit has been 
 *     reached, or FALSE otherwise.  
 */
BOOL h3wHdrAtSlidingLimit(Hci3WHdrStruct *cP, U8 numNodesInTXQ);

/*---------------------------------------------------------------------------
 * h3wHdrGet()
 *     This function extracts header information from a given buffer.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire frame header module context structure.  
 *     sourcePtr - This parameter must point to a buffer containing an HCI 
 *         3-wire framed message.  
 *     hdrInfoP - This parameter must point to a location where a frame 
 *         header structure can be written.  
 *     isNewActiveRX - This parameter must be set to TRUE if the given buffer 
 *         was newly received, or FALSE otherwise.  
 *     errObjP - A pointer to an error context structure.
 *
 * Returns:
 *     This function returns TRUE if it was successfully able to extract the 
 *     header data from the given buffer, or FALSE otherwise.  
 */
BOOL h3wHdrGet(Hci3WHdrStruct *cP,
               U8             *sourcePtr, 
               FrameHdrStruct *hdrInfoP,
               BOOL            isNewActiveRX,
               Hci3WErrStruct *errObjP);

/*---------------------------------------------------------------------------
 * h3wHdrInSlidingWin()
 *     This function determines if a header structure contains a sequence 
 *     number that is within the sliding window.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire frame header module context structure.  
 *     hdrOldP - This parameter must point to a frame header structure 
 *         containing the sequence number that is to be tested.  
 *     ackNum - This is the acknowledgement number that is used to determine 
 *         the position of the sliding window.  
 *
 * Returns:
 *     This function returns TRUE if the sequence number in the given header 
 *     is within the sliding window positioned by the given acknowledgement 
 *     number.  Otherwise, it returns FALSE.  
 */
BOOL h3wHdrInSlidingWin(Hci3WHdrStruct *cP, 
                        FrameHdrStruct *hdrOldP, 
                        SEQ_ENUM        ackNum);

/*---------------------------------------------------------------------------
 * h3wHdrPureACKWindowFull()
 *     This function determines whether the sliding window is full indicating 
 *     the need for a pure acknowledgement message.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire frame header module context structure.  
 *
 * Returns:
 *     This function returns TRUE if the sliding window is full, or FALSE 
 *     otherwise.  
 */
BOOL h3wHdrPureACKWindowFull(Hci3WHdrStruct *cP);

/*---------------------------------------------------------------------------
 * h3wHdrSet()
 *     This function sets the header of an HCI 3-wire framed message buffer 
 *     from the given parameters.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire frame header module context structure.  
 *     destP - This parameter must point to an HCI 3-wire framed message 
 *         buffer where the header can be written.  
 *     sendReliably - This parameter must be set to TRUE if the given buffer 
 *         is for a reliable message, or FALSE otherwise.  
 *     includeCRC - This parameter must be set to TRUE if a CRC will be 
 *         appended to the end of the given HCI 3-wire framed message, or 
 *         FALSE otherwise.  
 *     packetType - This parameter must be set to an enumeration 
 *         corresponding to the packet type.  
 *     payldLen - This parameter must be set to the size of the payload in 
 *         the given HCI 3-wire framed buffer.  
 *     errObjP - A pointer to an error context structure.
 *
 * Returns:
 *     This function returns TRUE if it was successfully able to set the 
 *     header bytes, or FALSE otherwise.  
 */
BOOL h3wHdrSet(Hci3WHdrStruct  *cP, 
               U8              *destP, 
               BOOL             sendReliably, 
               BOOL             includeCRC,
               PACKET_TYPE_ENUM packetType, 
               U16              payldLen,
               Hci3WErrStruct  *errObjP);

/*---------------------------------------------------------------------------
 * h3wHdrClearNumRXNeedingACK()
 *     This function clears the number of messages needing acknowledgement.  
 *     It should be called anytime a transmission occurs, since transmission 
 *     messages always acknowledge all outstanding messages.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire frame header module context structure.  
 */
void h3wHdrClearNumRXNeedingACK(Hci3WHdrStruct *cP);

/*---------------------------------------------------------------------------
 * h3wHdrFree()
 *     This function frees up any resources allocated by the frame header 
 *     module.  It cancels the pure acknowledgement timer.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire frame header module context structure.  
 */
void h3wHdrFree(Hci3WHdrStruct *cP);

/*---------------------------------------------------------------------------
 * h3wHdrIncrNumRXNeedingACK()
 *     This function increments the number of received messages needing 
 *     acknowledgement, and initiates the pure acknowledgement timer when 
 *     appropriate.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire frame header module context structure.  
 */
void h3wHdrIncrNumRXNeedingACK(Hci3WHdrStruct *cP);

/*---------------------------------------------------------------------------
 * h3wHdrResetSeqAck()
 *     This function resets the sequence and acknowledgement numbers to the 
 *     state they should be in right after the link establishment.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire frame header module context structure.  
 */
void h3wHdrResetSeqAck(Hci3WHdrStruct *cP);

/*---------------------------------------------------------------------------
 * h3wHdrTimerEventCallback()
 *     This function is invoked when the frame header timer fires.  If it is 
 *     given a valid context, it invokes the frame header timer event code.  
 * 
 * Parameters:
 *     evmTimerP - The event manager structure corresponding to the timer that 
 *         just fired.  
 */
void h3wHdrTimerEventCallback(struct _EvmTimer *evmTimerP);

/*---------------------------------------------------------------------------
 * h3wHdrUpdateACK()
 *     This function updates the acknowledgement number in a given header 
 *     byte, and recalculates the header checksum.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire frame header module context structure.  
 *     hdrOldP - This parameter must point to a frame header structure 
 *         containing the extracted header information for the given 
 *         destination.  
 *     destP - This parameter must point to a buffer containing an HCI 3-wire 
 *         framed message that can be updated.  
 */
void h3wHdrUpdateACK(Hci3WHdrStruct *cP, 
                     FrameHdrStruct *hdrOldP, 
                     U8             *destP);

/*---------------------------------------------------------------------------
 * h3wHdrTerm()
 *     This function destroys a given framed header module, cleaning up any 
 *     allocated resources.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire frame header module context structure.  
 */
void h3wHdrTerm(Hci3WHdrStruct *cP);

#endif /* __HCI3WHdr_h */ 

