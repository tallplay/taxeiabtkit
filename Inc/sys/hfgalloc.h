/****************************************************************************
 *
 * File:
 *     $Workfile:hfgalloc.h$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:6$
 *
 * Description: This file contains the memory allocation definitions
 *              the Hands-free profile.
 *             
 * Created:     April 28, 2005
 *
 * Copyright 2001-2005 Extended Systems, Inc.

 * Portions copyright 2005 iAnywhere Solutions, Inc.

 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any means, 
 * or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions,  
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#ifndef __HFGALLOC_H_
#define __HFGALLOC_H_

#include "sys/hfgi.h"

/*---------------------------------------------------------------------------
 * HandsFree structure
 *
 * The HandsFree structure contains all of the variables used in handfree.c
 *
 */
typedef struct _BtHfgContext {


    /* List of registered channels */
    ListEntry       channelList;

    /* RFCOMM variables */
    RfService         hfgService;
    RfChannel        *hfgChannel;
    U8                regCount;
#if BT_SECURITY == XA_ENABLED
    BtSecurityRecord  hfgSecRec;
#endif
    RfChannel         rfChannels[NUM_BT_DEVICES * 2 + 2];
    ListEntry         freeRfChannelList;

    /* Device Manager Variables */
    CmgrHandler       cmgrHandler;

    /* Hands-Free SDP Variables */
    U8                hfgSdpProtoDescList[14];
    U8                hfgSdpSupportedFeatures[3];
    SdpAttribute      hfgSdpAttribute[8];
    SdpRecord         hfgSdpRecord;

    /* AT Parser info */
    AtContext         atContext;

    /* State Table */
    HfgState           hfgState[4];

} BtHfgContext;

#if HFG_MEMORY_EXTERNAL == XA_DISABLED
#if XA_CONTEXT_PTR == XA_ENABLED
extern BtHfgContext *hfgContext;
#define HFG(s) (hfgContext->s)
#else /* XA_CONTEXT_PTR */
extern BtHfgContext hfgContext;
#define HFG(s) (hfgContext.s)
#endif /* XA_CONTEXT_PTR */
#endif

BOOL HfgAlloc(void);

#endif /* __HFGALLOC_H_ */
