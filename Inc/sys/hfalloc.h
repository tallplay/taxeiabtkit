/****************************************************************************
 *
 * File:
 *     $Workfile:hfalloc.h$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:6$
 *
 * Description: This file contains internal memory allocation definitiions for
 *     the Hands-free unit.
 *             
 * Created:     April 28, 2005
 *
 * Copyright 2000-2005 Extended Systems, Inc.

 * Portions copyright 2005 iAnywhere Solutions, Inc.

 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any means, 
 * or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions,  
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#ifndef __HFALLOC_H_
#define __HFALLOC_H_

#include "sys/hfi.h"

#define MAX_NUM_IND 16

/* The BtHfContext structure contains the global context of the Hands-free SDK. */
typedef struct _BtHfContext {

    /* List of registered channels */
    ListEntry       channelList;

    /* RFCOMM variables */
    RfService         hfService;
    RfChannel        *hfChannel;
    U8                regCount;
#if BT_SECURITY == XA_ENABLED
    BtSecurityRecord  hfSecRec;
#endif
    RfChannel         rfChannels[NUM_BT_DEVICES * 2 + 2];
    ListEntry         freeRfChannelList;

    /* Device Manager Variables */
    CmgrHandler       cmgrHandler;

    /* Hands-Free SDP Variables */
    U8              hfSdpProtoDescList[14];
    U8              hfSdpSupportedFeatures[3];
    SdpAttribute    hfSdpAttribute[7];
    SdpRecord       hfSdpRecord;

    /* AT Parser info */
    AtContext       atContext;
    U8              indMap[MAX_NUM_IND];
    U8              numInd;

    /* State Table */
    HfState        hfState[5];

    /* Command Prescan */
    HfCmdOverride  cmdOverride;
} BtHfContext;

#if HF_MEMORY_EXTERNAL == XA_DISABLED
#if XA_CONTEXT_PTR == XA_ENABLED
extern BtHfContext *hfContext;
#define HF(s) (hfContext->s)
#else /* XA_CONTEXT_PTR == XA_ENABLED */
extern BtHfContext hfContext;
#define HF(s) (hfContext.s)
#endif /* XA_CONTEXT_PTR */
#endif /* HF_MEMORY_EXTERNAL == XA_DISABLED */

BOOL HfAlloc(void);

#endif /* __HFALLOC_H_ */

