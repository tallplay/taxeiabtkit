#ifndef __HIDPI_H_
#define __HIDPI_H_

#include "hidp.h"

/* The BtAvctpContext structure contains the global context of AVDTP. */
typedef struct _BtHidpContext {
    ListEntry     chnlList;
    HidConnState  ctlConnState[HID_CONN_NUM_STATES];
    L2capPsm      ctlpsm;

    HidConnState  intConnState[HID_CONN_NUM_STATES];
    L2capPsm      intpsm;
} BtHidpContext;

#if XA_CONTEXT_PTR == XA_ENABLED
#if 0
extern BtHidpContext *hidpContext;
#define HIDP(s) (hidpContext->s)
#endif
#else /* XA_CONTEXT_PTR == XA_ENABLED */
extern BtHidpContext hidpContext;
#define HIDP(s) (hidpContext.s)
#endif /* XA_CONTEXT_PTR */

BOOL HidpAlloc(void);

#endif /* __AVCTPI_H_ */


