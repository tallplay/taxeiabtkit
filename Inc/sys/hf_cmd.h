/****************************************************************************
 *
 * File:
 *     $Workfile:hf_cmd.h$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:3$
 *
 * Description: This file contains the header file for the command queuing 
 *              functions of the Hands-free SDK.
 *             
 * Created:     February 11, 2005
 *
 * Copyright 2000-2005 Extended Systems, Inc.

 * Portions copyright 2005 iAnywhere Solutions, Inc.

 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any means, 
 * or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions,  
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#ifndef __HF_CMD_H_
#define __HF_CMD_H_

#include "sys/hfi.h"

BtStatus HfAddCommand(HfChannel *Channel, HfCommand *Command);
BtStatus HfRemoveCommand(HfChannel *Channel, HfCommand *Command);
HfCommand * HfEnumCommands(HfChannel *Channel, HfCommand *Command);
BtStatus HfCompleteCommand(HfChannel *Channel, HfCommand *Command);
void HfFlushCommands(HfChannel *Channel, BtStatus Status);
HfCommand * HfGetCurrentCommand(HfChannel *Channel);
BtStatus HfExecuteNextCommand(HfChannel *Channel);

#endif /* __HF_CMD_H_ */

