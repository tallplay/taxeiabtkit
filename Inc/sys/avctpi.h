/****************************************************************************
 *
 * File:
 *     $Workfile:avctpi.h$ for iAnywhere AV SDK, version 1.2.3
 *     $Revision:6$
 *
 * Description: This file contains internal definitions for the Audio/Video
 *     Remote Control Transport Protocol (AVCTP).
 *             
 * Created:     Mar 11, 2004
 *
 * Copyright 2004 - 2005 Extended Systems, Inc.

 * Portions copyright 2005-2006 iAnywhere Solutions, Inc.

 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any means, 
 * or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#ifndef __AVCTPI_H_
#define __AVCTPI_H_

#include "avctp.h"

/* The BtAvctpContext structure contains the global context of AVDTP. */
typedef struct _BtAvctpContext {
    ListEntry       chnlList;
    AvctpConnState  connState[AVCTP_CONN_NUM_STATES];
    L2capPsm        psm;
} BtAvctpContext;

#if XA_CONTEXT_PTR == XA_ENABLED
extern BtAvctpContext *avctpContext;
#define AVCTP(s) (avctpContext->s)
#else /* XA_CONTEXT_PTR == XA_ENABLED */
extern BtAvctpContext avctpContext;
#define AVCTP(s) (avctpContext.s)
#endif /* XA_CONTEXT_PTR */

BOOL AvctpAlloc(void);
BtStatus AvctpMsgSendCommand(AvctpChannel *chnl);
void AvtpMsgInit(AvctpChannel *Chnl);

#endif /* __AVCTPI_H_ */

