#ifndef __HID_H_
#define __HID_H_

#include "hidp.h"
#include "conmgr.h"

/*---------------------------------------------------------------------------
 * HID API layer
 *
 *
 *     This API is designed to support Human Interface.
 */

/****************************************************************************
 *
 * Constants
 *
 ****************************************************************************/

#define HID_NUM_ATTRIBUTES 23

/****************************************************************************
 *
 * Types
 *
 ****************************************************************************/
/*---------------------------------------------------------------------------
 * HidEvent type
 *      
 */
typedef HidpEvent HidEvent;

/** The transport layer is connected and commands/responses can now
 *  be exchanged.
 * 
 *  During this callback, the 'p.remDev' parameter is valid.
 */
#define HID_EVENT_CONNECT             1

/** The application will receive this event when a lower layer connection
 *  (L2CAP) has been disconnected.  Both the target and controller of the
 *  connection are notified.
 * 
 */
#define HID_EVENT_DISCONNECT         2

/** A remote device is attempting to connect the transport layer.
 *  Only the acceptor of the connection is notified.  The acceptor may
 *  call HID_ConnectRsp() to either accept or reject the connection.  If
 *  HID_ConnectRsp() is not called, the connection is automatically
 *  accepted.
 * 
 *  During this callback, the 'p.remDev' parameter is valid.
 */
#define HID_EVENT_CONNECT_IND           3

/*A call to BT_HID_SendInterrupt() has been sent*/
#define HID_EVENT_INT_DATA_SENT 		4

#define HID_EVENT_LAST                  HID_EVENT_CONNECT_IND + 12

/* End of HidEvent */

/*---------------------------------------------------------------------------
 * Hid_Control type
 *
 * This defines the control type.
 */
typedef U8 hid_event_control;


#define HID_CONTROL_HARD_RESET            1
#define HID_CONTROL_SOFT_RESET            2
#define HID_CONTROL_SUSPEND               3
#define HID_CONTROL_EXIT_SUSPEND          4
#define HID_CONTROL_VIRTUAL_CABLE_UNPLUG  5

/*---------------------------------------------------------------------------
 * hid_report_type type
 *
 * This defines the HID report type.
 */
typedef U8 hid_report_type;

#define HID_REPORT_OTHER_TYPE    0
#define HID_REPORT_INPUT_TYPE    1
#define HID_REPORT_OUTPUT_TYPE   2
#define HID_REPORT_FEATURE_TYPE  3

/*---------------------------------------------------------------------------
 * hid_protocol_type type
 *
 * This type defines the HID protocols.
 */
typedef U8 hid_protocol_type;

#define HID_REPORT_PROTOCOL  1
#define HID_BOOT_PROTOCOL    0

/*---------------------------------------------------------------------------
 * hid_result_code type
 *
 * HID transactions return a status describing the success or failure of the
 * transaction.  This type describes all the possible result codes.
 */
typedef U8 hid_result_code;

#define HID_RESULT_CODE_SUCCESS              0
#define HID_RESULT_CODE_NOT_READY            1
#define HID_RESULT_CODE_INVALID_REPORT_ID    2
#define HID_RESULT_CODE_UNSUPPORTED_REQUEST  3
#define HID_RESULT_CODE_INVALID_PARAMETER    4
#define HID_RESULT_CODE_UNKNOWN              5
#define HID_RESULT_CODE_FATAL                6

/* Forward reference */
typedef struct _HidChannel HidChannel;
typedef struct _HidCallbackParms HidCallbackParms;

/*---------------------------------------------------------------------------
 * HidCallback type
 *
 * A function of this type is called to indicate events to the application.
 */
typedef void (*HidCallback)(HidChannel *chnl, HidCallbackParms *Parms);

/* End of HidCallback */

/****************************************************************************
 *
 * Data Structures
 *
 ****************************************************************************/

/*---------------------------------------------------------------------------
 * HidCallbackParms structure
 *
 * Contains information for the application callback event.
 */
struct _HidCallbackParms {
    /* HID event */
    HidEvent      event;

    /* HID channel associated with the event */
    HidChannel   *channel;

    /* Status of the operation */
    BtStatus      status;

    /* Callback parameter object */
    union {
        /* Remote device associated with the event */
        BtRemoteDevice *remDev;

    	/*report*/
    	struct{
    		hid_report_type report_type;    /* Report type (input, output, or feature) */		
    		U8           		*data;          /* Report data */
        	U16           		dataLen;       /* Length of the report data */
    	}hid_report;
    	
    	struct{
    	    hid_report_type report_type;    /* Report type (input, output, or feature) */
    	    BOOL          useId;         /* Set to TRUE if reportId should be used */
    	    U8            reportId;      /* The report ID (optional) */
    	
    	    /* Indicates the maximum amount of report data to return. If 0,
    	     * indicates a request to deliver all outstanding data.
    	     *
    	     * Note that in boot mode, this value must be increased by 1 to accomodate
    	     * the length of the Report ID field.
    	     *
    	     * A device that receives a report request with a non-zero buffer size
    	     * should respond with a report no longer than the specified bufferSize,
    	     * truncating if necessary.
    	     */
    	    U16           buffer_size;    
    	}hid_report_req; /*identify an HID report request*/

    	/*hid_interrupt*/
    	struct{
    		hid_report_type 	report_type;    /* Report type (input, output, or feature) */		
    		U8           		*data;          /* Contains a pointer to interrupt data */
        	U16           		dataLen;       /* Contains the length of interrupt data */
    	}hid_interrupt;
    } p;
};

/*---------------------------------------------------------------------------
 * HidChannel structure
 *
 * Defines the RCP channel.
 */
struct _HidChannel {
    /* === Internal use only === */

    /* Application callback function */
    HidCallback   callback;

    /* HIDP Channel */
    HidpChannel    chnl;

    /* Connection Manager Handler */
    CmgrHandler     cmgrHandler;    
};

/****************************************************************************
 *
 * Function Reference
 *
 ****************************************************************************/

/*---------------------------------------------------------------------------
 * HID_Init()
 *
 *     Initialize the HID SDK.  This function should only be called
 *     once, normally at initialization time.  The calling of this function
 *     can be specified in overide.h using the XA_LOAD_LIST macro
 *     (i.e. #define XA_LOAD_LIST XA_MODULE(HID) ... ).
 *
 * Returns:
 *     TRUE - Initialization was successful
 *
 *     FALSE - Initialization failed.
 */
BOOL HID_Init(void);

/*---------------------------------------------------------------------------
 * HID_Register()
 *
 *      Registers an application callback to receive HID events. This 
 *      function must be called before any other HID functions.
 *
 * Parameters:
 *
 *      chnl - Channel structure that receives or initiates connections.
 *
 *      callback - Identifies the application function that will be called
 *        with HID events.
 *
 * Returns:
 *      BT_STATUS_SUCCESS - The HID application callback Function was
 *      successfully registered.
 *
 *      BT_STATUS_IN_USE - The specified channel is already in use.
 *
 *      BT_STATUS_INVALID_PARM - The chnl or Callback parameter does not contain 
 *         a valid pointer. (XA_ERROR_CHECK only).
 */
BtStatus HID_Register(HidChannel *chnl, HidCallback callback);

/*---------------------------------------------------------------------------
 * HID_Deregister()
 *
 *      De-registers the HID callback. After making this call
 *      successfully, the callback specified in HID_Register will
 *      receive no further events.
 *
 * Parameters:
 *
 *      chnl - Channel structure that receives or initiates connections.
 *
 * Returns:
 *      BT_STATUS_SUCCESS - The HID callback was successfully deregistered.
 * 
 *      BT_STATUS_IN_USE - The specified channel is still in use.
 *
 *      BT_STATUS_NOT_FOUND - An HID callback was not previously registered.
 *     
 *      BT_STATUS_INVALID_PARM - The chnl parameter does not contain a valid 
 *         pointer. (XA_ERROR_CHECK only).
 */
BtStatus HID_Deregister(HidChannel *chnl);

/*---------------------------------------------------------------------------
 * HID_Connect()
 * 
 *     Initiates a connection to a remote HID device.  This function is 
 *     used to establish the lower layer connection (L2CAP), which allows
 *     sending messages.
 *
 *     If the connection attempt is successful, the HID_EVENT_CONNECT event
 *     will be received.  If the connection attempt is unsuccessful, the
 *     HID_EVENT_DISCONNECT event will be received.
 *
 * Parameters:
 *
 *      chnl - Channel structure that receives or initiates connections.
 *
 *      addr - The Bluetooth address of the device to which the connection 
 *             should be made. If 0, the connection manager is used
 *             to select an appropriate device.
 *
 * Returns:
 *
 *     BT_STATUS_PENDING - The connection process has been successfully
 *         started. When the connection process is complete, the
 *         application callback will receive either the HID_EVENT_CONNECT or 
 *         HID_EVENT_DISCONNECT event.
 *
 *     BT_STATUS_IN_USE - This channel is already connected or is in the
 *         process of connecting.
 *
 *     BT_STATUS_INVALID_PARM - The chnl parameter does not contain a 
 *         valid pointer. (XA_ERROR_CHECK only).
 *
 *     BT_STATUS_NOT_FOUND - The specified device was not found in the device
 *         selector database.  The device must be discovered, paired, or added
 *         manually using DS_AddDevice();
 *
 *     Other - It is possible to receive other error codes, depending on the 
 *         lower layer service in use (L2CAP or Management Entity).
 */
BtStatus HID_Connect(HidChannel *chnl, BD_ADDR *addr);

#if 0
/*---------------------------------------------------------------------------
 * HID_ConnectRsp()
 * 
 *     Responds to a connection request from the remote HID device.  This 
 *     function is used to establish the lower layer connection (L2CAP), 
 *     which allows the sending of commands.
 *
 * Parameters:
 *
 *     Chnl - A registered and open HID channel.
 *
 *     Accept - TRUE accepts the connect or FALSE rejects the connection.
 *
 * Returns:
 *
 *     BT_STATUS_PENDING - The connection responses has been successfully
 *         sent. When the connection process is complete, the application 
 *         callback will receive the HID_EVENT_CONNECT event.
 *
 *     BT_STATUS_BUSY - The connection is already connected.
 *
 *     BT_STATUS_INVALID_PARM - The Chnl parameter does not contain a 
 *         valid pointer. (XA_ERROR_CHECK only).
 *
 *     BT_STATUS_NOT_FOUND - The specified device was not found in the device
 *         selector database.  The device must be discovered, paired, or added
 *         manually using DS_AddDevice();
 *
 *     Other - It is possible to receive other error codes, depending on the 
 *         lower layer service in use (L2CAP or Management Entity).
 */
BtStatus HID_ConnectRsp(HidChannel *Chnl, BOOL Accept);
#define HID_ConnectRsp(c, a) Hidp_ConnectRsp(&(c)->chnl, a)
#endif
/*---------------------------------------------------------------------------
 * HID_Disconnect()
 *
 *     Terminates a connection with a remote HID device.  The lower layer
 *     connection (L2CAP) is disconnected.
 *
 * Parameters:
 *
 *     chnl - A registered and open HID channel.
 *
 * Returns:
 *
 *     BT_STATUS_PENDING - The disconnect process has been successfully
 *         started. When the disconnect process is complete, the
 *         application callback will receive the HID_EVENT_DISCONNECT event.
 *
 *     BT_STATUS_INVALID_PARM - The chnl parameter does not contain a valid 
 *         pointer. (XA_ERROR_CHECK only).
 *
 *     BT_STATUS_NO_CONNECTION - No connection exists on the specified channel.
 *
 *     BT_STATUS_NOT_FOUND - The specified device was not found in the device
 *         selector database.  The device must be discovered, paired, or added
 *         manually using DS_AddDevice();
 *
 *     It is possible to receive other error codes, depending on the lower 
 *     layer service in use (L2CAP or Management Entity).
 */
BtStatus HID_Disconnect(HidChannel *chnl);

BtStatus HID_IntSendData(HidChannel *chnl, U16 datalen,  U8 *data);

  
#endif
