#ifndef __HIDCON_H_
#define __HIDCON_H_

#include "sys/hidconn.h"

/****************************************************************************
 *
 * Types
 *
 ****************************************************************************/

/*---------------------------------------------------------------------------
 * HidconEvent type
 *      
 */
typedef U8 HidpEvent;

/** The transport layer is connected and commands/responses can now
 * be exchanged.
 * 
 *  During this callback, the 'p.remDev' parameter is valid.
 */
#define HIDP_EVENT_CTL_CONNECT             1

/** A remote device is attempting to connect the transport layer.
 *  Only the acceptor of the connection is notified.  The acceptor must
 *  call HIDP_ConnectRsp() to either accept or reject the connection.
 * 
 *  During this callback, the 'p.remDev' parameter is valid.
 */
#define HIDP_EVENT_CTL_CONNECT_IND         2

/** The transport layer been disconnected. 
 * 
 *  During this callback, the 'p.remDev' parameter is valid.
 */
#define HIDP_EVENT_CTL_DISCONNECT          3

/** The transport layer is connected and commands/responses can now
 * be exchanged.
 * 
 *  During this callback, the 'p.remDev' parameter is valid.
 */
#define HIDP_EVENT_INT_CONNECT             4

/** A remote device is attempting to connect the transport layer.
 *  Only the acceptor of the connection is notified.  The acceptor must
 *  call HIDP_ConnectRsp() to either accept or reject the connection.
 * 
 *  During this callback, the 'p.remDev' parameter is valid.
 */
#define HIDP_EVENT_INT_CONNECT_IND         5

/** The transport layer been disconnected. 
 * 
 *  During this callback, the 'p.remDev' parameter is valid.
 */
#define HIDP_EVENT_INT_DISCONNECT          6

#define HIDP_EVENT_CTL_DATA_IND             7
#define HIDP_EVENT_CTL_DATA_SENT            8
#define HIDP_EVENT_INT_DATA_IND             9
#define HIDP_EVENT_INT_DATA_SENT            10

/* End of HidpEvent */

#define HIDP_EVENT_LAST                11

/* Forward references */
typedef struct _HidpCallbackParms HidpCallbackParms;
typedef struct _HidpChannel HidpChannel;

/*---------------------------------------------------------------------------
 * HidpCallback type
 *
 * A function of this type is called to indicate events to the application.
 */
typedef void (*HidpCallback)(HidpChannel *chnl, HidpCallbackParms *Parms);

/* End of HidpCallback */

/****************************************************************************
 *
 * Data Structures
 *
 ****************************************************************************/

/*---------------------------------------------------------------------------
 * HidpChannel structure
 *
 * Defines the HIDP channel.
 */
struct _HidpChannel {
    /* === Internal use only === */
    ListEntry     node;

    HidConn       ctlconn;
    HidConn       intconn;

    /* Application callback function */
    HidpCallback   callback;
};


/*---------------------------------------------------------------------------
 * HidpCallbackParms structure
 *
 * Contains information for the application callback event.
 * 
 */
struct _HidpCallbackParms {
    /* HIDP event */
    HidpEvent      event;

    /* HIDP channel associated with the event */
    HidpChannel   *channel;

    /* Status of event (valid only for certain events)  */
    BtStatus       status;

    U16 datalen;

    /* Callback parameter object, depending on "event" */
    union {
        /* Remote device associated with the event */
        BtRemoteDevice *remDev;
        U8 *data;
    } p;
};

/****************************************************************************
 *
 * Function Reference
 *
 ****************************************************************************/

/*---------------------------------------------------------------------------
 * HIDP_Init()
 *
 *     Initialize the HIDP SDK.  This function should only be called
 *     once, normally at initialization time.  The calling of this function
 *     can be specified in overide.h using the XA_LOAD_LIST macro
 *     (i.e. #define XA_LOAD_LIST XA_MODULE(HIDP) ... ).
 *
 * Returns:
 *     TRUE - Initialization was successful
 *
 *     FALSE - Initialization failed.
 */
BOOL HIDP_Init(void);

#if L2CAP_DEREGISTER_FUNC == XA_ENABLED
/*---------------------------------------------------------------------------
 * HIDP_Deinit()
 *
 *     Deinitializes the HIDP module. HIDP_Init() must later be called
 *     again to use any HIDP function call. This function does no
 *     error checking so do NOT call when HIDP is in use.
 *
 * Requires:
 *     L2CAP_DEREGISTER_FUNC set to XA_ENABLED
 */
void HIDP_Deinit(void);
#endif /* L2CAP_DEREGISTER_FUNC == XA_ENABLED */

/*---------------------------------------------------------------------------
 * HIDP_Register()
 *
 *      Registers an application callback to receive HIDP events. This 
 *      function must be called before any other HIDP functions.
 *
 * Parameters:
 *
 *      chnl - Channel structure that receives or initiates connections.
 *
 *      callback - Identifies the application function that will be called
 *        with HIDP events.
 *
 * Returns:
 *      BT_STATUS_SUCCESS - The HIDP application callback Function was
 *      successfully registered.
 *
 *      BT_STATUS_IN_USE - The specified channel is already in use.
 *
 *      BT_STATUS_INVALID_PARM - The chnl or Callback parameter does not contain 
 *         a valid pointer. (XA_ERROR_CHECK only).
 */
BtStatus HIDP_Register(HidpChannel *chnl, HidpCallback callback);

/*---------------------------------------------------------------------------
 * HIDP_Deregister()
 *
 *      De-registers the HIDP callback. After making this call
 *      successfully, the callback specified in HIDP_Register will
 *      receive no further events.
 *
 * Parameters:
 *
 *      chnl - Channel structure that receives or initiates connections.
 *
 * Returns:
 *      BT_STATUS_SUCCESS - The HIDP callback was successfully deregistered.
 * 
 *      BT_STATUS_IN_USE - The specified channel is still in use.
 *
 *      BT_STATUS_NOT_FOUND - An HIDP callback was not previously registered.
 *     
 *      BT_STATUS_INVALID_PARM - The chnl parameter does not contain a valid 
 *         pointer. (XA_ERROR_CHECK only).
 */
BtStatus HIDP_Deregister(HidpChannel *chnl);

/*---------------------------------------------------------------------------
 * HIDP_Connect()
 * 
 *     Initiates a connection to a remote HIDP device.  This function is 
 *     used to establish the lower layer connection (L2CAP), which allows
 *     sending messages
 *
 *     If the connection attempt is successful, the HIDP_EVENT_CONNECT event
 *     will be received.  If the connection attempt is unsuccessful, the
 *     HIDP_EVENT_DISCONNECT event will be received.
 *
 * Parameters:
 *
 *      chnl - Channel structure that receives or initiates connections.
 *
 *      remDev - A connected remote device.
 *
 * Returns:
 *
 *     BT_STATUS_PENDING - The connection process has been successfully
 *         started. When the connection process is complete, the
 *         application callback will receive either the HIDP_EVENT_CONNECT or 
 *         HIDP_EVENT_DISCONNECT event.
 *
 *     BT_STATUS_SUCCESS - The connection already exists.
 *
 *     BT_STATUS_INVALID_PARM - The chnl or addr parameter does not contain a 
 *         valid pointer. (XA_ERROR_CHECK only).
 *
 *     BT_STATUS_NOT_FOUND - The specified device was not found in the device
 *         selector database.  The device must be discovered, paired, or added
 *         manually using DS_AddDevice();
 *
 *     Other - It is possible to receive other error codes, depending on the 
 *         lower layer service in use (L2CAP or Management Entity).
 */
BtStatus HIDP_Connect(HidpChannel *chnl, BtRemoteDevice *RemDev);

/*---------------------------------------------------------------------------
 * HIDP_ConnectCtlRsp()
 * 
 *     Responds to a connection request from the remote HIDP device.  This 
 *     function is used to establish the lower layer connection (L2CAP), 
 *     which allows sending signaling messages, such as discover, 
 *     configuration, and stream management.
 *
 * Parameters:
 *
 *     Chnl - A registered and open HIDP channel.
 *
 *     Accept - TRUE accepts the connect or FALSE rejects the connection.
 *
 * Returns:
 *
 *     BT_STATUS_PENDING - The connection responses has been successfully
 *         sent. When the connection process is complete, the application 
 *         callback will receive the HIDP_EVENT_CONNECT event.
 *
 *     BT_STATUS_BUSY - The connection is already connected.
 *
 *     BT_STATUS_INVALID_PARM - The Chnl parameter does not contain a 
 *         valid pointer. (XA_ERROR_CHECK only).
 *
 *     BT_STATUS_NOT_FOUND - The specified device was not found in the device
 *         selector database.  The device must be discovered, paired, or added
 *         manually using DS_AddDevice();
 *
 *     Other - It is possible to receive other error codes, depending on the 
 *         lower layer service in use (L2CAP or Management Entity).
 */
BtStatus HIDP_CtlConnectRsp(HidpChannel *Chnl, BOOL Accept);

/*---------------------------------------------------------------------------
 * HIDP_ConnectIntRsp()
 * 
 *     Responds to a connection request from the remote HIDP device.  This 
 *     function is used to establish the lower layer connection (L2CAP), 
 *     which allows sending signaling messages, such as discover, 
 *     configuration, and stream management.
 *
 * Parameters:
 *
 *     Chnl - A registered and open HIDP channel.
 *
 *     Accept - TRUE accepts the connect or FALSE rejects the connection.
 *
 * Returns:
 *
 *     BT_STATUS_PENDING - The connection responses has been successfully
 *         sent. When the connection process is complete, the application 
 *         callback will receive the HIDP_EVENT_CONNECT event.
 *
 *     BT_STATUS_BUSY - The connection is already connected.
 *
 *     BT_STATUS_INVALID_PARM - The Chnl parameter does not contain a 
 *         valid pointer. (XA_ERROR_CHECK only).
 *
 *     BT_STATUS_NOT_FOUND - The specified device was not found in the device
 *         selector database.  The device must be discovered, paired, or added
 *         manually using DS_AddDevice();
 *
 *     Other - It is possible to receive other error codes, depending on the 
 *         lower layer service in use (L2CAP or Management Entity).
 */
BtStatus HIDP_IntConnectRsp(HidpChannel *Chnl, BOOL Accept);

/*---------------------------------------------------------------------------
 * HIDP_Disconnect()
 *
 *     Terminates a connection with a remote HIDP device.  The lower layer
 *     connection (L2CAP) is disconnected.
 *
 * Parameters:
 *
 *     chnl - A registered and open HIDP channel.
 *
 * Returns:
 *
 *     BT_STATUS_PENDING - The disconnect process has been successfully
 *         started. When the disconnect process is complete, the
 *         application callback will receive the HIDP_EVENT_DISCONNECT event.
 *
 *     BT_STATUS_INVALID_PARM - The chnl parameter does not contain a valid 
 *         pointer. (XA_ERROR_CHECK only).
 *
 *     BT_STATUS_NO_CONNECTION - No connection exists on the specified channel.
 *
 *     BT_STATUS_NOT_FOUND - The specified device was not found in the device
 *         selector database.  The device must be discovered, paired, or added
 *         manually using DS_AddDevice();
 *
 *     It is possible to receive other error codes, depending on the lower 
 *     layer service in use (L2CAP or Management Entity).
 */
BtStatus HIDP_Disconnect(HidpChannel *chnl);

/*---------------------------------------------------------------------------
 * HIDP_GetRemoteDevice()
 *
 *     Returns a pointer to the current remote device.
 *
 * Parameters:
 *
 *     chnl - An HIDP channel.
 *
 * Returns:
 *
 *     A pointer to a remote device.
 */
BtRemoteDevice * HIDP_RemoteDevice(HidpChannel *chnl);

BtStatus HIDP_CtlSendData(HidpChannel *chnl, U16 datalen,  U8 *data);
BtStatus HIDP_IntSendData(HidpChannel *chnl, U16 datalen,  U8 *data);

#endif
