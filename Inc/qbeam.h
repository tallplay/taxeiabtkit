#ifndef __QBEAM_H
#define __QBEAM_H
/****************************************************************************
 *
 * File:          qbeam.h
 *
 * Description:   This file specifies defines and function prototypes for the
 *                QBeam application.
 * 
 * Created:       August 6, 1998
 *
 * $Project:XTNDAccess IrDA SDK$
 *
 * Copyright 1999-2005 Extended Systems, Inc.  ALL RIGHTS RESERVED.
 *
 * Unpublished Confidential Information of Extended Systems, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of Extended Systems, Inc.
 * 
 * Use of this work is governed by a license granted by Extended Systems,
 * Inc.  This work contains confidential and proprietary information of
 * Extended Systems, Inc. which is protected by copyright, trade secret,
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/
#if OBEX_AUTHENTICATION == XA_ENABLED
#include <obexauth.h>
#endif /* OBEX_AUTHENTICATION == XA_ENABLED

/****************************************************************************
 *
 * QBeam Exported Function prototypes
 *
 ***************************************************************************/
extern BOOL Qb_Init(U8 QbInst);
extern void Qb_Deinit(U8 QbInst);
extern void Qb_Connect(U8 QbInst);
extern void Qb_Disconnect(U8 QbInst);
extern void Qb_Put(U8 QbInst, const char *ObName);
extern void Qb_Get(U8 QbInst, const char *ObName);
extern void Qb_SetPath(U8 QbInst, const char *PathName, BOOL Backup, BOOL NoCreate);
#if OBEX_SESSION_SUPPORT == XA_ENABLED
extern void Qb_CreateSession(U8 QbInst);
extern void Qb_CloseSession(U8 QbInst);
extern void Qb_SuspendSession(U8 QbInst);
extern void Qb_ResumeSession(U8 QbInst);
extern void Qb_SetSessionTimeout(U8 QbInst, U32 timeout, U8 role);
extern void Qb_SetTransport(U8 QbInst, const char *transName);
#endif /* OBEX_SESSION_SUPPORT == XA_ENABLED */
#if OBEX_AUTHENTICATION == XA_ENABLED
extern void Qb_SetPassword(U8 QbInst);
extern void Qb_SetUserId(U8 QbInst);
#endif /* OBEX_AUTHENTICATION == XA_ENABLED */
extern void Qb_ObjectPush(U8 QbInst, const char *ObName, ObexTpAddr *Target);
extern void Qb_BizExchange(U8 QbInst, const char *ObName, ObexTpAddr *Target);
extern void Qb_AbortClient(U8 QbInst);
extern void Qb_AbortServer(U8 QbInst);
extern void Qb_ClientTpConnect(U8 QbInst, ObexTpAddr *Target);
extern void Qb_ClientTpDisconnect(U8 QbInst);
#if OBEX_ALLOW_SERVER_TP_DISCONNECT == XA_ENABLED 
extern void Qb_ServerTpDisconnect(U8 QbInst);
#endif /* OBEX_ALLOW_SERVER_TP_DISCONNECT == XA_ENABLED */

/****************************************************************************
 *
 * QBeam Internal typedefs and defines
 *
 ***************************************************************************/
/* 
 * Transmit Header block construction buffer size. Although large, the real
 * upper limit is the OBEX packet size. For RAM efficiency this value should
 * be calculated as follows. The most header data the application will ever
 * send is in a client PUT which sends a Name header and a Length header. The
 * max name length the object store supports is 128 bytes ASCII, that's 256
 * bytes UNICODE + 3 bytes for the OBEX Name header (259 total). Add in a
 * length header (5 bytes) and you get the max required buffer size: 264 bytes.
 */
#define MAX_HB_BUFFER   264

/* 
 * QBeam Server Connection state
 */
#define QB_SERVER_CONN_DISC  0  /* Not connected              */
#define QB_SERVER_CONN_TP    1  /* Transport connection       */
#define QB_SERVER_CONN_OBEX  2  /* Transport+OBEX connection  */

/*
 * QBeam Server State
 */
#define SERVER_IDLE     10  /* Waiting for an operation             */
#define SERVER_PUT      11  /* Processing a PUT operation           */
#define SERVER_GET      12  /* Processing a GET operation           */
#define SERVER_SETPATH  13  /* Processing a SETPATH operation       */
#define SERVER_CONNECT  14  /* Processing a CONNECT operation       */
#define SERVER_DISCON   15  /* Processing a DISCONNECT operation    */
#define SERVER_ABORT    16  /* Processing an ABORT operation        */
#if OBEX_SESSION_SUPPORT == XA_ENABLED
#define SERVER_CREATE_SESSION  17  /* Processing a CREATE SESSION operation  */
#define SERVER_CLOSE_SESSION   18  /* Processing a CLOSE SESSION operation   */
#define SERVER_SUSPEND_SESSION 19  /* Processing a SUSPEND SESSION operation */
#define SERVER_RESUME_SESSION  20  /* Processing a RESUME SESSION operation  */
#define SERVER_SET_TIMEOUT     21  /* Processing a SET TIMEOUT operation     */
#endif /* OBEX_SESSION_SUPPORT == XA_ENABLED */

/*
 * QBeam Client State 
 */
#define CLIENT_IDLE                 0  /* Idle, No Connection                           */
#define CLIENT_CONNECT              1  /* Discovering OBEX Server                       */
#define CLIENT_CONNECT_RETRY        2  /* 2nd try discovering after media busy          */
#define CLIENT_TP_CONNECT           3  /* Establishing Transport Connection             */
#define CLIENT_CONNECTED            4  /* Transport connection exists.                  */
#define CLIENT_OBEX_CONNECT         5  /* Performing OBEX Connect Operation             */
#define CLIENT_OBEX_DISCON          6  /* Performing OBEX Disconnect Operation          */
#define CLIENT_OBEX_PUT             7  /* Performing OBEX Put Operation                 */
#define CLIENT_OBEX_GET             8  /* Performing OBEX Get Operation                 */
#define CLIENT_OBEX_DELETE          9  /* Performing OBEX Put-Delete Operation          */
#define CLIENT_OBEX_SETPATH         10 /* Performing OBEX SetPath Operation             */
#define CLIENT_ABORT                11 /* Performing OBEX Abort Operation               */
#define CLIENT_TP_DISCON            12 /* Disconnecting Transport Connection            */
#if OBEX_SESSION_SUPPORT == XA_ENABLED
#define CLIENT_OBEX_CREATE_SESSION  13 /* Performing OBEX Create Session Operation      */
#define CLIENT_OBEX_CLOSE_SESSION   14 /* Performing OBEX Close Session Operation       */
#define CLIENT_OBEX_SUSPEND_SESSION 15 /* Performing OBEX Suspend Session Operation     */
#define CLIENT_OBEX_RESUME_SESSION  16 /* Performing OBEX Resume Session Operation      */
#define CLIENT_OBEX_SET_TIMEOUT     17 /* Performing OBEX Set Session Timeout Operation */
#endif /* OBEX_SESSION_SUPPORT == XA_ENABLED */

/*
 * QBeam Device Roles
 */
#define QB_CLIENT   0
#define QB_SERVER   1

/*
 * QBeam Session State
 */
typedef U8 QbSessionState;

#define QB_SESSION_CLOSED       0
#define QB_SESSION_ACTIVE       1
#define QB_SESSION_SUSPENDED    2


#if (IRDA_STACK == XA_ENABLED) && (BT_STACK == XA_ENABLED) 
#define IAS_RESP_SIZE       50
#endif

#if (OBEX_SESSION_SUPPORT == XA_ENABLED)
#define NUM_SESSIONS    2

typedef struct _QbClientSessionData QbClientSessionData;
typedef struct _QbServerSessionData QbServerSessionData;
#endif /* (OBEX_SESSION_SUPPORT == XA_ENABLED) */

/* Number of client & server pairs to allocate space for. */
#ifndef NUM_INSTANCES
#define NUM_INSTANCES   2       /* Default 2 clients, 2 servers */
#endif /* NUM_INSTANCES */

#if NUM_INSTANCES < 1 || NUM_INSTANCES > 2
#error "NUM_INSTANCES must be 1 or 2."
#endif

#if OBEX_AUTHENTICATION == XA_ENABLED
/*
 * QBeam Shared data
 */
typedef struct _QbSharedData {
   BOOL              authenticated; /* OBEX Authentication flags */
   char              password[11];  /* OBEX Authentication password */
   char              userId[20];    /* OBEX Authentication userId */
   BOOL              setPassword;   /* Have we set the password yet? */
   BOOL              setUserId;     /* Have we set the userId yet? */
} QbSharedData;
#endif /* OBEX_AUTHENTICATION == XA_ENABLED */

/*
 * QBeam Client data
 */
typedef struct _QbClientData {
    /* Handler for current operation */
    void              (*handler)(ObexClientCallbackParms *parms);

    /* Object Name and Type for PUT or GET */
    const char         *objectName;
    const char         *objectType;

    /* Pathname for Set Path Operation */
    const char         *pathName;

    /* Default Object Store Handle */
    ObStoreHandle       object;

    /* Operational state */
    U8                  state;

    /* Obex Client application */
    ObexClientApp       clientApp;

    /* Header construction buffer */
    U8                  headerBlock[MAX_HB_BUFFER];

#if (OBEX_SESSION_SUPPORT == XA_ENABLED)
    /* Index of the session that is active. */
    U8                  activeSession;

    /* Index of the session that the current session operation applies to. */
    U8                  currSession;
#endif /* (OBEX_SESSION_SUPPORT == XA_ENABLED) */

#if (IRDA_STACK == XA_ENABLED) && (BT_STACK == XA_ENABLED)
    IrIasQuery          token;
    U8                  iasResponse[IAS_RESP_SIZE];
    BD_ADDR             bdAddr;
#endif /* (IRDA_STACK == XA_ENABLED) && (BT_STACK == XA_ENABLED) */

#if IRDA_STACK == XA_ENABLED
    U8                  currTransport;
#endif /* IRDA_STACK == XA_ENABLED */
    /* Server Instance, used for GetName() */
    U8                  inst;

#if (BT_STACK == XA_ENABLED) && (OBEX_ALLOW_SERVER_TP_CONNECT == XA_ENABLED)
    /* SDP record for Obex Server */
    SdpRecord           record;
    /* SDP attributes for Obex Server */
    SdpAttribute        attributes[2];
#endif /* (BT_STACK == XA_ENABLED) && (OBEX_ALLOW_SERVER_TP_CONNECT == XA_ENABLED) */ 

#if OBEX_AUTHENTICATION == XA_ENABLED
    U8                      clientNonce[AUTH_NONCE_LEN];
    ObexAuthChallengeInfo   rcvdChal;
    ObexAuthResponseInfo    rcvdResp;
#endif /* OBEX_AUTHENTICATION == XA_ENABLED */

} QbClientData;

/*
 * QBeam Server data
 */
typedef struct _QbServerData {
    /* Server transport connection state */
    U8                  connState;
    /* Current operation */
    U8                  operation;
    /* Default Object Store Handle */
    ObStoreHandle       object;
    /* TRUE if path has not been applied yet */
    BOOL                newPath;
    /* Set Path flags */
    U8                  flags;
    /* Obex Server application */
    ObexServerApp       serverApp;
    /* Enough space for Length Header */
    U8                  headerBlock[MAX_HB_BUFFER];

#if (OBEX_SESSION_SUPPORT == XA_ENABLED)
    /* Index of the session that the current session operation applies to. */
    U8                  currSession;
    /* Index of the session that is active. */
    U8                  activeSession;
#endif /* (OBEX_SESSION_SUPPORT == XA_ENABLED) */

#if BT_STACK == XA_ENABLED
    /* SDP record for Obex Server */
    SdpRecord           record;
    /* SDP attributes for Obex Server */
    SdpAttribute        attributes[6];
#endif /* BT_STACK == XA_ENABLED */ 

#if XA_DEBUG == XA_ENABLED
    /* Event information for debug routines (in obdebug.c) */
    ObServerEvent       lastEvent;
#endif /* XA_DEBUG == XA_ENABLED */
    /* Server Instance, used for GetName() */
    U8                  inst;

#if OBEX_AUTHENTICATION == XA_ENABLED
    ObexAuthChallengeInfo   rcvdChal;
#endif /* OBEX_AUTHENTICATION == XA_ENABLED */

} QbServerData;


#if (OBEX_SESSION_SUPPORT == XA_ENABLED)
/*
 * Qbeam Client Session Data 
 */
struct _QbClientSessionData {
    /* Client Session structure provided to OBEX during Resume or Create. */
    ObexClientSession   session;
        
    /** Group: The following variables hold information that the client 
     *         will need when resuming the session IF there is also a
     *         suspended operation on that session.
     */
    char                objectName[OBS_MAX_NAME_LEN];   /* Object name (Put or Get) */
/*  char                objectType[OBS_MAX_TYPE_LEN];   Object name (Put or Get) */
    U8                  headerBlock[MAX_HB_BUFFER];     /* Header block & length */
    U16                 headerLen;
    U32                 objectLen;                      /* For progress meter only. */

    /* Timer for tracking Suspend Timeout */
    EvmTimer            timer;

    /* Session State */
    QbSessionState      state;
};

/*
 * Qbeam Server Session Data 
 */
struct _QbServerSessionData {
    /* OBEX Server Session */
    ObexServerSession   session;

    /** Group: The following variables hold information that the server
     *         will need when resuming the session IF there is also a
     *         suspended operation on that session.
     */
    char                objectName[OBS_MAX_NAME_LEN];   /* Object name (Put or Get) */
/*  char                objectType[OBS_MAX_TYPE_LEN];  */  /* Object name (Put or Get) */
    U8                  headerBlock[MAX_HB_BUFFER];     /* Header block & length */
    U16                 headerLen;
    U32                 objectLen;                      /* For progress meter only. */

    /* Suspend Timers */
    EvmTimer            timer;

    /* Session State */
    QbSessionState      state;
};
#endif /* (OBEX_SESSION_SUPPORT == XA_ENABLED) */

#endif /* __QBEAM_H */

