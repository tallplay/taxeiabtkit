#ifndef __DDAN4W_
#define __DDAN4W_

typedef struct _BtAddrNamePair {
    BD_ADDR    bdAddr;
    char       name[34];
}BtAddrNamePair;

BtStatus DDAN_Open(const BD_ADDR *bdAddr);
BtStatus DDAN_Close(void);
BtStatus DDAN_Flush(void);
BtStatus DDAN_AddRecord(const BtAddrNamePair* record);
BtStatus DDAN_FindRecord(const BD_ADDR *bdAddr, BtAddrNamePair* record);
BtStatus DDAN_DeleteRecord(const BD_ADDR *bdAddr);
BtStatus DDAN_EnumDeviceRecords(I16 index, BtAddrNamePair* record);

#endif
