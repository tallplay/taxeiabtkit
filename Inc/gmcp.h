/****************************************************************************
 *
 * File:
 *     $Workfile:gmcp.h$ for iAnywhere AV SDK
 *     $Revision:1$
 *
 * Description: This file contains a test application for the A2DP profile.
 *             
 * Created:     May 2, 2006
 *
 * Copyright 2006 Extended Systems, Inc.

 * Portions copyright 2006 iAnywhere Solutions, Inc.

 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any means, 
 * or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, Inc.  
 * This work contains confidential and proprietary information of Extended 
 * Systems, Inc. which is protected by copyright, trade secret, trademark and 
 * other intellectual property rights.
 *
 ***************************************************************************/

#include "avrcp.h"

/*---------------------------------------------------------------------------
 * GMCP API layer
 *
 *     The Generic Media Control Profile (GMCP) defines procedures for
 *     exchanging media information between consumer electronics
 *     devices.  IT is typically targetd for use with media players, such as
 *     an MP3 player and media controllers like remote controls or a
 *     automobile head units.
 *
 *     This API is designed to support GMCP applications using the iAnywhere
 *     Blue SDK core protocol stack and the IAnywhere AVRCP profile.  This
 *     API is dependent on the AVRCP programming interface, which means that
 *     all registrations and connection establishment procedures are performed 
 *     using the AVRCP API.  Once an active AVRCP connection exists, the
 *     events, data structures, and procedures defined below can be used to
 *     exchange GMCP specific data.
 */

/****************************************************************************
 *
 * Constants
 *
 ****************************************************************************/

/*---------------------------------------------------------------------------
 * AVRCP_GMCP constant
 *
 *     When set to XA_ENABLED, the AVRCP profile adds handling for
 *     Generic Media Control Protocol (GMCP) commands.  This protocol is
 *     used to exchanged media information with the remote device.  Enabling
 *     this feature adds remote playlist management, along with other
 *     extended control interfaces.  GMCP is not currently a Bluetooth SIG
 *     profile.  It was designed by Impulsesoft Pvt. Ltd. for use in 
 *     conjuction with the AVRCP profile.  
 *
 *     For targets, the application will receive AVRCP_EVENT_GMCP_COMMAND
 *     events instead of the AVRCP_EVENT_COMMAND event for all GMCP commands.
 *     All other commands will appear in ordinary AVRCP_EVENT_COMMAND events; 
 *     responses for these commands MUST be sent.
 *
 *     For controllers, the application may invoke the AVRCP_SendGMCPCommand()
 *     API, and will receive AVRCP_EVENT_GMCP_RESPONSE notifications to indicate
 *     the target's responses.
 */
#ifndef AVRCP_GMCP
#define AVRCP_GMCP XA_ENABLED
#endif /* AVRCP_GMCP */

/*---------------------------------------------------------------------------
 * GMCP_VERSION
 *
 *     Version string for the current GMCP implementation.
 */
#define GMCP_VERSION "GMCP_0_49b"

/*---------------------------------------------------------------------------
 * GMCP_NUM_DEV_ATTR
 *
 *     Number of device attributes.
 */
#define GMCP_NUM_DEV_ATTRS 11

/*---------------------------------------------------------------------------
 * GMCP_NUM_ATTR
 *
 *     Number of supported attributes.
 */
#define GMCP_NUM_ATTRS 88

/****************************************************************************
 *
 * Types
 *
 ****************************************************************************/

/*---------------------------------------------------------------------------
 * GmcpAttrId type
 *
 * Defines the attibute IDs described by the MP3 IDv3 tags specification 
 * version 2.4.0.
 */
typedef U8 GmcpAttrId;

#define GMCP_AENC   0     /* Audio encryption */
#define GMCP_APIC   1     /* Attached picture */
#define GMCP_ASPI   2     /* Audio seek point index */
#define GMCP_COMM   3     /* Comments */
#define GMCP_COMR   4     /* Commercial frame */
#define GMCP_ENCR   5     /* Encryption method registration */
#define GMCP_EQUA   6     /* Equalisation (2) */
#define GMCP_ETCO   7     /* Event timing codes */
#define GMCP_GEOB   8     /* General encapsulated object */
#define GMCP_GRID   9     /* Group identification registration */
#define GMCP_LINK  10     /* Linked information */
#define GMCP_MCDI  11     /* Music CD identifier */
#define GMCP_MLLT  12     /* MPEG location lookup table */
#define GMCP_OWNE  13     /* Ownership frame */
#define GMCP_PRIV  14     /* Private frame */
#define GMCP_PCNT  15     /* Play counter */
#define GMCP_POPM  16     /* Popularimeter */
#define GMCP_POSS  17     /* Position synchronisation frame */
#define GMCP_RBUF  18     /* Recommended buffer size */
#define GMCP_RVA2  19     /* Relative volume adjustment (2) */
#define GMCP_RVRB  20     /* Reverb */
#define GMCP_SEEK  21     /* Seek frame */
#define GMCP_SIGN  22     /* Signature frame */
#define GMCP_SYLT  23     /* Synchronised lyric/text */
#define GMCP_SYTC  24     /* Synchronised tempo codes */
#define GMCP_TALB  25     /* Album/Movie/Show title */
#define GMCP_TBPM  26     /* BPM (beats per minute) */
#define GMCP_TCOM  27     /* Composer */
#define GMCP_TCON  28     /* Content type */
#define GMCP_TCOP  29     /* Copyright message */
#define GMCP_TDEN  30     /* Encoding time */
#define GMCP_TDLY  31     /* Playlist delay */
#define GMCP_TDOR  32     /* Original release time */
#define GMCP_TDRC  33     /* Recording time */
#define GMCP_TDRL  34     /* Release time */
#define GMCP_TDTG  35     /* Tagging time */
#define GMCP_TENC  36     /* Encoded by */
#define GMCP_TEXT  37     /* Lyricist/Text writer */
#define GMCP_TFLT  38     /* File type */
#define GMCP_TIPL  39     /* Involved people list */
#define GMCP_TIT1  40     /* Content group description */
#define GMCP_TIT2  41     /* Title/songname/content description */
#define GMCP_TIT3  42     /* Subtitle/Description refinement */
#define GMCP_TKEY  43     /* Initial key */
#define GMCP_TLAN  44     /* Language(s) */
#define GMCP_TLEN  45     /* Length */
#define GMCP_TMCL  46     /* Musician credits list */
#define GMCP_TMED  47     /* Media type */
#define GMCP_TMOO  48     /* Mood */
#define GMCP_TOAL  49     /* Original album/movie/show title */
#define GMCP_TOFN  50     /* Original filename */
#define GMCP_TOLY  51     /* Original lyricist(s)/text writer(s) */
#define GMCP_TOPE  52     /* Original artist(s)/performer(s) */
#define GMCP_TOWN  53     /* File owner/licensee */
#define GMCP_TPE1  54     /* Lead performer(s)/Soloist(s) */
#define GMCP_TPE2  55     /* Band/orchestra/accompaniment */
#define GMCP_TPE3  56     /* Conductor/performer refinement */
#define GMCP_TPE4  57     /* Interpreted, remixed, or otherwise modified by */
#define GMCP_TPOS  58     /* Part of a set */
#define GMCP_TPRO  59     /* Produced notice */
#define GMCP_TPUB  60     /* Publisher */
#define GMCP_TRCK  61     /* Track number/Position in set */
#define GMCP_TRSN  62     /* Internet radio station name */
#define GMCP_TRSO  63     /* Internet radio station owner */
#define GMCP_TSOA  64     /* Album sort order */
#define GMCP_TSOP  65     /* Performer sort order */
#define GMCP_TSOT  66     /* Title sort order */
#define GMCP_TSRC  67     /* ISRC (international standard recording code) */
#define GMCP_TSSE  68     /* Software/Hardware and settings used for encoding */
#define GMCP_TSST  69     /* Set subtitle */
#define GMCP_TXXX  70     /* User defined text information frame */
#define GMCP_UFID  71     /* Unique file identifier */
#define GMCP_USER  72     /* Terms of use */
#define GMCP_USLT  73     /* Unsynchronised lyric/text transcription */
#define GMCP_WCOM  74     /* Commercial information */
#define GMCP_WCOP  75     /* Copyright/Legal information */
#define GMCP_WOAF  76     /* Official audio file webpage */
#define GMCP_WOAR  77     /* Official artist/performer webpage */
#define GMCP_WOAS  78     /* Official audio source webpage */
#define GMCP_WORS  79     /* Official Internet radio station homepage */
#define GMCP_WPAY  80     /* Payment */
#define GMCP_WPUB  81     /* Publishers official webpage */
#define GMCP_WXXX  82     /* User defined URL link frame */

#define GMCP_CTGY  83     /* Category of an enumerated element. The value for
                           * this attribute could be one of the attribute Ids
                           * themselves. For example:
                           * TALB to indicate album, TOPE to indicate artist 
                           * and so on. This is a MANDATORY attribute for all
                           *  enumerated elements (used only /* in GMCP)
                           */

#define GMCP_SONG  84     /* Indicates that the type is a song (used only in
                           * GMCP)
                           */

#define GMCP_ALPA  85     /* Indicates alphabetical order sorting (used only 
                           * in GMCP) 
                           */

#define GMCP_PLST  86     /* Indicates that the type is a Playlist (used only
                           * in GMCP)
                           */

#define GMCP_GNRE  87     /* Indicates that the type is Genre (used only in 
                           * GMCP)
                           */

/* End of GmcpAttrId */

/*---------------------------------------------------------------------------
 * GmcpDevAttrId type
 *
 * Defines the attibute IDs used for specific device settings.
 */
typedef U8 GmcpDevAttrId;

#define GMCP_LANGUAGE       0  /* Supported Languages */
#define GMCP_SORT_ORDER     1  /* Current sort order */
#define GMCP_SORT_KEY       2  /* Attribute ID used as the sort key */
#define GMCP_SHUFFLE        3  /* Shuffle on/off */
#define GMCP_REPEAT         4  /* Repeat on/off */
#define GMCP_SLEEP_ON_IDLE  5  /* Automatic sleep on idle pause on/off */
#define GMCP_SLEEP_TIME     6  /* Time to wait till sleep mode (seconds) */
#define GMCP_EQUALIZER      7  /* Equalizer on/off */
#define GMCP_EXTRN_POWER    8  /* External power connected on/off */
#define GMCP_BATTERY_PCT    9  /* Battery percentage remaining */
#define GMCP_BATTERY_TIME   10 /* Battery time remaining */

/* End of GmcpDevAttrId */

/*---------------------------------------------------------------------------
 * GmcpLanguage type
 *
 * Used to holde the three letter abbreviation for one of the languages 
 * defined by ISO/FDIS 639-2.
 */
typedef U8 GmcpLanguage[3];

/* End of GmcpLanguage */

/*---------------------------------------------------------------------------
 * GmcpBool type
 *
 * Used to hold the yes/no boolean value for GMCP device settings.
 */
typedef U8 GmcpBool;

#define GMCP_OFF  0      /* Device setting is OFF */
#define GMCP_ON   1      /* Device setting is ON */

/* End of GmcpBool */

/*---------------------------------------------------------------------------
 * GmcpShuffle type
 *
 * Used to hold the yes/no boolean value for GMCP device settings.
 */
typedef U8 GmcpShuffle;

#define GMCP_SHFL_OFF     0      /* Shuffle setting is OFF */
#define GMCP_SHFL_ON      1      /* Shuffle setting is ON */
#define GMCP_SHFL_SONGS   2      /* Shuffle setting is ON (shuffle songs) */
#define GMCP_SHFL_ALBUMS  3      /* Device setting is ON (shuffle albums) */

/* End of GmcpBool */

/*---------------------------------------------------------------------------
 * GmcpSortOrder type
 *
 * Used to hold the sort order value for GMCP device settings.
 */
typedef U8 GmcpSortOrder;

#define GMCP_SORT_ASCENDING   0   /* Sorted in ascending order */
#define GMCP_SORT_DESCENDING  1   /* Sorted in decending order */

/* End of GmcpSortOrder */

/*---------------------------------------------------------------------------
 * GmcpRepeat type
 *
 * Used to hold the value of repeat status for GMCP device settings.
 */
typedef U8 GmcpRepeat;

#define GMCP_REP_OFF  0  /* Repeat is off */
#define GMCP_REP_ONE  1  /* Repeat one song */
#define GMCP_REP_ALL  2  /* Releat all songs */

/* End of GmcpRepeat */

/*---------------------------------------------------------------------------
 * GmcpOperation type
 *
 *     GMCP operations that may be sent/received by a controller or
 *     target). These codes are defined by the GMCP specification v 0.49b.
 */
typedef U8 GmcpOperation;

#define GMCP_GET_CAPABILITIES             0x10
#define GMCP_RETURN_CAPABILITIES          0x11
#define GMCP_GET_DEV_SETTINGS             0x12
#define GMCP_RETURN_DEVICE_SETTINGS       0x13
#define GMCP_UPDATE_DEVICE_SETTINGS       0x14
#define GMCP_GET_DEV_ATTRIB_NAME          0x15
#define GMCP_RETURN_DEV_ATTRIB_NAME       0x16
#define GMCP_START_SESSION                0x17
#define GMCP_STOP_SESSION                 0x18

#define GMCP_SET_ENUM_FILTER              0x20
#define GMCP_GET_NUM_ENUM_ELEMENTS        0x21
#define GMCP_RETURN_NUM_ENUM_ELEMENTS     0x22
#define GMCP_GET_FIRST_ENUM_ELEMENTS      0x23
#define GMCP_GET_NEXT_ENUM_ELEMENTS       0x24
#define GMCP_RETURN_ENUM_ELEMENTS         0x25
#define GMCP_MOVE_TO_INDEX                0x26
#define GMCP_GET_CURRENT_ENUM_INDEX       0x27
#define GMCP_RETURN_CURRENT_ENUM_INDEX    0x28
#define GMCP_SELECT_ENUM_INDEX            0x29
#define GMCP_GET_SELECTED_ENUM_INDEX      0x2a
#define GMCP_RETURN_SELECTED_ENUM_INDEX   0x2b
#define GMCP_MOVE_TO_PREVIOUS_ENUM        0x2c
#define GMCP_MOVE_TO_ROOT_ENUM            0x2d
#define GMCP_MARK_INDEX                   0x2e
#define GMCP_GET_LABEL                    0x2f
#define GMCP_RETURN_LABEL                 0x30
#define GMCP_SELECT_LABEL                 0x31
#define GMCP_MARK_LABEL                   0x32
#define GMCP_GET_ATTRIB_NAME              0x33
#define GMCP_RETURN_ATTRIB_NAME           0x34 
#define GMCP_GET_ID                       0x35
#define GMCP_RETURN_ID                    0x36
#define GMCP_MARK_ID                      0x37
#define GMCP_SELECT_ID                    0x38

#define GMCP_GET_ELEMENT_ATTRIB           0x50
#define GMCP_RETURN_ELEMENT_ATTRIB        0x51
#define GMCP_GET_PLAY_STATUS              0x52
#define GMCP_RETURN_PLAY_STATUS           0x53

#define GMCP_SET_ELEMENT_ABBTIB           0x60
#define GMCP_ADD_TO_ENUM_LIST             0x61
#define GMCP_REMOVE_FROM_ENUM_LIST        0x62
#define GMCP_PERFORM_ACTION               0x63

#define GMCP_REGISTER_NOTIFICATION        0x70
#define GMCP_NOTIFY                       0x71

#define GMCP_ACK                          0x80

/* End of GmcpOperation */

/*---------------------------------------------------------------------------
 * GmcpErrorCode
 *
 *     Error code for GMCP acknowledgements.
 */
typedef U8 GmcpErrorCode;

#define GMCP_ERR_CMD_NOT_SUPPORTED  0x01
#define GMCP_ERR_INVALID_COMMAND    0x02
#define GMCP_ERR_INCORRECT_PARM     0x03
#define GMCP_ERR_INTERNAL_ERROR     0x04

/* End of GmcpErrorCode */

/*---------------------------------------------------------------------------
 * GmcpCapabilityId type
 *
 *     Capability ID used by GMCP_GET_CAPABILITIES and 
 *     GMCP_RETURN_CAPABILITES operations.
 */
typedef U8 GmcpCapabilityId;

#define GMCP_CAP_VERSION          0x00
#define GMCP_OPTS_SUPPORTED       0x01
#define GMCP_DEV_ATTRS_SUPPORTED  0x02
#define GMCP_INFO                 0x03

/* End of GmcpCapabilityId */

/*---------------------------------------------------------------------------
 * GmcpDevAttrId type
 *
 *     Device attribute IDs.
 */
typedef U8 GmcpDevAttrId;

/* End of GmcpDevAttriId */

/* Forward References */
typedef struct _GmcpDeviceSetting GmcpDeviceSetting;
typedef struct _GmcpPdu GmcpPdu;

/****************************************************************************
 *
 * Data Structures
 *
 ****************************************************************************/

/*---------------------------------------------------------------------------
 * GmcpPdu structure
 *
 * Contains the data for a GMCP PDU.
 */
struct _GmcpPdu {

    /* GMCP operation (PDU ID) */
    GmcpOperation  op;

    /* GMCP operation parameter length */
    U16            parmLen;

    /* GMCP operation parameters */
    U8            *parms;

};

/*---------------------------------------------------------------------------
 * GmcpDeviceSetting Structure
 *
 * Contains the GMCP device settings.
 */
struct _GmcpDeviceSetting {
    GmcpDevAttrId attribute;        /* Device Attribute ID */

    union {
        GmcpBool       onOff;       /* State of on/off attributes */
        GmcpShuffle    shuffle;     /* State of shuffling */
        GmcpLanguage   lang;        /* The current language */
        GmcpSortOrder  sortOrder;   /* The sort order (ascending/descending) */
        GmcpAttrId     sortKey;     /* The Attribute ID use for sorting */
        GmcpRepeat     repeat;      /* Repeat state */
        U16            sleepTime;   /* Seconds before sleep mode */
        U8             battPct;     /* Battery percentage remaining */
        U32            battTime;    /* Battery time remaining */
    } value;

};

/****************************************************************************
 *
 * Function Reference
 *
 ****************************************************************************/

/*---------------------------------------------------------------------------
 * GMCP_StartSession()
 *
 *     Send the start session command.
 *
 * Parameters:
 *
 *     chnl - A registered and open AVRCP channel.
 *
 * Returns:
 *
 *     BT_STATUS_PENDING - The Start Session command operation has been started
 *         successfully. When the associated packet has been sent,
 *         the application callback will receive the AVRCP_EVENT_TX_DONE
 *         event.  When a response to the command is received from the
 *         target GMCP_EVENT_RESPONSE will be received.
 *
 *     BT_STATUS_BUSY - A command is already outstanding.
 *
 *     BT_STATUS_INVALID_PARM - The chnl parameter does not contain a valid 
 *         pointer, or the channel is not registered as a controller
 *         (XA_ERROR_CHECK only).
 *
 *     BT_STATUS_NOT_FOUND - The specified channel is not registered.
 */
BtStatus GMCP_StartSession(AvrcpChannel *chnl);

/*---------------------------------------------------------------------------
 * GMCP_StopSession()
 *
 *     Send the end session command.
 *
 * Parameters:
 *
 *     chnl - A registered and open AVRCP channel.
 *
 * Returns:
 *
 *     BT_STATUS_PENDING - The Start Session command operation has been started
 *         successfully. When the associated packet has been sent,
 *         the application callback will receive the AVRCP_EVENT_TX_DONE
 *         event.  When a response to the command is received from the
 *         target GMCP_EVENT_RESPONSE will be received.
 *
 *     BT_STATUS_BUSY - A command is already outstanding.
 *
 *     BT_STATUS_INVALID_PARM - The chnl parameter does not contain a valid 
 *         pointer, or the channel is not registered as a controller
 *         (XA_ERROR_CHECK only).
 *
 *     BT_STATUS_NOT_FOUND - The specified channel is not registered.
 */
BtStatus GMCP_StopSession(AvrcpChannel *chnl);

/*---------------------------------------------------------------------------
 * GMCP_SendCommand()
 *
 *     Sends a GMCP command on the specified channel. The channel must be
 *     registered as a controller (SC_AV_REMOTE_CONTROL), connected, in
 *     the open state, and without another command outstanding.  The GMCP
 *     session must be started before sending any GMCP commands (see
 *     GMCP_StartSession();
 *
 * Parameters:
 *
 *     chnl - A registered and open AVRCP channel.
 *
 *     pdu - The GMCP pdu structure.  This must be intitialized with the
 *           operation type and its parameters.
 * 
 * Returns:
 *
 *     BT_STATUS_PENDING - The send command operation has been started
 *         successfully. When the associated packet has been sent,
 *         the application callback will receive the AVRCP_EVENT_TX_DONE
 *         event.  When a response to the command is received from the
 *         target GMCP_EVENT_RESPONSE will be received.
 *
 *     BT_STATUS_BUSY - A command is already outstanding.
 *
 *     BT_STATUS_INVALID_PARM - The chnl parameter does not contain a valid 
 *         pointer, or the channel is not registered as a controller
 *         (XA_ERROR_CHECK only).
 *
 *     BT_STATUS_NOT_FOUND - The specified channel is not registered.
 */
BtStatus GMCP_SendCommand(AvrcpChannel *chnl, GmcpPdu *pdu);

/*---------------------------------------------------------------------------
 * GMCP_SendResponse()
 *
 *     Sends a GMCP command on the specified channel. The channel must be
 *     connected and in the open state.
 *
 * Parameters:
 *
 *     chnl - A registered and open AVRCP channel.
 *
 *     pdu - The GMCP pdu structure.  This must be intitialized with the
 *           operation type and its parameters.
 *
 *     response - The AVRCP response type.  Set to AVRCP_RESPONSE_ACCEPTED
 *                if the command was accepted, to AVRCP_RESPONSE_INTERIM if
 *                a response will be sent later, or to AVRCP_RESPONSE_REJECTED
 *                or AVRCP_RESPONSE_NOT_IMPLEMENTED if there is an error.
 *
 *     error - When response is either AVRCP_RESPONSE_REJECTED or
 *             AVRCP_RESPONSE_NOT_IMPLEMENTED, this parameter contains
 *             the error code that describes the condition. Ignored if 
 *             'response' is AVRCP_RESPONSE_ACCEPTED or AVRCP_RESPONSE_INTERIM.
 * 
 * Returns:
 *
 *     BT_STATUS_PENDING - The send response operation has been started
 *         successfully. When the associated packet has been sent,
 *         the application callback will receive the AVRCP_EVENT_TX_DONE
 *         event.
 *
 *     BT_STATUS_FAILED - There is no outstanding command to respond to.
 *
 *     BT_STATUS_INVALID_PARM - The chnl parameter does not contain a valid 
 *         pointer. (XA_ERROR_CHECK only).
 *
 *     BT_STATUS_NOT_FOUND - The specified channel is not registered.
 */
BtStatus GMCP_SendResponse(AvrcpChannel *chnl, GmcpPdu *pdu, 
                           AvrcpResponse response, GmcpErrorCode error);

/*---------------------------------------------------------------------------
 * GMCP_IsGmcpCommand()
 *
 *     Determines if the AVRCP command is intended for GMCP.
 *
 * Parameters:
 *
 *     Cmd - A pointer to an AVRCP command frame;
 *
 * Returns:
 *
 *    TRUE - The AVRCP command is targeted for GMCP.
 *
 *    FALSE - The AVRCP command is not targeted for GMCP.
 */
BOOL GMCP_IsGmcpCommand(AvrcpCmdFrame *Cmd);

/*---------------------------------------------------------------------------
 * GMCP_IsGmcpResponse()
 *
 *     Determines if the AVRCP response is intended for GMCP.
 *
 * Parameters:
 *
 *     Rsp - A pointer to an AVRCP response frame.
 *
 * Returns:
 *
 *    TRUE - The AVRCP response is targeted for GMCP.
 *
 *    FALSE - The AVRCP response is not targeted for GMCP.
 */
BOOL GMCP_IsGmcpResponse(AvrcpRspFrame *Rsp);

/*---------------------------------------------------------------------------
 * GMCP_ParseCommand()
 *
 *     Parses a GMCP command.
 *
 * Parameters:
 *
 *     chnl - A registered and open AVRCP channel.
 *
 *     Cmd - A pointer to an AVRCP command frame.
 *
 * Returns:
 *     void
 */
void GMCP_ParseCommand(AvrcpChannel *chnl, AvrcpCmdFrame *Cmd);

/*---------------------------------------------------------------------------
 * GMCP_ParseResponse()
 *
 *     Parses a GMCP response.
 *
 * Parameters:
 *
 *     chnl - A registered and open AVRCP channel.
 *
 *     Rsp - A pointer to an AVRCP response frame.
 *
 * Returns:
 *     void
 */
void GMCP_ParseResponse(AvrcpChannel *chnl, AvrcpRspFrame *Rsp);

