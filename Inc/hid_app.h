#ifndef __HID_APP_H_
#define __HID_APP_H_
#include "bttypes.h"

#ifdef TEST_AVRCP
BOOL HIDAPP_Init(AvrcpChannel *avrchnl, HidChannel *chnl, AvrcpCallback avrcb, HidCallback callback, BtCallBack btCallback);
#else
BOOL HIDAPP_Init(HidChannel *chnl, HidCallback callback, BtCallBack btCallback);
#endif
void HIDAPP_SetDiscoverable(BOOL on);
BtStatus HIDAPP_GetRemoteDeviceName(BD_ADDR addr, BtPageScanInfo psi, BtCallBack cb);
BtRemDevState HIDAPP_GetRemDevConState(BtDeviceContext *bdc);
BtDeviceStatus HIDAPP_GetRemDevSecState(BtDeviceContext *bdc);
BtStatus HIDAPP_ReadLocalBdAddr(BD_ADDR *Addr);
BtStatus HIDAPP_SetPin(BtDeviceContext *bdc, const U8 *pin, U8 len, BtPairingType type);


#endif
