/****************************************************************************
 *
 * File:        obparse.c
 *
 * Description: This file contains code for the OBEX packet parser.
 *
 * Created:     April 29, 1997
 *
 * Version:     MTObex 3.4
 *
 * Copyright 1997-2005 Extended Systems, Inc.  ALL RIGHTS RESERVED.
 *
 * Unpublished Confidential Information of Extended Systems, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of Extended Systems, Inc.
 * 
 * Use of this work is governed by a license granted by Extended Systems,
 * Inc.  This work contains confidential and proprietary information of
 * Extended Systems, Inc. which is protected by copyright, trade secret,
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/
#include <sys/obxalloc.h>
#include <sys/obex_utl.h>

static ObStatus ObParserSendPacket(ObexAppHandle *AppHndl, U8 cmd);
static U8 ParseHeaderType(ObexHeaderType headerType);

/*---------------------------------------------------------------------------
 *            ObSetParserInitState()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Set the OBEX parser to its initial state.
 *
 * Return:    void
 */
void ObParserSetInitState(ObexParser *obp)
{
    obp->curStageOff = 0;
    obp->stageLen = 3;
    obp->handle = 0;
    obp->rsp = 0;

    obp->rxState = OBSC_RX_WAIT;

}

/*---------------------------------------------------------------------------
 *            ObParseRx()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Process received data for the given OBEX Parser.
 *
 * Return:    void
 */
void ObParseRx(ObexAppHandle *AppHndl, U8* RxBuff, U16 RxLen)
{
    U16         n;
    U8          i;
    ObexOpcode  tmpOp;
    ObexParser *obp = &(AppHndl->parser);

    Assert(IsObexLocked());

#if JETTEST == XA_ENABLED
    /* If there's a sniffer, send the data there */
    if (OBD(sniffer)) {
        OBD(sniffer)(AppHndl, OBSNIFF_RX, RxBuff, RxLen);
    }
#endif
    
    if (obp->rxState == OBSC_RX_NOP) {
        /* Ignore the data */
        return;
    }

    /* Store the receive buffer and length */
    obp->rxBuff = RxBuff;
    obp->rxLen = RxLen;

    /* Process the data in obp->rxBuff of length obp->rxLen. */
    while (obp->rxLen > 0) {
        Assert(obp->stageLen <= 4); 
        Assert((obp->curStageOff == 0) || (obp->curStageOff < obp->stageLen));

        n = min((U16)(obp->stageLen - obp->curStageOff), obp->rxLen);
        Assert(n <= 4);

        /* Stage the data */
        for (i = obp->curStageOff; n > 0; n--, i++) {
            obp->stageBuff[i] = *(obp->rxBuff);
            (obp->rxBuff)++;
            obp->rxLen--;
            obp->curStageOff++;
        }

        /* Only call the state machine if the data has been completely
         * staged.
         */
        if (obp->curStageOff == obp->stageLen) {
            switch (obp->rxState) {

            case OBSC_RX_WAIT:
                /* We have the start of an OBEX packet. Get the packet length. */
                obp->packetLen = BEtoHost16(obp->stageBuff + 1);

                if (IsServerParser(obp)) {
                    /* This is the server so get the packet opcode */
                    obp->opcode = obp->stageBuff[0];
                } else {
                    /* This the client so get the packet response */
                    obp->rsp = obp->stageBuff[0];
                }

                /* Determine the opcode. Some opcodes like CONNECT and 
                 * SetPath have values beyond the length that are not headers.
                 */
                switch (obp->opcode & 0x7f) {
                case OB_OPCODE_CONNECT:
                    /* We need to get the OBEX version flags and
                     * Maximum OBEX packet length. */
                    obp->stageLen = 4;
                    obp->rxState = OBSC_RX_CONNECT;
                    obp->curPacketLen = OB_CONNECT_LEN;     /* 7 */
                    break;

                case OB_OPCODE_SET_PATH:
                    /* Set path command get the flags. The flags are only sent
                     * in the request packet (received by the server). The response
                     * does not include the extra 2 bytes. In this case drop 
                     * into the default case.
                     */
                    if (IsServerParser(obp)) {
                        obp->stageLen = 2;
                        obp->curPacketLen = 5;
                        obp->rxState = OBSC_RX_PATH;
                        break;
                    }

                default:
                    /* The other opcodes only have length followed
                     * by headers.
                     */
                    obp->curPacketLen = 3;
                    goto GiveStartInd;
                }
                break;

            case OBSC_RX_WAIT2:
            case OBSC_RX_WAIT3:
                /* Save the packet length */
                obp->packetLen = BEtoHost16((obp->stageBuff) + 1);
                obp->curPacketLen = 3;

                if (IsServerParser(obp)) {
                    /* If the opcode of this packet does not match the current
                     * opcode or the abort opcode then we have a problem so
                     * disconnect. Only PUT and GET can be aborted so check for
                     * that too. Note that all this only applies to the server.
                     */
                    tmpOp = obp->stageBuff[0] & 0x7f;
                    if (tmpOp != (obp->opcode & 0x7f)) {
                        if ((tmpOp != OB_OPCODE_ABORT) || 
                            (((obp->opcode & 0x7f) != OB_OPCODE_PUT) &&
                             ((obp->opcode & 0x7f) != OB_OPCODE_GET))) {
                            DebugPrint(("OBEX: opcode missmatch - disconnecting\n"));
                            goto disconnect;
                        } else {
                            /* We received an abort, indicate it as a new operation */
                            obp->opcode = obp->stageBuff[0];

                            /* If we haven't send a response to the current
                             * operation yet (rxState == WAIT2) then flag the
                             * condition where have two responses to send.
                             */
                            if (obp->rxState == OBSC_RX_WAIT2)
                                obp->flags |= OBPF_DEFER_OPER_IND;

                            goto GiveStartInd;
                        }
                    }
                    /* This is a command packet so save the opcode */
                    obp->opcode = obp->stageBuff[0];
                    Assert(obp->rxState == OBSC_RX_WAIT3);
                } else {
                    /* This is a response packet so save the response */
                    obp->rsp = obp->stageBuff[0];
                }

                /* Now deal with the headers */
                obp->headerLen = 0;
                goto HeaderCheckDone;

            case OBSC_RX_PATH:
                /* The set path flags & constants are in the stage buffer */
                obp->spFlags = obp->stageBuff[0];
                goto GiveStartInd;

            case OBSC_RX_CONNECT:
                /* We got the OBEX Connect parms so save them away. */
                obp->obexVersion = obp->stageBuff[0];
                obp->maxTxPacket = BEtoHost16(obp->stageBuff + 2);
                /* We should disconnect in response to any OBEX Connect with a 
                 * packet size lower than 255 bytes, but we'll be lenient since
                 * the Nokia 6210 phone passes in an illegal 59-byte packet size.  
                 * It does this since 59 bytes is the maximum amount of OBEX data 
                 * in a 64-byte IrDA frame.
                 */
                if (obp->maxTxPacket < 59) goto disconnect;
                obp->flags |= OBPF_CONNECTED;

                Report(("OBEX: Received OBEX Connect. OBEX Packet Size %d bytes.\n", obp->maxTxPacket));
GiveStartInd:
                /* Call the Command Indication start function and then
                 * check for headers.
                 */
                obp->headerLen = 0;
                obp->eventIndFunc(AppHndl, OBPE_OPER_IND_START);
                
                /* If the Command Interpreter aborted the operation, stop processing. */
                if (obp->rxState == OBSC_RX_NOP)
                    return;
                
                goto HeaderCheckDone;

            case OBSC_RX_PUT_HEAD1:
                /* StageBuff contains the Header type, save it off. */
                obp->header = (ObexHeaderType)obp->stageBuff[0];

                /* Determine how much more of the header to stage based
                 * on it's type
                 */
                obp->stageLen = ParseHeaderType(obp->header);
                obp->rxState = OBSC_RX_PUT_HEAD2;

                /* Update the amount of data received so far in the packet.
                 * Optimize by adding the data were about to stage since we
                 * wont check the length before we process it.
                 */
                obp->curPacketLen += obp->stageLen + 1;
                break;

            case OBSC_RX_PUT_HEAD2:
                /* If staged length is 2 bytes then it's the headers length field */
                if (obp->stageLen == 2) {
                    obp->totalHeaderLen = BEtoHost16(obp->stageBuff)-3;
                    obp->headerLen = obp->totalHeaderLen;
                }

                /* Although frivolous, this allows the application to call
                 * GetHeaderWriteLen() during a OBPE_PUT_HEADER event.
                 * This is important because the app can't distinguish
                 * between OBPE_PUT and OBPE_WRITE events.
                 */
                obp->dataLen = 0;

                /* Tell the next layer we are giving it a header. If the
                 * the header is a single byte or 4 byte quantity then we
                 * are done. If the header is an integer header then the
                 * 4 bytes of the result are in obp->stageBuff. For headers 
                 * with two bytes lengths we need to read more of the header.
                 */
                obp->eventIndFunc(AppHndl, OBPE_PUT_HEADER);
                
                /* If the Command Interpreter aborted the operation, stop processing. */
                if (obp->rxState == OBSC_RX_NOP)
                    return;

                /* If the whole header has been obtained then see if
                 * the packet is done (jump to HeaderCheckDone).
                 */
                if ((obp->stageLen != 2) || (obp->headerLen == 0)) {
                    goto HeaderCheckDone;
                } 
                
                obp->rxState = OBSC_RX_PUT_HEAD3;
                obp->stageLen = 0;
                
                /* Were passing up a pointer to rxBuff, if their is data
                 * present fall through to the next case.
                 */
                if (obp->rxLen == 0) {
                    break;
                }

            case OBSC_RX_PUT_HEAD3:
                /* Write the header. The data is pointed to by obp->rxBuff
                 * and the length is in obp->dataLen. */
                obp->dataLen = min(obp->rxLen, obp->headerLen);
                obp->headerLen -= obp->dataLen;
                obp->curPacketLen += obp->dataLen;

                obp->eventIndFunc(AppHndl, OBPE_WRITE_HEADER);

                /* If the Command Interpreter aborted the operation, stop processing. */
                if (obp->rxState == OBSC_RX_NOP)
                    return;
                
                /* We have processed obp->dataLen bytes in the buffer so update
                 * the buffer variables.
                 */
                obp->rxLen -= obp->dataLen;
                obp->rxBuff += obp->dataLen;

                /* See if we have any more data in the packet. Headers are
                 * suppose to fit completely in a single packet so we could
                 * check this and if an errors occurs abort the operation.
                 */
HeaderCheckDone:
                if (obp->curPacketLen == obp->packetLen) {
                    /* The packet is done. If the current header is not done then
                     * we are talking to a buggy implementation so disconnect
                     */
                    if (obp->headerLen > 0) {
                        DebugPrint(("OBEX: Header Length is not 0 - disconnect: HeaderLen : %d Data : %d\n", obp->headerLen, obp->rxBuff[0]));
                        goto disconnect;
                    }

                    /* Tell the Command Interpreter that the OBEX packet is done. */
                    obp->rxState = OBSC_RX_WAIT2;
                    obp->stageLen = 3;

                    /* Don't indicate OPER_IND if we already have an
                     * outstanding OPER_IND event. However, we need to float
                     * a no-op event through the command interpreter when
                     * OBEX_SERVER_CONS_SIZE is in use because it caches the
                     * start ind and may not have delivered the ABORT_START yet.
                     */
                    if (obp->flags & OBPF_DEFER_OPER_IND) {
#if (OBEX_SERVER_CONS_SIZE > 0) || (OBEX_SESSION_SUPPORT == XA_ENABLED)
                        obp->eventIndFunc(AppHndl, OBPE_NO_EVENT);
#endif /* (OBEX_SERVER_CONS_SIZE > 0) || (OBEX_SESSION_SUPPORT == XA_ENABLED) */
                        break;
                    }
                    obp->eventIndFunc(AppHndl, OBPE_OPER_IND);
                } else if (obp->headerLen == 0) {
                    /* Process the next header. We know that there is
                     * more data, so we can safely stage one byte.
                     */
                    obp->rxState = OBSC_RX_PUT_HEAD1;
                    obp->stageLen = 1;
                }
                break;

            default:
                Assert(0);
                break;
            
            } /* End switch(rxState) */
            
            /* In PUT_HEAD1 the stagelen was added to curPacketLen. This
             * allows us to check here if were trying to stage more data
             * than their is in the packet. This can detect packets that are
             * too long with garbage on the end. It can also detect undersized
             * CONNECT or SETPATH operations.
             */
            if (obp->curPacketLen > obp->packetLen) {
                /* We processed more data than the packet length. This
                 * indicates a buggy implementation so disconnect.
                 */
                DebugPrint(("OBEX: Packet data Overflow/Underflow - disconnect PacketLen : %d CurPacketLen : %d\n", obp->packetLen, obp->curPacketLen));
                goto disconnect;
            }
            obp->curStageOff = 0;
        
        } /* End if (curStageOff == stageLen) */
    
    } /* End While() */
    return;

disconnect:
    ObParserDisconnect(obp);

}


/*---------------------------------------------------------------------------
 *            ObParserSendPacket()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Send an OBEX packet. Calling this function starts the xmit
 *            of an OBEX packet. The parser will call the command interpreter
 *            iteratively looking for enough data to fill an OBEX packet or 
 *            until there is no more data. This is done via ObParserTxBuffAvail().
 *
 */
static ObStatus ObParserSendPacket(ObexAppHandle *AppHndl, U8 cmd)
{
    U8          *buff;
    U8          overhead;
    U16         len, obPacketLen, obexPacketSize;
    ObStatus    status;
#if (OBEX_ROLE_CLIENT == XA_ENABLED) && (OBEX_SESSION_SUPPORT == XA_ENABLED)
    ObexClientSession *session;
#endif /* (OBEX_ROLE_CLIENT == XA_ENABLED) && (OBEX_SESSION_SUPPORT == XA_ENABLED) */
    void        *ObexApp = AppHndl;
    ObexParser  *obp = &(AppHndl->parser);

    Assert(IsObexLocked());
    Assert(!(obp->flags & OBPF_PENDING));

    if ((obp->opcode & 0x7F) != OB_OPCODE_ABORT) {
        /* An Abort packet might be sent out during the receipt of a packet. */
        Assert(obp->rxState == OBSC_RX_WAIT2 || obp->rxState == OBSC_RX_WAIT);
    }

    /* Send the packet and header block data. */
    buff = OBSTACK_GetTxBuffer();
    if (buff == 0) {
        obp->flags |= OBPF_PENDING;
        return OB_STATUS_PENDING;
    }
    
    len = OBSTACK_MaxTxSize(obp->stack);   /* We assume Rx PDU size equals Tx */

    if ((obp->opcode & 0x7F) == OB_OPCODE_CONNECT) {
        /* 
         * This is an OBEX Connect request or response packet so write our
         * Connect parameters. We calculate the optimal OBEX Packet size,
         * based on our PERFORMANCE_MULTIPLIER and the TPDU size.
         */
        obexPacketSize = len * OBEX_PERFORMANCE_MULTIPLIER;
        obexPacketSize = max(obexPacketSize, OBEX_DEFAULT_PACKET_SIZE);

        buff[3] = 0x10;     /* OBEX protocol version 1.0 */
        buff[4] = 0x00;     /* No Connect flags */
        StoreBE16(buff+5, obexPacketSize); /* Our OBEX Packet size */

        overhead = OB_CONNECT_LEN;

    } else if (IsClientParser(obp) && 
               ((obp->opcode & 0x7F) == OB_OPCODE_SET_PATH)) {
        /* This is a set path packet for the client send the client
         * set path parameters. The server does not send parameters in
         * the response (a slight inconsistency in the OBEX protocol).
         */
        overhead = 5;
        buff[3] = obp->spFlags;
        buff[4] = 0;
    } else {
        overhead = 3;   /* For OBEX opcode and packet length */
    }

#if OBEX_SESSION_SUPPORT == XA_ENABLED
    if ((obp->opcode & 0x7F) != OB_OPCODE_SESSION) {
        if (IsClientParser(obp)) {
#if OBEX_ROLE_CLIENT == XA_ENABLED
            session = ((ObexClientApp *)ObexApp)->client.activeSession;
            if (session) {
                /* Add OBEX Session Sequence Number header to non-Session commands */
                buff[overhead++] = OBEXH_SESSION_SEQ_NUM;
                buff[overhead++] = session->seqNum;
                if (((ObexClientApp *)ObexApp)->client.seqNumError == ERR_NONE)
                    session->nextSeqNum++;
            }
#endif /* OBEX_ROLE_CLIENT == XA_ENABLED */
        }
        else if (IsServerParser(obp)) {
#if OBEX_ROLE_SERVER == XA_ENABLED
            if (((ObexServerApp *)ObexApp)->server.activeSession) {
                /* Add OBEX Session Sequence Number header to non-Session commands */
                buff[overhead++] = OBEXH_SESSION_SEQ_NUM;
                buff[overhead++] = ((ObexServerApp *)ObexApp)->server.activeSession->seqNum;
            }
#endif /* OBEX_ROLE_SERVER == XA_ENABLED */
        }
    }
#endif /* OBEX_SESSION_SUPPORT == XA_ENABLED */

    if (obp->handle != 0) {
        /* We have a header block so read as much as we can
         * into the tx buffer.
         */
#if OBEX_DYNAMIC_OBJECT_SUPPORT == XA_ENABLED
        if (ObGetHeaderBlockLen(ObexApp, obp->handle) == 0) {
            /* The object size is unknown, offer to read as much as will
             * fit. If we are not "connected", we probably need to reduce
             * the packet size to the default size (from the frame size). 
             */
            if (obp->flags & OBPF_CONNECTED)
                len = min(obp->maxTxPacket, len);
            else len = min(ObParserDefaultObexPacketSize(), len);

            len = ObReadHeaderBlockFlex(ObexApp, obp->handle, buff + overhead, (U16)(len - overhead));
            obPacketLen = overhead + len;
            len = obPacketLen;
#if OBEX_GET_USE_END_OF_BODY == XA_ENABLED
			if (IsServerParser(obp)) {
				/* ObReadHeaderBlockFlex could have changed the response code to SUCCESS 
				 * if it determined that the last portion of data was being sent out. Make
				 * sure we account for a potentially new response code
				 */
				cmd = ((ObexServerApp *)ObexApp)->server.resp;
				obp->rsp = ((ObexServerApp *)ObexApp)->server.resp;
			}
#endif /* OBEX_GET_USE_END_OF_BODY == XA_ENABLED */
        } else 
#endif /* OBEX_DYNAMIC_OBJECT_SUPPORT == XA_ENABLED */
        {
            obPacketLen = ObGetHeaderBlockLen(ObexApp, obp->handle) + overhead;
            len = min(len, obPacketLen);
            ObReadHeaderBlock(ObexApp, obp->handle, buff + overhead, (U16)(len - overhead));
        }
    } else {
        len = overhead;
        obPacketLen = overhead;
    }

#if XA_DEBUG == XA_ENABLED
    /* Insure that we're not overflowing the OBEX packet size */
    if (((obp->flags & OBPF_CONNECTED) && (obPacketLen > obp->maxTxPacket)) ||
         (!(obp->flags & OBPF_CONNECTED) && (obPacketLen > ObParserDefaultObexPacketSize()))) {
        DebugPrint(("Error: Sending Packet (%d bytes) exceeds Maximum OBEX Packet Size.\n", obPacketLen));
        Assert(0);
    }
#endif /* XA_DEBUG == XA_ENABLED */
    
    /* Put in the cmd and the length */

    buff[0] = cmd;
    buff[1] = (U8)(obPacketLen >> 8);
    buff[2] = (U8) obPacketLen;

    /* Now send this buffer */
    status = OBSTACK_SendTxBuffer(obp->stack, buff, len);
#if JETTEST == XA_ENABLED
    /* If there's a sniffer, send it there as well */
    if ((status == OB_STATUS_PENDING) || (status == OB_STATUS_SUCCESS)) {
        if (OBD(sniffer))
            OBD(sniffer)(AppHndl, OBSNIFF_TX, buff, len);
    }
#endif

    /* See if there is more header block data to send. */
    if (ObGetHeaderBlockLen(ObexApp, obp->handle) > 0) {
        ObParserTxBuffAvail(AppHndl);
    } else {
        /* We have sent the complete packet so set the handle to 0 */
        obp->handle = 0;
    }
    
    return status;
}


/*---------------------------------------------------------------------------
 *            ObServerSendResponse()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Send a response. This function should only be called after
 *            A call to obp->eventIndFunc() with the OBPE_OPER_IND event.
 *
 * Return:    void
 */
ObStatus ObServerSendResponse(ObexAppHandle *AppHndl, ObexRespCode rsp, ObHeaderBlock hb)
{
   ObexParser *obp = &(AppHndl->parser);

   Assert(IsServerParser(obp) && !IsClientParser(obp));
   Assert(rsp & OB_FINAL_BIT);
   Assert(obp->stack);
   
   if (obp->handle != 0 || obp->flags & OBPF_PENDING) {
       Assert(0);
       return OB_STATUS_BUSY;
   }
   
   /* Save the handle and response */
   obp->handle = hb;
   obp->rsp = rsp;

   return ObParserSendPacket(AppHndl, rsp);

}

/*---------------------------------------------------------------------------
 *            ObClientSendCommand()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Send an OBEX command. 
 *
 * Return:    void
 */
ObStatus ObClientSendCommand(ObexAppHandle *AppHndl, ObexOpcode cmd, ObHeaderBlock hb)
{
   ObexParser *obp = &(AppHndl->parser);

   Assert(IsClientParser(obp) && !IsServerParser(obp));
   Assert(obp->stack);

   /* Save the handle and opcode */
   obp->handle = hb;
   obp->opcode = cmd;

   return ObParserSendPacket(AppHndl, cmd);
}

/*---------------------------------------------------------------------------
 *            ObParserTxBuffAvail()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Continues the transmit of an OBEX Packet. This function is
 *            utilized when the OBEX packet size is greater than the transport
 *            PDU size. It is called by the parser and by the transport
 *            when a transmit packet is available.
 *
 * Return:    void
 */
void ObParserTxBuffAvail(ObexAppHandle *AppHndl)
{
    U8         *buff;
    U16         len, blockLen;
    ObStatus    status;
    void       *ObexApp = AppHndl;
    ObexParser *obp = &(AppHndl->parser);

    Assert(IsObexLocked());
    
    /* If the PENDING flag is set, we were about to send a command
     * but there were no packets available. Re-run the send logic.
     */
    if (obp->flags & OBPF_PENDING) {
        obp->flags &= ~OBPF_PENDING;

        if (IsClientParser(obp))
            status = ObParserSendPacket(AppHndl, obp->opcode);
        else status = ObParserSendPacket(AppHndl, obp->rsp);

        if (status != OB_STATUS_PENDING && status != OB_STATUS_SUCCESS) {
            goto HandleTxError;
        }
        return;
    }

    /* If the handle is non-zero we have more data to send. Loop
     * until we're out of data or transmit buffers.
     */
    if (obp->handle != 0) {

        while ((blockLen = ObGetHeaderBlockLen(ObexApp, obp->handle)) > 0) {
            /* Get a tx buffer */
            buff = OBSTACK_GetTxBuffer();
            if (buff == 0) {
                break;
            }
            
            len = min(OBSTACK_MaxTxSize(obp->stack), blockLen);
            ObReadHeaderBlock(ObexApp, obp->handle, buff, len);

            status = OBSTACK_SendTxBuffer(obp->stack, buff, len);
#if JETTEST == XA_ENABLED
            /* If there's a sniffer, send it there as well */
            if ((status == OB_STATUS_PENDING) || (status == OB_STATUS_SUCCESS)) {
                if (OBD(sniffer))
                    OBD(sniffer)(AppHndl, OBSNIFF_TX, buff, len);
            }
#endif
            if ((status != OB_STATUS_PENDING) && (status != OB_STATUS_SUCCESS)) {
HandleTxError:
                ObParserDisconnect(obp);
                return;
            }
        } /* End While() */

        if (blockLen == 0) {
            /* We are done with the Header block */
            obp->handle = 0;
        }
    } 
}

/*---------------------------------------------------------------------------
 *            ObParserSetAbort()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  This function is used to indicate to the parser whether an Abort
 *            operation must be started as soon as the last command has been
 *            sent.  Don't bother waiting for the OBEX Server to respond to
 *            current packet before sending an Abort.  The OBEX spec.
 *            indicates an Abort command can violate the request/response
 *            standard.
 *
 * Return:    void
 */
void ObParserSetAbort(ObexParser *obp)
{
    Assert(IsObexLocked());
    Assert(IsClientParser(obp));

    /* Inform the parser that an immediate abort is requested.
     * However, we must process this flag later, just in case
     * we are transmitting a packet when we request the 
     * immediate abort.
     */
    obp->abort = TRUE;
}

/*---------------------------------------------------------------------------
 *            ObParserTxDone()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  This function is used to indicate the completion of a transmit
 *            request.
 *
 * Return:    void
 */
void ObParserTxDone(ObexAppHandle *AppHndl)
{
    ObStatus        status;
    ObexParser      *obp = &(AppHndl->parser);

    Assert(IsObexLocked());

    /* If were still sending an OBEX Packet we have nothing to do here. */
    if (obp->handle != 0) {
        return;
    }
    
    /* A send just completed and we have no more data to send. */

    if (IsClientParser(obp) && obp->abort) {
        /* Send an Abort now that we are done sending the previous packet */
        obp->opcode = (OB_OPCODE_ABORT|OB_FINAL_BIT);
        status = ObParserSendPacket(AppHndl, obp->opcode);
        Assert(status == OB_STATUS_PENDING);
        obp->abort = FALSE;
    }

    if (IsServerParser(obp)) {
        /* If we are expected to send two responses, don't
         * complete the operation now. 
         */
        if (obp->flags & OBPF_DEFER_OPER_IND) {
            obp->flags &= ~OBPF_DEFER_OPER_IND;
            obp->eventIndFunc(AppHndl, OBPE_OPER_IND);
            return;
        }

        obp->rxState = OBSC_RX_WAIT3;

        /* If the response code indicates that the operation is complete,
         * tell the command interpreter to complete the operation.
         */
        if (obp->rsp != (OBRC_CONTINUE | OB_FINAL_BIT)) {
            ObParserSetInitState(obp);
            obp->eventIndFunc(AppHndl, OBPE_OPER_COMPLETE);
        }        
    }
}
            
/*---------------------------------------------------------------------------
 *            ObParserDisconnect()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Disconnect the OBEX parser connection.
 *
 * Return:    void
 */
void ObParserDisconnect(ObexParser *obp)
{
    Assert(IsObexLocked());

    obp->rxLen = 0;
    obp->handle = 0;
    obp->rxState = OBSC_RX_NOP;
    
    OBSTACK_Disconnect(obp->stack);
}

/*---------------------------------------------------------------------------
 *            ObParserDisconnectInd()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Called from the stack indicating that the connection has
 *            disconnected.
 *
 * Return:    void
 */
void ObParserDisconnectInd(ObexAppHandle *AppHndl)
{
    ObexParser *obp = &(AppHndl->parser);

    Assert(IsObexLocked());
    
    ObParserSetInitState(obp);

    ObParserUnlinkTransport(obp); /* Unlink Transport */
    obp->flags &= ~(OBPF_CONNECTED|OBPF_PENDING);
    obp->eventIndFunc(AppHndl, OBPE_DISCONNECT);
}

/*---------------------------------------------------------------------------
 *            ObParserLinkTransport()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Called from the stack to link the parser to the transport
 *            connection. If the parser is already linked it fails the 
 *            request.
 *
 * Return:    void
 */
BOOL ObParserLinkTransport(ObexParser *obp, ObexTransport *trans)
{
    Assert(IsObexLocked());
    if (obp->stack)
        return FALSE;

    obp->stack = trans;
    ObParserSetInitState(obp);

    return TRUE;
}

/*---------------------------------------------------------------------------
 *            ObParserMaxHbSize()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Determine the maximum amount of data the client or server 
 *            can send in the current packet, based on the opcode.
 *
 * Return:    Number of bytes
 */
U16 ObParserMaxHbSize(ObexParser *obp, ObexOpcode opcode)
{
    U16             maxHb;
#if OBEX_SESSION_SUPPORT == XA_ENABLED
    ObexAppHandle  *AppHndl = (ObexAppHandle*)ContainingRecord(obp, ObexAppHandle, parser);
#endif /* OBEX_SESSION_SUPPORT == XA_ENABLED */
    
    if (obp->flags & OBPF_CONNECTED)
        maxHb = obp->maxTxPacket - 3;
    else maxHb = ObParserDefaultObexPacketSize() - 3;

    if (opcode == OB_OPCODE_CONNECT)
        maxHb -= 4;
    else if ((opcode == OB_OPCODE_SET_PATH) && IsClientParser(obp))
        maxHb -= 2;
#if OBEX_SESSION_SUPPORT == XA_ENABLED
    else if (opcode == OB_OPCODE_SESSION) {
        /* Subtract the maximum amount of internal header space
         * required for a session request or response.
         */
        if (IsClientParser(obp))
            maxHb -= 54;
        else if (IsServerParser(obp))
            maxHb -= 56;
    }
    
    if (opcode != OB_OPCODE_SESSION) {
        /* Session operations do not have a sequence number header */
        if (IsClientParser(obp)) {
#if OBEX_ROLE_CLIENT == XA_ENABLED
            if (((ObexClientApp*)AppHndl)->client.activeSession)
                maxHb -= 2; /* Sequence Number header */
#endif /* OBEX_ROLE_CLIENT == XA_ENABLED */
        } else {
            Assert(IsServerParser(obp));
#if OBEX_ROLE_SERVER == XA_ENABLED
            if (((ObexServerApp*)AppHndl)->server.activeSession)
                maxHb -= 2; /* Sequence Number header */
#endif /* OBEX_ROLE_SERVER == XA_ENABLED */
        }
    }
#endif /* OBEX_SESSION_SUPPORT == XA_ENABLED */

    Assert (maxHb > 0);
    return maxHb;
}


/*---------------------------------------------------------------------------
 *            ParseHeaderType()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Determine the number of bytes to stage based on the header type.
 *
 * Return:    Number of bytes to stage
 */
static U8 ParseHeaderType(ObexHeaderType headerType)
{
    U8 num = 1;

    switch (headerType & 0xc0) {
    case 0x00:
    case 0x40:
        num = 2;
        break;
    case 0xc0:
        num = 4;
        break;
    }
    return num;
}

