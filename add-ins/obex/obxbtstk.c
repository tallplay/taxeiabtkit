/****************************************************************************
 *
 * File:          obxbtstk.c
 *
 * Description:   This file contains the code for OBEX Bluetooth stack
 *                interface module.
 * 
 * Created:       October 21, 1999
 *
 * Version:       MTObex 3.4
 *
 * Copyright 1999-2005 Extended Systems, Inc.  ALL RIGHTS RESERVED.
 *
 * Unpublished Confidential Information of Extended Systems, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any means, 
 * or stored in a database or retrieval system, without prior written 
 * permission of Extended Systems, Inc.
 * 
 * Use of this work is governed by a license granted by Extended Systems, Inc.  
 * This work contains confidential and proprietary information of Extended 
 * Systems, Inc. which is protected by copyright, trade secret, trademark and 
 * other intellectual property rights.
 *
 ****************************************************************************/
#include <obex.h>
#include <sys/obxbtstk.h>

#if BT_STACK == XA_ENABLED
#include <sys/hci.h>
#include <sys/debug.h>

#if (OBEX_ROLE_CLIENT == XA_ENABLED) || (OBEX_ALLOW_SERVER_TP_CONNECT == XA_ENABLED)
#if !defined(SDP_PARSING_FUNCS) || SDP_PARSING_FUNCS == XA_DISABLED
#error The OBEX Bluetooth transport requires the SDP parsing functions. 
#endif
#endif /* (OBEX_ROLE_CLIENT == XA_ENABLED) || (OBEX_ALLOW_SERVER_TP_CONNECT == XA_ENABLED) */

/****************************************************************************
 *
 * Internal Function prototypes
 *
 ***************************************************************************/
#if (OBEX_ROLE_SERVER == XA_ENABLED) || (OBEX_ALLOW_SERVER_TP_CONNECT == XA_ENABLED)
static void     BtServCallBack(RfChannel *Channel, RfCallbackParms *);
static void     BtServerGeneralCallback(const BtEvent *Event);
#endif /* (OBEX_ROLE_SERVER == XA_ENABLED) || (OBEX_ALLOW_SERVER_TP_CONNECT == XA_ENABLED) */

#if (OBEX_ROLE_CLIENT == XA_ENABLED) || (OBEX_ALLOW_SERVER_TP_CONNECT == XA_ENABLED)
static void     BtClientCallBack(RfChannel *Channel, RfCallbackParms *);
static void     BtClientGeneralCallback(const BtEvent *Event);
static BtStatus BtDisconnectClientLink(ObBtClientTransport *btxp);
static BtStatus BtStartServiceQuery(ObBtClientTransport *btxp, SdpQueryMode mode);
static ObStatus BtVerifySdpQueryRsp(ObBtClientTransport *btxp, SdpQueryToken *token);
#endif /* (OBEX_ROLE_CLIENT == XA_ENABLED) || (OBEX_ALLOW_SERVER_TP_CONNECT == XA_ENABLED) */

/* Functions used in ObTransFuncTable */
static ObStatus BTSTACK_DisconnectReq(ObexTransportContext con);
static U16      BTSTACK_MaxTxSize(ObexTransportContext con);
static ObStatus BTSTACK_SendTxBuffer(ObexTransportContext con, U8 *buff, U16 len);

static BOOL     BTSTACK_GetTpConnInfo(ObexTransportContext context, 
                                      ObexTpConnInfo *tpConnInfo);
#if OBEX_TRANSPORT_FLOW_CTRL == XA_ENABLED
static BOOL     BTSTACK_IsRxFlowAvailable(ObexTransportContext context);
static void     BTSTACK_PauseRxFlow(ObexTransportContext context);
static void     BTSTACK_ResumeRxFlow(ObexTransportContext context);
#endif /* OBEX_TRANSPORT_FLOW_CTRL == XA_ENABLED */

/****************************************************************************
 *
 * ROMable data
 *
 ****************************************************************************/

/*---------------------------------------------------------------------------
 *
 * Bluetooth Transport function call table
 */
static const ObTransFuncTable BtStkFuncTable = {
    BTSTACK_DisconnectReq,BTSTACK_MaxTxSize, 
    BTSTACK_SendTxBuffer, BTSTACK_GetTpConnInfo,
#if OBEX_TRANSPORT_FLOW_CTRL == XA_ENABLED
    BTSTACK_IsRxFlowAvailable, BTSTACK_PauseRxFlow, BTSTACK_ResumeRxFlow,
#endif /* OBEX_TRANSPORT_FLOW_CTRL == XA_ENABLED */
};

#if (OBEX_ROLE_SERVER == XA_ENABLED) || (OBEX_ALLOW_SERVER_TP_CONNECT == XA_ENABLED)
/*---------------------------------------------------------------------------
 * SDP Protocol Descriptor List object registered by OBEX.
 * 
 * Value of the protocol descriptor list for the Object Push profile.
 * This structure is a ROM'able representation of the RAM structure.
 * During ServerInit, this structure is copied into the RAM structure
 * used to register the server. A RAM structure must be used to be able
 * to dynamically set the RFCOMM channel in the RFCOMM protocol descriptor.
 */
static const U8 ObexProtoDescList[] = {
    SDP_ATTRIB_HEADER_8BIT(17),  /* Data element sequence, 17 bytes */

    /* Each element of the list is a Protocol descriptor which is a
     * data element sequence. The first element is L2CAP which only
     * has a UUID element.
     */
    SDP_ATTRIB_HEADER_8BIT(3),    /* Data element sequence for L2CAP, 3 bytes */
    SDP_UUID_16BIT(PROT_L2CAP),   /* Uuid16 for L2CAP in Big Endian */

    /* Next protocol descriptor in the list is RFCOMM. It contains two
     * elements which are the UUID and the channel. Ultimately this
     * channel will need to filled in with value returned by RFCOMM.
     * For now it will be set to 0x99 for testing purposes
     */
    SDP_ATTRIB_HEADER_8BIT(5),    /* Data element sequence for RFCOMM, 5 bytes */
    SDP_UUID_16BIT(PROT_RFCOMM),  /* Uuid16 for RFCOMM in Big Endian */
    SDP_UINT_8BIT(0x99),          /* Uint8 RFCOMM channel number - (set at run time) */

    /* The next protocol descriptor is for OBEX. It contains one element
     * which is the UUID.
     */
    SDP_ATTRIB_HEADER_8BIT(3),    /* Data element sequence for OBEX, 3 bytes */
    SDP_UUID_16BIT(PROT_OBEX)     /* Uuid16 for OBEX in Big Endian */
};
#endif /* (OBEX_ROLE_SERVER == XA_ENABLED) || (OBEX_ALLOW_SERVER_TP_CONNECT == XA_ENABLED) */

/****************************************************************************
 *
 * Functions
 *
 ****************************************************************************/
#if (OBEX_ROLE_CLIENT == XA_ENABLED) || (OBEX_ALLOW_SERVER_TP_CONNECT == XA_ENABLED)
/*---------------------------------------------------------------------------
 *            BTSTACK_ClientConnect()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Start a RFCOMM connection to an OBEX device.
 *
 * Return:    OB_STATUS_PENDING - operation is started successfully
 *            OB_STATUS_SUCCESS - operation is successful
 *            OB_STATUS_BUSY - operation failed because the client is busy.
 */
ObStatus BTSTACK_ClientConnect(ObexBtTarget         *Target, 
                               ObBtClientTransport  *btxp,
                               ObexTransport          **Trans)
{
    ObStatus status = OB_STATUS_BUSY;

#if XA_ERROR_CHECK == XA_ENABLED
    if (Target == 0 || !IsValidBdAddr(&Target->addr)) {
        return OB_STATUS_FAILED;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */
    Assert(Target && IsValidBdAddr(&Target->addr));

    switch (btxp->client.state) {
        case OCS_CONNECTED:
            Assert(btxp->remoteDevice);
            /* RFCOMM connection is up, just verify that we're
             * connected to the requested device.
             */
            if (AreBdAddrsEqual(&Target->addr, &btxp->remoteDevice->bdAddr)) {
                status = OB_STATUS_SUCCESS;
            } else {
                /* We're already connected to a DIFFERENT device */
                status = OB_STATUS_BUSY;
            }
            break;

        case OCS_IDLE:
            /* We have no Connection. Bring up the ACL connection and setup
             * the SDP query with the information provided.
             */
            Assert(btxp->remoteDevice == 0);

            btxp->sdpQueryToken.parms = Target->sdpQuery;
            btxp->sdpQueryToken.plen = Target->sdpQueryLen;
            btxp->sdpQueryToken.type = Target->sdpQueryType;
            
            status = ME_CreateLink(&btxp->aclHandle, &Target->addr, &Target->psi, 
                                   &btxp->remoteDevice);
            if (status == BT_STATUS_PENDING) {
                btxp->client.state = OCS_LINK_CONNECT;
                break;
            }
                
            if (status != BT_STATUS_SUCCESS)
                break;

            /* The desired ACL was already Connected, bring up RFCOMM */
            Assert(btxp->remoteDevice);
            Assert(AreBdAddrsEqual(&Target->addr, &btxp->remoteDevice->bdAddr));

            /* Query peer for Remote Object Push DLC */
            status = BtStartServiceQuery(btxp, BSQM_FIRST);
            if (status != BT_STATUS_PENDING) {

                if (status == BT_STATUS_INVALID_TYPE)
                    status = OB_STATUS_INVALID_PARM;

                /* Keeps BtDisconnectClientLink() from indicating disconnect */
                btxp->client.state = OCS_IDLE;
                BtDisconnectClientLink(btxp);
                break;
            }                    
            /* Service query started */
            btxp->client.state = OCS_SERVICE_QUERY;
            break;
    }

    *Trans = &btxp->client.transport;
    return status;

}

/*---------------------------------------------------------------------------
 *            BTSTACK_ClientDisconnect()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Disconnect the client's RFCOMM connection if it is up.
 *
 * Return:    OB_STATUS_PENDING - operation is started successfully
 *            OB_STATUS_FAILED - operation failed because Server is has a
 *            connection (only in no LMP Disconnect build). 
 *            OB_STATUS_NO_CONNECT - operation failed because there is not
 *            a client connection.
 *
 */
ObStatus BTSTACK_ClientDisconnect(ObBtClientTransport *btxp)
{
    ObStatus  status;

    switch (btxp->client.state) {
    case OCS_CONNECTED:
    case OCS_CONNECTING:
        /* An RFCOMM channel exists */
        status = RF_CloseChannel(&btxp->client.channel);
        Assert((status == BT_STATUS_PENDING) || (status == BT_STATUS_IN_PROGRESS) ||
               ((status == BT_STATUS_FAILED) && (btxp->client.state) == OCS_CONNECTING));

        /* If the RF_CloseChannel() call failed. We must be in the process
         * of connecting. The RF_EVENT_OPEN code will detect that were
         * closing and force the disconnect then.
         */
        btxp->client.state = OCS_DISCONNECTING;
        status = OB_STATUS_PENDING;
        break;

    case OCS_SERVICE_QUERY:
        /* Cannot cancel service query. Fall through to disconnect the ACL.
         * This will cause us to cleanup after the SDP response arrives.
         */
    case OCS_LINK_CONNECT:
        /* Set state to idle to avoid calling back the client.
         * Drop through to disconnect ACL.
         */
        btxp->client.state = OCS_IDLE;

    case OCS_IDLE:
        /* If an ACL exists, disconnect it. */
        if (btxp->remoteDevice) {
            status = BtDisconnectClientLink(btxp);
            if (status != OB_STATUS_SUCCESS) {
                btxp->client.state = OCS_DISCONNECTING;
                status = OB_STATUS_PENDING;
            }
            break;
        }
        /* Fall through to no connection */

    default:        
        status = OB_STATUS_NO_CONNECT;
        break;
    }

    return status;
}

/****************************************************************************
 *
 * OBEX Transport - Used by OBEX parser and command interpreter.
 *
 ****************************************************************************/

/*---------------------------------------------------------------------------
 *            BTSTACK_ClientInit()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initialize the OBEX Client.
 *
 * Return:    OB_STATUS_SUCCESS - Initialized successfully
 *
 */
ObStatus BTSTACK_ClientInit(ObBtClientTransport *btxp)
{
    BtStatus        status;
    BtStackState    state;

    ObexClientTransports *transport = ContainingRecord(btxp, ObexClientTransports, ObexClientBtTrans);
    ObexAppHandle *appHndl = (ObexAppHandle*)ContainingRecord(transport, ObexClientApp, trans);

    /* Initialize the Bluetooth client transport */
    InitBtClientTransport(btxp);

    Assert(btxp->client.state == OCS_UNINITIALIZED);
        
    btxp->client.app = appHndl;

    btxp->client.transport.funcTab = &BtStkFuncTable;
    btxp->client.transport.context = (ObexTransportContext)&btxp->client.channel;
    btxp->client.transport.connected = &btxp->client.state;
    btxp->client.transport.tpType = OBEX_TP_BLUETOOTH;

    /* Initialize ACL Link handle */
    btxp->remoteDevice = 0;
    ME_InitHandler(&btxp->aclHandle);
    btxp->aclHandle.callback = BtClientGeneralCallback;

    /* Initialize and register the global handler */
    ME_InitHandler(&btxp->client.globalHandler);
    btxp->client.globalHandler.callback = BtClientGeneralCallback;
    status = ME_RegisterGlobalHandler(&btxp->client.globalHandler);
    if (status != BT_STATUS_SUCCESS) goto done;

    /* Initialize the SDP Query Token */
    btxp->sdpQueryToken.callback = BtClientGeneralCallback;

    /* Initialize the RFCOMM parameters */
    btxp->client.channel.callback = BtClientCallBack;
    btxp->client.channel.maxFrameSize = RF_MAX_FRAME_SIZE;
    btxp->client.channel.priority = RF_DEFAULT_PRIORITY;

    /* We are able to deinitialize now */
    btxp->client.state = OCS_IDLE;

    if ((state = ME_GetStackInitState()) == BTSS_INITIALIZED) {
        /* The stack and BT radio are initialized, get the BD_ADDR now */
        status = ME_ReadLocalBdAddr(&btxp->client.devAddr);
        if (status != BT_STATUS_SUCCESS) goto done;
    }
    
    status = BT_STATUS_SUCCESS;
done:
    return status;

}

#if OBEX_DEINIT_FUNCS == XA_ENABLED
/*---------------------------------------------------------------------------
 *            BTSTACK_ClientDeinit()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Deinitialize the OBEX Client Bluetooth transport.
 *
 * Return:    TRUE -  Client stack deinitialized.
 *            FALSE - Client cannot deinitialize.
 *
 */
BOOL BTSTACK_ClientDeinit(ObBtClientTransport *btxp)
{
    BtStatus    status;
    ObexClientTransports *transport = ContainingRecord(btxp, ObexClientTransports, ObexClientBtTrans);
    ObexAppHandle *appHndl = (ObexAppHandle*)ContainingRecord(transport, ObexClientApp, trans);

    if (btxp->client.state == OCS_UNINITIALIZED) {
        /* Already deinitialized */
        return TRUE;
    }

    if (btxp->client.state != OCS_IDLE) {
        return FALSE;
    }

    Assert(btxp->client.app == appHndl);
    Assert(btxp->remoteDevice == 0);
    
    status = ME_UnregisterGlobalHandler(&btxp->client.globalHandler);
    if (status != BT_STATUS_SUCCESS) return FALSE;

    btxp->client.state = OCS_UNINITIALIZED;
    btxp->client.app = 0;

    return TRUE;
}
#endif /* OBEX_DEINIT_FUNCS == XA_ENABLED */

#if OBEX_PROVIDE_SDP_RESULTS == XA_ENABLED
/*---------------------------------------------------------------------------
 *            OBEX_RegisterSdpResultsCallback()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Registers an SDP Query results callback for the OBEX Client.
 *            This callback allows layers above OBEX to parse the received 
 *            SDP attributes for additional profile specific data.  Such
 *            parsing should be in addition to the SDP parsing of the Protocol 
 *            Descriptor List attribute that OBEX performs automatically.
 *
 * Return:    ObStatus
 *
 */
ObStatus OBEX_RegisterSdpResultsCallback(ObBtClientTransport *Btxp, 
                                         ObSdpResultsCb Callback)
{
#if XA_ERROR_CHECK == XA_ENABLED
    if (!Btxp || !Callback) {
        return OB_STATUS_INVALID_PARM;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    OS_LockObex();
    /* All SDP query results will be passed to this callback 
     * for further SDP parsing.
     */
    Btxp->sdpCallback = Callback;
    OS_UnlockObex();

    return OB_STATUS_SUCCESS;
}

/*---------------------------------------------------------------------------
 *            OBEX_DeregisterSdpResultsCallback()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Deregisters an SDP Query results callback for the OBEX Client.
 *
 * Return:    ObStatus
 *
 */
ObStatus OBEX_DeregisterSdpResultsCallback(ObBtClientTransport *Btxp)
{
#if XA_ERROR_CHECK == XA_ENABLED
    if (!Btxp) {
        return OB_STATUS_INVALID_PARM;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    OS_LockObex();
    /* All SDP query results will be passed to this callback 
     * for further SDP parsing.
     */
    Btxp->sdpCallback = 0;
    OS_UnlockObex();

    return OB_STATUS_SUCCESS;
}
#endif /* OBEX_PROVIDE_SDP_RESULTS == XA_ENABLED */
#endif /* (OBEX_ROLE_CLIENT == XA_ENABLED) || (OBEX_ALLOW_SERVER_TP_CONNECT == XA_ENABLED) */

#if (OBEX_ROLE_SERVER == XA_ENABLED) || (OBEX_ALLOW_SERVER_TP_CONNECT == XA_ENABLED)
/*---------------------------------------------------------------------------
 *            BTSTACK_ServerInit()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initialize the OBEX Server.
 *
 * Return:    OB_STATUS_SUCCESS - Initialized successfully
 *
 */
ObStatus BTSTACK_ServerInit(ObBtServerTransport *btxp)
{
    BtStatus        status;
    BtStackState    state;

    ObexServerTransports *transport = ContainingRecord(btxp, ObexServerTransports, ObexServerBtTrans);
    ObexAppHandle *appHndl = (ObexAppHandle*)ContainingRecord(transport, ObexServerApp, trans);

    /* Initialize the Bluetooth server transport */
    InitBtServerTransport(btxp);

    Assert(btxp->server.state == OCS_UNINITIALIZED);

    btxp->server.app = appHndl;

    /* Initialize and register the global handler */
    ME_InitHandler(&btxp->server.globalHandler);
    btxp->server.globalHandler.callback = BtServerGeneralCallback;
    status = ME_RegisterGlobalHandler(&btxp->server.globalHandler);
    if (status != BT_STATUS_SUCCESS) goto done;

    /* Bind the server to the RFCOMM protocol stack */
    btxp->server.channel.callback = BtServCallBack;
    btxp->server.channel.maxFrameSize = RF_MAX_FRAME_SIZE;
    btxp->server.channel.priority = RF_DEFAULT_PRIORITY;
    /* Indicate a new service is being entered into RFCOMM.  OBEX 
     * doesn't know whether this is intended, or whether multiple
     * instances of the same service are needed.  Therefore, by default,
     * we present each new Obex server as registering a new RFCOMM service */
    btxp->serverReg.serviceId = 0;
    
    if (RF_RegisterServerChannel(&btxp->server.channel, &btxp->serverReg, OBEX_PERFORMANCE_MULTIPLIER) != BT_STATUS_SUCCESS) {
        DebugPrint(("Fatal Error: Unable to bind the server to the protocol stack\n"));
        return OB_STATUS_FAILED;
    }

    /* Build SDP Protocol Descriptor List for our server. This structure
     * is available via the OBEX_ServerGetSdpProtocolDescList() function.
     */
    Assert(sizeof(ObexProtoDescList) == sizeof(btxp->sdpProtoDescList));
    OS_MemCopy((U8 *)btxp->sdpProtoDescList, (U8 *)ObexProtoDescList,
               sizeof(ObexProtoDescList));

    Assert(btxp->sdpProtoDescList[13] == 0x99);
    /* Insert the RFCOMM channel number into the Protocol Descriptor List */
    btxp->sdpProtoDescList[13] = btxp->serverReg.serviceId;

    btxp->server.transport.funcTab = &BtStkFuncTable;
    btxp->server.transport.context = (ObexTransportContext)&btxp->server.channel;
    btxp->server.transport.connected = &btxp->server.state;
    btxp->server.transport.tpType = OBEX_TP_BLUETOOTH;
    
    /* We are able to deinitialize now */
    btxp->server.state = OCS_IDLE;

    if ((state = ME_GetStackInitState()) == BTSS_INITIALIZED) {
        /* The stack and BT radio are initialized, get the BD_ADDR now */
        status = ME_ReadLocalBdAddr(&btxp->server.devAddr);
        if (status != BT_STATUS_SUCCESS) goto done;
    }
    
    status = BT_STATUS_SUCCESS;
done:
    return status;
}

#if OBEX_DEINIT_FUNCS == XA_ENABLED
/*---------------------------------------------------------------------------
 *            BTSTACK_ServerDeinit()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Deinitialize the OBEX Server Bluetooth transport.
 *
 * Return:    TRUE -  Server stack deinitialized.
 *            FALSE - Server cannot deinitialize.
 *
 */
BOOL BTSTACK_ServerDeinit(ObBtServerTransport *btxp)
{
    BtStatus    status;
    ObexServerTransports *transport = ContainingRecord(btxp, ObexServerTransports, ObexServerBtTrans);
    ObexAppHandle *appHndl = (ObexAppHandle*)ContainingRecord(transport, ObexServerApp, trans);

    if (btxp->server.state == OCS_UNINITIALIZED) {
        /* Already deinitialized */
        return TRUE;
    }
    
    if (btxp->server.state != OCS_IDLE) {
        return FALSE;
    }

    Assert(btxp->server.app == appHndl);
    
    /* Unbind channel */
    AssertEval(RF_DeregisterService(&btxp->serverReg) == BT_STATUS_SUCCESS);

    status = ME_UnregisterGlobalHandler(&btxp->server.globalHandler);
    if (status != BT_STATUS_SUCCESS) return FALSE;

    btxp->server.state = OCS_UNINITIALIZED;
    btxp->server.app = 0;
    
    return TRUE;
}
#endif /* OBEX_DEINIT_FUNCS == XA_ENABLED */

/*---------------------------------------------------------------------------
 *            OBEX_ServerGetSdpProtocolDescList()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Returns the protocol descriptor list used in SDP registration,
 *            as well as the number of protocol descriptors OBEX requires. 
 *            This function can be called with zero AttribSize to get the
 *            number of required protocol descriptors.
 *
 * Return:    Number of protocol descriptors OBEX requires.
 *
 */
U8 OBEX_ServerGetSdpProtocolDescList(ObBtServerTransport *Btxp, SdpAttribute *Attribute, U8 AttribSize)
{
    OS_LockObex();
    if (AttribSize > 0) {
        Attribute->id = AID_PROTOCOL_DESC_LIST;
        Attribute->len = sizeof(ObexProtoDescList);
        Attribute->value = Btxp->sdpProtoDescList;
    }
    OS_UnlockObex();

    /* OBEX needs one Protocol Descriptor List */
    return 1;
}
#endif /* (OBEX_ROLE_SERVER == XA_ENABLED) || (OBEX_ALLOW_SERVER_TP_CONNECT == XA_ENABLED) */
 
/****************************************************************************
 *
 * OBEX Bluetooth Transport - Internal functions
 *
 ****************************************************************************/

#if (OBEX_ROLE_SERVER == XA_ENABLED) || (OBEX_ALLOW_SERVER_TP_CONNECT == XA_ENABLED)
/*---------------------------------------------------------------------------
 *            BtServCallBack()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  The OBEX Server's stack callback function. The stack calls
 *            this function with stack events.
 *
 * Return:    void
 *
 */
static void BtServCallBack(RfChannel *Channel, RfCallbackParms *parms)
{
    BtStatus    status;
    ObexPacket *obp;
    ObBtServerTransport *btxp;

    OS_LockObex();

    /* Get the correct Bluetooth transport structure this callback is for */
    btxp = (ObBtServerTransport *)ContainingRecord(Channel, ObBtCommonTransport, channel);

    switch (parms->event) {
    case RFEVENT_OPEN_IND:
        /* Accept the incoming connection to our server if it's idle. */
        if (OBPROT_LinkTransport(btxp->server.app, &btxp->server.transport)) {
            Assert(btxp->server.state != OCS_CONNECTED);
            status = RF_AcceptChannel(Channel);

            btxp->server.state = OCS_CONNECTING;
        } else {
            /* Reject the connection, were busy */
            status = RF_RejectChannel(Channel);
        }            
        Assert(status == BT_STATUS_PENDING);
        break;

    case RFEVENT_OPEN:
        Assert(btxp->server.state == OCS_CONNECTING);
        btxp->server.state = OCS_CONNECTED;

#if OBEX_TRANSPORT_FLOW_CTRL == XA_ENABLED
        btxp->server.credits = 0;
        btxp->server.flags &= ~BTF_FLOW;
#endif /* OBEX_TRANSPORT_FLOW_CTRL == XA_ENABLED */
        /* Tell the application that the server has accepted a connection */
        OBPROT_Connected(btxp->server.app);

        SnifferRegisterEndpoint( SPI_RFCOMM, &Channel->dlci,
                                 RF_RemoteDevice(Channel), SPI_OBEX);
        break;

    case RFEVENT_DATA_IND:
        /* Process the received data */
#if OBEX_TRANSPORT_FLOW_CTRL == XA_ENABLED
        (btxp->server.credits)++;
#endif /* OBEX_TRANSPORT_FLOW_CTRL == XA_ENABLED */
        Report(("OBEX: Server RFCOMM Event: Received %d bytes\n", parms->dataLen));
        OBPROT_ParseRx(btxp->server.app, parms->ptrs.data, parms->dataLen);

        /* The packet has been consumed so advance credit */
#if OBEX_TRANSPORT_FLOW_CTRL == XA_ENABLED
        if ((btxp->server.flags & BTF_FLOW) == 0) {
            RF_AdvanceCredit(Channel, btxp->server.credits);
            btxp->server.credits = 0;
        }
#else
        RF_AdvanceCredit(Channel, 1);
#endif /* OBEX_TRANSPORT_FLOW_CTRL == XA_ENABLED */
        break;

    case RFEVENT_CLOSED:
        Report(("OBEX: Server RFCOMM Event: Closed\n"));
        if (btxp->server.state > OCS_IDLE) {
            btxp->server.state = OCS_IDLE;
            /* This call aborts the current operation if there
             * is one and reports the link status to the server.
             */
            OBPROT_Disconnected(btxp->server.app, ODR_UNKNOWN);
        }
        break;
    
    case RFEVENT_PACKET_HANDLED: 
        Report(("OBEX: Server RFCOMM Event: Transmit Complete\n"));

        obp = ContainingRecord(parms->ptrs.packet, ObexPacket, btpkt);
        Assert(&obp->btpkt == parms->ptrs.packet);

        if (parms->status == BT_STATUS_SUCCESS)
            OBPROT_TxDone(btxp->server.app, obp);
        else OBPROT_ReturnTxBuffer(obp->buffer);
        break;
    }

    OS_UnlockObex();
}

/*---------------------------------------------------------------------------
 *            BtServerGeneralCallback()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Processes events from the Management Entity and SDP. Only the
 *            server receives events via this handler.
 *
 * Return:    void
 *
 */
static void BtServerGeneralCallback(const BtEvent *Event)
{
    ObBtServerTransport *btxp;

    OS_LockObex();

    /* Get the correct Bluetooth transport structure this callback is for */
    btxp = (ObBtServerTransport *)ContainingRecord(Event->handler, ObBtCommonTransport, globalHandler);

    switch (Event->eType) {
    case BTEVENT_HCI_INITIALIZED:
        /* The stack and BT radio are initialized, get the BD_ADDR now */
        ME_ReadLocalBdAddr(&btxp->server.devAddr);
        break;

    case BTEVENT_HCI_DEINITIALIZED:
        /* HCI has been shut down. */
        Report(("OBEX: HCI Deinitialized\n"));
        btxp->server.state = OCS_IDLE;
        break;

    case BTEVENT_HCI_INIT_ERROR:
        /* Error, but HCI may be restarted. */
        Report(("OBEX: HCI Error\n"));
        btxp->server.state = OCS_IDLE;
        break;

    case BTEVENT_HCI_FAILED:
        /* Failure.  HCI will not be restarted */
        Report(("OBEX: HCI Failed\n"));
        btxp->server.state = OCS_IDLE;
        break;
    }

    OS_UnlockObex();
}
#endif /* (OBEX_ROLE_SERVER == XA_ENABLED) || (OBEX_ALLOW_SERVER_TP_CONNECT == XA_ENABLED) */

#if (OBEX_ROLE_CLIENT == XA_ENABLED) || (OBEX_ALLOW_SERVER_TP_CONNECT == XA_ENABLED)
/*---------------------------------------------------------------------------
 *            BtClientCallBack()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  The OBEX Client's stack callback function. The stack calls
 *            this function with stack events.
 *
 * Return:    void
 *
 */
void BtClientCallBack(RfChannel *Channel, RfCallbackParms* parms)
{
    BtStatus status;
    ObexPacket *obp;
    ObBtClientTransport *btxp;

    OS_LockObex();

    /* Get the correct Bluetooth transport structure this callback is for */
    btxp = (ObBtClientTransport *)ContainingRecord(Channel, ObBtCommonTransport, channel);

    switch (parms->event) {
    case RFEVENT_OPEN:
        /* The RFCOMM connection is now up. Call the application and
         * indicate that data can be written.
         */
        if (btxp->client.state == OCS_CONNECTING) {
            btxp->client.state = OCS_CONNECTED;

#if OBEX_TRANSPORT_FLOW_CTRL == XA_ENABLED
            btxp->client.credits = 0;
            btxp->client.flags &= ~BTF_FLOW;
#endif /* OBEX_TRANSPORT_FLOW_CTRL == XA_ENABLED */
            OBPROT_Connected(btxp->client.app);

            SnifferRegisterEndpoint(SPI_RFCOMM, &Channel->dlci,
                                    RF_RemoteDevice(Channel), SPI_OBEX);
        } else {
            /* We must be disconnecting */
            Assert(btxp->client.state == OCS_DISCONNECTING);

            AssertEval( RF_CloseChannel(Channel) == BT_STATUS_PENDING);
        }
        break;

    case RFEVENT_CLOSED:
        if ((btxp->client.state == OCS_CONNECTING) && (btxp->index < OBEX_RF_SERVICE_CHANNELS)) {
            /* Try another possible RFCOMM service channel if this last one failed during a 
             * connection.  This channel may be in use by another device, since we don't
             * register multiple channels on the same serviceId.
             */
            status = RF_OpenClientChannel(btxp->remoteDevice,btxp->remoteRfcServer[btxp->index++],
                                          &btxp->client.channel, OBEX_PERFORMANCE_MULTIPLIER);
            if (status != BT_STATUS_PENDING) {
                BtDisconnectClientLink(btxp);
            }
        }
        else if (btxp->client.state > OCS_IDLE) {
            Report(("OBEX: Client RFCOMM Event: Closed\n"));

            BtDisconnectClientLink(btxp);
        }
        break;

    case RFEVENT_DATA_IND:
        /* Process the received data */
#if OBEX_TRANSPORT_FLOW_CTRL == XA_ENABLED
        (btxp->client.credits)++;
#endif /* OBEX_TRANSPORT_FLOW_CTRL == XA_ENABLED */
        Report(("OBEX: Client RFCOMM Event: Received %d bytes\n", parms->dataLen));
        OBPROT_ParseRx(btxp->client.app, parms->ptrs.data, parms->dataLen);

        /* The packet has been consumed so advance credit */
#if OBEX_TRANSPORT_FLOW_CTRL == XA_ENABLED
        if ((btxp->client.flags & BTF_FLOW) == 0) {
            RF_AdvanceCredit(Channel, btxp->client.credits);
            btxp->client.credits = 0;
        }
#else
        RF_AdvanceCredit(Channel, 1);
#endif /* OBEX_TRANSPORT_FLOW_CTRL == XA_ENABLED */
        break;

    case RFEVENT_PACKET_HANDLED:
        Report(("OBEX: Client RFCOMM Event: Transmit Complete\n"));

        obp = ContainingRecord(parms->ptrs.packet, ObexPacket, btpkt);
        Assert(&obp->btpkt == parms->ptrs.packet);

        if (parms->status == BT_STATUS_SUCCESS)
            OBPROT_TxDone(btxp->client.app, obp);
        else OBPROT_ReturnTxBuffer(obp->buffer);
        break;
    }

    OS_UnlockObex();
}

/*---------------------------------------------------------------------------
 *            BtClientGeneralCallback()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Processes events from the Management Entity and SDP. Only the
 *            client receives events via this handler.
 *
 * Return:    void
 *
 */
static void BtClientGeneralCallback(const BtEvent *Event)
{
    ObBtClientTransport *btxp;
    ObStatus            status = BT_STATUS_FAILED;
#if OBEX_PROVIDE_SDP_RESULTS == XA_ENABLED
    U8                  customRfChannel = 0;
#endif /* OBEX_PROVIDE_SDP_RESULTS == XA_ENABLED */
 
    OS_LockObex();

    switch (Event->eType) {
    case BTEVENT_HCI_INITIALIZED:
        /* Get the correct Bluetooth transport structure this callback is for */
        btxp = (ObBtClientTransport *)ContainingRecord(Event->handler, ObBtCommonTransport, globalHandler);

        /* The stack and BT radio are initialized, get the BD_ADDR now */
        ME_ReadLocalBdAddr(&btxp->client.devAddr);
        break;

    case BTEVENT_HCI_DEINITIALIZED:
        /* Get the correct Bluetooth transport structure this callback is for */
        btxp = (ObBtClientTransport *)ContainingRecord(Event->handler, ObBtCommonTransport, globalHandler);

        /* HCI has been shut down. */
        Report(("OBEX: HCI Deinitialized\n"));
        btxp->client.state = OCS_IDLE;
        break;

    case BTEVENT_HCI_INIT_ERROR:
        /* Get the correct Bluetooth transport structure this callback is for */
        btxp = (ObBtClientTransport *)ContainingRecord(Event->handler, ObBtCommonTransport, globalHandler);

        /* Error, but HCI may be restarted. */
        Report(("OBEX: HCI Error\n"));
        btxp->client.state = OCS_IDLE;
        break;

    case BTEVENT_HCI_FAILED:
        /* Get the correct Bluetooth transport structure this callback is for */
        btxp = (ObBtClientTransport *)ContainingRecord(Event->handler, ObBtCommonTransport, globalHandler);

        /* Failure.  HCI will not be restarted */
        Report(("OBEX: HCI Failed\n"));
        btxp->client.state = OCS_IDLE;
        break;

    case BTEVENT_LINK_CONNECT_CNF:
        /* Get the correct Bluetooth transport structure this callback is for */
        btxp = ContainingRecord(Event->handler, ObBtClientTransport, aclHandle);

        if (Event->errCode == BEC_NO_ERROR) {
            Report(("OBEX: ME Reports Outbound Link Connected.\n"));
            if (btxp->client.state == OCS_LINK_CONNECT) {
                Assert(btxp->remoteDevice == Event->p.remDev);

                /* Client conection is up. If we initiated the connection,
                 * initiate service discovery.
                 */
                if (BtStartServiceQuery(btxp, BSQM_FIRST) == OB_STATUS_PENDING) {
                    /* Service query successfully started. */
                    btxp->client.state = OCS_SERVICE_QUERY;
                    break;
                }
            }

            /* Service query failed to start or we are no longer
             * trying to connect. Disconnect ACL. 
             */
            BtDisconnectClientLink(btxp);

        } else {
            /* Link Connect Failed case */
            Report(("OBEX: ME Reports Outbound Link Failed.\n"));
            btxp->remoteDevice = 0;

            if (btxp->client.state != OCS_IDLE) {
                btxp->client.state = OCS_IDLE;

                OBPROT_Disconnected(btxp->client.app, ODR_UNKNOWN);
            }
        }
        break;

    case SDEVENT_QUERY_RSP:
        /* Get the correct Bluetooth transport structure this callback is for */
        btxp = ContainingRecord(Event->p.token, ObBtClientTransport, sdpQueryToken);
        
        if (btxp->client.state != OCS_SERVICE_QUERY)
            break;

#if OBEX_PROVIDE_SDP_RESULTS == XA_ENABLED
        if (btxp->sdpCallback) {
            /* Store the received query token information relevant to the 
             * application SDP callback 
             */
            OS_MemCopy((U8 *)&btxp->sdpCallbackQueryToken.node, (U8 *)&Event->p.token->node, 
                       sizeof(ListEntry));
            btxp->sdpCallbackQueryToken.rm = Event->p.token->rm;
            btxp->sdpCallbackQueryToken.type = Event->p.token->type;
            btxp->sdpCallbackQueryToken.parms = Event->p.token->parms;
            btxp->sdpCallbackQueryToken.plen = Event->p.token->plen;
            btxp->sdpCallbackQueryToken.callback = Event->p.token->callback;
            btxp->sdpCallbackQueryToken.contState = Event->p.token->contState;
            btxp->sdpCallbackQueryToken.contStateLen = Event->p.token->contStateLen;
            btxp->sdpCallbackQueryToken.errorCode = Event->p.token->errorCode;
            btxp->sdpCallbackQueryToken.rLen = Event->p.token->rLen;
            btxp->sdpCallbackQueryToken.results = Event->p.token->results;
            btxp->sdpCallbackQueryToken.transId = Event->p.token->transId;
        }
#endif /* OBEX_PROVIDE_SDP_RESULTS == XA_ENABLED */
        status = BtVerifySdpQueryRsp(btxp, Event->p.token);
#if OBEX_PROVIDE_SDP_RESULTS == XA_ENABLED
        if (btxp->sdpCallback) {
            btxp->sdpCallback(&btxp->sdpCallbackQueryToken, &customRfChannel);

            if (customRfChannel != 0) {
                /* Found RFCOMM Channel for Object Push service, Open DLC */
                btxp->client.state = OCS_CONNECTING;

                status = RF_OpenClientChannel(btxp->remoteDevice,customRfChannel, &btxp->client.channel, 
                                              OBEX_PERFORMANCE_MULTIPLIER);
                if (status != BT_STATUS_PENDING) {
                    BtDisconnectClientLink(btxp);
                }
                break;
            }
        }
#endif /* OBEX_PROVIDE_SDP_RESULTS == XA_ENABLED */
        if (status == BT_STATUS_SUCCESS) {
            /* Found RFCOMM Channel for Object Push service, Open DLC */
            btxp->client.state = OCS_CONNECTING;

            for (btxp->index = 0; btxp->index < OBEX_RF_SERVICE_CHANNELS; btxp->index++) {
                status = RF_OpenClientChannel(btxp->remoteDevice,btxp->remoteRfcServer[btxp->index],
                                              &btxp->client.channel, OBEX_PERFORMANCE_MULTIPLIER);
                if (status == BT_STATUS_PENDING) break;
            }
            if (status != BT_STATUS_PENDING) {
                BtDisconnectClientLink(btxp);
            }
            break;
        } 
        else if (status == BT_STATUS_SDP_CONT_STATE) {
            /* Query again, since we didn't get the entire response last time */
            if (BtStartServiceQuery(btxp, BSQM_CONTINUE) != BT_STATUS_PENDING) {
                /* Keeps BtDisconnectClientLink() from indicating disconnect */
                btxp->client.state = OCS_IDLE;
                BtDisconnectClientLink(btxp);
            }                    
            break;
        }
        /* Object Push service was not found, drop through to error case */

    case SDEVENT_QUERY_ERR:
    case SDEVENT_QUERY_FAILED:
        /* Get the correct Bluetooth transport structure this callback is for */
        btxp = ContainingRecord(Event->p.token, ObBtClientTransport, sdpQueryToken);

        Report(("OBEX: SDP Query Failed\n"));
        OBPROT_Disconnected(btxp->client.app, ODR_NO_SERVICE_FOUND);
        
        btxp->client.state = OCS_IDLE;
        BtDisconnectClientLink(btxp);
        break;

    case BTEVENT_LINK_DISCONNECT:
        /* RFCOMM disconnect preceedes this so we never get this event */
        Report(("OBEX: ME Reports Link Disconnect.\n"));
        Assert(0);
        break;

    default:
        Report(("OBEX: Ignored ME Event %d\n", Event->eType));
        break;
    }

    OS_UnlockObex();
}

/*---------------------------------------------------------------------------
 *            BtStartServiceQuery()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initiate the SDP service query.
 *
 * Return:    
 *
 */
static BtStatus BtStartServiceQuery(ObBtClientTransport *btxp, SdpQueryMode mode)
{
    ObStatus status;

    btxp->sdpQueryToken.rm = btxp->remoteDevice;
    status = SDP_Query(&btxp->sdpQueryToken, mode);
    return status;
} 

/*---------------------------------------------------------------------------
 *            BtVerifySdpQueryRsp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Verify the SDP service query response.
 *
 * Return:    
 *
 */
static ObStatus BtVerifySdpQueryRsp(ObBtClientTransport *btxp, SdpQueryToken *token)
{
    BtStatus        status = BT_STATUS_FAILED;
    token->attribId = AID_PROTOCOL_DESC_LIST;
    token->uuid = PROT_RFCOMM;

    /* Parse the entire SDP response for RFCOMM channels from the Protocol Descriptor
     * List(s).  We will only store RFCOMM channels up to OBEX_RF_SERVICE_CHANNELS,
     * however, we will continue to parse all the data, just in case the application
     * needs the remainder of the response for its independent parsing.
     */
#if OBEX_PROVIDE_SDP_RESULTS == XA_ENABLED
    status = BT_STATUS_SUCCESS;
    while (status == BT_STATUS_SUCCESS) {
#else /* OBEX_PROVIDE_SDP_RESULTS == XA_ENABLED */
    while (btxp->numChannels < OBEX_RF_SERVICE_CHANNELS) {
#endif /* OBEX_PROVIDE_SDP_RESULTS == XA_ENABLED */
        if ((status = SDP_ParseAttributes(token)) == BT_STATUS_SUCCESS) {
            Report(("OBEX: SDP_ParseAttributes succeeded! RFCOMM Channel = %d,"
                   "Updated Buff Len = %d\n", token->valueBuff[0], token->remainLen));
            /* We had better have a U8 channel value, if not, look for any other channels */
            if ((token->totalValueLen == 1) && (token->availValueLen == 1)) {            
                if (btxp->numChannels < OBEX_RF_SERVICE_CHANNELS) {
                    /* Assign the RFCOMM channel number to our RFCOMM channel numbers in 
                     * the server transport.  We stop saving channel numbers when we hit
                     * our maximum: OBEX_RF_SERVICE_CHANNELS.
                     */
                    btxp->remoteRfcServer[btxp->numChannels++] = token->valueBuff[0];
                }
            }
            /* Continue if we have more data that we can parse. */
            token->mode = BSPM_RESUME;
        } else if (status == BT_STATUS_SDP_CONT_STATE) {
            Report(("OBEX: SDP_ParseAttributes - Continuation State. Query Again!\n"));
            token->mode = BSPM_CONT_STATE;
            return status;
        }
        else goto done;
    }
done:
    token->mode = BSPM_BEGINNING;
    if (btxp->numChannels != 0) {
        /* We found at least one match.  We can now connect RFCOMM. */
        btxp->numChannels = 0;
        return BT_STATUS_SUCCESS;
    }
    return status;
}

/*---------------------------------------------------------------------------
 *            BtDisconnectClientLink()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Disconnect clients ACL.
 *
 * Return:    void
 *
 */
static BtStatus BtDisconnectClientLink(ObBtClientTransport *btxp)
{
    BtStatus        status;
    ObexDiscReason  reason;

    if (btxp->client.state == OCS_DISCONNECTING)
        reason = ODR_USER_REQUEST;
    else reason = ODR_UNKNOWN;

    if (!btxp->remoteDevice) {
        /* We already disconnected, so don't bother failing ME_DisconnectLink */
        Assert(btxp->client.state == OCS_IDLE);
        return BT_STATUS_SUCCESS;
    }

    status = ME_DisconnectLink(&btxp->aclHandle, btxp->remoteDevice);
    if (status == BT_STATUS_SUCCESS) {
        btxp->remoteDevice = 0;
        if (btxp->client.state != OCS_IDLE) {

            btxp->client.state = OCS_IDLE;
            OBPROT_Disconnected(btxp->client.app, reason);
        }
    }
    Assert(status == BT_STATUS_SUCCESS || status == BT_STATUS_IN_PROGRESS);
    return status;
}
#endif /* (OBEX_ROLE_CLIENT == XA_ENABLED) || (OBEX_ALLOW_SERVER_TP_CONNECT == XA_ENABLED) */

/****************************************************************************
 *
 * OBEX Stack functions required by parser and command interpreter.
 * These functions are accessed via the ObTransFuncTable.
 *
 ****************************************************************************/

/*---------------------------------------------------------------------------
 *            BTSTACK_SendTxBuffer()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Send a buffer over the RFCOMM Connection.
 *
 * Return:    Status of the RF_SendData() operation.
 *
 */
static ObStatus BTSTACK_SendTxBuffer(ObexTransportContext con, U8* buff, U16 len)
{
    ObStatus    status;
    ObexPacket *txPacket = ContainingRecord(buff, ObexPacket, buffer);

    Assert(len <= BTSTACK_MaxTxSize(con));

    txPacket->btpkt.data = buff;
    txPacket->btpkt.dataLen = len;
    txPacket->btpkt.headerLen = 0;
    
    status = RF_SendData((RfChannel *)con, &(txPacket->btpkt));
    if (status != BT_STATUS_PENDING) {
        Assert(status != BT_STATUS_SUCCESS);
        /* Tx Failed! Put the packet back into the queue. */
        OBPROT_ReturnTxBuffer(buff);
    }
    return status;

}

/*---------------------------------------------------------------------------
 *            BTSTACK_DisconnectReq()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Disconnect the underlying RFCOMM connection.
 *
 * Return:    OB_STATUS_PENDING
 */
static ObStatus BTSTACK_DisconnectReq(ObexTransportContext con)
{
    RF_CloseChannel((RfChannel *)con);
    
    return OB_STATUS_PENDING;
}

/*---------------------------------------------------------------------------
 *            BTSTACK_MaxTxSize()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Returns the maximum transport protocol data unit that can be
 *            sent over the referenced transport connection.
 *
 * Return:    U16
 *            
 */
static U16 BTSTACK_MaxTxSize(ObexTransportContext con)
{
    return min(OBEX_TX_BUFF_SIZE, RF_FrameSize((RfChannel *)con));
}

/*---------------------------------------------------------------------------
 *            BTSTACK_GetTpConnInfo()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Retrieves OBEX transport layer connection information. This
 *            function can be called when a transport connection is active
 *            to retrieve connection specific information. It should be used
 *            in conjunction with the receive flow control API to retrieve 
 *            the minimum amount of application storage space (low water mark)
 *            used when deciding to pause and resume data flow.
 *
 * Return:    TRUE  - The structure was successfully completed.
 *            FALSE - The transport is not connected.
 */
static BOOL BTSTACK_GetTpConnInfo(ObexTransportContext  context,
                                  ObexTpConnInfo   *tpConnInfo)
{
    ObBtCommonTransport *btxp;

    tpConnInfo->tpType = OBEX_TP_BLUETOOTH;
    tpConnInfo->minRxBuffs = OBEX_PERFORMANCE_MULTIPLIER;
    tpConnInfo->maxPduSize = RF_FrameSize((RfChannel *)context);
    tpConnInfo->remDev = RF_RemoteDevice((RfChannel *)context); 

    if (tpConnInfo->maxPduSize == 0) {
        /* Estimate the max PDU size when the call fails. */
        tpConnInfo->maxPduSize = RF_MAX_FRAME_SIZE;
    }

    /* Get the local device address for the transport being used */
    btxp = ContainingRecord((RfChannel*)context, ObBtCommonTransport, channel);
    /* We do not have a transport connection */
    if (btxp->state != OCS_CONNECTED) return FALSE;
    tpConnInfo->devAddr = btxp->devAddr.addr;
    return TRUE;
}

#if OBEX_TRANSPORT_FLOW_CTRL == XA_ENABLED
/*---------------------------------------------------------------------------
 *            BTSTACK_IsRxFlowAvailable()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Allows the application to determine if a credit based 
 *            receive flow control is available.
 *
 * Return:    TRUE -  Receive flow control is available.
 *            FALSE - Receive flow control is not available.
 */
static BOOL BTSTACK_IsRxFlowAvailable(ObexTransportContext context)
{
    return RF_CreditFlowEnabled((RfChannel *)context);
}

/*---------------------------------------------------------------------------
 *            BTSTACK_PauseRxFlow()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Tells the transport layer to stop the remote device from
 *            sending data by refusing to advance transport credit. Once
 *            this function has been called, no more than the number of 
 *            bytes or packets reported by OBEX_GetTpConnInfo() will be
 *            received. This amount includes any currently indicated data 
 *            (if called during a data indication). Flow can be resumed
 *            with OBEX_ResumeRxFlow().
 *               
 * Return:    void
 *
 */
static void BTSTACK_PauseRxFlow(ObexTransportContext context)
{
    ObBtCommonTransport *btxp;

    btxp = ContainingRecord((RfChannel*)context, ObBtCommonTransport, channel);
    Assert(btxp->state == OCS_CONNECTED);
    btxp->flags |= BTF_FLOW;
}

/*---------------------------------------------------------------------------
 *            BTSTACK_ResumeRxFlow()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Opens the receive window after flow control was exerted.
 *
 * Return:    void
 *
 */
static void BTSTACK_ResumeRxFlow(ObexTransportContext context)
{
    ObBtCommonTransport *btxp;

    btxp = ContainingRecord((RfChannel*)context, ObBtCommonTransport, channel);
    Assert(btxp->state == OCS_CONNECTED);

    btxp->flags &= ~BTF_FLOW;
    RF_AdvanceCredit(&btxp->channel, btxp->credits);
    btxp->credits = 0;
}
#endif /* OBEX_TRANSPORT_FLOW_CTRL == XA_ENABLED */
#endif /* BT_STACK == XA_ENABLED */
