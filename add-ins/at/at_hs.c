/****************************************************************************
 *
 * File:
 *     $Workfile:at_hs.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:8$
 *
 * Description: This file contains an AT Command Processor.
 *             
 * Copyright 2002-2005 Extended Systems, Inc.

 * Portions copyright 2005 iAnywhere Solutions, Inc.

 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any means, 
 * or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions,  
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "atp.h"
#include "sys/atpi.h"
#include "osapi.h"

#if AT_HEADSET == XA_ENABLED

/*
 * Headset Commands and Responses
 */ 
const char *AT_HeadsetCmds[] = {
    /* AT capabilities from GSM 07.07 and V.25ter */
    "+VGM",  /* Microphone gain */
    "+VGS",  /* Speaker gain    */
    "+CKPD", /* Keypad control (AT+CKPD=200 used for Headset button press) */
    0
};

#if AT_ROLE_TERMINAL == XA_ENABLED
/*--------------------------------------------------------------------------- 
 * AT_Encode_Headset
 * 
 *     Encodes Headset Device (TE) originated AT commands.
 */ 
AtStatus TE_Encode_Headset(AtContext *Atc, U16 Type, const AtHeadsetCmd *Command, XaBufferDesc *Output)
{
    U16     avail = (Output->buffSize - Output->writeOffset) - 1;

    switch (Type) {
    case AT_MICROPHONE_GAIN:
        /* Syntax: AT+VGM=<gain> */
        if (avail < 3) {
            return AT_STATUS_NO_RESOURCES;
        }
        Output->writeOffset += AtItoA(Output->buff + Output->writeOffset,
                                      Command->mic.gain);
        break;
        
    case AT_SPEAKER_GAIN:
        /* Syntax: AT+VGS=<gain> */
        if (avail < 3) {
            return AT_STATUS_NO_RESOURCES;
        }
        Output->writeOffset += AtItoA(Output->buff + Output->writeOffset,
                                      Command->speaker.gain);
        break;

    case AT_KEYPAD_CONTROL:
        /* Syntax: +CKPD=<keys>[,<time>[,<pause>]] */
        if (avail < 3) {
            return AT_STATUS_NO_RESOURCES;
        }
        Output->writeOffset += AtItoA(Output->buff + Output->writeOffset,
                                      Command->keypad.button);
        break;

    default:
        return AT_STATUS_NOT_SUPPORTED;
    }

    return AT_STATUS_OK;
}

/*--------------------------------------------------------------------------- 
 * AT_Decode_Headset
 * 
 *     Decodes AT results received by the Headset Unit (TE).
 */ 
AtStatus TE_Decode_Headset(AtContext *Atc, U16 *Type, AtHeadsetResult *Result, XaBufferDesc *Input)
{
    U8             *ptr;
    AtParameter     parms[AT_MAX_PARMS];

    ptr = Input->buff + Input->readOffset;

    AtParseParameters(ptr, (U16)(Input->writeOffset - Input->readOffset), parms);

    switch (*Type) {
    case AT_MICROPHONE_GAIN:
        /* Syntax: AT+VGM=<gain> */
        if (parms[0].len > 0)
            Result->mic.gain = (U8)AtAtoI(ptr + parms[0].offset, parms[0].len);
        break;
        
    case AT_SPEAKER_GAIN:
        /* Syntax: AT+VGS=<gain> */
        if (parms[0].len > 0)
            Result->speaker.gain = (U8)AtAtoI(ptr + parms[0].offset, parms[0].len);
        break;

    default:
        return AT_STATUS_NOT_SUPPORTED;
    }

    return AT_STATUS_OK;
}
#endif /* AT_ROLE_TERMINAL == XA_ENABLED */


#if AT_ROLE_MOBILE == XA_ENABLED
/*--------------------------------------------------------------------------- 
 * AT_Encode_Headset
 * 
 *     Encodes Audio Gateway (ME) originated AT results.
 */ 
AtStatus ME_Encode_Headset(AtContext *Atc, U16 Type, const AtHeadsetResult *Result, XaBufferDesc *Output)
{
    U16     avail = (Output->buffSize - Output->writeOffset) - 2;

    switch (Type) {
    case AT_MICROPHONE_GAIN:
        /* Syntax: +VGM=<gain> */
        if (avail < 3) {
            return AT_STATUS_NO_RESOURCES;
        }
        Output->writeOffset += AtItoA(Output->buff + Output->writeOffset,
                                      Result->mic.gain);
        break;
        
    case AT_SPEAKER_GAIN:
        /* Syntax: +VGS=<gain> */
        if (avail < 3) {
            return AT_STATUS_NO_RESOURCES;
        }
        Output->writeOffset += AtItoA(Output->buff + Output->writeOffset,
                                      Result->speaker.gain);
        break;

    default:
        return AT_STATUS_NOT_SUPPORTED;
    }

    return AT_STATUS_OK;
}
        

/*--------------------------------------------------------------------------- 
 * AT_Decode_Headset
 * 
 *     Decodes AT commands received by the Audio Gateway (ME).
 */ 
AtStatus ME_Decode_Headset(AtContext *Atc, U16 *Type, AtHeadsetCmd *Command, XaBufferDesc *Input)
{
    U8             *ptr;
    AtParameter     parms[AT_MAX_PARMS];

    ptr = Input->buff + Input->readOffset;

    AtParseParameters(ptr, (U16)(Input->writeOffset - Input->readOffset), parms);

    switch (*Type) {
    case AT_MICROPHONE_GAIN:
        /* Syntax: +VGM=<gain> */
        if (parms[0].len > 0)
            Command->mic.gain = (U8)AtAtoI(ptr + parms[0].offset, parms[0].len);
        break;
        
    case AT_SPEAKER_GAIN:
        /* Syntax: +VGS=<gain> */
        if (parms[0].len > 0)
            Command->speaker.gain = (U8)AtAtoI(ptr + parms[0].offset, parms[0].len);
        break;

    case AT_KEYPAD_CONTROL:
        /* Syntax: +CKPD=<keys>[,<time>[,<pause>]] */
        if (parms[0].len > 0)
            Command->keypad.button = (U8)AtAtoI(ptr + parms[0].offset, parms[0].len);
        break;

    default:
        return AT_STATUS_NOT_SUPPORTED;
    }

    return AT_STATUS_OK;
}

#endif /* AT_ROLE_MOBILE == XA_ENABLED */

#endif /* AT_HEADSET == XA_ENABLED */
