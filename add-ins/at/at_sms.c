/****************************************************************************
 *
 * File:
 *     $Workfile:at_sms.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:8$
 *
 * Description: This file contains an SMS AT Command Processor.
 *             
 * Copyright 2002-2005 Extended Systems, Inc.

 * Portions copyright 2005 iAnywhere Solutions, Inc.

 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any means, 
 * or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions,  
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/
#include "atp.h"
#include "sys/atpi.h"
#include "osapi.h"

#if AT_SMS == XA_ENABLED

const char *AT_SmsCmds[] = {
    /* AT capabilities from GSM 07.07 and V.25ter */
    "+CSMS", /* Select messaging service. */
    "+CPMS", /* Select memory storages <mem1>,<mem2>,<mem3> used for reading, writing, etc. */
    "+CMGF", /* Select input and output format of messages. */
    "+CSCA", /* Set Short Message Service Centre Address [SMSC], through which mobile originated SMs are transmitted. */
    "+CSMP", /* Select values for additional parameters needed when SM is sent to the network or placed in storage. */
    "+CSCB", /* Select which types of Cell Broadcast Message are to be received by the ME. */
    "+CNMI", /* Select the procedure, how receiving of new messages from the network is indicated to the TE. */
    "+CMTI", /* Response used to notify the TE whenever SMSDELIVER is stored into ME. */
    "+CBMI", /* Response used to notify the TE whenever Cell Broadcast Message has arrived in ME. */
    "+CDS",  /* Unsolicited response code used to notify the SMSSTATUS-REPORTS to TE. */
    "+CMGL", /* Return messages with status value from message storage <mem1> to the TE. */
    "+CMGR", /* Read message from location value <index> in the message storage <mem1> of the ME. */
    "+CMGS", /* Send message from a TE to the network (SMS-SUBMIT). */
    "+CMSS", /* Send message from a specified location <index> in the message storage <mem2> of the ME to the network (SMS-SUBMIT or SMS-COMMAND). */
    "+CMGW", /* Store message (either SMS-DELIVER or SMS-SUBMIT) to the memory storage <mem2> of the ME. */
    "+CMGD", /* Delete message from preferred message storage <mem1> location <index> of the ME. */
    0
};

#if AT_ROLE_TERMINAL == XA_ENABLED
/*--------------------------------------------------------------------------- 
 * AT_Encode_Sms
 * 
 *     Encodes TE originated AT commands. 
 */ 
AtStatus TE_Encode_Sms(AtContext *Atc, U16 Type, const AtSmsCmd *Command, XaBufferDesc *Output)
{
    U16     avail = (Output->buffSize - Output->writeOffset) - 1;

    switch (Type) {
    default:
        return AT_STATUS_NOT_SUPPORTED;
    }
    return AT_STATUS_OK;
}

/*--------------------------------------------------------------------------- 
 * AT_Decode_Sms
 * 
 *     Decodes AT results received by the Common Unit (TE).
 */
AtStatus TE_Decode_Sms(AtContext *Atc, U16 *Type, AtSmsResult *Result, XaBufferDesc *Input)
{
    U8             *ptr = Input->buff + Input->readOffset;
    AtParameter     parms[AT_MAX_PARMS];

    AtParseParameters(ptr, (U16)(Input->writeOffset - Input->readOffset), parms);

    switch (*Type) {
    default:
        return AT_STATUS_NOT_SUPPORTED;
    }

    return AT_STATUS_OK;
}
#endif /* AT_ROLE_TERMINAL == XA_ENABLED */


#if AT_ROLE_MOBILE == XA_ENABLED
/*--------------------------------------------------------------------------- 
 * AT_Encode_Sms
 * 
 *     Encodes Audio Gateway (ME) originated AT results.
 */ 
AtStatus ME_Encode_Sms(AtContext *Atc, U16 Type, const AtSmsResult *Result, XaBufferDesc *Output)
{
    U16     avail = (Output->buffSize - Output->writeOffset) - 2;

    switch (Type) {
    default:
        return AT_STATUS_NOT_SUPPORTED;
    }

    return AT_STATUS_OK;
}
        
/*--------------------------------------------------------------------------- 
 * AT_Decode_Sms
 * 
 *     Decodes AT commands received by the Gateway (ME).
 */
AtStatus ME_Decode_Sms(AtContext *Atc, U16 *Type, AtSmsCmd *Command, XaBufferDesc *Input)
{
    U8             *ptr = Input->buff + Input->readOffset;
    AtParameter     parms[AT_MAX_PARMS];

    AtParseParameters(ptr, (U16)(Input->writeOffset - Input->readOffset), parms);

    switch (*Type) {
    default:
        return AT_STATUS_NOT_SUPPORTED;
    }

    return AT_STATUS_OK;
}
#endif /* AT_ROLE_MOBILE == XA_ENABLED */

#endif /* AT_SMS == XA_ENABLED */

