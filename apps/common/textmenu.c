/***************************************************************************
 *
 * File:
 *     $Workfile:textmenu.c$  for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:23$
 *
 * Description:
 *     This file contains the impelementation of the TextMenu library. It 
 *     provides the structure for building user menus in text or command
 *     prompt based user interface environments.
 *
 * Copyright 2000-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "textmenu.h"

extern void App_Report(char *format,...);

/****************************************************************************
 *
 * Internal Function prototypes
 *
 ****************************************************************************/
static BOOL MnuReadIntegerHandler(MnuTextMenu *Menu, MnuCommand Cmd, MnuArg Arg); 
static BOOL MnuReadStringHandler(MnuTextMenu *Menu, MnuCommand Cmd, MnuArg Arg);

#define isdigit(_K)     ((_K) < '0' || (_K) > '9' ? FALSE : TRUE)
#define ishex(_K)       (((_K) >= 'A' && (_K) <= 'F') || ((_K) >= 'a' && (_K) <= 'f') ? TRUE : FALSE)
#define isxdigit(_K)    (isdigit(_K) || ishex(_K))
#define tolower(_K)     ((_K) >= 'A' && (_K) <= 'Z' ? ((_K) - 'A') : (_K))


/****************************************************************************
 *
 * Internal RAM/ROM Data
 *
 ****************************************************************************/
static MnuTextMenu      *ActiveMenu;
static MnuReadInteger    MnuReadIntWs;
static MnuReadString     MnuReadStrWs;

static const MnuTextMenu MnuReadIntegerMenu = { 0, MnuReadIntegerHandler, 
                                                0, (U32)&MnuReadIntWs };
static const MnuTextMenu MnuReadStringMenu = { 0, MnuReadStringHandler, 
                                               0, (U32)&MnuReadStrWs };

/****************************************************************************
 *
 * Text Menu Public API
 *
 ****************************************************************************/

/*---------------------------------------------------------------------------
 *            Mnu_Load()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Loads the specified menu as the active menu.
 *
 * Return:    TRUE  - If new menu loaded successfully.
 *            FALSE - If new menu could not load.
 *                                 
 */
BOOL Mnu_Load(MnuTextMenu *Menu, BOOL Show)
{
    BOOL loaded;

    Assert (Menu && Menu->handler);

    loaded = (*Menu->handler)(Menu, M_ACTIVATE, ActiveMenu);
    if (loaded == TRUE)
        ActiveMenu = Menu;

    if (Show)
        Mnu_Show();

    return loaded;
}


/*---------------------------------------------------------------------------
 *            Mnu_Show()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Displays the currently active menu's options.
 *
 */
void Mnu_Show()
{
    const MnuItem    *m;

    if (ActiveMenu->title)
        App_Report("\n<----------------- %s ----------------->\n", ActiveMenu->title);

    (*ActiveMenu->handler)(ActiveMenu, M_SHOW, 0);

    if (ActiveMenu->items) {
        for (m = ActiveMenu->items; m->key; m++)
            if (m->text)
                App_Report("     %c    %s\n", m->key, m->text);

        App_Report("Enter Selection: \r");
    }
}


/*---------------------------------------------------------------------------
 *            Mnu_ProcessKey()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Processes keystrokes in the context of the current menu.
 *            Delivers any valid keystrokes to the menu handler.
 *
 */
MnuCommand Mnu_ProcessKey(char Key)
{
    const MnuItem *m;

    /* Some keys need special processing */
    if (Key == 0x03) {   /* Ctrl-C */
        return M_QUIT;
    }

    if (ActiveMenu) {
        if ((Key == '?' || Key == ' ') && ActiveMenu->items) {
            Mnu_EchoKeyNL(Key);
            Mnu_Show();
            return M_SHOW;
        }

        if (ActiveMenu->items) {
            for (m = ActiveMenu->items; m->key; m++) {
                if (tolower(m->key) == tolower(Key)) {
                    Mnu_EchoKeyNL(Key);
                    (*ActiveMenu->handler)(ActiveMenu, m->cmd, m->arg);
                    return m->cmd;
                }
            }
        } else {
            
            if ((*ActiveMenu->handler)(ActiveMenu, M_KEYSTROKE, &Key)) {
                Mnu_EchoKey(Key);
                return M_KEYSTROKE;
            }
        }
    }

    Mnu_EchoKey(KEY_BELL);    /* Invalid keystroke, Ring bell */
    return M_NOOP;
}


/*---------------------------------------------------------------------------
 *            Mnu_ShowPrompt()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Show the prompt to the user.
 *
 */
void Mnu_ShowPrompt(void)
{
    App_Report("Enter Selection: \r");
}


/*---------------------------------------------------------------------------
 *            Mnu_ReadInteger()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Reads an integer for the current menu. When the read operation
 *            is complete, the specified event is generated back to the parent
 *            with the MnuArg pointer pointing to a MnuReadInteger structure.
 *            The status & value parameters of the structure are valid during
 *            the event notification.
 */
void Mnu_ReadInteger(MnuCommand Event, U32 Min, U32 Max, U8 Base)
{
    Assert(Base == 10 || Base == 16);
    Assert(Min <= Max);

    ((MnuReadInteger *)(MnuReadIntegerMenu.workspace))->event = Event;
    ((MnuReadInteger *)(MnuReadIntegerMenu.workspace))->value = 0;
    ((MnuReadInteger *)(MnuReadIntegerMenu.workspace))->base = Base;
    ((MnuReadInteger *)(MnuReadIntegerMenu.workspace))->minValue = Min;
    ((MnuReadInteger *)(MnuReadIntegerMenu.workspace))->maxValue = Max;
    ((MnuReadInteger *)(MnuReadIntegerMenu.workspace))->status = MRERR_SUCCESS;

    Mnu_Load((MnuTextMenu *)&MnuReadIntegerMenu, FALSE);
}


/*---------------------------------------------------------------------------
 *            Mnu_ReadString()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Reads a string for the current menu. When the read operation
 *            is complete, the specified event is generated back to the parent
 *            with the MnuArg pointer pointing to a MnuReadString structure.
 *            The status, buffer and length parameters of the structure are
 *            valid during the event notification.
 */
void Mnu_ReadString(MnuCommand Event, void *Buffer, U16 BuffLen)
{
    ((MnuReadString *)MnuReadStringMenu.workspace)->event = Event;
    ((MnuReadString *)MnuReadStringMenu.workspace)->length = 0;
    ((MnuReadString *)MnuReadStringMenu.workspace)->maxLen = BuffLen-1;
    ((MnuReadString *)MnuReadStringMenu.workspace)->buffer = Buffer;
    ((MnuReadString *)MnuReadStringMenu.workspace)->status = MRERR_SUCCESS;

    Mnu_Load((MnuTextMenu *)&MnuReadStringMenu, FALSE);
}


/****************************************************************************
 *
 * Text Menu Internal functions
 *
 ****************************************************************************/

static BOOL MnuReadIntegerHandler(MnuTextMenu *Menu, MnuCommand Cmd, MnuArg Arg)
{
    MnuReadInteger *mri = (MnuReadInteger *)(MnuReadIntegerMenu.workspace);
    U16             i;

    switch (Cmd) {
    case M_ACTIVATE:
        /* Save old menu ptr */
        mri->parent = (MnuTextMenu *)Arg;
        break;

    case M_KEYSTROKE:
        switch (*(char *)Arg) {
        case KEY_ESC:
            /* Flag error and drop into next case to exit. */
            mri->status = MRERR_ESCAPE;

        case KEY_CR:
        case KEY_LF:
            if (mri->value < mri->minValue || mri->value > mri->maxValue)
                mri->status = MRERR_RANGE_ERR;

            /* Were done: Switch active menu back to parent
             * menu and deliver completion event to parent.
             */
            App_Report("\n");
            ActiveMenu = mri->parent;
            mri->parent->handler(mri->parent, mri->event, (MnuArg)mri);
            break;

        default:
            if (!isxdigit(*(char *)Arg) || (mri->base == 10 && !isdigit(*(char *)Arg))) {
                return FALSE;
            }

            if (isdigit(*(char *)Arg))
                i = *(char *)Arg - '0';
            else i = tolower(*(char *)Arg) - 'a';

            mri->value = (mri->value * mri->base) + i;
            break;
        }
        break;
    }

    return TRUE;
}

static BOOL MnuReadStringHandler(MnuTextMenu *Menu, MnuCommand Cmd, MnuArg Arg)
{
    MnuReadString   *mrs = (MnuReadString *)(MnuReadStringMenu.workspace);

    switch (Cmd) {
    case M_ACTIVATE:
        /* Save old menu ptr */
        mrs->parent = (MnuTextMenu *)Arg;
        break;

    case M_KEYSTROKE:
        switch (*(char *)Arg) {
        case KEY_ESC:
            /* Flag error and drop into next case to exit. */
            mrs->status = MRERR_ESCAPE;

        case KEY_CR:
        case KEY_LF:
            /* Were done: Switch active menu back to parent
             * menu and deliver completion event to parent.
             */
            App_Report("\n");
            mrs->buffer[mrs->length] = 0;
            ActiveMenu = mrs->parent;

            mrs->parent->handler(mrs->parent, mrs->event, (MnuArg)mrs);
            break;

        case KEY_BKSP:
            if (mrs->length == 0) {
                return FALSE;
            }
            mrs->length--;
            break;

        default:
            if (mrs->length < mrs->maxLen)
                mrs->buffer[mrs->length++] = *(char *)Arg;
            else return FALSE;
            break;
        }
        break;
    }

    return TRUE;
}


void MnuEchoKey(char Key, char Newline) 
{
    if (Key == KEY_ESC || Key == KEY_CR)
        return;

    if (Key == KEY_BKSP) {
        App_Report("%c %c%c", KEY_BKSP, KEY_BKSP, Newline);
        return;
    }

    App_Report("%c%c", Key, Newline);
}
