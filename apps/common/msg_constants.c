/***************************************************************************
 *
 * File:
 *     $Workfile:msg_constants.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:18$
 *
 * Description:
 *     This file contains functions for converting #defines to
 *     strings. It is for use by applications only.
 *
 * Copyright 1999-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "bttypes.h"

#if XA_DEBUG == XA_DISABLED
/* When debug is off, we reenable it and include debug.c to keep the
 * message string conversion functions around for the application.
 */
#undef XA_DEBUG
#define XA_DEBUG    XA_ENABLED
#include "../../stack/debug.c"
#endif

