/***************************************************************************
 *
 * File:
 *     $Workfile:ring.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:29$
 *
 * Description:
 *      This file contains a collection of utilities to manage ring- or
 *      circular buffers.
 *
 * Copyright 1999-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "ring.h"
#include "utils.h"


/*---------------------------------------------------------------------------
 * Initialize a ring buffer structure.
 *
 * Context: 
 *    Args: ring - pointer to RingBuf struct
 *          buf  - allocated buffer which will become the ring buffer
 *          len  - length of buf
 *  Return: 
 */
void RING_BufInit(RingBuf *ring, char buf[], S16 len)
{
    ring->start = buf;
    ring->end   = buf + len;    /* 'end' is 1 beyond of the buffer */
    RING_BufFlush(ring);
}


/*---------------------------------------------------------------------------
 * RING_BufFlush() 
 *      Clear data from a ring buffer.
 *
 * Requires:
 *     
 * Parameters:
 *      Pointer to RingBuf struct.
 *
 * Returns:
 */
void RING_BufFlush(RingBuf *ring)
{
    ring->data    = ring->start;
    ring->free    = ring->start;
    ring->dataLen = 0;
}


/*---------------------------------------------------------------------------
 * Copy data into the ring buffer from caller's buffer.
 * Does NOT overwrite data already in the buffer.
 *
 * Context: 
 *    Args: ring - pointer to RingBuf struct
 *          buf  - caller's destination buffer
 *          len  - max amount to copy
 *  Return: actual amount copied to buf
 */
S16 RING_WriteData(RingBuf *ring, char *buf, S16 len)
{
    S16 bytes;
    
    bytes = min(len, RING_FreeSpace(ring));

    len = bytes;
    ring->dataLen += bytes;

    while (bytes--) {
        *ring->free++ = *buf++;

        if (ring->free >= ring->end) {
            ring->free = ring->start;
        }
    }

    return len;
}


/*---------------------------------------------------------------------------
 * Move data from the ring buffer to caller's buffer.
 * Once the data is copied, the space in the ring buffer is considered
 * free.
 *
 * Context: 
 *    Args: ring - pointer to RingBuf struct
 *          buf  - caller's destination buffer
 *          len  - max amount to copy
 *  Return: actual amount copied to buf
 */
S16 RING_ReadData(RingBuf *ring, char *buf, S16 len)
{
    S16 bytes = 0;       /* bytes copied */
    
    len = min(len, ring->dataLen);

    while (bytes < len) {
        *buf++ = *ring->data++;
        bytes++;

        if (ring->data >= ring->end) {
            ring->data = ring->start;
        }
    }

    ring->dataLen -= bytes;
    
    return bytes;
}
    

/*---------------------------------------------------------------------------
 * RING_GetDataPtr() 
 *      Get a pointer to the data and the number of bytes. The number of
 *      bytes is for contiguous data only. This function might be used
 *      by a routine that did not have it's own buffer yet wanted access
 *      to data in the ring buffer.
 *
 * Requires:
 *     
 * Parameters:
 *      ring    pointer to ring buffer structure
 *      data    address of pointer that this function will update
 *      len     pointer to number of bytes requested
 *
 * Returns:
 *      data    contains pointer to beginning of data
 *      len     contains number of bytes available not exceeding original
 *              value; len represents the number of contiguous bytes.
 */
void RING_GetDataPtr(RingBuf *ring, char **data, S16 *len)
{
    *data = ring->data;
    *len  = min(*len, RING_ContigDataLen(ring));
}


/*---------------------------------------------------------------------------
 * RING_BufDelete() 
 *      Deletes bytes from the buffer. Bytes are removed starting with
 *      the oldest or "first in" data.
 *
 * Requires:
 *     
 * Parameters:
 *      ring    pointer to ring buffer structure
 *      len     bytes to delete; if len > existing bytes, existing bytes
 *              are removed
 * Returns:
 */
void RING_BufDelete(RingBuf *ring, S16 len)
{
    S16 wrap;

    len = min(len, RING_DataLen(ring));

    ring->data += len;
    ring->dataLen -= len;
    wrap = ring->data - ring->end;

    if (wrap >= 0) {
        ring->data = ring->start + wrap;
    }
}

