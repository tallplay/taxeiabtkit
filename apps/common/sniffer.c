/***************************************************************************
 *
 * File:
 *     $Workfile:sniffer.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:145$
 *
 * Description:
 *     Source code for sniffer protocol decode library.
 *
 * Copyright 1999-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "stdio.h"
#include "string.h"
#include "hcitrans.h"
#include "rfcomm.h"
#include "sdp.h"
#include "sniffer.h"

//#if XA_DEBUG == XA_ENABLED
#if 1
/* Uncomment this line to halt the sniffer when a decode error occurs. */
#define SNIFFER_STOP    /* __asm int 3 */

static XaSnifferCore Sniffer = {0}; /* Zeroes out sniffer context*/

#define BUF_TYPES (HCI_BUFTYPE_COMMAND|HCI_BUFTYPE_EVENT|HCI_BUFTYPE_ACL_DATA|HCI_BUFTYPE_SCO_DATA)

/*****************************************************************************
 *
 * Internal Function Prototypes
 *
 ****************************************************************************/
static void        DisplayHex(const U8 *Buffer, U16 Len);
static void        DisplayPacket(const char *Msg, const U8 *Buffer, U16 Len);
static void        DisplayStage(void);
static void        DisplayHexInternal(const U8 *Buffer, U16 Len);
static const char *ShowHeader(const char *Msg, const U8 *Buffer, U16 Len);
static const char *L2CAP_Parse(const U8 *Buffer, U16 BufferLen, BOOL First);
static const char *L2CAP_ParseBroadcast(const U8 *Buffer, U16 BufferLen);
static const char *SDP_Parse(const U8 *Buffer, U16 BufferLen, BOOL First);
static const char *RFCOMM_Parse(const U8 *Buffer, U16 BufferLen);
static void        HciCommand_Parse(const HciPacket *packet);
static void        HciEvent_Parse(const U8 *Buffer, U16 Len);
static const char *pObexRsp(U8 RespCode);
static const char *pObexOpcode(U8 Opcode);
static const char *pObexSessOpcode(U8 Opcode);
static const char *OBEX_Parse(struct _ObexContext *obp, const U8 *RxBuff, U16 RxLen);
static const char *InvokeRfcLayeredParser(U8 Channel, const U8 *Buffer, U16 BufferLen);
static void        InitObexParser(struct _ObexContext *obp);
static const char *pHC_LinkType(U8 LinkType);
static const char *pHC_AirMode(U8 scoMode);
static const char *pHC_EncryptMode(U8 Mode);
static const char *pHC_ClassOfDevice(U8 *p, char *string);
static char       *pPacketType(U16 Type, char *typeString);
static const char *pPsrMode(U8 Mode);
static const char *pPsMode(U8 Mode);
static const char *pScanEnable(U8 Mode);
static const char *pTimeOffset(void);
static ChannelInfo *NewChannelInfo(SniffEndpoint *Pattern);
#if TCS_CORDLESS == XA_ENABLED || TCS_INTERCOM == XA_ENABLED
const char *SNIFFER_TCS_Parse(const U8 *Buffer, U16 BufferLen);
#endif

#define SnifferMsg(MSG)     (*Sniffer.output)(MSG)

/*****************************************************************************
 *
 * Sniffer Main Entrypoints
 *
 ****************************************************************************/

/*---------------------------------------------------------------------------
 * SNIFFER_SetOption
 *
 */
BOOL SNIFFER_SetOption(U8 OptionType, void *OptionValue)
{
    switch (OptionType) {
    case SO_DISPLAY_FILTER:
        Sniffer.filter = *(SniffDispFilter *)OptionValue;
        break;

    case SO_DISPLAY_HANDLER:
        Sniffer.output = *(SniffDispHandler *)OptionValue;
        break;

    default:
        return FALSE;
    }

    return TRUE;
}

/*---------------------------------------------------------------------------
 * SNIFFER_GetOption
 *
 */
BOOL SNIFFER_GetOption(U8 OptionType, void *OptionValue)
{
    switch (OptionType) {
    case SO_DISPLAY_FILTER:
        *((SniffDispFilter *)OptionValue) = Sniffer.filter;
        break;

    case SO_DISPLAY_HANDLER:
        *((SniffDispHandler *)OptionValue) = Sniffer.output;
        break;

    default:
        return FALSE;
    }

    return TRUE;
}


/*---------------------------------------------------------------------------
 * SNIFFER_Reset
 *
 *     Reset the state of the sniffer to it's initial state. This function 
 *     is called when the sniffer is re-opened or when a critical error is
 *     encountered.
 */
void SNIFFER_Reset(BOOL Full)
{
    SniffDispHandler    handler = Sniffer.output;
    SniffDispFilter     filter = Sniffer.filter;
    I8                  i;

    if (Full || !Sniffer.reset) {

        /* Reinitialize the connection tables. */
        OS_MemSet((U8 *)&Sniffer, 0, sizeof(XaSnifferCore));

        /* Initialize the channel info structures */
        InitializeListHead(&Sniffer.channelList);
        for (i = 0; i < MAX_CHANNEL_INFO_STRUCTS; i++)
            InsertTailList(&Sniffer.channelList, &(Sniffer.channelTable[i].node));

        Sniffer.reset = TRUE;

    } else {

        /* Mark the packet parsers out of sync */
        OS_MemSet((U8 *)&Sniffer.rx, 0, sizeof(PacketInfo));
        OS_MemSet((U8 *)&Sniffer.tx, 0, sizeof(PacketInfo));

        /* Mark all the channel parser contexts out of sync */
        for (i = 0; i < MAX_CHANNEL_INFO_STRUCTS; i++) {
            OS_MemSet((void *)&Sniffer.channelTable[i].info, 0, sizeof(Sniffer.channelTable[i].info));
            if (Sniffer.channelTable[i].endpoint.type == SPI_OBEX)
                InitObexParser(&Sniffer.channelTable[i].info.obContext);
        }
    }

    /* Re-initialize time delta and restore saved settings */
    Sniffer.timeStart = 0;
    Sniffer.output = handler;
    Sniffer.filter = filter;
}


/*---------------------------------------------------------------------------
 * SNIFFER_RegisterEndpoint
 *
 *     
 *     
 *     
 */
void SNIFFER_RegisterEndpoint(SniffEndpoint *Endpoint)
{
    ChannelInfo *ci;

    if (!Sniffer.reset) 
        SNIFFER_Reset(TRUE);

    /* Make sure SPI_L2CAP is set in L2CAP FEC case, too. */
    if (Endpoint->proto & SPI_L2CAP_FEC)
        Endpoint->proto |= SPI_L2CAP;

    if (ci = NewChannelInfo(Endpoint)) {
        if (ci->endpoint.type == SPI_OBEX) {
            InitObexParser(&ci->info.obContext);
        }
    }
}


/*---------------------------------------------------------------------------
 * SNIFFER_DisplayHciPacket
 *
 *     This function decodes and displays a HCI transmit packet.
 */
void SNIFFER_DisplayHciPacket(const HciPacket *packet, U32 TimeStamp)
{
    U8      j;   
    char    msg[30];

    if (!Sniffer.output || !Sniffer.reset) return;

    Sniffer.timeStamp = TimeStamp;
    Sniffer.context = &Sniffer.tx;
    Sniffer.direction = SD_OUTBOUND;

    switch (packet->flags & BUF_TYPES) {
    case HCI_BUFTYPE_COMMAND:
        HciCommand_Parse(packet);
        break;

    case HCI_BUFTYPE_ACL_DATA:
        DisplayPacket("ACL-TX: ", packet->header, 4);

        for (j = 0; j < packet->fCount; j++)
            DisplayPacket("        ", packet->f[j].buffer, packet->f[j].len);
        break;

    case HCI_BUFTYPE_SCO_DATA:
        sprintf(msg, "SCO-TX: %s [Length %d]", pTimeOffset(), packet->header[2]);
        SnifferMsg(msg);
        DisplayHex(packet->header, 3);

        for (j = 0; j < packet->fCount; j++)
            DisplayHex(packet->f[j].buffer, packet->f[j].len);
        break;

    default:
        SnifferMsg("ERROR: Unknown HCI Packet Type!");
        SNIFFER_STOP;
        break;
    }

    DisplayStage();
}


/*---------------------------------------------------------------------------
 * SNIFFER_DisplayHciBuffer
 *
 *     This function decodes and displays a HCI receive buffer.
 */
void SNIFFER_DisplayHciBuffer(const HciBuffer *buffer, U32 TimeStamp)
{
    char msg[30];
    
    if (!Sniffer.output || !Sniffer.reset) return;

    Sniffer.timeStamp = TimeStamp;
    Sniffer.context = &Sniffer.rx;
    Sniffer.direction = SD_INBOUND;

    switch (buffer->flags & BUF_TYPES) {
    case HCI_BUFTYPE_EVENT:
        HciEvent_Parse(buffer->buffer, buffer->len);
        break;

    case HCI_BUFTYPE_ACL_DATA:
        DisplayPacket("ACL-RX: ", buffer->buffer, buffer->len);
        break;

    case HCI_BUFTYPE_SCO_DATA:
        sprintf(msg, "SCO-RX: %s [Length %d]", pTimeOffset(), buffer->buffer[2]);
        SnifferMsg(msg);

        DisplayHex(buffer->buffer + 3, (U16)(buffer->len - 3));
        break;

    default:
        SnifferMsg("ERROR: Unknown HCI Buffer Type!");
        SNIFFER_STOP;
        break;
    }

    DisplayStage();
}


#if 0
/*---------------------------------------------------------------------------
 * SNIFFER_DisplayLmpPacket()
 *
 *     This function dumps an INET-LMP packet.
 */
#include "\minios\hcitrans\inet\hostctlr.h"

void SNIFFER_DisplayLmpPacket(const char *RxTx, const U8 *Packet, U16 Len, U32 TimeStamp)
{
    static const char *pLmpType[] = { "CONN REQ", "ACCEPTED", "SWITCH REQ", 
                                      "SETUP COMPLETE", "NOT ACCEPTED", "DETACH",
                                      "IN RAND", "COMB KEY", "AU RAND", "SRES",
                                      "SCO REQ","REMOVE SCO"};
    char         output[50];
    U16          hcp;

    if (!Sniffer.output || !Sniffer.reset) return;

    Sniffer.timeStamp = TimeStamp;

    hcp = LEtoHost16(Packet);
    if (hcp < HCP_LMP_CONN_REQ || hcp > HCP_LMP_REMOVE_SCO_REQ) {
        return;        /* We don't decode it */
    }
    hcp -= HCP_LMP_CONN_REQ;

    sprintf(output, "LMP-%s : %s %s [Length %d]", RxTx, pLmpType[hcp], pTimeOffset(), Len-2);
    SnifferMsg(output);

    if (Len > 2)
        DisplayHex(Packet+2, (U16)(Len-2));
}
#else
void SNIFFER_DisplayLmpPacket(const char *RxTx, const U8 *Packet, U16 Len, U32 TimeStamp) {}
#endif

/*****************************************************************************
 *                                                                           *
 *             Internal Sniffer Display & Decode Functions                   *
 *                                                                           *
 *****************************************************************************/
/*
 * Calls functions to display ACL packet data.
 */
static void DisplayPacket(const char *Msg, const U8 *Buffer, U16 Len)
{
    const U8 *p;

    p = (const U8 *)ShowHeader(Msg, Buffer, Len);

    DisplayHex(p, (U16)(Len - (p - Buffer)));
    return;
}


/* 
 * Returns a channel info structure to match the specified pattern. If there is
 * no match, a new ChannelInfo structure is filled with the pattern and returned.
 */
static ChannelInfo *GetChannelInfo(SniffDirection Direction, SniffEndpoint *Pattern)
{
    ChannelInfo     *found;

#if XA_DEBUG == XA_ENABLED
    if (!IsListCircular(&Sniffer.channelList)) {
        SnifferMsg("ERROR: Channel list corrupted.");
        SNIFFER_STOP;
        SNIFFER_Reset(TRUE);
    }
#endif
    /* Iterate through all channels in the list, looking for a match.
     * Start from the back since that's where the most recent ones live.
     */
    for (found = (ChannelInfo *)Sniffer.channelList.Blink;
         found != (ChannelInfo *)&Sniffer.channelList;
         found = (ChannelInfo *)found->node.Blink) {

        if ((found->endpoint.proto & ~SPI_L2CAP_FEC) ==
            (Pattern->proto & ~SPI_L2CAP_FEC)) {

            /* Evaluate grounds for dismissing the found structure as a
             * potential match 
             */
            if ((Pattern->proto & SPI_LM) &&
                (found->endpoint.lmp != Pattern->lmp))
                continue;

            if (Pattern->proto & SPI_L2CAP) {
                if ((Direction & SD_OUTBOUND) &&
                    (found->endpoint.l2cap_remote != Pattern->l2cap))
                    continue;
                if ((Direction & SD_INBOUND) &&
                    (found->endpoint.l2cap != Pattern->l2cap))
                    continue;
            }

            if ((Pattern->proto & SPI_RFCOMM) &&
                (found->endpoint.rfcomm != Pattern->rfcomm))
                continue;

            /* We have a match so quit looking */
            break;
        }
    }
     
    if (found != (ChannelInfo *)&Sniffer.channelList) {
        /* If we found a match, grab it as-is */
        RemoveEntryList(&found->node);

        /* Promote the found channel info to the end of the list.  */
        InsertTailList(&Sniffer.channelList, &found->node);

#if XA_DEBUG == XA_ENABLED
        if (!IsListCircular(&Sniffer.channelList)) {
            SnifferMsg("ERROR: Channel list corrupted on return.");
            SNIFFER_STOP;
        }
#endif
        return found;
    } 

    return 0;
}


ChannelInfo *NewChannelInfo(SniffEndpoint *Pattern)
{
    ChannelInfo   *found;

    found = GetChannelInfo((SD_INBOUND|SD_OUTBOUND), Pattern);
    if (found == 0) {
        /* We did NOT find a match so we need to pluck one from
         * the front and prepare it.
         */
        found = (ChannelInfo *)RemoveHeadList(&Sniffer.channelList);

        /* Promote the found channel info to the end of the list. */
        InsertTailList(&Sniffer.channelList, &found->node);
    } else {
        Report(("SNIFFER: Reallocating existing matching channel.\n"));
    }
    OS_MemSet((void *)&found->info, 0, sizeof(found->info));

    found->endpoint = *Pattern;

    return found;
}


/*****************************************************************************
 *                                                                           *
 *                 Sniffer Output and Parsing functions                      *
 *                                                                           *
 *****************************************************************************/
/*
 * Displays hex output of provided data. In general this function
 * dumps any bytes not consumed by the protocol parsers.
 */
static void DisplayHex(const U8 *Buffer, U16 Len)
{
    U16       len = 0;

    /* Check for illegal lengths caused by rollovers. */
    if (Len > 0x7FFF) {
        SnifferMsg("ERROR: Invalid buffer length.");
        SNIFFER_STOP;
        return;
    }

    /* If there's less than a line to dump, stage it for later. */
    if (Len + Sniffer.stageLen < LINE_WIDTH) {
        OS_MemCopy(Sniffer.stage+Sniffer.stageLen, Buffer, Len);
        Sniffer.stageLen += Len;
        return;
    }

    /* If we have staged bytes, first try to fill the stage and dump it. */
    if (Sniffer.stageLen) {
        len = LINE_WIDTH - Sniffer.stageLen;
        Assert(len <= Len);
        OS_MemCopy(Sniffer.stage+Sniffer.stageLen, Buffer, len);
        /* Dump whats been staged */
        DisplayHexInternal(Sniffer.stage, (U16)(Sniffer.stageLen+len));
        Sniffer.stageLen = 0;
        Buffer += len;
        Len -= len;
    }

    /* Only send full lines to sniffer dump function. */
    len = (Len / LINE_WIDTH) * LINE_WIDTH;

    DisplayHexInternal(Buffer, len);

    /* Stage whatever is left. */
    if (Len > len) {
        OS_MemCopy(Sniffer.stage+Sniffer.stageLen, Buffer+len, Len - len);
        Sniffer.stageLen += Len - len;
        return;
    }
}

/* 
 * Displays any bytes still present in the stage buffer.
 */
static void DisplayStage(void)
{
    if (Sniffer.stageLen) {
        DisplayHexInternal(Sniffer.stage, Sniffer.stageLen);
        Sniffer.stageLen = 0;
    }
}

static void DisplayHexInternal(const U8 *Buffer, U16 Len)
{
    const U8 *p = Buffer;
    char     *o, output[(LINE_WIDTH * 4)+20]; /* 12 bytes padding */
    U16       po;

    po = 0;
    while (p < Buffer+Len) {
        o = output;

        /* Append proper line advance spaces */
        o += sprintf(o, "         ");

        /* Dumps the packet in basic ASCII dump format */
        po = 0;
        while (po < LINE_WIDTH) {
            if (p+po < Buffer+Len)
                o += sprintf(o, "%02x ", p[po]);
            else o += sprintf(o, "   ");
            if (++po == LINE_WIDTH/2) o += sprintf(o, "  ");
        }
        o += sprintf(o, "    ");

        po = 0;
        while (po < LINE_WIDTH) {
            if (p+po < Buffer+Len)
                o += sprintf(o, "%1c", ((p[po] >= ' ' && p[po] <= '~') ? p[po] : '.'));
            else break;
            po++;
        }

        SnifferMsg(output);
        p += po;
    }
}


/*
 * Decodes LMP header a passes data to next layer decoders
 */
const char *ShowHeader(const char *Msg, const U8 *Buffer, U16 BufferLen)
{
    U16             Opcode = Sniffer.context->lm_opcode;
    U16             llcLen = Sniffer.context->lm_llcLen;
    U16             lmLen = Sniffer.context->lm_lmLen;
    char            output[LINE_LENGTH];
    const char     *type;

    /*  sprintf(output, "%sShowHeader: In Opcode %04x, lmLen %d llcLen %d, BufferLen %d", Msg, Opcode, lmLen, llcLen, BufferLen);
        SnifferMsg(output);
    */

    if (Opcode == 0) {
        /* Buffer Len must be at least four bytes to retrieve HCI header. */
        if (BufferLen < 4) {
            SnifferMsg("ERROR: Buffer Length Underflow.");
            SNIFFER_STOP;
            return (const char *)(Buffer + BufferLen);
        }

        Opcode = LEtoHost16((U8 *)Buffer);

        /* Figure out the Opcode based on the HCI handle bits */
        if (Opcode & ACTIVE_BROADCAST) {
            if (Opcode & FIRST_PACKET) {
                type = "Active B'cast Data First";
            } else {
                type = "Active B'cast Data Continue";
            }
        } else if (Opcode & PICONET_BROADCAST) {
            if (Opcode & FIRST_PACKET) {
                type = "Piconet B'cast Data First";
            } else {
                type = "Piconet B'cast Data Continue";
            }
        } else {
            if (Opcode & FIRST_PACKET) {
                type = "Data First";
            } else {
                type = "Data Continue";
            }
        }
        /* Read the current handle and other details */
        Sniffer.context->lm_connHandle = Opcode & 0x0FFF;
        llcLen = LEtoHost16((U8 *)(Buffer+2));
        lmLen = 4;

        sprintf(output, "%s %s %s [Length %d]", Msg, type, pTimeOffset(), llcLen);
        SnifferMsg(output);

        if (Opcode & FIRST_PACKET) {
            Sniffer.context->l2cap_state = 0;
            Sniffer.context->lm_inSync = TRUE;
        }
    }
    
    if (BufferLen >= lmLen) {
        /* We have more data then the LmHeader */
        BufferLen -= lmLen;
        Buffer += lmLen;
        lmLen = 0;
    } else {
        /* Lm header is split across packets */
        lmLen -= BufferLen;
        Buffer += BufferLen;
        BufferLen = 0;
    }

    if ((BufferLen > 0x7FFF) || (lmLen > 0x7FFF)) {
        SnifferMsg("ERROR: Buffer or LM Length Underflow.");
        SNIFFER_STOP;
        return (const char *)(Buffer + BufferLen);
    }

    /* We have advanced past the LMP Header. If we are at the start
     * of an L2CAP packet, pass the data to the higher layer parsers.
     */
    if (BufferLen > 0) {
        if (Sniffer.context->lm_inSync && !(Sniffer.filter & SDF_DISPLAY_RAW)) {

            if (Opcode & (ACTIVE_BROADCAST|PICONET_BROADCAST))
                Buffer = L2CAP_ParseBroadcast(Buffer, BufferLen);
            else Buffer = L2CAP_Parse(Buffer, BufferLen, ((Opcode & FIRST_PACKET) ? TRUE : FALSE));
        }

        llcLen -= BufferLen;
    }

    if ((lmLen == 0) && (llcLen == 0)) {
        /* No more data expected for this packet */
        Opcode = 0;
    }

    if (llcLen > 0x7FFF) {
        sprintf(output, "ERROR: llc length Underflow: %lx",llcLen);
        SnifferMsg(output);
        SNIFFER_STOP;
        return (Buffer + BufferLen);
    }

    /* Copy the local values back into the context buffer */
    Sniffer.context->lm_opcode = Opcode;
    Sniffer.context->lm_llcLen = llcLen;
    Sniffer.context->lm_lmLen = lmLen;

    /* This Buffer ptr has already shimmed off Parsed Headers */
    return Buffer;
}


static const char *pTimeOffset(void)
{
    U32     delta;

    if (Sniffer.timeStart == 0)
        Sniffer.timeStart = Sniffer.timeStamp;

    delta = Sniffer.timeStamp - Sniffer.timeStart;

    sprintf(Sniffer.timeBuff, "+%02ld.%03lds", delta/1000, delta%1000);

    return Sniffer.timeBuff;
}


/*
 *---------------------------------------------------------------------------
 *            L2CAP Parsing functions
 *---------------------------------------------------------------------------
 */
/*
 * Parses L2CAP Connection Oriented Packet Headers and Signalling packet headers.
 */
static const char *L2CAP_Parse(const U8 *Buffer, U16 BufferLen, BOOL First)
{
    U8             *hdrBuff = Sniffer.context->l2cap_hdrBuff;    /* No need to copy out :) */
    U16             state = Sniffer.context->l2cap_state;
    U16             cid = Sniffer.context->l2cap_cid;
    SniffEndpoint   pattern;
    ChannelInfo    *ci;
    SniffProtocolId type;
    char            output[LINE_LENGTH];
    U8              sar = 0;

    /* sprintf(output, " L2CAP: In BufferLen %d, %s Segment, State %d", BufferLen, (First ? "First" : "Continued"), state);
     * SnifferMsg(output);
     */

    while (BufferLen > 0) {
        
        if (state < 4) { 
            /* Collect L2CAP Header */
            hdrBuff[state++] = *Buffer++;
            BufferLen--;
        }

        if (state > 4 && state < 9) {
            /* Collect L2CAP Signalling Header */
            hdrBuff[state++-5] = *Buffer++;
            BufferLen--;
        }

        if (state > 11 && state < 16) {
            /* Collect Flow & Error Control header */
            hdrBuff[state++-8] = *Buffer++;
            BufferLen--;
        }

        /* L2CAP Header is staged */
        switch (state) {
        case 4:
            /* First time through for this packet */
            cid = LEtoHost16(hdrBuff+2);
            Sniffer.context->l2cap_len = LEtoHost16(hdrBuff);
            sprintf(output, "         L2CAP: Packet Length %d for Channel Id %04x", LEtoHost16(hdrBuff), cid);
            SnifferMsg(output);

            if (cid == 0x0001)
                state = 5;
            else state = 11;
            break;

        case 9:
            /* Print out Signalling Packet Data */
            sprintf(output, "         L2CAP: %s Ident %02X, Length %04x",  
                    pL2CAP_SignalOpcode(hdrBuff[0]), hdrBuff[1], LEtoHost16(hdrBuff+2));
            SnifferMsg(output);

            /* Fall through to next state */

        case 10:
            if (BufferLen > LEtoHost16(hdrBuff+2)) {
                /* Dump content of Signalling command */
                DisplayHex(Buffer, LEtoHost16(hdrBuff+2));

                /* Advance buffer */
                BufferLen -= LEtoHost16(hdrBuff+2);
                Buffer += LEtoHost16(hdrBuff+2);
                state = 5;

            } else {
                BufferLen = 0;
                state = 10;
            }
            break;

        case 11:
        case 17:
            /* Let's look up the PSM by what has gone before. */
            pattern.proto = SPI_L2CAP|SPI_LM;
            pattern.lmp = Sniffer.context->lm_connHandle;
            pattern.l2cap = cid;
            type = 0;

            if (ci = GetChannelInfo(Sniffer.direction, &pattern)) {
                type = ci->endpoint.type;
#if L2CAP_FLOW_CONTROL == XA_ENABLED
                if (ci->endpoint.proto & SPI_L2CAP_FEC) {
                    if (state == 11) {
                        /* Need to process FEC header and FCS bytes first. */
                        state = 12;
                        break;
                    }
                    /* Shim off the FCS if this is the final fragment. */
                    if (Sniffer.context->l2cap_len == BufferLen)
                        BufferLen -= min(BufferLen, 2);

                    if (Sniffer.context->l2cap_inSync == FALSE)
                        type = 0;
                }
#endif /* L2CAP_FLOW_CONTROL == XA_ENABLED */
            }

            /* Demux protocol based on the PSM */
            switch (type) {
            case SPI_SDP:
                Assert(ci->endpoint.type == SPI_SDP);
                Buffer = SDP_Parse(Buffer, BufferLen, First);
                break;

            case SPI_RFCOMM:
                Buffer = (U8 *)RFCOMM_Parse(Buffer, BufferLen);
                break;

#if TCS_CORDLESS == XA_ENABLED || TCS_INTERCOM == XA_ENABLED
            case SPI_TCS:
                Buffer = SNIFFER_TCS_Parse(Buffer, BufferLen);
                break;
#endif
            case SPI_DATA:
                break; 

            default:
                Report(("Sniffer: Unknown Channel %x!\n", cid));
                break;
            }

            Sniffer.context->l2cap_len -= BufferLen;

            /* Terminate data processing loop */
            BufferLen = 0;
            break;

#if L2CAP_FLOW_CONTROL == XA_ENABLED
        case 14:
            /* Process FEC header in S-Frames */
            Sniffer.context->l2cap_len -= 2;

            if (hdrBuff[4] & 0x01) {
                /* S-Frame */
                sprintf(output, "         L2CAP: %s S-Frame: Nr %d %s", 
                        ((hdrBuff[4] & 0x0D) == 0x05 ? "REJ" : "RR"), (hdrBuff[5] & 0x3f),
                        ((hdrBuff[4] & 0x80) ? "(No Retransmission)" : ""));
                SnifferMsg(output);
                state = 17;
                break;
            }

            if ((hdrBuff[5] & 0xC0) == 0x40)    /* Stage SAR-START SDU length field. */
                break;

            /* Drop into next case */

        case 16:
            /* Process FEC header and SDU length field in I-Frames */
            sar = hdrBuff[5] & 0xC0;

            if (sar == 0x40)
                Sniffer.context->l2cap_len -= 2;

            if ((sar == 0x40) || (sar == 0x00))
                Sniffer.context->l2cap_inSync = TRUE;

            sprintf(output, "         L2CAP: %s I-Frame: Nr %d Ns %d, dataLen %d %s",
                    (sar == 0x00 ? "Unsegmented" : (sar == 0x40 ? "Starting" : (sar == 0x80 ? "Ending" : "Continuation"))),
                    (hdrBuff[5] & 0x3f), ((hdrBuff[4] >> 1) & 0x3f), 
                    ((sar == 0x40) ? LEtoHost16(hdrBuff+6) : Sniffer.context->l2cap_len - 2),
                    (hdrBuff[4] & 0x80 ? "(No Retransmission)" : ""));
            SnifferMsg(output);
            state = 17;
            break;
#endif /* L2CAP_FLOW_CONTROL == XA_ENABLED */
        }

    } /* end while */

    Sniffer.context->l2cap_state = state;
    Sniffer.context->l2cap_cid = cid;

    return Buffer;
}

/*
 * Parse L2CAP Broadcast Packets. Only parses the Connectionless Header.
 */
static const char *L2CAP_ParseBroadcast(const U8 *Buffer, U16 BufferLen)
{
    U8     *hdrBuff = Sniffer.context->l2cap_hdrBuff;    /* No need to copy out :) */
    U16     state = Sniffer.context->l2cap_state;
    char    output[LINE_LENGTH];

    while (BufferLen > 0) {

        if (state < 6) {
            /* Collect Header */
            hdrBuff[state++] = *Buffer++;
            BufferLen--;
            continue;
        } 
        else break;
    }

    if (state == 6) {
        /* Header is staged */
        if (LEtoHost16(hdrBuff+2) != 0x0002) {
            SNIFFER_STOP;
        }

        sprintf(output, "         L2CAP: Packet Length %04x for PSM %04x", LEtoHost16(hdrBuff), LEtoHost16(hdrBuff+4));
        SnifferMsg(output);
        state++;
    }

    Sniffer.context->l2cap_state = state;

    return Buffer;
}

/*
 *---------------------------------------------------------------------------
 *            RFCOMM Parsing functions
 *---------------------------------------------------------------------------
 */
const char *pRFC_Command(U8 Command)
{
    const char *cmdStr;

    switch (Command) {
    case 0x3F:
        cmdStr = "SABM";
        break;

    case 0x73:
        cmdStr = "UA_F";
        break;

    case 0x1F:
        cmdStr = "DM_F";
        break;

    case 0x0F:
        cmdStr = "DM";
        break;

    case 0x53:
        cmdStr = "DISC";
        break;

    case 0xEF:
        cmdStr = "UIH";
        break;

    case 0xFF:
        cmdStr = "UIH_F";
        break;

    default:
        cmdStr = "UNKNOWN";
        break;
    }

    return cmdStr;
}

const char *pRFC_MuxCommand(U8 Command)
{
    const char *cmdStr;

    switch(Command) {
    case 0x81:
        cmdStr = "PN";
        break;

    case 0x21:
        cmdStr = "TEST";
        break;

    case 0xA1:
        cmdStr = "FCON";
        break;

    case 0x61:
        cmdStr = "FCOFF";
        break;

    case 0xE1:
        cmdStr = "MSC";
        break;

    case 0x11:
        cmdStr = "NSC";
        break;

    case 0x91:
        cmdStr = "RPN";
        break;

    case 0x51:
        cmdStr = "RLS";
        break;

    default:
        cmdStr = "UNKNOWN";
        break;
    }       

    return cmdStr;
}

/*
 * Returns the buad rate string 
 */
const char *pRFC_BaudRate(U8 MSCBaud)
{
    const char *baudStr;

    switch(MSCBaud) {
    case 0:
        baudStr = "2400";
        break;

    case 1:
        baudStr = "4800";
        break;

    case 2:
        baudStr = "7200";
        break;

    case 3:
        baudStr = "9600";
        break;

    case 4:
        baudStr = "19200";
        break;

    case 5:
        baudStr = "38400";
        break;

    case 6:
        baudStr = "57600";
        break;

    case 7:
        baudStr = "115200";
        break;

    case 8:
        baudStr = "230400";
    }

    return baudStr;
}

/*
 * Parses RFCOMM Control Channel data.
 */
static const char *RFCTL_Parse(U8 Cmd, const U8 *Buffer, U16 BufferLen)
{
    switch(Cmd) {
    case 0x81:
        sprintf(Sniffer.context->output, "%s, Channel %d", Sniffer.context->output, Buffer[0] >> 1);
        SnifferMsg(Sniffer.context->output);

        sprintf(Sniffer.context->output, "         RFCOMM: Pri %d, Frame Size %d",
                Buffer[2], LEtoHost16(&Buffer[4]));
        SnifferMsg(Sniffer.context->output);

        if ((Buffer[1] & 0xF0) == 0xF0) {
            sprintf(Sniffer.context->output, "         RFCOMM: CFC Request, Initial Credit %d", 
                    Buffer[7] & 0x07);
            SnifferMsg(Sniffer.context->output);
        } else if ((Buffer[1] & 0xF0) == 0xE0) {
            sprintf(Sniffer.context->output, "         RFCOMM: CFC Response, Initial Credit %d", 
                    Buffer[7] & 0x07);
            SnifferMsg(Sniffer.context->output);
        }

        Buffer += BufferLen;
        break;

    case 0xE1:
        sprintf(Sniffer.context->output, "%s, Channel %d", Sniffer.context->output, Buffer[0] >> 3);
        SnifferMsg(Sniffer.context->output);

        sprintf(Sniffer.context->output, "         RFCOMM:");

        if (Buffer[1] & 0x02) {
            sprintf(Sniffer.context->output, "%s FC ", Sniffer.context->output);
        } 
         
        if (Buffer[1] & 0x04) {
            sprintf(Sniffer.context->output, "%s RTC ", Sniffer.context->output);
        }

        if (Buffer[1] & 0x08) {
            sprintf(Sniffer.context->output, "%s RTR ", Sniffer.context->output);
        }

        if (Buffer[1] & 0x40) {
            sprintf(Sniffer.context->output, "%s IC ", Sniffer.context->output);
        }

        if (Buffer[1] & 0x80) {
            sprintf(Sniffer.context->output, "%s DV ", Sniffer.context->output);
        }

        if (BufferLen > 2) {
            sprintf(Sniffer.context->output, "         RFCOMM: Break Length: %d", Buffer[2]);
        }

        SnifferMsg(Sniffer.context->output);
        Buffer += BufferLen;
        break;

    case 0x11:
        sprintf(Sniffer.context->output, "%s, Cmd %s ", Sniffer.context->output, pRFC_MuxCommand(Buffer[0]));
        SnifferMsg(Sniffer.context->output);
        Buffer++;
        break;

    case 0x91:
        sprintf(Sniffer.context->output, "%s, Channel %d", Sniffer.context->output, Buffer[0] >> 3);
        SnifferMsg(Sniffer.context->output);
        Buffer++;
        break;

    case 0x51:
        sprintf(Sniffer.context->output, "%s, Channel %d", Sniffer.context->output, Buffer[0] >> 3);
        SnifferMsg(Sniffer.context->output);

        if (Buffer[1] & 0x01) {

            sprintf(Sniffer.context->output, "         RFCOMM: Error -");

            if (Buffer[1] & 0x02) {
                sprintf(Sniffer.context->output, "%s Overrun ", Sniffer.context->output);
            }
     
            if (Buffer[1] & 0x04) {
                sprintf(Sniffer.context->output, "%s Parity ", Sniffer.context->output);
            }
     
            if (Buffer[1] & 0x08) {
                sprintf(Sniffer.context->output, "%s Framing ", Sniffer.context->output);
            }

            SnifferMsg(Sniffer.context->output);
        }

        Buffer += BufferLen;
        break;

    default:
        SnifferMsg(Sniffer.context->output);
        break;
    }         
    
    return Buffer;
}

/*
 * Set the next RFCOMM parser state
 */
void RFCOMM_NextState(U8 State, U8 StageLen)
{
    Sniffer.context->rfc_state = State;
    Sniffer.context->rfc_stageLen = StageLen;
    Sniffer.context->rfc_stageOffset = 0;
}

/*
 * Parses RFCOMM Packet headers.
 */
static const char *RFCOMM_Parse(const U8 *Buffer, U16 BufferLen)
{
    U16    n;
    U8     i;
    U8    *oldBuffer;       
    U16    oldRfLen;
    char  *cmdRspStr;

    while (BufferLen > 0) {
        /* Stage the necessary amount of data */
        n = min((U16)(Sniffer.context->rfc_stageLen - Sniffer.context->rfc_stageOffset), BufferLen);
        for (i = Sniffer.context->rfc_stageOffset; n > 0; n--, i++) {
            Sniffer.context->rfc_stageBuff[i] = *Buffer;
            Buffer++;
            BufferLen--;
            Sniffer.context->rfc_stageOffset++;
        }

        if (Sniffer.context->rfc_stageOffset == Sniffer.context->rfc_stageLen) {
            /* Data has been staged, run the state machine */

            switch (Sniffer.context->rfc_state) {
            case 0:
                /* Initialization state */
                RFCOMM_NextState(1, 2);
                break;

            case 1:
                /* Get the command and address */
                Sniffer.context->rfc_addr = Sniffer.context->rfc_stageBuff[0];
                Sniffer.context->rfc_cmd = Sniffer.context->rfc_stageBuff[1];
                Sniffer.context->rfc_cmdRsp = 0;
                sprintf(Sniffer.context->output, "         RFCOMM: %s, Addr 0x%02X (Channel %d),", 
                        pRFC_Command(Sniffer.context->rfc_cmd),
                        Sniffer.context->rfc_addr, 
                        Sniffer.context->rfc_addr >> 3);
            
                RFCOMM_NextState(2, 1);
                break;

            case 2:
                /* Get the first byte of the length */
                Sniffer.context->rfc_len = Sniffer.context->rfc_stageBuff[0];
                if (Sniffer.context->rfc_len & 0x01) {
                    /* Single byte length */
                    if (Sniffer.context->rfc_cmdRsp == 0) {
                        /* Length of RFCOMM command */
                        sprintf(Sniffer.context->output, "%s Len %d(+FCS)", 
                                Sniffer.context->output, 
                                Sniffer.context->rfc_len >> 1);
                        SnifferMsg(Sniffer.context->output);
                        RFCOMM_NextState(4, 1);
                    } else {
                        /* Length of Control Channel command */
                        sprintf(Sniffer.context->output, "%s Len %d", 
                                Sniffer.context->output, 
                                Sniffer.context->rfc_len >> 1);
                        RFCOMM_NextState(6, 0);
                    }
                } else {
                    RFCOMM_NextState(3, 1);
                }
                Sniffer.context->rfc_len = Sniffer.context->rfc_len >> 1;
                break;

            case 3:
                /* Double byte length */
                if (Sniffer.context->rfc_cmdRsp == 0) {
                    /* Length of RFCOMM command */
                    Sniffer.context->rfc_len = (Sniffer.context->rfc_len) | (Sniffer.context->rfc_stageBuff[0] << 7);
                    sprintf(Sniffer.context->output, "%s Len %d(+FCS)", Sniffer.context->output, Sniffer.context->rfc_len);
                    SnifferMsg(Sniffer.context->output);
                    RFCOMM_NextState(4, 1);
                } else {
                    /* Length of Control Channel command */
                    Sniffer.context->rfc_len = (Sniffer.context->rfc_len) | (Sniffer.context->rfc_stageBuff[0] << 7);
                    sprintf(Sniffer.context->output, "%s Len %d", Sniffer.context->output, Sniffer.context->rfc_len);
                    RFCOMM_NextState(6, 0);
                }
                break;

            case 4:
                /* Credit */
                if (Sniffer.context->rfc_cmd == 0xFF) {
                    /* Credit byte */
                    sprintf(Sniffer.context->output, "         RFCOMM: Credit %d", Sniffer.context->rfc_stageBuff[0]);
                    SnifferMsg(Sniffer.context->output);
                } else {
                    /* No credit, put the byte back */
                    Buffer--;
                    BufferLen++;
                    Sniffer.context->rfc_stageOffset--;
                }
                RFCOMM_NextState(5, 1);
                break;

            case 5:
                /* Look for a control channel command */
                if (((Sniffer.context->rfc_addr >> 2) == 0) &&
                    ((Sniffer.context->rfc_cmd & 0xEF) == 0xEF)) {
                    Sniffer.context->rfc_cmdRsp = Sniffer.context->rfc_stageBuff[0] & 0xFD;

                    /* Command or response? */
                    if ((U8)(Sniffer.context->rfc_stageBuff[0] & 0x02)) {
                        cmdRspStr = "Cmd";
                    } else {
                        cmdRspStr = "Rsp";
                    }

                    sprintf(Sniffer.context->output, "         RFCOMM: %s %s,", 
                            pRFC_MuxCommand(Sniffer.context->rfc_cmdRsp), 
                            cmdRspStr);

                    RFCOMM_NextState(2, 1);
                } else {
                    /* Not a control channel command.
                     * Done parsing, put the byte back 
                     */
                    Buffer--;
                    BufferLen++;
                    Sniffer.context->rfc_stageOffset--;
                    RFCOMM_NextState(6, 0);
                }
                break;
            case 6:
                /* If the packet contains higher layer data, try to have it parsed. */
                oldRfLen = Sniffer.context->rfc_len;
                oldBuffer = (char *)Buffer;
                if (((Sniffer.context->rfc_addr >> 3) != 0) && ((Sniffer.context->rfc_cmd & 0xEF) == 0xEF) && 
                    Sniffer.context->rfc_len && BufferLen) {

                    ChannelInfo    *ci;
                    SniffEndpoint   se;
                    U16             len;
                     
                    if (BufferLen < Sniffer.context->rfc_len)
                        len = BufferLen;
                    else len = Sniffer.context->rfc_len;

                    /* Query the Endpoint manager to see if any protocols are layered on this channel. */
                    se.proto = SPI_RFCOMM|SPI_LM;
                    se.lmp = Sniffer.context->lm_connHandle;
                    se.rfcomm = (U8)(Sniffer.context->rfc_addr >> 2);

                    if (ci = GetChannelInfo(Sniffer.direction, &se)) {

                        if (ci->endpoint.type == SPI_OBEX) 
                            Buffer = OBEX_Parse(&ci->info.obContext, Buffer, len);
                    }
                    Sniffer.context->rfc_len -= len;

                } else if (((Sniffer.context->rfc_addr >> 3) == 0) && Sniffer.context->rfc_len && BufferLen)  {
                    Buffer = RFCTL_Parse(Sniffer.context->rfc_cmdRsp, Buffer, (U16)min(Sniffer.context->rfc_len, BufferLen));
                    Sniffer.context->rfc_len -= min(Sniffer.context->rfc_len, BufferLen);
                } else {
                    Sniffer.context->rfc_len -= min(Sniffer.context->rfc_len, BufferLen);
                }

                BufferLen -= min(oldRfLen, BufferLen);

                if (Sniffer.context->rfc_len == 0) {
                    /* Done with data payload */
                    if (BufferLen == 0) {
                        RFCOMM_NextState(7, 0);
                    } else {
                        BufferLen = 0;
                        RFCOMM_NextState(1, 2);
                    }
                }
                break;
                 
            case 7:
                /* Only FCS exists, don't consume it. This way it will get
                 * dumped by the Hex dump function.
                 */
                if (BufferLen != 1) {
                    SnifferMsg("Error: FCS byte incorrect length.");
                    SNIFFER_STOP;
                }

                BufferLen = 0;

                RFCOMM_NextState(1, 2);
                break;
            }
        }
    }

    return Buffer;
}

/*
 *---------------------------------------------------------------------------
 *            SDP Parsing Functions
 *---------------------------------------------------------------------------
 */
/* SDP Parsing defines */
#define BEGIN_PACKET        0x00

#define SDP_ERROR_RESPONSE              0x01
#define SDP_SERVICE_SEARCH_REQ          0x02
#define SDP_SERVICE_SEARCH_RSP          0x03
#define SDP_SERVICE_ATTRIB_REQ          0x04
#define SDP_SERVICE_ATTRIB_RSP          0x05
#define SDP_SERVICE_SEARCH_ATTRIB_REQ   0x06
#define SDP_SERVICE_SEARCH_ATTRIB_RSP   0x07
#define SDP_RESERVED_ID                 0xFE

static char *SDP_GetAttribString(const U16 attrib) 
{
    switch (attrib) {
    case 0x0000: return "Service Record Handle";
    case 0x0001: return "Service Class Id List";
    case 0x0002: return "Service Record State";
    case 0x0003: return "Service Id";
    case 0x0004: return "Protocol Descriptor List";
    case 0x0005: return "Browse Group List";
    case 0x0006: return "Language Base Attribute Id List";
    case 0x0007: return "Service Info Time To Live";
    case 0x0008: return "Service Availability";
    case 0x0009: return "Bluetooth Profile Descriptor List";
    case 0x000A: return "Documentation URL";
    case 0x000B: return "Client Executable URL";
    case 0x000C: return "Icon URL";
        /* Assuming Primary Language (0x100) for next three attributes */
    case 0x0100: return "Service Name";
    case 0x0101: return "Service Description";
    case 0x0102: return "Provider Name";
        /* Could be either GroupID or VersionNumberList */
    case 0x0200: return "GroupID/Version Number";
    case 0x0201: return "Service Database State";
    case 0x300:  return "Service Version";
        /* Could be either External Network or Supported Data Stores List */
    case 0x301:  return "External Network/Supported Data Stores";
        /* Could be either Fax Class 1 Support or Remote Audio Volume Control */
    case 0x302:  return "Remote Volume Control/Fax Class 1";
        /* Could be either Supported Formats List or Fax Class 2.0 Support */
    case 0x303:  return "Supported Formats/Fax Class 2";
    case 0x304:  return "Fax Class 2 Support";
    case 0x305:  return "Audio Feedback Support";
    case 0x306:  return "Network Address";
    case 0x307:  return "WAP Gateway";
    case 0x308:  return "Home Page URL";
    case 0x309:  return "WAP Stack Type";
    case 0x310:  return "Supported Capabilities";
    case 0x311:  return "Supported Features";
    }
    return "";
}

static char *SDP_GetUuidString(const U16 uuid) 
{
    switch (uuid) {
    case 0x0001: return "SDP";
    case 0x0002: return "UDP";
    case 0x0003: return "RFCOMM";
    case 0x0004: return "TCP";
    case 0x0005: return "TCS-BIN";
    case 0x0006: return "TCS-AT";
    case 0x0008: return "OBEX";
    case 0x0009: return "IP";
    case 0x000A: return "FTP";
    case 0x000C: return "HTTP";
    case 0x000E: return "WSP";
    case 0x000F: return "BNEP";
    case 0x0010: return "UPnP";
    case 0x0011: return "HIDP";
    case 0x0012: return "HCR Control Channel";
    case 0x0014: return "HCR Data Channel";
    case 0x0100: return "L2CAP";
    case 0x1000: return "SDP Server Service Class Id";
    case 0x1001: return "Browse Group Descriptor Service Class Id";
    case 0x1002: return "Public Browse Group";
    case 0x1101: return "Serial Port";
    case 0x1102: return "LAN Access";
    case 0x1103: return "DUN";
    case 0x1104: return "IrMC Sync";
    case 0x1105: return "Object Push";
    case 0x1106: return "File Transfer";
    case 0x1107: return "IrMC Sync Command";
    case 0x1108: return "Headset";
    case 0x1109: return "Cordless Telephony";
    case 0x1110: return "Intercom";
    case 0x1111: return "Fax";
    case 0x1112: return "Headset Audio Gateway";
    case 0x1113: return "WAP";
    case 0x1114: return "WAP Client";
    case 0x1115: return "PANU";
    case 0x1116: return "NAP";
    case 0x1117: return "GN";
    case 0x1118: return "Direct Printing";
    case 0x1119: return "Reference Printing";
    case 0x111A: return "Imaging";
    case 0x111B: return "Imaging Responder";
    case 0x111C: return "Imaging Automatic Archive";
    case 0x111D: return "Imaging Referencec Objects";
    case 0x111E: return "HandsFree";
    case 0x111F: return "HandsFree Audio Gateway";
    case 0x1120: return "Direct Printing Ref Objects";
    case 0x1121: return "Reflected UI";
    case 0x1122: return "Basic Printing";
    case 0x1123: return "Printing Status";
    case 0x1124: return "Human Interface Device";
    case 0x1125: return "HCR";
    case 0x1126: return "HCR Print";
    case 0x1127: return "HCR Scan";
    case 0x1200: return "PnP Information";
    case 0x1201: return "Generic Networking";
    case 0x1202: return "Generic File Transfer";
    case 0x1203: return "Generic Audio";
    case 0x1204: return "Generic Telephony";
    }
    return "";
}

static void SDP_UpdateSdpState(void)
{
    /* If we have parsed all of the all of the data parameters
     * in this SDP packet, reset the SDP state to begin a new 
     * packet
     */
    if (Sniffer.context->sdp_paramLen == 0) {
        Sniffer.context->sdp_state = BEGIN_PACKET;
    }
}

static void SDP_UpdateBuffer(U16 offset)
{
    /* We have an offset - update the buffer length and buffer offset */
    Sniffer.context->sdp_offset += offset;
    Sniffer.context->sdp_bufferLen -= offset;
}

static void SDP_UpdateParamLen(void)
{
    /* Make sure any params are subtracted from the SDP Parameter Length */
    Sniffer.context->sdp_paramLen -= Sniffer.context->sdp_bufferLen;
}

static char *SDP_GetLength(const U8 *Buffer, U32 *len, U16 *offset)
{
    *len = SdpParseElement(Buffer+Sniffer.context->sdp_offset, offset);
    /* Update the buffer position and the buffer length */
    SDP_UpdateBuffer(*offset);

    switch (*offset) {
    case 2: return "1-byte length";
    case 3: return "2-byte length";
    case 5: return "4-byte length";
    default: return "invalid";      /* Invalid offset */
    }
}

static void SDP_ParseContinuationState(const U8 *Buffer) 
{
    /* CONTINUATION STATE */
    if (Sniffer.context->sdp_bufferLen >= 1) {
        /* If the continuation state is nonzero in request packets,
         * we aren't guaranteed to be able to parse the next response
         * frame. Don't parse anymore until the continuation state ends
         */
        if (Buffer[Sniffer.context->sdp_offset] != 0) {
            Sniffer.context->sdp_contState = TRUE;
        }
        else {
            Sniffer.context->sdp_contState = FALSE;
        }
        sprintf(Sniffer.context->output, "         SDP: Continuation State Len: %i",Buffer[Sniffer.context->sdp_offset]);
        SnifferMsg(Sniffer.context->output);
        /* Update the buffer position and the buffer length */
        SDP_UpdateBuffer(1);
        /* Can't really parse continuation state info - just print it 
         * as Hex 
         */
    }

}
static void SDP_ParseHeader(const U8 *Buffer)
{
    U8     *pduId;
    U16     predictedState;
    U16     transId;

    /* Make sure we actually have room for the PDU ID */
    if (Sniffer.context->sdp_bufferLen >= 1) {
        /* PDU ID */
        switch (Buffer[Sniffer.context->sdp_offset]) {
        case 0x01:
            predictedState = 1;
            pduId = "ErrorResp";
            break;
        case 0x02:
            predictedState = 2;
            pduId = "ServiceSearchReq";
            break;
        case 0x03:
            predictedState = 3;
            pduId = "ServiceSearchRsp";
            break;
        case 0x04:
            predictedState = 4;
            pduId = "ServiceAttribReq";
            break;
        case 0x05:
            predictedState = 5;
            pduId = "ServiceAttribRsp";
            break;
        case 0x06:
            predictedState = 6;
            pduId = "ServiceSearchAttribReq";
            break;
        case 0x07:
            predictedState = 7;
            pduId = "ServiceSearchAttribRsp";
            break;
        default:
            predictedState = SDP_RESERVED_ID;
            pduId = "RESERVED";
            break;
        }
        SDP_UpdateBuffer(1);
        
        /* Now make sure we have room for the Transaction ID */
        if (Sniffer.context->sdp_bufferLen >= 2) {
            /* TRANS ID (Transaction ID) */        
            transId = BEtoHost16(Buffer+1);
            SDP_UpdateBuffer(2);
        }
        else //tallplay add for bug
            transId = 0x00;
        
        /* Now make sure we have room for the Parameter Length */
        if (Sniffer.context->sdp_bufferLen >= 2) {
            /* PARAM LENGTH */
            Sniffer.context->sdp_paramLen = BEtoHost16(Buffer+3);
            /* We have the 5 header bytes - update the buffer offset 
             * and buffer length 
             */
            SDP_UpdateBuffer(2);
            Sniffer.context->sdp_state = predictedState;
        }

        /* Post the packet header to the sniffer window */
        sprintf(Sniffer.context->output, "         SDP: %s TransId: %i Param Len: %i", pduId, transId, Sniffer.context->sdp_paramLen);
        SnifferMsg(Sniffer.context->output);
    }
}

static void SDP_ParseErrorResponse(const U8 *Buffer)
{
    /* Make sure any params are subtracted from the SDP Parameter Length */
    SDP_UpdateParamLen();
    if (Sniffer.context->sdp_bufferLen >= 2){
        /* Post the error code to the sniffer window */
        sprintf(Sniffer.context->output, "         SDP: Error Code: %i",BEtoHost16(Buffer+Sniffer.context->sdp_offset));
        SnifferMsg(Sniffer.context->output);
        /* Update the buffer position and buffer length */
        SDP_UpdateBuffer(2);
        /* Any further data is ErrorInfo and is not currently
         * defined by the existing Error Codes in SDP 
         */
    }
    SDP_UpdateSdpState();
}

static void SDP_ParseServiceSearchRequest(const U8 *Buffer)
{
    char   *str;
    U32     len,len2,uuid;
    U16     offset,offset2;
    BOOL    lastPassed = FALSE;

    /* Make sure any params are subtracted from the SDP Parameter Length */
    SDP_UpdateParamLen();

    if ((Sniffer.context->sdp_bufferLen >= 3) && ((Buffer[Sniffer.context->sdp_offset] & DETD_MASK) == DETD_SEQ)) {
        /* Get the length and string translation of the Data Element Sequence */
        str = SDP_GetLength(Buffer, &len, &offset);

        /* Post Service Search Pattern to the sniffer window */
        sprintf(Sniffer.context->output, "         SDP: ServiceSearchPattern: DETD_SEQ, %s, Len: %li",str,len);
        SnifferMsg(Sniffer.context->output);

        /* If we have all of the ServiceSearchPattern then parse it */
        if (Sniffer.context->sdp_bufferLen >= len) {
        NextUuid:
            if ((Buffer[Sniffer.context->sdp_offset] & DETD_MASK) == DETD_UUID) {
                /* We have a new UUID - reset the value of the last pass */
                lastPassed = FALSE;
                len2 = SdpParseElement(Buffer+Sniffer.context->sdp_offset, &offset2);
                /* Offset should always be 1 for UUID's */
                if (offset2 == 1) {
                    SDP_UpdateBuffer(offset2);
                    /* Try to parse the UUID's that I know about */
                    if (len2 == 2) { 
                        /* Get a 16 bit UUID */
                        str = SDP_GetUuidString(BEtoHost16(Buffer+Sniffer.context->sdp_offset));
                    }
                    else if (len2 == 4) {
                        /* Get a 32 bit UUID by using only the 3rd and 4th bytes */
                        str = SDP_GetUuidString(BEtoHost16(Buffer+Sniffer.context->sdp_offset+2));
                    }
                    else {
                        /* Bad Length */
                        SDP_UpdateSdpState();
                        return;
                    }

                    if (strcmp(str,"") == 0) {
                        /* Unknown UUID - print as Hex */
                        if (len2 == 2) {
                            uuid = BEtoHost16(Buffer+Sniffer.context->sdp_offset);
                        }
                        else if (len2 == 4)    {
                            uuid = BEtoHost32(Buffer+Sniffer.context->sdp_offset);
                        }
                        sprintf(Sniffer.context->output, "         SDP: UUID: 0x%lx",uuid);
                    }
                    else {
                        /* Found a UUID - print as a String */
                        sprintf(Sniffer.context->output, "         SDP: UUID: %s",str);
                    }
                    /* Post the UUID to the sniffer window */
                    SnifferMsg(Sniffer.context->output);
                    
                    /* Update the buffer position and the buffer length */
                    SDP_UpdateBuffer((U16)len2);

                    /* Update the ServiceSearchPattern Length */
                    len -= len2;

                    /* Last UUID was correctly formed */
                    lastPassed = TRUE;

                    /* Another UUID exists in the ServiceSearchPattern - reparse */
                    if (len > 0) goto NextUuid;
                }
            }

            /* ONLY CONTINUE PARSING IF THE SERVICESEARCHPATTERN WAS PROPERLY FORMED */
            if (lastPassed) {
                /* Reset the last pass variable */
                lastPassed = FALSE;

                /* MAXIMUM SERVICE RECORD COUNT */
                if (Sniffer.context->sdp_bufferLen >= 2) {
                    sprintf(Sniffer.context->output, "         SDP: MaxServiceRecordCount: %i",BEtoHost16(Buffer+Sniffer.context->sdp_offset));
                    SnifferMsg(Sniffer.context->output);
                    /* Update the buffer position and the buffer length */
                    SDP_UpdateBuffer(2);

                    SDP_ParseContinuationState(Buffer);
                }
            }
        }
    }
    SDP_UpdateSdpState();
}

static void SDP_ParseServiceSearchResponse(const U8 *Buffer)
{   
    U16 i, currentRecCount;

    /* Make sure any params are subtracted from the SDP Parameter Length */
    SDP_UpdateParamLen();
                
    /* TOTAL SERVICE RECORD COUNT */
    if (Sniffer.context->sdp_bufferLen >= 2) {
        sprintf(Sniffer.context->output, "         SDP: TotalServiceRecordCount: %i",BEtoHost16(Buffer+Sniffer.context->sdp_offset));
        SnifferMsg(Sniffer.context->output);
        /* Update the buffer position and the buffer length */
        SDP_UpdateBuffer(2);

        /* CURRENT SERVICE RECORD COUNT */
        if (Sniffer.context->sdp_bufferLen >= 2) {
            currentRecCount = BEtoHost16(Buffer+Sniffer.context->sdp_offset);
            sprintf(Sniffer.context->output, "         SDP: CurrentServiceRecordCount: %i", currentRecCount);
            SnifferMsg(Sniffer.context->output);
            /* Update the buffer position and the buffer length */
            SDP_UpdateBuffer(2);

            /* Only try to parse this data if we this isn't a continuation 
             * state packet. Otherwise, our parsing engine might get confused.
             */
            if (!Sniffer.context->sdp_contState) {
                /* SERVICE RECORD HANDLE LIST */
                if (Sniffer.context->sdp_bufferLen >= (currentRecCount*4)) {
                    for (i=1; i<=currentRecCount; i++) {
                        sprintf(Sniffer.context->output,"         SDP: ServiceRecord #%i: 0x%lx", i,BEtoHost32(Buffer+Sniffer.context->sdp_offset));
                        SnifferMsg(Sniffer.context->output);
                        /* Update the buffer position and the buffer length */
                        SDP_UpdateBuffer(4);
                    }
                    SDP_ParseContinuationState(Buffer);
                }
            }
        }
    }
    SDP_UpdateSdpState();
}

static void SDP_ParseServiceAttributeRequest(const U8 *Buffer)
{
    char   *str;
    U32     len,len2;
    U16     offset,offset2,begRange,endRange;
    BOOL    lastPassed = FALSE;
    
    /* Make sure any params are subtracted from the SDP Parameter Length */
    SDP_UpdateParamLen();
                
    /* SERVICE RECORD HANDLE */
    if (Sniffer.context->sdp_bufferLen >= 4) {
        sprintf(Sniffer.context->output,"         SDP: ServiceRecord: 0x%lx", BEtoHost32(Buffer+Sniffer.context->sdp_offset));
        SnifferMsg(Sniffer.context->output);
        /* Update the buffer position and the buffer length */
        SDP_UpdateBuffer(4);

        /* MAXIMUM ATTRIBUTE BYTE COUNT */
        if (Sniffer.context->sdp_bufferLen >= 2) {
            sprintf(Sniffer.context->output, "         SDP: MaxAttributeByteCount: %i",BEtoHost16(Buffer+Sniffer.context->sdp_offset));
            SnifferMsg(Sniffer.context->output);
            /* Update the buffer position and the buffer length */
            SDP_UpdateBuffer(2);

            /* ATTRIBUTE ID LIST - make sure it is a Data Element Sequence and that
             *                     it actually has some data in it
             */
            if ((Sniffer.context->sdp_bufferLen >= 3) && ((Buffer[Sniffer.context->sdp_offset] & DETD_MASK) == DETD_SEQ)) {

                /* Get the length and string translation of the Data Element Sequence */
                str = SDP_GetLength(Buffer, &len, &offset);

                /* Post Attribute List ID to the sniffer window */
                sprintf(Sniffer.context->output, "         SDP: AttributeListID: DETD_SEQ, %s, Len: %li",str,len);
                SnifferMsg(Sniffer.context->output);

                /* Attribute ID List is empty - skip to Cont State */
                if (len == 0) {
                    SDP_ParseContinuationState(Buffer);
                    SDP_UpdateSdpState();
                    return;
                }

                /* We have all of the AttributeID List in this data fragment */
                if ((Sniffer.context->sdp_bufferLen >= len) && (len != 0)) {
                NextAttribId:
                    /* Attribute ID should be an Unsigned Integer */
                    if ((Buffer[Sniffer.context->sdp_offset] & DETD_MASK) == DETD_UINT) {
                        /* We have a new AttributeId/AttributeValue pair - reset the
                         * value of the last pass
                         */
                        lastPassed = FALSE;
                        len2 = SdpParseElement(Buffer+Sniffer.context->sdp_offset, &offset2);
                        
                        /* Offset should always be 1 for UINT's */
                        if (offset2 == 1) {
                            SDP_UpdateBuffer(offset2);
                            /* Try to parse the ATTRIBUTE ID's that I know about */
                            if (len2 == 2) {    
                                /* Get a 16 bit ATTRIBUTE ID */
                                str = SDP_GetAttribString(BEtoHost16(Buffer+Sniffer.context->sdp_offset));
                                if (strcmp(str,"") == 0) {
                                    /* Unknown Attribute - print as Hex */
                                    sprintf(Sniffer.context->output, "         SDP:   AttributeID (UINT): 0x%x",
                                            BEtoHost16(Buffer+Sniffer.context->sdp_offset));
                                }
                                else {
                                    /* Found an Attribute - print as a String */
                                    sprintf(Sniffer.context->output, "         SDP:   AttributeID (UINT): %s",
                                            str);
                                }
                            }
                            else { 
                                /* Get a range of ATTRIBUTE ID's */
                                begRange = BEtoHost16(Buffer+Sniffer.context->sdp_offset);
                                endRange = BEtoHost16(Buffer+Sniffer.context->sdp_offset+2);
                                /* Just print the range of attributes as Hex */
                                sprintf(Sniffer.context->output, "         SDP:   AttributeID (UINT): Beg. Range: 0x%x, End Range: 0x%x",
                                        begRange, endRange);
                            }
                            /* Post the Attribute ID to the sniffer window */
                            SnifferMsg(Sniffer.context->output);
                            
                            /* Update the buffer position and the buffer length */
                            SDP_UpdateBuffer((U16)len2);
                            /* Update the AttributeId  Length */
                            len -= len2;

                            /* Successful pass through the AttributeList */
                            lastPassed = TRUE;
                            /* Another AttributeID/AttributeValue pair exists in the AttributeList */
                            if (len > 0) goto NextAttribId;
                        }
                    }
                    /* ONLY PARSE CONT STATE IF ATTRIBUTE LIST PARSED PROPERLY */
                    if (lastPassed) {
                        /* Reset the last pass variable */
                        lastPassed = FALSE;
                        SDP_ParseContinuationState(Buffer);
                    }
                }
            }
        }
    }
    SDP_UpdateSdpState();
}

static void SDP_ParseServiceAttributeResponse(const U8 *Buffer)
{
    char   *str, *attrStr;
    U32     len,len2;
    U16     offset,offset2;
    BOOL    lastPassed = FALSE;

    /* Make sure any params are subtracted from the SDP Parameter Length */
    SDP_UpdateParamLen();
                
    /* ATTRIBUTE LIST BYTE COUNT */
    if (Sniffer.context->sdp_bufferLen >= 2) {
        sprintf(Sniffer.context->output, "         SDP: AttributeListByteCount: %i",BEtoHost16(Buffer+Sniffer.context->sdp_offset));
        SnifferMsg(Sniffer.context->output);
        /* Update the buffer position and the buffer length */
        SDP_UpdateBuffer(2);
          
        /* Only try to parse this data if we this isn't a continuation 
         * state packet. Otherwise our parsing engine might get confused
         */
        if (!Sniffer.context->sdp_contState) {
            /* ATTRIBUTE LIST - make sure it is a Data Element Sequence and that
             *                  it actually has some data in it
             */
            if ((Sniffer.context->sdp_bufferLen >= 3) && ((Buffer[Sniffer.context->sdp_offset] & DETD_MASK) == DETD_SEQ)) {
                /* Get the length and string translation of the Data Element Sequence */
                str = SDP_GetLength(Buffer, &len, &offset);

                /* Post Attribute List to the sniffer window */
                sprintf(Sniffer.context->output, "         SDP: AttributeList: DETD_SEQ, %s, Len: %li",str,len);
                SnifferMsg(Sniffer.context->output);

                /* Attribute ID List is empty - skip to Cont State */
                if (len == 0) {
                    SDP_ParseContinuationState(Buffer);
                    SDP_UpdateSdpState();
                    return;
                }

                /* We have all of the ATTRIBUTE ID LIST in this data fragment */
                if ((Sniffer.context->sdp_bufferLen >= len) && (len != 0)) {
                NextAttrib:
                    /* All Attribute ID's must be Unsigned Integers */
                    if ((Buffer[Sniffer.context->sdp_offset] & DETD_MASK) == DETD_UINT) {
                        /* We have a new AttributeId/AttributeValue pair - reset the
                         * value of the last pass
                         */
                        lastPassed = FALSE;
                        len2 = SdpParseElement(Buffer+Sniffer.context->sdp_offset, &offset2);
                        /* Offset should always be 1 for UINT's; We will only
                         * parse 2 byte lengths 
                         */
                        if ((offset2 == 1) && (len2 == 2)) {
                            /* Update the buffer position and the buffer length */
                            SDP_UpdateBuffer(offset2);
                            /* Update the Attribute Id List length */
                            len -= offset2;
                            /* Try to parse the Attribute */
                            str = SDP_GetAttribString(BEtoHost16(Buffer+Sniffer.context->sdp_offset));
                            if (strcmp(str,"") == 0) {
                                /* Unknown Attribute - print as Hex */
                                sprintf(Sniffer.context->output,"         SDP:   AttributeID (UINT): 0x%x",
                                        BEtoHost16(Buffer+Sniffer.context->sdp_offset));
                            }
                            else {
                                /* Found an Attribute - print as a String */
                                sprintf(Sniffer.context->output,"         SDP:   AttributeID (UINT): %s",str);
                            }
                            /* Post the Attribute ID to the sniffer window */
                            SnifferMsg(Sniffer.context->output);

                            /* Update the buffer position and the buffer length */
                            SDP_UpdateBuffer((U16)len2);
                            /* Update the Attribute Id List length */
                            len -= len2;

                            /* ONLY PARSE ATTRIBUTE VALUES IF ATTRIBUTE ID WAS VALID! */
                            /* ATTRIBUTE VALUE - only parse them if they are Data
                             *                   Element Sequences, UUIDs, Unsigned
                             *                   Integers, Bools, or Strings, otherwise
                             *                   leave them as Hex data.
                             */
                            if (Sniffer.context->sdp_bufferLen >= 1) {
                                switch (Buffer[Sniffer.context->sdp_offset] & DETD_MASK) {
                                case DETD_SEQ:
                                    attrStr = "SEQUENCE";
                                case DETD_TEXT:
                                    if ((Buffer[Sniffer.context->sdp_offset] & DETD_MASK) == DETD_TEXT) {
                                        attrStr = "STRING";
                                    }
                                            
                                    /* Get the length and string translation of the Data Element Sequence */
                                    str = SDP_GetLength(Buffer, &len2, &offset2);
        
                                    /* Post Attribute Value to the sniffer window */
                                    sprintf(Sniffer.context->output, "         SDP: AttributeValue: %s, %s, Len: %li",str,attrStr,len);
                                    SnifferMsg(Sniffer.context->output);
                                        
                                    /* Update the Attribute Id List length */
                                    len -= (offset2 + len2);

                                    /* Now print out the remaining portion of data */
                                    DisplayHex(Buffer+Sniffer.context->sdp_offset, (U16)len2);
    
                                    /* Update the buffer position and the buffer length */
                                    SDP_UpdateBuffer((U16)len2);

                                    /* Successful pass through Attribute Id/Attribute Value Pair */
                                    lastPassed = TRUE;

                                    /* ONLY PARSE NEXT ATTRIBUTE IF THE FIRST ONE PASSED */
                                    /* Another AttributeID and AttributeValue exist
                                     * in the AttributeList 
                                     */
                                    if (len > 0) goto NextAttrib;
                                    break;
                                    
                                case DETD_UINT:
                                    sprintf(Sniffer.context->output, "         SDP:   AttributeValue: UNSIGN INT, ");
                                    /* Find the length of the Data Element Sequence or Text segment,
                                     * and the offset 
                                     */
                                    len2= SdpParseElement(Buffer+Sniffer.context->sdp_offset, &offset2);
                                    /* UINT's must have a 1-byte offset */
                                    if (offset2 == 1) {
                                        sprintf(Sniffer.context->output+39, "1-byte length, ");

                                        /* Update the buffer position and the buffer length */
                                        SDP_UpdateBuffer(offset2);
                                        /* Update the Attribute Id List length */
                                        len -= offset2;
                                        sprintf(Sniffer.context->output+54, "Len: %li",len2);
                                        SnifferMsg(Sniffer.context->output);

                                        /* Now print out the remaining portion of data */
                                        DisplayHex(Buffer+Sniffer.context->sdp_offset, (U16)len2);
    
                                        /* Update the buffer position and the buffer length */
                                        SDP_UpdateBuffer((U16)len2);
                                        /* Update the Attribute Id List length */
                                        len -= len2;

                                        /* Successful pass through Attribute Id/Attribute Value Pair */
                                        lastPassed = TRUE;

                                        /* ONLY PARSE NEXT ATTRIBUTE IF THE FIRST ONE PASSED */
                                        /* Another AttributeID and AttributeValue exist
                                         * in the AttributeList 
                                         */
                                        if (len > 0) goto NextAttrib;
                                    }
                                    break;
                                    
                                case DETD_UUID:
                                    sprintf(Sniffer.context->output, "         SDP:   AttributeValue: UUID, ");
                                    /* Find the length of the Data Element Sequence or Text segment,
                                     * and the offset 
                                     */
                                    len2 = SdpParseElement(Buffer+Sniffer.context->sdp_offset, &offset2);
                                    /* UUID's must have a 1-byte offset */
                                    if (offset2 == 1) {
                                        sprintf(Sniffer.context->output+33, "1-byte length, ");
    
                                        /* Update the buffer position and the buffer length */
                                        SDP_UpdateBuffer(offset2);
                                        /* Update the Attribute Id List length */
                                        len -= offset2;
                                        sprintf(Sniffer.context->output+48, "Len: %li",len2);
                                        SnifferMsg(Sniffer.context->output);

                                        /* Now print out the remaining portion of data */
                                        DisplayHex(Buffer+Sniffer.context->sdp_offset, (U16)len2);
   
                                        /* Update the buffer position and the buffer length */
                                        SDP_UpdateBuffer((U16)len2);
                                        /* Update the Attribute Id List length */
                                        len -= len2;

                                        /* Successful pass through Attribute Id/Attribute Value Pair */
                                        lastPassed = TRUE;

                                        /* ONLY PARSE NEXT ATTRIBUTE IF THE FIRST ONE PASSED */
                                        /* Another AttributeID and AttributeValue exist
                                         * in the AttributeList 
                                         */
                                        if (len > 0) goto NextAttrib;
                                    }
                                    break;
                                  
                                case DETD_BOOL:
                                    sprintf(Sniffer.context->output, "         SDP:   AttributeValue: BOOL, ");
                                    /* Find the length of the Data Element Sequence or Text segment,
                                     * and the offset 
                                     */
                                    len2 = SdpParseElement(Buffer+Sniffer.context->sdp_offset, &offset2);
                                    /* BOOL's must have a 1-byte offset and a length of 1 */
                                    if ((offset2 == 1) && (len2 == 1)) {
                                        sprintf(Sniffer.context->output+33, "1-byte length, ");
    
                                        /* Update the buffer position and the buffer length */
                                        SDP_UpdateBuffer(offset2);
                                        /* Update the Attribute Id List length */
                                        len -= offset2;
                                        sprintf(Sniffer.context->output+48, "Len: %li ", len2);

                                        if (Buffer[Sniffer.context->sdp_offset] == FALSE) {
                                            sprintf(Sniffer.context->output+55, "Value: FALSE");
                                        }
                                        else {
                                            sprintf(Sniffer.context->output+55, "Value: TRUE");
                                        }
                                        SnifferMsg(Sniffer.context->output);

                                        /* Update the buffer position and the buffer length */
                                        SDP_UpdateBuffer((U16)len2);
                                        /* Update the Attribute Id List length */
                                        len -= len2;

                                        /* Successful pass through Attribute Id/Attribute Value Pair */
                                        lastPassed = TRUE;

                                        /* ONLY PARSE NEXT ATTRIBUTE IF THE FIRST ONE PASSED */
                                        /* Another AttributeID and AttributeValue exist
                                         * in the AttributeList 
                                         */
                                        if (len > 0) goto NextAttrib;
                                    }
                                    break;
                                }
                            }
                        }
                    }
                    /* ONLY PARSE CONT STATE IF ATTRIBUTE LIST PARSED PROPERLY */
                    if (lastPassed) {
                        /* Reset the last pass variable */
                        lastPassed = FALSE;                            
                        SDP_ParseContinuationState(Buffer);
                    }
                }
            }
        }
    }
    SDP_UpdateSdpState();
}

#if BT_STACK == XA_DISABLED
U32 SdpParseElement(const U8* p, U16* offset)
{
    U32 len = 0;

    if ((p[0] & DETD_MASK) == DETD_NIL) {
        *offset = 0;
        return 1;
    }

    switch (p[0] & DESD_MASK) {
    case DESD_1BYTE:
        *offset = 1;
        len = 1;
        break;

    case DESD_2BYTES:
        *offset = 1;
        len = 2;
        break;

    case DESD_4BYTES:
        *offset = 1;
        len = 4;
        break;

    case DESD_8BYTES:
        *offset = 1;
        len = 8;
        break;

    case DESD_16BYTES:
        *offset = 1;
        len = 16;
        break;

    case DESD_ADD_8BITS:
        *offset = 2;
        len = (U32) p[1];
        break;

    case DESD_ADD_16BITS:
        *offset = 3;
        len = (U32) BEtoHost16((const U8*)(p+1));
        break;

    case DESD_ADD_32BITS:
        *offset = 5;
        len = BEtoHost32((const U8*)(p+1));
        break;

    default:
        /* This case shouldn't be possible.  Complain if it somehow occurs */
        Assert(0);
        break;
    }
    return len;
}
#endif
static void SDP_ParseServiceSearchAttributeRequest(const U8 *Buffer)
{
    char   *str;
    U32     len,len2,uuid;
    U16     offset,offset2,begRange,endRange;
    BOOL    lastPassed = FALSE;

    /* Make sure any params are subtracted from the SDP Parameter Length */
    SDP_UpdateParamLen();
                
    /* SERVICE SEARCH PATTERN */
    if ((Sniffer.context->sdp_bufferLen >= 3) && ((Buffer[Sniffer.context->sdp_offset] & DETD_MASK) == DETD_SEQ)) {
        /* Get the length and string translation of the Data Element Sequence */
        str = SDP_GetLength(Buffer, &len, &offset);
        
        /* Post Service Search Pattern to the sniffer window */
        sprintf(Sniffer.context->output, "         SDP: ServiceSearchPattern: DETD_SEQ, %s, Len: %li",str,len);
        SnifferMsg(Sniffer.context->output);
        
        /* We have all of the ServiceSearchPattern - parse it */
        if (Sniffer.context->sdp_bufferLen >= len) {
        NextUuidSSA:
            if ((Buffer[Sniffer.context->sdp_offset] & DETD_MASK) == DETD_UUID) {
                /* We have a new UUID - reset the value of the last pass */
                lastPassed = FALSE;
                len2 = SdpParseElement(Buffer+Sniffer.context->sdp_offset, &offset2);

                /* Offset should always be 1 for UUID's */
                if (offset2 == 1) {
                    SDP_UpdateBuffer(offset2);
                    /* Try to parse the UUID's that I know about */
                    if (len2 == 2) { 
                        /* Get a 16 bit UUID */
                        str = SDP_GetUuidString(BEtoHost16(Buffer+Sniffer.context->sdp_offset));
                    }
                    else if (len2 == 4) {
                        /* Get a 32 bit UUID by using only the 3rd and 4th bytes */
                        str = SDP_GetUuidString(BEtoHost16(Buffer+Sniffer.context->sdp_offset+2));
                    }
                    else {
                        /* Bad Length */
                        SDP_UpdateSdpState();
                        return;
                    }

                    if (strcmp(str,"") == 0) {
                        /* Unknown UUID - print as Hex */
                        if (len2 == 2) {
                            uuid = BEtoHost16(Buffer+Sniffer.context->sdp_offset);
                        }
                        else if (len2 == 4)    {
                            uuid = BEtoHost32(Buffer+Sniffer.context->sdp_offset);
                        }
                        sprintf(Sniffer.context->output, "         SDP: UUID: 0x%lx",uuid);
                    }
                    else {
                        /* Found a UUID - print as a String */
                        sprintf(Sniffer.context->output, "         SDP: UUID: %s",str);
                    }
                    /* Post the UUID to the sniffer window */
                    SnifferMsg(Sniffer.context->output);
                
                    /* Update the buffer position and the buffer length */
                    SDP_UpdateBuffer((U16)len2);
                    /* Update the ServiceSearchPattern length */
                    len -= len2;

                    /* Successful pass through ServiceSearchPattern UUID */
                    lastPassed = TRUE;

                    /* Another UUID exists in the ServiceSearchPattern */
                    if (len > 0) goto NextUuidSSA;

                }
            }
            /* ONLY PARSE MAXIMUM ATTRIBUTE BYTE COUNT IF SERVICE SEARCH PATTERN PARSED PROPERLY */
            if (lastPassed) {
                /* Reset the last pass variable */
                lastPassed = FALSE;
 
                /* Now move on to the MAXIMUM ATTRIBUTE BYTE COUNT */
                if (Sniffer.context->sdp_bufferLen >= 2) {
                    sprintf(Sniffer.context->output, "         SDP: MaxAttributeByteCount: %i",BEtoHost16(Buffer+Sniffer.context->sdp_offset));
                    SnifferMsg(Sniffer.context->output);
                    /* Update the buffer position and the buffer length */
                    SDP_UpdateBuffer(2);
        
                    /* ATTRIBUTE ID LIST - make sure it is a Data Element Sequence and that
                     *                     it actually has some data in it
                     */
                    if ((Sniffer.context->sdp_bufferLen >= 3) && (Buffer[Sniffer.context->sdp_offset] & DETD_SEQ)) {
                        /* Get the length and string translation of the Data Element Sequence */
                        str = SDP_GetLength(Buffer, &len, &offset);
        
                        /* Post Attribute List ID to the sniffer window */
                        sprintf(Sniffer.context->output, "         SDP: AttributeListID: DETD_SEQ, %s, Len: %li",str,len);
                        SnifferMsg(Sniffer.context->output);
                        
                        /* Attribute ID List is empty - skip to Cont. State */
                        if (len == 0) {
                            SDP_ParseContinuationState(Buffer);
                            SDP_UpdateSdpState();
                            return;
                        }
                        
                        /* We have all of the AttributeID List in this data fragment - parse it */
                        if ((Sniffer.context->sdp_bufferLen >= len) && (len != 0)) {
                        NextAttribIdSSA:
                            /* All Attribute ID's must be Unsigned Integers */
                            if (Buffer[Sniffer.context->sdp_offset] & DETD_UINT) {
                                /* We have a new AttributeId/AttributeValue pair - reset the
                                 * value of the last pass
                                 */
                                lastPassed = FALSE;
                                len2 = SdpParseElement(Buffer+Sniffer.context->sdp_offset, &offset2);

                                /* Offset should always be 1 for UINT's */
                                if (offset2 == 1) {
                                    SDP_UpdateBuffer(offset2);
                                    /* Try to parse the ATTRIBUTE ID's that I know about */
                                    if (len2 == 2) {    
                                        /* Get a 16 bit ATTRIBUTE ID */
                                        str = SDP_GetAttribString(BEtoHost16(Buffer+Sniffer.context->sdp_offset));
                                        if (strcmp(str,"") == 0) {
                                            /* Unknown Attribute - print as Hex */
                                            sprintf(Sniffer.context->output, "         SDP:   AttributeID (UINT): 0x%x",
                                                    BEtoHost16(Buffer+Sniffer.context->sdp_offset));
                                        }
                                        else {
                                            /* Found an Attribute - print as a String */
                                            sprintf(Sniffer.context->output, "         SDP:   AttributeID (UINT): %s",
                                                    str);
                                        }
                                    }
                                    else { 
                                        /* Get a range of ATTRIBUTE ID's */
                                        begRange = BEtoHost16(Buffer+Sniffer.context->sdp_offset);
                                        endRange = BEtoHost16(Buffer+Sniffer.context->sdp_offset+2);
                                        /* Just print the range of attributes as Hex */
                                        sprintf(Sniffer.context->output, "         SDP:   AttributeID (UINT): Beg. Range: 0x%x, End Range: 0x%x",
                                                begRange, endRange);
                                    }
                                    /* Post the Attribute ID to the sniffer window */
                                    SnifferMsg(Sniffer.context->output);
                                
                                    /* Update the buffer position and the buffer length */
                                    SDP_UpdateBuffer((U16)len2);
                                    /* Update the Attribute Id List length */
                                    len -= len2;
                                        
                                    /* Successful pass through Attribute Id List */
                                    lastPassed = TRUE;

                                    /* ONLY PARSE THE NEXT ATTRIBUTE ID IF THE FIRST PASSED */
                                    /* Another Attribute ID exists in the AttributeList */
                                    if (len > 0) goto NextAttribIdSSA;
                                }
                            }
                            /* ONLY PARSE CONT STATE IF ATTRIBUTE LIST PARSED PROPERLY */
                            if (lastPassed) {
                                /* Reset the last pass variable */
                                lastPassed = FALSE;
                                SDP_ParseContinuationState(Buffer);
                            }
                        }
                    }
                }
            }
        }
    }
    SDP_UpdateSdpState();
}

static void SDP_ParseServiceSearchAttributeResponse(const U8 *Buffer)
{
    char   *str, *attrStr;
    U32     len,len2,len3;
    U16     offset,offset2,offset3;
    BOOL    lastPassed = FALSE;

    /* Make sure any params are subtracted from the SDP Parameter Length */
    SDP_UpdateParamLen();

    /* ATTRIBUTE LIST BYTE COUNT */
    if (Sniffer.context->sdp_bufferLen >= 2) {
        sprintf(Sniffer.context->output, "         SDP: AttributeListByteCount: %i",BEtoHost16(Buffer+Sniffer.context->sdp_offset));
        SnifferMsg(Sniffer.context->output);
        /* Update the buffer position and the buffer length */
        SDP_UpdateBuffer(2);
                
        /* Only try to parse this data if we this isn't a continuation 
         * state packet. Otherwise our parsing engine might get confused
         */
        if (!Sniffer.context->sdp_contState)
        {
            /* ATTRIBUTE LISTS - make sure it is a Data Element Sequence and that
             *                   it actually has some data in it
             */
            if ((Sniffer.context->sdp_bufferLen >= 3) && ((Buffer[Sniffer.context->sdp_offset] & DETD_MASK) == DETD_SEQ)) {
                /* Get the length and string translation of the Data Element Sequence */
                str = SDP_GetLength(Buffer, &len, &offset);
        
                /* Post Attribute Lists to the sniffer window */
                sprintf(Sniffer.context->output, "         SDP: AttributeLists: DETD_SEQ, %s, Len: %li",str,len);
                SnifferMsg(Sniffer.context->output);

                /* Attribute Lists is empty - skip to Cont. State */
                if (len == 0) {
                    SDP_ParseContinuationState(Buffer);
                    SDP_UpdateSdpState();
                    return;
                }
                    
                /* We have all of the AttributeLists in this data fragment - parse them */
                if ((Sniffer.context->sdp_bufferLen >= len) && (len != 0)) {
                NextAttributeList:
                    /* ATTRIBUTE ID LIST - make sure it is a Data Element Sequence and that
                     *                     it actually has some data in it
                     */
                    if ((Sniffer.context->sdp_bufferLen >= 3) && ((Buffer[Sniffer.context->sdp_offset] & DETD_MASK) == DETD_SEQ)) {
                        /* We have a new Attribute ID List - reset the value of the last pass */
                        lastPassed = FALSE;

                        /* Get the length and string translation of the Data Element Sequence */
                        str = SDP_GetLength(Buffer, &len2, &offset2);
        
                        /* Post Attribute List to the sniffer window */
                        sprintf(Sniffer.context->output, "         SDP: AttributeList: DETD_SEQ, %s, Len: %li",str,len2);
                        SnifferMsg(Sniffer.context->output);

                        /* Update the Attribute Lists length */
                        len -= (offset2 + len2);

                        /* Attribute ID List and AttributeLists is empty - skip to Cont. State */
                        if ((len2 == 0) && (len == 0)) {
                            SDP_ParseContinuationState(Buffer);
                            SDP_UpdateSdpState();
                            return;
                        }
                
                        /* We have all of the AttributeList in this data fragment - parse it */
                        if ((Sniffer.context->sdp_bufferLen >= len2) && (len2 != 0)) {
                        NextAttribSSA:
                            /* All Attribute ID's must be Unsigned Integers */
                            if ((Buffer[Sniffer.context->sdp_offset] & DETD_MASK) == DETD_UINT) {
                                /* We have a new Attribute ID/Attribute Value pair -
                                 * reset the value of the last pass 
                                 */
                                lastPassed = FALSE;
                                len3 = SdpParseElement(Buffer+Sniffer.context->sdp_offset, &offset3);

                                /* Offset should always be 1 for UINT's */
                                if (offset3 == 1) {
                                    SDP_UpdateBuffer(offset3);
                                    /* Try to parse the ATTRIBUTE ID's that I know about */
                                    if (len3 == 2) {    
                                        /* Update the Attribute Id List length */
                                        len2 -= offset3;

                                        /* Get a 16 bit ATTRIBUTE ID */
                                        str = SDP_GetAttribString(BEtoHost16(Buffer+Sniffer.context->sdp_offset));
                                        if (strcmp(str,"") == 0) {
                                            /* Unknown Attribute - print as Hex */
                                            sprintf(Sniffer.context->output, "         SDP:   AttributeID (UINT): 0x%x",
                                                    BEtoHost16(Buffer+Sniffer.context->sdp_offset));
                                        }
                                        else {
                                            /* Found an Attribute - print as a String */
                                            sprintf(Sniffer.context->output, "         SDP:   AttributeID (UINT): %s",
                                                    str);
                                        }
                                        /* Post the Attribute ID to the sniffer window */
                                        SnifferMsg(Sniffer.context->output);
                                
                                        /* Update the buffer position and the buffer length */
                                        SDP_UpdateBuffer((U16)len3);
                                        /* Update the Attribute Id List length */
                                        len2 -= len3;

                                        /* ONLY PARSE ATTRIBUTE VALUES IF ATTRIBUTE ID WAS VALID! */
                                        /* ATTRIBUTE VALUE - only parse them if they are Data
                                         *                   Element Sequences, UUIDs, Unsigned
                                         *                   Integers, Bools, or Strings, otherwise
                                         *                   leave them as Hex data.
                                         */
                                        if (Sniffer.context->sdp_bufferLen >= 1) {
                                            switch (Buffer[Sniffer.context->sdp_offset] & DETD_MASK) {
                                            case DETD_SEQ:
                                                attrStr = "SEQUENCE";
                                            case DETD_TEXT:
                                                if ((Buffer[Sniffer.context->sdp_offset] & DETD_MASK) == DETD_TEXT) {
                                                    attrStr = "STRING";
                                                }

                                                /* Get the length and string translation of the Data Element Sequence */
                                                str = SDP_GetLength(Buffer, &len3, &offset3);
        
                                                /* Post Attribute Value to the sniffer window */
                                                sprintf(Sniffer.context->output, "         SDP: AttributeValue: %s, %s, Len: %li",str,attrStr,len3);
                                                SnifferMsg(Sniffer.context->output);

                                                /* Update the Attribute Id List length */
                                                len2 -= (offset3 + len3);

                                                /* Now print out the remaining portion of data */
                                                DisplayHex(Buffer+Sniffer.context->sdp_offset, (U16)len3);
    
                                                /* Update the buffer position and the buffer length */
                                                SDP_UpdateBuffer((U16)len3);

                                                /* Successful pass through Attribute Id/Attribute Value Pair */
                                                lastPassed = TRUE;

                                                /* ONLY PARSE NEXT ATTRIBUTE IF THE FIRST ONE PASSED */
                                                /* Another AttributeID and AttributeValue exist
                                                 * in the AttributeList 
                                                 */
                                                if (len2 > 0) goto NextAttribSSA;
    
                                                /* ONLY PARSE THE NEXT ATTRIBUTE ID LIST IF THE FIRST ONE PASSED */
                                                /* Another AttributeList exists in the AttributeLists param */ 
                                                if (len > 0) goto NextAttributeList;
                                                break;
                                            case DETD_UINT:
                                                sprintf(Sniffer.context->output, "         SDP:   AttributeValue: UNSIGN INT, ");
                                                /* Find the length of the Data Element Sequence or Text segment,
                                                 * and the offset 
                                                 */
                                                len3 = SdpParseElement(Buffer+Sniffer.context->sdp_offset, &offset3);
                                                /* UINT's must have a 1-byte offset */
                                                if (offset3 == 1) {
                                                    sprintf(Sniffer.context->output+39, "1-byte length, ");
    
                                                    /* Update the buffer position and the buffer length */
                                                    SDP_UpdateBuffer(offset3);
                                                    /* Update the Attribute Id List length */
                                                    len2 -= offset3;
                                                    sprintf(Sniffer.context->output+54, "Len: %li",len3);
                                                    SnifferMsg(Sniffer.context->output);

                                                    /* Now print out the remaining portion of data */
                                                    DisplayHex(Buffer+Sniffer.context->sdp_offset, (U16)len3);
    
                                                    /* Update the buffer position and the buffer length */
                                                    SDP_UpdateBuffer((U16)len3);
                                                    /* Update the Attribute Id List length */
                                                    len2 -= len3;

                                                    /* Successful pass through Attribute Id/Attribute Value Pair */
                                                    lastPassed = TRUE;

                                                    /* ONLY PARSE NEXT ATTRIBUTE IF THE FIRST ONE PASSED */
                                                    /* Another AttributeID and AttributeValue exist
                                                     * in the AttributeList 
                                                     */
                                                    if (len2 > 0) goto NextAttribSSA;
    
                                                    /* ONLY PARSE THE NEXT ATTRIBUTE ID LIST IF THE FIRST ONE PASSED */
                                                    /* Another AttributeList exists in the AttributeLists param */ 
                                                    if (len > 0) goto NextAttributeList;
                                                }
                                                break;
                                                
                                            case DETD_UUID:
                                                sprintf(Sniffer.context->output, "         SDP:   AttributeValue: UUID, ");
                                                /* Find the length of the Data Element Sequence or Text segment,
                                                 * and the offset 
                                                 */
                                                len3 = SdpParseElement(Buffer+Sniffer.context->sdp_offset, &offset3);
                                                /* UUID's must have a 1-byte offset */
                                                if (offset3 == 1) {
                                                    sprintf(Sniffer.context->output+33, "1-byte length, ");
    
                                                    /* Update the buffer position and the buffer length */
                                                    SDP_UpdateBuffer(offset3);
                                                    /* Update the Attribute Id List length */
                                                    len2 -= offset3;
                                                    sprintf(Sniffer.context->output+48, "Len: %li",len3);
                                                    SnifferMsg(Sniffer.context->output);

                                                    /* Now print out the remaining portion of data */
                                                    DisplayHex(Buffer+Sniffer.context->sdp_offset, (U16)len3);
    
                                                    /* Update the buffer position and the buffer length */
                                                    SDP_UpdateBuffer((U16)len3);
                                                    /* Update the Attribute Id List length */
                                                    len2 -= len3;

                                                    /* Successful pass through Attribute Id/Attribute Value Pair */
                                                    lastPassed = TRUE;

                                                    /* ONLY PARSE NEXT ATTRIBUTE IF THE FIRST ONE PASSED */
                                                    /* Another AttributeID and AttributeValue exist
                                                     * in the AttributeList 
                                                     */
                                                    if (len2 > 0) goto NextAttribSSA;
    
                                                    /* ONLY PARSE THE NEXT ATTRIBUTE ID LIST IF THE FIRST ONE PASSED */
                                                    /* Another AttributeList exists in the AttributeLists param */ 
                                                    if (len > 0) goto NextAttributeList;
                                                }
                                                break;
                                                
                                            case DETD_BOOL:
                                                sprintf(Sniffer.context->output, "         SDP:   AttributeValue: BOOL, ");
                                                /* Find the length of the Data Element Sequence or Text segment,
                                                 * and the offset 
                                                 */
                                                len3 = SdpParseElement(Buffer+Sniffer.context->sdp_offset, &offset3);
                                                /* BOOL's must have a 1-byte offset and a length of 1 */
                                                if ((offset3 == 1) && (len3 == 1)) {
                                                    sprintf(Sniffer.context->output+33, "1-byte length, ");
    
                                                    /* Update the buffer position and the buffer length */
                                                    SDP_UpdateBuffer(offset3);
                                                    /* Update the Attribute Id List length */
                                                    len2 -= offset3;
                                                    sprintf(Sniffer.context->output+48, "Len: %li ", len3);

                                                    if (Buffer[Sniffer.context->sdp_offset] == FALSE) {
                                                        sprintf(Sniffer.context->output+55, "Value: FALSE");
                                                    }
                                                    else {
                                                        sprintf(Sniffer.context->output+55, "Value: TRUE");
                                                    }
                                                    SnifferMsg(Sniffer.context->output);

                                                    /* Update the buffer position and the buffer length */
                                                    SDP_UpdateBuffer((U16)len3);
                                                    /* Update the Attribute Id List length */
                                                    len2 -= len3;

                                                    /* Successful pass through Attribute Id/Attribute Value Pair */
                                                    lastPassed = TRUE;

                                                    /* ONLY PARSE NEXT ATTRIBUTE IF THE FIRST ONE PASSED */
                                                    /* Another AttributeID and AttributeValue exist
                                                     * in the AttributeList 
                                                     */
                                                    if (len2 > 0) goto NextAttribSSA;
    
                                                    /* ONLY PARSE THE NEXT ATTRIBUTE ID LIST IF THE FIRST ONE PASSED */
                                                    /* Another AttributeList exists in the AttributeLists param */ 
                                                    if (len > 0) goto NextAttributeList;
                                                }
                                                break;
                                            }
                                        }
                                    }   
                                }
                            }
                        }                                 
                    }

                    /* ONLY PARSE CONT STATE IF ATTRIBUTE LISTS PARSED PROPERLY */
                    if (lastPassed) {
                        /* Reset the last pass variable */
                        lastPassed = FALSE;             
                        SDP_ParseContinuationState(Buffer);
                    }
                }
            }
        }
    }
    SDP_UpdateSdpState();
}

/* 
 * Parses SDP Packet Headers.
 *
 * This parsing function attempts to parse all of the SDP 
 * attributes - ServiceSearch, ServiceAttrib, and ServiceSearchAttrib.  
 * UUID's and Attribute types are decoded into strings where possible.
 *
 * However, any response packets with continuation state information are
 * not parsed at this time, since it requires storing off many lengths and 
 * offsets from where we left off in the parsing process.  
 * 
 */
static const char *SDP_Parse(const U8 *Buffer, U16 BufferLen, BOOL First)
{
    /* Set the SDP Buffer Length */
    Sniffer.context->sdp_bufferLen = BufferLen;
    /* Reset SDP Offset */
    Sniffer.context->sdp_offset = 0;

 Beginning:
    switch (Sniffer.context->sdp_state) {
        /* We have a new SDP packet - parse the 5 byte header*/
    case BEGIN_PACKET:
        SDP_ParseHeader(Buffer);
        /* Parse the rest of the packet if this is the first 
         * LMP data indication. We don't parse data continue indications,
         * since we don't store enough information to know where to pick 
         * up in our parsing.
         */
        if ((Sniffer.context->sdp_bufferLen > 0) && First) {
            goto Beginning;
        }
        break;
    case SDP_ERROR_RESPONSE:
        /* Parse the SDP Error Response */
        SDP_ParseErrorResponse(Buffer);
        break;

    case SDP_SERVICE_SEARCH_REQ: 
        /* Parse the SDP Service Search Request */
        SDP_ParseServiceSearchRequest(Buffer);
        break;

    case SDP_SERVICE_SEARCH_RSP:
        /* Parse the SDP Service Search Response */
        SDP_ParseServiceSearchResponse(Buffer);
        break;

    case SDP_SERVICE_ATTRIB_REQ:
        /* Parse the SDP Service Attribute Request */
        SDP_ParseServiceAttributeRequest(Buffer);
        break;

    case SDP_SERVICE_ATTRIB_RSP:
        /* Parse the SDP Service Attribute Response */
        SDP_ParseServiceAttributeResponse(Buffer);
        break;

    case SDP_SERVICE_SEARCH_ATTRIB_REQ:
        /* Parse the SDP Service Search Attribute Request */
        SDP_ParseServiceSearchAttributeRequest(Buffer);
        break;

    case SDP_SERVICE_SEARCH_ATTRIB_RSP:
        /* Parse the SDP Service Search Attribute Response */
        SDP_ParseServiceSearchAttributeResponse(Buffer);
        break;

    case SDP_RESERVED_ID:
        /* Make sure any params are subtracted from the SDP Parameter Length */
        SDP_UpdateParamLen();
        SDP_UpdateSdpState();
        break;
    }

    /* Return a pointer to any unparsed portions of the SDP buffer */
    return Buffer+Sniffer.context->sdp_offset; 
}

/*
 *---------------------------------------------------------------------------
 *            HCI Command/Event Parsing Functions
 *---------------------------------------------------------------------------
 */
/*
 * Parses HCI Command Packets
 */
static void HciCommand_Parse(const HciPacket *packet)
{
    const U8   *p = packet->f[0].buffer;
    U8          addrString[BDADDR_NTOA_SIZE];
    U8          typeString[40];
    U8          cLine[9][(LINE_WIDTH * 4)+20];
    U8          i;

    for (i=0; i<9; i++) {
        cLine[i][0] = 0;
    }

    sprintf(cLine[0], "HCI-CMD: %s %s [Length %d]", 
            pHciCommand(LEtoHost16(packet->header)), 
            pTimeOffset(), 
            packet->header[2]);

    switch (LEtoHost16(packet->header)) {
    case HCC_CREATE_CONNECTION:
        sprintf(cLine[1], "         BD_ADDR:%s Type:%s", 
                bdaddr_ntoa((BD_ADDR *)p, addrString),
                pPacketType(LEtoHost16(p+6), typeString));
        sprintf(cLine[2], "         PSRMode:%s PSMode:%s ClkOffset:%d RSwitch:%s",
                pPsrMode(p[8]),
                pPsMode(p[9]),
                (LEtoHost16(p+10) & 0xEFFF),
                p[12] ? "Yes" : "No");
        p += 13;
        break;

    case HCC_DISCONNECT:
        sprintf(cLine[1], "         ConnHndl:%d Status:%s", 
                (LEtoHost16(p) & 0x0FFF),
                pHC_Status(p[2]));
        p += 3;
        break;

    case HCC_ACCEPT_CON_REQ:
        sprintf(cLine[1], "         BD_ADDR:%s Role:%s", 
                bdaddr_ntoa((BD_ADDR *)p, addrString),
                p[6] ? "Slave" : "Master");
        p += 7;
        break;

    case HCC_REJECT_CON_REQ:
        sprintf(cLine[1], "         BD_ADDR:%s Reason:%s", 
                bdaddr_ntoa((BD_ADDR *)p, addrString),
                pHC_Status(p[6]));
        p += 7;
        break;

    case HCC_LINK_KEY_REQ_REPL:
        sprintf(cLine[1], "         BD_ADDR:%s", 
                bdaddr_ntoa((BD_ADDR *)p, addrString));
        p += 6;
        break;

    case HCC_LINK_KEY_REQ_NEG_REPL:
        sprintf(cLine[1], "         BD_ADDR:%s", 
                bdaddr_ntoa((BD_ADDR *)p, addrString));
        p += 6;
        break;

    case HCC_PIN_CODE_REQ_REPL:
        sprintf(cLine[1], "         BD_ADDR:%s", 
                bdaddr_ntoa((BD_ADDR *)p, addrString));
        p += 6;
        break;

    case HCC_PIN_CODE_REQ_NEG_REPL:
        sprintf(cLine[1], "         BD_ADDR:%s", 
                bdaddr_ntoa((BD_ADDR *)p, addrString));
        p += 6;
        break;

    case HCC_SWITCH_ROLE:
        sprintf(cLine[1], "         BD_ADDR:%s", 
                bdaddr_ntoa((BD_ADDR *)p, addrString));
        p += 6;
        break;

    case HCC_WRITE_SCAN_ENABLE:
        sprintf(cLine[1], "         Scan_Enable:%s",
                pScanEnable(p[0]));
        p++;
        break;

    case HCC_SETUP_SYNC_CONNECTION:
        sprintf(cLine[1], "         ConnHndl:%d TxBandwidth:%ld RxBandwidth:%ld", 
                (LEtoHost16(p) & 0x0FFF),
                LEtoHost32(p+2),
                LEtoHost32(p+6));
        p += 2 + 4 + 4;
        break;

    case HCC_ACCEPT_SYNC_CON_REQ:
        sprintf(cLine[1], "         BD_ADDR:%s", 
                bdaddr_ntoa((BD_ADDR *)p, addrString));
        p += 6;
        break;

    case HCC_REJECT_SYNC_CON_REQ:
        sprintf(cLine[1], "         BD_ADDR:%s Reason:%s", 
                bdaddr_ntoa((BD_ADDR *)p, addrString),
                pHC_Status(p[6]));
        p += 7;
        break;

    case HCC_WRITE_LINK_POLICY:
        sprintf(cLine[1], "         ConnHndl:%d Enabled Modes:%s%s%s%s%s",
            (LEtoHost16(p) & 0x0FFFF),
            (LEtoHost16(p+2) == 0 ? "None" : ""),
            (LEtoHost16(p+2) & 0x01 ? "Role-switch " : ""),
            (LEtoHost16(p+2) & 0x02 ? "Hold " : ""),
            (LEtoHost16(p+2) & 0x04 ? "Sniff " : ""),
            (LEtoHost16(p+2) & 0x08 ? "Park " : ""));
        p += 4;
        break;

    default:
        break;
    }

    for (i=0; cLine[i][0]; i++) {
        SnifferMsg(cLine[i]);
    }

    DisplayHex(p, (U16)(packet->f[0].len-(p - packet->f[0].buffer)));
}

/*
 * Parses HCI Event Buffers
 */
static void HciEvent_Parse(const U8 *Buffer, U16 Len)
{
    const U8   *p = Buffer;
    U8          addrString[BDADDR_NTOA_SIZE];
    U8          eLine[9][(LINE_WIDTH * 4)+20];
    U8          i, j, numHndls;

    for (i=0; i<9; i++) {
        eLine[i][0] = 0;
    }

    sprintf(eLine[0], "HCI-EVT: %s %s [Length %d]", 
            pHciEvent(Buffer[0]), 
            pTimeOffset(),
            Buffer[1]);

    p += 2;

    switch (Buffer[0]) {
    case HCE_INQUIRY_COMPLETE:
        sprintf(eLine[1], "         Status:%s", 
                pHC_Status(p[0]));
        p++;
        break;

    case HCE_INQUIRY_RESULT:
    case HCE_INQUIRY_RESULT_WITH_RSSI:
        if (p[0] == 1) {
            sprintf(eLine[1], "         BD_ADDR: %s ClockOffset:%d", 
                    bdaddr_ntoa((BD_ADDR *)(p+1), addrString),
                    LEtoHost16(p+13));
            if (Buffer[0] == HCE_INQUIRY_RESULT_WITH_RSSI) {
                sprintf(eLine[1], "%s Rssi:%d",eLine[1],(S8)(p[14]));
            }
            sprintf(eLine[2], "         ");
            pHC_ClassOfDevice((U8 *)p + 9 + (Buffer[0]==HCE_INQUIRY_RESULT?1:0),
                eLine[2] + strlen(eLine[2]));
            p += 15;
        }
        break;

    case HCE_CONNECT_COMPLETE:
        sprintf(eLine[1], "         Status:%s ConnHndl:%d BD_ADDR:%s", 
                pHC_Status(p[0]),
                (LEtoHost16(p+1) & 0x0FFF),
                bdaddr_ntoa((BD_ADDR *)(p+3), addrString));
        sprintf(eLine[2], "         LinkType:%s EncryptMode:%s",
                pHC_LinkType(p[9]),
                pHC_EncryptMode(p[10]));
        p += 11;
        break;

    case HCE_CONNECT_REQUEST:
        sprintf(eLine[1], "         BD_ADDR:%s ConnType:%s",
                bdaddr_ntoa((BD_ADDR *)p, addrString),
                pHC_LinkType(p[9]));
        sprintf(eLine[2], "         ");
        pHC_ClassOfDevice((U8 *)p+6, eLine[2] + strlen(eLine[2]));
        p += 6 + 3 + 1;
        break;

    case HCE_DISCONNECT_COMPLETE:
        sprintf(eLine[1], "         Status:%s ConnHndl:%d Reason:%s", 
                pHC_Status(p[0]),
                (LEtoHost16(p+1) & 0x0FFF),
                pHC_Status(p[3]));
        p += 4;
        break;

    case HCE_AUTH_COMPLETE:
        sprintf(eLine[1], "         Status:%s ConnHndl:%d", 
                pHC_Status(p[0]),
                (LEtoHost16(p+1) & 0x0FFF));
        p += 3;
        break;

    case HCE_REMOTE_NAME_REQ_COMPLETE:
        sprintf(eLine[1], "         Status:%s BD_ADDR: %s", 
                pHC_Status(p[0]),
                bdaddr_ntoa((BD_ADDR *)(p+1), addrString));
        p += 7;
        break;

    case HCE_ENCRYPT_CHNG:
        sprintf(eLine[1], "         Status:%s ConnHndl:%d", 
                pHC_Status(p[0]),
                (LEtoHost16(p+1) & 0x0FFF));
        p += 3;
        break;

    case HCE_CHNG_CONN_LINK_KEY_COMPLETE:
        sprintf(eLine[1], "         Status:%s ConnHndl:%d", 
                pHC_Status(p[0]),
                (LEtoHost16(p+1) & 0x0FFF));
        p += 3;
        p++;
        break;

    case HCE_MASTER_LINK_KEY_COMPLETE:
        sprintf(eLine[1], "         Status:%s ConnHndl:%d", 
                pHC_Status(p[0]),
                (LEtoHost16(p+1) & 0x0FFF));
        p += 3;
        break;

    case HCE_READ_REMOTE_FEATURES_COMPLETE:
        sprintf(eLine[1], "         Status:%s ConnHndl:%d", 
                pHC_Status(p[0]),
                (LEtoHost16(p+1) & 0x0FFF));
        p += 3;
        break;

    case HCE_READ_REMOTE_VERSION_COMPLETE:
        sprintf(eLine[1], "         Status:%s ConnHndl:%d", 
                pHC_Status(p[0]),
                (LEtoHost16(p+1) & 0x0FFF));
        p += 3;
        break;

    case HCE_QOS_SETUP_COMPLETE:
        sprintf(eLine[1], "         Status:%s ConnHndl:%d", 
                pHC_Status(p[0]),
                (LEtoHost16(p+1) & 0x0FFF));
        p += 3;
        break;

    case HCE_COMMAND_COMPLETE:
        sprintf(eLine[1], "         NumHCICommandPackets:%d Command:%s", 
                p[0],
                pHciCommand(LEtoHost16(p+1)));
        p += 3;
        break;

    case HCE_COMMAND_STATUS:
        sprintf(eLine[1], "         Status:%s", 
                pHC_Status(p[0]));
        sprintf(eLine[2], "         NumHCICommandPackets:%d Command:%s", 
                p[1],
                pHciCommand(LEtoHost16(p+2)));
        p += 4;
        break;

    case HCE_HARDWARE_ERROR:
        break;

    case HCE_FLUSH_OCCURRED:
        sprintf(eLine[1], "         ConnHndl:%d", 
                (LEtoHost16(p) & 0x0FFF));
        p +=2;
        break;

    case HCE_ROLE_CHANGE:
        sprintf(eLine[1], "         Status:%s BD_ADDR:%s", 
                pHC_Status(p[0]),
                bdaddr_ntoa((BD_ADDR *)(p+1), addrString),
                p[7] ? "Slave" : "Master");
        p += 8;
        break;

    case HCE_NUM_COMPLETED_PACKETS:
        numHndls = p[0];
        for (i=0, j=1; i<numHndls; i++, j++) {
            sprintf(eLine[j], "         NumHndls:%d ConnHndl:%d NumPckts:%d", 
                    numHndls,
                    (LEtoHost16(p+1+2*i) & 0x0FFF),
                    (LEtoHost16(p+1+2*numHndls+2*i)));
            if ( j == 8) {
                for (j=0; j<9; j++) {
                    SnifferMsg(eLine[j]);
                }
                j = 0;
            }
        }
        p += 1 + numHndls*4;
        break;

    case HCE_MODE_CHNG:
        sprintf(eLine[1], "         Status:%s ConnHndl:%d", 
                pHC_Status(p[0]),
                (LEtoHost16(p+1) & 0x0FFF));
        p += 3;
        break;

    case HCE_RETURN_LINK_KEYS:
        break;

    case HCE_PIN_CODE_REQ:
        sprintf(eLine[1], "         BD_ADDR:%s", 
                bdaddr_ntoa((BD_ADDR *)p, addrString));
        p += 6;
        break;

    case HCE_LINK_KEY_REQ:
        sprintf(eLine[1], "         BD_ADDR:%s", 
                bdaddr_ntoa((BD_ADDR *)p, addrString));
        p += 6;
        break;

    case HCE_LINK_KEY_NOTIFY:
        sprintf(eLine[1], "         BD_ADDR:%s", 
                bdaddr_ntoa((BD_ADDR *)p, addrString));
        p += 6;
        break;

    case HCE_LOOPBACK_COMMAND:
        break;

    case HCE_DATA_BUFFER_OVERFLOW:
        break;

    case HCE_MAX_SLOTS_CHNG:
        sprintf(eLine[1], "         ConnHndl:%d MaxSlots:%d", 
                (LEtoHost16(p) & 0x0FFF),
                p[2]);
        p += 3;
        break;

    case HCE_READ_CLOCK_OFFSET_COMPLETE:
        sprintf(eLine[1], "         Status:%s ConnHndl:%d", 
                pHC_Status(p[0]),
                (LEtoHost16(p+1) & 0x0FFF));
        p += 3;
        break;

    case HCE_CONN_PACKET_TYPE_CHNG:
        sprintf(eLine[1], "         Status:%s ConnHndl:%d", 
                pHC_Status(p[0]),
                (LEtoHost16(p+1) & 0x0FFF));
        p += 3;
        break;

    case HCE_QOS_VIOLATION:
        sprintf(eLine[1], "         ConnHndl:%d", 
                (LEtoHost16(p) & 0x0FFF));
        p += 2;
        break;

    case HCE_PAGE_SCAN_MODE_CHANGE:
        sprintf(eLine[1], "         BD_ADDR:%s", 
                bdaddr_ntoa((BD_ADDR *)p, addrString));
        p += 6;
        break;

    case HCE_PAGE_SCAN_REPETITION_MODE:
        sprintf(eLine[1], "         BD_ADDR:%s", 
                bdaddr_ntoa((BD_ADDR *)p, addrString));
        p += 6;
        break;

    case HCE_SYNC_CONNECT_COMPLETE:
        sprintf(eLine[1], "         Status:%s ConnHndl:%d BD_ADDR:%s",
                pHC_Status(p[0]),
                (LEtoHost16(p+1) & 0x0FFF),
                bdaddr_ntoa((BD_ADDR *)p+3, addrString));
        p += 1 + 2 + 6;
        sprintf(eLine[2], "         LinkType:%s SlotsBetween:%d SlotsRetrans: %d",
                pHC_LinkType(p[0]),
                p[1], p[2]);
        p += 1 + 1 + 1;
        sprintf(eLine[3], "         RxPayload:%d TxPayload:%d AirMode:%s",
                LEtoHost16(p),
                LEtoHost16(p+2),
                pHC_AirMode(p[4]));
        p += 2 + 2 + 1;
        break;

    case HCE_SYNC_CONN_CHANGED:            
        sprintf(eLine[1], "         Status:%s ConnHndl:%d",
                pHC_Status(p[0]),
                (LEtoHost16(p+1) & 0x0FFF));
        p += 1 + 2;
        sprintf(eLine[2], "         SlotsBetween: %d SlotsRetrans: %d",
                p[1], p[2]);
        p += 1 + 1;
        sprintf(eLine[3], "         RxPayload:%d TxPayload:%d",
                LEtoHost16(p),
                LEtoHost16(p+2));
        p += 2 + 2;
        break;

    case HCE_VENDOR_SPECIFIC:
        break;

    case HCE_BLUETOOTH_LOGO:
        break;

    default:
        break;
    }
    
    for (i=0; eLine[i][0]; i++) {
        SnifferMsg(eLine[i]);
    }

    DisplayHex(p, (U16)(Len - (p - Buffer)));
}

const char *pScanEnable(U8 Enable)
{
    const char *ScanEnable[] = {
        "None",
        "Inquiry",
        "Page",
        "Inquiry & Page"
    };

    if (Enable > 3) return "Unknown";
    return ScanEnable[Enable];
}

const char *pPsMode(U8 Mode)
{
    const char *PsMode[] = {
        "Mandatory",
        "Optional I",
        "Optional II",
        "Optional III"
    };

    if (Mode > 3) return "Unknown";
    return PsMode[Mode];
}

const char *pPsrMode(U8 Mode)
{
    const char *PsrMode[] = {
        "R0",
        "R1",
        "R2"
    };

    if (Mode > 2) return "Unknown";
    return PsrMode[Mode];
}

const char *pHC_LinkType(U8 LinkType)
{
    const char  *hcLinkType[] = {
        "SCO_Connection",
        "ACL_Connection",
        "eSCO_Connection",
    };

    if (LinkType > 2) {
        return "Unknown";
    }

    return hcLinkType[LinkType];
}

const char *pHC_AirMode(U8 scoMode)
{
    const char *hcAirMode[] = {
        "u-LAW log",
        "A-LAW log",
        "CVSD",
        "Transparent",
    };
    if (scoMode > 3) {
        return "Unknown";
    }
    return hcAirMode[scoMode];
}

const char *pHC_EncryptMode(U8 Mode)
{
    const char  *hcEncryptMode[] = {
        "Disabled",
        "PtToPt",
        "PtToPt and Brdcst"
    };

    if (Mode > 2) {
        return "Unknown";
    }

    return hcEncryptMode[Mode];
}

const char *pHC_ClassOfDevice(U8 *p, char *string)
{
    U32 cod = (U32)(p[0]) | ((U32)(p[1])<<8) | ((U32)(p[2])<<16);

    /* Major service classes (any number) */
    string[0] = 0;
    strcat(string, "MajorService:");
    if (cod & COD_LIMITED_DISCOVERABLE_MODE) strcat(string, "Limited Discoverable|");
    if (cod & COD_POSITIONING) strcat(string, "Positioning|");
    if (cod & COD_NETWORKING) strcat(string, "Networking|");
    if (cod & COD_CAPTURING) strcat(string, "Capturing|");
    if (cod & COD_OBJECT_TRANSFER) strcat(string, "Object Transfer|");
    if (cod & COD_AUDIO) strcat(string, "Audio|");
    if (cod & COD_TELEPHONY) strcat(string, "Telephony|");
    if (cod & COD_INFORMATION) strcat(string, "Information|");
    
    /* Cap off major service class */
    if (string[strlen(string) - 1] == ':')
        strcat(string, "None");
    else
        string[strlen(string) - 1] = 0;


    strcat(string, " MajorDevice:");
    /* Major device classes (only one) */
    switch(cod & COD_MAJOR_UNCLASSIFIED)
    {
    case COD_MAJOR_MISCELLANEOUS:     strcat(string, "Miscellaneous"); break;
    case COD_MAJOR_COMPUTER:          strcat(string, "Computer"); break;
    case COD_MAJOR_PHONE:             strcat(string, "Phone"); break;
    case COD_MAJOR_LAN_ACCESS_POINT:  strcat(string, "LAN Access Point"); break;
    case COD_MAJOR_AUDIO:             strcat(string, "Audio"); break;
    case COD_MAJOR_PERIPHERAL:        strcat(string, "Peripheral"); break;
    case COD_MAJOR_IMAGING:           strcat(string, "Imaging"); break;
    case COD_MAJOR_UNCLASSIFIED:      strcat(string, "Unclassified"); break;
    default:                          strcat(string, "Unknown"); break;
    }

    sprintf(string, "%s MinorDevice:%02x", string, (U8)(cod & 0xFF));

    return string;
}

char *pPacketType(U16 Type, char *typeString)
{
    typeString[0] = 0;

    if ((Type & BAPT_DM1) == BAPT_DM1) {
        strcat(typeString, "DM1|");
    }
    if ((Type & BAPT_DH1) == BAPT_DH1) {
        strcat(typeString, "DH1|");
    }
    if ((Type & BAPT_AUX1) == BAPT_AUX1) {
        strcat(typeString, "AUX1|");
    }
    if ((Type & BAPT_DM3) == BAPT_DM3) {
        strcat(typeString, "DM3|");
    }
    if ((Type & BAPT_DH3) == BAPT_DH3) {
        strcat(typeString, "DH3|");
    }
    if ((Type & BAPT_DM5) == BAPT_DM5) {
        strcat(typeString, "DM5|");
    }
    if ((Type & BAPT_DH5) == BAPT_DH5) {
        strcat(typeString, "DH5|");
    }

    if (!typeString[0]) {
        strcat(typeString, "Unknown");
    }
    else {
        typeString[strlen(typeString) - 1] = 0;
    }

    return typeString;
}


/*
 *---------------------------------------------------------------------------
 *            OBEX Parsing Functions
 *---------------------------------------------------------------------------
 */
#define OBSC_RX_WAIT        0
#define OBSC_RX_STAGED      1
#define OBSC_RX_IGNORE      2

/*
 * Parses OBEX Packet headers.
 */
static const char *OBEX_Parse(ObexContext *obp, const U8 *RxBuff, U16 RxLen)
{
    U16     n, packetLen;
    U8      i, opcode;
    char    msg[LINE_LENGTH];

    /* Process the data in obp->rxBuff of length obp->rxLen. */
    while (RxLen > 0) {
        /* Restore the prior packet, if we got an Abort command during a response packet and we
         * aren't currently processing a sequence number header for the Abort command. 
         */
        if ((obp->savedCurPacketLen > 0) && (!obp->seqNumHeader)) {
            /* Restore the saved packet lengths */
            obp->curPacketLen = obp->savedCurPacketLen;
            obp->packetLen = obp->savedPacketLen;
            /* Restore the RX state */
            obp->rxState = OBSC_RX_IGNORE;
            /* Clear the saved lengths */
            obp->savedCurPacketLen = 0;
            obp->savedPacketLen = 0;
        }

        if ((obp->stageLen > 6) ||
            (!((obp->stageOffset == 0) || (obp->stageOffset < obp->stageLen)))) {
            SnifferMsg("ERROR: OBEX stage buffer invalid.");
            SNIFFER_STOP;
            InitObexParser(obp);
            return RxBuff;
        }

        if (obp->rxState == OBSC_RX_IGNORE) {
            /* It's possible to send an Abort command while a response is being 
             * received. 
             */
            if ((RxBuff[0] == 0xFF) && (BEtoHost16(RxBuff+1) == RxLen) && ((RxLen == 3) || (RxLen == 5))) {
                /* Save the current packet */
                obp->savedCurPacketLen = obp->curPacketLen;
                obp->savedPacketLen = obp->packetLen;
                /* Process the Abort command */
                obp->seqNumHeader = FALSE;
                obp->rxState = OBSC_RX_WAIT;
                /* Looks like a command, lets parse it! */
                obp->inSync = TRUE;
                /* Stage the packet length and opcode */
                obp->stageLen = 3;
                goto Stage;
            }

            if (RxLen > (obp->packetLen - obp->curPacketLen)) {
                SnifferMsg("ERROR: OBEX RxLen exceeded packet length.");
                SNIFFER_STOP;
                InitObexParser(obp);
                return RxBuff;
            }

            obp->curPacketLen += RxLen;
            /* In this state we do not need to do any further processing */
            break;
        }

    Stage:
        n = min((U16)(obp->stageLen - obp->stageOffset), RxLen);
        if (n > 6) {
            SnifferMsg("ERROR: OBEX 'n' exceeds 6.");
            SNIFFER_STOP;
            InitObexParser(obp);
            return RxBuff;
        }

        /* Stage the data */
        for (i = obp->stageOffset; n > 0; n--, i++) {
            obp->stageBuff[i] = *RxBuff;
            RxBuff++;
            RxLen--;
            obp->stageOffset++;
        }

        /* Only call the state machine if the data has been completely staged. */
        if (obp->stageOffset == obp->stageLen) {

            switch (obp->rxState) {
            case OBSC_RX_WAIT:
                /* We have what is likely the start of an OBEX packet. Assume
                 * we're in sync and retrieve the packet length and opcode.
                 */
                packetLen = BEtoHost16(obp->stageBuff + 1);
                opcode = (obp->stageBuff[0] & 0x7F);

                if (!obp->inSync) {
                    /* We've staged what might be an OBEX Command let's see. */
                    if (opcode > 5 || opcode == 4 || (RxLen > packetLen)) {
                        /* Not the start of a command, ignore it. */
                        InitObexParser(obp);
                        return RxBuff-obp->stageLen;
                    }
                    /* Looks like a command, lets parse it! */
                    obp->inSync = TRUE;
                }
                obp->packetLen = packetLen;

                /* Autodetect if the packet is a command or a response. */
                if (opcode <= 0x0F || opcode == 0x7F) {
                    /* This is a command so get the packet opcode */
                    obp->opcode = obp->stageBuff[0];
                    obp->isCommand = TRUE;
                } else {
                    /* This a response so get the packet response */
                    obp->rsp = obp->stageBuff[0];
                    obp->isCommand = FALSE;
                }

                /* Some opcodes like CONNECT and SetPath have 
                 * values beyond the length that are not headers. 
                 */
                switch (obp->opcode & 0x7F) {
                case 0: 
                    /* CONNECT: We need to get the OBEX version flags and
                     * Maximum OBEX packet length.
                     */
                    obp->stageLen = 4;
                    obp->curPacketLen = 7;
                    obp->rxState = OBSC_RX_STAGED;
                    /* See if we have a Session Sequence Number Header */
                    goto SeqNumHeader;

                case 7:
                    /* SESSION: We need to get the Session opcode out of the 
                     * Session Parameters Header */

                    /* The Session opcode has a length followed by headers. */
                    obp->stageLen = 0;
                    obp->curPacketLen = 3;
                    if (obp->isCommand) {
                        /* We have the expected Session Parameters header (0x52) and
                         * the Session Opcode tag (0x05) */
                        Assert ((RxBuff[0] == 0x52) && (RxBuff[3] == 0x05));
                        goto ShowObexPacket;
                    }
                    /* Drop down for SESSION Response */

                case 5:
                    /* SETPATH: The flags are only sent in the request packet 
                     * (received by the server). The response does not include
                     * the extra 2 bytes. In this case drop into the default case.
                     */
                    if (obp->isCommand) {
                        obp->stageLen = 2;
                        obp->curPacketLen = 5;
                        obp->rxState = OBSC_RX_STAGED;
                        /* See if we have a Session Sequence Number Header */
                        goto SeqNumHeader;
                    }
                    /* Drop down for SETPATH Response */

                default:
                    /* The other opcodes only have length followed by headers. */
                    obp->stageLen = 0;
                    obp->curPacketLen = 3;

                    if ((obp->packetLen >= (obp->curPacketLen+2)) && (RxLen > 0) &&
                        (RxBuff[obp->stageLen] == 0x93)) {
                        /* Looks like we have a Session Sequence Number Header */
                        obp->seqNumHeader = TRUE;
                        obp->stageLen += 2;
                        obp->curPacketLen += 2;
                        obp->rxState = OBSC_RX_STAGED;
                        break;
                    }
                    else {
                        /* Jump down to print the packet header */
                        goto ShowObexPacket;
                    }
                }
            SeqNumHeader:
                if ((obp->packetLen >= (obp->curPacketLen+2)) && (RxLen > 0) && 
                    (RxBuff[obp->stageLen] == 0x93)) {
                    /* Looks like we have a Session Sequence Number Header */
                    obp->seqNumHeader = TRUE;
                    obp->stageLen += 2;
                    obp->curPacketLen += 2;
                }
                break;

            case OBSC_RX_STAGED:
                /* If Connect, the parameters are in the stage buffer. */
                /* If SetPath Command, the flags & constants are in the stage buffer */
                /* If Session Command, the session opcode is in the stage buffer */
                /* Otherwise, the opcode and length are in the stage buffer. */
            ShowObexPacket:

                if (obp->isCommand) {
                    if ((obp->opcode & 0x7F) == 7) /* Session */
                        sprintf(msg, "         OBEX: %s Operation%s, Length %d", pObexSessOpcode(RxBuff[5]), ((obp->opcode & 0x80) ? " +Final Bit" : ""), obp->packetLen);
                    else sprintf(msg, "         OBEX: %s Operation%s, Length %d", pObexOpcode(obp->opcode), ((obp->opcode & 0x80) ? " +Final Bit" : ""), obp->packetLen);
                }
                else sprintf(msg, "         OBEX: %s Response%s, Length %d", pObexRsp(obp->rsp), ((obp->rsp & 0x80) ? " +Final Bit" : ""), obp->packetLen);
                SnifferMsg(msg);

                if ((obp->opcode & 0x7F) == 0) { /* Connect */
                    sprintf(msg, "         OBEX: Ver %d.%d, OBEX Max Packet %d, Flags 0x%02x", ((obp->stageBuff[0] & 0xF0) >> 4), (obp->stageBuff[0] & 0x0F), BEtoHost16(obp->stageBuff + 2), obp->stageBuff[1]);
                    SnifferMsg(msg);
                } else if (((obp->opcode & 0x7F) == 5) && obp->isCommand) { /* Set Path */
                    sprintf(msg, "         OBEX: Flags 0x%02x, Constants 0x%02x", obp->stageBuff[0], obp->stageBuff[1]);
                    SnifferMsg(msg);
                } else if (((obp->opcode & 0x7F) == 7) && obp->isCommand) { /* Session */
                    sprintf(msg, "         OBEX: %s", pObexSessOpcode(obp->stageBuff[0]));
                    SnifferMsg(msg);
                }

                if (obp->seqNumHeader) {
                    /* Sequence Number is the last byte in the stage buffer */
                    sprintf(msg, "         OBEX: Sequence Number %d", obp->stageBuff[obp->stageLen-1]);
                    SnifferMsg(msg);
                }
                /* We ignore the data part of the packet */
                obp->rxState = OBSC_RX_IGNORE;
                break;
            } /* End switch(rxState) */
            
            obp->stageOffset = 0;
        
        } /* End if (stageOffset == stageLen) */
    
    } /* End While() */

    if (obp->curPacketLen == obp->packetLen) {
        /* The packet is done. Prep for a new one. */
        obp->rxState = OBSC_RX_WAIT;
        obp->seqNumHeader = FALSE;
        obp->stageLen = 0;
        obp->stageLen = 3;
    }
    return RxBuff;
}

/*
 * Initialize an OBEX parser
 */
static void InitObexParser(ObexContext *obp)
{
    OS_MemSet((U8 *)obp, 0, sizeof(ObexContext));

    obp->rxState = OBSC_RX_WAIT;
    obp->seqNumHeader = FALSE;
    /* Stage the packet length and opcode */
    obp->stageLen = 3;
}


/*
 * Functions for converting OBEX Opcodes and common response values into strings
 */
static const char *pObexRsp(U8 RespCode)
{
    switch (RespCode & 0x7F) {
    case 0x10: return "Continue";
    case 0x20: return "Success";
    case 0x21: return "Created";
    case 0x22: return "Accepted";
    case 0x23: return "Non-Authoritative Info";
    case 0x24: return "No Content";
    case 0x25: return "Reset Content";
    case 0x26: return "Partial Content";
    case 0x30: return "Multiple Choices";
    case 0x31: return "Moved Permanently";
    case 0x32: return "Moved Temporarily";
    case 0x33: return "See Other";
    case 0x34: return "Not Modified";
    case 0x35: return "Use Proxy";
    case 0x40: return "Bad Request";
    case 0x41: return "Unauthorized";
    case 0x42: return "Payment Required";
    case 0x43: return "Forbidden";
    case 0x44: return "Not Found";
    case 0x45: return "Method Not Allowed";
    case 0x46: return "Not Acceptable";
    case 0x47: return "Proxy Authentication Required";
    case 0x48: return "Request Timeout";
    case 0x49: return "Conflict";
    case 0x4A: return "Gone";
    case 0x4B: return "Length Required";
    case 0x4C: return "Precondition Failed";
    case 0x4D: return "Entity Too Large";
    case 0x4E: return "URL Too Large";
    case 0x4F: return "Unsupported Media Type";
    case 0x50: return "Internal Server Error";
    case 0x51: return "Not Implemented";
    case 0x52: return "Bad Gateway";
    case 0x53: return "Service Unavailable";
    case 0x54: return "Gateway Timeout";
    case 0x55: return "HTTP Version Not Supported";
    case 0x60: return "Database Full";
    case 0x61: return "Database Locked";
    }
    sprintf (Sniffer.obexBuff, "Unknown (%x)", (RespCode & 0x7F));
    return Sniffer.obexBuff;
}

static const char *pObexSessOpcode(U8 Opcode)
{
    switch (Opcode) {
    case 0: return "Create Session";
    case 1: return "Close Session";
    case 2: return "Suspend Session";
    case 3: return "Resume Session";
    case 4: return "Set Timeout";
    }
    sprintf (Sniffer.obexBuff, "Unknown (%x)", (Opcode));
    return Sniffer.obexBuff;
}

static const char *pObexOpcode(U8 Opcode)
{
    switch (Opcode & 0x7F) {
    case 0: return "Connect";
    case 1: return "Disconnect";
    case 2: return "Put";
    case 3: return "Get";
    case 5: return "Set Path";
    case 7: return "Session";
    case 0x7F: return "Abort";
    }
    sprintf (Sniffer.obexBuff, "Unknown (%x)", (Opcode & 0x7F));
    return Sniffer.obexBuff;
}


/*
 *---------------------------------------------------------------------------
 *            New Protocol Parsing Functions
 *---------------------------------------------------------------------------
 */

/*
 *---------------------------------------------------------------------------
 *            TCS Parsing Functions
 *---------------------------------------------------------------------------
 */
#if TCS_CORDLESS == XA_ENABLED || TCS_INTERCOM == XA_ENABLED

#include "sys/tcsi.h"
const char *pTCS_Event(TcsEvent tcs_event);
const char *pTCS_Message(TcsMsgType msg);


#define TCS_INITIAL_STRING "         TCS: "
/* Call this to determine what should be pre-pended to the
 * next TCS output string. As a side effect, may dump the current
 * output string. Returns the current position.
 *
 */
static void TcsOutput(const char *buf)
{
    U16 index, bufLen;

    index = strlen(Sniffer.context->output);
    bufLen = min(LINE_LENGTH-16, strlen(buf));

    /* Do we need to wrap? */
    if (bufLen + index >= LINE_LENGTH)
    {
        SnifferMsg(Sniffer.context->output);
        Sniffer.context->output[0]=0;
    }

    /* If the buffer is empty put in our initial string */
    if (Sniffer.context->output[0] == 0)
    {
        strcpy(Sniffer.context->output, TCS_INITIAL_STRING);
    }
    else
    {
        strcat(Sniffer.context->output, ", ");
    }

    index = strlen(Sniffer.context->output);
    sprintf(Sniffer.context->output + index, "%.*s", bufLen, buf);
}

static void pTCS_Data(TcsMsgData *tcsmd)
{
    U16 i;
    char stage[LINE_LENGTH];

    Sniffer.context->output[0]=0;
    TcsOutput(pTCS_Message(tcsmd->msg));
    
    if (tcsmd->fields & TCS_FIELD_CCLASS) {
        switch (tcsmd->call_class) {
        case TCS_CC_EXTERNAL:
            TcsOutput("CallClass:external");
            break;
        case TCS_CC_INTERCOM:
            TcsOutput("CallClass:intercom");
            break;
        case TCS_CC_SERVICE:
            TcsOutput("CallClass:sevice");
            break;
        case TCS_CC_EMERGENCY:
            TcsOutput("CallClass:emergency");
            break;
        default:
            TcsOutput("CallClass:unknown");
            break;
        }
    }
    
    if (tcsmd->fields & TCS_FIELD_SENDDONE) {
        TcsOutput("SendDone");
    }
    
    if (tcsmd->fields & TCS_FIELD_BC) {
        switch (tcsmd->linkType) {
        case 0:
            strcpy(stage, "LinkType:sco");
            break;
        case 1:
            strcpy(stage, "LinkType:acl");
            break;
        case 2:
            strcpy(stage, "LinkType:none");
            break;
        default:
            strcpy(stage, "LinkType:unknown");

        }

        switch (tcsmd->uiPt & 0xF0) {
        case TCS_BC_PT_HV1:
            strcat(stage, "uiPt HV1/");
            break;
        case TCS_BC_PT_HV2:
            strcat(stage, "uipt HV2/");
            break;
        case TCS_BC_PT_HV3:
            strcat(stage, "uiPt HV3/");
            break;
        case TCS_BC_PT_DV:
            strcat(stage, "uiPt DV/");
            break;
        default:
            strcat(stage, "??? /");
            break;
        }

        switch (tcsmd->uiPt & 0x0F) {
        case TCS_BC_UIL1_CVSD:
            strcat(stage,"CVSD");
            break;
        case TCS_BC_UIL1_PCMAL:
            strcat(stage,"PCMAL");
            break;
        case TCS_BC_UIL1_PCMUL:
            strcat(stage,"PCMUL");
            break;
        default:
            strcat(stage,"???");
            break;
        }
        TcsOutput(stage);
    }
    if (tcsmd->fields & TCS_FIELD_CAUSE) {
        sprintf(stage, "Cause %d", tcsmd->cause);
        TcsOutput(stage);
    }
    
    if (tcsmd->fields & TCS_FIELD_SCO) {
        sprintf(stage, "ScoHandle %d", tcsmd->sco_handle);
        TcsOutput(stage);
    }
    
    if (tcsmd->fields & TCS_FIELD_KEY) {
        sprintf(stage, "Key 0x%x", tcsmd->key);
        TcsOutput(stage);
    }
    
    if (tcsmd->fields & TCS_FIELD_CING) {
        sprintf(stage, "CallingNumber %.*s", 
                min(tcsmd->calling.nlen,24), tcsmd->calling.number);
        TcsOutput(stage);
        sprintf(stage, "Calling type 0x%x, flags 0x%x",
                tcsmd->calling.ntypeNpid, tcsmd->calling.flags);
        TcsOutput(stage);
    }
    if (tcsmd->fields & TCS_FIELD_CLKOFF) {
        sprintf(stage, "ClockOffset 0x%4x", tcsmd->clk_off);
        TcsOutput(stage);
    }

    if (tcsmd->fields & TCS_FIELD_CDATA) {
        sprintf(stage, "Members:%d", tcsmd->num_members);
        TcsOutput(stage);
        for(i = 0; i < tcsmd->num_members; i++) {
            sprintf(stage, "Member %d code:%c%c", i,
                    tcsmd->inum_digit1[i], tcsmd->inum_digit2[i]);
            TcsOutput(stage);
        }
    }

    if (tcsmd->fields & TCS_FIELD_CED) {
        sprintf(stage, "CalledNumber %.*s", 
                min(tcsmd->called.nlen,24), tcsmd->called.number);
        TcsOutput(stage);
        sprintf(stage, "Called type 0x%x, flags 0x%x",
                tcsmd->called.ntypeNpid, tcsmd->called.flags);
        TcsOutput(stage);
    }
    
    SnifferMsg(Sniffer.context->output);
}

/* Main entrypoint for TCS decoder */
const char *SNIFFER_TCS_Parse(const U8 *Buffer, U16 BufferLen)
{
    TcsMsgData tcsmd;
    BtStatus status;
    U16 toCopy, toAccept;

    /* Grab TCS length from current l2cap length */
    if (Sniffer.context->tcs_len == 0)
    {
        Sniffer.context->tcs_len = Sniffer.context->l2cap_len;
        Sniffer.context->tcs_pos = 0;
    }

    /* Collect TCS data as it arrives into a local staging buffer. */
    toAccept = min(BufferLen, Sniffer.context->tcs_len - Sniffer.context->tcs_pos);
    if (Sniffer.context->tcs_pos < MAX_TCS_BUFF)
    {
        toCopy = min(MAX_TCS_BUFF - Sniffer.context->tcs_pos,
                     toAccept);
        OS_MemCopy(Sniffer.context->tcs_buff + Sniffer.context->tcs_pos,
                   Buffer, toCopy);
    }
    Sniffer.context->tcs_pos += toAccept;
    Buffer += toAccept;

    /* Don't need to print segmentation information
       sprintf(Sniffer.context->output, "         TCS: %d bytes, %d tlen, %d accepted", 
       BufferLen, Sniffer.context->tcs_len, toAccept);
       SnifferMsg(Sniffer.context->output);
    */

    /* If we have what we need, decode it */
    if (Sniffer.context->tcs_pos == Sniffer.context->tcs_len) {

        /* Was the buffer truncated? */
        if (Sniffer.context->tcs_pos > MAX_TCS_BUFF) {
            status = BT_STATUS_FAILED;
        } else {
            status = Tcs_msg_data_rx(&tcsmd, Sniffer.context->tcs_buff,
                                     Sniffer.context->tcs_len);
        }

        if (status == BT_STATUS_FAILED) {
            sprintf(Sniffer.context->output, "         TCS: %s, Tlen %d (parse failed)", 
                    pTCS_Message(Buffer[0]), Sniffer.context->l2cap_len);
            SnifferMsg(Sniffer.context->output);
        } else {
            pTCS_Data(&tcsmd);
        }

        Sniffer.context->tcs_len = 0;
    }
    else if (BufferLen > 0)
    {
        SnifferMsg(TCS_INITIAL_STRING " (waiting for rest of packet)");
    }

    return Buffer; 
}



#endif /* TCS_PROTOCOL, XA_DEBUG */


#endif /* XA_DEBUG */

