/***************************************************************************
 *
 * File:
 *     $Workfile:sdp_app.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:33$
 *
 * Description:   This file contains the code for the SDAP sample application. 
 *
 * Created:       February 20, 2001
 *
 * Copyright 2001-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "sdp.h"
#include "btalloc.h"
#include "sys/debug.h"
#include "stdio.h"
#include "windows.h"

#if SDP_PARSING_FUNCS == XA_DISABLED
#error The SDP sample application requires the SDP parsing functions.
#endif /* SDP_PARSING_FUNCS == XA_DISABLED */

/****************************************************************************
 *
 * Constants
 *
 ****************************************************************************/

#define FLAG_SERVICE_CLASS_ID_LIST          0x0001
#define FLAG_PROTOCOL_DESC_LIST             0x0002
#define FLAG_BT_PROFILE_DESC_LIST           0x0004
#define FLAG_SUPPORTED_FORMATS_LIST         0x0008
#define FLAG_SERVICE_NAME                   0x0010
#define FLAG_SUPPORTED_DATA_STORES_LIST     0x0020
#define FLAG_FAX_CLASS_1_SUPPORT            0x0040
#define FLAG_FAX_CLASS_20_SUPPORT           0x0080
#define FLAG_FAX_CLASS_2_SUPPORT            0x0100
#define FLAG_AUDIO_FEEDBACK_SUPPORT         0x0200
#define FLAG_SERVICE_DESCRIPTION            0x0400
#define FLAG_SERVICE_AVAILABILITY           0x0800
#define FLAG_IP_SUBNET                      0x1000
#define FLAG_REMOTE_AUDIO_VOL_CONTROL       0x2000
#define FLAG_EXTERNAL_NETWORK               0x4000

/****************************************************************************
 *
 * Module RAM data
 *
 ****************************************************************************/

SdpRecord   DialupSdpRecord; 
SdpRecord   FaxSdpRecord;
SdpRecord   ObexSdpRecord;
SdpRecord   SerialSdpRecord;
SdpRecord   LanSdpRecord;
SdpRecord   HeadsetSdpRecord;
SdpRecord   HeadsetGWSdpRecord;
SdpRecord   IntercomSdpRecord;
SdpRecord   CordlessSdpRecord;
SdpRecord   FileTransferSdpRecord;
SdpRecord   SyncSdpRecord;
SdpRecord   SyncCommandSdpRecord;

/* Static RAM data */
static BtHandler        btHandler;
static SdpQueryToken    queryToken;
static U16              flags;

/* Shared RAM data with the GUI */
BtRemoteDevice*         remDev;
U16                     qUuid;
U8                      uuidSize;
BtHandler               devHandler;

#define BT_128BIT_UUID(uuid16) \
    0x00, 0x00, \
    (U8)(((uuid16) & 0xff00) >> 8), (U8)((uuid16) & 0x00ff), \
    0x00, 0x00, 0x10, 0x00, \
    0x80, 0x00, 0x00, 0x80, 0x5F, 0x9B, 0x34, 0xFB

/****************************************************************************
 *
 * Module ROM data
 *
 ****************************************************************************/

/*-------------------------------------------------------------------------
 *
 * Public Browse Group Attribute
 *
 *     Any SdpAttribute structure can include this attribute to add itself
 *     to the root level of the Public Browse Group.
 */
static const U8 PublicBrowseGroup[] = {
    SDP_ATTRIB_HEADER_8BIT(3),          /* 3 bytes */
    SDP_UUID_16BIT(SC_PUBLIC_BROWSE_GROUP),  /* Public Browse Group */
};


/*-------------------------------------------------------------------------
 *
 * Dialup Networking SDP record attributes
 */

/* Attribute value of Dial-up Networking Service class ID list */
static const U8 DialupServClassVal[] = {
    SDP_ATTRIB_HEADER_8BIT(34),            /* Data element sequence, 34 bytes */
    /* ServiceClassIdList - most specific to most general class */
    SDP_UUID_128BIT(BT_128BIT_UUID(SC_DIALUP_NETWORKING)), /* Uuid128 Dialup Networking */
    SDP_UUID_128BIT(BT_128BIT_UUID(SC_GENERIC_NETWORKING)) /* Uuid128 Generic Networking */
};

/* Attribute value of Dial-up Netwokring Protocol descriptor list */
static U8 DialupProtocolDescVal[] = {
    SDP_ATTRIB_HEADER_8BIT(12),  /* Data element sequence, 12 bytes */
    SDP_ATTRIB_HEADER_8BIT(3),   /* Data element sequence for L2CAP, 3 bytes */
    SDP_UUID_16BIT(PROT_L2CAP),  /* Uuid16 L2CAP */
    SDP_ATTRIB_HEADER_8BIT(5),   /* Data element sequence for RFCOMM, 5 bytes */
    SDP_UUID_16BIT(PROT_RFCOMM), /* Uuid16 RFCOMM */
    SDP_UINT_8BIT(1)             /* Uint8 RFCOMM channel number - value will vary */
};

/* Attribute value of Dial-up networking Language Base */
static const U8 DialupLangBaseVal[] = {
    SDP_ATTRIB_HEADER_8BIT(9),   /* Data element sequence, 9 bytes */
    SDP_UINT_16BIT(0x656E),      /* Uint16 "en" (English) */
    SDP_UINT_16BIT(0x006A),      /* Uint16 UTF-8 encoding */
    SDP_UINT_16BIT(0x0100)       /* Uint16 primary language base ID */
};

/* Attribute value of Dial-up networking profile descriptor list */
static const U8 DialupProfileDescListVal[] = {
    SDP_ATTRIB_HEADER_8BIT(8),            /* Data element sequence, 8 bytes */
    SDP_ATTRIB_HEADER_8BIT(6),            /* Data element sequence for ProfileDescriptor, 6 bytes */
    SDP_UUID_16BIT(SC_DIALUP_NETWORKING), /* Uuid16 Dialup networking */
    SDP_UINT_16BIT(0x0100)                /* Uint16 version number */
};

/* Attribute value of Dialup networking service name */
static const U8 DialupServiceNameVal[] = {
    SDP_TEXT_8BIT(19),           /* Null terminated text string, 19 bytes */
    'D','i','a','l','-','u','p',' ',
    'N','e','t','w','o','r','k','i','n','g',0x00
};

/* Attribute value of Dialup networking audio feedback support */
static const U8 DialupAudioFeedbackSupport[] = {
    SDP_BOOL(0)                  /* No Audio Feedback Support */
};

static SdpAttribute DialupSdpAttribs[] = {
    /* Dialup networking service class ID list */
    SDP_ATTRIBUTE(AID_SERVICE_CLASS_ID_LIST, DialupServClassVal),
    /* Dialup networking protocol descriptor list */
    SDP_ATTRIBUTE(AID_PROTOCOL_DESC_LIST, DialupProtocolDescVal),
    /* Public Browse Group Service */
    SDP_ATTRIBUTE(AID_BROWSE_GROUP_LIST, PublicBrowseGroup),
    /* Dialup networking language base attribute ID list */
    SDP_ATTRIBUTE(AID_LANG_BASE_ID_LIST, DialupLangBaseVal),
    /* Dialup networking Bluetooth profile descriptor list */
    SDP_ATTRIBUTE(AID_BT_PROFILE_DESC_LIST, DialupProfileDescListVal),
    /* Dialup networking service name */
    SDP_ATTRIBUTE((AID_SERVICE_NAME + 0x0100), DialupServiceNameVal),
    /* Audio Feedback Support - where is this attribute defined? */
    SDP_ATTRIBUTE(AID_AUDIO_FEEDBACK_SUPPORT, DialupAudioFeedbackSupport)
};

/*-------------------------------------------------------------------------
 *
 * Fax SDP record attributes
 */

/* Attribute value of Fax Service class ID list */
static const U8 FaxServClassVal[] = {
    SDP_ATTRIB_HEADER_8BIT(10),           /* Data element sequence, 6 bytes */
    /* ServiceClassIdList - most specific to most general class */
    SDP_UUID_32BIT(SC_FAX),              /* Uuid32 Fax */
    SDP_UUID_32BIT(SC_GENERIC_TELEPHONY) /* Uuid32 Generic telephony */
};

/* Attribute value of Fax protocol descriptor list */
static U8 FaxProtocolDescVal[] = {
    SDP_ATTRIB_HEADER_8BIT(12),   /* Data element sequence, 12 bytes */
    SDP_ATTRIB_HEADER_8BIT(3),    /* Data element sequence for L2CAP, 3 bytes */
    SDP_UUID_16BIT(PROT_L2CAP),   /* Uuid16 L2CAP */
    SDP_ATTRIB_HEADER_8BIT(5),    /* Data element sequence for RFCOMM, 5 bytes */
    SDP_UUID_16BIT(PROT_RFCOMM),  /* Uuid16 RFCOMM */
    SDP_UINT_8BIT(2)              /* Uint8 RFCOMM channel number - value will vary */
};

/* Attribute value of Fax language base attribute ID list */
static const U8 FaxLangBaseVal[] = {
    SDP_ATTRIB_HEADER_8BIT(9), /* Data element sequence, 9 bytes */
    SDP_UINT_16BIT(0x656E),    /* Uint16 "en" (English) */
    SDP_UINT_16BIT(0x006A),    /* Uint16 UTF-8 encoding */
    SDP_UINT_16BIT(0x0100)     /* Uint16 primary language base ID */
};

/* Attribute value of Fax Bluetooth profile descriptor list */
static const U8 FaxProfileDescListVal[] = {
    SDP_ATTRIB_HEADER_8BIT(8), /* Data element sequence, 8 bytes */
    SDP_ATTRIB_HEADER_8BIT(6), /* Data element sequence for ProfileDescriptor, 6 bytes */
    SDP_UUID_16BIT(SC_FAX),    /* Uuid16 fax */
    SDP_UINT_16BIT(0x0100)     /* Uint16 version number */
};

/* Attribute value of Fax Service name */
static const U8 FaxServiceNameVal[] = {
    SDP_TEXT_8BIT(4),       /* Null terminated text string, 4 bytes */
    'F','a','x',0x00        /* Text string */
};

/* Attribute value of Fax Audio Feeback */
static const U8 FaxAudioFeedbackSupport[] = {
    SDP_BOOL(0)                  /* No Audio Feedback Support */
};

/* Attribute value of Fax Class 1 Support */
static const U8 FaxClass1Support[] = {
    SDP_BOOL(0)                  /* No Fax Class 1 Support */
};

/* Attribute value of Fax Class 2.0 Support */
static const U8 FaxClass20Support[] = {
    SDP_BOOL(0)                  /* No Fax Class 2.0 Support */
};

/* Attribute value of Fax Class 2 Support */
static const U8 FaxClass2Support[] = {
    SDP_BOOL(0)                  /* No Fax Class 2 Support */
};

static SdpAttribute FaxSdpAttribs[] = {
    /* Fax service class ID list */
    SDP_ATTRIBUTE(AID_SERVICE_CLASS_ID_LIST, FaxServClassVal),
    /* Fax protocol descriptor list */
    SDP_ATTRIBUTE(AID_PROTOCOL_DESC_LIST, FaxProtocolDescVal),
    /* Public Browse Group Service */
    SDP_ATTRIBUTE(AID_BROWSE_GROUP_LIST, PublicBrowseGroup), 
    /* Fax language base attribute ID list */
    SDP_ATTRIBUTE(AID_LANG_BASE_ID_LIST, FaxLangBaseVal),
    /* Fax Bluetooth profile descriptor list */
    SDP_ATTRIBUTE(AID_BT_PROFILE_DESC_LIST, FaxProfileDescListVal),
    /* Fax service name */
    SDP_ATTRIBUTE((AID_SERVICE_NAME + 0x0100), FaxServiceNameVal),
    /* Fax class 1 support */
    SDP_ATTRIBUTE(AID_FAX_CLASS_1_SUPPORT, FaxClass1Support),
    /* Fax class 2.0 support */
    SDP_ATTRIBUTE(AID_FAX_CLASS_20_SUPPORT, FaxClass20Support),
    /* Fax class 2 support */
    SDP_ATTRIBUTE(AID_FAX_CLASS_2_SUPPORT, FaxClass2Support),
    /* Audio feedback support */
    SDP_ATTRIBUTE(AID_AUDIO_FEEDBACK_SUPPORT, FaxAudioFeedbackSupport)
};

/*-------------------------------------------------------------------------
 *
 * OBEX object push SDP record attributes
 */

/* Attribute value of OBEX object push Service class ID list */
static const U8 ObexOpServClassVal[] = {
    SDP_ATTRIB_HEADER_8BIT(3),           /* Data element sequence, 3 bytes */
    SDP_UUID_16BIT(SC_OBEX_OBJECT_PUSH), /* Uuid16 OBEX object push */
};

/* Attribute value of OBEX object push protocol descriptor list */
static U8 ObexOpProtocolDescVal[] = {
    SDP_ATTRIB_HEADER_8BIT(17),  /* Data element sequence, 17 bytes */
    SDP_ATTRIB_HEADER_8BIT(3),   /* Data element sequence for L2CAP, 3 bytes */
    SDP_UUID_16BIT(PROT_L2CAP),  /* Uuid16 L2CAP */
    SDP_ATTRIB_HEADER_8BIT(5),   /* Data element sequence for RFCOMM, 5 bytes */
    SDP_UUID_16BIT(PROT_RFCOMM), /* Uuid16 RFCOMM */
    SDP_UINT_8BIT(3),            /* Uint8 RFCOMM channel number - value will vary */
    SDP_ATTRIB_HEADER_8BIT(3),   /* Data element sequence, for OBEX */
    SDP_UUID_16BIT(PROT_OBEX)    /* OBEX */
};

/* Attribute value of OBEX object push language base attribute ID list */
static const U8 ObexOpLangBaseVal[] = {
    SDP_ATTRIB_HEADER_8BIT(9),   /* Data element sequence, 9 bytes */
    SDP_UINT_16BIT(0x656A),      /* Uint16 "en" (English) */
    SDP_UINT_16BIT(0x006A),      /* Uint16 UTF-8 encoding */
    SDP_UINT_16BIT(0x0100)       /* Uint16 primary language base ID */
};

/* Attribute value of OBEX object push Bluetooth profile descriptor list */
static const U8 ObexOpProfileDescListVal[] = {
    SDP_ATTRIB_HEADER_8BIT(8),           /* Data element sequence, 8 bytes */
    SDP_ATTRIB_HEADER_8BIT(6),           /* Data element sequence for ProfileDescriptor, 6 bytes */
    SDP_UUID_16BIT(SC_OBEX_OBJECT_PUSH), /* Uuid16 Object Push */
    SDP_UINT_16BIT(0x0100)               /* Uint16 version number */
};

/* Attribute value of OBEX object push Service name */
static const U8 ObexOpServiceNameVal[] = {
    SDP_TEXT_8BIT(17),           /* Null terminated text string, 17 bytes */
    'O','B','E','X',' ',         /* Text string */
    'O','b','j','e','c','t',' ',
    'P','u','s','h',0x00
};

/* Attribute value of OBEX object push Supported Formats List */
static const U8 ObexOpSupportedFormatsList[] = {
    SDP_ATTRIB_HEADER_8BIT(2),          /* Data element sequence, 2 bytes */
    SDP_UINT_8BIT(0xFF)                 /* Uint8 supported formats (all) */
};

static SdpAttribute ObexOpSdpAttribs[] = {
    /* OBEX object push service class ID list */
    SDP_ATTRIBUTE(AID_SERVICE_CLASS_ID_LIST, ObexOpServClassVal), 
    /* OBEX object push protocol descriptor list */
    SDP_ATTRIBUTE(AID_PROTOCOL_DESC_LIST, ObexOpProtocolDescVal),
    /* Public Browse Group Service */
    SDP_ATTRIBUTE(AID_BROWSE_GROUP_LIST, PublicBrowseGroup), 
    /* OBEX object push language base attribute ID list */
    SDP_ATTRIBUTE(AID_LANG_BASE_ID_LIST, ObexOpLangBaseVal),
    /* OBEX object push Bluetooth profile descriptor list */
    SDP_ATTRIBUTE(AID_BT_PROFILE_DESC_LIST, ObexOpProfileDescListVal),
    /* OBEX object push service name */
    SDP_ATTRIBUTE((AID_SERVICE_NAME + 0x0100), ObexOpServiceNameVal),
    /* OBEX supported formats list */
    SDP_ATTRIBUTE(AID_SUPPORTED_FORMATS_LIST, ObexOpSupportedFormatsList)
};

/*-------------------------------------------------------------------------
 *
 * Serial Port SDP record attributes
 */

/* Attribute value of Serial Port Service Class Id List */ 
static const U8 SerialServClassIdVal[] = {
    SDP_ATTRIB_HEADER_8BIT(17),          /* Data element sequence, 17 bytes */
    SDP_UUID_128BIT(BT_128BIT_UUID(SC_SERIAL_PORT))      /* Uuid128 Serial Port */
};

/* Attribute value of Serial Port Protocol Descriptor List. */
static U8 SerialProtocolDescListVal[] = {
    SDP_ATTRIB_HEADER_8BIT(12),  /* Data element sequence, 12 bytes */
    SDP_ATTRIB_HEADER_8BIT(3),   /* Data element sequence for L2CAP, 3 bytes */     
    SDP_UUID_16BIT(PROT_L2CAP),  /* Uuid16 L2CAP */
    SDP_ATTRIB_HEADER_8BIT(5),   /* Data element sequence for RFCOMM, 5 bytes */
    SDP_UUID_16BIT(PROT_RFCOMM), /* Uuid16 RFCOMM */
    SDP_UINT_8BIT(4)             /* Uint8 RFCOMM channel number - value will vary */
};

/* Attribute value of Serial Port Language Base ID List */
static const U8 SerialLangBaseVal[] = {
    SDP_ATTRIB_HEADER_8BIT(9),   /* Data element sequence, 9 bytes */
    SDP_UINT_16BIT(0x656E),      /* Uint16 "en" (English) */
    SDP_UINT_16BIT(0x006A),      /* Uint16 UTF-8 encoding */
    SDP_UINT_16BIT(0x0100)       /* Uint16 primary language base ID */
};

/* Attribute value of Serial Port Service Name */
static const U8 SerialServiceName[] = {
    SDP_TEXT_8BIT(6),            /* Null terminated text string, 6 bytes */
    'M','o','d','e','m',0x00     /* Text String */
};

static SdpAttribute SerialSdpAttribs[] = {
    /* Service Class ID List attribute */
    SDP_ATTRIBUTE(AID_SERVICE_CLASS_ID_LIST, SerialServClassIdVal),
    /* Protocol Descriptor List attribute */
    SDP_ATTRIBUTE(AID_PROTOCOL_DESC_LIST, SerialProtocolDescListVal),
    /* Public Browse Group Service */
    SDP_ATTRIBUTE(AID_BROWSE_GROUP_LIST, PublicBrowseGroup), 
    /* Language Base ID List attribute */
    SDP_ATTRIBUTE(AID_LANG_BASE_ID_LIST, SerialLangBaseVal),
    /* Serial Port Profile Service Name */
    SDP_ATTRIBUTE((AID_SERVICE_NAME + 0x0100), SerialServiceName)
};

/*-------------------------------------------------------------------------
 *
 * LAN Access SDP record attributes
 */

/* Attribute value of LAN Service Class Id List */ 
static const U8 LanServClassIdVal[] = {
    SDP_ATTRIB_HEADER_8BIT(17),          /* Data element sequence, 17 bytes */
    SDP_UUID_128BIT(BT_128BIT_UUID(SC_LAN_ACCESS_PPP))   /* Uuid128 LAN Access using PPP */
};

/* Attribute value of LAN Protocol Descriptor List. */
static U8 LanProtocolDescListVal[] = {
    SDP_ATTRIB_HEADER_8BIT(12),  /* Data element sequence, 12 bytes */
    SDP_ATTRIB_HEADER_8BIT(3),   /* Data element sequence for L2CAP, 3 bytes */     
    SDP_UUID_16BIT(PROT_L2CAP),  /* Uuid16 L2CAP */
    SDP_ATTRIB_HEADER_8BIT(5),   /* Data element sequence for RFCOMM, 5 bytes */
    SDP_UUID_16BIT(PROT_RFCOMM), /* Uuid16 RFCOMM */
    SDP_UINT_8BIT(5)             /* Uint8 RFCOMM channel number - value will vary */
};

/* Attribute value of LAN Language Base ID List */
static const U8 LanLangBaseVal[] = {
    SDP_ATTRIB_HEADER_8BIT(9),   /* Data element sequence, 9 bytes */
    SDP_UINT_16BIT(0x656E),      /* Uint16 "en" (English) */
    SDP_UINT_16BIT(0x006A),      /* Uint16 UTF-8 encoding */
    SDP_UINT_16BIT(0x0100)       /* Uint16 primary language base ID */
};

/* Attribute value of LAN Service Availability */
static const U8 LanServiceAvailability[] = {
    SDP_UINT_8BIT(0xFF)          /* Service not in use, accepting new clients 
                                  * This value can vary 
                                  */
};

/* Attribute value of LAN Bluetooth Profile Descriptor List */
static const U8 LanProfileDescListVal[] = {
    SDP_ATTRIB_HEADER_8BIT(8),           /* Data element sequence, 8 bytes */
    SDP_ATTRIB_HEADER_8BIT(6),           /* Data element sequence for ProfileDescriptor, 6 bytes */
    SDP_UUID_16BIT(SC_LAN_ACCESS_PPP),   /* Uuid16 LAN Access using PPP */
    SDP_UINT_16BIT(0x0100)               /* Uint16 version number */
};

/* Attribute value of LAN Service Name */
static const U8 LanServiceName[] = {
    SDP_TEXT_8BIT(21),           /* Null terminated text string, 21 bytes */
    'L','A','N',' ',             /* Text String */
    'A','c','c','e','s','s',' ',
    'u','s','i','n','g',' ',
    'P','P','P',0x00
};

/* Attribute value of LAN Service Description */
static const U8 LanServiceDescription[] = {
    SDP_TEXT_8BIT(21),           /* Null terminated text string, 21 bytes */
    'L','A','N',' ',             /* Text String */
    'A','c','c','e','s','s',' ',
    'u','s','i','n','g',' ',
    'P','P','P',0x00
};

static SdpAttribute LanSdpAttribs[] = {
    /* Service Class ID List attribute */
    SDP_ATTRIBUTE(AID_SERVICE_CLASS_ID_LIST, LanServClassIdVal),
    /* Protocol Descriptor List attribute */
    SDP_ATTRIBUTE(AID_PROTOCOL_DESC_LIST, LanProtocolDescListVal),
    /* Public Browse Group Service */
    SDP_ATTRIBUTE(AID_BROWSE_GROUP_LIST, PublicBrowseGroup), 
    /* Language Base ID List attribute */
    SDP_ATTRIBUTE(AID_LANG_BASE_ID_LIST, LanLangBaseVal),
    /* Service Availability attribute */
    SDP_ATTRIBUTE(AID_SERVICE_AVAILABILITY, LanServiceAvailability),
    /* Bluetooth Profile Descriptor List attribute */
    SDP_ATTRIBUTE(AID_BT_PROFILE_DESC_LIST, LanProfileDescListVal),
    /* Serial Port Profile Service Name */
    SDP_ATTRIBUTE((AID_SERVICE_NAME + 0x0100), LanServiceName),
    /* Service Description attribute */
    SDP_ATTRIBUTE((AID_SERVICE_DESCRIPTION + 0x0100), LanServiceDescription)
};

/*-------------------------------------------------------------------------
 *
 * HEADSET SDP record attributes
 */

/* Attribute value of Headset Service Class Id List */ 
static const U8 HeadsetServClassIdVal[] = {
    SDP_ATTRIB_HEADER_8BIT(34),        /* Data element sequence, 34 bytes */
    /* ServiceClassIdList - most specific to most general class */
    SDP_UUID_128BIT(BT_128BIT_UUID(SC_HEADSET)),       /* Uuid128 Headset */
    SDP_UUID_128BIT(BT_128BIT_UUID(SC_GENERIC_AUDIO))  /* Uuid128 Generic Audio */
};

/* Attribute value of Headset Protocol Descriptor List. */
static U8 HeadsetProtocolDescListVal[] = {
    SDP_ATTRIB_HEADER_8BIT(12),  /* Data element sequence, 12 bytes */
    SDP_ATTRIB_HEADER_8BIT(3),   /* Data element sequence for L2CAP, 3 bytes */     
    SDP_UUID_16BIT(PROT_L2CAP),  /* Uuid16 L2CAP */
    SDP_ATTRIB_HEADER_8BIT(5),   /* Data element sequence for RFCOMM, 5 bytes */
    SDP_UUID_16BIT(PROT_RFCOMM), /* Uuid16 RFCOMM */
    SDP_UINT_8BIT(6)             /* Uint8 RFCOMM channel number - value will vary */
};

/* Attribute value of Headset Language Base ID List */
static const U8 HeadsetLangBaseVal[] = {
    SDP_ATTRIB_HEADER_8BIT(9),   /* Data element sequence, 9 bytes */
    SDP_UINT_16BIT(0x656E),      /* Uint16 "en" (English) */
    SDP_UINT_16BIT(0x006A),      /* Uint16 UTF-8 encoding */
    SDP_UINT_16BIT(0x0100)       /* Uint16 primary language base ID */
};

/* Attribute value of Headset Bluetooth Profile Descriptor List */
static const U8 HeadsetProfileDescListVal[] = {
    SDP_ATTRIB_HEADER_8BIT(8),           /* Data element sequence, 8 bytes */
    SDP_ATTRIB_HEADER_8BIT(6),           /* Data element sequence for ProfileDescriptor, 6 bytes */
    SDP_UUID_16BIT(SC_HEADSET),          /* Uuid16 Headset */
    SDP_UINT_16BIT(0x0100)               /* Uint16 version number */
};

/* Attribute value of Headset Service Name */
static const U8 HeadsetServiceName[] = {
    SDP_TEXT_8BIT(8),           /* Null terminated text string, 8 bytes */
    'H','e','a','d','s','e','t',0x00
};

/* Attribute value of Remote Audio Control */
static const U8 HeadsetRemoteAudioVolControl[] = {
    SDP_BOOL(0)                  /* No Remote Audio Control */
};

static SdpAttribute HeadsetSdpAttribs[] = {
    /* Service Class ID List attribute */
    SDP_ATTRIBUTE(AID_SERVICE_CLASS_ID_LIST, HeadsetServClassIdVal), 
    /* Protocol Descriptor List attribute */
    SDP_ATTRIBUTE(AID_PROTOCOL_DESC_LIST, HeadsetProtocolDescListVal),
    /* Public Browse Group Service */
    SDP_ATTRIBUTE(AID_BROWSE_GROUP_LIST, PublicBrowseGroup), 
    /* Language base Id List attribute */
    SDP_ATTRIBUTE(AID_LANG_BASE_ID_LIST, HeadsetLangBaseVal),
    /* Bluetooth Profile Descriptor List attribute */
    SDP_ATTRIBUTE(AID_BT_PROFILE_DESC_LIST, HeadsetProfileDescListVal),
    /* Service Name attribute */
    SDP_ATTRIBUTE((AID_SERVICE_NAME + 0x0100), HeadsetServiceName),
    /* Remote Audio Volume Control */
    SDP_ATTRIBUTE(AID_REMOTE_AUDIO_VOL_CONTROL, HeadsetRemoteAudioVolControl)
};

/*-------------------------------------------------------------------------
 *
 * HEADSET GATEWAY SDP record attributes
 */

/* Attribute value of Headset Gateway Service Class Id List */ 
static const U8 HeadsetGWServClassIdVal[] = {
    SDP_ATTRIB_HEADER_8BIT(34),                /* Data element sequence, 34 bytes */
    /* ServiceClassIdList - most specific to most general class */
    SDP_UUID_128BIT(BT_128BIT_UUID(SC_HEADSET_AUDIO_GATEWAY)), /* Uuid128 Headset Audio Gateway */
    SDP_UUID_128BIT(BT_128BIT_UUID(SC_GENERIC_AUDIO))          /* Uuid128 Generic Audio */
};

/* Attribute value of Headset Gateway Protocol Descriptor List. */
static U8 HeadsetGWProtocolDescListVal[] = {
    SDP_ATTRIB_HEADER_8BIT(12),  /* Data element sequence, 12 bytes */
    SDP_ATTRIB_HEADER_8BIT(3),   /* Data element sequence for L2CAP, 3 bytes */     
    SDP_UUID_16BIT(PROT_L2CAP),  /* Uuid16 L2CAP */
    SDP_ATTRIB_HEADER_8BIT(5),   /* Data element sequence for RFCOMM, 5 bytes */
    SDP_UUID_16BIT(PROT_RFCOMM), /* Uuid16 RFCOMM */
    SDP_UINT_8BIT(7)             /* Uint8 RFCOMM channel number - value will vary */
};

/* Attribute value of Headset Gateway Language Base ID List */
static const U8 HeadsetGWLangBaseVal[] = {
    SDP_ATTRIB_HEADER_8BIT(9),   /* Data element sequence, 9 bytes */
    SDP_UINT_16BIT(0x656E),      /* Uint16 "en" (English) */
    SDP_UINT_16BIT(0x006A),      /* Uint16 UTF-8 encoding */
    SDP_UINT_16BIT(0x0100)       /* Uint16 primary language base ID */
};

/* Attribute value of Headset Gateway Bluetooth Profile Descriptor List */
static const U8 HeadsetGWProfileDescListVal[] = {
    SDP_ATTRIB_HEADER_8BIT(8),           /* Data element sequence, 8 bytes */
    SDP_ATTRIB_HEADER_8BIT(6),           /* Data element sequence for ProfileDescriptor, 6 bytes */
    SDP_UUID_16BIT(SC_HEADSET),          /* Uuid16 Headset */
    SDP_UINT_16BIT(0x0100)               /* Uint16 version number */
};

/* Attribute value of Headset Gateway Service Name */
static const U8 HeadsetGWServiceName[] = {
    SDP_TEXT_8BIT(14),           /* Null terminated text string, 14 bytes */
    'V','o','i','c','e',' ',
    'G','a','t','e','w','a','y',0x00
};

static SdpAttribute HeadsetGWSdpAttribs[] = {
    /* Service Class ID List attribute */
    SDP_ATTRIBUTE(AID_SERVICE_CLASS_ID_LIST, HeadsetGWServClassIdVal), 
    /* Protocol Descriptor List attribute */
    SDP_ATTRIBUTE(AID_PROTOCOL_DESC_LIST, HeadsetGWProtocolDescListVal),
    /* Public Browse Group Service */
    SDP_ATTRIBUTE(AID_BROWSE_GROUP_LIST, PublicBrowseGroup), 
    /* Language base Id List attribute */
    SDP_ATTRIBUTE(AID_LANG_BASE_ID_LIST, HeadsetGWLangBaseVal),
    /* Bluetooth Profile Descriptor List attribute */
    SDP_ATTRIBUTE(AID_BT_PROFILE_DESC_LIST, HeadsetGWProfileDescListVal),
    /* Service Name attribute */
    SDP_ATTRIBUTE((AID_SERVICE_NAME + 0x0100), HeadsetGWServiceName),
};

/*-------------------------------------------------------------------------
 *
 * INTERCOM SDP record attributes
 */

/* Attribute value of Intercom Service Class Id List */ 
static const U8 IntercomServClassIdVal[] = {
    SDP_ATTRIB_HEADER_8BIT(34),             /* Data element sequence, 34 bytes */
    /* ServiceClassIdList - most specific to most general class */
    SDP_UUID_128BIT(BT_128BIT_UUID(SC_INTERCOM)),           /* Uuid128 Intercom */
    SDP_UUID_128BIT(BT_128BIT_UUID(SC_GENERIC_TELEPHONY))   /* Uuid128 Generic Telephony */
};

/* Attribute value of Intercom Protocol Descriptor List. */
static const U8 IntercomProtocolDescListVal[] = {
    SDP_ATTRIB_HEADER_8BIT(10),  /* Data element sequence, 10 bytes */
    SDP_ATTRIB_HEADER_8BIT(3),   /* Data element sequence for L2CAP, 3 bytes */     
    SDP_UUID_16BIT(PROT_L2CAP),  /* Uuid16 L2CAP */
    SDP_ATTRIB_HEADER_8BIT(3),   /* Data element sequence for TCS, 3 bytes */
    SDP_UUID_16BIT(PROT_TCS_BIN) /* Uuid16 TCS Binary */
};

/* Attribute value of Intercom Language Base ID List */
static const U8 IntercomLangBaseVal[] = {
    SDP_ATTRIB_HEADER_8BIT(9),   /* Data element sequence, 9 bytes */
    SDP_UINT_16BIT(0x656E),      /* Uint16 "en" (English) */
    SDP_UINT_16BIT(0x006A),      /* Uint16 UTF-8 encoding */
    SDP_UINT_16BIT(0x0100)       /* Uint16 primary language base ID */
};

/* Attribute value of Intercom Bluetooth Profile Descriptor List */
static const U8 IntercomProfileDescListVal[] = {
    SDP_ATTRIB_HEADER_8BIT(8),   /* Data element sequence, 8 bytes */
    SDP_ATTRIB_HEADER_8BIT(6),   /* Data element sequence for ProfileDescriptor, 6 bytes */
    SDP_UUID_16BIT(SC_INTERCOM), /* Uuid16 Intercom */
    SDP_UINT_16BIT(0x0100)       /* Uint16 version number */
};

/* Attribute value of Intercom Service Name */
static const U8 IntercomServiceName[] = {
    SDP_TEXT_8BIT(9),           /* Null terminated text string, 9 bytes */
    'I','n','t','e','r','c','o','m',0x00
};

static SdpAttribute IntercomSdpAttribs[] = {
    /* Service Class ID List attribute */
    SDP_ATTRIBUTE(AID_SERVICE_CLASS_ID_LIST, IntercomServClassIdVal), 
    /* Protocol Descriptor List attribute */
    SDP_ATTRIBUTE(AID_PROTOCOL_DESC_LIST, IntercomProtocolDescListVal),
    /* Public Browse Group Service */
    SDP_ATTRIBUTE(AID_BROWSE_GROUP_LIST, PublicBrowseGroup), 
    /* Language base Id List attribute */
    SDP_ATTRIBUTE(AID_LANG_BASE_ID_LIST, IntercomLangBaseVal),
    /* Bluetooth Profile Descriptor List attribute */
    SDP_ATTRIBUTE(AID_BT_PROFILE_DESC_LIST, IntercomProfileDescListVal),
    /* Service Name attribute */
    SDP_ATTRIBUTE((AID_SERVICE_NAME + 0x0100), IntercomServiceName)
};

/*-------------------------------------------------------------------------
 *
 * CORDLESS TELEPHONY SDP record attributes
 */

/* Attribute value of Cordless Telephony Service Class Id List */ 
static const U8 ClServClassIdVal[] = {
    SDP_ATTRIB_HEADER_8BIT(34),             /* Data element sequence, 34 bytes */
    /* ServiceClassIdList - most specific to most general class */
    SDP_UUID_128BIT(BT_128BIT_UUID(SC_CORDLESS_TELEPHONY)), /* Uuid128 Cordless Telephony */
    SDP_UUID_128BIT(BT_128BIT_UUID(SC_GENERIC_TELEPHONY))   /* Uuid128 Generic Telephony */
};

/* Attribute value of Cordless Telephony Protocol Descriptor List. */
static const U8 ClProtocolDescListVal[] = {
    SDP_ATTRIB_HEADER_8BIT(10),  /* Data element sequence, 10 bytes */
    SDP_ATTRIB_HEADER_8BIT(3),   /* Data element sequence for L2CAP, 3 bytes */     
    SDP_UUID_16BIT(PROT_L2CAP),  /* Uuid16 L2CAP */
    SDP_ATTRIB_HEADER_8BIT(3),   /* Data element sequence for TCS, 3 bytes */
    SDP_UUID_16BIT(PROT_TCS_BIN) /* Uuid16 TCS Binary */
};

/* Attribute value of Cordless Telephony Language Base ID List */
static const U8 ClLangBaseVal[] = {
    SDP_ATTRIB_HEADER_8BIT(9),   /* Data element sequence, 9 bytes */
    SDP_UINT_16BIT(0x656E),      /* Uint16 "en" (English) */
    SDP_UINT_16BIT(0x006A),      /* Uint16 UTF-8 encoding */
    SDP_UINT_16BIT(0x0100)       /* Uint16 primary language base ID */
};

/* Attribute value of Cordless Telephony Bluetooth Profile Descriptor List */
static const U8 ClProfileDescListVal[] = {
    SDP_ATTRIB_HEADER_8BIT(8),             /* Data element sequence, 8 bytes */
    SDP_ATTRIB_HEADER_8BIT(6),             /* Data element sequence for ProfileDescriptor, 6 bytes */
    SDP_UUID_16BIT(SC_CORDLESS_TELEPHONY), /* Uuid16 Cordless Telephony */
    SDP_UINT_16BIT(0x0100)                 /* Uint16 version number */
};

/* Attribute value of Cordless Telephony Service Name */
static const U8 ClServiceName[] = {
    SDP_TEXT_8BIT(20),           /* Null terminated text string, 20 bytes */
    'C','o','r','d','l','e','s','s','s',' ',
    'T','e','l','e','p','h','o','n','y',0x00
};

/* Attribute value of Cordless Telephony External Network */
static const U8 ClExternalNetwork[] = {
    SDP_UINT_8BIT(0x03)                  /* Uint8 GSM based cordless phone */
};

static SdpAttribute CordlessSdpAttribs[] = {
    /* Service Class ID List attribute */
    SDP_ATTRIBUTE(AID_SERVICE_CLASS_ID_LIST, ClServClassIdVal), 
    /* Protocol Descriptor List attribute */
    SDP_ATTRIBUTE(AID_PROTOCOL_DESC_LIST, ClProtocolDescListVal),
    /* Public Browse Group Service */
    SDP_ATTRIBUTE(AID_BROWSE_GROUP_LIST, PublicBrowseGroup), 
    /* Language base Id List attribute */
    SDP_ATTRIBUTE(AID_LANG_BASE_ID_LIST, ClLangBaseVal),
    /* Bluetooth Profile Descriptor List attribute */
    SDP_ATTRIBUTE(AID_BT_PROFILE_DESC_LIST, ClProfileDescListVal),
    /* Service Name attribute */
    SDP_ATTRIBUTE((AID_SERVICE_NAME + 0x0100), ClServiceName),
    /* External Network attribute */
    SDP_ATTRIBUTE(AID_EXTERNAL_NETWORK, ClExternalNetwork)
};

/*-------------------------------------------------------------------------
 *
 * FILE TRANSFER SDP record attributes
 */

/* Attribute value of File Transfer Service Class Id List */
static const U8 FtpServClassIdVal[] = {
    SDP_ATTRIB_HEADER_8BIT(17),             /* Data element sequence, 17 bytes */
    SDP_UUID_128BIT(BT_128BIT_UUID(SC_OBEX_FILE_TRANSFER))  /* Uuid128 Obex File Transfer */
};

/* Attribute value of File Transfer Protocol Descriptor List */
static U8 FtpProtocolDescListVal[] = {
    SDP_ATTRIB_HEADER_8BIT(17),  /* Data element sequence, 17 bytes */
    SDP_ATTRIB_HEADER_8BIT(3),   /* Data element sequence for L2CAP, 3 bytes */     
    SDP_UUID_16BIT(PROT_L2CAP),  /* Uuid16 L2CAP */
    SDP_ATTRIB_HEADER_8BIT(5),   /* Data element sequence for RFCOMM, 5 bytes */
    SDP_UUID_16BIT(PROT_RFCOMM), /* Uuid16 RFCOMM */
    SDP_UINT_8BIT(8),            /* Uint8 RFCOMM channel number - value will vary */
    SDP_ATTRIB_HEADER_8BIT(3),   /* Data element sequence for OBEX, 3 bytes */     
    SDP_UUID_16BIT(PROT_OBEX)    /* Uuid16 OBEX */
};

/* Attribute value of File Transfer Language Base ID List */
static const U8 FtpLangBaseVal[] = {
    SDP_ATTRIB_HEADER_8BIT(9),   /* Data element sequence, 9 bytes */
    SDP_UINT_16BIT(0x656E),      /* Uint16 "en" (English) */
    SDP_UINT_16BIT(0x006A),      /* Uint16 UTF-8 encoding */
    SDP_UINT_16BIT(0x0100)       /* Uint16 primary language base ID */
};

/* Attribute value of File Transfer Bluetooth Profile Descriptor List */
static const U8 FtpProfileDescListVal[] = {
    SDP_ATTRIB_HEADER_8BIT(8),             /* Data element sequence, 8 bytes */
    SDP_ATTRIB_HEADER_8BIT(6),             /* Data element sequence for ProfileDescriptor, 6 bytes */
    SDP_UUID_16BIT(SC_OBEX_FILE_TRANSFER), /* Uuid16 Obex File Transfer */
    SDP_UINT_16BIT(0x0100)                 /* Uint16 version number */
};

/* Attribute value of File Transfer Service Name */
static const U8 FtpServiceName[] = {
    SDP_TEXT_8BIT(19),           /* Null terminated text string, 19 bytes */
    'O','B','E','X',' ','F','i','l','e',' ',  
    'T','r','a','n','s','f','e','r',0x00  
};

static SdpAttribute FileTransferSdpAttribs[] = {
    /* Service Class ID List attribute */
    SDP_ATTRIBUTE(AID_SERVICE_CLASS_ID_LIST, FtpServClassIdVal), 
    /* Protocol Descriptor List attribute */
    SDP_ATTRIBUTE(AID_PROTOCOL_DESC_LIST, FtpProtocolDescListVal),
    /* Public Browse Group Service */
    SDP_ATTRIBUTE(AID_BROWSE_GROUP_LIST, PublicBrowseGroup), 
    /* Language base Id List attribute */
    SDP_ATTRIBUTE(AID_LANG_BASE_ID_LIST, FtpLangBaseVal),
    /* Bluetooth Profile Descriptor List attribute */
    SDP_ATTRIBUTE(AID_BT_PROFILE_DESC_LIST, FtpProfileDescListVal),
    /* Service Name attribute */
    SDP_ATTRIBUTE((AID_SERVICE_NAME + 0x0100), FtpServiceName)
};

/*-------------------------------------------------------------------------
 *
 * SYNCHRONIZATION SERVER SDP record attributes
 */

/* Attribute value of Synchronization Server Service Class Id List */
static const U8 SyncServClassIdVal[] = {
    SDP_ATTRIB_HEADER_8BIT(17),    /* Data element sequence, 17 bytes */
    SDP_UUID_128BIT(BT_128BIT_UUID(SC_IRMC_SYNC))  /* Uuid128 IrMC Synchronization */
};

/* Attribute value of Synchronization Server Protocol Descriptor List */
static U8 SyncProtocolDescListVal[] = {
    SDP_ATTRIB_HEADER_8BIT(17),  /* Data element sequence, 17 bytes */
    SDP_ATTRIB_HEADER_8BIT(3),   /* Data element sequence for L2CAP, 3 bytes */     
    SDP_UUID_16BIT(PROT_L2CAP),  /* Uuid16 L2CAP */
    SDP_ATTRIB_HEADER_8BIT(5),   /* Data element sequence for RFCOMM, 5 bytes */
    SDP_UUID_16BIT(PROT_RFCOMM), /* Uuid16 RFCOMM */
    SDP_UINT_8BIT(9),            /* Uint8 RFCOMM channel number - value will vary */
    SDP_ATTRIB_HEADER_8BIT(3),   /* Data element sequence for OBEX, 3 bytes */     
    SDP_UUID_16BIT(PROT_OBEX)    /* Uuid16 OBEX */
};

/* Attribute value of Synchronization Server Language Base ID List */
static const U8 SyncLangBaseVal[] = {
    SDP_ATTRIB_HEADER_8BIT(9),   /* Data element sequence, 9 bytes */
    SDP_UINT_16BIT(0x656E),      /* Uint16 "en" (English) */
    SDP_UINT_16BIT(0x006A),      /* Uint16 UTF-8 encoding */
    SDP_UINT_16BIT(0x0100)       /* Uint16 primary language base ID */
};

/* Attribute value of Synchronization Server Bluetooth Profile Descriptor List */
static const U8 SyncProfileDescListVal[] = {
    SDP_ATTRIB_HEADER_8BIT(8),    /* Data element sequence, 8 bytes */
    SDP_ATTRIB_HEADER_8BIT(6),    /* Data element sequence for ProfileDescriptor, 6 bytes */
    SDP_UUID_16BIT(SC_IRMC_SYNC), /* Uuid16 IrMC Synchronization */
    SDP_UINT_16BIT(0x0100)        /* Uint16 version number */
};

/* Attribute value of Synchronization Server Service Name */
static const U8 SyncServiceName[] = {
    SDP_TEXT_8BIT(21),           /* Null terminated text string, 21 bytes */
    'I','r','M','C',' ','S','y','n','c','h',
    'r','o','n','i','z','a','t','i','o','n',0x00  
};

/* Attribute value of Synchronization Server Supported Data Stores List */
static const U8 SyncSupportedDataStores[] = {
    SDP_ATTRIB_HEADER_8BIT(8),   /* Data element sequence, 8 bytes */
    SDP_UINT_8BIT(0x01),         /* Phonebook entries */
    SDP_UINT_8BIT(0x03),         /* Calendar entries */
    SDP_UINT_8BIT(0x05),         /* Notes entries */
    SDP_UINT_8BIT(0x06)          /* Messages entries */
                            
};

static SdpAttribute SyncSdpAttribs[] = {
    /* Service Class ID List attribute */
    SDP_ATTRIBUTE(AID_SERVICE_CLASS_ID_LIST, SyncServClassIdVal), 
    /* Protocol Descriptor List attribute */
    SDP_ATTRIBUTE(AID_PROTOCOL_DESC_LIST, SyncProtocolDescListVal),
    /* Public Browse Group Service */
    SDP_ATTRIBUTE(AID_BROWSE_GROUP_LIST, PublicBrowseGroup), 
    /* Language base Id List attribute */
    SDP_ATTRIBUTE(AID_LANG_BASE_ID_LIST, SyncLangBaseVal),
    /* Bluetooth Profile Descriptor List attribute */
    SDP_ATTRIBUTE(AID_BT_PROFILE_DESC_LIST, SyncProfileDescListVal),
    /* Service Name attribute */
    SDP_ATTRIBUTE((AID_SERVICE_NAME + 0x0100), SyncServiceName),
    /* Supported Data Stores List */
    SDP_ATTRIBUTE(AID_SUPPORTED_DATA_STORES_LIST, SyncSupportedDataStores)
};

/*-------------------------------------------------------------------------
 *
 * SYNCHRONIZATION COMMAND SERVICE SDP record attributes
 */

/* Attribute value of Synchronization Command Service Class Id List */
static const U8 SyncCommandServClassIdVal[] = {
    SDP_ATTRIB_HEADER_8BIT(17),            /* Data element sequence, 17 bytes */
    SDP_UUID_128BIT(BT_128BIT_UUID(SC_IRMC_SYNC_COMMAND))  /* Uuid128 IrMC Synchronization Command */
};

/* Attribute value of Synchronization Command Protocol Descriptor List */
static U8 SyncCommandProtocolDescListVal[] = {
    SDP_ATTRIB_HEADER_8BIT(17),  /* Data element sequence, 17 bytes */
    SDP_ATTRIB_HEADER_8BIT(3),   /* Data element sequence for L2CAP, 3 bytes */     
    SDP_UUID_16BIT(PROT_L2CAP),  /* Uuid16 L2CAP */
    SDP_ATTRIB_HEADER_8BIT(5),   /* Data element sequence for RFCOMM, 5 bytes */
    SDP_UUID_16BIT(PROT_RFCOMM), /* Uuid16 RFCOMM */
    SDP_UINT_8BIT(10),           /* Uint8 RFCOMM channel number - value will vary */
    SDP_ATTRIB_HEADER_8BIT(3),   /* Data element sequence for OBEX, 3 bytes */     
    SDP_UUID_16BIT(PROT_OBEX)    /* Uuid16 OBEX */
};

/* Attribute value of Synchronization Command Language Base ID List */
static const U8 SyncCommandLangBaseVal[] = {
    SDP_ATTRIB_HEADER_8BIT(9),   /* Data element sequence, 9 bytes */
    SDP_UINT_16BIT(0x656E),      /* Uint16 "en" (English) */
    SDP_UINT_16BIT(0x006A),      /* Uint16 UTF-8 encoding */
    SDP_UINT_16BIT(0x0100)       /* Uint16 primary language base ID */
};

/* Attribute value of Synchronization Command Bluetooth Profile Descriptor List */
static const U8 SyncCommandProfileDescListVal[] = {
    SDP_ATTRIB_HEADER_8BIT(8),    /* Data element sequence, 8 bytes */
    SDP_ATTRIB_HEADER_8BIT(6),    /* Data element sequence for ProfileDescriptor, 6 bytes */
    SDP_UUID_16BIT(SC_IRMC_SYNC), /* Uuid16 IrMC Synchronization */
    SDP_UINT_16BIT(0x0100)        /* Uint16 version number */
};

/* Attribute value of Synchronization Command Service Name */
static const U8 SyncCommandServiceName[] = {
    SDP_TEXT_8BIT(21),           /* Null terminated text string, 21 bytes */
    'S','y','n','c',' ',
    'C','o','m','m','a','n','d',' ',
    'S','e','r','v','i','c','e',0x00  
};

static SdpAttribute SyncCommandSdpAttribs[] = {
    /* Service Class ID List attribute */
    SDP_ATTRIBUTE(AID_SERVICE_CLASS_ID_LIST, SyncCommandServClassIdVal), 
    /* Protocol Descriptor List attribute */
    SDP_ATTRIBUTE(AID_PROTOCOL_DESC_LIST, SyncCommandProtocolDescListVal),
    /* Public Browse Group Service */
    SDP_ATTRIBUTE(AID_BROWSE_GROUP_LIST, PublicBrowseGroup), 
    /* Language base Id List attribute */
    SDP_ATTRIBUTE(AID_LANG_BASE_ID_LIST, SyncCommandLangBaseVal),
    /* Bluetooth Profile Descriptor List attribute */
    SDP_ATTRIBUTE(AID_BT_PROFILE_DESC_LIST, SyncCommandProfileDescListVal),
    /* Service Name attribute */
    SDP_ATTRIBUTE((AID_SERVICE_NAME + 0x0100), SyncCommandServiceName)
};

/*-------------------------------------------------------------------------
 *
 * Generic SDP query info 
 */

/* Service Search Attribute Request */
static U8 UuidServiceSearchAttribReq16[] = {
    /* First parameter is the search pattern in data element format. It
     * is a list of UUIDs. In this case it is a single UUID
     */
    SDP_ATTRIB_HEADER_8BIT(3),           /* Data Element sequence, 3 bytes */
    SDP_UUID_16BIT(SC_OBEX_OBJECT_PUSH), /* Uuid16 Default UUID value */

    /* The second parameter is the maximum number of bytes that can be
     * be received for the attribute list
     */
    0x02,0xA0,                   /* Max number of bytes for attribute is 672 */

    /* The third parameter is the actual attribute ID list which is a data
     * element sequence.
     */
    SDP_ATTRIB_HEADER_8BIT(5),   /* Data element sequence, 5 bytes */
    SDP_UINT_32BIT(0x0000FFFF),  /* Attribute range = all attributes */
};

static U8 UuidServiceSearchAttribReq32[] = {
    /* First parameter is the search pattern in data element format. It
     * is a list of UUIDs. In this case it is a single UUID
     */
    SDP_ATTRIB_HEADER_8BIT(5),           /* Data Element sequence, 5 bytes */
    SDP_UUID_32BIT(SC_OBEX_OBJECT_PUSH), /* Uuid32 Default UUID value */

    /* The second parameter is the maximum number of bytes that can be
     * be received for the attribute list
     */
    0x02,0xA0,                   /* Max number of bytes for attribute is 672 */

    /* The third parameter is the actual attribute ID list which is a data
     * element sequence.
     */
    SDP_ATTRIB_HEADER_8BIT(5),   /* Data element sequence, 5 bytes */
    SDP_UINT_32BIT(0x0000FFFF),  /* Attribute range = all attributes */
};

static U8 UuidServiceSearchAttribReq128[] = {
    /* First parameter is the search pattern in data element format. It
     * is a list of UUIDs. In this case it is a single UUID
     */
    SDP_ATTRIB_HEADER_8BIT(17),           /* Data Element sequence, 17 bytes */
    SDP_UUID_128BIT(BT_128BIT_UUID(SC_OBEX_OBJECT_PUSH)), /* Uuid128 Default UUID value */

    /* The second parameter is the maximum number of bytes that can be
     * be received for the attribute list
     */
    0x02,0xA0,                   /* Max number of bytes for attribute is 672 */

    /* The third parameter is the actual attribute ID list which is a data
     * element sequence.
     */
    SDP_ATTRIB_HEADER_8BIT(5),   /* Data element sequence, 5 bytes */
    SDP_UINT_32BIT(0x0000FFFF),  /* Attribute range = all attributes */
};

const U8 SerialName[] = {
    'M','o','d','e','m', 0x00
};

const U8 LanName[] = {
    'L','A','N',' ',     
    'A','c','c','e','s','s',' ',
    'u','s','i','n','g',' ',
    'P','P','P'
};

const U8 DialupName[] = {
    'D','i','a','l','-','u','p',' ',
    'n','e','t','w','o','r','k','i','n','g', 0x00
};

const U8 FaxName[] = {
    'F','a','x', 0x00
};

const U8 ObexOpName[] = {
    'O','B','E','X',' ', 
    'O','b','j','e','c','t',' ',
    'P','u','s','h', 0x00
};

const U8 HeadsetName[] = {
    'H','e','a','d','s','e','t',0x00
};

const U8 HeadsetGWName[] = {
    'V','o','i','c','e',' ','G','a','t','e','w','a','y',0x00
};

const U8 IntercomName[] = {
    'I','n','t','e','r','c','o','m',0x00
};

const U8 CordlessName[] = {
    'C','o','r','d','l','e','s','s','s',' ',
    'T','e','l','e','p','h','o','n','y',0x00
};

const U8 FileTransferName[] = {
    'O','B','E','X',' ','F','i','l','e',' ',  
    'T','r','a','n','s','f','e','r',0x00  
};

const U8 SyncName[] = {
    'I','r','M','C',' ','S','y','n','c','h',  
    'r','o','n','i','z','a','t','i','o','n',0x00  
};

const U8 SyncCommandName[] = {
    'S','y','n','c',' ',
    'C','o','m','m','a','n','d',' ',
    'S','e','r','v','i','c','e',0x00  
};
/****************************************************************************
 *
 * Function prototypes
 *
 ****************************************************************************/
extern void App_Report(char *format,...);

void InitializeSdpServer(void);
void InitializeSdpClient(void);
void DeinitializeSdpServer(void);
void DeinitializeSdpClient(void);
void SdpEventHandler(const BtEvent* event);

BtStatus VerifySdpObjectPush(SdpQueryToken* token);
BtStatus VerifySdpSerialPort(SdpQueryToken* token);
BtStatus VerifySdpFax(SdpQueryToken* token);
BtStatus VerifySdpDun(SdpQueryToken* token);
BtStatus VerifySdpLan(SdpQueryToken* token);
BtStatus VerifySdpHeadset(SdpQueryToken* token);
BtStatus VerifySdpHeadsetGW(SdpQueryToken* token);
BtStatus VerifySdpIntercom(SdpQueryToken* token);
BtStatus VerifySdpCordless(SdpQueryToken* token);
BtStatus VerifySdpFileTransfer(SdpQueryToken* token);
BtStatus VerifySdpSync(SdpQueryToken* token);
BtStatus VerifySdpSyncCommand(SdpQueryToken* token);

BtStatus SendSdpUuidQuery(SdpQueryMode mode, U8 uuidSize);

/****************************************************************************
 *
 * Functions
 *
 ****************************************************************************/

/*---------------------------------------------------------------------------
 *            InitializeSdpClient()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initialize the Sdp client portion of the application. 
 *
 * Return:    void 
 */
void InitializeSdpClient(void)
{
    BtStatus status;

    /* Setup a couple of handlers */
    ME_InitHandler(&btHandler);

    btHandler.callback = SdpEventHandler;

    status = ME_RegisterGlobalHandler(&btHandler);
    Assert(status == BT_STATUS_SUCCESS);
    ME_SetEventMask(&btHandler, BEM_ALL_EVENTS);

    remDev = 0;

    /* Initialize the queryToken */
    queryToken.callback = SdpEventHandler;
    queryToken.attribId = AID_SERVICE_CLASS_ID_LIST;
}

/*---------------------------------------------------------------------------
 *            InitializeSdpServer()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initialize the Sdp server portion of the application. 
 *
 * Return:    void 
 */
void InitializeSdpServer(void)
{
    BtStatus         status;

    /* Add an entries to the SDP server */
    DialupSdpRecord.attribs = DialupSdpAttribs;
    DialupSdpRecord.num = 7;   /* Number of attributes */
    DialupSdpRecord.classOfDevice = COD_TELEPHONY | COD_NETWORKING; 
    /* Use ME_SetClassOfDevice to set major class - PHONE */

    FaxSdpRecord.attribs = FaxSdpAttribs;
    FaxSdpRecord.num = 10;   /* Number of attributes */
    FaxSdpRecord.classOfDevice = COD_TELEPHONY; 
    /* Use ME_SetClassOfDevice to set major class - PHONE */

    ObexSdpRecord.attribs = ObexOpSdpAttribs;
    ObexSdpRecord.num = 7;   /* Number of attributes */
    ObexSdpRecord.classOfDevice = COD_OBJECT_TRANSFER; 
    /* No major/minor class needed */

    SerialSdpRecord.attribs = SerialSdpAttribs;
    SerialSdpRecord.num = 5;   /* Number of attributes */
    /* No Class Of Device for the Serial Port Profile */

    LanSdpRecord.attribs = LanSdpAttribs;
    LanSdpRecord.num = 8;   /* Number of attributes */
    LanSdpRecord.classOfDevice = COD_NETWORKING; 
    /* Use ME_SetClassOfDevice to set major class - LAN Access Point */

    HeadsetSdpRecord.attribs = HeadsetSdpAttribs;
    HeadsetSdpRecord.num = 7; /* Number of attributes */
    HeadsetSdpRecord.classOfDevice = COD_AUDIO; 
    /* Use ME_SetClassOfDevice to set major/minor classes - Audio major class, 
     * Headset minor class 
     */

    HeadsetGWSdpRecord.attribs = HeadsetGWSdpAttribs;
    HeadsetGWSdpRecord.num = 6; /* Number of attributes */
    HeadsetGWSdpRecord.classOfDevice = COD_AUDIO; 
    /* Use ME_SetClassOfDevice to set major/minor classes - Audio major class, 
     * Headset minor class 
     */

    IntercomSdpRecord.attribs = IntercomSdpAttribs;
    IntercomSdpRecord.num = 6; /* Number of attributes */
    IntercomSdpRecord.classOfDevice = COD_TELEPHONY; 
    /* Use ME_SetClassOfDevice to set major class - PHONE */

    CordlessSdpRecord.attribs = CordlessSdpAttribs;
    CordlessSdpRecord.num = 7; /* Number of attributes */
    CordlessSdpRecord.classOfDevice = COD_TELEPHONY; 
    /* Use ME_SetClassOfDevice to set major class - PHONE */

    FileTransferSdpRecord.attribs = FileTransferSdpAttribs;
    FileTransferSdpRecord.num = 6; /* Number of attributes */
    FileTransferSdpRecord.classOfDevice = COD_OBJECT_TRANSFER; 
    /* No major/minor class needed */

    SyncSdpRecord.attribs = SyncSdpAttribs;
    SyncSdpRecord.num = 7; /* Number of attributes */
    SyncSdpRecord.classOfDevice = COD_OBJECT_TRANSFER; 
    /* No major/minor class needed */

    SyncCommandSdpRecord.attribs = SyncCommandSdpAttribs;
    SyncCommandSdpRecord.num = 6; /* Number of attributes */
    SyncCommandSdpRecord.classOfDevice = COD_OBJECT_TRANSFER; 
    /* No major/minor class needed */

    /* Add the Profile SDP records */
    status = SDP_AddRecord(&DialupSdpRecord);
    Assert(status == BT_STATUS_SUCCESS);
    status = SDP_AddRecord(&FaxSdpRecord);
    Assert(status == BT_STATUS_SUCCESS);
    status = SDP_AddRecord(&ObexSdpRecord);
    Assert(status == BT_STATUS_SUCCESS);
    status = SDP_AddRecord(&SerialSdpRecord);
    Assert(status == BT_STATUS_SUCCESS);
    status = SDP_AddRecord(&LanSdpRecord);
    Assert(status == BT_STATUS_SUCCESS);
    status = SDP_AddRecord(&HeadsetSdpRecord);
    Assert(status == BT_STATUS_SUCCESS);
    status = SDP_AddRecord(&HeadsetGWSdpRecord);
    Assert(status == BT_STATUS_SUCCESS);
    status = SDP_AddRecord(&IntercomSdpRecord);
    Assert(status == BT_STATUS_SUCCESS);
    status = SDP_AddRecord(&CordlessSdpRecord);
    Assert(status == BT_STATUS_SUCCESS);
    status = SDP_AddRecord(&FileTransferSdpRecord);
    Assert(status == BT_STATUS_SUCCESS);
    status = SDP_AddRecord(&SyncSdpRecord);
    Assert(status == BT_STATUS_SUCCESS);
    status = SDP_AddRecord(&SyncCommandSdpRecord);
    Assert(status == BT_STATUS_SUCCESS);

}

/*---------------------------------------------------------------------------
 *            DeinitializeSdpClient()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Deinitialize the Sdp client portion of the application. 
 *
 * Return:    void 
 */
void DeinitializeSdpClient(void)
{
    BtStatus status;

    /* Unregister the global handlers */
    status = ME_UnregisterGlobalHandler(&btHandler);
    Assert(status == BT_STATUS_SUCCESS);
}

/*---------------------------------------------------------------------------
 *            DeinitializeSdpServer()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Deinitialize the Sdp server portion of the application. 
 *
 * Return:    void 
 */
void DeinitializeSdpServer(void)
{
    BtStatus         status;

    /* Remove the three SDP entries from the SDP server - 
     * DialupNetworking, Fax, and Object Push 
     */
    status = SDP_RemoveRecord(&DialupSdpRecord);
    status = SDP_RemoveRecord(&FaxSdpRecord);
    status = SDP_RemoveRecord(&ObexSdpRecord);
    status = SDP_RemoveRecord(&SerialSdpRecord);
    status = SDP_RemoveRecord(&LanSdpRecord);
    status = SDP_RemoveRecord(&HeadsetSdpRecord);
    status = SDP_RemoveRecord(&HeadsetGWSdpRecord);
    status = SDP_RemoveRecord(&IntercomSdpRecord);
    status = SDP_RemoveRecord(&CordlessSdpRecord);
    status = SDP_RemoveRecord(&FileTransferSdpRecord);
}

/*---------------------------------------------------------------------------
 *            SdpEventHandler()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handles the ME events
 *
 */
void SdpEventHandler(const BtEvent* event)
{
    char        msg[200];
    BtStatus    status;

    switch(event->eType) {
    case BTEVENT_DEVICE_SELECTED:
        if (event->errCode == BEC_NO_ERROR) {
            ME_InitHandler(&devHandler);
            devHandler.callback = SdpEventHandler;

            status = ME_CreateLink(&devHandler, &event->p.select->result->addr, 
                                   &event->p.select->result->psi, &remDev);

            App_Report("ME_CreateLink() returned %s.", pBT_Status(status));
        } else {
            App_Report("DS_SelectDevice() completed with error: %s.", pHC_Status(event->errCode));
            if (event->errCode == BEC_NOT_FOUND)
                App_Report("You must select a device in the Blue Manager.");
        }
        break;

    case BTEVENT_LINK_CONNECT_CNF:
        /* Skip displaying messages during the local handler event. */
        if (event->handler == &devHandler)
            break;

        if (event->errCode == BEC_NO_ERROR) {
            App_Report("Connection establshed.");
        } else { 
            App_Report("Connection failed, reason %s.", pHC_Status(event->errCode));
            remDev = 0;
        }
        break;

    case BTEVENT_ACCESSIBLE_CHANGE:
        sprintf(msg,"Accessible Mode change complete status = %d",
            event->errCode);
        App_Report(msg);
        break;

    case BTEVENT_LINK_CONNECT_IND:
        App_Report("Incoming connection");
        remDev = event->p.remDev;
        break;

    case BTEVENT_LINK_DISCONNECT:
        /* Skip displaying messages during the local handler event. */
        if (event->handler == &devHandler)
            break;

        App_Report("Global link disconnect event");
        if (remDev == event->p.remDev)
            remDev = 0;
        break;

    case SDEVENT_QUERY_RSP:
        switch (qUuid) {
        /* Verify the responses based on what our server would respond with */
        case SC_OBEX_OBJECT_PUSH:
            /* Verify Service Search Attribute Request query for 
             * OBEX OBJECT PUSH Record 
             */
            if ((status = VerifySdpObjectPush(event->p.token)) == BT_STATUS_SUCCESS) {
                App_Report("OBEX Object Push Query completed. Response VALID");
            } 
            else if (status == BT_STATUS_SDP_CONT_STATE) {
                goto queryAgain;
            }
            else {
                App_Report("OBEX OBJECT PUSH Query completed. Response INVALID");
            }
            break;

        case SC_SERIAL_PORT:
            /* Verify Service Search Attribute Request query for 
             * SERIAL PORT Record 
             */
            if ((status = VerifySdpSerialPort(event->p.token)) == BT_STATUS_SUCCESS) {
                App_Report("SERIAL PORT Query completed. Response VALID");
            } 
            else if (status == BT_STATUS_SDP_CONT_STATE) {
                goto queryAgain;
            }
            else {
                App_Report("SERIAL PORT Query completed. Response INVALID");
            }
            break;

        case SC_FAX:
            /* Verify Service Search Attribute Request query for FAX record */
            if ((status = VerifySdpFax(event->p.token)) == BT_STATUS_SUCCESS) {
                App_Report("FAX Query completed. Response VALID");
            } 
            else if (status == BT_STATUS_SDP_CONT_STATE) {
                goto queryAgain;
            }
            else {
                App_Report("FAX Query completed. Response INVALID");
            }
            break;

        case SC_DIALUP_NETWORKING:
            /* Verify Service Search Attribute Request query for DUN record */
            if ((status = VerifySdpDun(event->p.token)) == BT_STATUS_SUCCESS) {
                App_Report("DUN Query completed. Response VALID");
            } 
            else if (status == BT_STATUS_SDP_CONT_STATE) {
                goto queryAgain;
            }
            else {
                App_Report("DUN Query completed. Response INVALID");
            }
            break;

        case SC_LAN_ACCESS_PPP:
            /* Verify Service Search Attribute Request query for LAN record */
            if ((status = VerifySdpLan(event->p.token)) == BT_STATUS_SUCCESS) {
                App_Report("LAN Query completed. Response VALID");
            } 
            else if (status == BT_STATUS_SDP_CONT_STATE) {
                goto queryAgain;
            }
            else {
                App_Report("LAN Query completed. Response INVALID");
            }
            break;

        case SC_HEADSET:
            /* Verify Service Search Attribute Request query for HEADSET record */
            if ((status = VerifySdpHeadset(event->p.token)) == BT_STATUS_SUCCESS) {
                App_Report("HEADSET Query completed. Response VALID");
            } 
            else if (status == BT_STATUS_SDP_CONT_STATE) {
                goto queryAgain;
            }
            else {
                App_Report("HEADSET Query completed. Response INVALID");
            }
            break;

        case SC_HEADSET_AUDIO_GATEWAY:
            /* Verify Service Search Attribute Request query for HEADSET GATEWAY record */
            if ((status = VerifySdpHeadsetGW(event->p.token)) == BT_STATUS_SUCCESS) {
                App_Report("HEADSET GW Query completed. Response VALID");
            } 
            else if (status == BT_STATUS_SDP_CONT_STATE) {
                goto queryAgain;
            }
            else {
                App_Report("HEADSET GW Query completed. Response INVALID");
            }
            break;

        case SC_INTERCOM:
            /* Verify Service Search Attribute Request query for INTERCOM record */
            if ((status = VerifySdpIntercom(event->p.token)) == BT_STATUS_SUCCESS) {
                App_Report("INTERCOM Query completed. Response VALID");
            } 
            else if (status == BT_STATUS_SDP_CONT_STATE) {
                goto queryAgain;
            }
            else {
                App_Report("INTERCOM Query completed. Response INVALID");
            }
            break;

        case SC_CORDLESS_TELEPHONY:
            /* Verify Service Search Attribute Request query for CORDLESS record */
            if ((status = VerifySdpCordless(event->p.token)) == BT_STATUS_SUCCESS) {
                App_Report("CORDLESS Query completed. Response VALID");
            } 
            else if (status == BT_STATUS_SDP_CONT_STATE) {
                goto queryAgain;
            }
            else {
                App_Report("CORDLESS Query completed. Response INVALID");
            }
            break;

        case SC_OBEX_FILE_TRANSFER:
            /* Verify Service Search Attribute Request query for FILE TRANSFER record */
            if ((status = VerifySdpFileTransfer(event->p.token)) == BT_STATUS_SUCCESS) {
                App_Report("FILE TRANSFER Query completed. Response VALID");
            } 
            else if (status == BT_STATUS_SDP_CONT_STATE) {
                goto queryAgain;
            }
            else {
                App_Report("FILE TRANSFER Query completed. Response INVALID");
            }
            break;

        case SC_IRMC_SYNC:
            /* Verify Service Search Attribute Request query for SYNCHRONIZATION record */
            if ((status = VerifySdpSync(event->p.token)) == BT_STATUS_SUCCESS) {
                App_Report("SYNC Query completed. Response VALID");
            } 
            else if (status == BT_STATUS_SDP_CONT_STATE) {
                goto queryAgain;
            }
            else {
                App_Report("SYNC Query completed. Response INVALID");
            }
            break;
        
        case SC_IRMC_SYNC_COMMAND:
            /* Verify Service Search Attribute Request query for SYNCHRONIZATION COMMAND record */
            if ((status = VerifySdpSyncCommand(event->p.token)) == BT_STATUS_SUCCESS) {
                App_Report("SYNC COMMAND Query completed. Response VALID");
            } 
            else if (status == BT_STATUS_SDP_CONT_STATE) {
queryAgain:
                /* We are going to query again for the rest of the response */
                status = SendSdpUuidQuery(BSQM_CONTINUE, uuidSize);
                if (status != BT_STATUS_PENDING) {
                    /* SDP query failed to start properly */
                    sprintf(msg, "SDP Query failed to start. Status = %d",status);
                    App_Report(msg);
                }            
            }
            else {
                App_Report("SYNC COMMAND Query completed. Response INVALID");
            }
            break;

        }
        break;

    case SDEVENT_QUERY_ERR:
        break;

    case SDEVENT_QUERY_FAILED:
        break;
    }
}

BtStatus SendSdpUuidQuery(SdpQueryMode mode, U8 uuidSize)
{
    BtStatus        status;

    /* Put the proper UUID in the Service Search Attribute Request */
    switch (uuidSize) {
    case 128:
        StoreBE16(UuidServiceSearchAttribReq128+5, qUuid);
        queryToken.parms = UuidServiceSearchAttribReq128;
        queryToken.plen = sizeof(UuidServiceSearchAttribReq128);
        break;

    case 32:
        StoreBE16(UuidServiceSearchAttribReq32+5, qUuid);
        queryToken.parms = UuidServiceSearchAttribReq32;
        queryToken.plen = sizeof(UuidServiceSearchAttribReq32);
        break;

    case 16:
    default:
        StoreBE16(UuidServiceSearchAttribReq16+3, qUuid);
        queryToken.parms = UuidServiceSearchAttribReq16;
        queryToken.plen = sizeof(UuidServiceSearchAttribReq16);
        break;
    }

    queryToken.type = BSQT_SERVICE_SEARCH_ATTRIB_REQ;
    queryToken.rm = remDev;
    status = SDP_Query(&queryToken, mode);
    return status;
}

/*---------------------------------------------------------------------------
 *            VerifySdpObjectPush()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Parses the Object Push SDP response for the Service Class Id
 *            List, Protocol Desc List, Supported Formats List, and optionally
 *            the Profile Desc List and Service Name. In the case of a 
 *            continuation state, additional SDP queries will be issued until 
 *            all of the attributes have been found or until the entire SDP 
 *            response has been parsed.
 *            
 *
 * Return:    BT_STATUS_SUCCESS if SDP parsing is successful.
 *            BT_STATUS_FAILED if SDP parsing encountered an error.
 *            BT_STATUS_SDP_CONT_STATE if SDP parsing encountered a 
 *            continuation state that requires an additional SDP query.
 *
 */
BtStatus VerifySdpObjectPush(SdpQueryToken* token)
{
    U8              i;
    U32             attribFlag;
    BtStatus        status = BT_STATUS_FAILED;

/* Required:  Service Class Id List
 *            Protocol Descriptor List
 *            Supported Formats List
 * Optional:  Profile Descriptor List
 *            Service Name
 */

    /* Just searching for Attribute ID's, no UUID's needed */
    token->uuid = 0;

    for (i = 0; i < 5; i++) {
        if (i == 0) {
            if (flags & FLAG_SERVICE_CLASS_ID_LIST) continue;
            token->attribId = AID_SERVICE_CLASS_ID_LIST;
            attribFlag = FLAG_SERVICE_CLASS_ID_LIST;
        } else if (i == 1) {
            if (flags & FLAG_PROTOCOL_DESC_LIST) continue;
            token->attribId = AID_PROTOCOL_DESC_LIST;
            attribFlag = FLAG_PROTOCOL_DESC_LIST;
        } else if (i == 2) {
            if (flags & FLAG_BT_PROFILE_DESC_LIST) continue;
            token->attribId = AID_BT_PROFILE_DESC_LIST;
            attribFlag = FLAG_BT_PROFILE_DESC_LIST;
        } else if (i == 3) {
            if (flags & FLAG_SERVICE_NAME) continue;
            token->attribId = (AID_SERVICE_NAME + 0x0100);
            attribFlag = FLAG_SERVICE_NAME;
        } else if (i == 4) {
            if (flags & FLAG_SUPPORTED_FORMATS_LIST) continue;
            token->attribId = AID_SUPPORTED_FORMATS_LIST;
            attribFlag = FLAG_SUPPORTED_FORMATS_LIST;
        }

        if ((status = SDP_ParseAttributes(token)) == BT_STATUS_SUCCESS) {
            /* Flag the attribute */
            flags |= attribFlag;
            /* Parse the next attribute */
            token->mode = BSPM_RESUME;
        } else if (status == BT_STATUS_SDP_CONT_STATE) {
            Report(("SDAP: SDP_ParseAttributes - Continuation State. Query Again!\n"));
            token->mode = BSPM_CONT_STATE;
            return status;
        } 
    }

    /* Reset the SDP parsing mode */
    token->mode = BSPM_BEGINNING;
    /* Assume successful parsing */
    status = BT_STATUS_SUCCESS;

    if (!(flags & FLAG_SERVICE_CLASS_ID_LIST) && !(flags & FLAG_PROTOCOL_DESC_LIST) &&
        !(flags & FLAG_SUPPORTED_FORMATS_LIST)) {
        /* Fail: No Object Push service found. */
        status = BT_STATUS_FAILED;
        App_Report("Object Push Service Record NOT FOUND");
    } else if (!(flags & FLAG_SERVICE_CLASS_ID_LIST)) {
        /* Fail: No Service Class Id List found */
        status = BT_STATUS_FAILED;
        App_Report("MISSING: Service Class Id List (REQUIRED)");
    } else if (!(flags & FLAG_PROTOCOL_DESC_LIST)) {
        /* Fail: No Protocol Descriptor List found */
        status = BT_STATUS_FAILED;
        App_Report("MISSING: Protocol Descriptor List (REQUIRED)");
    } else if (!(flags & FLAG_SUPPORTED_FORMATS_LIST)) {
        /* Fail: No Supported Formats List found */
        status = BT_STATUS_FAILED;
        App_Report("MISSING: Supported Formats List (REQUIRED)");
    }

    flags = 0;
    return status;
}

/*---------------------------------------------------------------------------
 *            VerifySdpSerialPort()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Parses the Serial Port SDP response for the Service Class Id
 *            List, Protocol Desc List, and the Service Name. In the case of
 *            a continuation state, additional SDP queries will be issued
 *            until all of the attributes have been found or until the entire
 *            SDP response has been parsed.
 *            
 *
 * Return:    BT_STATUS_SUCCESS if SDP parsing is successful.
 *            BT_STATUS_FAILED if SDP parsing encountered an error.
 *            BT_STATUS_SDP_CONT_STATE if SDP parsing encountered a 
 *            continuation state that requires an additional SDP query.
 *
 */
BtStatus VerifySdpSerialPort(SdpQueryToken* token) 
{
    U8              i;
    U32             attribFlag;
    BtStatus        status = BT_STATUS_FAILED;

/* Required:  Service Class Id List
 *            Protocol Descriptor List
 *            Service Name
 */

    /* Just searching for Attribute ID's, no UUID's needed */
    token->uuid = 0;

    for (i = 0; i < 3; i++) {
        if (i == 0) {
            if (flags & FLAG_SERVICE_CLASS_ID_LIST) continue;
            token->attribId = AID_SERVICE_CLASS_ID_LIST;
            attribFlag = FLAG_SERVICE_CLASS_ID_LIST;
        } else if (i == 1) {
            if (flags & FLAG_PROTOCOL_DESC_LIST) continue;
            token->attribId = AID_PROTOCOL_DESC_LIST;
            attribFlag = FLAG_PROTOCOL_DESC_LIST;
        } else if (i == 2) {
            if (flags & FLAG_SERVICE_NAME) continue;
            token->attribId = (AID_SERVICE_NAME + 0x0100);
            attribFlag = FLAG_SERVICE_NAME;
        }

        if ((status = SDP_ParseAttributes(token)) == BT_STATUS_SUCCESS) {
            /* Flag the attribute */
            flags |= attribFlag;
            /* Parse the next attribute */
            token->mode = BSPM_RESUME;
        } else if (status == BT_STATUS_SDP_CONT_STATE) {
            Report(("SDAP: SDP_ParseAttributes - Continuation State. Query Again!\n"));
            token->mode = BSPM_CONT_STATE;
            return status;
        } 
    }

    /* Reset the SDP parsing mode */
    token->mode = BSPM_BEGINNING;
    /* Assume successful parsing */
    status = BT_STATUS_SUCCESS;

    if (!(flags & FLAG_SERVICE_CLASS_ID_LIST) && !(flags & FLAG_PROTOCOL_DESC_LIST) &&
        !(flags & FLAG_SERVICE_NAME)) {
        /* Fail: No Serial Port service found. */
        status = BT_STATUS_FAILED;
        App_Report("Serial Port Service Record NOT FOUND");
    } else if (!(flags & FLAG_SERVICE_CLASS_ID_LIST)) {
        /* Fail: No Service Class Id List found */
        status = BT_STATUS_FAILED;
        App_Report("MISSING: Service Class Id List (REQUIRED)");
    } else if (!(flags & FLAG_PROTOCOL_DESC_LIST)) {
        /* Fail: No Protocol Descriptor List found */
        status = BT_STATUS_FAILED;
        App_Report("MISSING: Protocol Descriptor List (REQUIRED)");
    } else if (!(flags & FLAG_SERVICE_NAME)) {
        /* Fail: No Service Name found */
        status = BT_STATUS_FAILED;
        App_Report("MISSING: Service Name (REQUIRED)");
    }

    flags = 0;
    return status;
}

/*---------------------------------------------------------------------------
 *            VerifySdpFax()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Parses the Fax SDP response for the Service Class Id
 *            List, Protocol Desc List, Profile Desc List, and optionally the
 *            Service Name, Fax Classes, and Audio Feedback Support. In the
 *            case of a continuation state, additional SDP queries will be
 *            issued until all of the attributes have been found or until the
 *            entire SDP response has been parsed.
 *            
 *
 * Return:    BT_STATUS_SUCCESS if SDP parsing is successful.
 *            BT_STATUS_FAILED if SDP parsing encountered an error.
 *            BT_STATUS_SDP_CONT_STATE if SDP parsing encountered a 
 *            continuation state that requires an additional SDP query.
 *
 */
BtStatus VerifySdpFax(SdpQueryToken* token) 
{
    U8              i;
    U32             attribFlag;
    BtStatus        status = BT_STATUS_FAILED;

/* Required:  Service Class Id List
 *            Protocol Descriptor List
 *            Profile Descriptor List
 * Optional:  Service Name
 *            Fax Class 1
 *            Fax Class 2
 *            Fax Class 2.0
 *            Audio Feedback Support
 */

    /* Just searching for Attribute ID's, no UUID's needed */
    token->uuid = 0;

    for (i = 0; i < 8; i++) {
        if (i == 0) {
            if (flags & FLAG_SERVICE_CLASS_ID_LIST) continue;
            token->attribId = AID_SERVICE_CLASS_ID_LIST;
            attribFlag = FLAG_SERVICE_CLASS_ID_LIST;
        } else if (i == 1) {
            if (flags & FLAG_PROTOCOL_DESC_LIST) continue;
            token->attribId = AID_PROTOCOL_DESC_LIST;
            attribFlag = FLAG_PROTOCOL_DESC_LIST;
        } else if (i == 2) {
            if (flags & FLAG_BT_PROFILE_DESC_LIST) continue;
            token->attribId = AID_BT_PROFILE_DESC_LIST;
            attribFlag = FLAG_BT_PROFILE_DESC_LIST;
        } else if (i == 3) {
            if (flags & FLAG_SERVICE_NAME) continue;
            token->attribId = (AID_SERVICE_NAME + 0x0100);
            attribFlag = FLAG_SERVICE_NAME;
        } else if (i == 4) {
            if (flags & FLAG_FAX_CLASS_1_SUPPORT) continue;
            token->attribId = AID_FAX_CLASS_1_SUPPORT;
            attribFlag = FLAG_FAX_CLASS_1_SUPPORT;
        } else if (i == 5) {
            if (flags & FLAG_FAX_CLASS_20_SUPPORT) continue;
            token->attribId = AID_FAX_CLASS_20_SUPPORT;
            attribFlag = FLAG_FAX_CLASS_20_SUPPORT;
        } else if (i == 6) {
            if (flags & FLAG_FAX_CLASS_2_SUPPORT) continue;
            token->attribId = AID_FAX_CLASS_2_SUPPORT;
            attribFlag = FLAG_FAX_CLASS_2_SUPPORT;
        } else if (i == 7) {
            if (flags & FLAG_AUDIO_FEEDBACK_SUPPORT) continue;
            token->attribId = AID_AUDIO_FEEDBACK_SUPPORT;
            attribFlag = FLAG_AUDIO_FEEDBACK_SUPPORT;
        }

        if ((status = SDP_ParseAttributes(token)) == BT_STATUS_SUCCESS) {
            /* Flag the attribute */
            flags |= attribFlag;
            /* Parse the next attribute */
            token->mode = BSPM_RESUME;
        } else if (status == BT_STATUS_SDP_CONT_STATE) {
            Report(("SDAP: SDP_ParseAttributes - Continuation State. Query Again!\n"));
            token->mode = BSPM_CONT_STATE;
            return status;
        } 
    }

    /* Reset the SDP parsing mode */
    token->mode = BSPM_BEGINNING;
    /* Assume successful parsing */
    status = BT_STATUS_SUCCESS;

    if (!(flags & FLAG_SERVICE_CLASS_ID_LIST) && !(flags & FLAG_PROTOCOL_DESC_LIST) &&
        !(flags & FLAG_BT_PROFILE_DESC_LIST)) {
        /* Fail: No Fax service found. */
        status = BT_STATUS_FAILED;
        App_Report("Fax Service Record NOT FOUND");
    } else if (!(flags & FLAG_SERVICE_CLASS_ID_LIST)) {
        /* Fail: No Service Class Id List found */
        status = BT_STATUS_FAILED;
        App_Report("MISSING: Service Class Id List (REQUIRED)");
    } else if (!(flags & FLAG_PROTOCOL_DESC_LIST)) {
        /* Fail: No Protocol Descriptor List found */
        status = BT_STATUS_FAILED;
        App_Report("MISSING: Protocol Descriptor List (REQUIRED)");
    } else if (!(flags & FLAG_BT_PROFILE_DESC_LIST)) {
        /* Fail: No Profile Descriptor List found */
        status = BT_STATUS_FAILED;
        App_Report("MISSING: Profile Descriptor List (REQUIRED)");
    }

    flags = 0;
    return status;
}

/*---------------------------------------------------------------------------
 *            VerifySdpDun()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Parses the Dialup Networking SDP response for the Service Class
 *            Id List, Protocol Desc List, Profile Desc List, and optionally 
 *            the Service Name and Audio Feedback Support. In the case of a
 *            continuation state, additional SDP queries will be issued until
 *            all of the attributes have been found or until the entire SDP
 *            response has been parsed.
 *            
 *
 * Return:    BT_STATUS_SUCCESS if SDP parsing is successful.
 *            BT_STATUS_FAILED if SDP parsing encountered an error.
 *            BT_STATUS_SDP_CONT_STATE if SDP parsing encountered a 
 *            continuation state that requires an additional SDP query.
 *
 */
BtStatus VerifySdpDun(SdpQueryToken* token) 
{
    U8              i;
    U32             attribFlag;
    BtStatus        status = BT_STATUS_FAILED;

/* Required:  Service Class Id List
 *            Protocol Descriptor List
 *            Profile Descriptor List
 * Optional:  Service Name
 *            Audio Feedback Support
 */

    /* Just searching for Attribute ID's, no UUID's needed */
    token->uuid = 0;

    for (i = 0; i < 5; i++) {
        if (i == 0) {
            if (flags & FLAG_SERVICE_CLASS_ID_LIST) continue;
            token->attribId = AID_SERVICE_CLASS_ID_LIST;
            attribFlag = FLAG_SERVICE_CLASS_ID_LIST;
        } else if (i == 1) {
            if (flags & FLAG_PROTOCOL_DESC_LIST) continue;
            token->attribId = AID_PROTOCOL_DESC_LIST;
            attribFlag = FLAG_PROTOCOL_DESC_LIST;
        } else if (i == 2) {
            if (flags & FLAG_BT_PROFILE_DESC_LIST) continue;
            token->attribId = AID_BT_PROFILE_DESC_LIST;
            attribFlag = FLAG_BT_PROFILE_DESC_LIST;
        } else if (i == 3) {
            if (flags & FLAG_SERVICE_NAME) continue;
            token->attribId = (AID_SERVICE_NAME + 0x0100);
            attribFlag = FLAG_SERVICE_NAME;
        } else if (i == 4) {
            if (flags & FLAG_AUDIO_FEEDBACK_SUPPORT) continue;
            token->attribId = AID_AUDIO_FEEDBACK_SUPPORT;
            attribFlag = FLAG_AUDIO_FEEDBACK_SUPPORT;
        }

        if ((status = SDP_ParseAttributes(token)) == BT_STATUS_SUCCESS) {
            /* Flag the attribute */
            flags |= attribFlag;
            /* Parse the next attribute */
            token->mode = BSPM_RESUME;
        } else if (status == BT_STATUS_SDP_CONT_STATE) {
            Report(("SDAP: SDP_ParseAttributes - Continuation State. Query Again!\n"));
            token->mode = BSPM_CONT_STATE;
            return status;
        } 
    }

    /* Reset the SDP parsing mode */
    token->mode = BSPM_BEGINNING;
    /* Assume successful parsing */
    status = BT_STATUS_SUCCESS;

    if (!(flags & FLAG_SERVICE_CLASS_ID_LIST) && !(flags & FLAG_PROTOCOL_DESC_LIST) &&
        !(flags & FLAG_BT_PROFILE_DESC_LIST)) {
        /* Fail: No Dial-up Networking service found. */
        status = BT_STATUS_FAILED;
        App_Report("Dial-up Networking Service Record NOT FOUND");
    } else if (!(flags & FLAG_SERVICE_CLASS_ID_LIST)) {
        /* Fail: No Service Class Id List found */
        status = BT_STATUS_FAILED;
        App_Report("MISSING: Service Class Id List (REQUIRED)");
    } else if (!(flags & FLAG_PROTOCOL_DESC_LIST)) {
        /* Fail: No Protocol Descriptor List found */
        status = BT_STATUS_FAILED;
        App_Report("MISSING: Protocol Descriptor List (REQUIRED)");
    } else if (!(flags & FLAG_BT_PROFILE_DESC_LIST)) {
        /* Fail: No Profile Descriptor List found */
        status = BT_STATUS_FAILED;
        App_Report("MISSING: Profile Descriptor List (REQUIRED)");
    }

    flags = 0;
    return status;
}

/*---------------------------------------------------------------------------
 *            VerifySdpLan()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Parses the Dialup Networking SDP response for the Service Class
 *            Id List, Protocol Desc List, and optionally the Profile Desc 
 *            List, Service Name, Service Desc, Service Availability, and IP
 *            Subnet. In the case of a continuation state, additional SDP 
 *            queries will be issued until all of the attributes have been 
 *            found or until the entire SDP response has been parsed.
 *            
 *
 * Return:    BT_STATUS_SUCCESS if SDP parsing is successful.
 *            BT_STATUS_FAILED if SDP parsing encountered an error.
 *            BT_STATUS_SDP_CONT_STATE if SDP parsing encountered a 
 *            continuation state that requires an additional SDP query.
 *
 */
BtStatus VerifySdpLan(SdpQueryToken* token) 
{
    U8              i;
    U32             attribFlag;
    BtStatus        status = BT_STATUS_FAILED;

/* Required:  Service Class Id List
 *            Protocol Descriptor List
 * Optional:  Service Availability
 *            Profile Desc List
 *            Service Name
 *            Service Desc
 *            IP Subnet
 */

    /* Just searching for Attribute ID's, no UUID's needed */
    token->uuid = 0;

    for (i = 0; i < 7; i++) {
        if (i == 0) {
            if (flags & FLAG_SERVICE_CLASS_ID_LIST) continue;
            token->attribId = AID_SERVICE_CLASS_ID_LIST;
            attribFlag = FLAG_SERVICE_CLASS_ID_LIST;
        } else if (i == 1) {
            if (flags & FLAG_PROTOCOL_DESC_LIST) continue;
            token->attribId = AID_PROTOCOL_DESC_LIST;
            attribFlag = FLAG_PROTOCOL_DESC_LIST;
        } else if (i == 2) {
            if (flags & FLAG_SERVICE_AVAILABILITY) continue;
            token->attribId = AID_SERVICE_AVAILABILITY;
            attribFlag = FLAG_SERVICE_AVAILABILITY;
        } else if (i == 3) {
            if (flags & FLAG_BT_PROFILE_DESC_LIST) continue;
            token->attribId = AID_BT_PROFILE_DESC_LIST;
            attribFlag = FLAG_BT_PROFILE_DESC_LIST;
        } else if (i == 4) {
            if (flags & FLAG_SERVICE_NAME) continue;
            token->attribId = (AID_SERVICE_NAME + 0x0100);
            attribFlag = FLAG_SERVICE_NAME;
        } else if (i == 5) {
            if (flags & FLAG_SERVICE_DESCRIPTION) continue;
            token->attribId = (AID_SERVICE_DESCRIPTION + 0x0100);
            attribFlag = FLAG_SERVICE_DESCRIPTION;
        } else if (i == 6) {
            if (flags & FLAG_IP_SUBNET) continue;
            token->attribId = AID_IP_SUBNET;
            attribFlag = FLAG_IP_SUBNET;
        }

        if ((status = SDP_ParseAttributes(token)) == BT_STATUS_SUCCESS) {
            /* Flag the attribute */
            flags |= attribFlag;
            /* Parse the next attribute */
            token->mode = BSPM_RESUME;
        } else if (status == BT_STATUS_SDP_CONT_STATE) {
            Report(("SDAP: SDP_ParseAttributes - Continuation State. Query Again!\n"));
            token->mode = BSPM_CONT_STATE;
            return status;
        } 
    }

    /* Reset the SDP parsing mode */
    token->mode = BSPM_BEGINNING;
    /* Assume successful parsing */
    status = BT_STATUS_SUCCESS;

    if (!(flags & FLAG_SERVICE_CLASS_ID_LIST) && !(flags & FLAG_PROTOCOL_DESC_LIST)) {
        /* Fail: No LAN Access service found. */
        status = BT_STATUS_FAILED;
        App_Report("LAN Access Service Record NOT FOUND");
    } else if (!(flags & FLAG_SERVICE_CLASS_ID_LIST)) {
        /* Fail: No Service Class Id List found */
        status = BT_STATUS_FAILED;
        App_Report("MISSING: Service Class Id List (REQUIRED)");
    } else if (!(flags & FLAG_PROTOCOL_DESC_LIST)) {
        /* Fail: No Protocol Descriptor List found */
        status = BT_STATUS_FAILED;
        App_Report("MISSING: Protocol Descriptor List (REQUIRED)");
    }

    flags = 0;
    return status;
}


/*---------------------------------------------------------------------------
 *            VerifySdpHeadset()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Parses the Headset SDP response for the Service Class Id List,
 *            Protocol Desc List, and optionally the Service Name, Profile 
 *            Desc List, and Remove Volume Control. In the case of a 
 *            continuation state, additional SDP queries will be issued until 
 *            all of the attributes have been found or until the entire SDP 
 *            response has been parsed.
 *            
 *
 * Return:    BT_STATUS_SUCCESS if SDP parsing is successful.
 *            BT_STATUS_FAILED if SDP parsing encountered an error.
 *            BT_STATUS_SDP_CONT_STATE if SDP parsing encountered a 
 *            continuation state that requires an additional SDP query.
 *
 */
BtStatus VerifySdpHeadset(SdpQueryToken* token) 
{
    U8              i;
    U32             attribFlag;
    BtStatus        status = BT_STATUS_FAILED;

/* Required:  Service Class Id List
 *            Protocol Descriptor List
 * Optional:  Service Name
 *            Profile Descriptor List
 *            Remote Audio Volume Control
 */

    /* Just searching for Attribute ID's, no UUID's needed */
    token->uuid = 0;

    for (i = 0; i < 5; i++) {
        if (i == 0) {
            if (flags & FLAG_SERVICE_CLASS_ID_LIST) continue;
            token->attribId = AID_SERVICE_CLASS_ID_LIST;
            attribFlag = FLAG_SERVICE_CLASS_ID_LIST;
        } else if (i == 1) {
            if (flags & FLAG_PROTOCOL_DESC_LIST) continue;
            token->attribId = AID_PROTOCOL_DESC_LIST;
            attribFlag = FLAG_PROTOCOL_DESC_LIST;
        } else if (i == 2) {
            if (flags & FLAG_BT_PROFILE_DESC_LIST) continue;
            token->attribId = AID_BT_PROFILE_DESC_LIST;
            attribFlag = FLAG_BT_PROFILE_DESC_LIST;
        } else if (i == 3) {
            if (flags & FLAG_SERVICE_NAME) continue;
            token->attribId = (AID_SERVICE_NAME + 0x0100);
            attribFlag = FLAG_SERVICE_NAME;
        } else if (i == 4) {
            if (flags & FLAG_REMOTE_AUDIO_VOL_CONTROL) continue;
            token->attribId = AID_REMOTE_AUDIO_VOL_CONTROL;
            attribFlag = FLAG_REMOTE_AUDIO_VOL_CONTROL;
        }

        if ((status = SDP_ParseAttributes(token)) == BT_STATUS_SUCCESS) {
            /* Flag the attribute */
            flags |= attribFlag;
            /* Parse the next attribute */
            token->mode = BSPM_RESUME;
        } else if (status == BT_STATUS_SDP_CONT_STATE) {
            Report(("SDAP: SDP_ParseAttributes - Continuation State. Query Again!\n"));
            token->mode = BSPM_CONT_STATE;
            return status;
        } 
    }

    /* Reset the SDP parsing mode */
    token->mode = BSPM_BEGINNING;
    /* Assume successful parsing */
    status = BT_STATUS_SUCCESS;

    if (!(flags & FLAG_SERVICE_CLASS_ID_LIST) && !(flags & FLAG_PROTOCOL_DESC_LIST)) {
        /* Fail: No Headset service found. */
        status = BT_STATUS_FAILED;
        App_Report("Headset Service Record NOT FOUND");
    } else if (!(flags & FLAG_SERVICE_CLASS_ID_LIST)) {
        /* Fail: No Service Class Id List found */
        status = BT_STATUS_FAILED;
        App_Report("MISSING: Service Class Id List (REQUIRED)");
    } else if (!(flags & FLAG_PROTOCOL_DESC_LIST)) {
        /* Fail: No Protocol Descriptor List found */
        status = BT_STATUS_FAILED;
        App_Report("MISSING: Protocol Descriptor List (REQUIRED)");
    }

    flags = 0;
    return status;
}

/*---------------------------------------------------------------------------
 *            VerifySdpHeadsetGW()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Parses the Headset GW SDP response for the Service Class Id List,
 *            Protocol Desc List, and optionally the Service Name and Profile 
 *            Desc List. In the case of a continuation state, additional SDP 
 *            queries will be issued until all of the attributes have been 
 *            found or until the entire SDP response has been parsed.
 *            
 *
 * Return:    BT_STATUS_SUCCESS if SDP parsing is successful.
 *            BT_STATUS_FAILED if SDP parsing encountered an error.
 *            BT_STATUS_SDP_CONT_STATE if SDP parsing encountered a 
 *            continuation state that requires an additional SDP query.
 *
 */
BtStatus VerifySdpHeadsetGW(SdpQueryToken* token) 
{
    U8              i;
    U32             attribFlag;
    BtStatus        status = BT_STATUS_FAILED;

/* Required:  Service Class Id List
 *            Protocol Descriptor List
 * Optional:  Profile Descriptor List
 *            Service Name
 */
    /* Just searching for Attribute ID's, no UUID's needed */
    token->uuid = 0;

    for (i = 0; i < 4; i++) {
        if (i == 0) {
            if (flags & FLAG_SERVICE_CLASS_ID_LIST) continue;
            token->attribId = AID_SERVICE_CLASS_ID_LIST;
            attribFlag = FLAG_SERVICE_CLASS_ID_LIST;
        } else if (i == 1) {
            if (flags & FLAG_PROTOCOL_DESC_LIST) continue;
            token->attribId = AID_PROTOCOL_DESC_LIST;
            attribFlag = FLAG_PROTOCOL_DESC_LIST;
        } else if (i == 2) {
            if (flags & FLAG_BT_PROFILE_DESC_LIST) continue;
            token->attribId = AID_BT_PROFILE_DESC_LIST;
            attribFlag = FLAG_BT_PROFILE_DESC_LIST;
        } else if (i == 3) {
            if (flags & FLAG_SERVICE_NAME) continue;
            token->attribId = (AID_SERVICE_NAME + 0x0100);
            attribFlag = FLAG_SERVICE_NAME;
        }

        if ((status = SDP_ParseAttributes(token)) == BT_STATUS_SUCCESS) {
            /* Flag the attribute */
            flags |= attribFlag;
            /* Parse the next attribute */
            token->mode = BSPM_RESUME;
        } else if (status == BT_STATUS_SDP_CONT_STATE) {
            Report(("SDAP: SDP_ParseAttributes - Continuation State. Query Again!\n"));
            token->mode = BSPM_CONT_STATE;
            return status;
        } 
    }

    /* Reset the SDP parsing mode */
    token->mode = BSPM_BEGINNING;
    /* Assume successful parsing */
    status = BT_STATUS_SUCCESS;

    if (!(flags & FLAG_SERVICE_CLASS_ID_LIST) && !(flags & FLAG_PROTOCOL_DESC_LIST)) {
        /* Fail: No Headset Gateway service found. */
        status = BT_STATUS_FAILED;
        App_Report("Headset Gateway Service Record NOT FOUND");
    } else if (!(flags & FLAG_SERVICE_CLASS_ID_LIST)) {
        /* Fail: No Service Class Id List found */
        status = BT_STATUS_FAILED;
        App_Report("MISSING: Service Class Id List (REQUIRED)");
    } else if (!(flags & FLAG_PROTOCOL_DESC_LIST)) {
        /* Fail: No Protocol Descriptor List found */
        status = BT_STATUS_FAILED;
        App_Report("MISSING: Protocol Descriptor List (REQUIRED)");
    }

    flags = 0;
    return status;
}

/*---------------------------------------------------------------------------
 *            VerifySdpIntercom()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Parses the Intercom SDP response for the Service Class Id List,
 *            Protocol Desc List, and optionally the Service Name and Profile 
 *            Desc List. In the case of a continuation state, additional SDP 
 *            queries will be issued until all of the attributes have been 
 *            found or until the entire SDP response has been parsed.
 *            
 *
 * Return:    BT_STATUS_SUCCESS if SDP parsing is successful.
 *            BT_STATUS_FAILED if SDP parsing encountered an error.
 *            BT_STATUS_SDP_CONT_STATE if SDP parsing encountered a 
 *            continuation state that requires an additional SDP query.
 *
 */
BtStatus VerifySdpIntercom(SdpQueryToken* token) 
{
    U8              i;
    U32             attribFlag;
    BtStatus        status = BT_STATUS_FAILED;

/* Required:  Service Class Id List
 *            Protocol Descriptor List
 * Optional:  Profile Descriptor List
 *            Service Name
 */
    /* Just searching for Attribute ID's, no UUID's needed */
    token->uuid = 0;

    for (i = 0; i < 4; i++) {
        if (i == 0) {
            if (flags & FLAG_SERVICE_CLASS_ID_LIST) continue;
            token->attribId = AID_SERVICE_CLASS_ID_LIST;
            attribFlag = FLAG_SERVICE_CLASS_ID_LIST;
        } else if (i == 1) {
            if (flags & FLAG_PROTOCOL_DESC_LIST) continue;
            token->attribId = AID_PROTOCOL_DESC_LIST;
            attribFlag = FLAG_PROTOCOL_DESC_LIST;
        } else if (i == 2) {
            if (flags & FLAG_BT_PROFILE_DESC_LIST) continue;
            token->attribId = AID_BT_PROFILE_DESC_LIST;
            attribFlag = FLAG_BT_PROFILE_DESC_LIST;
        } else if (i == 3) {
            if (flags & FLAG_SERVICE_NAME) continue;
            token->attribId = (AID_SERVICE_NAME + 0x0100);
            attribFlag = FLAG_SERVICE_NAME;
        }

        if ((status = SDP_ParseAttributes(token)) == BT_STATUS_SUCCESS) {
            /* Flag the attribute */
            flags |= attribFlag;
            /* Parse the next attribute */
            token->mode = BSPM_RESUME;
        } else if (status == BT_STATUS_SDP_CONT_STATE) {
            Report(("SDAP: SDP_ParseAttributes - Continuation State. Query Again!\n"));
            token->mode = BSPM_CONT_STATE;
            return status;
        } 
    }

    /* Reset the SDP parsing mode */
    token->mode = BSPM_BEGINNING;
    /* Assume successful parsing */
    status = BT_STATUS_SUCCESS;

    if (!(flags & FLAG_SERVICE_CLASS_ID_LIST) && !(flags & FLAG_PROTOCOL_DESC_LIST)) {
        /* Fail: No Intercom service found. */
        status = BT_STATUS_FAILED;
        App_Report("Intercom Service Record NOT FOUND");
    } else if (!(flags & FLAG_SERVICE_CLASS_ID_LIST)) {
        /* Fail: No Service Class Id List found */
        status = BT_STATUS_FAILED;
        App_Report("MISSING: Service Class Id List (REQUIRED)");
    } else if (!(flags & FLAG_PROTOCOL_DESC_LIST)) {
        /* Fail: No Protocol Descriptor List found */
        status = BT_STATUS_FAILED;
        App_Report("MISSING: Protocol Descriptor List (REQUIRED)");
    }

    flags = 0;
    return status;
}

/*---------------------------------------------------------------------------
 *            VerifySdpCordless()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Parses the Cordless SDP response for the Service Class Id List,
 *            Protocol Desc List, Profile Desc List, and optionally the 
 *            Service Name and External Network. In the case of a continuation 
 *            state, additional SDP queries will be issued until all of the 
 *            attributes have been found or until the entire SDP response has 
 *            been parsed.
 *            
 *
 * Return:    BT_STATUS_SUCCESS if SDP parsing is successful.
 *            BT_STATUS_FAILED if SDP parsing encountered an error.
 *            BT_STATUS_SDP_CONT_STATE if SDP parsing encountered a 
 *            continuation state that requires an additional SDP query.
 *
 */
BtStatus VerifySdpCordless(SdpQueryToken* token) 
{
    U8              i;
    U32             attribFlag;
    BtStatus        status = BT_STATUS_FAILED;

/* Required:  Service Class Id List
 *            Protocol Descriptor List
 *            Profile Descriptor List
 * Optional:  Service Name
 *            External Network
 */

    /* Just searching for Attribute ID's, no UUID's needed */
    token->uuid = 0;

    for (i = 0; i < 5; i++) {
        if (i == 0) {
            if (flags & FLAG_SERVICE_CLASS_ID_LIST) continue;
            token->attribId = AID_SERVICE_CLASS_ID_LIST;
            attribFlag = FLAG_SERVICE_CLASS_ID_LIST;
        } else if (i == 1) {
            if (flags & FLAG_PROTOCOL_DESC_LIST) continue;
            token->attribId = AID_PROTOCOL_DESC_LIST;
            attribFlag = FLAG_PROTOCOL_DESC_LIST;
        } else if (i == 2) {
            if (flags & FLAG_BT_PROFILE_DESC_LIST) continue;
            token->attribId = AID_BT_PROFILE_DESC_LIST;
            attribFlag = FLAG_BT_PROFILE_DESC_LIST;
        } else if (i == 3) {
            if (flags & FLAG_SERVICE_NAME) continue;
            token->attribId = (AID_SERVICE_NAME + 0x0100);
            attribFlag = FLAG_SERVICE_NAME;
        } else if (i == 4) {
            if (flags & FLAG_EXTERNAL_NETWORK) continue;
            token->attribId = AID_EXTERNAL_NETWORK;
            attribFlag = FLAG_EXTERNAL_NETWORK;
        }

        if ((status = SDP_ParseAttributes(token)) == BT_STATUS_SUCCESS) {
            /* Flag the attribute */
            flags |= attribFlag;
            /* Parse the next attribute */
            token->mode = BSPM_RESUME;
        } else if (status == BT_STATUS_SDP_CONT_STATE) {
            Report(("SDAP: SDP_ParseAttributes - Continuation State. Query Again!\n"));
            token->mode = BSPM_CONT_STATE;
            return status;
        } 
    }

    /* Reset the SDP parsing mode */
    token->mode = BSPM_BEGINNING;
    /* Assume successful parsing */
    status = BT_STATUS_SUCCESS;

    if (!(flags & FLAG_SERVICE_CLASS_ID_LIST) && !(flags & FLAG_PROTOCOL_DESC_LIST) &&
        !(flags & FLAG_BT_PROFILE_DESC_LIST)) {
        /* Fail: No Cordless service found. */
        status = BT_STATUS_FAILED;
        App_Report("Cordless Service Record NOT FOUND");
    } else if (!(flags & FLAG_SERVICE_CLASS_ID_LIST)) {
        /* Fail: No Service Class Id List found */
        status = BT_STATUS_FAILED;
        App_Report("MISSING: Service Class Id List (REQUIRED)");
    } else if (!(flags & FLAG_PROTOCOL_DESC_LIST)) {
        /* Fail: No Protocol Descriptor List found */
        status = BT_STATUS_FAILED;
        App_Report("MISSING: Protocol Descriptor List (REQUIRED)");
    } else if (!(flags & FLAG_BT_PROFILE_DESC_LIST)) {
        /* Fail: No Profile Descriptor List found */
        status = BT_STATUS_FAILED;
        App_Report("MISSING: Profile Descriptor List (REQUIRED)");
    }

    flags = 0;
    return status;
}

/*---------------------------------------------------------------------------
 *            VerifySdpFileTransfer()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Parses the File Transfer SDP response for the Service Class Id
 *            List, Protocol Desc List, and optionally the Service Name and
 *            Profile Desc List. In the case of a continuation state, 
 *            additional SDP queries will be issued until all of the 
 *            attributes have been found or until the entire SDP response 
 *            has been parsed.
 *            
 *
 * Return:    BT_STATUS_SUCCESS if SDP parsing is successful.
 *            BT_STATUS_FAILED if SDP parsing encountered an error.
 *            BT_STATUS_SDP_CONT_STATE if SDP parsing encountered a 
 *            continuation state that requires an additional SDP query.
 *
 */
BtStatus VerifySdpFileTransfer(SdpQueryToken* token)
{
    U8              i;
    U32             attribFlag;
    BtStatus        status = BT_STATUS_FAILED;

/* Required:  Service Class Id List
 *            Protocol Descriptor List
 * Optional:  Profile Descriptor List
 *            Service Name
 */
    /* Just searching for Attribute ID's, no UUID's needed */
    token->uuid = 0;

    for (i = 0; i < 4; i++) {
        if (i == 0) {
            if (flags & FLAG_SERVICE_CLASS_ID_LIST) continue;
            token->attribId = AID_SERVICE_CLASS_ID_LIST;
            attribFlag = FLAG_SERVICE_CLASS_ID_LIST;
        } else if (i == 1) {
            if (flags & FLAG_PROTOCOL_DESC_LIST) continue;
            token->attribId = AID_PROTOCOL_DESC_LIST;
            attribFlag = FLAG_PROTOCOL_DESC_LIST;
        } else if (i == 2) {
            if (flags & FLAG_BT_PROFILE_DESC_LIST) continue;
            token->attribId = AID_BT_PROFILE_DESC_LIST;
            attribFlag = FLAG_BT_PROFILE_DESC_LIST;
        } else if (i == 3) {
            if (flags & FLAG_SERVICE_NAME) continue;
            token->attribId = (AID_SERVICE_NAME + 0x0100);
            attribFlag = FLAG_SERVICE_NAME;
        }

        if ((status = SDP_ParseAttributes(token)) == BT_STATUS_SUCCESS) {
            /* Flag the attribute */
            flags |= attribFlag;
            /* Parse the next attribute */
            token->mode = BSPM_RESUME;
        } else if (status == BT_STATUS_SDP_CONT_STATE) {
            Report(("SDAP: SDP_ParseAttributes - Continuation State. Query Again!\n"));
            token->mode = BSPM_CONT_STATE;
            return status;
        } 
    }

    /* Reset the SDP parsing mode */
    token->mode = BSPM_BEGINNING;
    /* Assume successful parsing */
    status = BT_STATUS_SUCCESS;

    if (!(flags & FLAG_SERVICE_CLASS_ID_LIST) && !(flags & FLAG_PROTOCOL_DESC_LIST)) {
        /* Fail: No File Transfer service found. */
        status = BT_STATUS_FAILED;
        App_Report("File Transfer Service Record NOT FOUND");
    } else if (!(flags & FLAG_SERVICE_CLASS_ID_LIST)) {
        /* Fail: No Service Class Id List found */
        status = BT_STATUS_FAILED;
        App_Report("MISSING: Service Class Id List (REQUIRED)");
    } else if (!(flags & FLAG_PROTOCOL_DESC_LIST)) {
        /* Fail: No Protocol Descriptor List found */
        status = BT_STATUS_FAILED;
        App_Report("MISSING: Protocol Descriptor List (REQUIRED)");
    }

    flags = 0;
    return status;
}

/*---------------------------------------------------------------------------
 *            VerifySdpSync()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Parses the Synchronization SDP response for the Service Class
 *            Id List, Protocol Desc List, Supported Data Stores List, and 
 *            optionally the Service Name and Profile Desc List. In the case 
 *            of a continuation state, additional SDP queries will be issued 
 *            until all of the attributes have been found or until the entire 
 *            SDP response has been parsed.
 *            
 *
 * Return:    BT_STATUS_SUCCESS if SDP parsing is successful.
 *            BT_STATUS_FAILED if SDP parsing encountered an error.
 *            BT_STATUS_SDP_CONT_STATE if SDP parsing encountered a 
 *            continuation state that requires an additional SDP query.
 *
 */
BtStatus VerifySdpSync(SdpQueryToken* token) 
{
    U8              i;
    U32             attribFlag;
    BtStatus        status = BT_STATUS_FAILED;

/* Required:  Service Class Id List
 *            Protocol Descriptor List
 *            Supported Data Stores List
 * Optional:  Profile Descriptor List
 *            Service Name
 */

    /* Just searching for Attribute ID's, no UUID's needed */
    token->uuid = 0;

    for (i = 0; i < 5; i++) {
        if (i == 0) {
            if (flags & FLAG_SERVICE_CLASS_ID_LIST) continue;
            token->attribId = AID_SERVICE_CLASS_ID_LIST;
            attribFlag = FLAG_SERVICE_CLASS_ID_LIST;
        } else if (i == 1) {
            if (flags & FLAG_PROTOCOL_DESC_LIST) continue;
            token->attribId = AID_PROTOCOL_DESC_LIST;
            attribFlag = FLAG_PROTOCOL_DESC_LIST;
        } else if (i == 2) {
            if (flags & FLAG_BT_PROFILE_DESC_LIST) continue;
            token->attribId = AID_BT_PROFILE_DESC_LIST;
            attribFlag = FLAG_BT_PROFILE_DESC_LIST;
        } else if (i == 3) {
            if (flags & FLAG_SERVICE_NAME) continue;
            token->attribId = (AID_SERVICE_NAME + 0x0100);
            attribFlag = FLAG_SERVICE_NAME;
        } else if (i == 4) {
            if (flags & FLAG_SUPPORTED_DATA_STORES_LIST) continue;
            token->attribId = AID_SUPPORTED_DATA_STORES_LIST;
            attribFlag = FLAG_SUPPORTED_DATA_STORES_LIST;
        }

        if ((status = SDP_ParseAttributes(token)) == BT_STATUS_SUCCESS) {
            /* Flag the attribute */
            flags |= attribFlag;
            /* Parse the next attribute */
            token->mode = BSPM_RESUME;
        } else if (status == BT_STATUS_SDP_CONT_STATE) {
            Report(("SDAP: SDP_ParseAttributes - Continuation State. Query Again!\n"));
            token->mode = BSPM_CONT_STATE;
            return status;
        } 
    }

    /* Reset the SDP parsing mode */
    token->mode = BSPM_BEGINNING;
    /* Assume successful parsing */
    status = BT_STATUS_SUCCESS;

    if (!(flags & FLAG_SERVICE_CLASS_ID_LIST) && !(flags & FLAG_PROTOCOL_DESC_LIST) &&
        !(flags & FLAG_SUPPORTED_DATA_STORES_LIST)) {
        /* Fail: No Sync service found. */
        status = BT_STATUS_FAILED;
        App_Report("Sync Service Record NOT FOUND");
    } else if (!(flags & FLAG_SERVICE_CLASS_ID_LIST)) {
        /* Fail: No Service Class Id List found */
        status = BT_STATUS_FAILED;
        App_Report("MISSING: Service Class Id List (REQUIRED)");
    } else if (!(flags & FLAG_PROTOCOL_DESC_LIST)) {
        /* Fail: No Protocol Descriptor List found */
        status = BT_STATUS_FAILED;
        App_Report("MISSING: Protocol Descriptor List (REQUIRED)");
    } else if (!(flags & FLAG_SUPPORTED_DATA_STORES_LIST)) {
        /* Fail: No Supported Data Stores List found */
        status = BT_STATUS_FAILED;
        App_Report("MISSING: Supported Data Stores List (REQUIRED)");
    }

    flags = 0;
    return status;
}

/*---------------------------------------------------------------------------
 *            VerifySdpSyncCommand()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Parses the Synchronization Command SDP response for the Service
 *            Class Id List, Protocol Desc List, and optionally the Service 
 *            Name and Profile Desc List. In the case of a continuation state, 
 *            additional SDP queries will be issued until all of the 
 *            attributes have been found or until the entire SDP response 
 *            has been parsed.
 *            
 *
 * Return:    BT_STATUS_SUCCESS if SDP parsing is successful.
 *            BT_STATUS_FAILED if SDP parsing encountered an error.
 *            BT_STATUS_SDP_CONT_STATE if SDP parsing encountered a 
 *            continuation state that requires an additional SDP query.
 *
 */
BtStatus VerifySdpSyncCommand(SdpQueryToken* token) 
{
    U8              i;
    U32             attribFlag;
    BtStatus        status = BT_STATUS_FAILED;

/* Required:  Service Class Id List
 *            Protocol Descriptor List
 * Optional:  Profile Descriptor List
 *            Service Name
 */
    /* Just searching for Attribute ID's, no UUID's needed */
    token->uuid = 0;

    for (i = 0; i < 4; i++) {
        if (i == 0) {
            if (flags & FLAG_SERVICE_CLASS_ID_LIST) continue;
            token->attribId = AID_SERVICE_CLASS_ID_LIST;
            attribFlag = FLAG_SERVICE_CLASS_ID_LIST;
        } else if (i == 1) {
            if (flags & FLAG_PROTOCOL_DESC_LIST) continue;
            token->attribId = AID_PROTOCOL_DESC_LIST;
            attribFlag = FLAG_PROTOCOL_DESC_LIST;
        } else if (i == 2) {
            if (flags & FLAG_BT_PROFILE_DESC_LIST) continue;
            token->attribId = AID_BT_PROFILE_DESC_LIST;
            attribFlag = FLAG_BT_PROFILE_DESC_LIST;
        } else if (i == 3) {
            if (flags & FLAG_SERVICE_NAME) continue;
            token->attribId = (AID_SERVICE_NAME + 0x0100);
            attribFlag = FLAG_SERVICE_NAME;
        }

        if ((status = SDP_ParseAttributes(token)) == BT_STATUS_SUCCESS) {
            /* Flag the attribute */
            flags |= attribFlag;
            /* Parse the next attribute */
            token->mode = BSPM_RESUME;
        } else if (status == BT_STATUS_SDP_CONT_STATE) {
            Report(("SDAP: SDP_ParseAttributes - Continuation State. Query Again!\n"));
            token->mode = BSPM_CONT_STATE;
            return status;
        } 
    }

    /* Reset the SDP parsing mode */
    token->mode = BSPM_BEGINNING;
    /* Assume successful parsing */
    status = BT_STATUS_SUCCESS;

    if (!(flags & FLAG_SERVICE_CLASS_ID_LIST) && !(flags & FLAG_PROTOCOL_DESC_LIST)) {
        /* Fail: No Sync Command service found. */
        status = BT_STATUS_FAILED;
        App_Report("Sync Command Service Record NOT FOUND");
    } else if (!(flags & FLAG_SERVICE_CLASS_ID_LIST)) {
        /* Fail: No Service Class Id List found */
        status = BT_STATUS_FAILED;
        App_Report("MISSING: Service Class Id List (REQUIRED)");
    } else if (!(flags & FLAG_PROTOCOL_DESC_LIST)) {
        /* Fail: No Protocol Descriptor List found */
        status = BT_STATUS_FAILED;
        App_Report("MISSING: Protocol Descriptor List (REQUIRED)");
    }

    flags = 0;
    return status;
}
