/****************************************************************************
 *
 * File:        qbeam.c
 *
 * Description: This file contains the code for the OBEX QBeam 
 *              application. 
 *
 * Created:     February 10, 1997
 *
 * $Project$
 *
 * Copyright 1997-2005 Extended Systems, Inc.  ALL RIGHTS RESERVED.
 *
 * Unpublished Confidential Information of Extended Systems, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of Extended Systems, Inc.
 * 
 * Use of this work is governed by a license granted by Extended Systems,
 * Inc.  This work contains confidential and proprietary information of
 * Extended Systems, Inc. which is protected by copyright, trade secret,
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/
#include <osapi.h>

#include <obstore.h>
#include <obex.h>

#if BT_STACK == XA_ENABLED
#include <me.h>
#include <bttypes.h>
#include <sys/hci.h>
#endif /* BT_STACK == XA_ENABLED */

#include <qbeam.h>
#include <obdebug.c>
#if OBEX_AUTHENTICATION == XA_ENABLED
#include <obexauth.h>
#endif /* OBEX_AUTHENTICATION == XA_ENABLED */

/* These functions are exported by the UI for application output. */
extern void App_Report(char *format,...);
extern void App_Progress(U32 barId, U32 currPos, U32 maxPos);
extern U32  GetUIContext(U8 QbInst, U8 Role);
extern char* App_GetSavedObject(void);

/****************************************************************************
 *
 * QBeam Internal Function prototypes
 *
 ***************************************************************************/
static void  QbClientCallback(ObexClientCallbackParms *parms);
static void  QbServerCallback(ObexServerCallbackParms *parms);

static void  QbClientDefaultHandler(ObexClientCallbackParms *parms);
static void  QbClientPushHandler(ObexClientCallbackParms *parms);
static void  QbClientExchangeHandler(ObexClientCallbackParms *parms);

static ObStatus     ClientPutObject(U8 QbInst);
static ObStatus     ClientGetObject(U8 QbInst);
static ObStatus     ClientDeleteObject(U8 QbInst);
static ObStatus     ClientSetPath(U8 QbInst, BOOL, BOOL);
#if OBEX_SESSION_SUPPORT == XA_ENABLED
static ObStatus     ClientResumeObject(U8 QbInst);
static void         ClientSuspendSession(QbClientData *QbClient, BOOL Reset);
#endif /* OBEX_SESSION_SUPPORT == XA_ENABLED */

static void         ServerObjectRequest(BOOL DeleteObj, ObexServerApp *app);
static void         ServerProcessHeaders(ObexServerCallbackParms *parms);

static const char   *pObStatus(ObStatus Status);
static const char   *ServerOpName(U8 Operation);
static const char   *ClientOpName(U8 Operation);
static const char   *ClientAbortMsg(ObexClientApp *app);
static const char   *GetName(U8 Role, U8 Instance);
#if OBEX_SESSION_SUPPORT == XA_ENABLED
static BOOL         IsObjectActive(const char *ObName);
static void         SuspendTimerFire(EvmTimer *Timer);
static void         ServerSessionRequest(ObexServerCallbackParms *parms);
static void         ServerSuspendSession(QbServerData *QbServer, BOOL Reset);
static void         ServerResumeObject(QbServerData *Qbs, QbServerSessionData *QbsSession);
#endif /* OBEX_SESSION_SUPPORT == XA_ENABLED */
static U16          StrLen(const char *String);
#if OBEX_AUTHENTICATION == XA_ENABLED
static void QbClientAuthenticate(U8 QbInst);
static void QbAuthenticateResp(U8 QbInst, U8 Role);
extern BOOL App_Authenticate(void);
extern BOOL App_GetPassword(char *password);
extern BOOL App_GetUserId(char *userId);
extern BOOL App_Authenticate(void);
#endif /* OBEX_AUTHENTICATION == XA_ENABLED */

/****************************************************************************
 *
 * ROMable data
 *
 ****************************************************************************/
#if BT_STACK == XA_ENABLED
/*---------------------------------------------------------------------------
 *
 * SDP objects registered by Local Object Push Server.
 * They are the Service Class ID, the Protocol Descriptor List and the
 * supported formats list.
 */
static const U8 OPushServClassId[] = {
    SDP_ATTRIB_HEADER_8BIT(3),          /* Data Element Sequence, 3 bytes */
    /* This is a list of one element of type UUID */
    SDP_UUID_16BIT(SC_OBEX_OBJECT_PUSH) /* Object Push UUID in Big Endian */
};

/* Value of the Supported Formats List of the Object Push profile.
 */
static const U8 OPushServSupportedFormats[] = {
    SDP_ATTRIB_HEADER_8BIT(2),  /* Data Element Sequence, 2 bytes */
    SDP_UINT_8BIT(0xFF)         /* 'Any type of object' value */
};

/*
 * * OPTIONAL * Language BaseId List (goes with the service name).
 */
static const U8 OPushLangBaseIdList[] = {
    SDP_ATTRIB_HEADER_8BIT(9),  /* Data Element Sequence, 9 bytes */
    SDP_UINT_16BIT(0x656E),     /* English Language */
    SDP_UINT_16BIT(0x006A),     /* UTF-8 encoding */
    SDP_UINT_16BIT(0x0100)      /* Primary language base Id */
};

/*
 * * OPTIONAL * Object push service name (digianswer requires it).
 */
static const U8 OPushServServiceName[] = {
    SDP_TEXT_8BIT(17),          /* Null terminated text string, 17 bytes */
    'O', 'B', 'E', 'X', ' ', 'O', 'b', 'j', 'e', 'c', 't', 
    ' ', 'P', 'u', 's', 'h', '\0'
};

/*---------------------------------------------------------------------------
 * Object push Public Browse Group.
 */
static const U8 OPushBrowseGroup[] = {
    SDP_ATTRIB_HEADER_8BIT(3),          /* 3 bytes */
    SDP_UUID_16BIT(SC_PUBLIC_BROWSE_GROUP),  /* Public Browse Group */
};

/* Object Push Servers attributes.
 *
 * This is a ROM template for the RAM structure used to register the
 * Object Push Profile's SDP record.
 */
static const SdpAttribute OPushSdpAttributes[] = {
    /* Object push service class ID List attribute */
    SDP_ATTRIBUTE(AID_SERVICE_CLASS_ID_LIST, OPushServClassId), 
    /* OBEX protocol descriptor list attribute */
    0, 0, 0, 0,
    /* Public Browse Group Service */
    SDP_ATTRIBUTE(AID_BROWSE_GROUP_LIST, OPushBrowseGroup), 
    /* Language base id (Optional: Used with service name) */
    SDP_ATTRIBUTE(AID_LANG_BASE_ID_LIST, OPushLangBaseIdList),
    /* Object push service name (Optional) */
    SDP_ATTRIBUTE((AID_SERVICE_NAME + 0x0100), OPushServServiceName),
    /* Object push supported formats list */
    SDP_ATTRIBUTE(AID_SUPPORTED_FORMATS_LIST, OPushServSupportedFormats)
};

/*-------------------------------------------------------------------------
 *
 * SDP query info
 */
/* Service search attribute request for OBEX Object Push protocol descriptor
 * list. The service search pattern is very specific. It contains the UUIDs
 * for OBEX Object push, L2CAP, and RFCOMM in case there is a version of
 * of OBEX object push running over another protocol.
 */
static const U8 OPushServiceSearchAttribReq[] = {
    /* First parameter is the search pattern in data element format. It
     * is a list of 3 UUIDs. 
     */
    SDP_ATTRIB_HEADER_8BIT(9),             /* Data Element Sequence, 9 bytes */ 
    /* The first UUID in the list OBEX object push */
    SDP_UUID_16BIT(SC_OBEX_OBJECT_PUSH),   /* Object push UUID in Big Endian */
    /* The second UUID is L2CAP */
    SDP_UUID_16BIT(PROT_L2CAP),            /* L2CAP UUID in Big Endian */
    /* The third UUID is RFCOMM */
    SDP_UUID_16BIT(PROT_RFCOMM),           /* UUID for RFCOMM in Big Endian */

    /* The second parameter is the maximum number of bytes that can be
     * be received for the attribute list
     */
    0x00, 0x64,                 /* Max number of bytes for attribute is 100 */

    /* The third parameter is the actual attribute ID list which is a data
     * element sequence. We only want one attribute which is the protocol
     * descriptor
     */
    SDP_ATTRIB_HEADER_8BIT(3),  /* Data Element Sequence, 3 bytes */
    SDP_UINT_16BIT(0x0004),     /* Value of protocol descriptor list ID */
};

#endif /* BT_STACK == XA_ENABLED */

/*---------------------------------------------------------------------------
 *
 * Object Store Function call table
 */
static const ObStoreFuncTable QbStoreFuncTable = {
    OBSTORE_Read, 
    OBSTORE_Write, 
    OBSTORE_GetObjectLen,
#if OBEX_DYNAMIC_OBJECT_SUPPORT == XA_ENABLED
    OBSTORE_ReadFlex,
#endif /* OBEX_DYNAMIC_OBJECT_SUPPORT == XA_ENABLED */
#if OBEX_SESSION_SUPPORT == XA_ENABLED
    OBSTORE_SetOffset,
#endif /* OBEX_SESSION_SUPPORT == XA_ENABLED */
};

#if IRDA_STACK == XA_ENABLED
/*---------------------------------------------------------------------------
 *
 * Below is the device info sent in an XID frame during discovery. A
 * nickname is not required. The first byte is the first byte of
 * hints. Hints are not required but highly recommended. See the
 * IrLMP specification for how hints are used. The Third byte is
 * the character set for the nickname. The total size of the
 * hints, char set, and nickname must not exceed 23 bytes.
 */
const U8 deviceInfo[] = {
    IR_HINT_EXT, IR_HINT_OBEX, IR_CHAR_ASCII, 
    'X','T','N','D','A','c','c','e','s','s',' ','Q','B','e','a','m', 0
};

/*---------------------------------------------------------------------------
 *
 * OBEX IAS Service LSAP result. This value is in RAM because the LSAP 
 * value is not guaranteed to be fixed and may need to be updated after 
 * the server binds. It can be placed in ROM if the LSAP is known and fixed.
 */

U8 obexResult[] = {
    0x01,                    /* Type for Integer is 1 */
    0x00,0x00,0x00,0x02      /* Assumed Lsap */
};

U8 irXferResult[] = {
    0x01,                    /* Type for Integer is 1 */
    0x00,0x00,0x00,0x02      /* Assumed Lsap */
};

U8 customResult[] = {
    0x01,                    /* Type for Integer is 1 */
    0x00,0x00,0x00,0x02      /* Assumed Lsap */
};

/*---------------------------------------------------------------------------
 *
 * IAS objects registered by Local OBEX Server.
 */
/* OBEX LSAP attribute */
const IrIasAttribute obexAttribs = {
   (U8*)"IrDA:TinyTP:LsapSel", 19, (U8*)obexResult, sizeof(obexResult)
};

/* OBEX Class Object */
IrIasObject obexObject = {
    (U8*)"OBEX", 4, 1, (IrIasAttribute*)&obexAttribs
};

/* OBEX:IrXfer LSAP attribute */
const IrIasAttribute irXferAttribs = {
   (U8*)"IrDA:TinyTP:LsapSel", 19, (U8*)irXferResult, sizeof(irXferResult)
};

/* OBEX:IrXfer Class Object */
IrIasObject irXferObject = {
    (U8*)"OBEX:IrXfer", 11, 1, (IrIasAttribute*)&irXferAttribs
};

/* CUSTOM LSAP attribute - example */
const IrIasAttribute customAttribs = {
   (U8*)"IrDA:TinyTP:LsapSel", 19, (U8*)customResult, sizeof(customResult)
};

/* CUSTOM Class Object - example */
IrIasObject customObject = {
    (U8*)"CUSTOM", 6, 1, (IrIasAttribute*)&customAttribs
};

/*---------------------------------------------------------------------------
 *
 * Query string used by Client to find an OBEX Server
 */

/* OBEX Class query - example */
static const U8 obexQuery[] = {
    4,'O','B','E','X',19,'I','r','D','A',':','T','i','n','y','T','P',':',
   'L','s','a','p','S','e','l'
};

/* OBEX:IrXfer Class query - example */
static const U8 irXferQuery[] = {
    11,'O','B','E','X',':','I','r','X','f','e','r',19,'I','r','D','A',':',
   'T','i','n','y','T','P',':','L','s','a','p','S','e','l'
};

/* CUSTOM Class query - example */
static U8 customQuery[] = {
    6,'C','U','S','T','O','M',19,'I','r','D','A',':',
        'T','i','n','y','T','P',':','L','s','a','p','S','e','l'
};
#endif /* IRDA_STACK == XA_ENABLED */

/****************************************************************************
 *
 * Internal Defines
 *
 ****************************************************************************/

/****************************************************************************
 *
 * RAM data
 *
 ****************************************************************************/
/* Client and Server context structures */
QbClientData        QbClient[NUM_INSTANCES] = {0};
QbServerData        QbServer[NUM_INSTANCES] = {0};
#if OBEX_AUTHENTICATION == XA_ENABLED
QbSharedData        Qb[NUM_INSTANCES] = {0};
#endif /* OBEX_AUTHENTICATION == XA_ENABLED */

#if OBEX_SESSION_SUPPORT == XA_ENABLED
/* Client and Server Session context structures */
QbClientSessionData QbClientSession[NUM_SESSIONS] = {0};
QbServerSessionData QbServerSession[NUM_SESSIONS] = {0};
#endif /* OBEX_SESSION_SUPPORT == XA_ENABLED */

/* Number of active/initialized QBeam instances. */
U8                  QbActive = 0;

/*---------------------------------------------------------------------------
 *            Qb_Init
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initialize the OBEX Client, Server and Object Store.
 *
 * Return:    TRUE if initialized, FALSE otherwise
 *
 */
BOOL Qb_Init(U8 QbInst)
{
    ObStatus status;
#if TCP_STACK == XA_ENABLED
    U16      serverPort;
#endif /* TCP_STACK == XA_ENABLED */
#if IRDA_STACK == XA_ENABLED
    U8       lsap = 0;
#endif /* IRDA_STACK == XA_ENABLED */
#if OBEX_AUTHENTICATION == XA_ENABLED
    U8       i;
#endif /* OBEX_AUTHENTICATION == XA_ENABLED */
    if (QbInst >= NUM_INSTANCES) {
        return FALSE;
    }

    /* Initialize the Client */
    OBEX_InitAppHandle( &QbClient[QbInst].clientApp.handle, 
                        QbClient[QbInst].headerBlock, 
                        MAX_HB_BUFFER, &QbStoreFuncTable);

    status = OBEX_ClientInit( &QbClient[QbInst].clientApp, QbClientCallback, 
                              OBEX_EnumTransports());
    if (status != OB_STATUS_SUCCESS) {
        App_Report("Client: Initialization Failed!");
        return FALSE;
    }

    /* Set our Client handlers to use the default handler, just in case a
     * Server transport connection, or some unexpected global IrDA event occurs.
     */
    QbClient[QbInst].handler = QbClientDefaultHandler;

    /* Initialize the Server */
    OBEX_InitAppHandle( &QbServer[QbInst].serverApp.handle, 
                        QbServer[QbInst].headerBlock, 
                        MAX_HB_BUFFER, &QbStoreFuncTable);

    status = OBEX_ServerInit( &QbServer[QbInst].serverApp, QbServerCallback, 
                              OBEX_EnumTransports());
    if (status != OB_STATUS_SUCCESS) {
#if OBEX_DEINIT_FUNCS == XA_ENABLED
        OBEX_ClientDeinit(&QbClient[QbInst].clientApp);
#endif
        App_Report("Server: Initialization Failed!");
        return FALSE;
    }

#if TCP_STACK == XA_ENABLED 
    serverPort = OBEX_ServerGetTcpPort(&QbServer[QbInst].serverApp);
    App_Report("Server: Listening for transport connection (TCP port): %i", serverPort);
#endif /* TCP_STACK == XA_ENABLED */

#if IRDA_STACK == XA_ENABLED
    /* Set the IrDA Device name sent in discovery frames. */
    IR_SetDeviceInfo((U8 *)deviceInfo, (U8)sizeof(deviceInfo));

    if (QbInst == 0) {        
        /* Setup the Obex and IrXfer IAS objects for the First Server */
        status = OBEX_ServerGetIasLsap(&(QbServer[QbInst].serverApp), &lsap);
        if (status != OB_STATUS_SUCCESS) goto done;
        
        obexObject.attribs->value[4] = lsap;
        irXferObject.attribs->value[4] = lsap;

        /* Register the "OBEX" IAS Class name */
        status = IRIAS_Add(&obexObject);
        if (status != OB_STATUS_SUCCESS) goto done;
        /* Register the "IrXfer" IAS Class name */
        status = IRIAS_Add(&irXferObject);
        if (status != OB_STATUS_SUCCESS) goto done;
    }
    else {
        /* Setup the Custom IAS object for the Second Server */
        OBEX_ServerGetIasLsap(&(QbServer[QbInst].serverApp), &lsap);
        
        customObject.attribs->value[4] = lsap;
        
        /* Register the "CUSTOM" IAS Class name */
        status = IRIAS_Add(&customObject);
        if (status != OB_STATUS_SUCCESS) {
done:
            App_Report("Server: Initialization Failed!");
            return FALSE;
        }
    }
#endif /* IRDA_STACK == XA_ENABLED */

#if BT_STACK == XA_ENABLED
    /* Create the SDP entry for the OBEX Object Push server.
     * This is done by copying the ROM templates into RAM structures.
     */
    Assert(sizeof(OPushSdpAttributes) == sizeof(QbServer[QbInst].attributes));
    OS_MemCopy( (U8 *)&(QbServer[QbInst].attributes), 
                (U8 *)&OPushSdpAttributes, 
                sizeof(OPushSdpAttributes));

    OBEX_ServerGetSdpProtocolDescList(&(QbServer[QbInst].serverApp.trans.ObexServerBtTrans), 
                                      &(QbServer[QbInst].attributes[1]), 1);

    QbServer[QbInst].record.attribs = QbServer[QbInst].attributes;
    QbServer[QbInst].record.num = 6;
    QbServer[QbInst].record.classOfDevice = COD_OBJECT_TRANSFER;

    status = SDP_AddRecord(&(QbServer[QbInst].record));
    Assert(status == BT_STATUS_SUCCESS);

#if OBEX_SESSION_SUPPORT == XA_ENABLED
    /* Initialize active session to 'no active session'. */
    QbClient[QbInst].activeSession = NUM_SESSIONS;
    QbServer[QbInst].activeSession = NUM_SESSIONS;
#endif /* OBEX_SESSION_SUPPORT == XA_ENABLED */

    /* Set our local device name */
    ME_SetLocalDeviceName("XTNDAccess Blue Qbeam Application", 34);
#endif /* BT_STACK == XA_ENABLED */

#if OBEX_AUTHENTICATION == XA_ENABLED
    Qb[QbInst].authenticated = FALSE;
    /* No user-defined password, just a default */
    Qb[QbInst].setPassword = FALSE;
    /* No user-defined userId, just a default */
    Qb[QbInst].setUserId = FALSE;
    /* Set default password */
    Qb[QbInst].password[0] = 'M'; 
    Qb[QbInst].password[1] = 'y';
    Qb[QbInst].password[2] = 'P'; 
    Qb[QbInst].password[3] = 'a';
    Qb[QbInst].password[4] = 's'; 
    Qb[QbInst].password[5] = 's';
    Qb[QbInst].password[6] = 'w'; 
    Qb[QbInst].password[7] = 'o';
    Qb[QbInst].password[8] = 'r'; 
    Qb[QbInst].password[9] = 'd';
    Qb[QbInst].password[10] = 0;
    /* Set default userId */
    Qb[QbInst].userId[0] = 'M'; 
    Qb[QbInst].userId[1] = 'y'; 
    Qb[QbInst].userId[2] = 'U'; 
    Qb[QbInst].userId[3] = 's';
    Qb[QbInst].userId[4] = 'e'; 
    Qb[QbInst].userId[5] = 'r';
    Qb[QbInst].userId[6] = 'I'; 
    Qb[QbInst].userId[7] = 'd';
    for (i = 8; i < 20; i++)
        Qb[QbInst].userId[i] = 0;
#endif /* OBEX_AUTHENTICATION == XA_ENABLED */

    /* Now initialize the Object Store Component */
    if (QbActive == 0) {
        if (OBSTORE_Init() == FALSE) {
#if OBEX_DEINIT_FUNCS == XA_ENABLED
            OBEX_ClientDeinit(&QbClient[QbInst].clientApp);
            OBEX_ServerDeinit(&QbServer[QbInst].serverApp);
#endif
            App_Report("Object Store Initialization Failed!");
            return FALSE;
        }
    }

    QbClient[QbInst].inst = QbInst;
    QbServer[QbInst].inst = QbInst;

    QbActive++;
    return TRUE;
}

/*---------------------------------------------------------------------------
 *            Qb_Deinit
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Deinitialize the OBEX protocol.
 *
 * Return:    void
 *
 */
void Qb_Deinit(U8 QbInst)
{
    ObStatus status;

    Assert (QbInst < NUM_INSTANCES);

#if IRDA_STACK == XA_ENABLED
    if (QbInst == 0) {
        /* Remove Server's OBEX and IrXfer IAS entries */
        status = IRIAS_Remove(&obexObject);
        Assert(status == IR_STATUS_SUCCESS);
        status = IRIAS_Remove(&irXferObject);
    }
    else {
        /* Remove Server's CUSTOM IAS entry */
        status = IRIAS_Remove(&customObject);
    }
    Assert(status == IR_STATUS_SUCCESS);
#endif /* IRDA_STACK == XA_ENABLED */

#if OBEX_DEINIT_FUNCS == XA_ENABLED
    /* Deinitialize the Client */
    status = OBEX_ClientDeinit(&QbClient[QbInst].clientApp);
    if (status != OB_STATUS_SUCCESS)
        App_Report("%s Deinit failed: This is likely due to an existing connection",
                   GetName(QB_CLIENT, QbInst));

    /* Deinitialize the Server */
    status = OBEX_ServerDeinit(&QbServer[QbInst].serverApp);
    if (status != OB_STATUS_SUCCESS)
        App_Report("%s Deinit failed: This is likely due to an existing connection",
                   GetName(QB_SERVER, QbInst));
#endif /* OBEX_DEINITE_FUNCS == XA_ENABLED */

#if BT_STACK == XA_ENABLED
    status = SDP_RemoveRecord(&(QbServer[QbInst].record));
    Assert(status == BT_STATUS_SUCCESS);
#endif /* BT_STACK == XA_ENABLED */

    if (QbActive == 1)
        OBSTORE_Deinit();

    QbActive--;
}

/*---------------------------------------------------------------------------
 *            Qb_Connect
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Issue an OBEX Connect Request.
 *
 * Return:    void
 *
 */
void Qb_Connect(U8 QbInst)
{
    ObStatus status;

    QbClient[QbInst].handler = QbClientDefaultHandler;
    QbClient[QbInst].state = CLIENT_OBEX_CONNECT;

#if OBEX_AUTHENTICATION == XA_ENABLED
    QbClientAuthenticate(QbInst);
#endif /* OBEX_AUTHENTICATION == XA_ENABLED */
    status = OBEX_ClientConnect(&QbClient[QbInst].clientApp);

    App_Report("%s OBEX Connect returned \"%s\".", GetName(QB_CLIENT, QbInst), 
               pObStatus(status));
}

/*---------------------------------------------------------------------------
 *            Qb_Disconnect
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Issue an OBEX Disconnect Request.
 *
 * Return:    void
 *
 */
void Qb_Disconnect(U8 QbInst)
{
    ObStatus status;

    QbClient[QbInst].handler = QbClientDefaultHandler;
    QbClient[QbInst].state = CLIENT_OBEX_DISCON;

    status = OBEX_ClientDisconnect(&QbClient[QbInst].clientApp);

    App_Report("%s OBEX Disconnect returned \"%s\".", GetName(QB_CLIENT, QbInst), 
               pObStatus(status));
}

/*---------------------------------------------------------------------------
 *            Qb_Put
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Issue an OBEX Put Request.
 *
 * Return:    void
 *
 */
void Qb_Put(U8 QbInst, const char *ObName)
{
    ObStatus    status;
#if OBEX_SESSION_SUPPORT == XA_ENABLED
    if (IsObjectActive(ObName)) {
        /* Do not perform this operation. */
        App_Report("%s OBEX Put Canceled.  Another operation for this "
                   "object is in progress.", GetName(QB_CLIENT, QbInst));
        return;
    }
#endif /* OBEX_SESSION_SUPPORT == XA_ENABLED */

    QbClient[QbInst].handler = QbClientDefaultHandler;
    QbClient[QbInst].objectName = ObName;
    QbClient[QbInst].state = CLIENT_OBEX_PUT;

#if OBEX_AUTHENTICATION == XA_ENABLED
    QbClientAuthenticate(QbInst);
#endif /* OBEX_AUTHENTICATION == XA_ENABLED */
    status = ClientPutObject(QbInst);
    if (status == OB_STATUS_PENDING) {
        /* Reset progress bar */
        App_Progress(GetUIContext(QbInst, QB_CLIENT), 0, 0);
    }

    App_Report("%s OBEX Put returned \"%s\".", GetName(QB_CLIENT, QbInst),
                pObStatus(status));
}

/*---------------------------------------------------------------------------
 *            Qb_Get
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Issue an OBEX Get Request.
 *
 * Return:    void
 *
 */
void Qb_Get(U8 QbInst, const char *ObName)
{
    ObStatus status;

#if OBEX_SESSION_SUPPORT == XA_ENABLED
    if (IsObjectActive(ObName)) {
        /* Do not perform this operation. */
        App_Report("%s OBEX Get Canceled.  Another operation for this "
                   "object is in progress.",GetName(QB_CLIENT, QbInst));
        return;
    }
#endif /* OBEX_SESSION_SUPPORT == XA_ENABLED */

    QbClient[QbInst].handler = QbClientDefaultHandler;
    QbClient[QbInst].objectName = ObName;
    QbClient[QbInst].state = CLIENT_OBEX_GET;

#if OBEX_AUTHENTICATION == XA_ENABLED
    QbClientAuthenticate(QbInst);
#endif /* OBEX_AUTHENTICATION == XA_ENABLED */
    status = ClientGetObject(QbInst);
    if (status == OB_STATUS_PENDING) {
        /* Reset progress bar */
        App_Progress(GetUIContext(QbInst, QB_CLIENT), 0, 0);
    }

    App_Report("%s OBEX Get returned \"%s\".", GetName(QB_CLIENT, QbInst), 
        pObStatus(status));
}

/*---------------------------------------------------------------------------
 *            Qb_SetPath
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Issue an OBEX Set Path Request.
 *
 * Return:    void
 *
 */
void Qb_SetPath(U8 QbInst, const char *PathName, BOOL Backup, BOOL NoCreate)
{
    ObStatus status;

    QbClient[QbInst].handler = QbClientDefaultHandler;
    QbClient[QbInst].pathName = PathName;
    QbClient[QbInst].state = CLIENT_OBEX_SETPATH;

    status = ClientSetPath(QbInst, Backup, NoCreate);
    App_Report("%s OBEX Set Path returned \"%s\".", GetName(QB_CLIENT, QbInst),
                pObStatus(status));
}

#if OBEX_SESSION_SUPPORT == XA_ENABLED
/*---------------------------------------------------------------------------
 *            Qb_CreateSession
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Issue an OBEX Create Session Request.
 *
 * Return:    void
 *
 */
void Qb_CreateSession(U8 QbInst)
{
    ObStatus        status;
    U8              i;

    for (i = 0; i < NUM_SESSIONS; i++) {
        if (QbClientSession[i].state == QB_SESSION_CLOSED)
            break;
    }
    if (i == NUM_SESSIONS) {
        App_Report("%s All Sessions are in use.", GetName(QB_CLIENT, QbInst));
        return;
    }

    QbClient[QbInst].handler = QbClientDefaultHandler;
    QbClient[QbInst].state = CLIENT_OBEX_CREATE_SESSION;
    QbClient[QbInst].currSession = i;

    status = OBEX_ClientCreateSession( &QbClient[QbInst].clientApp, 
                                       &QbClientSession[i],
                                       OBEX_TIMEOUT_DONTCARE);

    App_Report("%s OBEX Create Session returned \"%s\".", GetName(QB_CLIENT, QbInst), 
               pObStatus(status));
}

/*---------------------------------------------------------------------------
 *            Qb_CloseSession
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Issue an OBEX Close Session Request.
 *
 * Return:    void
 *
 */
void Qb_CloseSession(U8 QbInst)
{
    ObStatus        status;
    U8              sessionId[16], i;

    for (i = 0; i < NUM_SESSIONS; i++) {
        if (QbClientSession[i].state != QB_SESSION_CLOSED)
            break;
    }
    if (i == NUM_SESSIONS) {
        App_Report("%s All Sessions are closed.", GetName(QB_CLIENT, QbInst));
        return;
    }

    QbClient[QbInst].handler = QbClientDefaultHandler;
    QbClient[QbInst].state = CLIENT_OBEX_CLOSE_SESSION;
    QbClient[QbInst].currSession = i;

    OBEX_CreateSessionId(&QbClientSession[i], sessionId);

    status = OBEX_ClientCloseSession(&QbClient[QbInst].clientApp, sessionId);

    App_Report("%s OBEX Close Session returned \"%s\".", GetName(QB_CLIENT, QbInst), 
               pObStatus(status));
}

/*---------------------------------------------------------------------------
 *            Qb_SuspendSession
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Issue an OBEX Suspend Session Request.
 *
 * Return:    void
 *
 */
void Qb_SuspendSession(U8 QbInst)
{
    ObStatus        status;

    if (QbClient[QbInst].activeSession == NUM_SESSIONS) {
        App_Report("%s There is no active Session to suspend.", GetName(QB_CLIENT, QbInst));
        return;
    }
    Assert(QbClientSession[QbClient[QbInst].activeSession].state == QB_SESSION_ACTIVE);

    QbClient[QbInst].handler = QbClientDefaultHandler;
    QbClient[QbInst].state = CLIENT_OBEX_SUSPEND_SESSION;
    status = OBEX_ClientSuspendSession(&QbClient[QbInst].clientApp,
                                       &QbClientSession[QbClient[QbInst].activeSession], 
                                       OBEX_TIMEOUT_DONTCARE);

    App_Report("%s OBEX Suspend Session returned \"%s\".", GetName(QB_CLIENT, QbInst), 
               pObStatus(status));
}

/*---------------------------------------------------------------------------
 *            Qb_ResumeSession
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Issue an OBEX Resume Session Request.
 *
 * Return:    void
 *
 */
void Qb_ResumeSession(U8 QbInst)
{
    ObStatus                status;
    ObexSessionResumeParms  parms = {0};
    QbClientData           *qbc = &QbClient[QbInst];
    U8                      i;
    
    for (i = 0; i < NUM_SESSIONS; i++) {
        if (QbClientSession[i].state == QB_SESSION_SUSPENDED)
            break;
    }
    if (i == NUM_SESSIONS) {
        App_Report("%s No suspended sessions to resume.", GetName(QB_CLIENT, QbInst));
        return;
    }

    qbc->handler = QbClientDefaultHandler;
    qbc->state = CLIENT_OBEX_RESUME_SESSION;
    qbc->currSession = i;

    /* See if there are any parameters from a suspended operatation that
     * need to be restored as part of the session resume. Remember, if there
     * is a suspended operation, it will resume automatically once the Session
     * Resume operation is complete.
     */
    if (StrLen(QbClientSession[i].objectName) > 0) {
        qbc->objectName = QbClientSession[i].objectName;
/*      qbc->objectType = QbClientSession[i].objectType; */

        /* Reopen the object we were exchanging. */
        ClientResumeObject(QbInst);
        parms.obsh = qbc->object;
    }

    /* Setup operation resume header parameters. */
    if ((parms.headerLen = QbClientSession[i].headerLen) > 0)
        parms.headerBuff = QbClientSession[i].headerBlock;

    status = OBEX_ClientResumeSession(&qbc->clientApp,
                                      &QbClientSession[i].session,
                                      &parms, OBEX_TIMEOUT_DONTCARE);

    if (status != OB_STATUS_PENDING && qbc->object)
        OBSTORE_Close(&qbc->object);

    if (status == OB_STATUS_PENDING)
        EVM_CancelTimer(&QbClientSession[i].timer);

    App_Report("%s OBEX Resume Session returned \"%s\".", GetName(QB_CLIENT, QbInst), 
               pObStatus(status));
}

/*---------------------------------------------------------------------------
 *            Qb_SetSessionTimeout
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Issue an OBEX Set Session Timeout Request.
 *
 * Return:    void
 *
 */
void Qb_SetSessionTimeout(U8 QbInst, U32 Timeout, U8 Role)
{
    QbClientData   *qbc = &QbClient[QbInst];
    QbServerData   *qbs = &QbServer[QbInst];
    ObStatus        status;
    U8              i;

    if (Role == QB_CLIENT) {
        /* Search for a session, starting with an active one. */
        if (qbc->activeSession == NUM_SESSIONS) {
            for (i = 0; i < NUM_SESSIONS; i++) {
                if (QbClientSession[i].state == QB_SESSION_SUSPENDED)
                    break;
            }
            if (i == NUM_SESSIONS) {
                App_Report("%s No active or suspended sessions to Set Timeout.", GetName(QB_CLIENT, QbInst));
                return;
            }
        }
        else i = qbc->activeSession;

        Assert(QbClientSession[i].state != QB_SESSION_CLOSED);

        qbc->handler = QbClientDefaultHandler;
        qbc->state = CLIENT_OBEX_SET_TIMEOUT;

        status = OBEX_ClientSetSessionTimeout( &qbc->clientApp,
                                               &QbClientSession[i].session,
                                               Timeout);
    } else {
        Assert (Role == QB_SERVER);

        if (qbs->activeSession == NUM_SESSIONS) {
            App_Report("%s No active session to Set Timeout.", GetName(QB_SERVER, QbInst));
            return;
        }

        status = OBEX_ServerSetSessionTimeout(&qbs->serverApp, Timeout);
        if (status == OB_STATUS_SUCCESS)
            Assert(QbServerSession[qbs->activeSession].state == QB_SESSION_ACTIVE);
    }

    App_Report("%s OBEX Set Session Timeout returned \"%s\".", GetName(Role, QbInst), pObStatus(status));
}
#endif /* OBEX_SESSION_SUPPORT == XA_ENABLED */

#if OBEX_AUTHENTICATION == XA_ENABLED 
/*---------------------------------------------------------------------------
 *            Qb_SetPassword
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Set the OBEX Authentication password
 *
 * Return:    void
 *
 */
void Qb_SetPassword(U8 QbInst)
{
    if (App_GetPassword(Qb[QbInst].password)) {
        /* User-defined password set */
        Qb[QbInst].setPassword = TRUE;  
        App_Report("%s Password Set.", GetName(QB_CLIENT, QbInst)); 
        App_Report("%s Password Set.", GetName(QB_SERVER, QbInst)); 
    }
    else {
        if (Qb[QbInst].setPassword) {
            App_Report("%s Using Current Password: %s.", GetName(QB_CLIENT, QbInst), Qb[QbInst].password);
            App_Report("%s Using Current Password: %s.", GetName(QB_SERVER, QbInst), Qb[QbInst].password);
        } else { 
            App_Report("%s Using Default Password: %s.", GetName(QB_CLIENT, QbInst), Qb[QbInst].password);
            App_Report("%s Using Default Password: %s.", GetName(QB_SERVER, QbInst), Qb[QbInst].password);
        }
    }
}

/*---------------------------------------------------------------------------
 *            Qb_SetUserId
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Set the OBEX Authentication user id
 *
 * Return:    void
 *
 */
void Qb_SetUserId(U8 QbInst)
{
    if (App_GetUserId(Qb[QbInst].userId)) {
        /* User-defined userId set */
        Qb[QbInst].setUserId = TRUE;
        App_Report("%s UserId Set.", GetName(QB_CLIENT, QbInst)); 
        App_Report("%s UserId Set.", GetName(QB_SERVER, QbInst)); 
    } else {
        if (Qb[QbInst].setUserId) {
            App_Report("%s Using Current UserId: %s.", GetName(QB_CLIENT, QbInst), Qb[QbInst].userId);
            App_Report("%s Using Current UserId: %s.", GetName(QB_SERVER, QbInst), Qb[QbInst].userId);
        } else {
            App_Report("%s Using Default UserId: %s.", GetName(QB_CLIENT, QbInst), Qb[QbInst].userId);
            App_Report("%s Using Default UserId: %s.", GetName(QB_SERVER, QbInst), Qb[QbInst].userId);
        }
    }
}
#endif /* OBEX_AUTHENTICATION == XA_ENABLED */

/*---------------------------------------------------------------------------
 *            Qb_ObjectPush
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initiate an OBEX Object Push procedure.
 *
 * Return:    void
 *
 */
void Qb_ObjectPush(U8 QbInst, const char *ObName, ObexTpAddr *Target)
{
   ObStatus status;

#if OBEX_SESSION_SUPPORT == XA_ENABLED
    if (IsObjectActive(ObName)) {
        /* Do not perform this operation. */
        App_Report("%s OBEX Object Push Canceled. Another operation for this"
                   "object is in progress.", GetName(QB_CLIENT, QbInst));
        return;
    }
#endif /* OBEX_SESSION_SUPPORT == XA_ENABLED */

    QbClient[QbInst].handler = QbClientPushHandler;
    QbClient[QbInst].objectName = ObName;

#if BT_STACK == XA_ENABLED
    if (Target->type == OBEX_TP_BLUETOOTH) {
        /* Setup the SDP Query parameters */
        Target->proto.bt.sdpQuery = OPushServiceSearchAttribReq;
        Target->proto.bt.sdpQueryLen = sizeof(OPushServiceSearchAttribReq);
        Target->proto.bt.sdpQueryType = BSQT_SERVICE_SEARCH_ATTRIB_REQ;
    }
#endif /* BT_STACK == XA_ENABLED */

#if IRDA_STACK== XA_ENABLED
    if (Target->type == OBEX_TP_IRDA) {
        if (QbInst == 0) {
            /* Use the Obex IAS Query */
            Target->proto.ir.iasQuery = obexQuery;
            Target->proto.ir.iasQueryLen = sizeof(obexQuery);
        }
        else {
            /* Use the Custom IAS Query */
            Target->proto.ir.iasQuery = customQuery;
            Target->proto.ir.iasQueryLen = sizeof(customQuery);
        }
    }
#endif /* IRDA_STACK == XA_ENABLED */

    QbClient[QbInst].state = CLIENT_TP_CONNECT;

#if IRDA_STACK == XA_ENABLED
    QbClient[QbInst].currTransport = Target->type;
#endif /* IRDA_STACK == XA_ENABLED */
    status = OBEX_ClientTpConnect(&QbClient[QbInst].clientApp, Target);
    if (status == OB_STATUS_SUCCESS) {
        /* The transport layer connection is up. Send an OBEX Connect. */
        ObexClientCallbackParms     parms = {0};

        parms.client = &QbClient[QbInst].clientApp;
        parms.event = OBCE_CONNECTED;

        QbClientPushHandler(&parms);
    }

    if ((status != OB_STATUS_SUCCESS) && (status != OB_STATUS_PENDING)) {
        /* The transport connection failed, reset our state information */
        QbClient[QbInst].state = CLIENT_IDLE;
    }

    App_Report("%s Transport Layer Connect Returned \"%s\".", 
                GetName(QB_CLIENT, 0), pObStatus(status));
}

/*---------------------------------------------------------------------------
 *            Qb_BizExchange
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initiate an OBEX Business Card Exchange procedure.
 *
 * Return:    void
 *
 */
void Qb_BizExchange(U8 QbInst, const char *ObName, ObexTpAddr *Target)
{
    ObStatus status;

#if OBEX_SESSION_SUPPORT == XA_ENABLED
    if (IsObjectActive(ObName)) {
        /* Do not perform this operation. */
        App_Report("%s OBEX VCard Exchange Canceled. Another operation for "
                   "this object is in progress.", GetName(QB_CLIENT, QbInst));
        return;
    }
#endif /* OBEX_SESSION_SUPPORT == XA_ENABLED */
    QbClient[QbInst].handler = QbClientExchangeHandler;
    QbClient[QbInst].objectName = ObName;
            
#if BT_STACK == XA_ENABLED
    if (Target->type == OBEX_TP_BLUETOOTH) {
        /* Setup the SDP Query parameters */
        Target->proto.bt.sdpQuery = OPushServiceSearchAttribReq;
        Target->proto.bt.sdpQueryLen = sizeof(OPushServiceSearchAttribReq);
        Target->proto.bt.sdpQueryType = BSQT_SERVICE_SEARCH_ATTRIB_REQ;
    }
#endif /* BT_STACK == XA_ENABLED */

#if IRDA_STACK== XA_ENABLED
    if (Target->type == OBEX_TP_IRDA) {
        if (QbInst == 0) {
            /* Use the Obex IAS Query */
            Target->proto.ir.iasQuery = obexQuery;
            Target->proto.ir.iasQueryLen = sizeof(obexQuery);
        }
        else {
            /* Use the Custom IAS Query */
            Target->proto.ir.iasQuery = customQuery;
            Target->proto.ir.iasQueryLen = sizeof(customQuery);
        }
    }
#endif /* IRDA_STACK == XA_ENABLED */

    QbClient[QbInst].state = CLIENT_TP_CONNECT;

#if IRDA_STACK == XA_ENABLED
    QbClient[QbInst].currTransport = Target->type;
#endif /* IRDA_STACK == XA_ENABLED */
    status = OBEX_ClientTpConnect(&QbClient[QbInst].clientApp, Target);
    if (status == OB_STATUS_SUCCESS) {
        ObexClientCallbackParms     parms = {0};

        parms.client = &QbClient[QbInst].clientApp;
        parms.event = OBCE_CONNECTED;

        QbClientExchangeHandler(&parms);
    }

    if ((status != OB_STATUS_SUCCESS) && (status != OB_STATUS_PENDING)) {
        /* The transport connection failed, reset our state information */
        QbClient[QbInst].state = CLIENT_IDLE;
    }

    App_Report("%s Transport Layer Connect Returned \"%s\".", 
                GetName(QB_CLIENT, QbInst), pObStatus(status));
}

/*---------------------------------------------------------------------------
 *            Qb_ClientTpConnect
 *---------------------------------------------------------------------------
 *
 * Synopsis:  OBEX Client initiates an OBEX Transport Connection.
 *
 * Return:    void
 *
 */
void Qb_ClientTpConnect(U8 QbInst, ObexTpAddr *Target)
{
    ObStatus status;

#if BT_STACK == XA_ENABLED
    if (Target->type == OBEX_TP_BLUETOOTH) {
        /* Setup the SDP Query parameters */
        Target->proto.bt.sdpQuery = OPushServiceSearchAttribReq;
        Target->proto.bt.sdpQueryLen = sizeof(OPushServiceSearchAttribReq);
        Target->proto.bt.sdpQueryType = BSQT_SERVICE_SEARCH_ATTRIB_REQ;
    }
#endif /* BT_STACK == XA_ENABLED */

#if IRDA_STACK== XA_ENABLED
    if (Target->type == OBEX_TP_IRDA) {
        if (QbInst == 0) {
            /* Use the Obex IAS Query */
            Target->proto.ir.iasQuery = obexQuery;
            Target->proto.ir.iasQueryLen = sizeof(obexQuery);
        }
        else {
            /* Use the Custom IAS Query */
            Target->proto.ir.iasQuery = customQuery;
            Target->proto.ir.iasQueryLen = sizeof(customQuery);
        }
    }
#endif /* IRDA_STACK == XA_ENABLED */

    QbClient[QbInst].handler = QbClientDefaultHandler;
    QbClient[QbInst].state = CLIENT_TP_CONNECT;
#if IRDA_STACK == XA_ENABLED
    QbClient[QbInst].currTransport = Target->type;
#endif /* IRDA_STACK == XA_ENABLED */
    status = OBEX_ClientTpConnect(&QbClient[QbInst].clientApp, Target);
    if (status == OB_STATUS_SUCCESS) {
        App_Report("%s Transport Layer connection established.", 
                    GetName(QB_CLIENT, QbInst));
        QbClient[QbInst].state = CLIENT_CONNECTED;
    } else {
        App_Report("%s Transport Layer Connect returned \"%s\".", 
               GetName(QB_CLIENT, QbInst), pObStatus(status));
    }

    if ((status != OB_STATUS_SUCCESS) && (status != OB_STATUS_PENDING)) {
        /* The transport connection failed, reset our state information */
        QbClient[QbInst].state = CLIENT_IDLE;
    }
}

/*---------------------------------------------------------------------------
 *            Qb_ClientTpDisconnect
 *---------------------------------------------------------------------------
 *
 * Synopsis:  OBEX Client initiates the destruction of an OBEX Transport 
 *            Connection.
 *
 * Return:    void
 *
 */
void Qb_ClientTpDisconnect(U8 QbInst)
{
    ObStatus status;

    QbClient[QbInst].handler = QbClientDefaultHandler;

    /* Set the state only if it doesn't clobber an active operation state. */
    if (QbClient[QbInst].state == CLIENT_IDLE)
        QbClient[QbInst].state = CLIENT_TP_DISCON;

    status = OBEX_ClientTpDisconnect(&QbClient[QbInst].clientApp, FALSE);
    if (status == OB_STATUS_SUCCESS) {
        App_Report("%s The transport connection has been disconnected.", 
                      GetName(QB_CLIENT, QbInst));
       
        /* Reset progress bar */
        App_Progress(GetUIContext(QbInst, QB_CLIENT), 0, 0);
        QbClient[QbInst].state = CLIENT_IDLE;
    } else {
        App_Report("%s Transport Layer Disconnect returned \"%s\".", 
                   GetName(QB_CLIENT, QbInst), pObStatus(status));
    }
}

#if OBEX_ALLOW_SERVER_TP_DISCONNECT == XA_ENABLED 
/*---------------------------------------------------------------------------
 *            Qb_ServerTpDisconnect
 *---------------------------------------------------------------------------
 *
 * Synopsis:  OBEX Server initiates the destruction of an OBEX Transport 
 *            Connection.
 *
 * Return:    void
 *
 */
void Qb_ServerTpDisconnect(U8 QbInst)
{
    ObStatus status;

    status = OBEX_ServerTpDisconnect(&QbServer[QbInst].serverApp, FALSE);
    if (status == OB_STATUS_SUCCESS) {
        App_Report("%s Transport Connection has been disconnected.", 
                   GetName(QB_SERVER, QbInst));
        QbServer[QbInst].connState = QB_SERVER_CONN_DISC;
        /* Reset progress bar */
        App_Progress(GetUIContext(QbInst, QB_SERVER), 0, 0);
    } else {
        App_Report("%s Transport Layer Disconnect returned \"%s\".", 
                   GetName(QB_SERVER, QbInst), pObStatus(status));
    }
}
#endif /* OBEX_ALLOW_SERVER_TP_DISCONNECT == XA_ENABLED */

/*---------------------------------------------------------------------------
 *            Qb_AbortServer
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Requests the current operation be aborted.
 *
 * Return:    void
 *
 */
void Qb_AbortServer(U8 QbInst)
{
    OBEX_ServerAbort(&QbServer[QbInst].serverApp, OBRC_CONFLICT);
    App_Report("%s Abort requested.", GetName(QB_SERVER, QbInst));
}

/*---------------------------------------------------------------------------
 *            Qb_AbortClient
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Requests the current operation be aborted.
 *
 * Return:    void
 *
 */
void Qb_AbortClient(U8 QbInst)
{
    ObStatus status;

    status = OBEX_ClientAbort(&QbClient[QbInst].clientApp);
    App_Report("%s Abort returned \"%s\".", 
               GetName(QB_CLIENT, QbInst), pObStatus(status));
}

/*---------------------------------------------------------------------------
 *            QbClientPushHandler
 *---------------------------------------------------------------------------
 *
 * Synopsis:  This function processes Application events for the PUSH Client.
 *
 * Return:    void
 *
 */
static void QbClientPushHandler(ObexClientCallbackParms *Parms)
{
    QbClientData   *qbc = ContainingRecord(Parms->client, QbClientData, clientApp);
    ObStatus        status;
#if IRDA_STACK == XA_ENABLED
    ObexTpAddr      Target;
#endif /* IRDA_STACK == XA_ENABLED */
#if OBEX_AUTHENTICATION == XA_ENABLED
    ObexAuthResponseVerify  verify;
#endif /* OBEX_AUTHENTICATION == XA_ENABLED */

    switch (Parms->event) {
    case OBCE_CONNECTED:
Connected:
        Assert(qbc->state == CLIENT_TP_CONNECT);
        /* The transport layer connection is up. Send an OBEX Connect. */
        qbc->state = CLIENT_OBEX_CONNECT;
        status = OBEX_ClientConnect(Parms->client);
        break;

    case OBCE_NO_SERVICE_FOUND:
        Assert(qbc->state == CLIENT_TP_CONNECT);
#if IRDA_STACK == XA_ENABLED
        if ((qbc->currTransport == OBEX_TP_IRDA) && (qbc->inst == 0)) {
            /* OBEX IAS Query failed, try IrXfer */ 
            Target.type = OBEX_TP_IRDA;
            Target.proto.ir.iasQuery = irXferQuery;
            Target.proto.ir.iasQueryLen = sizeof(irXferQuery);
            status = OBEX_ClientTpConnect(Parms->client, &Target);
            /* Clear out the transport, so we won't retry the IAS query again */
            qbc->currTransport = OBEX_TP_NONE;
            break;
        }
#endif /* IRDA_STACK == XA_ENABLED */
        App_Report("%s Unable to locate an OBEX Service.", 
            GetName(QB_CLIENT, qbc->inst));
        goto Reset;

    case OBCE_DISCOVERY_FAILED:
        App_Report("%s Unable to discover an OBEX Device.", 
            GetName(QB_CLIENT, qbc->inst));
        goto Reset;

    case OBCE_DISCONNECT:
Disconnected:
        /* The transport layer connection has been disconnected. */
        if (qbc->state == CLIENT_TP_CONNECT)
            App_Report("%s Unable to connect to OBEX server.", 
                        GetName(QB_CLIENT, qbc->inst));
        else {
#if IRDA_STACK == XA_ENABLED
            if (OBEX_IsIrLapConnected()) {
                App_Report("%s The IrLMP service connection has been disconnected.", 
                            GetName(QB_CLIENT, qbc->inst));
            } else {
                App_Report("%s The IrLAP transport connection has been disconnected.", 
                            GetName(QB_CLIENT, qbc->inst));
            }
#else /* IRDA_STACK == XA_ENABLED */
            App_Report("%s The transport connection has been disconnected.", 
                        GetName(QB_CLIENT, qbc->inst));
#endif /* IRDA_STACK == XA_ENABLED */
        }
       
Reset:
        /* Reset progress bar */
        App_Progress(GetUIContext(qbc->inst, QB_CLIENT), 0, 0);
        qbc->state = CLIENT_IDLE;
        return;
        
    case OBCE_COMPLETE:
        /* Reset progress bar */
        App_Progress(GetUIContext(qbc->inst, QB_CLIENT), 0, 0);

        /* The requested operation has completed. */
        switch (qbc->state) {
        case CLIENT_OBEX_CONNECT:
            /* OBEX connection is established, start PUT operation. */
            App_Report("%s OBEX Connect Operation is complete.", 
                       GetName(QB_CLIENT, qbc->inst));
            qbc->state = CLIENT_OBEX_PUT;
            status = ClientPutObject(qbc->inst);
            break;
           
        case CLIENT_OBEX_PUT:
            Assert(qbc->object);
            OBSTORE_Close(&qbc->object);

            /* OBEX PUT is complete, send OBEX Disconnect. */
            App_Report("%s OBEX Put Operation is complete.", 
                        GetName(QB_CLIENT, qbc->inst));
            qbc->state = CLIENT_OBEX_DISCON;
            status = OBEX_ClientDisconnect(Parms->client);
            break;
            
        case CLIENT_OBEX_DISCON:
            /* OBEX Disconnect is complete, disconnect link. */
            App_Report("%s OBEX Disconnect Operation is complete.", 
                        GetName(QB_CLIENT, qbc->inst));
            qbc->state = CLIENT_TP_DISCON;
#if OBEX_AUTHENTICATION == XA_ENABLED
            /* OBEX Disconnection. We are no longer authenticated */
            Qb[qbc->inst].authenticated = FALSE;
#endif /* OBEX_AUTHENTICATION == XA_ENABLED */
            status = OBEX_ClientTpDisconnect(Parms->client, FALSE);
            break;

        default:
            Assert(0);
            break;
        }
        break;

    case OBCE_ABORTED:
        /* The requested operation has failed. Alert the user and cleanup. */
        App_Report("%s The Operation was aborted, \"%s\".", 
                    GetName(QB_CLIENT, qbc->inst), ClientAbortMsg(Parms->client));

        if (OBEX_ClientGetAbortReason(Parms->client) != OBRC_LINK_DISCONNECT) {
            /* Disconnect the link */
            qbc->state = CLIENT_TP_DISCON;
            status = OBEX_ClientTpDisconnect(Parms->client, FALSE);
            break;
        }
        /* Reset progress bar */
        App_Progress(GetUIContext(qbc->inst, QB_CLIENT), 0, 0);
        return;

    case OBCE_HEADER_RX:
        if ((qbc->state == CLIENT_OBEX_GET) && 
            (Parms->u.headerRx.type == OBEXH_LENGTH)) {

            /* Set the incoming object length so the progress meter works. */
            OBSTORE_SetObjectLen(qbc->object, Parms->u.headerRx.value);
        }
#if OBEX_AUTHENTICATION == XA_ENABLED
        if (Parms->u.headerRx.type == OBEXH_AUTH_CHAL) {
            if (OBEXH_ParseAuthChallenge(&qbc->clientApp.handle, &qbc->rcvdChal)) {
                /* Respond to the Authentication Challenge */
                QbAuthenticateResp(qbc->inst, QB_CLIENT);
            }
        } else if (Parms->u.headerRx.type == OBEXH_AUTH_RESP) {
            if (OBEXH_ParseAuthResponse(&qbc->clientApp.handle, &qbc->rcvdResp)) {
                /* Validate the Authentication Response */            
                verify.nonce = qbc->clientNonce;
                verify.digest = qbc->rcvdResp.digest;
                verify.password = Qb[qbc->inst].password;
                verify.passwordLen = (U8)StrLen(Qb[qbc->inst].password);

                if (OBEXH_VerifyAuthResponse(&verify)) {
                    /* Validated - we are not authenticated */
                    Qb[qbc->inst].authenticated = TRUE;
                    App_Report("%s OBEX Authentication Succeeded.", 
                               GetName(QB_CLIENT, qbc->inst));
                } else {
                    /* Authentication failed */
                    App_Report("%s OBEX Authentication Failed.", 
                               GetName(QB_CLIENT, qbc->inst));
                }
            }
        }
#endif /* OBEX_AUTHENTICATION == XA_ENABLED */
        return;

#if OBEX_PACKET_FLOW_CONTROL == XA_ENABLED
      case OBCE_SEND_COMMAND:
         OBEX_ClientSendCommand(Parms->client);
         break;
#endif /* OBEX_PACKET_FLOW_CONTROL == XA_ENABLED */

    default:
        App_Report("%s Event %d ignored.", GetName(QB_CLIENT, qbc->inst), Parms->event);
        return;
    }

    /* Check the status of the new operation */
    if ((status != OB_STATUS_PENDING) && (status != OB_STATUS_SUCCESS)) {
        App_Report("%s Operation failed to start, reason \"%s\".", 
                    GetName(QB_CLIENT, qbc->inst), pObStatus(status));

        if ((qbc->state == CLIENT_OBEX_PUT) || (qbc->state == CLIENT_OBEX_DISCON)) {
            qbc->state = CLIENT_TP_DISCON;
            status = OBEX_ClientTpDisconnect(Parms->client, FALSE);
        }
    }

    if (status == OB_STATUS_SUCCESS) {
        if (qbc->state == CLIENT_TP_CONNECT) {
            /* Now we can connect our OBEX connection */
            goto Connected;
        } 
        else if (qbc->state == CLIENT_TP_DISCON) {
            goto Disconnected;
        }
    }
}

/*---------------------------------------------------------------------------
 *            QbClientExchangeHandler
 *---------------------------------------------------------------------------
 *
 * Synopsis:  This function processes Application events for the Business
 *            Card Exchange Client.
 *
 * Return:    void
 *
 */
static void QbClientExchangeHandler(ObexClientCallbackParms *Parms)
{
    QbClientData  *qbc = ContainingRecord(Parms->client, QbClientData, clientApp);
    ObStatus       status = OB_STATUS_PENDING;
    
    if (Parms->event == OBCE_COMPLETE) {
        /* Reset progress bar */
        App_Progress(GetUIContext(qbc->inst, QB_CLIENT), 0, 0);

        if (qbc->state == CLIENT_OBEX_PUT) {
            /* Put of our business card is complete, now Get theirs. */
            App_Report("%s OBEX Put Operation is complete.", GetName(QB_CLIENT, qbc->inst));

            Assert(qbc->object);
            OBSTORE_Close(&qbc->object);

            qbc->objectName = "";
            qbc->objectType = "text/x-vCard";

            qbc->state = CLIENT_OBEX_GET;
            status = ClientGetObject(qbc->inst);
        } 
        else if (qbc->state == CLIENT_OBEX_GET) {
            /* Put of our business card is complete, now Get theirs. */
            App_Report("%s OBEX Get Operation is complete.", GetName(QB_CLIENT, qbc->inst));

            Assert(qbc->object);
            OBSTORE_Close(&qbc->object);

            qbc->state = CLIENT_OBEX_DISCON;
            status = OBEX_ClientDisconnect(Parms->client);
        } 
        else goto PassOn;

        /* Check the status of the new operation */
    if ((status != OB_STATUS_PENDING) && (status != OB_STATUS_SUCCESS)) {
            App_Report("%s Operation failed to start, reason \"%s\".", 
                       GetName(QB_CLIENT, qbc->inst), pObStatus(status));
            qbc->state = CLIENT_TP_DISCON;
            status = OBEX_ClientTpDisconnect(Parms->client, FALSE);
            if (status == OB_STATUS_SUCCESS) {
                App_Report("%s The transport connection has been disconnected.", 
                            GetName(QB_CLIENT, qbc->inst));
       
                /* Reset progress bar */
                App_Progress(GetUIContext(qbc->inst, QB_CLIENT), 0, 0);
                qbc->state = CLIENT_IDLE;
            }
        }
        return;
    }
   
PassOn:
    /* The Exchange handler is built on top of the Push handler */
    QbClientPushHandler(Parms);
}

/*---------------------------------------------------------------------------
 *            QbClientDefaultHandler
 *---------------------------------------------------------------------------
 *
 * Synopsis:  This function processes Application events for the Client.
 *
 * Return:    void
 *
 */
static void QbClientDefaultHandler(ObexClientCallbackParms *Parms)
{
    QbClientData        *qbc = ContainingRecord(Parms->client, QbClientData, clientApp);
#if IRDA_STACK == XA_ENABLED
    ObexTpAddr      Target;
    ObStatus        status;
#endif /* IRDA_STACK == XA_ENABLED */
#if (OBEX_SESSION_SUPPORT == XA_ENABLED)
    QbClientSessionData *qbcSession;
#endif /* (OBEX_SESSION_SUPPORT == XA_ENABLED) */
#if OBEX_AUTHENTICATION == XA_ENABLED
    ObexAuthResponseVerify  verify;
#endif /* OBEX_AUTHENTICATION == XA_ENABLED */

    switch (Parms->event) {
    case OBCE_CONNECTED:
#if IRDA_STACK == XA_ENABLED
Connected:
#endif /* IRDA_STACK == XA_ENABLED */
        /* A Transport Layer Connection has been established. */
        App_Report("%s Transport Layer connection established.", 
                    GetName(QB_CLIENT, qbc->inst));
        qbc->state = CLIENT_CONNECTED;
        break;

    case OBCE_NO_SERVICE_FOUND:
#if IRDA_STACK == XA_ENABLED
        if ((qbc->currTransport == OBEX_TP_IRDA) && (qbc->inst == 0)) {
            /* OBEX IAS Query failed, try IrXfer */ 
            Target.type = OBEX_TP_IRDA;
            Target.proto.ir.iasQuery = irXferQuery;
            Target.proto.ir.iasQueryLen = sizeof(irXferQuery);
            status = OBEX_ClientTpConnect(Parms->client, &Target);
            /* Clear out the transport, so we won't retry the IAS query again */
            qbc->currTransport = OBEX_TP_NONE;
            /* Check the status of the new operation */
            if (status == OB_STATUS_SUCCESS) 
                goto Connected;
            else if (status == OB_STATUS_PENDING) 
                break;
        }
#endif /* IRDA_STACK == XA_ENABLED */
        App_Report("%s Unable to locate an OBEX Service.", 
            GetName(QB_CLIENT, qbc->inst));
        goto Reset;

    case OBCE_DISCOVERY_FAILED:
        App_Report("%s Unable to discover an OBEX Device.", 
            GetName(QB_CLIENT, qbc->inst));
        goto Reset;

    case OBCE_DISCONNECT:
        /* The transport layer connection has been disconnected. */
        if (qbc->state == CLIENT_TP_CONNECT)
            App_Report("%s Unable to connect to OBEX server.", 
                        GetName(QB_CLIENT, qbc->inst));
        else {
#if IRDA_STACK == XA_ENABLED
            if (OBEX_IsIrLapConnected()) {
                App_Report("%s The IrLMP service connection has been disconnected.", 
                            GetName(QB_CLIENT, qbc->inst));
            } else {
                App_Report("%s The IrLAP transport connection has been disconnected.", 
                            GetName(QB_CLIENT, qbc->inst));
            }
#else /* IRDA_STACK == XA_ENABLED */
            App_Report("%s The transport connection has been disconnected.", 
                        GetName(QB_CLIENT, qbc->inst));
#endif /* IRDA_STACK == XA_ENABLED */
        }
Reset:
        /* Reset progress bar */
        App_Progress(GetUIContext(qbc->inst, QB_CLIENT), 0, 0);
        qbc->state = CLIENT_IDLE;
        break;
        
    case OBCE_COMPLETE:
        App_Progress(GetUIContext(qbc->inst, QB_CLIENT), 0, 0);

        switch (qbc->state) {
        case CLIENT_OBEX_CONNECT:
            App_Report("%s OBEX Connect Operation is complete.", 
                        GetName(QB_CLIENT, qbc->inst));
            break;

        case CLIENT_OBEX_DISCON:
            /* OBEX Disconnect is complete, disconnect link. */
            App_Report("%s OBEX Disconnect Operation is complete.", 
                        GetName(QB_CLIENT, qbc->inst));
#if OBEX_AUTHENTICATION == XA_ENABLED
            /* OBEX Disconnection. We are no longer authenticated */
            Qb[qbc->inst].authenticated = FALSE;
#endif /* OBEX_AUTHENTICATION == XA_ENABLED */
            break;

        case CLIENT_OBEX_PUT:
            App_Report("%s OBEX Put Operation is complete.", 
                        GetName(QB_CLIENT, qbc->inst));
            goto ObsClose;

        case CLIENT_OBEX_GET:
            App_Report("%s OBEX Get Operation is complete.", 
                        GetName(QB_CLIENT, qbc->inst));
ObsClose:
            if (qbc->object)
                OBSTORE_Close(&qbc->object);
            break;

#if OBEX_SESSION_SUPPORT == XA_ENABLED
        case CLIENT_OBEX_CLOSE_SESSION:
            App_Report("%s OBEX Close Session Operation is complete.", 
                        GetName(QB_CLIENT, qbc->inst));

            QbClientSession[qbc->currSession].state = QB_SESSION_CLOSED;

            /* If we closed the active session, remove the reference. */
            if (qbc->activeSession == qbc->currSession)
                qbc->activeSession = NUM_SESSIONS;
            break;

        case CLIENT_OBEX_CREATE_SESSION:
            App_Report("%s OBEX Create Session Operation is complete.", 
                        GetName(QB_CLIENT, qbc->inst));

            qbc->activeSession = qbc->currSession;
            QbClientSession[qbc->activeSession].state = QB_SESSION_ACTIVE;
            break;

        case CLIENT_OBEX_SUSPEND_SESSION:
            App_Report("%s OBEX Suspend Session Operation is complete.", 
                        GetName(QB_CLIENT, qbc->inst));

            ClientSuspendSession(qbc, TRUE);
            break;

        case CLIENT_OBEX_RESUME_SESSION:
            App_Report("%s OBEX Resume Session Operation is complete.", 
                        GetName(QB_CLIENT, qbc->inst));

            qbc->activeSession = qbc->currSession;
            QbClientSession[qbc->activeSession].state = QB_SESSION_ACTIVE;
            break;

        case CLIENT_OBEX_SET_TIMEOUT:
            App_Report("%s OBEX Set Timeout Operation is complete.", 
                        GetName(QB_CLIENT, qbc->inst));
            break;
#endif /* OBEX_SESSION_SUPPORT == XA_ENABLED */

        default:
            /* The requested operation has completed. */
            App_Report("%s The current operation is complete.", 
                        GetName(QB_CLIENT, qbc->inst));
            break;
        }
        qbc->state = CLIENT_IDLE;
        break;
        
    case OBCE_ABORTED:
        /* The requested operation was aborted. */
        App_Report("%s The Operation was aborted, \"%s\".", 
                    GetName(QB_CLIENT, qbc->inst), 
                    ClientAbortMsg(Parms->client));
        
        if (qbc->object)
            OBSTORE_Close(&qbc->object);

        /* Reset progress bar */
        App_Progress(GetUIContext(qbc->inst, QB_CLIENT), 0, 0);
        break;
        
    case OBCE_HEADER_RX:
        if ((qbc->state == CLIENT_OBEX_GET) && 
            (Parms->u.headerRx.type == OBEXH_LENGTH)) {
            /* Set the incoming object length so the progress meter works. */
            OBSTORE_SetObjectLen(qbc->object, Parms->u.headerRx.value);
        }
#if OBEX_AUTHENTICATION == XA_ENABLED
        if (Parms->u.headerRx.type == OBEXH_AUTH_CHAL) {
            if (OBEXH_ParseAuthChallenge(&qbc->clientApp.handle, &qbc->rcvdChal)) {
                /* Respond to the Authentication Challenge */
                QbAuthenticateResp(qbc->inst, QB_CLIENT);
            }
        } else if (Parms->u.headerRx.type == OBEXH_AUTH_RESP) {
            if (OBEXH_ParseAuthResponse(&qbc->clientApp.handle, &qbc->rcvdResp)) {
                /* Validate the Authentication Response */            
                verify.nonce = qbc->clientNonce;
                verify.digest = qbc->rcvdResp.digest;
                verify.password = Qb[qbc->inst].password;
                verify.passwordLen = (U8)StrLen(Qb[qbc->inst].password);

                if (OBEXH_VerifyAuthResponse(&verify)) {
                    /* Validated - we are not authenticated */
                    Qb[qbc->inst].authenticated = TRUE;
                    App_Report("%s OBEX Authentication Succeeded.", 
                               GetName(QB_CLIENT, qbc->inst));
                } else {
                    /* Authentication failed */
                    App_Report("%s OBEX Authentication Failed.", 
                               GetName(QB_CLIENT, qbc->inst));
                }
            }
        }
#endif /* OBEX_AUTHENTICATION == XA_ENABLED */
        break;

#if OBEX_PACKET_FLOW_CONTROL == XA_ENABLED
    case OBCE_SEND_COMMAND:
        OBEX_ClientSendCommand(Parms->client);
        break;
#endif /* OBEX_PACKET_FLOW_CONTROL == XA_ENABLED */

#if (OBEX_SESSION_SUPPORT == XA_ENABLED)

    case OBCE_SESSION_SUSPENDED:
        App_Report("%s Received a Suspend Session event.", GetName(QB_CLIENT, qbc->inst));

        Assert(Parms->u.session == &QbClientSession[qbc->activeSession].session);
        ClientSuspendSession(qbc, TRUE);
        break;
      
    case OBCE_SESSION_PARMS_RX:
#if XA_DEBUG == XA_ENABLED
        ReportSessionParms(Parms->u.sessionParms);
#endif
/*      App_Report("%s Received Session Parms.", GetName(QB_CLIENT, qbc->inst)); */
        break;

    case OBCE_SUSPENDED:
        App_Report("%s %s Operation is suspended.", GetName(QB_CLIENT, qbc->inst),
                   ClientOpName(qbc->state));
        Assert(Parms->u.session == &QbClientSession[qbc->activeSession].session);

        qbcSession = ContainingRecord(Parms->u.session, QbClientSessionData, session);
        
        ClientSuspendSession(qbc, FALSE);

        /* Need to save all information provided by OBEX in the callback
         * parameters so we can resume the suspended operation later
         * when the session is resumed.
         */
        if (Parms->u.suspended.obsh) {
            /* Save the Object Name and current operation. */
            Assert(qbc->objectName);
            OS_MemCopy(qbcSession->objectName, qbc->objectName, (U16)(StrLen(qbc->objectName)+1));
            qbcSession->objectLen = qbc->object->fileLen;
        }
        if (Parms->u.suspended.headerLen) {
            /* Save the current header information. */
            OS_MemCopy(qbcSession->headerBlock, Parms->u.suspended.headerBuff, 
                       Parms->u.suspended.headerLen);
            qbcSession->headerLen = Parms->u.suspended.headerLen;
        }

        if (qbc->object)
            OBSTORE_Close(&qbc->object);

        qbc->state = CLIENT_IDLE;
        break;
        
    case OBCE_RESUME_OPER:
        qbc->state = CLIENT_OBEX_CONNECT + Parms->opcode;
        App_Report("%s Resuming %s operation.", GetName(QB_CLIENT, qbc->inst), ClientOpName(qbc->state));
        break;

#endif /* OBEX_SESSION_SUPPORT == XA_ENABLED */

    default:
        App_Report("%s Event %d ignored.", GetName(QB_CLIENT, qbc->inst), Parms->event);
        break;
    }
    return;
}

/*---------------------------------------------------------------------------
 *            QbClientCallback
 *---------------------------------------------------------------------------
 *
 * Synopsis:  This function dispatches OBEX client protocol events to the 
 *            proper handler.
 *
 * Return:    void
 *
 */
static void QbClientCallback(ObexClientCallbackParms *Parms)
{
    QbClientData *qbc = ContainingRecord(Parms->client, QbClientData, clientApp);

    Assert(qbc->handler);
    Assert(IsObexLocked());

   (*qbc->handler)(Parms);
}

/*---------------------------------------------------------------------------
 *            ClientPutObject
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Creates and initializes Object Store entry which describes
 *            object that OBEX is to put.
 *
 * Return:    OB_STATUS_FAILED - File could not be opened or put.    
 *            Otherwise the operation Started Successfully.
 */
static ObStatus ClientPutObject(U8 QbInst)
{
    ObStatus        status;
    ObStoreHandle   object;          /* Temp object store handle */
    U16             unicodeName[64];
    U16             nameSize;
    U16             len;

    App_Report("%s Starting Put operation of file \"%s\".", 
               GetName(QB_CLIENT, QbInst), QbClient[QbInst].objectName);

    /* Get a new object store entry and set the name. */
    object = OBSTORE_New();
    if (object == 0) {
        return OB_STATUS_FAILED;
    }

    len = StrLen(QbClient[QbInst].objectName);
    OBSTORE_AppendNameAscii(object, QbClient[QbInst].objectName, len);

    /* Open the Object store item for reading. */
    if (OBSTORE_Open(object, FALSE) != OBRC_SUCCESS) {
        App_Report("%s OBSTORE_Open Failed!", GetName(QB_CLIENT, QbInst));
        OBSTORE_Close(&object);
        return OB_STATUS_FAILED;
    }
    OBSTORE_SetUIContext(object, GetUIContext(QbInst, QB_CLIENT));

    nameSize = OBSTORE_GetNameLenUnicode(object);
    Assert(nameSize < (64*sizeof(U16)));
         
    /* Read the Unicode name into temp storage */
    OBSTORE_ReadNameUnicode(object, (U8 *)unicodeName,  nameSize, TRUE);
   
    /* Create the Name header for the PUT request. */
    OBEXH_BuildUnicode(&QbClient[QbInst].clientApp.handle, OBEXH_NAME, 
                        (U8 *)unicodeName, nameSize);
  
    /* Create the Length header for the PUT request. */
    OBEXH_Build4Byte(&QbClient[QbInst].clientApp.handle, OBEXH_LENGTH, 
                        OBSTORE_GetObjectLen(object));
   
    /* Instruct OBEX to Put the Object */
    status = OBEX_ClientPut(&QbClient[QbInst].clientApp, object);

    if (status != OB_STATUS_PENDING)
        OBSTORE_Close(&object);
    else QbClient[QbInst].object = object;

    return status;
}

/*---------------------------------------------------------------------------
 *            ClientDeleteObject
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Send delete request to remote device.
 *
 * Return:    OB_STATUS_FAILED - Operation could not be started.
 *            Otherwise the operation Started Successfully.
 *
 */
static ObStatus ClientDeleteObject(U8 QbInst)
{
    ObStoreHandle   object;          /* Temp object store handle */
    U16             unicodeName[64];
    U16             nameSize;
    U16             len;

    App_Report("%s Starting put-delete operation of file \"%s\".", 
               GetName(QB_CLIENT, QbInst), QbClient[QbInst].objectName);
               
    /* We just use an object store item temporarily because the
     * name conversion functions we need operate on object store
     * handles. The object store item is never sent.
     */
    object = OBSTORE_New();
    if (object == 0) {
        return OB_STATUS_FAILED;
    }
    
    len = StrLen(QbClient[QbInst].objectName);
    OBSTORE_AppendNameAscii(object, QbClient[QbInst].objectName, len);
      
    nameSize = OBSTORE_GetNameLenUnicode(object);
    Assert(nameSize < (64*sizeof(U16)));
         
    /* Read the Unicode name into temp storage */
    OBSTORE_ReadNameUnicode(object, (U8 *)unicodeName,  nameSize, TRUE);
   
    /* Create the Name header for the Delete request. */
    OBEXH_BuildUnicode(&QbClient[QbInst].clientApp.handle, OBEXH_NAME, 
                        (U8 *)unicodeName, nameSize);
   
    OBSTORE_Close(&object);

    /* 
     * Instruct OBEX to send a Put-delete operation. This is
     * done by sending a put operation without any object body.
     */
    return OBEX_ClientPut(&QbClient[QbInst].clientApp, 0);
}


/*---------------------------------------------------------------------------
 *            ClientGetObject
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Creates and initializes Object Store entry which describes
 *            file that OBEX is to get.
 *
 * Return:    OB_STATUS_FAILED - Operation could not be started.
 *            Otherwise the operation Started Successfully.
 *
 */
static ObStatus ClientGetObject(U8 QbInst)
{
    ObStoreHandle   object;          /* Temp object store handle */
    ObStatus        status;
    U16             unicodeName[64];
    U16             nameSize;
    U16             len;
    char            *objectName;

    object = 0;
    /* Clear the Object Type */
    QbClient[QbInst].objectType = "";

    len = StrLen(QbClient[QbInst].objectName);

    if (len > 0) { 
        App_Report("%s Starting Get operation for object \"%s\".", 
                   GetName(QB_CLIENT, QbInst), QbClient[QbInst].objectName);
    }
    else App_Report("%s Starting Get operation for default vCard object.", 
                    GetName(QB_CLIENT, QbInst));

    /* Create an object store item for writing and set it's name */
    object = OBSTORE_New();
    if (object == 0) {
        return OB_STATUS_FAILED;
    }
    OBSTORE_SetUIContext(object, GetUIContext(QbInst, QB_CLIENT));
    
    if (len > 0) {
        /* A file name exists so put into the freshly created
         * object store entry.
         */
        OBSTORE_AppendNameAscii(object, QbClient[QbInst].objectName, len);
        nameSize = OBSTORE_GetNameLenUnicode(object);
        Assert(nameSize < (64*sizeof(U16)));
         
        /* Read the Unicode name into temp storage */
        OBSTORE_ReadNameUnicode(object, (U8 *)unicodeName, nameSize, TRUE);
   
        /* Create the Name header for the GET request. */
        OBEXH_BuildUnicode(&QbClient[QbInst].clientApp.handle, OBEXH_NAME, 
                            (U8 *)unicodeName, nameSize);
    } else {      
        /* Set the Object Type */
        QbClient[QbInst].objectType = "text/x-vCard";
        objectName = App_GetSavedObject();
        if (objectName) {
            /* Ask for the default object name */
            OBSTORE_AppendNameAscii(object, objectName, StrLen(objectName));
        } else {
            /* No name provided so use the default object name */
            OBSTORE_SetNameDefault(object);
        }
        /* Create the Name header for the GET default request. */
        OBEXH_BuildUnicode(&QbClient[QbInst].clientApp.handle, OBEXH_NAME, 0, 0);
    }

    if (QbClient[QbInst].objectType) {
        /* Add a Type header */
        OBEXH_BuildByteSeq(&QbClient[QbInst].clientApp.handle, OBEXH_TYPE, 
                           (U8 *)QbClient[QbInst].objectType, 
                           (U16)(StrLen(QbClient[QbInst].objectType)+1));
    }

    if (OBSTORE_Create(object, FALSE) != OBRC_SUCCESS) {
        App_Report("%s OBSTORE_Create() failed.", GetName(QB_CLIENT, QbInst));
        OBEXH_FlushHeaders(&QbClient[QbInst].clientApp.handle);
        OBSTORE_Close(&object);
        return OB_STATUS_FAILED;
    }
   
    status = OBEX_ClientGet(&QbClient[QbInst].clientApp, object, FALSE );

    if (status != OB_STATUS_PENDING) {
        OBSTORE_Delete(&object, FALSE);
    }
    QbClient[QbInst].object = object;

    return status;

}

/*---------------------------------------------------------------------------
 *            ClientSetPath
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Creates and initializes Object Store entry which describes
 *            path that OBEX will set.
 *
 * Return:    TRUE  - Operation Started Successfully.
 *            FALSE - Bad path or request.
 *
 */
static ObStatus ClientSetPath(U8 QbInst, BOOL Backup, BOOL NoCreate)
{
    char         unicodeName[200];
    char        *ptr;
    ObStatus     status;
    U16          len;
    U8           flags;
    const char  *path;

    path = QbClient[QbInst].pathName;
    if (path)
        len = StrLen(QbClient[QbInst].pathName);
    else len = 0;
        
    if (len > 0) {
        App_Report("%s Starting set path operation for path \"%s\".", 
        GetName(QB_CLIENT, QbInst), QbClient[QbInst].pathName);
   
        /* First check if their are any "backups" appended to path */
        while (len > 1) {
            /* Check for Backup in path (ex. "..", "", "foo" */
            if (path[0] == '.' && path[1] == '.') {
                /* Strip off backup string */
                if (path[2] == '\\') {
                    path += 3;
                    len -= 3;
                } else if (path[2] == 0) {
                    path += 2;
                    len -= 2;
                } else {
                    /* Invalid path specified (ex. "..foo") */
                    return OB_STATUS_FAILED;
                }
                Backup = TRUE;
            }
            else break;
        }
   
        if (len > 0) {
            /* Convert remaining path to Unicode */
            ptr = unicodeName;
            while (*path != 0) {
                *ptr++ = 0;
                *ptr++ = *path++;
            }
            /* Null Terminate Unicode path */
            *ptr++ = 0;
            *ptr++ = 0;
            len++;
   
            OBEXH_BuildUnicode(&QbClient[QbInst].clientApp.handle, OBEXH_NAME, 
                                (U8 *)unicodeName, (U8)(len * 2) );
        }
    } else if (len == 0 && !Backup && !NoCreate) {
        /* Root Folder Reset was requested */
        App_Report("%s Starting set path operation - Root Folder Reset.", GetName(QB_CLIENT, QbInst));
        /* Path reset includes an empty name header */
        OBEXH_BuildUnicode(&QbClient[QbInst].clientApp.handle, OBEXH_NAME, 0, 0);
    }

    flags = (Backup ? OSPF_BACKUP : OSPF_NONE);
    flags |= (NoCreate ? OSPF_DONT_CREATE : OSPF_NONE);

    status = OBEX_ClientSetPath(&QbClient[QbInst].clientApp, flags);
    return status;
}


#if OBEX_SESSION_SUPPORT == XA_ENABLED
/*---------------------------------------------------------------------------
 *            ClientResumeObject
 *---------------------------------------------------------------------------
 *
 * Synopsis:  
 *            This function works for both PUT and GET operation resume.
 *            
 *
 * Return:    OB_STATUS_FAILED - The object could not be re-opened.
 *
 *            OB_STATUS_SUCCESS - The object was re-opened successfully.
 */
static ObStatus ClientResumeObject(U8 QbInst)
{
    QbClientData    *qbc = &QbClient[QbInst];
    ObStoreHandle    object;      /* Temp object store handle */
    U16              len;

    qbc->object = 0;

    if ((len = StrLen(qbc->objectName)) > 0)
        App_Report("%s Resuming operation for object \"%s\".",
                   GetName(QB_CLIENT, QbInst), qbc->objectName);
    else App_Report("%s Resuming operation for unnamed object.", 
                    GetName(QB_CLIENT, QbInst));

    /* Create an object store item for writing and set it's name */
    object = OBSTORE_New();
    if (object == 0) {
        return OB_STATUS_FAILED;
    }
    OBSTORE_SetUIContext(object, GetUIContext(QbInst, QB_CLIENT));
    
    if (len > 0) {
        /* A file name exists so put into the freshly created
         * object store entry.
         */
        OBSTORE_AppendNameAscii(object, qbc->objectName, len);
    } else {
        /* No name so use the default object name */
        OBSTORE_SetNameDefault(object);
    }

    /* Open the Object store item for reading. */
    if (OBSTORE_Open(object, FALSE) != OBRC_SUCCESS) {
        App_Report("%s OBSTORE_Open Failed!", GetName(QB_CLIENT, QbInst));
        OBSTORE_Close(&object);
        return OB_STATUS_FAILED;
    }
    qbc->object = object;

    /* Restore the ObStore object length for the progress meters. */
    OBSTORE_SetObjectLen(qbc->object, QbClientSession[qbc->currSession].objectLen);

    return OB_STATUS_SUCCESS;
}


/*---------------------------------------------------------------------------
 *            ClientSuspendSession
 *---------------------------------------------------------------------------
 */
static void ClientSuspendSession(QbClientData *QbClient, BOOL Reset)
{
    QbClientSessionData *qbcSession;

    /* Set Suspend Timer for the active (now suspended) session. */
    Assert(QbClient->activeSession < NUM_SESSIONS);
    Assert(QbClientSession[QbClient->activeSession].state == QB_SESSION_ACTIVE);
    
    qbcSession = &QbClientSession[QbClient->activeSession];

    qbcSession->state = QB_SESSION_SUSPENDED;

    if (Reset) {
        /* Clear these values so we don't mis-resume the session. */
        qbcSession->headerLen = 0;
        qbcSession->objectName[0] = 0;
    }        

    if (qbcSession->session.timeout != OBEX_TIMEOUT_INFINITE) {
        /* Set the timeout for the suspended session. */
        Assert(qbcSession->session.timeout != 0);

        qbcSession->timer.func = SuspendTimerFire;
        qbcSession->timer.context = (void *)QB_CLIENT;

        /* Timeout value is in seconds, but EVM_StartTimer expects milliseconds */
        EVM_StartTimer(&qbcSession->timer, (qbcSession->session.timeout * 1000));
    }

    QbClient->activeSession = NUM_SESSIONS;
}
#endif /* OBEX_SESSION_SUPPORT == XA_ENABLED */


/*---------------------------------------------------------------------------
 *            QbServerCallback
 *---------------------------------------------------------------------------
 *
 * Synopsis:  This function processes OBEX Server protocol events.
 *
 * Return:    void
 *
 */
static void QbServerCallback(ObexServerCallbackParms *parms)
{
    QbServerData   *qbs = ContainingRecord(parms->server, QbServerData, serverApp);
    ObexRespCode    rcode;

#if XA_DEBUG == XA_ENABLED
    qbs->lastEvent = ServerEventVerifier(qbs->lastEvent, parms->event);
#endif /* XA_DEBUG == XA_ENABLED */
    Assert(IsObexLocked());

    switch (parms->event) {
    case OBSE_CONNECTED:
        App_Report("%s Transport Connection has come up.", GetName(QB_SERVER, qbs->inst));
        qbs->connState = QB_SERVER_CONN_TP;
        /* A new connection is established. Reset the inbox. */
        OBSTORE_ResetPath();
        break;

    case OBSE_DISCONNECT:
        if (qbs->connState != QB_SERVER_CONN_DISC) {
#if IRDA_STACK == XA_ENABLED
            if (OBEX_IsIrLapConnected()) {
                App_Report("%s IrLMP service connection has been disconnected.", 
                           GetName(QB_SERVER, qbs->inst));
            } else {
                App_Report("%s IrLAP transport connection has been disconnected.", 
                           GetName(QB_SERVER, qbs->inst));
                qbs->connState = QB_SERVER_CONN_DISC;
            }
#else /* IRDA_STACK == XA_ENABLED */
            App_Report("%s Transport Connection has been disconnected.", 
                       GetName(QB_SERVER, qbs->inst));
            qbs->connState = QB_SERVER_CONN_DISC;
#endif /* IRDA_STACK == XA_ENABLED */
        }
        /* Reset progress bar */
        App_Progress(GetUIContext(qbs->inst, QB_SERVER), 0, 0);
        break;

    case OBSE_HEADER_RX:
#if OBEX_AUTHENTICATION == XA_ENABLED
        if (parms->u.headerRx.type == OBEXH_AUTH_CHAL) {
            if (OBEXH_ParseAuthChallenge(&qbs->serverApp.handle, &qbs->rcvdChal)) {
                /* Respond to the Authentication Challenge */
                QbAuthenticateResp(qbs->inst, QB_SERVER);
            }
        } else if (parms->u.headerRx.type == OBEXH_AUTH_RESP) {
            /* This shouldn't happen, since the Qbeam Server is not allowed
             * to initiate authentication at this time.
             */
        }
#endif /* OBEX_AUTHENTICATION == XA_ENABLED */
        ServerProcessHeaders(parms);
        break;

    case OBSE_PROVIDE_OBJECT:
        ServerObjectRequest(FALSE, parms->server);
        break;

    case OBSE_DELETE_OBJECT:
        ServerObjectRequest(TRUE, parms->server);
        break;

    case OBSE_ABORTED:
        if (qbs->operation == SERVER_SETPATH) {
            /* Cleanup any path changes that were started */
            if (qbs->newPath == FALSE)
                OBSTORE_CancelPathUpdate();
        }

        if (qbs->object)
            OBSTORE_Close(&qbs->object);
                 
        App_Report("%s %s operation aborted.", GetName(QB_SERVER, qbs->inst), 
                   ServerOpName(qbs->operation));

        /* Reset progress bar */
        App_Progress(GetUIContext(qbs->inst, QB_SERVER), 0, 0);
        qbs->operation = SERVER_IDLE;
        break;

    case OBSE_PRECOMPLETE:
        if (qbs->operation == SERVER_SETPATH) {
            /* If QbServer.newPath is false we've called AppendPathUnicode().
             * Now apply the new path.
             */
             if ((qbs->newPath == FALSE) || 
                 (qbs->flags != OSPF_NONE)) {
                 rcode = OBSTORE_UpdatePath(qbs->flags, TRUE);
                 if (rcode != OBRC_SUCCESS) {
                     /* Change dir failed, return error to client */
                     OBEX_ServerAbort(&qbs->serverApp, rcode);
                 }
             }
        } 
        else if (qbs->operation == SERVER_PUT) {
            if (qbs->object) {
                /* See if we can successfully close the file we've received */
                rcode = OBSTORE_Close(&qbs->object);
                if (rcode != OBRC_SUCCESS)
                    OBEX_ServerAbort(&qbs->serverApp, rcode);
            }
        }
        break;

    case OBSE_COMPLETE:
        switch (qbs->operation) {
        case SERVER_CONNECT:
            /* Were connected at the OBEX layer */
            qbs->connState = QB_SERVER_CONN_OBEX;
            break;

        case SERVER_DISCON:
            /* Its an OBEX layer disconnect */
            qbs->connState = QB_SERVER_CONN_TP;
            break;

        case SERVER_GET:
            /* Only GET still needs to close */
            if (qbs->object) {
                rcode = OBSTORE_Close(&qbs->object);
                Assert(rcode == OBRC_SUCCESS);
            }
            break;

#if (OBEX_SESSION_SUPPORT == XA_ENABLED)
        case SERVER_CLOSE_SESSION:
            /* Now that the close operation has completed, close the session. */
            QbServerSession[qbs->currSession].state = QB_SESSION_CLOSED;
            EVM_CancelTimer(&QbServerSession[qbs->currSession].timer);

            /* If we closed the active session, remove our reference to it. */
            if (qbs->currSession == qbs->activeSession)
                qbs->activeSession = NUM_SESSIONS;
            break;

        case SERVER_SUSPEND_SESSION:
            /* Now that the suspend operation has completed, suspend the session. */
            Assert(qbs->activeSession < NUM_SESSIONS);
            ServerSuspendSession(qbs, TRUE);
            break;
#endif /* (OBEX_SESSION_SUPPORT == XA_ENABLED) */
        }

        App_Report("%s %s operation complete.", GetName(QB_SERVER, qbs->inst), 
                   ServerOpName(qbs->operation));
        App_Progress(GetUIContext(qbs->inst, QB_SERVER), 0, 0);
        qbs->operation = SERVER_IDLE;
        break;

    case OBSE_PUT_START:
        App_Report("%s Receiving a Put operation.", GetName(QB_SERVER, qbs->inst));
        qbs->operation = SERVER_PUT;
        qbs->object = OBSTORE_New();
        break;

    case OBSE_GET_START:
        App_Report("%s Receiving a Get operation.", GetName(QB_SERVER, qbs->inst));
        qbs->operation = SERVER_GET;
        qbs->object = OBSTORE_New();
        break;
    
    case OBSE_SET_PATH_START:
        App_Report("%s Receiving a Set Path operation.", GetName(QB_SERVER, qbs->inst));
        qbs->operation = SERVER_SETPATH;
            
        qbs->newPath = TRUE;
        qbs->flags = (parms->u.setPathFlags & (OSPF_BACKUP|OSPF_DONT_CREATE));
        break;
        
    case OBSE_CONNECT_START:
        App_Report("%s Received a Connect operation.", GetName(QB_SERVER, qbs->inst));
        qbs->operation = SERVER_CONNECT;
        break;
        
    case OBSE_DISCONNECT_START:
        App_Report("%s Received a Disconnect operation.", GetName(QB_SERVER, qbs->inst));
        qbs->operation = SERVER_DISCON;
        break;
            
    case OBSE_ABORT_START:
        App_Report("%s Received an Abort operation.", GetName(QB_SERVER, qbs->inst));
        qbs->operation = SERVER_ABORT;
        break;

#if OBEX_PACKET_FLOW_CONTROL == XA_ENABLED
    case OBSE_SEND_RESPONSE:
        OBEX_ServerSendResponse(&qbs->serverApp);
        break;
#endif /* OBEX_PACKET_FLOW_CONTROL == XA_ENABLED */

#if OBEX_SESSION_SUPPORT == XA_ENABLED
    case OBSE_RESUME_OPER:
        switch (parms->opcode) {
        case OB_OPCODE_PUT:
            App_Report("%s Receiving a Put (Resume) operation.", GetName(QB_SERVER, qbs->inst));
            qbs->operation = SERVER_PUT;
            break;

        case OB_OPCODE_GET:
            App_Report("%s Receiving a Get (Resume) operation.", GetName(QB_SERVER, qbs->inst));
            qbs->operation = SERVER_GET;
            break;
    
        case OB_OPCODE_SET_PATH:
            App_Report("%s Receiving a Set Path (Resume) operation.", GetName(QB_SERVER, qbs->inst));
            qbs->operation = SERVER_SETPATH;
            break;
            
        case OB_OPCODE_CONNECT:
            App_Report("%s Received a Connect (Resume) operation.", GetName(QB_SERVER, qbs->inst));
            qbs->operation = SERVER_CONNECT;
            break;
            
        case OB_OPCODE_DISCONNECT:
            App_Report("%s Received a Disconnect (Resume) operation.", GetName(QB_SERVER, qbs->inst));
            qbs->operation = SERVER_DISCON;
            break;
            
        case OB_OPCODE_ABORT:
            App_Report("%s Received an Abort (Resume) operation.", GetName(QB_SERVER, qbs->inst));
            qbs->operation = SERVER_ABORT;
            break;
        }
        break;

    case OBSE_SESSION_START:
    case OBSE_SESSION_PARMS_RX:
    case OBSE_PROVIDE_SESSION:
    case OBSE_SESSION_SUSPENDED:
    case OBSE_SUSPENDED:
        ServerSessionRequest(parms);
        break;
#endif /* OBEX_SESSION_SUPPORT == XA_ENABLED */

    }
}

/*---------------------------------------------------------------------------
 *            ServerProcessHeaders
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Process the incoming headers based on the current operation.
 *
 * Return:    void
 *
 */
static void ServerProcessHeaders(ObexServerCallbackParms *parms)
{
    U8             *buff;
    U16             len;
    U32             fileLen;
    ObexRespCode    rcode;
    QbServerData   *qbs = ContainingRecord(parms->server, QbServerData, serverApp);

    switch (parms->u.headerRx.type) {
    case OBEXH_LENGTH:
        if ((qbs->operation == SERVER_PUT) || (qbs->operation == SERVER_GET)) {
            Assert(qbs->object);
                
            /* Store the Object Length */
            fileLen = parms->u.headerRx.value;
            OBSTORE_SetObjectLen(qbs->object, fileLen);
        } 
        break;
                
    case OBEXH_NAME:
        len = parms->u.headerRx.currLen;
        buff = parms->u.headerRx.buff;
                
        if (qbs->operation == SERVER_SETPATH) {
            if (parms->u.headerRx.totalLen == 0) {
                /* This must be handled now, Reset Path to default. */
                OBSTORE_ResetPath();
            } else {
                /* Call the Object Store to update the path */
                rcode = OBSTORE_AppendPathUnicode((U8 *)buff, len, qbs->newPath);
                qbs->newPath = FALSE;
                if (rcode != OBRC_SUCCESS) {
                    /* Append dir failed, return error to client */
                    OBEX_ServerAbort(&qbs->serverApp, rcode);
                }
            }
        } 
        else if ((qbs->operation == SERVER_PUT) || (qbs->operation == SERVER_GET)) {
            /*
             * Put and Get Operations:
             * Append the buffer to the object store name.  
             */
            Assert(qbs->object);
            OBSTORE_AppendNameUnicode(qbs->object, (U8 *)buff, len);
        }
        break;

    case OBEXH_TYPE:
        len = parms->u.headerRx.currLen;
        buff = parms->u.headerRx.buff;
        
        Assert(qbs->object);
        OBSTORE_AppendType(qbs->object, buff, len);
        break;
    }
}

/*---------------------------------------------------------------------------
 *            ServerObjectRequest
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Process the parsers request to open/create/delete an object.
 *
 * Return:    void
 *
 */
static void ServerObjectRequest(BOOL DeleteObj, ObexServerApp *app)
{
    ObexRespCode    rcode;
    QbServerData   *qbs = ContainingRecord(app, QbServerData, serverApp);
    
    Assert(qbs->object);
    
    if (DeleteObj) {
        App_Report("%s Object \"%s\" [DELETE].", GetName(QB_SERVER, qbs->inst), 
                   OBSTORE_GetNameAscii(qbs->object));

        rcode = OBSTORE_Delete(&qbs->object, TRUE);
        if (rcode != OBRC_SUCCESS)
            OBEX_ServerAbort(&qbs->serverApp, rcode);

    } else {
        if (qbs->operation == SERVER_GET) {
            rcode = OBSTORE_Open(qbs->object, TRUE);
            /* Be nice, build a LENGTH header for the response */
            if (rcode == OBRC_SUCCESS)
                OBEXH_Build4Byte(&qbs->serverApp.handle, OBEXH_LENGTH, 
                                 OBSTORE_GetObjectLen(qbs->object));
        } else {
            /* PUT Operation */
            rcode = OBSTORE_Create(qbs->object, TRUE);
        }

        if (rcode == OBRC_SUCCESS) {
            OBEX_ServerAccept(&qbs->serverApp, qbs->object);
            OBSTORE_SetUIContext(qbs->object, GetUIContext(qbs->inst, QB_SERVER));
        }
        App_Report("%s Object \"%s\".", GetName(QB_SERVER, qbs->inst), 
                   OBSTORE_GetNameAscii(qbs->object));
    }
}

#if (OBEX_SESSION_SUPPORT == XA_ENABLED)
/*---------------------------------------------------------------------------
 *            ServerResumeObject
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Reopens an object whose transfer (either PUT or GET) was
 *            suspended due to a disconnect while a reliable session was active.
 *
 */
static void ServerResumeObject(QbServerData *Qbs, QbServerSessionData *QbsSession)
{
    U16     len;

    Qbs->object = OBSTORE_New();

    if (Qbs->object) {
        if ((len = StrLen(QbsSession->objectName)) > 0)
            OBSTORE_AppendNameAscii(Qbs->object, QbsSession->objectName, len);

/*      if ((len = StrLen(QbsSession->objectType)) > 0)
 *          OBSTORE_AppendType(Qbs->object, QbsSession->objectType, len);
 */
        if (OBSTORE_Open(Qbs->object, TRUE) != OBRC_SUCCESS)
            OBSTORE_Close(&Qbs->object);
    }

    if (Qbs->object) {
        OBSTORE_SetUIContext(Qbs->object, GetUIContext(Qbs->inst, QB_SERVER));
        App_Report("%s Object \"%s\".", GetName(QB_SERVER, Qbs->inst), 
                   OBSTORE_GetNameAscii(Qbs->object));

        /* Restore the ObStore object length for the progress meters. */
        OBSTORE_SetObjectLen(Qbs->object, QbsSession->objectLen);
    }
}


/*---------------------------------------------------------------------------
 *            ServerSessionRequest
 *---------------------------------------------------------------------------
 *
 * Synopsis:  
 *
 */
static void ServerSessionRequest(ObexServerCallbackParms *Parms)
{
    U8                      i, sessId[16];
    U16                     len;
    ObStatus                status;
    QbServerData           *qbs = ContainingRecord(Parms->server, QbServerData, serverApp);
    QbServerSessionData    *qbsSession;
    ObexSessionResumeParms  params = {0};

    switch (Parms->event) {
    case OBSE_SESSION_START:

        qbs->currSession = NUM_SESSIONS;
        qbs->operation = (Parms->opcode + 1);

        switch (qbs->operation) {
        case SERVER_CREATE_SESSION:
            App_Report("%s Received a Create Session operation.", GetName(QB_SERVER, qbs->inst));
            break;
        case SERVER_CLOSE_SESSION:
            App_Report("%s Received a Close Session operation.", GetName(QB_SERVER, qbs->inst));
            break;
        case SERVER_SUSPEND_SESSION:
            App_Report("%s Received a Suspend Session operation.", GetName(QB_SERVER, qbs->inst));
            break;
        case SERVER_RESUME_SESSION:
            App_Report("%s Received a Resume Session operation.", GetName(QB_SERVER, qbs->inst));
            break;
        case SERVER_SET_TIMEOUT:
            App_Report("%s Received a Set Timeout operation.", GetName(QB_SERVER, qbs->inst));
            break;
        }
        break;

    case OBSE_SESSION_PARMS_RX:
#if XA_DEBUG == XA_ENABLED
        ReportSessionParms(Parms->u.sessionParms);
#endif
        if (qbs->operation == SERVER_RESUME_SESSION) {
            /* Find the session which is being resumed. */
            for (i = 0; i < NUM_SESSIONS; i++) {
                if (QbServerSession[i].state == QB_SESSION_SUSPENDED) {
                    OBEX_CreateSessionId(&QbServerSession[i].session, sessId);
                    if (OS_MemCmp(sessId, 16, Parms->u.sessionParms->sessionId, 16) == TRUE) {
                        break;
                    }
                }
            }
        }
        else if (qbs->operation == SERVER_CLOSE_SESSION) {
            /* Find the session which is being closed. */
            for (i = 0; i < NUM_SESSIONS; i++) {
                if (QbServerSession[i].state != QB_SESSION_CLOSED) {
                    OBEX_CreateSessionId(&QbServerSession[i].session, sessId);
                    if (OS_MemCmp(sessId, 16, Parms->u.sessionParms->sessionId, 16) == TRUE) {
                        break;
                    }
                }
            }
        } else {
/*          App_Report("%s Received Session Parms.", GetName(QB_SERVER, qbs->inst)); */
            break;
        }

        App_Report("%s Received Session Parms, %s matching session.", GetName(QB_SERVER, qbs->inst),
                   (i==NUM_SESSIONS ? "did not find" : "found"));

        if (i == NUM_SESSIONS) {
            if (qbs->operation == SERVER_CLOSE_SESSION) {
                /* We cannot find the correct session to close */
                OBEX_ServerAbort(Parms->server, OBRC_FORBIDDEN);
            } else if (qbs->operation == SERVER_RESUME_SESSION) {
                /* We cannot find the correct session to resume */
                OBEX_ServerAbort(Parms->server, OBRC_SERVICE_UNAVAILABLE);
            }
            break;
        }
        /* Update the current session index. */
        qbs->currSession = i;
        break;

    case OBSE_PROVIDE_SESSION:
        if (qbs->operation == SERVER_RESUME_SESSION) {
            i = qbs->currSession;

            /* Cancel the suspend timer, we've resumed the session */
            EVM_CancelTimer(&QbServerSession[i].timer);

            /* See if there are any parameters from a suspended operatation
             * that need to be restored as part of the session resume. Remember,
             * if there is a suspended operation, it will resume automatically
             * once the Session Resume operation is complete.
             */
            if (StrLen(QbServerSession[i].objectName)) {
/*              StrLen(QbServerSession[i].objectType)) { */
                /* Reopen the object we were exchanging. */
                ServerResumeObject(qbs, &QbServerSession[i]);
                params.obsh = qbs->object;
            }

            if (QbServerSession[i].headerLen) {
                params.headerLen = QbServerSession[i].headerLen;
                params.headerBuff = QbServerSession[i].headerBlock;
            }

            /* Accept the Resume Session request. */
            status = OBEX_ServerAcceptSession(Parms->server, 
                                              &QbServerSession[i].session, &params);
        }
        else if (qbs->operation == SERVER_CREATE_SESSION) {
            for (i = 0; i < NUM_SESSIONS; i++) {
                if (QbServerSession[i].state == QB_SESSION_CLOSED) {
                    /* Found an available session, accept the Create Session request. */
                    qbs->currSession = i;
                    
                    status = OBEX_ServerAcceptSession(Parms->server, 
                                                      &QbServerSession[i].session, 0);
                    break;
                }
            }
            if (i == NUM_SESSIONS) {
                App_Report("%s All Sessions are in use.", GetName(QB_SERVER, qbs->inst));
                break;
            }
        }
        else Assert(0);

        if (status != OB_STATUS_SUCCESS)
            App_Report("%s OBEX_ServerAcceptSession() returned %s.", 
                       GetName(QB_SERVER, qbs->inst), pObStatus(status));

        /* Update the session state to active. */
        QbServerSession[i].state = QB_SESSION_ACTIVE;
        qbs->activeSession = i;
        break;

    case OBSE_SUSPENDED:
        App_Report("%s %s operation suspended (link disconnect).", GetName(QB_SERVER, qbs->inst), 
                   ServerOpName(qbs->operation));

        qbsSession = ContainingRecord(Parms->u.session, QbServerSessionData, session);
        Assert(qbsSession->state == QB_SESSION_ACTIVE);
        Assert(Parms->u.session == &QbServerSession[qbs->activeSession].session);

        ServerSuspendSession(qbs, FALSE);

        /* Need to save all information provided by OBEX in the callback
         * parameters so we can resume the suspended operation later
         * when the session is resumed.
         */
        if (Parms->u.suspended.obsh) {
            /* Save the Object Name and Type for resumption later. */
            len = StrLen(OBSTORE_GetNameAscii(qbs->object)) + 1;
            OS_MemCopy(qbsSession->objectName, OBSTORE_GetNameAscii(qbs->object), len);

/*          len = StrLen(OBSTORE_GetTypeAscii(qbs->object)) + 1;
 *          OS_MemCopy(qbsSession->objectType, OBSTORE_GetTypeAscii(qbs->object), len);
 */
            qbsSession->objectLen = qbs->object->fileLen;
        }
        if (Parms->u.suspended.headerLen) {
            /* Save the current header information. */
            OS_MemCopy(qbsSession->headerBlock, Parms->u.suspended.headerBuff, 
                       Parms->u.suspended.headerLen);
            qbsSession->headerLen = Parms->u.suspended.headerLen;
        }

        if (qbs->object)
            OBSTORE_Close(&qbs->object);

        qbs->operation = SERVER_IDLE;
        break;

    case OBSE_SESSION_SUSPENDED:
        App_Report("%s Received a Suspend Session event.", GetName(QB_SERVER, qbs->inst));
        Assert(Parms->u.session == &QbServerSession[qbs->activeSession].session);
        ServerSuspendSession(qbs, TRUE);
        break;
    }
}


/*---------------------------------------------------------------------------
 *            ServerSuspendSession
 *---------------------------------------------------------------------------
 *
 */
static void ServerSuspendSession(QbServerData *QbServer, BOOL Reset)
{
    QbServerSessionData *qbsSession;

    Assert(QbServer->activeSession < NUM_SESSIONS);
    Assert(QbServerSession[QbServer->activeSession].state == QB_SESSION_ACTIVE);
    
    qbsSession = &QbServerSession[QbServer->activeSession];

    qbsSession->state = QB_SESSION_SUSPENDED;

    if (Reset) {
        /* Clear out possible previous objectName & headerLen values. */
        qbsSession->objectName[0] = 0;
        qbsSession->headerLen = 0;
    }

    if (qbsSession->session.timeout != OBEX_TIMEOUT_INFINITE) {
        /* Set the timeout for the suspended session. */
        Assert(qbsSession->session.timeout != 0);

        qbsSession->timer.func = SuspendTimerFire;
        qbsSession->timer.context = (void *)QB_SERVER;

        /* Timeout value is in seconds, but EVM_StartTimer expects milliseconds */
        EVM_StartTimer(&qbsSession->timer, (qbsSession->session.timeout * 1000));
    }

    QbServer->activeSession = NUM_SESSIONS;
}
#endif /* (OBEX_SESSION_SUPPORT == XA_ENABLED) */


/*---------------------------------------------------------------------------
 *            ServerOpName
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Return a pointer to the name of the current server operation.
 *
 * Return:    ASCII String pointer.
 *
 */
static const char *ServerOpName(U8 operation)
{
    switch (operation) {
    case SERVER_PUT:
        return "Put";
    case SERVER_GET:
        return "Get";
    case SERVER_SETPATH:
        return "Set Path";
    case SERVER_CONNECT:
        return "Connect";
    case SERVER_DISCON:
        return "Disconnect";
#if OBEX_SESSION_SUPPORT == XA_ENABLED 
    case SERVER_CREATE_SESSION:
        return "Create Session";
    case SERVER_CLOSE_SESSION:
        return "Close Session";
    case SERVER_SUSPEND_SESSION:
        return "Suspend Session";
    case SERVER_RESUME_SESSION:
        return "Resume Session";
    case SERVER_SET_TIMEOUT:
        return "Set Timeout";
#endif /* OBEX_SESSION_SUPPORT == XA_ENABLED */
    }
    return "Unknown";
}

/*---------------------------------------------------------------------------
 *            ClientOpName
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Return a pointer to the name of the current client operation.
 *
 * Return:    ASCII String pointer.
 *
 */
static const char *ClientOpName(U8 operation)
{
    static const char *opName[] = {
            "Idle", "Discovery", "Discovery Retry", "TP Connect", "Connected", 
            "OBEX Connect", "OBEX Disconnect", "OBEX Put", "OBEX Get",
            "OBEX Delete", "OBEX Setpath", "OBEX Abort", "TP Disconnect",
            "OBEX Create Session", "OBEX Close Session", "OBEX Suspend Session",
            "OBEX Resume Session", "OBEX Set Timeout" };

#if OBEX_SESSION_SUPPORT == XA_ENABLED
    if (operation > CLIENT_OBEX_SET_TIMEOUT) {
#else
    if (operation > CLIENT_TP_DISCON) {
#endif
        return "Unknown";
    }

    return opName[operation];
}

/*---------------------------------------------------------------------------
 *            pObStatus
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Return a pointer to a description of the OBEX status code.
 *
 * Return:    ASCII String pointer.
 *
 */
static const char *pObStatus(ObStatus status)
{
    switch (status) {
    case OB_STATUS_SUCCESS:
        return "Success";
    case OB_STATUS_FAILED:
        return "Failed";
    case OB_STATUS_PENDING:
        return "Pending";
    case OB_STATUS_DISCONNECT:
        return "Disconnect";
    case OB_STATUS_NO_CONNECT:
        return "No Connection";
    case OB_STATUS_MEDIA_BUSY:
        return "Media Busy";
    case OB_STATUS_BUSY:
        return "Status Busy";
    case OB_STATUS_NO_RESOURCES:
        return "Out of Resources";
    case OB_STATUS_INVALID_PARM:
        return "Invalid Parameter";
    case OB_STATUS_IN_PROGRESS:
        return "In Progress";
    case OB_STATUS_INVALID_HANDLE:
        return "Invalid Handle";
    case OB_STATUS_PACKET_TOO_SMALL:
        return "Packet Too Small";
    case OB_STATUS_NO_SESSION:
        return "No Session";
    }
    return "UNKNOWN";
}

/*---------------------------------------------------------------------------
 *            ClientAbortMsg
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Return a pointer to a description of the OBEX Abort reason.
 *
 * Return:    ASCII String pointer.
 *
 */
static const char *ClientAbortMsg(ObexClientApp *app)
{
    static char msg[20];

    switch (OBEX_ClientGetAbortReason(app)) {
    case OBRC_CLIENT_RW_ERROR:
        return "Client Internal R/W Error";
    case OBRC_LINK_DISCONNECT:
        return "Transport Link Disconnected";
    case OBRC_NOT_FOUND:
        return "Object Not Found";
    case OBRC_FORBIDDEN:
        return "Operation Forbidden";
    case OBRC_UNAUTHORIZED:
        return "Unauthorized Operation";
    case OBRC_NOT_ACCEPTABLE:
        return "Not Acceptable";
    case OBRC_CONFLICT:
        return "Conflict";
    case OBRC_USER_ABORT:
        return "User Aborted";
    case OBRC_BAD_REQUEST:
        return "Bad Request";
    case OBRC_SERVICE_UNAVAILABLE:
        return "Service Unavailable";
    }
    sprintf(msg, "Other (%02x)", OBEX_ClientGetAbortReason(app));
    return msg;
}

/*---------------------------------------------------------------------------
 *            StrLen
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Counts the length of a string.  
 *
 * Return:    Length of the string in bytes.
 *
 */
static U16 StrLen(const char *String)
{
    const char  *cp = String;

    Assert(String);

    while (*cp != 0) cp++;

    return (U16)(cp - String);
}

/*---------------------------------------------------------------------------
 *            GetName
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Returns a string value describing the current client or server.
 *
 * Return:    String describing the client or server being used.
 *
 */
static const char *GetName(U8 Role, U8 Instance)
{
    Assert((Role == QB_SERVER) || (Role == QB_CLIENT));

    if (Role == QB_CLIENT) {
        if (Instance == 0)          /* First client */
            return "Client:";
        return "Client2:";          /* Second client */
    } else {
        Assert(Role == QB_SERVER);
        if (Instance == 0)          /* First server */
            return "Server:";
        return "Server2:";          /* Second server */
    }
}

#if OBEX_SESSION_SUPPORT == XA_ENABLED
/*---------------------------------------------------------------------------
 *            IsObjectActive
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Determines if a current object is active based on the object
 *            name passed in.  This is only checked for multiple packet OBEX
 *            operations Put and Get.
 *
 * Return:    BOOL - whether object is active.
 *
 */
static BOOL IsObjectActive(const char *ObName)
{
#if 0
    U8      i,j;

    for (i = 0; i < NUM; i++) {
        for (j = 0; j < 2; j++) {
            if (QbClient[i].clientApp.client.activeSession != 
                &QbClient[i].QbClientSess[j].session) {
                /* Maybe a suspended operation */
                if (OS_MemCmp(QbClient[i].QbClientSess[j].sessionObjectName, 
                    StrLen(QbClient[i].QbClientSess[j].sessionObjectName), 
                    ObName, StrLen(ObName))) {
                    /* We are trying to use the same object as a suspended session.*/
                    return TRUE;
                }   
            }
        }
    }
#endif
    /* No suspended operations. */
    return FALSE;
}

/*
 * SuspendTimerFire
 *
 * Callback routine for the management of expired OBEX Session suspend timers.
 */
static void SuspendTimerFire(EvmTimer *Timer)
{
    QbServerSessionData    *qbsSession;
    QbClientSessionData    *qbcSession;

    /* Close the expired session */
    if (Timer->context == QB_CLIENT) {
        qbcSession = ContainingRecord(Timer, QbClientSessionData, timer);
        Assert(qbcSession->state == QB_SESSION_SUSPENDED);

        qbcSession->state = QB_SESSION_CLOSED;

    } else {
        Assert(Timer->context == (void *)QB_SERVER);

        qbsSession = ContainingRecord(Timer, QbServerSessionData, timer);
        Assert(qbsSession->state == QB_SESSION_SUSPENDED);

        qbsSession->state = QB_SESSION_CLOSED;
    }

    App_Report("%s Suspended Session timer expired.", GetName((U8)Timer->context, 0));
}

#endif /* OBEX_SESSION_SUPPORT == XA_ENABLED */

#if OBEX_AUTHENTICATION == XA_ENABLED
static void QbClientAuthenticate(U8 QbInst)
{
    ObexAuthChallenge   chal;
    U8                  chalStr[20];
    BOOL                status;
    TimeT               time;

    /* Authenticate the client if the UI allows it and if we haven't already */
    if (App_Authenticate() && !(Qb[QbInst].authenticated)) {
        chal.challenge = chalStr;
        chal.challengeLen = 15;

        time = OS_GetSystemTime();
        chalStr[0] = (U8)(time);
        chalStr[1] = (U8)(time >> 8);
        chalStr[2] = (U8)(time >> 16);
        chalStr[3] = (U8)(time >> 24);
        chalStr[4] = ':';
        OS_MemCopy(chalStr+5, Qb[QbInst].password, StrLen(Qb[QbInst].password));

        chal.options = 0x01;  /* Must send the UserId */
        chal.realm = "\0Network";  /* Use the Network UserId and Password */
        chal.realmLen = sizeof("\0Network");

        status = OBEXH_BuildAuthChallenge(&QbClient[QbInst].clientApp.handle, &chal, 
                                          QbClient[QbInst].clientNonce);
        Assert(status == TRUE);
        App_Report("%s Built Authentication Challenge header.", GetName(QB_CLIENT, QbInst));
    }
}

static void QbAuthenticateResp(U8 QbInst, U8 Role)
{
    ObexAuthResponse    rsp;
    BOOL                status;

    rsp.password = Qb[QbInst].password;
    rsp.passwordLen = (U8)StrLen(Qb[QbInst].password);
    rsp.userId = Qb[QbInst].userId;
    rsp.userIdLen = (U8)StrLen(Qb[QbInst].userId);

    if (Role = QB_CLIENT) {
        status = OBEXH_BuildAuthResponse(&QbClient[QbInst].clientApp.handle, &rsp, 
                                          QbClient[QbInst].rcvdChal.nonce);
        App_Report("%s Built Authentication Response header.", GetName(Role, QbInst));
    } else {
        status = OBEXH_BuildAuthResponse(&QbServer[QbInst].serverApp.handle, &rsp, 
                                         QbServer[QbInst].rcvdChal.nonce);
        App_Report("%s Built Authentication Response header.", GetName(Role, QbInst));
        Assert(status == TRUE);
    }
}
#endif /* OBEX_AUTHENTICATION == XA_ENABLED */
