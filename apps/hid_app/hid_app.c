#include "sdp.h"
#include "btalloc.h"
#include "sys/debug.h"
#include "stdio.h"
#include "windows.h"
#include "hid.h"
#include "avrcp.h"
#include "hid_app.h"

BtHandler GlobalHandler;

/****************************************************************************
 *
 * Function prototypes
 *
 ****************************************************************************/

#define KeepRunNext(exp)  if((exp) == 0) return FALSE;

#ifdef TEST_AVRCP
BOOL HIDAPP_Init(AvrcpChannel *avrchnl, HidChannel *chnl, AvrcpCallback avrcb, HidCallback callback, BtCallBack btCallback)
#else
BOOL HIDAPP_Init(HidChannel *chnl, HidCallback callback, BtCallBack btCallback)
#endif
{
    BOOL sts;
    BtStatus status;
    
    sts = CMGR_Init();
//    ME_SetConnectionRole(BCR_MASTER);
    
    KeepRunNext(sts == TRUE);

    ME_InitHandler(&GlobalHandler);
    GlobalHandler.callback = btCallback;

    //for low layer acl disconnect
    ME_RegisterGlobalHandler(&GlobalHandler);
    ME_SetEventMask(&GlobalHandler, BEM_LINK_DISCONNECT);

    //device handler
    DS_RegisterMonitorHandler(&GlobalHandler);

    //security handler
    SEC_RegisterPairingHandler(&GlobalHandler);    
    SEC_RegisterAuthorizeHandler(&GlobalHandler);

    //init hid
    sts = HID_Init();
    KeepRunNext(sts == TRUE);

    //hid handler
    status = HID_Register(chnl, callback);
    KeepRunNext(status == BT_STATUS_SUCCESS);

#ifdef TEST_AVRCP
    status = ME_SetClassOfDevice(/*COD_POSITIONING*/COD_LIMITED_DISCOVERABLE_MODE | COD_MAJOR_PERIPHERAL | COD_MINOR_PERIPH_KEYBOARD |COD_MINOR_PERIPH_REMOTECTRL/* | COD_MINOR_PERIPH_POINTING*/);
#else
    status = ME_SetClassOfDevice(/*COD_POSITIONING*/COD_LIMITED_DISCOVERABLE_MODE | COD_MAJOR_PERIPHERAL | COD_MINOR_PERIPH_KEYBOARD/* | COD_MINOR_PERIPH_POINTING*/);
#endif
    KeepRunNext(status == BT_STATUS_SUCCESS);

//tallplay test for avrcp
#ifdef TEST_AVRCP
    sts = AVRCP_Init();
    KeepRunNext(sts == TRUE);
    status = AVRCP_Register(avrchnl, avrcb, SC_AV_REMOTE_CONTROL);
    KeepRunNext(status == BT_STATUS_SUCCESS);
#endif
	return TRUE;
}

void HIDAPP_SetDiscoverable(BOOL on)
{  
    BtStatus status;
    if(on)
    {
          status = SEC_DisableSecurityMode3(&GlobalHandler);
         Assert(status == BT_STATUS_SUCCESS || status == BT_STATUS_PENDING);
        status = ME_SetAccessibleModeNC(BAM_LIMITED_ACCESSIBLE, 0);
        Assert(status == BT_STATUS_SUCCESS || status == BT_STATUS_PENDING);
    }
    else
    {
        status = SEC_EnableSecurityMode3(&GlobalHandler, FALSE);
        Assert(status == BT_STATUS_SUCCESS || status == BT_STATUS_PENDING);
        status = ME_SetAccessibleModeNC(BAM_CONNECTABLE_ONLY, 0);
        Assert(status == BT_STATUS_SUCCESS || status == BT_STATUS_PENDING);
    }
}

MeCommandToken nameTok;

BtStatus HIDAPP_GetRemoteDeviceName(BD_ADDR addr, BtPageScanInfo psi, BtCallBack cb)
{
        /* Start a name query for this device */
    nameTok.callback = cb;
    nameTok.p.name.bdAddr = addr;
    nameTok.p.name.io.in.psi = psi;

    return ME_GetRemoteDeviceName(&nameTok);

}

BtRemDevState HIDAPP_GetRemDevConState(BtDeviceContext *bdc)
{
    if(bdc->link)
    {
        return ME_GetRemDevState(bdc->link);
    }
    else
        return BDS_DISCONNECTED;
}


BtDeviceStatus HIDAPP_GetRemDevSecState(BtDeviceContext *bdc)
{
    return bdc->state;
}

BtStatus HIDAPP_ReadLocalBdAddr(BD_ADDR *Addr)
{
    return ME_ReadLocalBdAddr(Addr);
}

BtStatus HIDAPP_SetPin(BtDeviceContext *bdc, const U8 *pin, U8 len, BtPairingType type)
{
     BtRemoteDevice* remDev; 

     remDev = ME_FindRemoteDevice(&bdc->addr);
     return SEC_SetPin(remDev, pin, len,type);
}


