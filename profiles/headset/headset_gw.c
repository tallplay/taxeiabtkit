/****************************************************************************
 *
 * File:
 *     $Workfile:headset_gw.c$ for XTNDAccess Blue SDK, Version 1.3
 *     $Revision:129$
 *
 * Description: This file contains the headset audio gateway profile core.
 *             
 * Created:     September 12, 2000
 *
 * Copyright 2000-2002 Extended Systems, Inc.  ALL RIGHTS RESERVED.
 *
 * Unpublished Confidential Information of Extended Systems, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any means, 
 * or stored in a database or retrieval system, without prior written 
 * permission of Extended Systems, Inc.
 * 
 * Use of this work is governed by a license granted by Extended Systems, Inc.  
 * This work contains confidential and proprietary information of Extended 
 * Systems, Inc. which is protected by copyright, trade secret, trademark and 
 * other intellectual property rights.
 *
 ****************************************************************************/

#include <rfcomm.h>
#include <me.h>
#include <mesco.h>
#include <utils.h>
#include <sys/debug.h>
#include <hs_gw.h>
#include <sys/hs_gwi.h>

#if BT_HSP_GATEWAY == XA_ENABLED

#if SDP_PARSING_FUNCS == XA_DISABLED
#error The Headset profile requires the SDP parsing functions.
#endif /* SDP_PARSING_FUNCS == XA_DISABLED */

/****************************************************************************
 *
 * ROMable data
 *
 ****************************************************************************/
const U8  agckpdbuff[] = "AT+CKPD=200\r";
const U8  agvolcombuff[] = "AT+VGS=00\r";
const U8  agvolresbuff[] = "\r\n+VGS=00\r\n";
const U8  agringbuff[] = "\r\nRING\r\n";
const U8  agokbuff[] = "\r\nOK\r\n";
const U8  agerrbuff[] = "\r\nERROR\r\n";


/****************************************************************************
 *
 * RAMable data
 *
 ****************************************************************************/

#if XA_CONTEXT_PTR == XA_ENABLED
static Gateway temp;
Gateway *ag = &temp;
#else /* XA_CONTEXT_PTR == XA_ENABLED */
Gateway ag;
#endif /* XA_CONTEXT_PTR */

/*---------------------------------------------------------------------------
 * SDP Protocol Descriptor List object.
 * 
 * Value of the protocol descriptor list for the Headset Audio Gateway profile.
 * This structure is a ROM'able representation of the RAM structure.
 * During AG_Register, this structure is copied into the RAM structure
 * used to register the server. A RAM structure must be used to be able
 * to dynamically set the RFCOMM channel in the RFCOMM protocol descriptor.
 */
static const U8 AgProtoDescList[] = {
    SDP_ATTRIB_HEADER_8BIT(12),  /* Data element sequence, 12 bytes */

    /* Each element of the list is a Protocol descriptor which is a
     * data element sequence. The first element is L2CAP which only
     * has a UUID element.
     */
    SDP_ATTRIB_HEADER_8BIT(3),   /* Data element sequence for L2CAP, 3 bytes */     
    SDP_UUID_16BIT(PROT_L2CAP),  /* Uuid16 L2CAP */

    /* Next protocol descriptor in the list is RFCOMM. It contains two
     * elements which are the UUID and the channel. Ultimately this
     * channel will need to filled in with value returned by RFCOMM.
     */
    SDP_ATTRIB_HEADER_8BIT(5),   /* Data element sequence for RFCOMM, 5 bytes */
    SDP_UUID_16BIT(PROT_RFCOMM), /* Uuid16 RFCOMM */
    SDP_UINT_8BIT(99),           /* Uint8 RFCOMM channel number - value can vary */
};

/*---------------------------------------------------------------------------
 *
 * SDP objects registered by Headset Audio Gateway.
 * They are the Service Class ID and the Protocol Descriptor List
 */
static const U8 AgClassId[] = {
    SDP_ATTRIB_HEADER_8BIT(6),          /* Data Element Sequence, 6 bytes */
    SDP_UUID_16BIT(SC_HEADSET_AUDIO_GATEWAY), /* Headset Audio Gateway UUID in Big Endian */
    SDP_UUID_16BIT(SC_GENERIC_AUDIO) /* Generic Audio UUID in Big Endian */
};

/*
 * * OPTIONAL * Language BaseId List (goes with the service name).
 */
static const U8 AgLangBaseIdList[] = {
    SDP_ATTRIB_HEADER_8BIT(9),  /* Data Element Sequence, 9 bytes */
    SDP_UINT_16BIT(0x656E),     /* English Language */
    SDP_UINT_16BIT(0x006A),     /* UTF-8 encoding */
    SDP_UINT_16BIT(0x0100)      /* Primary language base Id */
};

/*
 * * OPTIONAL * Audio Gateway service name (digianswer requires it).
 */
static const U8 AgServiceName[] = {
    SDP_TEXT_8BIT(14),          /* Null terminated text string, 13 bytes */
    'A', 'U', 'D', 'I', 'O', ' ', 'G', 'a', 't', 'e', 'w', 
    'a', 'y', '\0'
};

/*---------------------------------------------------------------------------
 * Audio Gateway Public Browse Group.
 */
static const U8 AgBrowseGroup[] = {
    SDP_ATTRIB_HEADER_8BIT(3),          /* 3 bytes */
    SDP_UUID_16BIT(SC_PUBLIC_BROWSE_GROUP),  /* Public Browse Group */
};

/* Headset Audio Gateway's attributes.
 *
 * This is a ROM template for the RAM structure used to register the
 * Headset Audio Gateway's SDP record.
 */
static const SdpAttribute AgSdpAttributes[] = {
    /* Headset audio gateway class ID List attribute */
    SDP_ATTRIBUTE(AID_SERVICE_CLASS_ID_LIST, AgClassId), 
    /* Audio Gateway protocol descriptor list attribute */
    SDP_ATTRIBUTE(AID_PROTOCOL_DESC_LIST, AgProtoDescList),
    /* Public Browse Group Service */
    SDP_ATTRIBUTE(AID_BROWSE_GROUP_LIST, AgBrowseGroup), 
    /* Language base id (Optional: Used with service name) */
    SDP_ATTRIBUTE(AID_LANG_BASE_ID_LIST, AgLangBaseIdList),
    /* Audio Gateway service name (Optional) */
    SDP_ATTRIBUTE((AID_SERVICE_NAME + 0x0100), AgServiceName),
};

/*-------------------------------------------------------------------------
 *
 * SDP query info
 */
static const U8 AgServiceSearchAttribReq[] = {
    /* First parameter is the search pattern in data element format. It
     * is a list of 3 UUIDs. 
     */
    SDP_ATTRIB_HEADER_8BIT(9),       /* Data Element Sequence, 9 bytes */ 

    SDP_UUID_16BIT(SC_HEADSET),      /* UUID for Headset in Big Endian */
    SDP_UUID_16BIT(PROT_L2CAP),      /* L2CAP UUID in Big Endian       */
    SDP_UUID_16BIT(PROT_RFCOMM),     /* UUID for RFCOMM in Big Endian  */

    /* The second parameter is the maximum number of bytes that can be
     * be received for the attribute list
     */
    0x00, 0x64,                 /* Max number of bytes for attribute is 100 */

    /* The third parameter is the actual attribute ID list which is a data
     * element sequence. We only want one attribute which is the protocol
     * descriptor
     */
    SDP_ATTRIB_HEADER_8BIT(3),  /* Data Element Sequence, 3 bytes */
    SDP_UINT_16BIT(0x0004),     /* Value of protocol descriptor list ID */
};

/****************************************************************************
 *
 * Internal Function prototypes
 *
 ***************************************************************************/
static void     AgChangeState(AgState newstate);
static void     AgRingTimerFire(EvmTimer *Timer);
static void     AgEventHandler(const BtEvent *Event);
static void     AgRFC_Callback(RfChannel *Channel, RfCallbackParms *Parms);
static void     AgProcessRxData(RfChannel *Channel, RfCallbackParms *Parms);
static BtStatus AgVerifySdpQueryRsp(SdpQueryToken *token);
static void     AgScoOpen(void);
static BtStatus AgStartServiceQuery(SdpQueryMode mode);
static void     AgGlobalEventHandler(const BtEvent *event);
static void     AgSetConnectLevel(U8 level);
static BtStatus AgCheckSetMode(void);

/****************************************************************************
 *
 * Functions
 *
 ****************************************************************************/

/*---------------------------------------------------------------------------
 *            AG_Init()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Intializes context memory.
 *
 * Return:    TRUE - Initialization completed successfully.
 *
 */
BOOL AG_Init(void)
{
#if XA_CONTEXT_PTR == XA_ENABLED
    OS_MemSet((U8*)ag, 0, sizeof(Gateway));
#else
    OS_MemSet((U8*)&ag, 0, sizeof(Gateway));
#endif

    return TRUE;
}

/*---------------------------------------------------------------------------
 *            AG_Register()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Intializes necessary variables including app's callback function.
 *            Copies ROM structures into RAM.
 *
 * Return:    (See header file)
 *
 */
BtStatus AG_Register(const AgRegistration *reg)
{
    BtStatus     status;
    U8           i;

    OS_LockAg();

#if XA_ERROR_CHECK == XA_ENABLED
    if (AG(Initialized) == TRUE) {
        status = BT_STATUS_IN_USE;
        goto exit;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    AG(Initialized) = FALSE;
    AG(AppCallback) = reg->callback;
    AG(State) = AG_IDLE;
    AG(MicVol) = AG_DEFAULT_VOLUME;
    AG(SpkVol) = AG_DEFAULT_VOLUME;
    AgSetConnectLevel(AG_IDLE);
    AG(OutGoingConn) = FALSE;
    AG(audioPacketType) = BSPT_HV3;
    AG(receivedCKPD) = FALSE;
    AG(fixedHeadset) = FALSE;
    
    Assert(sizeof(agvolresbuff)-1 == sizeof(AG(volbuff)));
    OS_MemCopy(AG(volbuff), agvolresbuff, sizeof(agvolresbuff)-1);

    AG(handler).callback = AgEventHandler;
    ME_InitHandler(&AG(handler));

    /* Register Global Bluetooth stack event handler */
    ME_InitHandler(&AG(globalHandler));
    AG(globalHandler).callback = AgGlobalEventHandler;
    status = ME_RegisterGlobalHandler(&AG(globalHandler));
    Assert(status == BT_STATUS_SUCCESS);
    ME_SetEventMask(&AG(globalHandler), BEM_ALL_EVENTS);

#if AG_SECURITY == XA_ENABLED
    SEC_RegisterPairingHandler(&AG(handler));
#endif /* AG_SECURITY */

    /* Initialize RFCOMM */

    /* Server Channel */
    AG(rfSrvChannel).callback = AgRFC_Callback;
    AG(rfSrvChannel).maxFrameSize = RF_MAX_FRAME_SIZE;
    AG(rfSrvChannel).priority = RF_DEFAULT_PRIORITY;
    AG(rfService).serviceId = 0;

    /* Client Channel */
    AG(rfCltChannel).callback = AgRFC_Callback;
    AG(rfCltChannel).maxFrameSize = RF_MAX_FRAME_SIZE;
    AG(rfCltChannel).priority = RF_DEFAULT_PRIORITY;

    status = RF_RegisterServerChannel(&AG(rfSrvChannel), &AG(rfService), 1);
#if XA_ERROR_CHECK == XA_ENABLED
    if (status != BT_STATUS_SUCCESS) {
        status =  BT_STATUS_FAILED;
        goto exit;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    /* Initialize packet pool for RFCOMM messages */
    InitializeListHead(&AG(packetList));
    for (i = 0; i < AG_NUM_RFCOMM_PACKETS; i++) {
        InsertTailList(&AG(packetList), &(AG(packet)[i].node));
    }

    /* SDP Server Info */
    Assert(sizeof(AG(sdpProtoDescList)) == sizeof(AgProtoDescList));
    OS_MemCopy((U8 *)AG(sdpProtoDescList), (U8 *)AgProtoDescList,
               sizeof(AgProtoDescList));
    
    AG(sdpProtoDescList)[13] = AG(rfService).serviceId;

    /* OS_memcopy sdpatrribute */
    Assert(sizeof(AG(SdpAttribute)) == sizeof(AgSdpAttributes));
    OS_MemCopy((U8 *)AG(SdpAttribute), (U8 *)AgSdpAttributes,
            sizeof(AgSdpAttributes));

    AG(SdpAttribute[1]).value = AG(sdpProtoDescList);

    AG(sdprecord).attribs = AG(SdpAttribute);
    AG(sdprecord).num = 5;
    AG(sdprecord).classOfDevice = COD_AUDIO | COD_MAJOR_AUDIO | COD_MINOR_AUDIO_UNCLASSIFIED;

    status = SDP_AddRecord(&AG(sdprecord));
#if XA_ERROR_CHECK == XA_ENABLED
    if (status != BT_STATUS_SUCCESS) {
        status =  BT_STATUS_FAILED;
        goto exit;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(sizeof(AG(serviceSearchAttributeReq)) == sizeof(AgServiceSearchAttribReq));
    OS_MemCopy((U8 *)AG(serviceSearchAttributeReq), (U8 *)AgServiceSearchAttribReq,
                    sizeof(AgServiceSearchAttribReq));

    /* SDP Client Info */
    AG(sdpQueryToken).parms = AG(serviceSearchAttributeReq);
    AG(sdpQueryToken).plen = sizeof(AgServiceSearchAttribReq);
    AG(sdpQueryToken).type = BSQT_SERVICE_SEARCH_ATTRIB_REQ;
    AG(sdpQueryToken).callback = AgEventHandler;

    AG(Initialized) = TRUE;

#if XA_ERROR_CHECK == XA_ENABLED
exit:
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    OS_UnlockAg();
    return status;
}

/*---------------------------------------------------------------------------
 *            AG_Deregister()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Deinitializes RfComm service and SDP record, returns the state
 *            to AG_IDLE.
 *
 * Return:    (See header file)
 *
 */
BtStatus AG_Deregister(void)
{
    BtStatus status;

    OS_LockAg();

    if (AG(Initialized) == FALSE) {
        status = BT_STATUS_FAILED;
        goto exit;
    }

    if (AG(ConnectLevel) >= AG_RFCOMMOPEN) {
        status = BT_STATUS_BUSY;
        goto exit;
    }

    status = RF_DeregisterServerChannel(&AG(rfSrvChannel), &AG(rfService));
    Assert(status != BT_STATUS_FAILED);

    status = RF_DeregisterService(&AG(rfService));
    Assert(status != BT_STATUS_FAILED);

    EVM_CancelTimer(&AG(RingTimer));
    
    AgChangeState(AG_IDLE);
    /* Remove Headset profile SDP entry */
    status = SDP_RemoveRecord(&AG(sdprecord));

    status = ME_UnregisterGlobalHandler(&AG(globalHandler));
    Assert(status == BT_STATUS_SUCCESS);

    Report(("AG: Deinitialized\n"));
    AG(Initialized) = FALSE;

exit:

    OS_UnlockAg();
    return status;
}

/*---------------------------------------------------------------------------
 *            AG_RegisterHeadset()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Registers the AgDeviceContext containing the BD_ADDR and connect
 *            info of the headset.  This function MUST be called before a 
 *            connection can be created!
 *
 * Return:    (See header file)
 *
 */
BtStatus AG_RegisterHeadset(const BD_ADDR *headset)
{
#if XA_DEBUG == XA_ENABLED && XA_DEBUG_PRINT == XA_ENABLED
    U8          addr[BDADDR_NTOA_SIZE];
#endif
    BtStatus    status = BT_STATUS_FAILED;

    OS_LockAg();

    if ((headset != 0) && (AG(State) == AG_IDLE) && (AG(Initialized) == TRUE)) {
        OS_MemCopy(AG(devAddr).addr, (const U8 *)headset, BD_ADDR_SIZE);
        AG(fixedHeadset) = TRUE;
        status = BT_STATUS_SUCCESS;
        Report(("AG: Registered Headset %s", bdaddr_ntoa(&AG(devAddr), addr)));
    }

    OS_UnlockAg();
    return status;
}



/*---------------------------------------------------------------------------
 *            AG_EnableSecurity()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Enables security if flag == TRUE; if flag == FALSE, security is
 *            disabled.
 *
 * Return:    (See header file)
 */
#if AG_SECURITY == XA_ENABLED
BtStatus AG_EnableSecurity(BOOL flag)
{
    BtStatus        status;

    OS_LockAg();

    if ((flag == TRUE) && (AG(security) == FALSE)) {
        /* Register a security record and pairing handler for security mode 2 */
        AG(secRec).id =  SEC_RFCOMM_ID;
        AG(secRec).channel = AG(rfService).serviceId;
        AG(secRec).level = BSL_AUTHENTICATION_IN | BSL_AUTHENTICATION_OUT;
        status = SEC_Register(&AG(secRec));
#if XA_ERROR_CHECK == XA_ENABLED
        if (status != BT_STATUS_SUCCESS) {
            status = BT_STATUS_FAILED;
            goto exit;
        }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

        AG(security) = TRUE;
    }
    else if (flag == FALSE) {
        status = SEC_Unregister(&AG(secRec));
        AG(security) = FALSE;
    }

    OS_UnlockAg();

#if XA_ERROR_CHECK == XA_ENABLED
exit:
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    return status;
}
#endif /* AG_SECURITY */


/*---------------------------------------------------------------------------
 *            AG_DoConnect()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Starts the connection to the headset, app will be notified
 *            via callback when the connection has been established
 *
 * Return:    (See header file)
 *
 */
BtStatus AG_DoConnect(void)
{
    BtStatus status = BT_STATUS_FAILED;
    
    OS_LockAg();

#if XA_ERROR_CHECK == XA_ENABLED
    /* We can only start a connection if a device has been registered */
    if (AG(Initialized) == FALSE) {
        Report(("AG: The Audio Gateway has not been initialized."));
        goto exit;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    if (AG(State) == AG_IDLE) {
        AgChangeState(AG_OUTGOING);
    }

    switch(AG(ConnectLevel)) {
    case AG_IDLE:
        if (AG(fixedHeadset) == FALSE) {
            AG(deviceQuery).callback = AgEventHandler;
            AG(deviceQuery).quality.mask = (BDQM_SERVICE_CLASS|BDQM_DEVICE_CLASS|
                                            BDQM_DEVICE_STATUS|BDQM_SERVICE_UUID);
            AG(deviceQuery).quality.status = BDS_TRUSTED;
            AG(deviceQuery).quality.serviceClass = COD_AUDIO;
            AG(deviceQuery).quality.deviceClass = COD_MAJOR_AUDIO;
            AG(deviceQuery).quality.serviceUuid = SC_HEADSET;

            status = DS_SelectDevice(&AG(deviceQuery));
            if (status != BT_STATUS_PENDING) {
                Report(("AG: Select Device failed to start. Error %s\n", pBT_Status(status)));
                status = BT_STATUS_FAILED;
                break;
            }

            AgSetConnectLevel(AG_SELECTING);
            break;
        }

        /* Already have fixed headset device address. Connect now. */
        AgSetConnectLevel(AG_SELECTING);
        /* Drop into next state. */

    case AG_SELECTING:
        status = ME_CreateLink(&AG(handler), &AG(devAddr), 0, &AG(link));
        if (status == BT_STATUS_SUCCESS) {
            /* Simulate Connect event */
            BtEvent     e;
                    
            e.eType = BTEVENT_LINK_CONNECT_CNF;
            e.errCode = BEC_NO_ERROR;
            e.p.remDev = AG(link);    /* CreateLink() ret'd remDev struct */

            /* The status is still pending since we need to do the SDP query, 
             * Rfcomm connection and SCO connection before the link is fully
             * functional
             */
            status = BT_STATUS_PENDING;
            AgEventHandler(&e);
        } else if (status == BT_STATUS_PENDING) {
            AgSetConnectLevel(AG_ACLPENDING);
        }
        else {
            Report(("AG: Connect Link failed to start. Error = %s", pBT_Status(status)));
            AgChangeState(AG_IDLE);
            /* We don't want to confuse the app if it receives BT_STATUS_NO_RESOURCES
             * or any other failure status from ME_CreateLink so just return failed.
             */
            status = BT_STATUS_FAILED;
        }
        break;

    case AG_ACLPENDING:
        status = BT_STATUS_PENDING;
        break;

    case AG_ACLOPEN:
        if (AG(State) == AG_OUTGOING) {
            if (AgCheckSetMode() == BT_STATUS_SUCCESS) {
                status = AgStartServiceQuery(BSQM_FIRST);
                if (status == BT_STATUS_PENDING) {
                    /* Service query successfully started. */
                    AgSetConnectLevel(AG_SDPPENDING);
                    break;
                }
                else {
                    /* Service query failed to start. Disconnect ACL. */
                    AG_DoDisconnect();
                    status = BT_STATUS_FAILED;
                }
            }
            else {
                /* Couldn't come out of Park or Sniff */
                AG_DoDisconnect();
                status = BT_STATUS_FAILED;
            }
        }
        break;

    case AG_SDPPENDING:
        status = BT_STATUS_PENDING;
        break;

    case AG_SDPSUCCESS:
        if (AgCheckSetMode() == BT_STATUS_SUCCESS) {
            status = RF_OpenClientChannel(AG(link), AG(remoteRfcServer), 
                                         &AG(rfCltChannel), 1);
            if (status != BT_STATUS_PENDING) {
                Report(("AG: RF_OpenClientChannel failed!!\n"));
                AG_DoDisconnect();
            }
            else {
                AgSetConnectLevel(AG_RFCOMMPENDING);
            }
        } 
        else {
            /* Couldn't come out of Park or Sniff */
            AG_DoDisconnect();
            status = BT_STATUS_FAILED;
        }
        break;

    case AG_RFCOMMPENDING:
        status = BT_STATUS_PENDING;
        break;

    case AG_RFCOMMOPEN:
        if (AG(State) != AG_INCOMING) {
            /* to create a connection w/o the RING skip ahead to the
             * AG_RINGING case and just go from there....
             */
            status = AgSendMessage(AG_RING);
            if (status == BT_STATUS_SUCCESS) {

                /* Start the RING timer */
                AG(RingTimer).func = AgRingTimerFire;
                AG(ringCount) = 1;
                EVM_StartTimer(&AG(RingTimer), (TimeT)AG_RINGTIMER);
                AgSetConnectLevel(AG_RINGING);
            }
        }
        else {
            status = ME_CreateLink(&AG(handler), &AG(devAddr), 0, &AG(link));
            if (status == BT_STATUS_SUCCESS) {
                /* Simulate Connect event */
                BtEvent     e;
                    
                e.eType = BTEVENT_LINK_CONNECT_CNF;
                e.errCode = BEC_NO_ERROR;
                e.p.remDev = AG(link);    /* CreateLink() ret'd remDev struct */

                /* The status is still pending since we need to do the SDP query, 
                 * Rfcomm connection and SCO connection before the link is fully
                 * functional
                 */
                status = BT_STATUS_PENDING;
                AgEventHandler(&e);
            }
        }
        break;

    case AG_RINGING:
        if (AG(receivedCKPD) == TRUE) {
            AgScoOpen();
        }
        else {
            AG_DoDisconnect();
        }
        break;

    case AG_SCOPENDING:
        status = BT_STATUS_PENDING;
        AG_DoDisconnect();
        break;

    case AG_SCOOPEN:
        status = BT_STATUS_SUCCESS;
        break;
    }

#if XA_ERROR_CHECK == XA_ENABLED
exit:
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    OS_UnlockAg();
    return status;
}

/*---------------------------------------------------------------------------
 *            AG_DoDisconnect()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Starts (or continues) the disconnect process.  App will be 
 *            notified via the callback when the disconnect process is complete
 *
 * Return:    (See header file)
 *
 */
BtStatus AG_DoDisconnect(void)
{
    BtStatus status;

    OS_LockAg();

#if XA_ERROR_CHECK == XA_ENABLED
    if (AG(State) == AG_IDLE) {
        status = BT_STATUS_FAILED;
        goto exit;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    AgChangeState(AG_DISCONNECTING);
    
    /* Cancel the timer if it is running */
    EVM_CancelTimer(&AG(RingTimer));

    if (AG(ConnectLevel) >= AG_SCOOPEN) {
        Report(("AG: closing sco\n"));
        status = SCO_DisconnectLink(AG(scoConnect));
        if (status != BT_STATUS_PENDING) {
            status = BT_STATUS_FAILED;
            goto exit;
        }
    }
    else if (AG(ConnectLevel) >= AG_RFCOMMOPEN) {
        Report(("AG: Closing RFCOMM\n"));
        status = RF_CloseChannel(AG(DefaultChannel));
        if (status != BT_STATUS_PENDING) {
            status = BT_STATUS_FAILED;
            goto exit;
        }
    }
    else if (AG(ConnectLevel) >= AG_ACLOPEN) {
        Report(("AG: Closing ACL\n"));
        status = ME_DisconnectLink(&AG(handler), AG(link));
        if (status == BT_STATUS_SUCCESS) {
            /* whatever state we are in just return everything to IDLE */
            AgChangeState(AG_IDLE);    
        } else {
            Report(("AG: Link Disconnect failed. Error = %i", status));
            AgChangeState(AG_IDLE);
            status = BT_STATUS_FAILED;
            goto exit;
        } 
    }
    else {
        status = BT_STATUS_SUCCESS;
    }

exit:

    OS_UnlockAg();
    return status;
} 

/*---------------------------------------------------------------------------
 *            AG_ChangeVolume()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Changes the volume on the audio gateway and informs the headset
 *            of the change.
 *
 * Return:    (See header file)
 *
 */
BtStatus AG_ChangeVolume(AgVolumeType type, AgVolumeChange dir, U8 vol)
{
    BtStatus status;
    U8       gain = 0;
    BtPacket *txPacket;

    /* vol is unsigned, so cannot be less than 0 */
    CheckUnlockedParm(BT_STATUS_INVALID_PARM,
                      vol <= AG_MAX_VOLUME);

    OS_LockAg();

    if ((status = AgCheckSetMode()) != BT_STATUS_SUCCESS) {
        goto exit;
    }

    if (AG(ConnectLevel) < AG_RFCOMMOPEN) {
        status = BT_STATUS_FAILED;
        goto exit;
    }

    if (!IsListEmpty(&(AG(packetList)))) {
        txPacket = (BtPacket *)RemoveHeadList(&(AG(packetList)));
    }
    else {
        status = BT_STATUS_BUSY;
        goto exit;
    }

    switch (dir) {
    case AG_UPVOL:
        if (type == AG_MIC) {
            if (AG(MicVol) < AG_MAX_VOLUME) 
                AG(MicVol)++;
                gain = AG(MicVol);
        }
        else {
            Assert(type == AG_SPK);
            if (AG(SpkVol) < AG_MAX_VOLUME)
                AG(SpkVol)++;
                gain = AG(SpkVol);
        }
        break;

    case AG_DOWNVOL:
        if (type == AG_MIC) {
            if (AG(MicVol) > 0) 
                AG(MicVol)--;
                gain = AG(MicVol);
        }
        else {
            Assert(type == AG_SPK);
            if (AG(SpkVol) > 0)
                AG(SpkVol)--;
                gain = AG(SpkVol);
        }
        break;

    case AG_SETVOL:
        gain = vol;
        if (type == AG_MIC) 
            AG(MicVol) = gain;
        else
            AG(SpkVol) = gain;
        break;
    }

    AG(volbuff[5]) = type;
    if (gain > 9) {
        AG(volbuff[7]) = '1';
        AG(volbuff[8]) = gain - 10 + '0';
    }
    else {
        AG(volbuff[7]) = '0';
        AG(volbuff[8]) = gain + '0';
    }
    
    txPacket->dataLen = 11;
    txPacket->data = AG(volbuff);

    status = RF_SendData(AG(DefaultChannel), txPacket);
    if (status == BT_STATUS_PENDING) {
        status = BT_STATUS_SUCCESS;
        if (type == AG_MIC) {
            AG(Callbackinfo).event = AG_MICVOL_CHANGE;
            AG(Callbackinfo).info.volume = AG(MicVol);
            AG(AppCallback(&AG(Callbackinfo)));
        }
        else {
            AG(Callbackinfo).event = AG_SPKVOL_CHANGE;
            AG(Callbackinfo).info.volume = AG(SpkVol);
            AG(AppCallback(&AG(Callbackinfo)));
        }
    }

exit:
    
    OS_UnlockAg();
    return status;
}


/*---------------------------------------------------------------------------
 *            AG_EnterPinCode()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  If a PIN request is received from the stack, the app is notified
 *            via its callback.  The app is then responsible for calling this 
 *            function w/ the PIN which will be sent back to the stack.
 *
 * Return:    (See header file)
 *
 */
#if AG_SECURITY == XA_ENABLED
BtStatus AG_EnterPinCode(AgPinCodeReq   *pinParms)
{
    BtStatus status;

    OS_LockAg();

    status = SEC_SetPin(AG(link), pinParms->Pin, pinParms->PinLen, pinParms->TrustLevel);
    if (status != BT_STATUS_PENDING) {
        status = BT_STATUS_FAILED;
    }

    OS_UnlockAg();
    return status;
}
#endif


/*---------------------------------------------------------------------------
 *            AG_ButtonPressed()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Depending on the state and which button was pressed, this
 *            will start an outgoing call, disconnect a call, or change the 
 *            volume on the microphone and the speaker.
 *
 * Return:    (See header file)
 *
 */
BtStatus AG_ButtonPressed(AgButton   buttonNum)
{
    BtStatus status = BT_STATUS_FAILED;

    OS_LockAg();

    switch(buttonNum) {
    case AG_BUTTON:
        switch(AG(State)) {

        case AG_IDLE:
            status = AG_DoConnect();
            break;

        case AG_INCOMING:
            status = AG_DoConnect();
            break;

        case AG_OUTGOING:
            status = AG_DoConnect();
            break;

        case AG_CONNECTED:
            Report(("AG: AG_ButtonPressed starting AG_DoDisconnect()\n"));
            status = AG_DoDisconnect();
            break;

        case AG_DISCONNECTING:
            break;

        }
        break;

    case AG_BUTTON_MVDOWN:
        status = AG_ChangeVolume(AG_MIC, AG_DOWNVOL, 0);
        break;
        
    case AG_BUTTON_MVUP:
        status = AG_ChangeVolume(AG_MIC, AG_UPVOL, 0);
        break;

    case AG_BUTTON_SVDOWN:
        status = AG_ChangeVolume(AG_SPK, AG_DOWNVOL, 0);
        break;
        
    case AG_BUTTON_SVUP:
        status = AG_ChangeVolume(AG_SPK, AG_UPVOL, 0);
        break;
    }

    OS_UnlockAg();
    return status;
}

/*---------------------------------------------------------------------------
 *            AG_GetState()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Returns the current state of the audio gateway.
 *
 * Return:    AG(State)
 *
 */
AgState AG_GetState(void)
{  
    return AG(State);
}


/*---------------------------------------------------------------------------
 *            AG_GetRemoteDevice()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Returns a link to the BtRemoteDevice structure, which contains
 *            information about the current link (link policy, BD_ADDR, etc).
 *
 * Return:    AG(link)
 *
 */
BtRemoteDevice *AG_GetRemoteDevice(void)
{
    return AG(link);
}

/****************************************************************************
 *
 * Internal functions
 *
 ****************************************************************************/

/*---------------------------------------------------------------------------
 *            AgChangeState()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Changes audio gateway state and calls the app's callback
 *            to notify the app of the new state.
 *
 * Return:    
 *
 */
static void AgChangeState(AgState newstate)
{
    /* The new and old states are the same, so there is nothing to change */
    if (AG(State) == newstate) {
        return;
    }

    if (newstate == AG_OUTGOING) {
        AG(OutGoingConn) = TRUE;
    }


    AG(State) = newstate;

    AG(Callbackinfo).event = AG_STATE_CHANGE;
    AG(Callbackinfo).info.state = newstate;
    AG(AppCallback(&AG(Callbackinfo)));

    /* reset variables since the connection is idle */
    if (AG(State) == AG_IDLE) {
        AG(OutGoingConn) = FALSE;
        AG(link) = 0;        
        AgSetConnectLevel(AG_IDLE);
        AG(receivedCKPD) = FALSE;
    }
}


/*---------------------------------------------------------------------------
 *            AgRingTimerFire()
 *---------------------------------------------------------------------------
 *
 * Synopsis: Timer is used to cancel outgoing call if the headset doesn't
 *           respond 
 *           
 *
 * Return:    
 *
 */
static void AgRingTimerFire(EvmTimer *Timer)
{
    Report(("AG: RingTimer has Fired\n"));

    AG(ringCount)++;
    if (AG(ringCount) > AG_MAX_RING_COUNT) {
        /* Disconnect the RFCOMM and ACL connections */
        AG_DoDisconnect();
    }
    else {
        /* Send another RING message */
        if (AgCheckSetMode() == BT_STATUS_SUCCESS) {
            AgSendMessage(AG_RING);
        } else {
            AG_DoDisconnect();
        }

        /* Start the timer over again */
        EVM_StartTimer(&AG(RingTimer), (TimeT)AG_RINGTIMER);
    }
}


/*---------------------------------------------------------------------------
 *            AgStartServiceQuery()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initiate the SDP service query.
 *
 * Return:    
 *
 */
static BtStatus AgStartServiceQuery(SdpQueryMode mode)
{
    BtStatus status;

    AG(sdpQueryToken).rm = AG(link);
    status = SDP_Query(&AG(sdpQueryToken), mode);
    return status;
} 

/*---------------------------------------------------------------------------
 *            AgSendMessage()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Send an AT command to the headset
 *
 * Return:    
 *
 */
BtStatus AgSendMessage(AgMsgType type) 
{
    BtStatus status;
    BtPacket *txPacket;

    if (AG(ConnectLevel) < AG_RFCOMMOPEN) {
        return BT_STATUS_FAILED;
    }

    if (!IsListEmpty(&(AG(packetList)))) {
        txPacket = (BtPacket *)RemoveHeadList(&(AG(packetList)));
    }
    else {
        return BT_STATUS_BUSY;
    }

    switch(type) {
    case AG_RING:
        txPacket->dataLen = 8;
        txPacket->data = (U8 *)agringbuff;
        break;

    case AG_OK:
        txPacket->dataLen = 6;
        txPacket->data = (U8 *)agokbuff;
        break;

    case AG_ERR:
        txPacket->dataLen = 9;
        txPacket->data = (U8 *)agerrbuff;
        break;
    default:
        return BT_STATUS_FAILED;
    }

    if (AgCheckSetMode() == BT_STATUS_SUCCESS) {
        status = RF_SendData(AG(DefaultChannel), txPacket);
        if (status == BT_STATUS_PENDING) {
            status = BT_STATUS_SUCCESS;
        }
    } else {
        status = BT_STATUS_FAILED;
    }
    return status;
}


/*---------------------------------------------------------------------------
 *            AgEventHandler()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handles all ME and SDP events
 *
 * Return:    
 *
 */
static void AgEventHandler(const BtEvent *event)
{
    BtStatus status;

   
    OS_LockAg();

    switch (event->eType) {
    case BTEVENT_DEVICE_SELECTED:
        if (event->errCode != BEC_NO_ERROR) {
            Report(("AG: Device Selection failed!\n"));
            AgChangeState(AG_IDLE);
            break;
        }

        OS_MemCopy(AG(devAddr).addr, event->p.select->result->addr.addr, 6);
        AG_DoConnect();
        break;

    case BTEVENT_LINK_CONNECT_CNF:
        if (event->errCode != BEC_NO_ERROR) {
            Report(("AG: ACL link connect failed!\n"));
            AgChangeState(AG_IDLE);
            break;
        }
        Assert(AG(link) == event->p.remDev);
        SCO_RegisterHandler(AG(link), &AG(handler));

        if (AG(OutGoingConn) == TRUE) {

            Report(("AG: AG(ConnectLevel) = AG_ACLOPEN\n"));
            AgSetConnectLevel(AG_ACLOPEN);
            AG_DoConnect();
        }
        break;
    
    case BTEVENT_LINK_DISCONNECT:
        Report(("AG: AclOpen == FALSE\n"));
        AgSetConnectLevel(AG_IDLE);
        /* whatever state we are in just return everything to IDLE */
        AgChangeState(AG_IDLE);        
        break;

    case BTEVENT_SCO_CONNECT_IND:
        Report(("AG: BTEVENT_SCO_CONNECT_IND\n"));
        break;

    case BTEVENT_SCO_CONNECT_CNF:
        if (event->errCode != BEC_NO_ERROR) {
            Report(("AG: SCO connect had an error!!\n"));
            if (AG(OutGoingConn) == TRUE) {
                AG_DoDisconnect();
            }
            else {
                AgSendMessage(AG_ERR);
            }
            break;
        }
        
        AG(scoConnect) = event->p.scoConnect.scoCon;             
        Report(("AG: ConnectLevel: AG_SCOOPEN\n"));
        AgSetConnectLevel(AG_SCOOPEN);
        /* who initiated the connection is only important while the connection
         * is being established, after that it doesn't matter.
         */
        AG(OutGoingConn) = FALSE;
        AgChangeState(AG_CONNECTED);
        break;

    case BTEVENT_SCO_DISCONNECT:
        AG(scoConnect) = 0;

        Report(("AG: ConnectLevel: AG_RFCOMMOPEN (disconnecting)\n"));
        if (AG(DefaultChannel) != 0) {
            AgSetConnectLevel(AG_RFCOMMOPEN);   
        } else {
            AgSetConnectLevel(AG_ACLOPEN);
        }

        SCO_UnregisterHandler(AG(link), &AG(handler));

        Report(("AG: BTEVENT_SCO_DISCONNECT received\n"));
        /* If we are disconnecting the link then disconnect the ACL link as well */
        status = AG_DoDisconnect();
        break;

    case SDEVENT_QUERY_RSP:
        status = AgVerifySdpQueryRsp(event->p.token);
        if (status == BT_STATUS_SUCCESS) {
            AgSetConnectLevel(AG_SDPSUCCESS);
            AG_DoConnect();            
        }
        else if (status == BT_STATUS_SDP_CONT_STATE) {
            /* We are going to query again for the rest of the response */
            status = AgStartServiceQuery(BSQM_CONTINUE);
            if (status != BT_STATUS_PENDING) {
                /* SDP query failed to start properly, so disconnect */
                Report(("AG: SDP query failed to start, aborting connection\n"));
                AG_DoDisconnect();
            }
            break;
        }
        else {
            Report(("AG: SDP query failed\n"));
            status = AG_DoDisconnect();
        }
        break;

        /* Headset profile was not found, drop through to error case */
    case SDEVENT_QUERY_ERR:
    case SDEVENT_QUERY_FAILED:
        Report(("AG: GW: SDP Query Failed\n"));
        
        AgChangeState(AG_DISCONNECTING);
        AgSetConnectLevel(AG_ACLOPEN);
        AG_DoDisconnect();
        break;

#if AG_SECURITY == XA_ENABLED
    case BTEVENT_PIN_REQ: 
        Report(("AG: PIN Request Received.\n"));
        /* call the app's callback with a PIN_REQUEST event */
        AG(link) = event->p.remDev;
        AG(Callbackinfo).event = AG_PIN_REQUEST;
        AG(Callbackinfo).info.bdAddr = AG(link)->bdAddr;
        AG(AppCallback(&AG(Callbackinfo)));
        break;

    case BTEVENT_PAIRING_COMPLETE:
        Report(("AG: Pairing Complete.\n"));
        break;

    case BTEVENT_SECURITY3_COMPLETE:
        Report(("AG: Security Mode 3 Complete.\n"));
        break;

    case BTEVENT_AUTHENTICATED:
        break;
#endif /* AG_SECURITY */

    }

    OS_UnlockAg();
}


/*---------------------------------------------------------------------------
 *            AgGlobalEventHandler()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Processes HCI Initialization events.
 *
 * Return:    void
 *
 */
static void AgGlobalEventHandler(const BtEvent *event)
{
    OS_LockAg();

    switch (event->eType) {
    case BTEVENT_LINK_CONNECT_IND:
        AG(link) = event->p.remDev;
        break;

    case BTEVENT_HCI_INITIALIZED: 
        AG(RadioActive) = TRUE;
        break;

    case BTEVENT_HCI_DEINITIALIZED:
        Report(("AG: Radio is down\n"));
        AG(RadioActive) = FALSE;
        break;

    case BTEVENT_HCI_FAILED:
        Report(("AG: HCI Initialization FAILED\n"));
        AG(RadioActive) = FALSE;
        break;
    }

    OS_UnlockAg();
}


/*---------------------------------------------------------------------------
 *            AgVerifySdpQueryRsp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Parses the Headset SDP response for the RFCOMM channel.  
 *            In the case of a continuation state, additional SDP queries 
 *            will be issued until the attribute has been found or until
 *            the entire SDP response has been parsed.
 *            
 *
 * Return:    BT_STATUS_SUCCESS if SDP parsing is successful.
 *            BT_STATUS_FAILED if SDP parsing encountered an error.
 *            BT_STATUS_SDP_CONT_STATE if SDP parsing encountered a 
 *            continuation state that requires an additional SDP query.
 *
 */
static BtStatus AgVerifySdpQueryRsp(SdpQueryToken *token)
{
    BtStatus        status = BT_STATUS_FAILED;

    /* Get the RFCOMM Channel */
    token->attribId = AID_PROTOCOL_DESC_LIST;
        
    token->uuid = PROT_RFCOMM;
    if ((status = SDP_ParseAttributes(token)) == BT_STATUS_SUCCESS) {
        /* We had better have a U8 channel value */
        if ((token->totalValueLen != 1) || (token->availValueLen != 1)) {
            status = BT_STATUS_FAILED;
            goto done;
        }
        Report(("AG: SDP_ParseAttributes succeeded! RFCOMM Channel = %d,"
                "Updated Buff Len = %d\n", token->valueBuff[0], 
                token->remainLen));
        /* Assign the RFCOMM channel number */
        AG(remoteRfcServer) = token->valueBuff[0];
    }
    
done:
    if (status == BT_STATUS_SDP_CONT_STATE) {
        Report(("AG: SDP_ParseAttributes - Continuation State. Query Again!\n"));
        token->mode = BSPM_CONT_STATE;
        return status;
    } else if (status == BT_STATUS_FAILED) {
        /* Failed to retrieve the RFCOMM channel number */
        Report(("AG: SDP_ParseAttributes - Failure!\n"));
    }
    
    /* Reset the SDP parsing mode */
    token->mode = BSPM_BEGINNING;

    return status;
}

/*---------------------------------------------------------------------------
 *            AgRFC_Callback()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  RFCOMM callback; handles all RFCOMM events.
 *
 * Return:    
 *
 */
void AgRFC_Callback(RfChannel *Channel, RfCallbackParms *Parms)
{
    BtStatus status;

    switch (Parms->event) {

    case RFEVENT_OPEN_IND:
        AG(DefaultChannel) = Channel;
        AgSetConnectLevel(AG_RFCOMMOPEN);
        RF_AcceptChannel(Channel);
        AgChangeState(AG_INCOMING);
        break;

    case RFEVENT_OPEN:
        AG(DefaultChannel) = Channel;
        AgSetConnectLevel(AG_RFCOMMOPEN);
        if (AG(State) == AG_INCOMING)
            OS_MemCopy((U8 *)&AG(devAddr).addr, (const U8 *)Parms->ptrs.remDev->bdAddr.addr, 6);  

#if AG_CONNECT_NO_RING == XA_ENABLED
        /* In order to bypass the RING, just set the connect level to AG_RINGING
         * and say that you received a response to the RING, and just keep going.
         */
        AgSetConnectLevel(AG_RINGING);
        AG(receivedCKPD) = TRUE;
#endif
        AG_DoConnect();
        break;
    
    case RFEVENT_PACKET_HANDLED:
        InsertTailList(&(AG(packetList)), &Parms->ptrs.packet->node);
        break;

    case RFEVENT_DATA_IND:
        RF_AdvanceCredit(Channel, 1);
        AgProcessRxData(Channel, Parms);
        break;

    case RFEVENT_CLOSE_IND:
        break;

    case RFEVENT_CLOSED:
        AG(DefaultChannel) = 0;
        AgSetConnectLevel(AG_ACLOPEN);
        Report(("AG: RFEVENT_CLOSED received\n"));
        status = AG_DoDisconnect();
        break;

    case RFEVENT_MODEM_STATUS_IND:
        break;

    default:
        Report(("AG: RFC Unknown event: %i\n", Parms->event));   
    }
}

/*---------------------------------------------------------------------------
 *            AgProcessRxData()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Processes Rfcomm data
 *
 * Return:    
 *
 */
void AgProcessRxData(RfChannel *Channel, RfCallbackParms *Parms)
{
    U16 dataLen = 0;
    U8  gain = 0;

    dataLen = Parms->dataLen;

    if (AG(Initialized) == FALSE) {
        Report(("AG: RFCOMM command received before a headset has been registered\n"));
        AgSendMessage(AG_ERR);
        return;
    }

    /* See if it is an AT+CKPD command */
    if ((dataLen == 12) && (OS_MemCmp(Parms->ptrs.data, 12, agckpdbuff, 12))) {
        if ((AG(ConnectLevel) < AG_SCOOPEN) && (AG(receivedCKPD) == FALSE)){
            AG(receivedCKPD) = TRUE;
            if (AG(ConnectLevel) == AG_RINGING) {
                EVM_CancelTimer(&AG(RingTimer));
            }
            if (AG(OutGoingConn) == FALSE) {
                AgChangeState(AG_INCOMING);
                AgSetConnectLevel(AG_RINGING);
            }
            AgSendMessage(AG_OK);
            AG_DoConnect();
            return;
        }
        else  {
            /* release SCO, release ACL connection */
            AG_DoDisconnect();
        }
    }
    /* Volume command */
    else if ((dataLen >= 9) && (OS_MemCmp(Parms->ptrs.data, 5, agvolcombuff, 5))) {

        if (dataLen == 9) {
            gain = (Parms->ptrs.data[7] - '0');
        } else {
            if (Parms->ptrs.data[7] == '1') {
                gain = (10 + (Parms->ptrs.data[8] - '0'));
            } else if (Parms->ptrs.data[7] == '0'){
                gain = (Parms->ptrs.data[8] - '0');
            }
            else {
                /* invalid volume level */
                AgSendMessage(AG_ERR);
                return;
            }
        }
        if (gain > AG_MAX_VOLUME) {
            Report(("Invalid volume gain level\n"));
            AgSendMessage(AG_ERR);
            return;
        }

        switch (Parms->ptrs.data[5]) {
        
        /* Microphone Gain */
        case AG_MIC:
            AG(MicVol) = gain;
            AG(Callbackinfo).event = AG_MICVOL_CHANGE;
            AG(Callbackinfo).info.volume = AG(MicVol);
            AG(AppCallback(&AG(Callbackinfo)));
            break;

        /* Speaker Gain */
        case AG_SPK:
            AG(SpkVol) = gain;
            AG(Callbackinfo).event = AG_SPKVOL_CHANGE;
            AG(Callbackinfo).info.volume = AG(SpkVol);
            AG(AppCallback(&AG(Callbackinfo)));
            break;
        }
    }
    else {
        /* send an error message */
        Report(("AG: invalid RF data (%s) received (len: %i)\n", Parms->ptrs.data, dataLen));
        AgSendMessage(AG_ERR);
        return;
    }
    AgSendMessage(AG_OK);
}

/*---------------------------------------------------------------------------
 *            AgScoOpen()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Create an SCO connection to the headset
 *
 * Return:    
 *
 */
void AgScoOpen(void)
{
    BtStatus status;

    /* Establish a new SCO channel to the device */
    status = SCO_CreateLink(&AG(scoConnect), AG(link), AG(audioPacketType));

    if (status != BT_STATUS_PENDING) {
        Report(("AG: Audio: SCO connect request failed. %d\n", status));
    }
    else {
        Report(("AG: SCO connect pending.\n"));
    }
}


/*---------------------------------------------------------------------------
 *            AgSetConnectLevel()
 *---------------------------------------------------------------------------
 *
 * Synopsis: Simple function to set AG(ConnectLevel)
 *           
 *
 * Return:   none
 *
 */
void AgSetConnectLevel(U8 level)
{
    AG(ConnectLevel) = level;
}


/*---------------------------------------------------------------------------
 *            AgCheckSetMode()
 *---------------------------------------------------------------------------
 *
 * Synopsis: Checks the ACL link mode and makes it active if necessary.
 *           
 *
 * Return:   none
 *
 */
BtStatus AgCheckSetMode(void)
{
    BtStatus status;

    if (ME_GetCurrentMode(AG(link)) == BLM_PARK_MODE) {
        status = ME_StopPark(AG(link));
        if ((status != BT_STATUS_PENDING) &&
            (status != BT_STATUS_IN_PROGRESS)) {
            Report(("AG: Could not bring device out of Park Mode.\n"));
            return BT_STATUS_FAILED;
        }
    } else if (ME_GetCurrentMode(AG(link)) == BLM_SNIFF_MODE) {
        ME_StopSniff(AG(link));
    }

    return BT_STATUS_SUCCESS;
}


#if XA_DEBUG == XA_ENABLED
static U8  AgLock = 0;

void OS_LockAg(void)
{
    OS_LockStack();

    Assert(AgLock < 5);

    AgLock++;
}

void OS_UnlockAg(void)
{
    Assert(AgLock > 0);

    AgLock--;

    OS_UnlockStack();
}

#endif /* XA_DEBUG == XA_ENABLED */

#endif /* BT_HSP_GATEWAY == XA_ENABLED */
