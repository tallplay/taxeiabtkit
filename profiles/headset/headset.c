/****************************************************************************
 *
 * File:
 *     $Workfile:headset.c$ for XTNDAccess Blue SDK, Version 1.3
 *     $Revision:137$
 *
 * Description: This file contains the headset profile core.
 *             
 * Created:     September 12, 2000
 *
 * Copyright 2000-2002 Extended Systems, Inc.  ALL RIGHTS RESERVED.
 *
 * Unpublished Confidential Information of Extended Systems, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any means, 
 * or stored in a database or retrieval system, without prior written 
 * permission of Extended Systems, Inc.
 * 
 * Use of this work is governed by a license granted by Extended Systems, Inc.  
 * This work contains confidential and proprietary information of Extended 
 * Systems, Inc. which is protected by copyright, trade secret, trademark and 
 * other intellectual property rights.
 *
 ****************************************************************************/

#include <sys/evmxp.h>
#include <rfcomm.h>
#include <me.h>
#include <mesco.h>
#include <utils.h>
#include <sys/debug.h>
#include <headset.h>
#include <sys/headseti.h>

#if BT_HSP_HEADSET == XA_ENABLED

#if SDP_PARSING_FUNCS == XA_DISABLED
#error The Headset profile requires the SDP parsing functions.
#endif /* SDP_PARSING_FUNCS == XA_DISABLED */

/****************************************************************************
 *
 * ROMable data
 *
 ****************************************************************************/
const U8  hsckpdbuff[]   = "AT+CKPD=200\r";
const U8  hsvolcombuff[] = "AT+VGS=00\r";
const U8  hsvolresbuff[] = "\r\n+VGS=00\r\n";
const U8  hsringbuff[]   = "\r\nRING\r\n";
const U8  hsokbuff[]     = "\r\nOK\r\n";
const U8  hserrbuff[]    = "\r\nERROR\r\n";

/****************************************************************************
 *
 * RAMable data
 *
 ****************************************************************************/
#if XA_CONTEXT_PTR == XA_ENABLED
static Headset temp;
Headset *hs = &temp;
#else /* XA_CONTEXT_PTR == XA_ENABLED */
Headset hs;
#endif /* XA_CONTEXT_PTR */

/*---------------------------------------------------------------------------
 * SDP Protocol Descriptor List object registered by Headset.
 * 
 * Value of the protocol descriptor list for the Headset profile.
 * This structure is a ROM'able representation of the RAM structure.
 * During ServerInit, this structure is copied into the RAM structure
 * used to register the server. A RAM structure must be used to be able
 * to dynamically set the RFCOMM channel in the RFCOMM protocol descriptor.
 */
static const U8 HsProtoDescList[] = {
    SDP_ATTRIB_HEADER_8BIT(12),  /* Data element sequence, 12 bytes */

    /* Each element of the list is a Protocol descriptor which is a
     * data element sequence. The first element is L2CAP which only
     * has a UUID element.
     */
    SDP_ATTRIB_HEADER_8BIT(3),   /* Data element sequence for L2CAP, 3 bytes */     
    SDP_UUID_16BIT(PROT_L2CAP),  /* Uuid16 L2CAP */

    /* Next protocol descriptor in the list is RFCOMM. It contains two
     * elements which are the UUID and the channel. Ultimately this
     * channel will need to filled in with value returned by RFCOMM.
     */
    SDP_ATTRIB_HEADER_8BIT(5),   /* Data element sequence for RFCOMM, 5 bytes */
    SDP_UUID_16BIT(PROT_RFCOMM), /* Uuid16 RFCOMM */
    SDP_UINT_8BIT(99),           /* Uint8 RFCOMM channel number - value can vary */
};

/*---------------------------------------------------------------------------
 *
 * SDP objects registered by Headset
 * They are the Service Class ID and the Protocol Descriptor List 
 */
static const U8 HsClassId[] = {
    SDP_ATTRIB_HEADER_8BIT(6),          /* Data Element Sequence, 6 bytes */
    SDP_UUID_16BIT(SC_HEADSET), /* Headset Audio Gateway UUID in Big Endian */
    SDP_UUID_16BIT(SC_GENERIC_AUDIO) /* Generic Audio UUID in Big Endian */
};

/*
 * * OPTIONAL * Language BaseId List (goes with the service name).
 */
static const U8 HsLangBaseIdList[] = {
    SDP_ATTRIB_HEADER_8BIT(9),  /* Data Element Sequence, 9 bytes */
    SDP_UINT_16BIT(0x656E),     /* English Language */
    SDP_UINT_16BIT(0x006A),     /* UTF-8 encoding */
    SDP_UINT_16BIT(0x0100)      /* Primary language base Id */
};

/*
 * * OPTIONAL * Object push service name (digianswer requires it).
 */
static const U8 HsServiceName[] = {
    SDP_TEXT_8BIT(14),          /* Null terminated text string, 13 bytes */
    'A', 'U', 'D', 'I', 'O', ' ', 'H', 'e', 'a', 'd', 's', 
    'e', 't', '\0'
};

/*---------------------------------------------------------------------------
 * Headset Public Browse Group.
 */
static const U8 HsBrowseGroup[] = {
    SDP_ATTRIB_HEADER_8BIT(3),          /* 3 bytes */
    SDP_UUID_16BIT(SC_PUBLIC_BROWSE_GROUP),  /* Public Browse Group */
};

/* Headset's attributes.
 *
 * This is a ROM template for the RAM structure used to register the
 * Headset's SDP record.
 */
static const SdpAttribute HsSdpAttributes[] = {
    /* Headset audio gateway class ID List attribute */
    SDP_ATTRIBUTE(AID_SERVICE_CLASS_ID_LIST, HsClassId), 
    /* Audio Gateway protocol descriptor list attribute */
    SDP_ATTRIBUTE(AID_PROTOCOL_DESC_LIST, HsProtoDescList),
    /* Public Browse Group Service */
    SDP_ATTRIBUTE(AID_BROWSE_GROUP_LIST, HsBrowseGroup), 
    /* Language base id (Optional: Used with service name) */
    SDP_ATTRIBUTE(AID_LANG_BASE_ID_LIST, HsLangBaseIdList),
    /* Audio Gateway service name (Optional) */
    SDP_ATTRIBUTE((AID_SERVICE_NAME + 0x0100), HsServiceName),
};

/*-------------------------------------------------------------------------
 *
 * SDP query info
 */
static const U8 HsServiceSearchAttribReq[] = {
    /* First parameter is the search pattern in data element format. It
     * is a list of 3 UUIDs. 
     */
    SDP_ATTRIB_HEADER_8BIT(9),           /* Data Element Sequence, 9 bytes */ 

    SDP_UUID_16BIT(SC_HEADSET_AUDIO_GATEWAY), /* UUID for Headset          */
    SDP_UUID_16BIT(PROT_L2CAP),               /* L2CAP UUID                */
    SDP_UUID_16BIT(PROT_RFCOMM),              /* UUID for RFCOMM           */

    /* The second parameter is the maximum number of bytes that can be
     * be received for the attribute list
     */
    0x00, 0x64,                 /* Max number of bytes for attribute is 100 */

    /* The third parameter is the actual attribute ID list which is a data
     * element sequence. We only want one attribute which is the protocol
     * descriptor
     */
    SDP_ATTRIB_HEADER_8BIT(3),  /* Data Element Sequence, 3 bytes */
    SDP_UINT_16BIT(0x0004),     /* Value of protocol descriptor list ID */
};

/****************************************************************************
 *
 * Internal Function prototypes
 *
 ***************************************************************************/

static void HsChangeState(HsState newstate);
static void HsRFC_Callback(RfChannel *Channel, RfCallbackParms *Parms);
static void HsEventHandler(const BtEvent *Event);
static void HsProcessRxData(RfChannel *Channel, RfCallbackParms *Parms);
static BtStatus HsStartServiceQuery(SdpQueryMode mode);
static BtStatus HsVerifySdpQueryRsp(SdpQueryToken *token);
static void HsScoOpen(BtErrorCode reason);
static void HsGlobalEventHandler(const BtEvent *event);
static void HsChangeDdbRecord(void);
void HsLastPacketType(U8  type);
BtStatus HsDisconnect(void);
BtStatus HsSendCKPD(void);
static BtStatus HsCheckSetMode(void);


/****************************************************************************
 *
 * Functions
 *
 ****************************************************************************/

/*---------------------------------------------------------------------------
 *            HS_Init()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Intializes context memory.
 *
 * Return:    TRUE - Initialization completed successfully.
 *
 */
BOOL HS_Init(void)
{
#if XA_CONTEXT_PTR == XA_ENABLED
    OS_MemSet((U8*)hs, 0, sizeof(Headset));
#else
    OS_MemSet((U8*)&hs, 0, sizeof(Headset));
#endif

    return TRUE;
}

/*---------------------------------------------------------------------------
 *            HS_Register()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Intializes necessary variables including app's callback function.
 *            Copies ROM structures into RAM.
 *
 * Return:    (See header file)
 *
 */
BtStatus HS_Register(const HsRegistration *reg)
{
    BtStatus        status;
    U8              i;
    BtLinkPolicy    policy;

    OS_LockHs();
    
#if XA_ERROR_CHECK == XA_ENABLED
    if (HS(Initialized) == TRUE) {
        status = BT_STATUS_BUSY;
        goto exit;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    HS(Initialized) = FALSE;
    HS(AppCallback) = reg->callback;
    HS(ConnectLevel) = HS_IDLE;
    HS(State) = HS_IDLE;
    HS(MicVol) = HS_DEFAULT_VOLUME;
    HS(SpkVol) = HS_DEFAULT_VOLUME;
    HS(OutGoingConn) = FALSE;
    HS(HdcRegistered) = FALSE;
    HS(link) = 0;

    Assert(sizeof(hsckpdbuff)-1 == sizeof(HS(ckpdbuff)));
    OS_MemCopy(HS(ckpdbuff), hsckpdbuff, sizeof(hsckpdbuff)-1);
    Assert(sizeof(hsvolcombuff)-1 == sizeof(HS(volbuff)));
    OS_MemCopy(HS(volbuff), hsvolcombuff, sizeof(hsvolcombuff)-1);

    /* Register Global Bluetooth stack event handler */
    ME_InitHandler(&HS(globalHandler));
    HS(globalHandler).callback = HsGlobalEventHandler;
    status = ME_RegisterGlobalHandler(&HS(globalHandler));
    Assert(status == BT_STATUS_SUCCESS);
    ME_SetEventMask(&HS(globalHandler), BEM_ALL_EVENTS);

    HS(handler).callback = HsEventHandler;
    ME_InitHandler(&HS(handler));

#if HS_SECURITY == XA_ENABLED
    SEC_RegisterPairingHandler(&HS(handler));
#endif

    /* Set the link policy */
    policy = BLP_SNIFF_MODE | BLP_PARK_MODE | BLP_HOLD_MODE;
    status = ME_SetDefaultLinkPolicy(policy, policy);
    if (status == BT_STATUS_FAILED) {
        goto exit;
    }

    /* Initialize RFCOMM stuff */
    HS(rfService).serviceId = 0;

    /* Server Channel */
    HS(rfSrvChannel).callback = HsRFC_Callback;
    HS(rfSrvChannel).maxFrameSize = RF_MAX_FRAME_SIZE;
    HS(rfSrvChannel).priority = RF_DEFAULT_PRIORITY;

    /* Client Channel */
    HS(rfCltChannel).callback = HsRFC_Callback;
    HS(rfCltChannel).maxFrameSize = RF_MAX_FRAME_SIZE;
    HS(rfCltChannel).priority = RF_DEFAULT_PRIORITY;

    status = RF_RegisterServerChannel(&HS(rfSrvChannel), &HS(rfService), 1);
#if XA_ERROR_CHECK == XA_ENABLED
    if (status != BT_STATUS_SUCCESS) {
        status =  BT_STATUS_FAILED;
        goto exit;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    /* Initialize packet pool for RFCOMM messages */
    InitializeListHead(&HS(packetList));
    for (i = 0; i < HS_NUM_RFCOMM_PACKETS; i++) {
        InsertTailList(&HS(packetList), &(HS(packet)[i].node));
    }

    /* SDP Server Info */
    Assert(sizeof(HS(sdpProtoDescList)) == sizeof(HsProtoDescList));
    OS_MemCopy((U8 *)HS(sdpProtoDescList), (U8 *)HsProtoDescList,
               sizeof(HsProtoDescList));

    HS(sdpProtoDescList)[13] = HS(rfService).serviceId;

    /* OS_memcopy sdpatrribute */
    Assert(sizeof(HS(SdpAttribute)) == sizeof(HsSdpAttributes));
    OS_MemCopy((U8 *)HS(SdpAttribute), (U8 *)HsSdpAttributes,
            sizeof(HsSdpAttributes));

    HS(SdpAttribute[1]).value = HS(sdpProtoDescList);

    HS(sdprecord).attribs = HS(SdpAttribute);
    HS(sdprecord).num = 5;
    HS(sdprecord).classOfDevice = COD_AUDIO | COD_MAJOR_AUDIO | COD_MINOR_AUDIO_HEADSET;

    status = SDP_AddRecord(&HS(sdprecord));
#if XA_ERROR_CHECK == XA_ENABLED
    if (status != BT_STATUS_SUCCESS) {
        status =  BT_STATUS_FAILED;
        goto exit;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(sizeof(HS(serviceSearchAttributeReq)) == sizeof(HsServiceSearchAttribReq));
    OS_MemCopy((U8 *)HS(serviceSearchAttributeReq), (U8 *)HsServiceSearchAttribReq,
                    sizeof(HsServiceSearchAttribReq));

    /* SDP Client Info */
    HS(sdpQueryToken).parms = HS(serviceSearchAttributeReq);
    HS(sdpQueryToken).plen = sizeof(HsServiceSearchAttribReq);
    HS(sdpQueryToken).type = BSQT_SERVICE_SEARCH_ATTRIB_REQ;
    HS(sdpQueryToken).callback = HsEventHandler;

    HS(Initialized) = TRUE;

exit:
    OS_UnlockHs();
    return status;
}


/*---------------------------------------------------------------------------
 *            HS_Deregister()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Deinitializes RfComm service and SDP record
 *
 * Return:    (See header file)
 *
 */
BtStatus HS_Deregister(void)
{
    BtStatus status;

    OS_LockHs();

#if XA_ERROR_CHECK == XA_ENABLED
    if (HS(Initialized) == FALSE) {
        status = BT_STATUS_FAILED;
        goto exit;
    }

    if (HS(ConnectLevel) >= HS_RFCOMMOPEN) {
        status = BT_STATUS_BUSY;
        goto exit;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    status = RF_DeregisterServerChannel(&HS(rfSrvChannel), &HS(rfService));
    Assert(status != BT_STATUS_FAILED);
    
    status = RF_DeregisterService(&HS(rfService));
    Assert(status != BT_STATUS_FAILED);

    SDP_RemoveRecord(&HS(sdprecord));
    
    status = ME_UnregisterGlobalHandler(&HS(globalHandler));
    Assert(status == BT_STATUS_SUCCESS);

    HS(Initialized) = FALSE;
    Report(("HS: Headset Deinitialized\n"));

    HS(HdcRegistered) = FALSE;

#if XA_ERROR_CHECK == XA_ENABLED
exit:
#endif /* XA_ERROR_CHECK == XA_ENABLED */
    OS_UnlockHs();
    return status;
}

/*---------------------------------------------------------------------------
 *            HS_RegisterGateway()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Registers the HsDeviceContext containing the BD_ADDR and connect
 *            info of the audio gateway.  This function MUST be called before a 
 *            connection can be created, if HS_USEDDB is disabled.
 *
 * Return:    (See header file)
 *
 */
BtStatus HS_RegisterGateway(HsDeviceContext *Hdc)
{
#if XA_DEBUG_PRINT == XA_ENABLED
    char        addr[BDADDR_NTOA_SIZE];
#endif
    BtStatus    status;

    /* The pointer to the HsDeviceContext cannot be NULL, and there shouldn't
     * be a connection open when this function is called
     */
    OS_LockHs();

    if ((Hdc != 0) && (HS(State) == HS_IDLE) && (HS(Initialized) == TRUE)) {
        OS_MemCopy((U8 *)&HS(Hdc), (const U8 *)Hdc, sizeof(HS(Hdc)));
        HS(HdcRegistered) = TRUE;
        Report(("HS: Registered Gateway %s\n", bdaddr_ntoa(&HS(Hdc).addr, addr)));
        HsChangeDdbRecord();
        status = BT_STATUS_SUCCESS;
    }
    else {
        status = BT_STATUS_FAILED;
    }

    OS_UnlockHs();
    return status;
}


/*---------------------------------------------------------------------------
 *            HS_EnableSecurity()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Enables (and disables) security.  If flag == TRUE, then security
 *            is turned on and the PIN is stored. If security was already 
 *            enabled, the new PIN is stored.  If flag == FALSE security is 
 *            disabled.
 *
 * Return:    (See header file)
 *
 */
#if HS_SECURITY == XA_ENABLED
BtStatus HS_EnableSecurity(BOOL flag)
{
    BtStatus status;

    OS_LockHs();

    if (flag == TRUE) {
        /* set the PIN */
        if (HS(security) == FALSE) {
            /* Enable Security Mode 3, but do not require encryption */
            status = SEC_EnableSecurityMode3(&HS(handler), FALSE);
#if XA_ERROR_CHECK == XA_ENABLED
            if ((status != BT_STATUS_PENDING) && (status != BT_STATUS_SUCCESS)) {
                status = BT_STATUS_FAILED;
                goto exit;
            }
#endif /* XA_ERROR_CHECK == XA_ENABLED */
        }
        HS(security) = TRUE;
    }
    else if (flag == FALSE) {
        SEC_DisableSecurityMode3(&HS(handler));
        HS(security) = FALSE;
    }

#if XA_ERROR_CHECK == XA_ENABLED
exit:
#endif /* XA_ERROR_CHECK == XA_ENABLED */
    OS_UnlockHs();
    return BT_STATUS_SUCCESS;
}
#endif /* HS_SECURITY */


/*---------------------------------------------------------------------------
 *            HS_EnterPinCode()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  If a PIN request is received from the stack, the app is notified
 *            via its callback.  The app is then responsible for calling this 
 *            function w/ the PIN which will be sent back to the stack.
 *
 * Return:    (See header file)
 *
 */
#if HS_SECURITY == XA_ENABLED
BtStatus HS_EnterPinCode(HsPinCodeReq   *pinParms)
{
    BtStatus status;

    OS_LockHs();

    status = SEC_SetPin(HS(link), pinParms->Pin, pinParms->PinLen, pinParms->TrustLevel);
    if (status == BT_STATUS_PENDING) {
        status = BT_STATUS_SUCCESS;
    } else {
        status = BT_STATUS_FAILED;
    }

    OS_UnlockHs();
    return status;
}
#endif


/*---------------------------------------------------------------------------
 *            HS_DoConnect()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Starts the connection to the headset, app will be notified
 *            via callback when the connection has been established
 *
 * Return:    (See header file)
 *
 */
BtStatus HS_DoConnect(void)
{
    BtStatus status = BT_STATUS_FAILED;

    OS_LockHs();

#if XA_ERROR_CHECK == XA_ENABLED
    if (HS(HdcRegistered) == FALSE) {
        goto exit;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */


    switch (HS(ConnectLevel)) {
    case HS_IDLE:
        HS(OutGoingConn) = TRUE;
        HsChangeState(HS_OUTGOING);
        status = ME_CreateLink(&HS(handler), &HS(Hdc).addr, 0, &HS(link));
        if (status == BT_STATUS_SUCCESS) {
            /* Simulate Connect */
            BtEvent     e;
                    
            e.eType = BTEVENT_LINK_CONNECT_CNF;
            e.errCode = BEC_NO_ERROR;
            e.p.remDev = HS(link);    /* CreateLink() ret'd remDev struct */

            status = BT_STATUS_PENDING;
            HsEventHandler(&e);
        } else if (status == BT_STATUS_PENDING) {
            HS(ConnectLevel) = HS_ACLPENDING;
        }
        else {
            Report(("HS: Connect Link failed to start. Error = %s\n", pBT_Status(status)));
            HS(ConnectLevel) = HS_IDLE;
            /* We don't want to confuse the app if it receives BT_STATUS_NO_RESOURCES
             * or any other failure status from ME_CreateLink so just return failed.
             */
            status = BT_STATUS_FAILED;
        }
        break;

    case HS_ACLPENDING:
        status = BT_STATUS_PENDING;
        break;

    case HS_ACLOPEN:
        if (HsCheckSetMode() == BT_STATUS_SUCCESS) {
            status = HsStartServiceQuery(BSQM_FIRST);
            if (status == BT_STATUS_PENDING) {
                HS(ConnectLevel) = HS_SDPPENDING;
            } else {
                /* SDP query failed to start properly, so disconnect */
                Report(("HS: SDP query failed to start, aborting connection\n"));
                HsDisconnect();
                status = BT_STATUS_FAILED;
            }
        } else {
            /* Could not come out of park or sniff mode */
            HsDisconnect();
            status = BT_STATUS_FAILED;
        }
        break;

    case HS_SDPPENDING:
        status = BT_STATUS_PENDING;
        break;

    case HS_SDPSUCCESS:
        if (HsCheckSetMode() == BT_STATUS_SUCCESS) {
            status = RF_OpenClientChannel(HS(link),
                                              HS(remoteRfcServer),
                                              &HS(rfCltChannel), 1);
            if (status != BT_STATUS_PENDING) {
                Report(("HS: RFCOMM Channel failed to open, aborting connection\n"));
                HsDisconnect();
                status = BT_STATUS_FAILED;
            }
            else {
                HS(ConnectLevel) = HS_RFCOMMPENDING;
            }
        } else {
            /* Could not come out of park or sniff mode */
            HsDisconnect();
            status = BT_STATUS_FAILED;
        }
        break;

    case HS_RFCOMMPENDING:
        status = BT_STATUS_PENDING;
        break;

    case HS_RFCOMMOPEN:
        status = HsSendCKPD();
        break;

    case HS_SCOOPEN:
        status = BT_STATUS_SUCCESS;
    }
#if XA_ERROR_CHECK == XA_ENABLED
exit:
#endif /* XA_ERROR_CHECK == XA_ENABLED */
    OS_UnlockHs();
    return status;
}


/*---------------------------------------------------------------------------
 *            HS_DoAccept()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Accepts the incoming call.  The app will be notified via the
 *            callback when the connection has been established.
 *
 * Return:    (See header file)
 *
 */
BtStatus HS_DoAccept()
{
    BtStatus status;
    
    OS_LockHs();

    /* ACL Connection already established, RFCOMM channel open */
    status = HsSendCKPD();

    OS_UnlockHs();
    return status;
}

/*---------------------------------------------------------------------------
 *            HS_DoDisconnect()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Sends a message to the audio gateway to disconnect.  App will be 
 *            notified via the callback when the disconnect process is complete
 *
 * Return:    (See header file)
 *
 */
BtStatus HS_DoDisconnect(void) 
{
    BtStatus status;

    OS_LockHs();

#if XA_ERROR_CHECK == XA_ENABLED
    if (HS(ConnectLevel) == HS_IDLE) {
        status = BT_STATUS_SUCCESS;
        goto exit;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    /* This will disconnect the call */
    status = HsSendCKPD();
    if (status == BT_STATUS_SUCCESS) {
        status = BT_STATUS_PENDING;
    }

#if XA_ERROR_CHECK == XA_ENABLED
exit:
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    OS_UnlockHs();
    return status;
}

/*---------------------------------------------------------------------------
 *            HS_ChangeVolume()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Changes the volume on the headset and informs the audio gateway
 *            of the change.
 *
 * Return:    (See header file)
 *
 */
BtStatus HS_ChangeVolume(HsVolumeType type, HsVolumeChange dir, U8 vol)
{
    BtStatus status;
    U8       gain = 0;
    BtPacket *txPacket;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM,
                      vol <= HS_MAX_VOLUME);

    OS_LockHs();

    if ((status = HsCheckSetMode()) != BT_STATUS_SUCCESS) {
        goto exit;
    }

    if (HS(ConnectLevel) < HS_RFCOMMOPEN) {
        status = BT_STATUS_FAILED;
        goto exit;
    }

    if (!IsListEmpty(&(HS(packetList)))) {
        txPacket = (BtPacket *)RemoveHeadList(&(HS(packetList)));
    }
    else {
        status = BT_STATUS_BUSY;
        goto exit;
    }


    switch (dir) {
    case HS_UPVOL:
        if (type == HS_MIC) {
            if (HS(MicVol) < HS_MAX_VOLUME) 
                HS(MicVol)++;
                gain = HS(MicVol);
        }
        else {
            Assert(type == HS_SPK);
            if (HS(SpkVol) < HS_MAX_VOLUME)
                HS(SpkVol)++;
                gain = HS(SpkVol);
        }
        break;

    case HS_DOWNVOL:
        if (type == HS_MIC) {
            if (HS(MicVol) > 0) 
                HS(MicVol)--;
                gain = HS(MicVol);
        }
        else {
            Assert(type == HS_SPK);
            if (HS(SpkVol) > 0)
                HS(SpkVol)--;
                gain = HS(SpkVol);
        }
        break;

    case HS_SETVOL:
        gain = vol;
        if (type == HS_MIC)
            HS(MicVol) = gain;
        else
            HS(SpkVol) = gain;
        break;
    }

    HS(volbuff[5]) = type;
    if (gain > 9) {
        HS(volbuff[7]) = '1';
        HS(volbuff[8]) = gain - 10 + '0';
    }
    else {
        HS(volbuff[7]) = '0';
        HS(volbuff[8]) = gain + '0';
    }
    txPacket->dataLen = 10;
    txPacket->data = HS(volbuff);

    status = RF_SendData(HS(DefaultChannel), txPacket);
    if (status == BT_STATUS_PENDING) {
        status = BT_STATUS_SUCCESS;
        if (type == HS_MIC) {
            HsLastPacketType(HS_MICVOL_CHANGE);
        }
        if (type == HS_SPK) {
            HsLastPacketType(HS_SPKVOL_CHANGE);
        }
    }

exit:

    OS_UnlockHs();
    return status;
}


/*---------------------------------------------------------------------------
 *            HS_ButtonPressed()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Depending on the state and which button was pressed, this
 *            will start an outgoing call, accept an incoming call, 
 *            disconnect a call, or change the volume on the microphone or 
 *            the speaker.
 *
 * Return:    (See header file)
 *
 */
BtStatus HS_ButtonPressed(HsButton   buttonNum)
{
    BtStatus status = BT_STATUS_FAILED;

    OS_LockHs();

    if (buttonNum == HS_BUTTON) {
        if (HS(ConnectLevel) < HS_RFCOMMOPEN) {
            status = HS_DoConnect();
        }
        else {
            status = HS_DoAccept();
        }
        goto exit;
    }

    if (HS(ConnectLevel) < HS_RFCOMMOPEN) {
        status = BT_STATUS_FAILED;
        goto exit;
    }

    switch(buttonNum) {
    case HS_BUTTON_MVDOWN:
        status = HS_ChangeVolume(HS_MIC, HS_DOWNVOL, 0);
        break;
        
    case HS_BUTTON_MVUP:
        status = HS_ChangeVolume(HS_MIC, HS_UPVOL, 0);
        break;

    case HS_BUTTON_SVDOWN:
        status = HS_ChangeVolume(HS_SPK, HS_DOWNVOL, 0);
        break;
        
    case HS_BUTTON_SVUP:
        status = HS_ChangeVolume(HS_SPK, HS_UPVOL, 0);
        break;
    }

exit:

    OS_UnlockHs();
    return status;
}

/*---------------------------------------------------------------------------
 *            HS_GetState()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Returns the current state of the headset.
 *
 * Return:    (See header file)
 *
 */
HsState HS_GetState(void)
{
    return HS(State);
}

/*---------------------------------------------------------------------------
 *            HS_GetRemoteDevice()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Returns a link to the BtRemoteDevice structure, which contains
 *            information about the current link (link policy, BD_ADDR, etc).
 *
 * Return:    HS(link)
 *
 */
BtRemoteDevice *HS_GetRemoteDevice(void)
{
    return HS(link);
}


/****************************************************************************
 *
 * Internal functions
 *
 ****************************************************************************/

/*---------------------------------------------------------------------------
 *            HsAclDisconnect()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Disconnects the ACL link
 *
 * Return:    
 *
 */
BtStatus HsDisconnect(void)
{
    BtStatus status;

    /* We are bailing out of a connect request for some reason */
    if (HS(ConnectLevel) >= HS_RFCOMMOPEN) {
        status = RF_CloseChannel(HS(DefaultChannel));
        if (status != BT_STATUS_PENDING) {
            status = BT_STATUS_FAILED;
        }
    }
    else if (HS(ConnectLevel) >= HS_ACLOPEN) {
        status = ME_DisconnectLink(&HS(handler), HS(link));
        if (status != BT_STATUS_FAILED) {
            /* whatever state we are in just return everything to IDLE */
            HS(link) = 0;
            HsChangeState(HS_IDLE);   
            HS(ConnectLevel) = HS_IDLE;

        } else {
            Report(("HS: Link Disconnect failed. Error = %i\n", status));
        } 
    }
    else {
        status = BT_STATUS_SUCCESS;
    }

    return status;
} 


/*---------------------------------------------------------------------------
 *            HsSendCKPD()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Sends AT commands to the headset audio gateway.  See Section 4.6
 *            of the Headset Profile for syntax of each AT command.
 *
 * Return:    
 *
 */
BtStatus HsSendCKPD(void) 
{
    BtStatus status;
    BtPacket *txPacket;

    if (HS(ConnectLevel) < HS_RFCOMMOPEN) {
        return BT_STATUS_FAILED;
    }

    if (!IsListEmpty(&(HS(packetList)))) {
        txPacket = (BtPacket *)RemoveHeadList(&(HS(packetList)));
    }
    else {
        return BT_STATUS_BUSY;
    }

    txPacket->dataLen = 12;
    txPacket->data = (char *)hsckpdbuff;
        
    if (HsCheckSetMode() == BT_STATUS_SUCCESS) {
        status = RF_SendData(HS(DefaultChannel),txPacket);
        if (status == BT_STATUS_PENDING) {
            HsLastPacketType(HS_STATE_CHANGE);
            status = BT_STATUS_SUCCESS;
        }
    } else {
        status = BT_STATUS_FAILED;
    }

    return status;
}

/*---------------------------------------------------------------------------
 *            HsChangeState()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Changes headset's state and calls the app's callback
 *            to notify the app of the new state.
 *
 * Return:    
 *
 */
static void HsChangeState(HsState newstate)
{
    /* The new and old states are the same, so there is nothing to change 
     * The only state we send consecutive notices about is HS_INCOMING.  This
     * is to inform the app of each RING that we receive.
     */
    if ((HS(State) == newstate) && (HS(State) != HS_INCOMING)) {
        return;
    }

    HS(State) = newstate;

    HS(Callbackinfo).event = HS_STATE_CHANGE;
    HS(Callbackinfo).info.state = newstate;
    HS(AppCallback(&HS(Callbackinfo)));

    if (HS(State) == HS_IDLE) {
        HS(OutGoingConn) = FALSE;
        HS(link) = 0;    
    }
}

void HsLastPacketType(U8  type)
{
    HS(lastPacket) = type;
}

/*---------------------------------------------------------------------------
 *            HsStartServiceQuery()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initiate the SDP service query.
 *
 * Return:    
 *
 */
static BtStatus HsStartServiceQuery(SdpQueryMode mode)
{
    BtStatus status;

    HS(sdpQueryToken).rm = HS(link);
    status = SDP_Query(&HS(sdpQueryToken), mode);
    return status;
} 


/*---------------------------------------------------------------------------
 *            HsEventHandler()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handles all ME and SDP events
 *
 * Return:    
 *
 */
static void HsEventHandler(const BtEvent *event)
{
    BtStatus        status;
#if HS_SECURITY == XA_ENABLED && XA_DEBUG_PRINT == XA_ENABLED
    char            addr[BDADDR_NTOA_SIZE];
#endif

    OS_LockHs();

    switch (event->eType) {
    case BTEVENT_LINK_CONNECT_CNF:
        if (event->errCode == BEC_NO_ERROR) {

            Assert(HS(link) == event->p.remDev);

            HS(ConnectLevel) = HS_ACLOPEN;
            HS_DoConnect();
        } else {

            /* Link Connect Failed case */
            Report(("HS: Headset: Outbound Link Failed.\n"));
            HS(ConnectLevel) = HS_IDLE;
            HsChangeState(HS_IDLE);
        }
        break;
        
    case BTEVENT_LINK_DISCONNECT:
        Report(("HS: ACL link Disconnected.\n"));
        HS(ConnectLevel) = HS_IDLE;
        HsChangeState(HS_IDLE);
        break;

    case BTEVENT_SCO_CONNECT_REQ:
        HS(scoConnect) = event->p.scoConnect.scoCon;
        HsScoOpen(BEC_NO_ERROR);
        break;

    case BTEVENT_SCO_CONNECT_IND:
        if (event->errCode != 0) {
            Report(("HS: SCO connect error (ind) %d\n", event->errCode));
            HS(scoConnect) = 0;
        }
        else {
            HS(scoConnect) = event->p.scoConnect.scoCon; 
            HS(ConnectLevel) = HS_SCOOPEN;

            /* Once we are connected, whether the headset initiated the connection
             * or not doesn't matter
             */
            HS(OutGoingConn) = FALSE;
            HsChangeState(HS_CONNECTED);

            OS_MemCopy((U8 *)&HS(Hdc).addr.addr, (const U8 *)event->p.remDev->bdAddr.addr, 6);  
            HS(HdcRegistered) = TRUE;
            HsChangeDdbRecord();
        }
        break;

    case BTEVENT_SCO_DISCONNECT:
        Report(("HS: SCO Disconnected.\n"));
        HS(scoConnect) = 0;
        HS(ConnectLevel) = HS_RFCOMMOPEN;
        break;

    case SDEVENT_QUERY_RSP:
        status = HsVerifySdpQueryRsp(event->p.token);
        if (status == BT_STATUS_SUCCESS) {
            HS(ConnectLevel) = HS_SDPSUCCESS;
            status = HS_DoConnect();
        }
        else if (status == BT_STATUS_SDP_CONT_STATE) {
            /* We are going to query again for the rest of the response */
            status = HsStartServiceQuery(BSQM_CONTINUE);
            if (status != BT_STATUS_PENDING) {
                /* SDP query failed to start properly, so disconnect */
                Report(("HS: SDP query failed to start, aborting connection\n"));
                HsDisconnect();
            }
            break;
        }
        else {
            HsDisconnect();
        }
        break;

        /* Headset profile was not found, drop through to error case */
    case SDEVENT_QUERY_ERR:
    case SDEVENT_QUERY_FAILED:
        Report(("HS: Headset: SDP Query Failed\n"));
        
        HS(ConnectLevel) = HS_ACLOPEN;
        HsDisconnect();
        break;

#if HS_SECURITY == XA_ENABLED
    case BTEVENT_PIN_REQ: 
        Report(("HS: PIN Request Received.\n"));
        /* call the app's callback with a PIN_REQUEST event */
        HS(Callbackinfo).event = HS_PIN_REQUEST;
        HS(link) = event->p.remDev;
        HS(AppCallback(&HS(Callbackinfo)));
        break;

    case BTEVENT_PAIRING_COMPLETE:
        /* set headset_gw to the newly paired device */
        OS_MemCopy((U8 *)&HS(Hdc).addr.addr, (const U8 *)event->p.remDev->bdAddr.addr, 6);  
        HS(HdcRegistered) = TRUE;
        Report(("HS: Paired with %s\n", bdaddr_ntoa(&HS(Hdc).addr, addr)));
        HsChangeDdbRecord();
        break;

    case BTEVENT_SECURITY3_COMPLETE:
        Report(("HS: Security Mode 3 Complete.\n"));
        break;
#endif /* HS_SECURITY */

    }

    OS_UnlockHs();
}


/*---------------------------------------------------------------------------
 *            HsGlobalEventHandler()
 *---------------------------------------------------------------------------
 *
 * Synopsis:   Processes HCI Initialization events.
 *
 * Return:     void
 */
static void HsGlobalEventHandler(const BtEvent *event)
{
    BtStatus         status;
#if HS_USEDDB == XA_ENABLED
    BtDeviceRecord   record; 
#if XA_DEBUG_PRINT == XA_ENABLED
    char             addr[BDADDR_NTOA_SIZE];
#endif
#endif

    OS_LockHs();

    switch (event->eType) {
    case BTEVENT_LINK_CONNECT_IND:
        HS(ConnectLevel) = HS_ACLOPEN;
        HS(link) = event->p.remDev;
        break;

    case BTEVENT_LINK_DISCONNECT:
        HS(ConnectLevel) = HS_IDLE;
        HsChangeState(HS_IDLE);
        break;

    case BTEVENT_HCI_INITIALIZED: 
        /* Set the accessibility mode */
        status = ME_SetAccessibleModeNC(BAM_GENERAL_ACCESSIBLE, 0);
        if (status == BT_STATUS_PENDING) {
            Report(("HS: Setting General Accessible mode.\n"));
        } 
        else if (status != BT_STATUS_SUCCESS) {
            Report(("HS: Accessibility operation failed. Error = %s.\n", pBT_Status(status)));
        }

#if HS_USEDDB == XA_ENABLED
        if (HS(HdcRegistered) == FALSE) {
            /* If there are any DDB records, register the first DDB record as an audio gateway */
            status = SEC_EnumDeviceRecords(0, &record);
            if (status == BT_STATUS_SUCCESS) {
                /* a record was found, so we can register it as an audio gateway */
                OS_MemCopy((U8 *)&HS(Hdc).addr.addr, (const U8 *)record.bdAddr.addr, 6);  
                Report(("HS:  Using %s as a gateway\n", bdaddr_ntoa(&record.bdAddr, addr)));
                HS(HdcRegistered) = TRUE;
            }
        }
#endif
        break;

#if XA_DEBUG == XA_ENABLED
    case BTEVENT_HCI_DEINITIALIZED:
        Report(("AG: Radio is down\n"));
        break;

    case BTEVENT_HCI_FAILED:
        Report(("HS: HCI Initialization FAILED\n"));
        break;
#endif
    }

    OS_UnlockHs();
}

/*---------------------------------------------------------------------------
 *            HsChangeDdbRecord()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Stores the new device record in the DDB, overwriting any previous
 *            records that may have been in the DDB.  Only enable HS_USEDDB if
 *            no other application will be running on this device!!
 *
 * Return:    void
 */
static void HsChangeDdbRecord(void)
{
#if HS_USEDDB != XA_ENABLED
    return;
#else

    BtStatus        status;
    U8              i;
    BtDeviceRecord  record;
    BOOL            addrFound;
#if XA_DEBUG_PRINT == XA_ENABLED
    char            addr[BDADDR_NTOA_SIZE];
#endif

    /* need to remove all DDB records that do not match with the newly paired device */
    i = 0;
    addrFound = FALSE;
    while (SEC_EnumDeviceRecords(i, &record) == BT_STATUS_SUCCESS) {
        if (!OS_MemCmp(record.bdAddr.addr, 6, (const U8 *)&HS(Hdc).addr.addr, 6) == TRUE) {
            Report(("Deleting old device record\n"));
            status = SEC_DeleteDeviceRecord(&record.bdAddr);
            if (status != BT_STATUS_SUCCESS)
                Report(("Error deleting record\n"));
        }
        else {
            addrFound = TRUE;
        }
        i++;
    }

    if (addrFound == FALSE) {
        OS_MemCopy((U8 *)&record.bdAddr.addr, (const U8 *)&HS(Hdc).addr.addr, 6);
        record.trusted = TRUE;  
        status = SEC_AddDeviceRecord(&record);
        if (status == BT_STATUS_SUCCESS) {
            Report(("HS: Paired with new device: %s\n", bdaddr_ntoa(&HS(Hdc).addr, addr)));
        }
        else {
            Report(("HS: Pairing failed\n"));
        }
    }
#endif /* HS_USEDDB */
}
 
/*---------------------------------------------------------------------------
 *            HsVerifySdpQueryRsp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Parses the Headset AG's SDP response for the RFCOMM channel.  
 *            In the case of a continuation state, additional SDP queries 
 *            will be issued until the attribute has been found or until
 *            the entire SDP response has been parsed.
 *            
 *
 * Return:    BT_STATUS_SUCCESS if SDP parsing is successful.
 *            BT_STATUS_FAILED if SDP parsing encountered an error.
 *            BT_STATUS_SDP_CONT_STATE if SDP parsing encountered a 
 *            continuation state that requires an additional SDP query.
 *
 */
static BtStatus HsVerifySdpQueryRsp(SdpQueryToken *token)
{
    BtStatus        status = BT_STATUS_FAILED;

    /* Get the RFCOMM Channel */
    token->attribId = AID_PROTOCOL_DESC_LIST;

    token->uuid = PROT_RFCOMM;
    if ((status = SDP_ParseAttributes(token)) == BT_STATUS_SUCCESS) {
        /* We had better have a U8 channel value */
        if ((token->totalValueLen != 1) || (token->availValueLen != 1)) {
            status = BT_STATUS_FAILED;
            goto done;
        }
        Report(("HS: SDP_ParseAttributes succeeded! RFCOMM Channel = %d,"
                "Updated Buff Len = %d\n", token->valueBuff[0], 
                token->remainLen));
        /* Assign the RFCOMM channel number */
        HS(remoteRfcServer) = token->valueBuff[0];
    }
    
done:
    if (status == BT_STATUS_SDP_CONT_STATE) {
        Report(("HS: SDP_ParseAttributes - Continuation State. Query Again!\n"));
        token->mode = BSPM_CONT_STATE;
        return status;
    } else if (status == BT_STATUS_FAILED) {
        /* Failed to retrieve the RFCOMM channel number */
        Report(("HS: SDP_ParseAttributes - Failure!\n"));
    }
    
    /* Reset the SDP parsing mode */
    token->mode = BSPM_BEGINNING;

    return status;
}

/*---------------------------------------------------------------------------
 *            HsRFC_Callback()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  RFCOMM callback; handles all RFCOMM events.
 *
 * Return:    
 *
 */
static void HsRFC_Callback(RfChannel *Channel, RfCallbackParms *Parms)
{
    switch (Parms->event) {

    case RFEVENT_OPEN_IND:
        HS(DefaultChannel) = Channel;
        RF_AcceptChannel(Channel);
        break;

    case RFEVENT_OPEN:
        HS(DefaultChannel) = Channel;
        SCO_RegisterHandler(HS(link), &HS(handler));
        if (HS(ConnectLevel) == HS_RFCOMMPENDING) {

            HS(ConnectLevel) = HS_RFCOMMOPEN;
            /* headset is setting up an outgoing connection, so send an AT+CKPD command */
            HsSendCKPD();
        }
        else {
            HS(ConnectLevel) = HS_RFCOMMOPEN;
        }
        break;
    
    case RFEVENT_PACKET_HANDLED:
        InsertTailList(&(HS(packetList)), &Parms->ptrs.packet->node);
        break;

    case RFEVENT_DATA_IND:
        RF_AdvanceCredit(Channel, 1);
        HsProcessRxData(Channel, Parms);
        break;

    case RFEVENT_CLOSE_IND:
        break;

    case RFEVENT_CLOSED:
        HS(ConnectLevel) = HS_ACLOPEN;
        if (HS(OutGoingConn) == TRUE) {
            HsDisconnect();
        }
        SCO_UnregisterHandler(HS(link), &HS(handler));
        HsChangeState(HS_DISCONNECTING);        
        break;

    default:
        Report(("HS: Unknown RFCOMM event: %i\n", Parms->event));
        break;
    }
}

/*---------------------------------------------------------------------------
 *            HsProcessRxData()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Processes Rfcomm data (AT commands) from the audio gateway
 *
 * Return:    
 *
 */
static void HsProcessRxData(RfChannel *Channel, RfCallbackParms *Parms)
{
    U16 dataLen = 0;
    U8  gain = 0;

    dataLen = Parms->dataLen;

    /* Incoming RING */
    if ((dataLen == 8) && (OS_MemCmp(Parms->ptrs.data, 8, hsringbuff, 8))) {
       
        Report(("HS: Incoming Call...\n"));
        /* Incoming RING command from Gateway, switch state to INCOMING */
        HsChangeState(HS_INCOMING);
    }

    /* Volume Data */
    else if ((dataLen >= 10) && (OS_MemCmp(Parms->ptrs.data, 5, hsvolresbuff, 5))) {
        if (dataLen == 10) {
            gain = (Parms->ptrs.data[7] - '0');
        } else {
            if (Parms->ptrs.data[7] == '1') {
                gain = (10 + (Parms->ptrs.data[8] - '0'));
            } else {
                gain = (Parms->ptrs.data[8] - '0');
            }
        }
        if (gain > HS_MAX_VOLUME) {
            Report(("Invalid volume gain level\n"));
            return;
        }

        switch(Parms->ptrs.data[5]) {           
        /* Microphone Gain */
        case HS_MIC:
            HS(MicVol) = gain;
            HS(Callbackinfo).event = HS_MICVOL_CHANGE;
            HS(Callbackinfo).info.vol.status = BT_STATUS_SUCCESS;
            HS(Callbackinfo).info.vol.volume = HS(MicVol);
            HS(AppCallback(&HS(Callbackinfo)));
            break;

        /* Speaker Gain */
        case HS_SPK:
            HS(SpkVol) = gain;
            HS(Callbackinfo).event = HS_SPKVOL_CHANGE;
            HS(Callbackinfo).info.vol.status = BT_STATUS_SUCCESS;
            HS(Callbackinfo).info.vol.volume = HS(SpkVol);
            HS(AppCallback(&HS(Callbackinfo)));
            break;
        }
    }
    /* OK message */
    else if ((dataLen == 6) && (OS_MemCmp(Parms->ptrs.data, 6, hsokbuff, 6))) {
        if (HS(lastPacket) == HS_MICVOL_CHANGE) {
            HS(Callbackinfo).event = HS_MICVOL_CHANGE;
            HS(Callbackinfo).info.vol.status = BT_STATUS_SUCCESS;
            HS(Callbackinfo).info.vol. volume = HS(MicVol);
            HS(AppCallback(&HS(Callbackinfo)));
        } 
        else if (HS(lastPacket) == HS_SPKVOL_CHANGE) {
            HS(Callbackinfo).event = HS_SPKVOL_CHANGE;
            HS(Callbackinfo).info.vol.status = BT_STATUS_SUCCESS;
            HS(Callbackinfo).info.vol. volume = HS(SpkVol);
            HS(AppCallback(&HS(Callbackinfo)));
        } 
        return;
    }
    /* Error message */
    else if ((dataLen == 9) && (OS_MemCmp(Parms->ptrs.data, 9, hserrbuff, 9))) {
        Report(("HS: ERROR message received\n"));
        if (HS(lastPacket) == HS_MICVOL_CHANGE) {
            HS(Callbackinfo).event = HS_MICVOL_CHANGE;
            HS(Callbackinfo).info.vol.status = BT_STATUS_FAILED;
            HS(AppCallback(&HS(Callbackinfo)));
        }
        else if (HS(lastPacket) == HS_SPKVOL_CHANGE) {
            HS(Callbackinfo).event = HS_SPKVOL_CHANGE;
            HS(Callbackinfo).info.vol.status = BT_STATUS_FAILED;
            HS(AppCallback(&HS(Callbackinfo)));
        } 
        if (HS(OutGoingConn) == TRUE) {
            /* The Audio gateway is not accepting our connect request, so
             * disconnect the RFCOMM and ACL links 
             */
            HsDisconnect();
        }
        return;
    }
    else {
        Report(("HS: Invalid Rf data received: (%s)\n", Parms->ptrs.data));
        return;
    }
}

/*---------------------------------------------------------------------------
 *            HsScoOpen()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  If the reason is good, then accept the incoming SCO connection,
 *            otherwise reject the connection. Reason is the BtErrorCode
 *            returned from the BTEVENT_SCO_VSET_COMPLETE event.
 *
 * Return:    
 *
 */
static void HsScoOpen(BtErrorCode reason)
{
    BtStatus status;

    if (reason != BEC_NO_ERROR) {
        status = SCO_RejectIncomingConnection(HS(scoConnect), &HS(handler), reason);
        Report(("HS: SCO Connect Reject Status %s\n", pBT_Status(status)));
    } else {
        status = SCO_AcceptIncomingConnection(HS(scoConnect), &HS(handler));
        Report(("HS: SCO Connect Accept Status %s\n", pBT_Status(status)));
    }
}

/*---------------------------------------------------------------------------
 *            HsCheckSetMode()
 *---------------------------------------------------------------------------
 *
 * Synopsis: Checks the ACL link mode and makes it active if necessary.
 *           
 *
 * Return:   none
 *
 */
BtStatus HsCheckSetMode(void)
{
    BtStatus status;

    if (ME_GetCurrentMode(HS(link)) == BLM_PARK_MODE) {
        status = ME_StopPark(HS(link));
        if ((status != BT_STATUS_PENDING) &&
            (status != BT_STATUS_IN_PROGRESS)) {
            Report(("HS: Could not bring device out of Park Mode.\n"));
            return BT_STATUS_FAILED;
        }
    } else if (ME_GetCurrentMode(HS(link)) == BLM_SNIFF_MODE) {
        ME_StopSniff(HS(link));
    }

    return BT_STATUS_SUCCESS;
}


#if XA_DEBUG == XA_ENABLED
static U8  HsLock = 0;

void OS_LockHs(void)
{
    OS_LockStack();

    Assert(HsLock < 4);

    HsLock++;
}

void OS_UnlockHs(void)
{
    Assert(HsLock > 0);

    HsLock--;

    OS_UnlockStack();
}

#endif /* BT_HSP_HEADSET == XA_ENABLED */

#endif /* XA_DEBUG == XA_ENABLED */
