/***************************************************************************
 *
 * File:
 *     $Workfile:goep.c$ for XTNDAccess Blue SDK, Version 1.3
 *     $Revision:105$
 *
 * Description:
 *     This file contains the functions that comprise the Generic Object
 *     Exchange profile implementation.
 *
 * Created:
 *     September 4, 2000
 *
 * Copyright 2000-2005 Extended Systems, Inc.  ALL RIGHTS RESERVED.
 *
 * Unpublished Confidential Information of Extended Systems, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of Extended Systems, Inc.
 * 
 * Use of this work is governed by a license granted by Extended Systems, 
 * Inc.  This work contains confidential and proprietary information of 
 * Extended Systems, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/
#include <osapi.h>
#include <sys/goepext.h>

#include <../../add-ins/obex/obdebug.c>

/*
 * The GOEP layer relies on events deliverd as part of the OBEX packet
 * flow control option. Therefore it must be enabled when using the GOEP.
 * It also uses server header build and client header parse functions.
 */
#if OBEX_PACKET_FLOW_CONTROL == XA_DISABLED
#error "OBEX_PACKET_FLOW_CONTROL Must be enabled."
#endif /* OBEX_PACKET_FLOW_CONTROL == XA_DISABLED */

#if OBEX_HEADER_BUILD == XA_DISABLED
#error "OBEX_HEADER_BUILD Must be enabled."
#endif /* OBEX_HEADER_BUILD == XA_DISABLED */

/****************************************************************************
 *
 * RAM Data
 *
 ****************************************************************************/
#if OBEX_ROLE_CLIENT == XA_ENABLED
#if XA_CONTEXT_PTR == XA_ENABLED
static GoepClientData   tempClient;
GoepClientData         *GoeClient = &tempClient;
#else /* XA_CONTEXT_PTR == XA_ENABLED */
GoepClientData          GoeClient;
#endif /* XA_CONTEXT_PTR == XA_ENABLED */
#endif /* OBEX_ROLE_CLIENT == XA_ENABLED */

#if OBEX_ROLE_SERVER == XA_ENABLED
#if XA_CONTEXT_PTR == XA_ENABLED
static GoepServerData   tempServer;
GoepServerData         *GoeServer = &tempServer;
#else /* XA_CONTEXT_PTR == XA_ENABLED */
GoepServerData          GoeServer;
#endif /* XA_CONTEXT_PTR == XA_ENABLED */
#endif /* OBEX_ROLE_SERVER == XA_ENABLED */

/****************************************************************************
 *
 * Internal Function Prototypes
 *
 ****************************************************************************/
#if OBEX_ROLE_CLIENT == XA_ENABLED
#if BT_STACK == XA_ENABLED
static void GoepClntBtEventHandler(const BtEvent *event);
#endif /* BT_STACK == XA_ENABLED */
static void GoepClntCallback(ObexClientCallbackParms *parms);
static void GoepClientProcessHeaders(ObexClientCallbackParms *parms);
static void NotifyCurrClient(ObexClientApp *clientApp, GoepEventType Event);
static void NotifyAllClients(ObexClientApp *clientApp, GoepEventType Event);
#if GOEP_ADDITIONAL_HEADERS > 0
static BOOL ClientBuildHeaders(GoepClientObexCons *client, U16 *more);
#endif /* GOEP_ADDITIONAL_HEADERS > 0 */
static U16  Strlen(const char *String);
#if GOEP_DOES_UNICODE_CONVERSIONS == XA_DISABLED
static U16  Unilen(const U16  *String);
#endif /* GOEP_DOES_UNICODE_CONVERSIONS == XA_DISABLED */
#endif /* OBEX_ROLE_CLIENT == XA_ENABLED */

#if OBEX_ROLE_SERVER == XA_ENABLED
#if BT_STACK == XA_ENABLED
static void GoepSrvrBtEventHandler(const BtEvent *event);
#endif /* BT_STACK == XA_ENABLED */
static void GoepSrvrCallback(ObexServerCallbackParms *parms);
static void GoepServerProcessHeaders(ObexServerCallbackParms *parms);
static void NotifyCurrServer(ObexServerApp *serverApp, GoepEventType Event);
static void NotifyAllServers(ObexServerApp *serverApp, GoepEventType Event);
static void StartServerOperation(ObexServerApp *serverApp, GoepOperation Op);
static void AssociateServer(ObexServerApp *serverApp);
#if GOEP_ADDITIONAL_HEADERS > 0
static BOOL ServerBuildHeaders(GoepServerObexCons *server);
#endif /* GOEP_ADDITIONAL_HEADERS > 0 */
#endif /* OBEX_ROLE_SERVER == XA_ENABLED */

#if GOEP_DOES_UNICODE_CONVERSIONS == XA_ENABLED
static U16  AsciiToUnicode(U16 *Unistring, const char *String);
#endif /* GOEP_DOES_UNICODE_CONVERSIONS == XA_ENABLED */
static void DoHeaderCopy(U8 *Dst, U16 *Len, U16 MaxLen, U8 *Src, U16 SrcLen);
static void DoUniHeaderCopy(GoepUniType *Dst, U16 *Len, U16 MaxLen, U8 *Src, U16 SrcLen);
static BOOL GetFreeConnId(U8 *id, U8 type);

/****************************************************************************
 *
 * Public API Functions
 *
 ****************************************************************************/
/*---------------------------------------------------------------------------
 *            GOEP_Init()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initialize the GOEP component.  This must be the first GOEP 
 *            function called by the application layer, or if multiple 
 *            GOEP applications exist, this function should be called
 *            at system startup (see XA_LOAD_LIST in config.h).  OBEX must 
 *            also be initialized separately.
 *
 * Return:    TRUE or FALSE
 *
 */
BOOL GOEP_Init(void)
{
    U8 i;

    OS_LockStack();

#if OBEX_ROLE_CLIENT == XA_ENABLED
#if XA_CONTEXT_PTR == XA_ENABLED
    OS_MemSet((U8 *)GoeClient, 0, sizeof(GoepClientData));
#else /* XA_CONTEXT_PTR == XA_ENABLED */
    OS_MemSet((U8 *)&GoeClient, 0, sizeof(GoepClientData));
#endif /* XA_CONTEXT_PTR == XA_ENABLED */
#endif /* OBEX_ROLE_CLIENT == XA_ENABLED */

#if OBEX_ROLE_SERVER == XA_ENABLED
#if XA_CONTEXT_PTR == XA_ENABLED
    OS_MemSet((U8 *)GoeServer, 0, sizeof(GoepServerData));
#else /* XA_CONTEXT_PTR == XA_ENABLED */
    OS_MemSet((U8 *)&GoeServer, 0, sizeof(GoepServerData));
#endif /* XA_CONTEXT_PTR == XA_ENABLED */
#endif /* OBEX_ROLE_SERVER == XA_ENABLED */

    /* Setup a connection Id and connection Count for each client/server */
    for (i = 0; i < GOEP_NUM_OBEX_CONS; i++) {
#if OBEX_ROLE_CLIENT == XA_ENABLED
        /* Setup the connId */
        GOEC(clients)[i].connId = i;
        /* Setup the connCount */
        GOEC(clients)[i].connCount = 0;
#endif /* OBEX_ROLE_CLIENT == XA_ENABLED */
#if OBEX_ROLE_SERVER == XA_ENABLED
        /* Setup the connId */
        GOES(servers)[i].connId = i;
        /* Setup the connCount */
        GOES(servers)[i].connCount = 0;
#endif /* OBEX_ROLE_SERVER == XA_ENABLED */
    }

    /* GOEP Initialized */
#if OBEX_ROLE_CLIENT == XA_ENABLED
    GOEC(initialized) = TRUE;
#endif /* OBEX_ROLE_CLIENT == XA_ENABLED */
#if OBEX_ROLE_SERVER == XA_ENABLED
    GOES(initialized) = TRUE;
#endif /* OBEX_ROLE_SERVER == XA_ENABLED */

    OS_UnlockStack();
    return TRUE;

}
 
#if OBEX_ROLE_SERVER == XA_ENABLED
/*---------------------------------------------------------------------------
 *            GOEP_RegisterServer()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initialize the OBEX Server.
 *
 * Return:    ObStatus
 *
 */
ObStatus GOEP_RegisterServer(GoepServerApp *Service, 
                             const ObStoreFuncTable *obStoreFuncs)
{
    ObStatus  status = OB_STATUS_SUCCESS;
    U8        id;
#if OBEX_SERVER_CONS_SIZE > 0
    U8        i;
#endif /* OBEX_SERVER_CONS_SIZE > 0 */

    OS_LockStack();

#if XA_ERROR_CHECK == XA_ENABLED
    if (GOES(initialized) != TRUE) {
        /* GOEP is not initialized */
        status = OB_STATUS_FAILED;
        goto Error;
    }

    if ((!Service) || (!obStoreFuncs) || (Service->type >= GOEP_MAX_PROFILES) || 
        (Service->connId >= GOEP_NUM_OBEX_CONS) || (Service->callback == 0) || 
#if OBEX_SERVER_CONS_SIZE > 0
        (Service->numTargets > OBEX_SERVER_CONS_SIZE) ||
#endif /* OBEX_SERVER_CONS_SIZE > 0 */
        ((Service->connFlags != GOEP_NEW_CONN) && 
        (Service->connFlags != GOEP_MUX_CONN))) {
        status = OB_STATUS_INVALID_PARM;
        goto Error;
    }

    if (Service->connFlags == GOEP_MUX_CONN) {
        /* This is an existing connection.  Make sure we aren't trying 
         * to add the same profile twice.
         */
        if (GOES(servers)[Service->connId].profiles[Service->type]) {
            status = OB_STATUS_INVALID_PARM;
            goto Error;
        }
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */
    
    Assert(Service && obStoreFuncs && (Service->type < GOEP_MAX_PROFILES) &&
           (Service->connId < GOEP_NUM_OBEX_CONS) && (Service->callback != 0) &&
           ((Service->connFlags == GOEP_NEW_CONN) || 
           (Service->connFlags == GOEP_MUX_CONN)));

    /* Create an OBEX connection if available, and if the Server requests a 
     * new connection 
     */
    if ((GOES(connCount) < GOEP_NUM_OBEX_CONS) && 
        (Service->connFlags == GOEP_NEW_CONN)) {
        /* Get a free server conn Id */
        if (!GetFreeConnId(&id, GOEP_SERVER)) {
            status = OB_STATUS_NO_RESOURCES;
            goto Error;
        }
        OBEX_InitAppHandle(&GOES(servers)[id].obs.handle, GOES(servers)[id].
                    headerBlock, (U16)GOEP_SERVER_HB_SIZE, obStoreFuncs);

        status = OBEX_ServerInit(&GOES(servers)[id].obs, GoepSrvrCallback, 
                                 OBEX_EnumTransports());

        if (status == OB_STATUS_SUCCESS) {
            Service->connId = id;
            /* Increment the current number of Obex connections */
            GOES(connCount)++;
        }
    }
    else if ((GOES(connCount) >= GOEP_NUM_OBEX_CONS) && 
             (Service->connFlags == GOEP_NEW_CONN)) {
        /* Not enough Obex connections allocated in GOEP to allocate a new 
         * Connection Id 
         */
        Report(("Current Obex connections = %i, greater than or equal to \
                 GOEP_NUM_OBEX_CONS = %i\n", GOES(connCount), 
                 GOEP_NUM_OBEX_CONS));
        status = OB_STATUS_NO_RESOURCES;
        goto Error;
    }

    if (status == OB_STATUS_SUCCESS) {
#if OBEX_SERVER_CONS_SIZE > 0
        for (i = 0; i < Service->numTargets; i++) {
            Assert(Service->type != GOEP_PROFILE_OPUSH);
            status = OBEX_ServerRegisterTarget(&GOES(servers)[Service->connId].obs, 
                                               Service->target[i]);
            if (status != OB_STATUS_SUCCESS) {
#if OBEX_DEINIT_FUNCS == XA_ENABLED
                if (GOES(servers)[Service->connId].connCount == 0)
                    OBEX_ServerDeinit(&GOES(servers)[Service->connId].obs);
#endif /* OBEX_DEINIT_FUNCS == XA_ENABLED */
                goto Error;
            }
        }
#endif /* OBEX_SERVER_CONS_SIZE > 0 */
        /* Set this server to "in use" */
        GOES(servers)[Service->connId].connCount++;
        GOES(servers)[Service->connId].profiles[Service->type] = Service;

        if (Service->appParent == 0)
            Service->appParent = Service->callback;
    }

Error:
    OS_UnlockStack();
    return status;
}

#if BT_SECURITY == XA_ENABLED
/*---------------------------------------------------------------------------
 *            GOEP_RegisterServerSecurityRecord()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Registers a security record for the GOEP service.
 *
 * Return:    (See goep.h)
 */
BtStatus GOEP_RegisterServerSecurityRecord(GoepServerApp *Service, BtSecurityLevel Level)
{
    BtStatus status = BT_STATUS_FAILED;

    OS_LockStack();

    /* Register security for the GOEP service */
    Service->secRecord.id =  SEC_GOEP_ID;
    Service->secRecord.channel = (U32)Service;
    Service->secRecord.level = Level;
    status = SEC_Register(&Service->secRecord);

    OS_UnlockStack();
    return status;
}

/*---------------------------------------------------------------------------
 *            GOEP_UnregisterServerSecurityRecord()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Unregisters a security handler for the GOEP service.
 *
 * Return:    (See goep.h)
 */
BtStatus GOEP_UnregisterServerSecurityRecord(GoepServerApp *Service)
{
    BtStatus status;

    OS_LockStack();

    /* Unregister security for the GOEP Service */
    status = SEC_Unregister(&Service->secRecord);

    OS_UnlockStack();

    return status;
}
#endif /* BT_SECURITY == XA_ENABLED */

/*---------------------------------------------------------------------------
 *            GOEP_GetObexServer()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Retrieves the OBEX Server pertaining to a specific GOEP Server.  
 *            This function is valid after GOEP_Init and GOEP_RegisterServer 
 *            have been called.
 *
 * Return:    ObexServerApp
 *
 */
ObexServerApp* GOEP_GetObexServer(GoepServerApp *Service)
{
    OS_LockStack();

#if XA_ERROR_CHECK == XA_ENABLED
    if ((!Service) || (Service->connId >= GOEP_NUM_OBEX_CONS)) {
        OS_UnlockStack();
        return 0;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(Service && (Service->connId < GOEP_NUM_OBEX_CONS));

    OS_UnlockStack();
    return &(GOES(servers)[Service->connId].obs);
}


#if OBEX_DEINIT_FUNCS == XA_ENABLED
/*---------------------------------------------------------------------------
 *            GOEP_DeregisterServer()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Deinitialize the OBEX Server.
 *
 * Return:    ObStatus
 *
 */
ObStatus GOEP_DeregisterServer(GoepServerApp *Service)
{
    ObStatus    status;
    GoepServerObexCons *server;
#if OBEX_SERVER_CONS_SIZE > 0
    U8          i;
#endif /* OBEX_SERVER_CONS_SIZE > 0 */

    OS_LockStack();

#if XA_ERROR_CHECK == XA_ENABLED
    if (!Service) {
        status = OB_STATUS_INVALID_PARM;
        goto Error;
    }

    if (GOES(initialized) != TRUE) {
        /* GOEP is not initialized, so there is nothing to deinit */
        status = OB_STATUS_SUCCESS;
        goto Error;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(Service);

    server = &GOES(servers)[Service->connId];
    
#if XA_ERROR_CHECK == XA_ENABLED
    if ((Service->type >= GOEP_MAX_PROFILES) || 
        (Service->connId >= GOEP_NUM_OBEX_CONS) ||
#if OBEX_SERVER_CONS_SIZE > 0
        (Service->numTargets > OBEX_SERVER_CONS_SIZE) ||
#endif /* OBEX_SERVER_CONS_SIZE > 0 */
        (server->profiles[Service->type] != Service)) {
        status = OB_STATUS_INVALID_PARM;
        goto Error;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert((Service->type < GOEP_MAX_PROFILES) &&
           (Service->connId < GOEP_NUM_OBEX_CONS) &&
           (server->profiles[Service->type] == Service));

    /* We had better have a connection open */
    if (GOES(connCount) == 0) {
        status = OB_STATUS_FAILED;
        goto Error;
    }

    /* See if they are processing an operation */
    if ((server->currOp.oper != GOEP_OPER_NONE) &&
        (server->currOp.handler == Service)) {
        status = OB_STATUS_BUSY;
        goto Error;
    }
    server->profiles[Service->type] = 0;

#if OBEX_SERVER_CONS_SIZE > 0
    for (i = 0; i < Service->numTargets; i++) {
        OBEX_ServerDeregisterTarget(&server->obs, Service->target[i]);
    }
#endif /* OBEX_SERVER_CONS_SIZE > 0 */

    if (--(server->connCount) > 0) {
        status = OB_STATUS_SUCCESS;
        goto Error;
    }

    --GOES(connCount);
    status = OBEX_ServerDeinit(&server->obs);

Error:
    OS_UnlockStack();
    return status;
}
#endif /* OBEX_DEINIT_FUNCS == XA_ENABLED */

/*---------------------------------------------------------------------------
 *            GOEP_ServerAbort
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Abort the current server operation.
 *
 * Return:    ObStatus
 *
 */
ObStatus GOEP_ServerAbort(GoepServerApp *Service, ObexRespCode Resp)
{
    ObStatus    status;
    GoepServerObexCons *server;

    OS_LockStack();

#if XA_ERROR_CHECK == XA_ENABLED
    if (!Service) {
        status = OB_STATUS_INVALID_PARM;
        goto Error;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(Service);

    server = &GOES(servers)[Service->connId];
    
#if XA_ERROR_CHECK == XA_ENABLED
    if ((Service->type >= GOEP_MAX_PROFILES) || 
        (Service->connId >= GOEP_NUM_OBEX_CONS) ||
        (server->profiles[Service->type] != Service)) {
        status = OB_STATUS_INVALID_PARM;
        goto Error;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert((Service->type < GOEP_MAX_PROFILES) &&
           (Service->connId < GOEP_NUM_OBEX_CONS) &&
           (server->profiles[Service->type] == Service));

    if (((Service != 0) && (Service != server->currOp.handler)) ||
        (server->currOp.oper == GOEP_OPER_NONE)) {
        status = OB_STATUS_FAILED;
        goto Error;
    }

    status = OBEX_ServerAbort(&server->obs, Resp);

Error:
    OS_UnlockStack();
    return status;
}

/*---------------------------------------------------------------------------
 *            GOEP_ServerAccept
 *---------------------------------------------------------------------------
 *
 * Synopsis:  This function is called to accept a Push or Pull request.
 *            It MUST be called during the GOEP_EVENT_PROVIDE_OBJECT indication for
 *            these operations. Failure to do so will abort the operation.
 *
 * Return:    ObStatus
 *
 */
ObStatus GOEP_ServerAccept(GoepServerApp *Service, void *Obsh)
{
    ObStatus            status;
    GoepServerObexCons *server;

    OS_LockStack();

#if XA_ERROR_CHECK == XA_ENABLED
    if (!Service) {
        status = OB_STATUS_INVALID_PARM;
        goto Error;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(Service);

    server = &GOES(servers)[Service->connId];
    
#if XA_ERROR_CHECK == XA_ENABLED
    if ((!Obsh) || (Service->type >= GOEP_MAX_PROFILES) || 
        (Service->connId >= GOEP_NUM_OBEX_CONS) ||
        (server->profiles[Service->type] != Service)) {
        status = OB_STATUS_INVALID_PARM;
        goto Error;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(Obsh && (Service->type < GOEP_MAX_PROFILES) && 
           (Service->connId < GOEP_NUM_OBEX_CONS) &&
           (server->profiles[Service->type] == Service));

    if ((server->currOp.handler != Service) ||
        (server->currOp.event != GOEP_EVENT_PROVIDE_OBJECT)) {
        status = OB_STATUS_FAILED;
        goto Error;
    }

    if (server->currOp.oper == GOEP_OPER_PULL && 
        server->currOp.info.pushpull.objectLen)
        OBEXH_Build4Byte(&server->obs.handle, OBEXH_LENGTH, 
                          server->currOp.info.pushpull.objectLen);

    server->object = Obsh;

    OBEX_ServerAccept(&server->obs, server->object);

    status = OB_STATUS_SUCCESS;

Error:
    OS_UnlockStack();
    return status;
}


#if OBEX_BODYLESS_GET == XA_ENABLED
/*---------------------------------------------------------------------------
 *            GOEP_ServerAcceptNoObject
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Accepts a GET without specifying a target object.
 *
 * Return:    ObStatus
 *
 */
ObStatus GOEP_ServerAcceptNoObject(GoepServerApp *Service)
{
    ObStatus            status;
    GoepServerObexCons *server;

    OS_LockStack();

#if XA_ERROR_CHECK == XA_ENABLED
    if (!Service) {
        status = OB_STATUS_INVALID_PARM;
        goto Error;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(Service);

    server = &GOES(servers)[Service->connId];
    
#if XA_ERROR_CHECK == XA_ENABLED
    if ((Service->type >= GOEP_MAX_PROFILES) || 
        (Service->connId >= GOEP_NUM_OBEX_CONS) ||
        (server->profiles[Service->type] != Service) ||
        (server->currOp.oper != GOEP_OPER_PULL)) {
        status = OB_STATUS_INVALID_PARM;
        goto Error;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert((Service->type < GOEP_MAX_PROFILES) && 
           (Service->connId < GOEP_NUM_OBEX_CONS) &&
           (server->profiles[Service->type] == Service) &&
           (server->currOp.oper == GOEP_OPER_PULL));

    if ((server->currOp.handler != Service) ||
        (server->currOp.event != GOEP_EVENT_PROVIDE_OBJECT)) {
        status = OB_STATUS_FAILED;
        goto Error;
    }

    server->object = 0;

    OBEX_ServerAcceptNoObject(&server->obs);

    status = OB_STATUS_SUCCESS;

Error:
    OS_UnlockStack();
    return status;
}
#endif /* OBEX_BODYLESS_GET == XA_ENABLED */


/*---------------------------------------------------------------------------
 *            GOEP_ServerContinue
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Called to keep data flowing between the client and the server.
 *            This function must be called once for every GOEP_EVENT_CONTINUE
 *            event received. It does not have to be called in the context
 *            of the callback. It can be deferred for flow control reasons.
 *
 * Return:    ObStatus
 */
ObStatus GOEP_ServerContinue(GoepServerApp *Service)
{
    ObStatus            status;
    GoepServerObexCons *server;

    OS_LockStack();

#if XA_ERROR_CHECK == XA_ENABLED
    if (!Service) {
        status = OB_STATUS_INVALID_PARM;
        goto Error;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(Service);

    server = &GOES(servers)[Service->connId];
    
#if XA_ERROR_CHECK == XA_ENABLED
    if ((Service->type >= GOEP_MAX_PROFILES) || 
        (Service->connId >= GOEP_NUM_OBEX_CONS) ||
        (server->profiles[Service->type] != Service)) {
        status = OB_STATUS_INVALID_PARM;
        goto Error;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert((Service->type < GOEP_MAX_PROFILES) && 
           (Service->connId < GOEP_NUM_OBEX_CONS) &&
           (server->profiles[Service->type] == Service));

    if ((server->currOp.handler != Service) || (!server->oustandingResp) ||
        ((server->currOp.event != GOEP_EVENT_CONTINUE) &&
        ((server->currOp.event != GOEP_EVENT_START) || (server->currOp.oper != GOEP_OPER_ABORT)))) {
        status = OB_STATUS_FAILED;
        goto Error;
    }

#if GOEP_ADDITIONAL_HEADERS > 0
    if (ServerBuildHeaders(server) == FALSE) {
        OBEXH_FlushHeaders(&server->obs.handle);
        status = OB_STATUS_FAILED;
        goto Error;
    }
#endif /* GOEP_ADDITIONAL_HEADERS > 0 */

    server->oustandingResp = FALSE;
    OBEX_ServerSendResponse(&server->obs);

    status = OB_STATUS_SUCCESS;

Error:
    OS_UnlockStack();
    return status;
}

#if OBEX_AUTHENTICATION == XA_ENABLED
/*---------------------------------------------------------------------------
 *            GOEP_ServerAuthenticate
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Respond to an authentication challenge received from a client, 
 *            or send a challenge to the client.
 *
 * Return:    ObStatus
 *
 */
ObStatus GOEP_ServerAuthenticate(GoepServerApp     *Service, 
                                 ObexAuthResponse  *Response,
                                 ObexAuthChallenge *Challenge )
{
    ObStatus    status;
    GoepServerObexCons *server;
    BOOL success = FALSE;

    OS_LockStack();

#if XA_ERROR_CHECK == XA_ENABLED
    if (!Service) {
        status = OB_STATUS_INVALID_PARM;
        goto Error;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(Service);

    server = &GOES(servers)[Service->connId];

#if XA_ERROR_CHECK == XA_ENABLED
    if ((Challenge && Response) || (!Challenge && !Response) ||
        (Service->type >= GOEP_MAX_PROFILES) || 
        (Service->connId >= GOEP_NUM_OBEX_CONS) ||
        (server->profiles[Service->type] != Service)) {
        status = OB_STATUS_INVALID_PARM;
        goto Error;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert((Challenge || Response) && !(Challenge && Response) && 
           (Service->type < GOEP_MAX_PROFILES) && 
           (Service->connId < GOEP_NUM_OBEX_CONS) &&
           (server->profiles[Service->type] == Service));

    if (server->currOp.handler != Service) {
        status = OB_STATUS_FAILED;
        goto Error;
    }

    /* Build Challenge if one was provided. Otherwise build a
     * response, as long as we've received a challenge.
     */
    if (Challenge) {
        success = OBEXH_BuildAuthChallenge(&server->obs.handle, Challenge, 
                                            server->nonce);
        /*
         * Since we are challenging the client, the proper thing
         * to do is to respond with unauthorized.
         */
        status = OBEX_ServerAbort(&server->obs, OBRC_UNAUTHORIZED);
        if (status != OB_STATUS_SUCCESS) goto Error;
    } 
    else if (server->flags & GOEF_CHALLENGE) {
        success = OBEXH_BuildAuthResponse(&server->obs.handle, Response, 
                                           server->currOp.challenge.nonce);
        server->flags &= ~GOEF_CHALLENGE;
    }

    if (!success) {
        status = OB_STATUS_FAILED;
    } else {
        status = OB_STATUS_SUCCESS;
    }

Error:
    OS_UnlockStack();
    return status;
}

/*---------------------------------------------------------------------------
 *            GOEP_ServerVerifyAuthResponse
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Verifies the received authentication response.
 *
 * Return:    TRUE if client's response is authenticated.
 *
 */
BOOL GOEP_ServerVerifyAuthResponse(GoepServerApp *Service, U8 *Password, 
                                   U8 PasswordLen)
{
    BOOL                    status;
    ObexAuthResponseVerify  verify;
    GoepServerObexCons     *server;

    OS_LockStack();

#if XA_ERROR_CHECK == XA_ENABLED
    if ((!Service) || (!Password)) {
        status = FALSE;
        goto Error;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(Service && Password);

    server = &GOES(servers)[Service->connId];
  
    if (server->flags & GOEF_RESPONSE) {
        /* Verify the authentication response */
        verify.nonce = server->nonce;
        verify.digest = server->currOp.response.digest;
        verify.password = Password;
        verify.passwordLen = PasswordLen;

        status = OBEXH_VerifyAuthResponse(&verify);
    } else status = FALSE;

Error:
    OS_UnlockStack();
    return status;
}
#endif /* OBEX_AUTHENTICATION == XA_ENABLED */

#if OBEX_TRANSPORT_FLOW_CTRL == XA_ENABLED
/*---------------------------------------------------------------------------
 *            GOEP_ServerGetTpConnInfo
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Retrieves OBEX transport layer connection information.  This 
 *            function can be called when a transport connection is active to 
 *            retrieve connection specific information.   
 *
 * Return:    
 *     TRUE - The tpInfo structure was successfully completed.
 *     FALSE - The transport is not connected (XA_ERROR_CHECK only).
 *
 */
BOOL GOEP_ServerGetTpConnInfo(GoepServerApp *Service, 
                                   ObexTpConnInfo *tpInfo)
                                   
{
    GoepServerObexCons *server;

    OS_LockStack();

#if XA_ERROR_CHECK == XA_ENABLED
    if ((!Service) || (!tpInfo)) {
        OS_UnlockStack();
        return FALSE;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(Service && tpInfo);

    server = &GOES(servers)[Service->connId];
    
    OS_UnlockStack();
    return OBEX_GetTpConnInfo(&server->obs.handle, tpInfo);
}
#endif /* OBEX_TRANSPORT_FLOW_CTRL == XA_ENABLED */

#if GOEP_ADDITIONAL_HEADERS > 0
/*---------------------------------------------------------------------------
 *            GOEP_ServerQueueHeader()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  This function queues a Byte Sequence, UNICODE, 1-byte, or 4-byte 
 *            OBEX header for transmission by the GOEP Server.
 *
 * Return:    True if the OBEX Header was queued successfully.
 */
BOOL GOEP_ServerQueueHeader(GoepServerApp *Server, ObexHeaderType Type,
                            const U8 *Value, U16 Len) {
    U8                  i;
    BOOL                status;
    GoepServerObexCons *server;

    OS_LockStack();

#if XA_ERROR_CHECK == XA_ENABLED
    if (!Server) { 
        status = FALSE;
        goto Error;
    }

    /* Only allow certain headers to be queued for transmission */
    if ((Type == OBEXH_TYPE) || (Type == OBEXH_TARGET) || 
        (Type == OBEXH_WHO) || (Type == OBEXH_CONNID) ||
#if OBEX_SESSION_SUPPORT == XA_ENABLED
        (Type == OBEXH_SESSION_PARAMS) || (Type == OBEXH_SESSION_SEQ_NUM) ||
#endif /* OBEX_SESSION_SUPPORT == XA_ENABLED */
        (Type == OBEXH_AUTH_CHAL) || (Type == OBEXH_AUTH_RESP) || (Type == OBEXH_WAN_UUID)) {
        status = FALSE;
        goto Error;
    }

#if GOEP_DOES_UNICODE_CONVERSIONS == XA_DISABLED
    /* Unicode headers must have an even length if we aren't
     * automatically performing the unicode conversions. 
     */
    if (OBEXH_IsUnicode(Type) && ((Len % 2) != 0)) {
        status = FALSE;
        goto Error;
    }
#endif /* GOEP_DOES_UNICODE_CONVERSIONS == XA_DISABLED */
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(Server);

    server = &GOES(servers)[Server->connId];
 
#if XA_ERROR_CHECK == XA_ENABLED
    /* Non-Body headers cannot exceed the available OBEX packet size */
    if ((Type != OBEXH_BODY) && (Len > OBEXH_GetAvailableTxHeaderLen(&server->obs.handle))) {
        status = FALSE;
        goto Error;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    for (i = 0; i < GOEP_ADDITIONAL_HEADERS; i++) {
        if (server->queuedHeaders[i].type == 0) {
            /* Found an empty header to queue */
            server->queuedHeaders[i].type = Type;
            server->queuedHeaders[i].buffer = Value;
            server->queuedHeaders[i].len = Len;
            status = TRUE;
            goto Error;
        }
    }

    /* No empty headers available */
    status = FALSE;

Error:
    OS_UnlockStack();
    return status;
}
#endif /* GOEP_ADDITIONAL_HEADERS > 0 */


#if OBEX_ALLOW_SERVER_TP_CONNECT == XA_ENABLED
/*---------------------------------------------------------------------------
 *            GOEP_ServerTpConnect
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initiate an OBEX Transport Connection from the GOEP Server.
 *
 * Return:    ObStatus
 *
 */
ObStatus GOEP_ServerTpConnect(GoepServerApp *Server, ObexTpAddr *Target)
{
    ObStatus			status;
    GoepServerObexCons *server;
#if BT_STACK == XA_ENABLED
	U8					i;
	BOOL				targetAddr = FALSE;
#endif /* BT_STACK == XA_ENABLED */

    OS_LockStack();

#if XA_ERROR_CHECK == XA_ENABLED
    if (!Server) {
        status = OB_STATUS_INVALID_PARM;
        goto Error;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(Server);

    server = &GOES(servers)[Server->connId];
    
#if XA_ERROR_CHECK == XA_ENABLED
    if ((Server->type >= GOEP_MAX_PROFILES) ||
        (Server->connId >= GOEP_NUM_OBEX_CONS) || !Target ||
        (server->profiles[Server->type] != Server)) {
        status = OB_STATUS_INVALID_PARM;
        goto Error;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert((Server->type < GOEP_MAX_PROFILES) && 
           (Server->connId < GOEP_NUM_OBEX_CONS) && Target &&
           (server->profiles[Server->type] == Server));

    if (server->flags & GOEF_ACTIVE) {
        /* Already connected */
        status = OB_STATUS_SUCCESS;
        goto Error;
    }

#if BT_STACK == XA_ENABLED
	for (i = 0; i < BD_ADDR_SIZE; i++) {
		/* Check if a Bluetooth address has been provided by the app. If any
		 * byte in the 6-byte Bluetooth address is nonzero, we assume it to 
		 * be a valid Bluetooth address.
		 */
		if (Target->proto.bt.addr.addr[i] != 0x00) {
			targetAddr = TRUE;
			break;
		}
	}
	
    if ((Target->type == OBEX_TP_BLUETOOTH) && (!targetAddr)) {
		/* No address was provided, so we'll use the Device Manager 
		 * to allow the application to select a device.
		 */
		GOES(deviceQuery).callback = GoepSrvrBtEventHandler;
		GOES(deviceQuery).quality.mask = BDQM_DEVICE_STATUS;
		GOES(deviceQuery).quality.status = BDS_IN_RANGE;
		
		GOES(tpAddr).type = Target->type;
		GOES(tpAddr).proto.bt.sdpQuery = Target->proto.bt.sdpQuery;
		GOES(tpAddr).proto.bt.sdpQueryLen = Target->proto.bt.sdpQueryLen;
		GOES(tpAddr).proto.bt.sdpQueryType = Target->proto.bt.sdpQueryType;
		GOES(storedAppPtr) = Server;
		status = DS_SelectDevice(&GOES(deviceQuery));
		if (status != BT_STATUS_PENDING) {
			Report(("GOEP: Select Device failed to start. Error 0x%x\n", status));
		}
		goto Done;
	}
#endif /* BT_STACK == XA_ENABLED */

    status = OBEX_ServerTpConnect(&server->obs, Target);
    
    if (status == OB_STATUS_SUCCESS) {
        /* The transport connection is up */
        server->currOp.oper = GOEP_OPER_NONE;
        server->flags |= (GOEF_ACTIVE|GOEF_SERVER_TP_INITIATED);
    } 
    else if (status == OB_STATUS_PENDING) {
        /* The transport connection is coming up */
        server->currOp.handler = Server;
        server->flags |= GOEF_SERVER_TP_INITIATED;
    }

#if BT_STACK == XA_ENABLED
Done:
#endif /* BT_STACK == XA_ENABLED */

Error:
    OS_UnlockStack();
    return status;
}
#endif /* OBEX_ALLOW_SERVER_TP_CONNECT == XA_ENABLED */

#if OBEX_ALLOW_SERVER_TP_DISCONNECT == XA_ENABLED
/*---------------------------------------------------------------------------
 *            GOEP_ServerTpDisconnect
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initiate destruction of an OBEX Transport Connection from the
 *            GOEP Server.
 *
 * Return:    ObStatus
 *
 */
ObStatus GOEP_ServerTpDisconnect(GoepServerApp *Server)
{
    ObStatus    status;
    GoepServerObexCons *server;

    OS_LockStack();

#if XA_ERROR_CHECK == XA_ENABLED
    if (!Server) {
        status = OB_STATUS_INVALID_PARM;
        goto Error;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(Server);

    server = &GOES(servers)[Server->connId];
    
#if XA_ERROR_CHECK == XA_ENABLED
    if ((Server->type >= GOEP_MAX_PROFILES) ||
        (Server->connId >= GOEP_NUM_OBEX_CONS) || 
        (server->profiles[Server->type] != Server)) {
        status = OB_STATUS_INVALID_PARM;
        goto Error;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert((Server->type < GOEP_MAX_PROFILES) && 
           (Server->connId < GOEP_NUM_OBEX_CONS) &&
           (server->profiles[Server->type] == Server));


    if ((server->flags & GOEF_ACTIVE) == 0) {
        /* Already disconnected */
        status = OB_STATUS_NO_CONNECT;
        goto Error;
    }

    /* It's time to disconnect the transport connection */
    status = OBEX_ServerTpDisconnect(&server->obs, TRUE);

    if (status == OB_STATUS_SUCCESS) {
        /* The transport connection is down */
        server->currOp.oper = GOEP_OPER_NONE;
        server->flags &= ~GOEF_ACTIVE;
        server->flags &= ~GOEF_SERVER_TP_INITIATED;
    } 
    else if (status == OB_STATUS_PENDING) {
        /* The transport connection is going down */
        server->currOp.handler = Server;
    }

Error:
    OS_UnlockStack();
    return status;
}
#endif /* OBEX_ALLOW_SERVER_TP_DISCONNECT == XA_ENABLED */

#endif /* OBEX_ROLE_SERVER == XA_ENABLED */

#if OBEX_ROLE_CLIENT == XA_ENABLED
/*---------------------------------------------------------------------------
 *            GOEP_RegisterClient()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initialize the OBEX Client.
 *
 * Return:    ObStatus
 *
 */
ObStatus GOEP_RegisterClient(GoepClientApp *Client, 
                             const ObStoreFuncTable *obStoreFuncs)
{
    U8        id;
    ObStatus    status = OB_STATUS_SUCCESS;

    OS_LockStack();

#if XA_ERROR_CHECK == XA_ENABLED
    if (GOEC(initialized) != TRUE) {
        /* GOEP is not initialized */
        status = OB_STATUS_FAILED;
        goto Error;
    }

    if ((!Client) || (!obStoreFuncs) || (Client->type >= GOEP_MAX_PROFILES) || 
        (Client->connId >= GOEP_NUM_OBEX_CONS) || (Client->callback == 0) || 
        ((Client->connFlags != GOEP_NEW_CONN) && 
        (Client->connFlags != GOEP_MUX_CONN))) {
        status = OB_STATUS_INVALID_PARM;
        goto Error;
    }

    if (Client->connFlags == GOEP_MUX_CONN) {
        /* This is an existing connection.  Make sure we aren't trying 
         * to add the same profile twice.
         */
        if (GOEC(clients)[Client->connId].profiles[Client->type]) {
            status = OB_STATUS_INVALID_PARM;
            goto Error;
        }
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(Client && obStoreFuncs && (Client->type < GOEP_MAX_PROFILES) &&
           (Client->connId < GOEP_NUM_OBEX_CONS) && (Client->callback != 0) &&
           ((Client->connFlags == GOEP_NEW_CONN) || 
           (Client->connFlags == GOEP_MUX_CONN)));

    /* Create an OBEX connection if available, and if the Client requests 
     * a new connection 
     */
    if ((GOEC(connCount) < GOEP_NUM_OBEX_CONS) && 
        (Client->connFlags == GOEP_NEW_CONN)) {
        /* Get a free client conn Id */
        if (!GetFreeConnId(&id, GOEP_CLIENT)) {
            status = OB_STATUS_NO_RESOURCES;
            goto Error;
        }

        OBEX_InitAppHandle(&GOEC(clients)[id].obc.handle, 
                GOEC(clients)[id].headerBlock, (U16)GOEP_CLIENT_HB_SIZE, obStoreFuncs);

        status = OBEX_ClientInit(&GOEC(clients)[id].obc, GoepClntCallback, 
                        OBEX_EnumTransports());

        if (status == OB_STATUS_SUCCESS) {
            Client->connId = id;
            /* Increment the current number of Obex connections */
            GOEC(connCount)++;
        }
    }
    else if ((GOEC(connCount) >= GOEP_NUM_OBEX_CONS) && 
             (Client->connFlags == GOEP_NEW_CONN)) {
        /* Not enough Obex connections allocated in GOEP to allocate 
         * a new Connection Id 
         */
        Report(("Current Obex connections = %i, greater than or equal \
                to GOEP_NUM_OBEX_CONS = %i\n", GOEC(connCount), 
                GOEP_NUM_OBEX_CONS));
        status = OB_STATUS_NO_RESOURCES;
        goto Error;
    }

    if (status == OB_STATUS_SUCCESS) {
        /* Set this client to "in use" */
        GOEC(clients)[Client->connId].connCount++;
        GOEC(clients)[Client->connId].profiles[Client->type] = Client;
        Client->connState = CS_DISCONNECTED;
        Client->obexConnId = OBEX_INVALID_CONNID;

        if (Client->appParent == 0)
            Client->appParent = Client->callback;
    }

Error:
    OS_UnlockStack();
    return status;
}

#if BT_SECURITY == XA_ENABLED
/*---------------------------------------------------------------------------
 *            GOEP_RegisterClientSecurityRecord()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Registers a security record for the GOEP client.
 *
 * Return:    (See goep.h)
 */
BtStatus GOEP_RegisterClientSecurityRecord(GoepClientApp *Client, BtSecurityLevel Level)
{
    BtStatus status = BT_STATUS_FAILED;

    OS_LockStack();

    /* Register security for the GOEP service */
    Client->secRecord.id =  SEC_GOEP_ID;
    Client->secRecord.channel = (U32)Client;
    Client->secRecord.level = Level;
    status = SEC_Register(&Client->secRecord);

    OS_UnlockStack();
    return status;
}

/*---------------------------------------------------------------------------
 *            GOEP_UnregisterClientSecurityRecord()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Unregisters a security handler for the GOEP client.
 *
 * Return:    (See goep.h)
 */
BtStatus GOEP_UnregisterClientSecurityRecord(GoepClientApp *Client)
{
    BtStatus status;

    OS_LockStack();

    /* Unregister security for the GOEP Client */
    status = SEC_Unregister(&Client->secRecord);

    OS_UnlockStack();

    return status;
}
#endif /* BT_SECURITY == XA_ENABLED */

/*---------------------------------------------------------------------------
 *            GOEP_GetObexClient()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Retrieves the OBEX Client pertaining to a specific GOEP Client.  
 *            This function is valid after GOEP_Init and GOEP_RegisterClient 
 *            have been called.
 *
 * Return:    ObexClientApp
 *
 */
ObexClientApp* GOEP_GetObexClient(GoepClientApp *Client)
{
    OS_LockStack();

#if XA_ERROR_CHECK == XA_ENABLED
    if ((!Client) || (Client->connId >= GOEP_NUM_OBEX_CONS)) {
        OS_UnlockStack();
        return 0;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(Client && (Client->connId < GOEP_NUM_OBEX_CONS));
    
    OS_UnlockStack();
    return &(GOEC(clients)[Client->connId].obc);
}

#if OBEX_DEINIT_FUNCS == XA_ENABLED
/*---------------------------------------------------------------------------
 *            GOEP_DeregisterClient()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Deinitialize the OBEX Client.
 *
 * Return:    ObStatus
 *
 */
ObStatus GOEP_DeregisterClient(GoepClientApp *Client)
{   
    ObStatus    status;
    GoepClientObexCons *client;

    OS_LockStack();

#if XA_ERROR_CHECK == XA_ENABLED
    if (!Client) {
        status = OB_STATUS_INVALID_PARM;
        goto Error;
    }

    if (GOEC(initialized) != TRUE) {
        /* GOEP is not initialized, so there is nothing to deinit */
        status = OB_STATUS_SUCCESS;
        goto Error;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(Client);

    client = &GOEC(clients)[Client->connId];
    
#if XA_ERROR_CHECK == XA_ENABLED
    if ((Client->type >= GOEP_MAX_PROFILES) || 
        (Client->connId >= GOEP_NUM_OBEX_CONS) ||
        (client->profiles[Client->type] != Client)) {
        status = OB_STATUS_INVALID_PARM;
        goto Error;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */
    
    Assert((Client->type < GOEP_MAX_PROFILES) &&
           (Client->connId < GOEP_NUM_OBEX_CONS) &&
           (client->profiles[Client->type] == Client));

    /* See if it is connected */
    if (Client->connState != CS_DISCONNECTED) {
        status = OB_STATUS_BUSY;
        goto Error;
    }
    client->profiles[Client->type] = 0;

    Assert(GOEC(connCount) > 0);

    if (--(client->connCount) > 0) {
        status = OB_STATUS_SUCCESS;
        goto Error;
    }

    --GOEC(connCount);
    status = OBEX_ClientDeinit(&client->obc);

Error:
    OS_UnlockStack();
    return status;
}
#endif /* OBEX_DEINIT_FUNCS == XA_ENABLED */

/*---------------------------------------------------------------------------
 *            GOEP_Connect
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Issue an OBEX Connect Request. If the Connect parameter is not
 *            used, set it to zero.
 *
 * Return:    ObStatus
 *
 */
ObStatus GOEP_Connect(GoepClientApp *Client, GoepConnectReq *Connect)
{
#if GOEP_ADDITIONAL_HEADERS > 0
    U16                 more;
#endif /* GOEP_ADDITIONAL_HEADERS > 0 */
    ObStatus            status;
    GoepClientObexCons *client;
#if OBEX_ALLOW_SERVER_TP_CONNECT == XA_ENABLED
    BOOL                success;
    ObexTpConnInfo      tpInfo;
#endif /* OBEX_ALLOW_SERVER_TP_CONNECT == XA_ENABLED */

    OS_LockStack();

#if XA_ERROR_CHECK == XA_ENABLED
    if (!Client) {
        status = OB_STATUS_INVALID_PARM;
        goto Error;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(Client);

    client = &GOEC(clients)[Client->connId];
    
#if XA_ERROR_CHECK == XA_ENABLED
    if ((Client->type >= GOEP_MAX_PROFILES) || 
        (Client->connId >= GOEP_NUM_OBEX_CONS) || 
        (client->profiles[Client->type] != Client)) {
        status = OB_STATUS_INVALID_PARM;
        goto Error;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert((Client->type < GOEP_MAX_PROFILES) && 
           (Client->connId < GOEP_NUM_OBEX_CONS) &&
           (client->profiles[Client->type] == Client));

    if (client->currOp.handler) {
        status = OB_STATUS_BUSY;
        goto Error;
    }

    if (Connect) {
        /* Build OBEX Connect headers */
        if (Connect->target)
            OBEXH_BuildByteSeq(&client->obc.handle, OBEXH_TARGET, 
                Connect->target, Connect->targetLen);
        if (Connect->who)
            OBEXH_BuildByteSeq(&client->obc.handle, OBEXH_WHO, 
                Connect->who, Connect->whoLen);
#if OBEX_AUTHENTICATION == XA_ENABLED
        if (Connect->response) {
            if ((client->flags & GOEF_CHALLENGE) == 0) {
                status = OB_STATUS_INVALID_PARM;
                goto Error;
            }
            OBEXH_BuildAuthResponse(&client->obc.handle, Connect->response, 
                client->currOp.challenge.nonce);
        }

        if (Connect->challenge)
            OBEXH_BuildAuthChallenge(&client->obc.handle, Connect->challenge, 
                client->nonce);
#endif /* OBEX_AUTHENTICATION == XA_ENABLED */
    }

    client->currOp.oper = GOEP_OPER_CONNECT;

#if GOEP_ADDITIONAL_HEADERS > 0
    if (ClientBuildHeaders(client, &more) == FALSE) {
        Assert(more == 0);
        OBEXH_FlushHeaders(&client->obc.handle);
        status = OB_STATUS_FAILED;
        goto Error;
    }
#endif /* GOEP_ADDITIONAL_HEADERS > 0 */

#if BT_SECURITY == XA_ENABLED
    /* Check security for this GoepClientApp */
    Client->secToken.id = SEC_GOEP_ID;
    if (client->flags & GOEF_CLIENT_TP_INITIATED) {
        /* Client initiated transport connection */
        Client->secToken.remDev = client->obc.trans.ObexClientBtTrans.remoteDevice;
    } 
#if OBEX_ALLOW_SERVER_TP_CONNECT == XA_ENABLED
    else {
        /* Must be server initiated, check the transport connection information */
        success = OBEX_GetTpConnInfo(&client->obc.handle, &tpInfo);
        Assert(success);
        Client->secToken.remDev = tpInfo.remDev;
    }
#endif /* OBEX_ALLOW_SERVER_TP_CONNECT == XA_ENABLED */
    Client->secToken.channel = (U32)Client;
    Client->secToken.incoming = FALSE;
    status = SEC_AccessRequest(&(Client->secToken));
    if (status == BT_STATUS_SUCCESS) {
        status = OBEX_ClientConnect(&client->obc);
    } else if (status != BT_STATUS_PENDING) {
        status = OB_STATUS_RESTRICTED;
    }
#else /* BT_SECURITY == XA_ENABLED */
    status = OBEX_ClientConnect(&client->obc);
#endif /* BT_SECURITY == XA_ENABLED */

    if (status == OB_STATUS_PENDING) {
        client->currOp.handler = Client;
        /* Clear authentication flags */
#if OBEX_AUTHENTICATION == XA_ENABLED
        client->flags &= ~(GOEF_CHALLENGE|GOEF_RESPONSE);
#endif /* OBEX_AUTHENTICATION == XA_ENABLED */
    } else {
        client->currOp.oper = GOEP_OPER_NONE;
    }

Error:
    OS_UnlockStack();
    return status;
}

/*---------------------------------------------------------------------------
 *            GOEP_Disconnect
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Issue an OBEX Disconnect Request.
 *
 * Return:    ObStatus
 *
 */
ObStatus GOEP_Disconnect(GoepClientApp *Client)
{
#if GOEP_ADDITIONAL_HEADERS > 0
    U16         more;
#endif /* GOEP_ADDITIONAL_HEADERS > 0 */
    ObStatus    status;
    GoepClientObexCons *client;

    OS_LockStack();

#if XA_ERROR_CHECK == XA_ENABLED
    if (!Client) {
        status = OB_STATUS_INVALID_PARM;
        goto Error;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(Client);

    client = &GOEC(clients)[Client->connId];
    
#if XA_ERROR_CHECK == XA_ENABLED
    if ((Client->type >= GOEP_MAX_PROFILES) ||
        (Client->connId >= GOEP_NUM_OBEX_CONS) || 
        (client->profiles[Client->type] != Client)) {
        status = OB_STATUS_INVALID_PARM;
        goto Error;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert((Client->type < GOEP_MAX_PROFILES) && 
           (Client->connId < GOEP_NUM_OBEX_CONS) && 
           (client->profiles[Client->type] == Client));

    if (client->currOp.handler) {
        status = OB_STATUS_BUSY;
        goto Error;
    }

    if (Client->obexConnId != OBEX_INVALID_CONNID)
        OBEXH_Build4Byte(&client->obc.handle, OBEXH_CONNID, 
                Client->obexConnId);

    client->currOp.oper = GOEP_OPER_DISCONNECT;

#if GOEP_ADDITIONAL_HEADERS > 0
    if (ClientBuildHeaders(client, &more) == FALSE) {
        Assert(more == 0);
        OBEXH_FlushHeaders(&client->obc.handle);
        status = OB_STATUS_FAILED;
        goto Error;
    }
#endif /* GOEP_ADDITIONAL_HEADERS > 0 */

    status = OBEX_ClientDisconnect(&client->obc);
    if (status == OB_STATUS_PENDING) {
        client->currOp.handler = Client;
        /* Clear authentication flags */
#if OBEX_AUTHENTICATION == XA_ENABLED
        client->flags &= ~(GOEF_CHALLENGE|GOEF_RESPONSE);
#endif /* OBEX_AUTHENTICATION == XA_ENABLED */
        Client->obexConnId = OBEX_INVALID_CONNID;
    } else {
        client->currOp.oper = GOEP_OPER_NONE;
    }

Error:
    OS_UnlockStack();
    return status;
}

/*---------------------------------------------------------------------------
 *            GOEP_Push
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Issue an OBEX Put. To delete an object, set the Object Store
 *            Handle in the GoepObjectReq to zero.
 *
 * Return:    ObStatus
 *
 */
ObStatus GOEP_Push(GoepClientApp *Client, GoepObjectReq *Object)
{
#if GOEP_ADDITIONAL_HEADERS > 0
    U16         more;
#endif /* GOEP_ADDITIONAL_HEADERS > 0 */
    U16         len;
    ObStatus    status;
#if GOEP_DOES_UNICODE_CONVERSIONS == XA_ENABLED
    U16         uniName[GOEP_MAX_UNICODE_LEN];
#endif /* GOEP_DOES_UNICODE_CONVERSIONS == XA_ENABLED */
    GoepClientObexCons *client;

    OS_LockStack();

#if XA_ERROR_CHECK == XA_ENABLED
    if (!Client) {
        status = OB_STATUS_INVALID_PARM;
        goto Error;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(Client);

    client = &GOEC(clients)[Client->connId];
    
#if XA_ERROR_CHECK == XA_ENABLED
    if ((Client->type >= GOEP_MAX_PROFILES) ||
        (Client->connId >= GOEP_NUM_OBEX_CONS)|| (!Object) ||
        (client->profiles[Client->type] != Client)) {
        status = OB_STATUS_INVALID_PARM;
        goto Error;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert((Client->type < GOEP_MAX_PROFILES) && 
           (Client->connId < GOEP_NUM_OBEX_CONS) && Object &&
           (client->profiles[Client->type] == Client));

    if (client->currOp.handler) {
        status = OB_STATUS_BUSY;
        goto Error;
    }

#if OBEX_PUT_DELETE == XA_DISABLED
    if (Object->object == 0) {
        status = OB_STATUS_INVALID_HANDLE;
        goto Error;
    }
#endif /* OBEX_PUT_DELETE == XA_DISABLED */

    if (Client->obexConnId != OBEX_INVALID_CONNID)
        OBEXH_Build4Byte(&client->obc.handle, OBEXH_CONNID, 
            Client->obexConnId);

    if (Object->name) {
#if GOEP_DOES_UNICODE_CONVERSIONS == XA_ENABLED
        /* Make sure GOEP_MAX_UNICODE_LEN is large enough for the name header
         * in Unicode form, to prevent memory corruption problems
         */
        Assert(GOEP_MAX_UNICODE_LEN >= (Strlen(Object->name) * 2 + 2)); 
        len = AsciiToUnicode(uniName, Object->name) + 2;
        if (len > 2)
            OBEXH_BuildUnicode(&client->obc.handle, OBEXH_NAME, (U8 *)uniName, 
                len);
        else OBEXH_BuildUnicode(&client->obc.handle, OBEXH_NAME, 0, 0);
#else /* GOEP_DOES_UNICODE_CONVERSIONS == XA_ENABLED */
        len = Unilen(Object->name) + 2;
        if (len > 2)
            OBEXH_BuildUnicode(&client->obc.handle, OBEXH_NAME, 
                (U8 *)Object->name, len);
        else OBEXH_BuildUnicode(&client->obc.handle, OBEXH_NAME, 0, 0);
#endif /* GOEP_DOES_UNICODE_CONVERSIONS == XA_ENABLED */
    }

    if (Object->type) {
        len = Strlen(Object->type) + 1;
        if (len > 1)
            OBEXH_BuildByteSeq(&client->obc.handle, OBEXH_TYPE, Object->type, 
                len);
    }

    if (Object->object && Object->objectLen) 
        OBEXH_Build4Byte(&client->obc.handle, OBEXH_LENGTH, Object->objectLen);

    client->object = Object->object;
    client->currOp.oper = GOEP_OPER_PUSH;

#if GOEP_ADDITIONAL_HEADERS > 0
    if (ClientBuildHeaders(client, &more) == FALSE) {
        Assert(more == 0);
        OBEXH_FlushHeaders(&client->obc.handle);
        status = OB_STATUS_FAILED;
        goto Error;
    }
#endif /* GOEP_ADDITIONAL_HEADERS > 0 */

    /* Put or Put Delete based on the value of Object->object */
    status = OBEX_ClientPut(&client->obc, client->object); 

    if (status == OB_STATUS_PENDING) {

#if OBEX_PUT_DELETE == XA_ENABLED
        if (Object->object == 0)
            client->currOp.oper = GOEP_OPER_DELETE;
#endif /* OBEX_PUT_DELETE == XA_ENABLED */
        client->currOp.handler = Client;
        /* Clear authentication flags */
#if OBEX_AUTHENTICATION == XA_ENABLED
        client->flags &= ~(GOEF_CHALLENGE|GOEF_RESPONSE);
#endif /* OBEX_AUTHENTICATION == XA_ENABLED */
    } else {
        client->currOp.oper = GOEP_OPER_NONE;
    }

Error:
    OS_UnlockStack();
    return status;
}

/*---------------------------------------------------------------------------
 *            GOEP_Pull
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Issue an OBEX Get.
 *
 * Return:    ObStatus
 *
 */
ObStatus GOEP_Pull(GoepClientApp *Client, GoepObjectReq *Object, BOOL More)
{
    ObStatus    status;
#if GOEP_DOES_UNICODE_CONVERSIONS == XA_ENABLED
    U16         uniName[GOEP_MAX_UNICODE_LEN];
#endif /* GOEP_DOES_UNICODE_CONVERSIONS == XA_ENABLED */
    GoepClientObexCons *client;
    U16         len, more = 0;

    OS_LockStack();

#if XA_ERROR_CHECK == XA_ENABLED
    if (!Client) {
        status = OB_STATUS_INVALID_PARM;
        goto Error;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(Client);

    client = &GOEC(clients)[Client->connId];
    
#if XA_ERROR_CHECK == XA_ENABLED
    if ((Client->type >= GOEP_MAX_PROFILES) ||
        (Client->connId >= GOEP_NUM_OBEX_CONS)|| (!Object) ||
        (client->profiles[Client->type] != Client)) {
        status = OB_STATUS_INVALID_PARM;
        goto Error;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert((Client->type < GOEP_MAX_PROFILES) && 
           (Client->connId < GOEP_NUM_OBEX_CONS) && Object &&
           (client->profiles[Client->type] == Client));

    if (client->currOp.handler) {
        status = OB_STATUS_BUSY;
        goto Error;
    }

    if (Object->object == 0) {
        status = OB_STATUS_INVALID_HANDLE;
        goto Error;
    }

    if (Client->obexConnId != OBEX_INVALID_CONNID)
        OBEXH_Build4Byte(&client->obc.handle, OBEXH_CONNID, 
            Client->obexConnId);

    if (Object->name) {
#if GOEP_DOES_UNICODE_CONVERSIONS == XA_ENABLED
        /* Make sure GOEP_MAX_UNICODE_LEN is large enough for the name header
         * in Unicode form, to prevent memory corruption problems
         */
        Assert(GOEP_MAX_UNICODE_LEN >= (Strlen(Object->name) * 2 + 2)); 
        len = AsciiToUnicode(uniName, Object->name) + 2;
        if (len > 2)
            OBEXH_BuildUnicode(&client->obc.handle, OBEXH_NAME, (U8 *)uniName, len);
        else OBEXH_BuildUnicode(&client->obc.handle, OBEXH_NAME, 0, 0);
#else /* GOEP_DOES_UNICODE_CONVERSIONS == XA_ENABLED */
        len = Unilen(Object->name) + 2;
        if (len > 2)
            OBEXH_BuildUnicode(&client->obc.handle, 
                    OBEXH_NAME, (U8 *)Object->name, len);
        else OBEXH_BuildUnicode(&client->obc.handle, OBEXH_NAME, 0, 0);
#endif /* GOEP_DOES_UNICODE_CONVERSIONS == XA_ENABLED */
    }

    if (Object->type) {
        len = Strlen(Object->type) + 1;
        if (len > 1)
            OBEXH_BuildByteSeq(&client->obc.handle, OBEXH_TYPE, 
                Object->type, len);
    }

    client->currOp.oper = GOEP_OPER_PULL;

#if GOEP_ADDITIONAL_HEADERS > 0
    if (ClientBuildHeaders(client, &more) == FALSE) {
        OBEXH_FlushHeaders(&client->obc.handle);
        status = OB_STATUS_FAILED;
        goto Error;
    }
#endif /* GOEP_ADDITIONAL_HEADERS > 0 */

    client->object = Object->object;

    status = OBEX_ClientGet(&client->obc, client->object, (((more > 0) || More)? TRUE : FALSE));
    if (status == OB_STATUS_PENDING) {
        client->currOp.handler = Client;
        /* Clear authentication flags */
#if OBEX_AUTHENTICATION == XA_ENABLED
        client->flags &= ~(GOEF_CHALLENGE|GOEF_RESPONSE);
#endif /* OBEX_AUTHENTICATION == XA_ENABLED */
    } else {
        client->currOp.oper = GOEP_OPER_NONE;
    }

Error:
    OS_UnlockStack();
    return status;
}

/*---------------------------------------------------------------------------
 *            GOEP_SetFolder
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Issue an OBEX SetPath command.
 *
 * Return:    ObStatus
 *
 */
ObStatus GOEP_SetFolder(GoepClientApp *Client, GoepSetFolderReq *Folder)
{
#if GOEP_ADDITIONAL_HEADERS > 0
    U16         more;
#endif /* GOEP_ADDITIONAL_HEADERS > 0 */
    U16         len;
    ObStatus    status;
#if GOEP_DOES_UNICODE_CONVERSIONS == XA_ENABLED
    U16         uniName[GOEP_MAX_UNICODE_LEN];
#endif /* GOEP_DOES_UNICODE_CONVERSIONS == XA_ENABLED */
    GoepClientObexCons *client;

    OS_LockStack();

#if XA_ERROR_CHECK == XA_ENABLED
    if (!Client) {
        status = OB_STATUS_INVALID_PARM;
        goto Error;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(Client);

    client = &GOEC(clients)[Client->connId];
    
#if XA_ERROR_CHECK == XA_ENABLED
    if ((Client->type >= GOEP_MAX_PROFILES) ||
        (Client->connId >= GOEP_NUM_OBEX_CONS) || (!Folder) ||
        (client->profiles[Client->type] != Client)) {
        status = OB_STATUS_INVALID_PARM;
        goto Error;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert((Client->type < GOEP_MAX_PROFILES) &&
           (Client->connId < GOEP_NUM_OBEX_CONS) && Folder &&
           (client->profiles[Client->type] == Client));

    if (client->currOp.handler) {
        status = OB_STATUS_BUSY;
        goto Error;
    }

    if (Client->obexConnId != OBEX_INVALID_CONNID)
        OBEXH_Build4Byte(&client->obc.handle, 
                OBEXH_CONNID, Client->obexConnId);

    if (Folder->name) {
#if GOEP_DOES_UNICODE_CONVERSIONS == XA_ENABLED
        /* Make sure GOEP_MAX_UNICODE_LEN is large enough for the name header
         * in Unicode form, to prevent memory corruption problems
         */
        Assert(GOEP_MAX_UNICODE_LEN >= (Strlen(Folder->name) * 2 + 2)); 
        len = AsciiToUnicode(uniName, Folder->name) + 2;
        if (len > 2)
            OBEXH_BuildUnicode(&client->obc.handle, OBEXH_NAME, (U8 *)uniName, len);
        else OBEXH_BuildUnicode(&client->obc.handle, OBEXH_NAME, 0, 0);
#else /* GOEP_DOES_UNICODE_CONVERSIONS == XA_ENABLED */
        len = Unilen(Folder->name) + 2;
        if (len > 2)
            OBEXH_BuildUnicode(&client->obc.handle, OBEXH_NAME, 
                (U8 *)Folder->name, len);
        else OBEXH_BuildUnicode(&client->obc.handle, OBEXH_NAME, 
                0, 0);
#endif /* GOEP_DOES_UNICODE_CONVERSIONS == XA_ENABLED */
    } else if (Folder->reset) {
        /* Path reset requires an empty name header to be formed */
        OBEXH_BuildUnicode(&client->obc.handle, OBEXH_NAME, 0, 0);
    }

    client->currOp.oper = GOEP_OPER_SETFOLDER;

#if GOEP_ADDITIONAL_HEADERS > 0
    if (ClientBuildHeaders(client, &more) == FALSE) {
        Assert(more == 0);
        OBEXH_FlushHeaders(&client->obc.handle);
        status = OB_STATUS_FAILED;
        goto Error;
    }
#endif /* GOEP_ADDITIONAL_HEADERS > 0 */

    status = OBEX_ClientSetPath(&client->obc, Folder->flags);
    if (status == OB_STATUS_PENDING) {
        client->currOp.handler = Client;
        /* Clear authentication flags */
#if OBEX_AUTHENTICATION == XA_ENABLED
        client->flags &= ~(GOEF_CHALLENGE|GOEF_RESPONSE);
#endif /* OBEX_AUTHENTICATION == XA_ENABLED */
    } else {
        client->currOp.oper = GOEP_OPER_NONE;    
    }

Error:
    OS_UnlockStack();
    return status;
}

/*---------------------------------------------------------------------------
 *            GOEP_ClientAbort
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Abort the current operation.
 *
 * Return:    ObStatus
 *
 */
ObStatus GOEP_ClientAbort(GoepClientApp *Client)
{
    GoepClientObexCons *client;
    ObStatus status = OB_STATUS_FAILED;

    OS_LockStack();

#if XA_ERROR_CHECK == XA_ENABLED
    if (!Client) {
        status = OB_STATUS_INVALID_PARM;
        goto Error;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(Client);

    client = &GOEC(clients)[Client->connId];
    
#if XA_ERROR_CHECK == XA_ENABLED
    if ((Client->type >= GOEP_MAX_PROFILES) ||
        (Client->connId >= GOEP_NUM_OBEX_CONS) ||
        (client->profiles[Client->type] != Client)) {
        status = OB_STATUS_INVALID_PARM;
        goto Error;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert((Client->type < GOEP_MAX_PROFILES) &&
           (Client->connId < GOEP_NUM_OBEX_CONS) &&
           (client->profiles[Client->type] == Client));

    if (Client == client->currOp.handler)
        status = OBEX_ClientAbort(&client->obc);

Error:
    OS_UnlockStack();
    return status;
}

#if OBEX_AUTHENTICATION == XA_ENABLED
/*---------------------------------------------------------------------------
 *            GOEP_ClientVerifyAuthResponse
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Verifies the received authentication response.
 *
 * Return:    TRUE if server's response is authenticated.
 *
 */
BOOL GOEP_ClientVerifyAuthResponse(GoepClientApp *Client, 
                                   U8 *Password, 
                                   U8 PasswordLen)
{
    BOOL                    status;
    ObexAuthResponseVerify  verify;
    GoepClientObexCons     *client;

    OS_LockStack();

#if XA_ERROR_CHECK == XA_ENABLED
    if ((!Client) || (!Password)) {
        status = FALSE;
        goto Error;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(Client && Password);

    client = &GOEC(clients)[Client->connId];
    
    if (client->flags & GOEF_RESPONSE) {
        /* Verify the authenticaiton response */
        verify.nonce = client->nonce;
        verify.digest = client->currOp.response.digest;
        verify.password = Password;
        verify.passwordLen = PasswordLen;

        status = OBEXH_VerifyAuthResponse(&verify);
        goto Error;
    }

    status = FALSE;

Error:
    OS_UnlockStack();
    return status;
}
#endif /* OBEX_AUTHENTICATION == XA_ENABLED */

#if OBEX_TRANSPORT_FLOW_CTRL == XA_ENABLED
/*---------------------------------------------------------------------------
 *            GOEP_ClientGetTpConnInfo
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Retrieves OBEX transport layer connection information.  This 
 *            function can be called when a transport connection is active to 
 *            retrieve connection specific information.   
 *
 * Return:    
 *     TRUE - The tpInfo structure was successfully completed.
 *     FALSE - The transport is not connected (XA_ERROR_CHECK only).
 *
 */
BOOL GOEP_ClientGetTpConnInfo(GoepClientApp *Client, 
                                   ObexTpConnInfo *tpInfo)
                                   
{
    GoepClientObexCons *client;

    OS_LockStack();

#if XA_ERROR_CHECK == XA_ENABLED
    if ((!Client) || (!tpInfo)) {
        OS_UnlockStack();
        return FALSE;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(Client && tpInfo);

    client = &GOEC(clients)[Client->connId];
        
    OS_UnlockStack();
    return OBEX_GetTpConnInfo(&client->obc.handle, tpInfo);
}
#endif /* OBEX_TRANSPORT_FLOW_CTRL == XA_ENABLED */

/*---------------------------------------------------------------------------
 *            GOEP_TpConnect
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initiate an OBEX Transport Connection.
 *
 * Return:    ObStatus
 *
 */
ObStatus GOEP_TpConnect(GoepClientApp *Client, ObexTpAddr *Target)
{
    ObStatus			status;
    GoepClientObexCons *client;
#if BT_STACK == XA_ENABLED
	U8					i;
	BOOL				targetAddr = FALSE;
#endif /* BT_STACK == XA_ENABLED */

    OS_LockStack();

#if XA_ERROR_CHECK == XA_ENABLED
    if (!Client) {
        status = OB_STATUS_INVALID_PARM;
        goto Error;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(Client);

    client = &GOEC(clients)[Client->connId];
    
#if XA_ERROR_CHECK == XA_ENABLED
    if ((Client->type >= GOEP_MAX_PROFILES) ||
        (Client->connId >= GOEP_NUM_OBEX_CONS) || !Target ||
        (client->profiles[Client->type] != Client)) {
        status = OB_STATUS_INVALID_PARM;
        goto Error;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert((Client->type < GOEP_MAX_PROFILES) && 
           (Client->connId < GOEP_NUM_OBEX_CONS) && Target &&
           (client->profiles[Client->type] == Client));

    if (client->currOp.handler) {
        status = OB_STATUS_BUSY;
        goto Error;
    }

    if (client->flags & GOEF_ACTIVE) {
        Client->connState = CS_CONNECTED;
        status = OB_STATUS_SUCCESS;
        goto Error;
    }

#if BT_STACK == XA_ENABLED
	for (i = 0; i < BD_ADDR_SIZE; i++) {
		/* Check if a Bluetooth address has been provided by the app. If any
		 * byte in the 6-byte Bluetooth address is nonzero, we assume it to 
		 * be a valid Bluetooth address.
		 */
		if (Target->proto.bt.addr.addr[i] != 0x00) {
			targetAddr = TRUE;
			break;
		}
	}

	if ((Target->type == OBEX_TP_BLUETOOTH) && (!targetAddr) && 
		(Client->connState != CS_CONNECTING)) {
		/* No address was provided, so we'll use the Device Manager 
		 * to allow the application to select a device.
		 */
		GOEC(deviceQuery).callback = GoepClntBtEventHandler;
		GOEC(deviceQuery).quality.mask = BDQM_DEVICE_STATUS;
		GOEC(deviceQuery).quality.status = BDS_IN_RANGE;
		
		GOEC(tpAddr).type = Target->type;
		GOEC(tpAddr).proto.bt.sdpQuery = Target->proto.bt.sdpQuery;
		GOEC(tpAddr).proto.bt.sdpQueryLen = Target->proto.bt.sdpQueryLen;
		GOEC(tpAddr).proto.bt.sdpQueryType = Target->proto.bt.sdpQueryType;
		GOEC(storedAppPtr) = Client;
		status = DS_SelectDevice(&GOEC(deviceQuery));
		if (status != BT_STATUS_PENDING) {
			Report(("GOEP: Select Device failed to start. Error 0x%x\n", status));
		} else {
	        Client->connState = CS_CONNECTING;
		}
		goto Done;
	}
#endif /* BT_STACK == XA_ENABLED */

    status = OBEX_ClientTpConnect(&client->obc, Target);
    
    if (status == OB_STATUS_SUCCESS) {
        /* The transport connection is up */
        client->currOp.oper = GOEP_OPER_NONE;
        Client->connState = CS_CONNECTED;
        client->flags |= (GOEF_ACTIVE|GOEF_CLIENT_TP_INITIATED);
    } 
    else if (status == OB_STATUS_PENDING) {
        /* The transport connection is coming up */
        Client->connState = CS_CONNECTING;
        client->currOp.handler = Client;
        client->flags |= GOEF_CLIENT_TP_INITIATED;
    }

#if BT_STACK == XA_ENABLED
Done:
#endif /* BT_STACK == XA_ENABLED */

Error:
    OS_UnlockStack();
    return status;
}

/*---------------------------------------------------------------------------
 *            GOEP_TpDisconnect
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initiate destruction of an OBEX Transport Connection.
 *
 * Return:    ObStatus
 *
 */
ObStatus GOEP_TpDisconnect(GoepClientApp *Client)
{
    ObStatus    status;
    GoepClientObexCons *client;
    I8          i, inUse = 0;

    OS_LockStack();

#if XA_ERROR_CHECK == XA_ENABLED
    if (!Client) {
        status = OB_STATUS_INVALID_PARM;
        goto Error;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(Client);

    client = &GOEC(clients)[Client->connId];
    
#if XA_ERROR_CHECK == XA_ENABLED
    if ((Client->type >= GOEP_MAX_PROFILES) ||
        (Client->connId >= GOEP_NUM_OBEX_CONS) || 
        (client->profiles[Client->type] != Client)) {
        status = OB_STATUS_INVALID_PARM;
        goto Error;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert((Client->type < GOEP_MAX_PROFILES) && 
           (Client->connId < GOEP_NUM_OBEX_CONS) &&
           (client->profiles[Client->type] == Client));

    if (client->currOp.handler) {
        status = OB_STATUS_BUSY;
        goto Error;
    }

    if (Client->connState != CS_CONNECTED) {
        status = OB_STATUS_NO_CONNECT;
        goto Error;
    }

    for (i = 0; i < GOEP_MAX_PROFILES; i++) {
        if ((client->profiles[i]) &&
            (client->profiles[i]->connState == CS_CONNECTED)) {
            inUse++;
        }
    }

    if (inUse > 1) {
        /* Tell the client its disconnected */
        Client->connState = CS_DISCONNECTED;
        status = OB_STATUS_SUCCESS;
        goto Error;
    }

    /* It's time to disconnect the transport connection */
    status = OBEX_ClientTpDisconnect(&client->obc, TRUE);

    if (status == OB_STATUS_SUCCESS) {
        /* The transport connection is down */
        client->currOp.oper = GOEP_OPER_NONE;
        Client->connState = CS_DISCONNECTED;
        client->flags &= ~GOEF_ACTIVE;
        client->flags &= ~GOEF_CLIENT_TP_INITIATED;
    } 
    else if (status == OB_STATUS_PENDING) {
        /* The transport connection is going down */
        client->currOp.handler = Client;
    }

Error:
    OS_UnlockStack();
    return status;
}

/*---------------------------------------------------------------------------
 *            GOEP_ClientContinue
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Called to keep data flowing between the client and the server.
 *            This function must be called once for every GOEP_EVENT_CONTINUE
 *            event received. It does not have to be called in the context
 *            of the callback. It can be deferred for flow control reasons.
 *
 * Return:    ObStatus
 */
ObStatus GOEP_ClientContinue(GoepClientApp *Client)
{
#if GOEP_ADDITIONAL_HEADERS > 0
    U16                 more;
#endif /* GOEP_ADDITIONAL_HEADERS > 0 */
    ObStatus            status;
    GoepClientObexCons *client;

    OS_LockStack();

#if XA_ERROR_CHECK == XA_ENABLED
    if (!Client) {
       status = OB_STATUS_INVALID_PARM;
       goto Error;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(Client);

    client = &GOEC(clients)[Client->connId];
        
#if XA_ERROR_CHECK == XA_ENABLED
    /* Verify Client Conn Id */
    if ((Client->type >= GOEP_MAX_PROFILES) ||
        (Client->connId >= GOEP_NUM_OBEX_CONS) || 
        (client->profiles[Client->type] != Client)) {
        status = OB_STATUS_INVALID_PARM;
        goto Error;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert((Client->type < GOEP_MAX_PROFILES) && 
           (Client->connId < GOEP_NUM_OBEX_CONS) &&
           (client->profiles[Client->type] == Client));

    if ((client->currOp.handler != Client) ||
        (client->currOp.event != GOEP_EVENT_CONTINUE)) {
        status = OB_STATUS_FAILED;
        goto Error;
    }

#if GOEP_ADDITIONAL_HEADERS > 0
    if (ClientBuildHeaders(client, &more) == FALSE) {
        OBEXH_FlushHeaders(&client->obc.handle);
        status = OB_STATUS_FAILED;
        goto Error;
    }
#endif /* GOEP_ADDITIONAL_HEADERS > 0 */

    OBEX_ClientSendCommand(&client->obc);

    status = OB_STATUS_SUCCESS;

Error:
    OS_UnlockStack();
    return status;
}

#if GOEP_ADDITIONAL_HEADERS > 0
/*---------------------------------------------------------------------------
 *            GOEP_ClientQueueHeader()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  This function queues a Byte Sequence, UNICODE, 1-byte, or 4-byte 
 *            OBEX header for transmission by the GOEP Client.
 *
 * Return:    True if the OBEX Header was queued successfully.
 */
BOOL GOEP_ClientQueueHeader(GoepClientApp *Client, ObexHeaderType Type,
                                   const U8 *Value, U16 Len) {
    U8                  i;
    BOOL                status;
    GoepClientObexCons *client;

    OS_LockStack();

#if XA_ERROR_CHECK == XA_ENABLED
    if (!Client) {
        status = FALSE;
        goto Error;
    }
   
    /* NAME and TYPE headers are not recommended, since they are typically built by GOEP
     * when initiating Push, Pull, or SetPath operations. If header ordering is critical, 
     * these headers can be queued in a different order.  If this is done, be sure not to 
     * add these headers a second time when the Push, Pull, or SetPath operation is initiated.
     */

    /* Only allow certain headers to be queued for transmission */
    if ((Type == OBEXH_TARGET) || (Type == OBEXH_WHO) || (Type == OBEXH_CONNID) ||
#if OBEX_SESSION_SUPPORT == XA_ENABLED
        (Type == OBEXH_SESSION_PARAMS) || (Type == OBEXH_SESSION_SEQ_NUM) ||
#endif /* OBEX_SESSION_SUPPORT == XA_ENABLED */
        (Type == OBEXH_AUTH_CHAL) || (Type == OBEXH_AUTH_RESP) || (Type == OBEXH_WAN_UUID)) {
        status = FALSE;
        goto Error;
    }

#if GOEP_DOES_UNICODE_CONVERSIONS == XA_DISABLED
    /* Unicode headers must have an even length if we aren't
     * automatically performing the unicode conversions. 
     */
    if (OBEXH_IsUnicode(Type) && ((Len % 2) != 0)) {
        status = FALSE;
        goto Error;
    }
#endif /* GOEP_DOES_UNICODE_CONVERSIONS == XA_DISABLED */
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(Client);

    client = &GOEC(clients)[Client->connId];

#if XA_ERROR_CHECK == XA_ENABLED
    /* Non-Body headers cannot exceed the available OBEX packet size */
    if ((Type != OBEXH_BODY) && (Len > OBEXH_GetAvailableTxHeaderLen(&client->obc.handle))) {
        status = FALSE;
        goto Error;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    for (i = 0; i < GOEP_ADDITIONAL_HEADERS; i++) {
        if (client->queuedHeaders[i].type == 0) {
            /* Found an empty header to queue */
            client->queuedHeaders[i].type = Type;
            client->queuedHeaders[i].buffer = Value;
            client->queuedHeaders[i].len = Len;
            status = TRUE;
            goto Error;
        }
    }

    /* No empty headers available */
    status = FALSE;

Error:
    OS_UnlockStack();
    return status;
}
#endif /* GOEP_ADDITIONAL_HEADERS > 0 */
#endif /* OBEX_ROLE_CLIENT == XA_ENABLED */

/*---------------------------------------------------------------------------
 *            GOEP_GetConnectionId
 *---------------------------------------------------------------------------
 *
 *     
 * Synopsis:  This function call facilitates the use of the OBEX client/server
 *            multiplexing capabilities of the GOEP layer, by retrieving a 
 *            connection Id (if one exists) for an OBEX client/server 
 *            connection.  A connection Id will be returned if an OBEX client/
 *            server connection exists and if no instances of the requested 
 *            profile are currently registered.  This is due to the fact that 
 *            only one instance of a specific profile is allowed over the same 
 *            OBEX connection.  
 *
 * Return:    Connection Id - 0-254, 255 (No connection Id exists)
 */
U8 GOEP_GetConnectionId(GoepRole role, GoepProfile profileType)
{
#if (OBEX_ROLE_CLIENT == XA_ENABLED) || (OBEX_ROLE_SERVER == XA_ENABLED)
    U8 i;
#endif /* (OBEX_ROLE_CLIENT == XA_ENABLED) || (OBEX_ROLE_SERVER == XA_ENABLED) */

    OS_LockStack();

    if (role == GOEP_CLIENT) {
#if OBEX_ROLE_CLIENT == XA_ENABLED
        Assert(GOEC(connCount) <= GOEP_NUM_OBEX_CONS);
        for (i = 0; i < GOEC(connCount); i++) {
            if (!GOEC(clients)[i].profiles[profileType]) {
                OS_UnlockStack();
                return GOEC(clients)[i].connId;
            }
        }
#endif /* OBEX_ROLE_CLIENT == XA_ENABLED */    
    }
    else if (role == GOEP_SERVER) {
#if OBEX_ROLE_SERVER == XA_ENABLED
        Assert(GOES(connCount) <= GOEP_NUM_OBEX_CONS);
        for (i = 0; i < GOES(connCount); i++) {
            if (!GOES(servers)[i].profiles[profileType]) {
                OS_UnlockStack();
                return GOES(servers)[i].connId;
            }
        }
#endif /* OBEX_ROLE_SERVER == XA_ENABLED */    
    }

    /* No Connection ID found */
    OS_UnlockStack();
    return 0xFF;
}

/****************************************************************************
 *
 * Internal Functions
 *
 ****************************************************************************/

#if OBEX_ROLE_SERVER == XA_ENABLED
#if (BT_STACK == XA_ENABLED) && (OBEX_ALLOW_SERVER_TP_CONNECT == XA_ENABLED)
/*---------------------------------------------------------------------------
 *            GoepSrvrBtEventHandler()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handles all ME and SDP events
 *
 * Return:    
 *
 */
static void GoepSrvrBtEventHandler(const BtEvent *event)
{
    ObStatus status;
    GoepServerObexCons *server;

	Assert(GOES(storedAppPtr));
    server = &GOES(servers)[GOES(storedAppPtr)->connId];
    
    switch (event->eType) {
    case BTEVENT_DEVICE_SELECTED:
        if (event->errCode == BEC_REQUEST_CANCELLED) {
            Report(("GOEP: Device Selection was cancelled!\n"));
            break;
        }
 
        if (event->errCode != BEC_NO_ERROR) {
            Report(("GOEP: Device Selection failed!\n"));
            break;
        }

        OS_MemCopy((U8 *)&GOES(tpAddr).proto.bt.psi,(U8 *)&GOES(deviceQuery).result->psi, sizeof(BtPageScanInfo));
        OS_MemCopy(GOES(tpAddr).proto.bt.addr.addr, GOES(deviceQuery).result->addr.addr, BD_ADDR_SIZE);

        status = GOEP_ServerTpConnect(GOES(storedAppPtr), &GOES(tpAddr));
		if ((status != OB_STATUS_PENDING) && (status != OB_STATUS_SUCCESS)) {
		    Report(("GOEP: Transport Connection Failure"));
		}
        break;
	}
}
#endif /* (BT_STACK == XA_ENABLED) && (OBEX_ALLOW_SERVER_TP_CONNECT == XA_ENABLED) */

/*---------------------------------------------------------------------------
 *            GoepSrvrCallback
 *---------------------------------------------------------------------------
 *
 * Synopsis:  This function processes OBEX Server protocol events.
 *
 * Return:    void
 *
 */
static void GoepSrvrCallback(ObexServerCallbackParms *parms)
{
#if BT_SECURITY == XA_ENABLED
    BtStatus            status;
    BOOL                success;
    ObexTpConnInfo      tpInfo;
#endif /* BT_SECURITY == XA_ENABLED */
    GoepServerObexCons *server = (GoepServerObexCons*)parms->server;

#if XA_DEBUG == XA_ENABLED
    server->lastEvent = ServerEventVerifier(server->lastEvent, parms->event);
#endif /* XA_DEBUG == XA_ENABLED */
    Assert(IsObexLocked());

    switch ( parms->event ) {
    case OBSE_SEND_RESPONSE:
        server->oustandingResp = TRUE;

#if BT_SECURITY == XA_ENABLED
        if (parms->opcode == OB_OPCODE_CONNECT) {
            /* Make sure the Server has been assigned */
            if ((server->currOp.event == GOEP_EVENT_NONE) &&
                (server->currOp.handler == 0)) {
                /* The Server hasn't been assigned yet. Indicate the START
                 * event early to make sure the server is assigned.
                 */
                NotifyCurrServer(parms->server, GOEP_EVENT_START);
            }

            if (server->currOp.handler) {
                /* Check security for this GoepServerApp */
                if ((server->flags & GOEF_SERVER_TP_INITIATED) == 0) {
                    /* Client initiated transport connection, check the 
                     * transport connection information 
                     */
                    success = OBEX_GetTpConnInfo(&server->obs.handle, &tpInfo);
                    Assert(success);
                    server->currOp.handler->secToken.remDev = tpInfo.remDev;
                } 
#if OBEX_ALLOW_SERVER_TP_CONNECT == XA_ENABLED
                else {
                    /* Must be server initiated */
                    server->currOp.handler->secToken.remDev = 
                        server->obs.trans.ObexClientBtTrans.remoteDevice;
                }
#endif /* OBEX_ALLOW_SERVER_TP_CONNECT == XA_ENABLED */
                server->currOp.handler->secToken.id = SEC_GOEP_ID;
                server->currOp.handler->secToken.channel = (U32)server->currOp.handler;
                server->currOp.handler->secToken.incoming = TRUE;
                status = SEC_AccessRequest(&(server->currOp.handler->secToken));
                if (status == BT_STATUS_SUCCESS) {
                    NotifyCurrServer(parms->server, GOEP_EVENT_CONTINUE);
                } else if (status != BT_STATUS_PENDING) {
                    /* Abort the connection operation */
                    status = OBEX_ServerAbort(&server->obs, OBRC_UNAUTHORIZED);
                    Assert(status == OB_STATUS_SUCCESS);
                    NotifyCurrServer(parms->server, GOEP_EVENT_CONTINUE);
                }
            } else {
                /* No server existed to perform security with */
                NotifyCurrServer(parms->server, GOEP_EVENT_CONTINUE);
            }
        } else {
            /* Not processing a connect operation, proceed as normal */
            NotifyCurrServer(parms->server, GOEP_EVENT_CONTINUE);
        }
#else /* BT_SECURITY == XA_ENABLED */
        NotifyCurrServer(parms->server, GOEP_EVENT_CONTINUE);
#endif /* BT_SECURITY == XA_ENABLED */
        break;

    case OBSE_PRECOMPLETE:
        NotifyCurrServer(parms->server, GOEP_EVENT_PRECOMPLETE);
        break;

    case OBSE_CONNECTED:
        server->flags |= GOEF_ACTIVE;

        server->currOp.oper = GOEP_OPER_NONE;
        NotifyAllServers(parms->server, GOEP_EVENT_TP_CONNECTED);
        break;

    case OBSE_DISCONNECT:
        if (server->flags & GOEF_ACTIVE) {
            server->flags &= ~GOEF_ACTIVE;
            server->flags &= ~GOEF_SERVER_TP_INITIATED;

            server->currOp.oper = GOEP_OPER_NONE;
            NotifyAllServers(parms->server, GOEP_EVENT_TP_DISCONNECTED);
        }
        break;

    case OBSE_HEADER_RX:
        GoepServerProcessHeaders(parms);
        break;

    case OBSE_DELETE_OBJECT:
        /* Change operation to delete, drop into START state notification */
        server->currOp.oper = GOEP_OPER_DELETE;
        NotifyCurrServer(parms->server, GOEP_EVENT_DELETE_OBJECT);
        break;

    case OBSE_PROVIDE_OBJECT:
        NotifyCurrServer(parms->server, GOEP_EVENT_PROVIDE_OBJECT);
        break;
        
    case OBSE_ABORTED:
        if (server->currOp.event != GOEP_EVENT_NONE) {
            /* Makes sure that the currOp.oper is correct during Aborts. */
            if (server->abortedOper != GOEP_OPER_NONE) {
                server->currOp.oper = server->abortedOper;
                server->abortedOper = GOEP_OPER_NONE;
            }
            NotifyCurrServer(parms->server, GOEP_EVENT_ABORTED);
        }
        server->currOp.oper = GOEP_OPER_NONE;
        break;

    case OBSE_COMPLETE:
        NotifyCurrServer(parms->server, GOEP_EVENT_COMPLETE);
        server->currOp.oper = GOEP_OPER_NONE;
        break;

    case OBSE_PUT_START:
        StartServerOperation(parms->server, GOEP_OPER_PUSH);
        break;

    case OBSE_GET_START:
        StartServerOperation(parms->server, GOEP_OPER_PULL);
        break;
    
    case OBSE_SET_PATH_START:
        StartServerOperation(parms->server, GOEP_OPER_SETFOLDER);
        server->currOp.info.setfolder.flags = parms->u.setPathFlags;
        break;
        
    case OBSE_CONNECT_START:
        StartServerOperation(parms->server, GOEP_OPER_CONNECT);
        break;
        
    case OBSE_DISCONNECT_START:
        StartServerOperation(parms->server, GOEP_OPER_DISCONNECT);
        break;
            
    case OBSE_ABORT_START:
        server->abortedOper = server->currOp.oper;
        StartServerOperation(parms->server, GOEP_OPER_ABORT);
        /* Inform the application that an Abort operation is
         * starting.  If an outstanding response exists, it must 
         * be sent via GOEP_ServerContinue to allow the Abort 
         * operation to complete.
         */
        Assert(server->currOp.handler == 0);
        AssociateServer(parms->server);
        NotifyCurrServer(parms->server, GOEP_EVENT_START);
        break;
    }
}


/*---------------------------------------------------------------------------
 *            GoepServerProcessHeaders
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Process the incoming headers based on the current operation.
 *            Note that ConnId and Target are processed by the OBEX layer.
 *
 * Return:    void
 *
 */
static void GoepServerProcessHeaders(ObexServerCallbackParms *parms)
{
    GoepServerObexCons *server = (GoepServerObexCons*)parms->server;

    switch (server->currOp.oper) {
    case GOEP_OPER_CONNECT:
#if GOEP_MAX_WHO_LEN > 0
        if (parms->u.headerRx.type == OBEXH_WHO) {
            DoHeaderCopy(server->currOp.info.connect.who, 
                &server->currOp.info.connect.whoLen, GOEP_MAX_WHO_LEN, 
                parms->u.headerRx.buff, parms->u.headerRx.currLen);
            return;
        }
#endif /* GOEP_MAX_WHO_LEN > 0 */
        break;

    case GOEP_OPER_PUSH:
    case GOEP_OPER_PULL:
        switch (parms->u.headerRx.type) {
        case OBEXH_NAME:
            DoUniHeaderCopy(server->currOp.info.pushpull.name, 
                &server->currOp.info.pushpull.nameLen, GOEP_MAX_UNICODE_LEN, 
                parms->u.headerRx.buff, parms->u.headerRx.currLen);                
            return;

#if GOEP_MAX_TYPE_LEN > 0
        case OBEXH_TYPE:
            DoHeaderCopy(server->currOp.info.pushpull.type, 
                &server->currOp.info.pushpull.typeLen, GOEP_MAX_TYPE_LEN, 
                parms->u.headerRx.buff, parms->u.headerRx.currLen);                
            return;
#endif /* GOEP_MAX_WHO_LEN > 0 */
        case OBEXH_LENGTH:
            server->currOp.info.pushpull.objectLen = parms->u.headerRx.value;
        }
        break;

    case GOEP_OPER_SETFOLDER:
        if (parms->u.headerRx.type == OBEXH_NAME) {
            if (parms->u.headerRx.totalLen == 0) 
                server->currOp.info.setfolder.reset = TRUE;
            else server->currOp.info.setfolder.reset = FALSE;

            DoUniHeaderCopy(server->currOp.info.setfolder.name, 
                &server->currOp.info.setfolder.nameLen, GOEP_MAX_UNICODE_LEN,
                parms->u.headerRx.buff, parms->u.headerRx.currLen);                
            return;
        }
        break;

    case GOEP_OPER_DISCONNECT:
        break;
    }

#if OBEX_AUTHENTICATION == XA_ENABLED
    /* We accept authentication headers with any operation. */
    switch (parms->u.headerRx.type) {
    case OBEXH_AUTH_CHAL:
        if (OBEXH_ParseAuthChallenge(&parms->server->handle, 
                    &server->currOp.challenge)) {
            /* Full auth challenge has been received, indicate it. */
            server->flags |= GOEF_CHALLENGE;
            NotifyCurrServer(parms->server, GOEP_EVENT_AUTH_CHALLENGE);
        }
        return;

    case OBEXH_AUTH_RESP:
        if (OBEXH_ParseAuthResponse(&parms->server->handle, 
                    &server->currOp.response)) {
            /* Full auth response has been received, indicate it. */
            server->flags |= GOEF_RESPONSE;
            NotifyCurrServer(parms->server, GOEP_EVENT_AUTH_RESPONSE);
        }
        return;
    }
#endif /* OBEX_AUTHENTICATION == XA_ENABLED */

    if (parms->u.headerRx.type == OBEXH_TARGET ||
        parms->u.headerRx.type == OBEXH_CONNID) {
        /* We do not pass up Target or ConnId headers, in order 
         * for Directed connections and operations to setup properly.
         */
        return;
    }

    /* If we have not processed the OBEX header already we will accept it now, regardless
     * the current operation. 
     */
    server->currOp.header.type = parms->u.headerRx.type;
    if (OBEXH_Is1Byte(server->currOp.header.type) || 
        OBEXH_Is4Byte(server->currOp.header.type)) {
        /* 1-byte or 4-byte*/
        server->currOp.header.value = parms->u.headerRx.value;
    } else if (OBEXH_IsUnicode(server->currOp.header.type)) {
        /* Unicode */
        DoUniHeaderCopy(server->currOp.header.unicodeBuffer, &server->currOp.header.len, 
            GOEP_MAX_UNICODE_LEN, parms->u.headerRx.buff, parms->u.headerRx.currLen);                
        /* Don't indicate the Unicode header until it is completely copied */
        if (parms->u.headerRx.remainLen > 0) return;
        /* Point the header buffer pointer to the unicode buffer */
        server->currOp.header.buffer = (U8 *)server->currOp.header.unicodeBuffer;
    } else {
        /* Byte Sequence */
        Assert(OBEXH_IsByteSeq(server->currOp.header.type));
        server->currOp.header.totalLen = parms->u.headerRx.totalLen;
        server->currOp.header.remainLen = parms->u.headerRx.remainLen;
        server->currOp.header.len = parms->u.headerRx.currLen;
        server->currOp.header.buffer = parms->u.headerRx.buff;
    }
    NotifyCurrServer(parms->server, GOEP_EVENT_HEADER_RX);

    /* Clear out the header information in preparation for a new header */
    OS_MemSet((U8 *)&server->currOp.header, 0, sizeof(server->currOp.header));
}

/*---------------------------------------------------------------------------
 *            AssociateServer
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Associate a specific profile server based on the target header
 *            for this particular Obex Server.
 *
 * Return:    void
 *
 */
static void AssociateServer(ObexServerApp *serverApp)
{
#if OBEX_SERVER_CONS_SIZE > 0
    U8 i,j;
#endif /* OBEX_SERVER_CONS_SIZE > 0 */
    GoepServerObexCons *server = (GoepServerObexCons*)serverApp;

    Assert(server->connId < GOEP_NUM_OBEX_CONS);

    /* Try to associate the operation with a server */
#if OBEX_SERVER_CONS_SIZE > 0
        for (i = 0; i < GOEP_MAX_PROFILES; i++) {
            if (server->profiles[i]) {
                Assert(server->profiles[i]->numTargets <= OBEX_SERVER_CONS_SIZE);
                for (j = 0; j < server->profiles[i]->numTargets; j++) {
                    if (server->profiles[i]->target[j] == OBEX_ServerGetConnInfo(serverApp)) {
                        /* We have found a matching target header for the active connection
                         * ID of the current operation.
                         */
                        server->currOp.handler = server->profiles[i];
                        return;
                    }
                }
                if (!server->profiles[i]->numTargets && !OBEX_ServerGetConnInfo(serverApp)) {
                    /* We do not have active connection ID for this operation and this 
                     * profile has no target headers; therefore, set this profile as our handler.
                     */
                    server->currOp.handler = server->profiles[i];
                    return;
                }
            }
        }
#else /* OBEX_SERVER_CONS_SIZE > 0 */
    /* Assign Object Push Server */
    server->currOp.handler = server->profiles[GOEP_PROFILE_OPUSH];
#endif /* OBEX_SERVER_CONS_SIZE > 0 */
}

/*---------------------------------------------------------------------------
 *            StartServerOperation
 *---------------------------------------------------------------------------
 *
 */
static void StartServerOperation(ObexServerApp *serverApp, GoepOperation Op)
{
    GoepServerObexCons *server = (GoepServerObexCons*)serverApp;

    OS_MemSet((U8 *)&server->currOp, 0, sizeof(server->currOp));

    /* Set event to NONE so we can track when the START event is delivered. */
    if (Op == GOEP_OPER_ABORT) {
        /* We will generate the GOEP_EVENT_START automatically when 
         * OBSE_ABORT_START is received, so there is no need to set 
         * GOEP_EVENT_NONE to generate it again.
         */
        server->currOp.event = GOEP_EVENT_START;
    } else server->currOp.event = GOEP_EVENT_NONE;
    server->currOp.oper = Op;

    /* Clear authentication flags */
#if OBEX_AUTHENTICATION == XA_ENABLED
    server->flags &= ~(GOEF_CHALLENGE|GOEF_RESPONSE);
#endif /* OBEX_AUTHENTICATION == XA_ENABLED */
}


/*---------------------------------------------------------------------------
 *            NotifyAllServers
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Notify all profile servers for this Obex Server of the
 *            GOEP event.
 *
 * Return:    void
 *
 */
static void NotifyAllServers(ObexServerApp *serverApp, GoepEventType Event)
{
    U8  i;
    GoepServerObexCons *server = (GoepServerObexCons*)serverApp;

    Assert(server->connId < GOEP_NUM_OBEX_CONS);

    server->currOp.event = Event; 
    for (i = 0; i < GOEP_MAX_PROFILES; i++) {
        if (server->profiles[i]) {
            server->currOp.handler = server->profiles[i];
            server->profiles[i]->appParent(&server->currOp);
        }
    }
}

/*---------------------------------------------------------------------------
 *            NotifyCurrServer
 *---------------------------------------------------------------------------
 *
 */
static void NotifyCurrServer(ObexServerApp *serverApp, GoepEventType Event)
{
    ObStatus    status;
    GoepServerObexCons *server = (GoepServerObexCons*)serverApp;

    if (server->currOp.oper == GOEP_OPER_PUSH || 
        server->currOp.oper == GOEP_OPER_PULL) {
        /* Indicate the status of the final bit in the current OBEX packet */
        server->currOp.info.pushpull.finalBit = 
            ((serverApp->handle.parser.opcode & 0x80) ? TRUE : FALSE);
    }

    /* If this is the first event for this operation, find the correct
     * server and make sure we always indicate a START event first.
     */
    if (server->currOp.event == GOEP_EVENT_NONE) {
        Assert(server->currOp.handler == 0);
        AssociateServer(serverApp);

        /* Be sure to indicate a start event before any other event. */
        if (Event != GOEP_EVENT_START) {
            if (server->currOp.handler) {
                server->currOp.event = GOEP_EVENT_START;
                server->currOp.handler->appParent(&server->currOp);
            }
        }
    }

    server->currOp.event = Event;

    /* If we found a server, indicate the event now. */
    if (server->currOp.handler) {
        server->currOp.handler->appParent(&server->currOp);
        return;
    }

    /* We did not find a suitable server, handle operation rejection. */
    if (Event == GOEP_EVENT_CONTINUE) {
        status = OBEX_ServerAbort(serverApp, OBRC_SERVICE_UNAVAILABLE);
        Assert(status == OB_STATUS_SUCCESS);
        OBEX_ServerSendResponse(&server->obs);
        return;
    }
}
#endif /* OBEX_ROLE_SERVER == XA_ENABLED */

#if OBEX_ROLE_CLIENT == XA_ENABLED

#if BT_STACK == XA_ENABLED
/*---------------------------------------------------------------------------
 *            GoepClntBtEventHandler()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handles all ME and SDP events
 *
 * Return:    
 *
 */
static void GoepClntBtEventHandler(const BtEvent *event)
{
    ObStatus status;
    GoepClientObexCons *client;

	Assert(GOEC(storedAppPtr));
    client = &GOEC(clients)[GOEC(storedAppPtr)->connId];
    
    switch (event->eType) {
    case BTEVENT_DEVICE_SELECTED:
        if (event->errCode == BEC_REQUEST_CANCELLED) {
            Report(("GOEP: Device Selection was cancelled!\n"));
		    GOEC(storedAppPtr)->connState = CS_DISCONNECTED;
            break;
        }
 
        if (event->errCode != BEC_NO_ERROR) {
            Report(("GOEP: Device Selection failed!\n"));
		    GOEC(storedAppPtr)->connState = CS_DISCONNECTED;
            break;
        }

        OS_MemCopy((U8 *)&GOEC(tpAddr).proto.bt.psi,(U8 *)&GOEC(deviceQuery).result->psi, sizeof(BtPageScanInfo));
        OS_MemCopy(GOEC(tpAddr).proto.bt.addr.addr, GOEC(deviceQuery).result->addr.addr, BD_ADDR_SIZE);

        status = GOEP_TpConnect(GOEC(storedAppPtr), &GOEC(tpAddr));
		if ((status != OB_STATUS_PENDING) && (status != OB_STATUS_SUCCESS)) {
		    Report(("GOEP: Transport Connection Failure"));
		    GOEC(storedAppPtr)->connState = CS_DISCONNECTED;
		}
        break;
	}
}
#endif /* BT_STACK == XA_ENABLED */

/*---------------------------------------------------------------------------
 *            GoepClntCallback
 *---------------------------------------------------------------------------
 *
 * Synopsis:  This function processes OBEX client protocol events.
 *
 * Return:    void
 *
 */
static void GoepClntCallback(ObexClientCallbackParms *parms)
{
    GoepClientObexCons *client = (GoepClientObexCons*)parms->client;

    Assert(IsObexLocked());
    switch (parms->event) {
    case OBCE_SEND_COMMAND:
        Assert(client->currOp.event != GOEP_EVENT_NONE);
        NotifyCurrClient(parms->client, GOEP_EVENT_CONTINUE);
        break;

    case OBCE_CONNECTED:
        /* A Transport Layer Connection has been established. */
        Assert((client->flags & GOEF_ACTIVE) == 0);
        client->flags |= GOEF_ACTIVE;
        NotifyAllClients(parms->client, GOEP_EVENT_TP_CONNECTED);
        break;

    case OBCE_DISCONNECT:
        client->flags &= ~GOEF_ACTIVE;
        client->flags &= ~GOEF_CLIENT_TP_INITIATED;
        NotifyAllClients(parms->client, GOEP_EVENT_TP_DISCONNECTED);
        break;
        
    case OBCE_COMPLETE:
        /* The requested operation has completed. */
        client->currOp.reason = OBRC_SUCCESS;
        NotifyCurrClient(parms->client, GOEP_EVENT_COMPLETE);
        break;
        
    case OBCE_ABORTED:
        /* The requested operation was aborted. */
        client->currOp.reason = parms->u.abortReason;
        if (client->currOp.reason == 
            OBRC_LINK_DISCONNECT) {
            client->flags &= ~GOEF_ACTIVE;
            client->flags &= ~GOEF_CLIENT_TP_INITIATED;
            NotifyCurrClient(parms->client, GOEP_EVENT_ABORTED);
            NotifyAllClients(parms->client, GOEP_EVENT_TP_DISCONNECTED);
        } else {
            NotifyCurrClient(parms->client, GOEP_EVENT_ABORTED);
        }
        break;
        
    case OBCE_NO_SERVICE_FOUND:
        Assert((client->flags & GOEF_ACTIVE) == 0);
        client->flags &= ~GOEF_CLIENT_TP_INITIATED;
        NotifyCurrClient(parms->client, GOEP_EVENT_NO_SERVICE_FOUND);
        break;

    case OBCE_DISCOVERY_FAILED:
        Assert((client->flags & GOEF_ACTIVE) == 0);
        client->flags &= ~GOEF_CLIENT_TP_INITIATED;
        NotifyCurrClient(parms->client, GOEP_EVENT_DISCOVERY_FAILED);
        break;

    case OBCE_HEADER_RX:
        GoepClientProcessHeaders(parms);
        break;

    default:
        Assert(0);
        break;
    }
}

/*---------------------------------------------------------------------------
 *            GoepClientProcessHeaders
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Process the incoming headers based on the current operation.
 *
 * Return:    void
 *
 */
void GoepClientProcessHeaders(ObexClientCallbackParms *parms)
{
    GoepClientObexCons *client = (GoepClientObexCons*)parms->client;

    switch (client->currOp.oper) {

    case GOEP_OPER_CONNECT:
#if GOEP_MAX_WHO_LEN > 0
        if (parms->u.headerRx.type == OBEXH_WHO) {
            DoHeaderCopy(client->currOp.info.connect.who, 
                &client->currOp.info.connect.whoLen, GOEP_MAX_WHO_LEN, 
                parms->u.headerRx.buff, parms->u.headerRx.currLen);
            return;
        }
#endif /* GOEP_MAX_WHO_LEN > 0 */
        if (parms->u.headerRx.type == OBEXH_CONNID) {
            client->currOp.handler->obexConnId = parms->u.headerRx.value;
            return;
        }
        break;
       
    case GOEP_OPER_PULL:
        if (parms->u.headerRx.type == OBEXH_LENGTH) {
            /* Set the incoming object length so the progress meter works. */
            client->currOp.info.pull.objectLen = parms->u.headerRx.value;
        }
        break;
    }

#if OBEX_AUTHENTICATION == XA_ENABLED
    /* We accept authentication headers with any operation */
    switch (parms->u.headerRx.type) {
    case OBEXH_AUTH_CHAL:
        if (OBEXH_ParseAuthChallenge(&parms->client->handle, 
                &client->currOp.challenge)) {
            /* Full auth challenge has been received, indicate it. */
            client->flags |= GOEF_CHALLENGE;
            NotifyCurrClient(parms->client, GOEP_EVENT_AUTH_CHALLENGE);
        }
        return;

    case OBEXH_AUTH_RESP:
        if (OBEXH_ParseAuthResponse(&parms->client->handle, 
                &client->currOp.response)) {
            /* Full auth response has been received, indicate it. */
            client->flags |= GOEF_RESPONSE;
            NotifyCurrClient(parms->client, GOEP_EVENT_AUTH_RESPONSE);
        }
        return;
    }
#endif /* OBEX_AUTHENTICATION == XA_ENABLED */

    if (parms->u.headerRx.type == OBEXH_TARGET ||
        parms->u.headerRx.type == OBEXH_CONNID) {
        /* We do not pass up Target or ConnId headers, in order 
         * for Directed connections and operations to setup properly.
         */
        return;
    }

    /* If we have not processed the OBEX header already we will accept it now, regardless
     * the current operation. 
     */
    client->currOp.header.type = parms->u.headerRx.type;
    if (OBEXH_Is1Byte(client->currOp.header.type) || 
        OBEXH_Is4Byte(client->currOp.header.type)) {
        /* 1-byte or 4-byte */
        client->currOp.header.value = parms->u.headerRx.value;
    } else if (OBEXH_IsUnicode(client->currOp.header.type)) {
        /* Unicode */
        DoUniHeaderCopy(client->currOp.header.unicodeBuffer, &client->currOp.header.len,
            GOEP_MAX_UNICODE_LEN, parms->u.headerRx.buff, parms->u.headerRx.currLen);                
        /* Don't indicate the Unicode header until it is completely copied */
        if (parms->u.headerRx.remainLen > 0) return;
        /* Point the header buffer pointer to the unicode buffer */
        client->currOp.header.buffer = (U8 *)client->currOp.header.unicodeBuffer;
    } else {
        /* Byte Sequence */
        Assert(OBEXH_IsByteSeq(client->currOp.header.type));
        client->currOp.header.totalLen = parms->u.headerRx.totalLen;
        client->currOp.header.remainLen = parms->u.headerRx.remainLen;
        client->currOp.header.len = parms->u.headerRx.currLen;
        client->currOp.header.buffer = parms->u.headerRx.buff;
    }
    NotifyCurrClient(parms->client, GOEP_EVENT_HEADER_RX);

    /* Clear out the header information in preparation for a new header */
    OS_MemSet((U8 *)&client->currOp.header, 0, sizeof(client->currOp.header));
}

/*---------------------------------------------------------------------------
 *            NotifyAllClients
 *---------------------------------------------------------------------------
 *
 */
static void NotifyAllClients(ObexClientApp *clientApp, GoepEventType Event)
{
    GoepClientEvent     tempOp;
    U8                  i;
    GoepClientObexCons  *client = (GoepClientObexCons*)clientApp;

    Assert(client->connId < GOEP_NUM_OBEX_CONS);

    Assert((Event == GOEP_EVENT_TP_CONNECTED) ||
           (Event == GOEP_EVENT_TP_DISCONNECTED));

    client->currOp.event = Event;
    client->currOp.handler = 0;
    client->currOp.oper = GOEP_OPER_NONE;

    tempOp = client->currOp;

    for (i = 0; i < GOEP_MAX_PROFILES; i++) {
        if (client->profiles[i]) {
            client->profiles[i]->obexConnId = OBEX_INVALID_CONNID;
            tempOp.handler = client->profiles[i];

            if ((Event == GOEP_EVENT_TP_DISCONNECTED) &&
                ((client->profiles[i]->connState == 
                  CS_CONNECTED) || (client->profiles[i]->connState == 
                  CS_CONNECTING))) {
                /* Deliver disconnect event */
                client->profiles[i]->connState = CS_DISCONNECTED;
                client->profiles[i]->appParent(&tempOp);
            }
            else if ((Event == GOEP_EVENT_TP_CONNECTED) && 
#if OBEX_ALLOW_SERVER_TP_CONNECT == XA_ENABLED
                     ((client->profiles[i]->connState == CS_CONNECTING)
                     || (client->profiles[i]->connState == CS_DISCONNECTED))) {
#else /* OBEX_ALLOW_SERVER_TP_CONNECT == XA_ENABLED */
                     (client->profiles[i]->connState == CS_CONNECTING)) {
#endif /* OBEX_ALLOW_SERVER_TP_CONNECT == XA_ENABLED */
                /* Deliver connect event */
                client->profiles[i]->connState = CS_CONNECTED;
                client->profiles[i]->appParent(&tempOp);
            }
        }
    }
}

/*---------------------------------------------------------------------------
 *            NotifyCurrClient
 *---------------------------------------------------------------------------
 *
 */
static void NotifyCurrClient(ObexClientApp *clientApp, GoepEventType Event)
{
    GoepClientEvent  tempOp;
    GoepClientObexCons  *client = (GoepClientObexCons*)clientApp;

    Assert(client->currOp.handler);

    client->currOp.event = Event;
    tempOp = client->currOp;

    if (Event >= GOEP_EVENT_COMPLETE) {
        Assert((Event == GOEP_EVENT_TP_CONNECTED) || 
               (Event == GOEP_EVENT_TP_DISCONNECTED) || 
               (Event == GOEP_EVENT_ABORTED) || 
               (Event == GOEP_EVENT_DISCOVERY_FAILED) ||
               (Event == GOEP_EVENT_NO_SERVICE_FOUND) ||
               (Event == GOEP_EVENT_COMPLETE));
        if ((Event == GOEP_EVENT_DISCOVERY_FAILED) || 
            (Event == GOEP_EVENT_NO_SERVICE_FOUND)) {
            /* Reset the connection state for this profile */
            client->currOp.handler->connState = CS_DISCONNECTED;
        }
        /* Were done, remove the handler before indicating the event. */
        client->currOp.handler = 0;
    }

    /* Indicate the event */
    tempOp.event = Event;
    tempOp.handler->appParent(&tempOp);
}

/*---------------------------------------------------------------------------
 *            Strlen
 *---------------------------------------------------------------------------
 *
 */
static U16 Strlen(const char *String)
{
   const char  *cp = String;

   Assert(String);

   while (*cp != 0) cp++;

   return (U16)(cp - String);
}

#if GOEP_DOES_UNICODE_CONVERSIONS == XA_DISABLED
/*---------------------------------------------------------------------------
 *            Unilen
 *---------------------------------------------------------------------------
 *
 */
static U16 Unilen(const U16 *Unistr)
{
   const U16  *cp = Unistr;

   Assert(Unistr);

   while (*cp != 0) cp++;

   return (U16)((U8*)cp - (U8*)Unistr);
}
#endif /* GOEP_DOES_UNICODE_CONVERSIONS == XA_DISABLED */
#endif /* OBEX_ROLE_CLIENT == XA_ENABLED */

#if GOEP_DOES_UNICODE_CONVERSIONS == XA_ENABLED
/*---------------------------------------------------------------------------
 *            AsciiToUnicode
 *---------------------------------------------------------------------------
 *
 */
static U16 AsciiToUnicode(U16 *Unistr, const char *String)
{
   const char  *src = String;
   char        *dst = (char *)Unistr;

   while (*src != 0) {
        *dst++ = 0;
        *dst++ = *src++;
   }

   /* Add null terminator but don't advance ptr */
   *(dst+0) = 0;
   *(dst+1) = 0;

   return (dst - (char *)Unistr);
}
#endif /* GOEP_DOES_UNICODE_CONVERSIONS == XA_ENABLED */

#define LEN_UNICODE_ODD_FLAG    0x8000
#define LEN_UNICODE_MASK        0x7FFF
/*---------------------------------------------------------------------------
 *            DoUniHeaderCopy
 *---------------------------------------------------------------------------
 *
 */
static void DoUniHeaderCopy(GoepUniType *Dst, U16 *Len, U16 MaxLen, 
                            U8 *Src, U16 SrcLen)
{
#if GOEP_DOES_UNICODE_CONVERSIONS == XA_ENABLED
    U8  *src;
    U16  toCopy;

    src = Src;
    toCopy = SrcLen;

    /* If we have run out of space to write data, return */
    if ((*Len & LEN_UNICODE_MASK) == MaxLen) {
        *Len &= LEN_UNICODE_MASK;
        return;
    }
    
    /* Advance destination ptr past written data. */
    Dst += (*Len & LEN_UNICODE_MASK);

    /* Convert to ASCII as we copy */
    while (toCopy) {
        if (*Len & LEN_UNICODE_ODD_FLAG) {    /* Unicode Odd */
            /* This is the byte to store */
            *Dst++ = *src;
            *Len &= ~LEN_UNICODE_ODD_FLAG;
            (*Len)++;
        } else {
            *Len |= LEN_UNICODE_ODD_FLAG;
        }
        src++;
        toCopy--;

        if ((*Len & LEN_UNICODE_MASK) == MaxLen) {
            *Len &= LEN_UNICODE_MASK;
            break;
        }
    }
#else /* GOEP_DOES_UNICODE_CONVERSIONS == XA_ENABLED */
    /* Since we are copying the bytes one at a time, we must make sure
     * that MaxLen reflects a larger type that GoepUniType may have 
     * referenced (i.e. U16).  Doing so will make sure that we can 
     * copy every byte within the GoepUniType buffer provided.
     */
    DoHeaderCopy((U8 *)Dst, Len, (U16)(MaxLen*sizeof(GoepUniType)), Src, SrcLen);
#endif /* GOEP_DOES_UNICODE_CONVERSIONS == XA_ENABLED */
}

/*---------------------------------------------------------------------------
 *            DoHeaderCopy
 *---------------------------------------------------------------------------
 *
 */
static void DoHeaderCopy(U8 *Dst, U16 *DstLen, U16 MaxLen, U8 *Src, U16 SrcLen)
{
    U16 toCopy;

    /* Advance destination ptr past written data. */
    Dst += *DstLen;

    toCopy = min((MaxLen - *DstLen), SrcLen);
    OS_MemCopy(Dst, Src, toCopy);
    *DstLen += toCopy;
}

/*---------------------------------------------------------------------------
 *            GetFreeConnId
 *---------------------------------------------------------------------------
 *
 */
static BOOL GetFreeConnId(U8 *id, U8 type)
{
    U8 i;

    for (i = 0; i < GOEP_NUM_OBEX_CONS; i++) {
        if (type == GOEP_SERVER) {
#if OBEX_ROLE_SERVER == XA_ENABLED 
            if (GOES(servers)[i].connCount == 0) {
                /* Free server */
                *id = i;
                return TRUE;
            }
#endif /* OBEX_ROLE_SERVER == XA_ENABLED */
        }
        else if (type == GOEP_CLIENT) {
#if OBEX_ROLE_CLIENT == XA_ENABLED
            if (GOEC(clients)[i].connCount == 0) {
                /* Free client */
                *id = i;
                return TRUE;
            }
#endif /* OBEX_ROLE_CLIENT == XA_ENABLED */
        }
    }

    return FALSE;
}

#if GOEP_ADDITIONAL_HEADERS > 0
#if OBEX_ROLE_SERVER == XA_ENABLED
/*---------------------------------------------------------------------------
 *            ServerBuildHeaders()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  This function builds Byte Sequence, UNICODE, 1-byte, and 4-byte 
 *            OBEX headers for transmission by the GOEP Server.  
 *
 * Return:    True if the OBEX Headers were built successfully. False indicates 
 *            that the headers could not be built successfully.
 */
static BOOL ServerBuildHeaders(GoepServerObexCons *server) {
    U8              i;
    U32             value;
#if GOEP_DOES_UNICODE_CONVERSIONS == XA_ENABLED
    U16             len;
    U16             uniName[GOEP_MAX_UNICODE_LEN];
#endif /* GOEP_DOES_UNICODE_CONVERSIONS == XA_ENABLED */
    BOOL            status = TRUE;

    for (i = 0; i < GOEP_ADDITIONAL_HEADERS; i++) {
        if (server->queuedHeaders[i].type != 0) {
            switch (server->queuedHeaders[i].type & 0xC0) {
            case 0x00:
                /* Unicode */
#if GOEP_DOES_UNICODE_CONVERSIONS == XA_ENABLED
                /* Make sure GOEP_MAX_UNICODE_LEN is large enough for the header
                 * in Unicode form to prevent memory corruption problems.
                 */
                if ((server->queuedHeaders[i].len * 2) > GOEP_MAX_UNICODE_LEN) {
                    status = FALSE;
                    break;
                }
            
                len = AsciiToUnicode(uniName, server->queuedHeaders[i].buffer) + 2;
                if (len > 2)
                    status = OBEXH_BuildUnicode(&server->obs.handle, server->queuedHeaders[i].type, 
                        (char *)uniName, len);
                else status = OBEXH_BuildUnicode(&server->obs.handle, server->queuedHeaders[i].type, 0, 0);
#else /* GOEP_DOES_UNICODE_CONVERSIONS == XA_ENABLED */
                if (server->queuedHeaders[i].len > 2)
                    status = OBEXH_BuildUnicode(&server->obs.handle, server->queuedHeaders[i].type, 
                        server->queuedHeaders[i].buffer, server->queuedHeaders[i].len);
                else status = OBEXH_BuildUnicode(&server->obs.handle, server->queuedHeaders[i].type, 0, 0);
#endif /* GOEP_DOES_UNICODE_CONVERSIONS == XA_ENABLED */
                break;

            case 0x40:
                /* Byte Sequence */
                status = OBEXH_BuildByteSeq(&server->obs.handle, server->queuedHeaders[i].type,
                            server->queuedHeaders[i].buffer, server->queuedHeaders[i].len);
                break;

            case 0x80:
                /* 1-byte */
                status = OBEXH_Build1Byte(&server->obs.handle, server->queuedHeaders[i].type,
                            server->queuedHeaders[i].buffer[0]);
                break;

            case 0xC0:
                /* 4-byte */
                value = BEtoHost32(server->queuedHeaders[i].buffer);
                status = OBEXH_Build4Byte(&server->obs.handle, server->queuedHeaders[i].type,
                            value);
                break;
            }

            if (status == FALSE) {
                /* Clear the unsuccessful header */
                OS_MemSet((U8 *)&server->queuedHeaders[i], 0, sizeof(server->queuedHeaders[i]));
                return FALSE;
            }
        }
    }

    if (status == TRUE) {
        /* Clear all of the built headers, since they have been added successfully */
        for (i = 0; i < GOEP_ADDITIONAL_HEADERS; i++) 
            OS_MemSet((U8 *)&server->queuedHeaders[i], 0, sizeof(server->queuedHeaders[i]));
    }

    return status;
}
#endif /* OBEX_ROLE_SERVER == XA_ENABLED */

#if OBEX_ROLE_CLIENT == XA_ENABLED
/*---------------------------------------------------------------------------
 *            ClientBuildHeaders()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  This function builds Byte Sequence, UNICODE, 1-byte, and 4-byte 
 *            OBEX headers for transmission by the GOEP Client. Only a GET
 *            request can have BODY headers that span multiple OBEX packets.
 *            This function will queue as much header data as possible in the 
 *            current OBEX packet, with the remainder of the BODY header 
 *            information being sent in subsequent packets.  However, if a 
 *            BODY header exists, it will be the only header sent in the GET
 *            request.  This ensures that any headers sent after the BODY 
 *            header (i.e. End Of Body) will fit into one GET request packet.
 *
 * Return:    True if the OBEX Headers were built successfully. False indicates 
 *            that the headers could not be built successfully.
 */
static BOOL ClientBuildHeaders(GoepClientObexCons *client, U16 *more) {
    U8              i, index;
    U32             value;
#if GOEP_DOES_UNICODE_CONVERSIONS == XA_ENABLED
    U16             len;
    U16             uniName[GOEP_MAX_UNICODE_LEN];
#endif /* GOEP_DOES_UNICODE_CONVERSIONS == XA_ENABLED */
    U16             transmitLen = 0;
    BOOL            status = TRUE;

    /* Assume no more header information to send */
    *more = 0;

    for (i = 0; i < GOEP_ADDITIONAL_HEADERS; i++) {
        if (client->queuedHeaders[i].type != 0) {
            switch (client->queuedHeaders[i].type & 0xC0) {
            case 0x00:
                /* Unicode */
#if GOEP_DOES_UNICODE_CONVERSIONS == XA_ENABLED
                /* Make sure GOEP_MAX_UNICODE_LEN is large enough for the header
                 * in Unicode form to prevent memory corruption problems.
                 */
                if ((client->queuedHeaders[i].len * 2) > GOEP_MAX_UNICODE_LEN) {
                    status = FALSE;
                    break;
                }
            
                len = AsciiToUnicode(uniName, client->queuedHeaders[i].buffer) + 2;
                if (len > 2)
                    status = OBEXH_BuildUnicode(&client->obc.handle, client->queuedHeaders[i].type, 
                        (char *)uniName, len);
                else status = OBEXH_BuildUnicode(&client->obc.handle, client->queuedHeaders[i].type, 0, 0);
#else /* GOEP_DOES_UNICODE_CONVERSIONS == XA_ENABLED */
                if (client->queuedHeaders[i].len > 2)
                    status = OBEXH_BuildUnicode(&client->obc.handle, client->queuedHeaders[i].type, 
                        client->queuedHeaders[i].buffer, client->queuedHeaders[i].len);
                else status = OBEXH_BuildUnicode(&client->obc.handle, client->queuedHeaders[i].type, 0, 0);
#endif /* GOEP_DOES_UNICODE_CONVERSIONS == XA_ENABLED */
                break;

            case 0x40:                
                /* Byte Sequence */
                if ((client->currOp.oper == GOEP_OPER_PULL) && 
                    (client->queuedHeaders[i].type == OBEXH_BODY)) {
                    /* Calculate how much Body header data can fit into an OBEX packet */
                    transmitLen = min(client->queuedHeaders[i].len, 
                                      (OBEXH_GetAvailableTxHeaderLen(&client->obc.handle) - 3));
                } else {
                    /* The entire header must fit in one OBEX packet, if not we should not add the header */
                    transmitLen = client->queuedHeaders[i].len;
                }

                /* Byte Sequence */
                status = OBEXH_BuildByteSeq(&client->obc.handle, client->queuedHeaders[i].type,
                             client->queuedHeaders[i].buffer, transmitLen);

                if (status == TRUE) {
                    if ((client->currOp.oper == GOEP_OPER_PULL) && 
                        (client->queuedHeaders[i].type == OBEXH_BODY)) {
                        if ((client->queuedHeaders[i].len - transmitLen) > 0) {
                            /* Update the length and buffer, since additional data needs 
                             * to be sent in our next packet.
                             */
                            client->queuedHeaders[i].len -= transmitLen;
                            client->queuedHeaders[i].buffer += transmitLen;
                            *more = client->queuedHeaders[i].len;
                            goto Done;
                        }

                        /* Other headers may exist after the BODY header.  However, since we don't know
                         * how much space is available in the packet after the BODY is complete, we'll 
                         * wait to send any remaining headers in our next packet.  All remaining headers
                         * after the BODY must fit into one packet.
                         */
                        *more = client->queuedHeaders[i].len;
                        /* Clear the complete BODY header. */
                        i++;
                        goto Done;
                    }
                }
                break;

            case 0x80:
                /* 1-byte */
                status = OBEXH_Build1Byte(&client->obc.handle, client->queuedHeaders[i].type,
                            client->queuedHeaders[i].buffer[0]);
                break;
                
            case 0xC0:
                /* 4-byte */
                value = BEtoHost32(client->queuedHeaders[i].buffer);
                status = OBEXH_Build4Byte(&client->obc.handle, client->queuedHeaders[i].type, value);
                break;
            }

            if (status == FALSE) {
                /* Clear the failed queued header. */
                OS_MemSet((U8 *)&client->queuedHeaders[i], 0, sizeof(client->queuedHeaders[i]));
                return FALSE;
            }
        }
    }

Done:
    if (status == TRUE) {
        /* Clear all of the built headers, since they have been added successfully */
        for (index = 0; index < i; index++) 
            OS_MemSet((U8 *)&client->queuedHeaders[index], 0, sizeof(client->queuedHeaders[index]));
    }
    
    return status;
}
#endif /* OBEX_ROLE_CLIENT == XA_ENABLED */
#endif /* GOEP_ADDITIONAL_HEADERS > 0 */

#if BT_SECURITY == XA_ENABLED
/*---------------------------------------------------------------------------
 *            GoepSecCallback()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Callback for security check.
 */
void GoepSecCallback(const BtEvent *Event)
{
    ObStatus             status;
#if OBEX_ROLE_CLIENT == XA_ENABLED
    GoepClientObexCons  *client = 0;
    GoepClientApp       *clientApp = 0;
#endif /* OBEX_ROLE_CLIENT == XA_ENABLED */
#if OBEX_ROLE_SERVER == XA_ENABLED 
    GoepServerObexCons  *server = 0;
    GoepServerApp       *serverApp = 0;
#endif /* OBEX_ROLE_SERVER == XA_ENABLED */

    if (Event->p.secToken->incoming == FALSE) {
#if OBEX_ROLE_CLIENT == XA_ENABLED
        /* This is an outgoing connection (GOEP Client) */
        clientApp = (GoepClientApp *)Event->p.secToken->channel;
        client = &GOEC(clients)[clientApp->connId];
        if (Event->eType == BTEVENT_ACCESS_APPROVED) {
            /* Security approved, now send the OBEX Connect operation */
            status = OBEX_ClientConnect(&client->obc);
        } else {
            /* Security failed */
            client->currOp.reason = OBRC_UNAUTHORIZED;
            NotifyCurrClient(&client->obc, GOEP_EVENT_ABORTED);
        }
#endif /* OBEX_ROLE_CLIENT == XA_ENABLED */
    } 
#if OBEX_ROLE_SERVER == XA_ENABLED
    else {
        /* This is an incoming connection (GOEP Server) */
        serverApp = (GoepServerApp *)Event->p.secToken->channel;
        server = &GOES(servers)[serverApp->connId];
        if (Event->eType == BTEVENT_ACCESS_APPROVED) {
            /* Security approved - now pass up the CONTINUE event */
            NotifyCurrServer(&server->obs, GOEP_EVENT_CONTINUE);
        } else {
            /* Security failed - reject the operation, and pass up the CONTINUE event */
            status = OBEX_ServerAbort(&server->obs, OBRC_UNAUTHORIZED);
            Assert(status == OB_STATUS_SUCCESS);
            NotifyCurrServer(&server->obs, GOEP_EVENT_CONTINUE);
        }
    }
#endif /* OBEX_ROLE_SERVER == XA_ENABLED */
}
#endif /* BT_SECURITY == XA_ENABLED */
