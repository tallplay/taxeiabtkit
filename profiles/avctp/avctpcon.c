/****************************************************************************
 *
 * File:
 *     $Workfile:avctpcon.c$ for iAnywhere AV SDK, version 1.2.3
 *     $Revision:13$
 *
 * Description: This file contains the L2CAP/ACL connection state machine 
 *              for AVCTP connections.
 *             
 * Created:     Mar 12, 2004
 *
 * Copyright 2004 - 2005 Extended Systems, Inc.

 * Portions copyright 2005-2006 iAnywhere Solutions, Inc.

 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any means, 
 * or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, Inc. 
 * This work contains confidential and proprietary information of Extended 
 * Systems, Inc. which is protected by copyright, trade secret, trademark and 
 * other intellectual property rights.
 *
 ****************************************************************************/

#include "avctp.h"
#include "sys/avctpcon.h"
#include "sys/avctpi.h"

/* Function Prototypes */
static void AvctpFreeConn(AvctpConn *Conn);

/*---------------------------------------------------------------------------
 *            AvctpConnDisconnected()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Disconnected state.
 *
 */
static void AvctpConnDisconnected(AvctpConn *Conn, L2capCallbackParms *Parms)
{
    AvctpConnCallbackParms connParms;

    switch (Parms->event) {
    case L2EVENT_CONNECT_IND:

        /* Notify the application */
        Conn->remDev = Parms->aclLink;
        connParms.event = AVCTP_CONN_EVENT_CONNECT_IND;
        connParms.ptrs.remDev = Conn->remDev;
        Conn->callback(Conn, &connParms);
        break;
    case L2EVENT_DISCONNECTED:
        /* Conn disconnected, notify the app */
        AvctpFreeConn(Conn);
        break;
    }
}

/*---------------------------------------------------------------------------
 *            AvctpConnPending()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Conn Pending state.
 *
 */
static void AvctpConnConnPending(AvctpConn *Conn, L2capCallbackParms *Parms)
{
    AvctpConnCallbackParms connParms;

    switch (Parms->event) {
    case L2EVENT_CONNECTED:
        /* Connection is up */
        Conn->state = AVCTP_STATE_CONNECTED;
        Conn->remDev = Parms->aclLink;

        /* Notify the application */
        connParms.event = AVCTP_CONN_EVENT_CONNECT;
        connParms.ptrs.remDev = Conn->remDev;
        Conn->callback(Conn, &connParms);
        break;

    case L2EVENT_DISCONNECTED:
        /* Conn disconnected, notify the app */
        AvctpFreeConn(Conn);
        break;
    }
}

/*---------------------------------------------------------------------------
 *            AvctpConnConnIncoming()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Connect Incoming state.
 *
 */
static void AvctpConnConnIncoming(AvctpConn *Conn, L2capCallbackParms *Parms)
{
    AvctpConnCallbackParms connParms;

    switch (Parms->event) {
    case L2EVENT_CONNECTED:
        /* Connection is up */
        Conn->state = AVCTP_STATE_CONNECTED;
        Conn->remDev = Parms->aclLink;

        /* Notify the application */
        connParms.event = AVCTP_CONN_EVENT_CONNECT;
        connParms.ptrs.remDev = Conn->remDev;
        Conn->callback(Conn, &connParms);
        break;

    case L2EVENT_DISCONNECTED:
        /* Conn disconnected, notify the app */
        AvctpFreeConn(Conn);
        break;
    }
}

/*---------------------------------------------------------------------------
 *            AvctpConnDiscPending()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Disconnect Pending state.
 *
 */
static void AvctpConnDiscPending(AvctpConn *Conn, L2capCallbackParms *Parms)
{
    AvctpConnCallbackParms connParms;

    switch (Parms->event) {
    case L2EVENT_DISCONNECTED:
        /* Conn disconnected, notify the app */
        AvctpFreeConn(Conn);
        break;

    case L2EVENT_PACKET_HANDLED:
        /* Notify the application */
        connParms.event = AVCTP_CONN_EVENT_DATA_SENT;
        Conn->callback(Conn, &connParms);
        break;
    }
}

/*---------------------------------------------------------------------------
 *            AvctpConnDisconnected()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Disconnected state.
 *
 */
static void AvctpConnDiscIncoming(AvctpConn *Conn, L2capCallbackParms *Parms)
{
    AvctpConnCallbackParms connParms;

    switch (Parms->event) {
    case L2EVENT_DISCONNECTED:
        /* Conn disconnected, notify the app */
        AvctpFreeConn(Conn);
        break;

    case L2EVENT_PACKET_HANDLED:
        /* Notify the application */
        connParms.event = AVCTP_CONN_EVENT_DATA_SENT;
        Conn->callback(Conn, &connParms);
        break;
    }
}

/*---------------------------------------------------------------------------
 *            AvctpConnConnected()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Connected state.
 *
 */
static void AvctpConnConnected(AvctpConn *Conn, L2capCallbackParms *Parms)
{
    AvctpConnCallbackParms connParms;

    switch (Parms->event) {
    case L2EVENT_DISCONNECTED:
        /* Conn disconnected, notify the app */
        AvctpFreeConn(Conn);
        break;

    case L2EVENT_PACKET_HANDLED:
        /* Notify the application */
        connParms.event = AVCTP_CONN_EVENT_DATA_SENT;
        Conn->callback(Conn, &connParms);
        break;

    case L2EVENT_DATA_IND:
        connParms.event = AVCTP_CONN_EVENT_DATA_IND;
        connParms.dataLen = Parms->dataLen;
        connParms.ptrs.data = Parms->ptrs.data;
        Conn->callback(Conn, &connParms);
        break;
    }
}

/*---------------------------------------------------------------------------
 *            AvctpConnInitStateMachine()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initializes the state table.
 *
 */
void AvctpConnInitStateMachine(void)
{
    AVCTP(connState)[0] = AvctpConnDisconnected;
    AVCTP(connState)[1] = AvctpConnConnPending;
    AVCTP(connState)[2] = AvctpConnConnIncoming;
    AVCTP(connState)[3] = AvctpConnDiscPending;
    AVCTP(connState)[4] = AvctpConnDiscIncoming;
    AVCTP(connState)[5] = AvctpConnConnected;
}

/*---------------------------------------------------------------------------
 *            AvctpConnFindChannel()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Find the AV channel matching the remote device.
 *
 */
AvctpChannel *AvctpConnFindChannel(BtRemoteDevice *remDev)
{
    AvctpChannel    *chnl;

    /* Find the AV channel associated with this device */
    chnl = (AvctpChannel *)GetHeadList(&AVCTP(chnlList));
    while (chnl != (AvctpChannel *)&AVCTP(chnlList)) {
        if (chnl->conn.remDev == remDev) {
            /* Found it */
            break;
        }

        /* Get the next connection structure */
        chnl = (AvctpChannel *)GetNextNode(&chnl->node);
    }

    if (chnl == (AvctpChannel *)&AVCTP(chnlList)) {
        /* Channel was not found */
        chnl = 0;
    }

    return chnl;
}

/*---------------------------------------------------------------------------
 *            AvctpConnFindFreeChannel()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Find a free AV channel.
 *
 */
AvctpChannel *AvctpConnFindFreeChannel(void)
{
    AvctpChannel    *chnl;

    /* Find the AV channel associated with this device */
    chnl = (AvctpChannel *)GetHeadList(&AVCTP(chnlList));
    while (chnl != (AvctpChannel *)&AVCTP(chnlList)) {
        if (chnl->conn.state == AVCTP_STATE_DISCONNECTED) {
            /* Found it */
            break;
        }

        /* Get the next connection structure */
        chnl = (AvctpChannel *)GetNextNode(&chnl->node);
    }

    if (chnl == (AvctpChannel *)&AVCTP(chnlList)) {
        /* Channel was not found */
        chnl = 0;
    }

    return chnl;
}


/*---------------------------------------------------------------------------
 *            AvctpL2Callback()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  L2CAP callback routine.
 *
 */
void AvctpL2Callback(L2capChannelId L2ChannelId, L2capCallbackParms *Parms)
{
    AvctpChannel    *chnl;
    AvctpConn       *conn = 0;

    chnl = AvctpConnFindChannel(Parms->aclLink);
    if (chnl) {
        /* Found a matching channel */
        if (Parms->event != L2EVENT_CONNECT_IND) {
            conn = &chnl->conn;
        } else {
            /* Already connected. Fail this one. */
            (void)L2CAP_ConnectRsp(L2ChannelId,
                                   L2CONN_REJECT_NO_RESOURCES, 0);
            return;
        }
    } else {
        /* No channel found */
        if (Parms->event == L2EVENT_CONNECT_IND) {
            /* No channel, search for connection */
            chnl = AvctpConnFindFreeChannel();
            if (chnl) {
                conn = &chnl->conn;
                conn->l2ChannelId = L2ChannelId;
            } else {
                /* No channel to connect to */
                (void)L2CAP_ConnectRsp(L2ChannelId, 
                                       L2CONN_REJECT_NO_RESOURCES, 0);
                return;
            }
        } else {
            return;
        }
    }

    /* Call the state machine */
    AVCTP(connState)[conn->state](conn, Parms);
}


/*---------------------------------------------------------------------------
 *            AvctpFreeConn()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Clear the connection state and notify the app.
 *
 */
static void AvctpFreeConn(AvctpConn *Conn) 
{
    AvctpConnCallbackParms connParms;

    if (Conn->state != AVCTP_STATE_DISCONNECTED) {

        /* Clean up the state */
        Conn->state = AVCTP_STATE_DISCONNECTED;
        Conn->l2ChannelId = 0;

        /* Notify the application */
        connParms.event = AVCTP_CONN_EVENT_DISCONNECT;
        connParms.ptrs.remDev = Conn->remDev;
        Conn->remDev = 0;
        Conn->callback(Conn, &connParms);
    }
}

/*---------------------------------------------------------------------------
 *            AvctpInitConn()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initialize the connection structure.
 *
 */
void AvctpInitConn(AvctpConn *conn, AvctpConnCallback callback)
{
    /* Initialize the connection structure */
    conn->remDev = 0;
    conn->callback = callback;
    conn->state = AVCTP_STATE_DISCONNECTED;
    conn->l2ChannelId = 0;
}

/*---------------------------------------------------------------------------
 *            AvctpDisconnect()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Disconnect the L2CAP channel.
 *
 */
BtStatus AvctpDisconnect(AvctpConn *Conn)
{
    BtStatus status = BT_STATUS_SUCCESS;

    if (Conn->state != AVCTP_STATE_DISCONNECTED) {
        status = L2CAP_DisconnectReq(Conn->l2ChannelId);
        if (status == BT_STATUS_PENDING) {
            /* Disconnect Started, wait for L2CAP callback */
            Conn->state = AVCTP_STATE_DISC_PENDING;
        }
    }

    return status;
}

/*---------------------------------------------------------------------------
 *            AvctpConnect()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Create an L2CAP channel.
 *
 */
BtStatus AvctpConnect(AvctpConn *Conn, BtRemoteDevice *RemDev)
{
    BtStatus        status;

    /* Establish the L2CAP link */
    status = L2CAP_ConnectReq(&AVCTP(psm), BT_PSM_AVCTP, 
                              RemDev, 0, 
                              &Conn->l2ChannelId);

    if (status == BT_STATUS_PENDING) {
        /* Connection Initiated */
        Conn->remDev = RemDev;
        Conn->state = AVCTP_STATE_CONN_PENDING;
    }

    return status;
}

/*---------------------------------------------------------------------------
 *            AvctpConnectRsp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Respond to the incoming L2CAP connection.
 *
 */
BtStatus AvctpConnectRsp(AvctpConn *Conn, BOOL Accept)
{
    BtStatus status;

    /* Incoming connection, accept it, go to conn incoming state */
    if (Accept) {
        status = L2CAP_ConnectRsp(Conn->l2ChannelId, L2CONN_ACCEPTED, 0);
    } else {
        status = L2CAP_ConnectRsp(Conn->l2ChannelId, L2CONN_REJECT_NO_RESOURCES,
                                  0);
    }

    if (status == BT_STATUS_PENDING) {
        Conn->state = AVCTP_STATE_CONN_INCOMING;
    }

    return status;
}

/*---------------------------------------------------------------------------
 *            AvctpIsConnect()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Determine if AVCTP is connected.
 *
 */
BOOL AvctpIsConnected(AvctpConn *Conn)
{
    if (Conn->state == AVCTP_STATE_DISCONNECTED) {
        return FALSE;
    }

    return TRUE;
}
