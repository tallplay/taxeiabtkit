/****************************************************************************
 *
 * File:
 *     $Workfile:avctpmsg.c$ for iAnywhere AV SDK, version 1.2.3
 *     $Revision:13$
 *
 * Description: This file contains the Message handler.
 *             
 * Created:     Mar 23, 2004
 *
 * Copyright 2004 - 2005 Extended Systems, Inc.

 * Portions copyright 2005-2006 iAnywhere Solutions, Inc.

 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any means, 
 * or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, Inc.  
 * This work contains confidential and proprietary information of Extended 
 * Systems, Inc. which is protected by copyright, trade secret, trademark and 
 * other intellectual property rights.
 *
 ****************************************************************************/

#include "sdp.h"
#include "sys/avctpi.h"

/* Function Prototypes */
static void AvctpConnCb(AvctpConn *Conn, AvctpConnCallbackParms *parms);

/* Message handler Transmit States */
#define AVCTP_MSG_TXSTATE_IDLE            0
#define AVCTP_MSG_TXSTATE_WAIT_RSP        1

/* Message handler Receive States */
#define AVCTP_MSG_RXSTATE_BEGIN           0
#define AVCTP_MSG_RXSTATE_CONTINUE        1
#define AVCTP_MSG_RXSTATE_IGNORE          2

/*---------------------------------------------------------------------------
 *            AvtpMsgInit()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initialize the Message handler.
 *
 */
void AvtpMsgInit(AvctpChannel *chnl)
{
    AvctpInitConn(&chnl->conn, AvctpConnCb);
}

/*---------------------------------------------------------------------------
 *            AvctpMsgSendCommand()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Send a command.
 *
 */
BtStatus AvctpMsgSendCommand(AvctpChannel *chnl)
{
    BtStatus status;

    if (chnl->txState == AVCTP_MSG_TXSTATE_IDLE) {
        chnl->txState = AVCTP_MSG_TXSTATE_WAIT_RSP;
        status = AVTP_SendStart(&chnl->avtpChnl, &chnl->cmdPacket);
    }
    else {
        status = BT_STATUS_BUSY;
    }

    return status;
}

/*---------------------------------------------------------------------------
 *            AvctpMsgHandleCommand()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle a command.
 *
 */
static void AvctpMsgHandleCommand(AvctpChannel *chnl, AvtpCallbackParms *parms)
{
    AvctpCallbackParms  info;

    if (parms->rxId != SC_AV_REMOTE_CONTROL) {
        Report(("AVCTP: Invalid Profile ID\n"));
        chnl->rspPacket.msgType = AVTP_MSG_TYPE_REJECT;
        chnl->rspPacket.txId = parms->rxId;
        chnl->rspPacket.txIdSize = 2;
        chnl->rspPacket.txData = 0;
        chnl->rspPacket.txDataLen = 0;
        chnl->rspPacket.msgHdrLen = 0;
        /* There's no frame to send along; completion of this message
         * should not be indicated back to the app
         */
        chnl->curFrame = 0;
        chnl->rxState = AVCTP_MSG_RXSTATE_IGNORE;
        AssertEval(AVTP_SendStart(&chnl->avtpChnl,  &chnl->rspPacket) == 
                   BT_STATUS_PENDING);
        return;
    }

    if (parms->pktType == AVTP_PACKET_TYPE_START ||
        parms->pktType == AVTP_PACKET_TYPE_SINGLE) {

        if (parms->len < 3) {
            Report(("AVCTP: Missing parameters, disconnecting\n"));
            (void)AvctpDisconnect(&chnl->conn);
            return;
        }

        /* Move channel state into COMMAND-processing */
        chnl->state = AVCTP_CHAN_STATE_COMMAND;

        /* Decode parameters buffer */
        chnl->rx.cmdFrame.ctype = parms->data[0] & 0x0f;
        chnl->rx.cmdFrame.subunitType = parms->data[1] >> 3;
        chnl->rx.cmdFrame.subunitId = parms->data[1] & 0x07;
        chnl->rx.cmdFrame.opcode = parms->data[2];

        chnl->rx.cmdFrame.operands = parms->data + 3;  
        chnl->rx.cmdFrame.operandLen = parms->len - 3;
        chnl->rx.cmdFrame.more = (parms->pktType == AVTP_PACKET_TYPE_START);

        /* Prepare to indicate this command to the application */
        info.event = AVCTP_EVENT_COMMAND;
    }
    else {
        /* Prepare to indicate more operand data to the app */
        chnl->rx.cmdFrame.operandLen = parms->len;
        chnl->rx.cmdFrame.operands = parms->data;
        chnl->rx.cmdFrame.more = (parms->pktType == AVTP_PACKET_TYPE_CONTINUE);
        info.event = AVCTP_EVENT_OPERANDS;
    }

    /* Indicate command (or operands) to application */
    info.channel = chnl;
    info.p.cmdFrame = &chnl->rx.cmdFrame;

    chnl->callback(chnl, &info);
}

/*---------------------------------------------------------------------------
 *            AvctpMsgHandleResponse()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle a response.
 *
 */
static void AvctpMsgHandleResponse(AvctpChannel *chnl, AvtpCallbackParms *parms)
{
    AvctpCallbackParms  info;

    if (parms->rxId != SC_AV_REMOTE_CONTROL) {
        Report(("AVCTP: Invalid PID, disconnecting\n"));
        (void)AvctpDisconnect(&chnl->conn);
        return;
    }

    if (parms->pktType == AVTP_PACKET_TYPE_START ||
        parms->pktType == AVTP_PACKET_TYPE_SINGLE) {

        if (parms->len < 3) {
            Report(("AVCTP: Missing parameters, disconnecting\n"));
            (void)AvctpDisconnect(&chnl->conn);
            return;
        }

        /* Decode parameters buffer */
        chnl->rx.rspFrame.response = parms->data[0] & 0x0f;
        chnl->rx.rspFrame.subunitType = parms->data[1] >> 3;
        chnl->rx.rspFrame.subunitId = parms->data[1] & 0x07;
        chnl->rx.rspFrame.opcode = parms->data[2];

        chnl->rx.rspFrame.operands = parms->data + 3;
        chnl->rx.rspFrame.operandLen = parms->len - 3;
        chnl->rx.rspFrame.more = (parms->pktType == AVTP_PACKET_TYPE_START);
        
        /* Prepare to indicate response to the application */
        info.event = AVCTP_EVENT_RESPONSE;
    }
    else {        
        /* Prepare to indicate additional operands to the application */
        chnl->rx.rspFrame.operands = parms->data;
        chnl->rx.rspFrame.operandLen = parms->len;
        chnl->rx.rspFrame.more = (parms->pktType == AVTP_PACKET_TYPE_CONTINUE);
        info.event = AVCTP_EVENT_OPERANDS;
    }

    /* Indicate this event to the application */
    info.channel = chnl;
    info.p.rspFrame = &chnl->rx.rspFrame;
    chnl->callback(chnl, &info);

    if (chnl->rx.rspFrame.response == AVCTP_RESPONSE_INTERIM &&
        chnl->rx.rspFrame.opcode == AVCTP_OPCODE_VENDOR_DEPENDENT) {
        chnl->txState = AVCTP_MSG_TXSTATE_WAIT_RSP;
    }
}

/*---------------------------------------------------------------------------
 *            AvtpMsgHandleReject()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle a reject.
 *
 */
static void AvctpMsgHandleReject(AvctpChannel *chnl, AvtpCallbackParms *parms)
{
    AvctpCallbackParms  info;

    if (parms->pktType == AVTP_PACKET_TYPE_START ||
        parms->pktType == AVTP_PACKET_TYPE_SINGLE) {
        if (parms->len >= 3) {
            chnl->rx.rspFrame.response = parms->data[0] & 0x0f;
            chnl->rx.rspFrame.subunitType = parms->data[1] >> 3;
            chnl->rx.rspFrame.subunitId = parms->data[1] & 0x07;
            chnl->rx.rspFrame.opcode = parms->data[2];
            chnl->rx.rspFrame.operandLen = ((parms->packetsLeft + 1) * (L2CAP_MTU - 1)) - 6;
            if (parms->pktType == AVTP_PACKET_TYPE_SINGLE) chnl->rx.rspFrame.operandLen++;
            chnl->rx.rspFrame.operands = parms->data + 3;
        }

        /* Indicate the response to the application */
        info.event = AVCTP_EVENT_REJECT;
        info.channel = chnl;
        info.p.rspFrame = &chnl->rx.rspFrame;
        chnl->callback(chnl, &info);
    }
    else {
        chnl->rx.rspFrame.operands = parms->data;
        chnl->rx.rspFrame.operandLen = parms->len;
    }
    
    if (chnl->rx.rspFrame.operandLen) {
        /* Indicate the operands to the application */
        info.event = AVCTP_EVENT_OPERANDS;
        info.channel = chnl;
        info.p.rspFrame = &chnl->rx.rspFrame;
        chnl->callback(chnl, &info);
    }
}

/*---------------------------------------------------------------------------
 *            AvctpMsgCallback()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Message channel callback.
 *
 */
static void AvctpMsgCb(AvtpChannel *avtpChnl, AvtpCallbackParms *parms)
{
    AvctpChannel *chnl = ContainingRecord(avtpChnl, AvctpChannel, avtpChnl);
    AvctpCallbackParms info;

    switch (parms->event) {
    case AVTP_EVENT_TX_DONE:
        Report(("AVCTP: Message Sent\n"));
        if (chnl->curFrame) {
            info.status = BT_STATUS_SUCCESS;
            info.event = AVCTP_EVENT_TX_DONE;
            info.channel = chnl;
            info.p.cmdFrame = chnl->curFrame;
            chnl->callback(chnl, &info);
        }
        chnl->rxState = AVCTP_MSG_RXSTATE_BEGIN;
        break;

    case AVTP_EVENT_TX_ERROR:
        Report(("AVCTP: Error Sending Message\n"));
        if (chnl->curFrame) {
            chnl->txState = AVCTP_MSG_TXSTATE_IDLE;
            chnl->rxState = AVCTP_MSG_RXSTATE_BEGIN;
            info.status = BT_STATUS_FAILED;
            info.event = AVCTP_EVENT_TX_DONE;
            info.channel = chnl;
            info.p.cmdFrame = chnl->curFrame;
            chnl->callback(chnl, &info);
        }
        break;

    case AVTP_EVENT_TX_TIMEOUT:
        Report(("AVCTP: Message Timed out\n"));
        if (chnl->curFrame) {
            chnl->txState = AVCTP_MSG_TXSTATE_IDLE;
            chnl->rxState = AVCTP_MSG_RXSTATE_BEGIN;
            info.status = BT_STATUS_TIMEOUT;
            info.event = AVCTP_EVENT_CMD_TIMEOUT;
            info.channel = chnl;
            info.p.cmdFrame = chnl->curFrame;
            chnl->callback(chnl, &info);
        }
        break;

    case AVTP_EVENT_RX_IND:
        if (chnl->rxState != AVCTP_MSG_RXSTATE_IGNORE) {
            switch (parms->msgType) {
            case AVTP_MSG_TYPE_COMMAND:
                Report(("AVCTP: Command Received\n"));
                AvctpMsgHandleCommand(chnl, parms);
                break;

            case AVTP_MSG_TYPE_ACCEPT:
                Report(("AVCTP: Response Received\n"));
                if (chnl->txState != AVCTP_MSG_TXSTATE_WAIT_RSP) {
                    chnl->rxState = AVCTP_MSG_RXSTATE_IGNORE;
                    break;
                }
                if (parms->packetsLeft == 0) {
                    chnl->txState = AVCTP_MSG_TXSTATE_IDLE;
                    chnl->rxState = AVCTP_MSG_RXSTATE_BEGIN;
                }
                AvctpMsgHandleResponse(chnl, parms);
                break;

            case AVTP_MSG_TYPE_REJECT:
                Report(("AVCTP: Reject Received\n"));
                if (chnl->txState != AVCTP_MSG_TXSTATE_WAIT_RSP) {
                    chnl->rxState = AVCTP_MSG_RXSTATE_IGNORE;
                    break;
                }
                if (parms->packetsLeft == 0) {
                    chnl->txState = AVCTP_MSG_TXSTATE_IDLE;
                    chnl->rxState = AVCTP_MSG_RXSTATE_BEGIN;
                }
                AvctpMsgHandleReject(chnl, parms);
                break;

            default:
                break;
            }
        }
        else if (parms->packetsLeft == 0) {
            chnl->rxState = AVCTP_MSG_RXSTATE_BEGIN;
        }
        break;

    case AVTP_EVENT_RX_UNKNOWN_MESSAGE:
    case AVTP_EVENT_RX_UNKNOWN_PACKET:
    case AVTP_EVENT_RX_BAD_TRANS_ID:
        Report(("AVCTP: Received an invalid message packet\n"));
        (void)AvctpDisconnect(&chnl->conn);
        break;

    case AVTP_EVENT_RX_BUFFER_UNDERRUN:
    case AVTP_EVENT_RX_BUFFER_OVERRUN:
        Report(("AVCTP: Invalid Response Length\n"));
        (void)AvctpDisconnect(&chnl->conn);
        break;

    default:
        break;
    }

    if (chnl->rxState != AVCTP_MSG_RXSTATE_IGNORE) {
        if ((parms->event != AVTP_EVENT_TX_DONE) && parms->packetsLeft) {
            chnl->rxState = AVCTP_MSG_RXSTATE_CONTINUE;
        }
        else {
            chnl->rxState = AVCTP_MSG_RXSTATE_BEGIN;
        }
    }
}

/*---------------------------------------------------------------------------
 *            AvctpConnCb()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Connection callback.
 *
 */
void AvctpConnCb(AvctpConn *Conn, AvctpConnCallbackParms *Parms)
{
    AvctpChannel       *chnl = ContainingRecord(Conn, AvctpChannel, conn);
    AvctpCallbackParms  info;

    switch (Parms->event) {
    case AVCTP_CONN_EVENT_CONNECT:
        Report(("AVCTP:  Message Channel Connected\n"));
        chnl->rxState = AVCTP_MSG_RXSTATE_BEGIN;
        chnl->txState = AVCTP_MSG_TXSTATE_IDLE;
        AVTP_InitChannel(&chnl->avtpChnl, chnl->conn.l2ChannelId, 
                          AvctpMsgCb, AVCTP_RTX_TIMEOUT);

        /* Indicate the connection to the application */
        info.event = AVCTP_EVENT_CONNECT;
        info.channel = chnl;
        info.p.remDev = Parms->ptrs.remDev;
        chnl->callback(chnl, &info);
        break;

    case AVCTP_CONN_EVENT_CONNECT_IND:
        /* Indicate the incoming connection to the application */
        info.event = AVCTP_EVENT_CONNECT_IND;
        info.channel = chnl;
        info.p.remDev = Parms->ptrs.remDev;
        chnl->callback(chnl, &info);
        break;

    case AVCTP_CONN_EVENT_DISCONNECT:
        Report(("AVCTP:  Message Channel Disconnected\n"));
        AVTP_DeinitChannel(&chnl->avtpChnl);

        chnl->rxState = AVCTP_MSG_RXSTATE_BEGIN;
        chnl->txState = AVCTP_MSG_TXSTATE_IDLE;

        /* Indicate the connection to the application */
        info.event = AVCTP_EVENT_DISCONNECT;
        info.channel = chnl;
        info.p.remDev = Parms->ptrs.remDev;
        chnl->callback(chnl, &info);
        break;

    case AVCTP_CONN_EVENT_DATA_SENT:
        Report(("AVCTP:  Message Data Sent\n"));
        AVTP_SendContinue(&chnl->avtpChnl);
        break;

    case AVCTP_CONN_EVENT_DATA_IND:
        Report(("AVCTP: Message Data Received = %d\n", Parms->dataLen));
        AVTP_Receive(&chnl->avtpChnl, Parms->ptrs.data, Parms->dataLen, 2);
        break;
    }
}
