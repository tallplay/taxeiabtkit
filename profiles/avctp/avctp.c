/****************************************************************************
 *
 * File:
 *     $Workfile:avctp.c$ for iAnywhere AV SDK, version 1.2.3
 *     $Revision:14$
 *
 * Description: This file contains public API for the Audio/Video
 *     Remote Control Transport Protocol (AVCTP).
 *             
 * Created:     Mar 10, 2004
 *
 * Copyright 2004 - 2005 Extended Systems, Inc.

 * Portions copyright 2005-2006 iAnywhere Solutions, Inc.

 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any means, 
 * or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, Inc. 
 * This work contains confidential and proprietary information of Extended 
 * Systems, Inc. which is protected by copyright, trade secret, trademark and 
 * other intellectual property rights.
 *
 ****************************************************************************/

#include "sdp.h"
#include "sys/avctpi.h"
#include "sys/avctpcon.h"

#if XA_CONTEXT_PTR == XA_ENABLED
/* The stack context is accessed through a pointer. Normally in this case
 * the stack context would be dynamically allocated. For testing purposes
 * the pointer points to a global structure.
 */
BtAvctpContext  avTmp;
BtAvctpContext *avctpContext = &avTmp;
#else /* XA_CONTEXT_PTR == XA_ENABLED */
BtAvctpContext avctpContext;
#endif /* XA_CONTEXT_PTR */

/*---------------------------------------------------------------------------
 * AVCTP_Init()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initialize the AVCTP SDK.  This function should only be called
 *            once, normally at initialization time.  The calling of this 
 *            function can be specified in overide.h using the XA_LOAD_LIST 
 *            macro (i.e. #define XA_LOAD_LIST XA_MODULE(AV)).
 *
 * Return:    (See header file)
 */
BOOL AVCTP_Init(void)
{
    BOOL status = TRUE;

    /* Initialize Memory */
    status = AvctpAlloc();

    /* Initialize the connection state machine */
    AvctpConnInitStateMachine();

    /* Register AVCTP with L2CAP */
    AVCTP(psm).callback = AvctpL2Callback;
    AVCTP(psm).psm = BT_PSM_AVCTP;
    AVCTP(psm).localMtu = L2CAP_MAXIMUM_MTU;
    AVCTP(psm).minRemoteMtu = L2CAP_MINIMUM_MTU;

#if L2CAP_FLOW_CONTROL == XA_ENABLED
    AVCTP(psm).inLinkMode = AVCTP(psm).outLinkMode = L2MODE_BASIC;
#endif /* L2CAP_FLOW_CONTROL == XA_ENABLED */

    if (L2CAP_RegisterPsm(&(AVCTP(psm))) != BT_STATUS_SUCCESS) {
        /* Unable to register */
        Report(("AVCTP:  Unable to register with L2CAP.\n"));
        status = FALSE;
    }

    /* Initialized lists */
    InitializeListHead(&AVCTP(chnlList));

    return status;
}


#if L2CAP_DEREGISTER_FUNC == XA_ENABLED
/*---------------------------------------------------------------------------
 * AVCTP_Deinit()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Deinitializes the AVCTP module. Init() must later be called
 *            again to use any AVCTP function call. This function does no
 *            error checking so do NOT call when AVCTP is in use.
 *             
 * Return:    (See header file)
 */
void AVCTP_Deinit(void)
{
    (void)L2CAP_DeregisterPsm(&(AVCTP(psm)));
}
#endif /* L2CAP_DEREGISTER_FUNC == XA_ENABLED */

/*---------------------------------------------------------------------------
 * AVCTP_Register()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Registers an application callback to create/receive AVCTP 
 *            connections.  This function must be called before any other 
 *            AVCTP functions.
 *
 * Return:    (See header file)
 */
BtStatus AVCTP_Register(AvctpChannel *chnl, AvctpCallback callback)
{
    BtStatus   status = BT_STATUS_SUCCESS;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, chnl != 0);
    CheckUnlockedParm(BT_STATUS_INVALID_PARM, callback != 0);

    OS_LockStack();

    /* Register this connection in the list */
    if (!IsNodeOnList(&AVCTP(chnlList), &chnl->node)) {

        /* Clear the connection structure */
        OS_MemSet((U8*)chnl, 0, sizeof(AvctpChannel));

        /* Register the Avctp connection */
        chnl->callback = callback;

        /* Initialize the message handler */
        AvtpMsgInit(chnl);

        /* Insert the channel on the list */
        InsertTailList(&AVCTP(chnlList), &chnl->node);
    } else {

        /* Already registered */
        status = BT_STATUS_IN_USE;
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 * AVCTP_Deregister()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  De-registers the AVCTP callback. After making this call
 *            successfully, the callback specified in AVCTP_Register will
 *            receive no further events.
 *
 * Return:    (See header file)
 */
BtStatus AVCTP_Deregister(AvctpChannel *chnl)
{
    BtStatus   status = BT_STATUS_SUCCESS;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, chnl != 0);

    OS_LockStack();

    if (IsNodeOnList(&AVCTP(chnlList), &chnl->node)) {
        if (!AvctpIsConnected(&chnl->conn)) {
            /* No connection is up, remove from list */
            RemoveEntryList(&chnl->node);
        } else {
            /* A connection is up */
            status = BT_STATUS_IN_USE;
        }
    } else {
        /* Connection is not registered */
        status = BT_STATUS_NOT_FOUND;
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 * AVCTP_Connect()
 *---------------------------------------------------------------------------
 *
 * Synopsis: Connect to the remote device.
 *
 * Return:   (See header file)
 */
BtStatus AVCTP_Connect(AvctpChannel *chnl, BtRemoteDevice *remDev)
{
    BtStatus status = BT_STATUS_NOT_FOUND;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, chnl != 0);
    CheckUnlockedParm(BT_STATUS_INVALID_PARM, remDev != 0);

    OS_LockStack();

    /* See if the chnl is registered */
    if (IsNodeOnList(&AVCTP(chnlList), &chnl->node)) {
        if (!AvctpIsConnected(&chnl->conn)) {
            /* Start the connection */
            status = AvctpConnect(&chnl->conn, remDev);
        } else {
            /* Already connected */
            status = BT_STATUS_IN_USE;
        }
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 * AVCTP_ConnectRsp()
 *---------------------------------------------------------------------------
 *
 * Synopsis: Respond to a connect request from the remote device.
 *
 * Return:   (See header file)
 */
BtStatus AVCTP_ConnectRsp(AvctpChannel *Chnl, BOOL Accept)
{
    BtStatus status = BT_STATUS_NOT_FOUND;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Chnl != 0);

    OS_LockStack();

    /* See if the Chnl is registered */
    if (IsNodeOnList(&AVCTP(chnlList), &Chnl->node)) {
        if (!AvctpIsConnected(&Chnl->conn)) {
            /* Start the connection */
            status = AvctpConnectRsp(&Chnl->conn, Accept);
        } else {
            /* Already connected */
            status = BT_STATUS_BUSY;
        }
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 * AVCTP_Disconnect()
 *---------------------------------------------------------------------------
 *
 * Synopsis: Disconnect from the remote device.
 *
 * Return:   (See header file)
 */
BtStatus AVCTP_Disconnect(AvctpChannel *chnl)
{
    BtStatus status = BT_STATUS_NOT_FOUND;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, chnl != 0);

    OS_LockStack();

    /* See if the chnl is registered */
    if (IsNodeOnList(&AVCTP(chnlList), &chnl->node)) {
        if (AvctpIsConnected(&chnl->conn)) {
            /* Disconnect */
            status = AvctpDisconnect(&chnl->conn);
        } else {
            /* No connection exists */
            status = BT_STATUS_NO_CONNECTION;
        }
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 * AVCTP_SendCommand()
 *---------------------------------------------------------------------------
 *
 * Synopsis: Send a command to the target.
 *
 * Return:   (See header file)
 */
BtStatus AVCTP_SendCommand(AvctpChannel *chnl, AvctpCmdFrame *cmdFrame)
{
    BtStatus status = BT_STATUS_NOT_FOUND;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, chnl != 0);

    OS_LockStack();

    if (IsNodeOnList(&AVCTP(chnlList), &chnl->node)) {
        if (chnl->state != AVCTP_CHAN_STATE_COMMAND) {
            chnl->curFrame = cmdFrame;
            chnl->cmdPacket.msgType = AVTP_MSG_TYPE_COMMAND;
            chnl->cmdPacket.txId = SC_AV_REMOTE_CONTROL;
            chnl->cmdPacket.txIdSize = 2;
            chnl->cmdPacket.msgHdrLen = cmdFrame->headerLen + 3;
            chnl->cmdPacket.msgHdr[0] = cmdFrame->ctype & 0x0f;
            chnl->cmdPacket.msgHdr[1] = cmdFrame->subunitType << 3;
            chnl->cmdPacket.msgHdr[1] += cmdFrame->subunitId;
            chnl->cmdPacket.msgHdr[2] = cmdFrame->opcode;
            if (cmdFrame->headerLen > 0) {
                OS_MemCopy(&chnl->cmdPacket.msgHdr[3], 
                           cmdFrame->header, 
                           cmdFrame->headerLen);
            }
            chnl->cmdPacket.txDataLen = cmdFrame->operandLen;
            chnl->cmdPacket.txData = cmdFrame->operands;

            status = AvctpMsgSendCommand(chnl);
        } else {
            status = BT_STATUS_FAILED;
        }
    } else {
        status = BT_STATUS_NOT_FOUND;
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 * AVCTP_SendResponse()
 *---------------------------------------------------------------------------
 *
 * Synopsis: Send a response to the controller.
 *
 * Return:   (See header file)
 */
BtStatus AVCTP_SendResponse(AvctpChannel *chnl, AvctpRspFrame *rspFrame)
{
    BtStatus status = BT_STATUS_FAILED;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, chnl != 0);

    OS_LockStack();

    if (IsNodeOnList(&AVCTP(chnlList), &chnl->node)) {
        if (rspFrame->response == AVCTP_RESPONSE_INTERIM &&
            rspFrame->opcode != AVCTP_OPCODE_VENDOR_DEPENDENT) {
            status = BT_STATUS_FAILED;
        } else if (chnl->state == AVCTP_CHAN_STATE_COMMAND) {
            if (rspFrame->response != AVCTP_RESPONSE_INTERIM) {
                chnl->state = AVCTP_CHAN_STATE_OPEN;
            }
            chnl->curFrame = rspFrame;
            chnl->rspPacket.msgType = AVTP_MSG_TYPE_ACCEPT;
            chnl->rspPacket.txId = SC_AV_REMOTE_CONTROL;
            chnl->rspPacket.txIdSize = 2;
            chnl->rspPacket.msgHdrLen = rspFrame->headerLen + 3;
            chnl->rspPacket.msgHdr[0] = rspFrame->response & 0x0f;
            chnl->rspPacket.msgHdr[1] = rspFrame->subunitType << 3;
            chnl->rspPacket.msgHdr[1] += rspFrame->subunitId;
            chnl->rspPacket.msgHdr[2] = rspFrame->opcode;
            if (rspFrame->headerLen > 0) {
                OS_MemCopy(&chnl->rspPacket.msgHdr[3], 
                           rspFrame->header, 
                           rspFrame->headerLen);
            }
            chnl->rspPacket.txDataLen = rspFrame->operandLen;
            chnl->rspPacket.txData = rspFrame->operands;

            status = AVTP_SendStart(&chnl->avtpChnl, &chnl->rspPacket);
        } else {
            status = BT_STATUS_FAILED;
        }
    } else {
        status = BT_STATUS_NOT_FOUND;
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 * AVCTP_RemoteDevice()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Returns a pointer to the current remote device.
 *
 * Return:    void
 */
BtRemoteDevice * AVCTP_RemoteDevice(AvctpChannel *chnl)
{
    BtRemoteDevice *remDev;

    if (AvctpIsConnected(&chnl->conn)) {
        remDev = chnl->conn.remDev;
    } else {
        remDev = 0;
    }

    return remDev;
}

/*---------------------------------------------------------------------------
 * AvctpAlloc()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initializes the global context for the AV profile.  This 
 *            function can be replaced if necessary.
 *
 * Return:    void
 */
BOOL AvctpAlloc(void)
{
    U8* ptr; 

    /* Fill memory with 0. */
#if XA_CONTEXT_PTR == XA_ENABLED
    ptr = (U8*)avctpContext;
#else
    ptr = (U8*)&avctpContext;
#endif /* XA_CONTEXT_PTR */

    OS_MemSet(ptr, 0, (U32)sizeof(BtAvctpContext));

    return TRUE;
}

