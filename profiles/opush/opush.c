/***************************************************************************
 *
 * File:
 *     $Workfile:opush.c$ for XTNDAccess Blue SDK, Version 1.3
 *     $Revision:44$
 *
 * Description:
 *     This file contains the functions that comprise the Object Push
 *     Profile implementation.
 *
 * Created:
 *     September 4, 2000
 *
 * Copyright 2000-2005 Extended Systems, Inc.  ALL RIGHTS RESERVED.
 *
 * Unpublished Confidential Information of Extended Systems, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of Extended Systems, Inc.
 * 
 * Use of this work is governed by a license granted by Extended Systems, 
 * Inc.  This work contains confidential and proprietary information of 
 * Extended Systems, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include <osapi.h>
#include <opush.h>
#include <sys/goepext.h>

#if BT_STACK == XA_ENABLED
/****************************************************************************
 *
 * ROMable data
 *
 ****************************************************************************/

#if OBEX_ROLE_SERVER == XA_ENABLED
/*---------------------------------------------------------------------------
 *
 * SDP objects registered by Local Object Push Server.
 * They are the Service Class ID, the Protocol Descriptor List and the
 * supported formats list.
 */
static const U8 OPushServClassId[] = {
    SDP_ATTRIB_HEADER_8BIT(3),          /* Data Element Sequence, 3 bytes */
    /* This is a list of one element of type UUID */
    SDP_UUID_16BIT(SC_OBEX_OBJECT_PUSH) /* Object Push UUID in Big Endian */
};

/* Value of the Supported Formats List of the Object Push profile.
 */
static const U8 OPushServSupportedFormats[] = {
    SDP_ATTRIB_HEADER_8BIT(2),  /* Data Element Sequence, 2 bytes */
    SDP_UINT_8BIT(0xFF)         /* 'Any type of object' value */
};

/*
 * * OPTIONAL * Language BaseId List (goes with the service name).
 */
static const U8 OPushLangBaseIdList[] = {
    SDP_ATTRIB_HEADER_8BIT(9),  /* Data Element Sequence, 9 bytes */
    SDP_UINT_16BIT(0x656E),     /* English Language */
    SDP_UINT_16BIT(0x006A),     /* UTF-8 encoding */
    SDP_UINT_16BIT(0x0100)      /* Primary language base Id */
};

/*
 * * OPTIONAL * Object push service name (digianswer requires it).
 */
static const U8 OPushServServiceName[] = {
    SDP_TEXT_8BIT(17),          /* Null terminated text string, 17 bytes */
    'O', 'B', 'E', 'X', ' ', 'O', 'b', 'j', 'e', 'c', 't', 
    ' ', 'P', 'u', 's', 'h', '\0'
};

/*---------------------------------------------------------------------------
 * Object push Public Browse Group.
 */
static const U8 OPushBrowseGroup[] = {
    SDP_ATTRIB_HEADER_8BIT(3),          /* 3 bytes */
    SDP_UUID_16BIT(SC_PUBLIC_BROWSE_GROUP)  /* Public Browse Group */
};

/* Object Push Servers attributes.
 *
 * This is a ROM template for the RAM structure used to register the
 * Object Push Profile's SDP record.
 */
static const SdpAttribute OPushSdpAttributes[] = {
    /* Object push service class ID List attribute */
    SDP_ATTRIBUTE(AID_SERVICE_CLASS_ID_LIST, OPushServClassId), 
    /* OBEX protocol descriptor list attribute */
    {0, 0, 0, 0 },
    /* Public Browse Group Service */
    SDP_ATTRIBUTE(AID_BROWSE_GROUP_LIST, OPushBrowseGroup), 
    /* Language base id (Optional: Used with service name) */
    SDP_ATTRIBUTE(AID_LANG_BASE_ID_LIST, OPushLangBaseIdList),
    /* Object push service name (Optional) */
    SDP_ATTRIBUTE((AID_SERVICE_NAME + 0x0100), OPushServServiceName),
    /* Object push supported formats list */
    SDP_ATTRIBUTE(AID_SUPPORTED_FORMATS_LIST, OPushServSupportedFormats)
};
#endif /* OBEX_ROLE_SERVER == XA_ENABLED */

#if OBEX_ROLE_CLIENT == XA_ENABLED
/*-------------------------------------------------------------------------
 *
 * SDP query info
 */
/* Service search attribute request for OBEX Object Push protocol descriptor
 * list. The service search pattern is very specific. It contains the UUIDs
 * for OBEX Object push, L2CAP, and RFCOMM in case there is a version of
 * of OBEX object push running over another protocol.
 */
static const U8 OPushServiceSearchAttribReq[] = {
    /* First parameter is the search pattern in data element format. It
     * is a list of 3 UUIDs. 
     */
    SDP_ATTRIB_HEADER_8BIT(9),             /* Data Element Sequence, 9 bytes */ 
    /* The first UUID in the list OBEX object push */
    SDP_UUID_16BIT(SC_OBEX_OBJECT_PUSH),   /* Object push UUID in Big Endian */
    /* The second UUID is L2CAP */
    SDP_UUID_16BIT(PROT_L2CAP),            /* L2CAP UUID in Big Endian */
    /* The third UUID is RFCOMM */
    SDP_UUID_16BIT(PROT_RFCOMM),           /* UUID for RFCOMM in Big Endian */

    /* The second parameter is the maximum number of bytes that can be
     * be received for the attribute list
     */
    0x00, 0x64,                 /* Max number of bytes for attribute is 100 */

    /* The third parameter is the actual attribute ID list which is a data
     * element sequence. We only want one attribute which is the protocol
     * descriptor
     */
    SDP_ATTRIB_HEADER_8BIT(3),  /* Data Element Sequence, 3 bytes */
    SDP_UINT_16BIT(0x0004)     /* Value of protocol descriptor list ID */
};
#endif /* OBEX_ROLE_CLIENT == XA_ENABLED */
#endif /* BT_STACK == XA_ENABLED */

/****************************************************************************
 *
 * RAM data
 *
 ****************************************************************************/
#if XA_CONTEXT_PTR == XA_ENABLED
static OPushExtensionData   temp;
static OPushExtensionData  *OPush = &temp;
#else /* XA_CONTEXT_PTR == XA_ENABLED */
static OPushExtensionData   OPush;
#endif /* XA_CONTEXT_PTR == XA_ENABLED */


/*****************************************************************************
 *
 * Object Push Profile Public API's
 *
 ****************************************************************************/

/*---------------------------------------------------------------------------
 *            OPUSH_Init()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initialize the OPUSH component.  This must be the first OPUSH 
 *            function called by the application layer, or if multiple 
 *            OPUSH applications exist, this function should be called
 *            at system startup (see XA_LOAD_LIST in config.h).  GOEP and 
 *            OBEX must also be initialized separately.
 *
 * Return:    TRUE or FALSE
 *
 */
BOOL OPUSH_Init(void)
{
    OS_LockStack();

#if XA_CONTEXT_PTR == XA_ENABLED
    OS_MemSet((U8 *)OPush, 0, sizeof(OPushExtensionData)); 
#else /* XA_CONTEXT_PTR == XA_ENABLED */
    OS_MemSet((U8 *)&OPush, 0, sizeof(OPushExtensionData)); 
#endif /* XA_CONTEXT_PTR == XA_ENABLED */

    /* OPUSH initialized */
    OPP(initialized) = TRUE;
    
    OS_UnlockStack();
    return TRUE;
}

#if BT_STACK == XA_ENABLED
/*****************************************************************************
 *
 * API for Managing the Object Push Profile SDP Database
 *
 ****************************************************************************/

#if OBEX_ROLE_SERVER == XA_ENABLED
/*---------------------------------------------------------------------------
 *            OPUSH_AddServiceRecord
 *---------------------------------------------------------------------------
 *
 * Synopsis: Register all mandatory SDP records for the Object Push Service.
 *           This function is called automatically by the extended API.
 *
 * Return:   BtStatus
 *
 */
BtStatus OPUSH_AddServiceRecord(GoepServerApp *Server)
{
    BtStatus    status = BT_STATUS_INVALID_PARM;

    OS_LockStack();

#if XA_ERROR_CHECK == XA_ENABLED
    if (!Server) {
        goto Error;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(Server);

    if (OPP(sdp)[Server->connId].registered) {
        status = BT_STATUS_SUCCESS;
        goto Error;
    }

    /* Create the SDP entry for the OBEX Object Push server.
     * This is done by copying the ROM templates into RAM structures.
     */
    Assert(sizeof(OPushSdpAttributes) == sizeof(OPP(sdp)[Server->connId].attributes));
    OS_MemCopy( (U8 *)&OPP(sdp)[Server->connId].attributes, 
                (U8 *)&OPushSdpAttributes, 
                sizeof(OPushSdpAttributes) );

    OBEX_ServerGetSdpProtocolDescList(&GOES(servers)[Server->connId].obs.trans.ObexServerBtTrans, &OPP(sdp)[Server->connId].attributes[1], 1);

    OPP(sdp)[Server->connId].record.attribs = OPP(sdp)[Server->connId].attributes;
    OPP(sdp)[Server->connId].record.num = 6;
    OPP(sdp)[Server->connId].record.classOfDevice = COD_OBJECT_TRANSFER;

    status = SDP_AddRecord(&OPP(sdp)[Server->connId].record);
    if (status == BT_STATUS_SUCCESS)
        OPP(sdp)[Server->connId].registered = TRUE;

Error:
    OS_UnlockStack();
    return status;
}

/*---------------------------------------------------------------------------
 *            OPUSH_RemoveServiceRecord
 *---------------------------------------------------------------------------
 *
 * Synopsis: Deregisters SDP records for the Object Push Service.
 *           This function is called automatically by the extended API.
 *
 * Return:   BtStatus 
 *
 */
BtStatus OPUSH_RemoveServiceRecord(GoepServerApp *Server)
{
    BtStatus status = BT_STATUS_FAILED;

    OS_LockStack();

#if XA_ERROR_CHECK == XA_ENABLED
    if (!Server) {
        status = BT_STATUS_INVALID_PARM;
        goto Error;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(Server);

    if (OPP(sdp)[Server->connId].registered) {
        /* Remove Servers OBEX SDP entry */
        status = SDP_RemoveRecord(&OPP(sdp)[Server->connId].record);
        if (status == BT_STATUS_SUCCESS)
            OPP(sdp)[Server->connId].registered = FALSE;
    }

Error:
    OS_UnlockStack();
    return status;
}
#endif /* OBEX_ROLE_SERVER == XA_ENABLED */
    
#if OBEX_ROLE_CLIENT == XA_ENABLED
/*---------------------------------------------------------------------------
 *            OPUSH_BuildServiceQuery
 *---------------------------------------------------------------------------
 *
 * Synopsis: Loads the Object Push Service Query into the specified pointers.
 *           This function is called automatically by the extended API.
 *
 * Return:   void
 *
 */
void OPUSH_BuildServiceQuery(const U8 **QueryPtr, U16 *QueryLen, SdpQueryType *QueryType)
{
    OS_LockStack();

    Assert(QueryPtr && QueryLen && QueryType);

    *QueryPtr = OPushServiceSearchAttribReq;
    *QueryLen = sizeof(OPushServiceSearchAttribReq);
    *QueryType = BSQT_SERVICE_SEARCH_ATTRIB_REQ;

    OS_UnlockStack();
}
#endif /* OBEX_ROLE_CLIENT == XA_ENABLED */
#endif /* BT_STACK == XA_ENABLED */

#if OPUSH_EXPANDED_API == XA_ENABLED
/*****************************************************************************
 *
 * API for the Object Push Profile Client & Server
 *
 ****************************************************************************/

#if OBEX_ROLE_SERVER == XA_ENABLED
static void OPushServerCallback(struct _GoepServerEvent *);
/*---------------------------------------------------------------------------
 *            OPUSH_RegisterServer
 *---------------------------------------------------------------------------
 *
 * Synopsis: Registers the object push server with the GOEP multiplexor.
 *           This includes registering the SDP records for the Object Push
 *           Service.
 *
 * Return:   ObStatus
 *
 */
ObStatus OPUSH_RegisterServer(GoepServerApp *Server, const ObStoreFuncTable *obStoreFuncs)
{
    U8          freeId;
    ObStatus    status = OB_STATUS_BUSY;

    OS_LockStack();

#if XA_ERROR_CHECK == XA_ENABLED
    if ((!Server) || (!obStoreFuncs)) {
        status = OB_STATUS_INVALID_PARM;
        goto Error;
    }

    if (OPP(initialized) != TRUE) {
        /* OPUSH not initialized */
        status = OB_STATUS_FAILED;
        goto Error;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(Server && obStoreFuncs);

    Server->type = GOEP_PROFILE_OPUSH;
    Server->appParent = OPushServerCallback;

#if OBEX_SERVER_CONS_SIZE > 0
    Server->numTargets = 0;
#endif /* OBEX_SERVER_CONS_SIZE > 0 */

    freeId = GOEP_GetConnectionId(GOEP_SERVER, GOEP_PROFILE_OPUSH);
    if (freeId != 0xFF) {
        /* We can multiplex OPUSH over an existing connection, so we'll do it */
        Server->connFlags = GOEP_MUX_CONN;
        Server->connId = freeId;
    }
    else {
        /* We have to make a new connection for the OPUSH profile */
        Server->connFlags = GOEP_NEW_CONN;
        /* Initialize the connId - filled in later */
        Server->connId = 0;
    }

    /* Register SDP Server with GOEP mux */
    status = GOEP_RegisterServer(Server, obStoreFuncs);
    if (status != OB_STATUS_SUCCESS) {
        goto Error;
    }

#if BT_STACK == XA_ENABLED
    /* Add Object Push Service record to SDP database */
    status = OPUSH_AddServiceRecord(Server);
#endif /* BT_STACK == XA_ENABLED */

#if OBEX_DEINIT_FUNCS == XA_ENABLED
    if (status != OB_STATUS_SUCCESS) {
        GOEP_DeregisterServer(Server);
        goto Error;
    }
#else /* OBEX_DEINIT_FUNCS == XA_ENABLED */
    Assert(status == OB_STATUS_SUCCESS);
#endif /* OBEX_DEINIT_FUNCS == XA_ENABLED */

Error:
    OS_UnlockStack();
    return status;
}

#if OBEX_DEINIT_FUNCS == XA_ENABLED
/*---------------------------------------------------------------------------
 *            OPUSH_DeregisterServer
 *---------------------------------------------------------------------------
 *
 * Synopsis: Deregisters the object push server from the GOEP multiplexor.
 *           This includes removing the SDP records for the Object Push
 *           Service.
 *
 * Return:   ObStatus
 *
 */
ObStatus OPUSH_DeregisterServer(GoepServerApp *Server)
{
    ObStatus status = OB_STATUS_INVALID_PARM;

    OS_LockStack();

#if XA_ERROR_CHECK == XA_ENABLED
    if (!Server) {
        goto Error;
    }

    if (OPP(initialized) != TRUE) {
        /* OPUSH not initialized, so there is nothing to deinit */
        status = OB_STATUS_SUCCESS;
        goto Error;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(Server);

    /* Deregister SDP Server from GOEP mux */
    status = GOEP_DeregisterServer(Server);

#if BT_STACK == XA_ENABLED 
    if (status == OB_STATUS_SUCCESS) {
        /* Remove Object Push Service record from SDP database */
        OPUSH_RemoveServiceRecord(Server);
    }
#endif /* BT_STACK == XA_ENABLED */

Error:
    OS_UnlockStack();
    return status;
}
#endif /* OBEX_DEINIT_FUNCS == XA_ENABLED */
#endif /* OBEX_ROLE_SERVER == XA_ENABLED */

#if OBEX_ROLE_CLIENT == XA_ENABLED
static void OPushClientCallback(struct _GoepClientEvent *);
/*---------------------------------------------------------------------------
 *            OPUSH_RegisterClient
 *---------------------------------------------------------------------------
 *
 * Synopsis: Registers the object push client with the GOEP multiplexor.
 *
 * Return:   ObStatus
 *
 */
ObStatus OPUSH_RegisterClient(GoepClientApp *Client, const ObStoreFuncTable *obStoreFuncs)
{
    U8 freeId;
    ObStatus status = OB_STATUS_BUSY;

    OS_LockStack();

#if XA_ERROR_CHECK == XA_ENABLED
    if ((!Client) || (!obStoreFuncs)) {
        status = OB_STATUS_INVALID_PARM;
        goto Error;
    }

    if (OPP(initialized) != TRUE) {
        /* OPUSH not initialized */
        status = OB_STATUS_FAILED;
        goto Error;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(Client && obStoreFuncs);

    Client->type = GOEP_PROFILE_OPUSH;
    Client->appParent = OPushClientCallback;

    freeId = GOEP_GetConnectionId(GOEP_CLIENT, GOEP_PROFILE_OPUSH);
    if (freeId != 0xFF) {
        /* We can multiplex OPUSH over an existing connection, so we'll do it */
        Client->connFlags = GOEP_MUX_CONN;
        Client->connId = freeId;
    }
    else {
        /* We have to make a new connection for the OPUSH profile */
        Client->connFlags = GOEP_NEW_CONN;
        /* Initialize the connId - filled in later */
        Client->connId = 0;
    }

    /* Register OPush Client with GOEP mux */
    status = GOEP_RegisterClient(Client, obStoreFuncs);

Error:
    OS_UnlockStack();
    return status;
}

#if OBEX_DEINIT_FUNCS == XA_ENABLED
/*---------------------------------------------------------------------------
 *            OPUSH_DeregisterClient
 *---------------------------------------------------------------------------
 *
 * Synopsis: Deregisters the object push client from the GOEP multiplexor.
 *
 * Return:   ObStatus
 *
 */
ObStatus OPUSH_DeregisterClient(GoepClientApp *Client)
{
    ObStatus status = OB_STATUS_INVALID_PARM;

    OS_LockStack();

#if XA_ERROR_CHECK == XA_ENABLED
    if (!Client) {
        goto Error;
    }

    if (OPP(initialized) != TRUE) {
        /* OPUSH not initialized, so there is nothing to deinit */
        status = OB_STATUS_SUCCESS;
        goto Error;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(Client);

    /* Deregister OPush Client from GOEP mux */
    status = GOEP_DeregisterClient(Client);
    
Error:
    OS_UnlockStack();
    return status;
}
#endif /* OBEX_DEINIT_FUNCS == XA_ENABLED */

/*---------------------------------------------------------------------------
 *            OPUSH_Connect
 *---------------------------------------------------------------------------
 *
 * Synopsis: Maps the Object Push Client Connect function to the GOEP function.
 *           Refer to GOEP_Connect() in goep.h for more information.
 *
 * Return:   ObStatus
 *
 */
ObStatus OPUSH_Connect(GoepClientApp *Client, ObexTpAddr *Target)
{
    ObStatus    status = OB_STATUS_INVALID_PARM;

    OS_LockStack();

#if XA_ERROR_CHECK == XA_ENABLED
    if ((!Client) || (!Target)) {
        goto Error;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(Client && Target);

#if BT_STACK == XA_ENABLED
    if (Target->type == OBEX_TP_BLUETOOTH) {
        OPUSH_BuildServiceQuery( &Target->proto.bt.sdpQuery,
                                 &Target->proto.bt.sdpQueryLen,
                                 &Target->proto.bt.sdpQueryType );
    }
#endif /* BT_STACK == XA_ENABLED */

    if ((status = GOEP_TpConnect(Client, Target)) == OB_STATUS_SUCCESS) {
        /* The transport layer connection is up. Send an OBEX Connect. */
        status = GOEP_Connect(Client, 0);
    }

Error:
    OS_UnlockStack();
    return status;
}

/****************************************************************************
 *
 * Internal functions
 *
 ****************************************************************************/

/*---------------------------------------------------------------------------
 *            OPushClientCallback
 *---------------------------------------------------------------------------
 *
 * Synopsis:  This function processes GOEP Client protocol events.
 *
 */
static void OPushClientCallback(GoepClientEvent *Event)
{
    ObStatus        status;

    switch (Event->event) {
    case GOEP_EVENT_TP_CONNECTED:
        /* The transport layer connection is up. Send an OBEX Connect. */
        status = GOEP_Connect(Event->handler, 0);
        break;

    case GOEP_EVENT_TP_DISCONNECTED:
        /* The transport layer connection has been disconnected. */
        Event->handler->callback(Event);
        return;
        
    case GOEP_EVENT_COMPLETE:
        /* The requested operation has completed. */
        switch (Event->oper) {
        case GOEP_OPER_CONNECT:
        case GOEP_OPER_ABORT:
        case GOEP_OPER_PUSH:
        case GOEP_OPER_PULL:
            /* Tell the app were done */
            Event->handler->callback(Event);
            return;
            
        case GOEP_OPER_DISCONNECT:
            /* OBEX Disconnect is complete, disconnect link. */
            status = GOEP_TpDisconnect(Event->handler);
            break;

        default:
            Assert(0);
            return;
        }
        break;

    case GOEP_EVENT_ABORTED:
        /* The requested operation has failed. Alert the user. */
        if (Event->oper == GOEP_OPER_CONNECT || Event->oper == GOEP_OPER_DISCONNECT) {
            /* One of the intermediate operations failed. We cannot stop here.
             * Disconnect the link.
             */
            if (Event->reason != OBRC_LINK_DISCONNECT) {
                /* Disconnect the link */
                status = GOEP_TpDisconnect(Event->handler);
                break;
            }

            /* We're disconnected, indicate it to the client. */
            Event->event = GOEP_EVENT_TP_DISCONNECTED;
            Event->oper = GOEP_OPER_NONE;

            Event->handler->callback(Event);
            return;
        }

        /* Requested operation was Aborted. Tell the client. */
        Event->handler->callback(Event);
        return;

    default:
        Event->handler->callback(Event);
        return;
    }

    /* Check the status of the new operation */
    if (status != OB_STATUS_PENDING) {
        status = GOEP_TpDisconnect(Event->handler);

        if (status != OB_STATUS_PENDING)
            Event->handler->callback(Event);
    }
}
#endif /* OBEX_ROLE_CLIENT == XA_ENABLED */

#if OBEX_ROLE_SERVER == XA_ENABLED
/*---------------------------------------------------------------------------
 *            OPushServerCallback
 *---------------------------------------------------------------------------
 *
 * Synopsis:  This function processes GOEP Server protocol events.
 *
 */
static void OPushServerCallback(GoepServerEvent *Event)
{
    if ((Event->oper == GOEP_OPER_SETFOLDER) || (Event->oper == GOEP_OPER_DELETE)) {
        /* It's an illegal Object Push command. Abort it and hide it from the app. */
        if (Event->event == GOEP_EVENT_START)
            GOEP_ServerAbort(Event->handler, OBRC_FORBIDDEN);

        if (Event->event == GOEP_EVENT_CONTINUE)
            GOEP_ServerContinue(Event->handler);
        return;
    }

#if OBEX_AUTHENTICATION == XA_ENABLED
    /* Skim off authentication headers. */
    if ((Event->event == GOEP_EVENT_AUTH_CHALLENGE) ||
        (Event->event == GOEP_EVENT_AUTH_RESPONSE)) {
        return;
    }
#endif /* OBEX_AUTHENTICATION == XA_ENABLED */

    /* Notify Server of event */
    Event->handler->callback(Event);
}
#endif /* OBEX_ROLE_SERVER == XA_ENABLED */
#endif /* OPUSH_EXPANDED_API == XA_ENABLED */
