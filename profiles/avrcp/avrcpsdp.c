/*****************************************************************************
 *
 * File:
 *     $Workfile:avrcpsdp.c$ for iAnywhere AV SDK, version 1.2.3
 *     $Revision:15$
 *
 * Description: This file contains code for the AVRCP profile. 
 *
 * Created:     May 19, 2004
 *
 * Copyright 2000-2005 Extended Systems, Inc.

 * Portions copyright 2005-2006 iAnywhere Solutions, Inc.

 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any means, 
 * or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, Inc.  
 * This work contains confidential and proprietary information of Extended 
 * Systems, Inc. which is protected by copyright, trade secret, trademark and 
 * other intellectual property rights.
 *
 ****************************************************************************/

#include "bttypes.h"
#include "btalloc.h"
#include "sys/avrcpi.h"

/****************************************************************************
 *
 * ROMable data
 *
 ****************************************************************************/
const U8 BtBaseUUID[] = {
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x10, 0x00,
    0x80, 0x00, 0x00, 0x80, 0x5F, 0x9B, 0x34, 0xFB
};

/****************************************************************************
 *
 * SDP objects registered by AVRCP Targets.
 *
 ****************************************************************************/

/*---------------------------------------------------------------------------
 * SDP Service Class ID.
 */
static const U8 AvrcpCtServiceClassId[] = {
    SDP_ATTRIB_HEADER_8BIT(3),              /* DES  */
    SDP_UUID_16BIT(SC_AV_REMOTE_CONTROL)    /* UUID */
};

/*---------------------------------------------------------------------------
 * SDP Service Class ID.
 */
static const U8 AvrcpTgServiceClassId[] = {
    SDP_ATTRIB_HEADER_8BIT(3),                  /* DES  */
    SDP_UUID_16BIT(SC_AV_REMOTE_CONTROL_TARGET) /* UUID */
};

/*---------------------------------------------------------------------------
 * SDP Bluetooth Profile Descriptor List.
 */
static const U8 AvrcpBtProfDescList[] = {
    SDP_ATTRIB_HEADER_8BIT(8),              /* DES, 8 bytes    */
    SDP_ATTRIB_HEADER_8BIT(6),              /* DES #0, 6 bytes */
    SDP_UUID_16BIT(SC_AV_REMOTE_CONTROL),   /* Uuid16          */
    SDP_UINT_16BIT(0x0100)                  /* Uint16 version  */
};

/*---------------------------------------------------------------------------
 * SDP Protocol Descriptor List.
 * 
 * Value of the protocol descriptor list for the AVRCP Service.
 * This structure is a ROM'able representation of the RAM structure.
 * During AVRCP_Init, this structure is copied into the RAM structure
 * and used to register. A RAM structure is used to enable dynamic
 * setting of certain values.
 */
static const U8 AvrcpProtoDescList[] = {
    /* Data element sequence */
    SDP_ATTRIB_HEADER_8BIT(16),

    /* Each element of the list is a Protocol descriptor which is a
     * data element sequence. The first element is L2CAP which has
     * a UUID element and a PSM.
     */
    SDP_ATTRIB_HEADER_8BIT(6),      /* DES, 6 bytes   */     
    SDP_UUID_16BIT(PROT_L2CAP),     /* Uuid16         */
    SDP_UINT_16BIT(BT_PSM_AVCTP),   /* PSM            */

    /* The next protocol descriptor is for AVCTP. It contains 2 elements
     * UUID and Version.
     */
    SDP_ATTRIB_HEADER_8BIT(6),      /* DES, 6 bytes   */     
    SDP_UUID_16BIT(PROT_AVCTP),     /* Uuid16         */
    SDP_UINT_16BIT(0x0100),         /* Uint16 version */
};

/*---------------------------------------------------------------------------
 * Public Browse Group.
 */
static const U8 AvrcpBrowseGroup[] = {
    SDP_ATTRIB_HEADER_8BIT(3),              /* 3 bytes */
    SDP_UUID_16BIT(SC_PUBLIC_BROWSE_GROUP), /* Public Browse Group */
};

/*---------------------------------------------------------------------------
 * AVRCP attributes.
 *
 * This is a ROM template for the RAM structure used to register the
 * SDP record. The number of elements of this array is defined
 * by AVRCP_NUM_ATTRIBUTES.
 */
const SdpAttribute AvrcpSdpAttributes[] = {
    /* Service class ID List attribute */
    SDP_ATTRIBUTE(AID_SERVICE_CLASS_ID_LIST, 0),    /* AvrcpCtServiceClassId */ 
    /* Protocol Descriptor list attribute */
    SDP_ATTRIBUTE(AID_PROTOCOL_DESC_LIST, 0),       /* AvrcpProtoDescList */
    /* Public Browse Group Service */
    SDP_ATTRIBUTE(AID_BROWSE_GROUP_LIST, 0),        /* AvrcpBrowseGroup */
    /* Profile Descriptor list attribute */
    SDP_ATTRIBUTE(AID_BT_PROFILE_DESC_LIST, 0),     /* AvrcpBtProfDescList */
};

/****************************************************************************
 *
 * SDP objects used to query AVRCP services.
 *
 ****************************************************************************/

/*-------------------------------------------------------------------------
 *
 * SDP query info
 *
 * Service search attribute request.
 */
const U8 AvrcpServiceSearchAttribReq[] = {
    /* First parameter is the search pattern in data element format. It
     * is a list of 4 UUIDs. 
     */
    SDP_ATTRIB_HEADER_8BIT(9),              /* DES, 9 bytes */
    SDP_UUID_16BIT(SC_AV_REMOTE_CONTROL),   /* UUID         */
    /* The second UUID is L2CAP */
    SDP_UUID_16BIT(PROT_L2CAP),             /* L2CAP UUID   */
    /* The third UUID is AVCTP */
    SDP_UUID_16BIT(PROT_AVCTP),             /* AVCTP UUID   */

    /* The second parameter is the maximum number of bytes that can be
     * be received for the attribute list
     */
    0x02, 0xA0, /* Max number of bytes for attribute is 672 */

    /* The third parameter is the actual attribute ID list which is a data
     * element sequence. We only want one attribute which is the protocol
     * descriptor
     */
    SDP_ATTRIB_HEADER_8BIT(6),                  /* DES, 3 bytes      */
    SDP_UINT_16BIT(AID_SERVICE_CLASS_ID_LIST),  /* Service ID        */
    SDP_UINT_16BIT(AID_PROTOCOL_DESC_LIST)      /* Prot Desc List ID */
};

/****************************************************************************
 *
 * Function prototypes
 *
 ****************************************************************************/
/*---------------------------------------------------------------------------
 *            AvrcpRegisterSdp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Registers an SDP entry.
 *
 * Return:    BT_STATUS_FAILED
 *            (Also see SDP_AddRecord)
 */
BtStatus AvrcpRegisterSdp(AvrcpChannel *Chnl)
{
    BtStatus    status;
    SdpRecord  *sdpRecord;

    /* Configure the attributes */
    switch (Chnl->role) {
    case AVRCP_CT:
        if (AVRCP(ctChannelCount)++ > 0) return BT_STATUS_SUCCESS;

        /* Copy the SDP attributes to RAM */
        Assert(sizeof(AVRCP(ctSdpAttrib)) == sizeof(AvrcpSdpAttributes));

        OS_MemCopy((U8 *)&AVRCP(ctSdpAttrib), (U8 *)AvrcpSdpAttributes,
                   sizeof(AvrcpSdpAttributes));

        AVRCP(ctSdpAttrib)[0].value = AvrcpCtServiceClassId;
        AVRCP(ctSdpAttrib)[0].len = sizeof(AvrcpCtServiceClassId);
        AVRCP(ctSdpAttrib)[1].value = AvrcpProtoDescList;
        AVRCP(ctSdpAttrib)[1].len = sizeof(AvrcpProtoDescList);
        AVRCP(ctSdpAttrib)[2].value = AvrcpBrowseGroup;
        AVRCP(ctSdpAttrib)[2].len = sizeof(AvrcpBrowseGroup);
        AVRCP(ctSdpAttrib)[3].value = AvrcpBtProfDescList;
        AVRCP(ctSdpAttrib)[3].len = sizeof(AvrcpBtProfDescList);

        AVRCP(ctSdpRecord).attribs = AVRCP(ctSdpAttrib);
        AVRCP(ctSdpRecord).num = AVRCP_NUM_ATTRIBUTES;
        sdpRecord = &AVRCP(ctSdpRecord);
        break;

    case AVRCP_TG:
        if (AVRCP(tgChannelCount)++ > 0) return BT_STATUS_SUCCESS;

        /* Copy the SDP attributes to RAM */
        Assert(sizeof(AVRCP(tgSdpAttrib)) == sizeof(AvrcpSdpAttributes));

        OS_MemCopy((U8 *)&AVRCP(tgSdpAttrib), (U8 *)AvrcpSdpAttributes,
                   sizeof(AvrcpSdpAttributes));

        AVRCP(tgSdpAttrib)[0].value = AvrcpTgServiceClassId;
        AVRCP(tgSdpAttrib)[0].len = sizeof(AvrcpTgServiceClassId);
        AVRCP(tgSdpAttrib)[1].value = AvrcpProtoDescList;
        AVRCP(tgSdpAttrib)[1].len = sizeof(AvrcpProtoDescList);
        AVRCP(tgSdpAttrib)[2].value = AvrcpBrowseGroup;
        AVRCP(tgSdpAttrib)[2].len = sizeof(AvrcpBrowseGroup);
        AVRCP(tgSdpAttrib)[3].value = AvrcpBtProfDescList;
        AVRCP(tgSdpAttrib)[3].len = sizeof(AvrcpBtProfDescList);

        AVRCP(tgSdpRecord).attribs = AVRCP(tgSdpAttrib);
        AVRCP(tgSdpRecord).num = AVRCP_NUM_ATTRIBUTES;
        sdpRecord = &AVRCP(tgSdpRecord);
        break;

    default:
        status = BT_STATUS_FAILED;
        goto fail;
    }

    sdpRecord->classOfDevice = 0;
    status = SDP_AddRecord(sdpRecord);

fail:
    return status;
}


/*---------------------------------------------------------------------------
 *            AvrcpDeregisterSdp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Deregisters an SDP entry.
 *
 * Return:    BT_STATUS_FAILED
 *            (Also see SDP_RemoveRecord)
 */
BtStatus AvrcpDeregisterSdp(AvrcpChannel *Chnl)
{
    BtStatus status = BT_STATUS_SUCCESS;

    if (Chnl->role == AVRCP_CT) {
        if (--AVRCP(ctChannelCount) == 0) {
            status = SDP_RemoveRecord(&AVRCP(ctSdpRecord));
        }
    } else if (Chnl->role == AVRCP_TG) {
        if (--AVRCP(tgChannelCount) == 0) {
            status = SDP_RemoveRecord(&AVRCP(tgSdpRecord));
        }
    } else {
        status = BT_STATUS_FAILED;
    }

    return status;
}

/*---------------------------------------------------------------------------
 *            AvrcpStartServiceQuery()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Starts the SDP query.
 *            
 *
 * Return:    
 *
 */
BtStatus AvrcpStartServiceQuery(AvrcpChannel *Chnl, SdpQueryMode mode)
{
    BtStatus status;
    U16      role;

    if (mode == BSQM_FIRST) {
        /* Initialize the Query token */
        Assert(sizeof(Chnl->sdpSrchAttrib) == 
               sizeof(AvrcpServiceSearchAttribReq));

        OS_MemCopy((U8 *)(Chnl->sdpSrchAttrib), 
                   (U8 *)AvrcpServiceSearchAttribReq,
                   sizeof(AvrcpServiceSearchAttribReq));

        /* Fix up SDP attribute value to point to desired role */
        if (Chnl->role == AVRCP_CT) {
            role = AVRCP_TG;
        } else if (Chnl->role == AVRCP_TG) {
            role = AVRCP_CT;
        } else {
            return BT_STATUS_FAILED;
        }
        StoreBE16(&(Chnl->sdpSrchAttrib[3]), role);

        Chnl->sdpQueryToken.parms = Chnl->sdpSrchAttrib;
        Chnl->sdpQueryToken.plen = sizeof(AvrcpServiceSearchAttribReq);
        Chnl->sdpQueryToken.type = BSQT_SERVICE_SEARCH_ATTRIB_REQ;
        Chnl->sdpQueryToken.callback = AvrcpSdpCallback;

        /* Perform an SDP Query */
        Chnl->sdpQueryToken.attribId = AID_SERVICE_CLASS_ID_LIST; 
        Chnl->sdpQueryToken.uuid = role;
        Chnl->sdpQueryToken.mode = BSPM_BEGINNING;
    }

    Chnl->sdpQueryToken.rm = Chnl->cmgrHandler.bdc->link;
    status = SDP_Query(&Chnl->sdpQueryToken, mode);

    return status;
}

/*---------------------------------------------------------------------------
 *            AvrcpVerifySdpQueryRsp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Parses the SDP response for the Service Class 
 *            and Protocol Descriptor List.  In the case of a continuation 
 *            state, additional SDP queries will be issued until all of the
 *            attributes have been found or until the entire SDP response
 *            has been parsed.
 *            
 *
 * Return:    BT_STATUS_SUCCESS if SDP parsing is successful.
 *            BT_STATUS_FAILED if SDP parsing encountered an error.
 *            BT_STATUS_SDP_CONT_STATE if SDP parsing encountered a 
 *            continuation state that requires an additional SDP query.
 */
BtStatus AvrcpVerifySdpQueryRsp(SdpQueryToken *token)
{
    BOOL          found = FALSE;
    BtStatus      status = BT_STATUS_FAILED;

    /* Verify AVRCP Service */
    if (token->attribId == AID_SERVICE_CLASS_ID_LIST) {
        if ((status = SDP_ParseAttributes(token)) == BT_STATUS_SUCCESS) {
            Report(("AVRCP: Service found, Looking for AVCTP!\n"));
            token->attribId = AID_PROTOCOL_DESC_LIST;
        }
    }

    /* Verify AVCTP protocol */
    if (token->attribId == AID_PROTOCOL_DESC_LIST) {
        token->uuid = PROT_AVCTP;
        if ((status = SDP_ParseAttributes(token)) == BT_STATUS_SUCCESS) {
            Report(("AVRCP: AVCTP found, SDP_ParseAttributes succeeded!\n"));
            found = TRUE;
        }
    }

    if (status == BT_STATUS_SDP_CONT_STATE) {
        Report(("AVRCP: SDP_ParseAttributes - Continuation State. Query Again!\n"));
        token->mode = BSPM_CONT_STATE;
        return status;
    } else if (status == BT_STATUS_FAILED) {
        /* An SDP parsing failure would require us to reissue our first SDP
         * Query again.  Instead, we'll just skip the optional attributes
         * and stop parsing if we encounter an unexpected failure.
         */
        Report(("AVRCP: SDP_ParseAttributes - Failure!\n"));
    }

    /* Reset the attribute to parse */
    token->attribId = AID_SERVICE_CLASS_ID_LIST;
    /* Reset the SDP parsing mode */
    token->mode = BSPM_BEGINNING;

    /* Return the status of the L2CAP PSM parsing, since this
     * is the only critical information to obtain.
     */
    if (found) {
        return BT_STATUS_SUCCESS;
    } else {
        return status;
    }
}

/*---------------------------------------------------------------------------
 *            AvrcpSdpCallback()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Callback routine for SDP events.
 *
 * Return:    void
 */
void AvrcpSdpCallback(const BtEvent *event)
{
    AvrcpChannel *chnl = ContainingRecord(event->p.token, 
                                          AvrcpChannel, 
                                          sdpQueryToken);

    BtRemoteDevice *remDev;
    BtStatus        status;

    remDev = chnl->cmgrHandler.bdc->link;
    switch (event->eType) {
    case SDEVENT_QUERY_RSP:
        status = AvrcpVerifySdpQueryRsp(event->p.token);
        if (status == BT_STATUS_SUCCESS) {
            /* Connect the L2CAP */
            status = AVCTP_Connect(&chnl->chnl, remDev);
        } else if (status == BT_STATUS_SDP_CONT_STATE) {
            /* We are going to query again for the rest of the response */
            status = AvrcpStartServiceQuery(chnl, BSQM_CONTINUE);
            if (status != BT_STATUS_PENDING) {
                /* SDP query failed to start properly, so disconnect */
                Report(("AVRCP: SDP Query Failed\n"));
                goto error_exit;
            }
        } else {
            /* Query failed, fail the connect attempt */
            Report(("AVRCP: could not parse SDP response\n"));
            goto error_exit;
        }
        break;

    case SDEVENT_QUERY_ERR:
    case SDEVENT_QUERY_FAILED:
        /* Query failed, fail the connect attempt */
        Report(("AVRCP: avrcp query failed\n"));
        goto error_exit;

#if XA_DEBUG == XA_ENABLED
    default:
        Report(("AVRCP: SDP callback unexpected event: %i\n", event->eType));
        Assert(0);
        break;
#endif /* XA_DEBUG == XA_ENABLED */
    }

    return;

    error_exit:

    /* Try to connect anyway to the well known PSM */
    status = AVCTP_Connect(&chnl->chnl, remDev);
    if (status != BT_STATUS_PENDING) {
        AvrcpReportFailedConnect(chnl);
    }
}

