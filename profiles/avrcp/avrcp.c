/****************************************************************************
 *
 * File:
 *     $Workfile:avrcp.c$ for iAnywhere AV SDK, version 1.2.3
 *     $Revision:41$
 *
 * Description: This file contains public API for the Audio/Video
 *     Remote Control Profile (AVRCP).
 *             
 * Created:     Mar 10, 2004
 *
 * Copyright 2004 - 2005 Extended Systems, Inc.

 * Portions copyright 2005-2006 iAnywhere Solutions, Inc.

 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any means, 
 * or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, Inc. 
 * This work contains confidential and proprietary information of Extended 
 * Systems, Inc. which is protected by copyright, trade secret, trademark and 
 * other intellectual property rights.
 *
 ****************************************************************************/

#include "sys/avrcpi.h"

#if XA_CONTEXT_PTR == XA_ENABLED
/* The stack context is accessed through a pointer. Normally in this case
 * the stack context would be dynamically allocated. For testing purposes
 * the pointer points to a global structure.
 */
BtAvrcpContext  avTmp;
BtAvrcpContext *avrcpContext = &avTmp;
#else /* XA_CONTEXT_PTR == XA_ENABLED */
BtAvrcpContext avrcpContext;
#endif /* XA_CONTEXT_PTR */
static void AvrcpCmgrCallback(CmgrHandler *cHandler, 
                              CmgrEvent Event, 
                              BtStatus Status);
#if AVRCP_PANEL_SUBUNIT == XA_ENABLED
static BOOL AvrcpHandlePanelCmd(AvrcpChannel *chnl, AvrcpCallbackParms *parms);
static BOOL AvrcpHandlePanelCmdControl(AvrcpChannel *chnl, AvrcpCallbackParms *parms);
static void AvrcpPanelTimerExpired(EvmTimer *timer);
static void AvrcpHandlePanelInput(AvrcpChannel *chnl);
static BOOL AvrcpHandlePanelRsp(AvrcpChannel *chnl, AvrcpCallbackParms *parms);
#endif /* AVRCP_PANEL_SUBUNIT == XA_ENABLED */

/*---------------------------------------------------------------------------
 * AVRCP_Init()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initialize the AVRCP SDK.  This function should only be called
 *            once, normally at initialization time.  The calling of this 
 *            function can be specified in overide.h using the XA_LOAD_LIST 
 *            macro (i.e. #define XA_LOAD_LIST XA_MODULE(AV)).
 *
 * Return:    (See header file)
 */
BOOL AVRCP_Init(void)
{
    BOOL status;

    /* Initialize Memory */
    status = AvrcpAlloc();
    if (status) {
        status = AVCTP_Init();
    }

    return status;
}

/*---------------------------------------------------------------------------
 * AvCtpCallback()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Callback function for AVCTP.
 *
 * Return:    void
 */
void AvCtpCallback(AvctpChannel *Chnl, AvctpCallbackParms *Parms)
{
    AvrcpChannel *chnl = ContainingRecord(Chnl, AvrcpChannel, chnl);
    AvrcpCallbackParms parms;

    if (Parms->event == AVCTP_EVENT_DISCONNECT) {
        /* Release link handler */
        (void)CMGR_RemoveDataLink(&chnl->cmgrHandler);
        (void)CMGR_DeregisterHandler(&chnl->cmgrHandler);
    }

    parms.channel = chnl;
    parms.event = Parms->event;
    parms.status = Parms->status;

    /* Assigns the pointer in the "p" union (whatever it is) */
    parms.p.remDev = Parms->p.remDev;

#if AVRCP_PANEL_SUBUNIT == XA_ENABLED
    /* Do special panel handling */
    switch(parms.event) {

    case AVRCP_EVENT_CONNECT:
    case AVRCP_EVENT_DISCONNECT:
        EVM_CancelTimer(&chnl->panelTimer);

        /* Initialize values as necessary */
        if (chnl->role == SC_AV_REMOTE_CONTROL) {
            chnl->panelState = AVRCP_PANEL_STATE_C_IDLE;
            chnl->panel.ct.opRead = 0;
            chnl->panel.ct.opWrite = 0;

            /* Prepare the cmd frame */
            chnl->panel.ct.cmd.ctype = AVCTP_CTYPE_CONTROL;
            chnl->panel.ct.cmd.subunitType = AVRCP_SUBUNIT_PANEL;
            chnl->panel.ct.cmd.subunitId = 0x0;
            chnl->panel.ct.cmd.opcode = AVRCP_OPCODE_PASS_THROUGH;
            chnl->panel.ct.cmd.operandLen = 2;
            chnl->panel.ct.cmd.operands = chnl->panel.ct.data;
            chnl->panel.ct.data[1] = 0;
        }
        else if (chnl->role == SC_AV_REMOTE_CONTROL_TARGET) {
            chnl->panelState = AVRCP_PANEL_STATE_T_IDLE;
            chnl->panel.tg.curOp = AVRCP_POP_INVALID;
        } else {
            Assert(0);
        }
        break;

    case AVRCP_EVENT_RESPONSE:

        if (AvrcpHandlePanelRsp(chnl, &parms)) return;

        /* Give the panel code a chance to send a new command */
        AvrcpHandlePanelInput(chnl);
        break;

    case AVRCP_EVENT_COMMAND:

        if (AvrcpHandlePanelCmd(chnl, &parms)) return;
        break;

    case AVRCP_EVENT_TX_DONE:
        /* Do NOT indicate internal cmd/rsp to app, just quietly
         * consume. 
         */
        if ((parms.p.rspFrame == &chnl->panel.tg.rsp) || 
            (parms.p.cmdFrame == &chnl->panel.ct.cmd))
            return;
        break;
    }
    
#endif /* AVRCP_PANEL_SUBUNIT == XA_ENABLED */

    chnl->callback(chnl, &parms);
}


/*---------------------------------------------------------------------------
 * AVRCP_Register()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Registers an application callback to create/receive AVRCP 
 *            connections.  This function must be called before any other 
 *            AVRCP functions.
 *
 * Return:    (See header file)
 */
BtStatus AVRCP_Register(AvrcpChannel *chnl, AvrcpCallback callback, 
                        AvrcpRole role)
{
    BtStatus status;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, chnl != 0);
    CheckUnlockedParm(BT_STATUS_INVALID_PARM, callback != 0);

    OS_LockStack();

    status = AVCTP_Register(&chnl->chnl, AvCtpCallback);
    if (status == BT_STATUS_SUCCESS) {
        chnl->role = role;
        chnl->callback = callback;
        OS_MemSet((U8*)&chnl->cmgrHandler, 0, sizeof(CmgrHandler));
        /* Register with SDP */
        status = AvrcpRegisterSdp(chnl);
        if (status != BT_STATUS_SUCCESS) {
            AVCTP_Deregister(&chnl->chnl);
        }

#if AVRCP_PANEL_SUBUNIT == XA_ENABLED
        chnl->panelTimer.func = AvrcpPanelTimerExpired;
        chnl->panelTimer.context = chnl;
#endif
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 * AVRCP_Deregister()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  De-registers the AVRCP callback. After making this call
 *            successfully, the callback specified in AVRCP_Register will
 *            receive no further events.
 *
 * Return:    (See header file)
 */
BtStatus AVRCP_Deregister(AvrcpChannel *chnl)
{
    BtStatus status;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, chnl != 0);

    OS_LockStack();

    status = AVCTP_Deregister(&chnl->chnl);
    if (status == BT_STATUS_SUCCESS) {
        /* Deregister SDP */
        AvrcpDeregisterSdp(chnl);
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 * AVRCP_Connect()
 *---------------------------------------------------------------------------
 *
 * Synopsis: Connect to the remote device.
 *
 * Return:   (See header file)
 */
BtStatus AVRCP_Connect(AvrcpChannel *chnl, BD_ADDR *addr)
{
    BtStatus status;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, chnl != 0);

    OS_LockStack();

    /* Create a link */
    status = CMGR_RegisterHandler(&chnl->cmgrHandler, AvrcpCmgrCallback);
    if (status == BT_STATUS_SUCCESS) {
        status = CMGR_CreateDataLink(&chnl->cmgrHandler, addr);
        if (status == BT_STATUS_SUCCESS) {
            status = AvrcpStartServiceQuery(chnl, BSQM_FIRST);
        }
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 * AVRCP_Disconnect()
 *---------------------------------------------------------------------------
 *
 * Synopsis: Disconnect from the remote device.
 *
 * Return:   (See header file)
 */
BtStatus AVRCP_Disconnect(AvrcpChannel *chnl)
{
    BtStatus status;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, chnl != 0);

    OS_LockStack();

    /* Release link handler */
    status = AVCTP_Disconnect(&chnl->chnl);

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 * AVRCP_SendCommand()
 *---------------------------------------------------------------------------
 *
 * Synopsis: Send a command to the target.
 *
 * Return:   (See header file)
 */
BtStatus AVRCP_SendCommand(AvrcpChannel *chnl, AvrcpCmdFrame *cmdFrame)
{
    BtStatus status;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, (chnl != 0) &&
        (chnl->role == SC_AV_REMOTE_CONTROL));

    OS_LockStack();

    cmdFrame->headerLen = 0;
    status = AVCTP_SendCommand(&chnl->chnl, cmdFrame);

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 * AVRCP_SendResponse()
 *---------------------------------------------------------------------------
 *
 * Synopsis: Send a response to the controller.
 *
 * Return:   (See header file)
 */
BtStatus AVRCP_SendResponse(AvrcpChannel *chnl, AvrcpRspFrame *rspFrame)
{
    BtStatus status;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, chnl != 0);

    OS_LockStack();

    rspFrame->headerLen = 0;
    status = AVCTP_SendResponse(&chnl->chnl, rspFrame);

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 * AvrcpAlloc()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initializes the global context for the AV profile.  This 
 *            function can be replaced if necessary.
 *
 * Return:    void
 */
BOOL AvrcpAlloc(void)
{
    U8* ptr; 

    /* Fill memory with 0. */
#if XA_CONTEXT_PTR == XA_ENABLED
    ptr = (U8*)avrcpContext;
#else
    ptr = (U8*)&avrcpContext;
#endif /* XA_CONTEXT_PTR */

    OS_MemSet(ptr, 0, (U32)sizeof(BtAvrcpContext));

    return TRUE;
}

/*---------------------------------------------------------------------------
 *            AvrcpReportFailedConnect()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Report a failed connection attempt
 *
 */
void AvrcpReportFailedConnect(AvrcpChannel *chnl)
{
    AvrcpCallbackParms  info;

    /* Release link handler */
    (void)CMGR_RemoveDataLink(&chnl->cmgrHandler);
    (void)CMGR_DeregisterHandler(&chnl->cmgrHandler);

    /* Indicate the response to the application */
    info.event = AVRCP_EVENT_DISCONNECT;
    info.channel = chnl;
    chnl->callback(chnl, &info);
}

/*---------------------------------------------------------------------------
 *            AvrcpCmgrCallback()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Called by device manager with link state events.
 *
 */
static void AvrcpCmgrCallback(CmgrHandler *cHandler, 
                              CmgrEvent Event, 
                              BtStatus Status)
{
    AvrcpChannel   *chnl = ContainingRecord(cHandler, AvrcpChannel, cmgrHandler);

    switch (Event) {
    
    case CMEVENT_DATA_LINK_CON_CNF:
        if (Status == BT_STATUS_SUCCESS) {
            /* ACL is connected, start service query */
            if (AvrcpStartServiceQuery(chnl, BSQM_FIRST) == BT_STATUS_PENDING) {
                return;
            }
        }

        /* Disconnect and call the application */
        AvrcpReportFailedConnect(chnl);
        break;
    }
}

#if AVRCP_PANEL_SUBUNIT == XA_ENABLED

/*---------------------------------------------------------------------------
 * AVRCP_SetPanelKey()
 *---------------------------------------------------------------------------
 *
 * Synopsis: Queues a key operation to be sent.
 *
 * Return:   (See header file)
 */
BtStatus AVRCP_SetPanelKey(AvrcpChannel *chnl, AvrcpPanelOperation op,
                                  BOOL press)
{
    BtStatus status = BT_STATUS_NO_RESOURCES;
    U8 qLen, qFree, oldWrite;
    AvrcpPanelOperation lastOp = 0;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, chnl != 0);

    if (chnl->role != SC_AV_REMOTE_CONTROL) {
        return BT_STATUS_FAILED;
    }

    OS_LockStack();  

    op = op & ~AVRCP_POP_RELEASE;
    qLen = AVRCP_OpQueueLen(chnl);
    qFree = AVRCP_OpQueueFree(chnl);
    oldWrite = chnl->panel.ct.opWrite;

    if (qLen) {
        lastOp = AVRCP_OpQueueGet(chnl, qLen - 1);
    }

    /* If this is a new op and the previous key was not released, we must
     * automatically release it. */
    if ( (qLen) &&
         ( (lastOp & AVRCP_POP_RELEASE) == 0) &&
         ( (op != (lastOp & ~AVRCP_POP_RELEASE)) ||
           ( (press) && (op == (lastOp & ~AVRCP_POP_RELEASE)) ) ) ) {
        if (!qFree) {
            goto error;
        }
        lastOp = lastOp | AVRCP_POP_RELEASE;
        AVRCP_OpQueuePush(chnl, lastOp);
        qFree--;
        qLen++;
    }

    /* If this is a release request and the previous item was not a press
     * for the same operation, automatically add a "press"
     */
    if ( (!press) &&        
         ( (!qLen) ||
           (qLen && (lastOp != op) ) ) ) {
        if (!qFree) {
            goto error;
        }
        AVRCP_OpQueuePush(chnl, op);
        qFree--;
        qLen++;            
    }

    /* Finally, add this very operation, if there is room */
    if (!qFree) {
        goto error;
    }
    AVRCP_OpQueuePush(chnl, op | (press ? 0 : AVRCP_POP_RELEASE));
    AvrcpHandlePanelInput(chnl);
    status = BT_STATUS_PENDING;
    goto done;

error:
    chnl->panel.ct.opWrite = oldWrite;
done:
    OS_UnlockStack();

    return status;
}


/*---------------------------------------------------------------------------
 *            AvrcpPanelCnf()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Deliver a cnf event to the channel application
 *
 */
static void AvrcpPanelCnf(AvrcpChannel *chnl,
                          AvrcpResponse rsp, AvrcpPanelOperation op)
{
    AvrcpCallbackParms parms;

    parms.channel = chnl;
    parms.p.panelCnf.operation = op & ~AVRCP_POP_RELEASE;
    parms.p.panelCnf.press = ((op & AVRCP_POP_RELEASE) ? FALSE : TRUE);
    parms.p.panelCnf.response = rsp;
    parms.event = AVRCP_EVENT_PANEL_CNF;
    chnl->callback(chnl, &parms);

    /* After notifying application, more keystrokes might have appeared. */
    AvrcpHandlePanelInput(chnl);
}


/*---------------------------------------------------------------------------
 *            AvrcpHandlePanelInput()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handles case in which a panel subunit controller should check
 *            to see if its input can be delivered.
 */
static void AvrcpHandlePanelInput(AvrcpChannel *chnl)
{
    AvrcpPanelOperation op;
    BtStatus status;
    U16 opLen;

    /* Make sure there is something to do */
    opLen = AVRCP_OpQueueLen(chnl);
    if (!opLen) return;

    op = AVRCP_OpQueueGet(chnl, 0);

    switch(chnl->panelState) {
    case AVRCP_PANEL_STATE_C_IDLE:
        Assert(0 == (op & AVRCP_POP_RELEASE));
        chnl->panel.ct.data[0] = op;
        chnl->panel.ct.cmd.headerLen = 0;
        status = AVCTP_SendCommand(&chnl->chnl, &chnl->panel.ct.cmd);

        if (BT_STATUS_PENDING == status ) {
            chnl->panelState = AVRCP_PANEL_STATE_C_PRESS_R;
        } else if (BT_STATUS_BUSY != status) {
            Report(("AVRCP: Couldn't send command.\n"));
            AVRCP_Disconnect(chnl);
        }
        break;

    case AVRCP_PANEL_STATE_C_PRESS:
        /* If there are at least two items then we know the current
         * op was released.
         */
        if (opLen >= 2) {

            /* Cancel the holding timer */
            EVM_CancelTimer(&chnl->panelTimer);

            /* Indicate release to target */
            chnl->panel.ct.data[0] = op | AVRCP_POP_RELEASE;
            chnl->panel.ct.cmd.headerLen = 0;
            if (BT_STATUS_PENDING !=
                AVCTP_SendCommand(&chnl->chnl, &chnl->panel.ct.cmd)) {
                Report(("AVRCP: Couldn't send command.\n"));
                AVRCP_Disconnect(chnl);
            }
            chnl->panelState = AVRCP_PANEL_STATE_C_RELEASE;
        }
        break;

    case AVRCP_PANEL_STATE_C_SKIP:
        /* If there are at least two items then we know the current
         * op was released.
         */
        if (opLen >= 2) {
            /* Indicate that the release of the op was skipped */
            AVRCP_OpQueueAdvance(chnl, 2);
            chnl->panelState = AVRCP_PANEL_STATE_C_IDLE;
            AvrcpPanelCnf(chnl, AVRCP_RESPONSE_SKIPPED, (AvrcpResponse)(op | AVRCP_POP_RELEASE));
        }        
        break;
    }

    return;
}

/*---------------------------------------------------------------------------
 *            AvrcpHandlePanelRsp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Dispatch for responses directed to a Panel Subunit controller.
 *
 * Returns:   TRUE if the event indicated in "parms" was handled. FALSE
 *            if it could not be processed.
 */
static BOOL AvrcpHandlePanelRsp(AvrcpChannel *chnl, AvrcpCallbackParms *parms)
{
    AvrcpResponse rsp = parms->p.rspFrame->response;
    AvrcpPanelOperation op;
   
    if ((rsp != AVRCP_RESPONSE_ACCEPTED) &&
        (rsp != AVRCP_RESPONSE_REJECTED) &&
        (rsp != AVRCP_RESPONSE_NOT_IMPLEMENTED))
    {
        Report(("AVRCP: Invalid response code %d, changing to REJECTED\n"));
        rsp = AVRCP_RESPONSE_REJECTED;
    }

    switch(chnl->panelState) {
    case AVRCP_PANEL_STATE_C_PRESS_R:

        /* We could verify the response contents but for now just assume it is correct.
         */
        if (parms->p.rspFrame->response == AVRCP_RESPONSE_ACCEPTED) {
            EVM_StartTimer(&chnl->panelTimer, AVRCP_PANEL_HOLDUPDATE_TIME);
            chnl->panelState = AVRCP_PANEL_STATE_C_PRESS;
        } else {
            chnl->panelState = AVRCP_PANEL_STATE_C_SKIP;
        }
        AvrcpPanelCnf(chnl, rsp, AVRCP_OpQueueGet(chnl, 0));

        return TRUE;

    case AVRCP_PANEL_STATE_C_RELEASE:
        /* On release response, advance the opqueue and signal the app */
        Assert(AVRCP_OpQueueLen(chnl) >= 2);
        op = AVRCP_OpQueueGet(chnl, 1);
        AVRCP_OpQueueAdvance(chnl, 2);
        chnl->panelState = AVRCP_PANEL_STATE_C_IDLE;
        AvrcpPanelCnf(chnl, rsp, op);
        return TRUE;

    default:
        /* We are simply not expecting responses in other states. */
        return FALSE;
    }
}

/*---------------------------------------------------------------------------
 *            AvrcpHandlePanelCmd()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Dispatch for commands directed to a Panel Subunit target.
 *
 * Returns:   TRUE if the event indicated in "parms" was handled. FALSE
 *            if it could not be processed.
 */
static BOOL AvrcpHandlePanelCmd(AvrcpChannel *chnl, AvrcpCallbackParms *parms)
{
    /* Intercept commands destined for the panel subunit target */
    if (parms->p.cmdFrame->subunitType == AVRCP_SUBUNIT_PANEL)
    {
        if (chnl->role == SC_AV_REMOTE_CONTROL_TARGET) {
            if ((parms->p.cmdFrame->ctype == AVCTP_CTYPE_CONTROL) &&
                (parms->p.cmdFrame->opcode == AVCTP_OPCODE_PASS_THROUGH) &&
                (parms->p.cmdFrame->operandLen >= 2) &&
                ((parms->p.cmdFrame->operands[0] & 0x7F) != AVRCP_POP_VENDOR_UNIQUE)) {
                
                while (!AvrcpHandlePanelCmdControl(chnl, parms));
                
                return TRUE;
            }
        } else {
            Report(("AVRCP: Panel controller received command. Ignoring.\n"));
            return FALSE;
        }
    } else if ((parms->p.cmdFrame->subunitType == AVRCP_SUBUNIT_UNIT) &&
        (parms->p.cmdFrame->ctype == AVRCP_CTYPE_STATUS) &&
        (parms->p.cmdFrame->opcode == AVRCP_OPCODE_UNIT_INFO)) {

        /* Handle a UNIT_INFO command */

        /* Send back a canned response indicating PANEL */
        chnl->panel.tg.rsp.response = AVCTP_RESPONSE_IMPLEMENTED_STABLE;
        chnl->panel.tg.rsp.subunitType = AVRCP_SUBUNIT_UNIT;
        chnl->panel.tg.rsp.subunitId = 0x7; /* Ignore */
        chnl->panel.tg.rsp.opcode = AVRCP_OPCODE_UNIT_INFO;
        chnl->panel.tg.rsp.operandLen = 5;
        chnl->panel.tg.rsp.operands = chnl->panel.tg.data;
        chnl->panel.tg.rsp.headerLen = 0;
        chnl->panel.tg.data[0] = 0x07; /* As per specification */
        chnl->panel.tg.data[1] = (AVRCP_SUBUNIT_PANEL << 3) + 0; /* Unit_type of Panel */
        OS_MemCopy((U8 *)chnl->panel.tg.data + 2, (U8 *)AVRCP_PANEL_COMPANY_ID, 3);
        if (BT_STATUS_PENDING != AVCTP_SendResponse(&chnl->chnl, &chnl->panel.tg.rsp)) {
            Report(("AVRCP: Couldn't send response.\n"));
            AVRCP_Disconnect(chnl);
        } 
        return TRUE;
    } else if ((parms->p.cmdFrame->subunitType == AVRCP_SUBUNIT_UNIT) &&
        (parms->p.cmdFrame->ctype == AVRCP_CTYPE_STATUS) &&
        (parms->p.cmdFrame->opcode == AVRCP_OPCODE_SUBUNIT_INFO)) {

        /* Handle a SUBUNIT_INFO command */

        /* Send back a canned response indicating PANEL */
        chnl->panel.tg.rsp.response = AVCTP_RESPONSE_IMPLEMENTED_STABLE;
        chnl->panel.tg.rsp.subunitType = AVRCP_SUBUNIT_UNIT;
        chnl->panel.tg.rsp.subunitId = 0x7; /* Ignore */
        chnl->panel.tg.rsp.opcode = AVRCP_OPCODE_SUBUNIT_INFO;
        chnl->panel.tg.rsp.operandLen = 5;
        chnl->panel.tg.rsp.operands = chnl->panel.tg.data;
        chnl->panel.tg.rsp.headerLen = 0;
        chnl->panel.tg.data[0] = 0x07; /* As per specification */
        chnl->panel.tg.data[1] = (AVRCP_SUBUNIT_PANEL << 3) + 0; /* Unit_type of Panel */
        chnl->panel.tg.data[2] = 0xFF;
        chnl->panel.tg.data[3] = 0xFF;
        chnl->panel.tg.data[4] = 0xFF;
        if (BT_STATUS_PENDING != AVCTP_SendResponse(&chnl->chnl, &chnl->panel.tg.rsp)) {
            Report(("AVRCP: Couldn't send response.\n"));
            AVRCP_Disconnect(chnl);
        } 
        return TRUE;           
    }
    return FALSE;
}

/*---------------------------------------------------------------------------
 *            AvrcpPanelInd()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Deliver a PANEL_IND event to the application.
 *
 */
static void AvrcpPanelInd(AvrcpChannel *chnl,
                          AvrcpEvent event)
{
    AvrcpCallbackParms parms;

    parms.channel = chnl;
    parms.p.panelInd.operation = chnl->panel.tg.curOp;
    parms.event = event;
    chnl->callback(chnl, &parms);

    /* Reset the response byte in case it was altered */
    if ((chnl->panel.tg.curRsp != AVCTP_RESPONSE_REJECTED) &&
        (chnl->panel.tg.curRsp != AVCTP_RESPONSE_NOT_IMPLEMENTED)) {
        chnl->panel.tg.curRsp = AVCTP_RESPONSE_ACCEPTED;
    }

    /* After indicating a release be sure to reset the curOp */
    if (event == AVRCP_EVENT_PANEL_RELEASE)
    {
        chnl->panel.tg.curOp = AVRCP_POP_INVALID;
    }
}

/*---------------------------------------------------------------------------
 *            AvrcpHandlePanelCmdControl()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Dispatch for commands directed to a Panel Subunit target.
 *
 * Returns:   TRUE if the event indicated in "parms" was handled. FALSE
 *            if it could not be processed.
 */
static BOOL AvrcpHandlePanelCmdControl(AvrcpChannel *chnl, AvrcpCallbackParms *parms)
{
    BOOL released = (parms->p.cmdFrame->operands[0] & 0x80) == 0x80;
    AvrcpPanelOperation op = parms->p.cmdFrame->operands[0] & 0x7F;

    /* Prepare a response frame from cmd data */
    OS_MemCopy((U8 *)&chnl->panel.tg.rsp,
        (U8 *)parms->p.cmdFrame, sizeof(AvctpCmdFrame));
    chnl->panel.tg.rsp.response = AVCTP_RESPONSE_ACCEPTED;
    chnl->panel.tg.rsp.operands = chnl->panel.tg.data;
    chnl->panel.tg.rsp.operandLen = 2;
    chnl->panel.tg.data[0] = parms->p.cmdFrame->operands[0];
    chnl->panel.tg.data[1] = 0;

    switch(chnl->panelState) {

    case AVRCP_PANEL_STATE_T_IDLE:
        if (!released) {

            /* Accept by default */
            chnl->panel.tg.curRsp = AVCTP_RESPONSE_ACCEPTED;

            /* Notify the application */
            chnl->panel.tg.curOp = parms->p.cmdFrame->operands[0] & 0x7F;
            AvrcpPanelInd(chnl, AVRCP_EVENT_PANEL_PRESS);

            chnl->panel.tg.rsp.response = chnl->panel.tg.curRsp;
            
            if (chnl->panel.tg.curRsp == AVCTP_RESPONSE_ACCEPTED) {
                chnl->panelState = AVRCP_PANEL_STATE_T_PRESS;
                EVM_StartTimer(&chnl->panelTimer, AVRCP_PANEL_PRESSHOLD_TIME);
            }
        } else {

            if (op == chnl->panel.tg.curOp) {
                /* Finally received a certain outstanding op. Don't
                 * notify app.
                 */
                chnl->panel.tg.curOp = AVRCP_POP_INVALID;

                /* Respond with standing response type */
                chnl->panel.tg.rsp.response = chnl->panel.tg.curRsp;

            } else {
                /* Accept by default */
                chnl->panel.tg.curRsp = AVCTP_RESPONSE_ACCEPTED;
                chnl->panel.tg.curOp = op;
                /* Indicate this new op */
                AvrcpPanelInd(chnl, AVRCP_EVENT_PANEL_PRESS);

                chnl->panel.tg.rsp.response = chnl->panel.tg.curRsp;
            
                if (chnl->panel.tg.curRsp == AVCTP_RESPONSE_ACCEPTED) {
                    /* Indicate the release as well */
                    AvrcpPanelInd(chnl, AVRCP_EVENT_PANEL_RELEASE);
                }
            }
        }

        chnl->panel.tg.rsp.headerLen = 0;
        if (BT_STATUS_PENDING != AVCTP_SendResponse(&chnl->chnl, &chnl->panel.tg.rsp)) {
            Report(("AVRCP: Couldn't send response.\n"));
            AVRCP_Disconnect(chnl);
        }

        break;

    case AVRCP_PANEL_STATE_T_PRESS:
        if (op == chnl->panel.tg.curOp) {

            if (released) {
                /* Notify the application */
                AvrcpPanelInd(chnl, AVRCP_EVENT_PANEL_RELEASE);

                /* Cancel the timer and go back to IDLE */
                EVM_CancelTimer(&chnl->panelTimer);
                chnl->panelState = AVRCP_PANEL_STATE_T_IDLE;
            }

            /* Deliver a response in either case */
            chnl->panel.tg.rsp.headerLen = 0;
            if (BT_STATUS_PENDING != AVCTP_SendResponse(&chnl->chnl, &chnl->panel.tg.rsp)) {
                Report(("AVRCP: Couldn't send response.\n"));
                AVRCP_Disconnect(chnl);
            }
        } else {
            /* Release existing key (notify app) */
            AvrcpPanelInd(chnl, AVRCP_EVENT_PANEL_RELEASE);

            /* Reset to IDLE */
            EVM_CancelTimer(&chnl->panelTimer);
            chnl->panelState = AVRCP_PANEL_STATE_T_IDLE;

            return FALSE;
        }
        break;

    case AVRCP_PANEL_STATE_T_HOLD:
        if (op == chnl->panel.tg.curOp) {
            if (released) {
                /* Notify the application */
                AvrcpPanelInd(chnl, AVRCP_EVENT_PANEL_RELEASE);

                chnl->panelState = AVRCP_PANEL_STATE_T_IDLE;
                EVM_CancelTimer(&chnl->panelTimer);

            } else {
                /* Pressing and holding - no app notification */

                /* Reset timer */
                EVM_StartTimer(&chnl->panelTimer, AVRCP_PANEL_AUTORELEASE_TIME);
            }

            /* Deliver a response */
            chnl->panel.tg.rsp.headerLen = 0;
            if (BT_STATUS_PENDING != AVCTP_SendResponse(&chnl->chnl, &chnl->panel.tg.rsp)) {
                Report(("AVRCP: Couldn't send response.\n"));
                AVRCP_Disconnect(chnl);
            }
        } else {
            /* Release existing key (notify app) */
            AvrcpPanelInd(chnl, AVRCP_EVENT_PANEL_RELEASE);
            
            /* Reset to IDLE */
            chnl->panelState = AVRCP_PANEL_STATE_T_IDLE;
            EVM_CancelTimer(&chnl->panelTimer);
    
            return FALSE;
        }
        break;

    default:
        Assert(0);
        chnl->panelState = AVRCP_PANEL_STATE_T_IDLE;
        return FALSE;
    }
    return TRUE;
}

static void AvrcpPanelTimerExpired(EvmTimer *timer)
{
    AvrcpChannel *chnl = (AvrcpChannel *)timer->context;
    AvrcpPanelOperation op = chnl->panel.tg.curOp;

    switch(chnl->panelState) {
    case AVRCP_PANEL_STATE_T_PRESS:
        /* Timer going off during PRESS indicates a HOLD */
        AvrcpPanelInd(chnl, AVRCP_EVENT_PANEL_HOLD);
        chnl->panelState = AVRCP_PANEL_STATE_T_HOLD;

        /* Now reset the timer to detect a "lost" release button */
        EVM_StartTimer(timer, AVRCP_PANEL_AUTORELEASE_TIME - AVRCP_PANEL_PRESSHOLD_TIME);
        break;

    case AVRCP_PANEL_STATE_T_HOLD:
        /* Timer going off during HOLD indicates a RELEASE */
        AvrcpPanelInd(chnl, AVRCP_EVENT_PANEL_RELEASE);

        /* Reset current operation back to previous value so that
         * corresponding (late) release command will be ignored
         */
        chnl->panel.tg.curOp = op;

        chnl->panelState = AVRCP_PANEL_STATE_T_IDLE;
        break;

    case AVRCP_PANEL_STATE_C_PRESS:
        /* Send a press update and move back into PRESS_R */
        chnl->panel.ct.cmd.headerLen = 0;
        if (BT_STATUS_PENDING != 
            AVCTP_SendCommand(&chnl->chnl, &chnl->panel.ct.cmd)) {
            Report(("AVRCP: Couldn't send command.\n"));
            AVRCP_Disconnect(chnl);
        }
        chnl->panelState = AVRCP_PANEL_STATE_C_PRESS_R;
        break;

    default:
        /* No timer should be active in other states */
        Assert(0);
        break;
    }
}

#endif /* AVRCP_PANEL_SUBUNIT == XA_ENABLED */
