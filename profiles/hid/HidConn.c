#include "hidp.h"
#include "sys/hidconn.h"
#include "sys/hidpi.h"

/* Function Prototypes */
static void HidFreeConn(HidConn *Conn);

/*---------------------------------------------------------------------------
 *            HidConnDisconnected()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Disconnected state.
 *
 */
static void HidConnDisconnected(HidConn *Conn, L2capCallbackParms *Parms)
{
    HidConnCallbackParms connParms;

    switch (Parms->event) {
    case L2EVENT_CONNECT_IND:

        /* Notify the application */
        Conn->remDev = Parms->aclLink;
        connParms.event = HID_CONN_EVENT_CONNECT_IND;
        connParms.ptrs.remDev = Conn->remDev;
        Conn->callback(Conn, &connParms);
        break;
    case L2EVENT_DISCONNECTED:
        /* Conn disconnected, notify the app */
        HidFreeConn(Conn);
        break;
    }
}

/*---------------------------------------------------------------------------
 *            HidConnPending()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Conn Pending state.
 *
 */
static void HidConnConnPending(HidConn *Conn, L2capCallbackParms *Parms)
{
    HidConnCallbackParms connParms;

    switch (Parms->event) {
    case L2EVENT_CONNECTED:
        /* Connection is up */
        Conn->state = HID_STATE_CONNECTED;
        Conn->remDev = Parms->aclLink;

        /* Notify the application */
        connParms.event = HID_CONN_EVENT_OUT_CONNECTED;
        connParms.ptrs.remDev = Conn->remDev;
        Conn->callback(Conn, &connParms);
        break;

    case L2EVENT_DISCONNECTED:
        /* Conn disconnected, notify the app */
        HidFreeConn(Conn);
        break;
    default:
        Report(("-------------------------------->Hidconn unknow event = %d", Parms->event));
        break;
    }
}

/*---------------------------------------------------------------------------
 *            HidConnConnIncoming()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Connect Incoming state.
 *
 */
static void HidConnConnIncoming(HidConn *Conn, L2capCallbackParms *Parms)
{
    HidConnCallbackParms connParms;

    switch (Parms->event) {
    case L2EVENT_CONNECTED:
        /* Connection is up */
        Conn->state = HID_STATE_CONNECTED;
        Conn->remDev = Parms->aclLink;

        /* Notify the application */
        connParms.event = HID_CONN_EVENT_IN_CONNECTED;
        connParms.ptrs.remDev = Conn->remDev;
        Conn->callback(Conn, &connParms);
        break;

    case L2EVENT_DISCONNECTED:
        /* Conn disconnected, notify the app */
        HidFreeConn(Conn);
        break;
    }
}

/*---------------------------------------------------------------------------
 *            HidConnDiscPending()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Disconnect Pending state.
 *
 */
static void HidConnDiscPending(HidConn *Conn, L2capCallbackParms *Parms)
{
    HidConnCallbackParms connParms;

    switch (Parms->event) {
    case L2EVENT_DISCONNECTED:
        /* Conn disconnected, notify the app */
        HidFreeConn(Conn);
        break;

    case L2EVENT_PACKET_HANDLED:
        /* Notify the application */
        connParms.event = HID_CONN_EVENT_DATA_SENT;
        Conn->callback(Conn, &connParms);
        break;
    }
}

/*---------------------------------------------------------------------------
 *            HidConnDisconnected()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Disconnected state.
 *
 */
static void HidConnDiscIncoming(HidConn *Conn, L2capCallbackParms *Parms)
{
    HidConnCallbackParms connParms;

    switch (Parms->event) {
    case L2EVENT_DISCONNECTED:
        /* Conn disconnected, notify the app */
        HidFreeConn(Conn);
        break;

    case L2EVENT_PACKET_HANDLED:
        /* Notify the application */
        connParms.event = HID_CONN_EVENT_DATA_SENT;
        Conn->callback(Conn, &connParms);
        break;
    }
}

/*---------------------------------------------------------------------------
 *            HidConnConnected()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Connected state.
 *
 */
static void HidConnConnected(HidConn *Conn, L2capCallbackParms *Parms)
{
    HidConnCallbackParms connParms;

    switch (Parms->event) {
    case L2EVENT_DISCONNECTED:
        /* Conn disconnected, notify the app */
        HidFreeConn(Conn);
        break;

    case L2EVENT_PACKET_HANDLED:
        /* Notify the application */
        Conn->btPacketNum++;
        connParms.event = HID_CONN_EVENT_DATA_SENT;
        Conn->callback(Conn, &connParms);
        break;

    case L2EVENT_DATA_IND:
        connParms.event = HID_CONN_EVENT_DATA_IND;
        connParms.dataLen = Parms->dataLen;
        connParms.ptrs.data = Parms->ptrs.data;
        Conn->callback(Conn, &connParms);
        break;
    }
}

/*---------------------------------------------------------------------------
 *            HidConnInitStateMachine()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initializes the state table.
 *
 */
void HidConnInitStateMachine(void)
{ 
    HIDP(intConnState)[HID_STATE_DISCONNECTED] = HidConnDisconnected;
    HIDP(intConnState)[HID_STATE_CONN_PENDING] = HidConnConnPending;
    HIDP(intConnState)[HID_STATE_CONN_INCOMING] = HidConnConnIncoming;
    HIDP(intConnState)[HID_STATE_DISC_PENDING] = HidConnDiscPending;
    HIDP(intConnState)[HID_STATE_DISC_INCOMING] = HidConnDiscIncoming;
    HIDP(intConnState)[HID_STATE_CONNECTED] = HidConnConnected;

    HIDP(ctlConnState)[HID_STATE_DISCONNECTED] = HidConnDisconnected;
    HIDP(ctlConnState)[HID_STATE_CONN_PENDING] = HidConnConnPending;
    HIDP(ctlConnState)[HID_STATE_CONN_INCOMING] = HidConnConnIncoming;
    HIDP(ctlConnState)[HID_STATE_DISC_PENDING] = HidConnDiscPending;
    HIDP(ctlConnState)[HID_STATE_DISC_INCOMING] = HidConnDiscIncoming;
    HIDP(ctlConnState)[HID_STATE_CONNECTED] = HidConnConnected;
}

/*---------------------------------------------------------------------------
 *            HidConnFindCtlChannel()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Find the AV channel matching the remote device.
 *
 */
HidpChannel *HidConnFindCtlChannel(BtRemoteDevice *remDev)
{
    HidpChannel    *chnl;

    /* Find the AV channel associated with this device */
    chnl = (HidpChannel *)GetHeadList(&HIDP(chnlList));
    while (chnl != (HidpChannel *)&HIDP(chnlList)) {
        if (chnl->ctlconn.remDev == remDev) {
            /* Found it */
            break;
        }

        /* Get the next connection structure */
        chnl = (HidpChannel *)GetNextNode(&chnl->node);
    }

    if (chnl == (HidpChannel *)&HIDP(chnlList)) {
        /* Channel was not found */
        chnl = 0;
    }

    return chnl;
}

HidpChannel *HidConnFindIntChannel(BtRemoteDevice *remDev)
{
    HidpChannel    *chnl;

    /* Find the AV channel associated with this device */
    chnl = (HidpChannel *)GetHeadList(&HIDP(chnlList));
    while (chnl != (HidpChannel *)&HIDP(chnlList)) {
        if (chnl->intconn.remDev == remDev) {
            /* Found it */
            break;
        }

        /* Get the next connection structure */
        chnl = (HidpChannel *)GetNextNode(&chnl->node);
    }

    if (chnl == (HidpChannel *)&HIDP(chnlList)) {
        /* Channel was not found */
        chnl = 0;
    }

    return chnl;
}

/*---------------------------------------------------------------------------
 *            HidConnFindFreeChannel()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Find a free Ctl channel.
 *
 */
HidpChannel *HidConnFindFreeChannel(void)
{
    HidpChannel    *chnl;

    /* Find the AV channel associated with this device */
    chnl = (HidpChannel *)GetHeadList(&HIDP(chnlList));
    while (chnl != (HidpChannel *)&HIDP(chnlList)) {
        if (chnl->ctlconn.state == HID_STATE_DISCONNECTED) {
            /* Found it */
            break;
        }

        /* Get the next connection structure */
        chnl = (HidpChannel *)GetNextNode(&chnl->node);
    }

    if (chnl == (HidpChannel *)&HIDP(chnlList)) {
        /* Channel was not found */
        chnl = 0;
    }

    return chnl;
}

/*---------------------------------------------------------------------------
 *            HidCtlL2Callback()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  L2CAP callback routine.
 *
 */

void HidL2CtlCallback(L2capChannelId L2ChannelId, L2capCallbackParms *Parms)
{
    HidpChannel    *chnl;
    HidConn       *conn = 0;

    Report(("hidconn: ctl event = %d\n", Parms->event));
    chnl = HidConnFindCtlChannel(Parms->aclLink);
    if (chnl) {
        /* Found a matching channel */
        if (Parms->event != L2EVENT_CONNECT_IND) {
            conn = &chnl->ctlconn;
        } else {
            /* Already connected. Fail this one. */
            (void)L2CAP_ConnectRsp(L2ChannelId,
                                   L2CONN_REJECT_NO_RESOURCES, 0);
            return;
        }
    } else {
        /* No channel found */
        if (Parms->event == L2EVENT_CONNECT_IND) {
            /* No channel, search for connection */
            chnl = HidConnFindFreeChannel();
            if (chnl) {
                conn = &chnl->ctlconn;
                conn->l2ChannelId = L2ChannelId;
            } else {
                /* No channel to connect to */
                (void)L2CAP_ConnectRsp(L2ChannelId, 
                                       L2CONN_REJECT_NO_RESOURCES, 0);
                return;
            }
        } else {
            return;
        }
    }

    /* Call the state machine */
    HIDP(ctlConnState)[conn->state](conn, Parms);
}

/*---------------------------------------------------------------------------
 *            HidIntL2Callback()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  L2CAP callback routine.
 *
 */
void HidL2IntCallback(L2capChannelId L2ChannelId, L2capCallbackParms *Parms)
{
    HidpChannel    *chnl;
    HidConn       *conn = 0;

    Report(("hidconn int event = %d\n", Parms->event));

    if (Parms->event == L2EVENT_CONNECT_IND)
        chnl = HidConnFindCtlChannel(Parms->aclLink);
    else
        chnl = HidConnFindIntChannel(Parms->aclLink);

    if(chnl)
    {
        conn = &chnl->intconn;

        if (Parms->event == L2EVENT_CONNECT_IND) {
            conn->l2ChannelId = L2ChannelId;
        }
    }
    else
    {
        if (Parms->event == L2EVENT_CONNECT_IND) {
            /* Control need to connect first */
            (void)L2CAP_ConnectRsp(L2ChannelId, 
                                   L2CONN_REJECT_NO_RESOURCES, 0);
        }
        return;
    }

    /* Call the state machine */
    HIDP(intConnState)[conn->state](conn, Parms);
}


/*---------------------------------------------------------------------------
 *            HidFreeConn()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Clear the connection state and notify the app.
 *
 */
static void HidFreeConn(HidConn *Conn) 
{
    HidConnCallbackParms connParms;

    if (Conn->state != HID_STATE_DISCONNECTED) {

        /* Clean up the state */
        Conn->state = HID_STATE_DISCONNECTED;
        Conn->l2ChannelId = 0;

        /* Notify the application */
        connParms.event = HID_CONN_EVENT_DISCONNECT;
        connParms.ptrs.remDev = Conn->remDev;
        Conn->remDev = 0;
        Conn->callback(Conn, &connParms);
    }
}

/*---------------------------------------------------------------------------
 *            HidInitConn()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initialize the connection structure.
 *
 */
void HidInitConn(HidConn *conn, HidConnCallback callback)
{
    /* Initialize the connection structure */
    conn->remDev = 0;
    conn->callback = callback;
    conn->state = HID_STATE_DISCONNECTED;
    conn->l2ChannelId = 0;
    conn->btPacketIdx = 0;
    conn->btPacketNum = CONN_BT_PACKET_NUM;
    OS_MemSet((U8*)&(conn->btPacket), 0, sizeof(BtPacket));
}

/*---------------------------------------------------------------------------
 *            HidDisconnect()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Disconnect the L2CAP channel.
 *
 */
BtStatus HidDisconnect(HidConn *Conn)
{
    BtStatus status = BT_STATUS_SUCCESS;

    if (Conn->state != HID_STATE_DISCONNECTED) {
        status = L2CAP_DisconnectReq(Conn->l2ChannelId);
        if (status == BT_STATUS_PENDING) {
            /* Disconnect Started, wait for L2CAP callback */
            Conn->state = HID_STATE_DISC_PENDING;
        }
    }

    return status;
}

/*---------------------------------------------------------------------------
 *            HidCtlConnect()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Create an L2CAP channel.
 *
 */
BtStatus HidCtlConnect(HidConn *Conn, BtRemoteDevice *RemDev)
{
    BtStatus        status;

    /* Establish the L2CAP link */
    status = L2CAP_ConnectReq(&HIDP(ctlpsm), BT_PSM_HID_CTRL, 
                              RemDev, 0, 
                              &Conn->l2ChannelId);

    if (status == BT_STATUS_PENDING) {
        /* Connection Initiated */
        Conn->remDev = RemDev;
        Conn->state = HID_STATE_CONN_PENDING;
    }

    return status;
}

/*---------------------------------------------------------------------------
 *            HidIntConnect()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Create an L2CAP channel.
 *
 */
BtStatus HidIntConnect(HidConn *Conn, BtRemoteDevice *RemDev)
{
    BtStatus        status;

    /* Establish the L2CAP link */
    status = L2CAP_ConnectReq(&HIDP(intpsm), BT_PSM_HID_INTR, 
                              RemDev, 0, 
                              &Conn->l2ChannelId);

    if (status == BT_STATUS_PENDING) {
        /* Connection Initiated */
        Conn->remDev = RemDev;
        Conn->state = HID_STATE_CONN_PENDING;
    }

    return status;
}

/*---------------------------------------------------------------------------
 *            HidConnectRsp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Respond to the incoming L2CAP connection.
 *
 */
BtStatus HidConnectRsp(HidConn *Conn, BOOL Accept)
{
    BtStatus status;

    /* Incoming connection, accept it, go to conn incoming state */
    if (Accept) {
        status = L2CAP_ConnectRsp(Conn->l2ChannelId, L2CONN_ACCEPTED, 0);
    } else {
        status = L2CAP_ConnectRsp(Conn->l2ChannelId, L2CONN_REJECT_NO_RESOURCES,
                                  0);
    }

    if (status == BT_STATUS_PENDING) {
        Conn->state = HID_STATE_CONN_INCOMING;
    }

    return status;
}

/*---------------------------------------------------------------------------
 *            HidIsConnect()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Determine if HIDP is connected.
 *
 */
BOOL HidIsConnected(HidConn *Conn)
{
    if (Conn->state == HID_STATE_DISCONNECTED) {
        return FALSE;
    }

    return TRUE;
}

BtStatus HidSendData(HidConn *conn, U16 datalen,  U8 *data)
{
if(conn->btPacketNum)
{
        conn->btPacketIdx++;
        if(conn->btPacketIdx == CONN_BT_PACKET_NUM)
        {
            conn->btPacketIdx = 0;
        }
        conn->btPacket[conn->btPacketIdx].dataLen = datalen;
        conn->btPacket[conn->btPacketIdx].data = data;
        conn->btPacket[conn->btPacketIdx].headerLen = 0;
        conn->btPacket[conn->btPacketIdx].tailLen = 0;
   conn->btPacketNum --;
        return L2CAP_SendData(conn->l2ChannelId, &(conn->btPacket[conn->btPacketIdx]));
}
return BT_STATUS_BUSY;
}


