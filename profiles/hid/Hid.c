#include "sys/hidi.h"

#if XA_CONTEXT_PTR == XA_ENABLED
/* The stack context is accessed through a pointer. Normally in this case
 * the stack context would be dynamically allocated. For testing purposes
 * the pointer points to a global structure.
 */
BtHidContext  avTmp;
BtHidContext *hidContext = &avTmp;
#else /* XA_CONTEXT_PTR == XA_ENABLED */
BtHidContext hidContext;
#endif /* XA_CONTEXT_PTR */
static void HidCmgrCallback(CmgrHandler *cHandler, 
                              CmgrEvent Event, 
                              BtStatus Status);

U8 hidctldata[20];
/*---------------------------------------------------------------------------
 * HID_Init()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initialize the HID SDK.  This function should only be called
 *            once, normally at initialization time.  The calling of this 
 *            function can be specified in overide.h using the XA_LOAD_LIST 
 *            macro (i.e. #define XA_LOAD_LIST XA_MODULE(AV)).
 *
 * Return:    (See header file)
 */
BOOL HID_Init(void)
{
    BOOL status;

    /* Initialize Memory */
    status = HidAlloc();
    if (status) {
        HID(hid_prot_type) = HID_BOOT_PROTOCOL;
        status = HIDP_Init();
    }

    return status;
}

/*---------------------------------------------------------------------------
 * HidpCallback()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Callback function for AVCTP.
 *
 * Return:    void
 */
void HidpCB(HidpChannel *Chnl, HidpCallbackParms *Parms)
{
    HidChannel *chnl = ContainingRecord(Chnl, HidChannel, chnl);
    HidCallbackParms parms;

    switch(Parms->event)
    {
        case HIDP_EVENT_CTL_DISCONNECT: 
        {
            /* Release link handler */
            (void)CMGR_RemoveDataLink(&chnl->cmgrHandler);
            (void)CMGR_DeregisterHandler(&chnl->cmgrHandler);
            parms.event = HID_EVENT_DISCONNECT;

            break;
        }
        
        case HIDP_EVENT_CTL_CONNECT:
        {
            //keep in mind
            return;
        }

        case HIDP_EVENT_CTL_CONNECT_IND:
        {
            HIDP_CtlConnectRsp(Chnl, TRUE);
            return;
        }

        case HIDP_EVENT_INT_CONNECT:
        {
            //pass to application layer;
            Report(("HID: HIDP_EVENT_INT_CONNECT\n"));
            parms.event = HID_EVENT_CONNECT;
            break;
        }

        case HIDP_EVENT_INT_CONNECT_IND:
        {
            HIDP_IntConnectRsp(Chnl, TRUE);
            return;
        }

        case HIDP_EVENT_INT_DATA_SENT:
            Report(("HID: HIDP_EVENT_INT_DATA_SENT\n"));
            parms.event = HID_EVENT_INT_DATA_SENT;
            parms.p.remDev = Parms->p.remDev;
            break;
        case HIDP_EVENT_INT_DATA_IND:
        {
            Report(("HID: HIDP_EVENT_INT_DATA_IND\n"));               
            break;
        }
        case HIDP_EVENT_CTL_DATA_IND:
        {
            if(Parms->datalen > 0)
            {
                U8 THdr;
                THdr = Parms->p.data[0];
                switch(THdr >> 4) //THdr
                {
                    case 0x1: //HID_CONTROL
                    if((THdr & 0xf) == HID_CONTROL_VIRTUAL_CABLE_UNPLUG)
                    {
                        Report(("HID: Virtual Cable unplug\n"));
                    }
                    break;
                    case 0x04: //GET_REPORT
                        Report(("HID: Get report received\n"));
                        hidctldata[0] = 0x03; //unsupport request
                        HIDP_CtlSendData(Chnl, 1, hidctldata);
                        break;
                    case 0x05: //SET_REPORT
                        Report(("HID: Set report received\n"));
                        hidctldata[0] = 0x03; //unsupport request
                        HIDP_CtlSendData(Chnl, 1, hidctldata);
                        break;
                    case 0x06: //GET_PROTOCOL
                        Report(("HID: Get protocol received\n"));
                        hidctldata[0] = HID(hid_prot_type);
                        HIDP_CtlSendData(Chnl, 1, hidctldata);
                        break;
                    case 0x07: //SET_PROTOCOL
                        Report(("HID: Set protocol received\n"));
                        #if 1
                        HID(hid_prot_type) = THdr & 0xf;
                        
                        hidctldata[0] = 0x00; //success
                        HIDP_CtlSendData(Chnl, 1, hidctldata);
                        #endif
                        break;
                    case 0x08: //GET_IDLE
                        Report(("HID: Get idle received\n"));
                        hidctldata[0] = 0x00;
                        HIDP_CtlSendData(Chnl, 1, hidctldata);
                        break;
                    case 0x09: //SET_IDLE
                        Report(("HID: Set idle received\n"));
                        HID(hid_prot_type) = THdr & 0xf;
                        hidctldata[0] = 0x00;//success 3; //unsupport request
                        HIDP_CtlSendData(Chnl, 1, hidctldata);
                        break;

                    default:
                        Report(("HID: Unknown control received = 0x%x\n", THdr));
                        break;
                }
            }
            return; //temporary
        }
        default:
            return;
            break;
    }

    parms.channel = chnl;
    parms.status = Parms->status;

    /* Assigns the pointer in the "p" union (whatever it is) */
    parms.p.remDev = Parms->p.remDev;

    chnl->callback(chnl, &parms);
}


/*---------------------------------------------------------------------------
 * HID_Register()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Registers an application callback to create/receive HID 
 *            connections.  This function must be called before any other 
 *            HID functions.
 *
 * Return:    (See header file)
 */
BtStatus HID_Register(HidChannel *chnl, HidCallback callback)
{
    BtStatus status;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, chnl != 0);
    CheckUnlockedParm(BT_STATUS_INVALID_PARM, callback != 0);

    OS_LockStack();

    status = HIDP_Register(&chnl->chnl, HidpCB);
    if (status == BT_STATUS_SUCCESS) {
        chnl->callback = callback;
        OS_MemSet((U8*)&chnl->cmgrHandler, 0, sizeof(CmgrHandler));
        /* Register with SDP */
        status = HidRegisterSdp(chnl);
        if (status != BT_STATUS_SUCCESS) {
            HIDP_Deregister(&chnl->chnl);
        }

       // DidRegisterSdp(2); //jis
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 * HID_Deregister()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  De-registers the HID callback. After making this call
 *            successfully, the callback specified in HID_Register will
 *            receive no further events.
 *
 * Return:    (See header file)
 */
BtStatus HID_Deregister(HidChannel *chnl)
{
    BtStatus status;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, chnl != 0);

    OS_LockStack();

    status = HIDP_Deregister(&chnl->chnl);
    if (status == BT_STATUS_SUCCESS) {
        /* Deregister SDP */
        HidDeregisterSdp(chnl);
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 * HID_Connect()
 *---------------------------------------------------------------------------
 *
 * Synopsis: Connect to the remote device.
 *
 * Return:   (See header file)
 */
BtStatus HID_Connect(HidChannel *chnl, BD_ADDR *addr)
{
    BtStatus status;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, chnl != 0);

    OS_LockStack();

    status = CMGR_RegisterHandler(&chnl->cmgrHandler, HidCmgrCallback);
    if (status == BT_STATUS_SUCCESS) {
        status = CMGR_CreateDataLink(&chnl->cmgrHandler, addr);
		if(status != BT_STATUS_SUCCESS || status != BT_STATUS_PENDING)
		{
		    (void)CMGR_DeregisterHandler(&chnl->cmgrHandler);
		}
    }

    OS_UnlockStack();

    return status;
}


/*---------------------------------------------------------------------------
 * HID_Disconnect()
 *---------------------------------------------------------------------------
 *
 * Synopsis: Disconnect from the remote device.
 *
 * Return:   (See header file)
 */
BtStatus HID_Disconnect(HidChannel *chnl)
{
    BtStatus status;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, chnl != 0);

    OS_LockStack();

    /* Release link handler */
    status = HIDP_Disconnect(&chnl->chnl);

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 * HidAlloc()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initializes the global context for the AV profile.  This 
 *            function can be replaced if necessary.
 *
 * Return:    void
 */
BOOL HidAlloc(void)
{
    U8* ptr; 

    /* Fill memory with 0. */
#if XA_CONTEXT_PTR == XA_ENABLED
    ptr = (U8*)hidContext;
#else
    ptr = (U8*)&hidContext;
#endif /* XA_CONTEXT_PTR */

    OS_MemSet(ptr, 0, (U32)sizeof(BtHidContext));

    return TRUE;
}

/*---------------------------------------------------------------------------
 *            HidReportFailedConnect()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Report a failed connection attempt
 *
 */
void HidReportFailedConnect(HidChannel *chnl, BtRemoteDevice *btrem)
{
    HidCallbackParms  info;

    /* Release link handler */
    (void)CMGR_RemoveDataLink(&chnl->cmgrHandler);
    (void)CMGR_DeregisterHandler(&chnl->cmgrHandler);

    /* Indicate the response to the application */
    info.event = HID_EVENT_DISCONNECT;
    info.channel = chnl;
    info.p.remDev = btrem;
    chnl->callback(chnl, &info);
}

/*---------------------------------------------------------------------------
 *            HidCmgrCallback()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Called by device manager with link state events.
 *
 */
static void HidCmgrCallback(CmgrHandler *cHandler, 
                              CmgrEvent Event, 
                              BtStatus Status)
{
    HidChannel   *chnl = ContainingRecord(cHandler, HidChannel, cmgrHandler);
//    HidCallbackParms parms;
    
    switch (Event) {
    
    case CMEVENT_DATA_LINK_CON_CNF:
        if (Status == BT_STATUS_SUCCESS) {
            if(HIDP_Connect(&chnl->chnl, cHandler->remDev) == BT_STATUS_PENDING)
            return;
        }

        /* Disconnect and call the application */
        HidReportFailedConnect(chnl, cHandler->remDev);
        break;
    case CMEVENT_ENCRYPTION_CHANGE:
        break;
    case CMEVENT_DATA_LINK_DIS:
        /* Disconnect and call the application */
        HidReportFailedConnect(chnl, cHandler->remDev);
        break;
    default:
        Assert(0);
        break;
    }
}

BtStatus HID_IntSendData(HidChannel *chnl, U16 datalen,  U8 *data)
{
    BtStatus sts = BT_STATUS_FAILED;
    
    if(datalen)
    {
        if(data[0] == 0xa1) //input type data only
        {
            return HIDP_IntSendData(&chnl->chnl, datalen, data);
        }
    }

    return sts;
}

