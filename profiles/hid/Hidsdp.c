#include "bttypes.h"
#include "btalloc.h"
#include "sys/hidi.h"

/****************************************************************************
 *
 * ROMable data
 *
 ****************************************************************************/
/*---------------------------------------------------------------------------
 * HID Language ID Base List.
 *
 * Defines how Bluetooth strings are mapped to HID LANGID and string 
 * indices.  For a complete description, see section 7.11.7 of the
 * Bluetooth HID Profile specification.  The default value is a sample
 * that supports only United States English.
 */
static const U8 Hid_LangIdBaseList[] = {
    SDP_ATTRIB_HEADER_8BIT(8),   /* Data element sequence */

    /* Each element of the list is a data element sequence describing a HID
     * language ID base.  Only one element is included.
     */
    SDP_ATTRIB_HEADER_8BIT(6),   /* Data element sequence. */
    SDP_UINT_16BIT(0x0409),      /* Language = English (United States) */
    SDP_UINT_16BIT(0x0100)       /* Bluetooth String Offset */

    /* Additional languages may be defined.  Each language is defined by
     * a data element sequence with 2 elements.
     *
     * The first element defines the language and is encoded according to the
     * the "Universal Serial Bus Language Identifiers (LANGIDs)" specification.
     *
     * The second element defines the Bluetooth base attribute ID as is defined
     * in the Language Base Attribute ID List (see HidLangBaseAttrIdList above).
     *
     * Headers must be added along with the actual element (HID Language ID
     * base) data.  Also,  the length in the first header must be adjusted.
     */
};

static const U8 HidServiceRecordHandleVal[] = {
    SDP_UINT_32BIT(0x00010002)
};

static const U8 HidServClassIdVal[] = {
    SDP_ATTRIB_HEADER_8BIT(3),           /* Data element sequence, 3 bytes */
    SDP_UUID_16BIT(SC_HUMAN_INTERFACE_DEVICE), /* Uuid16 HID */
};

/* Attribute value of Hid Protocol Descriptor List. */
static U8 HidProtocolDescListVal[] = {
    SDP_ATTRIB_HEADER_8BIT(13),  /* Data element sequence, 12 bytes */
    SDP_ATTRIB_HEADER_8BIT(6),   /* Data element sequence for L2CAP, 3 bytes */     
    SDP_UUID_16BIT(PROT_L2CAP),  /* Uuid16 L2CAP */
    SDP_UINT_16BIT(BT_PSM_HID_CTRL),  /* Uuid16 PSM */    
    SDP_ATTRIB_HEADER_8BIT(3),   /* Data element sequence for RFCOMM, 5 bytes */
    SDP_UUID_16BIT(PROT_HIDP),  /* Uuid16 HID protocol */};

/* Attribute value of Hid Language Base ID List */
static const U8 HidLangBaseVal[] = {
    SDP_ATTRIB_HEADER_8BIT(9),   /* Data element sequence, 9 bytes */
    SDP_UINT_16BIT(0x656E),      /* Uint16 "en" (English) */
    SDP_UINT_16BIT(0x006A),      /* Uint16 UTF-8 encoding */
    SDP_UINT_16BIT(0x0100)       /* Uint16 primary language base ID */
};

/* Attribute value of Hid Bluetooth Profile Descriptor List */
static const U8 HidProfileDescListVal[] = {
    SDP_ATTRIB_HEADER_8BIT(8),           /* Data element sequence, 8 bytes */
    SDP_ATTRIB_HEADER_8BIT(6),           /* Data element sequence for ProfileDescriptor, 6 bytes */
    SDP_UUID_16BIT(SC_HUMAN_INTERFACE_DEVICE),          /* Uuid16 Hid */
    SDP_UINT_16BIT(0x0100)               /* Uint16 version number */
};

/* Attribute value of Hid Protocol Descriptor List. */
static U8 HidAdditionalProtocolDescListVal[] = {
    SDP_ATTRIB_HEADER_8BIT(15),  /* Data element sequence, 15 bytes */
    SDP_ATTRIB_HEADER_8BIT(13),  /* Data element sequence, 15 bytes */
    SDP_ATTRIB_HEADER_8BIT(6),   /* Data element sequence for L2CAP, 3 bytes */     
    SDP_UUID_16BIT(PROT_L2CAP),  /* Uuid16 L2CAP */
    SDP_UINT_16BIT(BT_PSM_HID_INTR),  /* Uuid16 PSM */    
    SDP_ATTRIB_HEADER_8BIT(3),   /* Data element sequence for RFCOMM, 5 bytes */
    SDP_UUID_16BIT(PROT_HIDP),  /* Uuid16 HID protocol */};


/*---------------------------------------------------------------------------
 * * OPTIONAL *  ServiceName
 *
 * This is the English string.  Other languguages can be defined.
 */
static const U8 Hid_ServiceName[] = {
    SDP_TEXT_8BIT(19),          /* Null terminated text string */
    'B', 'T', ' ', 'T', 'a', 'x', 'e', 'i', 'a', ' ', 'H','I', 
    'D', ' ', 'K', 'M', 'S', 'H' , '\0'
};

/*---------------------------------------------------------------------------
 * * OPTIONAL *  ServiceDescription
 *
 * This is the English string.  Other languguages can be defined.
 */
static const U8 Hid_ServiceDescription[] = {
    SDP_TEXT_8BIT(17),          /* Null terminated text string */
    'S', 'h', 'a', 'r', 'e', 'K', 'e', 'y', 'b', 'o', 'a', 
    'r', 'd', 'M', 'o', 'u', '\0'
};

/*---------------------------------------------------------------------------
 * * OPTIONAL *  ProviderName
 *
 * This is the English string.  Other languguages can be defined.
 */
static const U8 Hid_ProviderName[] = {
    SDP_TEXT_8BIT(9),          /* Null terminated text string */
    'T', 'a', 'x', 'e', 'i', 'a', ' ', ' ', '\0'
};

/*---------------------------------------------------------------------------
 * Device Release Number.
 */
#define SDP_HID_DEVICE_RELEASE 0x0100
static const U8 Hid_DeviceRelease[] = {
    SDP_UINT_16BIT(SDP_HID_DEVICE_RELEASE)
};

/*---------------------------------------------------------------------------
 * Parser Version.
 */
#define SDP_HID_PARSER_VERSION 0x0111
static const U8 Hid_ParserVersion[] = {
    SDP_UINT_16BIT(SDP_HID_PARSER_VERSION)
};

/*---------------------------------------------------------------------------
 * Device Subclass.
 */
//keyboard 0x40
//mouse 0x80
#define SDP_HID_DEVICE_SUBCLASS   ((U8)(0x00000040))
static const U8 Hid_DeviceSubclass[] = {
    SDP_UINT_8BIT(SDP_HID_DEVICE_SUBCLASS)
};

/*---------------------------------------------------------------------------
 * Country Code.
 */
#define SDP_HID_COUNTRY_CODE 0x21
static const U8 Hid_Countrycode[] = {
    SDP_UINT_8BIT(SDP_HID_COUNTRY_CODE)
};

/*---------------------------------------------------------------------------
 * Virtual Cable.
 */
#define SDP_HID_VIRTUAL_CABLE TRUE
static const U8 Hid_VirtualCable[] = {
    SDP_BOOL(SDP_HID_VIRTUAL_CABLE)
};

/*---------------------------------------------------------------------------
 * Initiate Reconnect.
 */
#define SDP_HID_RECONNECT_INITIATE TRUE
static const U8 Hid_ReconnectInitiate[] = {
    SDP_BOOL(SDP_HID_RECONNECT_INITIATE)
};

/*---------------------------------------------------------------------------
 * Descriptor List.
 */

#define SDP_HID_DESCRIPTOR_LEN 139  
#define SDP_HID_DESCRIPTOR_TYPE 0x22
#define SDP_HID_DESCRIPTOR \
0x05, 0x01,           /* Usage Page (Generic Desktop)      */ \
0x09, 0x02,           /* Usage (Mouse)                     */ \
0xA1, 0x01,           /* Collection (Application)          */ \
0x85, 0x02,           /* REPORT_ID (2)                     */ \
0x09, 0x01,           /* Usage (Pointer)                   */ \
0xA1, 0x00,           /* Collection (Application)          */ \
0x05, 0x09,           /* Usage Page (Button)               */ \
0x19, 0x01,           /* Usage Minimum (1)                 */ \
0x29, 0x08,           /* Usage Maximum (8)                 */ \
0x15, 0x00,           /* Logical Minimum (0)               */ \
0x25, 0x01,           /* Logical Maximum (1)               */ \
0x75, 0x01,           /* Report Size (1)                   */ \
0x95, 0x08,           /* Report Count (8)                  */ \
0x81, 0x02,           /* Input (Data, Variable, Absolute)  */ \
0x05, 0x01,           /* Usage Page (Generic Desktop)      */ \
0x09, 0x30,           /* Usage X                           */ \
0x09, 0x31,           /* Usage Y                           */ \
0x09, 0x38,           /* Usage Wheel                       */ \
0x15, 0x81,           /* Logical Minimum (-127)            */ \
0x25, 0x7F,           /* Logical Maximum (127)             */ \
0x75, 0x08,           /* Report Size (8)                   */ \
0x95, 0x03,           /* Report Count (3)                  */ \
0x81, 0x06,           /* Input (Data, Variable, Absolute)  */ \
0xC0, 0xC0,           /* End Collection                    */ \
                      /* End Collection                    */ \
0x05, 0x01,           /* Usage Page (Generic Desktop)      */ \
0x09, 0x06,           /* Usage (Keyboard)                  */ \
0xA1, 0x01,           /* Collection (Application)          */ \
0x85, 0x01,           /* REPORT_ID (1)                     */ \
0x75, 0x01,           /* Report Size (1)                   */ \
0x95, 0x08,           /* Report Count (8)                  */ \
0x05, 0x07,           /* Usage Page (Key codes)            */ \
0x19, 0xE0,           /* Usage Minimum (224)               */ \
0x29, 0xE7,           /* Usage Maximum (231)               */ \
0x15, 0x00,           /* Logical Minimum (0)               */ \
0x25, 0x01,           /* Logical Maximum (1)               */ \
0x81, 0x02,           /* Input (Data, Variable, Absolute)  */ \
0x95, 0x01,           /* Report Count (1)                  */ \
0x75, 0x08,           /* Report Size (8)                   */ \
0x81, 0x03,           /* Input (xxxxxx)                    */ \
0x95, 0x05,           /* Report Count (5)                  */ \
0x75, 0x01,           /* Report Size (1)                   */ \
0x05, 0x08,           /* Usage Page (LED)                  */ \
0x19, 0x01,           /* Usage Minimum (1)                 */ \
0x29, 0x05,           /* Usage Maximum (5)                 */ \
0x91, 0x02,           /* Output (Data, Variable, Absolute) */ \
0x95, 0x01,           /* Report Count (1)                  */ \
0x75, 0x03,           /* Report Size (3)                   */ \
0x91, 0x03,           /* Output (Constant)                 */ \
0x95, 0x06,           /* Report Count (6)                  */ \
0x75, 0x08,           /* Report Size (8)                   */ \
0x15, 0x00,           /* Logical Minimum (0)               */ \
0x26, 0xFF, 0x00,     /* Logical Minimum (255)             */ \
0x05, 0x07,           /* Usage Page (Key codes)            */ \
0x19, 0x00,           /* Usage Minimum (0)                 */ \
0x29, 0xFF,           /* Usage Maximum (255)               */ \
0x81, 0x00,           /* Input (Data, Array)               */ \
0xC0,                 /* END_COLLECTION                    */ \
0x05, 0x0C,           /* USAGE_PAGE (Consumer Devices)     */ \
0x09, 0x01,           /* USAGE (Consumer Control)          */ \
0xA1, 0x01,           /* COLLECTION (Application)          */ \
0x85, 0x03,           /* REPORT_ID (3)                     */ \
0x15, 0x01,           /* LOGICAL_MINIMUM (1)               */ \
0x26, 0x9C, 0x02,     /* LOGICAL_MAXIMUM (652)             */ \
0x75, 0x10,           /* REPORT_SIZE (16)                  */ \
0x95, 0x02,           /* REPORT_COUNT (2)                  */ \
0x19, 0x01,           /* USAGE_MINIMUM (Consumer Control)  */ \
0x2A, 0x9C, 0x02,     /* USAGE_MAXIMUM (Police Alarm)      */ \
0x81, 0x60,           /* INPUT (Data,Ary,Abs,NPrf,Null)    */ \
0xC0                  /* END_COLLECTION                    */


static const U8 Hid_DescriptorList[] = {
    SDP_ATTRIB_HEADER_8BIT(SDP_HID_DESCRIPTOR_LEN + 6), /* Data element sequence */

    /* Each element of the list is a HID descriptor which is a
     * data element sequence.
     */
    SDP_ATTRIB_HEADER_8BIT(SDP_HID_DESCRIPTOR_LEN + 4),
    SDP_UINT_8BIT(SDP_HID_DESCRIPTOR_TYPE),  /* Report Descriptor Type */

    /* Length of the HID descriptor */
    SDP_TEXT_8BIT(SDP_HID_DESCRIPTOR_LEN),

    /* The actual descriptor is defined in hid.h or in overide.h */
    SDP_HID_DESCRIPTOR

    /* Addition descriptors may be added, but the header must be added
     * along with the actual descriptor data.  Also, all lengths in the 
     * headers above must be adjusted.
     */
};

/*---------------------------------------------------------------------------
 * Profile Version
 */
static const U8 Hid_ProfileVersion[] = {
    SDP_UINT_16BIT(0x0100)
};

/*---------------------------------------------------------------------------
 * Boot Device
 */
#define SDP_HID_BOOT_DEVICE TRUE

static const U8 Hid_BootDevice[] = {
    SDP_BOOL(SDP_HID_BOOT_DEVICE)
};

/*---------------------------------------------------------------------------
 * * OPTIONAL *  Hid_SdpDisable
 */
static const U8 Hid_SdpDisable[] = {
    SDP_BOOL(FALSE)
};

/*---------------------------------------------------------------------------
 * * OPTIONAL *  Hid_BatteryPower
 */
#define SDP_HID_BATTERY_POWER FALSE
static const U8 Hid_BatteryPower[] = {
    SDP_BOOL(SDP_HID_BATTERY_POWER)
};

/*---------------------------------------------------------------------------
 * * OPTIONAL *  Hid_RemoteWake
 */
#define SDP_HID_REMOTE_WAKE TRUE
static const U8 Hid_RemoteWake[] = {
    SDP_BOOL(SDP_HID_REMOTE_WAKE)
};

/*---------------------------------------------------------------------------
 * * OPTIONAL *  Hid_SupervisionTimeout
 */
#define SDP_HID_SUPERVISION_TIMEOUT 0x7D00

static const U8 Hid_SupervisionTimeout[] = {
    SDP_UINT_16BIT(SDP_HID_SUPERVISION_TIMEOUT)
};

/*---------------------------------------------------------------------------
 * * OPTIONAL *  Hid_NormallyConnectable
 */
#define SDP_HID_NORMALLY_CONNECTABLE TRUE
static const U8 Hid_NormallyConnectable[] = {
    SDP_BOOL(SDP_HID_NORMALLY_CONNECTABLE)
};


static const U8 DidServClassIdVal[] = {
    SDP_ATTRIB_HEADER_8BIT(3),           /* Data element sequence, 3 bytes */
    SDP_UUID_16BIT(SC_PNP_INFO), /* Uuid16 HID */
};

static U8 DidProtocolDescListVal[] = {
    SDP_ATTRIB_HEADER_8BIT(13),  /* Data element sequence, 12 bytes */
    SDP_ATTRIB_HEADER_8BIT(6),   /* Data element sequence for L2CAP, 3 bytes */     
    SDP_UUID_16BIT(PROT_L2CAP),  /* Uuid16 L2CAP */
    SDP_UINT_16BIT(BT_PSM_SDP),  /* Uuid16 PSM */    
    SDP_ATTRIB_HEADER_8BIT(3),   /* Data element sequence for RFCOMM, 5 bytes */
    SDP_UUID_16BIT(PROT_SDP),  /* Uuid16 HID protocol */};

static const U8 DidProfileDescListVal[] = {
    SDP_ATTRIB_HEADER_8BIT(8),           /* Data element sequence, 8 bytes */
    SDP_ATTRIB_HEADER_8BIT(6),           /* Data element sequence for ProfileDescriptor, 6 bytes */
    SDP_UUID_16BIT(SC_PNP_INFO),          /* Uuid16 Hid */
    SDP_UINT_16BIT(0x0100)               /* Uint16 version number */
};

static const U8 DidSpecIdVal[] = {
    SDP_UINT_16BIT(0x0103)
};

static const U8 DidVendorIdVal[] = {
    SDP_UINT_16BIT(0x05ac) //apple
};

static const U8 DidAnsiProductIdVal[] = {
    SDP_UINT_16BIT(0x0239)  //9 ansi, a iso, b jis
};

static const U8 DidIsoProductIdVal[] = {
    SDP_UINT_16BIT(0x023a)  //9 ansi, a iso, b jis
};

static const U8 DidJisProductIdVal[] = {
    SDP_UINT_16BIT(0x023b)  //9 ansi, a iso, b jis
};

static const U8 DidVersionVal[] = {
    SDP_UINT_16BIT(0x0213)//2.1.3, 
};

static const U8 DidPrimaryRecordVal[] = {
    SDP_BOOL(TRUE)
};

static const U8 DidVendorIdSourceVal[] = {
    SDP_UINT_16BIT(0x02)//usb
};

static SdpAttribute DidSdpAttribs[] = {
    SDP_ATTRIBUTE(AID_SERVICE_CLASS_ID_LIST, DidServClassIdVal), 
    SDP_ATTRIBUTE(AID_PROTOCOL_DESC_LIST, DidProtocolDescListVal),
    SDP_ATTRIBUTE(AID_BT_PROFILE_DESC_LIST, DidProfileDescListVal),
    SDP_ATTRIBUTE(AID_DID_SPEC_ID, DidSpecIdVal),
    SDP_ATTRIBUTE(AID_DID_VENDOR_ID, DidVendorIdVal),
    SDP_ATTRIBUTE(AID_DID_PRODUCT_ID, DidAnsiProductIdVal),
    SDP_ATTRIBUTE(AID_DID_VERSION, DidVersionVal),
    SDP_ATTRIBUTE(AID_DID_PRIMARY_RECORD, DidPrimaryRecordVal),
    SDP_ATTRIBUTE(AID_DID_VENDOR_ID_SOURCE, DidVendorIdSourceVal),
};

static SdpAttribute HidSdpAttribs[] = {
    /* Service Class ID List attribute */
    SDP_ATTRIBUTE(AID_SERVICE_CLASS_ID_LIST, HidServClassIdVal), 

    /* Protocol Descriptor List attribute */
    SDP_ATTRIBUTE(AID_PROTOCOL_DESC_LIST, HidProtocolDescListVal),
    /* Language base Id List attribute */
    SDP_ATTRIBUTE(AID_LANG_BASE_ID_LIST, HidLangBaseVal),
    /* Bluetooth Profile Descriptor List attribute */
    SDP_ATTRIBUTE(AID_BT_PROFILE_DESC_LIST, HidProfileDescListVal),
    /* Bluetooth Profile Descriptor List attribute */
    SDP_ATTRIBUTE(AID_ADDITIONAL_PROT_DESC_LISTS, HidAdditionalProtocolDescListVal),
    
	/* HID Service Name in English */
	SDP_ATTRIBUTE((AID_SERVICE_NAME + 0x0100), Hid_ServiceName),
	/* HID Service Description in English */
	SDP_ATTRIBUTE((AID_SERVICE_DESCRIPTION+ 0x0100), Hid_ServiceDescription),
    /* HID Provider Name in English*/
	SDP_ATTRIBUTE((AID_PROVIDER_NAME + 0x0100), Hid_ProviderName),

    /* Mandatory HID attributes */

    /* Device release number */
	SDP_ATTRIBUTE(AID_HID_DEVICE_RELEASE, Hid_DeviceRelease),
    /* HID parser version */
	SDP_ATTRIBUTE(AID_HID_PARSER_VERSION, Hid_ParserVersion),
    /* Device subclass */
	SDP_ATTRIBUTE(AID_HID_DEVICE_SUBCLASS, Hid_DeviceSubclass),
    /* Country Code */
	SDP_ATTRIBUTE(AID_HID_COUNTRY_CODE, Hid_Countrycode),
    /* Virtual Cable */
	SDP_ATTRIBUTE(AID_HID_VIRTUAL_CABLE,Hid_VirtualCable),
    /* Device initiates reconnect */
	SDP_ATTRIBUTE(AID_HID_RECONNECT_INIT,Hid_ReconnectInitiate),
    /* HID descriptor list */
	SDP_ATTRIBUTE(AID_HID_DESCRIPTOR_LIST, Hid_DescriptorList),
    /* Language ID Base List */
	SDP_ATTRIBUTE(AID_HID_LANG_ID_BASE_LIST,Hid_LangIdBaseList),

    /* Optional HID attibutes */

    /* SDP Disable/Enable */
	SDP_ATTRIBUTE(AID_HID_SDP_DISABLE,Hid_SdpDisable),
    /* Battery powered */
	SDP_ATTRIBUTE(AID_HID_BATTERY_POWER,Hid_BatteryPower),
    /* Device support of remote wakeup */
	SDP_ATTRIBUTE(AID_HID_REMOTE_WAKE,Hid_RemoteWake),

    /* Mandatory HID attribute */

    /* HID profile version*/
	SDP_ATTRIBUTE(AID_HID_PROFILE_VERSION,Hid_ProfileVersion),

    /* Optional HID attributes */

    /* Recommended supervision timeout */
	SDP_ATTRIBUTE(AID_HID_SUPERV_TIMEOUT,Hid_SupervisionTimeout),
    /* Device connectability */
	SDP_ATTRIBUTE(AID_HID_NORM_CONNECTABLE,Hid_NormallyConnectable),

    /* Support for boot protocol */
	SDP_ATTRIBUTE(AID_HID_BOOT_DEVICE,Hid_BootDevice),
};

/****************************************************************************
 *
 * SDP objects used to query HID services.
 *
 ****************************************************************************/

/****************************************************************************
 *
 * Function prototypes
 *
 ****************************************************************************/
SdpRecord DidSdpRecord;
BtStatus DidRegisterSdp(U8 ansi_iso_jis) //0, 1, 2
{
    BtStatus    status;
    SdpRecord  *sdpRecord;

    if(ansi_iso_jis == 0)
    {
//        DidSdpAttribs[5].value[2] = 0x39;
        DidSdpAttribs[5].value = DidAnsiProductIdVal;
    }
    else if(ansi_iso_jis == 1)
    {
//        DidSdpAttribs[5].value[2] = 0x3a;
        DidSdpAttribs[5].value = DidIsoProductIdVal;
    }
    else if(ansi_iso_jis == 2)
    {
//        DidSdpAttribs[5].value[2] = 0x3b;
        DidSdpAttribs[5].value = DidJisProductIdVal;
    }

    DidSdpRecord.attribs = DidSdpAttribs;
    DidSdpRecord.num = 9;
    sdpRecord = &DidSdpRecord;


    sdpRecord->classOfDevice = 0;
    
    status = SDP_AddRecord(sdpRecord);
    return status;
}

BtStatus DidDeregisterSdp()
{
    BtStatus status = BT_STATUS_SUCCESS;

    status = SDP_RemoveRecord(&DidSdpRecord);
  
    return status;
}


/*---------------------------------------------------------------------------
 *            HidRegisterSdp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Registers an SDP entry.
 *
 * Return:    BT_STATUS_FAILED
 *            (Also see SDP_AddRecord)
 */
BtStatus HidRegisterSdp(HidChannel *Chnl)
{
    BtStatus    status;
    SdpRecord  *sdpRecord;

    /* Configure the attributes */
    if (HID(ChannelCount)++ > 0) return BT_STATUS_SUCCESS;

    HID(SdpRecord).attribs = HidSdpAttribs;
    HID(SdpRecord).num = HID_NUM_ATTRIBUTES;
    sdpRecord = &HID(SdpRecord);

    sdpRecord->classOfDevice = COD_POSITIONING;
    
    status = SDP_AddRecord(sdpRecord);
    return status;
}


/*---------------------------------------------------------------------------
 *            HidDeregisterSdp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Deregisters an SDP entry.
 *
 * Return:    BT_STATUS_FAILED
 *            (Also see SDP_RemoveRecord)
 */
BtStatus HidDeregisterSdp(HidChannel *Chnl)
{
    BtStatus status = BT_STATUS_SUCCESS;

    if (--HID(ChannelCount) == 0) {
            status = SDP_RemoveRecord(&HID(SdpRecord));
    }
  
    return status;
}


