#include "sdp.h"
#include "sys/hidpi.h"
#include "sys/hidconn.h"

#if XA_CONTEXT_PTR == XA_ENABLED
/* The stack context is accessed through a pointer. Normally in this case
 * the stack context would be dynamically allocated. For testing purposes
 * the pointer points to a global structure.
 */
BtHidpContext  hidTmp;
BtHidpContext *hidpContext = &hidTmp;
#else /* XA_CONTEXT_PTR == XA_ENABLED */
BtHidpContext hidpContext;
#endif /* XA_CONTEXT_PTR */

static void HidCtlConnCb(HidConn *Conn, HidConnCallbackParms *Parms);
static void HidIntConnCb(HidConn *Conn, HidConnCallbackParms *Parms);

/*---------------------------------------------------------------------------
 * HIDP_Init()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initialize the HIDP SDK.  This function should only be called
 *            once, normally at initialization time.  The calling of this 
 *            function can be specified in overide.h using the XA_LOAD_LIST 
 *            macro (i.e. #define XA_LOAD_LIST XA_MODULE(AV)).
 *
 * Return:    (See header file)
 */
BOOL HIDP_Init(void)
{
    BOOL status = TRUE;

    /* Initialize Memory */
    status = HidpAlloc();

    /* Initialize the connection state machine */
    HidConnInitStateMachine();

    /* Register HIDP with L2CAP */
    HIDP(ctlpsm).callback = HidL2CtlCallback;
    HIDP(ctlpsm).psm = BT_PSM_HID_CTRL;
    HIDP(ctlpsm).localMtu = L2CAP_MINIMUM_MTU;//L2CAP_MAXIMUM_MTU;
    HIDP(ctlpsm).minRemoteMtu = L2CAP_MINIMUM_MTU;

#if L2CAP_FLOW_CONTROL == XA_ENABLED
    HIDP(ctlpsm).inLinkMode = HIDP(ctlpsm).outLinkMode = L2MODE_BASIC;
#endif /* L2CAP_FLOW_CONTROL == XA_ENABLED */

    if (L2CAP_RegisterPsm(&(HIDP(ctlpsm))) != BT_STATUS_SUCCESS) {
        /* Unable to register */
        Report(("HIDP:  Unable to register HIDC with L2CAP.\n"));
        status = FALSE;
    }

    /* Register HIDP with L2CAP */
    HIDP(intpsm).callback = HidL2IntCallback;
    HIDP(intpsm).psm = BT_PSM_HID_INTR;
    HIDP(intpsm).localMtu = L2CAP_MINIMUM_MTU;//L2CAP_MAXIMUM_MTU;
    HIDP(intpsm).minRemoteMtu = L2CAP_MINIMUM_MTU;
#if  L2CAP_QOS == XA_ENABLED
    HIDP(intpsm).QosEnable = 0;
    HIDP(intpsm).QosServiceType = 0x01; //00: no traffic, 01: best effort, 02:guaranteed
    HIDP(intpsm).QosTokenRate = 900; //octet/s
    HIDP(intpsm).QosTokenBucketSize = 10;
    HIDP(intpsm).QosPeakBandWidth = 900;
    HIDP(intpsm).QosLatency = 10;
    HIDP(intpsm).QosDelayVariation = 10;
#endif

#if L2CAP_FLOW_CONTROL == XA_ENABLED
    HIDP(intpsm).inLinkMode = HIDP(intpsm).outLinkMode = L2MODE_BASIC;
#endif /* L2CAP_FLOW_CONTROL == XA_ENABLED */

    if (L2CAP_RegisterPsm(&(HIDP(intpsm))) != BT_STATUS_SUCCESS) {
        /* Unable to register */
        Report(("HIDP:  Unable to register HIDI with L2CAP.\n"));
        status = FALSE;
    }

    /* Initialized lists */
    InitializeListHead(&HIDP(chnlList));

    return status;
}

#if L2CAP_DEREGISTER_FUNC == XA_ENABLED
/*---------------------------------------------------------------------------
 * HIDP_Deinit()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Deinitializes the HIDP module. Init() must later be called
 *            again to use any HIDP function call. This function does no
 *            error checking so do NOT call when HIDP is in use.
 *             
 * Return:    (See header file)
 */
void HIDP_Deinit(void)
{
    (void)L2CAP_DeregisterPsm(&(HIDP(intpsm)));
    (void)L2CAP_DeregisterPsm(&(HIDP(ctlpsm)));
}
#endif /* L2CAP_DEREGISTER_FUNC == XA_ENABLED */


/*---------------------------------------------------------------------------
 * HIDP_Register()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Registers an application callback to create/receive HIDP 
 *            connections.  This function must be called before any other 
 *            HIDP functions.
 *
 * Return:    (See header file)
 */
BtStatus HIDP_Register(HidpChannel *chnl, HidpCallback callback)
{
    BtStatus   status = BT_STATUS_SUCCESS;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, chnl != 0);
    CheckUnlockedParm(BT_STATUS_INVALID_PARM, callback != 0);

    OS_LockStack();

    /* Register this connection in the list */
    if (!IsNodeOnList(&HIDP(chnlList), &chnl->node)) {

        /* Clear the connection structure */
        OS_MemSet((U8*)chnl, 0, sizeof(HidpChannel));

        /* Register the Hidp connection */
        chnl->callback = callback;

        /* Initialize the connection handler */
        HidInitConn(&chnl->ctlconn, HidCtlConnCb);
        HidInitConn(&chnl->intconn, HidIntConnCb);

        /* Insert the channel on the list */
        InsertTailList(&HIDP(chnlList), &chnl->node);
    } else {

        /* Already registered */
        status = BT_STATUS_IN_USE;
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 * HIDP_Deregister()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  De-registers the HIDP callback. After making this call
 *            successfully, the callback specified in HIDP_Register will
 *            receive no further events.
 *
 * Return:    (See header file)
 */
BtStatus HIDP_Deregister(HidpChannel *chnl)
{
    BtStatus   status = BT_STATUS_SUCCESS;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, chnl != 0);

    OS_LockStack();

    if (IsNodeOnList(&HIDP(chnlList), &chnl->node)) {
        if (!HidIsConnected(&chnl->ctlconn)) {
            /* No connection is up, remove from list */
            RemoveEntryList(&chnl->node);
        } else {
            /* A connection is up */
            status = BT_STATUS_IN_USE;
        }
    } else {
        /* Connection is not registered */
        status = BT_STATUS_NOT_FOUND;
    }

    OS_UnlockStack();

    return status;
}


/*---------------------------------------------------------------------------
 * HIDP_Connect()
 *---------------------------------------------------------------------------
 *
 * Synopsis: Connect to the remote device.
 *
 * Return:   (See header file)
 */
BtStatus HIDP_Connect(HidpChannel *chnl, BtRemoteDevice *remDev)
{
    BtStatus status = BT_STATUS_NOT_FOUND;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, chnl != 0);
    CheckUnlockedParm(BT_STATUS_INVALID_PARM, remDev != 0);

    OS_LockStack();

    /* See if the chnl is registered */
    if (IsNodeOnList(&HIDP(chnlList), &chnl->node)) {
        if (!HidIsConnected(&chnl->ctlconn) && !HidIsConnected(&chnl->intconn)) {
            
            /* Start the connections */
            /*control first then interrupt*/
            status = HidCtlConnect(&chnl->ctlconn, remDev);
        } else {
            /* Already connected */
            status = BT_STATUS_IN_USE;
        }
    }

    OS_UnlockStack();

    return status;
}


/*---------------------------------------------------------------------------
 * HIDP_CtlConnectRsp()
 *---------------------------------------------------------------------------
 *
 * Synopsis: Respond to a connect request from the remote device.
 *
 * Return:   (See header file)
 */
BtStatus HIDP_CtlConnectRsp(HidpChannel *Chnl, BOOL Accept)
{
    BtStatus status = BT_STATUS_NOT_FOUND;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Chnl != 0);

    OS_LockStack();

    /* See if the Chnl is registered */
    if (IsNodeOnList(&HIDP(chnlList), &Chnl->node)) {
        if (!HidIsConnected(&Chnl->ctlconn)) {
            /* Start the connection */
            status = HidConnectRsp(&Chnl->ctlconn, Accept);
        } else {
            /* Already connected */
            status = BT_STATUS_BUSY;
        }
    }

    OS_UnlockStack();

    return status;
}


/*---------------------------------------------------------------------------
 * HIDP_IntConnectRsp()
 *---------------------------------------------------------------------------
 *
 * Synopsis: Respond to a connect request from the remote device.
 *
 * Return:   (See header file)
 */
BtStatus HIDP_IntConnectRsp(HidpChannel *Chnl, BOOL Accept)
{
    BtStatus status = BT_STATUS_NOT_FOUND;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Chnl != 0);

    OS_LockStack();

    /* See if the Chnl is registered */
    if (IsNodeOnList(&HIDP(chnlList), &Chnl->node)) {
        if (!HidIsConnected(&Chnl->intconn)) {
            /* Start the connection */
            status = HidConnectRsp(&Chnl->intconn, Accept);
        } else {
            /* Already connected */
            status = BT_STATUS_BUSY;
        }
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 * HIDP_Disconnect()
 *---------------------------------------------------------------------------
 *
 * Synopsis: Disconnect from the remote device.
 *
 * Return:   (See header file)
 */
BtStatus HIDP_Disconnect(HidpChannel *chnl)
{
    BtStatus status = BT_STATUS_NOT_FOUND;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, chnl != 0);

    OS_LockStack();

    /* See if the chnl is registered */
    if (IsNodeOnList(&HIDP(chnlList), &chnl->node)) {
        if (HidIsConnected(&chnl->ctlconn)) {
            /* Disconnect */
            status = HidDisconnect(&chnl->intconn);
            status = HidDisconnect(&chnl->ctlconn);
        } else {
            /* No connection exists */
            status = BT_STATUS_NO_CONNECTION;
        }
    }

    OS_UnlockStack();

    return status;
}


/*---------------------------------------------------------------------------
 * HIDP_RemoteDevice()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Returns a pointer to the current remote device.
 *
 * Return:    void
 */
BtRemoteDevice * HIDP_RemoteDevice(HidpChannel *chnl)
{
    BtRemoteDevice *remDev;

    if (HidIsConnected(&chnl->ctlconn)) {
        remDev = chnl->ctlconn.remDev;
    } else {
        remDev = 0;
    }

    return remDev;
}

BtStatus HIDP_CtlSendData(HidpChannel *chnl, U16 datalen,  U8 *data)
{
    BtStatus status = BT_STATUS_NOT_FOUND;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, chnl != 0);

    OS_LockStack();

    /* See if the Chnl is registered */
    if (IsNodeOnList(&HIDP(chnlList), &chnl->node)) {
        if (HidIsConnected(&chnl->ctlconn)) {
            /* Start the connection */
            status = HidSendData(&chnl->ctlconn, datalen, data);
        } else {
            /* Already connected */
            status = BT_STATUS_CONNECTION_FAILED;
        }
    }

    OS_UnlockStack();

    return status;

}

BtStatus HIDP_IntSendData(HidpChannel *chnl, U16 datalen,  U8 *data)
{
    BtStatus status = BT_STATUS_NOT_FOUND;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, chnl != 0);

    OS_LockStack();

    /* See if the Chnl is registered */
    if (IsNodeOnList(&HIDP(chnlList), &chnl->node)) {
        if (HidIsConnected(&chnl->intconn)) {
            /* Start the connection */
            status = HidSendData(&chnl->intconn, datalen, data);
        } else {
            /* Already connected */
            status = BT_STATUS_CONNECTION_FAILED;
        }
    }

    OS_UnlockStack();

    return status;

}

/*---------------------------------------------------------------------------
 * HidpAlloc()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initializes the global context for the HID profile.  This 
 *            function can be replaced if necessary.
 *
 * Return:    void
 */
BOOL HidpAlloc(void)
{
    U8* ptr; 

    /* Fill memory with 0. */
#if XA_CONTEXT_PTR == XA_ENABLED
    ptr = (U8*)hidpContext;
#else
    ptr = (U8*)&hidpContext;
#endif /* XA_CONTEXT_PTR */

    OS_MemSet(ptr, 0, (U32)sizeof(BtHidpContext));

    return TRUE;
}


/*---------------------------------------------------------------------------
 *            HidCtlConnCb()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Connection callback.
 *
 */
static void HidCtlConnCb(HidConn *Conn, HidConnCallbackParms *Parms)
{
    HidpChannel       *chnl = ContainingRecord(Conn, HidpChannel, ctlconn);
    HidpCallbackParms  info;

    switch (Parms->event) {
    case HID_CONN_EVENT_IN_CONNECTED:
    case HID_CONN_EVENT_OUT_CONNECTED:
        Report(("HIDP:  Control Channel Connected\n"));

        /* Indicate the connection to the application */
        info.event = HIDP_EVENT_CTL_CONNECT;
        info.channel = chnl;
        info.p.remDev = Parms->ptrs.remDev;
        chnl->callback(chnl, &info);
        if(Parms->event == HID_CONN_EVENT_OUT_CONNECTED)
            HidIntConnect(&chnl->intconn, Parms->ptrs.remDev);

        break;

    case HID_CONN_EVENT_CONNECT_IND:
        /* Indicate the incoming connection to the application */
        info.event = HIDP_EVENT_CTL_CONNECT_IND;
        info.channel = chnl;
        info.p.remDev = Parms->ptrs.remDev;
        chnl->callback(chnl, &info);
        
        break;

    case HID_CONN_EVENT_DISCONNECT:
        Report(("HIDP:  Control Channel Disconnected\n"));

        /* Indicate the connection to the application */
        info.event = HIDP_EVENT_CTL_DISCONNECT;
        info.channel = chnl;
        info.p.remDev = Parms->ptrs.remDev;
        chnl->callback(chnl, &info);
        break;

    case HID_CONN_EVENT_DATA_SENT:
        Report(("HIDP:  Control Data Sent\n"));
        break;

    case HID_CONN_EVENT_DATA_IND:
        Report(("HIDP: Control Data Received = %d\n", Parms->dataLen));
        info.event = HIDP_EVENT_CTL_DATA_IND;
        info.channel = chnl;
        info.datalen = Parms->dataLen;
        info.p.data = Parms->ptrs.data;
        chnl->callback(chnl, &info);
        break;
    }
}



/*---------------------------------------------------------------------------
 *            HidIntConnCb()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Connection callback.
 *
 */

static void HidIntConnCb(HidConn *Conn, HidConnCallbackParms *Parms)
{
    HidpChannel       *chnl = ContainingRecord(Conn, HidpChannel, intconn);
    HidpCallbackParms  info;

    switch (Parms->event) {
    case HID_CONN_EVENT_IN_CONNECTED:
    case HID_CONN_EVENT_OUT_CONNECTED:
        Report(("HIDP:  Int Channel Connected\n"));

        /* Indicate the connection to the application */
        info.event = HIDP_EVENT_INT_CONNECT;
        info.channel = chnl;
        info.p.remDev = Parms->ptrs.remDev;
        chnl->callback(chnl, &info);
        break;

    case HID_CONN_EVENT_CONNECT_IND:
        Report(("HIDP: Int channel incomming indication"));
        /* Indicate the incoming connection to the application */
        info.event = HIDP_EVENT_INT_CONNECT_IND;
        info.channel = chnl;
        info.p.remDev = Parms->ptrs.remDev;
        chnl->callback(chnl, &info);
        break;

    case HID_CONN_EVENT_DISCONNECT:
        Report(("HIDP:  Int Channel Disconnected\n"));

        /* Indicate the connection to the application */
        info.event = HIDP_EVENT_INT_DISCONNECT;
        info.channel = chnl;
        info.p.remDev = Parms->ptrs.remDev;
        chnl->callback(chnl, &info);
        break;

    case HID_CONN_EVENT_DATA_SENT:
        Report(("HIDP:  Int Data Sent\n"));
        info.event = HIDP_EVENT_INT_DATA_SENT;
        info.channel = chnl;
        info.p.remDev = Parms->ptrs.remDev;
        chnl->callback(chnl, &info);
        break;

    case HID_CONN_EVENT_DATA_IND:
        Report(("HIDP: Int Data Received = %d\n", Parms->dataLen));
        info.event = HIDP_EVENT_INT_DATA_IND;
        info.channel = chnl;
        info.datalen = Parms->dataLen;
        info.p.data = Parms->ptrs.data;
        chnl->callback(chnl, &info);
        
        break;
    }
}

