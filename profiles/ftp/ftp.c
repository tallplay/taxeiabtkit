/***************************************************************************
 *
 * File:
 *     $Workfile:ftp.c$ for XTNDAccess Blue SDK, Version 1.3
 *     $Revision:40$
 *
 * Description:
 *     This file contains the functions that comprise the OBEX File
 *     Transfer Profile implementation.
 *
 * Created:
 *     September 4, 2000
 *
 * Copyright 2000-2005 Extended Systems, Inc.  ALL RIGHTS RESERVED.
 *
 * Unpublished Confidential Information of Extended Systems, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of Extended Systems, Inc.
 * 
 * Use of this work is governed by a license granted by Extended Systems, 
 * Inc.  This work contains confidential and proprietary information of 
 * Extended Systems, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include <osapi.h>
#include <ftp.h>
#include <sys/goepext.h>

#if BT_STACK == XA_ENABLED
/****************************************************************************
 *
 * ROMable data
 *
 ****************************************************************************/

#if OBEX_ROLE_SERVER == XA_ENABLED
/*---------------------------------------------------------------------------
 *
 * SDP objects registered by Local File Transfer Server.
 * They are the Service Class ID, the Protocol Descriptor List and the
 * supported formats list.
 */
static const U8 FtpServClassId[] = {
    SDP_ATTRIB_HEADER_8BIT(3),              /* Data element sequence, 3 bytes */
    /* This is a list of one element of type UUID */
    SDP_UUID_16BIT(SC_OBEX_FILE_TRANSFER)  /* FTP UUID in Big Endian */
};

/*
 * * OPTIONAL * Language BaseId List (goes with the service name).
 */
static const U8 FtpLangBaseIdList[] = {
    SDP_ATTRIB_HEADER_8BIT(9),  /* Data Element Sequence, 9 bytes */
    SDP_UINT_16BIT(0x656E),     /* English Language */
    SDP_UINT_16BIT(0x006A),     /* UTF-8 encoding */
    SDP_UINT_16BIT(0x0100)      /* Primary language base Id */
};

/*
 * OPTIONAL File Transfer service name (digianswer requires it).
 */
static const U8 FtpServServiceName[] = {
    SDP_TEXT_8BIT(19),          /* Null terminated text string, 19 bytes */
    'O', 'B', 'E', 'X', ' ', 'F', 'i', 'l', 'e', ' ',  
    'T', 'r', 'a', 'n', 's', 'f', 'e', 'r', '\0'  
};

/*---------------------------------------------------------------------------
 * Ftp Public Browse Group.
 */
static const U8 FtpBrowseGroup[] = {
    SDP_ATTRIB_HEADER_8BIT(3),          /* 3 bytes */
    SDP_UUID_16BIT(SC_PUBLIC_BROWSE_GROUP)  /* Public Browse Group */
};

/* File Transfer Servers attributes.
 *
 * This is a ROM template for the RAM structure used to register the
 * File Transfer Profile's SDP record.
 */
static const SdpAttribute FtpSdpAttributes[] = {
    /* Object push service class ID List attribute */
    SDP_ATTRIBUTE(AID_SERVICE_CLASS_ID_LIST, FtpServClassId), 
    /* OBEX protocol descriptor list attribute */
    {0, 0, 0, 0 },
    /* Public Browse Group Service */
    SDP_ATTRIBUTE(AID_BROWSE_GROUP_LIST, FtpBrowseGroup), 
    /* Language base id (Optional: Used with service name) */
    SDP_ATTRIBUTE(AID_LANG_BASE_ID_LIST, FtpLangBaseIdList),
    /* Object push service name (Optional) */
    SDP_ATTRIBUTE((AID_SERVICE_NAME + 0x0100), FtpServServiceName),
};
#endif /* OBEX_ROLE_SERVER == XA_ENABLED */

#if OBEX_ROLE_CLIENT == XA_ENABLED
/*-------------------------------------------------------------------------
 *
 * SDP query info
 */
/* Service search attribute request for OBEX File Transfer protocol descriptor
 * list. The service search pattern is very specific. It contains the UUIDs
 * for OBEX File Transfer, L2CAP, and RFCOMM in case there is a version of
 * of OBEX File Transfer running over another protocol.
 */
static const U8 FtpServiceSearchAttribReq[] = {
    /* First parameter is the search pattern in data element format. It
     * is a list of 3 UUIDs. 
     */
    SDP_ATTRIB_HEADER_8BIT(9),             /* Data Element Sequence, 9 bytes */ 
    /* The first UUID in the list OBEX object push */
    SDP_UUID_16BIT(SC_OBEX_FILE_TRANSFER), /* FTP UUID in Big Endian */
    /* The second UUID is L2CAP */
    SDP_UUID_16BIT(PROT_L2CAP),            /* L2CAP UUID in Big Endian */
    /* The third UUID is RFCOMM */
    SDP_UUID_16BIT(PROT_RFCOMM),           /* UUID for RFCOMM in Big Endian */

    /* The second parameter is the maximum number of bytes that can be
     * be received for the attribute list
     */
    0x00, 0x64,                 /* Max number of bytes for attribute is 100 */

    /* The third parameter is the actual attribute ID list which is a data
     * element sequence. We only want one attribute which is the protocol
     * descriptor
     */
    SDP_ATTRIB_HEADER_8BIT(3),  /* Data Element Sequence, 3 bytes */
    SDP_UINT_16BIT(0x0004)     /* Value of protocol descriptor list ID */
};
#endif /* OBEX_ROLE_CLIENT == XA_ENABLED */
#endif /* BT_STACK == XA_ENABLED */

/*-------------------------------------------------------------------------
 *
 * OBEX File Transfer UUID for Directed connections
 */
static const U8 FileXferUuid[] = { 0xF9, 0xEC, 0x7B, 0xC4, 0x95, 0x3C, 0x11, 0xD2,
                                   0x98, 0x4E, 0x52, 0x54, 0x00, 0xDC, 0x9E, 0x09 };

/****************************************************************************
 *
 * RAM data
 *
 ****************************************************************************/
#if XA_CONTEXT_PTR == XA_ENABLED
static FtpExtensionData   temp;
static FtpExtensionData  *Ftp = &temp;
#else /* XA_CONTEXT_PTR == XA_ENABLED */
static FtpExtensionData   Ftp;
#endif /* XA_CONTEXT_PTR == XA_ENABLED */

/*****************************************************************************
 *
 * File Transfer Profile Public API's
 *
 ****************************************************************************/

/*---------------------------------------------------------------------------
 *            FTP_Init()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initialize the FTP component.  This must be the first FTP 
 *            function called by the application layer, or if multiple 
 *            FTP applications exist, this function should be called
 *            at system startup (see XA_LOAD_LIST in config.h).  GOEP 
 *            and OBEX must also be initialized separately.
 *
 * Return:    TRUE or FALSE
 *
 */
BOOL FTP_Init(void)
{
    OS_LockStack();

#if XA_CONTEXT_PTR == XA_ENABLED
    OS_MemSet((U8 *)Ftp, 0, sizeof(FtpExtensionData)); 
#else /* XA_CONTEXT_PTR == XA_ENABLED */
    OS_MemSet((U8 *)&Ftp, 0, sizeof(FtpExtensionData)); 
#endif /* XA_CONTEXT_PTR == XA_ENABLED */

    /* FTP initialized */
    FTP(initialized) = TRUE;

    OS_UnlockStack();
    return TRUE;
}

#if BT_STACK == XA_ENABLED
/*****************************************************************************
 *
 * API for Managing the File Transfer Profile SDP Database
 *
 ****************************************************************************/
#if OBEX_ROLE_SERVER == XA_ENABLED
/*---------------------------------------------------------------------------
 *            FTP_AddServiceRecord
 *---------------------------------------------------------------------------
 *
 * Synopsis: Register all mandatory SDP records for the File Transfer Service.
 *           This function is called automatically by the extended API.
 *
 * Return:   BtStatus 
 *
 */
BtStatus FTP_AddServiceRecord(GoepServerApp *Server)
{
    BtStatus    status = BT_STATUS_INVALID_PARM;

    OS_LockStack();

#if XA_ERROR_CHECK == XA_ENABLED
    if (!Server) {
        goto Error;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(Server);

    if (FTP(sdp)[Server->connId].registered) {
        status = BT_STATUS_SUCCESS;
        goto Error;
    }

    /* Create the SDP entry for the OBEX File Transfer server.
     * This is done by copying the ROM templates into RAM structures.
     */
    Assert(sizeof(FtpSdpAttributes) == sizeof(FTP(sdp)[Server->connId].attributes));
    OS_MemCopy( (U8 *)&FTP(sdp)[Server->connId].attributes,
                (U8 *)&FtpSdpAttributes, 
                sizeof(FtpSdpAttributes) );

    OBEX_ServerGetSdpProtocolDescList(&GOES(servers)[Server->connId].obs.trans.ObexServerBtTrans, &FTP(sdp)[Server->connId].attributes[1], 1);

    FTP(sdp)[Server->connId].record.attribs = FTP(sdp)[Server->connId].attributes;
    FTP(sdp)[Server->connId].record.num = 5;
    FTP(sdp)[Server->connId].record.classOfDevice = COD_OBJECT_TRANSFER;

    status = SDP_AddRecord(&FTP(sdp)[Server->connId].record);
    if (status == BT_STATUS_SUCCESS)
        FTP(sdp)[Server->connId].registered = TRUE;

Error:
    OS_UnlockStack();
    return status;
}

/*---------------------------------------------------------------------------
 *            FTP_RemoveServiceRecord
 *---------------------------------------------------------------------------
 *
 * Synopsis: Deregisters SDP records for the File Transfer Service.
 *           This function is called automatically by the extended API.
 *
 * Return:   BtStatus 
 *
 */
BtStatus FTP_RemoveServiceRecord(GoepServerApp *Server)
{
    BtStatus status = BT_STATUS_FAILED;

    OS_LockStack();

#if XA_ERROR_CHECK == XA_ENABLED
    if (!Server) {
        status = BT_STATUS_INVALID_PARM;
        goto Error;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(Server);

    if (FTP(sdp)[Server->connId].registered) {
        /* Remove Servers OBEX SDP entry */
        status = SDP_RemoveRecord(&FTP(sdp)[Server->connId].record);
        if (status == BT_STATUS_SUCCESS)
            FTP(sdp)[Server->connId].registered = FALSE;
    }

Error:
    OS_UnlockStack();
    return status;
}
#endif /* OBEX_ROLE_SERVER == XA_ENABLED */
    
#if OBEX_ROLE_CLIENT == XA_ENABLED
/*---------------------------------------------------------------------------
 *            FTP_BuildServiceQuery
 *---------------------------------------------------------------------------
 *
 * Synopsis: Loads the File Transfer Service Query into the specified pointers.
 *           This function is called automatically by the extended API.
 *
 * Return:   void
 *
 */
void FTP_BuildServiceQuery(const U8 **QueryPtr, U16 *QueryLen, SdpQueryType *QueryType)
{
    OS_LockStack();

    Assert(QueryPtr && QueryLen && QueryType);

    *QueryPtr = FtpServiceSearchAttribReq;
    *QueryLen = sizeof(FtpServiceSearchAttribReq);
    *QueryType = BSQT_SERVICE_SEARCH_ATTRIB_REQ;

    OS_UnlockStack();
}
#endif /* OBEX_ROLE_CLIENT == XA_ENABLED */
#endif /* BT_STACK == XA_ENABLED */

#if FTP_EXPANDED_API == XA_ENABLED
/*****************************************************************************
 *
 * API for the File Transfer Profile Client & Server
 *
 ****************************************************************************/

#if OBEX_ROLE_SERVER == XA_ENABLED
/*---------------------------------------------------------------------------
 *            FTP_RegisterServer
 *---------------------------------------------------------------------------
 *
 * Synopsis: Registers the file transfer server with the GOEP multiplexor.
 *           This includes registering the SDP records for the File Transfer
 *           Service and the OBEX File Transfer UUID.
 *
 * Return:   ObStatus
 *
 */
ObStatus FTP_RegisterServer(GoepServerApp *Server, const ObStoreFuncTable *obStoreFuncs)
{
    ObStatus status = OB_STATUS_INVALID_PARM;
    U8       freeId;

    OS_LockStack();

#if XA_ERROR_CHECK == XA_ENABLED
    if ((!Server) || (!obStoreFuncs)) {
        goto Error;
    }

    if (FTP(initialized) != TRUE) {
        /* FTP not initialized */
        status = OB_STATUS_FAILED;
        goto Error;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(Server && obStoreFuncs);

#if OBEX_SERVER_CONS_SIZE > 0
    FTP(obexConn).target = FileXferUuid;
    FTP(obexConn).targetLen = 16;
    Server->numTargets = 1;
    Server->target[0] = &FTP(obexConn);
#endif /* OBEX_SERVER_CONS_SIZE > 0 */

    Server->type = GOEP_PROFILE_FTP;
    Server->appParent = 0;

    freeId = GOEP_GetConnectionId(GOEP_SERVER, GOEP_PROFILE_FTP);
    if (freeId != 0xFF) {
        /* We can multiplex FTP over an existing connection, so we'll do it */
        Server->connFlags = GOEP_MUX_CONN;
        Server->connId = freeId;
    }
    else {
        /* We have to make a new connection for the FTP profile */
        Server->connFlags = GOEP_NEW_CONN;
        /* Initialize the connId - filled in later */
        Server->connId = 0;
    }

    /* Register SDP Server & Target with GOEP mux */
    status = GOEP_RegisterServer(Server, obStoreFuncs);
    if (status != OB_STATUS_SUCCESS) {
        goto Error;
    }

#if BT_STACK == XA_ENABLED
    /* Add File Transfer Service record to SDP database */
    status = FTP_AddServiceRecord(Server);
#endif /* BT_STACK == XA_ENABLED */
#if OBEX_DEINIT_FUNCS == XA_ENABLED
    if (status != OB_STATUS_SUCCESS) {
        GOEP_DeregisterServer(Server);
        goto Error;
    }
#else /* OBEX_DEINIT_FUNCS == XA_ENABLED */
    Assert(status == OB_STATUS_SUCCESS);
#endif /* OBEX_DEINIT_FUNCS == XA_ENABLED */
    
Error:
    OS_UnlockStack();
    return status;
}

#if OBEX_DEINIT_FUNCS == XA_ENABLED
/*---------------------------------------------------------------------------
 *            FTP_DeregisterServer
 *---------------------------------------------------------------------------
 *
 * Synopsis: Deregisters the file transfer server from the GOEP multiplexor.
 *           This includes removing the SDP records for the File Transfer
 *           Service and the OBEX File Transfer UUID.
 *
 * Return:   ObStatus
 *
 */
ObStatus FTP_DeregisterServer(GoepServerApp *Server)
{
    ObStatus status = OB_STATUS_INVALID_PARM;

    OS_LockStack();

#if XA_ERROR_CHECK == XA_ENABLED
    if (!Server) {
        goto Error;
    }

    if (FTP(initialized) != TRUE) {
        /* FTP not initialized, so there is nothing to deinit */
        status = OB_STATUS_SUCCESS;
        goto Error;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(Server);

    /* Deregister SDP Server from GOEP mux */
    status = GOEP_DeregisterServer(Server);

#if BT_STACK == XA_ENABLED
    if (status == OB_STATUS_SUCCESS) {
        /* Remove File Transfer Service record from SDP database */
        FTP_RemoveServiceRecord(Server);
    }
#endif /* BT_STACK == XA_ENABLED */

Error:
    OS_UnlockStack();
    return status;
}
#endif /* OBEX_DEINIT_FUNCS == XA_ENABLED */
#endif /* OBEX_ROLE_SERVER == XA_ENABLED */

#if OBEX_ROLE_CLIENT == XA_ENABLED
static void FtpClientCallback(struct _GoepClientEvent *);

/*---------------------------------------------------------------------------
 *            FTP_RegisterClient
 *---------------------------------------------------------------------------
 *
 * Synopsis: Registers the file transfer client with the GOEP multiplexor.
 *
 * Return:   ObStatus
 *
 */
ObStatus FTP_RegisterClient(GoepClientApp *Client, const ObStoreFuncTable *obStoreFuncs)
{
    U8       freeId;
    ObStatus status = OB_STATUS_INVALID_PARM;

    OS_LockStack();

#if XA_ERROR_CHECK == XA_ENABLED
    if ((!Client) || (!obStoreFuncs)) {
        goto Error;
    }

    if (FTP(initialized) != TRUE) {
        /* FTP not initialized */
        status = OB_STATUS_FAILED;
        goto Error;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(Client && obStoreFuncs);

    Client->type = GOEP_PROFILE_FTP;
    Client->appParent = FtpClientCallback;

    freeId = GOEP_GetConnectionId(GOEP_CLIENT, GOEP_PROFILE_FTP);
    if (freeId != 0xFF) {
        /* We can multiplex FTP over an existing connection, so we'll do it */
        Client->connFlags = GOEP_MUX_CONN;
        Client->connId = freeId;
    }
    else {
        /* We have to make a new connection for the FTP profile */
        Client->connFlags = GOEP_NEW_CONN;
        /* Initialize the connId - filled in later */
        Client->connId = 0;
    }

    /* Register SDP Client with GOEP mux */
    status = GOEP_RegisterClient(Client, obStoreFuncs);

Error:
    OS_UnlockStack();
    return status;
}

#if OBEX_DEINIT_FUNCS == XA_ENABLED
/*---------------------------------------------------------------------------
 *            FTP_DeregisterClient
 *---------------------------------------------------------------------------
 *
 * Synopsis: Deregisters the file transfer client from the GOEP multiplexor.
 *
 * Return:   ObStatus
 *
 */
ObStatus FTP_DeregisterClient(GoepClientApp *Client)
{
    ObStatus status = OB_STATUS_INVALID_PARM;

    OS_LockStack();

#if XA_ERROR_CHECK == XA_ENABLED
    if (!Client) {
        goto Error;
    }

    if (FTP(initialized) != TRUE) {
        /* FTP not initialized, so there is nothing to deinit */
        status = OB_STATUS_SUCCESS;
        goto Error;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(Client);

    /* Deregister SDP Client from GOEP mux */
    status = GOEP_DeregisterClient(Client);

Error:
    OS_UnlockStack();
    return status;
}
#endif /* OBEX_DEINIT_FUNCS == XA_ENABLED */

/*---------------------------------------------------------------------------
 *            FTP_Connect
 *---------------------------------------------------------------------------
 *
 * Synopsis: Establishes a File Transfer Client Connection to the specified
 *           target. Refer to GOEP_Connect() in goep.h for more information on
 *           the GoepConnectReq parameter. The fields pointed to by the
 *           GoepConnectReq structure must remain valid until completion is
 *           indicated by a call to the client callback function.
 *
 * Return:   ObStatus
 *
 */
ObStatus FTP_Connect(GoepClientApp *Client, ObexTpAddr *Target, 
                         GoepConnectReq *ConnReq)
{
    ObStatus    status = OB_STATUS_INVALID_PARM;

    OS_LockStack();

#if XA_ERROR_CHECK == XA_ENABLED
    if ((!Client) || (!Target)) {
        goto Error;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(Client && Target && 
          (Client == GOEC(clients)[Client->connId].profiles[GOEP_PROFILE_FTP]));

#if BT_STACK == XA_ENABLED
    if (Target->type == OBEX_TP_BLUETOOTH) {
        FTP_BuildServiceQuery( &Target->proto.bt.sdpQuery,
                                   &Target->proto.bt.sdpQueryLen,
                                   &Target->proto.bt.sdpQueryType );
    }
#endif /* BT_STACK == XA_ENABLED */

    /* Copy clients connect options and set target info */
    if (ConnReq)
        OS_MemCopy((U8 *)&FTP(connect), (U8 *)ConnReq, sizeof(GoepConnectReq));
    else OS_MemSet((U8 *)&FTP(connect), 0, sizeof(GoepConnectReq));

    FTP(connect).target = FileXferUuid;
    FTP(connect).targetLen = 16;

    if ((status = GOEP_TpConnect(Client, Target)) == OB_STATUS_SUCCESS) {
        /* The transport layer connection is up. Send an OBEX Connect. */
        status = GOEP_Connect(Client, &FTP(connect));
    }

Error:
    OS_UnlockStack();
    return status;
}

/*---------------------------------------------------------------------------
 *            FTP_Delete
 *---------------------------------------------------------------------------
 *
 * Synopsis: Deletes the specified file on the server. 
 *
 * Return:   ObStatus
 *
 */
ObStatus FTP_Delete(GoepClientApp *Client, const GoepUniType *ObjectName)
{
    GoepObjectReq   request;
    ObStatus        status = OB_STATUS_INVALID_PARM;

    OS_LockStack();

#if XA_ERROR_CHECK == XA_ENABLED
    if ((!Client) || (!ObjectName)) {
        goto Error;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(Client && ObjectName);

    OS_MemSet((U8 *)&request, 0, sizeof(GoepObjectReq)); 

    request.name = ObjectName;

    status = GOEP_Push(Client, &request);

Error:
    OS_UnlockStack();
    return status;
}

/*---------------------------------------------------------------------------
 *            FTP_SetFolderFwd
 *---------------------------------------------------------------------------
 *
 * Synopsis: Maps the File Transfer Client Set Folder forward function to the
 *           GOEP SetFolder function.
 *
 * Return:   ObStatus
 *
 */
ObStatus FTP_SetFolderFwd(GoepClientApp *Client, const GoepUniType *FolderName, 
                           BOOL AllowCreate)
{
    GoepFolderReq   folder;
    ObStatus        status = OB_STATUS_INVALID_PARM;

    OS_LockStack();

#if XA_ERROR_CHECK == XA_ENABLED
    if ((!Client) || (!FolderName)) {
        goto Error;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(Client && FolderName);

    folder.name = FolderName;
    folder.reset = FALSE;
    folder.flags = (AllowCreate == FALSE ? OSPF_DONT_CREATE : OSPF_NONE);

    status = GOEP_SetFolder(Client, &folder);

Error:
    OS_UnlockStack();
    return status;
}

/*---------------------------------------------------------------------------
 *            FTP_SetFolderBkup
 *---------------------------------------------------------------------------
 *
 * Synopsis: Maps the File Transfer Client Set Folder Backup function to the
 *           GOEP SetFolder function.
 *
 * Return:   ObStatus
 *
 */
ObStatus FTP_SetFolderBkup(GoepClientApp *Client)
{
    GoepFolderReq   folder;
    ObStatus        status = OB_STATUS_INVALID_PARM;

    OS_LockStack();

#if XA_ERROR_CHECK == XA_ENABLED
    if (!Client) {
        goto Error;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(Client);

    folder.name = 0;
    folder.reset = FALSE;
    folder.flags = (OSPF_DONT_CREATE|OSPF_BACKUP);

    status = GOEP_SetFolder(Client, &folder);

Error:
    OS_UnlockStack();
    return status;
}

/*---------------------------------------------------------------------------
 *            FTP_SetFolderRoot
 *---------------------------------------------------------------------------
 *
 * Synopsis: Maps the File Transfer Client Set Root Folder function to the
 *           GOEP SetFolder function.
 *
 * Return:   ObStatus
 *
 */
ObStatus FTP_SetFolderRoot(GoepClientApp *Client)
{
    GoepFolderReq   folder;
    ObStatus        status = OB_STATUS_INVALID_PARM;

    OS_LockStack();

#if XA_ERROR_CHECK == XA_ENABLED
    if (!Client) {
        goto Error;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(Client);

    folder.name = 0;
    folder.reset = TRUE;
    folder.flags = OSPF_DONT_CREATE;

    status = GOEP_SetFolder(Client, &folder);

Error:
    OS_UnlockStack();
    return status;
}


/****************************************************************************
 *
 * Internal functions
 *
 ****************************************************************************/

/*---------------------------------------------------------------------------
 *            FtpClientCallback
 *---------------------------------------------------------------------------
 *
 * Synopsis:  This function processes GOEP Client protocol events.
 *
 */
static void FtpClientCallback(GoepClientEvent *Event)
{
    ObStatus    status = OB_STATUS_FAILED;

    switch (Event->event) {
    default:
        /* Most events are passed clear to the application. */
        Event->handler->callback(Event);
        return;
        
    case GOEP_EVENT_TP_CONNECTED:
        /* The transport layer connection is up. Send an OBEX Connect. */
        status = GOEP_Connect(Event->handler, &FTP(connect));
        break;

    case GOEP_EVENT_COMPLETE:
        /* The requested operation has completed. */
        if (Event->oper == GOEP_OPER_DISCONNECT) {
            /* OBEX Disconnect is complete, disconnect link. */
            status = GOEP_TpDisconnect(Event->handler);
            break;
        }

        if (Event->oper == GOEP_OPER_CONNECT) {
            if (Event->handler->obexConnId == OBEX_INVALID_CONNID) {
                /* We did not get connected to the FTP Server.
                 * Change the event to aborted and set the abort reason code.
                 */
                Event->reason = OBRC_SERVICE_UNAVAILABLE;
                Event->event = GOEP_EVENT_ABORTED;
            }
        }

        /* Tell the app were done */
        Event->handler->callback(Event);
        return;

    case GOEP_EVENT_ABORTED:
        /* The requested operation has failed. Alert the user. */
        if (Event->oper == GOEP_OPER_DISCONNECT) {
            /* It was an intermediate disconnect that failed. Continue the
             * disconnect process if the link is still up.
             */
            if (Event->reason != OBRC_LINK_DISCONNECT) {
                /* Disconnect the link */
                status = GOEP_TpDisconnect(Event->handler);
            }
            break;
        }

        /* Requested operation was Aborted. Tell the client. */
        Event->handler->callback(Event);
        return;
    }

    /* Check the status of the new operation */
    if (status != OB_STATUS_PENDING) {
        status = GOEP_TpDisconnect(Event->handler);

        if (status != OB_STATUS_PENDING)
            Event->handler->callback(Event);
    }
}
#endif /* OBEX_ROLE_CLIENT == XA_ENABLED */
#endif /* FTP_EXPANDED_API == XA_ENABLED */
