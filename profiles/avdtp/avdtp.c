/****************************************************************************
 *
 * File:
 *     $Workfile:avdtp.c$ for iAnywhere AV SDK, version 1.2.3
 *     $Revision:57$
 *
 * Description: This file contains public API for the Audio/Video
 *     Distribution Transport Protocol (AVDTP).
 *             
 * Created:     Mar 10, 2004
 *
 * Copyright 2004 - 2005 Extended Systems, Inc.

 * Portions copyright 2005-2006 iAnywhere Solutions, Inc.

 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any means, 
 * or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, Inc. 
 * This work contains confidential and proprietary information of Extended 
 * Systems, Inc. which is protected by copyright, trade secret, trademark and 
 * other intellectual property rights.
 *
 ****************************************************************************/

#include "sys/avalloc.h"
#include "sys/avsigmgr.h"
#include "sys/avdtpcon.h"

/*---------------------------------------------------------------------------
 * AvdtpFindContentProtection()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Finds a registered content protection structure based on the 
 *            type
 *
 * Return:    Pointer to a registered content protection structure.
 */
AvdtpContentProt *AvdtpFindContentProtection(AvdtpStream *Stream,
                                             AvdtpContentProtType type)
{
    AvdtpContentProt *cp;

    cp = (AvdtpContentProt *)GetHeadList(&Stream->cpList);
    while (cp != (AvdtpContentProt *)&Stream->cpList) {
        if (cp->cpType == type) {
            break;
        }

        /* Get the next structure */
        cp = (AvdtpContentProt *)GetNextNode(&cp->node);
    }

    if (cp == (AvdtpContentProt *)&Stream->cpList) {
        cp = 0;
    }

    return cp;
}

/*---------------------------------------------------------------------------
 * AVDTP_Init()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initialize the AVDTP SDK.  This function should only be called
 *            once, normally at initialization time.  The calling of this 
 *            function can be specified in overide.h using the XA_LOAD_LIST 
 *            macro (i.e. #define XA_LOAD_LIST XA_MODULE(AV)).
 *
 * Return:    (See header file)
 */
BOOL AVDTP_Init(void)
{
    I8 i;

    BOOL status = TRUE;

    /* Initialize Memory */
    status = AvdtpAlloc();
    if (status == FALSE) {
        goto exit;
    }

    /* Initialize the connection state machine */
    AvdtpConnInitStateMachine();

    /* Register AVDTP with L2CAP */
    AVDTP(psm).callback = AvL2Callback;
    AVDTP(psm).psm = BT_PSM_AVDTP;
    AVDTP(psm).localMtu = L2CAP_MAXIMUM_MTU;
    AVDTP(psm).minRemoteMtu = L2CAP_MINIMUM_MTU;

#if L2CAP_FLOW_CONTROL == XA_ENABLED
    AVDTP(psm).inLinkMode = AVDTP(psm).outLinkMode = L2MODE_BASIC;
#endif /* L2CAP_FLOW_CONTROL == XA_ENABLED */

    if (L2CAP_RegisterPsm(&(AVDTP(psm))) != BT_STATUS_SUCCESS) {
        /* Unable to register */
        Report(("AVDTP: Unable to register with L2CAP.\n"));
        status = FALSE;
    }

    /* Initialized lists */
    InitializeListHead(&AVDTP(chnlList));
    InitializeListHead(&AVDTP(streamList));
    InitializeListHead(&AVDTP(txPacketList));
    for (i = 0; i < AVDTP_NUM_TX_PACKETS; i++) {
        AVDTP(txPackets)[i].txData = AVDTP(txData)[i];
        InsertTailList(&AVDTP(txPacketList), &AVDTP(txPackets)[i].node);
    }

    exit:

    return status;
}

/*---------------------------------------------------------------------------
 * AVDTP_Register()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Registers an application callback to create/receive AVDTP 
 *            connections.  This function must be called before any other 
 *            AVDTP functions.
 *
 * Return:    (See header file)
 */
BtStatus AVDTP_Register(AvdtpChannel *Chnl, AvdtpCallback Callback)
{
    BtStatus   status = BT_STATUS_SUCCESS;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Chnl != 0);
    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Callback != 0);

    OS_LockStack();

    /* Register this connection in the list */
    if (!IsNodeOnList(&AVDTP(chnlList), &Chnl->node)) {

        /* Clear the connection structure */
        OS_MemSet((U8*)Chnl, 0, sizeof(AvdtpChannel));

        /* Register the Avdtp connection */
        Chnl->callback = Callback;

        /* Initialize the Signal Manager handler */
        AvdtpSigInit(Chnl);

        /* Insert the channel on the list */
        InsertTailList(&AVDTP(chnlList), &Chnl->node);

        /* Initialize the channel's stream list */
        InitializeListHead(&Chnl->streamList);
    } else {

        /* Already registered */
        status = BT_STATUS_IN_USE;
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 * AVDTP_Deregister()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  De-registers the AVDTP callback. After making this call
 *            successfully, the callback specified in AVDTP_Register will
 *            receive no further events.
 *
 * Return:    (See header file)
 */
BtStatus AVDTP_Deregister(AvdtpChannel *Chnl)
{
    BtStatus   status = BT_STATUS_SUCCESS;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Chnl != 0);

    OS_LockStack();

    if (IsNodeOnList(&AVDTP(chnlList), &Chnl->node)) {
        if (!AvdtpSigIsConnected(Chnl)) {
            /* No connection is up, remove from list */
            RemoveEntryList(&Chnl->node);
        } else {
            /* A connection is up */
            status = BT_STATUS_BUSY;
        }
    } else {
        /* Connection is not registered */
        status = BT_STATUS_NOT_FOUND;
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 * AVDTP_Connect()
 *---------------------------------------------------------------------------
 *
 * Synopsis: Connect to the remote device.
 *
 * Return:   (See header file)
 */
BtStatus AVDTP_Connect(AvdtpChannel *Chnl, BtRemoteDevice *RemDev)
{
    BtStatus status = BT_STATUS_NOT_FOUND;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Chnl != 0);
    CheckUnlockedParm(BT_STATUS_INVALID_PARM, RemDev != 0);

    OS_LockStack();

    /* See if the Chnl is registered */
    if (IsNodeOnList(&AVDTP(chnlList), &Chnl->node)) {
        if (!AvdtpSigIsConnected(Chnl)) {
            /* Start the connection */
            status = AvdtpSigConnect(Chnl, RemDev);
        } else {
            /* Already connected */
            status = BT_STATUS_BUSY;
        }
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 * AVDTP_ConnectRsp()
 *---------------------------------------------------------------------------
 *
 * Synopsis: Respond to a connect request from the remote device.
 *
 * Return:   (See header file)
 */
BtStatus AVDTP_ConnectRsp(AvdtpChannel *Chnl, BOOL Accept)
{
    BtStatus status = BT_STATUS_NOT_FOUND;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Chnl != 0);

    OS_LockStack();

    /* See if the Chnl is registered */
    if (IsNodeOnList(&AVDTP(chnlList), &Chnl->node)) {
        if (!AvdtpSigIsConnected(Chnl)) {
            /* Start the connection */
            status = AvdtpSigConnectRsp(Chnl, Accept);
        } else {
            /* Already connected */
            status = BT_STATUS_BUSY;
        }
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 * AVDTP_Disconnect()
 *---------------------------------------------------------------------------
 *
 * Synopsis: Disconnect from the remote device.
 *
 * Return:   (See header file)
 */
BtStatus AVDTP_Disconnect(AvdtpChannel *Chnl)
{
    BtStatus status = BT_STATUS_NOT_FOUND;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Chnl != 0);

    OS_LockStack();

    /* See if the Chnl is registered */
    if (IsNodeOnList(&AVDTP(chnlList), &Chnl->node)) {
        if (AvdtpSigIsConnected(Chnl)) {
            /* Disconnect */
            status = AvdtpSigDisconnect(Chnl);
        } else {
            /* No connection exists */
            status = BT_STATUS_NO_CONNECTION;
        }
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 * AVDTP_RegisterStream()
 *---------------------------------------------------------------------------
 *
 * Synopsis:
 *
 * Return:    (See header file)
 */
BtStatus AVDTP_RegisterStream(AvdtpStream *Stream, 
                              AvdtpCodec *Codec)
{
    BtStatus     status = BT_STATUS_SUCCESS;
    AvdtpChannel *chnl;
    AvdtpStream  *strm;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Stream != 0);
    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Codec != 0);
    CheckUnlockedParm(BT_STATUS_INVALID_PARM, (Stream->mediaType & ~0xF) == 0);
    CheckUnlockedParm(BT_STATUS_INVALID_PARM, (Stream->endPointType & ~0x1) == 0);

    OS_LockStack();

    /* Register this stream in the list */
    if (!IsNodeOnList(&AVDTP(streamList), &Stream->node)) {

        if (AVDTP(numStreams) < MAX_NUM_STREAMS) {
            /* Initialize the stream structure */
            Stream->state = 0;
            Stream->chnl = 0;
            OS_MemSet((U8 *)&Stream->cpCfg, 0, sizeof(AvdtpContentProt));
            OS_MemSet((U8 *)&Stream->codecCfg, 0, sizeof(AvdtpCodec));
            Stream->cpCfg.data = Stream->cpCfgValue;
            Stream->codecCfg.elements = Stream->codecCfgElem;
            OS_MemSet((U8 *)&Stream->cfgReq.cp, 0, sizeof(AvdtpContentProt));
            OS_MemSet((U8 *)&Stream->cfgReq.codec, 0, sizeof(AvdtpCodec));
            Stream->cfgReq.cp.data = Stream->cpReqValue;
            Stream->cfgReq.codec.elements = Stream->codecReqElem;
            Stream->codec = Codec;
            Stream->abError = AVDTP_ERR_NO_ERROR;

            /* Initialize content protection list */
            InitializeListHead(&Stream->cpList);

            /* Register the Avdtp stream connection */
            AvdtpInitConn(&Stream->conn, AvdtpStrmConnCallback);

            /* Generate a unique stream ID, start with 1 */
            Stream->locStrmId = 1;

            /* Search open streams */
            chnl = (AvdtpChannel *)GetHeadList(&AVDTP(chnlList));
            while (chnl != (AvdtpChannel *)&AVDTP(chnlList)) {
                strm = (AvdtpStream *)GetHeadList(&chnl->streamList);
                while (strm != (AvdtpStream *)&chnl->streamList) {
                    if (strm->locStrmId == Stream->locStrmId) {
                        Stream->locStrmId++;
                    }

                    strm = (AvdtpStream *)GetNextNode(&strm->node);
                }

                /* Get the next connection structure */
                chnl = (AvdtpChannel *)GetNextNode(&chnl->node);
            }

            /* Search Idle streams */
            strm = (AvdtpStream *)GetHeadList(&AVDTP(streamList));
            while (strm != (AvdtpStream *)&AVDTP(streamList)) {
                if (strm->locStrmId == Stream->locStrmId) {
                    Stream->locStrmId++;
                }

                strm = (AvdtpStream *)GetNextNode(&strm->node);
            }

            /* Insert on the free list */
            InsertTailList(&AVDTP(streamList), &Stream->node);

            /* Count the registered streams */
            AVDTP(numStreams)++;
        } else {
            status = BT_STATUS_NO_RESOURCES;
        }
    } else {
        status = BT_STATUS_IN_USE;
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 * AVDTP_DeregisterStream()
 *---------------------------------------------------------------------------
 *
 * Synopsis:
 *
 * Return:    (See header file)
 */
BtStatus AVDTP_DeregisterStream(AvdtpStream *Stream)
{
    BtStatus   status = BT_STATUS_SUCCESS;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Stream != 0);

    OS_LockStack();

    if (IsNodeOnList(&AVDTP(streamList), &Stream->node)) {
        if (Stream->state == AVDTP_STRM_STATE_IDLE) {
            InitializeListHead(&Stream->cpList);
            RemoveEntryList(&Stream->node);
            AVDTP(numStreams)--;
        } else {
            /* The stream is in use */
            status = BT_STATUS_BUSY;
        }
    } else {
        /* Stream is not registered */
        status = BT_STATUS_NOT_FOUND;
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 * AVDTP_AddContentProtection()
 *---------------------------------------------------------------------------
 *
 * Synopsis:
 *
 * Return:    (See header file)
 */
BtStatus AVDTP_AddContentProtection(AvdtpStream *Stream, AvdtpContentProt *Cp)
{
    BtStatus   status = BT_STATUS_SUCCESS;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Stream != 0);
    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Cp != 0);

    OS_LockStack();

    if (IsNodeOnList(&AVDTP(streamList), &Stream->node)) {
        if (Stream->state == AVDTP_STRM_STATE_IDLE) {
            /* Add content protection to the registration list */
            if (!IsNodeOnList(&Stream->cpList, &Cp->node)) {
                InsertTailList(&Stream->cpList, &Cp->node);
            } else {
                status = BT_STATUS_IN_USE;
            }
        } else {
            /* The stream is in use */
            status = BT_STATUS_BUSY;
        }
    } else {
        /* Stream is not registered */
        status = BT_STATUS_NOT_FOUND;
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 * AVDTP_DiscoverStreams()
 *---------------------------------------------------------------------------
 *
 * Synopsis:
 *
 * Return:    (See header file)
 */
BtStatus AVDTP_DiscoverStreams(AvdtpChannel *Chnl)
{
    BtStatus status;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Chnl != 0);

    OS_LockStack();

    if (IsNodeOnList(&AVDTP(chnlList), &Chnl->node)) {
        if (AvdtpSigIsConnected(Chnl)) {
            /* Do a search for a stream with matching capabilities */
            status = AvdtpSigStreamDiscover(Chnl);
        } else {
            status = BT_STATUS_NO_CONNECTION;
        }
    } else {
        /* No connection exists */
        status = BT_STATUS_NOT_FOUND;
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 * AVDTP_GetCapabilities()
 *---------------------------------------------------------------------------
 *
 * Synopsis:
 *
 * Return:    (See header file)
 */
BtStatus AVDTP_GetCapabilities(AvdtpChannel *Chnl, AvdtpStreamId StrmId)
{
    BtStatus status;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Chnl != 0);

    OS_LockStack();

    if (IsNodeOnList(&AVDTP(chnlList), &Chnl->node)) {
        if (AvdtpSigIsConnected(Chnl)) {
            /* Do a search for a stream with matching capabilities */
            status = AvdtpSigStreamGetCapabilities(Chnl, StrmId);
        } else {
            status = BT_STATUS_NO_CONNECTION;
        }
    } else {
        status = BT_STATUS_NOT_FOUND;
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 * AVDTP_GetConfig()
 *---------------------------------------------------------------------------
 *
 * Synopsis:
 *
 * Return:    (See header file)
 */
BtStatus AVDTP_GetConfig(AvdtpStream *Stream)
{
    BtStatus status;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Stream != 0);

    OS_LockStack();

    if (Stream->chnl) {
        if (IsNodeOnList(&AVDTP(chnlList), &Stream->chnl->node)) {
            if (IsNodeOnList(&Stream->chnl->streamList, &Stream->node)) {
                status = AvdtpSigStreamGetConfig(Stream->chnl, Stream);
            } else {
                status = BT_STATUS_NOT_FOUND;
            }
        } else {
            status = BT_STATUS_NOT_FOUND;
        }
    } else {
        status = BT_STATUS_NO_CONNECTION;
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 * AVDTP_OpenStream()
 *---------------------------------------------------------------------------
 *
 * Synopsis:
 *
 * Return:    (See header file)
 */
BtStatus AVDTP_OpenStream(AvdtpChannel *Chnl, AvdtpStream *Stream,
                          AvdtpStreamId StrmId, AvdtpCodec *Codec,
                          AvdtpContentProt *Cp)
{
    BtStatus status;
    AvdtpContentProt *cp;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Chnl != 0);
    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Stream != 0);
    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Codec != 0);

    OS_LockStack();

    if (IsNodeOnList(&AVDTP(chnlList), &Chnl->node)) {
        if (IsNodeOnList(&AVDTP(streamList), &Stream->node)) {
            if (AvdtpSigIsConnected(Chnl)) {
                /* Set the configuration */
                if (Stream->state == AVDTP_STRM_STATE_IDLE) {
                    Stream->remStrmId = StrmId;
                    if (Cp) {
                        cp = AvdtpFindContentProtection(Stream, Cp->cpType);
                        if (!cp) {
                            status = BT_STATUS_INVALID_PARM;
                            goto exit;
                        }
                    }
                    status = AvdtpSigStreamSetConfig(Chnl, Stream, Codec, Cp);
                } else if (Stream->state == AVDTP_STRM_STATE_CONFIGURED) {
                    status = AvdtpSigStreamOpen(Chnl, Stream);
                } else {
                    status = BT_STATUS_IN_USE;
                }
            } else {
                status = BT_STATUS_NO_CONNECTION;
            }
        } else {
            status = BT_STATUS_NOT_FOUND;
        }
    } else {
        status = BT_STATUS_NOT_FOUND;
    }

    exit:

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 * AVDTP_OpenStreamRsp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:
 *
 * Return:    (See header file)
 */
BtStatus AVDTP_OpenStreamRsp(AvdtpChannel *Chnl, AvdtpStream *Stream, 
                             AvdtpError Error, AvdtpCapabilityType CapType)
{
    BtStatus status;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Chnl != 0);
    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Stream != 0);

    OS_LockStack();

    if (Stream->chnl) {
        if (IsNodeOnList(&AVDTP(chnlList), &Chnl->node)) {
            if (IsNodeOnList(&Chnl->streamList, &Stream->node)) {
                if (Stream->state >= AVDTP_STRM_STATE_CONFIGURED) {
                    Chnl->currentTxStream = Stream;
                    if (!Error) {

                        if (!IsListEmpty(&AVDTP(txPacketList))) {
                            /* Save configuration state configured parms */
                            Chnl->rspSigPacket = 
                                (AvtpPacket *)RemoveHeadList(&AVDTP(txPacketList));
                            Stream->cpCfg.cpType = Stream->cfgReq.cp.cpType;
                            Stream->cpCfg.dataLen = Stream->cfgReq.cp.dataLen;
                            OS_MemCopy((U8 *)Stream->cpCfg.data, 
                                       (U8 *)Stream->cfgReq.cp.data, 
                                       Stream->cfgReq.cp.dataLen);
                            Stream->codecCfg.codecType = 
                                Stream->cfgReq.codec.codecType;
                            Stream->codecCfg.elemLen = 
                                Stream->cfgReq.codec.elemLen;
                            OS_MemCopy((U8 *)Stream->codecCfg.elements, 
                                       (U8 *)Stream->cfgReq.codec.elements,
                                       Stream->cfgReq.codec.elemLen);

                            /* Accept the Open request */
                            Chnl->rspSigPacket->msgType = AVTP_MSG_TYPE_ACCEPT;
                            Chnl->rspSigPacket->txIdSize = 1;
                            Chnl->rspSigPacket->txId = AVDTP_SIG_SET_CONFIG;
                            Chnl->rspSigPacket->txDataLen = 0;

                            status = AVTP_SendStart(&Chnl->sigChnl, 
                                                Chnl->rspSigPacket);
                            if (status == BT_STATUS_PENDING) {
                                Stream->state = AVDTP_STRM_STATE_CONFIGURED;
                            } else {
                                InsertTailList(&AVDTP(txPacketList), 
                                               &Chnl->rspSigPacket->node);
                            }
                        } else {
                            status = BT_STATUS_NO_RESOURCES;
                        }
                    } else {
                        if (!IsListEmpty(&AVDTP(txPacketList))) {
                            /* Reject the Open request */
                            Chnl->rspSigPacket = 
                                (AvtpPacket *)RemoveHeadList(&AVDTP(txPacketList));
                            Chnl->rspSigPacket->msgType = AVTP_MSG_TYPE_REJECT;
                            Chnl->rspSigPacket->txIdSize = 1;
                            Chnl->rspSigPacket->txId = AVDTP_SIG_SET_CONFIG;
                            Chnl->rspSigPacket->txData[0] = CapType;
                            Chnl->rspSigPacket->txData[1] = Error;
                            Chnl->rspSigPacket->txDataLen = 2;
                            status = AVTP_SendStart(&Chnl->sigChnl, 
                                                    Chnl->rspSigPacket);
                            if (status != BT_STATUS_PENDING) {
                                InsertTailList(&AVDTP(txPacketList), 
                                               &Chnl->rspSigPacket->node);
                            }
                        } else {
                            status = BT_STATUS_NO_RESOURCES;
                        }
                    }
                } else {
                    status = BT_STATUS_IN_USE;
                }
            } else {
                status = BT_STATUS_NOT_FOUND;
            }
        } else {
            status = BT_STATUS_NOT_FOUND;
        }
    } else {
        status = BT_STATUS_NO_CONNECTION;
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 * AVDTP_CloseStream()
 *---------------------------------------------------------------------------
 *
 * Synopsis:
 *
 * Return:    (See header file)
 */
BtStatus AVDTP_CloseStream(AvdtpStream *Stream)
{
    BtStatus status;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Stream != 0);

    OS_LockStack();

    if (Stream->chnl) {
        if (IsNodeOnList(&AVDTP(chnlList), &Stream->chnl->node)) {
            if (IsNodeOnList(&Stream->chnl->streamList, &Stream->node)) {
                if ((Stream->state == AVDTP_STRM_STATE_OPEN) ||
                    (Stream->state == AVDTP_STRM_STATE_STREAMING)) {
                    status = AvdtpSigStreamClose(Stream->chnl, Stream);
                    if (status == BT_STATUS_PENDING) {
                        Stream->state = AVDTP_STRM_STATE_CLOSING;
                    }
                } else {
                    status = BT_STATUS_FAILED;
                }
            } else {
                status = BT_STATUS_NOT_FOUND;
            }
        } else {
            status = BT_STATUS_NOT_FOUND;
        }
    } else {
        status = BT_STATUS_NO_CONNECTION;
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 * AVDTP_Reconfig()
 *---------------------------------------------------------------------------
 *
 * Synopsis:
 *
 * Return:    (See header file)
 */
BtStatus AVDTP_Reconfig(AvdtpStream *Stream, AvdtpCodec *Codec,
                        AvdtpContentProt *Cp)
{
    BtStatus status;
    AvdtpContentProt *cp;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Stream != 0);
    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Codec != 0);

    OS_LockStack();

    if (Stream->chnl) {
        if (IsNodeOnList(&AVDTP(chnlList), &Stream->chnl->node)) {
            if (IsNodeOnList(&Stream->chnl->streamList, &Stream->node)) {
                if (Stream->state == AVDTP_STRM_STATE_OPEN) {
                    if (Cp) {
                        cp = AvdtpFindContentProtection(Stream, Cp->cpType);
                        if (!cp) {
                            status = BT_STATUS_INVALID_PARM;
                            goto exit;
                        }
                    }
                    status = AvdtpSigStreamReconfig(Stream->chnl, Stream, 
                                                    Codec, Cp);
                } else {
                    status = BT_STATUS_FAILED;
                }
            } else {
                status = BT_STATUS_NOT_FOUND;
            }
        } else {
            status = BT_STATUS_NOT_FOUND;
        }
    } else {
        status = BT_STATUS_NO_CONNECTION;
    }

    exit:

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 * AVDTP_ReconfigRsp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:
 *
 * Return:    (See header file)
 */
BtStatus AVDTP_ReconfigRsp(AvdtpStream *Stream, AvdtpError Error, 
                           AvdtpCapabilityType Type)
{
    AvdtpChannel *chnl;
    BtStatus      status;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Stream != 0);

    OS_LockStack();

    if (Stream->chnl) {
        chnl = Stream->chnl;
        if (IsNodeOnList(&AVDTP(chnlList), &Stream->chnl->node)) {
            if (IsNodeOnList(&Stream->chnl->streamList, &Stream->node)) {
                if (Stream->state == AVDTP_STRM_STATE_OPEN) {
                    status = AvdtpSigStreamReconfigRsp(Stream->chnl, 
                                                       Stream, Error, Type);
                } else {
                    status = BT_STATUS_FAILED;
                }
            } else {
                status = BT_STATUS_NOT_FOUND;
            }
        } else {
            status = BT_STATUS_NOT_FOUND;
        }
    } else {
        status = BT_STATUS_NO_CONNECTION;
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 * AVDTP_StartStream()
 *---------------------------------------------------------------------------
 *
 * Synopsis:
 *
 * Return:    (See header file)
 */
BtStatus AVDTP_StartStream(AvdtpStream *Stream)
{
    BtStatus status;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Stream != 0);

    OS_LockStack();

    if (Stream->chnl) {
        if (IsNodeOnList(&AVDTP(chnlList), &Stream->chnl->node)) {
            if (IsNodeOnList(&Stream->chnl->streamList, &Stream->node)) {
                if (Stream->state == AVDTP_STRM_STATE_OPEN) {
                    status = AvdtpSigStreamStart(Stream->chnl, Stream);
                } else {
                    status = BT_STATUS_FAILED;
                }
            } else {
                status = BT_STATUS_NOT_FOUND;
            }
        } else {
            status = BT_STATUS_NOT_FOUND;
        }
    } else {
        status = BT_STATUS_NO_CONNECTION;
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 * AVDTP_StartStreamRsp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:
 *
 * Return:    (See header file)
 */
BtStatus AVDTP_StartStreamRsp(AvdtpStream *Stream, AvdtpError Error)
{
    BtStatus status;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Stream != 0);

    OS_LockStack();

    if (Stream->chnl) {
        if (IsNodeOnList(&AVDTP(chnlList), &Stream->chnl->node)) {
            if (IsNodeOnList(&Stream->chnl->streamList, &Stream->node)) {
                if (Stream->state == AVDTP_STRM_STATE_OPEN) {
                    status = AvdtpSigStreamStartRsp(Stream->chnl, Stream, Error);
                } else {
                    status = BT_STATUS_FAILED;
                }
            } else {
                status = BT_STATUS_NOT_FOUND;
            }
        } else {
            status = BT_STATUS_NOT_FOUND;
        }
    } else {
        status = BT_STATUS_NO_CONNECTION;
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 * AVDTP_SuspendStream()
 *---------------------------------------------------------------------------
 *
 * Synopsis:
 *
 * Return:    (See header file)
 */
BtStatus AVDTP_SuspendStream(AvdtpStream *Stream)
{
    BtStatus status;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Stream != 0);

    OS_LockStack();

    if (Stream->chnl) {
        if (IsNodeOnList(&AVDTP(chnlList), &Stream->chnl->node)) {
            if (IsNodeOnList(&Stream->chnl->streamList, &Stream->node)) {
                if (Stream->state == AVDTP_STRM_STATE_STREAMING) {
                    status = AvdtpSigStreamSuspend(Stream->chnl, Stream);
                } else {
                    status = BT_STATUS_FAILED;
                }
            } else {
                status = BT_STATUS_NOT_FOUND;
            }
        } else {
            status = BT_STATUS_NOT_FOUND;
        }
    } else {
        status = BT_STATUS_NO_CONNECTION;
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 * AVDTP_SecurityControlReq()
 *---------------------------------------------------------------------------
 *
 * Synopsis:
 *
 * Return:    (See header file)
 */
BtStatus AVDTP_SecurityControlReq(AvdtpStream *Stream, U8 *Data, U16 Len)
{
    BtStatus status;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Stream != 0);
    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Data != 0);

    OS_LockStack();

    if (Stream->chnl) {
        if (IsNodeOnList(&AVDTP(chnlList), &Stream->chnl->node)) {
            if (IsNodeOnList(&Stream->chnl->streamList, &Stream->node)) {
                if ((Stream->state != AVDTP_STRM_STATE_IDLE) &&
                    (Stream->state != AVDTP_STRM_STATE_CLOSING)) {
                    if (Stream->cpCfg.cpType) {
                        status = AvdtpSigStreamSecurityCtrl(Stream->chnl, 
                                                          Stream, 
                                                          Len, Data,
                                                          AVTP_MSG_TYPE_COMMAND, 
                                                          AVDTP_ERR_NO_ERROR);
                    } else {
                        status = BT_STATUS_NOT_SUPPORTED;
                    }
                } else {
                    status = BT_STATUS_FAILED;
                }
            } else {
                status = BT_STATUS_NOT_FOUND;
            }
        } else {
            status = BT_STATUS_NOT_FOUND;
        }
    } else {
        status = BT_STATUS_NO_CONNECTION;
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 * AVDTP_SecurityControlRsp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:
 *
 * Return:    (See header file)
 */
BtStatus AVDTP_SecurityControlRsp(AvdtpStream *Stream, 
                                  U8 *Data, U16 Len, 
                                  AvdtpError Error)
{
    BtStatus status;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Stream != 0);
    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Data != 0);

    OS_LockStack();

    if (Stream->chnl) {
        if (IsNodeOnList(&AVDTP(chnlList), &Stream->chnl->node)) {
            if (IsNodeOnList(&Stream->chnl->streamList, &Stream->node)) {
                if ((Stream->state != AVDTP_STRM_STATE_IDLE) &&
                    (Stream->state != AVDTP_STRM_STATE_CLOSING)) {
                    if (!Error) {
                        status = AvdtpSigStreamSecurityCtrl(Stream->chnl, 
                                                           Stream, 
                                                           Len, Data,
                                                           AVTP_MSG_TYPE_ACCEPT,
                                                           0);
                    } else {
                        status = AvdtpSigStreamSecurityCtrl(Stream->chnl, 
                                                           Stream, 
                                                           Len, Data,
                                                           AVTP_MSG_TYPE_REJECT, 
                                                           Error);
                    }
                } else {
                    status = BT_STATUS_FAILED;
                }
            } else {
                status = BT_STATUS_NOT_FOUND;
            }
        } else {
            status = BT_STATUS_NOT_FOUND;
        }
    } else {
        status = BT_STATUS_NO_CONNECTION;
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 * AVDTP_AbortStream()
 *---------------------------------------------------------------------------
 *
 * Synopsis:
 *
 * Return:    (See header file)
 */
BtStatus AVDTP_AbortStream(AvdtpStream *Stream)
{
    BtStatus status;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Stream != 0);

    OS_LockStack();

    if (Stream->chnl) {
        if (IsNodeOnList(&AVDTP(chnlList), &Stream->chnl->node)) {
            if (IsNodeOnList(&Stream->chnl->streamList, &Stream->node)) {
                if ((Stream->state >= AVDTP_STRM_STATE_CONFIGURED) &&
                    (Stream->state <= AVDTP_STRM_STATE_CLOSING)) {
                    status = AvdtpSigStreamAbort(Stream->chnl, Stream);
                    if (status == BT_STATUS_PENDING) {
                        Stream->state = AVDTP_STRM_STATE_ABORTING;
                    }
                } else {
                    status = BT_STATUS_FAILED;
                }
            } else {
                status = BT_STATUS_NOT_FOUND;
            }
        } else {
            status = BT_STATUS_NOT_FOUND;
        }
    } else {
        status = BT_STATUS_NO_CONNECTION;
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 * AVDTP_StreamSendRawPacket()
 *---------------------------------------------------------------------------
 *
 * Synopsis:
 *
 * Return:    (See header file)
 */
BtStatus AVDTP_StreamSendRawPacket(AvdtpStream *Stream, BtPacket *Packet)
{
    BtStatus status;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Stream != 0);
    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Packet != 0);
    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Packet->data != 0);

    OS_LockStack();

    if (Stream->chnl) {
        if (IsNodeOnList(&AVDTP(chnlList), &Stream->chnl->node)) {
            if (IsNodeOnList(&Stream->chnl->streamList, &Stream->node)) {
                if (Stream->state == AVDTP_STRM_STATE_STREAMING) {
                    status = L2CAP_SendData(Stream->conn.l2ChannelId, Packet);
                } else {
                    status = BT_STATUS_FAILED;
                }
            } else {
                status = BT_STATUS_NOT_FOUND;
            }
        } else {
            status = BT_STATUS_NOT_FOUND;
        }
    } else {
        status = BT_STATUS_NO_CONNECTION;
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 * AVDTP_GetRemoteDevice()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Returns the remote device structure used by this channel.  Only 
 *            valid when a connection exists.
 *
 * Return:    (See header file)
 */
BtRemoteDevice * AVDTP_GetRemoteDevice(AvdtpChannel *Channel)
{
    BtRemoteDevice *remDev = 0;

    OS_LockStack();

    if (Channel != 0) {
        if (IsNodeOnList(&AVDTP(chnlList), &Channel->node) &&
            (Channel->conn.state != AVDTP_STATE_DISCONNECTED)) {

            /* This is a registered/connected channel */
            remDev = Channel->conn.remDev;
        }
    }

    OS_UnlockStack();

    return remDev;
}

/*---------------------------------------------------------------------------
 * AVDTP_IsConnected()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Returns the current state of an AVDTP connection.
 *
 * Return:    (See header file)
 */
BOOL AVDTP_IsConnected(AvdtpChannel *Channel)
{
    BOOL connected = FALSE;

    OS_LockStack();

    if (Channel != 0) {
        if (IsNodeOnList(&AVDTP(chnlList), &Channel->node)) {
            connected = Channel->conn.state == AVDTP_STATE_CONNECTED ? 
                        TRUE : FALSE;
        }
    }

    OS_UnlockStack();

    return connected;
}

/*---------------------------------------------------------------------------
 * AVDTP_StreamState()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Returns the current state of a stream.
 *
 * Return:    (See header file)
 */
AvdtpStreamState AVDTP_GetStreamState(AvdtpStream *Stream)
{
    AvdtpStreamState state = AVDTP_STRM_STATE_IDLE;

    OS_LockStack();

    if (Stream != 0) {
        if (Stream->chnl) {
            if (IsNodeOnList(&Stream->chnl->streamList, &Stream->node)) {
                /* This is a registered stream */
                state = Stream->state;
            }
        }
    }

    OS_UnlockStack();

    return state;
}

/*---------------------------------------------------------------------------
 * AVDTP_CreateMediaHeader()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Used to create a media packet header before transmiting media data 
 *            using the AVDTP_StreamSendRawPacket().
 *
 * Returns:   (See header file)
 */
U16 AVDTP_CreateMediaHeader(AvdtpMediaHeader *Header, U8 *Buffer)
{
    I16 i = Header->csrcCount;

    if (Buffer) {
        /* First Octet */
        Buffer[0] = ((Header->version << 6) & 0xC0) | 
                    ((Header->padding << 5) & 0x20) |
                    (Header->csrcCount > 0 ? 0x10 : 0x00) | 
                    (Header->csrcCount & 0x0F);

        /* 2nd Octet */
        Buffer[1] = ((Header->marker << 7) & 0x80) | 
                    (Header->payloadType & 0x7F);

        /* Sequence number */
        StoreBE16(&Buffer[2], Header->sequenceNumber);

        /* Timestamp */
        StoreBE32(&Buffer[4], Header->timestamp);

        /* Synch source */
        StoreBE32(&Buffer[8], Header->ssrc);

        /* Contributing Sources */
        for (i = 0; i < Header->csrcCount; i++) {
            if (Buffer) {
                StoreBE32(&Buffer[12 + i * 4], Header->csrcList[i]);
            }
        }
    }

    /* Return the header size */
    return 12 + i * 4;
}                            

/*---------------------------------------------------------------------------
 * AVDTP_ParseMediaHeader()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Used to parse a media packet header when a media packet has 
 *            been received from the remote device.
 *
 * Returns:   (See header file)
 */
U16 AVDTP_ParseMediaHeader(AvdtpMediaHeader *Header, U8 *Buffer)
{
    I16 i = 0;

    Header->version = Buffer[0] >> 6;
    Header->padding = (Buffer[0] & 0x20) >> 5;
    Header->csrcCount = Buffer[0] & 0x0F;
    Header->marker = Buffer[1] >> 7;
    Header->payloadType = (Buffer[1] & 0x7F);
    Header->sequenceNumber = BEtoHost16(&Buffer[2]);
    Header->timestamp = BEtoHost32(&Buffer[4]);
    Header->ssrc = BEtoHost32(&Buffer[8]);

    if (Buffer[0] & 0x10) {
        /* An extention header exists */
        for (i = 0; i < Header->csrcCount; i++) {
            Header->csrcList[i] = BEtoHost32(&Buffer[12 + i]);
        }
    }

    /* Return the header size */
    return 12 + i * 4;
}

