/****************************************************************************
 *
 * File:
 *     $Workfile:avdtpcon.c$ for iAnywhere AV SDK, version 1.2.3
 *     $Revision:37$
 *
 * Description: This file contains the L2CAP/ACL connection state machine 
 *              for AVDTP connections.
 *             
 * Created:     Mar 12, 2004
 *
 * Copyright 2004 - 2005 Extended Systems, Inc.

 * Portions copyright 2005-2006 iAnywhere Solutions, Inc.

 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any means, 
 * or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, Inc. 
 * This work contains confidential and proprietary information of Extended 
 * Systems, Inc. which is protected by copyright, trade secret, trademark and 
 * other intellectual property rights.
 *
 ****************************************************************************/

#include "avdtp.h"
#include "sys/avdtpcon.h"
#include "sys/avalloc.h"

/* Function Prototypes */
static void AvdtpFreeConn(AvdtpConn *Conn, BtStatus Status);

/*---------------------------------------------------------------------------
 *            AvdtpConnDisconnected()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Disconnected state.
 *
 */
static void AvdtpConnDisconnected(AvdtpConn *Conn, L2capCallbackParms *Parms)
{
    AvdtpConnCallbackParms connParms;

    switch (Parms->event) {
    case L2EVENT_CONNECT_IND:

        /* Notify the application */
        Conn->remDev = Parms->aclLink;
        connParms.event = AVDTP_CONN_EVENT_CONNECT_IND;
        connParms.ptrs.remDev = Conn->remDev;
        Conn->callback(Conn, &connParms);
        break;
    case L2EVENT_DISCONNECTED:
        /* Conn disconnected, notify the app */
        AvdtpFreeConn(Conn, Parms->status);
        break;
    }
}

/*---------------------------------------------------------------------------
 *            AvdtpConnPending()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Conn Pending state.
 *
 */
static void AvdtpConnConnPending(AvdtpConn *Conn, L2capCallbackParms *Parms)
{
    AvdtpConnCallbackParms connParms;

    connParms.status = Parms->status;

    switch (Parms->event) {
    case L2EVENT_CONNECTED:
        /* Connection is up */
        Conn->state = AVDTP_STATE_CONNECTED;
        Conn->remDev = Parms->aclLink;

        /* Notify the application */
        connParms.event = AVDTP_CONN_EVENT_CONNECT;
        connParms.ptrs.remDev = Conn->remDev;
        Conn->callback(Conn, &connParms);
        break;
    case L2EVENT_DISCONNECTED:
        /* Conn disconnected, notify the app */
        AvdtpFreeConn(Conn, Parms->status);
        break;
    }
}

/*---------------------------------------------------------------------------
 *            AvdtpConnConnIncoming()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Connect Incoming state.
 *
 */
static void AvdtpConnConnIncoming(AvdtpConn *Conn, L2capCallbackParms *Parms)
{
    AvdtpConnCallbackParms connParms;

    connParms.status = Parms->status;

    switch (Parms->event) {
    case L2EVENT_CONNECTED:
        /* Connection is up */
        Conn->state = AVDTP_STATE_CONNECTED;
        Conn->remDev = Parms->aclLink;

        /* Notify the application */
        connParms.event = AVDTP_CONN_EVENT_CONNECT;
        connParms.ptrs.remDev = Conn->remDev;
        Conn->callback(Conn, &connParms);
        break;
    case L2EVENT_DISCONNECTED:
        /* Conn disconnected, notify the app */
        AvdtpFreeConn(Conn, Parms->status);
        break;
    }
}

/*---------------------------------------------------------------------------
 *            AvdtpConnDiscPending()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Disconnect Pending state.
 *
 */
static void AvdtpConnDiscPending(AvdtpConn *Conn, L2capCallbackParms *Parms)
{
    AvdtpConnCallbackParms connParms;

    connParms.status = Parms->status;

    switch (Parms->event) {
    case L2EVENT_DISCONNECTED:
        /* Conn disconnected, notify the app */
        AvdtpFreeConn(Conn, Parms->status);
        break;
    case L2EVENT_PACKET_HANDLED:
        /* Notify the application */
        connParms.event = AVDTP_CONN_EVENT_DATA_SENT;
        connParms.ptrs.packet = Parms->ptrs.packet;
        Conn->callback(Conn, &connParms);
        break;
    }
}

/*---------------------------------------------------------------------------
 *            AvdtpConnDiscIncoming()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Disconnected state.
 *
 */
static void AvdtpConnDiscIncoming(AvdtpConn *Conn, L2capCallbackParms *Parms)
{
    AvdtpConnCallbackParms connParms;

    connParms.status = Parms->status;

    switch (Parms->event) {
    case L2EVENT_DISCONNECTED:
        /* Conn disconnected, notify the app */
        AvdtpFreeConn(Conn, Parms->status);
        break;
    case L2EVENT_PACKET_HANDLED:
        /* Notify the application */
        connParms.event = AVDTP_CONN_EVENT_DATA_SENT;
        connParms.ptrs.packet = Parms->ptrs.packet;
        Conn->callback(Conn, &connParms);
        break;
    }
}

/*---------------------------------------------------------------------------
 *            AvdtpConnConnected()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Connected state.
 *
 */
static void AvdtpConnConnected(AvdtpConn *Conn, L2capCallbackParms *Parms)
{
    AvdtpConnCallbackParms connParms;

    connParms.status = Parms->status;

    switch (Parms->event) {
    case L2EVENT_DISCONNECTED:
        /* Conn disconnected, notify the app */
        AvdtpFreeConn(Conn, Parms->status);
        break;
    case L2EVENT_PACKET_HANDLED:
        /* Notify the application */
        connParms.event = AVDTP_CONN_EVENT_DATA_SENT;
        connParms.ptrs.packet = Parms->ptrs.packet;
        Conn->callback(Conn, &connParms);
        break;
    case L2EVENT_DATA_IND:
        connParms.event = AVDTP_CONN_EVENT_DATA_IND;
        connParms.dataLen = Parms->dataLen;
        connParms.ptrs.data = Parms->ptrs.data;
        Conn->callback(Conn, &connParms);
        break;
    }
}

/*---------------------------------------------------------------------------
 *            AvdtpConnInitStateMachine()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initializes the state table.
 *
 */
void AvdtpConnInitStateMachine(void)
{
    AVDTP(connState)[0] = AvdtpConnDisconnected;
    AVDTP(connState)[1] = AvdtpConnConnPending;
    AVDTP(connState)[2] = AvdtpConnConnIncoming;
    AVDTP(connState)[3] = AvdtpConnDiscPending;
    AVDTP(connState)[4] = AvdtpConnDiscIncoming;
    AVDTP(connState)[5] = AvdtpConnConnected;
}

/*---------------------------------------------------------------------------
 *            AvdtpConnFindChannel()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Find the AV channel matching the remote device.
 *
 */
AvdtpChannel * AvdtpConnFindChannel(BtRemoteDevice *remDev)
{
    AvdtpChannel    *chnl;

    /* Find the AV channel associated with this device */
    chnl = (AvdtpChannel *)GetHeadList(&AVDTP(chnlList));
    while (chnl != (AvdtpChannel *)&AVDTP(chnlList)) {
        if ((chnl->conn.remDev != 0) && (chnl->conn.remDev == remDev)) {
            /* Found it */
            break;
        }

        /* Get the next connection structure */
        chnl = (AvdtpChannel *)GetNextNode(&chnl->node);
    }

    if (chnl == (AvdtpChannel *)&AVDTP(chnlList)) {
        /* Channel was not found */
        chnl = 0;
    }

    return chnl;
}

/*---------------------------------------------------------------------------
 *            AvdtpConnFindFreeChannel()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Find a free AV channel.
 *
 */
AvdtpChannel * AvdtpConnFindFreeChannel(void)
{
    AvdtpChannel    *chnl;

    /* Find the AV channel associated with this device */
    chnl = (AvdtpChannel *)GetHeadList(&AVDTP(chnlList));
    while (chnl != (AvdtpChannel *)&AVDTP(chnlList)) {
        if (chnl->conn.state == AVDTP_STATE_DISCONNECTED) {
            /* Found it */
            break;
        }

        /* Get the next connection structure */
        chnl = (AvdtpChannel *)GetNextNode(&chnl->node);
    }

    if (chnl == (AvdtpChannel *)&AVDTP(chnlList)) {
        /* Channel was not found */
        chnl = 0;
    }

    return chnl;
}

/*---------------------------------------------------------------------------
 *            AvdtpConnFindConn()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Find the AV connection matching the L2CAP Channel ID.
 *
 */
AvdtpConn * AvdtpConnFindConn(AvdtpChannel *Chnl, 
                              L2capChannelId L2ChannelId)
{
    AvdtpConn   *conn;
    AvdtpStream *strm;

    conn = &Chnl->conn;
    if (Chnl->conn.l2ChannelId != L2ChannelId) {
        /* This channel belongs to one of the media transports */
        strm = (AvdtpStream *)GetHeadList(&Chnl->streamList);
        while (strm != (AvdtpStream *)&Chnl->streamList) {
            if ((strm->conn.l2ChannelId == L2ChannelId) &&
                (strm->state >= AVDTP_STRM_STATE_OPEN)) {
                /* Found It */
                conn = &strm->conn;
                break;
            }

            /* Get the next connection structure */
            strm = (AvdtpStream *)GetNextNode(&strm->node);
        }

        if (strm == (AvdtpStream *)&Chnl->streamList) {
            conn = 0;
        }
    }

    return conn;
}

/*---------------------------------------------------------------------------
 *            AvdtpConnFindOrphanedStreamConn()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Find the stream connection where no channel connection exists.
 *            This state can happen when the ACL link is terminated.  Since
 *            the first L2CAP disconnect indication we receive is for the 
 *            first connection created (the signal channel connection), any 
 *            streams associated with that signal channel are orphaned when
 *            the signal channel resources are cleaned up.
 *
 */
AvdtpConn * AvdtpConnFindOrphanedStreamConn(BtRemoteDevice *RemDev,
                                            L2capChannelId L2ChannelId)
{
    AvdtpChannel    *chnl;
    AvdtpConn       *conn = 0;

    /* Find the AV channel associated with this device */
    chnl = (AvdtpChannel *)GetHeadList(&AVDTP(chnlList));
    while (chnl != (AvdtpChannel *)&AVDTP(chnlList)) {
        if (!IsListEmpty(&chnl->streamList)) {
            conn = AvdtpConnFindConn(chnl, L2ChannelId);
            if (conn && (conn->remDev == RemDev)) {
                break;
            }
        }

        /* Get the next connection structure */
        chnl = (AvdtpChannel *)GetNextNode(&chnl->node);
    }

    if (chnl == (AvdtpChannel *)&AVDTP(chnlList)) {
        /* Channel was not found */
        conn = 0;
    }

    return conn;
}

/*---------------------------------------------------------------------------
 *            AvL2Callback()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  L2CAP callback routine.
 *
 */
void AvL2Callback(L2capChannelId L2ChannelId, L2capCallbackParms *Parms)
{
    AvdtpChannel      *chnl;
    AvdtpConn         *conn = 0;

    chnl = AvdtpConnFindChannel(Parms->aclLink);
    if (chnl) {
        /* Found a matching channel */
        if (Parms->event != L2EVENT_CONNECT_IND) {
            /* Connection exists */
            conn = AvdtpConnFindConn(chnl, L2ChannelId);
            if (!conn) {
                /* Ignore this event, unknown channel */
                return;
            }
        } else {
            /* New incoming stream */
            conn = AvdtpConnFindConn(chnl, 0);
            if (!conn) {
                /* No stream to connect to */
                (void)L2CAP_ConnectRsp(L2ChannelId, 
                                       L2CONN_REJECT_NO_RESOURCES, 0);
                return;
            }
            conn->l2ChannelId = L2ChannelId;
        }
    } else {
        /* No channel found */
        if (Parms->event == L2EVENT_CONNECT_IND) {
            /* No channel, search for connection */
            chnl = AvdtpConnFindFreeChannel();
            if (chnl) {
                conn = &chnl->conn;
                conn->l2ChannelId = L2ChannelId;
            } else {
                /* No channel to connect to */
                (void)L2CAP_ConnectRsp(L2ChannelId, 
                                       L2CONN_REJECT_NO_RESOURCES, 0);
                return;
            }
        } else {
            /* No channel, search all allocated streams */
            conn = AvdtpConnFindOrphanedStreamConn(Parms->aclLink, L2ChannelId);
            if (!conn) {
                /* No channels for this event */
                return;
            }
        }
    }

    /* Call the state machine */
    AVDTP(connState)[conn->state](conn, Parms);
}


/*---------------------------------------------------------------------------
 *            AvdtpFreeConn()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Clear the connection state and notify the app.
 *
 */
static void AvdtpFreeConn(AvdtpConn *Conn, BtStatus Status) 
{
    AvdtpConnCallbackParms connParms;

    connParms.status = Status;

    if (Conn->state != AVDTP_STATE_DISCONNECTED) {

        /* Clean up the state */
        Conn->state = AVDTP_STATE_DISCONNECTED;
        Conn->l2ChannelId = 0;

        /* Notify the application */
        connParms.event = AVDTP_CONN_EVENT_DISCONNECT;
        connParms.ptrs.remDev = Conn->remDev;
        Conn->remDev = 0;
        Conn->callback(Conn, &connParms);
    }
}

/*---------------------------------------------------------------------------
 *            AvdtpInitConn()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initialize the connection structure.
 *
 */
void AvdtpInitConn(AvdtpConn *Conn, AvdtpConnCallback Callback)
{
    /* Initialize the connection structure */
    Conn->remDev = 0;
    Conn->callback = Callback;
    Conn->state = AVDTP_STATE_DISCONNECTED;
    Conn->l2ChannelId = 0;
}

/*---------------------------------------------------------------------------
 *            AvdtpDisconnect()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Disconnect the L2CAP channel.
 *
 */
BtStatus AvdtpDisconnect(AvdtpConn *Conn)
{
    BtStatus status = BT_STATUS_SUCCESS;

    if (Conn->state != AVDTP_STATE_DISCONNECTED) {
        status = L2CAP_DisconnectReq(Conn->l2ChannelId);
        if (status == BT_STATUS_PENDING) {
            /* Disconnect Started, wait for L2CAP callback */
            Conn->state = AVDTP_STATE_DISC_PENDING;
        }
    }

    return status;
}

/*---------------------------------------------------------------------------
 *            AvdtpConnect()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Create an L2CAP channel.
 *
 */
BtStatus AvdtpConnect(AvdtpConn *Conn, BtRemoteDevice *RemDev, BOOL reliable)
{
    BtStatus status;

    UNUSED_PARAMETER(reliable);

    /* Establish the L2CAP link */
    status = L2CAP_ConnectReq(&AVDTP(psm), BT_PSM_AVDTP, 
                              RemDev, 0, 
                              &Conn->l2ChannelId);

    if (status == BT_STATUS_PENDING) {
        /* Connection Initiated */
        Conn->state = AVDTP_STATE_CONN_PENDING;
        Conn->remDev = RemDev;
    }

    return status;
}

/*---------------------------------------------------------------------------
 *            AvdtpConnectRsp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Respond to the incoming L2CAP connection.
 *
 */
BtStatus AvdtpConnectRsp(AvdtpConn *Conn, BOOL Accept)
{
    BtStatus status;

    /* Incoming connection, accept it, go to conn incoming state */
    if (Accept) {
        status = L2CAP_ConnectRsp(Conn->l2ChannelId, L2CONN_ACCEPTED, 0);
    } else {
        status = L2CAP_ConnectRsp(Conn->l2ChannelId, L2CONN_REJECT_NO_RESOURCES, 
                                  0);
    }

    if (status == BT_STATUS_PENDING) {
        Conn->state = AVDTP_STATE_CONN_INCOMING;
    }

    return status;
}

/*---------------------------------------------------------------------------
 *            AvdtpIsConnect()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Determine if AVDTP is connected.
 *
 */
BOOL AvdtpIsConnected(AvdtpConn *Conn)
{
    if (Conn->state == AVDTP_STATE_DISCONNECTED) {
        return FALSE;
    }

    return TRUE;
}

