/****************************************************************************
 *
 * File:
 *     $Workfile:avsigmgr.c$ for iAnywhere AV SDK, version 1.2.3
 *     $Revision:79$
 *
 * Description: This file contains the Signal Manager.
 *             
 * Created:     Mar 23, 2004
 *
 * Copyright 2004 - 2005 Extended Systems, Inc.

 * Portions copyright 2005-2006 iAnywhere Solutions, Inc.

 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any means, 
 * or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, Inc.  
 * This work contains confidential and proprietary information of Extended 
 * Systems, Inc. which is protected by copyright, trade secret, trademark and 
 * other intellectual property rights.
 *
 ****************************************************************************/

#include "avdtp.h"
#include "sys/avalloc.h"
#include "sys/avsigmgr.h"

/* Function Prototypes */
static void AvdtpSigMgrSignalCallback(AvtpChannel *SigChnl,
                                      AvtpCallbackParms *Parms);
static void AvdtpSigMgrConnCallback(AvdtpConn *Conn, 
                                    AvdtpConnCallbackParms *Parms);
static void AvdtpSigEncodeCapabilities(AvtpPacket *SigPacket, 
                                       AvdtpStream *Stream,
                                       U8 signal);

/*---------------------------------------------------------------------------
 *            AvdtpInsertStreamList()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Inserts a stream (in order) onto the unused stream list.
 *
 */
void AvdtpInsertStreamList(AvdtpStream *Stream)
{
    AvdtpStream *strm = (AvdtpStream *)GetHeadList(&AVDTP(streamList));
    while (strm != (AvdtpStream *)&AVDTP(streamList)) {
        if (Stream->locStrmId > strm->locStrmId) {
            strm = (AvdtpStream *)GetNextNode(&strm->node);
            continue;
        }
        break;
    }
    
    InsertTailList(&strm->node, &Stream->node);
}

/*---------------------------------------------------------------------------
 *            AvdtpSigInit()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initialize the Signal Manager handler.
 *
 */
void AvdtpSigInit(AvdtpChannel *Chnl)
{
    AvdtpInitConn(&Chnl->conn, AvdtpSigMgrConnCallback);
}

/*---------------------------------------------------------------------------
 *            AvdtpSigSendCommand()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Send a signal command.
 *
 */
static BtStatus AvdtpSigSendCommand(AvdtpChannel *Chnl)
{
    BtStatus status;

    if ((Chnl->txState == AVTP_SIG_MGR_TXSTATE_IDLE) ||
        (Chnl->cmdSigPacket->txId == AVDTP_SIG_ABORT)) {
        Chnl->txState = AVTP_SIG_MGR_TXSTATE_WAIT_RSP;
        status = AVTP_SendStart(&Chnl->sigChnl, Chnl->cmdSigPacket);
    } else {
        status = BT_STATUS_BUSY;
    }

    return status;
}

/*---------------------------------------------------------------------------
 *            AvdtpSigStreamSendSimpleCmd()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Send a simple command on the signal channel.
 *
 */
BtStatus AvdtpSigStreamSendSimpleCmd(AvdtpChannel *Chnl,
                                     U8 SigCmd,
                                     AvdtpStream *Stream,
                                     AvdtpStreamId StrmId)
{
    BtStatus status = BT_STATUS_NO_RESOURCES;

    if (!IsListEmpty(&AVDTP(txPacketList))) {
        Chnl->cmdSigPacket = (AvtpPacket *)RemoveHeadList(&AVDTP(txPacketList));
        if (SigCmd != AVDTP_SIG_DISCOVER) {
            Chnl->cmdSigPacket->txData[0] = StrmId << 2;
            Chnl->cmdSigPacket->txDataLen = 1;
        } else {
            Chnl->cmdSigPacket->txDataLen = 0;
        }

        Chnl->cmdSigPacket->msgType = AVTP_MSG_TYPE_COMMAND;
        Chnl->cmdSigPacket->msgHdrLen = 0;
        Chnl->cmdSigPacket->txIdSize = 1;
        Chnl->cmdSigPacket->txId = SigCmd;
        Chnl->currentTxStream = Stream;
        status = AvdtpSigSendCommand(Chnl);
        if (status != BT_STATUS_PENDING) {
            InsertTailList(&AVDTP(txPacketList), &Chnl->cmdSigPacket->node);
        }
    }

    return status;
}

/*---------------------------------------------------------------------------
 *            AvdtpSigStreamSetConfig()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Set the stream configuration.
 *
 */
BtStatus AvdtpSigStreamSetConfig(AvdtpChannel *Chnl, AvdtpStream *Stream,
                                 AvdtpCodec *Codec, AvdtpContentProt *Cp)
{
    BtStatus status = BT_STATUS_NO_RESOURCES;

    if (!IsListEmpty(&AVDTP(txPacketList))) {
        Chnl->cmdSigPacket = (AvtpPacket *)RemoveHeadList(&AVDTP(txPacketList));
        Chnl->cmdSigPacket->txData[0] = Stream->remStrmId << 2;
        Chnl->cmdSigPacket->txData[1] = Stream->locStrmId << 2;
        Chnl->cmdSigPacket->txDataLen = 2;
        Chnl->cmdSigPacket->msgType = AVTP_MSG_TYPE_COMMAND;
        Chnl->cmdSigPacket->msgHdrLen = 0;
        Chnl->cmdSigPacket->txIdSize = 1;
        Chnl->cmdSigPacket->txId = AVDTP_SIG_SET_CONFIG;

        /* Save the content protection request */
        if (Cp) {
            /* Content protection is being configured */
            Stream->cfgReq.cp.cpType = Cp->cpType;
            Stream->cfgReq.cp.dataLen = min(Cp->dataLen, AVDTP_MAX_CP_VALUE_SIZE);
            OS_MemCopy((U8 *)Stream->cfgReq.cp.data, (U8 *)Cp->data, Cp->dataLen);
        } else {
            /* No content protection */
            Stream->cfgReq.cp.cpType = 0;
            Stream->cfgReq.cp.dataLen = 0;
        }

        /* Save the codec request */
        Stream->cfgReq.codec.codecType = Codec->codecType;
        Stream->cfgReq.codec.elemLen = min(Codec->elemLen, 
                                           AVDTP_MAX_CODEC_ELEM_SIZE);
        OS_MemCopy((U8 *)Stream->cfgReq.codec.elements, (U8 *)Codec->elements, 
                   Codec->elemLen);

        /* Encode the packet */
        Stream->chnl = Chnl;
        AvdtpSigEncodeCapabilities(Chnl->cmdSigPacket, Stream, 
                                   AVDTP_SIG_SET_CONFIG);
        Chnl->currentTxStream = Stream;
        status = AvdtpSigSendCommand(Chnl);
        if (status != BT_STATUS_PENDING) {
            InsertTailList(&AVDTP(txPacketList), &Chnl->cmdSigPacket->node);
        }
    }

    return status;
}

/*---------------------------------------------------------------------------
 *            AvdtpSigStreamReconfig()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Reconfigure the stream.
 *
 */
BtStatus AvdtpSigStreamReconfig(AvdtpChannel *Chnl, AvdtpStream *Stream,
                                AvdtpCodec *Codec, AvdtpContentProt *Cp)
{
    BtStatus status = BT_STATUS_NO_RESOURCES;

    if (!IsListEmpty(&AVDTP(txPacketList))) {
        Chnl->cmdSigPacket = (AvtpPacket *)RemoveHeadList(&AVDTP(txPacketList));
        Chnl->cmdSigPacket->txData[0] = Stream->remStrmId << 2;
        Chnl->cmdSigPacket->txDataLen = 1;
        Chnl->cmdSigPacket->msgType = AVTP_MSG_TYPE_COMMAND;
        Chnl->cmdSigPacket->msgHdrLen = 0;
        Chnl->cmdSigPacket->txIdSize = 1;
        Chnl->cmdSigPacket->txId = AVDTP_SIG_RECONFIG;

        /* Save the content protection request */
        if (Cp) {
            /* Content protection is being configured */
            Stream->cfgReq.cp.cpType = Cp->cpType;
            Stream->cfgReq.cp.dataLen = min(Cp->dataLen, AVDTP_MAX_CP_VALUE_SIZE);
            OS_MemCopy((U8 *)Stream->cfgReq.cp.data, (U8 *)Cp->data, Cp->dataLen);
        } else {
            /* No content protection */
            Stream->cfgReq.cp.cpType = 0;
            Stream->cfgReq.cp.dataLen = 0;
        }

        /* Save the codec request */
        Stream->cfgReq.codec.codecType = Codec->codecType;
        Stream->cfgReq.codec.elemLen = min(Codec->elemLen, 
                                           AVDTP_MAX_CODEC_ELEM_SIZE);
        OS_MemCopy((U8 *)Stream->cfgReq.codec.elements, (U8 *)Codec->elements, 
                   Codec->elemLen);

        /* Encode the packet */
        AvdtpSigEncodeCapabilities(Chnl->cmdSigPacket, Stream, AVDTP_SIG_RECONFIG);
        Chnl->currentTxStream = Stream;
        status = AvdtpSigSendCommand(Chnl);
        if (status != BT_STATUS_PENDING) {
            InsertTailList(&AVDTP(txPacketList), &Chnl->cmdSigPacket->node);
        }
    }

    return status;
}

/*---------------------------------------------------------------------------
 *            AvdtpSigStreamReconfigRsp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Send start stream response.
 *
 */
BtStatus AvdtpSigStreamReconfigRsp(AvdtpChannel *Chnl, AvdtpStream *Stream, 
                                   AvdtpError Error, AvdtpCapabilityType Type)
{
    BtStatus status = BT_STATUS_NO_RESOURCES;

    if (!IsListEmpty(&AVDTP(txPacketList))) {
        Chnl->rspSigPacket = (AvtpPacket *)RemoveHeadList(&AVDTP(txPacketList));
        Chnl->rspSigPacket->msgHdrLen = 0;
        Chnl->rspSigPacket->txIdSize = 1;
        Chnl->rspSigPacket->txId = AVDTP_SIG_RECONFIG;
        if (Error == AVDTP_ERR_NO_ERROR) {

            /* Configuration was accepted, move it to configured variables */
            Stream->cpCfg.cpType = Stream->cfgReq.cp.cpType;
            Stream->cpCfg.dataLen = Stream->cfgReq.cp.dataLen;
            OS_MemCopy((U8 *)Stream->cpCfg.data, (U8 *)Stream->cfgReq.cp.data, 
                       Stream->cfgReq.cp.dataLen);
            Stream->codecCfg.codecType = Stream->cfgReq.codec.codecType;
            Stream->codecCfg.elemLen = Stream->cfgReq.codec.elemLen;
            OS_MemCopy((U8 *)Stream->codecCfg.elements, 
                       (U8 *)Stream->cfgReq.codec.elements,
                       Stream->cfgReq.codec.elemLen);

            Chnl->rspSigPacket->msgType = AVTP_MSG_TYPE_ACCEPT;
            Chnl->rspSigPacket->txDataLen = 0;
        } else {
            Chnl->rspSigPacket->msgType = AVTP_MSG_TYPE_REJECT;
            Chnl->rspSigPacket->txData[0] = Type;
            Chnl->rspSigPacket->txData[1] = Error;
            Chnl->rspSigPacket->txDataLen = 2;
        }

        Chnl->currentTxStream = Stream;
        status = AVTP_SendStart(&Chnl->sigChnl, Chnl->rspSigPacket);
        if (status != BT_STATUS_PENDING) {
            InsertTailList(&AVDTP(txPacketList), &Chnl->rspSigPacket->node);
        }
    }

    return status;
}


/*---------------------------------------------------------------------------
 *            AvdtpSigStreamSecurityCtrl()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Send security control information.
 *
 */
BtStatus AvdtpSigStreamSecurityCtrl(AvdtpChannel *Chnl, AvdtpStream *Stream,
                                    U16 Len, U8 *Data, U8 msgType, 
                                    AvdtpError Error)
{
    BtStatus     status = BT_STATUS_NO_RESOURCES;
    U8           offset = 0;
    AvtpPacket *sigPacket;

    if (msgType == AVTP_MSG_TYPE_COMMAND) {
        /* Encode the stream ID */
        if (!IsListEmpty(&AVDTP(txPacketList))) {
            Chnl->cmdSigPacket = (AvtpPacket *)RemoveHeadList(&AVDTP(txPacketList));
            sigPacket = Chnl->cmdSigPacket;
            sigPacket->txData[offset++] = Stream->remStrmId << 2;
        } else {
            goto exit;
        }
    } else {
        if (!IsListEmpty(&AVDTP(txPacketList))) {
            Chnl->rspSigPacket = (AvtpPacket *)RemoveHeadList(&AVDTP(txPacketList));
            sigPacket = Chnl->rspSigPacket;
        } else {
            goto exit;
        }
    }

    if (!Error) {
        /* Encode the security data */
        OS_MemCopy(&sigPacket->txData[offset], Data, Len);
        offset += Len;
    } else {
        sigPacket->txData[offset] = Error;
        offset++;
    }

    sigPacket->txDataLen = offset;
    sigPacket->msgType = msgType;
    sigPacket->msgHdrLen = 0;
    sigPacket->txIdSize = 1;
    sigPacket->txId = AVDTP_SIG_SECURITY_CTRL;
    Chnl->currentTxStream = Stream;
    if (msgType == AVTP_MSG_TYPE_COMMAND) {
        status = AvdtpSigSendCommand(Chnl);
    } else {
        status = AVTP_SendStart(&Chnl->sigChnl, sigPacket);
    }

    if (status != BT_STATUS_PENDING) {
        InsertTailList(&AVDTP(txPacketList), &sigPacket->node);
    }

exit:

    return status;
}

/*---------------------------------------------------------------------------
 *            AvdtpSigStreamStartRsp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Send start stream response.
 *
 */
BtStatus AvdtpSigStreamStartRsp(AvdtpChannel *Chnl, AvdtpStream *Stream, 
                                AvdtpError Error)
{
    BtStatus status = BT_STATUS_PENDING;

    if (Chnl->streamsLeft) {
        if (!IsListEmpty(&AVDTP(txPacketList))) {
            if (Chnl->strmStartRspFlag != TRUE) {
                /* Allocated the response packet */
                Chnl->rspSigPacket = (AvtpPacket *)RemoveHeadList(&AVDTP(txPacketList));
            }
            Chnl->rspSigPacket->msgHdrLen = 0;
            Chnl->rspSigPacket->txIdSize = 1;
            Chnl->rspSigPacket->txId = AVDTP_SIG_START;
            if (Error == AVDTP_ERR_NO_ERROR) {
                if (Chnl->rspSigPacket->msgType != AVTP_MSG_TYPE_REJECT) {
                    Chnl->rspSigPacket->msgType = AVTP_MSG_TYPE_ACCEPT;
                    Chnl->rspSigPacket->txDataLen = 0;
                }
            } else {
                Chnl->rspSigPacket->msgType = AVTP_MSG_TYPE_REJECT;
                Chnl->rspSigPacket->txData[0] = Stream->locStrmId;
                Chnl->rspSigPacket->txData[1] = Error;
                Chnl->rspSigPacket->txDataLen = 2;
            }

            if (Chnl->streamsLeft == 1) {
                Chnl->streamsLeft--;
                Chnl->currentTxStream = Stream;
                status = AVTP_SendStart(&Chnl->sigChnl, Chnl->rspSigPacket);
                if (status != BT_STATUS_PENDING) {
                    InsertTailList(&AVDTP(txPacketList), &Chnl->rspSigPacket->node);
                }

                Chnl->strmStartRspFlag = FALSE;
            }
        } else {
            status = BT_STATUS_NO_RESOURCES;
        }
    } else {
        status = BT_STATUS_FAILED;
    }

    return status;
}

/*---------------------------------------------------------------------------
 *            AvdtpSigEncodeCapabilities()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Encode capabilties into the signal channel.
 *
 */
static void AvdtpSigEncodeCapabilities(AvtpPacket *SigPacket, 
                                       AvdtpStream *Stream,
                                       U8 signal)
{
    AvdtpContentProt *cp;
    AvdtpCodec *codec;

    if (signal != AVDTP_SIG_RECONFIG) {
        /* Add the Media Transport */
        SigPacket->txData[SigPacket->txDataLen] = AVDTP_SRV_CAT_MEDIA_TRANSPORT;
        SigPacket->txData[SigPacket->txDataLen+1] = 0;
        SigPacket->txDataLen += 2;
    }

    if ((signal == AVDTP_SIG_SET_CONFIG) ||
        (signal == AVDTP_SIG_RECONFIG)) {
        /* Requested codec */
        codec = &Stream->cfgReq.codec;
    } else if (signal == AVDTP_SIG_GET_CONFIG) {
        /* Configured codec */
        codec = &Stream->codecCfg;
    } else {
        /* Registered codec */
        codec = Stream->codec;
    }

    /* Add the Codec */
    SigPacket->txData[SigPacket->txDataLen] = AVDTP_SRV_CAT_MEDIA_CODEC;
    SigPacket->txData[SigPacket->txDataLen+1] = codec->elemLen + 2;
    SigPacket->txData[SigPacket->txDataLen+2] = Stream->mediaType << 4;
    SigPacket->txData[SigPacket->txDataLen+3] = codec->codecType;
    OS_MemCopy((U8*)&SigPacket->txData[SigPacket->txDataLen+4], 
               (U8*)codec->elements, codec->elemLen);
    SigPacket->txDataLen += codec->elemLen + 4;

    switch (signal) {
    case AVDTP_SIG_SET_CONFIG:
    case AVDTP_SIG_RECONFIG:
    case AVDTP_SIG_GET_CONFIG:
        if (signal == AVDTP_SIG_GET_CONFIG) {
            cp = &Stream->cpCfg;
        } else {
            cp = &Stream->cfgReq.cp;
        }
        if (cp->cpType) {
            /* Add the configured Content Protection */
            SigPacket->txData[SigPacket->txDataLen] = 
                AVDTP_SRV_CAT_CONTENT_PROTECTION;
            SigPacket->txData[SigPacket->txDataLen+1] = cp->dataLen + 2;
            StoreLE16(&SigPacket->txData[SigPacket->txDataLen+2], cp->cpType);
            OS_MemCopy(&SigPacket->txData[SigPacket->txDataLen+4], 
                       cp->data, cp->dataLen);
            SigPacket->txDataLen += cp->dataLen + 4;
        }
        break;
    default:
        /* Add the registered Content protection */
        cp = (AvdtpContentProt *)GetHeadList(&Stream->cpList);
        while (cp != (AvdtpContentProt *)&Stream->cpList) {

            SigPacket->txData[SigPacket->txDataLen] = 
                AVDTP_SRV_CAT_CONTENT_PROTECTION;
            SigPacket->txData[SigPacket->txDataLen+1] = cp->dataLen + 2;
            StoreLE16(&SigPacket->txData[SigPacket->txDataLen+2], cp->cpType);
            OS_MemCopy(&SigPacket->txData[SigPacket->txDataLen+4], 
                       cp->data, cp->dataLen);

            /* Get the next connection structure */
            SigPacket->txDataLen += cp->dataLen + 4;
            cp = (AvdtpContentProt *)GetNextNode(&cp->node);
        }
        break;
    }
}

/*---------------------------------------------------------------------------
 *            AvdtpSigFindStream()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Find a stream for based on the stream ID.
 *
 */
static AvdtpStream * AvdtpSigFindStream(AvdtpChannel *Chnl, 
                                        AvdtpStreamId StrmId)
{
    AvdtpStream *strm;

    /* Look on current channel first */
    strm = (AvdtpStream *)GetHeadList(&Chnl->streamList);
    while (strm != (AvdtpStream *)&Chnl->streamList) {

        if (strm->locStrmId == StrmId) {
            /* Found it */
            break;
        }

        /* Get the next connection structure */
        strm = (AvdtpStream *)GetNextNode(&strm->node);
    }

    if (strm == (AvdtpStream *)&Chnl->streamList) {
        /* Look for registered stream */
        strm = (AvdtpStream *)GetHeadList(&AVDTP(streamList));
        while (strm != (AvdtpStream *)&AVDTP(streamList)) {

            if (strm->locStrmId == StrmId) {
                /* Found it */
                break;
            }

            /* Get the next connection structure */
            strm = (AvdtpStream *)GetNextNode(&strm->node);
        }

        if (strm == (AvdtpStream *)&AVDTP(streamList)) {
            /* Not Found */
            strm = 0;
        }
    }

    return strm;
}

/*---------------------------------------------------------------------------
 *            AvdtpSigParseDiscoveries()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Parse discovered streams from the signal channel.
 *
 */
static void AvdtpSigParseDiscoveries(AvdtpChannel *Chnl, 
                                     AvtpCallbackParms *Parms)
{
    AvdtpCallbackParms info;
    AvdtpStreamInfo streamInfo;
    U16 rxLen = Parms->len;
    U16 n;
    U8  i;

    /* Store the receive buffer */
    Chnl->parser.rxBuff = Parms->data;
    info.status = Parms->status;
    info.error = AVDTP_ERR_NO_ERROR;

    /* Process the data in Chnl->parser.rxBuff of length rxLen */
    while (rxLen > 0) {
        Assert(Chnl->parser.stageLen <= 2);
        Assert((Chnl->parser.curStageOff == 0) ||
               (Chnl->parser.curStageOff < Chnl->parser.stageLen));

        n = min((U16)(Chnl->parser.stageLen - Chnl->parser.curStageOff), 
                rxLen);
        Assert(n <= 2);

        /* Stage the data */
        for (i = Chnl->parser.curStageOff; n > 0; n--, i++) {
            Chnl->parser.stageBuff[i] = *(Chnl->parser.rxBuff);
            (Chnl->parser.rxBuff)++;
            rxLen--;
            Chnl->parser.curStageOff++;
        }

        /* Only call the state machine if the data has been completely
         * staged.
         */
        if (Chnl->parser.curStageOff == Chnl->parser.stageLen) {

            /* Discovery responses */
            info.event = AVDTP_EVENT_STREAM_INFO;
            info.channel = Chnl;
            streamInfo.id = (Chnl->parser.stageBuff[0] & 0xFC) >> 2;
            streamInfo.inUse = (Chnl->parser.stageBuff[0] & 0x02) >> 1;
            streamInfo.mediaType = Chnl->parser.stageBuff[1] >> 4;
            streamInfo.streamType = (Chnl->parser.stageBuff[1] & 0x08) >> 3;

            /* Make the stream info callback */
            info.len = sizeof(AvdtpStreamInfo);
            info.p.streamInfo = &streamInfo;
            Chnl->callback(Chnl, &info);

            Chnl->parser.curStageOff = 0;
        }
    }

    if (Parms->packetsLeft == 0) {
        /* No more streams, Make the discover complete callback */
        info.event = AVDTP_EVENT_DISCOVER_CNF;
        info.len = 0;
        Chnl->callback(Chnl, &info);
    }
}

/*---------------------------------------------------------------------------
 *            AvdtpSigParseCapabilityRsp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Parse capabilties from the signal channel.
 *
 */
static void AvdtpSigParseCapabilityRsp(AvdtpChannel *Chnl, 
                                       AvtpCallbackParms *Parms,
                                       BOOL config)
{
    AvdtpCallbackParms info;
    U16 rxLen = Parms->len;
    BOOL makeCallback;
    U16 n;
    U8  i;

    /* Store the receive buffer */
    Chnl->parser.rxBuff = Parms->data;

    /* Start setting callback parms */
    info.channel = Chnl;
    info.status = Parms->status;
    info.error = AVDTP_ERR_NO_ERROR;

    /* Process the data in Chnl->parser.rxBuff of length rxLen */
    while (rxLen > 0) {
        Assert(Chnl->parser.stageLen <= MAX_CAPABILITY_SIZE);
        Assert((Chnl->parser.curStageOff == 0) ||
               (Chnl->parser.curStageOff < Chnl->parser.stageLen));

        n = min((U16)(Chnl->parser.stageLen - Chnl->parser.curStageOff), 
                rxLen);
        Assert(n <= MAX_CAPABILITY_SIZE);

        /* Stage the data */
        for (i = Chnl->parser.curStageOff; n > 0; n--, i++) {
            Chnl->parser.stageBuff[i] = *(Chnl->parser.rxBuff);
            (Chnl->parser.rxBuff)++;
            rxLen--;
            Chnl->parser.curStageOff++;
        }

        /* Only call the state machine if the data has been completely
         * staged.
         */
        if (Chnl->parser.curStageOff == Chnl->parser.stageLen) {
            switch (Chnl->parser.rxState) {
            case 0:   /* Service Category and length */
                Chnl->parser.stageLen = Chnl->parser.stageBuff[1];
                Report(("AVDTP: stageLen = %d\n", Chnl->parser.stageLen));
                Chnl->capability.type = Chnl->parser.stageBuff[0];
                Chnl->parser.rxState++;
                break;
            case 1:   /* Elements */
                switch (Chnl->capability.type) {
                case AVDTP_SRV_CAT_MEDIA_CODEC:
                    Chnl->capability.p.codec.codecType =
                        Chnl->parser.stageBuff[1];
                    Chnl->capability.p.codec.elemLen = 
                        Chnl->parser.stageLen - 2;
                    Chnl->capability.p.codec.elements =
                        &Chnl->parser.stageBuff[2];
                    makeCallback = TRUE;
                    break;
                case AVDTP_SRV_CAT_CONTENT_PROTECTION:
                    Chnl->capability.p.cp.cpType = 
                        LEtoHost16(&Chnl->parser.stageBuff[0]);
                    Chnl->capability.p.cp.dataLen =
                        Chnl->parser.stageLen - 2;
                    Chnl->capability.p.cp.data =
                        &Chnl->parser.stageBuff[2];
                    makeCallback = TRUE;
                    break;
                default:
                    /* This capability is transport specific */
                    makeCallback = FALSE;
                    break;
                }

                Chnl->parser.rxState = 0;
                Chnl->parser.stageLen = 2;

                if (makeCallback) {
                    /* Set up the callback parms */
                    info.p.capability = &Chnl->capability;
                    info.len = sizeof(AvdtpCapability);
                    if (config) {
                        info.event = AVDTP_EVENT_CFG_CAPABILITY;
                    } else {
                        info.event = AVDTP_EVENT_CAPABILITY;
                    }
                    Chnl->callback(Chnl, &info);
                }
                break;
            }

            Chnl->parser.curStageOff = 0;
        }
    }

    if (Parms->packetsLeft == 0) {
        /* No more capabilities, make the Get Capabilities complete callback */
        if (config) {
            info.event = AVDTP_EVENT_GET_CFG_CNF;
        } else {
            info.event = AVDTP_EVENT_GET_CAP_CNF;
        }
        info.len = 0;
        Chnl->callback(Chnl, &info);
    }
}

/*---------------------------------------------------------------------------
 *            AvdtpSigParseConfigCmd()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Parse config parameters from the signal channel.
 *
 */
static BOOL AvdtpSigParseConfigCmd(AvdtpChannel *Chnl, AvdtpStream *Stream,
                                   AvtpCallbackParms *Parms,
                                   BOOL reconfig)
{
    AvdtpCallbackParms info;
    U16 rxLen = Parms->len;
    AvdtpContentProt *cp;
    AvdtpContentProtType cpType;
    U16 n;
    U8  i;

    /* Store the receive buffer */
    Chnl->parser.rxBuff = Parms->data;

    /* Start setting callback parms */
    info.channel = Chnl;
    info.stream = Stream;
    info.error = AVDTP_ERR_NO_ERROR;
    info.status = Parms->status;

    /* Initialize the config request */
    Stream->cfgReq.cp.cpType = 0;
    Stream->cfgReq.cp.dataLen = 0;

    /* Process the data in Chnl->parser.rxBuff of length rxLen */
    while (rxLen > 0) {
        Assert(Chnl->parser.stageLen <= MAX_CAPABILITY_SIZE);
        Assert((Chnl->parser.curStageOff == 0) ||
               (Chnl->parser.curStageOff < Chnl->parser.stageLen));

        n = min((U16)(Chnl->parser.stageLen - Chnl->parser.curStageOff), 
                rxLen);
        Assert(n <= MAX_CAPABILITY_SIZE);

        /* Stage the data */
        for (i = Chnl->parser.curStageOff; n > 0; n--, i++) {
            Chnl->parser.stageBuff[i] = *(Chnl->parser.rxBuff);
            (Chnl->parser.rxBuff)++;
            rxLen--;
            Chnl->parser.curStageOff++;
        }

        /* Only call the state machine if the data has been completely
         * staged.
         */
        if (Chnl->parser.curStageOff == Chnl->parser.stageLen) {
            switch (Chnl->parser.rxState) {
            case 0: /* Stream ID's */
                if (!reconfig) {
                    Stream->remStrmId = Chnl->parser.stageBuff[1] >> 2;
                } else {
                    Chnl->parser.stageLen = 2;
                }
                Chnl->parser.rxState++;
                break;
            case 1:   /* Service Category and length */
                Chnl->parser.stageLen = Chnl->parser.stageBuff[1];
                Report(("AVDTP: stageLen = %d\n", Chnl->parser.stageLen));
                Chnl->capability.type = Chnl->parser.stageBuff[0];
                Chnl->parser.rxState++;
                if (Chnl->parser.stageLen != 0) {
                    break;
                }
                /* Fall Through */
            case 2:   /* Elements */
                switch (Chnl->capability.type) {
                case AVDTP_SRV_CAT_MEDIA_TRANSPORT:
                    /* This is supported */
                    if (reconfig) {
                        /* Cannot reconfig transport */
                        Chnl->rspSigPacket->txData[0] = Chnl->capability.type;
                        Chnl->rspSigPacket->txData[1] = 
                            AVDTP_ERR_INVALID_CAPABILITIES;
                        Chnl->rspSigPacket->txDataLen = 2;

                        /* Abort parsing */
                        Chnl->parser.curStageOff = 0;
                        Chnl->parser.rxState = 0;
                        return FALSE;
                    } else if (Chnl->parser.stageLen) {
                        /* Invalid - Reject */
                        Chnl->rspSigPacket->txData[0] = Chnl->capability.type;
                        Chnl->rspSigPacket->txData[1] = 
                            AVDTP_ERR_BAD_MEDIA_TRANSPORT_FORMAT;
                        Chnl->rspSigPacket->txDataLen = 2;

                        /* Abort parsing */
                        Chnl->parser.curStageOff = 0;
                        Chnl->parser.rxState = 0;
                        return FALSE;
                    }
                    break;
                case AVDTP_SRV_CAT_MEDIA_CODEC:
                    /* Media Codec is supported, verify codec type and 
                     * call the application to validate codec parameters.
                     */
                    if (Stream->codec->codecType == Chnl->parser.stageBuff[1]) {
                        Stream->cfgReq.codec.codecType = 
                            Chnl->parser.stageBuff[1];
                        Stream->cfgReq.codec.elemLen = 
                            min(Chnl->parser.stageLen - 2, 
                                AVDTP_MAX_CODEC_ELEM_SIZE);
                        OS_MemCopy((U8 *)Stream->cfgReq.codec.elements,
                                   (U8 *)&Chnl->parser.stageBuff[2],
                                   Stream->cfgReq.codec.elemLen);
                    } else {
                        /* Invalid codec - Reject */
                        Chnl->rspSigPacket->txData[0] = Chnl->capability.type;
                        Chnl->rspSigPacket->txData[1] = 
                            AVDTP_ERR_NOT_SUPPORTED_CODEC_TYPE;
                        Chnl->rspSigPacket->txDataLen = 2;

                        /* Abort parsing */
                        Chnl->parser.curStageOff = 0;
                        Chnl->parser.rxState = 0;
                        return FALSE;
                    }
                    break;
                case AVDTP_SRV_CAT_CONTENT_PROTECTION:
                    /* Content Protection is supported, verify cp type and call 
                     * the application to validate CP parameters.
                     */
                    cpType = LEtoHost16(&Chnl->parser.stageBuff[0]);
                    if ((Chnl->parser.stageLen > 2) &&
                        ((cpType == AVDTP_CP_TYPE_DTCP) || 
                         (cpType == AVDTP_CP_TYPE_SCMS_T))) {
                        cp = (AvdtpContentProt *)GetHeadList(&Stream->cpList);
                        while (cp != (AvdtpContentProt *)&Stream->cpList) {
                            if (cp->cpType == cpType) {
                                /* This content protection type exists */
                                Stream->cfgReq.cp.cpType = 
                                    LEtoHost16(&Chnl->parser.stageBuff[0]);
                                Stream->cfgReq.cp.dataLen = 
                                    min(Chnl->parser.stageLen - 2, 
                                        AVDTP_MAX_CP_VALUE_SIZE);
                                OS_MemCopy((U8 *)Stream->cfgReq.cp.data,
                                           (U8 *)&Chnl->parser.stageBuff[2],
                                           Stream->cfgReq.cp.dataLen);
                                break;
                            }

                            cp = (AvdtpContentProt *)GetNextNode(&cp->node);
                        }

                        if (cp == (AvdtpContentProt *)&Stream->cpList) {
                            /* Unsupported Content Protection - Reject */
                            Chnl->rspSigPacket->txData[0] = 
                                Chnl->capability.type;
                            Chnl->rspSigPacket->txData[1] = 
                                AVDTP_ERR_UNSUPPORTED_CONFIGURATION;
                            Chnl->rspSigPacket->txDataLen = 2;

                            /* Abort parsing */
                            Chnl->parser.curStageOff = 0;
                            Chnl->parser.rxState = 0;
                            return FALSE;
                        }
                    } else {
                        /* Bad Content Protection format */
                        Chnl->rspSigPacket->txData[0] = Chnl->capability.type;
                        Chnl->rspSigPacket->txData[1] = AVDTP_ERR_BAD_CP_FORMAT;
                        Chnl->rspSigPacket->txDataLen = 2;

                        /* Abort parsing */
                        Chnl->parser.curStageOff = 0;
                        Chnl->parser.rxState = 0;
                        return FALSE;
                    }

                    break;
                case AVDTP_SRV_CAT_REPORTING:
                case AVDTP_SRV_CAT_RECOVERY:
                case AVDTP_SRV_CAT_HEADER_COMPRESSION:
                case AVDTP_SRV_CAT_MULTIPLEXING:
                    /* Error, unsupported capability */
                    Chnl->rspSigPacket->txData[0] = Chnl->capability.type;
                    Chnl->rspSigPacket->txData[1] = 
                        AVDTP_ERR_UNSUPPORTED_CONFIGURATION;
                    Chnl->rspSigPacket->txDataLen = 2;

                    /* Abort parsing */
                    Chnl->parser.curStageOff = 0;
                    Chnl->parser.rxState = 0;
                    return FALSE;
                default:
                    /* Error, unknown capability */
                    Chnl->rspSigPacket->txData[0] = Chnl->capability.type;
                    Chnl->rspSigPacket->txData[1] = 
                        AVDTP_ERR_BAD_SERV_CATEGORY;
                    Chnl->rspSigPacket->txDataLen = 2;

                    /* Abort parsing */
                    Chnl->parser.curStageOff = 0;
                    Chnl->parser.rxState = 0;
                    return FALSE;
                }

                Chnl->parser.rxState = 1;
                Chnl->parser.stageLen = 2;
                break;
            }

            Chnl->parser.curStageOff = 0;
        }
    }

    if (Parms->packetsLeft == 0) {

        if (Chnl->parser.rxState != 1) {
            /* Bad Length in a capability */
            Chnl->rspSigPacket->txData[0] = Chnl->capability.type;
            if (Chnl->capability.type == AVDTP_SRV_CAT_MEDIA_TRANSPORT) {
                Chnl->rspSigPacket->txData[1] = 
                    AVDTP_ERR_BAD_MEDIA_TRANSPORT_FORMAT;
            } else {
                Chnl->rspSigPacket->txData[1] = AVDTP_ERR_BAD_LENGTH;
            }
            Chnl->rspSigPacket->txDataLen = 2;

            /* Abort parsing */
            Chnl->parser.curStageOff = 0;
            Chnl->parser.rxState = 0;
            return FALSE;
        }

        Chnl->parser.rxState = 0;
        if (reconfig) {
            /* No more capabilities, make the Config callback */
            info.event = AVDTP_EVENT_STREAM_RECONFIG_IND;
        } else {
            /* Change state and register the stream on the channel */

            info.event = AVDTP_EVENT_STREAM_OPEN_IND;
            Stream->state = AVDTP_STRM_STATE_CONFIGURED;
            Stream->chnl = Chnl;
            RemoveEntryList(&Stream->node);
            InsertTailList(&Chnl->streamList, &Stream->node);
        }

        info.p.configReq = &Stream->cfgReq;
        info.len = sizeof(AvdtpConfigRequest);
        Chnl->callback(Chnl, &info);
    }

    return TRUE;
}

/*---------------------------------------------------------------------------
 *            AvdtpSigParseStreamStart()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Parse the Stream Start command.
 *
 */
static BOOL AvdtpSigParseStreamStart(AvdtpChannel *Chnl, 
                                     AvtpCallbackParms *Parms)
{
    AvdtpCallbackParms info;
    U16 rxLen = Parms->len;
    U8 i = 0;

    /* Store the receive buffer */
    Chnl->parser.rxBuff = Parms->data;
    Chnl->streamsLeft = 0;
    Chnl->rspSigPacket->msgType = 0;
    Chnl->streamsLeft = Parms->len;

    /* Process the data in Chnl->parser.rxBuff of length rxLen */
    while (rxLen-- > 0) {

        info.status = Parms->status;
        info.error = AVDTP_ERR_NO_ERROR;
        info.event = AVDTP_EVENT_STREAM_START_IND;
        info.channel = Chnl;

        info.stream = AvdtpSigFindStream(Chnl, 
                                        (AvdtpStreamId)(Parms->data[i++] >> 2));
        if (!info.stream) {     /* UPF: Fail here for BI-20-C */
            Chnl->rspSigPacket->txData[0] = Parms->data[i - 1];
            Chnl->rspSigPacket->txData[1] = AVDTP_ERR_BAD_ACP_SEID;
            Chnl->rspSigPacket->txDataLen = 2;
            Chnl->streamsLeft = 0;
            return FALSE;
        } else if (info.stream->state != AVDTP_STRM_STATE_OPEN) {
            Chnl->rspSigPacket->txData[0] = Parms->data[i - 1];
            Chnl->rspSigPacket->txData[1] = AVDTP_ERR_BAD_STATE;
            Chnl->rspSigPacket->txDataLen = 2;
            Chnl->streamsLeft = 0;
            return FALSE;
        }

        /* Make the stream info callback */
        info.len = 0;
        Chnl->callback(Chnl, &info);
    }

    return TRUE;
}

/*---------------------------------------------------------------------------
 *            AvdtpSigParseStreamSuspend()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Parse the Stream Suspend command.
 *
 */
static BOOL AvdtpSigParseStreamSuspend(AvdtpChannel *Chnl, 
                                       AvtpCallbackParms *Parms)
{
    AvdtpCallbackParms info;
    U16 rxLen = Parms->len;
    U8 i = 0;

    /* Store the receive buffer */
    Chnl->parser.rxBuff = Parms->data;

    /* Process the data in Chnl->parser.rxBuff of length rxLen */
    while (rxLen-- > 0) {

        info.stream = AvdtpSigFindStream(Chnl, 
                                        (AvdtpStreamId)(Parms->data[i++] >> 2));
        if (!info.stream) {     /* UPF: Fail here for BI-20-C */
            Chnl->rspSigPacket->txData[0] = Parms->data[i - 1];
            Chnl->rspSigPacket->txData[1] = AVDTP_ERR_BAD_ACP_SEID;
            Chnl->rspSigPacket->txDataLen = 2;
            return FALSE;
        } else if (info.stream->state != AVDTP_STRM_STATE_STREAMING) {
            Chnl->rspSigPacket->txData[0] = Parms->data[i - 1];
            Chnl->rspSigPacket->txData[1] = AVDTP_ERR_BAD_STATE;
            Chnl->rspSigPacket->txDataLen = 2;
            return FALSE;
        }

        info.stream->state = AVDTP_STRM_STATE_OPEN;
        info.channel = Chnl;
        info.event = AVDTP_EVENT_STREAM_SUSPENDED;
        info.error = AVDTP_ERR_NO_ERROR;
        info.len = 0;
        Chnl->callback(Chnl, &info);
    }

    return TRUE;
}

/*---------------------------------------------------------------------------
 *            AvdtpCloseStream()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Close the stream.
 *
 */
static void AvdtpCloseStream(AvdtpStream *Stream, AvdtpError Error, 
                             AvdtpCapability *Cap, U16 Len, BtStatus Status)
{
    AvdtpCallbackParms info;

    /* Reset Stream State */
    Stream->state = AVDTP_STRM_STATE_IDLE;
    OS_MemSet((U8 *)&Stream->cpCfg, 0, sizeof(AvdtpContentProt));
    OS_MemSet((U8 *)&Stream->codecCfg, 0, sizeof(AvdtpCodec));
    Stream->cpCfg.data = Stream->cpCfgValue;
    Stream->codecCfg.elements = Stream->codecCfgElem;
    OS_MemSet((U8 *)&Stream->cfgReq, 0, sizeof(AvdtpConfigRequest));
    Stream->cfgReq.cp.data = Stream->cpReqValue;
    Stream->cfgReq.codec.elements = Stream->codecReqElem;
    Stream->abError = AVDTP_ERR_NO_ERROR;

    /* Indicate closed event to the application */
    info.channel = Stream->chnl;
    info.event = AVDTP_EVENT_STREAM_CLOSED;
    info.status = Status;
    info.error = Error;
    info.stream = Stream;
    info.p.capability = Cap;
    info.len = Len;

    if (Stream->chnl) {
        Stream->chnl->callback(Stream->chnl, &info);
        Stream->chnl = 0;
    }
}

/*---------------------------------------------------------------------------
 *            AvdtpSigHandleCommand()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle a signal command.
 *
 */
static void AvdtpSigHandleCommand(AvdtpChannel *Chnl, AvtpCallbackParms *Parms)
{
    AvdtpStream *strm = 0;
    U8          i = 0;
    U8          msgType = AVTP_MSG_TYPE_REJECT;
    U8          oldState;
    BOOL        respond = TRUE;
    AvdtpChannel *chnl;
    AvdtpCallbackParms info;
    BtStatus    status;
    AvtpPacket *rspPacket;

    if (IsListEmpty(&AVDTP(txPacketList))) {
        /* No packet resource available, the command will be ignored */
        Report(("AVDTP: No resource available in AvdtpSigHandleCommand()\n"));
        return;
    } else {
        rspPacket = (AvtpPacket *)RemoveHeadList(&AVDTP(txPacketList));
        Chnl->rspSigPacket = rspPacket;
    }

    if (Chnl->txState == AVTP_SIG_MGR_TXSTATE_WAIT_RSP) {
        if (Parms->rxId == 0) {
            /* Received a General Reject */
            Chnl->txState = AVTP_SIG_MGR_TXSTATE_IDLE;
            respond = FALSE;
            goto error_send;
        }
    }

    if (Parms->rxId == AVDTP_SIG_DISCOVER) {
        strm = (AvdtpStream *)GetHeadList(&AVDTP(streamList));
    } else {
        if (Chnl->rxState == AVTP_SIG_MGR_RXSTATE_BEGIN) {
            strm = AvdtpSigFindStream(Chnl, (U8)(Parms->data[0] >> 2));
            Chnl->currentRxStream = strm;
        } else {
            strm = Chnl->currentRxStream;
        }
    }

    info.status = Parms->status;

    switch (Parms->rxId) {
    case AVDTP_SIG_DISCOVER:

        if (Parms->len != 0) {
            /* Reject the request (BAD LENGTH) */
            rspPacket->txData[0] = AVDTP_ERR_BAD_LENGTH;
            rspPacket->txDataLen = 1;
            break;
        }

        /* Return the registered (unused) streams */
        Assert(Parms->packetsLeft == 0);
        rspPacket->txDataLen = 0;
        while (strm != (AvdtpStream *)&AVDTP(streamList)) {

            rspPacket->txData[i] = strm->locStrmId << 2;
            if (strm->state != AVDTP_STRM_STATE_IDLE) {
                /* Stream in use */
                rspPacket->txData[i] |= 0x02;
            }

            rspPacket->txData[i+1] = strm->mediaType << 4;
            rspPacket->txData[i+1] |= strm->endPointType << 3;

            /* Get the next connection structure */
            strm = (AvdtpStream *)GetNextNode(&strm->node);
            rspPacket->txDataLen += 2;
            msgType = AVTP_MSG_TYPE_ACCEPT;
            i += 2;
        }

        /* Return active (in use) streams */
        chnl = (AvdtpChannel *)GetHeadList(&AVDTP(chnlList));
        while (chnl != (AvdtpChannel *)&AVDTP(chnlList)) {
            strm = (AvdtpStream *)GetHeadList(&chnl->streamList);
            while (strm != (AvdtpStream *)&chnl->streamList) {

                rspPacket->txData[i] = strm->locStrmId << 2;
                if (strm->state != AVDTP_STRM_STATE_IDLE) {
                    /* Stream in use */
                    rspPacket->txData[i] |= 0x02;
                }

                rspPacket->txData[i+1] = strm->mediaType << 4;
                rspPacket->txData[i+1] |= strm->endPointType << 3;

                /* Get the next connection structure */
                strm = (AvdtpStream *)GetNextNode(&strm->node);
                rspPacket->txDataLen += 2;
                msgType = AVTP_MSG_TYPE_ACCEPT;
                i += 2;
            }

            chnl = (AvdtpChannel *)GetNextNode(&chnl->node);
        }

        if (msgType == AVTP_MSG_TYPE_REJECT) {
            /* No streams available; reject with anonymous error code. */
            rspPacket->txData[0] = 0xC0;
            rspPacket->txDataLen = 1;
        }
        break;
    case AVDTP_SIG_GET_CAPABILITIES:
        if (strm) {
            if (Parms->len != 1) {
                /* Reject the request (BAD LENGTH) */
                rspPacket->txData[0] = AVDTP_ERR_BAD_LENGTH;
                rspPacket->txDataLen = 1;
                break;
            }

            /* Return the capabilities of the specified stream */
            Assert(Parms->packetsLeft == 0);
            rspPacket->txDataLen = 0;
            AvdtpSigEncodeCapabilities(rspPacket, strm, 
                                       AVDTP_SIG_GET_CAPABILITIES);
            msgType = AVTP_MSG_TYPE_ACCEPT;
        } else {
            rspPacket->txData[0] = AVDTP_ERR_BAD_ACP_SEID;
            rspPacket->txDataLen = 1;
        }
        break;
    case AVDTP_SIG_GET_CONFIG:
        if (strm) {
            if (Parms->len != 1) {
                /* Reject the request (BAD LENGTH) */
                rspPacket->txData[0] = AVDTP_ERR_BAD_LENGTH;
                rspPacket->txDataLen = 1;
                break;
            }

            if ((strm->state > AVDTP_STRM_STATE_IDLE) ||
                (strm->state <= AVDTP_STRM_STATE_STREAMING)) {
                /* Return the current configuration */
                Assert(Parms->packetsLeft == 0);
                rspPacket->txDataLen = 0;
                AvdtpSigEncodeCapabilities(rspPacket, strm, 
                                           AVDTP_SIG_GET_CONFIG);
                msgType = AVTP_MSG_TYPE_ACCEPT;
            } else {
                /* Reject the request (BAD STATE) */
                rspPacket->txData[0] = AVDTP_ERR_BAD_STATE;
                rspPacket->txDataLen = 1;
            }
        } else {
            rspPacket->txData[0] = AVDTP_ERR_BAD_ACP_SEID;
            rspPacket->txDataLen = 1;
        }
        break;
    case AVDTP_SIG_RECONFIG:    /* UPF: Fail here for SMG/BI-15-C */
        if (strm) {
            if (strm->state == AVDTP_STRM_STATE_OPEN) {
                if (Chnl->rxState == AVTP_SIG_MGR_RXSTATE_BEGIN) {
                    Chnl->parser.rxState = 0;
                    Chnl->parser.stageLen = 1;

                    /* Preset the request to existing configuration */
                    strm->cfgReq.codec.codecType = strm->codecCfg.codecType;
                    strm->cfgReq.codec.elemLen = strm->codecCfg.elemLen;
                    OS_MemCopy((U8 *)strm->cfgReq.codec.elements,
                               (U8 *)strm->codecCfg.elements,
                               strm->codecCfg.elemLen);
                    strm->cfgReq.cp.cpType = strm->cpCfg.cpType;
                    strm->cfgReq.cp.dataLen = strm->cpCfg.dataLen;
                    OS_MemCopy((U8 *)strm->cfgReq.cp.data,
                               (U8 *)strm->cfgReq.cp.data,
                               strm->cpCfg.dataLen);
                }
                if (AvdtpSigParseConfigCmd(Chnl, strm, Parms, TRUE)) {
                    respond = FALSE;
                }
            } else {
                /* Reject the request (BAD STATE) */
                rspPacket->txData[0] = Parms->data[1];
                rspPacket->txData[1] = AVDTP_ERR_NOT_IN_USE;
                rspPacket->txDataLen = 2;
            }
        } else {
            /* Reject the request (BAD SEID) */
            rspPacket->txData[0] = Parms->data[1];
            rspPacket->txData[1] = AVDTP_ERR_BAD_ACP_SEID;
            rspPacket->txDataLen = 2;
        }
        break;
    case AVDTP_SIG_SET_CONFIG:      /* UPF: Fail here for BTR/BI-01-C */
        if (strm) {
            if (strm->state == AVDTP_STRM_STATE_IDLE) {
                if (Chnl->rxState == AVTP_SIG_MGR_RXSTATE_BEGIN) {
                    Chnl->parser.rxState = 0;
                    Chnl->parser.stageLen = 2;

                    /* Preset the request to existing configuration */
                    strm->cfgReq.codec.codecType = strm->codecCfg.codecType;
                    strm->cfgReq.codec.elemLen = strm->codecCfg.elemLen;
                    OS_MemCopy((U8 *)strm->cfgReq.codec.elements,
                               (U8 *)strm->codecCfg.elements,
                               strm->codecCfg.elemLen);
                    strm->cfgReq.cp.cpType = strm->cpCfg.cpType;
                    strm->cfgReq.cp.dataLen = strm->cpCfg.dataLen;
                    OS_MemCopy((U8 *)strm->cfgReq.cp.data,
                               (U8 *)strm->cfgReq.cp.data,
                               strm->cpCfg.dataLen);
                }
                if (AvdtpSigParseConfigCmd(Chnl, strm, Parms, FALSE)) {
                    respond = FALSE;
                }
            } else {
                /* Reject the request (IN USE) */
                rspPacket->txData[0] = Parms->data[2];
                rspPacket->txData[1] = AVDTP_ERR_IN_USE;
                rspPacket->txDataLen = 2;
            }
        } else {
            /* Reject the request (BAD SEID) */
            rspPacket->txData[0] = Parms->data[2];
            rspPacket->txData[1] = AVDTP_ERR_BAD_ACP_SEID;
            rspPacket->txDataLen = 2;
        }
        break;
    case AVDTP_SIG_OPEN:        /* UPF: Fail here for SMG/BI-17-C */
        if (strm) {
            if (Parms->len != 1) {
                /* Reject the request (BAD LENGTH) */
                rspPacket->txData[0] = AVDTP_ERR_BAD_LENGTH;
                rspPacket->txDataLen = 1;
                break;
            }

            /* Change state and register the stream on the channel */
            if (strm->state == AVDTP_STRM_STATE_CONFIGURED) {
                msgType = AVTP_MSG_TYPE_ACCEPT;
                rspPacket->txDataLen = 0;
                strm->state = AVDTP_STRM_STATE_OPEN;
            } else {
                /* Reject the request (BAD STATE) */
                rspPacket->txData[0] = AVDTP_ERR_BAD_STATE;
                rspPacket->txDataLen = 1;
            }
        } else {
            /* Reject the request (BAD SEID) */
            rspPacket->txData[0] = AVDTP_ERR_BAD_ACP_SEID;
            rspPacket->txDataLen = 1;
        }
        break;
    case AVDTP_SIG_START:
        if (AvdtpSigParseStreamStart(Chnl, Parms)) {
            respond = FALSE;
        }
        break;
    case AVDTP_SIG_CLOSE:       /* UPF: Fail here for SMG/BI-23,24-C */
        if (strm) {
            if (Parms->len != 1) {
                /* Reject the request (BAD LENGTH) */
                rspPacket->txData[0] = AVDTP_ERR_BAD_LENGTH;
                rspPacket->txDataLen = 1;
                break;
            }

            if ((strm->state == AVDTP_STRM_STATE_OPEN) ||
                (strm->state == AVDTP_STRM_STATE_STREAMING)) {
                msgType = AVTP_MSG_TYPE_ACCEPT;
                info.channel = Chnl;
                info.event = AVDTP_EVENT_STREAM_CLOSE_IND;
                info.error = AVDTP_ERR_NO_ERROR;
                info.stream = strm;
                info.len = 0;
                Chnl->callback(Chnl, &info);
                rspPacket->txDataLen = 0;
            } else {
                /* Reject the request (BAD STATE) */
                rspPacket->txData[0] = AVDTP_ERR_BAD_STATE;
                rspPacket->txDataLen = 1;
            }
        } else {
            rspPacket->txData[0] = AVDTP_ERR_BAD_ACP_SEID;
            rspPacket->txDataLen = 1;
        }
        break;
    case AVDTP_SIG_SUSPEND:     /* UPF: Fail here for SMG/BI-26,27-C */
        if (AvdtpSigParseStreamSuspend(Chnl, Parms)) {
            /* All streams started */
            msgType = AVTP_MSG_TYPE_ACCEPT;
            rspPacket->txDataLen = 0;
        }
        break;
    case AVDTP_SIG_ABORT:
        if (strm) {
            if (Parms->len != 1) {
                respond = FALSE;
                break;
            }

            oldState = strm->state;
            if (strm->state == AVDTP_STRM_STATE_CONFIGURED) {
                /* Remove the stream from the in-use list */
                RemoveEntryList(&strm->node);
                AvdtpInsertStreamList(strm);
            }
            
            msgType = AVTP_MSG_TYPE_ACCEPT;
            rspPacket->txDataLen = 0;
            strm->state = AVDTP_STRM_STATE_ABORTING;
            info.channel = Chnl;
            info.event = AVDTP_EVENT_STREAM_ABORTED;
            info.error = AVDTP_ERR_NO_ERROR;
            info.stream = strm;
            info.len = 0;
            Chnl->callback(Chnl, &info);

            if (oldState == AVDTP_STRM_STATE_CONFIGURED) {
                AvdtpCloseStream(info.stream, 
                                 info.stream->abError, 
                                 0, 0, Parms->status);
            }
            break;
        }
    case AVDTP_SIG_SECURITY_CTRL:
        if (strm) {
            if ((strm->state == AVDTP_STRM_STATE_CONFIGURED) ||
                (strm->state == AVDTP_STRM_STATE_OPEN) ||
                (strm->state == AVDTP_STRM_STATE_STREAMING)) {
                respond = FALSE;
                info.channel = Chnl;
                info.stream = strm;
                info.error = AVDTP_ERR_NO_ERROR;
                info.p.data = Parms->data;
                info.len = Parms->len;
                info.event = AVDTP_EVENT_STREAM_SECURITY_IND;
                Chnl->callback(Chnl, &info);
            } else {
                /* Reject the request (BAD STATE) */
                rspPacket->txData[0] = AVDTP_ERR_BAD_STATE;
                rspPacket->txDataLen = 1;
            }
        } else {
            rspPacket->txData[0] = AVDTP_ERR_BAD_ACP_SEID;
            rspPacket->txDataLen = 1;
        }
        break;
    default:
        /* General Reject */
        msgType = AVTP_MSG_TYPE_COMMAND;
        rspPacket->txData[0] = 0;
        Parms->rxId = 0;
        break;
    }

    error_send:

    /* Send the response */
    if (respond) {
        rspPacket->msgType = msgType;
        rspPacket->msgHdrLen = 0;
        rspPacket->txIdSize = 1;
        rspPacket->txId = Parms->rxId;
        if ((msgType == AVTP_MSG_TYPE_REJECT) && (Parms->packetsLeft > 0)) {
            /* Error ignore rest of packet */
            Chnl->rxState = AVTP_SIG_MGR_RXSTATE_IGNORE;
        } else {
            Chnl->currentTxStream = strm;
            status = AVTP_SendStart(&Chnl->sigChnl, rspPacket);
            if (status != BT_STATUS_PENDING) {
                InsertTailList(&AVDTP(txPacketList), &rspPacket->node);
            }
        }
    } else {
        InsertTailList(&AVDTP(txPacketList), &rspPacket->node);
    }
}

/*---------------------------------------------------------------------------
 *            AvdtpSigHandleResponse()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle a signal response.
 *
 */
static void AvdtpSigHandleResponse(AvdtpChannel *Chnl, 
                                   AvtpCallbackParms *Parms)
{
    AvdtpCallbackParms info;

    info.channel = Chnl;
    info.stream = Chnl->currentTxStream;
    info.len = 0;
    info.status = Parms->status;
    info.error = AVDTP_ERR_NO_ERROR;

    switch (Parms->rxId) {
    case AVDTP_SIG_DISCOVER:
        if (Chnl->rxState == AVTP_SIG_MGR_RXSTATE_BEGIN) {
            Chnl->parser.rxState = 0;
            Chnl->parser.stageLen = 2;
        }
        AvdtpSigParseDiscoveries(Chnl, Parms);
        break;
    case AVDTP_SIG_GET_CAPABILITIES:
        if (Chnl->rxState == AVTP_SIG_MGR_RXSTATE_BEGIN) {
            Chnl->parser.rxState = 0;
            Chnl->parser.stageLen = 2;
        }
        AvdtpSigParseCapabilityRsp(Chnl, Parms, FALSE);
        break;
    case AVDTP_SIG_SET_CONFIG:
        if (Parms->packetsLeft == 0) {

            /* Configuration was accepted, move it to configured variables */
            info.stream->cpCfg.cpType = info.stream->cfgReq.cp.cpType;
            info.stream->cpCfg.dataLen = info.stream->cfgReq.cp.dataLen;
            OS_MemCopy((U8 *)info.stream->cpCfg.data, 
                       (U8 *)info.stream->cfgReq.cp.data, 
                       info.stream->cfgReq.cp.dataLen);
            info.stream->codecCfg.codecType = 
                info.stream->cfgReq.codec.codecType;
            info.stream->codecCfg.elemLen = info.stream->cfgReq.codec.elemLen;
            OS_MemCopy((U8 *)info.stream->codecCfg.elements, 
                       (U8 *)info.stream->cfgReq.codec.elements,
                       info.stream->cfgReq.codec.elemLen);

            /* Set the state and make the callback */
            Chnl->currentTxStream->state = AVDTP_STRM_STATE_CONFIGURED;
            RemoveEntryList(&Chnl->currentTxStream->node);
            InsertTailList(&Chnl->streamList, &Chnl->currentTxStream->node);

#ifdef AV_WORKER            
            info.event = AVDTP_EVENT_SET_CFG_CNF;
            info.p.configReq = &info.stream->cfgReq;
            info.len = sizeof(AvdtpConfigRequest);
            Chnl->callback(Chnl, &info);
#endif

            /* Open the stream */
            AssertEval(AvdtpSigStreamOpen(Chnl, Chnl->currentTxStream));
        }
        break;
    case AVDTP_SIG_GET_CONFIG:
        if (Chnl->rxState == AVTP_SIG_MGR_RXSTATE_BEGIN) {
            Chnl->parser.rxState = 0;
            Chnl->parser.stageLen = 2;
        }
        AvdtpSigParseCapabilityRsp(Chnl, Parms, TRUE);
        break;
    case AVDTP_SIG_RECONFIG:
        if (Parms->packetsLeft == 0) {

            /* Configuration was accepted, move it to configured variables */
            info.stream->cpCfg.cpType = info.stream->cfgReq.cp.cpType;
            info.stream->cpCfg.dataLen = info.stream->cfgReq.cp.dataLen;
            OS_MemCopy((U8 *)info.stream->cpCfg.data, 
                       (U8 *)info.stream->cfgReq.cp.data, 
                       info.stream->cfgReq.cp.dataLen);
            info.stream->codecCfg.codecType = 
                info.stream->cfgReq.codec.codecType;
            info.stream->codecCfg.elemLen = info.stream->cfgReq.codec.elemLen;
            OS_MemCopy((U8 *)info.stream->codecCfg.elements, 
                       (U8 *)info.stream->cfgReq.codec.elements,
                       info.stream->cfgReq.codec.elemLen);

            info.event = AVDTP_EVENT_STREAM_RECONFIG_CNF;
            info.p.configReq = &info.stream->cfgReq;
            info.len = sizeof(AvdtpConfigRequest);
            Chnl->callback(Chnl, &info);
        }
        break;
    case AVDTP_SIG_OPEN:
        /* Open the L2CAP channel for the stream */
        if (AvdtpConnect(&Chnl->currentTxStream->conn, 
                         Chnl->conn.remDev, FALSE) != BT_STATUS_PENDING) {
            /* Could not start new L2CAP channel, abort */
            Chnl->currentTxStream->abError = 0xFF;
            (void)AvdtpSigStreamAbort(Chnl, Chnl->currentTxStream);
        } else {
            Chnl->currentTxStream->state = AVDTP_STRM_STATE_OPEN;
        }
        break;
    case AVDTP_SIG_CLOSE:
        Chnl->currentTxStream->state = AVDTP_STRM_STATE_CLOSING;
        if (AvdtpDisconnect(&Chnl->currentTxStream->conn) != 
            BT_STATUS_PENDING) {
            /* Could not close the stream L2CAP channel, disconnect channel */
            (void)AvdtpDisconnect(&Chnl->conn);
        }
        break;
    case AVDTP_SIG_START:
        info.event = AVDTP_EVENT_STREAM_STARTED;
        info.stream->state = AVDTP_STRM_STATE_STREAMING;
        Chnl->callback(Chnl, &info);
        break;
    case AVDTP_SIG_SUSPEND:
        info.event = AVDTP_EVENT_STREAM_SUSPENDED;
        info.stream->state = AVDTP_STRM_STATE_OPEN;
        Chnl->callback(Chnl, &info);
        break;
    case AVDTP_SIG_ABORT:
        info.stream->state = AVDTP_STRM_STATE_ABORTING;
        info.event = AVDTP_EVENT_STREAM_ABORTED;
        Chnl->callback(Chnl, &info);

        if (AvdtpIsConnected(&Chnl->currentTxStream->conn)) {
            if (AvdtpDisconnect(&Chnl->currentTxStream->conn) != 
                BT_STATUS_PENDING) {
                /* Could not close the stream L2CAP channel, 
                 * disconnect signal channel 
                 */
                (void)AvdtpDisconnect(&Chnl->conn);
            }
        } else {
            RemoveEntryList(&info.stream->node);
            AvdtpInsertStreamList(info.stream);

            if (Chnl->currentTxStream->abError == 0xFF) {
                Chnl->currentTxStream->abError = AVDTP_ERR_NO_ERROR;
            }

            AvdtpCloseStream(info.stream, 
                             info.stream->abError, 
                             0, 0, Parms->status);
        }
        break;
    case AVDTP_SIG_SECURITY_CTRL:
        info.p.data = Parms->data;
        info.len = Parms->len;
        info.event = AVDTP_EVENT_STREAM_SECURITY_CNF;
        Chnl->callback(Chnl, &info);
        break;
    }
}

/*---------------------------------------------------------------------------
 *            AvdtpSigHandleReject()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle a signal reject.
 *
 */
void AvdtpSigHandleReject(AvdtpChannel *Chnl, 
                          AvtpCallbackParms *Parms)
{
    AvdtpCallbackParms info;
    AvdtpCapability    capability;

    info.channel = Chnl;
    info.stream = Chnl->currentTxStream;
    info.len = 0;
    info.status = Parms->status;
    info.error = Parms->data[0];

    /* If error code was not provided in response, backfill */
    if (!info.error) {
        info.error = AVDTP_ERR_NOT_SUPPORTED_COMMAND;
    }

    switch (Parms->rxId) {
    case AVDTP_SIG_DISCOVER:
        info.event = AVDTP_EVENT_DISCOVER_CNF;
        break;
    case AVDTP_SIG_GET_CAPABILITIES:
        info.event = AVDTP_EVENT_GET_CAP_CNF;
        break;
    case AVDTP_SIG_SET_CONFIG:
        capability.type = Parms->data[0];

#ifdef AV_WORKER
        info.event = AVDTP_EVENT_SET_CFG_CNF;

        info.error = Parms->data[1];
        if (!info.error) {
            info.error = AVDTP_ERR_NOT_SUPPORTED_COMMAND;
        }

        info.p.capability = &capability;
        info.len = sizeof(AvdtpCapability);
        Chnl->callback(Chnl, &info);
#endif
        if (info.stream->state < AVDTP_STRM_STATE_OPEN) {
            AvdtpCloseStream(Chnl->currentTxStream, Parms->data[1], 
                             &capability, sizeof(AvdtpCapability), 
                             Parms->status);
        }
        return;
    case AVDTP_SIG_GET_CONFIG:
        info.event = AVDTP_EVENT_GET_CFG_CNF;
        break;
    case AVDTP_SIG_RECONFIG:
        capability.type = Parms->data[0];
        info.event = AVDTP_EVENT_STREAM_RECONFIG_CNF;

        info.error = Parms->data[1];
        if (!info.error) {
            info.error = AVDTP_ERR_NOT_SUPPORTED_COMMAND;
        }

        info.p.capability = &capability;
        info.len = sizeof(AvdtpCapability);
        break;
    case AVDTP_SIG_OPEN:
        /* Abort the stream, so we don't stay in the 'configured' state */
        info.stream->abError = Parms->data[0];
        (void)AvdtpSigStreamAbort(info.stream->chnl, info.stream);
        return;
    case AVDTP_SIG_START:
        info.error = Parms->data[1];
        if (!info.error) {
            info.error = AVDTP_ERR_NOT_SUPPORTED_COMMAND;
        }

        info.event = AVDTP_EVENT_STREAM_STARTED;
        break;
    case AVDTP_SIG_CLOSE:
        info.status = BT_STATUS_CANCELLED;
        info.stream->state = AVDTP_STRM_STATE_OPEN;
        info.event = AVDTP_EVENT_STREAM_CLOSED;
        break;
    case AVDTP_SIG_SUSPEND:
        info.error = Parms->data[1];
        if (!info.error) {
            info.error = AVDTP_ERR_NOT_SUPPORTED_COMMAND;
        }

        info.event = AVDTP_EVENT_STREAM_SUSPENDED;
        break;
    case AVDTP_SIG_ABORT:
        info.event = AVDTP_EVENT_STREAM_ABORTED;
        break;
    case AVDTP_SIG_SECURITY_CTRL:
        info.event = AVDTP_EVENT_STREAM_SECURITY_CNF;
        break;
    }

    Chnl->callback(Chnl, &info);
}

/*---------------------------------------------------------------------------
 *            AvdtpSigHandleTimeout()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle a signal timeout.
 *
 */
void AvdtpSigHandleTimeout(AvdtpChannel *Chnl, AvtpCallbackParms *Parms)
{
    AvdtpCallbackParms info;
    AvdtpCapability    capability;

    info.channel = Chnl;
    info.stream = Chnl->currentTxStream;
    info.len = 0;
    info.status = BT_STATUS_TIMEOUT;
    info.error = AVDTP_ERR_UNKNOWN_ERROR;

    switch (Parms->packet->txId) {
    case AVDTP_SIG_DISCOVER:
        info.event = AVDTP_EVENT_DISCOVER_CNF;
        break;
    case AVDTP_SIG_GET_CAPABILITIES:
        info.event = AVDTP_EVENT_GET_CAP_CNF;
        break;
    case AVDTP_SIG_SET_CONFIG:
        capability.type = AVDTP_ERR_UNKNOWN_ERROR;

#ifdef AV_WORKER
        info.event = AVDTP_EVENT_SET_CFG_CNF;
        info.p.capability = &capability;
        info.len = sizeof(AvdtpCapability);
        Chnl->callback(Chnl, &info);
#endif

        if (info.stream->state < AVDTP_STRM_STATE_OPEN) {
            AvdtpCloseStream(Chnl->currentTxStream, Parms->data[1], 
                             &capability, sizeof(AvdtpCapability), 
                             Parms->status);
        }
        return;
    case AVDTP_SIG_GET_CONFIG:
        info.event = AVDTP_EVENT_GET_CFG_CNF;
        break;
    case AVDTP_SIG_RECONFIG:
        capability.type = AVDTP_ERR_UNKNOWN_ERROR;
        info.event = AVDTP_EVENT_STREAM_RECONFIG_CNF;
        info.p.capability = &capability;
        info.len = sizeof(AvdtpCapability);
        break;
    case AVDTP_SIG_OPEN:
        /* Abort the stream, so we don't stay in the 'configured' state */
        info.stream->abError = Parms->data[0];
        (void)AvdtpSigStreamAbort(info.stream->chnl, info.stream);
        info.event = AVDTP_EVENT_STREAM_OPEN;
        return;
    case AVDTP_SIG_START:
        info.event = AVDTP_EVENT_STREAM_STARTED;
        break;
    case AVDTP_SIG_CLOSE:
        info.event = AVDTP_EVENT_STREAM_CLOSED;
        break;
    case AVDTP_SIG_SUSPEND:
        info.event = AVDTP_EVENT_STREAM_SUSPENDED;
        break;
    case AVDTP_SIG_ABORT:
        info.event = AVDTP_EVENT_STREAM_ABORTED;
        break;
    case AVDTP_SIG_SECURITY_CTRL:
        info.event = AVDTP_EVENT_STREAM_SECURITY_CNF;
        break;
    }

    Chnl->callback(Chnl, &info);
}

/*---------------------------------------------------------------------------
 *            AvdtpSigMgrSignalCallback()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Signal channel callback.
 *
 */
static void AvdtpSigMgrSignalCallback(AvtpChannel *SigChnl, 
                                      AvtpCallbackParms *Parms)
{
    AvdtpChannel *chnl = ContainingRecord(SigChnl, AvdtpChannel, sigChnl);
    AvdtpCapability capability;
    AvdtpCallbackParms info;
    BtStatus status;

    info.status = Parms->status;

    switch (Parms->event) {
    case AVTP_EVENT_TX_DONE:
        Report(("AVDTP: Signal TX Done\n"));

        /* Free the packet */
        InsertTailList(&AVDTP(txPacketList), &Parms->packet->node);

        /* Handle the event */
        info.channel = chnl;
        info.stream = chnl->currentTxStream;
        info.len = 0;
        info.error = AVDTP_ERR_NO_ERROR;
        if (chnl->currentTxStream) {
            switch (Parms->packet->txId) {
            case AVDTP_SIG_START:
                if (Parms->packet->msgType == AVTP_MSG_TYPE_ACCEPT) {
                    info.event = AVDTP_EVENT_STREAM_STARTED;
                    info.stream->state = AVDTP_STRM_STATE_STREAMING;
                    chnl->callback(chnl, &info);
                } else if (Parms->packet->msgType == AVTP_MSG_TYPE_REJECT) {
                    info.event = AVDTP_EVENT_STREAM_SUSPENDED;
                    info.stream->state = AVDTP_STRM_STATE_OPEN;
                    chnl->callback(chnl, &info);
                }
                break;
            case AVDTP_SIG_SECURITY_CTRL:
                info.event = AVDTP_EVENT_STREAM_SECURITY_CNF;
                if (Parms->packet->msgType == AVTP_MSG_TYPE_ACCEPT) {
                    chnl->callback(chnl, &info);
                } else if (Parms->packet->msgType == AVTP_MSG_TYPE_REJECT) {
                    info.error = Parms->packet->txData[2];
                    chnl->callback(chnl, &info);
                }
                break;
            case AVDTP_SIG_RECONFIG:
                info.event = AVDTP_EVENT_STREAM_RECONFIG_CNF;
                if (Parms->packet->msgType == AVTP_MSG_TYPE_ACCEPT) {
                    info.p.configReq = &info.stream->cfgReq;
                    info.len = sizeof(AvdtpConfigRequest);
                    chnl->callback(chnl, &info);
                } else if (Parms->packet->msgType == AVTP_MSG_TYPE_REJECT) {
                    capability.type = Parms->packet->txData[0];
                    info.p.capability = &capability;
                    info.error = Parms->packet->txData[1];
                    info.len = sizeof(AvdtpCapability);
                    chnl->callback(chnl, &info);
                }
                break;
            case AVDTP_SIG_SET_CONFIG:
                if (Parms->packet->msgType == AVTP_MSG_TYPE_REJECT) {                    
                    RemoveEntryList(&info.stream->node);
                    AvdtpInsertStreamList(info.stream);
                    AvdtpCloseStream(info.stream, AVDTP_ERR_NO_ERROR, 
                                     0, 0, BT_STATUS_SUCCESS);
                }
                break;
            }
        }
        break;
    case AVTP_EVENT_TX_ERROR:
        Report(("AVDTP: Signal TX Error\n"));
        InsertTailList(&AVDTP(txPacketList), &Parms->packet->node);
        (void)AvdtpDisconnect(&chnl->conn);
        break;
    case AVTP_EVENT_TX_TIMEOUT:
        Report(("AVDTP: Signal TX Timeout\n"));
        AvdtpSigHandleTimeout(chnl, Parms);
        break;
    case AVTP_EVENT_RX_IND:
        if (chnl->rxState != AVTP_SIG_MGR_RXSTATE_IGNORE) {
            switch (Parms->msgType) {
            case AVTP_MSG_TYPE_COMMAND:
                Report(("AVDTP: Signal Command Received (%d)\n", Parms->rxId));
                AvdtpSigHandleCommand(chnl, Parms);
                break;
            case AVTP_MSG_TYPE_ACCEPT:
                Report(("AVDTP: Signal Response Received\n"));
                if (Parms->packetsLeft == 0) {
                    chnl->txState = AVTP_SIG_MGR_TXSTATE_IDLE;
                }
                AvdtpSigHandleResponse(chnl, Parms);
                break;
            case AVTP_MSG_TYPE_REJECT:
                Report(("AVDTP: Signal Reject Received\n"));
                if (Parms->packetsLeft == 0) {
                    chnl->txState = AVTP_SIG_MGR_TXSTATE_IDLE;
                }
                AvdtpSigHandleReject(chnl, Parms);
                break;
            }
            /* Set the receive state */
            if (Parms->packetsLeft > 0) {
                chnl->rxState = AVTP_SIG_MGR_RXSTATE_CONTINUE;
            } else {
                chnl->rxState = AVTP_SIG_MGR_RXSTATE_BEGIN;
            }
        } else if (Parms->packetsLeft == 0) {
            chnl->rxState = AVTP_SIG_MGR_RXSTATE_BEGIN;
            if (Parms->msgType == AVTP_MSG_TYPE_COMMAND) {
                chnl->currentTxStream = chnl->currentRxStream;
                status = AVTP_SendStart(&chnl->sigChnl, chnl->rspSigPacket); 
                if (status != BT_STATUS_PENDING) {
                    InsertTailList(&AVDTP(txPacketList), &chnl->rspSigPacket->node);
                }
            }
        }
        break;

    case AVTP_EVENT_RX_UNKNOWN_MESSAGE:
    case AVTP_EVENT_RX_BAD_TRANS_ID:
    case AVTP_EVENT_RX_UNKNOWN_PACKET:
    case AVTP_EVENT_RX_BUFFER_UNDERRUN:
        /* Ignore unknown messages, transaction ID's, bad packets,
         * and incomplete packets
         */
        break;

    case AVTP_EVENT_RX_BUFFER_OVERRUN:
        if (Parms->msgType == AVTP_MSG_TYPE_COMMAND) {
            Report(("AVDTP: Invalid Signal Command Length\n"));
            if (!IsListEmpty(&AVDTP(txPacketList))) {
                chnl->rspSigPacket = (AvtpPacket *)RemoveHeadList(&AVDTP(txPacketList));
                chnl->parser.rxState = 0;
                chnl->parser.curStageOff = 0;
                chnl->rxState = AVTP_SIG_MGR_RXSTATE_BEGIN;
                chnl->rspSigPacket->txData[0] = AVDTP_ERR_BAD_LENGTH;
                chnl->rspSigPacket->txDataLen = 1;
                chnl->rspSigPacket->msgType = AVTP_MSG_TYPE_REJECT;
                chnl->rspSigPacket->msgHdrLen = 0;
                chnl->rspSigPacket->txIdSize = 1;
                chnl->rspSigPacket->txId = Parms->rxId;
                chnl->currentTxStream = chnl->currentRxStream;
                status = AVTP_SendStart(SigChnl,  chnl->rspSigPacket);
                if (status != BT_STATUS_PENDING) {
                    InsertTailList(&AVDTP(txPacketList), &chnl->rspSigPacket->node);
                }
            }
            break;
        } else {
            Report(("AVDTP: Invalid Signal Response Length\n"));
            (void)AvdtpDisconnect(&chnl->conn);
        }
    }
}

/*---------------------------------------------------------------------------
 *            AvdtpSigMgrClearConnection()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Connection callback.
 *
 */
static void AvdtpSigMgrClearConnection(AvdtpChannel *Chnl, 
                                       BtRemoteDevice *RemDev,
                                       BtStatus status)
{
    AvdtpCallbackParms info;

    Chnl->parser.rxState = 0;
    Chnl->parser.curStageOff = 0;
    Chnl->rxState = AVTP_SIG_MGR_RXSTATE_BEGIN;
    Chnl->txState = AVTP_SIG_MGR_TXSTATE_IDLE;

    /* Indicate the disconnect to the application */
    info.event = AVDTP_EVENT_DISCONNECT;
    info.status = status;
    info.error = AVDTP_ERR_NO_ERROR;
    info.channel = Chnl;
    info.p.remDev = RemDev;
    info.len = sizeof(BtRemoteDevice);
    Chnl->callback(Chnl, &info);
}

/*---------------------------------------------------------------------------
 *            AvdtpSigMgrConnCallback()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Connection callback.
 *
 */
static void AvdtpSigMgrConnCallback(AvdtpConn *Conn, 
                                    AvdtpConnCallbackParms *Parms)
{
    AvdtpChannel      *chnl = ContainingRecord(Conn, AvdtpChannel, conn);
    AvdtpCallbackParms info;
    AvdtpStream       *strm, *nextStrm;

    info.status = Parms->status;

    switch (Parms->event) {
    case AVDTP_CONN_EVENT_CONNECT:
        Report(("AVDTP: Signal Channel Connected\n"));
        AVTP_InitChannel(&chnl->sigChnl, chnl->conn.l2ChannelId, 
                         AvdtpSigMgrSignalCallback, AVDTP_RTX_SIG_TIMEOUT);

        /* Indicate the connection to the application */
        info.event = AVDTP_EVENT_CONNECT;
        info.error = AVDTP_ERR_NO_ERROR;
        info.channel = chnl;
        info.len = sizeof(BtRemoteDevice);
        info.p.remDev = Parms->ptrs.remDev;
        chnl->callback(chnl, &info);
        break;
    case AVDTP_CONN_EVENT_CONNECT_IND:
        /* Indicate the incoming connection to the application */
        info.event = AVDTP_EVENT_CONNECT_IND;
        info.error = AVDTP_ERR_NO_ERROR;
        info.channel = chnl;
        info.len = sizeof(BtRemoteDevice);
        info.p.remDev = Parms->ptrs.remDev;
        chnl->callback(chnl, &info);
        break;
    case AVDTP_CONN_EVENT_DISCONNECT:
        Report(("AVDTP: Signal Channel Disconnected\n"));
        AVTP_DeinitChannel(&chnl->sigChnl);

        if (!IsListEmpty(&chnl->streamList)) {
            /* First disconnect any streams that failed config */
            strm = (AvdtpStream *)GetHeadList(&chnl->streamList);
            while (strm != (AvdtpStream *)&chnl->streamList) {
                nextStrm = (AvdtpStream *)GetNextNode(&strm->node);
                if (!AvdtpIsConnected(&strm->conn)) {
                    RemoveEntryList(&strm->node);
                    AvdtpInsertStreamList(strm);
                    AvdtpCloseStream(strm, strm->abError,
                                     0, 0, Parms->status);
                }
                strm = nextStrm;
            }

            /* Now disconnect the rest */
            if (!IsListEmpty(&chnl->streamList)) {
                strm = (AvdtpStream *)GetHeadList(&chnl->streamList);
                (void)AvdtpDisconnect(&strm->conn);
                return;
            }
        }

        AvdtpSigMgrClearConnection(chnl, Parms->ptrs.remDev, Parms->status);
        break;
    case AVDTP_CONN_EVENT_DATA_SENT:
        strm = chnl->currentTxStream;
        Report(("AVDTP: Signal Data Sent\n"));
        AVTP_SendContinue(&chnl->sigChnl);
        break;
    case AVDTP_CONN_EVENT_DATA_IND:
        Report(("AVDTP: Signal Data Received = %d\n", Parms->dataLen));
        AVTP_Receive(&chnl->sigChnl, Parms->ptrs.data, Parms->dataLen, 1);
        break;
    }
}

/*---------------------------------------------------------------------------
 *            AvdtpStrmConnCallback()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Connection callback.
 *
 */
void AvdtpStrmConnCallback(AvdtpConn *Conn, AvdtpConnCallbackParms *Parms)
{
    AvdtpStream       *strm = ContainingRecord(Conn, AvdtpStream, conn);
    AvdtpChannel      *chnl = strm->chnl;
    AvdtpCallbackParms info;

    info.status = Parms->status;

    switch (Parms->event) {
    case AVDTP_CONN_EVENT_CONNECT:
        Report(("AVDTP: Stream Connected\n"));

        /* Indicate the connection to the application */
        info.event = AVDTP_EVENT_STREAM_OPEN;
        info.error = AVDTP_ERR_NO_ERROR;
        info.channel = chnl;
        info.stream = strm;
        info.p.configReq = &strm->cfgReq;
        info.len = sizeof(AvdtpConfigRequest);
        chnl->callback(chnl, &info);
        break;
    case AVDTP_CONN_EVENT_CONNECT_IND:
        (void)AvdtpConnectRsp(Conn, TRUE);
        break;
    case AVDTP_CONN_EVENT_DISCONNECT:
        Report(("AVDTP: Stream Disconnect\n"));
        Assert(!IsListEmpty(&chnl->streamList));

        RemoveEntryList(&strm->node);
        AvdtpInsertStreamList(strm);
        
        /* Indicate closed event to the application */
        AvdtpCloseStream(strm, strm->abError, 0, 0, Parms->status);

        /* If the signal channel is disconnected disconnect any
         * remaining streams.
         */
        if (!AvdtpIsConnected(&chnl->conn)) {
            if (!IsListEmpty(&chnl->streamList)) {
                /* Disconnect the next stream */
                strm = (AvdtpStream *)GetHeadList(&chnl->streamList);
                (void)AvdtpDisconnect(&strm->conn);
            } else {
                /* The signal channel was disconnected, indicate this to the
                 * application.
                 */
                AvdtpSigMgrClearConnection(chnl, Parms->ptrs.remDev, 
                                           Parms->status);
            }
        }
        break;
    case AVDTP_CONN_EVENT_DATA_SENT:
        Report(("AVDTP: Stream Data Sent\n"));
        info.event = AVDTP_EVENT_STREAM_PACKET_SENT;
        info.error = AVDTP_ERR_NO_ERROR;
        info.channel = chnl;
        info.stream = strm;
        info.p.packet = Parms->ptrs.packet;
        info.len = 0;
        chnl->callback(chnl, &info);
        break;
    case AVDTP_CONN_EVENT_DATA_IND:
        Report(("AVDTP: Stream Data Received = %d\n", Parms->dataLen));
        info.event = AVDTP_EVENT_STREAM_DATA_IND;
        info.error = AVDTP_ERR_NO_ERROR;
        info.channel = chnl;
        info.stream = strm;
        info.p.data = Parms->ptrs.data;
        info.len = Parms->dataLen;
        chnl->callback(chnl, &info);
        break;
    }
}



