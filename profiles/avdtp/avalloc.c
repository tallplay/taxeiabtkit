/****************************************************************************
 *
 * File:
 *     $Workfile:avalloc.c$ for iAnywhere AV SDK, version 1.2.3
 *     $Revision:7$
 *
 * Description: This file contains internal memory allocation routines for
 *     the Audio/Video Distribution Transport Protocol (AVDTP).
 *             
 * Created:     Mar 11, 2004
 *
 * Copyright 2004 - 2005 Extended Systems, Inc.

 * Portions copyright 2005-2006 iAnywhere Solutions, Inc.

 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any means, 
 * or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, Inc. 
 * This work contains confidential and proprietary information of Extended 
 * Systems, Inc. which is protected by copyright, trade secret, trademark and 
 * other intellectual property rights.
 *
 ****************************************************************************/

#include "sys/avalloc.h"

#if XA_CONTEXT_PTR == XA_ENABLED
/* The stack context is accessed through a pointer. Normally in this case
 * the stack context would be dynamically allocated. For testing purposes
 * the pointer points to a global structure.
 */
BtAvdtpContext  avTmp;
BtAvdtpContext *avdtpContext = &avTmp;
#else /* XA_CONTEXT_PTR == XA_ENABLED */
BtAvdtpContext avdtpContext;
#endif /* XA_CONTEXT_PTR */

/*---------------------------------------------------------------------------
 * AvdtpAlloc()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initializes the global context for the AVDTP protocol.  This 
 *            function can be replaced if necessary.
 *
 * Return:    void
 */
BOOL AvdtpAlloc(void)
{
    U8* ptr; 

    /* Fill memory with 0. */
#if XA_CONTEXT_PTR == XA_ENABLED
    ptr = (U8*)avdtpContext;
#else
    ptr = (U8*)&avdtpContext;
#endif /* XA_CONTEXT_PTR */

    OS_MemSet(ptr, 0, (U32)sizeof(BtAvdtpContext));

    return TRUE;
}

