/****************************************************************************
 *
 * File:
 *     $Workfile:conmgr.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:58$
 *
 * Description: This file contains the connection manager.
 *             
 * Created:     June 12, 2003
 *
 * Copyright 2003-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any means, 
 * or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions,
 * Inc.  This work contains confidential and proprietary information of
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret,
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "sys/conmgri.h"

#if XA_DEBUG == XA_ENABLED
#include "sys/debug.h"
#endif

#if NUM_SCO_CONNS > 0
#include "mesco.h"
#endif

#if CMGR_MEMORY_EXTERNAL == XA_DISABLED
#if XA_CONTEXT_PTR == XA_ENABLED
BtCmgrContext *cmgrContext;
#define CMGR(s) (cmgrContext->s)
#else /* XA_CONTEXT_PTR == XA_ENABLED */
BtCmgrContext cmgrContext;
#define CMGR(s) (cmgrContext.s)
#endif /* XA_CONTEXT_PTR */
#endif /* CMGR_MEMORY_EXTERNAL == XA_DISABLED */

/*---------------------------------------------------------------------------
 *            ConnMgrIsLinkUp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Returns the state of the data link (connection manager internal)
 *
 * Return:    (See header file)
 */
static BOOL ConnMgrIsLinkUp(CmgrHandler *Handler) 
{
    BOOL status;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Handler != 0);

    OS_LockStack();

    if (IsNodeOnList(&CMGR(handlerList), &Handler->node)) {
        if ((Handler->bdc) && (Handler->bdc->link)) {
            status = (ME_GetRemDevState(Handler->bdc->link) == BDS_DISCONNECTED) ? FALSE : TRUE;
        } else {
            status = FALSE;
        }
    } else {
        status = FALSE;
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            ConnMgrGetMaxSniffTime()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Finds the maximum sniff time enabled by registered handlers
 *            and return a pointer to that handler.  Called only when setting
 *            a new timer.
 *
 * Return:    void
 */
static CmgrHandler *ConnMgrGetMaxSniffTime(BtRemoteDevice *RemDev)
{
    CmgrHandler *handler = (CmgrHandler *)GetHeadList(&CMGR(handlerList));
    CmgrHandler *maxHandler = 0;

    while (handler != (CmgrHandler *)&CMGR(handlerList)) {
        if (handler->bdc && (handler->bdc->link == RemDev)) {

            /* Cancel any running timer */
            if (handler->sniffTimer.context) {
                EVM_CancelTimer(&handler->sniffTimer);
            }

            /* Find the max timer */
            if (maxHandler) {
                if (handler->sniffTimeout > maxHandler->sniffTimeout) {
                    maxHandler = handler;
                }
            } else {
                maxHandler = handler;
            }
        }

        handler = (CmgrHandler *)GetNextNode(&handler->node);
    }

    return maxHandler;
}

/*---------------------------------------------------------------------------
 *            ConnMgrCheckStartSniffTimer()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Checks for the correct state and starts the timer.
 *
 * Return:    void
 */
static void ConnMgrCheckStartSniffTimer(BtRemoteDevice *RemDev)
{
    CmgrHandler *handler = ConnMgrGetMaxSniffTime(RemDev);

    /* Start timer if possible */
    if (handler) {
        if ((handler->sniffTimeout > 0) &&
            ConnMgrIsLinkUp(handler) && 
#if NUM_SCO_CONNS > 0
            !CMGR_IsAudioUp(handler) && 
#endif
            (ME_GetCurrentMode(handler->bdc->link) == BLM_ACTIVE_MODE)) {

            /* Start the timer */
            handler->sniffTimer.context = handler;
            EVM_StartTimer(&handler->sniffTimer, handler->sniffTimeout);
        }
    }
}

/*---------------------------------------------------------------------------
 *            ConnMgrSniffTimeoutHandler()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Puts the device in sniff mode when the specified time has
 *            expired.
 *
 * Return:    void
 */
static void ConnMgrSniffTimeoutHandler(EvmTimer *timer)
{
    CmgrHandler *handler = timer->context;

    if (ConnMgrIsLinkUp(handler) && 
#if NUM_SCO_CONNS > 0
        !CMGR_IsAudioUp(handler) && 
#endif
        (ME_GetCurrentMode(handler->bdc->link) == BLM_ACTIVE_MODE)) {

        /* OK to start sniff mode */
        if (ME_StartSniff(handler->bdc->link, &handler->sniffInfo) != 
            BT_STATUS_PENDING) {

            /* Could not start sniff mode, start the timer again */
            EVM_StartTimer(timer, handler->sniffTimeout);
        } else {
            /* Timer is off untill sniff mode is exited */
            timer->context = 0;
        }
    }
}

/*---------------------------------------------------------------------------
 *            ConnMgrGetSniffExitPolicy()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Gets composite sniff exit policy of all handlers on a given
 *            link.
 *
 * Return:    void
 */
static CmgrSniffExitPolicy ConnMgrGetSniffExitPolicy(CmgrHandler *Handler)
{
    CmgrHandler *handler = (CmgrHandler *)GetHeadList(&CMGR(handlerList));
    CmgrSniffExitPolicy sniffPolicy = CMGR_SNIFF_DONT_CARE;

    while (handler != (CmgrHandler *)&CMGR(handlerList)) {
        if (Handler->bdc && handler->bdc && 
            (Handler->bdc->link == handler->bdc->link)) {

            /* Set the sniff exit policy.  It will be based on the policy of
             * all devices on the link.
             */
            sniffPolicy |= Handler->sniffPolicy;
        }

        handler = (CmgrHandler *)GetNextNode(&handler->node);
    }

    return sniffPolicy;
}


/*---------------------------------------------------------------------------
 *            ConnMgrGetFirstHandlerByRemDev()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Gets the first handler using the specified link.
 *
 * Return:    void
 */
static CmgrHandler * ConnMgrGetFirstHandlerByRemDev(BtRemoteDevice *RemDev)
{
    CmgrHandler *handler = (CmgrHandler *)GetHeadList(&CMGR(handlerList));

    while (handler != (CmgrHandler *)&CMGR(handlerList)) {
        if (handler->bdc && (handler->bdc->link == RemDev)) {
            break;
        }

        handler = (CmgrHandler *)GetNextNode(&handler->node);
    }

    if (handler == (CmgrHandler *)&CMGR(handlerList)) {
        /* No handler found */
        handler = 0;
    }

    return handler;
}

/*---------------------------------------------------------------------------
 *            ConnMgrStayMaster()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  See if some handler needs to be Master.
 *
 * Return:    void
 */
static BOOL ConnMgrStayMaster(void)
{
    CmgrHandler *handler = (CmgrHandler *)GetHeadList(&CMGR(handlerList));

    while (handler != (CmgrHandler *)&CMGR(handlerList)) {
        if (handler->flags & CMGR_FLAG_STAY_MASTER) {
            return TRUE;
        }

        handler = (CmgrHandler *)GetNextNode(&handler->node);
    }

    return FALSE;
}

#if NUM_SCO_CONNS > 0

/*---------------------------------------------------------------------------
 *            ConnMgrScoNotify()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Notify all relevant handlers of the SCO connect.
 *
 * Return:    void
 */
static BOOL ConnMgrScoNotify(const BtEvent *btEvent, CmgrEvent Event, BtStatus Status)
{
    CmgrHandler  *handler = (CmgrHandler *)GetHeadList(&CMGR(handlerList));
    CmgrHandler  *nextHandler;
    BOOL          notified = FALSE;

    while (handler != (CmgrHandler *)&CMGR(handlerList)) {
        if (handler->bdc && 
            (handler->bdc->link == btEvent->p.scoConnect.scoCon->scord) &&
            (handler->flags & CMGR_FLAG_SCO_REGISTERED)) {

            /* Set up and call the SCO handler */
            handler->errCode = btEvent->errCode;
            handler->scoConnect = btEvent->p.scoConnect.scoCon;

            if (Event == CMEVENT_AUDIO_LINK_CON) {
                if (Status == BT_STATUS_SUCCESS) {
                    handler->flags |= CMGR_FLAG_AUDIO_UP;
                } else {
                    handler->flags &= ~CMGR_FLAG_AUDIO_UP;
                }
            } else if (Event == CMEVENT_AUDIO_LINK_DIS) {
                handler->flags &= ~CMGR_FLAG_AUDIO_UP;
#if NUM_SCO_CONNS > 0
#if BT_SCO_HCI_DATA == XA_ENABLED
            } else if (Event == CMEVENT_AUDIO_DATA_SENT) {
                handler->audioPacket = btEvent->p.scoPacketHandled.scoPacket;
            } else if (Event == CMEVENT_AUDIO_DATA) {
                handler->audioData.len = btEvent->p.scoDataInd.len;
                handler->audioData.data = btEvent->p.scoDataInd.ptr;
#endif
            } else if (Event == CMEVENT_AUDIO_LINK_REQ) {
                CMGR(scoRequest) = TRUE;
#endif
            }

            /* Get the next node before the callback in case it is
             * deregistered.
             */
            nextHandler = (CmgrHandler *)GetNextNode(&handler->node);

            if ((Event != CMEVENT_AUDIO_LINK_REQ) || 
                 (handler->flags & CMGR_FLAG_HANDLE_SCO)) {
                handler->callback(handler, Event, Status);
                notified = TRUE;
            }
        } else {
            nextHandler = (CmgrHandler *)GetNextNode(&handler->node);
        }

        handler = nextHandler;
    }

    return notified;
}

/*---------------------------------------------------------------------------
 *            ConnMgrIsAudioUp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  See if an audio link is up on the remote device.
 *
 * Return:    void
 */
static BOOL ConnMgrIsAudioUp(BtRemoteDevice *RemDev)
{
    CmgrHandler *handler = (CmgrHandler *)GetHeadList(&CMGR(handlerList));

    while (handler != (CmgrHandler *)&CMGR(handlerList)) {
        if (handler->bdc && (RemDev == handler->bdc->link)) {
            if (handler->flags & CMGR_FLAG_AUDIO_UP) {
                return TRUE;
            }
        }

        handler = (CmgrHandler *)GetNextNode(&handler->node);
    }

    return FALSE;
}

/*---------------------------------------------------------------------------
 *            ConnMgrCountScoHandlers()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Count the number of Sco handlers registered on the 
 *            specified device.
 *
 * Return:    void
 */
static U16 ConnMgrCountScoHandlers(BtRemoteDevice *RemDev)
{
    U16          count = 0;
    CmgrHandler *handler = (CmgrHandler *)GetHeadList(&CMGR(handlerList));

    while (handler != (CmgrHandler *)&CMGR(handlerList)) {
        if (handler->bdc && (handler->bdc->link == RemDev)) {
            if (handler->flags & CMGR_FLAG_SCO_REGISTERED) {
                count++;
            }
        }

        handler = (CmgrHandler *)GetNextNode(&handler->node);
    }

    return count;
}

/*---------------------------------------------------------------------------
 *            ConnMgrCreateAudioLink()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Create an eSCO or SCO connection.
 *
 * Return:    (see SCO_CreateLink and SCO_SetupLink)
 */
static BtStatus ConnMgrCreateAudioLink(BtScoConnect **scoConnect, 
                                       BtRemoteDevice *RemDev, U8 flag)
{
    BtStatus        status;
    BtScoTxParms    scoTxParms;

    if  ((flag == CMGR_AUDIO_PARMS_S1) || 
         (flag == CMGR_AUDIO_PARMS_S2) ||
         (flag == CMGR_AUDIO_PARMS_S3)) {

        scoTxParms.transmitBandwidth = 0x1f40;                /* 8000    */
        scoTxParms.receiveBandwidth = 0x1f40;                 /* 8000    */

        if ((flag == CMGR_AUDIO_PARMS_S1) || (flag == CMGR_AUDIO_PARMS_S2)) {
            scoTxParms.maxLatency = 0x0007;                   /* 7ms     */
        } else {
            scoTxParms.maxLatency = 0x000A;                   /* 10ms    */
        }

        scoTxParms.voiceSetting = CMGR(scoCustomParms).voiceSetting;
        scoTxParms.retransmissionEffort = 0x01;               /* 1 retry */

        /* Initialize supported packet types */
        scoTxParms.eScoPktType = 0;
        if (ME_GetBtFeatures(1) &0x08) scoTxParms.eScoPktType |= BESPT_HV1;
        if (ME_GetBtFeatures(1) &0x10) scoTxParms.eScoPktType |= BESPT_HV2;
        if (ME_GetBtFeatures(1) &0x20) scoTxParms.eScoPktType |= BESPT_HV3;
        if (ME_GetBtFeatures(3) &0x80) scoTxParms.eScoPktType |= BESPT_EV3;

        if (flag == CMGR_AUDIO_PARMS_S1) {
            if (ME_GetBtFeatures(5) & 0x20) {
                /* 2-EV3 supported, but can't be used */
                scoTxParms.eScoPktType |= BESPT_EV3 | 0x0040; /* EV3 */
            } 
        }

        /* Create an eSCO link */
        status = SCO_SetupLink(scoConnect, 
                               RemDev, 
                               &scoTxParms);
    } else if ((flag == CMGR_AUDIO_PARMS_CUSTOM) &&
               (ME_GetBtFeatures(3) & 0x80)) {
        /* Create a custom eSCO link */
        scoTxParms.transmitBandwidth = CMGR(scoCustomParms).transmitBandwidth;
        scoTxParms.receiveBandwidth = CMGR(scoCustomParms).receiveBandwidth;
        scoTxParms.voiceSetting = CMGR(scoCustomParms).voiceSetting;
        scoTxParms.maxLatency = CMGR(scoCustomParms).maxLatency;
        scoTxParms.retransmissionEffort = CMGR(scoCustomParms).retransmissionEffort;
        scoTxParms.eScoPktType = CMGR(scoCustomParms).eScoPktType;

        /* Create an eSCO link */
        status = SCO_SetupLink(scoConnect, 
                               RemDev, 
                               &scoTxParms);
    } else {
        /* Create a SCO link */
        status = SCO_CreateLink(scoConnect, 
                                RemDev, 
                                BSPT_HV1|BSPT_HV2|BSPT_HV3);
    }

    return status;
}


/*---------------------------------------------------------------------------
 *            ConnMgrAcceptAudioLink()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Accept eSCO or SCO connection.
 *
 * Return:    (see SCO_AcceptIncomingConnection)
 */
static BtStatus ConnMgrAcceptAudioLink(BtScoConnect *scoConnect)
{
    BtStatus        status;

    if (scoConnect->scoLinkType == BLT_ESCO) {

        if (CMGR(scoDefaultParms) != CMGR_AUDIO_PARMS_CUSTOM) {
            scoConnect->scoTxParms.transmitBandwidth = 0x1f40;  /* 8000       */
            scoConnect->scoTxParms.receiveBandwidth = 0x1f40;   /* 8000       */
            scoConnect->scoTxParms.maxLatency = 0xFFFF;         /* Don't Care */
            scoConnect->scoTxParms.retransmissionEffort = 0xFF; /* Don't Care */
            scoConnect->scoTxParms.voiceSetting = CMGR(scoCustomParms).voiceSetting;
    
            if (CMGR(scoDefaultParms) == CMGR_AUDIO_PARMS_SCO) {
                scoConnect->scoTxParms.eScoPktType = BESPT_HV1 | BESPT_HV2 | BESPT_HV3;
            } else {
    
                /* Initialize supported packet types */
                scoConnect->scoTxParms.eScoPktType = 0;
                if (ME_GetBtFeatures(1) &0x08) scoConnect->scoTxParms.eScoPktType |= BESPT_HV1;
                if (ME_GetBtFeatures(1) &0x10) scoConnect->scoTxParms.eScoPktType |= BESPT_HV2;
                if (ME_GetBtFeatures(1) &0x20) scoConnect->scoTxParms.eScoPktType |= BESPT_HV3;
                if (ME_GetBtFeatures(3) &0x80) scoConnect->scoTxParms.eScoPktType |= BESPT_EV3;
    
                if (CMGR(scoDefaultParms) == CMGR_AUDIO_PARMS_S1) {
                    if (ME_GetBtFeatures(5) & 0x20) {
                        /* 2-EV3 supported, but can't be used */
                        scoConnect->scoTxParms.eScoPktType |= BESPT_EV3 | 0x0040; /* EV3 */
                    } 
                }
            }
        } else {
            /* Use the custom parms */
            scoConnect->scoTxParms.transmitBandwidth = CMGR(scoCustomParms).transmitBandwidth;
            scoConnect->scoTxParms.receiveBandwidth = CMGR(scoCustomParms).receiveBandwidth;
            scoConnect->scoTxParms.maxLatency = CMGR(scoCustomParms).maxLatency;
            scoConnect->scoTxParms.voiceSetting = CMGR(scoCustomParms).voiceSetting;
            scoConnect->scoTxParms.eScoPktType = CMGR(scoCustomParms).eScoPktType;
            scoConnect->scoTxParms.retransmissionEffort = CMGR(scoCustomParms).retransmissionEffort;
        }
    }

    status = SCO_AcceptIncomingConnection(scoConnect, &CMGR(scoHandler));

    return status;
}

/*---------------------------------------------------------------------------
 *            ConnMgrScoRetryTimeout()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Delayed creation of the audio link.
 *
 * Return:    void
 */
static void ConnMgrScoRetryTimeout(EvmTimer *timer)
{
    BtScoConnect *scoCon = timer->context;
        
    AssertEval(ConnMgrCreateAudioLink(&scoCon, scoCon->scord, CMGR(scoParms)) == BT_STATUS_PENDING);
}
    
#endif

/*---------------------------------------------------------------------------
 *            ConnManager()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Tracks and manages the ACL link.
 *
 * Return:    void
 */
static void ConnManager(const BtEvent *Event)
{
    BtStatus      status;
    CmgrHandler  *cmgrHandler;                           
    CmgrHandler  *nextHandler;

    Report(("CMGR: Received event %s\n", pME_Event(Event->eType)));

    switch (Event->eType) {

    case BTEVENT_LINK_CONNECT_IND:

        if (Event->handler == &CMGR(globHandler)) {
            if (Event->errCode == BEC_NO_ERROR) {
                CMGR(linkCount)++;

                /* Try a Master/Slave switch after the first connection is 
                 * established.  If this is successful, any remote devices 
                 * attempting subsequent connections must be forced to change 
                 * roles.
                 */
                if (ConnMgrStayMaster()) {
                    CMGR(roleSwitch) = TRUE;
                }

                status = BT_STATUS_SUCCESS;
            } else {
                status = BT_STATUS_FAILED;
            }

            cmgrHandler = (CmgrHandler *)GetHeadList(&CMGR(handlerList));
            while (cmgrHandler != (CmgrHandler *)&CMGR(handlerList)) {

                if (!ConnMgrIsLinkUp(cmgrHandler)) {
                    /* Notify handler of the link connect */
                    cmgrHandler->remDev = Event->p.remDev;
                    cmgrHandler->errCode = Event->errCode;
                    cmgrHandler->callback(cmgrHandler, CMEVENT_DATA_LINK_CON_IND, 
                                          BT_STATUS_SUCCESS);
                }

                cmgrHandler = (CmgrHandler *)GetNextNode(&cmgrHandler->node);
            }

            if (Event->errCode != BEC_NO_ERROR) {
                /* Start sniff timer if it has been set */
                ConnMgrCheckStartSniffTimer(Event->p.remDev);

#if NUM_SCO_CONNS > 0
                /* Start with the highest possible audio settings */
                CMGR(scoParms) = CMGR(scoDefaultParms);
#endif
            }
        }
        break;
    case BTEVENT_LINK_DISCONNECT:

        cmgrHandler = ContainingRecord(Event->handler, CmgrHandler, btHandler);

        if (Event->handler == &CMGR(globHandler)) {
            Assert(CMGR(linkCount) > 0);
            CMGR(linkCount)--;
        } else {
            /* Notify the handler of the link disconnect */
            cmgrHandler->remDev = Event->p.remDev;
            cmgrHandler->errCode = Event->errCode;
            cmgrHandler->callback(cmgrHandler, CMEVENT_DATA_LINK_DIS, BT_STATUS_SUCCESS);
            if (IsNodeOnList(&CMGR(handlerList), &cmgrHandler->node)) {
                /* The handler is still registered */
                cmgrHandler->bdc = 0;
            }
        }
        break;
    case BTEVENT_ROLE_CHANGE:
        if (Event->handler == &CMGR(globHandler)) {
            if (Event->p.roleChange.newRole == BCR_MASTER) {
                /* The role has been set to MASTER.  Any devices attempting to
                 * connect must now be forced to change roles.
                 */
                Report(("CMGR: Role is Master\n"));
                if (ConnMgrStayMaster()) {
                    ME_SetConnectionRole(BCR_MASTER);
                } else {
                    ME_SetConnectionRole(BCR_ANY);
                }
            } else {
                /* The role has been set to SLAVE.  No other devices will be
                 * able to connect.
                 */
                if (CMGR(roleSwitch)) {
                    ME_SwitchRole(Event->p.remDev);
                    Report(("CMGR: Attempting M/S switch...\n"));
                } else {
                    Report(("CMGR: Role is Slave\n"));
                    ME_SetConnectionRole(BCR_ANY);
                }
            }
            CMGR(roleSwitch) = FALSE;
        }
        break;

    case BTEVENT_LINK_CONNECT_CNF:

        cmgrHandler = ContainingRecord(Event->handler, CmgrHandler, btHandler);

        if (Event->handler == &CMGR(globHandler)) {
            if (Event->errCode == BEC_NO_ERROR) {
                CMGR(linkCount)++;
            }
        } else {
            if (Event->errCode == BEC_NO_ERROR) {

                /* Connection connection was successful */
                status = BT_STATUS_SUCCESS;

                if (ConnMgrStayMaster()) {
                    ME_SetConnectionRole(BCR_MASTER);
                }
            } else {

                /* Connection connection failed, no retry */
                status = BT_STATUS_FAILED;
            }

            cmgrHandler->remDev = Event->p.remDev;
            cmgrHandler->errCode = Event->errCode;
            cmgrHandler->callback(cmgrHandler, CMEVENT_DATA_LINK_CON_CNF, status);

            if (status == BT_STATUS_SUCCESS) {
                /* Start sniff timer if it has been set */
                if (IsNodeOnList(&CMGR(handlerList), &cmgrHandler->node)) {
                    /* The handler is still registered */
                    ConnMgrCheckStartSniffTimer(cmgrHandler->bdc->link);
                }
            }
        }
        break;
        
    case BTEVENT_DEVICE_SELECTED:

        cmgrHandler = (CmgrHandler *)GetHeadList(&CMGR(handlerList));
        while (cmgrHandler != (CmgrHandler *)&CMGR(handlerList)) {

            if (cmgrHandler->flags & CMGR_FLAG_SELECTING) {
                /* Device selected, complete the connection */

                cmgrHandler->flags &= ~CMGR_FLAG_SELECTING;
                cmgrHandler->errCode = Event->errCode;

                nextHandler = (CmgrHandler *)GetNextNode(&cmgrHandler->node);

                if (Event->errCode == BEC_NO_ERROR) {
                    /*Continue on with trying to create ACL*/
                    Assert(Event->p.select != 0);
                    Assert(Event->p.select->result != 0);

                    cmgrHandler->bdc = Event->p.select->result;

                    status = ME_CreateLink(&cmgrHandler->btHandler, 
                        &cmgrHandler->bdc->addr, 
                        &cmgrHandler->bdc->psi, 
                        &cmgrHandler->bdc->link);

                    if (status == BT_STATUS_SUCCESS) {
                        cmgrHandler->callback(cmgrHandler, 
                                              CMEVENT_DATA_LINK_CON_CNF, 
                                              BT_STATUS_SUCCESS);
                    }
                    else if (status != BT_STATUS_PENDING) {
                        cmgrHandler->callback(cmgrHandler, 
                                              CMEVENT_DATA_LINK_CON_CNF, 
                                              BT_STATUS_FAILED);
                    }
                } else {
                    /*Connection attempt failed*/
                    cmgrHandler->callback(cmgrHandler, 
                                          CMEVENT_DATA_LINK_CON_CNF, 
                                          BT_STATUS_FAILED);
                }
            }
          else
          {
             nextHandler = (CmgrHandler *)GetNextNode(&cmgrHandler->node);
          }

            cmgrHandler = nextHandler;
        }
        break;

#if NUM_SCO_CONNS > 0
    case BTEVENT_SCO_CONNECT_IND:
        /* SCO is connected */
        if (Event->errCode == BEC_NO_ERROR) {
            status = BT_STATUS_SUCCESS;
        } else {
            status = BT_STATUS_FAILED;
        }

        /* Report the SCO connection */
        AssertEval(ConnMgrScoNotify(Event, CMEVENT_AUDIO_LINK_CON, status) == TRUE);
        break;

    case BTEVENT_SCO_CONNECT_REQ:
        /* Incoming SCO connection */

        if (ConnMgrIsAudioUp(Event->p.scoConnect.scoCon->scord)) {
            /* Audio is up already, reject this request */
            SCO_RejectIncomingConnection(Event->p.scoConnect.scoCon,
                                         &CMGR(scoHandler),
                                         BEC_LIMITED_RESOURCE);
            return;
        } else {
            if (!ConnMgrScoNotify(Event, CMEVENT_AUDIO_LINK_REQ, BT_STATUS_SUCCESS)) {
                /* Accept the SCO connection */
                status = ConnMgrAcceptAudioLink(Event->p.scoConnect.scoCon);
                CMGR(scoRequest) = FALSE;
            }
        }
        break;

    case BTEVENT_SCO_CONNECT_CNF:
        /* Report the SCO connection */
        if (Event->errCode == BEC_NO_ERROR) {
            status = BT_STATUS_SUCCESS;
        } else {
            if (CMGR(scoParms) == CMGR_AUDIO_PARMS_SCO) {
                status = BT_STATUS_FAILED;
            } else {
                if (CMGR(scoParms != CMGR_AUDIO_PARMS_CUSTOM)) {
                    CMGR(scoParms)--;
                } else {
                    CMGR(scoParms) = CMGR_AUDIO_PARMS_S1;
                }

                /* Try next audio settings (delayed) */
                CMGR(scoRetryTimer).context = Event->p.scoConnect.scoCon;
                EVM_StartTimer(&CMGR(scoRetryTimer), 0);
                return;
            }
        }

        AssertEval(ConnMgrScoNotify(Event, CMEVENT_AUDIO_LINK_CON, status) == TRUE);

        if (Event->errCode != BEC_NO_ERROR) {
            /* Start sniff timer if it has been set */
            ConnMgrCheckStartSniffTimer(Event->p.scoConnect.scoCon->scord);
        }
        break;

    case BTEVENT_SCO_CONN_CHNG:
        break;

    case BTEVENT_SCO_DISCONNECT:
        /* SCO connection is down */

        if (Event->errCode == BEC_NO_ERROR) {
            status = BT_STATUS_SUCCESS;
        } else {
            status = BT_STATUS_FAILED;
        }

        AssertEval(ConnMgrScoNotify(Event, CMEVENT_AUDIO_LINK_DIS, status) == TRUE);

        /* Start sniff timer if it has been set */
        ConnMgrCheckStartSniffTimer(Event->p.scoConnect.scoCon->scord);

        break;

#if BT_SCO_HCI_DATA == XA_ENABLED
    case BTEVENT_SCO_DATA_CNF:

        if (Event->errCode == BEC_NO_ERROR) {
            status = BT_STATUS_SUCCESS;
        } else {
            status = BT_STATUS_FAILED;
        }

        AssertEval(ConnMgrScoNotify(Event, CMEVENT_AUDIO_DATA_SENT, status) == TRUE);
        break;
    case BTEVENT_SCO_DATA_IND:
        AssertEval(ConnMgrScoNotify(Event, CMEVENT_AUDIO_DATA, BT_STATUS_SUCCESS) == TRUE);
        break;
#endif
#endif

    case BTEVENT_MODE_CHANGE:
        /* Sniff mode change */
        cmgrHandler = ContainingRecord(Event->handler, CmgrHandler, btHandler);
        status = BT_STATUS_SUCCESS;

        cmgrHandler->errCode = Event->errCode;
        if (Event->p.modeChange.curMode == BLM_ACTIVE_MODE) {
            
            /* Indicate that sniff mode was exited */
            cmgrHandler->callback(cmgrHandler, CMEVENT_EXIT_SNIFF_PARK_MODE, status);
            if (!IsNodeOnList(&CMGR(handlerList), &cmgrHandler->node)) {
                /* The handler is no longer registered */
                return;
            }

            if (cmgrHandler->errCode == BEC_NO_ERROR) {
                /* Start sniff timer if it has been set */
                ConnMgrCheckStartSniffTimer(cmgrHandler->bdc->link);
            }

#if NUM_SCO_CONNS > 0
            if (cmgrHandler->flags & CMGR_FLAG_STARTING_SCO) {
            
                cmgrHandler->flags &= ~CMGR_FLAG_STARTING_SCO;

                /* Create the audio link */
                status = ConnMgrCreateAudioLink(&cmgrHandler->scoConnect,
                                                 cmgrHandler->bdc->link,
                                                 CMGR(scoParms));

                if (status != BT_STATUS_PENDING) {
                    /* Can't start SCO link, report failure */
                    cmgrHandler->callback(cmgrHandler, CMEVENT_AUDIO_LINK_CON, status);
                } else {
                    return;
                }
            }
#endif
        } else {
            /* Indicate that sniff mode was entered */
            cmgrHandler->callback(cmgrHandler, CMEVENT_ENTER_SNIFF_MODE, status);
        }
        break;

    case BTEVENT_ACL_DATA_NOT_ACTIVE:

        /* Trying to send data while in Park Mode */
        if (ME_GetCurrentMode(Event->p.remDev) == BLM_PARK_MODE) {
            /* Stop Park Mode */
            (void)ME_StopPark(Event->p.remDev);
        } else if (ME_GetCurrentMode(Event->p.remDev) == BLM_SNIFF_MODE) {

            cmgrHandler = ConnMgrGetFirstHandlerByRemDev(Event->p.remDev);
            /* Search for a connection that requires exiting sniff mode */
            if (cmgrHandler &&
                (ConnMgrGetSniffExitPolicy(cmgrHandler) & CMGR_SNIFF_EXIT_ON_SEND)) {

                /* Stop Sniff Mode */
                (void)ME_StopSniff(Event->p.remDev);
            }
        }
        break;
    case BTEVENT_ENCRYPT_COMPLETE:
        break;
    case BTEVENT_ENCRYPTION_CHANGE:
        cmgrHandler = ContainingRecord(Event->handler, CmgrHandler, btHandler);
        //cmgrHandler->remDev = Event->p.encrypt.remDev;, alread assigned in connect ind
        cmgrHandler->flags = Event->p.encrypt.mode;
        cmgrHandler->callback(cmgrHandler, CMEVENT_ENCRYPTION_CHANGE, BT_STATUS_SUCCESS);
        break;

    }
}

/*---------------------------------------------------------------------------
 *            CMGR_Init()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initialize the connection manager
 *
 * Return:    (See header file)
 */
BOOL CMGR_Init(void)
{
    /* Register the Connection Manager as global handler */
    ME_InitHandler(&CMGR(globHandler));
    CMGR(globHandler).callback = ConnManager;
    CMGR(linkCount) = 0;
    ME_RegisterGlobalHandler(&CMGR(globHandler));
    ME_SetEventMask(&CMGR(globHandler), BEM_ROLE_CHANGE |
                                        BEM_LINK_CONNECT_IND |
                                        BEM_LINK_CONNECT_CNF |
                                        BEM_LINK_DISCONNECT);
    ME_SetConnectionRole(BCR_ANY);

    InitializeListHead(&CMGR(handlerList));
    CMGR(roleSwitch) = FALSE;
#if NUM_SCO_CONNS > 0
    CMGR(scoRequest) = FALSE;
    CMGR(scoDefaultParms) = CMGR_AUDIO_DEFAULT_PARMS;
    CMGR(scoParms) = CMGR(scoDefaultParms);
    CMGR(scoCustomParms).voiceSetting = BSAS_DEFAULT;
    CMGR(scoCustomParms).maxLatency = 0x0007;
    CMGR(scoCustomParms).retransmissionEffort = 0x01;
    CMGR(scoCustomParms).eScoPktType = BESPT_EV3;
    CMGR(scoCustomParms).transmitBandwidth = 0x1f40;
    CMGR(scoCustomParms).receiveBandwidth = 0x1f40;
    CMGR(scoRetryTimer).func = ConnMgrScoRetryTimeout;
#endif
    return TRUE;
}

/*---------------------------------------------------------------------------
 *            CMGR_RegisterHandler()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Registers and initalizes the connection manager handler
 *
 * Return:    (See header file)
 */
BtStatus CMGR_RegisterHandler(CmgrHandler *Handler, CmgrCallback Callback)
{
    BtStatus status = BT_STATUS_SUCCESS;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Handler != 0);
    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Callback != 0);

    OS_LockStack();

    if (Handler && Callback) {
        if (!IsNodeOnList(&CMGR(handlerList), &Handler->node)) {
            /* Initialize the handler */
            Handler->bdc = 0;
            Handler->callback = Callback;

            /* Initialize sniff timer */
            Handler->sniffPolicy = CMGR_DEFAULT_SNIFF_EXIT_POLICY;
            Handler->sniffTimeout = 0;
            OS_MemSet((U8*)&Handler->sniffInfo, 0, sizeof(BtSniffInfo));
            Handler->sniffTimer.func = ConnMgrSniffTimeoutHandler;
            Handler->sniffTimer.context = 0;

            /* Put it on the list of registered handlers */
            InsertTailList(&CMGR(handlerList), &Handler->node);
        } else {
            status = BT_STATUS_IN_USE;
        }
    } else {
        status = BT_STATUS_INVALID_PARM;
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            CMGR_DeregisterHandler()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Deregisters a connection manager handler
 *
 * Return:    (See header file)
 */
BtStatus CMGR_DeregisterHandler(CmgrHandler *Handler)
{
    BtStatus status = BT_STATUS_SUCCESS;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Handler != 0);

    OS_LockStack();

    if (Handler) {
        if (IsNodeOnList(&CMGR(handlerList), &Handler->node)) {
            /* Remove the handler */
            (void)CMGR_ClearSniffTimer(Handler);
            RemoveEntryList(&Handler->node);
        } else {
            status = BT_STATUS_NOT_FOUND;
        }
    } else {
        status = BT_STATUS_INVALID_PARM;
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            CMGR_CreateDataLink()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Create the ACL link
 *
 * Return:    (See header file)
 */
BtStatus CMGR_CreateDataLink(CmgrHandler *Handler, BD_ADDR *Addr)
{
    BtStatus        status;
    BtDeviceContext tmplt;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Handler != 0);

    OS_LockStack();

    if (IsNodeOnList(&CMGR(handlerList), &Handler->node)) {
        if (!ConnMgrIsLinkUp(Handler)) {
            Handler->bdc = 0;
            Handler->flags = 0;
            Handler->errCode = 0;

            /* Connect to the specified device */
            ME_InitHandler(&Handler->btHandler);
            Handler->btHandler.callback = ConnManager;

            /* Find a device (either discovered or in DDB */
            Handler->bdc = DS_FindDevice(Addr);
            if (!Handler->bdc) {

                if (Addr != 0) {
                    OS_MemSet((U8 *)&tmplt, 0, (U32)sizeof(BtDeviceContext));
                    OS_MemCopy((U8 *)&tmplt.addr, (U8 *)Addr, sizeof(BD_ADDR));
                    status = DS_AddDevice(&tmplt, &Handler->bdc);
                } else {
                    /* Attempt to get a valid device from DS */
                    
                    /* The rest of devQuery must be filled in by caller */
                    Handler->devQuery.callback = ConnManager;
                    status = DS_SelectDevice(&Handler->devQuery);
                    
                    if (status == BT_STATUS_SUCCESS) {
                        Handler->bdc = Handler->devQuery.result;
                    } else if (status == BT_STATUS_PENDING) {
                        Handler->flags |= CMGR_FLAG_SELECTING;
                    }
                }

                if (status != BT_STATUS_SUCCESS) {                 
                    goto exit;
                }
            }

            /* Create the ACL */
            status = ME_CreateLink(&Handler->btHandler, &Handler->bdc->addr, 
                                   0,  &Handler->bdc->link);
        } else {
            status = BT_STATUS_SUCCESS;
        }
    } else {
        status = BT_STATUS_NOT_FOUND;
    }

exit:

    OS_UnlockStack();

    return status;
}

BtStatus CMGR_SetLinkEncryption(CmgrHandler *Handler, BtRemoteDevice *remDev,
                               BtEncryptMode mode)
{
    BtStatus        status;
    BtDeviceContext tmplt;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Handler != 0);

    OS_LockStack();

    if (IsNodeOnList(&CMGR(handlerList), &Handler->node)) {
        if (ConnMgrIsLinkUp(Handler)) {
            ////Handler->bdc = 0;
            Handler->flags = 0;
            Handler->errCode = 0;

            /* Connect to the specified device */
            //we have already set that in CMR_CreateDataLink
            ////ME_InitHandler(&Handler->btHandler);
            ////Handler->btHandler.callback = ConnManager;

            status = SEC_SetLinkEncryption(&Handler->btHandler, Handler->bdc->link, mode);
        } else {
            status = BT_STATUS_NO_CONNECTION;
        }
    } else {
        status = BT_STATUS_NOT_FOUND;
    }

exit:

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            CMGR_RemoveDataLink()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Brings the ACL down
 *
 * Return:    (See header file)
 */
BtStatus CMGR_RemoveDataLink(CmgrHandler *Handler)
{
    BtStatus status = BT_STATUS_SUCCESS;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Handler != 0);

    OS_LockStack();

    if (IsNodeOnList(&CMGR(handlerList), &Handler->node)) {

#if NUM_SCO_CONNS > 0
            if ((status == BT_STATUS_SUCCESS) || (status == BT_STATUS_PENDING)) {
                (void)CMGR_DeregisterScoHandler(Handler);
            }
#endif

        if (ConnMgrIsLinkUp(Handler)) {
            status = ME_DisconnectLink(&Handler->btHandler, Handler->bdc->link);
            if (status != BT_STATUS_SUCCESS) {
                status = ME_ForceDisconnectLinkWithReason(0, Handler->bdc->link, 
                                                          BEC_USER_TERMINATED, 
                                                          TRUE);
            }
        }
    } else {
        status = BT_STATUS_NOT_FOUND;
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            CMGR_IsLinkUp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Returns the state of the data link
 *
 * Return:    (See header file)
 */
BOOL CMGR_IsLinkUp(CmgrHandler *Handler) 
{
    BOOL status;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Handler != 0);

    OS_LockStack();

    if (IsNodeOnList(&CMGR(handlerList), &Handler->node)) {
        if ((Handler->bdc) && (Handler->bdc->link)) {
            status = (ME_GetRemDevState(Handler->bdc->link) == BDS_DISCONNECTED) ? FALSE : TRUE;
        } else if (Handler->remDev) {
            status = (ME_GetRemDevState(Handler->remDev) == BDS_DISCONNECTED) ? FALSE : TRUE;
        } else {
            status = FALSE;
        }
    } else {
        status = FALSE;
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            CMGR_GetNumLinks()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Returns the number of active ACL links.
 *
 * Return:    (See header file)
 */
U8 CMGR_GetNumLinks(void)
{
    U8 numLinks;

    OS_LockStack();

    numLinks = CMGR(linkCount);

    OS_UnlockStack();

    return numLinks;
}


#if NUM_SCO_CONNS > 0
/*---------------------------------------------------------------------------
 *            CmgrRegisterAudioHandler()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Register a SCO handler.
 *
 * Return:    (See header file)
 */
BtStatus CmgrRegisterAudioHandler(CmgrHandler *Handler, BOOL flag)
{
    BtStatus status = BT_STATUS_SUCCESS;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Handler != 0);

    OS_LockStack();

    /* Register a SCO handler */
    if (IsNodeOnList(&CMGR(handlerList), &Handler->node)) {

        if (ConnMgrCountScoHandlers(Handler->bdc->link) == 0) {
            /* First registration */
            CMGR_SetAudioDefaultParms(CMGR(scoDefaultParms));
            ME_InitHandler(&CMGR(scoHandler));
            CMGR(scoHandler).callback = ConnManager;
            status = SCO_RegisterHandler(Handler->bdc->link, &CMGR(scoHandler));
        }

        if (status == BT_STATUS_SUCCESS) {
            Handler->flags |= CMGR_FLAG_SCO_REGISTERED;

            if (ConnMgrIsLinkUp(Handler) &&
                (ConnMgrIsAudioUp(Handler->bdc->link))) {
                Handler->flags |= CMGR_FLAG_AUDIO_UP;
            }

            if (flag) {
                Handler->flags |= CMGR_FLAG_HANDLE_SCO;
            } else {
                Handler->flags &= ~CMGR_FLAG_HANDLE_SCO;
            }
        }
    } else {
        status = BT_STATUS_NOT_FOUND;
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            CmgrDeregisterAudioHandler()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Deregister a SCO handler.
 *
 * Return:    (See header file)
 */
BtStatus CmgrDeregisterAudioHandler(CmgrHandler *Handler)
{
    BtStatus status = BT_STATUS_SUCCESS;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Handler != 0);

    OS_LockStack();

    if (IsNodeOnList(&CMGR(handlerList), &Handler->node)) {
        /* Unregister the SCO handler */
        if (Handler->flags & CMGR_FLAG_SCO_REGISTERED) {
            if (ConnMgrCountScoHandlers(Handler->bdc->link) == 1) {
                status = SCO_UnregisterHandler(Handler->bdc->link, &CMGR(scoHandler));
            }

            if (status == BT_STATUS_SUCCESS) {
                Handler->flags &= ~(CMGR_FLAG_SCO_REGISTERED | CMGR_FLAG_HANDLE_SCO);
            }
        }
    } else {
        status = BT_STATUS_NOT_FOUND;
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            CMGR_CreateAudioLink()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Create the SCO connection
 *
 * Return:    (See header file)
 */
BtStatus CMGR_CreateAudioLink(CmgrHandler *Handler)
{
    BtStatus status = BT_STATUS_NO_CONNECTION;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Handler != 0);

    OS_LockStack();

    if (IsNodeOnList(&CMGR(handlerList), &Handler->node)) {
        Handler->errCode = 0;
        if (ConnMgrIsLinkUp(Handler)) {
            if (!(Handler->flags & CMGR_FLAG_AUDIO_UP)) {

                if (ME_GetCurrentMode(Handler->bdc->link) == BLM_ACTIVE_MODE) {

                    /* Create the audio link */
                    status = ConnMgrCreateAudioLink(&Handler->scoConnect,
                                                    Handler->bdc->link,
                                                    CMGR(scoParms));
                } else {
                    if (ME_GetCurrentMode(Handler->bdc->link) == BLM_SNIFF_MODE) {
                        if (ConnMgrGetSniffExitPolicy(Handler) & 
                            CMGR_SNIFF_EXIT_ON_AUDIO) {

                            /* Stop sniff mode fist */
                            Handler->flags |= CMGR_FLAG_STARTING_SCO;
                            status = ME_StopSniff(Handler->bdc->link);

                        } else {

                            /* Create the audio link */
                            status = ConnMgrCreateAudioLink(&Handler->scoConnect,
                                                            Handler->bdc->link,
                                                            CMGR(scoParms));
                        }
                    } else if (ME_GetCurrentMode(Handler->bdc->link) == 
                               BLM_PARK_MODE) {

                        /* Stop park mode fist */
                        Handler->flags |= CMGR_FLAG_STARTING_SCO;
                        status = ME_StopPark(Handler->bdc->link);
                    }
                }
            } else {
                status = BT_STATUS_SUCCESS;
            }
        }
    } else {
        status = BT_STATUS_NOT_FOUND;
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            CMGR_AcceptAudioLink()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Accepts or rejects an audio link.
 *
 * Return:    (See header file)
 */
BtStatus CMGR_AcceptAudioLink(CmgrHandler *Handler, BtErrorCode Error)
{
    BtStatus status;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Handler != 0);

    OS_LockStack();

    if (IsNodeOnList(&CMGR(handlerList), &Handler->node)) {
        if (CMGR(scoRequest)) {
            if (Error == BEC_NO_ERROR) {
                status = ConnMgrAcceptAudioLink(Handler->scoConnect);
            } else {
                status = SCO_RejectIncomingConnection(Handler->scoConnect, 
                                                      &CMGR(scoHandler),
                                                      Error);
            }
            CMGR(scoRequest) = FALSE;
        } else {
            status = BT_STATUS_PENDING;
        }
    } else {
        status = BT_STATUS_NOT_FOUND;
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            CMGR_RemoveAudioLink()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Bring down the SCO connection
 *
 * Return:    (See header file)
 */
BtStatus CMGR_RemoveAudioLink(CmgrHandler *Handler)
{
    BtStatus status = BT_STATUS_NO_CONNECTION;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Handler != 0);

    OS_LockStack();

    if (IsNodeOnList(&CMGR(handlerList), &Handler->node)) {
        if (Handler->flags & CMGR_FLAG_AUDIO_UP) {
            status = SCO_DisconnectLink(Handler->scoConnect);
        }
    } else {
        status = BT_STATUS_NOT_FOUND;
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            CMGR_IsAudioUp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Returns the state of the audio link
 *
 * Return:    (See header file)
 */
BOOL CMGR_IsAudioUp(CmgrHandler *Handler) 
{
    BOOL status; 

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Handler != 0);

    OS_LockStack();

    if (IsNodeOnList(&CMGR(handlerList), &Handler->node)) {
        status = (Handler->flags & CMGR_FLAG_AUDIO_UP) ? TRUE : FALSE;
    } else {
        status = FALSE;
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            CMGR_SetAudioDefaultParms()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Sets the default parameters for SCO or eSCO connections.
 *
 * Return:    (See header file)
 */
void CMGR_SetAudioDefaultParms(CmgrAudioParms Parms)
{
    OS_LockStack();

    CMGR(scoDefaultParms) = Parms;

    if (CMGR(scoDefaultParms != CMGR_AUDIO_PARMS_CUSTOM)) {
        if (ME_GetBtFeatures(3) & 0x80) {
            /* eSCO supported */
            if (CMGR(scoDefaultParms) > CMGR_AUDIO_PARMS_SCO) {
                if (ME_GetBtVersion() == 2) {
                    /* 1.2 Radio - SCO and EV3 is supported */
                    CMGR(scoDefaultParms) = CMGR_AUDIO_PARMS_S1;
                } else {
                    /* 2.0 or greater Radio */
                    if ((CMGR(scoDefaultParms) > CMGR_AUDIO_PARMS_S1) &&
                        !(ME_GetBtFeatures(5) & 0x20)) {
                        /* 2-EV3 not supported */
                        CMGR(scoDefaultParms) = CMGR_AUDIO_PARMS_S1;
                    }
                }
            }
        } else {
            /* Only SCO settings are supported */
            CMGR(scoDefaultParms) = CMGR_AUDIO_PARMS_SCO;
        }
    }

    /* Start with the highest possible audio settings */
    CMGR(scoParms) = CMGR(scoDefaultParms);

    OS_UnlockStack();
}

/*---------------------------------------------------------------------------
 *            CMGR_GetAudioDefaultParms()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Gets the default parameters for SCO or eSCO connections.
 *
 * Return:    (See header file)
 */
CmgrAudioParms CMGR_GetAudioDefaultParms(void)
{
    CmgrAudioParms parms;

    OS_LockStack();

    CMGR_SetAudioDefaultParms(CMGR(scoDefaultParms));
    parms = CMGR(scoDefaultParms);

    OS_UnlockStack();

    return parms;
}

/*---------------------------------------------------------------------------
 *            CMGR_SetAudioVoiceSettings()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Sets the default parameters for SCO or eSCO connections.
 *
 * Return:    (See header file)
 */
void CMGR_SetAudioVoiceSettings(BtScoAudioSettings settings)
{
    OS_LockStack();

    CMGR(scoCustomParms).voiceSetting = settings;

    OS_UnlockStack();
}

/*---------------------------------------------------------------------------
 *            CMGR_GetAudioVoiceSettings()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Returns current Audio Voice Settings.
 *
 * Return:    (See header file)
 */
BtScoAudioSettings CMGR_GetAudioVoiceSettings(void) 
{
    return (CMGR(scoCustomParms).voiceSetting);
}

/*---------------------------------------------------------------------------
 *            CMGR_SetAudioCustomParms()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Sets the custom audio paraemters.
 *
 * Return:    (See header file)
 */
BtStatus CMGR_SetAudioCustomParms(BtScoTxParms *customParms)
{
    CheckUnlockedParm(BT_STATUS_INVALID_PARM, customParms != 0);

    OS_LockStack();

    OS_MemCopy((U8*)&CMGR(scoCustomParms), (U8*)customParms, sizeof(BtScoTxParms));

    OS_UnlockStack();

    return BT_STATUS_SUCCESS;
}

/*---------------------------------------------------------------------------
 *            CMGR_GetAudioCustomParms()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Copies the current custom parms into the specified memory.
 *
 * Return:    (See header file)
 */
BtStatus CMGR_GetAudioCustomParms(BtScoTxParms *customParms) 
{
    CheckUnlockedParm(BT_STATUS_INVALID_PARM, customParms != 0);

    OS_LockStack();

    OS_MemCopy((U8*)customParms, (U8*)&CMGR(scoCustomParms), sizeof(BtScoTxParms));

    OS_UnlockStack();

    return BT_STATUS_SUCCESS;
}
#endif

/*---------------------------------------------------------------------------
 *            CMGR_SetSniffTimer()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Sets the timeout value used to put the link into sniff mode. 
 *            This timer starts after a link connection is complete.  It is 
 *            disabled when a SCO (or eSCO) connection is established and the 
 *            sniff mode is exited.  When the SCO (or eSCO) connection is 
 *            terminated the timer is started again.  
 *
 *            The sniff timeout can be set to any value, however, it is
 *            important to note that a value of 0 means that the handler
 *            doesn't care if sniff mode is used and a value of -1 means
 *            that sniff mode is disabled for the specified handler (which 
 *            also disables sniff mode for all handlers).
 *
 * Return:    (See header file)
 */
BtStatus CMGR_SetSniffTimer(CmgrHandler *Handler, 
                            BtSniffInfo* SniffInfo, 
                            TimeT Time)
{
    BtStatus status = BT_STATUS_SUCCESS;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Handler != 0);
    CheckUnlockedParm(BT_STATUS_INVALID_PARM, SniffInfo != 0);

    OS_LockStack();

    if (IsNodeOnList(&CMGR(handlerList), &Handler->node)) {
        /* Save sniff timer info */
        Handler->sniffTimeout = Time;
        OS_MemCopy((U8*)&Handler->sniffInfo, (U8*)SniffInfo, sizeof(BtSniffInfo));
        if (Handler->bdc) {
            ConnMgrCheckStartSniffTimer(Handler->bdc->link);
        }               } else {
        status = BT_STATUS_NOT_FOUND;
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            CMGR_ClearSniffTimer()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Clears the sniff mode timeout value.  This is equivalent to
 *            saying that the handler no longer cares if the link is put
 *            into sniff mode or not.
 *
 * Return:    (See header file)
 */
BtStatus CMGR_ClearSniffTimer(CmgrHandler *Handler)
{
    BtStatus status = BT_STATUS_SUCCESS;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Handler != 0);

    OS_LockStack();

    if (IsNodeOnList(&CMGR(handlerList), &Handler->node)) {
        Handler->sniffTimeout = 0;
        if (Handler->sniffTimer.context) {
            EVM_CancelTimer(&Handler->sniffTimer);
        }
    } else {
        status = BT_STATUS_NOT_FOUND;
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            CMGR_IndicateLinkActivity()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Indicates that activity has occured on the link, which restarts
 *            the sniff timeout.
 *
 * Return:    (See header file)
 */
void CMGR_IndicateLinkActivity(CmgrHandler *Handler)
{
    OS_LockStack();

    if (Handler) {
        if (IsNodeOnList(&CMGR(handlerList), &Handler->node)) {
            if (Handler->sniffTimer.context) {
                EVM_CancelTimer(&Handler->sniffTimer);
                ConnMgrCheckStartSniffTimer(Handler->bdc->link);
            }
        }
    }

    OS_UnlockStack();
}

/*---------------------------------------------------------------------------
 *            CMGR_DisableSniffTimer()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Disables sniff mode for the specified handler.  This also 
 *            disables sniff mode for all handlers.
 *
 * Return:    (See header file)
 */
BtStatus CMGR_DisableSniffTimer(CmgrHandler *Handler)
{
    BtStatus status = BT_STATUS_SUCCESS;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Handler != 0);

    OS_LockStack();

    if (IsNodeOnList(&CMGR(handlerList), &Handler->node)) {
        /* Disable sniff timer */
        Handler->sniffTimeout = CMGR_SNIFF_DISABLED;
        ConnMgrCheckStartSniffTimer(Handler->bdc->link);

        /* Stop sniff mode on this link */
        (void)ME_StopSniff(Handler->bdc->link);
    } else {
        status = BT_STATUS_NOT_FOUND;
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            CMGR_SetSniffExitPolicy()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Sets the policy for exiting sniff mode. 
 *
 * Return:    (See header file)
 */
BtStatus CMGR_SetSniffExitPolicy(CmgrHandler *Handler, CmgrSniffExitPolicy Policy)
{
    BtStatus status = BT_STATUS_NOT_FOUND;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Handler != 0);

    OS_LockStack();

    if (IsNodeOnList(&CMGR(handlerList), &Handler->node)) {
        Handler->sniffPolicy = Policy;
        status = BT_STATUS_SUCCESS;
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            CMGR_GetSniffExitPolicy()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Gets the policy for exiting sniff mode. 
 *
 * Return:    (See header file)
 */
CmgrSniffExitPolicy CMGR_GetSniffExitPolicy(CmgrHandler *Handler)
{
    CmgrSniffExitPolicy policy = 0;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Handler != 0);

    OS_LockStack();

    if (IsNodeOnList(&CMGR(handlerList), &Handler->node)) {
        policy = Handler->sniffPolicy ;
    }

    OS_UnlockStack();

    return policy;
}

/*---------------------------------------------------------------------------
 *            CMGR_SetMasterRole()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Attempts to keep the local device in the Master role
 *
 * Return:    (See header file)
 */
BtStatus CMGR_SetMasterRole(CmgrHandler *Handler, BOOL flag)
{
    BtStatus status = BT_STATUS_NOT_FOUND;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Handler != 0);

    OS_LockStack();

    if (IsNodeOnList(&CMGR(handlerList), &Handler->node)) {
        if (flag) {
            Handler->flags |= CMGR_FLAG_STAY_MASTER;
            ME_SetConnectionRole(BCR_MASTER);
        } else {
            Handler->flags &= ~CMGR_FLAG_STAY_MASTER;
            ME_SetConnectionRole(BCR_ANY);
        }

        status = BT_STATUS_SUCCESS;
    }

    OS_UnlockStack();

    return status;
}


