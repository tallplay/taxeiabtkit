/****************************************************************************
 *
 * File:
 *     $Workfile:avtp.c$ for iAnywhere AV SDK, version 1.2.3
 *     $Revision:23$
 *
 * Description: This file contains code for encoding AVDTP and AVCTP packets.
 *             
 * Created:     Mar 22, 2004
 *
 * Copyright 2004 - 2005 Extended Systems, Inc.

 * Portions copyright 2005-2006 iAnywhere Solutions, Inc.

 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any means, 
 * or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "sys/avtp.h"

/* Function Prototypes */
static BtStatus AvtpSendPacket(AvtpChannel *avtpChnl);
static void AvtpTxTimeout(EvmTimer *Timer);

/*---------------------------------------------------------------------------
 *            AVTP_InitChannel()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initialize the Channel.
 *
 */
void AVTP_InitChannel(AvtpChannel *chnl, 
                       L2capChannelId L2ChannelId, 
                       AvtpCallback Callback,
                       TimeT Timeout)
{
    OS_MemSet((U8 *)chnl, 0, sizeof(AvtpChannel));
    InitializeListHead(&chnl->avPacketList);
    chnl->l2ChannelId = L2ChannelId;
    chnl->callback = Callback;
    chnl->txTimeout = Timeout;
}

/*---------------------------------------------------------------------------
 *            AVTP_DeinitChannel()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Deinitialize the Channel.
 *
 */
void AVTP_DeinitChannel(AvtpChannel *chnl)
{
    if (chnl->txTimer.context) {
        chnl->txTimer.context = 0;
        EVM_CancelTimer(&chnl->txTimer);
    }
}

/*---------------------------------------------------------------------------
 *            AVTP_SendStart()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Start sending a packet.
 *            Determines how many L2CAP packets are required.
 *
 * Output:
 *      chnl->txPacketsLeft is set to number of L2CAP packets required
 *      to send the AV packet.
 *
 * Returns:
 *      BT_STATUS_PENDING - packet passed successfully to L2CAP.
 */
BtStatus AVTP_SendStart(AvtpChannel *chnl, AvtpPacket *avtpPacket)
{
    BtStatus status = BT_STATUS_IN_USE;
    U8       hdrDiff;
    U16      remainder;

    if (chnl->txState == AVTP_TRANS_STATE_FIRST) {
        /* Send a packet */
        Report(("AVTP: Sending %d bytes\n", avtpPacket->txDataLen));
        chnl->curAvPacket = avtpPacket;
        chnl->offset = 0;

        if (chnl->curAvPacket->msgHdrLen > 0) {
            hdrDiff = chnl->curAvPacket->msgHdrLen + 3;
        } else {
            hdrDiff = 2;
        }

        /* See if this is a single packet */
        if (chnl->curAvPacket->txDataLen <= L2CAP_MTU - hdrDiff) {
            chnl->txPacketsLeft = 1;
            chnl->packetSize = chnl->curAvPacket->txDataLen;
        } else {
            /* Calculate number of packets to send */
            chnl->txPacketsLeft = (chnl->curAvPacket->txDataLen + hdrDiff) / 
                                  (L2CAP_MTU - 1) + 1;

            /* Adjust number of packets if last one only contains a header */
            remainder = (chnl->curAvPacket->txDataLen + hdrDiff) %
                        (L2CAP_MTU - 1);
            if (remainder == 0) {
                chnl->txPacketsLeft--;
            }

            /* Set the reference packet size */
            chnl->packetSize = L2CAP_MTU - 1;
        }

        /* Start sending the packet */
        status = AvtpSendPacket(chnl);
        if (status == BT_STATUS_PENDING) {
            Report(("AVTP:  Sending packet\n"));
            chnl->txState = AVTP_TRANS_STATE_CONTINUE;
            if (avtpPacket->msgType == AVTP_MSG_TYPE_COMMAND) {
                /* Start Timer */
                chnl->txTimer.func = AvtpTxTimeout;
                chnl->txTimer.context = chnl;
                chnl->resetTimer = FALSE;
                EVM_StartTimer(&chnl->txTimer, chnl->txTimeout);
            }
        }
    } else {
        if (!IsNodeOnList(&chnl->avPacketList, &avtpPacket->node)) {
            InsertTailList(&chnl->avPacketList, &avtpPacket->node);
            status = BT_STATUS_PENDING;
        }
    }

    return status;
}

/*---------------------------------------------------------------------------
 *            AVTP_SendContinue()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Continue sending a packet.
 *
 */
void AVTP_SendContinue(AvtpChannel *chnl)
{
    BtStatus            status;
    AvtpCallbackParms   parms;
    AvtpPacket         *avtpPacket;

    parms.status = BT_STATUS_SUCCESS;

    if (chnl->txState == AVTP_TRANS_STATE_CONTINUE) {
        if (chnl->txPacketsLeft == 0) {
            /* Set up the Event */
            chnl->txState = AVTP_TRANS_STATE_FIRST;
            parms.event = AVTP_EVENT_TX_DONE;
            parms.packet = chnl->curAvPacket;

            /* Acknowledge the last packet */
            chnl->callback(chnl, &parms);

            /* Check for another packet */
            while (!IsListEmpty(&chnl->avPacketList)) {
                /* There is a new packet to send */
                avtpPacket = (AvtpPacket *)RemoveHeadList(&chnl->avPacketList);
                status = AVTP_SendStart(chnl, avtpPacket);
                if (status != BT_STATUS_PENDING) {
                    /* Failed to send next packet */
                    parms.event = AVTP_EVENT_TX_ERROR;
                    parms.status = status;
                    parms.packet = avtpPacket;
                    chnl->callback(chnl, &parms);
                } else {
                    break;
                }
            }
        } else {
            /* Send a packet */
            status = AvtpSendPacket(chnl);
            if (status != BT_STATUS_PENDING) {
                /* Could not send */
                if (chnl->txTimer.context) {
                    chnl->txTimer.context = 0;
                    EVM_CancelTimer(&chnl->txTimer);
                }
                chnl->txState = AVTP_TRANS_STATE_FIRST;
                parms.event = AVTP_EVENT_TX_ERROR;
                parms.status = status;
                chnl->callback(chnl, &parms);
            }
        }
    }
}

/*---------------------------------------------------------------------------
 *            AVTP_Receive()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Parse received data.
 *
 */
void AVTP_Receive(AvtpChannel *chnl, U8 *Buffer, U16 Len, U8 rxIdSize)
{
    U8  messageType = Buffer[0] & 0x03;
    U8  packetType = (Buffer[0] & 0x0C) >> 2;
    U8  transId = Buffer[0] >> 4;
    U16 rxId = 0;
    U8  offset = 0; 
    U8  event = AVTP_EVENT_RX_IND;
    AvtpCallbackParms parms;

    /* Parse and get number of packets */
    switch (packetType) {
    case AVTP_PACKET_TYPE_SINGLE:
        Report(("AVTP: Receiving Single packet\n"));
        chnl->rxState = AVTP_TRANS_STATE_FIRST;
        chnl->rxPacketsLeft = 0;
        rxId = rxIdSize == 1 ? Buffer[1] : BEtoHost16(Buffer + 1);
        chnl->rxId = rxId;
        chnl->resetTimer = FALSE;
        if ((rxIdSize == 2) && ((Buffer[3] & 0x0f) == 0x0f)) {
            /* This is an interim response, reset timer. */
            chnl->resetTimer = TRUE;
        }

        /* Set pointer to the packet data */
        offset += 1 + rxIdSize;
        break;

    case AVTP_PACKET_TYPE_START:
        Report(("AVTP: Receiving Start packet\n"));
        chnl->rxState = AVTP_TRANS_STATE_CONTINUE;
        chnl->rxPacketsLeft = Buffer[1] - 1;
        rxId = rxIdSize == 1 ? Buffer[2] : BEtoHost16(Buffer + 2);
        chnl->rxId = rxId;
        chnl->resetTimer = FALSE;
        if ((rxIdSize == 2) && ((Buffer[4] & 0x0f) == 0x0f)) {
            /* This is an interim response, reset timer. */
            chnl->resetTimer = TRUE;
        }

        /* Set pointer to the packet data */
        offset += 2 + rxIdSize;
        break;

    case AVTP_PACKET_TYPE_CONTINUE:
        /* Continue Packet */
        Report(("AVTP: Receiving Continue packet\n"));
        rxId = chnl->rxId;
        if (chnl->rxPacketsLeft == 0) {
            /* Received too many packets */
            Report(("AVTP: RX Buffer Overrun\n"));
            chnl->rxState = AVTP_TRANS_STATE_FIRST;
            event = AVTP_EVENT_RX_BUFFER_OVERRUN;
            goto call_back;
        }
        chnl->rxPacketsLeft--;
        
        /* Set pointer to the packet data */
        offset++;
        break;

    case AVTP_PACKET_TYPE_END:
        /* Last Packet */
        Report(("AVTP: Receiving End packet\n"));
        chnl->rxState = AVTP_TRANS_STATE_FIRST;
        rxId = chnl->rxId;
        chnl->rxPacketsLeft--;
        if (chnl->rxPacketsLeft != 0) {
            /* Received too few packets */
            Report(("AVTP: RX Buffer Underrun\n"));
            event = AVTP_EVENT_RX_BUFFER_UNDERRUN;
            goto call_back;
        }

        /* Set pointer to the packet data */
        offset++;
        break;

    default:
        /* Unknown packet type */
        chnl->rxState = AVTP_TRANS_STATE_FIRST;
        Report(("AVTP: RX Unknown Packet\n"));
        event = AVTP_EVENT_RX_UNKNOWN_PACKET;
        goto call_back;
    }

    /* Call the Channel Manager callback */
    if (messageType == AVTP_MSG_TYPE_COMMAND) {
        /* Receiving a command */
        chnl->acpTransId = transId;
    } else if ((messageType == AVTP_MSG_TYPE_ACCEPT) ||
               (messageType == AVTP_MSG_TYPE_REJECT)) {

        /* Receiving a response */
        if (chnl->intTransId == transId) {
            if (!chnl->resetTimer &&
                ((packetType == AVTP_PACKET_TYPE_SINGLE) ||
                (packetType == AVTP_PACKET_TYPE_END))) {
                /* Transaction ID matches */
                chnl->intTransId = ++chnl->intTransId % 16;
            }
        } else {
            Report(("AVTP: RX Bad Transaction ID\n"));
            chnl->rxState = AVTP_TRANS_STATE_FIRST;
            event = AVTP_EVENT_RX_BAD_TRANS_ID;
            goto call_back;
        }

        if (messageType == AVTP_MSG_TYPE_ACCEPT) {
            /* Accept Message */
            if (chnl->rxPacketsLeft == 0) {
                /* Response Received */
                if (chnl->txTimer.context) {
                    if (chnl->resetTimer) {
                        chnl->txTimer.func = AvtpTxTimeout;
                        chnl->txTimer.context = chnl;
                        EVM_StartTimer(&chnl->txTimer, chnl->txTimeout);
                    } else {
                        chnl->txTimer.context = 0;
                        EVM_CancelTimer(&chnl->txTimer);
                    }
                }
            }
        } else if (messageType == AVTP_MSG_TYPE_REJECT) {
            /* Reject Message */
            if (chnl->rxPacketsLeft == 0) {
                /* Response Received */
                if (chnl->txTimer.context) {
                    chnl->txTimer.context = 0;
                    EVM_CancelTimer(&chnl->txTimer);
                }
            }
        } else {
            /* Unknown message type */
            Report(("AVTP: RX Unknown Message\n"));
            chnl->rxState = AVTP_TRANS_STATE_FIRST;
            event = AVTP_EVENT_RX_UNKNOWN_MESSAGE;
        }
    } else {
        /* Unknown message type */
        Report(("AVTP:  RX Bad Message Type\n"));
        chnl->acpTransId = transId;
        chnl->rxState = AVTP_TRANS_STATE_FIRST;
        event = AVTP_EVENT_RX_UNKNOWN_MESSAGE;
    }

call_back:

    parms.event = event;
    parms.status = BT_STATUS_SUCCESS;
    parms.msgType = messageType;
    parms.pktType = packetType;
    parms.rxId = rxId;
    parms.packetsLeft = chnl->rxPacketsLeft;
    parms.data = Buffer + offset;
    parms.len = Len - offset;
    chnl->callback(chnl, &parms);
}

/*---------------------------------------------------------------------------
 *            AvtpSendPacket()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Send a single packet to L2CAP.
 *            AV packet is fragmented (if neccessary) into an L2CAP packet.
 *            Only 1 packet is sent. Subsequent ones will be sent when the
 *            one sent is confirmed.
 */
static BtStatus AvtpSendPacket(AvtpChannel *chnl)
{
    BtStatus status;
    U8  headerLen;
    U8  transId;
    U8 *hdrPos;

    if ((chnl->curAvPacket->msgType == AVTP_MSG_TYPE_COMMAND) && 
        (chnl->curAvPacket->txId != 0)) {
        transId = chnl->intTransId;
    } else {
        transId = chnl->acpTransId;
    }

    if (chnl->txState == AVTP_TRANS_STATE_FIRST) {
        /* Start State */
        if (chnl->txPacketsLeft > 1) {
            /* Start Packet */
            hdrPos = chnl->packet.header + BT_PACKET_HEADER_LEN;

            if (chnl->curAvPacket->msgHdrLen > 0) {
                hdrPos -= chnl->curAvPacket->msgHdrLen;
                OS_MemCopy(hdrPos, chnl->curAvPacket->msgHdr, 
                           chnl->curAvPacket->msgHdrLen);
            }

            if (chnl->curAvPacket->txIdSize == 2) {
                hdrPos -= 2;
                StoreBE16(hdrPos, chnl->curAvPacket->txId);
            } else {
                hdrPos--;
                *hdrPos = (U8)chnl->curAvPacket->txId;
            }

            hdrPos--;
            *hdrPos = chnl->txPacketsLeft;

            hdrPos--;
            *hdrPos = transId << 4;
            *hdrPos += (U8)(AVTP_PACKET_TYPE_START) << 2;
            *hdrPos += chnl->curAvPacket->msgType;

            headerLen = chnl->packet.header + BT_PACKET_HEADER_LEN - hdrPos;
        } else {
            /* Single Packet */
            hdrPos = chnl->packet.header + BT_PACKET_HEADER_LEN;

            if (chnl->curAvPacket->msgHdrLen > 0) {
                hdrPos -= chnl->curAvPacket->msgHdrLen;
                OS_MemCopy(hdrPos, chnl->curAvPacket->msgHdr, 
                           chnl->curAvPacket->msgHdrLen);
            }

            if (chnl->curAvPacket->txIdSize == 2) {
                hdrPos -= 2;
                StoreBE16(hdrPos, chnl->curAvPacket->txId);
            } else {
                hdrPos--;
                *hdrPos = (U8)chnl->curAvPacket->txId;
            }

            hdrPos--;
            *hdrPos = transId << 4;
            *hdrPos += (U8)(AVTP_PACKET_TYPE_SINGLE) << 2;
            *hdrPos += chnl->curAvPacket->msgType;

            headerLen = chnl->packet.header + BT_PACKET_HEADER_LEN - hdrPos;
        }
    } else {
        /* Continue State */
        hdrPos = chnl->packet.header + BT_PACKET_HEADER_LEN;

        hdrPos--;
        *hdrPos = transId << 4;
        if (chnl->txPacketsLeft > 1) {
            /* Continue Packet */
            *hdrPos += (U8)(AVTP_PACKET_TYPE_CONTINUE) << 2;
        } else {
            /* End Packet */
            *hdrPos += (U8)(AVTP_PACKET_TYPE_END) << 2;
        }

        *hdrPos += chnl->curAvPacket->msgType;

        headerLen = chnl->packet.header + BT_PACKET_HEADER_LEN - hdrPos;
    }

    /* Setup the packet */
    chnl->packet.headerLen = headerLen;
    if (chnl->curAvPacket->txData) {
        chnl->packet.data = chnl->curAvPacket->txData + chnl->offset;
    }
    chnl->packet.dataLen = min(chnl->packetSize, chnl->curAvPacket->txDataLen - 
                                                 chnl->offset);
    if ((chnl->packet.dataLen + headerLen) > L2CAP_MTU) {
        chnl->packet.dataLen -= chnl->packet.dataLen + headerLen - L2CAP_MTU;
    }

    /* Send it to L2CAP */
    status = L2CAP_SendData(chnl->l2ChannelId, &chnl->packet);

    if (status == BT_STATUS_PENDING) {
        /* Calculate bytes left to send */
        chnl->offset += chnl->packet.dataLen;
        chnl->txPacketsLeft--;
    }

    return status;
}

/*---------------------------------------------------------------------------
 *            AvtpTxTimeout()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Timeout handler for commands.
 *
 */
static void AvtpTxTimeout(EvmTimer *Timer)
{
    AvtpChannel *chnl = (AvtpChannel *)Timer->context;
    AvtpCallbackParms parms;

    chnl->txTimer.context = 0;
    chnl->txState = AVTP_TRANS_STATE_FIRST;
    parms.event = AVTP_EVENT_TX_TIMEOUT;
    parms.status = BT_STATUS_SUCCESS;
    parms.packet = chnl->curAvPacket;
    Report(("AVTP:  TX Timeout\n"));
    chnl->callback(chnl, &parms);
}
