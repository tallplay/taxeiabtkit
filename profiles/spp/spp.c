/***************************************************************************
 *
 * File:
 *     $Workfile:spp.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:45$
 *
 * Description:
 *     Sample Port API Profile for RFCOMM.
 *
 *
 * Copyright 1999-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "sdp.h"
#include "spp.h"

#if XA_MULTITASKING == XA_DISABLED
#if BT_SPP_IGNORE_MULTITASKING != XA_ENABLED 
#   error !!! SPP is designed for a multitasking OS.
#endif /* BT_SPP_IGNORE_MULTITASKING */
#endif /* XA_MULTITASKING */
 
#if (SPP_CLIENT == XA_DISABLED) && (SPP_SERVER == XA_DISABLED)
#   error !!! Either SPP_CLIENT and/or SPP_SERVER must be enabled. !!!
#endif



/***********************************************************************
 *
 * Defines, Typedefs and Structures
 *
 ***********************************************************************/

#define Display(x)          Report(x)

#define DC1     0x11        /* Default XON char */
#define DC3     0x13        /* Default XOFF char */
typedef BOOL (*DevState)(SppDev *, RfCallbackParms *);

#define stateDisconnected     stateDefaultHandler

/***********************************************************************
 *
 * Function Prototypes
 *
 ***********************************************************************/
 
static void rfCallbackFunc(RfChannel *chan, RfCallbackParms *parms);
static void defaultEventHandler(SppDev *dev, RfCallbackParms *parms);
static BOOL stateDefaultHandler(SppDev *dev, RfCallbackParms *parms);
static BOOL stateConnected(SppDev *dev, RfCallbackParms *parms);
static BOOL stateClosed(SppDev *dev, RfCallbackParms *parms);
static void sendToRfComm(SppDev *dev);
static void setHighWaterMark(SppDev *dev);
static void advanceCredit(SppDev *dev);
static void closeDevice(SppDev *dev);
static void initDevice(SppDev *dev);
#if SPP_CLIENT == XA_ENABLED
static BtStatus openClientPort(SppDev *dev, BtRemoteDevice *btDevice);
static BtStatus queryForService(SppDev *dev);
static void queryCallback(const BtEvent *event);
static BtStatus sppConnect(SppDev *dev, BtRemoteDevice *remDev, U8 serverId);
#endif
#if SPP_SERVER == XA_ENABLED
static BtStatus openServerPort(SppDev *dev);
static void deRegisterService(SppDev *dev);
static BtStatus addSdpService(SppDev *dev);
static void removeSdpService(SppDev *dev);
static void insertRfCommChannel(SdpRecord *rec, U8 channel);
#endif
#if XA_DEBUG == XA_ENABLED
static const char *pRfEvent(RfEvent event);
#endif 


/***********************************************************************
 *
 * ROM Variables
 *
 ***********************************************************************/

/*-------------------------------------------------------------------------
 * SPP state machine.
 */
static const DevState deviceState[] = {
    stateClosed,   
    stateDisconnected,
    stateConnected
};

/* SPP_Open has not been called or SPP_Closed was last called */
#define DEVICE_STATE_CLOSED         0

/* SPP_Open has been called but the device is not connected. */
#define DEVICE_STATE_DISCONNECTED   1

/* The device is opened and connected to a remote device. */
#define DEVICE_STATE_CONNECTED      2



/****************************************************************************
 *
 * RAM Data Section
 *
 ****************************************************************************/



/****************************************************************************
 *
 * Function Section
 *
 ****************************************************************************/
               
/*---------------------------------------------------------------------------
 * SPP_InitDevice() 
 */
void SPP_InitDevice(SppDev *dev, BtPacket *txPacket, U16 numPackets)
{
    U16 i;

    Assert(dev);
    Assert((dev->portType == SPP_CLIENT_PORT) || (dev->portType == SPP_SERVER_PORT));
    Assert(txPacket);
    Assert(numPackets != 0);

#if XA_ERROR_CHECK == XA_ENABLED
    if ((! dev) || ((dev->portType != SPP_CLIENT_PORT) && 
                    (dev->portType != SPP_SERVER_PORT))) {
        return;
    }
#endif

    dev->state = DEVICE_STATE_CLOSED;
    
    InitializeListHead(&dev->txPacketList);
    for (i = 0; i < numPackets; i++) {
        InsertHeadList(&dev->txPacketList, (ListEntry *)(txPacket + i));
    }
}


/*---------------------------------------------------------------------------
 * SPP_Open() 
 *      This is the common entry poS16 for opening a server or client port.
 */
BtStatus SPP_Open(SppDev *dev, BtRemoteDevice *btDevice)
{
    BtStatus rc;

    Assert(dev);
#if XA_ERROR_CHECK == XA_ENABLED
    if (! dev) {
        return BT_STATUS_FAILED;
    }
#endif

    if (dev->state != DEVICE_STATE_CLOSED) {
        return BT_STATUS_FAILED;
    }

    SPPOS_LockDev(dev->osDev);

    initDevice(dev);

    if (dev->portType & SPP_SERVER_PORT) {
#if SPP_SERVER == XA_ENABLED
        rc = openServerPort(dev);
#else
        rc = BT_STATUS_FAILED;
#endif
    } else {
#if SPP_CLIENT == XA_ENABLED
        rc = openClientPort(dev, btDevice);
#else
        rc = BT_STATUS_FAILED;
#endif
    }

    if ((rc == BT_STATUS_SUCCESS) || (rc == BT_STATUS_PENDING)) {
        dev->state = DEVICE_STATE_DISCONNECTED;
        rc = BT_STATUS_SUCCESS;
    } else {
        rc = BT_STATUS_FAILED;
        dev->state = DEVICE_STATE_CLOSED;
    }

    SPPOS_UnlockDev(dev->osDev);

    return rc;
}

 
 
/*---------------------------------------------------------------------------
 * SPP_Close() 
 *      Close the device and release any resources as appropriate for your
 *      OS. 
 *
 * Requires:
 *     The caller of this function assures that its caller owns the device.
 *     Therefore, if dev->state == NULL, return success anyway. This 
 *     condition will exist if the remove device closes the connection
 *     first.
 *
 * Parameters:
 *
 * Returns:
 */
BtStatus SPP_Close(SppDev *dev)
{
    BtStatus status = BT_STATUS_FAILED;

    SPPOS_LockDev(dev->osDev);

    if (dev->state != DEVICE_STATE_CLOSED) {
        if (dev->state == DEVICE_STATE_CONNECTED) {
            SPPOS_UnlockDev(dev->osDev);
            status = RF_CloseChannel(&dev->channel);
            if ((status == BT_STATUS_PENDING) || (status == BT_STATUS_IN_PROGRESS)) {
                status = BT_STATUS_SUCCESS;
            } else {
                status = BT_STATUS_FAILED;
            }
            SPPOS_LockDev(dev->osDev);
        } else {
            status = BT_STATUS_SUCCESS;
        }
    }

    SPPOS_UnlockDev(dev->osDev);

#if SPP_SERVER == XA_ENABLED
    if ((dev->portType & SPP_SERVER_PORT) 
        && (dev->state != DEVICE_STATE_CLOSED)) {
        deRegisterService(dev);
        closeDevice(dev);
    }
#endif /* SPP_SERVER == XA_ENABLED */

    dev->state = DEVICE_STATE_CLOSED;
    return status;
}

 
 
/*---------------------------------------------------------------------------
 * SPP_Read() 
 *      
 */
BtStatus SPP_Read(SppDev *dev, char *buffer, U16 *maxBytes)
{
    BtStatus status = BT_STATUS_FAILED;

    SPPOS_LockDev(dev->osDev);

    if (dev->state != DEVICE_STATE_CLOSED) {

        status = SPPOS_ReadBuf(dev->osDev, buffer, maxBytes);
        advanceCredit(dev);
    }

    SPPOS_UnlockDev(dev->osDev);
    return status;
}

 
 
/*---------------------------------------------------------------------------
 * SPP_Write() 
 *      Driver write routine. Data is copied to the driver's buffer and
 *      sent to RFCOMM. The caller's buffer is free to reuse when this
 *      call returns.
 */
BtStatus SPP_Write(SppDev *dev, char *buffer, U16 *nBytes)
{
    BtStatus status = BT_STATUS_FAILED;
    
    SPPOS_LockDev(dev->osDev);

    if (dev->state != DEVICE_STATE_CLOSED) {
        Report(("tx %d", *nBytes));
        status = SPPOS_WriteBuf(dev->osDev, buffer, nBytes);
        Report(("/%d\n", *nBytes));
        if (status == BT_STATUS_SUCCESS) {
            sendToRfComm(dev);
        } else {
            status = BT_STATUS_FAILED;
        }
    }

    SPPOS_UnlockDev(dev->osDev);
    return status;
}

 
 
/*---------------------------------------------------------------------------
 * SPP_Ioctl() 
 */
BtStatus SPP_Ioctl(SppDev *dev, U16 function, void *arg)
{
    BtStatus status;

    SPPOS_LockDev(dev->osDev);

    dev->portSettings.parmMask = 0;
    status = SPPOS_Ioctl(dev->osDev, function, arg);

#if RF_SEND_CONTROL == XA_ENABLED
    if ((status == BT_STATUS_SUCCESS) && (dev->portSettings.parmMask)) {
        status = SPP_SendPortSettings(dev);
    }
#endif

    SPPOS_UnlockDev(dev->osDev);

    return status;
}


void SPP_SetBaud(SppDev *dev, U8 baud)
{
    dev->portSettings.baudRate = baud;
    dev->portSettings.parmMask = RF_PARM_BAUDRATE;
}


void SPP_SetFlowControl(SppDev *dev, U8 val)
{
    dev->portSettings.flowControl = val;
    dev->portSettings.parmMask |= RF_PARM_FLOW_DTR_DSR
                                  | RF_PARM_FLOW_RTS_CTS
                                  | RF_PARM_FLOW_XON_XOFF;
}


void SPP_SetDataFormat(SppDev *dev, U8 val)
{
    dev->portSettings.dataFormat = val 
        & (U8)(RF_PARITY_TYPE_MASK | RF_PARITY_MASK | RF_STOP_BITS_MASK 
               | RF_DATA_BITS_MASK);
    dev->portSettings.parmMask |= RF_PARM_DATA_FORMAT;
}


BtStatus SPP_SetModemControl(SppDev *dev, RfSignals signals)
{
    BtStatus status = BT_STATUS_SUCCESS;

    dev->lModemStatus.signals = signals;

#if RF_SEND_CONTROL == XA_ENABLED
    status = RF_SetModemStatus(&dev->channel, &dev->lModemStatus);
    if (status == BT_STATUS_PENDING) {
        status = BT_STATUS_SUCCESS;
        SPPOS_Wait(dev->osDev, RF_T2_TIMEOUT);
    } else {
        status = BT_STATUS_FAILED;
    }
#endif /* RF_SEND_CONTROL == XA_ENABLED */
    return status;

}


/*---------------------------------------------------------------------------
 *  Tell RFCOMM to send the port settings to the remote device. 
 *  Note that we can have only one outstanding port request at a time.
 *  To ensure that we will block until the response is returned or a
 *  timer expires.
 */
BtStatus SPP_SendPortSettings(SppDev *dev)
{
#if RF_SEND_CONTROL == XA_ENABLED
    BtStatus rc;

    SPPOS_UnlockDev(dev->osDev);

    Report(("SENDING port settings\n"));
    if (RF_RequestPortSettings(&dev->channel, &dev->portSettings) == BT_STATUS_PENDING) {
        rc = BT_STATUS_SUCCESS;
        SPPOS_Wait(dev->osDev, RF_T2_TIMEOUT);
    } else {
        rc = BT_STATUS_FAILED;
    }
    SPPOS_LockDev(dev->osDev);

    return rc;
#else
    return BT_STATUS_SUCCESS;
#endif
}


#if SPP_CLIENT == XA_ENABLED

/*---------------------------------------------------------------------------
 * sppConnect()
 *      If remDev is not NULL, save the args in the device struct.
 *      If the device is a client and already open, tart an RFCOMM connection
 *      to a server.
 */
static BtStatus sppConnect(SppDev *dev, BtRemoteDevice *remDev, U8 serverId)
{
    BtStatus rc;

    if (remDev) {
        dev->type.client.remDev = remDev;
        dev->type.client.serverId = serverId;
    }

    if (dev->state == DEVICE_STATE_DISCONNECTED) {
        /* RF_OpenClientChannel checks for null remDev so we need not. */
        rc = RF_OpenClientChannel(dev->type.client.remDev, 
                                  dev->type.client.serverId, 
                                  &dev->channel,
                                  dev->credit);

        if (rc == BT_STATUS_PENDING) {
            rc = BT_STATUS_SUCCESS;
            dev->state = DEVICE_STATE_DISCONNECTED;
        } else {
            rc = BT_STATUS_FAILED;
        }
    } else {
        rc = BT_STATUS_SUCCESS;
    }

    return rc;
}


/*---------------------------------------------------------------------------
 * queryForService() 
 *      Start an SDP query of the specified remoted device for a serial
 *      port.
 *     
 * Requires:
 *      Existing ACL with the remote device.
 *
 * Parameters:
 *      dev     the device to query
 *
 * Returns:
 *      SPP_INVALID_CHANNEL     SDP did not locate the serial port service
 *      On success a valid channel is returned that can be used to complete
 *      a connection to the serial port service on the remote device (see
 *      SPP_Open, sppConnect).
 */
static BtStatus queryForService(SppDev *dev)
{
    Assert(dev);

    if (! dev) {
        return 0;
    }

    dev->type.client.sdpToken.attribId     = AID_PROTOCOL_DESC_LIST;
    dev->type.client.sdpToken.uuid         = PROT_RFCOMM;
    dev->type.client.sdpToken.mode         = BSPM_BEGINNING;
    dev->type.client.sdpToken.callback     = queryCallback;
    if (SDP_Query(&dev->type.client.sdpToken, BSQM_FIRST) == BT_STATUS_PENDING) {
        return BT_STATUS_SUCCESS;
    } else {
        return BT_STATUS_FAILED;
    }
} 
#endif /* SPP_CLIENT == XA_ENABLED */


/*---------------------------------------------------------------------------
 * initDevice() 
 *      Do some common initialization to device.
 *
 * Requires:
 *     
 * Parameters:
 *      dev         pointer to device structure 
 */
static void initDevice(SppDev *dev)
{

    /* Initialize RS232 signals and other port settings */
    dev->portSettings.baudRate      = RF_BAUD_9600;
    dev->portSettings.dataFormat    = RF_DATA_BITS_8 | RF_STOP_BITS_1
                                      | RF_PARITY_NONE;
    dev->portSettings.flowControl   = RF_FLOW_CTRL_NONE;
    dev->portSettings.xonChar       = DC1;
    dev->portSettings.xoffChar      = DC3;
    dev->lineStatus                 = 0;
    dev->rModemStatus.signals       = 0;
    dev->credit                     = 1;

    /* Flush the buffers */
    SPPOS_FlushRx(dev->osDev);
    SPPOS_FlushTx(dev->osDev);
}


#if SPP_SERVER == XA_ENABLED
/*---------------------------------------------------------------------------
 * addSpdService() 
 *      Establish some links in the device structure and register the 
 *      service with SDP.
 *
 *      When we initialized the device structure (SPP_InitDevice), we copied
 *      some SDP-related templates into the device structure. Some fields
 *      in the device structure have been updated after SPP_InitDevice, so now
 *      we can go through and fix some links.
 *     
 * Parameters:
 *      dev     pointer to structure describing remote device
 *
 * Returns:
 *      BT_STATUS_SUCCESS
 *      Error code from SDP_AddRecord
 */
static BtStatus addSdpService(SppDev *dev)
{
    BtStatus         status;

    /* Put the channel (serverId) in the Protocol Descriptor list */
    insertRfCommChannel(dev->type.sppService->sdpRecord, 
                        dev->type.sppService->service.serviceId);

    status = SDP_AddRecord(dev->type.sppService->sdpRecord);
    return status;
}


static void removeSdpService(SppDev *dev)
{
    AssertEval(SDP_RemoveRecord(dev->type.sppService->sdpRecord) != BT_STATUS_FAILED);
}


/*---------------------------------------------------------------------------
 * Register the server with RFCOMM and initialize values.
 *
 * Returns:
 *      BT_STATUS_SUCCESS
 *      BT_STATUS_FAILED
 */
static BtStatus openServerPort(SppDev *dev)
{
    BtStatus bts;
    
    dev->channel.callback     = rfCallbackFunc;
    dev->channel.maxFrameSize = RF_MAX_FRAME_SIZE;
    dev->channel.priority     = 63;
    dev->state                = DEVICE_STATE_DISCONNECTED;

    SPPOS_UnlockDev(dev->osDev);
    /* Does not return pending */
    bts = RF_RegisterServerChannel(&dev->channel, 
                                   &dev->type.sppService->service,
                                   dev->credit);
    SPPOS_LockDev(dev->osDev);

    if (bts == BT_STATUS_SUCCESS) {
        Report(("SPP_Open: service available on RFCOMM channel %d\n", 
                dev->type.sppService->service.serviceId));
        if (dev->type.sppService->numPorts == 0) {
            bts = addSdpService(dev);
            if (bts != BT_STATUS_SUCCESS) {
                bts = BT_STATUS_FAILED;
                deRegisterService(dev);
                closeDevice(dev);
                dev->type.sppService->numPorts--;    /* Negate following increment */
            }
        }
        dev->type.sppService->numPorts++;
    }

    return bts;
}
#endif /* SPP_SERVER == XA_ENABLED */
        


#if SPP_CLIENT == XA_ENABLED
/*---------------------------------------------------------------------------
 * openClientPort() 
 *      Initialize values.
 *
 * Returns:
 *      BT_STATUS_SUCCESS
 *      BT_STATUS_INVALID_PARM
 */
static BtStatus openClientPort(SppDev *dev, BtRemoteDevice *btDevice)
{
    BtStatus status;

#if XA_ERROR_CHECK == XA_ENABLED
    if ((btDevice == 0) || 
        (ME_FindRemoteDevice(&btDevice->bdAddr) != btDevice)) {
        return BT_STATUS_INVALID_PARM;
    }
#endif
    dev->channel.callback     = rfCallbackFunc;
    dev->channel.maxFrameSize = RF_MAX_FRAME_SIZE;
    dev->channel.priority     = 63;
    dev->state                = DEVICE_STATE_DISCONNECTED;
    dev->type.client.remDev   = btDevice;
    dev->type.client.sdpToken.rm = btDevice;
    status = queryForService(dev);
    
    return status;
}


/*---------------------------------------------------------------------------
 * Callback function for queryForService.
 */
static void queryCallback(const BtEvent *event)
{
    BtStatus status;
    SdpQueryToken *token;
    SppDev *dev;
    SppClient *client;

    /* The token is a member of a structure within the SppDev structure.
     *      SppDev->type.client.sdpToken (type is a union)
     * thus we can find the address of SppDev.
     */
    token  = event->p.token;
    client = ContainingRecord(event->p.token, SppClient, sdpToken);
    dev = ContainingRecord(client, SppDev, type);
    
    switch (event->eType) {

    case SDEVENT_QUERY_RSP:
        status = SDP_ParseAttributes(token);
        if (status == BT_STATUS_SUCCESS) {
            /* Value returned should be U8 for channel. */
            Assert(token->totalValueLen == 1);
            status = sppConnect(dev, token->rm, token->valueBuff[0]);
            Report(("SPP: SDP_Connect() returned %d\n", status));
        } else if (status == BT_STATUS_SDP_CONT_STATE) {
            /* We need to continue the query. */
            token->mode = BSPM_CONT_STATE;
            status = SDP_Query(token, BSQM_CONTINUE);
            Report(("SPP: SDP_Query() returned %d\n", status));
        }
        break;

    case SDEVENT_QUERY_ERR:
    case SDEVENT_QUERY_FAILED:
        Report(("SPP: SDP Query Failed\n"));
        /* Do we not indicate this back somehow? */
        break;
    }
}

#endif /* SPP_CLIENT == XA_ENABLED */


/*---------------------------------------------------------------------------
 * rfCallbackFunc() 
 *      RFCOMM callback function. This routine is used for all serial
 *      ports' callback so we need to ensure that it is re-entrant.
 *      
 * Requires:
 *     
 * Parameters:
 *      chan    the RfChannel to which the event applies
 *      parms   callback parameters
 *
 * Returns:
 */
static void rfCallbackFunc(RfChannel *chan, RfCallbackParms *parms)
{
    SppDev *dev;

    Assert(chan);

    /* The RfChannel * that we passed to RFCOMM was a pointer to the 
     * channel in our device structure. Now we need to get the address
     * of that structure so we know to whom this even belongs.
     */
    dev = ContainingRecord(chan, SppDev, channel);
    Assert(dev);

    Report(("SPP: %s\n", pRfEvent(parms->event)));

    SPPOS_LockDev(dev->osDev);

    if ((*deviceState[dev->state])(dev, parms) == FALSE) {
        /* Event was not handled by state machine */
        defaultEventHandler(dev, parms);
    }

    SPPOS_UnlockDev(dev->osDev);
}


#if XA_DEBUG == XA_ENABLED
static const char *pRfEvent(RfEvent event)
{
    const char *msg;

    switch(event) {
    case RFEVENT_OPEN_IND:
        msg = "RFEVENT_OPEN_IND";
        break;
    case RFEVENT_OPEN:
        msg = "RFEVENT_OPEN";
        break;
    case RFEVENT_CLOSE_IND:
        msg = "RFEVENT_CLOSE_IND";
        break;
    case RFEVENT_CLOSED:
        msg = "RFEVENT_CLOSED";
        break;
    case RFEVENT_DATA_IND:
        msg = "RFEVENT_DATA_IND";
        break;
    case RFEVENT_PACKET_HANDLED:
        msg = "RFEVENT_PACKET_HANDLED";
        break;
#if RF_SEND_TEST == XA_ENABLED
    case RFEVENT_TEST_CNF:
        msg = "RFEVENT_TEST_CNF";
        break;
#endif /* RF_SEND_TEST enabled */
    case RFEVENT_PORT_NEG_IND:
        msg = "RFEVENT_PORT_NEG_IND";
        break;
#if RF_SEND_CONTROL == XA_ENABLED
    case RFEVENT_PORT_NEG_CNF:
        msg = "RFEVENT_PORT_NEG_CNF";
        break;
#endif /* RF_SEND_CONTROL enabled */
    case RFEVENT_PORT_STATUS_IND:
        msg = "RFEVENT_PORT_STATUS_IND";
        break;
#if RF_SEND_CONTROL == XA_ENABLED
    case RFEVENT_PORT_STATUS_CNF:
        msg = "RFEVENT_PORT_STATUS_CNF";
        break;
#endif /* RF_SEND_CONTROL enabled */
    case RFEVENT_MODEM_STATUS_IND:
        msg = "RFEVENT_MODEM_STATUS_IND";
        break;
    case RFEVENT_MODEM_STATUS_CNF:
        msg = "RFEVENT_MODEM_STATUS_CNF";
        break;
#if RF_SEND_CONTROL == XA_ENABLED
    case RFEVENT_LINE_STATUS_IND:
        msg = "RFEVENT_LINE_STATUS_IND";
        break;
    case RFEVENT_LINE_STATUS_CNF:
        msg = "RFEVENT_LINE_STATUS_CNF";
        break;
#endif /* RF_SEND_CONTROL enabled */
    case RFEVENT_FLOW_OFF_IND:
        msg = "RFEVENT_FLOW_OFF_IND";
        break;
    case RFEVENT_FLOW_ON_IND:
        msg = "RFEVENT_FLOW_ON_IND";
        break;
    case RFEVENT_RESOURCE_FREE:
        msg = "RFEVENT_RESOURCE_FREE";
        break;
    default:
        msg = "unknown";
    }

    return msg;
}
#endif /* XA_DEBUG == XA_ENABLED */

/*---------------------------------------------------------------------------
 * defaultEventHandler() 
 *      This is a default handler for RFCOMM events. Each state gets a
 *      chance to intercept events. If it doesn't care about them or likes
 *      the default, it can be handled here.
 *
 * Requires:
 *      LOCKDEV() must be called prior to entry
 *     
 * Parameters:
 *      dev     pointer to device structure
 *      parms   callback parameters
 *   
 * Returns:
 */
static void defaultEventHandler(SppDev *dev, RfCallbackParms *parms)
{
    BtStatus status;

    switch (parms->event) {
    
    case RFEVENT_PACKET_HANDLED:
        /* 
         *  An acknowledgment for RF_SendData() is received. Remove those
         *  bytes from our buffer and send any more data we have pending.
         */
        SPPOS_ReturnBuf(dev->osDev, (char *)parms->ptrs.packet->data, parms->ptrs.packet->dataLen);
        InsertHeadList(&dev->txPacketList, &parms->ptrs.packet->node);
        sendToRfComm(dev);
        break;

    case RFEVENT_CLOSE_IND:
        Report(("SPP: RFEVENT_CLOSE_IND\n"));
        break;

    case RFEVENT_CLOSED:
        /*
         * Channel was closed. The following code usually pertains only to
         * clients closing: either remote or local. If we're a server
         * closing, the event is handled by the state machine; however, 
         * servers can get this event here instead of in the state machine
         * when the link is forced down.
         */
        Report(("SPP: RFEVENT_CLOSED\n"));

        dev->rModemStatus.signals &= ~(RF_CD | RF_DSR | RF_CTS);

        if (dev->state != DEVICE_STATE_CLOSED) {
            dev->state = DEVICE_STATE_DISCONNECTED;
        }
        break;

    case RFEVENT_DATA_IND:
        {
            U16 bytes = parms->dataLen;

            /* Data was received on the channel. */
            dev->credit--;

            AssertEval((SPPOS_DevRx(dev->osDev, (char *)parms->ptrs.data, &bytes) == 
                        BT_STATUS_SUCCESS));
            Report(("rx %d, saved %d, value %c\n", parms->dataLen, bytes, *parms->ptrs.data));
            if (bytes < parms->dataLen) {
                /* Our buffer was full so log an overrun */
                Display(("SPP: RX overrun\n"));
                dev->lineStatus |= RF_OVERRUN;
#if RF_SEND_CONTROL == XA_ENABLED
                status = RF_SetLineStatus(&dev->channel, dev->lineStatus);
                Assert((status == BT_STATUS_PENDING)
                       || (status == XA_STATUS_IN_PROGRESS));
#endif
            }
        }
        break;
        
    case RFEVENT_OPEN_IND:
        status = RF_AcceptChannel(&dev->channel);
        if (status != BT_STATUS_PENDING) {
            Report(("SPP: RF_AcceptChannel failed: %d\n", status));
        }
        break;
    
    case RFEVENT_OPEN:
        Report(("SPP: channel open.\n"));
        dev->state = DEVICE_STATE_CONNECTED;
        dev->lModemStatus.signals = RF_DTR | RF_CTS | RF_CD;
        setHighWaterMark(dev);
        advanceCredit(dev);
        /* App may have already written to the device. */
        sendToRfComm(dev);
        break;

    case RFEVENT_PORT_NEG_IND:        
        /* Port negotiation request was received */
        /* Automatically accept since we're basically a legacy serial driver */
        dev->portSettings = *parms->ptrs.portSettings;
        break;

#if RF_SEND_CONTROL == XA_ENABLED

    case RFEVENT_PORT_NEG_CNF:        /* Port negotiation confirmed */
        SPPOS_Resume(dev->osDev);
        dev->portSettings = *parms->ptrs.portSettings;
        break;

    case RFEVENT_MODEM_STATUS_CNF:    /* Modem status confirmed */
        SPPOS_Resume(dev->osDev);
        break;

    case RFEVENT_MODEM_STATUS_IND:    /* Modem status received */
        dev->rModemStatus.signals  = parms->ptrs.modemStatus->signals;
        dev->rModemStatus.breakLen = parms->ptrs.modemStatus->breakLen;
        break;

    case RFEVENT_LINE_STATUS_IND:    /* Line status received */
        /* Line status should not be cleared until the app reads it. */
        dev->lineStatus |= *(parms->ptrs.lineStatus);
        break;

    case RFEVENT_LINE_STATUS_CNF:    /* Line status confirmed */
        break;

    case RFEVENT_PORT_STATUS_IND:
        status = RF_SendPortStatus(&dev->channel, &dev->portSettings);
        Assert(status == BT_STATUS_SUCCESS);
        break;

    case RFEVENT_PORT_STATUS_CNF:
        break;
#endif /* SEND_CONTROL enabled */

    case RFEVENT_FLOW_OFF_IND:
    case RFEVENT_FLOW_ON_IND:
        break;

#if RF_SEND_TEST == XA_ENABLED
    case RFEVENT_TEST_CNF:
#endif
        Report(("SPP: pass-thru event - %d\n", parms->event));
        break;

    default:
        Report(("SPP: unknown event - %d\n", parms->event));
    }
}


 
/*---------------------------------------------------------------------------
 * stateDefaultHandler() 
 *      State machine: catch-all allows defaultHandler to handle events
 *
 * Requires:
 *     
 * Parameters:
 *      dev     pointer to device structure
 *      parms   callback parameters
 *
 * Returns:
 *      FALSE   pass event to default handler
 */
static BOOL stateDefaultHandler(SppDev *dev, RfCallbackParms *parms)
{
    UNUSED_PARAMETER(dev);
    UNUSED_PARAMETER(parms);
    return FALSE;
}


 
/*---------------------------------------------------------------------------
 * stateClosed() 
 *      State machine: ignore all RFCOMM events
 *
 * Requires:
 *     
 * Parameters:
 *      dev     pointer to device structure
 *      parms   callback parameters
 *
 * Returns:
 *      FALSE   pass event to default handler
 */
static BOOL stateClosed(SppDev *dev, RfCallbackParms *parms)
{
    UNUSED_PARAMETER(dev);
    UNUSED_PARAMETER(parms);
    return TRUE;
}


/*---------------------------------------------------------------------------
 * stateConnected() 
 *      State machine: the channel/port is open and ready for activity.
 *
 * Requires:
 *     
 * Parameters:
 *      dev     pointer to device structure
 *      parms   callback parameters
 *   
 * Returns:
 *      TRUE    event was handled here
 *      FALSE   pass event to default handler
 */
static BOOL stateConnected(SppDev *dev, RfCallbackParms *parms)
{
    UNUSED_PARAMETER(dev);
    UNUSED_PARAMETER(parms);
    return FALSE;
}


#if SPP_SERVER == XA_ENABLED
/*---------------------------------------------------------------------------
 * deRegisterService() 
 *      Deregisters the channel. If it is the last channel open for the
 *      service, it also removes the SDP service record.
 *
 * Requires:
 *     
 * Parameters:
 *      dev     pointer to service device structure
 *
 * Returns:
 */
static void deRegisterService(SppDev *dev)
{
    BtStatus status;

    status = RF_DeregisterServerChannel(&dev->channel, 
                                        &dev->type.sppService->service);
    Assert(status != BT_STATUS_FAILED);

    if (status == BT_STATUS_SUCCESS) {
        dev->type.sppService->numPorts--;
        if (dev->type.sppService->numPorts == 0) {
            /* This is the last channel registered for the service. */
            AssertEval(RF_DeregisterService(&dev->type.sppService->service) 
                       != BT_STATUS_FAILED);
            removeSdpService(dev);
            dev->type.sppService->service.serviceId = 0;
        }
    }
}
#endif /* SPP_SERVER == XA_ENABLED */


/*---------------------------------------------------------------------------
 * sendToRfComm() 
 *      If we have not been xoff'ed and there is data in the write queue 
 *      for the port, send it to RFCOMM. Since RFCOMM does not have it's 
 *      own buffers, we must leave our data untouched until a packet
 *      handled event is returned.
 *
 * Requires:
 *      LOCKDEV() must be called prior to entry.
 *      Great care! There can be disasterous result when the device is
 *      unlocked. The other thread can barge right it.
 *
 * Parameters:
 *      dev     pointer to device structure
 *
 * Returns:
 */
static void sendToRfComm(SppDev *dev)
{
    U16 bytes;
    BtStatus status;
    BtPacket *pkt;

    if (IsListEmpty(&dev->txPacketList)) {
        /* No packets for transmitting */
        return;
    }

    pkt = (BtPacket *)RemoveHeadList(&dev->txPacketList);

    /* Safely grab frame size. */
    SPPOS_UnlockDev(dev->osDev);
    bytes = RF_FrameSize(&dev->channel);
    SPPOS_LockDev(dev->osDev);

    Report(("->RF %d/", bytes));
    SPPOS_DevTx(dev->osDev, (char **)&pkt->data, &bytes);
    Report(("%d\n", bytes));

    Assert(pkt->data);

    if (bytes > 0) {

        pkt->dataLen = bytes;
        /*
         * Note that it is possible (probable) that if this is the
         * app thread, we will get a callback BEFORE LOCKDEV() is 
         * reached below.
         */
        SPPOS_UnlockDev(dev->osDev);
        status = RF_SendData(&dev->channel, pkt);
        SPPOS_LockDev(dev->osDev);

        if (status != BT_STATUS_PENDING) {
            InsertHeadList(&dev->txPacketList, &pkt->node);
            Report(("SPP: RF_SendData failed: %d\n", status));
        }
    } else {
        InsertHeadList(&dev->txPacketList, &pkt->node);
    }
}


/*---------------------------------------------------------------------------
 * advanceCredit() 
 *      Advance credit to the other device.
 *     
 * Parameters:
 *      dev - pointer to SppDev
 */
static void advanceCredit(SppDev *dev)
{
    BtStatus status;
    S16 intCredit;
    U8 credit;

    /* Calculate total amount of credit. */
    SPPOS_UnlockDev(dev->osDev);
    Assert(RF_FrameSize(&dev->channel) != 0);
    intCredit = (S8)((S16)(dev->highWater - SPPOS_RxBytes(dev->osDev)) / 
                  RF_FrameSize(&dev->channel));
    SPPOS_LockDev(dev->osDev);

    /* Check for U8 overflow. */
    if (intCredit > 0xff) {
        credit = 0xff;
    } else {
        credit = (U8)intCredit;
    }

    if (credit > dev->credit) {
        /* Subtract credit already outstanding. */
        credit -= dev->credit;
        SPPOS_UnlockDev(dev->osDev);

        status = RF_AdvanceCredit(&dev->channel, credit);

        SPPOS_LockDev(dev->osDev);
        if (status == BT_STATUS_SUCCESS) {
            dev->credit += credit;
        }

        dev->lModemStatus.signals &= ~RF_FLOW;
    }
}


/*---------------------------------------------------------------------------
 * setHighWaterMark() 
 *      
 *     
 * Parameters:
 *      dev - pointer to SppDev
 */
static void setHighWaterMark(SppDev *dev)
{
    if (RF_CreditFlowEnabled(&dev->channel) == TRUE) {
        /* Use entire buffer. */
        dev->highWater = SPPOS_RxFree(dev->osDev);
    } else {
        /* Inefficient flow control. Set mark at 50% */
        dev->highWater = SPPOS_RxFree(dev->osDev) / 2;
        /* Must have at least one packet's space. */
        Assert(dev->highWater >= RF_FrameSize(&dev->channel));
    }
}


/*---------------------------------------------------------------------------
 * closeDevice() 
 *      Cleanup when closing a device.
 *
 * Requires:
 *     
 * Parameters:
 *      dev     pointer to SppDev that is being closed
 * Returns:
 */
static void closeDevice(SppDev *dev)
{
    SPPOS_FlushRx(dev->osDev);
    SPPOS_FlushTx(dev->osDev);
    dev->state = DEVICE_STATE_CLOSED;
}

/*---------------------------------------------------------------------------
 * insertRfCommChannel() 
 *      Insert an RFCOMM channel number into an SDP attribute list.
 *      Given and SDP record, this function finds the protocol descriptor
 *      list. It inserts the channel number into the last byte of this list.
 *
 * Requires:
 *     
 * Parameters:
 *
 * Returns:
 */
static void insertRfCommChannel(SdpRecord *rec, U8 channel)
{
    S16 i;
    SdpAttribute *attrib;

    attrib = rec->attribs;

    /* Search the attribute list */
    for (i = 0; i < rec->num; i++) {
        Report(("id=%04x, len=%d\n", attrib[i].id, attrib[i].len));
        if (attrib[i].id == AID_PROTOCOL_DESC_LIST) {
            /* Found the Protocol Descriptor List */
            Assert(SDP_GetU16(attrib[i].value + attrib[i].len - 4) == PROT_RFCOMM);
            *(U8 *)(attrib[i].value + attrib[i].len - 1) = channel;
            break;
        }
    }
}
