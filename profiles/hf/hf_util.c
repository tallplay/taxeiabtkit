/****************************************************************************
 *
 * File:
 *     $Workfile:hf_util.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:14$
 *
 * Description: This file contains utility functions for the Hands-free SDK.
 *             
 * Created:     February 11, 2005
 *
 * Copyright 2000-2005 Extended Systems, Inc.

 * Portions copyright 2005 iAnywhere Solutions, Inc.

 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any means, 
 * or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions,  
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "sys/hfi.h"
#include "sys/hf_cmd.h"
#include "sys/hfalloc.h"
#include "conmgr.h"

#if HF_USE_CALL_MANAGER == XA_ENABLED
#include "sys/hf_cmgr.h"
#endif
      
/*---------------------------------------------------------------------------
 *            HF_Init()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initialize HF SDK.  Initialize state and other variables.
 *
 * Return:    TRUE - Memory successfully initialized.
 */
BOOL HF_Init(void)
{
    U16 i;

    /* Initialize context memory */
    if (!(HfAlloc())) {
        return FALSE;
    }

    /* Load the state machines' function tables */
    HfInitStateMachine();

    /* Initialize lists */
    InitializeListHead(&HF(channelList));
    InitializeListHead(&HF(freeRfChannelList));
    for (i = 0; i < NUM_BT_DEVICES * 2 + 2; i++) {
        HF(rfChannels)[i].userContext = 0;
        InsertTailList(&HF(freeRfChannelList), &HF(rfChannels)[i].node);
    }

    if (AT_Init(&HF(atContext)) != AT_STATUS_OK) {
        return FALSE;
    }

    return TRUE;
}

/*---------------------------------------------------------------------------
 *            HfParseRfcommData()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Parses received RFCOMM data and passes it to the AT parser.
 *
 * Return:    BT_STATUS_PENDING - More data to parse.
 *            BT_STATUS_SUCCESS - Done parsing data.
 *            BT_STATUS_FAILED  - Receive buffer overflow.
 */
BtStatus HfParseRfcommData(HfChannel *channel, 
                            RfCallbackParms *parms, 
                            U16 *offset, 
                            AtResults *atParms)
{
    BtStatus    status = BT_STATUS_FAILED;
    U16         i;

    for (i = *offset; i < parms->dataLen; i++) {

        (*offset)++;
        if (channel->atRxLen >= HF_RECV_BUFFER_SIZE) {
            status = BT_STATUS_FAILED;
            break;
        }

        if ((parms->ptrs.data[i] != '\r') && (parms->ptrs.data[i] != '\n')) {
            channel->atRxBuffer[channel->atRxLen++] = parms->ptrs.data[i];
            status = BT_STATUS_PENDING;
        } else if (channel->atRxLen > 0) {
            /* At the end of the command */
            channel->atRxBuffer[channel->atRxLen] = 0;
            AtParseResultCode(channel, channel->atRxBuffer, 
                              channel->atRxLen, atParms);
            channel->atRxLen = 0;
            status = BT_STATUS_SUCCESS;
            break;
        }
    }

    return status;
}

/*---------------------------------------------------------------------------
 *            HfAppCallback()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Sets up the callback parameters and calls the application.
 *
 * Return:    void
 */
void HfAppCallback(HfChannel *Channel, HfEvent Event, 
                    BtStatus Status, void *data)
{
    HfCallbackParms hfParms;

    hfParms.event   = Event;
    hfParms.p.ptr   = data;
    hfParms.status  = Status;
    hfParms.errCode = Channel->cmgrHandler.errCode;

    Channel->callback(Channel, &hfParms);
}

#if HF_USE_PHONEBOOK_COMMANDS == XA_ENABLED
/*---------------------------------------------------------------------------
 *            HfPhonebookCallback()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handles Phonebook response codes and calls the application.
 *
 * Return:    void
 */
void HfPhonebookCallback(HfChannel *Channel, AtResults *Parms)
{
    HfPhonebookInfo  pbInfo;
    HfPhonebookSize  pbSize;
    HfPhonebookEntry pbEntry;

    switch (Parms->type) {
    case (AT_SELECT_PHONEBOOK_STORAGE | AT_TEST):
        /* Handle phonebook storage test */
        HfAppCallback(Channel, HF_EVENT_PHONEBOOK_STORAGE,
                      BT_STATUS_SUCCESS, 
                      (void *)(U32)Parms->p.pb.storage_test.supported);
        break;
    case (AT_SELECT_PHONEBOOK_STORAGE | AT_READ):
        /* Handle phonebook storage read */
        pbInfo.selected = Parms->p.pb.storage_read.selected;
        pbInfo.used = Parms->p.pb.storage_read.used;
        pbInfo.total = Parms->p.pb.storage_read.total;
        HfAppCallback(Channel, HF_EVENT_PHONEBOOK_INFO,
                      BT_STATUS_SUCCESS, 
                      (void *)(U32)&pbInfo);
        break;
    case (AT_READ_PHONEBOOK_ENTRY | AT_TEST):
        /* Handle phonebook storage read entries test */
        AT_Range2MinMax(Parms->p.pb.read_test.range, 
                        &pbSize.index1,
                        &pbSize.index2);

        pbSize.numberLen = Parms->p.pb.read_test.numLength;
        pbSize.textLen = Parms->p.pb.read_test.textLength;

        HfAppCallback(Channel, HF_EVENT_PHONEBOOK_SIZE,
                      BT_STATUS_SUCCESS, 
                      (void *)(U32)&pbSize);
        break;
    case AT_READ_PHONEBOOK_ENTRY:
    case AT_FIND_PHONEBOOK_ENTRY:
        /* Handle phonebook entries */
        pbEntry.index = Parms->p.pb.read.index;
        pbEntry.number = Parms->p.pb.read.number;
        pbEntry.text = Parms->p.pb.read.text;
        pbEntry.type = Parms->p.pb.read.type;
        HfAppCallback(Channel, HF_EVENT_PHONEBOOK_ENTRY,
                      BT_STATUS_SUCCESS, (void *)(U32)&pbEntry);
        break;
    }
}
#endif

/*---------------------------------------------------------------------------
 *            HfIndicatorCallback()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handles read indicators response code and calls the application.
 *
 * Return:    void
 */
void HfIndicatorCallback(HfChannel *Channel, AtResults *Parms)
{
    I8 i;
    U8 event = 0xFF;
    U16 indicator;

    /* Handle signal strength inidicator */
    for (i = 0; i < HF(numInd); i++) {

        /* Set the indicator value */
        indicator = HF(indMap)[i];

        switch (indicator) {
        case AT_IND_SETUP:
        case AT_IND_SETUP2:
            indicator = AT_IND_POLL_SETUP;
            break;
        case AT_IND_CALL:
            indicator = AT_IND_POLL_CALL;
            break;
        case AT_IND_CALLHELD:
            indicator = AT_IND_POLL_CALLHELD;
        }

        /* Set up the event value */
        if (indicator != 0xFF) {
            /* This indicator is valid */
            switch (indicator) {
            case AT_IND_SIGNAL:
                event = HF_EVENT_SIGNAL_IND;
                break;
            case AT_IND_SERVICE:
                event = HF_EVENT_SERVICE_IND;
                break;
            case AT_IND_ROAM:
                event = HF_EVENT_ROAM_IND;
                break;
#if HF_USE_MESSAGING_COMMANDS
            case AT_IND_SMS:
                event = HF_EVENT_SMS_IND;
                break;
#endif
            case AT_IND_BATTCHG:
                event = HF_EVENT_BATTERY_IND;
                break;

#if HF_USE_CALL_MANAGER == XA_ENABLED
            case AT_IND_POLL_CALL:
            case AT_IND_POLL_SETUP:
            case AT_IND_POLL_CALLHELD:
                    
                /* Call the call manager */
                CallManager(Channel, 
                            EG_AT_INTERNAL_POLL,
                            indicator, 
                            (U32)Parms->p.hf.indicators_read.ind[i].value);
                break;
#else
            case AT_IND_POLL_CALL:
                event = HF_EVENT_CALL_IND;
                break;
            case AT_IND_POLL_SETUP:
                event = HF_EVENT_CALLSETUP_IND;
                break;
            case AT_IND_POLL_CALLHELD:
                event = HF_EVENT_CALLHELD_IND;
                break;
#endif
            }
        }

        /* Make a callback for events not handled by Call Manager */
        if (event != 0xFF) {
            HfAppCallback(Channel, event,  BT_STATUS_SUCCESS, 
                   (void *)(U32)Parms->p.hf.indicators_read.ind[i].value);
            event = 0xFF;
        }
    }
}


/*---------------------------------------------------------------------------
 *            HfEventCallback()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handles Indicator Event response codes and calls the application.
 *
 * Return:    void
 */
void HfEventCallback(HfChannel *Channel, AtResults *Parms)
{
    U16 event = 0xFF;
    U16 indicator;

    /* Set up the indicator value */
    indicator = HF(indMap)[Parms->p.hf.event.indicator - 1];
    if (indicator == AT_IND_SETUP2) {
        indicator = AT_IND_SETUP;
    }

    /* Set up the event value */
    switch (indicator) {
    case AT_IND_SIGNAL:
        event = HF_EVENT_SIGNAL_IND;
        break;
    case AT_IND_SERVICE:
        event = HF_EVENT_SERVICE_IND;
        break;
    case AT_IND_ROAM:
        event = HF_EVENT_ROAM_IND;
        break;
    case AT_IND_SMS:
        event = AT_IND_SMS;
        break;
    case AT_IND_BATTCHG:
        event = HF_EVENT_BATTERY_IND;
        break;

#if HF_USE_CALL_MANAGER == XA_ENABLED
    case AT_IND_CALL:
    case AT_IND_SETUP:
    case AT_IND_CALLHELD:
        CallManager(Channel, 
                    EG_AT_INTERNAL, 
                    indicator, 
                    (U32)Parms->p.hf.event.value);
        break;
#else
    case AT_IND_CALL:
        event = HF_EVENT_CALL_IND;
        break;
    case AT_IND_SETUP:
        event = HF_EVENT_CALLSETUP_IND;
        break;
    case AT_IND_CALLHELD:
        event = HF_EVENT_CALLHELD_IND;
        break;
#endif
    }

    /* Make a callback for events not handled by Call Manager */
    if (event != 0xFF) {
        HfAppCallback(Channel, event,  BT_STATUS_SUCCESS, 
               (void *)(U32)Parms->p.hf.event.value);
    }
}


/*---------------------------------------------------------------------------
 *            HfCloseChannel()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Clean up the channel state and notify app
 *
 * Return:    void
 */
void HfCloseChannel(HfChannel *Channel)
{
    HfCallbackParms  hfParms;

    if (Channel->state != HF_STATE_CLOSED) {
        /* Clean up the state */
        Channel->flags = 0;
        Channel->queryFlags = 0;

        Channel->state = HF_STATE_CLOSED;

#if HF_USE_CALL_MANAGER == XA_ENABLED
        Channel->pollFlags = 0;
        if (Channel->pollTimer.context) {
            EVM_CancelTimer(&Channel->pollTimer);
        }
#endif

        if (Channel->atTimer.context) {
            EVM_CancelTimer(&Channel->atTimer);
        }

        if (Channel->ringTimer.context) {
            EVM_CancelTimer(&Channel->ringTimer);
        }

        /* Return outstanding commands */
        HfFlushCommands(Channel, BT_STATUS_NO_CONNECTION);

        /* Release link handler */
        if (Channel->linkFlags & HF_LINK_ACL) {
            if (CMGR_RemoveDataLink(&Channel->cmgrHandler) == BT_STATUS_SUCCESS) {
                /* Clear the link flags */
                Channel->linkFlags = 0;
            }
        }

#if HF_SNIFF_TIMER >= 0
        /* No sniff timer needed any more */
        (void)CMGR_ClearSniffTimer(&Channel->cmgrHandler);
#endif

        /* Notify the application */
        hfParms.event = HF_EVENT_SERVICE_DISCONNECTED;
        hfParms.status = BT_STATUS_SUCCESS;
        hfParms.errCode = Channel->cmgrHandler.errCode;
        hfParms.p.remDev = CMGR_GetRemoteDevice(&Channel->cmgrHandler);
        Channel->cmgrHandler.bdc = 0;
        Channel->callback(Channel, &hfParms);
    }
}

/*---------------------------------------------------------------------------
 *            HfFindChannel()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Find the Hf channel based on the remote device.
 *
 * Return:    A pointer to the hf channel, NULL if not found.
 */
HfChannel * HfFindChannel(BtRemoteDevice *remDev)
{
    HfChannel *hfChannel;

    hfChannel = (HfChannel *)GetHeadList(&HF(channelList));
    while (&hfChannel->node != &HF(channelList)) {
        if ((hfChannel->cmgrHandler.bdc) && 
            (CMGR_GetRemoteDevice(&hfChannel->cmgrHandler) == remDev)) {
            break;
        }

        /* Look at the next node */
        hfChannel = (HfChannel *)GetNextNode(&hfChannel->node);
    }

    if (&hfChannel->node == &HF(channelList)) {
        /* Not found */
        hfChannel = 0;
    }

    return hfChannel;
}

/*---------------------------------------------------------------------------
 *            HfGetClosedChannel()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Finds an available (closed) channel.
 *
 * Return:    A pointer to the hf channel, NULL if not found.
 */
HfChannel * HfGetClosedChannel(void)
{
    HfChannel *hfChannel;

    hfChannel = (HfChannel *)GetHeadList(&HF(channelList));
    while (&hfChannel->node != &HF(channelList)) {
        if (hfChannel->state == HF_STATE_CLOSED) {
            break;
        }

        /* Look at the next node */
        hfChannel = (HfChannel *)GetNextNode(&hfChannel->node);
    }

    if (&hfChannel->node == &HF(channelList)) {
        /* Not found */
        hfChannel = 0;
    }

    return hfChannel;
}

/*---------------------------------------------------------------------------
 *            HfAllocRfChannel()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Allocate an RF channel (make it "in use").
 *
 * Return:    A pointer to the RF channel, NULL if not found.
 */
RfChannel *HfAllocRfChannel(void)
{
    RfChannel *rfChannel = 0;

    if (!IsListEmpty(&HF(freeRfChannelList))) {
        rfChannel = (RfChannel *)RemoveHeadList(&HF(freeRfChannelList)); 
    }

    if (rfChannel) {
        rfChannel->callback = HfRfCallback;
        rfChannel->maxFrameSize = RF_MAX_FRAME_SIZE;
        rfChannel->priority = RF_DEFAULT_PRIORITY;
    }

    return rfChannel;
}

/*---------------------------------------------------------------------------
 *            HfFreeRfChannel()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Frees an RF channel (makes it available).
 *
 * Return:    void
 */
void HfFreeRfChannel(RfChannel *rfChannel)
{
    if (!IsNodeOnList(&HF(freeRfChannelList), &rfChannel->node)) {
        InsertTailList(&HF(freeRfChannelList), &rfChannel->node);
    }
}

