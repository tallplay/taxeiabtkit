/****************************************************************************
 *
 * File:
 *     $Workfile:hf_sdp.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:10$
 *
 * Description: This file contains the SDP code for the Hands-free SDK.
 *             
 * Created:     February 11, 2005
 *
 * Copyright 2000-2005 Extended Systems, Inc.

 * Portions copyright 2005 iAnywhere Solutions, Inc.

 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any means, 
 * or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions,  
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "sys/hfi.h"
#include "sys/hfalloc.h"

#if SDP_PARSING_FUNCS == XA_DISABLED
#error The Hands-free SDK requires the SDP parsing functions.
#endif /* SDP_PARSING_FUNCS == XA_DISABLED */

/****************************************************************************
 *
 * ROMable data
 *
 ****************************************************************************/

/****************************************************************************
 * Hands-Free SDP Entries
 ****************************************************************************/

/*---------------------------------------------------------------------------
 *
 * ServiceClassIDList 
 */
static const U8 HfClassId[] = {
    SDP_ATTRIB_HEADER_8BIT(6),        /* Data Element Sequence, 6 bytes */
    SDP_UUID_16BIT(SC_HANDSFREE),     /* Hands-Free UUID in Big Endian */
    SDP_UUID_16BIT(SC_GENERIC_AUDIO)  /* Generic Audio UUID in Big Endian */
};

/*---------------------------------------------------------------------------
 * SDP Protocol Descriptor List object registered by Hands-Free.
 * 
 * Value of the protocol descriptor list for the Hands-Free profile.
 * This structure is a ROM'able representation of the RAM structure.
 * This structure is copied into a RAM structure used to register the 
 * service. A RAM structure must be used to be able to dynamically set the 
 * RFCOMM channel in the RFCOMM protocol descriptor.
 */
static const U8 HfProtoDescList[] = {
    SDP_ATTRIB_HEADER_8BIT(12),  /* Data element sequence, 12 bytes */

    /* Each element of the list is a Protocol descriptor which is a
     * data element sequence. The first element is L2CAP which only
     * has a UUID element.
     */
    SDP_ATTRIB_HEADER_8BIT(3),   /* Data element sequence for L2CAP, 3 bytes */     
    SDP_UUID_16BIT(PROT_L2CAP),  /* Uuid16 L2CAP */

    /* Next protocol descriptor in the list is RFCOMM. It contains two
     * elements which are the UUID and the channel. Ultimately this
     * channel will need to filled in with value returned by RFCOMM.
     */
    SDP_ATTRIB_HEADER_8BIT(5),   /* Data element sequence for RFCOMM, 5 bytes */
    SDP_UUID_16BIT(PROT_RFCOMM), /* Uuid16 RFCOMM */
    SDP_UINT_8BIT(99)            /* Uint8 RFCOMM channel number - value can vary */
};


/*
 * BluetoothProfileDescriptorList
 */
static const U8 HfProfileDescList[] = {
    SDP_ATTRIB_HEADER_8BIT(8),            /* Data element sequence, 8 bytes */
    SDP_ATTRIB_HEADER_8BIT(6),            /* Data element sequence for ProfileDescriptor, 6 bytes */
    SDP_UUID_16BIT(SC_HANDSFREE),         /* Uuid16 Hands-Free */
    SDP_UINT_16BIT(HF_GW_VERSION_1_5)     /* Uint16 version number */
};

/*
 * * OPTIONAL * Language BaseId List (goes with the service name).
 */
static const U8 HfLangBaseIdList[] = {
    SDP_ATTRIB_HEADER_8BIT(9),  /* Data Element Sequence, 9 bytes */
    SDP_UINT_16BIT(0x656E),     /* English Language */
    SDP_UINT_16BIT(0x006A),     /* UTF-8 encoding */
    SDP_UINT_16BIT(0x0100)      /* Primary language base Id */
};

/*
 * * OPTIONAL *  ServiceName
 */
static const U8 HfServiceName[] = {
    SDP_TEXT_8BIT(16),          /* Null terminated text string */
    'H', 'a', 'n', 'd', 's', '-', 'F', 'r', 'e', 'e', ' ', 
    'u', 'n', 'i', 't', '\0'
};

/*
 * * OPTIONAL *  Supported Features
 */
static const U8 HfSupportedFeatures[] = {
    SDP_UINT_16BIT(0x0000)     /* Features are set when registering service */
};


/*---------------------------------------------------------------------------
 * Hands-Free Public Browse Group.
 */
static const U8 HfBrowseGroup[] = {
    SDP_ATTRIB_HEADER_8BIT(3),               /* 3 bytes */
    SDP_UUID_16BIT(SC_PUBLIC_BROWSE_GROUP),  /* Public Browse Group */
};

/* Hands-Free attributes.
 *
 * This is a ROM template for the RAM structure used to register the
 * Hands-Free SDP record.
 */
static const SdpAttribute HfSdpAttributes[] = {
    /* Hands-Free audio gateway class ID List attribute */
    SDP_ATTRIBUTE(AID_SERVICE_CLASS_ID_LIST, HfClassId), 
    /* Audio Gateway protocol descriptor list attribute */
    SDP_ATTRIBUTE(AID_PROTOCOL_DESC_LIST, HfProtoDescList),
    /* Public Browse Group Service */
    SDP_ATTRIBUTE(AID_BROWSE_GROUP_LIST, HfBrowseGroup), 
    /* Language base id (Optional: Used with service name) */
    SDP_ATTRIBUTE(AID_LANG_BASE_ID_LIST, HfLangBaseIdList),
    /* Hands-Free Bluetooth profile descriptor list */
    SDP_ATTRIBUTE(AID_BT_PROFILE_DESC_LIST, HfProfileDescList),
    /* Hands-Free service name*/
    SDP_ATTRIBUTE((AID_SERVICE_NAME + 0x0100), HfServiceName),
    SDP_ATTRIBUTE(AID_SUPPORTED_FEATURES, HfSupportedFeatures)
};

/*-------------------------------------------------------------------------
 *
 * SDP query info
 */
static const U8 HfServiceSearchAttribReq[] = {
    /* First parameter is the search pattern in data element format. It
     * is a list of 3 UUIDs. 
     */
    SDP_ATTRIB_HEADER_8BIT(9),             /* Data Element Sequence, 9 bytes */ 

    SDP_UUID_16BIT(SC_HANDSFREE_AUDIO_GATEWAY), /* UUID for Hands-Free AG in Big Endian */
    SDP_UUID_16BIT(PROT_L2CAP),            /* L2CAP UUID in Big Endian */
    SDP_UUID_16BIT(PROT_RFCOMM),           /* UUID for RFCOMM in Big Endian */

    /* The second parameter is the maximum number of bytes that can be
     * be received for the attribute list
     */
    0x00, 0x64,                 /* Max number of bytes for attribute is 100 */

    SDP_ATTRIB_HEADER_8BIT(12),
    SDP_UINT_16BIT(AID_PROTOCOL_DESC_LIST), 
    SDP_UINT_16BIT(AID_BT_PROFILE_DESC_LIST),
    SDP_UINT_16BIT(AID_EXTERNAL_NETWORK),
    SDP_UINT_16BIT(AID_SUPPORTED_FEATURES)
};

/* Interal function prototypes */
static void HfSdpEventHandler(const BtEvent *Event);

/****************************************************************************
 *
 * Functions
 *
 ****************************************************************************/

/*---------------------------------------------------------------------------
 *            HfRegisterSdpServices()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Registers the SDP services.
 *
 * Return:    See SDP_AddRecord().
 */
BtStatus HfRegisterSdpServices(HfChannel *Channel)
{
    BtStatus status;

    /* RFCOMM Channel */
    Assert(sizeof(HF(hfSdpProtoDescList)) == sizeof(HfProtoDescList));
    OS_MemCopy((U8 *)HF(hfSdpProtoDescList), (U8 *)HfProtoDescList,
               sizeof(HfProtoDescList));
    HF(hfSdpProtoDescList)[13] = HF(hfService).serviceId;

    /* Supported Features */
    Assert(sizeof(HF(hfSdpSupportedFeatures)) == sizeof(HfSupportedFeatures));
    OS_MemCopy((U8 *)HF(hfSdpSupportedFeatures), (U8 *)HfSupportedFeatures,
               sizeof(HfSupportedFeatures));
    StoreBE16(HF(hfSdpSupportedFeatures)+1, (HF_SDK_FEATURES & 0x1F));

    /* Register Hands-free SDP Attributes */
    Assert(sizeof(HF(hfSdpAttribute)) == sizeof(HfSdpAttributes));
    OS_MemCopy((U8 *)HF(hfSdpAttribute), (U8 *)HfSdpAttributes,
               sizeof(HfSdpAttributes));

    HF(hfSdpAttribute[1]).value = HF(hfSdpProtoDescList);
    HF(hfSdpAttribute[6]).value = HF(hfSdpSupportedFeatures);

    HF(hfSdpRecord).attribs = HF(hfSdpAttribute);
    HF(hfSdpRecord).num = 7;
    HF(hfSdpRecord).classOfDevice = COD_AUDIO | COD_MAJOR_AUDIO | 
                                     COD_MINOR_AUDIO_HANDSFREE;

    status = SDP_AddRecord(&HF(hfSdpRecord));

    return status;
}

/*---------------------------------------------------------------------------
 *            HfDeregisterSdpServices()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Deregisters the SDP services.
 *
 * Return:    See SDP_RemoveRecord().
 */
BtStatus HfDeregisterSdpServices(void)
{
    BtStatus status;

    /* Remove the Hands-free entry */
    status = SDP_RemoveRecord(&HF(hfSdpRecord));
    
    return status;
}

/*---------------------------------------------------------------------------
 *            HfStartServiceQuery()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initiate the SDP service query.
 *
 * Return:    See SDP_Query().
 */
BtStatus HfStartServiceQuery(HfChannel *Channel, SdpQueryMode mode)
{
    BtStatus status;

    /* Search for Hands-free profile */
    Channel->sdpQueryToken.parms = HfServiceSearchAttribReq;
    Channel->sdpQueryToken.plen = sizeof(HfServiceSearchAttribReq);
    Channel->sdpQueryToken.type = BSQT_SERVICE_SEARCH_ATTRIB_REQ;
    Channel->sdpQueryToken.callback = HfSdpEventHandler;
    Channel->sdpQueryToken.rm = Channel->cmgrHandler.bdc->link;
    status = SDP_Query(&Channel->sdpQueryToken, mode);
    return status;
} 

/*---------------------------------------------------------------------------
 *            HfVerifySdpQueryRsp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Parses the SDP response for the RFCOMM channel,
 *            Profile Descriptor List, External Network and Supported 
 *            Features.  In the case of a continuation state, additional
 *            SDP queries will be issued until all of the attributes have
 *            been found or until the entire SDP response has been parsed.
 *            
 *
 * Return:    BT_STATUS_SUCCESS if SDP parsing is successful.
 *            BT_STATUS_FAILED if SDP parsing encountered an error.
 *            BT_STATUS_SDP_CONT_STATE if SDP parsing encountered a 
 *            continuation state that requires an additional SDP query.
 */
BtStatus HfVerifySdpQueryRsp(HfChannel *Channel, SdpQueryToken *token)
{
    BtStatus        status = BT_STATUS_FAILED;

    if (!(Channel->queryFlags & SDP_QUERY_FLAG_PROTOCOL)) {

        /* Get the RFCOMM channel */
        token->attribId = AID_PROTOCOL_DESC_LIST;
        token->uuid = PROT_RFCOMM;

        if ((status = SDP_ParseAttributes(token)) == BT_STATUS_SUCCESS) {
            /* We had better have a U8 channel value, if not, look for any other channels */
            if ((token->totalValueLen == 1) && (token->availValueLen == 1)) {
                Report(("HF: SDP_ParseAttributes succeeded! RFCOMM channel = %d,"
                        "Updated Buff Len = %d\n", token->valueBuff[0], 
                        token->remainLen));
                /* Assign the RFCOMM channel number */
                Channel->rfServerChannel = token->valueBuff[0];
                token->mode = BSPM_RESUME;
                Channel->queryFlags |= SDP_QUERY_FLAG_PROTOCOL;
            }
        }

        /* Handle continuation state */
        if (status == BT_STATUS_SDP_CONT_STATE) goto done;
    }

    if (!(Channel->queryFlags & SDP_QUERY_FLAG_PROFILE)) {

        /* Get the BT Profile Version */
        token->attribId = AID_BT_PROFILE_DESC_LIST;
        token->uuid = SC_HANDSFREE;
        status = SDP_ParseAttributes(token);
        if (status == BT_STATUS_FAILED) {
            token->uuid = SC_HANDSFREE_AUDIO_GATEWAY;
            status = SDP_ParseAttributes(token);
        }

        if (status == BT_STATUS_SUCCESS) {
            if ((token->totalValueLen == 2) && 
                ((token->availValueLen == 2) || (token->availValueLen == 1))) {

                /* Assign the Profile UUID and Profile Version */
                if (token->availValueLen == 1) {
                    Channel->version += token->valueBuff[0];
                } else {
                    Channel->version = SDP_GetU16(token->valueBuff);
                }

                token->mode = BSPM_RESUME;
                Channel->queryFlags |= SDP_QUERY_FLAG_PROFILE;
            }
        }


        if ((status == BT_STATUS_SDP_CONT_STATE) && (token->availValueLen == 1)) {
            /* It's possible that only a portion of the data might occur.
             * If so, we'll copy what we have.  The data arrives in big endian format.
             */
            Channel->version = ((U16)token->valueBuff[0]) << 8;
        }

        /* Handle continuation state */
        if (status == BT_STATUS_SDP_CONT_STATE) goto done;
    }

    if (!(Channel->queryFlags & SDP_QUERY_FLAG_NETWORK)) {

        /* Get the External Network settings */
        token->attribId = AID_EXTERNAL_NETWORK;
        token->uuid = 0;
        if ((status = SDP_ParseAttributes(token)) == BT_STATUS_SUCCESS) {
            if ((token->totalValueLen == 1) && (token->availValueLen == 1)) {

                if (Channel->version == HF_GW_VERSION_1_0) {
                    /* Assign the reject feature */
                    Channel->gwFeatures |= token->valueBuff[0] == 1 ? 
                                           HF_GW_FEATURE_CALL_REJECT : 0;
                    Channel->queryFlags |= SDP_QUERY_FLAG_NETWORK;
                    token->mode = BSPM_RESUME;
                }
            }
        }

        /* Handle continuation state */
        if (status == BT_STATUS_SDP_CONT_STATE) goto done;
    }

    if (!(Channel->queryFlags & SDP_QUERY_FLAG_FEATURES)) {

        /* Get the Supported Features */
        token->attribId = AID_SUPPORTED_FEATURES;
        token->uuid = 0;
        if ((status = SDP_ParseAttributes(token)) == BT_STATUS_SUCCESS) {
            if ((token->totalValueLen == 2) && 
                ((token->availValueLen == 2) || (token->availValueLen == 1))) {

                /* Assign the Supported Features */
                if (token->availValueLen == 1) {
                    Channel->gwFeatures += token->valueBuff[0];
                } else {
                    Channel->gwFeatures = SDP_GetU16(token->valueBuff);
                }

          Channel->queryFlags |= SDP_QUERY_FLAG_FEATURES;

          if (!(Channel->flags & CHANNEL_FLAG_OUTGOING)) {

                    /* Report features to the app */
              if (Channel->gwFeatures & HF_GW_FEATURE_IN_BAND_RING) {
                        Channel->ibRing = TRUE;
              }

              if (Channel->gwFeatures & HF_GW_FEATURE_ECHO_NOISE) {
                        Channel->nrec = TRUE;
              }

              HfAppCallback(Channel, HF_EVENT_GATEWAY_FEATURES, BT_STATUS_SUCCESS, 
                      (void *)(U32)Channel->gwFeatures);
          }
            }
        }

        if ((status == BT_STATUS_SDP_CONT_STATE) && (token->availValueLen == 1)) {
            /* It's possible that only a portion of the data might occur.
             * If so, we'll copy what we have.  The data arrives in big endian format.
             */
            Channel->gwFeatures = ((U16)token->valueBuff[0]) << 8;
        } else if (status != BT_STATUS_SUCCESS) {
            /* No features found, use defaults */
            HfAppCallback(Channel, HF_EVENT_GATEWAY_FEATURES, BT_STATUS_SUCCESS, 
                          (void *)(U32)Channel->gwFeatures);
        }
    }

done:
    if (status == BT_STATUS_SDP_CONT_STATE) {
        Report(("HF: SDP_ParseAttributes - Continuation State. Query Again!\n"));
        token->mode = BSPM_CONT_STATE;
        return status;
    }

    /* Reset the attribute to parse */
    token->attribId = AID_PROTOCOL_DESC_LIST;
    /* Reset the SDP parsing mode */
    token->mode = BSPM_BEGINNING;

    /* Return the status of the RFCOMM channel parsing, since this
     * is the only critical information to obtain a connection.
     */
    if (Channel->queryFlags & SDP_QUERY_FLAG_PROTOCOL)
        return BT_STATUS_SUCCESS;
    else return BT_STATUS_FAILED;
}

/*---------------------------------------------------------------------------
 *            HfSdpEventHandler()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handles all SDP events
 */
static void HfSdpEventHandler(const BtEvent *Event)
{
    BtStatus        status;
    HfChannel     *channel = ContainingRecord(Event->p.token, 
                                               HfChannel, sdpQueryToken);

    switch (Event->eType) {
    case SDEVENT_QUERY_RSP:
        status = HfVerifySdpQueryRsp(channel, Event->p.token);
        if (status == BT_STATUS_SUCCESS) {
            /* SDP result parsing completed */
            if (channel->flags & CHANNEL_FLAG_OUTGOING) {

                /* Connect to hands-free */
                channel->hfChannel = HfAllocRfChannel();
                if (channel->hfChannel) {
                    status = RF_OpenClientChannel(channel->cmgrHandler.bdc->link,
                                                  channel->rfServerChannel,
                                                  channel->hfChannel, 1);
                } else {
                    status = BT_STATUS_NO_RESOURCES;
                }

                if (status == BT_STATUS_PENDING) {
                    channel->state = HF_STATE_CONN_PENDING;
                } else {
                    Report(("HF: Could not open RFCOMM channel\n"));
                    HfCloseChannel(channel);
                }
            }
            break;
        } else if (status == BT_STATUS_SDP_CONT_STATE) {
            /* We are going to query again for the rest of the response */
            status = HfStartServiceQuery(channel, BSQM_CONTINUE);
            if ((status == BT_STATUS_PENDING) || 
                !(channel->flags & CHANNEL_FLAG_OUTGOING)) {
            } else {
                Report(("HF: Could not restart query\n"));
                HfCloseChannel(channel);
            }
            break;
        } else {
            /* Query failed */
            Report(("HF: SDP Query failed\n"));
            HfCloseChannel(channel);
        }
        break;
    case SDEVENT_QUERY_ERR:
    case SDEVENT_QUERY_FAILED:
        if (channel->flags & CHANNEL_FLAG_OUTGOING) {

            /* Query failed */
            Report(("HF: SDP Query failed\n"));
            HfCloseChannel(channel);
        }
        break;
    }
}

