/****************************************************************************
 *
 * File:
 *     $Workfile:hfg.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:18$
 *
 * Description: This file contains API functions for the Hands-free Audio
 *              Gateway SDK.
 *             
 * Created:     February 11, 2005
 *
 * Copyright 2000-2005 Extended Systems, Inc.

 * Portions copyright 2005 iAnywhere Solutions, Inc.

 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any means, 
 * or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions,  
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "sys/hfgalloc.h"
#include "conmgr.h"
#include "btalloc.h"

/*---------------------------------------------------------------------------
 *            HfgServiceConnectionCallback()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Called by device manager with link state events.
 *
 * Return:    (See header file)
 *
 */
void HfgServiceConnectionCallback(CmgrHandler *Handler, CmgrEvent Event,
                                  BtStatus Status) 
{
    HfgChannel *channel = ContainingRecord(Handler, HfgChannel, cmgrHandler);

    switch (Event) {
    case CMEVENT_DATA_LINK_CON_CNF:

        if (Status == BT_STATUS_SUCCESS) {

            /* Register a SCO handler */
            Status = CMGR_RegisterScoHandler(&channel->cmgrHandler);
            if (Status == BT_STATUS_SUCCESS) {
                /* Start the service search */
                channel->linkFlags |= HFG_LINK_ACL;
                channel->sdpQueryToken.attribId = AID_PROTOCOL_DESC_LIST;
                if ((Status = HfgStartServiceQuery(channel, BSQM_FIRST) == 
                     BT_STATUS_PENDING)) {
                    return;
                }
            } else {
                /* Could not register a SCO handler */
                AssertEval(RF_CloseChannel(channel->hfgChannel) == BT_STATUS_PENDING);
            }
        }

        /* Error, call the application */
        HfgCloseChannel(channel);
        break;

    case CMEVENT_DATA_LINK_DIS:
        HfgCloseChannel(channel);
        break;

    case CMEVENT_AUDIO_LINK_CON:
        HfgAppCallback(channel, HFG_EVENT_AUDIO_CONNECTED, Status, 
                       (U32)channel->cmgrHandler.bdc->link);
        break;

    case CMEVENT_AUDIO_LINK_DIS:
        if (channel->linkFlags & HFG_LINK_REMOVE_HF) {
            channel->linkFlags &= ~HFG_LINK_REMOVE_HF;
            AssertEval(RF_CloseChannel(channel->hfgChannel) == BT_STATUS_PENDING);
        }

        HfgAppCallback(channel, HFG_EVENT_AUDIO_DISCONNECTED, Status, 
                       (U32)channel->cmgrHandler.bdc->link);
        break;

#if BT_SCO_HCI_DATA == XA_ENABLED
    case CMEVENT_AUDIO_DATA:
        HfgAppCallback(channel, HFG_EVENT_AUDIO_DATA, Status, (U32)&Handler->audioData);
        break;

    case CMEVENT_AUDIO_DATA_SENT:
        HfgAppCallback(channel, HFG_EVENT_AUDIO_DATA_SENT, Status, (U32)&Handler->audioPacket);
        break;
#endif
    }
}

/*---------------------------------------------------------------------------
 *            HFG_Register()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initializes all necessary data structures, registers with
 *            RFCOMM and SDP. 
 *
 * Return:    (See header file)
 *
 */
BtStatus HFG_Register(HfgChannel *Channel, HfgCallback Callback)
{
    BtStatus   status = BT_STATUS_SUCCESS;
    RfChannel *rfChannel;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Channel != 0);

    OS_LockStack();

    /* Register this channel in the list */
    if (!IsNodeOnList(&HFG(channelList), &Channel->node)) {

        /* Clear the channel structure */
        OS_MemSet((U8*)Channel, 0, sizeof(HfgChannel));

        /* Register RFCOMM service */
        if (!HFG(regCount++)) {

            /* This is the fist registration with RFCOMM */
            HFG(hfgService).serviceId = 0;

            /* Register a channel to receive Handsfree connections */
            rfChannel = HfgAllocRfChannel();
            if (rfChannel) {
                status = RF_RegisterServerChannel(rfChannel, &HFG(hfgService), 1);
            } else {
                /* No channels left to register */
                status = BT_STATUS_NO_RESOURCES;
                goto error;
            }

            rfChannel->userContext = (void *)1;
            HFG(hfgChannel) = rfChannel;

            /* Register with SDP if this is the first channel */
            status = HfgRegisterSdpServices(Channel);

            if (status != BT_STATUS_SUCCESS) {
                /* Failed SDP registration, free the HF channels */
                RF_DeregisterServerChannel(rfChannel, &HFG(hfgService));
                goto error;
            }

#if BT_SECURITY == XA_ENABLED
            /* Also, register security records */
            HFG(hfgSecRec).id =  SEC_RFCOMM_ID;
            HFG(hfgSecRec).channel = HFG(hfgService).serviceId;
            HFG(hfgSecRec).level = HFG_SECURITY_SETTINGS;    
            status = SEC_Register(&HFG(hfgSecRec));
            if (status != BT_STATUS_SUCCESS) {
                /* Failed security registration, free the HF channels */
                (void)HfgDeregisterSdpServices();
                RF_DeregisterServerChannel(rfChannel, &HFG(hfgService));
                goto error;
            }
#endif

        }

        if (status == BT_STATUS_SUCCESS) {

            /* Register the HF Audio Gateway channel */
            Channel->callback = Callback;
            Channel->atTxPacket.data = Channel->atTxData;

            /* Initialize indicators */
            Channel->call = 0;
            Channel->callSetup = 0;
            Channel->held = 0;
            Channel->service = 0;
            Channel->signal = 0;
            Channel->battery = 0;

            /* Initialize default state */
            Channel->nrecDisable = FALSE;
            Channel->ibRing = TRUE;

            /* Insert the channel on the list */
            InsertTailList(&HFG(channelList), &Channel->node);
            InitializeListHead(&Channel->rspQueue);
            (void)CMGR_RegisterHandler(&Channel->cmgrHandler, HfgServiceConnectionCallback);
        }
    } else {
        /* Already registered */
        status = BT_STATUS_IN_USE;
        goto error;
    }

error:

    OS_UnlockStack();

    return status;
}


/*---------------------------------------------------------------------------
 *            HFG_Deregister()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Deregisters with RFCOMM and SDP. 
 *
 * Return:    (See header file)
 *
 */
BtStatus HFG_Deregister(HfgChannel *Channel)
{
    BtStatus   status = BT_STATUS_SUCCESS;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Channel != 0);

    OS_LockStack();

    if (IsNodeOnList(&HFG(channelList), &Channel->node)) {

        Assert(HFG(regCount));
        if (!(--HFG(regCount))) {

            /* Deregister with SDP */
            (void)HfgDeregisterSdpServices();

#if BT_SECURITY == XA_ENABLED
            /* Remove Security Record */
            SEC_Unregister(&HFG(hfgSecRec));
#endif

            /* Deregister the RFCOMM HF channel */
            status = RF_DeregisterServerChannel(HFG(hfgChannel),
                                                &HFG(hfgService));
            if (status == BT_STATUS_SUCCESS) {
                /* A HF channel was unregistered, free it */
                HfgFreeRfChannel(HFG(hfgChannel));
                HFG(hfgChannel)->userContext = 0;
            }

        }

        (void)CMGR_DeregisterHandler(&Channel->cmgrHandler);
        RemoveEntryList(&Channel->node);
    } else {
        /* Channel is not registered */
        status = BT_STATUS_NOT_FOUND;
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            HFG_CreateServiceLink()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Opens a service level connection. 
 *
 * Return:    (See header file)
 *
 */
BtStatus HFG_CreateServiceLink(HfgChannel *Channel, BD_ADDR *Addr)
{
    BtStatus status = BT_STATUS_NOT_FOUND;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Channel != 0);

    OS_LockStack();

    /* See if the channel is registered */
    if (IsNodeOnList(&HFG(channelList), &Channel->node)) {

        if (Channel->state == HFG_STATE_CLOSED) {

            /* Initialize channel structure */
            Channel->version = 0;
            Channel->hfFeatures = 0;

            /* Establish the ACL link */
            status = CMGR_CreateDataLink(&Channel->cmgrHandler, Addr);
            if (status == BT_STATUS_SUCCESS) {

                /* Register a SCO handler */
                status = CMGR_RegisterScoHandler(&Channel->cmgrHandler);
                if (status == BT_STATUS_SUCCESS) {

                    /* Start the service search */
                    Channel->linkFlags |= HFG_LINK_ACL;
                    Channel->sdpQueryToken.attribId = AID_PROTOCOL_DESC_LIST;
                    status = HfgStartServiceQuery(Channel, BSQM_FIRST);
                } else {
                    /* Could not register a SCO handler */
                    (void)CMGR_RemoveDataLink(&Channel->cmgrHandler);
                }
            }

            if (status == BT_STATUS_PENDING) {

                /* Connection was started */
                Channel->state = HFG_STATE_CONN_PENDING;
                Channel->flags = CHANNEL_FLAG_OUTGOING;
            }
        } else {
            /* Channel is already in use */
            status = BT_STATUS_IN_USE;
        }
    }

    OS_UnlockStack();

    return status;
}


/*---------------------------------------------------------------------------
 *            HFG_DisconnectServiceLink()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Close a service level connection. 
 *
 * Return:    (See header file)
 *
 */
BtStatus HFG_DisconnectServiceLink(HfgChannel *Channel)
{
    BtStatus status = BT_STATUS_NOT_FOUND;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Channel != 0);

    OS_LockStack();

    /* See if the channel is registered */
    if (IsNodeOnList(&HFG(channelList), &Channel->node)) {

        if (Channel->state == HFG_STATE_OPEN) {
            if (Channel->linkFlags == HFG_LINK_ACL) {
                /* Only an ACL connection */
                status = BT_STATUS_SUCCESS;
                Channel->cmgrHandler.errCode = BEC_LOCAL_TERMINATED;
                HfgCloseChannel(Channel);
            } else {
                if (Channel->linkFlags & HFG_LINK_HANDSFREE) {
                    if (CMGR_IsAudioUp(&Channel->cmgrHandler)) {
                        /* Audio still up, take it down */
                        Channel->linkFlags |= HFG_LINK_REMOVE_HF;
                        status = CMGR_RemoveAudioLink(&Channel->cmgrHandler);
                    } else {
                        status = RF_CloseChannel(Channel->hfgChannel);
                    }
                }
            }
        } else {
            status = BT_STATUS_NO_CONNECTION;
        }
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            HFG_CreateAudioLink()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Creates an audio (SCO) link to the audio gateway.
 *
 * Return:    (See header file)
 *
 */
BtStatus HFG_CreateAudioLink(HfgChannel *Channel)
{
    BtStatus status = BT_STATUS_NOT_FOUND;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Channel != 0);

    OS_LockStack();

    if (IsNodeOnList(&HFG(channelList), &Channel->node)) {
        if (Channel->state == HFG_STATE_OPEN) {
            status = CMGR_CreateAudioLink(&Channel->cmgrHandler);
        } else {
            status = BT_STATUS_NO_CONNECTION;
        }
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            HFG_DisconnectAudioLink()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Releases the audio connection with the audio gateway.
 *
 * Return:    (See header file)
 *
 */
BtStatus HFG_DisconnectAudioLink(HfgChannel *Channel)
{
    BtStatus status = BT_STATUS_NOT_FOUND;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Channel != 0);

    OS_LockStack();

    if (IsNodeOnList(&HFG(channelList), &Channel->node)) {
        if (Channel->state == HFG_STATE_OPEN) {
            status = CMGR_RemoveAudioLink(&Channel->cmgrHandler);
        } else {
            status = BT_STATUS_NO_CONNECTION;
        }
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            HFG_SetIndicatorValue()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Sets the current value for an indicator.
 *
 * Return:    (See header file)
 */
BtStatus HFG_SetIndicatorValue(HfgChannel *Channel, HfgIndicator Ind, 
                               U8 Value, HfgResponse *Response)
{
    BtStatus status = BT_STATUS_NOT_FOUND;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Channel != 0);
    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Response != 0);

    OS_LockStack();

    if (IsNodeOnList(&HFG(channelList), &Channel->node)) {

        status = BT_STATUS_SUCCESS;

        /* Set the indicator */
        switch (Ind) {
        case HFG_IND_SERVICE:
            if ((Value == 0) || (Value == 1)) {
                Channel->service = Value;
            } else {
                status = BT_STATUS_INVALID_PARM;
            }
            break;
        case HFG_IND_CALL:
            if ((Value == 0) || (Value == 1)) {
                Channel->call = Value;
            } else {
                status = BT_STATUS_INVALID_PARM;
            }
            break;
        case HFG_IND_CALL_SETUP:
            if (Value <= 3) {
                Channel->callSetup = Value;
            } else {
                status = BT_STATUS_INVALID_PARM;
            }
            break;
        case HFG_IND_CALL_HELD:
            if (2 >= Value) {
                Channel->held = Value;
            } else {
                status = BT_STATUS_INVALID_PARM;
            }
            break;
        case HFG_IND_BATTERY:
            if (Value <= 5) {
                Channel->battery = Value;
            } else {
                status = BT_STATUS_INVALID_PARM;
            }
            break;
        case HFG_IND_SIGNAL:
            if (Value <= 5) {
                Channel->signal = Value;
            } else {
                status = BT_STATUS_INVALID_PARM;
            }
            break;
        case HFG_IND_ROAMING:
            if ((Value == 0) || (Value == 1)) {
                Channel->roaming = Value;
            } else {
                status = BT_STATUS_INVALID_PARM;
            }
            break;
        default:
            status = BT_STATUS_INVALID_PARM;
            break;
        }

        if ((Channel->state == HFG_STATE_OPEN) &&
            (status != BT_STATUS_INVALID_PARM) &&
            (Channel->indEnabled)) {

            /* Send the indicator */
            Response->type = AT_INDICATOR_EVENT;
            Response->p.hf.event.indicator = Ind;
            Response->p.hf.event.value = Value;

            status = AtSendResults(Channel, Response);
        }
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            HFG_GetIndicatorValue()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Gets the current value for an indicator.
 *
 * Return:    (See header file)
 */
BtStatus HFG_GetIndicatorValue(HfgChannel *Channel, HfgIndicator Ind, U8 *Value)
{
    BtStatus status = BT_STATUS_NOT_FOUND;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Channel != 0);

    OS_LockStack();

    if (IsNodeOnList(&HFG(channelList), &Channel->node)) {

        status = BT_STATUS_SUCCESS;

        /* Set the indicator */
        switch (Ind) {
        case HFG_IND_SERVICE:
            *Value = Channel->service;
            break;
        case HFG_IND_CALL:
            *Value = Channel->call;
            break;
        case HFG_IND_CALL_SETUP:
            *Value = Channel->callSetup;
            break;
        case HFG_IND_CALL_HELD:
            *Value = Channel->held;
            break;
        case HFG_IND_BATTERY:
            *Value = Channel->battery;
            break;
        case HFG_IND_SIGNAL:
            *Value = Channel->signal;
            break;
        default:
            status = BT_STATUS_INVALID_PARM;
            break;
        }
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            HFG_SendHfResults()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Generic routine for sending results.
 *
 * Return:    (See header file)
 */
BtStatus HFG_SendHfResults(HfgChannel *Channel, AtCommand Command, 
                           U32 Parms, U16 ParmLen, HfgResponse *Response,
                           BOOL done)
{
    BtStatus status = BT_STATUS_NOT_FOUND;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Channel != 0);

    OS_LockStack();

    if (IsNodeOnList(&HFG(channelList), &Channel->node)) {
        if (Channel->state == HFG_STATE_OPEN) {

            Response->type = Command;

            /* Set up the repsonse parm */
            switch (Command) {
            case AT_ERROR:
            case AT_MICROPHONE_GAIN:
            case AT_SPEAKER_GAIN:
                Response->p.parm8 = (U8)Parms;
                break;
            case AT_IN_BAND_RING_TONE:
            case AT_VOICE_RECOGNITION:
                Response->p.parmB = (BOOL)Parms;
                break;
            case AT_VOICE_TAG:
                Response->p.hf.voiceTag.number = (const char *)Parms;
                break;
            case AT_RAW:
                Response->p.data = (char *)Parms;
                break;
            default:
                if (Parms) {
                    OS_MemCopy((U8*)&Response->p.hf, (const char *)Parms, ParmLen);
                }
            }

            switch (Command) {
            case AT_LIST_CURRENT_CALLS:
                if (Response->p.hf.currentCalls.state == HFG_CALL_STATUS_NONE) {
                    if (done) {
                        Response->type = AT_OK;
                        done = FALSE;
                    } else {
                        status = BT_STATUS_SUCCESS;
                        goto exit;
                    }
                }
                break;
            case AT_CALL_ID:
                if (!(Channel->callId)) {
                    status = BT_STATUS_NOT_SUPPORTED;
                };
                break;
            case AT_ERROR:
                if (Channel->extendedErrors) {
                    Response->type = AT_EXTENDED_ERROR;
                }
                break;
            }

            /* Send the response */
            status = AtSendResults(Channel, Response);
            if ((status == BT_STATUS_PENDING) && (done)) {
                Channel->flags |= CHANNEL_FLAG_SEND_OK;
            }

            /* Update internal state */
            if (status == BT_STATUS_PENDING) {
                switch (Response->type) {
                case AT_IN_BAND_RING_TONE:
                    Channel->ibRing = Response->p.hf.inBandRing.enabled;
                    break;
                case AT_VOICE_RECOGNITION:
                    Channel->voiceRec = Response->p.hf.vrec.enabled;
                    break;
                }
            }
        } else {
            status = BT_STATUS_NO_CONNECTION;
        }
    }

exit:

    OS_UnlockStack();

    return status;
}


/*---------------------------------------------------------------------------
 *             HFG_IsNRECEnabled()
 *---------------------------------------------------------------------------
 *
 * Synopsis:   Returns TRUE if Noise Reduction and Echo Cancelling is enabled in 
 *             the audio gateway.
 *
 * Return:     (See header file);
 */
BOOL HFG_IsNRECEnabled(HfgChannel *Channel)
{
    BOOL enabled = FALSE;

    OS_LockStack();

    if ((Channel) && IsNodeOnList(&HFG(channelList), &Channel->node)) {
        if (Channel->state == HFG_STATE_OPEN) {
            enabled = !Channel->nrecDisable;
        }
    }

    OS_UnlockStack();

    return enabled;
}

/*---------------------------------------------------------------------------
 *             HFG_IsInbandRingEnabled()
 *---------------------------------------------------------------------------
 *
 * Synopsis:   Returns TRUE if In-band Ringing is is enabled in the audio 
 *             gateway.
 *
 * Return:     (See header file);
 */
BOOL HFG_IsInbandRingEnabled(HfgChannel *Channel)
{
    BOOL enabled = FALSE;

    OS_LockStack();

    if ((Channel) && IsNodeOnList(&HFG(channelList), &Channel->node)) {
        if (Channel->state == HFG_STATE_OPEN) {
            enabled = Channel->ibRing;
        }
    }

    OS_UnlockStack();

    return enabled;
}

/*---------------------------------------------------------------------------
 *             HFG_IsCallIdNotifyEnabled()
 *---------------------------------------------------------------------------
 *
 * Synopsis:   Returns TRUE if Caller ID notification is enabled in the audio 
 *             gateway.
 *
 * Return:     (See header file);
 */
BOOL HFG_IsCallIdNotifyEnabled(HfgChannel *Channel)
{
    BOOL enabled = FALSE;

    OS_LockStack();

    if ((Channel) && IsNodeOnList(&HFG(channelList), &Channel->node)) {
        if (Channel->state == HFG_STATE_OPEN) {
            enabled = Channel->callId;
        }
    }

    OS_UnlockStack();

    return enabled;
}

/*---------------------------------------------------------------------------
 *             HFG_IsVoiceRecActive()
 *---------------------------------------------------------------------------
 *
 * Synopsis:   Returns TRUE if Voice Recognition is active in the audio gateway.  
 *
 * Return:     (See header file);
 */
BOOL HFG_IsVoiceRecActive(HfgChannel *Channel)
{
    BOOL active = FALSE;

    OS_LockStack();

    if ((Channel) && IsNodeOnList(&HFG(channelList), &Channel->node)) {
        if (Channel->state == HFG_STATE_OPEN) {
            active = Channel->voiceRec;
        }
    }

    OS_UnlockStack();

    return active;
}

/*---------------------------------------------------------------------------
 *             HFG_IsCallWaitingActive()
 *---------------------------------------------------------------------------
 *
 * Synopsis:   Returns TRUE if Call Waiting is active in the audio gateway.  
 *
 * Return:     (See header file);
 */
BOOL HFG_IsCallWaitingActive(HfgChannel *Channel)
{
    BOOL active = FALSE;

    OS_LockStack();

    if ((Channel) && IsNodeOnList(&HFG(channelList), &Channel->node)) {
        if (Channel->state == HFG_STATE_OPEN) {
            active = Channel->callWaiting;
        }
    }

    OS_UnlockStack();

    return active;
}

#if HFG_SNIFF_TIMER >= 0
/*---------------------------------------------------------------------------
 *             HFG_EnableSniffMode
 *---------------------------------------------------------------------------
 *
 * Synopsis:    Enables/Disables placing link into sniff mode on the specified 
 *              channel
 */
BtStatus HFG_EnableSniffMode(HfgChannel *Channel, BOOL Enable) 
{
    BtSniffInfo sniffInfo;

    BtStatus    status = BT_STATUS_NOT_FOUND;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Channel != 0);

    OS_LockStack();

    if ((Channel) && IsNodeOnList(&HFG(channelList), &Channel->node)) {
        if (Enable) {
            if (!(Channel->flags & CHANNEL_FLAG_SNIFFING) && 
                (Channel->state == HFG_STATE_OPEN)) {
                /* Start the sniff timer */
                sniffInfo.minInterval = HFG_SNIFF_MIN_INTERVAL;
                sniffInfo.maxInterval = HFG_SNIFF_MAX_INTERVAL;
                sniffInfo.attempt = HFG_SNIFF_ATTEMPT;
                sniffInfo.timeout = HFG_SNIFF_TIMEOUT;
                CMGR_SetSniffTimer(&Channel->cmgrHandler, &sniffInfo, 
                                   HFG_SNIFF_TIMER);
            }
        } else {
            if ((Channel->flags & CHANNEL_FLAG_SNIFFING) && 
                (Channel->state == HFG_STATE_OPEN)) {
                CMGR_ClearSniffTimer(&Channel->cmgrHandler);
            }
        }

        /* Set the internal state */
        if (Enable) {
            Channel->flags |= CHANNEL_FLAG_SNIFFING;
        } else {
            Channel->flags &= ~CHANNEL_FLAG_SNIFFING;
        }

        status = BT_STATUS_SUCCESS;
    }

    OS_UnlockStack();

    return status;

}
#endif



