/****************************************************************************
 *
 * File:
 *     $Workfile:hfg_sdp.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:12$
 *
 * Description: This file contains the SDP code for the Hands-free SDK.
 *             
 * Created:     February 11, 2005
 *
 * Copyright 2000-2005 Extended Systems, Inc.

 * Portions copyright 2005 iAnywhere Solutions, Inc.

 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any means, 
 * or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions,  
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "sys/hfgalloc.h"
#include "btalloc.h"

#if SDP_PARSING_FUNCS == XA_DISABLED
#error The Hands-free Gateway SDK requires the SDP parsing functions.
#endif /* SDP_PARSING_FUNCS == XA_DISABLED */

/****************************************************************************
 *
 * ROMable data
 *
 ****************************************************************************/

/****************************************************************************
 * Hands-Free Gateway SDP Entries
 ****************************************************************************/

/*---------------------------------------------------------------------------
 *
 * ServiceClassIDList 
 */
static const U8 HfgClassId[] = {
    SDP_ATTRIB_HEADER_8BIT(6),                  /* Data Element Sequence, 
                                                 * 6 bytes 
                                                 */
    SDP_UUID_16BIT(SC_HANDSFREE_AUDIO_GATEWAY), /* Hands-Free UUID in 
                                                 * Big Endian 
                                                 */
    SDP_UUID_16BIT(SC_GENERIC_AUDIO)            /* Generic Audio UUID in Big
                                                 * Endian 
                                                 */
};

/*---------------------------------------------------------------------------
 * SDP Protocol Descriptor List object registered by Hands-Free Audio Gateway.
 * 
 * Value of the protocol descriptor list for the Hands-Free Audio Gateway 
 * profile.  This structure is a ROM'able representation of the RAM structure.
 * This structure is copied into a RAM structure used to register the 
 * service. A RAM structure must be used to be able to dynamically set the 
 * RFCOMM channel in the RFCOMM protocol descriptor.
 */
static const U8 HfgProtoDescList[] = {
    SDP_ATTRIB_HEADER_8BIT(12),  /* Data element sequence, 12 bytes */

    /* Each element of the list is a Protocol descriptor which is a
     * data element sequence. The first element is L2CAP which only
     * has a UUID element.
     */
    SDP_ATTRIB_HEADER_8BIT(3),   /* Data element sequence for L2CAP, 3 bytes */     
    SDP_UUID_16BIT(PROT_L2CAP),  /* Uuid16 L2CAP */

    /* Next protocol descriptor in the list is RFCOMM. It contains two
     * elements which are the UUID and the channel. Ultimately this
     * channel will need to filled in with value returned by RFCOMM.
     */
    SDP_ATTRIB_HEADER_8BIT(5),   /* Data element sequence for RFCOMM, 5 bytes */
    SDP_UUID_16BIT(PROT_RFCOMM), /* Uuid16 RFCOMM */
    SDP_UINT_8BIT(99)            /* Uint8 RFCOMM channel number - value can vary */
};


/*
 * BluetoothProfileDescriptorList
 */
static const U8 HfgProfileDescList[] = {
    SDP_ATTRIB_HEADER_8BIT(8),            /* Data element sequence, 8 bytes */
    SDP_ATTRIB_HEADER_8BIT(6),            /* Data element sequence for ProfileDescriptor, 6 bytes */
    SDP_UUID_16BIT(SC_HANDSFREE),         /* Uuid16 Hands-Free */
    SDP_UINT_16BIT(HFG_HF_VERSION_1_5)    /* Uint16 version number */
};

/*
 * * OPTIONAL * Language BaseId List (goes with the service name).
 */
static const U8 HfgLangBaseIdList[] = {
    SDP_ATTRIB_HEADER_8BIT(9),  /* Data Element Sequence, 9 bytes */
    SDP_UINT_16BIT(0x656E),     /* English Language */
    SDP_UINT_16BIT(0x006A),     /* UTF-8 encoding */
    SDP_UINT_16BIT(0x0100)      /* Primary language base Id */
};

/*
 * * OPTIONAL *  ServiceName
 */
static const U8 HfgServiceName[] = {
    SDP_TEXT_8BIT(14),          /* Null terminated text string */
    'V', 'o', 'i', 'c', 'e', 'g', ' ', 'a', 't', 'e', 'w', 'a', 
    'y', '\0'
};

/*
 * * OPTIONAL *  Network
 */
#if HFG_SDK_FEATURES & HFG_FEATURE_REJECT
static const U8 HfgNetwork[] = {
    SDP_UINT_8BIT(0x01)     /* Features are set when registering service */
};
#else
static const U8 HfgNetwork[] = {
    SDP_UINT_8BIT(0x00)     /* Features are set when registering service */
};
#endif

/*
 * * OPTIONAL *  Supported Features
 */
static const U8 HfgSupportedFeatures[] = {
    SDP_UINT_16BIT(0x0000)     /* Features are set when registering service */
};

/*---------------------------------------------------------------------------
 * Hands-Free Public Browse Group.
 */
static const U8 HfgBrowseGroup[] = {
    SDP_ATTRIB_HEADER_8BIT(3),               /* 3 bytes */
    SDP_UUID_16BIT(SC_PUBLIC_BROWSE_GROUP),  /* Public Browse Group */
};

/* Hands-Free attributes.
 *
 * This is a ROM template for the RAM structure used to register the
 * Hands-Free SDP record.
 */
static const SdpAttribute HfgSdpAttributes[] = {
    /* Hands-Free audio gateway class ID List attribute */
    SDP_ATTRIBUTE(AID_SERVICE_CLASS_ID_LIST, HfgClassId), 
    /* Audio Gateway protocol descriptor list attribute */
    SDP_ATTRIBUTE(AID_PROTOCOL_DESC_LIST, HfgProtoDescList),
    /* Public Browse Group Service */
    SDP_ATTRIBUTE(AID_BROWSE_GROUP_LIST, HfgBrowseGroup), 
    /* Language base id (Optional: Used with service name) */
    SDP_ATTRIBUTE(AID_LANG_BASE_ID_LIST, HfgLangBaseIdList),
    /* Hands-Free Bluetooth profile descriptor list */
    SDP_ATTRIBUTE(AID_BT_PROFILE_DESC_LIST, HfgProfileDescList),
    /* Hands-Free service name*/
    SDP_ATTRIBUTE((AID_SERVICE_NAME + 0x0100), HfgServiceName),
    /* Network feature */
    SDP_ATTRIBUTE(AID_EXTERNAL_NETWORK, HfgNetwork),
    /* Supported features */
    SDP_ATTRIBUTE(AID_SUPPORTED_FEATURES, HfgSupportedFeatures)
};

/*-------------------------------------------------------------------------
 *
 * SDP query info
 */
static const U8 HfgServiceSearchAttribReq[] = {
    /* First parameter is the search pattern in data element format. It
     * is a list of 3 UUIDs. 
     */
    SDP_ATTRIB_HEADER_8BIT(9),     /* Data Element Sequence, 9 bytes */ 

    SDP_UUID_16BIT(SC_HANDSFREE),  /* UUID for Hands-Free AG in Big Endian */
    SDP_UUID_16BIT(PROT_L2CAP),    /* L2CAP UUID in Big Endian */
    SDP_UUID_16BIT(PROT_RFCOMM),   /* UUID for RFCOMM in Big Endian */

    /* The second parameter is the maximum number of bytes that can be
     * be received for the attribute list
     */
    0x00, 0x64,                 /* Max number of bytes for attribute is 100 */

    SDP_ATTRIB_HEADER_8BIT(9),
    SDP_UINT_16BIT(AID_PROTOCOL_DESC_LIST), 
    SDP_UINT_16BIT(AID_BT_PROFILE_DESC_LIST),
    SDP_UINT_16BIT(AID_SUPPORTED_FEATURES)
};

/* Internal function prototypes */
static void HfgSdpEventHandler(const BtEvent *Event);

/****************************************************************************
 *
 * Functions
 *
 ****************************************************************************/

/*---------------------------------------------------------------------------
 *            HfgRegisterSdpServices()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Registers the SDP services.
 *
 * Return:    See SDP_AddRecord().
 */
BtStatus HfgRegisterSdpServices(HfgChannel *Channel)
{
    BtStatus status;

    /* RFCOMM Channel */
    Assert(sizeof(HFG(hfgSdpProtoDescList)) == sizeof(HfgProtoDescList));
    OS_MemCopy((U8 *)HFG(hfgSdpProtoDescList), (U8 *)HfgProtoDescList,
               sizeof(HfgProtoDescList));
    HFG(hfgSdpProtoDescList)[13] = HFG(hfgService).serviceId;

    /* Supported Features */
    Assert(sizeof(HFG(hfgSdpSupportedFeatures)) == sizeof(HfgSupportedFeatures));
    OS_MemCopy((U8 *)HFG(hfgSdpSupportedFeatures), (U8 *)HfgSupportedFeatures,
               sizeof(HfgSupportedFeatures));
    StoreBE16(HFG(hfgSdpSupportedFeatures)+1, (U16)(HFG_SDK_FEATURES & 0x1F));

    /* Register Hands-free SDP Attributes */
    Assert(sizeof(HFG(hfgSdpAttribute)) == sizeof(HfgSdpAttributes));
    OS_MemCopy((U8 *)HFG(hfgSdpAttribute), (U8 *)HfgSdpAttributes,
               sizeof(HfgSdpAttributes));

    HFG(hfgSdpAttribute[1]).value = HFG(hfgSdpProtoDescList);
    HFG(hfgSdpAttribute[7]).value = HFG(hfgSdpSupportedFeatures);

    HFG(hfgSdpRecord).attribs = HFG(hfgSdpAttribute);
    HFG(hfgSdpRecord).num = 8;
    HFG(hfgSdpRecord).classOfDevice = COD_AUDIO | COD_MAJOR_AUDIO | 
                                     COD_MINOR_AUDIO_HANDSFREE;

    status = SDP_AddRecord(&HFG(hfgSdpRecord));

    return status;
}

/*---------------------------------------------------------------------------
 *            HfgDeregisterSdpServices()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Deregisters the SDP services.
 *
 * Return:    See SDP_RemoveRecord().
 */
BtStatus HfgDeregisterSdpServices(void)
{
    BtStatus status;

    /* Remove the Hands-free entry */
    status = SDP_RemoveRecord(&HFG(hfgSdpRecord));
    
    return status;
}

/*---------------------------------------------------------------------------
 *            HfgStartServiceQuery()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initiate the SDP service query.
 *
 * Return:    See SDP_Query().
 */
BtStatus HfgStartServiceQuery(HfgChannel *Channel, SdpQueryMode mode)
{
    BtStatus status;

    /* Search for Hands-free profile */
    Channel->sdpQueryToken.parms = HfgServiceSearchAttribReq;
    Channel->sdpQueryToken.plen = sizeof(HfgServiceSearchAttribReq);
    Channel->sdpQueryToken.type = BSQT_SERVICE_SEARCH_ATTRIB_REQ;
    Channel->sdpQueryToken.callback = HfgSdpEventHandler;
    Channel->sdpQueryToken.rm = Channel->cmgrHandler.bdc->link;
    status = SDP_Query(&Channel->sdpQueryToken, mode);
    return status;
} 

/*---------------------------------------------------------------------------
 *            HfgVerifySdpQueryRsp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Parses the SDP response for the RFCOMM channel,
 *            Profile Descriptor List, and Supported Features.  
 *            In the case of a continuation state, additional
 *            SDP queries will be issued until all of the attributes have
 *            been found or until the entire SDP response has been parsed.
 *            
 *
 * Return:    BT_STATUS_SUCCESS if SDP parsing is successful.
 *            BT_STATUS_FAILED if SDP parsing encountered an error.
 *            BT_STATUS_SDP_CONT_STATE if SDP parsing encountered a 
 *            continuation state that requires an additional SDP query.
 */
BtStatus HfgVerifySdpQueryRsp(HfgChannel *Channel, SdpQueryToken *token)
{
    BtStatus        status = BT_STATUS_FAILED;

    if (!(Channel->queryFlags & SDP_QUERY_FLAG_PROTOCOL)) {

        /* Get the RFCOMM channel */
        token->attribId = AID_PROTOCOL_DESC_LIST;
        token->uuid = PROT_RFCOMM;

        if ((status = SDP_ParseAttributes(token)) == BT_STATUS_SUCCESS) {
            /* We had better have a U8 channel value, if not, look for any other channels */
            if ((token->totalValueLen == 1) && (token->availValueLen == 1)) {
                Report(("HFG: SDP_ParseAttributes succeeded! RFCOMM channel = %d,"
                        "Updated Buff Len = %d\n", token->valueBuff[0], 
                        token->remainLen));
                /* Assign the RFCOMM channel number */
                Channel->rfServerChannel = token->valueBuff[0];
                token->mode = BSPM_RESUME;
                Channel->queryFlags |= SDP_QUERY_FLAG_PROTOCOL;
            }
        }

        /* Handle continuation state */
        if (status == BT_STATUS_SDP_CONT_STATE) goto done;
    }

    if (!(Channel->queryFlags & SDP_QUERY_FLAG_PROFILE)) {

        /* Get the BT Profile Version */
        token->attribId = AID_BT_PROFILE_DESC_LIST;
        token->uuid = SC_HANDSFREE;
        status = SDP_ParseAttributes(token);
        if (status == BT_STATUS_FAILED) {
            token->uuid = SC_HANDSFREE_AUDIO_GATEWAY;
            status = SDP_ParseAttributes(token);
        }

        if (status == BT_STATUS_SUCCESS) {
            if ((token->totalValueLen == 2) && 
                ((token->availValueLen == 2) || (token->availValueLen == 1))) {

                /* Assign the Profile UUID and Profile Version */
                if (token->availValueLen == 1) {
                    Channel->version += token->valueBuff[0];
                } else {
                    Channel->version = SDP_GetU16(token->valueBuff);
                }

                token->mode = BSPM_RESUME;
                Channel->queryFlags |= SDP_QUERY_FLAG_PROFILE;
            }
        }


        if ((status == BT_STATUS_SDP_CONT_STATE) && (token->availValueLen == 1)) {
            /* It's possible that only a portion of the data might occur.
             * If so, we'll copy what we have.  The data arrives in big endian format.
             */
            Channel->version = ((U16)token->valueBuff[0]) << 8;
        }

        /* Handle continuation state */
        if (status == BT_STATUS_SDP_CONT_STATE) goto done;
    }

    if (!(Channel->queryFlags & SDP_QUERY_FLAG_FEATURES)) {

        /* Get the Supported Features */
        token->attribId = AID_SUPPORTED_FEATURES;
        token->uuid = 0;
        if ((status = SDP_ParseAttributes(token)) == BT_STATUS_SUCCESS) {
            if ((token->totalValueLen == 2) && 
                ((token->availValueLen == 2) || (token->availValueLen == 1))) {

                /* Assign the Supported Features */
                if (token->availValueLen == 1) {
                    Channel->hfFeatures += token->valueBuff[0];
                } else {
                    Channel->hfFeatures = SDP_GetU16(token->valueBuff);
                }

                Channel->queryFlags |= SDP_QUERY_FLAG_FEATURES;

                if (!(Channel->flags & CHANNEL_FLAG_OUTGOING)) {
                    Channel->flags |= CHANNEL_FLAG_FEATURES_SENT;
                    Channel->cmgrHandler.errCode = BEC_NO_ERROR;
                    HfgAppCallback(Channel, HFG_EVENT_HANDSFREE_FEATURES, BT_STATUS_SUCCESS, 
                                   (U32)(U32)Channel->hfFeatures);
                }
            }
        }

        if ((status == BT_STATUS_SDP_CONT_STATE) && (token->availValueLen == 1)) {
            /* It's possible that only a portion of the data might occur.
             * If so, we'll copy what we have.  The data arrives in big endian format.
             */
            Channel->hfFeatures = ((U16)token->valueBuff[0]) << 8;
        }
    }

done:
    if (status == BT_STATUS_SDP_CONT_STATE) {
        Report(("HFG: SDP_ParseAttributes - Continuation State. Query Again!\n"));
        token->mode = BSPM_CONT_STATE;
        return status;
    }

    /* Reset the attribute to parse */
    token->attribId = AID_PROTOCOL_DESC_LIST;
    /* Reset the SDP parsing mode */
    token->mode = BSPM_BEGINNING;

    /* Return the status of the RFCOMM channel parsing, since this
     * is the only critical information to obtain a connection.
     */
    if (Channel->queryFlags & SDP_QUERY_FLAG_PROTOCOL)
        return BT_STATUS_SUCCESS;
    else return BT_STATUS_FAILED;
}

/*---------------------------------------------------------------------------
 *            HfgSdpEventHandler()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handles all SDP events
 */
static void HfgSdpEventHandler(const BtEvent *Event)
{
    BtStatus        status;
    HfgChannel     *channel = ContainingRecord(Event->p.token, 
                                               HfgChannel, sdpQueryToken);

    switch (Event->eType) {
    case SDEVENT_QUERY_RSP:
        status = HfgVerifySdpQueryRsp(channel, Event->p.token);
        if (status == BT_STATUS_SUCCESS) {
            /* SDP result parsing completed */
            if (channel->flags & CHANNEL_FLAG_OUTGOING) {

                /* Connect to hands-free */
                channel->hfgChannel = HfgAllocRfChannel();
                if (channel->hfgChannel) {
                    status = RF_OpenClientChannel(channel->cmgrHandler.bdc->link,
                                                  channel->rfServerChannel,
                                                  channel->hfgChannel, 1);
                } else {
                    status = BT_STATUS_NO_RESOURCES;
                }

                if (status == BT_STATUS_PENDING) {
                    channel->state = HFG_STATE_CONN_PENDING;
                } else {
                    Report(("HFG: Could not open RFCOMM channel\n"));
                    channel->cmgrHandler.errCode = BEC_LOCAL_TERMINATED;
                    HfgCloseChannel(channel);
                }
            }
            break;
        } else if (status == BT_STATUS_SDP_CONT_STATE) {
            /* We are going to query again for the rest of the response */
            status = HfgStartServiceQuery(channel, BSQM_CONTINUE);
            if ((status == BT_STATUS_PENDING) || 
                !(channel->flags & CHANNEL_FLAG_OUTGOING)) {
            } else {
                Report(("HFG: Could not restart query\n"));
                channel->cmgrHandler.errCode = BEC_LOCAL_TERMINATED;
                HfgCloseChannel(channel);
            }
            break;
        } else {
            /* Query failed */
            Report(("HFG: SDP Query failed\n"));
            channel->cmgrHandler.errCode = BEC_LOCAL_TERMINATED;
            HfgCloseChannel(channel);
        }
        break;
    case SDEVENT_QUERY_ERR:
    case SDEVENT_QUERY_FAILED:
        if (channel->flags & CHANNEL_FLAG_OUTGOING) {

            /* Query failed */
            Report(("HFG: SDP Query failed\n"));
            channel->cmgrHandler.errCode = BEC_LOCAL_TERMINATED;
            HfgCloseChannel(channel);
        }
        break;
    }
}

