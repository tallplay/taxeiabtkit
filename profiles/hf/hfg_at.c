/****************************************************************************
 *
 * File:
 *     $Workfile:hfg_at.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:9$
 *
 * Description: This file contains functions for transmitting AT responses.
 *             
 * Created:     February 11, 2005
 *
 * Copyright 2000-2005 Extended Systems, Inc.

 * Portions copyright 2005 iAnywhere Solutions, Inc.

 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any means, 
 * or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions,  
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "atp.h"
#include "sys/hfgalloc.h"
#include "btalloc.h"

/*--------------------------------------------------------------------------- 
 * AtSendresponse
 * 
 *     Encodes and sends an AT Results.
 */ 
BtStatus AtSendResults(HfgChannel *Channel, AtResults *Results)
{
    BtStatus  status;
    AtStatus  atStatus;

    if (Channel->state < HFG_STATE_OPEN) {
        status = BT_STATUS_NO_CONNECTION;
        goto exit;
    }

    /* Queue the AT Results when a response is outstanding */
    if (Channel->flags & CHANNEL_FLAG_TX_IN_PROGRESS) {
        if (!IsNodeOnList(&Channel->rspQueue, &Results->node)) {
            InsertTailList(&Channel->rspQueue, &Results->node);
            status = BT_STATUS_PENDING;
        } else {
            status = BT_STATUS_IN_USE;
        }
        goto exit;
    }

    /* Encode AT Results */
    Channel->atBuffer.buff = Channel->atTxData;
    Channel->atBuffer.readOffset = 0;
    Channel->atBuffer.writeOffset = 0;
    Channel->atBuffer.buffSize = HFG_TX_BUFFER_SIZE;
    atStatus = ME_Encode(&HFG(atContext), Results, &Channel->atBuffer);
    if (atStatus != AT_STATUS_OK) {
        status = BT_STATUS_FAILED;
        goto exit;
    }

    /* Send the packet */
    Channel->bytesToSend = Channel->atBuffer.writeOffset;
    Channel->atTxPacket.dataLen = Channel->bytesToSend;
    Channel->atTxPacket.data = Channel->atTxData;
    status = HfgAtSendRfPacket(Channel, &Channel->atTxPacket);
    if (status == BT_STATUS_PENDING) {

        Channel->flags |= CHANNEL_FLAG_TX_IN_PROGRESS;
        Channel->currentAtResults = Results;
    }

 exit:

    return status;
}

/*--------------------------------------------------------------------------- 
 * HfgAtSendRfPacket
 * 
 *     Sends a packet over RFCOMM.
 */ 
BtStatus HfgAtSendRfPacket(HfgChannel *Channel, BtPacket *Packet)
{
    BtStatus status;

    if (Packet->dataLen > RF_FrameSize(Channel->hfgChannel)) {
        Packet->dataLen = RF_FrameSize(Channel->hfgChannel);
    }

    status = RF_SendData(Channel->hfgChannel, Packet);
    if (status == BT_STATUS_PENDING) {
        Channel->bytesToSend -= Packet->dataLen;
    }

    return status;
}

/*--------------------------------------------------------------------------- 
 * AtSendComplete
 * 
 *     Completes the response and sends a new one if queued.
 */ 
BtStatus AtSendComplete(HfgChannel *Channel, BtPacket *Packet)
{
    AtResults *atResults;
    BtStatus   status = BT_STATUS_SUCCESS;

    Channel->flags &= ~CHANNEL_FLAG_TX_IN_PROGRESS;
    Channel->lastAtResults = Channel->currentAtResults;
    if (!IsListEmpty(&Channel->rspQueue)) {
        atResults = (AtResults *)RemoveHeadList(&Channel->rspQueue);
        status = AtSendResults(Channel, atResults);
    }

    return status;
}

/*--------------------------------------------------------------------------- 
 * AtParseCommand
 * 
 *     Parses the result code.
 */ 
void AtParseCommand(HfgChannel *Channel, U8 *Input, 
                    U16 InputLen, AtCommands *Command)
{
    AtStatus   atStatus;

    Channel->atBuffer.buff = Input;
    Channel->atBuffer.readOffset = 0;
    Channel->atBuffer.writeOffset = InputLen;
    Channel->atBuffer.buffSize = InputLen;
    atStatus = ME_Decode(&HFG(atContext), Command, &Channel->atBuffer);

    if (atStatus != AT_STATUS_OK) {
        /* Unsuppored command, indicate unknown AT data */
        Command->type = AT_UNKNOWN;
    }
}


