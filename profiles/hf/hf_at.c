/****************************************************************************
 *
 * File:
 *     $Workfile:hf_at.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:7$
 *
 * Description: This file contains functions for transmitting AT commands.
 *             
 * Created:     February 11, 2005
 *
 * Copyright 2000-2005 Extended Systems, Inc.

 * Portions copyright 2005 iAnywhere Solutions, Inc.

 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any means, 
 * or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions,  
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "atp.h"
#include "sys/hfi.h"
#include "sys/hf_cmd.h"
#include "sys/hfalloc.h"

/****************************************************************************
 *
 * ROMable data
 *
 ****************************************************************************/
 
const char *HfIndMsg[] = {
    "CALL",
    "SIGNAL",
    "SERVICE",
    "ROAM",
    "MESSAGE",
    "BATTCHG",
    "CALLSETUP",
    "CALL_SETUP",
    "CALLHELD",
    0
};

/*--------------------------------------------------------------------------- 
 * AtTimeout
 * 
 *     Handler for command timeouts.
 */
void AtTimeout(EvmTimer *Timer)
{
    HfChannel     *channel = Timer->context;
    AtCommands    *atCommand;

    /* Complete the command, queue up the next one */
    Timer->context = 0;
    channel->flags &= ~CHANNEL_FLAG_TX_IN_PROGRESS;
    channel->lastAtCommand = channel->currentAtCommand;
    if (!IsListEmpty(&channel->txQueue)) {
        atCommand = (AtCommands *)RemoveHeadList(&channel->txQueue);
        if (AtSendCommand(channel, atCommand) != BT_STATUS_PENDING) {
            /* Fatal error, Failed to send command */
            RF_CloseChannel(channel->hfChannel);
            return;
        }
    }

    /* Indicate timeout to the state machine */
    HF(hfState)[channel->state](channel, EVENT_COMMAND_TIMEOUT, 0);
}

/*--------------------------------------------------------------------------- 
 * AtSendRaw
 * 
 *     Sends a raw AT command.
 */ 
BtStatus AtSendRaw(HfChannel *Channel, const char *AtString)
{
    BtStatus status;

    /* Send the packet */
    Channel->bytesToSend = OS_StrLen(AtString);
    OS_MemCopy((U8*)Channel->atTxData, AtString, Channel->bytesToSend);
    Channel->atTxPacket.dataLen = Channel->bytesToSend;
    Channel->atTxPacket.data = Channel->atTxData;
    status = HfAtSendRfPacket(Channel, &Channel->atTxPacket);
    if (status == BT_STATUS_PENDING) {

        Channel->flags |= CHANNEL_FLAG_TX_IN_PROGRESS;

        /* Set a timer to clean up the state if OK is not received */
        Channel->atTimer.func = AtTimeout;
        Channel->atTimer.context = Channel;
        EVM_StartTimer(&Channel->atTimer, HF_COMMAND_TIMEOUT);
        Channel->currentAtCommand = &Channel->atCommand;
    }

    return status;
}

/*--------------------------------------------------------------------------- 
 * AtSendCommand
 * 
 *     Encodes and sends an AT command.
 */ 
BtStatus AtSendCommand(HfChannel *Channel, AtCommands *command)
{
    BtStatus  status;
    AtStatus  atStatus;

    if (Channel->state < HF_STATE_NEGOTIATE) {
        status = BT_STATUS_NO_CONNECTION;
        goto exit;
    }

    /* Queue the AT command when a command is outstanding */
    if (Channel->flags & CHANNEL_FLAG_TX_IN_PROGRESS) {
        if (!IsNodeOnList(&Channel->txQueue, &command->node)) {
            InsertTailList(&Channel->txQueue, &command->node);
            status = BT_STATUS_PENDING;
        } else {
            status = BT_STATUS_IN_USE;
        }
        goto exit;
    }

    /* Encode AT command */
    Channel->atBuffer.buff = Channel->atTxData;
    Channel->atBuffer.readOffset = 0;
    Channel->atBuffer.writeOffset = 0;
    Channel->atBuffer.buffSize = HF_TX_BUFFER_SIZE;
    atStatus = TE_Encode(&HF(atContext), command, &Channel->atBuffer);
    if (atStatus != AT_STATUS_OK) {
        status = BT_STATUS_FAILED;
        goto exit;
    }

    /* Send the packet */
    Channel->bytesToSend = Channel->atBuffer.writeOffset;
    Channel->atTxPacket.dataLen = Channel->bytesToSend;
    Channel->atTxPacket.data = Channel->atTxData;
    status = HfAtSendRfPacket(Channel, &Channel->atTxPacket);
    if (status == BT_STATUS_PENDING) {

        Channel->flags |= CHANNEL_FLAG_TX_IN_PROGRESS;

        /* Set a timer to clean up the state if OK is not received */
        Channel->atTimer.func = AtTimeout;
        Channel->atTimer.context = Channel;
        EVM_StartTimer(&Channel->atTimer, HF_COMMAND_TIMEOUT);
        Channel->currentAtCommand = command;
    }

 exit:

    return status;
}

BtStatus HfAtSendRfPacket(HfChannel *Channel, 
                        BtPacket *Packet)
{
    BtStatus status = BT_STATUS_IN_USE;

    if (Packet->dataLen > RF_FrameSize(Channel->hfChannel)) {
        Packet->dataLen = RF_FrameSize(Channel->hfChannel);
    }

    if (!(Packet->flags & BTP_FLAG_INUSE)) {
        status = RF_SendData(Channel->hfChannel, Packet);
        if (status == BT_STATUS_PENDING) {
            Packet->flags |= BTP_FLAG_INUSE;
            Channel->bytesToSend -= Packet->dataLen;
        }
    }

    return status;
}

/*--------------------------------------------------------------------------- 
 * AtParseResultCode
 * 
 *     Parses the result code.
 */ 
void AtParseResultCode(HfChannel *Channel, U8 *Input, 
                       U16 InputLen, AtResults *Results)
{
    AtCommands *atCommand;
    AtStatus    atStatus;

    Channel->atBuffer.buff = Input;
    Channel->atBuffer.readOffset = 0;
    Channel->atBuffer.writeOffset = InputLen;
    Channel->atBuffer.buffSize = InputLen;
    atStatus = TE_Decode(&HF(atContext), Results, &Channel->atBuffer);

    if (atStatus == AT_STATUS_OK) {
        if ((Results->type == AT_OK) ||
            (Results->type == AT_ERROR)  ||
            (Results->type == AT_EXTENDED_ERROR)) {
            
            /* A command is complete, send the next if there is one */
            Channel->flags &= ~CHANNEL_FLAG_TX_IN_PROGRESS;
            Channel->lastAtCommand = Channel->currentAtCommand;
            if (!IsListEmpty(&Channel->txQueue)) {
                atCommand = (AtCommands *)RemoveHeadList(&Channel->txQueue);
                if (AtSendCommand(Channel, atCommand) != BT_STATUS_PENDING) {
                    /* Fatal error, Failed to send command */
                    RF_CloseChannel(Channel->hfChannel);
                }
            }
            EVM_CancelTimer(&Channel->atTimer);
        }
    } else {
        /* Erroneous or unsuppored result codes */
        Results->type = AT_UNKNOWN;
    }

}


