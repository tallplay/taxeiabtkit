/****************************************************************************
 *
 * File:
 *     $Workfile:hfg_util.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:15$
 *
 * Description: This file contains utility functions for the Hands-free SDK.
 *             
 * Created:     February 11, 2005
 *
 * Copyright 2000-2005 Extended Systems, Inc.

 * Portions copyright 2005 iAnywhere Solutions, Inc.

 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any means, 
 * or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions,  
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "sys/hfgalloc.h"
#include "conmgr.h"
#include "btalloc.h"

/*---------------------------------------------------------------------------
 *            HFG_Init()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initialize HF Gateway SDK.  Initialize state and other variables.
 *
 * Return:    TRUE - Memory successfully initialized.
 */
BOOL HFG_Init(void)
{
    U16 i;

    /* Initialize context memory */
    if (!(HfgAlloc())) {
        return FALSE;
    }

    /* Load the state machines' function tables */
    HfgInitStateMachine();

    /* Initialize lists */
    InitializeListHead(&HFG(channelList));
    InitializeListHead(&HFG(freeRfChannelList));
    for (i = 0; i < NUM_BT_DEVICES * 2 + 2; i++) {
        HFG(rfChannels)[i].userContext = 0;
        InsertTailList(&HFG(freeRfChannelList), &HFG(rfChannels)[i].node);
    }

    if (AT_Init(&HFG(atContext)) != AT_STATUS_OK) {
        return FALSE;
    }

    return TRUE;
}

/*---------------------------------------------------------------------------
 *            HfgParseRfcommData()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Parses received RFCOMM data and passes it to the AT parser.
 *
 * Return:    BT_STATUS_PENDING - More data to parse.
 *            BT_STATUS_SUCCESS - Done parsing data.
 */
BtStatus HfgParseRfcommData(HfgChannel *channel, 
                            RfCallbackParms *parms, 
                            U16 *offset, 
                            AtCommands *atParms)
{
    BtStatus    status = BT_STATUS_FAILED;
    U16         i;

    for (i = *offset; i < parms->dataLen; i++) {

        (*offset)++;
        if (channel->atRxLen >= HFG_RECV_BUFFER_SIZE) {
            status = BT_STATUS_FAILED;
            break;
        }

        channel->atRxBuffer[channel->atRxLen++] = parms->ptrs.data[i];

        if (parms->ptrs.data[i] == '\r') {

            /* At the end of the command */
            channel->atRxLen--;
            channel->atRxBuffer[channel->atRxLen] = 0;
            AtParseCommand(channel, channel->atRxBuffer, 
                           channel->atRxLen, atParms);
            channel->atRxLen = 0;
            status = BT_STATUS_SUCCESS;
            break;
        }
    }

    return status;
}

/*---------------------------------------------------------------------------
 *            HfgAppCallback()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Sets up the callback parameters and calls the application.
 *
 * Return:    void
 */
void HfgAppCallback(HfgChannel *Channel, HfgEvent Event, 
                    BtStatus Status, U32 data)
{
    HfgCallbackParms hfgParms;

    hfgParms.event = Event;
    hfgParms.p.ptr = (void *)data;
    hfgParms.status = Status;
    hfgParms.errCode = Channel->cmgrHandler.errCode;

    Channel->callback(Channel, &hfgParms);
}

/*---------------------------------------------------------------------------
 *            HfgReportServiceConnected()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Indicates that the service level connection has been established.
 *
 * Return:    void
 */
void HfgReportServiceConnected(HfgChannel *Channel)
{
#if HFG_SNIFF_TIMER >= 0
    BtSniffInfo sniffInfo;
#endif

    /* Service channel is up, tell the app */
    Channel->state = HFG_STATE_OPEN;
    Channel->cmgrHandler.errCode = BEC_NO_ERROR;
    HfgAppCallback(Channel, HFG_EVENT_SERVICE_CONNECTED, BT_STATUS_SUCCESS, 
                   (U32)Channel->cmgrHandler.bdc->link);

#if HFG_SNIFF_TIMER >= 0
    if (Channel->flags & CHANNEL_FLAG_SNIFFING) {
        /* Start the sniff timer */
        sniffInfo.minInterval = HFG_SNIFF_MIN_INTERVAL;
        sniffInfo.maxInterval = HFG_SNIFF_MAX_INTERVAL;
        sniffInfo.attempt = HFG_SNIFF_ATTEMPT;
        sniffInfo.timeout = HFG_SNIFF_TIMEOUT;
        CMGR_SetSniffTimer(&Channel->cmgrHandler, &sniffInfo, 
                           HFG_SNIFF_TIMER);
    }
#endif
}

/*---------------------------------------------------------------------------
 *            HfgSetupIndicatorTestRsp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Sets up the response to a Indicator Test command.
 *
 * Return:    void
 */
void HfgSetupIndicatorTestRsp(HfgChannel *Channel)
{
    Channel->flags |= CHANNEL_FLAG_SEND_OK;
    Channel->atResults.type = AT_INDICATORS | AT_TEST;
    Channel->atResults.p.hf.indicators_test.num = 7;
    Channel->atResults.p.hf.indicators_test.ind[0].description = "service";
    Channel->atResults.p.hf.indicators_test.ind[0].range = "0,1";
    Channel->atResults.p.hf.indicators_test.ind[1].description = "call";
    Channel->atResults.p.hf.indicators_test.ind[1].range = "0,1";
    Channel->atResults.p.hf.indicators_test.ind[2].description = "callsetup";
    Channel->atResults.p.hf.indicators_test.ind[2].range = "0,3";
    Channel->atResults.p.hf.indicators_test.ind[3].description = "callheld";
    Channel->atResults.p.hf.indicators_test.ind[3].range = "0,2";
    Channel->atResults.p.hf.indicators_test.ind[4].description = "battchg";
    Channel->atResults.p.hf.indicators_test.ind[4].range = "0,5";
    Channel->atResults.p.hf.indicators_test.ind[5].description = "signal";
    Channel->atResults.p.hf.indicators_test.ind[5].range = "0,5";
    Channel->atResults.p.hf.indicators_test.ind[6].description = "roam";
    Channel->atResults.p.hf.indicators_test.ind[6].range = "0,1";
}

/*---------------------------------------------------------------------------
 *            HfgSetupIndicatorReadRsp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Sets up the response to a Indicator Read command.
 *
 * Return:    void
 */
void HfgSetupIndicatorReadRsp(HfgChannel *Channel)
{
    Channel->flags |= CHANNEL_FLAG_SEND_OK;
    Channel->atResults.type = AT_INDICATORS | AT_READ;
    Channel->atResults.p.hf.indicators_read.num = 7;
    Channel->atResults.p.hf.indicators_read.ind[0].value = Channel->service;
    Channel->atResults.p.hf.indicators_read.ind[1].value = Channel->call;
    Channel->atResults.p.hf.indicators_read.ind[2].value = Channel->callSetup;
    Channel->atResults.p.hf.indicators_read.ind[3].value = Channel->held;
    Channel->atResults.p.hf.indicators_read.ind[4].value = Channel->battery;
    Channel->atResults.p.hf.indicators_read.ind[5].value = Channel->signal;
    Channel->atResults.p.hf.indicators_read.ind[6].value = Channel->roaming;
}

/*---------------------------------------------------------------------------
 *            HfgSetupCallHoldReadRsp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Sets up the response to a Call Hold Read command.
 *
 * Return:    void
 */
void HfgSetupCallHoldReadRsp(HfgChannel *Channel)
{
#if HFG_SDK_FEATURES & HFG_FEATURE_THREE_WAY_CALLS
            Channel->flags |= CHANNEL_FLAG_SEND_OK;
            Channel->atResults.type = AT_CALL_HOLD | AT_TEST;
            Channel->atResults.p.hf.hold_test.flags = 
                                        AT_FLAG_HOLD_RELEASE_HELD_CALLS    |
                                        AT_FLAG_HOLD_RELEASE_ACTIVE_CALLS  |
                                        AT_FLAG_HOLD_HOLD_ACTIVE_CALLS     |
                                        AT_FLAG_HOLD_ADD_HELD_CALL         |
                                        AT_FLAG_HOLD_EXPLICIT_TRANSFER;
#if HFG_SDK_FEATURES & HFG_FEATURE_ENHANCED_CALL_CTRL
            Channel->atResults.p.hf.hold_test.flags |= 
                                        AT_FLAG_HOLD_RELEASE_SPECIFIC_CALL |
                                        AT_FLAG_HOLD_HOLD_SPECIFIC_CALL;

#endif
#else
            Channel->atResults.type = AT_OK;
#endif
}

/*---------------------------------------------------------------------------
 *            HfgCloseChannel()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Clean up the channel state and notify app
 *
 * Return:    void
 */
void HfgCloseChannel(HfgChannel *Channel)
{
    HfgCallbackParms  hfgParms;

    if (Channel->state != HFG_STATE_CLOSED) {
        /* Clean up the state */
        Channel->flags = 0;
        Channel->queryFlags = 0;
        Channel->nrecDisable = FALSE;
        Channel->ibRing = TRUE;

        Channel->state = HFG_STATE_CLOSED;

        /* Release link handler */
        if (Channel->linkFlags & HFG_LINK_ACL) {
            if (CMGR_RemoveDataLink(&Channel->cmgrHandler) == BT_STATUS_SUCCESS) {
                /* Clear the link flags */
                Channel->linkFlags = 0;
            }
        }

        /* No sniff timer needed any more */
        (void)CMGR_ClearSniffTimer(&Channel->cmgrHandler);

        /* Notify the application */
        hfgParms.event = HFG_EVENT_SERVICE_DISCONNECTED;
        hfgParms.status = BT_STATUS_SUCCESS;
        hfgParms.p.remDev = CMGR_GetRemoteDevice(&Channel->cmgrHandler);
        hfgParms.errCode = Channel->cmgrHandler.errCode;
        Channel->cmgrHandler.bdc = 0;
        Channel->callback(Channel, &hfgParms);
    }
}

/*---------------------------------------------------------------------------
 *            HfgFindChannel()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Find the Hfg channel based on the remote device.
 *
 * Return:    A pointer to the hf channel, NULL if not found.
 */
HfgChannel * HfgFindChannel(BtRemoteDevice *remDev)
{
    HfgChannel *hfgChannel;

    hfgChannel = (HfgChannel *)GetHeadList(&HFG(channelList));
    while (&hfgChannel->node != &HFG(channelList)) {
        if ((hfgChannel->cmgrHandler.bdc) && 
            (CMGR_GetRemoteDevice(&hfgChannel->cmgrHandler) == remDev)) {
            break;
        }

        /* Look at the next node */
        hfgChannel = (HfgChannel *)GetNextNode(&hfgChannel->node);
    }

    if (&hfgChannel->node == &HFG(channelList)) {
        /* Not found */
        hfgChannel = 0;
    }

    return hfgChannel;
}

/*---------------------------------------------------------------------------
 *            HfgGetClosedChannel()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Finds an available (closed) channel.
 *
 * Return:    A pointer to the hf channel, NULL if not found.
 */
HfgChannel * HfgGetClosedChannel(void)
{
    HfgChannel *hfgChannel;

    hfgChannel = (HfgChannel *)GetHeadList(&HFG(channelList));
    while (&hfgChannel->node != &HFG(channelList)) {
        if (hfgChannel->state == HFG_STATE_CLOSED) {
            break;
        }

        /* Look at the next node */
        hfgChannel = (HfgChannel *)GetNextNode(&hfgChannel->node);
    }

    if (&hfgChannel->node == &HFG(channelList)) {
        /* Not found */
        hfgChannel = 0;
    }

    return hfgChannel;
}

/*---------------------------------------------------------------------------
 *            HfgAllocRfChannel()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Allocate an RF channel (make it "in use").
 *
 * Return:    A pointer to the RF channel, NULL if not found.
 */
RfChannel *HfgAllocRfChannel(void)
{
    RfChannel *rfChannel = 0;

    if (!IsListEmpty(&HFG(freeRfChannelList))) {
        rfChannel = (RfChannel *)RemoveHeadList(&HFG(freeRfChannelList)); 
    }

    if (rfChannel) {
        rfChannel->callback = HfgRfCallback;
        rfChannel->maxFrameSize = RF_MAX_FRAME_SIZE;
        rfChannel->priority = RF_DEFAULT_PRIORITY;
    }

    return rfChannel;
}

/*---------------------------------------------------------------------------
 *            HfgFreeRfChannel()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Frees an RF channel (makes it available).
 *
 * Return:    void
 */
void HfgFreeRfChannel(RfChannel *rfChannel)
{
    if (!IsNodeOnList(&HFG(freeRfChannelList), &rfChannel->node)) {
        InsertTailList(&HFG(freeRfChannelList), &rfChannel->node);
    }
}

