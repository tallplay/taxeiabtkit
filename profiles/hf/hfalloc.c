/****************************************************************************
 *
 * File:
 *     $Workfile:hfalloc.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:4$
 *
 * Description: This file contains internal memory allocation routines for
 *     the Hands-free unit.
 *             
 * Created:     April 28, 2005
 *
 * Copyright 2000-2005 Extended Systems, Inc.

 * Portions copyright 2005 iAnywhere Solutions, Inc.

 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any means, 
 * or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions,  
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "sys/hfalloc.h"

#if HF_MEMORY_EXTERNAL == XA_DISABLED
#if XA_CONTEXT_PTR == XA_ENABLED
BtHfContext  hfTmp;
BtHfContext *hfContext = &hfTmp;
#else /* XA_CONTEXT_PTR == XA_ENABLED */
BtHfContext hfContext;
#endif /* XA_CONTEXT_PTR */
#endif /* HF_MEMORY_EXTERNAL == XA_DISABLED */

/*---------------------------------------------------------------------------
 * HfAlloc()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initializes the global context for the Hands-free Unit.  This 
 *            function can be replaced if necessary.
 *
 * Return:    void
 */
BOOL HfAlloc(void)
{
#if HF_MEMORY_EXTERNAL == XA_DISABLED

    U8* ptr; 

    /* Fill memory with 0. */
#if XA_CONTEXT_PTR == XA_ENABLED
    ptr = (U8*)hfContext;
#else
    ptr = (U8*)&hfContext;
#endif /* XA_CONTEXT_PTR */

    OS_MemSet(ptr, 0, (U32)sizeof(BtHfContext));

#endif /* HF_MEMORY_EXTERNAL == XA_DISABLED */

    return TRUE;
}

