/****************************************************************************
 *
 * File:
 *     $Workfile:hfgalloc.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:4$
 *
 * Description: This file contains the memory allocation function
 *              the Hands-free profile.
 *             
 * Created:     April 28, 2005
 *
 * Copyright 2001-2005 Extended Systems, Inc.

 * Portions copyright 2005 iAnywhere Solutions, Inc.

 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any means, 
 * or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions,  
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "sys/hfgalloc.h"

#if HFG_MEMORY_EXTERNAL == XA_DISABLED
#if XA_CONTEXT_PTR == XA_ENABLED
BtHfgContext  hfgTmp;
BtHfgContext *hfgContext = &hfgTmp;
#else /* XA_CONTEXT_PTR == XA_ENABLED */
BtHfgContext hfgContext;
#endif /* XA_CONTEXT_PTR */
#endif /* HFG_MEMORY_EXTERNAL == XA_DISABLED */

/*---------------------------------------------------------------------------
 * HfAlloc()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initializes the global context for the Hands-free Unit.  This 
 *            function can be replaced if necessary.
 *
 * Return:    void
 */
BOOL HfgAlloc(void)
{
#if HFG_MEMORY_EXTERNAL == XA_DISABLED

    U8* ptr; 

    /* Fill memory with 0. */
#if XA_CONTEXT_PTR == XA_ENABLED
    ptr = (U8*)hfgContext;
#else
    ptr = (U8*)&hfgContext;
#endif /* XA_CONTEXT_PTR */

    OS_MemSet(ptr, 0, (U32)sizeof(BtHfgContext));

#endif /* HFG_MEMORY_EXTERNAL == XA_DISABLED */

    return TRUE;
}

