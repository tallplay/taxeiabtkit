/****************************************************************************
 *
 * File:
 *     $Workfile:hfg_sm.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:18$
 *
 * Description: This file contains the connection state machine for the 
 *     hands-free component of the Hands-free SDK.
 *             
 * Created:     February 11, 2005
 *
 * Copyright 2000-2005 Extended Systems, Inc.

 * Portions copyright 2005 iAnywhere Solutions, Inc.

 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any means, 
 * or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions,  
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "sys/hfgalloc.h"
#include "btalloc.h"

/*---------------------------------------------------------------------------
 *            HfgStateClosed()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handles the closed state for the HF state machine.
 *
 * Return:    void
 */
static void HfgStateClosed(HfgChannel *Channel, U8 Event, AtCommands *Parms)
{
    if (Event == EVENT_OPEN_IND) {
        /* Incoming connection, initialize channel state */
        if (RF_AcceptChannel(Channel->hfgChannel) == BT_STATUS_PENDING) {
            Channel->state = HFG_STATE_CONN_INCOMING;
            Channel->version = 0;
            Channel->hfFeatures = 0;
            HFG(hfgChannel) = 0;

            Channel->cmgrHandler.errCode = BEC_NO_ERROR;
            HfgAppCallback(Channel, HFG_EVENT_SERVICE_CONNECT_REQ,
                           BT_STATUS_SUCCESS, (U32)Channel->cmgrHandler.bdc->link);
        }
    }

    /* Ignore any other events */
}

/*---------------------------------------------------------------------------
 *            HfgStateConnPending()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handles the connect pending state for the HF state machine.
 *
 * Return:    void
 */
static void HfgStateConnPending(HfgChannel *Channel, U8 Event, AtCommands *Parms)
{
    if (Event == EVENT_OPEN) {
        Channel->state = HFG_STATE_OPEN;
        Channel->flags |= CHANNEL_FLAG_NEGOTIATE;
    }

    /* Ignore any other events */
}

/*---------------------------------------------------------------------------
 *            HfgStateIncoming()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handles the incoming state for the HF state machine.
 *
 * Return:    void
 */
static void HfgStateConnIncoming(HfgChannel *Channel, U8 Event, AtCommands *Parms)
{
    /* Incoming or outgoing, the same negotiation sequence is used */
    HfgStateConnPending(Channel, Event, Parms);
}

/*---------------------------------------------------------------------------
 *            HfgStateOpen()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handles the open state for the HF state machine.
 *
 * Return:    void
 */
static void HfgStateOpen(HfgChannel *Channel, U8 Event, AtCommands *Parms)
{
    BtStatus    status;
    AtResults  *results;

    /* Preinitialize the OK response */
    Channel->cmgrHandler.errCode = BEC_NO_ERROR;
    Channel->atResults.type = AT_OK;
    results = &Channel->atResults;

    switch (Event) {
    case EVENT_AT_DATA:
        switch (Parms->type) {
        case AT_SUPPORTED_FEATURES:
            /* Respond with the supported features */
            Channel->flags |= CHANNEL_FLAG_SEND_OK;
            Channel->atResults.type = AT_SUPPORTED_FEATURES;
            Channel->atResults.p.hf.features.bitmap = HFG_SDK_FEATURES;

            /* Report the hands-free device features */
            Channel->hfFeatures = Parms->p.hf.features.bitmap;
            Channel->flags |= CHANNEL_FLAG_FEATURES_SENT;
            Channel->cmgrHandler.errCode = BEC_NO_ERROR;
            HfgAppCallback(Channel, HFG_EVENT_HANDSFREE_FEATURES, 
                           BT_STATUS_SUCCESS, 
                           (U32)Channel->hfFeatures);
            break;

        case (AT_INDICATORS | AT_TEST):
            HfgSetupIndicatorTestRsp(Channel);
            break;

        case (AT_INDICATORS | AT_READ):
            if (!(Channel->flags & CHANNEL_FLAG_FEATURES_SENT)) {
                if (!(Channel->flags & CHANNEL_FLAG_OUTGOING)) {
                    /* Supported features are not known, and SDP query is necessary */
                    Channel->sdpQueryToken.attribId = AID_PROTOCOL_DESC_LIST;
                    if (HfgStartServiceQuery(Channel, BSQM_FIRST) != 
                        BT_STATUS_PENDING) {
                        /* Can't start the query */
                        (void)RF_CloseChannel(Channel->hfgChannel);
                    }
                } else {
                    Channel->flags |= CHANNEL_FLAG_FEATURES_SENT;
                    Channel->cmgrHandler.errCode = BEC_NO_ERROR;
                    HfgAppCallback(Channel, HFG_EVENT_HANDSFREE_FEATURES, 
                                   BT_STATUS_SUCCESS, 
                                   (U32)Channel->hfFeatures);
                }
            }
            HfgSetupIndicatorReadRsp(Channel);
            break;

        case AT_EVENT_REPORTING:

            if (Parms->p.hf.report.mode == 3) {
                Channel->indEnabled = Parms->p.hf.report.ind;
            }

#if HFG_SDK_FEATURES & HFG_FEATURE_THREE_WAY_CALLS
            if (!(Channel->hfFeatures & HFG_HANDSFREE_FEATURE_CALL_WAITING) &&
                (Channel->flags & CHANNEL_FLAG_NEGOTIATE)) {
                Channel->flags |= CHANNEL_FLAG_NEG_DONE;
            }
#else
            if (Channel->flags & CHANNEL_FLAG_NEGOTIATE) {
                Channel->flags |= CHANNEL_FLAG_NEG_DONE;
            }
#endif
            break;

        case (AT_CALL_HOLD | AT_TEST):
            HfgSetupCallHoldReadRsp(Channel);

#if HFG_SDK_FEATURES & HFG_FEATURE_THREE_WAY_CALLS
            if (Channel->flags & CHANNEL_FLAG_NEGOTIATE) {
                Channel->flags |= CHANNEL_FLAG_NEG_DONE;
            }
#endif
            break;

        case AT_ANSWER:
            HfgAppCallback(Channel, HFG_EVENT_ANSWER_CALL, 
                           BT_STATUS_SUCCESS, (U32)0);
            return;

        case AT_DIAL_NUMBER:
            HfgAppCallback(Channel, HFG_EVENT_DIAL_NUMBER, 
                           BT_STATUS_SUCCESS, (U32)Parms->p.hf.dial.number);
            return;

        case AT_DIAL_MEMORY:
            HfgAppCallback(Channel, HFG_EVENT_MEMORY_DIAL, 
                           BT_STATUS_SUCCESS, (U32)Parms->p.hf.dial.number);
            return;

        case AT_REDIAL:
            HfgAppCallback(Channel, HFG_EVENT_REDIAL, 
                           BT_STATUS_SUCCESS, (U32)0);
            return;

        case AT_CALL_HOLD:
            HfgAppCallback(Channel, HFG_EVENT_CALL_HOLD, 
                           BT_STATUS_SUCCESS, (U32)&Parms->p.hf.hold);
            return;

#if HF_USE_RESP_HOLD == XA_ENABLED
        case AT_RESPONSE_AND_HOLD | AT_READ:
            HfgAppCallback(Channel, HF_QUERY_RESPONSE_HOLD, 
                           BT_STATUS_SUCCESS, (U32)0);
            return;

        case AT_RESPONSE_AND_HOLD:
            HfgAppCallback(Channel, HF_QUERY_RESPONSE_HOLD, 
                           BT_STATUS_SUCCESS, (U32)Parms->p.hf.btrh.setting);
            return;
#endif

        case AT_HANG_UP:
            HfgAppCallback(Channel, HFG_EVENT_HANGUP, 
                           BT_STATUS_SUCCESS, (U32)0);
            return;

        case AT_GENERATE_DTMF_TONE:
            HfgAppCallback(Channel, HFG_EVENT_GENERATE_DTMF, 
                           BT_STATUS_SUCCESS, (U32)Parms->p.hf.dtmf.tone);
            return;

#if HFG_SDK_FEATURES & HFG_FEATURE_VOICE_TAG
        case AT_VOICE_TAG:
            HfgAppCallback(Channel, HFG_EVENT_GET_LAST_VOICE_TAG, 
                           BT_STATUS_SUCCESS, (U32)0);
            return;
#endif

#if HFG_SDK_FEATURES & HFG_FEATURE_VOICE_RECOGNITION
        case AT_VOICE_RECOGNITION:
            Channel->voiceRec = Parms->p.hf.vrec.enabled;
            HfgAppCallback(Channel, HFG_EVENT_ENABLE_VOICE_RECOGNITION, 
                           BT_STATUS_SUCCESS, (U32)Parms->p.hf.vrec.enabled);
            break;
#endif

#if HFG_SDK_FEATURES & HFG_FEATURE_ECHO_NOISE
        case AT_ECHO_C_AND_NOISE_R:
            Channel->nrecDisable = TRUE;
            HfgAppCallback(Channel, HFG_EVENT_DISABLE_NREC, 
                           BT_STATUS_SUCCESS, (U32)0);
            break;
#endif

        case AT_LIST_CURRENT_CALLS:
            HfgAppCallback(Channel, HFG_EVENT_LIST_CURRENT_CALLS, 
                           BT_STATUS_SUCCESS, (U32)0);
            return;

        case AT_CALL_ID:
            Channel->callId = Parms->p.hf.callId.enabled;
            HfgAppCallback(Channel, HFG_EVENT_ENABLE_CALLER_ID, 
                           BT_STATUS_SUCCESS, (U32)Parms->p.hf.callId.enabled);
            break;

        case AT_CALL_WAIT_NOTIFY:
            Channel->callWaiting = Parms->p.hf.wait.notify;
            HfgAppCallback(Channel, HFG_EVENT_ENABLE_CALL_WAITING, 
                           BT_STATUS_SUCCESS, (U32)Parms->p.hf.wait.notify);
            break;
            
        case AT_SUBSCRIBER_NUM:
            HfgAppCallback(Channel, HFG_EVENT_QUERY_SUBSCRIBER_NUMBER, 
                           BT_STATUS_SUCCESS, (U32)0);
            return;

        case AT_NETWORK_OPERATOR:
            if ((Parms->p.hf.networkOper.mode == 3) &&
                (Parms->p.hf.networkOper.format == 0)) {
            } else {
                if (Channel->extendedErrors) {
                    Channel->atResults.type = AT_EXTENDED_ERROR;
                    Channel->atResults.p.error.type = ATCME_OP_NOT_SUPPORTED;
                } else {
                    Channel->atResults.type = AT_ERROR;
                }
            }
            break;
            
        case (AT_NETWORK_OPERATOR | AT_READ):
            HfgAppCallback(Channel, HFG_EVENT_QUERY_NETWORK_OPERATOR, 
                           BT_STATUS_SUCCESS, (U32)0);
            return;

        case AT_SET_ERROR_MODE:
            if (Parms->p.error.mode == 1) {
                Channel->extendedErrors = TRUE;
                HfgAppCallback(Channel, HFG_EVENT_ENABLE_EXTENDED_ERRORS, 
                               BT_STATUS_SUCCESS, (U32)0);
            } else {
                if (Channel->extendedErrors) {
                    Channel->atResults.type = AT_EXTENDED_ERROR;
                    Channel->atResults.p.error.type = ATCME_OP_NOT_SUPPORTED;
                } else {
                    Channel->atResults.type = AT_ERROR;
                }
            }
            break;

        case AT_MICROPHONE_GAIN:
            HfgAppCallback(Channel, HFG_EVENT_REPORT_MIC_VOLUME, 
                           BT_STATUS_SUCCESS, (U32)Parms->p.hs.mic.gain);
            break;

        case AT_SPEAKER_GAIN:
            HfgAppCallback(Channel, HFG_EVENT_REPORT_SPK_VOLUME, 
                           BT_STATUS_SUCCESS, (U32)Parms->p.hs.speaker.gain);
            break;
            
        default:
            if (Channel->extendedErrors) {
                Channel->atResults.type = AT_EXTENDED_ERROR;
                Channel->atResults.p.error.type = ATCME_OP_NOT_SUPPORTED;
            } else {
                Channel->atResults.type = AT_ERROR;
            }
            break;
        }
        break;

    case EVENT_RF_PACKET_SENT:
        if (Channel->lastAtResults == &Channel->atResults) {
            
            /* Results sent internally */
            if (Channel->flags & CHANNEL_FLAG_NEG_DONE) {
                /* Negotiation is complete */
                Channel->flags &= ~(CHANNEL_FLAG_NEG_DONE | 
                                    CHANNEL_FLAG_NEGOTIATE);

                /* Service channel is up, tell the app */
                HfgReportServiceConnected(Channel);
            }

            if (!(Channel->flags & CHANNEL_FLAG_SEND_OK)) {
                /* Done sending */
                return;
            }

            /* Send OK */
            Channel->flags &= ~CHANNEL_FLAG_SEND_OK;
        } else {
            if (Channel->flags & CHANNEL_FLAG_SEND_OK) {
                
                /* Send OK */
                Channel->lastAtResults->type = AT_OK;
                Channel->flags &= ~CHANNEL_FLAG_SEND_OK;
                results = Channel->lastAtResults;
            } else {

                /* Results sent from application */
                HfgAppCallback(Channel, HFG_EVENT_RESPONSE_COMPLETE, 
                               BT_STATUS_SUCCESS, (U32)Channel->lastAtResults);
                return;
            }
        }
        break;

    default:
        Assert(0);
        break;
    }

    status = AtSendResults(Channel, results);
    if (status != BT_STATUS_PENDING) {
        /* Fatal error sending response, disconnect */
        (void)RF_CloseChannel(Channel->hfgChannel);
    }
}

/*---------------------------------------------------------------------------
 *            HfInitStateMachine()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initializes the HF state machine.
 *
 * Return:    void
 */
void HfgInitStateMachine(void)
{
    HFG(hfgState)[0] = HfgStateClosed;
    HFG(hfgState)[1] = HfgStateConnPending;
    HFG(hfgState)[2] = HfgStateConnIncoming;
    HFG(hfgState)[3] = HfgStateOpen;
}

/*---------------------------------------------------------------------------
 *            HfRfCallback()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  RFCOMM callback for the HF state machine.
 *
 * Return:    void
 */
void HfgRfCallback(RfChannel *Channel, RfCallbackParms *Parms)
{
    HfgChannel  *HfgChannel;
    AtCommands  atParms;
    U8          event = 0;
    U16         offset = 0;
    RfChannel  *rfChannel;

    if ((Parms->event == RFEVENT_OPEN_IND) ||
        (Parms->event == RFEVENT_OPEN) ||
        (Parms->event == RFEVENT_CLOSED)) {
        HfgChannel = HfgFindChannel(Parms->ptrs.remDev);
    } else {
        HfgChannel = HfgFindChannel(RF_RemoteDevice(Channel));
    }
    if (HfgChannel) {
        HfgChannel->hfgChannel = Channel;
    }

    switch (Parms->event) {
    case RFEVENT_OPEN_IND:

        /* Find the device */
        Report(("HFG: RFCOMM incoming hands-free connection\n"));

        if (!HfgChannel) {
            /* Get a new channel */
            HfgChannel = HfgGetClosedChannel();
            if (HfgChannel) {
                /* Found a new device */
                HfgChannel->hfgChannel = Channel;

                if (CMGR_CreateDataLink(&HfgChannel->cmgrHandler, 
                                        &Parms->ptrs.remDev->bdAddr) !=
                    BT_STATUS_SUCCESS) {
                    /* Could not create a link to the ACL */
                    (void)RF_RejectChannel(Channel);
                    return;
                }
                 
                if (CMGR_RegisterScoHandler(&HfgChannel->cmgrHandler) != BT_STATUS_SUCCESS) {
                    /* Could not register a SCO handler, reject connection */
                    (void)RF_RejectChannel(Channel);
                    (void)CMGR_RemoveDataLink(&HfgChannel->cmgrHandler);
                    return;
                }

                HfgChannel->linkFlags |= HFG_LINK_ACL;
                event = EVENT_OPEN_IND;
            } else {
                /* Reject the channel */
                (void)RF_RejectChannel(Channel);
            }
        } else {
            /* A connection exists to this device */
            (void)RF_RejectChannel(Channel);
        }
        break;

    case RFEVENT_OPEN:

        /* Register a SCO handler */
        if (HfgChannel) {
            if (HFG(hfgChannel) == 0) {
                rfChannel = HfgAllocRfChannel();
                if (rfChannel) {
                    if (RF_RegisterServerChannel(rfChannel, &HFG(hfgService), 1) ==
                        BT_STATUS_SUCCESS) {
                        HFG(hfgChannel) = rfChannel;
                        rfChannel->userContext = (void *)(U32)1;
                    } else {
                        HfgFreeRfChannel(rfChannel);
                        HFG(hfgChannel) = 0;
                    }
                }
            }

            Report(("HFG: RFCOMM hands-free connection established\n"));
            HfgChannel->linkFlags |= HFG_LINK_HANDSFREE;
            event = EVENT_OPEN;
        }
        break;

    case RFEVENT_DATA_IND:

        if (HfgChannel) {
            Report(("HFG: RFCOMM hands-free data received\n"));
            RF_AdvanceCredit(Channel, 1);

            event = EVENT_AT_DATA;
            while (offset < Parms->dataLen) {
                if (HfgParseRfcommData(HfgChannel, Parms, &offset, &atParms) == BT_STATUS_SUCCESS) {
                    HFG(hfgState)[HfgChannel->state](HfgChannel, event, &atParms);
                }
            }
        }
        return;

    case RFEVENT_PACKET_HANDLED:

        if (HfgChannel) {
            if ((HfgChannel->bytesToSend > 0) &&
                (Parms->status == BT_STATUS_SUCCESS)) {
                /* Send more data */
                Parms->ptrs.packet->data += Parms->ptrs.packet->dataLen;
                Parms->ptrs.packet->dataLen = HfgChannel->bytesToSend;
                if (HfgAtSendRfPacket(HfgChannel, 
                                   Parms->ptrs.packet) != BT_STATUS_PENDING) {
                    Report(("HFG: Unable to send RFCOMM packet\n"));
                    HfgChannel->cmgrHandler.errCode = BEC_LOCAL_TERMINATED;
                    HfgCloseChannel(HfgChannel);
                }
                return;
            } else {
                AtSendComplete(HfgChannel, Parms->ptrs.packet);
            }
            event = EVENT_RF_PACKET_SENT;
        } else {
            return;
        }
        break;

    case RFEVENT_CLOSED:

        Report(("HFG: RFCOMM hands-free connection closed\n"));

        if (Channel->userContext) {
            if (RF_DeregisterServerChannel(Channel, &HFG(hfgService)) == BT_STATUS_SUCCESS) {
                HfgFreeRfChannel(Channel);
                Channel->userContext = 0;
            }
        } else {
            HfgFreeRfChannel(Channel);
        }

        if (HFG(hfgChannel) == 0) {
            rfChannel = HfgAllocRfChannel();
            if (rfChannel) {
                if (RF_RegisterServerChannel(rfChannel, &HFG(hfgService), 1) ==
                    BT_STATUS_SUCCESS) {
                    HFG(hfgChannel) = rfChannel;
                    rfChannel->userContext = (void *)(U32)1;
                } else {
                    HfgFreeRfChannel(rfChannel);
                    HFG(hfgChannel) = 0;
                }
            }
        }

        if (HfgChannel) {
            HfgChannel->linkFlags &= ~HFG_LINK_HANDSFREE;
            CMGR_DeregisterScoHandler(&HfgChannel->cmgrHandler);

            HfgChannel->cmgrHandler.errCode = BEC_USER_TERMINATED;
            if (CMGR_GetLinkState(&HfgChannel->cmgrHandler) == BDS_CONNECTED) {
                HfgCloseChannel(HfgChannel);
            }
        }
        return;

    default:
        /* Ignore other events */
        return;
    }

    HFG(hfgState)[HfgChannel->state](HfgChannel, event, &atParms);
}

