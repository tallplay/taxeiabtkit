/****************************************************************************
 *
 * File:
 *     $Workfile:hf_cmgr.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:32$
 *
 * Description: This file contains the call state manager for the Hf SDK.
 *             
 * Created:     Februrary 11, 2005
 *
 * Copyright 2000-2005 Extended Systems, Inc.

 * Portions copyright 2005 iAnywhere Solutions, Inc.

 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any means, 
 * or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions,  
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "sys/hf_cmgr.h"
#include "sys/hf_cmd.h"
#include "sys/hfalloc.h"

#if HF_USE_CALL_MANAGER == XA_ENABLED

static void CallMgrPollTimeoutHandler(EvmTimer *timer);

/*---------------------------------------------------------------------------
 *            CallMgrSendPoll()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Polls the call state from the audio gateway.
 *
 * Return:    void
 */
static BtStatus CallMgrSendPoll(HfChannel *Channel)
{
    RfChannel *rfChannel;
    TimeT pInterval = HF_POLL_TIMEOUT;

    if (((!(Channel->pollFlags & CALLMGR_POLL_ATTEMPTED)) || 
         (Channel->pollFlags & CALLMGR_POLL_RELIABLE)) && 
        (!(Channel->pollFlags & CALLMGR_POLL_STARTED))) {
#if HF_POLL_TIMEOUT > 0
        Channel->pollFlags |= CALLMGR_POLL_STARTED;
        Channel->pollTimer.func = CallMgrPollTimeoutHandler;
        Channel->pollTimer.context = Channel;
        if (PL_FAST == Channel->hfPollInterval) {
            pInterval = HF_POLL_TIMEOUT_ACTIVE;
        }
        EVM_StartTimer(&Channel->pollTimer, pInterval);
#endif
    }

    if (Channel->pollFlags & CALLMGR_POLL_LIST_CALLS) {
        /* Already polling */
        return BT_STATUS_PENDING;
    }

    if ((!(Channel->pollFlags & CALLMGR_POLL_ATTEMPTED)) || 
        (Channel->pollFlags & CALLMGR_POLL_RELIABLE)) {
        /* Get call list */
        Channel->pollCommand.type = AT_LIST_CURRENT_CALLS;
        Channel->pollFlags |= CALLMGR_POLL_LIST_CALLS;

        rfChannel = Channel->hfChannel;

        return AtSendCommand(Channel, &Channel->pollCommand);
    }
    return BT_STATUS_CANCELLED;
}                                                                    

/*---------------------------------------------------------------------------
 *            CallMgrRingTimeoutHandler()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Sets call state to none when no RING command is received within 
 *            the specified time.
 *
 * Return:    void
 */
static void CallMgrRingTimeoutHandler(EvmTimer *timer)
{
    HfChannel *channel = timer->context;

    timer->context = 0;
    CallManager(channel, EG_STATE_MACHINE, EVENT_RING_TIMEOUT, 0);
}

/*---------------------------------------------------------------------------
 *            CallMgrPollTimeoutHandler()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Polls the call state from the audio gateway.
 *
 * Return:    void
 */
static void CallMgrPollTimeoutHandler(EvmTimer *timer)
{
    HfChannel *channel = timer->context;
    TimeT pInterval = HF_POLL_TIMEOUT;

    channel->pollTimer.context = 0;
    if (CallMgrSendPoll(channel) != BT_STATUS_PENDING) {
        /* Failed to send, reset flags */
        channel->pollFlags &= (CALLMGR_POLL_ATTEMPTED | CALLMGR_POLL_RELIABLE);
        if ((!(channel->pollFlags & CALLMGR_POLL_ATTEMPTED)) || 
            (channel->pollFlags & CALLMGR_POLL_RELIABLE)) {
            channel->pollTimer.context = channel;
            if (PL_FAST == channel->hfPollInterval) {
                pInterval = HF_POLL_TIMEOUT_ACTIVE;
            }
            EVM_StartTimer(timer, pInterval);
        }
    }
}

/*---------------------------------------------------------------------------
 *            CallMgrQuickPoll()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Sets a quick timer to polls the call state from the audio gateway.
 *
 * Return:    void
 */
static void CallMgrQuickPoll(HfChannel *Channel)
{
    if ((!(Channel->pollFlags & CALLMGR_POLL_ATTEMPTED)) || 
        (Channel->pollFlags & CALLMGR_POLL_RELIABLE)) {
        if (Channel->pollFlags & CALLMGR_POLL_LIST_CALLS) {
            /* Already polling */
            EVM_CancelTimer(&Channel->pollTimer);
        }

        Channel->pollTimer.func = CallMgrPollTimeoutHandler;
        Channel->pollTimer.context = Channel;
        EVM_StartTimer(&Channel->pollTimer, 1000);
    }
}

/*---------------------------------------------------------------------------
 *            CallMgrHandleCallsetup()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handleds the callsetup indicator.
 *
 * Return:    void
 */
static void CallMgrHandleCallsetup(HfChannel *Channel, U8 value)
{
    if (value == HF_CALL_SETUP_IN) {
        /* Incoming call */
        if ((Channel->callState[0] == HF_CALL_STATUS_NONE) ||
            (Channel->callState[0] == HF_CALL_STATUS_INCOMING)) {
            if (!(Channel->pollFlags & CALLMGR_POLL_RELIABLE)) {
                Channel->callState[0] = HF_CALL_STATUS_INCOMING;
            }
            Channel->currentContext = 0;
        } else {
            if (Channel->callWaiting) {
                if (!(Channel->pollFlags & CALLMGR_POLL_RELIABLE)) {
                    if ((Channel->callState[1] == HF_CALL_STATUS_NONE) ||
                        (Channel->callState[1] == HF_CALL_STATUS_INCOMING)) {
                        Channel->callState[1] = HF_CALL_STATUS_INCOMING;
                        Channel->currentContext = 1;
                    }
                }
            }
        }
    } else if ((value == HF_CALL_SETUP_OUT) ||
               (value == HF_CALL_SETUP_ALERT)) {
        /* Outgoing call */
        if ((Channel->callState[0] == HF_CALL_STATUS_NONE) ||
            (Channel->callState[0] == HF_CALL_STATUS_DIALING) ||
            (Channel->callState[0] == HF_CALL_STATUS_ALERTING)) {
            if (!(Channel->pollFlags & CALLMGR_POLL_RELIABLE)) {
                if (Channel->callState[0] != HF_CALL_STATUS_ALERTING) {
                    Channel->callState[0] = value;
                    if (Channel->callState[1] == HF_CALL_STATUS_ACTIVE) {
                        Channel->callState[1] = HF_CALL_STATUS_HELD;
                    }
                }
            }
            Channel->currentContext = 0;
        } else {
            if (Channel->callWaiting) {
                if (!(Channel->pollFlags & CALLMGR_POLL_RELIABLE)) {
                    if ((Channel->callState[1] == HF_CALL_STATUS_NONE) ||
                        (Channel->callState[1] == HF_CALL_STATUS_DIALING) ||
                        (Channel->callState[1] == HF_CALL_STATUS_ALERTING)) {
                        if (Channel->callState[1] != HF_CALL_STATUS_ALERTING) {
                            Channel->callState[1] = value;
                            if (Channel->callState[0] == HF_CALL_STATUS_ACTIVE) {
                                Channel->callState[0] = HF_CALL_STATUS_HELD;
                            }
                        }
                        Channel->currentContext = 1;
                    }
                }
            }
        }
    } else if (value == HF_CALL_SETUP_NONE) {
        if (!(Channel->pollFlags & CALLMGR_POLL_RELIABLE)) {
            if ((Channel->callState[0] == HF_CALL_STATUS_INCOMING) ||
                (Channel->callState[0] == HF_CALL_STATUS_DIALING) ||
                (Channel->callState[0] == HF_CALL_STATUS_ALERTING)) {
                Channel->callState[0] = HF_CALL_STATUS_NONE;
            } else if ((Channel->callState[1] == HF_CALL_STATUS_INCOMING) ||
                       (Channel->callState[1] == HF_CALL_STATUS_DIALING) ||
                       (Channel->callState[1] == HF_CALL_STATUS_ALERTING)) {
                Channel->callState[1] = HF_CALL_STATUS_NONE;
            }
        }
    }

    return;
}

/*---------------------------------------------------------------------------
 *            CallMgrSetupIndications()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Indicates whether the AGW supports call setup indication 
 *            reporting.
 *
 * Return:    TRUE  - The AGW indicates call setup changes.
 *            FALSE - The AGW does not indicate call setup changes.
 */
BOOL CallMgrSetupIndications( void )
{

    BOOL retVal = TRUE;
    BOOL checkingInd;

    U8 indNdx;

    /* Loop through the indicator mapping table and determine 
     * whether the AGW supports call setup indication reporting.  
     * If not, we will have to make some (occassionally 
     * invalid) assumptions about the call setup state.
     */ 
    indNdx = 0;
    do {
        checkingInd = FALSE;
        if (indNdx < HF(numInd)) {
            if ((HF(indMap)[indNdx] != AT_IND_SETUP) &&
                (HF(indMap)[indNdx] != AT_IND_SETUP2)) {
                checkingInd = TRUE;
                indNdx++;
            }
        } else {
            retVal = FALSE;
        }
    } while(checkingInd);
    return(retVal);
}

/*---------------------------------------------------------------------------
 *            CallManager()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Tracks and manages the call state.
 *
 * Return:    TRUE - The event was handled internally.
 *            FALSE - The event was not handled.
 */
BOOL CallManager(HfChannel *Channel, 
                 EventGroupType EventGroup, 
                 U16 Event, 
                 U32 Parms)
{
    HfCallStateParms stateParms;
    BOOL             handled = FALSE;
    HfCommand       *command = HfGetCurrentCommand(Channel);
    AtResults       *result = (AtResults *)Parms;
    HfCallIdParms    callId;
    HfCallStatus     saveState1 = Channel->callState[0];
    HfCallStatus     saveState2 = Channel->callState[1];
    TimeT            pInterval = HF_POLL_TIMEOUT;
    U8               index;
    BOOL             reportsCallSetup;

    Assert(0 == (0x0FFF & EventGroup));
    Assert(0 == (0xF000 & Event));

    reportsCallSetup = CallMgrSetupIndications();
    switch (EventGroup + Event) {
    case EG_STATE_MACHINE + EVENT_POLL_COMPLETE:
        if ((U8)Parms == AT_OK) {
            if (Channel->pollFlags & CALLMGR_POLL_LIST_CALLS) {
                /* Polling for list of calls has successfully completed */
                Channel->pollFlags &= ~CALLMGR_POLL_LIST_CALLS;
                if (!(Channel->pollFlags & CALLMGR_POLL_ATTEMPTED)) {
                    Channel->pollFlags |= CALLMGR_POLL_ATTEMPTED;
                }
                if (!(Channel->pollFlags & CALLMGR_POLL_RELIABLE)) {
                    /* Indicate reliability to the application */
                    Channel->pollFlags |= CALLMGR_POLL_RELIABLE;
                    HfAppCallback(Channel, HF_EVENT_CALL_LISTING_ENABLED, 
                                   BT_STATUS_SUCCESS, 0);
                }

                if (!(Channel->pollFlags & CALLMGR_POLL_RESP_RECVD_1)) {
                    /* No unsolicited result code received - no calls */
                    Channel->callState[0] = HF_CALL_STATUS_NONE;
                } 
                if (!(Channel->pollFlags & CALLMGR_POLL_RESP_RECVD_2)) {
                    Channel->callState[1] = HF_CALL_STATUS_NONE;
                }
                 
                /* Response was received and handled */
                Channel->pollFlags &= ~CALLMGR_POLL_RESP_RECVD_1;
                Channel->pollFlags &= ~CALLMGR_POLL_RESP_RECVD_2;

                handled = TRUE;

#if HF_POLL_TIMEOUT > 0

                if ((!(Channel->pollFlags & CALLMGR_POLL_ATTEMPTED)) || 
                    (Channel->pollFlags & CALLMGR_POLL_RELIABLE)) {
                    /* Start the poll timer again */
                    if (!Channel->pollTimer.context) {
                        /* Poll timer is not currently running */
                        Channel->pollTimer.context = Channel;
                        if (PL_FAST == Channel->hfPollInterval) {
                            pInterval = HF_POLL_TIMEOUT_ACTIVE;
                        }
                        EVM_StartTimer(&Channel->pollTimer, pInterval);
                    }
                }
#endif
            } 
        } else {
            /* Error case */
            if (Channel->pollFlags & CALLMGR_POLL_LIST_CALLS) {
                if (!(Channel->pollFlags & CALLMGR_POLL_ATTEMPTED)) {
                    Channel->pollFlags |= CALLMGR_POLL_ATTEMPTED;
                    Channel->pollFlags &= ~CALLMGR_POLL_RELIABLE;
                }
                /* CLCC not supported */ 
                Channel->pollFlags &= ~CALLMGR_POLL_LIST_CALLS;
                handled = TRUE;

            } 
        }
        Channel->pollFlags |= CALLMGR_POLL_ATTEMPTED;
        break;
    case EG_STATE_MACHINE + EVENT_COMMAND_COMPLETE:
        if (command->status == BT_STATUS_SUCCESS) {
            if (command->callMgrFlags & CALLMGR_FLAG_ACCEPT) {
                /* HF Accept incoming call, hold active */
                command->callMgrFlags &= ~CALLMGR_FLAG_ACCEPT;
                Channel->callState[Channel->currentContext] = HF_CALL_STATUS_ACTIVE;

                /* See if other line was active */
                if (Channel->callState[(Channel->currentContext + 1) % 2] ==
                    HF_CALL_STATUS_ACTIVE) {
                    /* Other line is now held */
                    Channel->callState[(Channel->currentContext + 1) % 2] = 
                        HF_CALL_STATUS_HELD;
                }
            } else if (command->callMgrFlags & CALLMGR_FLAG_REPLACE) {
                /* HF Accept incoming call, drop active */
                command->callMgrFlags &= ~CALLMGR_FLAG_REPLACE;
                Channel->callState[Channel->currentContext] = HF_CALL_STATUS_ACTIVE;

                /* See if other line was active */
                if (Channel->callState[(Channel->currentContext + 1) % 2] ==
                    HF_CALL_STATUS_ACTIVE) {
                    /* Other line is now dropped */
                    Channel->callState[(Channel->currentContext + 1) % 2] = 
                        HF_CALL_STATUS_NONE;
                }
            } else if (command->callMgrFlags & CALLMGR_FLAG_HANGUP) {
                /* Hang up active call, held goes active.
                 */
                command->callMgrFlags &= ~CALLMGR_FLAG_HANGUP;
                Channel->callState[Channel->currentContext] = 
                    HF_CALL_STATUS_NONE;

                /* See if other line was held */
                if (Channel->callState[(Channel->currentContext + 1) % 2] ==
                    HF_CALL_STATUS_HELD) {
                    /* Other line goes from held to active */
                    Channel->callState[(Channel->currentContext + 1) % 2] = 
                        HF_CALL_STATUS_ACTIVE;
                /* See if other line was also active (conferenced) */
                } else if (Channel->callState[(Channel->currentContext + 1) % 2] ==
                           HF_CALL_STATUS_ACTIVE) {
                    /* Other line in three-way call goes from active to none */
                    Channel->callState[(Channel->currentContext + 1) % 2] = 
                        HF_CALL_STATUS_NONE;
                }
            } else if (command->callMgrFlags & CALLMGR_FLAG_RELEASE) {
                /* Release all held calls */
                command->callMgrFlags &= ~CALLMGR_FLAG_RELEASE;
                if (Channel->callState[0] == HF_CALL_STATUS_HELD) {
                    Channel->callState[0] = HF_CALL_STATUS_NONE;
                }
                if (Channel->callState[1] == HF_CALL_STATUS_HELD) {
                    Channel->callState[1] = HF_CALL_STATUS_NONE;
                }
            } else if (command->callMgrFlags & CALLMGR_FLAG_HOLD) {
                /* Change the current active call to held.  If the other 
                 * call was held, change it to active.  If the other 
                 * call was also active, then a conference call is in 
                 * effect, and the other call is also changed to held.
                 */ 
                command->callMgrFlags &= ~CALLMGR_FLAG_HOLD;
                Channel->callState[Channel->currentContext] = HF_CALL_STATUS_HELD;

                /* See if other line was held or active */
                if (Channel->callState[(Channel->currentContext + 1) % 2] ==
                    HF_CALL_STATUS_HELD) {
                    /* Other line is now active */
                    Channel->callState[(Channel->currentContext + 1) % 2] = 
                        HF_CALL_STATUS_ACTIVE;
                } else if (Channel->callState[(Channel->currentContext + 1) % 2] ==
                           HF_CALL_STATUS_ACTIVE) {
                    /* Other line is now held */
                    Channel->callState[(Channel->currentContext + 1) % 2] = 
                        HF_CALL_STATUS_HELD;
                }
            } else if (command->callMgrFlags & CALLMGR_FLAG_ACTIVATE) {
                /* Held calls become active */
                command->callMgrFlags &= ~CALLMGR_FLAG_ACTIVATE;
                Channel->callState[Channel->currentContext] = HF_CALL_STATUS_ACTIVE;

                /* See if other line was held */
                if (Channel->callState[(Channel->currentContext + 1) % 2] ==
                    HF_CALL_STATUS_HELD) {
                    /* Other line is now active */
                    Channel->callState[(Channel->currentContext + 1) % 2] = 
                        HF_CALL_STATUS_ACTIVE;
                }
            } else if (command->callMgrFlags & CALLMGR_FLAG_REJECT) {
                /* Reject incoming (or outgoing), 2nd line stays same */
                command->callMgrFlags &= ~CALLMGR_FLAG_REJECT;

                /* If the AGW provides call setup indications, then wait for 
                 * the call setup state to be reported.  If not, then assume 
                 * (sometimes incorrectly) that the call has been rejected.
                 */
                if (FALSE == reportsCallSetup) {
                    Channel->callState[Channel->currentContext] =
                        HF_CALL_STATUS_NONE;
                }
            } else if (command->callMgrFlags & CALLMGR_FLAG_CALL_OUTGOING) {
                command->callMgrFlags &= ~CALLMGR_FLAG_CALL_OUTGOING;

                /* If the AGW provides call setup indications, then wait for 
                 * the call setup state to be reported.  If not, then assume 
                 * (sometimes incorrectly) that active calls on the other 
                 * line are put on hold so that the outgoing call can be made.
                 */
                if (FALSE == reportsCallSetup) {
                    if (Channel->callState[
                        (Channel->currentContext + 1) % 2] == 
                            HF_CALL_STATUS_ACTIVE) {
                        /* Other line is now held */
                        Channel->callState[(Channel->currentContext + 1) % 2] = 
                            HF_CALL_STATUS_HELD;
                    }
                }
            } else if (command->callMgrFlags & CALLMGR_FLAG_CONFERENCE) {
                /* A call was added to the conference */
                command->callMgrFlags &= ~CALLMGR_FLAG_CONFERENCE;
                Channel->callState[0] = HF_CALL_STATUS_ACTIVE;
                Channel->callState[1] = HF_CALL_STATUS_NONE;
                Channel->multiParty = TRUE;
            }
        } else {
            if (BT_STATUS_TIMEOUT != command->status) {

                /* A command message was sent, and 
                 * an Error response was received.
                 */ 

                if ((command->callMgrFlags & CALLMGR_FLAG_CALL_OUTGOING) &&
                    (Channel->callState[Channel->currentContext] 
                        == HF_CALL_STATUS_DIALING)) {
                    command->callMgrFlags &= ~CALLMGR_FLAG_CALL_OUTGOING;

                    /* If the AGW provides call setup indications, 
                     * then wait for the call setup state to be 
                     * reported.  If not, then assume (sometimes 
                     * incorrectly) that the call has been rejected.
                     */
                    if (FALSE == reportsCallSetup) {
                        Channel->callState[Channel->currentContext] =
                            HF_CALL_STATUS_NONE;
                    }
                }

                command->callMgrFlags = 0;
            }
        }

        /* Poll for call state */
        (void)CallMgrQuickPoll(Channel);
        break;

    case EG_STATE_MACHINE + EVENT_RING_TIMEOUT:
        if (Channel->callState[0] == HF_CALL_STATUS_INCOMING) {
            Channel->callState[0] = HF_CALL_STATUS_NONE;
        } else if (Channel->callState[1] == HF_CALL_STATUS_INCOMING) {
            Channel->callState[1] = HF_CALL_STATUS_NONE;
        }
        break;

    case EG_STATE_MACHINE + EVENT_RF_PACKET_SENT:
        /* 6310i does not return OK when dialing, indicate the state change here */
        if (Channel->callState[0] == HF_CALL_STATUS_DIALING) {
            saveState1 = HF_CALL_STATUS_UNKNOWN;
        } else if (Channel->callState[1] == HF_CALL_STATUS_DIALING) {
            saveState2 = HF_CALL_STATUS_UNKNOWN;
        }
        break;

    case EG_HFEVENT + HF_EVENT_SERVICE_CONNECTED:
        if (!(Channel->pollFlags & CALLMGR_POLL_STARTED)) {
            /* This is the first time indicators have been read, 
             * set the initial state and start polling.
            */
            saveState1 = HF_CALL_STATUS_UNKNOWN;
            saveState2 = HF_CALL_STATUS_UNKNOWN;

            /* Poll for call state */
            (void)CallMgrQuickPoll(Channel);
        }
        break;

    case EG_AT_INTERNAL_POLL + AT_IND_POLL_CALL:
    case EG_AT_INTERNAL_POLL + AT_IND_POLL_SETUP:

        switch (Event) {
        case AT_IND_POLL_CALL:
            /* Call state */
            if ((Channel->callState[0] != HF_CALL_STATUS_DIALING) &&
                (Channel->callState[0] != HF_CALL_STATUS_ALERTING) &&
                (Channel->callState[0] != HF_CALL_STATUS_INCOMING)) {
                if ((Parms == HF_CALL_NONE)) {
                    /* No calls */
                    Channel->callState[0] = HF_CALL_STATUS_NONE;
                    Channel->callState[1] = HF_CALL_STATUS_NONE;
                } else {
                    if ((Channel->callState[0] == HF_CALL_STATUS_NONE) &&
                        (Channel->callState[1] == HF_CALL_STATUS_NONE)) {
                        /* One new call exists */
                        Channel->callState[0] = HF_CALL_STATUS_ACTIVE;
                        Channel->callState[1] = HF_CALL_STATUS_NONE;
                    }
                }
            }
            break;
        case AT_IND_POLL_SETUP:
            /* Call setup */
            CallMgrHandleCallsetup(Channel, (U8)Parms);
            break;
        }
        break;

#if HF_USE_RESP_HOLD == XA_ENABLED
    case EG_AT_HANDSFREE + AT_RESPONSE_AND_HOLD:
        /* Poll for call state */
        (void)CallMgrQuickPoll(Channel);
        break;
#endif

    case EG_AT_PROCESSOR + AT_NO_CARRIER:
    case EG_AT_PROCESSOR + AT_BUSY:
    case EG_AT_PROCESSOR + AT_NO_ANSWER:
        if ((Channel->callState[0] == HF_CALL_STATUS_DIALING) ||
            (Channel->callState[0] == HF_CALL_STATUS_ALERTING)) {
            Channel->callState[0] = HF_CALL_STATUS_NONE;
            handled = TRUE;
        } else {
            if ((Channel->callState[1] == HF_CALL_STATUS_DIALING) ||
                (Channel->callState[1] == HF_CALL_STATUS_ALERTING)) {
                Channel->callState[1] = HF_CALL_STATUS_NONE;
                handled = TRUE;
            }
        }
        break;

    case EG_AT_INTERNAL + AT_IND_CALL:
    case EG_AT_INTERNAL + AT_IND_SETUP:
    case EG_AT_INTERNAL + AT_IND_CALLHELD:

        handled = TRUE;

        /* Set the call state */
        switch (Event) {
        case AT_IND_CALL:
            if (!(Channel->pollFlags & CALLMGR_POLL_RELIABLE)) {
                if (Parms == HF_CALL_NONE) {
                    /* No held or active calls exist */
                    if ((HF_CALL_STATUS_ACTIVE == Channel->callState[0]) ||
                        (HF_CALL_STATUS_HELD == Channel->callState[0])) {
                        Channel->callState[0] = HF_CALL_STATUS_NONE;
                    }
                    if ((HF_CALL_STATUS_ACTIVE == Channel->callState[1]) ||
                        (HF_CALL_STATUS_HELD == Channel->callState[1])) {
                        Channel->callState[1] = HF_CALL_STATUS_NONE;
                    }
                } else {

                    /* Some AGWs illegally send a +CIEV message indicating 
                     * that a call is active when there is already another 
                     * active call.  Don't change the state here if there is 
                     * a call on either line that is either active or held.
                     */ 
                    if (((Channel->callState[0] != HF_CALL_STATUS_ACTIVE) &&
                         (Channel->callState[0] != HF_CALL_STATUS_HELD)) ||
                        ((Channel->callState[1] != HF_CALL_STATUS_ACTIVE) &&
                         (Channel->callState[1] != HF_CALL_STATUS_HELD))) {
                        if ((Channel->callState[0] == HF_CALL_STATUS_NONE) && 
                            (Channel->callState[1] == HF_CALL_STATUS_NONE)) {

                            /* No callsetup state */
                            Channel->callState[0] = HF_CALL_STATUS_ACTIVE;
                            Channel->callState[1] = HF_CALL_STATUS_NONE;
                        } else if (Channel->callState[0] != HF_CALL_STATUS_NONE) {
                            /* Channel 1 went active */
                            Channel->callState[0] = HF_CALL_STATUS_ACTIVE;
                        } else {
                            /* Channel 2 went active */
                            Channel->callState[1] = HF_CALL_STATUS_ACTIVE;
                        }
                    }
                }
            }
            break;
        case AT_IND_SETUP:
            CallMgrHandleCallsetup(Channel, (U8)Parms);
            break;

        default:
            handled = FALSE;
            break;
        }

        /* Poll for call state */
        (void)CallMgrQuickPoll(Channel);
        break;

    case EG_AT_PROCESSOR + AT_RING:

        /* Ringing, there is only 1 call */

        if (Channel->ringTimer.context) {
            /* Ring timer is running, cancel it */
            EVM_CancelTimer(&Channel->ringTimer);
        }

        /* Start the ring timer */
        Channel->ringTimer.func = CallMgrRingTimeoutHandler;
        Channel->ringTimer.context = Channel;
        EVM_StartTimer(&Channel->ringTimer, HF_RING_TIMEOUT);

        if (Channel->callState[0] == HF_CALL_STATUS_NONE) {
            Channel->callState[0] = HF_CALL_STATUS_INCOMING;
            Channel->callState[1] = HF_CALL_STATUS_NONE;
        }
        handled = TRUE;
        break;

    case EG_AT_HANDSFREE + AT_CALL_WAIT_NOTIFY:

        if (Channel->callWaiting) {
            /* A new (2nd) call is coming in */
            if ((Channel->callState[0] == HF_CALL_STATUS_NONE) ||
                (Channel->callState[0] == HF_CALL_STATUS_INCOMING)) {
                Channel->callState[0] = HF_CALL_STATUS_INCOMING;
                Channel->currentContext = 0;
            } else if ((Channel->callState[1] == HF_CALL_STATUS_NONE) ||
                    (Channel->callState[1] == HF_CALL_STATUS_INCOMING)) {
                    Channel->callState[1] = HF_CALL_STATUS_INCOMING;
                    Channel->currentContext = 1;
            } else {
                return TRUE;
            }
            if (OS_StrLen(result->p.hf.wait.number) > 0) {
                /* Report the caller ID number */
                if (Channel->callId) {
                    callId.line = Channel->currentContext + 1;
                    callId.index = Channel->currentContext + 1;
                    callId.number = result->p.hf.wait.number;
                    callId.type = result->p.hf.wait.type;
                    callId.multiParty = FALSE;
                    HfAppCallback(Channel, HF_EVENT_CALLER_ID, 
                                  BT_STATUS_SUCCESS, &callId);
                }
            }
        }
        handled = TRUE;
        break;

    case EG_AT_HANDSFREE + AT_CALL_ID:
        /* An incoming call has been identified */
        Channel->currentContext = 0;
        if (Channel->callState[0] != HF_CALL_STATUS_INCOMING) {
            if (Channel->callState[1] == HF_CALL_STATUS_INCOMING) {
                Channel->currentContext = 1;
            }
        }
        if (OS_StrLen(result->p.hf.callId.number) > 0) {
            /* Report the caller ID number */
            if (Channel->callId) {
                callId.line = Channel->currentContext + 1;
                callId.index = Channel->currentContext + 1;
                callId.number = result->p.hf.callId.number;
                callId.type = result->p.hf.callId.type;
                callId.multiParty = FALSE;
                HfAppCallback(Channel, HF_EVENT_CALLER_ID, BT_STATUS_SUCCESS,
                               &callId);
            }
        }
        handled = TRUE;
        break;

    case EG_AT_HANDSFREE + AT_LIST_CURRENT_CALLS:

        /* Response to CLCC */

        if (result->p.hf.currentCalls.multiParty) {
            index = 0;
        } else if (result->p.hf.currentCalls.index > 1) {
            index = 1;
        } else {
            index = result->p.hf.currentCalls.index - 1;
        }

        switch (result->p.hf.currentCalls.state) {
        case ATCS_ACTIVE:
            Channel->callState[index] = HF_CALL_STATUS_ACTIVE;
            break;
        case ATCS_HELD:
            Channel->callState[index] = HF_CALL_STATUS_HELD;
            break;
        case ATCS_DIALING:
            Channel->callState[index] = HF_CALL_STATUS_DIALING;
            break;
        case ATCS_ALERTING:
            Channel->callState[index] = HF_CALL_STATUS_ALERTING;
            break;
        case ATCS_INCOMING:
            Channel->callState[index] = HF_CALL_STATUS_INCOMING;
            break;
        case ATCS_WAITING:
            Channel->callState[index] = HF_CALL_STATUS_INCOMING;
            break;
        }

        if (result->p.hf.currentCalls.multiParty) {
            Channel->multiParty = TRUE;
        }

        if (Channel->callId) {
            callId.line = index + 1;
            callId.index = result->p.hf.currentCalls.index;
            if (result->p.hf.currentCalls.multiParty) {
                callId.multiParty = TRUE;
            } else {
                callId.multiParty = FALSE;
            }
            callId.number = result->p.hf.currentCalls.number;
            callId.type = result->p.hf.currentCalls.type;
            HfAppCallback(Channel, HF_EVENT_CALLER_ID, BT_STATUS_SUCCESS,
                          &callId);
        }

        if (index == 0) {
            Channel->pollFlags |= CALLMGR_POLL_RESP_RECVD_1;
        } else {
            Channel->pollFlags |= CALLMGR_POLL_RESP_RECVD_2;
        }

        handled = TRUE;
        break;
    }

    /* Determine whether two or more calls could be conferenced 
     * on line 1.  If not, then clear the multiParty indication.
     */ 
    if ((Channel->callState[0] != HF_CALL_STATUS_ACTIVE) && 
        (Channel->callState[0] != HF_CALL_STATUS_HELD)) {
        Channel->multiParty = FALSE;
    }

    if (Channel->callState[0] != saveState1) {
        /* Report line status */
        stateParms.line = 1;
        stateParms.state = Channel->callState[0];
        HfAppCallback(Channel, HF_EVENT_CALL_STATE, BT_STATUS_SUCCESS, &stateParms);
        if (Channel->callState[0] != HF_CALL_STATUS_INCOMING) {
            if (Channel->ringTimer.context) {

                /* The ring timer is used to generate an event if too 
                 * much time elapses since the last ring on an incoming 
                 * call on line 1.  (This will happen on older cell-
                 * phones if an incoming call is terminated before it 
                 * is answered.)  If line 1 goes to a state other than 
                 * incoming, then the ring timer should be canceled.
                 */

                EVM_CancelTimer(&Channel->ringTimer);
                Channel->ringTimer.context = 0;
            }
        }
    }

    if (Channel->callState[1] != saveState2) {
        stateParms.line = 2;
        stateParms.state = Channel->callState[1];
        HfAppCallback(Channel, HF_EVENT_CALL_STATE, BT_STATUS_SUCCESS, &stateParms);
    }

    if ((!(Channel->pollFlags & CALLMGR_POLL_ATTEMPTED)) || 
        (Channel->pollFlags & CALLMGR_POLL_RELIABLE)) {
        if ((Channel->callState[0] == HF_CALL_STATUS_NONE) && 
            (Channel->callState[1] == HF_CALL_STATUS_NONE)) {
            Channel->hfPollInterval = PL_SLOW;
        } else {
            if (PL_FAST != Channel->hfPollInterval) {
                Channel->hfPollInterval = PL_FAST;
                if (Channel->pollFlags & CALLMGR_POLL_LIST_CALLS) {
                    /* Already polling */
                    EVM_CancelTimer(&Channel->pollTimer);
                }
                Channel->pollTimer.func = CallMgrPollTimeoutHandler;
                Channel->pollTimer.context = Channel;
                EVM_StartTimer(&Channel->pollTimer, HF_POLL_TIMEOUT_ACTIVE);
            }
        }
    }

    return handled;
}

/*---------------------------------------------------------------------------
 *            CallMgrAnswerCall()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Answers an incoming or waiting call.
 *
 * Return:    BT_STATUS_FAILED - The call could not be answered (no incoming).
 *            (also see HF_SendAtCommand)
 */
BtStatus CallMgrAnswerCall(HfChannel *Channel, HfCommand *Command)
{
    BtStatus status = BT_STATUS_SUCCESS;
    U8       line = 0;

    Command->callMgrFlags = 0;

    /* See which line has an incoming call (if any) */
    if (Channel->callState[0] == HF_CALL_STATUS_INCOMING) {
        line = 0;
    } else if (Channel->callState[1] == HF_CALL_STATUS_INCOMING) {
        line = 1;
    } else {
        status = BT_STATUS_FAILED;
    }

    if (status == BT_STATUS_SUCCESS) {
        /* There is an incoming call */
        if (Channel->callState[(line + 1) % 2] == HF_CALL_STATUS_NONE) {
            /* Answer the call */
            Command->type = HF_COMMAND_ANSWER_CALL;
            Command->callMgrFlags |= CALLMGR_FLAG_ACCEPT;
        } else if ((Channel->callState[(line + 1) % 2] == HF_CALL_STATUS_ACTIVE) ||
                   (Channel->callState[(line + 1) % 2] == HF_CALL_STATUS_HELD)) {
            /* 3-Way call, do the default Hold */
            Command->type = HF_COMMAND_CALL_HOLD;
            Command->parms[0] = (U32)AT_HOLD_HOLD_ACTIVE_CALLS;
            Command->parms[1] = 0;

            Command->callMgrFlags |= CALLMGR_FLAG_ACCEPT;
        } else {
            status = BT_STATUS_FAILED;
        }
    }

    if (status == BT_STATUS_SUCCESS) {
        Channel->currentContext = line;
        status = HfAddCommand(Channel, Command);
        if (status == BT_STATUS_SUCCESS) {
            status = BT_STATUS_PENDING;
        }
    }

    return status;
}

/*---------------------------------------------------------------------------
 *            CallMgrHangupCall()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Hangs up an active (or held) call, rejects an incoming call, and
 *            cancels an outgoing call.
 *
 * Return:    BT_STATUS_FAILED - Could not hang up or call does not exist.
 *            (also see HF_SendAtCommand)
 */
BtStatus CallMgrHangupCall(HfChannel *Channel, HfCommand *Command)
{
    BtStatus status = BT_STATUS_SUCCESS;
    U8       line = 0;

    Command->callMgrFlags = 0;

    /* See which line has an incoming call (if any) */
    if (Channel->callState[0] == HF_CALL_STATUS_INCOMING) {
        line = 0;
    } else if (Channel->callState[1] == HF_CALL_STATUS_INCOMING) {
        line = 1;
    } else {
        status = BT_STATUS_FAILED;
    }

    if (status == BT_STATUS_SUCCESS) {
        /* Incoming connection */
        if (Channel->callState[(line + 1) % 2] == HF_CALL_STATUS_NONE) {
            /* No other call */
            Command->type = HF_COMMAND_HANGUP_CALL;

            Command->callMgrFlags |= CALLMGR_FLAG_REJECT;
        } else if (Channel->callState[(line + 1) % 2] == HF_CALL_STATUS_ACTIVE) {

            /* Hangup active, incoming becomes active */
            Command->type = HF_COMMAND_CALL_HOLD;
            Command->parms[0] = (U32)AT_HOLD_RELEASE_ACTIVE_CALLS;
            Command->parms[1] = 0;

            /* CDMA Treos work differently than GSM Treos in this 
             * case.  GSM Treos go from Active-Incoming to None- 
             * Active.  CDMA Treos move the incoming call from line 2 
             * to line 1 by going from Active-Incoming to None-None 
             * (for around 10 seconds), and then to Incoming-None.  
             * If the AGW supports CLCC polling, then do not assume 
             * that the state goes to None-Active by setting the 
             * CALLMGR_FLAG_REPLACE flag; but instead wait for the 
             * CLCC poll to indicate what is really happening.
             */ 
            if ((Channel->pollFlags & CALLMGR_POLL_ATTEMPTED) && 
                (!(Channel->pollFlags & CALLMGR_POLL_RELIABLE))) {
                Command->callMgrFlags |= CALLMGR_FLAG_REPLACE;
            }
        } else if (Channel->callState[(line + 1) % 2] == HF_CALL_STATUS_HELD) {
            /* Reject incoming, held call unchanged */
            Command->type = HF_COMMAND_CALL_HOLD;
            Command->parms[0] = AT_HOLD_RELEASE_HELD_CALLS;
            Command->parms[1] = 0;

            Command->callMgrFlags |= CALLMGR_FLAG_REJECT;
        } else {
            status = BT_STATUS_FAILED;
        }
        goto exit;
    } else {
        status = BT_STATUS_SUCCESS;
    }

    /* See which line has an outgoing call (if any) */
    if ((Channel->callState[0] == HF_CALL_STATUS_DIALING) ||
        (Channel->callState[0] == HF_CALL_STATUS_ALERTING)) {
        line = 0;
    } else if ((Channel->callState[1] == HF_CALL_STATUS_DIALING) ||
               (Channel->callState[1] == HF_CALL_STATUS_ALERTING)) {
        line = 1;
    } else {
        status = BT_STATUS_FAILED;
    }

    if (status == BT_STATUS_SUCCESS) {
        /* There is an outgoing call */
        Command->type = HF_COMMAND_HANGUP_CALL;
        Command->callMgrFlags |= CALLMGR_FLAG_REJECT;
        goto exit;
    } else {
        status = BT_STATUS_SUCCESS;
    } 

    /* See if a line is active */
    if (Channel->callState[0] == HF_CALL_STATUS_ACTIVE) {
        line = 0;
    } else if (Channel->callState[1] == HF_CALL_STATUS_ACTIVE) {
        line = 1;
    } else {
        status = BT_STATUS_FAILED;
    }

    if (status == BT_STATUS_SUCCESS) {
        if (Channel->callState[(line + 1) % 2] == HF_CALL_STATUS_NONE) {
            /* No other call */
            Command->type = HF_COMMAND_HANGUP_CALL;
            Command->callMgrFlags |= CALLMGR_FLAG_HANGUP;
        } else {
            /* Hangup the active call */
            Command->type = HF_COMMAND_CALL_HOLD;
            Command->parms[0] = (U32)AT_HOLD_RELEASE_ACTIVE_CALLS;
            Command->parms[1] = 0;
            /* CDMA Treos work differently than GSM Treos 
             * in this case.  GSM Treos go from Active-Held to 
             * None-Active.  CDMA Treos terminate the active call 
             * on line 1, then move the held call from line 2 to 
             * line 1 by going from Active-Held to Active-None (for 
             * around 10 seconds), and then to Incoming-None.  If 
             * the AGW supports CLCC polling, then do not assume 
             * that the state goes to None-Active by setting the 
             * CALLMGR_FLAG_HANGUP flag; but instead wait for the 
             * CLCC poll to indicate what is really happening.
             */ 
            if ((Channel->pollFlags & CALLMGR_POLL_ATTEMPTED) && 
                (!(Channel->pollFlags & CALLMGR_POLL_RELIABLE))) {
                Command->callMgrFlags |= CALLMGR_FLAG_HANGUP;
            }
        }
        goto exit;
    } else {
        status = BT_STATUS_SUCCESS;
    }

    /* See if a line is held */
    if (Channel->callState[0] == HF_CALL_STATUS_HELD) {
        line = 0;
    } else if (Channel->callState[1] == HF_CALL_STATUS_HELD) {
        line = 1;
    } else {
        status = BT_STATUS_FAILED;
    }

    if (status == BT_STATUS_SUCCESS) {
        /* Release all held calls */
        if (Channel->callState[(line + 1) % 2] != HF_CALL_STATUS_ACTIVE) {
            Command->type = HF_COMMAND_CALL_HOLD;
            Command->parms[0] = AT_HOLD_RELEASE_HELD_CALLS;
            Command->parms[1] = 0;

            /* Some AGWs such as the Treos do not carry out AT+CHLD=0 
             * commands to release held calls.  If the AGW supports CLCC 
             * polling, then do not assume that the held calls go to none 
             * by setting the CALLMGR_FLAG_RELEASE flag; but instead wait 
             * for the CLCC poll to indicate what is really happening.
             */ 
            if ((Channel->pollFlags & CALLMGR_POLL_ATTEMPTED) && 
                (!(Channel->pollFlags & CALLMGR_POLL_RELIABLE))) {
                /* Release the held call */
                Command->callMgrFlags |= CALLMGR_FLAG_RELEASE;
            }
            goto exit;
        }
    }

    /* The default action is just to hang up */
    Command->type = HF_COMMAND_HANGUP_CALL;
    Command->callMgrFlags |= CALLMGR_FLAG_HANGUP;
    status = BT_STATUS_SUCCESS;

exit:

    if (status == BT_STATUS_SUCCESS) {
        Channel->currentContext = line;
        status = HfAddCommand(Channel, Command);
        if (status == BT_STATUS_SUCCESS) {
            status = BT_STATUS_PENDING;
        }
    }

    return status;
}

/*---------------------------------------------------------------------------
 *            CallMgrInitiateCall()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initiates an outgoing call.
 *
 * Return:    BT_STATUS_FAILED - A call cannot be initiated.
 *            (also see HF_SendAtCommand)
 */
BtStatus CallMgrInitiateCall(HfChannel *Channel, HfCommand *Command)
{
    BtStatus status;

    BOOL reportsCallSetup = CallMgrSetupIndications();

    Command->callMgrFlags = 0;
    if ((Channel->callState[0] == HF_CALL_STATUS_NONE) && 
        (Channel->callState[1] == HF_CALL_STATUS_NONE)) {
        Channel->currentContext = 0;
        if (FALSE == reportsCallSetup) {
             Channel->callState[0] = HF_CALL_STATUS_DIALING;
        }

    } else if ((Channel->callState[0] == HF_CALL_STATUS_NONE) && 
               ((Channel->callState[1] == HF_CALL_STATUS_HELD) ||
                (Channel->callState[1] == HF_CALL_STATUS_ACTIVE)) &&
               (Channel->callWaiting)) {

        Channel->currentContext = 0;

        if (FALSE == reportsCallSetup) {
            Channel->callState[0] = HF_CALL_STATUS_DIALING;
        }

    } else if (((Channel->callState[0] == HF_CALL_STATUS_HELD) || 
                (Channel->callState[0] == HF_CALL_STATUS_ACTIVE)) &&
               (Channel->callState[1] == HF_CALL_STATUS_NONE) &&
               (Channel->callWaiting)) {

        Channel->currentContext = 1;
        if (FALSE == reportsCallSetup) {
            Channel->callState[1] = HF_CALL_STATUS_DIALING;
        }

    } else {
        return BT_STATUS_FAILED;
    }

    Command->callMgrFlags |= CALLMGR_FLAG_CALL_OUTGOING;
    status = HfAddCommand(Channel, Command);
    if (status == BT_STATUS_SUCCESS) {
        status = BT_STATUS_PENDING;
    }

    return status;
}

/*---------------------------------------------------------------------------
 *            CallMgrIsActiveCall()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Returns TRUE if any call is active.
 *
 * Return:    TRUE - A call is active.
 *            FALSE - No call is active.
 */
BOOL CallMgrIsActiveCall(HfChannel *Channel)
{
    if ((Channel->callState[0] == HF_CALL_STATUS_ACTIVE) ||
        (Channel->callState[1] == HF_CALL_STATUS_ACTIVE)) {
        return TRUE;
    } else {
        return FALSE;
    }
}

/*---------------------------------------------------------------------------
 *            CallMgrIsActiveCall()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Returns TRUE if any call is incoming.
 *
 * Return:    TRUE - A call is incoming.
 *            FALSE - No call is incoming.
 */
BOOL CallMgrIsIncomingCall(HfChannel *Channel)
{
    if ((Channel->callState[0] == HF_CALL_STATUS_INCOMING) ||
        (Channel->callState[1] == HF_CALL_STATUS_INCOMING)) {
        return TRUE;
    } else {
        return FALSE;
    }
}

/*---------------------------------------------------------------------------
 *            CallMgrCallHold()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Peforms 3-Way calling hold functions.
 *
 * Return:    BT_STATUS_FAILED - The command could not be issued, because the
 *                call is not in the correct state.
 *            (also see HF_SendAtCommand)
 */
BtStatus CallMgrCallHold(HfChannel *Channel, 
                         HfHoldAction Action, 
                         U8 Index, 
                         HfCommand *Command)
{
    BtStatus status = BT_STATUS_SUCCESS;
    U8       line = 0;
    BOOL     conferencable = FALSE;

    Command->callMgrFlags = 0;

    switch (Action) {
    case HF_HOLD_RELEASE_HELD_CALLS:
        /* See if there is an held call */
        if (Channel->callState[0] == HF_CALL_STATUS_HELD) {
            line = 0;
        } else if (Channel->callState[1] == HF_CALL_STATUS_HELD) {
            line = 1;
        } else {
            status = BT_STATUS_FAILED;
        }

        if (status == BT_STATUS_SUCCESS) {
            if ((Channel->callState[(line + 1) % 2] == HF_CALL_STATUS_ACTIVE) ||
                (Channel->callState[(line + 1) % 2] == HF_CALL_STATUS_NONE) ||
                (Channel->callState[(line + 1) % 2] == HF_CALL_STATUS_HELD)) {

                /* Some AGWs such as the Treos do not carry out AT+CHLD=0 
                 * commands to release held calls.  If the AGW supports CLCC 
                 * polling, then do not assume that the held calls go to none 
                 * by setting the CALLMGR_FLAG_RELEASE flag; but instead wait 
                 * for the CLCC poll to indicate what is really happening.
                 */ 
                if ((Channel->pollFlags & CALLMGR_POLL_ATTEMPTED) && 
                    (!(Channel->pollFlags & CALLMGR_POLL_RELIABLE))) {
                    /* Release the held call */
                    Command->callMgrFlags |= CALLMGR_FLAG_RELEASE;
                }
            } else if (Channel->callState[(line + 1) % 2] == HF_CALL_STATUS_INCOMING) {
                /* Reject the incoming call */
                Command->callMgrFlags |= CALLMGR_FLAG_REJECT;
                line = (line + 1) % 2;
            } else {
                /* Any other state causes a failure */
                status = BT_STATUS_FAILED;
            }
        }
        break;

    case HF_HOLD_RELEASE_ACTIVE_CALLS:

        if (Index != 0) {
            goto exit;
        }

        /* See if there is an incoming call */
        if (Channel->callState[0] == HF_CALL_STATUS_INCOMING) {
            line = 0;
        } else if (Channel->callState[1] == HF_CALL_STATUS_INCOMING) {
            line = 1;
        } else {
            status = BT_STATUS_FAILED;
        }

        if (status == BT_STATUS_SUCCESS) {
            if (Channel->callState[(line + 1) % 2] == HF_CALL_STATUS_NONE) {
                /* Reject the incoming call */
                Command->callMgrFlags |= CALLMGR_FLAG_REJECT;
            } else if (Channel->callState[(line + 1) % 2] == HF_CALL_STATUS_ACTIVE) {

                /* CDMA Treos work differently than GSM Treos in this 
                 * case.  GSM Treos go from Active-Incoming to None- 
                 * Active.  CDMA Treos move the incoming call from line 2 
                 * to line 1 by going from Active-Incoming to None-None 
                 * (for around 10 seconds), and then to Incoming-None.  
                 * If the AGW supports CLCC polling, then do not assume 
                 * that the state goes to None-Active by setting the 
                 * CALLMGR_FLAG_REPLACE flag; but instead wait for the 
                 * CLCC poll to indicate what is really happening.
                 */ 
                if ((Channel->pollFlags & CALLMGR_POLL_ATTEMPTED) && 
                    (!(Channel->pollFlags & CALLMGR_POLL_RELIABLE))) {
                    /* Replace the active call with the incoming call */
                    Command->callMgrFlags |= CALLMGR_FLAG_REPLACE;
                }

            } else {
                /* Any other state causes a failure */
                status = BT_STATUS_FAILED;
            }
            goto exit;
        } else {
            status = BT_STATUS_SUCCESS;
        }

        /* See if there is an outgoing call */
        if ((Channel->callState[0] == HF_CALL_STATUS_DIALING) ||
            (Channel->callState[0] == HF_CALL_STATUS_ALERTING)) {
            line = 0;
        } else if ((Channel->callState[1] == HF_CALL_STATUS_ALERTING) ||
                   (Channel->callState[1] == HF_CALL_STATUS_DIALING)) {
            line = 1;
        } else {
            status = BT_STATUS_FAILED;
        }

        if (status == BT_STATUS_SUCCESS) {
            if (Channel->callState[(line + 1) % 2] == HF_CALL_STATUS_NONE) {
                /* Cancel (Reject) the outgoing call */
                Command->callMgrFlags |= CALLMGR_FLAG_REJECT;
            } else {
                status = BT_STATUS_FAILED;
            }
            goto exit;
        } else {
            status = BT_STATUS_SUCCESS;
        }

        /* See if there is an active call */
        if (Channel->callState[0] == HF_CALL_STATUS_ACTIVE) {
            line = 0;
        } else if (Channel->callState[1] == HF_CALL_STATUS_ACTIVE) {
            line = 1;
        } else {
            status = BT_STATUS_FAILED;
        }

        if (status == BT_STATUS_SUCCESS) {
            if ((Channel->callState[(line + 1) % 2] == HF_CALL_STATUS_NONE) ||
                (Channel->callState[(line + 1) % 2] == HF_CALL_STATUS_HELD) ||
                (Channel->callState[(line + 1) % 2] == HF_CALL_STATUS_ACTIVE)) {
                /* Hang up the active call */

                /* CDMA Treos work differently than GSM Treos 
                 * in this case.  GSM Treos go from Active-Held to 
                 * None-Active.  CDMA Treos terminate the active call 
                 * on line 1, then move the held call from line 2 to 
                 * line 1 by going from Active-Held to Active-None (for 
                 * around 10 seconds), and then to Incoming-None.  If 
                 * the AGW supports CLCC polling, then do not assume 
                 * that the state goes to None-Active by setting the 
                 * CALLMGR_FLAG_HANGUP flag; but instead wait for the 
                 * CLCC poll to indicate what is really happening.
                 */ 
                if ((Channel->pollFlags & CALLMGR_POLL_ATTEMPTED) && 
                    (!(Channel->pollFlags & CALLMGR_POLL_RELIABLE))) {
                    Command->callMgrFlags |= CALLMGR_FLAG_HANGUP;
                }
            } else {
                /* Any other state causes a failure */
                status = BT_STATUS_FAILED;
            }
        }
        break;

    case HF_HOLD_HOLD_ACTIVE_CALLS:

        if (Index != 0) {
            goto exit;
        }

        /* See if there is an incoming call */
        if (Channel->callState[0] == HF_CALL_STATUS_INCOMING) {
            line = 0;
        } else if (Channel->callState[1] == HF_CALL_STATUS_INCOMING) {
            line = 1;
        } else {
            status = BT_STATUS_FAILED;
        }

        if (status == BT_STATUS_SUCCESS) {
            if ((Channel->callState[(line + 1) % 2] == HF_CALL_STATUS_ACTIVE) ||
                (Channel->callState[(line + 1) % 2] == HF_CALL_STATUS_HELD)) {
                /* Accept the incoming call */
                Command->callMgrFlags |= CALLMGR_FLAG_ACCEPT;
            } else {
                /* Any other state causes a failure */
                status = BT_STATUS_FAILED;
            }
            goto exit;
        } else {
            status = BT_STATUS_SUCCESS;
        }

        /* See if there is an active call */
        if (Channel->callState[0] == HF_CALL_STATUS_ACTIVE) {
            line = 0;
        } else if (Channel->callState[1] == HF_CALL_STATUS_ACTIVE) {
            line = 1;
        } else {
            status = BT_STATUS_FAILED;
        }

        if (status == BT_STATUS_SUCCESS) {
            if ((Channel->callState[(line + 1) % 2] == HF_CALL_STATUS_NONE) ||
                (Channel->callState[(line + 1) % 2] == HF_CALL_STATUS_HELD) ||
                (Channel->callState[(line + 1) % 2] == HF_CALL_STATUS_ACTIVE)) {
                /* Some AGWs such as the Treos ignore commands to hold 
                 * active calls.  If the AGW supports CLCC polling, 
                 * then do not assume that the state goes to Held by 
                 * setting the CALLMGR_FLAG_HOLD flag; but instead wait 
                 * for the CLCC poll to indicate what is really happening.
                 */ 
                if ((Channel->pollFlags & CALLMGR_POLL_ATTEMPTED) && 
                    (!(Channel->pollFlags & CALLMGR_POLL_RELIABLE))) {
                    /* Hold the active call */
                    Command->callMgrFlags |= CALLMGR_FLAG_HOLD;
                }
            } else {
                /* Any other state causes a failure */
                status = BT_STATUS_FAILED;
            }
            goto exit;
        } else {
            status = BT_STATUS_SUCCESS;
        }

        /* See if there is an held call */
        if (Channel->callState[0] == HF_CALL_STATUS_HELD) {
            line = 0;
        } else if (Channel->callState[1] == HF_CALL_STATUS_HELD) {
            line = 1;
        } else {
            status = BT_STATUS_FAILED;
        }

        if (status == BT_STATUS_SUCCESS) {
            if ((Channel->callState[(line + 1) % 2] == HF_CALL_STATUS_NONE) ||
                (Channel->callState[(line + 1) % 2] == HF_CALL_STATUS_HELD) ||
                (Channel->callState[(line + 1) % 2] == HF_CALL_STATUS_ACTIVE)) {
                /* Activate the held call */
                Command->callMgrFlags |= CALLMGR_FLAG_ACTIVATE;
            } else {
                /* Any other state causes a failure */
                status = BT_STATUS_FAILED;
            }
        }
        break;

    case HF_HOLD_ADD_HELD_CALL:
        /* See if there is an active call */
        if (Channel->callState[0] == HF_CALL_STATUS_ACTIVE) {
            line = 0;
        } else if (Channel->callState[1] == HF_CALL_STATUS_ACTIVE) {
            line = 1;
        } else {
            status = BT_STATUS_FAILED;
        }

        if (status == BT_STATUS_SUCCESS) {
            if ((!(Channel->pollFlags & CALLMGR_POLL_ATTEMPTED)) || 
                (Channel->pollFlags & CALLMGR_POLL_RELIABLE)) {
                /* Since CLCC polling is supported, do not assume 
                 * there is a limit on the number of calls that can 
                 * be conferenced, as the CLCC reply message will 
                 * indicate when conference additions fail or work.
                 */ 
                conferencable = TRUE;
            } else {
                /* Since CLCC polling is not supported, 
                 * assume (sometimes incorrectly) that a 
                 * maximum of two calls can be conferenced.
                 */ 
                if (FALSE == Channel->multiParty) {
                    conferencable = TRUE;
                }
            }
            if (conferencable && 
                (Channel->callState[(line + 1) % 2] == HF_CALL_STATUS_HELD)) {
                /* Index 1 becomes the group call */
                Command->callMgrFlags |= CALLMGR_FLAG_CONFERENCE;
            } else {
                status = BT_STATUS_FAILED;
            }
        }
        break;

    case HF_HOLD_CALL_TRANSFER:
        /* See if the calls are active */
        if (Channel->callState[0] == HF_CALL_STATUS_ACTIVE) {
            line = 0;
        } else if (Channel->callState[1] == HF_CALL_STATUS_ACTIVE) {
            line = 1;
        } else {
            status = BT_STATUS_FAILED;
        }

        if (status == BT_STATUS_SUCCESS) {
            if (Channel->callState[(line + 1) % 2] == HF_CALL_STATUS_ACTIVE) {
                /* Some AGWs do not carry out explicit transfer commands 
                 * to hang up calls.  If the AGW supports CLCC polling, 
                 * then do not assume that the calls go to none by 
                 * setting the CALLMGR_FLAG_HANGUP flag; but instead wait 
                 * for the CLCC poll to indicate what is really happening.
                 */ 
                if ((Channel->pollFlags & CALLMGR_POLL_ATTEMPTED) && 
                    (!(Channel->pollFlags & CALLMGR_POLL_RELIABLE))) {
                    /* Deactivate all calls */
                    Command->callMgrFlags |= CALLMGR_FLAG_HANGUP;
                }
            } else {
                /* Any other state causes a failure */
                status = BT_STATUS_FAILED;
            }
            goto exit;
        } else {
            status = BT_STATUS_SUCCESS;
        }

        /* See if the calls are held */
        if (Channel->callState[0] == HF_CALL_STATUS_HELD) {
            line = 0;
        } else if (Channel->callState[1] == HF_CALL_STATUS_HELD) {
            line = 1;
        } else {
            status = BT_STATUS_FAILED;
        }

        if (status == BT_STATUS_SUCCESS) {
            if (Channel->callState[(line + 1) % 2] == HF_CALL_STATUS_HELD) {
                /* Some AGWs do not carry out explicit transfer commands 
                 * to hang up calls.  If the AGW supports CLCC polling, 
                 * then do not assume that the calls go to none by 
                 * setting the CALLMGR_FLAG_HANGUP flag; but instead wait 
                 * for the CLCC poll to indicate what is really happening.
                 */ 
                if ((Channel->pollFlags & CALLMGR_POLL_ATTEMPTED) && 
                    (!(Channel->pollFlags & CALLMGR_POLL_RELIABLE))) {
                    /* Deactivate all calls */
                    Command->callMgrFlags |= CALLMGR_FLAG_HANGUP;
                }
            } else {
                /* Any other state causes a failure */
                status = BT_STATUS_FAILED;
            }
        }
        break;
    }

exit:

    if (status == BT_STATUS_SUCCESS) {
        Channel->currentContext = line;
        Command->type = HF_COMMAND_CALL_HOLD;
        Command->parms[0] = (U32)Action;
        Command->parms[1] = (U32)Index;
        status = HfAddCommand(Channel, Command);
        if (status == BT_STATUS_SUCCESS) {
            status = BT_STATUS_PENDING;
        }
    }

    return status;
} 

#endif


