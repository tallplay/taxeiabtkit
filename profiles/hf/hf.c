/****************************************************************************
 *
 * File:
 *     $Workfile:hf.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:14$
 *
 * Description: This file contains API functions for the Hands-free SDK.
 *             
 * Created:     February 11, 2005
 *
 * Copyright 2000-2005 Extended Systems, Inc.

 * Portions copyright 2005 iAnywhere Solutions, Inc.

 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any means, 
 * or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions,  
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "sys/hfi.h"
#include "sys/hf_cmd.h"
#include "sys/hfalloc.h"
#include "conmgr.h"

#if HF_USE_CALL_MANAGER == XA_ENABLED
#include "sys/hf_cmgr.h"
#endif
      
/*---------------------------------------------------------------------------
 *            HfServiceConnectionCallback()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Called by device manager with link state events.
 *
 * Return:    (See header file)
 *
 */
void HfServiceConnectionCallback(CmgrHandler *Handler, CmgrEvent Event,
                                 BtStatus Status) 
{
    HfChannel *channel = ContainingRecord(Handler, HfChannel, cmgrHandler);

    switch (Event) {
    case CMEVENT_DATA_LINK_CON_CNF:

        if (Status == BT_STATUS_SUCCESS) {
            /* Start the service search */
            channel->linkFlags |= HF_LINK_ACL;
            channel->sdpQueryToken.attribId = AID_PROTOCOL_DESC_LIST;
            if ((Status = HfStartServiceQuery(channel, BSQM_FIRST) == 
                 BT_STATUS_PENDING)) {
                return;
            }
        }

        /* Error, call the application */
        HfCloseChannel(channel);
        break;

    case CMEVENT_DATA_LINK_DIS:
        HfCloseChannel(channel);
        break;

    case CMEVENT_AUDIO_LINK_CON:
        HfAppCallback(channel, HF_EVENT_AUDIO_CONNECTED, Status, 
                       channel->cmgrHandler.bdc->link);
        break;

    case CMEVENT_AUDIO_LINK_DIS:
        if (channel->linkFlags & HF_LINK_REMOVE_HF) {
            channel->linkFlags &= ~HF_LINK_REMOVE_HF;
            AssertEval(RF_CloseChannel(channel->hfChannel) == BT_STATUS_PENDING);
        }

        HfAppCallback(channel, HF_EVENT_AUDIO_DISCONNECTED, Status, 
                       channel->cmgrHandler.bdc->link);
        break;

#if BT_SCO_HCI_DATA == XA_ENABLED
    case CMEVENT_AUDIO_DATA:
        HfAppCallback(channel, HF_EVENT_AUDIO_DATA, Status, &Handler->audioData);
        break;

    case CMEVENT_AUDIO_DATA_SENT:
        HfAppCallback(channel, HF_EVENT_AUDIO_DATA_SENT, Status, &Handler->audioPacket);
        break;
#endif
    }
}

/*---------------------------------------------------------------------------
 *            HF_Register()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initializes all necessary data structures, registers with
 *            RFCOMM and SDP. 
 *
 * Return:    (See header file)
 *
 */
BtStatus HF_Register(HfChannel *Channel, HfCallback Callback)
{
    BtStatus   status = BT_STATUS_SUCCESS;
    RfChannel *rfChannel;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Channel != 0);

    OS_LockStack();

    /* Register this channel in the list */
    if (!IsNodeOnList(&HF(channelList), &Channel->node)) {

        /* Clear the channel structure */
        OS_MemSet((U8*)Channel, 0, sizeof(HfChannel));

        /* Register RFCOMM service */
        if (!HF(regCount++)) {

            /* This is the fist registration with RFCOMM */
            HF(hfService).serviceId = 0;

            /* Register a channel to receive Handsfree connections */
            rfChannel = HfAllocRfChannel();
            if (rfChannel) {
                status = RF_RegisterServerChannel(rfChannel, &HF(hfService), 1);
            } else {
                /* No channels left to register */
                status = BT_STATUS_NO_RESOURCES;
                goto error;
            }

            rfChannel->userContext = (void *)1;
            HF(hfChannel) = rfChannel;

            /* Register with SDP if this is the first channel */
            status = HfRegisterSdpServices(Channel);

            if (status != BT_STATUS_SUCCESS) {
                /* Failed SDP registration, free the HF channels */
                RF_DeregisterServerChannel(rfChannel, &HF(hfService));
                goto error;
            }

#if BT_SECURITY == XA_ENABLED
            /* Also, register security records */
            HF(hfSecRec).id =  SEC_RFCOMM_ID;
            HF(hfSecRec).channel = HF(hfService).serviceId;
            HF(hfSecRec).level = HF_SECURITY_SETTINGS;    
            status = SEC_Register(&HF(hfSecRec));
            if (status != BT_STATUS_SUCCESS) {
                /* Failed security registration, free the HF channels */
                (void)HfDeregisterSdpServices();
                RF_DeregisterServerChannel(rfChannel, &HF(hfService));
                goto error;
            }
#endif

        }

        if (status == BT_STATUS_SUCCESS) {

            /* Register the Hf channel */
            Channel->callback = Callback;
            Channel->atTxPacket.data = Channel->atTxData;

            /* Default gateway features */
            Channel->gwFeatures = HF_GW_FEATURE_3_WAY | HF_GW_FEATURE_IN_BAND_RING;

#if HF_USE_CALL_MANAGER == XA_ENABLED
            Channel->pollPacket.data = Channel->pollData;
#endif

            /* Insert the channel on the list */
            InsertTailList(&HF(channelList), &Channel->node);
            InitializeListHead(&Channel->cmdQueue);
            InitializeListHead(&Channel->txQueue);
            (void)CMGR_RegisterHandler(&Channel->cmgrHandler, HfServiceConnectionCallback);
        }
    } else {
        /* Already registered */
        status = BT_STATUS_IN_USE;
        goto error;
    }

error:

    OS_UnlockStack();

    return status;
}


/*---------------------------------------------------------------------------
 *            HF_Deregister()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Deregisters with RFCOMM and SDP. 
 *
 * Return:    (See header file)
 *
 */
BtStatus HF_Deregister(HfChannel *Channel)
{
    BtStatus   status = BT_STATUS_SUCCESS;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Channel != 0);

    OS_LockStack();

    if (IsNodeOnList(&HF(channelList), &Channel->node)) {

        Assert(HF(regCount));
        if (!(--HF(regCount))) {

            /* Deregister with SDP */
            (void)HfDeregisterSdpServices();

#if BT_SECURITY == XA_ENABLED
            /* Remove Security Record */
            SEC_Unregister(&HF(hfSecRec));
#endif

            /* Deregister the RFCOMM HF channel */
            status = RF_DeregisterServerChannel(HF(hfChannel),
                                                &HF(hfService));
            if (status == BT_STATUS_SUCCESS) {
                /* A HF channel was unregistered, free it */
                HfFreeRfChannel(HF(hfChannel));
                HF(hfChannel)->userContext = 0;
            }

        }

        (void)CMGR_DeregisterHandler(&Channel->cmgrHandler);
        RemoveEntryList(&Channel->node);
    } else {
        /* Channel is not registered */
        status = BT_STATUS_NOT_FOUND;
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            HF_CreateServiceLink()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Opens a service level connection. 
 *
 * Return:    (See header file)
 *
 */
BtStatus HF_CreateServiceLink(HfChannel *Channel, BD_ADDR *Addr)
{
    BtStatus status = BT_STATUS_NOT_FOUND;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Channel != 0);

    OS_LockStack();

    /* See if the channel is registered */
    if (IsNodeOnList(&HF(channelList), &Channel->node)) {

        if (Channel->state == HF_STATE_CLOSED) {

            /* Initialize channel structure */
            Channel->version = 0;
            Channel->gwFeatures = 0;
            Channel->nextCommand = 0;

            /* Establish the ACL link */
            status = CMGR_CreateDataLink(&Channel->cmgrHandler, Addr);
            if (status == BT_STATUS_SUCCESS) {
                /* Start the service search */
                Channel->linkFlags |= HF_LINK_ACL;
                Channel->sdpQueryToken.attribId = AID_PROTOCOL_DESC_LIST;
                status = HfStartServiceQuery(Channel, BSQM_FIRST);
            }

            if (status == BT_STATUS_PENDING) {
                /* Connection was started */
                Channel->state = HF_STATE_CONN_PENDING;
                Channel->flags = CHANNEL_FLAG_OUTGOING;
            }
        } else {
            /* Channel is already in use */
            status = BT_STATUS_IN_USE;
        }
    }

    OS_UnlockStack();

    return status;
}


/*---------------------------------------------------------------------------
 *            HF_DisconnectServiceLink()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Close a service level connection. 
 *
 * Return:    (See header file)
 *
 */
BtStatus HF_DisconnectServiceLink(HfChannel *Channel)
{
    BtStatus status = BT_STATUS_NOT_FOUND;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Channel != 0);

    OS_LockStack();

    /* See if the channel is registered */
    if (IsNodeOnList(&HF(channelList), &Channel->node)) {

        if (Channel->state == HF_STATE_OPEN) {
            if (Channel->linkFlags == HF_LINK_ACL) {
                /* Only an ACL connection */
                status = BT_STATUS_SUCCESS;
                HfCloseChannel(Channel);
            } else {
                if (Channel->linkFlags & HF_LINK_HANDSFREE) {
                    if (CMGR_IsAudioUp(&Channel->cmgrHandler)) {
                        /* Audio still up, take it down */
                        Channel->linkFlags |= HF_LINK_REMOVE_HF;
                        status = CMGR_RemoveAudioLink(&Channel->cmgrHandler);
                    } else {
                        status = RF_CloseChannel(Channel->hfChannel);
                    }
                }
            }
        } else {
            status = BT_STATUS_NO_CONNECTION;
        }
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            HF_CreateAudioLink()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Creates an audio (SCO) link to the audio gateway.
 *
 * Return:    (See header file)
 *
 */
BtStatus HF_CreateAudioLink(HfChannel *Channel)
{
    BtStatus status = BT_STATUS_NOT_FOUND;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Channel != 0);

    OS_LockStack();

    if (IsNodeOnList(&HF(channelList), &Channel->node)) {
        if (Channel->state == HF_STATE_OPEN) {
            status = CMGR_CreateAudioLink(&Channel->cmgrHandler);
        } else {
            status = BT_STATUS_NO_CONNECTION;
        }
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            HF_DisconnectAudioLink()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Releases the audio connection with the audio gateway.
 *
 * Return:    (See header file)
 *
 */
BtStatus HF_DisconnectAudioLink(HfChannel *Channel)
{
    BtStatus status = BT_STATUS_NOT_FOUND;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Channel != 0);

    OS_LockStack();

    if (IsNodeOnList(&HF(channelList), &Channel->node)) {
        if (Channel->state == HF_STATE_OPEN) {
            status = CMGR_RemoveAudioLink(&Channel->cmgrHandler);
        } else {
            status = BT_STATUS_NO_CONNECTION;
        }
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            HF_AnswerCall()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Answer an incoming call.  If Call Manager is enabled then, for 
 *            3-Way calling, perform the default action (hold active calls)
 *            if a call already exists.
 *
 * Return:    (See header file)
 *
 */
BtStatus HF_AnswerCall(HfChannel *Channel, HfCommand *Command)
{
    BtStatus status = BT_STATUS_NOT_FOUND;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Channel != 0);
    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Command != 0);

    OS_LockStack();

    if (IsNodeOnList(&HF(channelList), &Channel->node)) {
        if (Channel->state == HF_STATE_OPEN) {
#if HF_USE_CALL_MANAGER == XA_ENABLED
            status = CallMgrAnswerCall(Channel, Command);
#else
            Command->type = HF_COMMAND_ANSWER_CALL;
            status = HfAddCommand(Channel, Command);
            if (status == BT_STATUS_SUCCESS) {
                status = BT_STATUS_PENDING;
            }
#endif
        } else {
            status = BT_STATUS_NO_CONNECTION;
        }
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            HF_DialNumber()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initiates an outgoing call using a phone number.
 *
 * Return:    (See header file)
 *
 */
BtStatus HF_DialNumber(HfChannel *Channel, U8 *Number, U16 Len, HfCommand *Command)
{
    BtStatus status = BT_STATUS_NOT_FOUND;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Channel != 0);
    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Command != 0);

    OS_LockStack();

    if (IsNodeOnList(&HF(channelList), &Channel->node)) {
        if (Channel->state == HF_STATE_OPEN) {
            Command->type = HF_COMMAND_DIAL_NUMBER;
            Number[Len] = 0;
            Command->parms[0] = (U32)Number;
#if HF_USE_CALL_MANAGER == XA_ENABLED
            status = CallMgrInitiateCall(Channel, Command);
#else
            status = HfAddCommand(Channel, Command);
            if (status == BT_STATUS_SUCCESS) {
                status = BT_STATUS_PENDING;
            }
#endif
        } else {
            status = BT_STATUS_NO_CONNECTION;
        }
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            HF_MemoryDial()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initiates an outgoing call using a memory location on the phone.
 *
 * Return:    (See header file)
 *
 */
BtStatus HF_MemoryDial(HfChannel *Channel, U8 *Location, U16 Len, HfCommand *Command)
{
    BtStatus status = BT_STATUS_NOT_FOUND;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Channel != 0);
    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Command != 0);

    OS_LockStack();

    if (IsNodeOnList(&HF(channelList), &Channel->node)) {
        if (Channel->state == HF_STATE_OPEN) {
            Command->type = HF_COMMAND_DIAL_MEMORY;
            Location[Len] = 0;
            Command->parms[0] = (U32)Location;
#if HF_USE_CALL_MANAGER == XA_ENABLED
            status = CallMgrInitiateCall(Channel, Command);
#else
            status = HfAddCommand(Channel, Command);
            if (status == BT_STATUS_SUCCESS) {
                status = BT_STATUS_PENDING;
            }
#endif
        } else {
            status = BT_STATUS_NO_CONNECTION;
        }
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            HF_Redial()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initiates an outgoing call based on the last number dialed.
 *
 * Return:    (See header file)
 *
 */
BtStatus HF_Redial(HfChannel *Channel, HfCommand *Command)
{
    BtStatus status = BT_STATUS_NOT_FOUND;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Channel != 0);
    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Command != 0);

    OS_LockStack();

    if (IsNodeOnList(&HF(channelList), &Channel->node)) {
        if (Channel->state == HF_STATE_OPEN) {
            Command->type = HF_COMMAND_REDIAL;
#if HF_USE_CALL_MANAGER == XA_ENABLED
            status = CallMgrInitiateCall(Channel, Command);
#else
            status = HfAddCommand(Channel, Command);
            if (status == BT_STATUS_SUCCESS) {
                status = BT_STATUS_PENDING;
            }
#endif
        } else {
            status = BT_STATUS_NO_CONNECTION;
        }
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            HF_CallHold()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Issues a command to the audio gateway to manage multi-party 
 *            calling.
 *
 * Return:    (See header file)
 *
 */
BtStatus HF_CallHold(HfChannel *Channel, 
                      HfHoldAction HoldAction, 
                      U8 Index,
                      HfCommand *Command)
{
    BtStatus status = BT_STATUS_NOT_FOUND;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Channel != 0);
    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Command != 0);

    OS_LockStack();

    if (IsNodeOnList(&HF(channelList), &Channel->node)) {
        if (Channel->state == HF_STATE_OPEN) {
#if HF_USE_CALL_MANAGER == XA_ENABLED
            status = CallMgrCallHold(Channel, HoldAction, Index, Command);
#else
            Command->type = HF_COMMAND_CALL_HOLD;
            Command->parms[0] = (U32)HoldAction;
            Command->parms[1] = (U32)Index;
            status = HfAddCommand(Channel, Command);
            if (status == BT_STATUS_SUCCESS) {
                status = BT_STATUS_PENDING;
            }
#endif
        } else {
            status = BT_STATUS_NO_CONNECTION;
        }
    }

    OS_UnlockStack();

    return status;
}

#if HF_USE_RESP_HOLD == XA_ENABLED
/*---------------------------------------------------------------------------
 *            HF_QueryResponseHold()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Issues a Query for the state of Response and Hold on the audio 
 *            gateway.  This feature is typically supported in Japanese markets.
 *
 * Return:    (See header file)
 */
BtStatus HF_QueryResponseHold(HfChannel *Channel, HfCommand *Command)
{
    BtStatus status = BT_STATUS_NOT_FOUND;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Channel != 0);
    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Command != 0);

    OS_LockStack();

    if (IsNodeOnList(&HF(channelList), &Channel->node)) {
        if (Channel->state == HF_STATE_OPEN) {
            Command->type = HF_COMMAND_QUERY_RESPONSE_HOLD;
            status = HfAddCommand(Channel, Command);
            if (status == BT_STATUS_SUCCESS) {
                status = BT_STATUS_PENDING;
            }
        } else {
            status = BT_STATUS_NO_CONNECTION;
        }
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            HF_ResponseHold()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Issues a Response and Hold command to the audio gateway to manage 
 *            incoming calls.
 *
 * Return:    (See header file)
 */
BtStatus HF_ResponseHold(HfChannel *Channel, HfHoldAction RespHoldAction, 
                         HfCommand *Command)
{
    BtStatus status = BT_STATUS_NOT_FOUND;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Channel != 0);
    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Command != 0);

    OS_LockStack();

    if (IsNodeOnList(&HF(channelList), &Channel->node)) {
        if (Channel->state == HF_STATE_OPEN) {
            Command->type = HF_COMMAND_RESPONSE_HOLD;
            Command->parms[0] = (U32)RespHoldAction;
            status = HfAddCommand(Channel, Command);
            if (status == BT_STATUS_SUCCESS) {
                status = BT_STATUS_PENDING;
            }
        } else {
            status = BT_STATUS_NO_CONNECTION;
        }
    }

    OS_UnlockStack();

    return status;
}

#endif

/*---------------------------------------------------------------------------
 *            HF_Hangup()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Hangup (or reject) an call.
 *
 * Return:    (See header file)
 *
 */
BtStatus HF_Hangup(HfChannel *Channel, HfCommand *Command)
{
    BtStatus status = BT_STATUS_NOT_FOUND;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Channel != 0);
    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Command != 0);

    OS_LockStack();

    if (IsNodeOnList(&HF(channelList), &Channel->node)) {
        if (Channel->state == HF_STATE_OPEN) {
#if HF_USE_CALL_MANAGER == XA_ENABLED
            status = CallMgrHangupCall(Channel, Command);
#else
            Command->type = HF_COMMAND_HANGUP_CALL;
            status = HfAddCommand(Channel, Command);
            if (status == BT_STATUS_SUCCESS) {
                status = BT_STATUS_PENDING;
            }
#endif
        } else {
            status = BT_STATUS_NO_CONNECTION;
        }
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            HF_ListCurrentCalls()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Queries the audio gateway for any calls.
 *
 * Return:    (See header file)
 */
BtStatus HF_ListCurrentCalls(HfChannel *Channel, HfCommand *Command)
{
    BtStatus status = BT_STATUS_NOT_FOUND;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Channel != 0);
    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Command != 0);

    OS_LockStack();

    if (IsNodeOnList(&HF(channelList), &Channel->node)) {
        if (Channel->state == HF_STATE_OPEN) {
            Command->type = HF_COMMAND_LIST_CURRENT_CALLS;
            status = HfAddCommand(Channel, Command);
            if (status == BT_STATUS_SUCCESS) {
                status = BT_STATUS_PENDING;
            }
        } else {
            status = BT_STATUS_NO_CONNECTION;
        }
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            HF_EnableCallerIdNotify()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Enables notification of call waiting.
 *
 * Return:    (See header file)
 *
 */
BtStatus HF_EnableCallerIdNotify(HfChannel *Channel, 
                                  BOOL Enabled, 
                                  HfCommand *Command)
{
    BtStatus status = BT_STATUS_NOT_FOUND;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Channel != 0);
    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Command != 0);

    OS_LockStack();

    if (IsNodeOnList(&HF(channelList), &Channel->node)) {
        if (Channel->state == HF_STATE_OPEN) {
            Command->type = HF_COMMAND_ENABLE_CID_NOTIFY;
            Command->parms[0] = (U32)Enabled;
            status = HfAddCommand(Channel, Command);
            if (status == BT_STATUS_SUCCESS) {
                status = BT_STATUS_PENDING;
            }
        } else {
            status = BT_STATUS_NO_CONNECTION;
        }
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            HF_EnableCallWaitNotify()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Enables notification of call waiting.
 *
 * Return:    (See header file)
 *
 */
BtStatus HF_EnableCallWaitNotify(HfChannel *Channel, 
                                  BOOL Enabled, 
                                  HfCommand *Command)
{
    BtStatus status = BT_STATUS_NOT_FOUND;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Channel != 0);
    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Command != 0);

    OS_LockStack();

    if (IsNodeOnList(&HF(channelList), &Channel->node)) {
        if (Channel->state == HF_STATE_OPEN) {
            Command->type = HF_COMMAND_ENABLE_WAIT_NOTIFY;
            Command->parms[0] = (U32)Enabled;
            status = HfAddCommand(Channel, Command);
            if (status == BT_STATUS_SUCCESS) {
                status = BT_STATUS_PENDING;
            }
        } else {
            status = BT_STATUS_NO_CONNECTION;
        }
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            HF_GenerateDtmf()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Send a DTMF code to the network. A call MUST be ongoing in order
 *            to generate a DTMF code.
 *
 * Return:    (See header file)
 *
 */
BtStatus HF_GenerateDtmf(HfChannel *Channel, U8 dtmfTone, HfCommand *Command)
{
    BtStatus status = BT_STATUS_NOT_FOUND;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Channel != 0);

    OS_LockStack();

    if (IsNodeOnList(&HF(channelList), &Channel->node)) {
        if (Channel->state == HF_STATE_OPEN) {
            Command->type = HF_COMMAND_GENERATE_DTMF;
            Command->parms[0] = dtmfTone;
            status = HfAddCommand(Channel, Command);
            if (status == BT_STATUS_SUCCESS) {
                status = BT_STATUS_PENDING;
            }
        } else {
            status = BT_STATUS_NO_CONNECTION;
        }
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            HF_GetLastVoiceTag()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Retrieves the number associated with the last voice tag recorded 
 *            in the Hands-free unit.
 *
 * Return:    (See header file)
 *
 */
BtStatus HF_GetLastVoiceTag(HfChannel *Channel, 
                             HfCommand *Command)
{
    BtStatus status = BT_STATUS_NOT_FOUND;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Channel != 0);
    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Command != 0);

    OS_LockStack();

    if (IsNodeOnList(&HF(channelList), &Channel->node)) {
        if (Channel->state == HF_STATE_OPEN) {
            Command->type = HF_COMMAND_GET_LAST_VOICE_TAG;
            status = HfAddCommand(Channel, Command);
            if (status == BT_STATUS_SUCCESS) {
                status = BT_STATUS_PENDING;
            }
        } else {
            status = BT_STATUS_NO_CONNECTION;
        }
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            HF_EnableVoiceRecognition()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Enables/Disables voice recognition on the audio gateway.
 *
 * Return:    (See header file)
 *
 */
BtStatus HF_EnableVoiceRecognition(HfChannel *Channel, 
                                   BOOL Enabled, 
                                   HfCommand *Command)
{
    BtStatus status = BT_STATUS_NOT_FOUND;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Channel != 0);
    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Command != 0);

    OS_LockStack();

    if (IsNodeOnList(&HF(channelList), &Channel->node)) {
        if (Channel->state == HF_STATE_OPEN) {
            Command->type = HF_COMMAND_VOICE_RECOGNITION;
            Command->parms[0] = (U32)Enabled;
            status = HfAddCommand(Channel, Command);
            if (status == BT_STATUS_SUCCESS) {
                status = BT_STATUS_PENDING;
            }
        } else {
            status = BT_STATUS_NO_CONNECTION;
        }
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            HF_DisableNREC()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Disables noise reduction and echo canceling.
 *
 * Return:    (See header file)
 *
 */
BtStatus HF_DisableNREC(HfChannel *Channel, 
                         HfCommand *Command)
{
    BtStatus status = BT_STATUS_NOT_FOUND;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Channel != 0);
    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Command != 0);

    OS_LockStack();

    if (IsNodeOnList(&HF(channelList), &Channel->node)) {
        if (Channel->state == HF_STATE_OPEN) {
            Command->type = HF_COMMAND_DISABLE_NREC;
            status = HfAddCommand(Channel, Command);
            if (status == BT_STATUS_SUCCESS) {
                status = BT_STATUS_PENDING;
            }
        } else {
            status = BT_STATUS_NO_CONNECTION;
        }
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            HF_ReportMicVolume()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Reports the current microphone gain of the Hands-free device.
 *
 * Return:    (See header file)
 *
 */
BtStatus HF_ReportMicVolume(HfChannel *Channel, U8 Gain, HfCommand *Command)
{
    BtStatus status = BT_STATUS_NOT_FOUND;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Channel != 0);
    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Command != 0);

    OS_LockStack();

    if (IsNodeOnList(&HF(channelList), &Channel->node)) {
        if (Channel->state == HF_STATE_OPEN) {
            /* Send the volume command */
            Command->type = HF_COMMAND_REPORT_MIC_VOLUME;
            Command->parms[0] = (U32)Gain;
            status = HfAddCommand(Channel, Command);
            if (status == BT_STATUS_SUCCESS) {
                status = BT_STATUS_PENDING;
            }
        } else {
            status = BT_STATUS_NO_CONNECTION;
        }
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            HF_ReportSpeakerVolume()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Reports the current speaker gain of the Hands-free device.
 *
 * Return:    (See header file)
 *
 */
BtStatus HF_ReportSpeakerVolume(HfChannel *Channel, U8 Gain, HfCommand *Command)
{
    BtStatus status = BT_STATUS_NOT_FOUND;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Channel != 0);
    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Command != 0);

    OS_LockStack();

    if (IsNodeOnList(&HF(channelList), &Channel->node)) {
        if (Channel->state == HF_STATE_OPEN) {
            /* Send the volume command */
            Command->type = HF_COMMAND_REPORT_SPEAKER_VOLUME;
            Command->parms[0] = (U32)Gain;
            status = HfAddCommand(Channel, Command);
            if (status == BT_STATUS_SUCCESS) {
                status = BT_STATUS_PENDING;
            }
        } else {
            status = BT_STATUS_NO_CONNECTION;
        }
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            HF_QueryNetworkOperator()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Queries the audio gateway for the Network Operator.
 *
 * Return:    (See header file)
 *
 */
BtStatus HF_QueryNetworkOperator(HfChannel *Channel, HfCommand *Command)
{
    BtStatus status = BT_STATUS_NOT_FOUND;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Channel != 0);
    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Command != 0);

    OS_LockStack();

    if (IsNodeOnList(&HF(channelList), &Channel->node)) {
        if (Channel->state == HF_STATE_OPEN) {
            /* Send the volume command */
            Command->type = HF_COMMAND_QUERY_NETWORK_OPER;
            status = HfAddCommand(Channel, Command);
            if (status == BT_STATUS_SUCCESS) {
                status = BT_STATUS_PENDING;
            }
        } else {
            status = BT_STATUS_NO_CONNECTION;
        }
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            HF_QuerySubscriberNumber()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Queries the audio gateway for the Subscriber Number.
 *
 * Return:    (See header file)
 *
 */
BtStatus HF_QuerySubscriberNumber(HfChannel *Channel, HfCommand *Command)
{
    BtStatus status = BT_STATUS_NOT_FOUND;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Channel != 0);
    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Command != 0);

    OS_LockStack();

    if (IsNodeOnList(&HF(channelList), &Channel->node)) {
        if (Channel->state == HF_STATE_OPEN) {
            /* Send the volume command */
            Command->type = HF_COMMAND_QUERY_SUBSCRIBER_NUM;
            status = HfAddCommand(Channel, Command);
            if (status == BT_STATUS_SUCCESS) {
                status = BT_STATUS_PENDING;
            }
        } else {
            status = BT_STATUS_NO_CONNECTION;
        }
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            HF_EnableExtendedErrors()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Enables extended error codes on the audio gateway.
 *
 * Return:    (See header file)
 *
 */
BtStatus HF_EnableExtendedErrors(HfChannel *Channel, HfCommand *Command)
{
    BtStatus status = BT_STATUS_NOT_FOUND;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Channel != 0);
    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Command != 0);

    OS_LockStack();

    if (IsNodeOnList(&HF(channelList), &Channel->node)) {
        if (Channel->state == HF_STATE_OPEN) {
            /* Send the volume command */
            Command->type = HF_COMMAND_ENABLE_EXTENDED_ERR;
            status = HfAddCommand(Channel, Command);
            if (status == BT_STATUS_SUCCESS) {
                status = BT_STATUS_PENDING;
            }
        } else {
            status = BT_STATUS_NO_CONNECTION;
        }
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            HF_SendAtCommand()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Queue an AT command for transmission.
 *
 * Return:    (See header file)
 *
 */
BtStatus HF_SendAtCommand(HfChannel *Channel, const char *AtString,
                          HfCommand *Command)
{
    BtStatus status = BT_STATUS_NOT_FOUND;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Channel != 0);
    CheckUnlockedParm(BT_STATUS_INVALID_PARM, AtString != 0);
    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Command != 0);

    OS_LockStack();

    if (IsNodeOnList(&HF(channelList), &Channel->node)) {
        if (Channel->state == HF_STATE_OPEN) {
            if (OS_StrLen(AtString) <= HF_TX_BUFFER_SIZE) {
                Command->parms[0] = (U32)AtString;
                Command->type = HF_COMMAND_SEND_AT_COMMAND;
                status = HfAddCommand(Channel, Command);
                if (status == BT_STATUS_SUCCESS) {
                    status = BT_STATUS_PENDING;
                }
            } else {
                status = BT_STATUS_FAILED;
            }
        } else {
            status = BT_STATUS_NO_CONNECTION;
        }
    }

    OS_UnlockStack();

    return status;
}

#if HF_USE_PHONEBOOK_COMMANDS == XA_ENABLED
/*---------------------------------------------------------------------------
 *            HF_QueryPhonebooks()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Queries the Audio Gateway for its supported phonebooks.
 *
 * Return:    (See header file)
 */
BtStatus HF_QueryPhonebooks(HfChannel *Channel, HfCommand *Command)
{
    BtStatus        status = BT_STATUS_NOT_FOUND;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Channel != 0);
    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Command != 0);

    OS_LockStack();

    if (IsNodeOnList(&HF(channelList), &Channel->node)) {
        if (Channel->state == HF_STATE_OPEN) {
            Command->type = HF_COMMAND_QUERY_PB;
            status = HfAddCommand(Channel, Command);
            if (status == BT_STATUS_SUCCESS) {
                status = BT_STATUS_PENDING;
            }
        } else {
            status = BT_STATUS_NO_CONNECTION;
        }
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            HF_SelectPhonebook()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Makes the specifiec phonebook the active one for subsequent 
 *            calls.
 *
 * Return:    (See header file)
 */
BtStatus HF_SelectPhonebook(HfChannel *Channel, 
                            HfPhonebooks Phonebook, 
                            HfCommand *Command)
{
    BtStatus       status = BT_STATUS_NOT_FOUND;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Channel != 0);
    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Command != 0);

    OS_LockStack();

    if (IsNodeOnList(&HF(channelList), &Channel->node)) {
        if (Channel->state == HF_STATE_OPEN) {
            Command->type = HF_COMMAND_SELECT_PB;
            Command->parms[0] = (U32)Phonebook;
            status = HfAddCommand(Channel, Command);
            if (status == BT_STATUS_SUCCESS) {
                status = BT_STATUS_PENDING;
            }
        } else {
            status = BT_STATUS_NO_CONNECTION;
        }
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            HF_GetCurrentPhonebookInfo()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Queries for the active phonebook.
 *
 * Return:    (See header file)
 */
BtStatus HF_GetCurrentPhonebookInfo(HfChannel *Channel, HfCommand *Command)
{
    BtStatus       status = BT_STATUS_NOT_FOUND;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Channel != 0);
    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Command != 0);

    OS_LockStack();

    if (IsNodeOnList(&HF(channelList), &Channel->node)) {
        if (Channel->state == HF_STATE_OPEN) {
            Command->type = HF_COMMAND_GET_CURRENT_PB_INFO;
            status = HfAddCommand(Channel, Command);
            if (status == BT_STATUS_SUCCESS) {
                status = BT_STATUS_PENDING;
            }
        } else {
            status = BT_STATUS_NO_CONNECTION;
        }
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            HF_GetPhonebookSize()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Gets the number of entries and the size of the elements of the
 *            active phonebook.
 *
 * Return:    (See header file)
 */
BtStatus HF_GetPhonebookSize(HfChannel *Channel, HfCommand *Command)
{
    BtStatus        status = BT_STATUS_NOT_FOUND;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Channel != 0);
    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Command != 0);

    OS_LockStack();

    if (IsNodeOnList(&HF(channelList), &Channel->node)) {
        if (Channel->state == HF_STATE_OPEN) {
            Command->type = HF_COMMAND_GET_PB_SIZE;
            status = HfAddCommand(Channel, Command);
            if (status == BT_STATUS_SUCCESS) {
                status = BT_STATUS_PENDING;
            }
        } else {
            status = BT_STATUS_NO_CONNECTION;
        }
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            HF_ReadPhonebooksEntries()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Reads entries from the specified phonebook in the specified 
 *            range.
 *
 * Return:    (See header file)
 */
BtStatus HF_ReadPhonebookEntries(HfChannel *Channel, 
                                  U16 From, U16 To, 
                                  HfCommand *Command)
{
    BtStatus        status = BT_STATUS_NOT_FOUND;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Channel != 0);
    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Command != 0);

    OS_LockStack();

    if (IsNodeOnList(&HF(channelList), &Channel->node)) {
        if (Channel->state == HF_STATE_OPEN) {
            Command->type = HF_COMMAND_READ_PB_ENTRIES;
            Command->parms[0] = (U32)From;
            Command->parms[1] = (U32)To;
            status = HfAddCommand(Channel, Command);
            if (status == BT_STATUS_SUCCESS) {
                status = BT_STATUS_PENDING;
            }
        } else {
            status = BT_STATUS_NO_CONNECTION;
        }
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            HF_FindPhonebookEntries()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Finds all entries beginning with the specified string.
 *
 * Return:    (See header file)
 */
BtStatus HF_FindPhonebookEntries(HfChannel *Channel, const char *Text, 
                                 HfCommand *Command)
{
    BtStatus        status = BT_STATUS_NOT_FOUND;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Channel != 0);
    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Command != 0);

    OS_LockStack();

    if (IsNodeOnList(&HF(channelList), &Channel->node)) {
        if (Channel->state == HF_STATE_OPEN) {
            Command->type = HF_COMMAND_FIND_PB_ENTRIES;
            Command->parms[0] = (U32)Text;
            status = HfAddCommand(Channel, Command);
            if (status == BT_STATUS_SUCCESS) {
                status = BT_STATUS_PENDING;
            }
        } else {
            status = BT_STATUS_NO_CONNECTION;
        }
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            HF_WritePhonebookEntry()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Writes the phonebook entry.
 *
 * Return:    (See header file)
 */
BtStatus HF_WritePhonebookEntry(HfChannel *Channel, 
                                 U16 Index, 
                                 const char *Number, 
                                 const char *Text, 
                                 U8  Type,
                                 HfCommand *Command)
{
    BtStatus        status = BT_STATUS_NOT_FOUND;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Channel != 0);
    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Command != 0);

    OS_LockStack();

    if (IsNodeOnList(&HF(channelList), &Channel->node)) {
        if (Channel->state == HF_STATE_OPEN) {
            Command->type = HF_COMMAND_WRITE_PB_ENTRY;
            Command->parms[0] = (U32)Index;
            Command->parms[1] = (U32)Number;
            Command->parms[2] = (U32)Text;
            Command->parms[3] = (U32)Type;
            status = HfAddCommand(Channel, Command);
            if (status == BT_STATUS_SUCCESS) {
                status = BT_STATUS_PENDING;
            }
        } else {
            status = BT_STATUS_NO_CONNECTION;
        }
    }

    OS_UnlockStack();

    return status;
}

#endif

/*---------------------------------------------------------------------------
 *            HF_IsChannelOpen()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Returns true of a channel is open.
 *
 * Return:    (See header file)
 */
BOOL HF_IsChannelOpen(HfChannel *Channel)
{
    BOOL state = FALSE;

    OS_LockStack();

    if ((Channel) && (IsNodeOnList(&HF(channelList), &Channel->node))) {
        state = Channel->state == HF_STATE_OPEN ? TRUE : FALSE;
    }

    OS_UnlockStack();

    return state;
}

/*---------------------------------------------------------------------------
 *            HF_IsACLConnected()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Returns TRUE if the ACL link is up.
 *
 * Return:    (See header file)
 */
BOOL HF_IsACLConnected(HfChannel *Channel)
{
    BOOL state = FALSE;

    OS_LockStack();

    if ((Channel) && (IsNodeOnList(&HF(channelList), &Channel->node))) {
        state = Channel->linkFlags & HF_LINK_ACL ? TRUE : FALSE;
    }

    OS_UnlockStack();

    return state;
}

/*---------------------------------------------------------------------------
 *            HF_IsHandsfreeConnected()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Returns TRUE if the handsfree profile is connected.
 *
 * Return:    (See header file)
 */
BOOL HF_IsHandsfreeConnected(HfChannel *Channel)
{
    BOOL state = FALSE;

    OS_LockStack();

    if ((Channel) && (IsNodeOnList(&HF(channelList), &Channel->node))) {
        state = Channel->linkFlags & HF_LINK_HANDSFREE ? TRUE : FALSE;
    }

    OS_UnlockStack();

    return state;
}

/*---------------------------------------------------------------------------
 *            HF_IsAudioConnected()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Returns TRUE if an audio connection is up.
 *
 * Return:    (See header file)
 */
BOOL HF_IsAudioConnected(HfChannel *Channel)
{
    BOOL state = FALSE;

    OS_LockStack();

    if ((Channel) && (IsNodeOnList(&HF(channelList), &Channel->node))) {
        state = CMGR_IsAudioUp(&Channel->cmgrHandler);
    }

    OS_UnlockStack();

    return state;
}

#if HF_USE_CALL_MANAGER == XA_ENABLED
/*---------------------------------------------------------------------------
 *            HF_CallState()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Returns the current call state of the specified line.
 *
 * Return:    (See header file)
 */
HfCallStatus HF_CallState(HfChannel *Channel, U8 Line)
{
    HfCallStatus callState = HF_CALL_STATUS_UNKNOWN;

    OS_LockStack();

    if ((Channel) && IsNodeOnList(&HF(channelList), &Channel->node)) {
        if (Channel->state == HF_STATE_OPEN) {
            callState = Channel->callState[Line];
        }
    }

    OS_UnlockStack();

    return callState;
}
#endif

/*---------------------------------------------------------------------------
 *            HF_GatewayFeatures()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Returns the gateway features.
 *
 * Return:    (See header file)
 */
HfGatewayFeatures HF_GatewayFeatures(HfChannel *Channel)
{
    HfGatewayFeatures features = 0;

    OS_LockStack();

    if ((Channel) && IsNodeOnList(&HF(channelList), &Channel->node)) {
        if (Channel->state == HF_STATE_OPEN) {
            features = Channel->gwFeatures;
        }
    }

    OS_UnlockStack();

    return features;
}

/*---------------------------------------------------------------------------
 *            HF_RemoteDevice()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Returns the remote device associated with the channel.
 *
 * Return:    (See header file)
 */
BtRemoteDevice * HF_RemoteDevice(HfChannel *Channel)
{
    BtRemoteDevice *remDev = 0;

    OS_LockStack();

    if ((Channel) && (IsNodeOnList(&HF(channelList), &Channel->node))) {
        if (Channel->cmgrHandler.bdc) {
            remDev = Channel->cmgrHandler.bdc->link;
        }
    }

    OS_UnlockStack();

    return remDev;
}

/*---------------------------------------------------------------------------
 *            HF_ProfileVersion()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Returns the profile version retrieved from the SDP database of 
 *            the remote device for this service connection.
 *
 * Returns:   (See header file)
 */
HfGatewayVersion HF_ProfileVersion(HfChannel *Channel)
{
    HfGatewayVersion version = HF_GW_VERSION_UNKNOWN;

    OS_LockStack();

    if ((Channel) && IsNodeOnList(&HF(channelList), &Channel->node)) {
        if (Channel->state == HF_STATE_OPEN) {
            version = Channel->version;
        }
    }

    OS_UnlockStack();

    return version;
}


/*---------------------------------------------------------------------------
 *            HF_GatewayHoldFeatures()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Returns the hold features of the audio gateway.
 *
 * Returns:   (See header file)
 */
HfGwHoldFeatures HF_GatewayHoldFeatures(HfChannel *Channel)
{
    HfGwHoldFeatures gwHoldFeatures = 0;

    OS_LockStack();

    if ((Channel) && IsNodeOnList(&HF(channelList), &Channel->node)) {
        if (Channel->state == HF_STATE_OPEN) {
            gwHoldFeatures = Channel->gwHoldFeatures;
        }
    }

    OS_UnlockStack();

    return gwHoldFeatures;
}

/*---------------------------------------------------------------------------
 *            HF_RegisterCmdOverride()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Registers a callback function to be called any time the command
 *            state machine is called.
 *
 * Returns:   (See header file)
 */
void HF_RegisterCmdOverride(HfCmdOverride Callback)
{
    OS_LockStack();

    HF(cmdOverride) = Callback;

    OS_UnlockStack();
}


/*---------------------------------------------------------------------------
 *             HF_SpeakerGain()
 *---------------------------------------------------------------------------
 *
 * Synopsis:   Returns the current value of speaker gain.
 *
 * Return:     (See header file)
 */
U8 HF_SpeakerGain(HfChannel *Channel)
{
    U8 gain = 0;

    OS_LockStack();

    if ((Channel) && IsNodeOnList(&HF(channelList), &Channel->node)) {
        if (Channel->state == HF_STATE_OPEN) {
            gain = Channel->speakerGain;
        }
    }

    OS_UnlockStack();

    return gain;
}

/*---------------------------------------------------------------------------
 *             HF_MicGain()
 *---------------------------------------------------------------------------
 *
 * Synopsis:   Returns the current value of microphone gain.
 *
 * Return:     (See header file)
 */
U8 HF_MicGain(HfChannel *Channel)
{
    U8 gain = 0;

    OS_LockStack();

    if ((Channel) && IsNodeOnList(&HF(channelList), &Channel->node)) {
        if (Channel->state == HF_STATE_OPEN) {
            gain = Channel->micGain;
        }
    }

    OS_UnlockStack();

    return gain;
}

/*---------------------------------------------------------------------------
 *             HF_IsNRECEnabled()
 *---------------------------------------------------------------------------
 *
 * Synopsis:   Returns TRUE if Noise Reduction and Echo Cancelling is enabled in 
 *             the audio gateway.
 *
 * Return:     (See header file);
 */
BOOL HF_IsNRECEnabled(HfChannel *Channel)
{
    BOOL enabled = FALSE;

    OS_LockStack();

    if ((Channel) && IsNodeOnList(&HF(channelList), &Channel->node)) {
        if (Channel->state == HF_STATE_OPEN) {
            enabled = Channel->nrec;
        }
    }

    OS_UnlockStack();

    return enabled;
}

/*---------------------------------------------------------------------------
 *             HF_IsInbandRingEnabled()
 *---------------------------------------------------------------------------
 *
 * Synopsis:   Returns TRUE if In-band Ringing is is enabled in the audio 
 *             gateway.
 *
 * Return:     (See header file);
 */
BOOL HF_IsInbandRingEnabled(HfChannel *Channel)
{
    BOOL enabled = FALSE;

    OS_LockStack();

    if ((Channel) && IsNodeOnList(&HF(channelList), &Channel->node)) {
        if (Channel->state == HF_STATE_OPEN) {
            enabled = Channel->ibRing;
        }
    }

    OS_UnlockStack();

    return enabled;
}

/*---------------------------------------------------------------------------
 *             HF_IsCallIdNotifyEnabled()
 *---------------------------------------------------------------------------
 *
 * Synopsis:   Returns TRUE if Caller ID notification is enabled in the audio 
 *             gateway.
 *
 * Return:     (See header file);
 */
BOOL HF_IsCallIdNotifyEnabled(HfChannel *Channel)
{
    BOOL enabled = FALSE;

    OS_LockStack();

    if ((Channel) && IsNodeOnList(&HF(channelList), &Channel->node)) {
        if (Channel->state == HF_STATE_OPEN) {
            enabled = Channel->callId;
        }
    }

    OS_UnlockStack();

    return enabled;
}

/*---------------------------------------------------------------------------
 *             HF_IsVoiceRecActive()
 *---------------------------------------------------------------------------
 *
 * Synopsis:   Returns TRUE if Voice Recognition is active in the audio gateway.  
 *
 * Return:     (See header file);
 */
BOOL HF_IsVoiceRecActive(HfChannel *Channel)
{
    BOOL active = FALSE;

    OS_LockStack();

    if ((Channel) && IsNodeOnList(&HF(channelList), &Channel->node)) {
        if (Channel->state == HF_STATE_OPEN) {
            active = Channel->voiceRec;
        }
    }

    OS_UnlockStack();

    return active;
}

/*---------------------------------------------------------------------------
 *             HF_IsCallWaitingActive()
 *---------------------------------------------------------------------------
 *
 * Synopsis:   Returns TRUE if Call Waiting is active in the audio gateway.  
 *
 * Return:     (See header file);
 */
BOOL HF_IsCallWaitingActive(HfChannel *Channel)
{
    BOOL active = FALSE;

    OS_LockStack();

    if ((Channel) && IsNodeOnList(&HF(channelList), &Channel->node)) {
        if (Channel->state == HF_STATE_OPEN) {
            active = Channel->callWaiting;
        }
    }

    OS_UnlockStack();

    return active;
}

#if HF_SNIFF_TIMER >= 0
/*---------------------------------------------------------------------------
 *             HF_EnableSniffMode
 *---------------------------------------------------------------------------
 *
 * Synopsis:    Enables/Disables placing link into sniff mode on the specified 
 *              channel
 */
BtStatus HF_EnableSniffMode(HfChannel *Channel, BOOL Enable) 
{
    BtSniffInfo sniffInfo;
    BtStatus    status = BT_STATUS_NOT_FOUND;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Channel != 0);

    OS_LockStack();

    if ((Channel) && IsNodeOnList(&HF(channelList), &Channel->node)) {
        if (Enable) {
            if (!(Channel->flags & CHANNEL_FLAG_SNIFFING) && 
                (Channel->state == HF_STATE_OPEN)) {
                /* Start the sniff timer */
                sniffInfo.minInterval = HF_SNIFF_MIN_INTERVAL;
                sniffInfo.maxInterval = HF_SNIFF_MAX_INTERVAL;
                sniffInfo.attempt = HF_SNIFF_ATTEMPT;
                sniffInfo.timeout = HF_SNIFF_TIMEOUT;
                CMGR_SetSniffTimer(&Channel->cmgrHandler, &sniffInfo, 
                                   HF_SNIFF_TIMER);
            }
        } else {
            if ((Channel->flags & CHANNEL_FLAG_SNIFFING) && 
                (Channel->state == HF_STATE_OPEN)) {
                CMGR_ClearSniffTimer(&Channel->cmgrHandler);
            }
        }

        /* Set the internal state */
        if (Enable) {
            Channel->flags |= CHANNEL_FLAG_SNIFFING;
        } else {
            Channel->flags &= ~CHANNEL_FLAG_SNIFFING;
        }

        status = BT_STATUS_SUCCESS;
    }

    OS_UnlockStack();

    return status;
}
#endif

