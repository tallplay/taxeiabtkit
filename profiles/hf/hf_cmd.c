/****************************************************************************
 *
 * File:
 *     $Workfile:hf_cmd.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:8$
 *
 * Description: This file contains the command queuing functions for the 
 *              Hands-free SDK.
 *             
 * Created:     February 11, 2005
 *
 * Copyright 2000-2005 Extended Systems, Inc.

 * Portions copyright 2005 iAnywhere Solutions, Inc.

 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any means, 
 * or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions,  
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "sys/hf_cmd.h"
#include "sys/hfalloc.h"

#if HF_USE_CALL_MANAGER == XA_ENABLED
#include "sys/hf_cmgr.h"
#endif

/*---------------------------------------------------------------------------
 *            HfAddCommand()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Add a logical command to the queue.
 *
 */
BtStatus HfAddCommand(HfChannel *Channel, HfCommand *Command)
{
    BtStatus status;

    if (!IsNodeOnList(&Channel->cmdQueue, &Command->node)) {
        Command->state = 0;
        if (IsListEmpty(&Channel->cmdQueue)) {
            InsertTailList(&Channel->cmdQueue, &Command->node);
            status = HfExecuteNextCommand(Channel);
            if (status != BT_STATUS_PENDING) {
                RemoveEntryList(&Command->node);
            }
        } else {
            InsertTailList(&Channel->cmdQueue, &Command->node);
        }
        status = BT_STATUS_SUCCESS;
    } else {
        status = BT_STATUS_IN_USE;
    }

    return status;
}

/*---------------------------------------------------------------------------
 *            HfRemoveCommand()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Remove a logical command from the queue.
 *
 */
BtStatus HfRemoveCommand(HfChannel *Channel, HfCommand *Command)
{
    BtStatus status;

    if (IsNodeOnList(&Channel->cmdQueue, &Command->node)) {
        RemoveEntryList(&Command->node);
        status = BT_STATUS_SUCCESS;
    } else {
        status = BT_STATUS_NOT_FOUND;
    }

    return status;
}

/*---------------------------------------------------------------------------
 *            HfEnumCommands()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Enumerate commands on the queue.
 *
 */
HfCommand * HfEnumCommands(HfChannel *Channel, HfCommand *Command)
{
    HfCommand *command = 0;

    if (!Command) {
        /* Get the first entry */
        if (!IsListEmpty(&Channel->cmdQueue)) {
            command = (HfCommand *)GetHeadList(&Channel->cmdQueue);
        }
    } else {
        /* Get the next entry */
        if (!IsNodeOnList(&Channel->cmdQueue, &Command->node)) {
            command = (HfCommand *)GetNextNode(&Command->node);
            if (command == (HfCommand *)&Channel->cmdQueue) {
                /* At the end of the list */
                command = 0;
            }
        }
    }

    return command;
}

/*---------------------------------------------------------------------------
 *            HfCompleteCommand()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Complete a command and notify the application.
 *
 */
BtStatus HfCompleteCommand(HfChannel *Channel, 
                            HfCommand *Command)
{
    BtStatus    status = BT_STATUS_NOT_FOUND;

    if (IsNodeOnList(&Channel->cmdQueue, &Command->node)) {

#if HF_USE_CALL_MANAGER == XA_ENABLED
        /* Indicate completion to Call Manager */
        switch (Command->type) {
        case HF_COMMAND_ANSWER_CALL:
        case HF_COMMAND_HANGUP_CALL:
        case HF_COMMAND_DIAL_NUMBER:       
        case HF_COMMAND_DIAL_MEMORY:       
        case HF_COMMAND_REDIAL:
        case HF_COMMAND_CALL_HOLD:
            CallManager(Channel, EG_STATE_MACHINE, EVENT_COMMAND_COMPLETE, 0);
            break;
        }
#endif

        /* Indicate completion to the application */
        status = HfRemoveCommand(Channel, Command);
        HfAppCallback(Channel, HF_EVENT_COMMAND_COMPLETE, 
                       Command->status, Command);
    }

    return status;
}

/*---------------------------------------------------------------------------
 *            HfGetCurrentCommand()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Get a pointer to the first command on the queue.
 *
 */
HfCommand * HfGetCurrentCommand(HfChannel *Channel)
{
    HfCommand *command = 0;

    if (!IsListEmpty(&Channel->cmdQueue)) {
        command = (HfCommand *)GetHeadList(&Channel->cmdQueue);
    }

    return command;
}

/*---------------------------------------------------------------------------
 *            HfFlushCommands()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Flushes all commands on the queue.
 *
 */
void HfFlushCommands(HfChannel *Channel, BtStatus Status)
{
    HfCommand *command;

    while (!IsListEmpty(&Channel->cmdQueue)) {
        command = (HfCommand *)GetHeadList(&Channel->cmdQueue);
        command->status = Status;
        AssertEval(HfCompleteCommand(Channel, command) == BT_STATUS_SUCCESS);
    }
}

/*---------------------------------------------------------------------------
 *            HfExecuteNextCommand()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Executes the next step in the current command on the queue, or
 *            completes and starts the next command.
 *
 */
BtStatus HfExecuteNextCommand(HfChannel *Channel)
{
    BtStatus   status = BT_STATUS_NOT_FOUND;
    HfCommand *command;

    while (!IsListEmpty(&Channel->cmdQueue)) {
        command = HfGetCurrentCommand(Channel);
        Assert(command);

        if (HF(cmdOverride)) {
            if (HF(cmdOverride)(Channel, command) != BT_STATUS_FAILED) {
                goto xmit;
            }
        }

        if (command->state == 0) {
            /* This is the initial execution of this command */
            switch (command->type) {
            case HF_COMMAND_ANSWER_CALL:
                Channel->atCommand.type = AT_ANSWER;
                break;
            case HF_COMMAND_HANGUP_CALL:       
                Channel->atCommand.type = AT_HANG_UP;
                break;
            case HF_COMMAND_DIAL_NUMBER:       
                Channel->atCommand.type = AT_DIAL_NUMBER;
                Channel->atCommand.p.hf.dial.number = (U8 *)command->parms[0];
                break;
            case HF_COMMAND_DIAL_MEMORY:       
                Channel->atCommand.type = AT_DIAL_MEMORY;
                Channel->atCommand.p.hf.dial.number = (U8 *)command->parms[0];
                break;
            case HF_COMMAND_REDIAL:
                Channel->atCommand.type = AT_REDIAL;
                break;
            case HF_COMMAND_ENABLE_WAIT_NOTIFY:
                Channel->atCommand.type = AT_CALL_WAIT_NOTIFY;
                Channel->atCommand.p.hf.wait.notify = (BOOL)(command->parms[0] ? 1: 0);
                break;
            case HF_COMMAND_ENABLE_CID_NOTIFY:
                Channel->atCommand.type = AT_CALL_ID;
                Channel->atCommand.p.hf.callId.enabled = (BOOL)(command->parms[0] ? 1: 0);
                break;
            case HF_COMMAND_DISABLE_NREC:
                Channel->atCommand.type = AT_ECHO_C_AND_NOISE_R;
                break;
            case HF_COMMAND_CALL_HOLD:
                Channel->atCommand.type = AT_CALL_HOLD;
                Channel->atCommand.p.hf.hold.action = (U8)command->parms[0];
                Channel->atCommand.p.hf.hold.call = (U8)command->parms[1];
                break;
            case HF_COMMAND_VOICE_RECOGNITION:
                Channel->atCommand.type = AT_VOICE_RECOGNITION;
                Channel->atCommand.p.hf.vrec.enabled = (BOOL)(command->parms[0] ? 1: 0);
                break;
            case HF_COMMAND_GET_LAST_VOICE_TAG:
                Channel->atCommand.type = AT_VOICE_TAG;
                break;
            case HF_COMMAND_REPORT_SPEAKER_VOLUME:
                Channel->atCommand.type = AT_SPEAKER_GAIN;
                Channel->atCommand.p.hs.speaker.gain = (U8)command->parms[0];
                break;
            case HF_COMMAND_REPORT_MIC_VOLUME:
                Channel->atCommand.type = AT_MICROPHONE_GAIN;
                Channel->atCommand.p.hs.mic.gain = (U8)command->parms[0];
                break;
            case HF_COMMAND_QUERY_NETWORK_OPER:
                if (!(Channel->flags & CHANNEL_FLAG_COPS_SET)) {
                    Channel->atCommand.type = AT_NETWORK_OPERATOR;
                    Channel->atCommand.p.hf.networkOper.mode = 3;
                    Channel->atCommand.p.hf.networkOper.format = 0;
                } else {
                    Channel->atCommand.type = AT_NETWORK_OPERATOR | AT_READ;
                }
                break;
            case HF_COMMAND_QUERY_SUBSCRIBER_NUM:
                Channel->atCommand.type = AT_SUBSCRIBER_NUM;
                break;
            case HF_COMMAND_GENERATE_DTMF:
                Channel->atCommand.type = AT_GENERATE_DTMF_TONE;
                Channel->atCommand.p.hf.dtmf.tone = (U8)command->parms[0];
                break;
            case HF_COMMAND_LIST_CURRENT_CALLS:
                Channel->atCommand.type = AT_LIST_CURRENT_CALLS;
                break;
            case HF_COMMAND_ENABLE_EXTENDED_ERR:
                Channel->atCommand.type = AT_SET_ERROR_MODE;
                Channel->atCommand.p.error.mode = 1;
                break;

#if HF_USE_RESP_HOLD == XA_ENABLED
            case HF_COMMAND_QUERY_RESPONSE_HOLD:
                Channel->atCommand.type = AT_RESPONSE_AND_HOLD | AT_READ;
                break;
            case HF_COMMAND_RESPONSE_HOLD:
                Channel->atCommand.type = AT_RESPONSE_AND_HOLD;
                Channel->atCommand.p.hf.btrh.setting = 
                                            (HfHoldAction)command->parms[0];
                break;
#endif
            case HF_COMMAND_SEND_AT_COMMAND:
                break;

#if HF_USE_PHONEBOOK_COMMANDS == XA_ENABLED
            case HF_COMMAND_QUERY_PB:
                Channel->atCommand.type = AT_SELECT_PHONEBOOK_STORAGE | AT_TEST;
                break;
            case HF_COMMAND_SELECT_PB:
                Channel->atCommand.type = AT_SELECT_PHONEBOOK_STORAGE;
                Channel->atCommand.p.pb.storage.select = 
                                            (AtPbStorageType)command->parms[0];
                break;
            case HF_COMMAND_GET_CURRENT_PB_INFO:
                Channel->atCommand.type = AT_SELECT_PHONEBOOK_STORAGE | AT_READ;
                break;
            case HF_COMMAND_GET_PB_SIZE:
                Channel->atCommand.type = AT_READ_PHONEBOOK_ENTRY | AT_TEST;
                break;
            case HF_COMMAND_READ_PB_ENTRIES:
                Channel->atCommand.type = AT_READ_PHONEBOOK_ENTRY;
                Channel->atCommand.p.pb.read.first = (U16)command->parms[0];
                Channel->atCommand.p.pb.read.last = (U16)command->parms[1];
                break;
            case HF_COMMAND_FIND_PB_ENTRIES:
                Channel->atCommand.type = AT_FIND_PHONEBOOK_ENTRY;
                Channel->atCommand.p.pb.find.text = 
                                                (const char *)command->parms[0];
                break;
            case HF_COMMAND_WRITE_PB_ENTRY:
                Channel->atCommand.type = AT_WRITE_PHONEBOOK_ENTRY;
                Channel->atCommand.p.pb.write.index = (U16)command->parms[0];
                Channel->atCommand.p.pb.write.number = 
                                                (const char *)command->parms[1];
                Channel->atCommand.p.pb.write.text = (U8 *)command->parms[2];
                Channel->atCommand.p.pb.write.type = 
                                              (AtNumberFormat)command->parms[3];
                break;
#endif

            }
            status = BT_STATUS_PENDING;
        } else {
            if (command->type == HF_COMMAND_QUERY_NETWORK_OPER) {
                if (!(Channel->flags & CHANNEL_FLAG_COPS_SET)) {
                    Channel->flags |= CHANNEL_FLAG_COPS_SET;
                    Channel->atCommand.type = AT_NETWORK_OPERATOR | AT_READ;
                    status = BT_STATUS_PENDING;
                } else {
                    status = BT_STATUS_SUCCESS;
                }
            } else {
                status = BT_STATUS_SUCCESS;
            }
        }

        xmit:

        if (status == BT_STATUS_SUCCESS) {
            status = HfCompleteCommand(Channel, command);
            if (status != BT_STATUS_SUCCESS) {
                /* A critical error occured */
                break;
            }
        } else if (status == BT_STATUS_PENDING) {
            if (command->type == HF_COMMAND_SEND_AT_COMMAND) {
                status = AtSendRaw(Channel, (const char *)command->parms[0]);
            } else {
                status = AtSendCommand(Channel, &Channel->atCommand);
            }
            command->state++;
            break;
        }
    }

    return status;
}


