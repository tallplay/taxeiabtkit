/****************************************************************************
 *
 * File:
 *     $Workfile:hf_sm.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:20$
 *
 * Description: This file contains the connection state machine for the 
 *     hands-free component of the Hands-free SDK.
 *             
 * Created:     February 11, 2005
 *
 * Copyright 2000-2005 Extended Systems, Inc.

 * Portions copyright 2005 iAnywhere Solutions, Inc.

 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any means, 
 * or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions,  
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "sys/hfi.h"
#include "sys/hf_cmd.h"
#include "sys/hfalloc.h"

#if HF_USE_CALL_MANAGER == XA_ENABLED
#include "sys/hf_cmgr.h"
#endif

/*---------------------------------------------------------------------------
 *            HfStateClosed()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handles the closed state for the HF state machine.
 *
 * Return:    void
 */
static void HfStateClosed(HfChannel *Channel, U8 Event, AtResults *Parms)
{
    if (Event == EVENT_OPEN_IND) {
        /* Incoming connection, initialize channel state */
        if (RF_AcceptChannel(Channel->hfChannel) == BT_STATUS_PENDING) {
            Channel->state = HF_STATE_CONN_INCOMING;
            Channel->version = 0;
            Channel->gwFeatures = 0;
            HF(hfChannel) = 0;

            HfAppCallback(Channel, HF_EVENT_SERVICE_CONNECT_REQ,
                           BT_STATUS_SUCCESS, Channel->cmgrHandler.bdc->link);
        }
    }

    /* Ignore any other events */
}

/*---------------------------------------------------------------------------
 *            HfStateConnPending()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handles the connect pending state for the HF state machine.
 *
 * Return:    void
 */
static void HfStateConnPending(HfChannel *Channel, U8 Event, AtResults *Parms)
{
    if (Event == EVENT_OPEN) {
        Channel->state = HF_STATE_NEGOTIATE;
        if (!(Channel->flags & CHANNEL_FLAG_OUTGOING) || 
            (Channel->version != HF_GW_VERSION_0_96)) {
            /* Not a .96 device, send BRSF */
            Channel->atCommand.type = AT_SUPPORTED_FEATURES;
            Channel->atCommand.p.hf.features.bitmap = HF_SDK_FEATURES;
            Channel->flags |= CHANNEL_FLAG_TEST_INDICATORS;
        } else {
            /* This is a .96 device, features come from SDP */
            if (Channel->queryFlags & SDP_QUERY_FLAG_FEATURES) {
                /* Supported features where found in the SDP entry */
                if (Channel->gwFeatures & HF_GW_FEATURE_IN_BAND_RING) {
                    Channel->ibRing = TRUE;
                }
                if (Channel->gwFeatures & HF_GW_FEATURE_ECHO_NOISE) {
                    Channel->nrec = TRUE;
                }
            }

            /* Notify the app of supported features */
            HfAppCallback(Channel, HF_EVENT_GATEWAY_FEATURES, BT_STATUS_SUCCESS, 
                           (void *)(U32)Channel->gwFeatures);

            /* Test indicators */
            Channel->atCommand.type = AT_INDICATORS | AT_TEST;
            Channel->flags |= CHANNEL_FLAG_READ_INDICATORS;
        }

        if (AtSendCommand(Channel, &Channel->atCommand) != BT_STATUS_PENDING) {
            /* Fatal error, Failed to send command */
            (void)RF_CloseChannel(Channel->hfChannel);
        }
    }

    /* Ignore any other events */
}

/*---------------------------------------------------------------------------
 *            HfStateIncoming()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handles the incoming state for the HF state machine.
 *
 * Return:    void
 */
static void HfStateConnIncoming(HfChannel *Channel, U8 Event, AtResults *Parms)
{
    /* Incoming or outgoing, the same negotiation sequence is used */
    HfStateConnPending(Channel, Event, Parms);
}

/*---------------------------------------------------------------------------
 *            HfStateNegotiate()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handles the negotiate state for the HF state machine.
 *
 * Return:    void
 */
static void HfStateNegotiate(HfChannel *Channel, U8 Event, AtResults *Parms)
{
#if HF_SNIFF_TIMER >= 0
    BtSniffInfo sniffInfo;
#endif

    switch (Event) {
    case EVENT_AT_DATA:
        switch (Parms->type) {
        case AT_OK:
        case AT_ERROR:
        case AT_EXTENDED_ERROR:
            /* Hands-Free */
            if (Channel->flags & CHANNEL_FLAG_TEST_INDICATORS) {

                if ((Parms->type != AT_OK) &&
                    !(Channel->flags & CHANNEL_FLAG_OUTGOING) &&
                    (Channel->currentAtCommand->type == AT_SUPPORTED_FEATURES)) {

                    /* Supported features (BRSF) failed, do SDP query */
                    Channel->sdpQueryToken.attribId = AID_PROTOCOL_DESC_LIST;
                    if (HfStartServiceQuery(Channel, BSQM_FIRST) != 
                        BT_STATUS_PENDING) {
                        /* Can't start a query */
                        (void)RF_CloseChannel(Channel->hfChannel);
                    }
                }

                /* Get supported indicators */
                Channel->flags &= ~CHANNEL_FLAG_TEST_INDICATORS;
                Channel->atCommand.type = AT_INDICATORS | AT_TEST;
                Channel->flags |= CHANNEL_FLAG_READ_INDICATORS;
            } else if (Channel->flags & CHANNEL_FLAG_READ_INDICATORS) {

                /* Read the current values of the indicators */
                Channel->flags &= ~CHANNEL_FLAG_READ_INDICATORS;
                Channel->atCommand.type = AT_INDICATORS | AT_READ;
                Channel->flags |= CHANNEL_FLAG_EVENT_REPORT;
            } else if (Channel->flags & CHANNEL_FLAG_EVENT_REPORT) {

                /* Enable event reporting */
                Channel->flags &= ~CHANNEL_FLAG_EVENT_REPORT;
                Channel->atCommand.type = AT_EVENT_REPORTING;
                Channel->atCommand.p.hf.report.mode = 3;
                Channel->atCommand.p.hf.report.ind = 1;
                Channel->flags |= CHANNEL_FLAG_TEST_HOLD;
            } else if (Channel->flags & CHANNEL_FLAG_TEST_HOLD) {

                /* Test support for Hold */
                Channel->flags &= ~CHANNEL_FLAG_TEST_HOLD;
                Channel->flags |= CHANNEL_FLAG_NEG_DONE;

#if (HF_SDK_FEATURES & HF_FEATURE_CALL_WAITING)
                if (Channel->gwFeatures & HF_GW_FEATURE_3_WAY) {
                    Channel->atCommand.type = AT_CALL_HOLD | AT_TEST;
                } else {
                    HfStateNegotiate(Channel, Event, Parms);
                    return;
                }
#else
                HfStateNegotiate(Channel, Event, Parms);
                return;
#endif
            } else if (Channel->flags & CHANNEL_FLAG_NEG_DONE) {

                /* Service channel is up, tell the app */
                Channel->flags &= ~CHANNEL_FLAG_NEG_DONE;
                Channel->state = HF_STATE_OPEN;
                HfAppCallback(Channel, HF_EVENT_SERVICE_CONNECTED, BT_STATUS_SUCCESS, 
                               Channel->cmgrHandler.bdc->link);
                
#if HF_SNIFF_TIMER >= 0
                if (Channel->flags & CHANNEL_FLAG_SNIFFING) {
                    /* Start the sniff timer */
                    sniffInfo.minInterval = HF_SNIFF_MIN_INTERVAL;
                    sniffInfo.maxInterval = HF_SNIFF_MAX_INTERVAL;
                    sniffInfo.attempt = HF_SNIFF_ATTEMPT;
                    sniffInfo.timeout = HF_SNIFF_TIMEOUT;
                    CMGR_SetSniffTimer(&Channel->cmgrHandler, &sniffInfo, 
                                       HF_SNIFF_TIMER);
                }
#endif

#if HF_USE_CALL_MANAGER == XA_ENABLED
                (void)CallManager(Channel, 
                                  EG_HFEVENT, 
                                  HF_EVENT_SERVICE_CONNECTED, 
                                  0);
#endif
                return;
            }
            if (AtSendCommand(Channel, &Channel->atCommand) != BT_STATUS_PENDING) {
                /* Fatal error, Failed to send command */
                (void)RF_CloseChannel(Channel->hfChannel);
            }
            break;
        case (AT_INDICATORS | AT_TEST):
            /* Create a mapping of the remote devices indicators */
            AT_MapHfIndicators(HfIndMsg, HF(indMap), MAX_NUM_IND, &Parms->p.hf);
            HF(numInd) = Parms->p.hf.indicators_test.num;
            break;
        case (AT_INDICATORS | AT_READ):
            /* Notify app of the rest of the indicators */
            HfIndicatorCallback(Channel, Parms);
            break;
        case (AT_CALL_HOLD | AT_TEST):
            Channel->gwHoldFeatures = Parms->p.hf.hold_test.flags;
            HfAppCallback(Channel, HF_EVENT_GW_HOLD_FEATURES, BT_STATUS_SUCCESS,
                           (void *)(U32)Channel->gwHoldFeatures);
            break;
        case AT_SUPPORTED_FEATURES:
            /* Save features */
            Channel->gwFeatures = Parms->p.hf.features.bitmap;

                /* Report features to the app */
            if (Channel->gwFeatures & HF_GW_FEATURE_IN_BAND_RING) {
                Channel->ibRing = TRUE;
            }

            if (Channel->gwFeatures & HF_GW_FEATURE_ECHO_NOISE) {
                Channel->nrec = TRUE;
            }

            HfAppCallback(Channel, HF_EVENT_GATEWAY_FEATURES, BT_STATUS_SUCCESS, 
                           (void *)(U32)Channel->gwFeatures);
            break;
        }
        break;

    case EVENT_COMMAND_TIMEOUT:
        /* Request timed out, abort the connection */
        (void)RF_CloseChannel(Channel->hfChannel);
        break;

    case EVENT_RF_PACKET_SENT:
        break;

    default:
        Assert(0);
        break;
    }
}

/*---------------------------------------------------------------------------
 *            HfStateOpen()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handles the open state for the HF state machine.
 *
 * Return:    void
 */
static void HfStateOpen(HfChannel *Channel, U8 Event, AtResults *Parms)
{
    HfCommand *tmpCommand;
    BtStatus   status;
    HfAtData   atData;

    switch (Event) {
    case EVENT_AT_DATA:
        /* Pass event to call manager first */
#if HF_USE_CALL_MANAGER == XA_ENABLED
        switch (Parms->type) {
        case AT_RING:
        case AT_NO_CARRIER:
        case AT_BUSY:
        case AT_NO_ANSWER:
        case AT_DELAYED:
        case AT_BLACKLISTED:
            if (CallManager(Channel, 
                            EG_AT_PROCESSOR, 
                            Parms->type, 
                            (U32)Parms)) {
                return;
            }
            break;
        case AT_CALL_WAIT_NOTIFY:
        case AT_CALL_ID:
        case AT_LIST_CURRENT_CALLS:
#if HF_USE_RESP_HOLD == XA_ENABLED
        case AT_RESPONSE_AND_HOLD:
#endif
            if (CallManager(Channel, 
                            EG_AT_HANDSFREE, 
                            Parms->type, 
                            (U32)Parms)) {
                return;
            }
            break;
        }
#endif

        /* Handle unhandled and other events */
        switch (Parms->type) {

#if HF_USE_CALL_MANAGER != XA_ENABLED

        case AT_RING:
            HfAppCallback(Channel, HF_EVENT_RING_IND,  BT_STATUS_SUCCESS, 0);
            break;
        case AT_CALL_WAIT_NOTIFY:
            HfAppCallback(Channel, HF_EVENT_WAIT_NOTIFY,  BT_STATUS_SUCCESS, 
                           (void *)&Parms->p.hf.wait);
            break;
        case AT_CALL_ID:
            HfAppCallback(Channel, HF_EVENT_CALLER_ID_NOTIFY,  BT_STATUS_SUCCESS, 
                           (void *)&Parms->p.hf.callId);
            break;
        case AT_LIST_CURRENT_CALLS:
            HfAppCallback(Channel, HF_EVENT_CURRENT_CALL_STATE, BT_STATUS_SUCCESS,
                          (void *)&Parms->p.hf.currentCalls);
            break;

#if HF_USE_RESP_HOLD == XA_ENABLED
        case AT_RESPONSE_AND_HOLD:
            HfAppCallback(Channel, HF_EVENT_RESPONSE_HOLD, BT_STATUS_SUCCESS,
                          (void *)Parms->p.hf.btrh.setting);
            break;
#endif

#endif

        case AT_INDICATOR_EVENT:
            HfEventCallback(Channel, Parms);
            break;
        case (AT_INDICATORS | AT_READ):
            HfIndicatorCallback(Channel, Parms);
            break;
        case AT_MICROPHONE_GAIN:
            Channel->micGain = Parms->p.hs.mic.gain;
            HfAppCallback(Channel, HF_EVENT_MIC_VOLUME,  BT_STATUS_SUCCESS, 
                           (void *)(U32)Parms->p.hs.mic.gain);
            break;
        case AT_SPEAKER_GAIN:
            Channel->speakerGain = Parms->p.hs.speaker.gain;
            HfAppCallback(Channel, HF_EVENT_SPEAKER_VOLUME,  BT_STATUS_SUCCESS, 
                           (void *)(U32)Parms->p.hs.speaker.gain);
            break;

        case AT_VOICE_RECOGNITION:
            Channel->voiceRec = Parms->p.hf.vrec.enabled;
            HfAppCallback(Channel, HF_EVENT_VOICE_REC_STATE, BT_STATUS_SUCCESS, 
                           (void *)(U32)Parms->p.hf.vrec.enabled);
            break;

        case AT_VOICE_TAG:
            HfAppCallback(Channel, HF_EVENT_VOICE_TAG_NUMBER,  BT_STATUS_SUCCESS, 
                           (void *)(U32)Parms->p.hf.voiceTag.number);
            break;
        case AT_IN_BAND_RING_TONE:
            Channel->ibRing = Parms->p.hf.inBandRing.enabled;
            HfAppCallback(Channel, HF_EVENT_IN_BAND_RING,  BT_STATUS_SUCCESS, 
                           (void *)(U32)Parms->p.hf.inBandRing.enabled);
            break;

        case AT_SUBSCRIBER_NUM:
            HfAppCallback(Channel, HF_EVENT_SUBSCRIBER_NUMBER, BT_STATUS_SUCCESS,
                          (void *)&Parms->p.hf.subscribeNum);
            break;

        case (AT_NETWORK_OPERATOR | AT_READ):
            HfAppCallback(Channel, HF_EVENT_NETWORK_OPERATOR, BT_STATUS_SUCCESS,
                          (void *)Parms->p.hf.networkOper_read.oper);
            break;

        case AT_NO_CARRIER:
            HfAppCallback(Channel, HF_EVENT_NO_CARRIER, BT_STATUS_SUCCESS,
                          (void *)Parms->p.hf.networkOper_read.oper);
            break;
        case AT_BUSY:
            HfAppCallback(Channel, HF_EVENT_BUSY, BT_STATUS_SUCCESS,
                          (void *)Parms->p.hf.networkOper_read.oper);
            break;
        case AT_NO_ANSWER:
            HfAppCallback(Channel, HF_EVENT_NO_ANSWER, BT_STATUS_SUCCESS,
                          (void *)Parms->p.hf.networkOper_read.oper);
            break;
        case AT_DELAYED:
            HfAppCallback(Channel, HF_EVENT_DELAYED, BT_STATUS_SUCCESS,
                          (void *)Parms->p.hf.networkOper_read.oper);
            break;
        case AT_BLACKLISTED:
            HfAppCallback(Channel, HF_EVENT_BLACKLISTED, BT_STATUS_SUCCESS,
                          (void *)Parms->p.hf.networkOper_read.oper);
            break;

#if HF_USE_PHONEBOOK_COMMANDS == XA_ENABLED
        case (AT_SELECT_PHONEBOOK_STORAGE | AT_TEST):
        case (AT_SELECT_PHONEBOOK_STORAGE | AT_READ):
        case (AT_READ_PHONEBOOK_ENTRY | AT_TEST):
        case AT_READ_PHONEBOOK_ENTRY:
        case AT_FIND_PHONEBOOK_ENTRY:
            HfPhonebookCallback(Channel, Parms);
            break;
#endif

        case AT_OK:
            /* Handle unhandled command complete */
#if HF_USE_CALL_MANAGER == XA_ENABLED
            if (Channel->lastAtCommand != &Channel->pollCommand) {
#endif
                tmpCommand = HfGetCurrentCommand(Channel);
                if (tmpCommand) {
                    switch (tmpCommand->type) {
#if HF_USE_CALL_MANAGER == XA_ENABLED
                    case HF_COMMAND_LIST_CURRENT_CALLS:
                        Channel->pollFlags |= CALLMGR_POLL_LIST_CALLS;
                        (void)CallManager(Channel, 
                                          EG_STATE_MACHINE, 
                                          EVENT_POLL_COMPLETE, 
                                          (U32)AT_OK);
                        break;
#endif
                    case HF_COMMAND_ENABLE_CID_NOTIFY:
                        Channel->callId = (BOOL)tmpCommand->parms[0];
                        break;
                    case HF_COMMAND_DISABLE_NREC:
                        Channel->nrec = FALSE;
                        break;
                    case HF_COMMAND_VOICE_RECOGNITION:
                        Channel->voiceRec = (BOOL)tmpCommand->parms[0];
                        break;
                    case HF_COMMAND_ENABLE_WAIT_NOTIFY:
                        Channel->callWaiting = (BOOL)tmpCommand->parms[0];
                        break;
                    case HF_COMMAND_REPORT_SPEAKER_VOLUME:
                        Channel->speakerGain = (U8)tmpCommand->parms[0];
                        break;
                    case HF_COMMAND_REPORT_MIC_VOLUME:
                        Channel->micGain = (U8)tmpCommand->parms[0];
                        break;
                    }
                    tmpCommand->cmeError = ATCME_UNKNOWN;
                    tmpCommand->status = BT_STATUS_SUCCESS;
                    status = HfExecuteNextCommand(Channel);
                    if (status != BT_STATUS_PENDING && status != BT_STATUS_SUCCESS) {
                        /* Execution of the next command, critical error,
                         * flush the queue.
                         */
                        HfFlushCommands(Channel, BT_STATUS_FAILED);
                    }
                }
#if HF_USE_CALL_MANAGER == XA_ENABLED
            } else {
                (void)CallManager(Channel, 
                                  EG_STATE_MACHINE, 
                                  EVENT_POLL_COMPLETE, 
                                  (U32)Parms->type);
            }
#endif
            break;

        case AT_ERROR:
#if HF_USE_CALL_MANAGER == XA_ENABLED
            if (Channel->lastAtCommand != &Channel->pollCommand) {
#endif
                tmpCommand = HfGetCurrentCommand(Channel);
                if (tmpCommand) {
                    tmpCommand->cmeError = ATCME_UNKNOWN;
                    tmpCommand->status = BT_STATUS_FAILED;
                    status = HfExecuteNextCommand(Channel);
                    if (status != BT_STATUS_PENDING && status != BT_STATUS_SUCCESS) {
                        /* Execution of the next command, critical error,
                         * flush the queue.
                         */
                        HfFlushCommands(Channel, BT_STATUS_FAILED);
                    }
                }
#if HF_USE_CALL_MANAGER == XA_ENABLED
            } else {
                (void)CallManager(Channel, 
                                  EG_STATE_MACHINE, 
                                  EVENT_POLL_COMPLETE, 
                                  (U32)Parms->type);
            }
#endif
            break;
        case AT_EXTENDED_ERROR:
#if HF_USE_CALL_MANAGER == XA_ENABLED
            if ((Channel->lastAtCommand != &Channel->pollCommand) &&
                (Channel->lastAtCommand->type != HF_COMMAND_LIST_CURRENT_CALLS)) {
#endif
                tmpCommand = HfGetCurrentCommand(Channel);
                if (tmpCommand) {
                    tmpCommand->cmeError = Parms->p.error.type;
                    tmpCommand->status = BT_STATUS_FAILED;
                    status = HfExecuteNextCommand(Channel);
                    if (status != BT_STATUS_PENDING && status != BT_STATUS_SUCCESS) {
                        /* Execution of the next command, critical error,
                         * flush the queue.
                         */
                        HfFlushCommands(Channel, BT_STATUS_FAILED);
                    }
                }
#if HF_USE_CALL_MANAGER == XA_ENABLED
            } else {
                (void)CallManager(Channel, 
                                  EG_STATE_MACHINE, 
                                  EVENT_POLL_COMPLETE, 
                                  (U32)Parms->type);
            }
#endif
            break;

        default:
            /* Handle unhandled result code */
            atData.data = Channel->atRxBuffer;
            atData.dataLen = Channel->atRxLen;
            HfAppCallback(Channel, HF_EVENT_AT_RESULT_DATA, 
                           BT_STATUS_SUCCESS, &atData);
            break;
        }
        break;

    case EVENT_COMMAND_TIMEOUT:
        /* An AT command timed out */
#if HF_USE_CALL_MANAGER == XA_ENABLED
        if ((Channel->lastAtCommand != &Channel->pollCommand) &&
            (Channel->lastAtCommand->type != HF_COMMAND_LIST_CURRENT_CALLS)) {
#endif
            tmpCommand = HfGetCurrentCommand(Channel);
            if (tmpCommand) {
                tmpCommand->cmeError = ATCME_UNKNOWN;
                tmpCommand->status = BT_STATUS_TIMEOUT;
                status = HfExecuteNextCommand(Channel);
                if (status != BT_STATUS_PENDING && status != BT_STATUS_SUCCESS) {
                    /* Execution of the next command, critical error,
                     * flush the queue.
                     */
                    HfFlushCommands(Channel, BT_STATUS_FAILED);
                }
            }
#if HF_USE_CALL_MANAGER == XA_ENABLED
        } else {
            (void)CallManager(Channel, 
                              EG_STATE_MACHINE, 
                              EVENT_POLL_COMPLETE, 
                              (U32)AT_TIMEOUT);
        }
#endif
        break;

    case EVENT_RF_PACKET_SENT:
#if HF_USE_CALL_MANAGER == XA_ENABLED
        /* An RFCOMM packet was sent */
        if (HfGetCurrentCommand(Channel)) {
            (void)CallManager(Channel, EG_STATE_MACHINE, Event, 0);
        }
#endif
        break;

    default:
        Assert(0);
        break;
    }
}

/*---------------------------------------------------------------------------
 *            HfInitStateMachine()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initializes the HF state machine.
 *
 * Return:    void
 */
void HfInitStateMachine(void)
{
    HF(hfState)[0] = HfStateClosed;
    HF(hfState)[1] = HfStateConnPending;
    HF(hfState)[2] = HfStateConnIncoming;
    HF(hfState)[3] = HfStateNegotiate;
    HF(hfState)[4] = HfStateOpen;
}

/*---------------------------------------------------------------------------
 *            HfRfCallback()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  RFCOMM callback for the HF state machine.
 *
 * Return:    void
 */
void HfRfCallback(RfChannel *Channel, RfCallbackParms *Parms)
{
    HfChannel  *hfChannel;
    AtResults   atParms;
    U8          event = 0;
    U16         offset = 0;
    RfChannel  *rfChannel;

    if ((Parms->event == RFEVENT_OPEN_IND) ||
        (Parms->event == RFEVENT_OPEN) ||
        (Parms->event == RFEVENT_CLOSED)) {
        hfChannel = HfFindChannel(Parms->ptrs.remDev);
    } else {
        hfChannel = HfFindChannel(RF_RemoteDevice(Channel));
    }
    if (hfChannel) {
        hfChannel->hfChannel = Channel;
    }

    switch (Parms->event) {
    case RFEVENT_OPEN_IND:

        /* Find the device */
        Report(("HF: RFCOMM incoming hands-free connection\n"));

        if (!hfChannel) {
            /* Get a new channel */
            hfChannel = HfGetClosedChannel();
            if (hfChannel) {
                /* Found a new device */
                hfChannel->hfChannel = Channel;

                if (CMGR_CreateDataLink(&hfChannel->cmgrHandler, 
                                        &Parms->ptrs.remDev->bdAddr) !=
                    BT_STATUS_SUCCESS) {
                    /* Could not create a link to the ACL */
                    RF_RejectChannel(Channel);
                    return;
                } else {
                    hfChannel->linkFlags |= HF_LINK_ACL;
                }

                event = EVENT_OPEN_IND;
            } else {
                /* Reject the channel */
                RF_RejectChannel(Channel);
                return;
            }
        } else {
            /* A connection exists to this device */
            RF_RejectChannel(Channel);
            return;
        }
        break;

    case RFEVENT_OPEN:

        /* Register a SCO handler */
        Assert(hfChannel);
        AssertEval(CMGR_RegisterScoHandler(&hfChannel->cmgrHandler) ==
                   BT_STATUS_SUCCESS);

        if (HF(hfChannel) == 0) {
            rfChannel = HfAllocRfChannel();
            if (rfChannel) {
                if (RF_RegisterServerChannel(rfChannel, &HF(hfService), 1) ==
                    BT_STATUS_SUCCESS) {
                    HF(hfChannel) = rfChannel;
                    rfChannel->userContext = (void *)(U32)1;
                } else {
                    HfFreeRfChannel(rfChannel);
                    HF(hfChannel) = 0;
                }
            }
        }

        Report(("HF: RFCOMM hands-free connection established\n"));
        hfChannel->linkFlags |= HF_LINK_HANDSFREE;

#if HF_USE_CALL_MANAGER == XA_ENABLED
        hfChannel->callState[0] = HF_CALL_STATUS_NONE;
        hfChannel->callState[1] = HF_CALL_STATUS_NONE;
#endif
        event = EVENT_OPEN;
        break;

    case RFEVENT_DATA_IND:

        Assert(hfChannel);
        Report(("HF: RFCOMM hands-free data received\n"));
        RF_AdvanceCredit(Channel, 1);
        CMGR_IndicateLinkActivity(&hfChannel->cmgrHandler);

        event = EVENT_AT_DATA;
        while (offset < Parms->dataLen) {
            if (HfParseRfcommData(hfChannel, Parms, &offset, &atParms) == BT_STATUS_SUCCESS) {
                HF(hfState)[hfChannel->state](hfChannel, event, &atParms);
            }
        }
        return;

    case RFEVENT_PACKET_HANDLED:

        if (hfChannel) {
            CMGR_IndicateLinkActivity(&hfChannel->cmgrHandler);
            if (hfChannel->bytesToSend > 0) {
                /* Send more data */
                Parms->ptrs.packet->data += Parms->ptrs.packet->dataLen;
                Parms->ptrs.packet->dataLen = hfChannel->bytesToSend;
                if (HfAtSendRfPacket(hfChannel, 
                                   Parms->ptrs.packet) != BT_STATUS_PENDING) {
                    Report(("HF: Unable to send RFCOMM packet\n"));
                    (void)RF_CloseChannel(hfChannel->hfChannel);
                }
                return;
            }

            Parms->ptrs.packet->flags &= ~BTP_FLAG_INUSE;
            event = EVENT_RF_PACKET_SENT;
        } else {
            return;
        }
        break;

    case RFEVENT_CLOSED:

        Report(("HF: RFCOMM hands-free connection closed\n"));

        if (Channel->userContext) {
            if (RF_DeregisterServerChannel(Channel, &HF(hfService)) == BT_STATUS_SUCCESS) {
                HfFreeRfChannel(Channel);
                Channel->userContext = 0;
            }
        } else {
            HfFreeRfChannel(Channel);
        }

        if (HF(hfChannel) == 0) {
            rfChannel = HfAllocRfChannel();
            if (rfChannel) {
                if (RF_RegisterServerChannel(rfChannel, &HF(hfService), 1) ==
                    BT_STATUS_SUCCESS) {
                    HF(hfChannel) = rfChannel;
                    rfChannel->userContext = (void *)(U32)1;
                } else {
                    HfFreeRfChannel(rfChannel);
                    HF(hfChannel) = 0;
                }
            }
        }

        if (hfChannel) {
            hfChannel->linkFlags &= ~HF_LINK_HANDSFREE;
            CMGR_DeregisterScoHandler(&hfChannel->cmgrHandler);

            hfChannel->cmgrHandler.errCode = BEC_USER_TERMINATED;
            if (CMGR_GetLinkState(&hfChannel->cmgrHandler) == BDS_CONNECTED) {
                HfCloseChannel(hfChannel);
            }
        }
        return;

    default:
        /* Ignore other events */
        return;
    }

    HF(hfState)[hfChannel->state](hfChannel, event, &atParms);
}

