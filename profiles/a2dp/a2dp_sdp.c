/****************************************************************************
 *
 * File:
 *     $Workfile:a2dp_sdp.c$ for iAnywhere AV SDK, version 1.2.3
 *     $Revision:13$
 *
 * Description: This file contains SDP code for the A2DP profile
 *             
 * Created:     June 3, 2004
 *
 * Copyright 2004 - 2005 Extended Systems, Inc.

 * Portions copyright 2005-2006 iAnywhere Solutions, Inc.

 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any means, 
 * or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, Inc.  
 * This work contains confidential and proprietary information of Extended 
 * Systems, Inc. which is protected by copyright, trade secret, trademark and 
 * other intellectual property rights.
 *
 ****************************************************************************/

#include "sys/a2dpi.h"

#if SDP_PARSING_FUNCS == XA_DISABLED
#error The A2DP SDK requires the SDP parsing functions.
#endif /* SDP_PARSING_FUNCS == XA_DISABLED */

static void A2dpSdpEventHandler(const BtEvent *Event);

/****************************************************************************
 *
 * ROMable data
 *
 ****************************************************************************/

/****************************************************************************
 * A2DP SDP Entries - Audio Source
 ****************************************************************************/

#if A2DP_SOURCE == XA_ENABLED

/*---------------------------------------------------------------------------
 *
 * ServiceClassIDList
 */
static const U8 A2dpSrcClassId[] = {
    SDP_ATTRIB_HEADER_8BIT(3),        /* Data Element Sequence, 6 bytes */
    SDP_UUID_16BIT(SC_AUDIO_SOURCE)   /* Audio Sink UUID in Big Endian */
};

#endif /* A2DP_SOURCE == XA_ENABLED */


/*---------------------------------------------------------------------------
 * SDP Protocol Descriptor List object registered by A2DP.
 * 
 * Value of the protocol descriptor list for the A2DP Profile.
 * This structure is a ROM'able representation of the RAM structure.
 * This structure is copied into a RAM structure used to register the 
 * service. A RAM structure must be used to be able to dynamically set the 
 * RFCOMM channel in the RFCOMM protocol descriptor.
 */
static const U8 A2dpProtoDescList[] = {
    SDP_ATTRIB_HEADER_8BIT(16),  /* Data element sequence, 13 bytes */

    /* Each element of the list is a Protocol descriptor which is a
     * data element sequence. The first element is L2CAP which only
     * has a UUID element.
     */
    SDP_ATTRIB_HEADER_8BIT(6),   /* Data element sequence for L2CAP, 3 bytes */     
    SDP_UUID_16BIT(PROT_L2CAP),  /* L2CAP UUID */
    SDP_UINT_16BIT(PROT_AVDTP),  /* AVDTP PSM */

    /* Next protocol descriptor in the list is AVDTP. It contains one
     * element which is the version.
     */
    SDP_ATTRIB_HEADER_8BIT(6),   /* Data element sequence for AVDTP, 3 bytes */
    SDP_UUID_16BIT(PROT_AVDTP),  /* AVDTP UUID */
    SDP_UINT_16BIT(0x0100)       /* Uint8 AVDTP Version (1.0) */
};

/*---------------------------------------------------------------------------
 * A2DP Public Browse Group.
 */
static const U8 A2dpBrowseGroup[] = {
    SDP_ATTRIB_HEADER_8BIT(3),               /* 3 bytes */
    SDP_UUID_16BIT(SC_PUBLIC_BROWSE_GROUP)   /* Public Browse Group */
};

/*---------------------------------------------------------------------------
 * A2DP Profile Descriptor List
 */
static const U8 A2dpProfileDescList[] = {
    SDP_ATTRIB_HEADER_8BIT(8),   /* Data element sequence, 8 bytes */
    SDP_ATTRIB_HEADER_8BIT(6),   /* Data element sequence for ProfileDescriptor, 6 bytes */
    SDP_UUID_16BIT(SC_AUDIO_DISTRIBUTION),
    SDP_UINT_16BIT(0x0100)
};

/*---------------------------------------------------------------------------
 * * OPTIONAL *  ServiceName
 */
static const U8 A2dpServiceName[] = {
    SDP_TEXT_8BIT(15),          /* Null terminated text string */
    'A', 'd', 'v', 'a', 'n', 'c', 'e', 'd', ' ', 'A', 'u', 
    'd', 'i', 'o', '\0'
};

/*---------------------------------------------------------------------------
 * * OPTIONAL *  ProviderName
 */
static const U8 A2dpProviderName[] = {
    SDP_TEXT_8BIT(17),          /* Null terminated text string */
    'E', 'x', 't', 'e', 'n', 'd', 'e', 'd', ' ', 'S', 'y', 
    's', 't', 'e', 'm', 's', '\0'
};

#if A2DP_SOURCE == XA_ENABLED

/*---------------------------------------------------------------------------
 *  * OPTIONAL *  Supported Features
 */
static const U8 A2dpSrcSupportedFeatures[] = {
    SDP_UINT_16BIT(A2DP_SRC_FEATURES)   
};

/* A2DP Source attributes.
 *
 * This is a ROM template for the RAM structure used to register the
 * A2DP Source SDP record.
 */
static const SdpAttribute A2dpSrcSdpAttributes[] = {
    /* Audio Source class ID List attribute */
    SDP_ATTRIBUTE(AID_SERVICE_CLASS_ID_LIST, A2dpSrcClassId), 
    /* A2DP protocol descriptor list attribute */
    SDP_ATTRIBUTE(AID_PROTOCOL_DESC_LIST, A2dpProtoDescList),
    /* Public Browse Group Service */
    SDP_ATTRIBUTE(AID_BROWSE_GROUP_LIST, A2dpBrowseGroup), 
    /* A2DP profile descriptor list attribute */
    SDP_ATTRIBUTE(AID_BT_PROFILE_DESC_LIST, A2dpProfileDescList),
    /* A2DP Service Name */
    SDP_ATTRIBUTE((AID_SERVICE_NAME + 0x0100), A2dpServiceName),
    /* A2DP Provider Name */
    SDP_ATTRIBUTE((AID_PROVIDER_NAME + 0x0100), A2dpProviderName),
    /* A2DP Source supported features */
    SDP_ATTRIBUTE(AID_SUPPORTED_FEATURES, A2dpSrcSupportedFeatures)
};

/*-------------------------------------------------------------------------
 *
 * SDP query info
 */
static const U8 A2dpSrcServiceSearchAttribReq[] = {
    /* First parameter is the search pattern in data element format. It
     * is a list of 3 UUIDs. 
     */
    SDP_ATTRIB_HEADER_8BIT(9),           /* Data Element Sequence, 9 bytes */ 

    SDP_UUID_16BIT(SC_AUDIO_SINK),       /* UUID for Audio Sink            */
    SDP_UUID_16BIT(PROT_L2CAP),          /* L2CAP UUID                     */
    SDP_UUID_16BIT(PROT_AVDTP),          /* AVDTP UUID                     */

    /* The second parameter is the maximum number of bytes that can be
     * be received for the attribute list
     */
    0x00, 0x64,                 /* Max number of bytes for attribute is 100 */

    /* The third parameter is the actual attribute ID list which is a data
     * element sequence. We only want one attribute which is the protocol
     * descriptor
     */
    SDP_ATTRIB_HEADER_8BIT(9),              /* Data Element Sequence, 12 bytes */
    SDP_UINT_16BIT(AID_PROTOCOL_DESC_LIST), /* Protocol descriptor list ID */
    SDP_UINT_16BIT(AID_BT_PROFILE_DESC_LIST),
    SDP_UINT_16BIT(AID_SUPPORTED_FEATURES)
};

#endif /* A2DP_SOURCE == XA_ENABLED */

/****************************************************************************
 * A2DP SDP Entries - Audio Sink
 ****************************************************************************/

#if A2DP_SINK == XA_ENABLED

/*---------------------------------------------------------------------------
 *
 * ServiceClassIDList
 */
static const U8 A2dpSnkClassId[] = {
    SDP_ATTRIB_HEADER_8BIT(3),        /* Data Element Sequence, 6 bytes */
    SDP_UUID_16BIT(SC_AUDIO_SINK)     /* Audio Sink UUID in Big Endian */
};

/*---------------------------------------------------------------------------
 *  * OPTIONAL *  Supported Features
 */
static const U8 A2dpSnkSupportedFeatures[] = {
    SDP_UINT_16BIT(A2DP_SNK_FEATURES)   
};

/* A2DP Sink attributes.
 *
 * This is a ROM template for the RAM structure used to register the
 * A2DP Sink SDP record.
 */
static const SdpAttribute A2dpSnkSdpAttributes[] = {
    /* Audio Sink class ID List attribute */
    SDP_ATTRIBUTE(AID_SERVICE_CLASS_ID_LIST, A2dpSnkClassId), 
    /* A2DP protocol descriptor list attribute */
    SDP_ATTRIBUTE(AID_PROTOCOL_DESC_LIST, A2dpProtoDescList),
    /* Public Browse Group Service */
    SDP_ATTRIBUTE(AID_BROWSE_GROUP_LIST, A2dpBrowseGroup), 
    /* A2DP profile descriptor list attribute */
    SDP_ATTRIBUTE(AID_BT_PROFILE_DESC_LIST, A2dpProfileDescList),
    /* A2DP Service Name */
    SDP_ATTRIBUTE((AID_SERVICE_NAME + 0x0100), A2dpServiceName),
    /* A2DP Provider Name */
    SDP_ATTRIBUTE((AID_PROVIDER_NAME + 0x0100), A2dpProviderName),
    /* A2DP Sink supported features */
    SDP_ATTRIBUTE(AID_SUPPORTED_FEATURES, A2dpSnkSupportedFeatures)
};

/*-------------------------------------------------------------------------
 *
 * SDP query info
 */
static const U8 A2dpSnkServiceSearchAttribReq[] = {
    /* First parameter is the search pattern in data element format. It
     * is a list of 3 UUIDs. 
     */
    SDP_ATTRIB_HEADER_8BIT(9),           /* Data Element Sequence, 9 bytes */ 

    SDP_UUID_16BIT(SC_AUDIO_SOURCE),     /* UUID for Audio Source          */
    SDP_UUID_16BIT(PROT_L2CAP),          /* L2CAP UUID                     */
    SDP_UUID_16BIT(PROT_AVDTP),          /* AVDTP UUID                     */

    /* The second parameter is the maximum number of bytes that can be
     * be received for the attribute list
     */
    0x00, 0x64,                 /* Max number of bytes for attribute is 100 */

    /* The third parameter is the actual attribute ID list which is a data
     * element sequence. We only want one attribute which is the protocol
     * descriptor
     */
    SDP_ATTRIB_HEADER_8BIT(9),              /* Data Element Sequence, 6 bytes */
    SDP_UINT_16BIT(AID_PROTOCOL_DESC_LIST), /* Protocol descriptor list ID */
    SDP_UINT_16BIT(AID_BT_PROFILE_DESC_LIST),
    SDP_UINT_16BIT(AID_SUPPORTED_FEATURES)
};

#endif /* A2DP_SINK == XA_ENABLED */

/****************************************************************************
 *
 * Functions
 *
 ****************************************************************************/

/*---------------------------------------------------------------------------
 *            A2dpRegisterSdpService()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Registers the Source or Sink SDP services.
 *
 * Return:    See SDP_AddRecord().
 */
BtStatus A2dpRegisterSdpService(A2dpEndpointType streamType)
{
    BtStatus status = BT_STATUS_FAILED;

#if A2DP_SOURCE == XA_ENABLED
    if (streamType == A2DP_STREAM_TYPE_SOURCE) {
        /* Register Hands-free SDP Attributes */
        Assert(sizeof(A2DP(a2dpSrcSdpAttribute)) == sizeof(A2dpSrcSdpAttributes));
        OS_MemCopy((U8 *)A2DP(a2dpSrcSdpAttribute), (U8 *)A2dpSrcSdpAttributes,
                   sizeof(A2dpSrcSdpAttributes));

        A2DP(a2dpSrcSdpRecord).attribs = A2DP(a2dpSrcSdpAttribute);
        A2DP(a2dpSrcSdpRecord).num = 7;
        A2DP(a2dpSrcSdpRecord).classOfDevice = COD_CAPTURING | COD_MAJOR_AUDIO | 
                                               A2DP_MINOR_DEVICE_CLASS;

        status = SDP_AddRecord(&A2DP(a2dpSrcSdpRecord));
        if (status != BT_STATUS_SUCCESS) {
            goto exit;
        }
    }
#endif

#if A2DP_SINK == XA_ENABLED
    if (streamType == A2DP_STREAM_TYPE_SINK) {
        /* Register Hands-free SDP Attributes */
        Assert(sizeof(A2DP(a2dpSnkSdpAttribute)) == sizeof(A2dpSnkSdpAttributes));
        OS_MemCopy((U8 *)A2DP(a2dpSnkSdpAttribute), (U8 *)A2dpSnkSdpAttributes,
                   sizeof(A2dpSnkSdpAttributes));

        A2DP(a2dpSnkSdpRecord).attribs = A2DP(a2dpSnkSdpAttribute);
        A2DP(a2dpSnkSdpRecord).num = 7;
        A2DP(a2dpSnkSdpRecord).classOfDevice = COD_RENDERING | COD_MAJOR_AUDIO | 
                                               A2DP_MINOR_DEVICE_CLASS;

        status = SDP_AddRecord(&A2DP(a2dpSnkSdpRecord));
    }
#endif

#if A2DP_SOURCE == XA_ENABLED
exit:
#endif

    return status;
}

/*---------------------------------------------------------------------------
 *            A2dpDeregisterSdpService()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Deregisters the Sink SDP service.
 *
 * Return:    See SDP_RemoveRecord().
 */
BtStatus A2dpDeregisterSdpService(A2dpEndpointType streamType)
{
    BtStatus status = BT_STATUS_FAILED;

#if A2DP_SOURCE == XA_ENABLED
    if (streamType == A2DP_STREAM_TYPE_SOURCE) {
        /* Remove the SDP entry */
        status = SDP_RemoveRecord(&A2DP(a2dpSrcSdpRecord));
        if (status != BT_STATUS_SUCCESS) {
            goto exit;
        }
    }
#endif

#if A2DP_SINK == XA_ENABLED
    if (streamType == A2DP_STREAM_TYPE_SINK) {
        /* Remove the SDP entry */
        status = SDP_RemoveRecord(&A2DP(a2dpSnkSdpRecord));
    }
#endif

#if A2DP_SOURCE == XA_ENABLED
exit:
#endif

    return status;
}

/*---------------------------------------------------------------------------
 *            A2dpStartServiceQuery()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initiate the SDP service query for a Source or Sink on the 
 *            specified device.
 *
 * Return:    See SDP_Query().
 */
BtStatus A2dpStartServiceQuery(A2dpDevice *Device, 
                               A2dpEndpointType streamType, 
                               SdpQueryMode mode)
{
    BtStatus status = BT_STATUS_FAILED;

#if A2DP_SOURCE == XA_ENABLED
    if (streamType == A2DP_STREAM_TYPE_SOURCE) {
        /* Search for the Sink on the remote device */
        Device->sdpQueryToken.parms = A2dpSrcServiceSearchAttribReq;
        Device->sdpQueryToken.plen = sizeof(A2dpSrcServiceSearchAttribReq);

        Device->sdpQueryToken.type = BSQT_SERVICE_SEARCH_ATTRIB_REQ;
        Device->sdpQueryToken.callback = A2dpSdpEventHandler;
        Device->sdpQueryToken.rm = Device->cmgrHandler.bdc->link;
        status = SDP_Query(&Device->sdpQueryToken, mode);
        goto exit;
    }
#endif

#if A2DP_SINK == XA_ENABLED
    if (streamType == A2DP_STREAM_TYPE_SINK) {
        /* Search for the Sink on the remote device */
        Device->sdpQueryToken.parms = A2dpSnkServiceSearchAttribReq;
        Device->sdpQueryToken.plen = sizeof(A2dpSnkServiceSearchAttribReq);

        Device->sdpQueryToken.type = BSQT_SERVICE_SEARCH_ATTRIB_REQ;
        Device->sdpQueryToken.callback = A2dpSdpEventHandler;
        Device->sdpQueryToken.rm = Device->cmgrHandler.bdc->link;
        status = SDP_Query(&Device->sdpQueryToken, mode);
    }
#endif

#if A2DP_SOURCE == XA_ENABLED
exit:
#endif

    return status;
} 

/*---------------------------------------------------------------------------
 *            A2dpVerifySdpQueryRsp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Parses the SDP response for the Profile and Protocol
 *            versions and the Supported Features.  In the case of a 
 *            continuation state, additional SDP queries will be issued until 
 *            all of the attributes have been found or until the entire SDP 
 *            response has been parsed.
 *            
 *
 * Return:    BT_STATUS_SUCCESS if SDP parsing is successful.
 *            BT_STATUS_FAILED if SDP parsing encountered an error.
 *            BT_STATUS_SDP_CONT_STATE if SDP parsing encountered a 
 *            continuation state that requires an additional SDP query.
 */
BtStatus A2dpVerifySdpQueryRsp(A2dpDevice *Device, SdpQueryToken *token)
{
    BtStatus status = BT_STATUS_FAILED;


    if (!(Device->queryFlags & A2DP_SDP_QUERY_FLAG_PROTOCOL)) {

        /* Get the AVDTP version */
        token->attribId = AID_PROTOCOL_DESC_LIST;
        token->uuid = PROT_AVDTP;

        if ((status = SDP_ParseAttributes(token)) == BT_STATUS_SUCCESS) {
            /* We had better have a U16 version */
            if ((token->totalValueLen == 2) && 
                ((token->availValueLen == 2) || (token->availValueLen == 1))) {

                Report(("A2DP: SDP_ParseAttributes succeeded! AVDTP version = %x,"
                        "Updated Buff Len = %d\n", token->valueBuff[0], 
                        token->remainLen));

                /* Assign the Profile UUID and Profile Version */
                if (token->availValueLen == 1) {
                    Device->avdtpVersion += token->valueBuff[0];
                } else {
                    Device->avdtpVersion = SDP_GetU16(token->valueBuff);
                }
            }

            token->mode = BSPM_RESUME;
            Device->queryFlags |= A2DP_SDP_QUERY_FLAG_PROTOCOL;
        }

        if (status == BT_STATUS_SDP_CONT_STATE) {
            if (token->availValueLen == 1) {
                /* It's possible that only a portion of the data might occur.
                 * If so, we'll copy what we have.  The data arrives in big 
                 * endian format.
                 */
                Device->avdtpVersion = ((U16)token->valueBuff[0]) << 8;
            }

            goto done;
        }
    }

    if (!(Device->queryFlags & A2DP_SDP_QUERY_FLAG_PROFILE)) {

        /* Get the A2DP Profile Version */
        token->attribId = AID_BT_PROFILE_DESC_LIST;
        token->uuid = SC_AUDIO_DISTRIBUTION;

        if ((status = SDP_ParseAttributes(token)) == BT_STATUS_SUCCESS) {
            /* We had better have a U16 version */
            if ((token->totalValueLen == 2) && 
                ((token->availValueLen == 2) || (token->availValueLen == 1))) {

                Report(("A2DP: SDP_ParseAttributes succeeded! A2DP version = %x,"
                        "Updated Buff Len = %d\n", token->valueBuff[0], 
                        token->remainLen));

                /* Assign the Profile UUID and Profile Version */
                if (token->availValueLen == 1) {
                    Device->a2dpVersion += token->valueBuff[0];
                } else {
                    Device->a2dpVersion = SDP_GetU16(token->valueBuff);
                }
            }

            token->mode = BSPM_RESUME;
            Device->queryFlags |= A2DP_SDP_QUERY_FLAG_PROFILE;
        }

        if (status == BT_STATUS_SDP_CONT_STATE) {
            if (token->availValueLen == 1) {
                /* It's possible that only a portion of the data might occur.
                 * If so, we'll copy what we have.  The data arrives in big 
                 * endian format.
                 */
                Device->a2dpVersion = ((U16)token->valueBuff[0]) << 8;
            }

            goto done;
        }
    }

    if (!(Device->queryFlags & A2DP_SDP_QUERY_FLAG_FEATURES)) {

        /* Get the Supported Features */
        token->attribId = AID_SUPPORTED_FEATURES;
        token->uuid = 0;
        if ((status = SDP_ParseAttributes(token)) == BT_STATUS_SUCCESS) {
            if ((token->totalValueLen == 2) && 
                ((token->availValueLen == 2) || (token->availValueLen == 1))) {

                /* Assign the Supported Features */
                if (token->availValueLen == 1) {
                    Device->features += token->valueBuff[0];
                } else {
                    Device->features = SDP_GetU16(token->valueBuff);
                }

                Device->queryFlags |= A2DP_SDP_QUERY_FLAG_FEATURES;
            }
        }

        if (status == BT_STATUS_SDP_CONT_STATE) {
            if (token->availValueLen == 1) {
                /* It's possible that only a portion of the data might occur.
                 * If so, we'll copy what we have.  The data arrives in big 
                 * endian format.
                 */
                Device->features = ((U16)token->valueBuff[0]) << 8;
            }
        }
    }

done:
    if (status == BT_STATUS_SDP_CONT_STATE) {
        Report(("A2DP: SDP_ParseAttributes - Continuation State. Query Again!\n"));
        token->mode = BSPM_CONT_STATE;
        return status;
    }

    /* Reset the attribute to parse */
    token->attribId = AID_PROTOCOL_DESC_LIST;
    /* Reset the SDP parsing mode */
    token->mode = BSPM_BEGINNING;

    /* Return the status of the service/protocol/profile parsing, since this
     * is the only critical information to obtain a connection.
     */
    if (Device->queryFlags & (A2DP_SDP_QUERY_FLAG_SERVICE | 
                            A2DP_SDP_QUERY_FLAG_PROTOCOL | 
                            A2DP_SDP_QUERY_FLAG_PROFILE))
        return BT_STATUS_SUCCESS;
    else return BT_STATUS_FAILED;
}

/*---------------------------------------------------------------------------
 *            A2dpSdpEventHandler()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handles all SDP events
 */
static void A2dpSdpEventHandler(const BtEvent *Event)
{
    BtStatus        status;
    A2dpDevice     *device = ContainingRecord(Event->p.token, 
                                              A2dpDevice, sdpQueryToken);
    A2dpEndpointType  streamType;
    BtRemoteDevice *remDev;

    switch (Event->eType) {
    case SDEVENT_QUERY_RSP:
        status = A2dpVerifySdpQueryRsp(device, Event->p.token);
        if (status == BT_STATUS_SUCCESS) {
            /* SDP result parsing completed */
            remDev = device->cmgrHandler.bdc->link;
            status = AVDTP_Connect(&device->channel, remDev);
            if (status != BT_STATUS_PENDING) {
                /* Failure, free the device */
                Report(("A2DP: Could not open the connection\n"));
                A2dpClose(device);
            }
            break;
        } else if (status == BT_STATUS_SDP_CONT_STATE) {
            /* We are going to query again for the rest of the response */
#if A2DP_SOURCE == XA_ENABLED
            if (device->sdpQueryToken.parms == A2dpSrcServiceSearchAttribReq) {
                streamType = A2DP_STREAM_TYPE_SOURCE;
            } else {
#endif
#if A2DP_SINK == XA_ENABLED
                streamType = A2DP_STREAM_TYPE_SINK;
#endif
#if A2DP_SOURCE == XA_ENABLED
            }
#endif
            status = A2dpStartServiceQuery(device, streamType, BSQM_CONTINUE);
            if (status != BT_STATUS_PENDING) {
                Report(("A2DP: Could not restart query\n"));
                A2dpClose(device);
            }
            break;
        } else {
            /* Service was not found */
            Report(("A2DP: SDP Query failed\n"));
            A2dpClose(device);
        }
        break;
    case SDEVENT_QUERY_ERR:
    case SDEVENT_QUERY_FAILED:
        /* Query Failed */
        Report(("A2DP: SDP Query failed\n"));
        A2dpClose(device);
        break;
    }
}



