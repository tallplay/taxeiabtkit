/****************************************************************************
 *
 * File:
 *     $Workfile:a2dp_sm.c$ for iAnywhere AV SDK, version 1.2.3
 *     $Revision:44$
 *
 * Description: This file contains the state machine for the Advanced Audio
 *     Distribution Profile (A2DP).
 *             
 * Created:     June 9, 2004
 *
 * Copyright 2004 - 2005 Extended Systems, Inc.

 * Portions copyright 2005-2006 iAnywhere Solutions, Inc.

 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any means, 
 * or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, Inc. 
 * This work contains confidential and proprietary information of Extended 
 * Systems, Inc. which is protected by copyright, trade secret, trademark and 
 * other intellectual property rights.
 *
 ****************************************************************************/

#include "sys/a2dpi.h"
#include "oslib.h"

/*---------------------------------------------------------------------------
 *            A2dpFreeStreamInfo()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Frees allocated stream information.
 *
 */
void A2dpFreeStreamInfo(A2dpStream *Stream)
{
    A2dpStreamInfo *sInfo;

    /* Free all stream info in the list */
    while (!IsListEmpty(&Stream->streamInfoList)) {
        sInfo = (A2dpStreamInfo *)RemoveHeadList(&Stream->streamInfoList);
        InsertTailList(&A2DP(freeStreamInfoList), &sInfo->node);
    }
}

/*---------------------------------------------------------------------------
 *            A2dpStreamClose()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Close a stream and notify the application.
 *
 */
void A2dpStreamClose(A2dpStream *Stream, AvdtpCallbackParms *Parms)
{
    A2dpCallbackParms parms;
    BtStatus          status;

    if (Stream->flags & A2DP_STRM_FLAG_IDLE) {
        parms.event = A2DP_EVENT_STREAM_IDLE;
        parms.len = 0;
        parms.error = AVDTP_ERR_NO_ERROR;
        parms.status = BT_STATUS_SUCCESS;
        Stream->callback(Stream, &parms);
        return;
    }

    if (A2DP(devRefCount) == 1) {
        if (AVDTP_IsConnected(&Stream->device->channel)) {
            /* This is the last stream using this device.  Shut it down */
            if (Stream->flags & A2DP_STRM_FLAG_CLOSE_INT) {
                /* The local device is shutting down this stream */
                OS_MemCopy((U8 *)&Stream->lastParms, (U8 *)Parms, 
                           sizeof(AvdtpCallbackParms));
                if (Parms->len) {
                    Stream->lastParms.p.capability = &Stream->lastCap;
                    OS_MemCopy((U8 *)Stream->lastParms.p.capability, 
                               (U8 *)Parms->p.capability, 
                               sizeof(AvdtpCapability));
                }
                status = AVDTP_Disconnect(&Stream->device->channel);
                if (status == BT_STATUS_PENDING) {
                    return;
                }
            }
        }
    }

    /* Decrement the reference count */
    Assert(A2DP(devRefCount));
    A2DP(devRefCount)--;

    /* Free stream info */
    if (Stream->currentInfo) {
        InsertTailList(&A2DP(freeStreamInfoList), &Stream->currentInfo->node);
        Stream->currentInfo = 0;
    }
    A2dpFreeStreamInfo(Stream);

    /* Clears reference to the device */
    Stream->device = 0;

    /* Clears the state flags */
    Stream->flags = 0;

    /* Report to app */
    parms.event = A2DP_EVENT_STREAM_CLOSED;
    parms.p.capability = Parms->p.capability;
    parms.len = Parms->len;
    parms.error = Parms->error;
    parms.status = Parms->status;
    OS_MemSet((U8 *)&Stream->lastParms, 0, sizeof(AvdtpCallbackParms));
    Stream->callback(Stream, &parms);
}

/*---------------------------------------------------------------------------
 *            A2dpClose()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Close the device and all open streams.
 *
 */
void A2dpClose(A2dpDevice *Device)
{
    BtStatus    status;
    A2dpStream *stream, *nextStream;

    /* Close all streams */
    if (IsNodeOnList(&A2DP(inuseDeviceList), &Device->node)) {
        Device->flags = 0;
        RemoveEntryList(&Device->node);
        (void)AVDTP_Deregister(&Device->channel);
        InsertTailList(&A2DP(freeDeviceList), &Device->node);

        stream = (A2dpStream *)GetHeadList(&A2DP(streamList));
        while (stream != (A2dpStream *)&A2DP(streamList)) {
            nextStream = (A2dpStream *)GetNextNode(&stream->node);
            if (stream->device == Device) {
                stream->flags &= ~A2DP_STRM_FLAG_IDLE;
                A2dpStreamClose(stream, &stream->lastParms);
            }
            stream = nextStream;
        }

        (void)CMGR_RemoveDataLink(&Device->cmgrHandler);
        (void)CMGR_DeregisterHandler(&Device->cmgrHandler);
    }

    if ((A2DP(currentDevice) == 0) &&
        !(IsListEmpty(&A2DP(streamList)))) {
        /* Register the freed device */
        A2DP(currentDevice) = (A2dpDevice *)RemoveHeadList(&A2DP(freeDeviceList));
        status = AVDTP_Register(&Device->channel, A2dpAvdtpCallback);
        if (status == BT_STATUS_FAILED) {
            InsertTailList(&A2DP(freeDeviceList), 
                           &A2DP(currentDevice)->node);
            A2DP(currentDevice) = 0;
        }
    }
}

/*---------------------------------------------------------------------------
 *            A2dpFindNextStreamInfo()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Find the next matching stream and get it's capabilities.
 *
 * Returns:   BT_STATUS_PENDING if GetCapabilities was initiated,
 *            BT_STATUS_FAILED if no more streams to query or if an error
 *                occured
 *
 */
static BtStatus A2dpFindNextStreamInfo(A2dpStream *Stream)
{
    BtStatus status = BT_STATUS_FAILED;

    /* If we're looking at a stream info, free it (we know it failed) */
    if (Stream->currentInfo)
    {
        InsertTailList(&A2DP(freeStreamInfoList), &Stream->currentInfo->node);
        Stream->currentInfo = 0;
    }

    /* Search all discovered streams for an unused stream of the correct type */
    while (!IsListEmpty(&Stream->streamInfoList)) {

        Stream->currentInfo = (A2dpStreamInfo *)
            RemoveHeadList(&Stream->streamInfoList);

        /* Try to query for capabilities */
        status = AVDTP_GetCapabilities(&Stream->device->channel,
            Stream->currentInfo->info.id);
        if (status == BT_STATUS_PENDING) break;

        /* If query failed, go to the next one and try again. */
        status = BT_STATUS_FAILED;
        InsertTailList(&A2DP(freeStreamInfoList), &Stream->currentInfo->node);
        Stream->currentInfo = 0;
    }

    return status;
}

/*---------------------------------------------------------------------------
 *            A2dpAvdtpCallback()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Callback function to receive AVDTP events.
 */
void A2dpAvdtpCallback(AvdtpChannel *Chnl, AvdtpCallbackParms *Parms)
{
    A2dpDevice *device = ContainingRecord(Chnl, A2dpDevice, channel);
    A2dpStream *stream = ContainingRecord(Parms->stream, A2dpStream, stream);
    A2dpCallbackParms parms;
    A2dpStreamInfo *info;
    BtStatus status;
    BOOL doCallback = FALSE;
    A2dpSbcPacket *sbcPacket;
    U16 maxPacketLen;
    AvdtpContentProt *cp;

    parms.status = Parms->status;
    parms.error = Parms->error;

    switch (Parms->event) {
    case AVDTP_EVENT_CONNECT_IND:
        if (CMGR_RegisterHandler(&device->cmgrHandler, 
                                 A2dpCmgrCallback) == BT_STATUS_SUCCESS) {
            if (CMGR_CreateDataLink(&device->cmgrHandler, 
                                    &Parms->p.remDev->bdAddr) == BT_STATUS_SUCCESS) {

                if (AVDTP_ConnectRsp(Chnl, TRUE) != BT_STATUS_PENDING) {
                    AssertEval(CMGR_RemoveDataLink(&device->cmgrHandler) == BT_STATUS_SUCCESS);
                    AssertEval(CMGR_DeregisterHandler(&device->cmgrHandler) == BT_STATUS_SUCCESS);
                }
            } else {
                AssertEval(CMGR_DeregisterHandler(&device->cmgrHandler) == BT_STATUS_SUCCESS);
             }
        }
        break;
    case AVDTP_EVENT_CONNECT:
        if (device->currentStream && 
            (device->flags & A2DP_DEV_FLAG_OUTGOING)) {
            status = AVDTP_DiscoverStreams(&device->channel);
            if (status != BT_STATUS_PENDING) {
                /* Failure, free the device */
                A2dpClose(device);
            } else {
                device->currentStream->flags |= A2DP_STRM_FLAG_DISCOVERY;
            }
        } else {
            if (!IsListEmpty(&A2DP(freeDeviceList))) {
                /* Register a new device */
                A2DP(currentDevice) = 
                    (A2dpDevice *)RemoveHeadList(&A2DP(freeDeviceList));
                status = AVDTP_Register(&A2DP(currentDevice)->channel, 
                                        A2dpAvdtpCallback);
                if (status == BT_STATUS_FAILED) {
                    /* Failed to register */
                    InsertTailList(&A2DP(freeDeviceList), 
                                   &A2DP(currentDevice)->node);
                    A2DP(currentDevice) = device;
                } 
            } else {
                /* No more devices */
                A2DP(currentDevice) = 0;
            }

            InsertTailList(&A2DP(inuseDeviceList), &device->node);
        }
        break;
    case AVDTP_EVENT_DISCONNECT:
        A2dpClose(device);
        break;
    case AVDTP_EVENT_STREAM_INFO:
        /* Is this a good candidate stream? Must be free, Audio-based, and
         * opposite of current stream's direction (snk/src). If so, try to cache it.
         */
        if ((Parms->p.streamInfo->inUse == FALSE) &&
            (Parms->p.streamInfo->mediaType == AVDTP_MEDIA_TYPE_AUDIO) &&
            (Parms->p.streamInfo->streamType != device->currentStream->type) &&
            (0 != (info = (A2dpStreamInfo *)RemoveHeadList(&A2DP(freeStreamInfoList))))) {

            /* Make a copy of stream info for later use. */
            OS_MemCopy((U8 *)&info->info, (U8 *)Parms->p.streamInfo, 
                       sizeof(AvdtpStreamInfo));
            InsertTailList(&device->currentStream->streamInfoList, &info->node);
        }
        break;
    case AVDTP_EVENT_DISCOVER_CNF:
        if (Parms->error == AVDTP_ERR_NO_ERROR) {
            /* Begin querying stream capabilities for a match */
            Assert(device->currentStream->currentInfo == 0);
            if (BT_STATUS_PENDING == A2dpFindNextStreamInfo(device->currentStream))
                break;
        }
        /* No matching stream */
        stream->flags |= A2DP_STRM_FLAG_CLOSE_INT;
        A2dpStreamClose(device->currentStream, Parms);
        break;
    case AVDTP_EVENT_CAPABILITY:
        /* Compare capabilities, connect if there is a match */
        stream = device->currentStream;
        if (Parms->p.capability->type == AVDTP_SRV_CAT_MEDIA_CODEC) {
            /* See if this stream supports the correct codec */
            stream->flags &= ~A2DP_STRM_FLAG_QUERY_CODEC;
            if (device->currentStream->stream.codec &&
                (Parms->p.capability->p.codec.codecType == 
                 device->currentStream->stream.codec->codecType)) {
                /* Codec type matches */
                stream->flags |= A2DP_STRM_FLAG_QUERY_CODEC;
                
                /* Notify the application of the codec parameters */
                parms.event = A2DP_EVENT_CODEC_INFO;
                parms.len = sizeof(AvdtpCodec);
                parms.p.codec = &Parms->p.capability->p.codec;
                stream->callback(stream, &parms);
            }
        } else if (Parms->p.capability->type == AVDTP_SRV_CAT_CONTENT_PROTECTION) {
            /* See if content protection is requested */
            stream->flags &= ~A2DP_STRM_FLAG_QUERY_CP;

            /* Search for matching cp */
            cp = (AvdtpContentProt *)GetHeadList(&stream->stream.cpList);
            while (cp && (cp != (AvdtpContentProt *)&stream->stream.cpList)) {
                /* See if the stream supports content protection */
                if (Parms->p.capability->p.cp.cpType == cp->cpType) {

                    /* Content Protection matches */
                    stream->flags |= A2DP_STRM_FLAG_QUERY_CP;

                    /* Notify the application of the content protection parameters */
                    parms.event = A2DP_EVENT_CP_INFO;
                    parms.len = sizeof(AvdtpContentProt);
                    parms.p.cp = &Parms->p.capability->p.cp;
                    stream->callback(stream, &parms);
                    break;
                }
                
                cp = (AvdtpContentProt *)GetNextNode(&cp->node);
            }
        }
        break;
    case AVDTP_EVENT_GET_CAP_CNF:
        stream = device->currentStream;
        /* Continue with capabilities of the next stream */
        if (!(stream->flags & A2DP_STRM_FLAG_GET_CAP)) {
            if (Parms->error == AVDTP_ERR_NO_ERROR) {
                
                if (stream->flags & A2DP_STRM_FLAG_QUERY_CODEC) {
                    /* A matching codec was found */
                    stream->flags &= ~A2DP_STRM_FLAG_DISCOVERY;
                    parms.event = A2DP_EVENT_GET_CONFIG_IND;
                    parms.len = 0;
                    stream->callback(stream, &parms);
                    break;
                }
                
                /* Look for next stream or give up */
                if (BT_STATUS_PENDING != A2dpFindNextStreamInfo(stream))
                    stream->flags |= A2DP_STRM_FLAG_CLOSE_INT;
                    A2dpStreamClose(stream, Parms);                
            } else {
                stream->flags |= A2DP_STRM_FLAG_CLOSE_INT;
                if (AVDTP_GetStreamState(&stream->stream) 
                    != AVDTP_STRM_STATE_IDLE) {                    
                    /* Stream is open, close it */
                    AVDTP_CloseStream(&stream->stream);
                } else {
                    /* Stream is disconnected */
                    A2dpStreamClose(stream, Parms);
                }
            }
        } else {
            /* Get capabilities complete */
            stream->flags &= ~A2DP_STRM_FLAG_GET_CAP;
            parms.event = A2DP_EVENT_GET_CAP_CNF;
            stream->callback(stream, &parms);
        }
        break;
    case AVDTP_EVENT_CFG_CAPABILITY:
        break;
#ifdef AV_WORKER
    case AVDTP_EVENT_SET_CFG_CNF:
        stream = ContainingRecord(Chnl->currentTxStream, A2dpStream, stream);
        parms.event = A2DP_AVDTP_EVENT_SET_CFG_CNF;
        parms.p.cp = Parms->p.cp;
        doCallback = TRUE;
        break;
#endif
    case AVDTP_EVENT_GET_CFG_CNF:
#ifdef AV_WORKER
        stream = ContainingRecord(Chnl->currentTxStream, A2dpStream, stream);
        parms.event = A2DP_AVDTP_EVENT_GET_CFG_CNF;
        parms.p.cp = Parms->p.cp;
        doCallback = TRUE;
#endif
        break;
    case AVDTP_EVENT_STREAM_OPEN_IND:
        /* Received an open request */
        stream->currentInfo = 
            (A2dpStreamInfo *)RemoveHeadList(&A2DP(freeStreamInfoList));
        stream->currentInfo->info.inUse = FALSE;
        stream->currentInfo->info.id = Parms->stream->remStrmId;
        stream->currentInfo->info.mediaType = Parms->stream->mediaType;
        stream->currentInfo->info.streamType = Parms->stream->endPointType;
        parms.event = A2DP_EVENT_STREAM_OPEN_IND;
        if (!(stream->flags & A2DP_STRM_FLAG_IDLE)) {
            A2DP(devRefCount)++;
        }
        stream->device = device;
        parms.p.configReq = Parms->p.configReq;
        doCallback = TRUE;
        break;
    case AVDTP_EVENT_STREAM_OPEN:
        /* A stream is open */
        parms.event = A2DP_EVENT_STREAM_OPEN;

        /* Free stream info, we don't need it anymore */
        A2dpFreeStreamInfo(stream);
    
        parms.p.configReq = Parms->p.configReq;
        doCallback = TRUE;
        break;
    case AVDTP_EVENT_STREAM_CLOSE_IND:
        /* A stream is closing */
        stream->flags |= A2DP_STRM_FLAG_IDLE;
        break;
    case AVDTP_EVENT_STREAM_CLOSED:
        /* A stream is closed */
        stream->device = device;
        if (Parms->status != BT_STATUS_CANCELLED) {
            A2dpStreamClose(stream, Parms);
            stream->flags &= ~A2DP_STRM_FLAG_CLOSE_INT;
        } else {
            /* Send the event (close failed) */
            parms.event = A2DP_EVENT_STREAM_CLOSED;
            doCallback = TRUE;
        }
        break;
    case AVDTP_EVENT_STREAM_START_IND:
        /* An incoming stream is starting */
        parms.event = A2DP_EVENT_STREAM_START_IND;
        doCallback = TRUE;
        break;
    case AVDTP_EVENT_STREAM_STARTED:
        /* A stream has started */
        if (parms.status == BT_STATUS_SUCCESS) {
            CMGR_DisableSniffTimer(&device->cmgrHandler);
        }
        parms.event = A2DP_EVENT_STREAM_STARTED;
        doCallback = TRUE;
        break;
    case AVDTP_EVENT_STREAM_SUSPENDED:
        /* A stream is suspended */
        if (parms.status == BT_STATUS_SUCCESS) {
            CMGR_ClearSniffTimer(&device->cmgrHandler);
        }
        parms.event = A2DP_EVENT_STREAM_SUSPENDED;
        doCallback = TRUE;
        break;
    case AVDTP_EVENT_STREAM_RECONFIG_IND:
        /* A stream has received a request to be reconfigured */
        parms.event = A2DP_EVENT_STREAM_RECONFIG_IND;
        parms.p.configReq = Parms->p.configReq;
        doCallback = TRUE;
        break;
    case AVDTP_EVENT_STREAM_RECONFIG_CNF:
        /* A stream has been reconfigured */
        parms.event = A2DP_EVENT_STREAM_RECONFIG_CNF;
        parms.p.capability = Parms->p.capability;
        doCallback = TRUE;
        break;
    case AVDTP_EVENT_STREAM_SECURITY_IND:
        /* Security Data is incoming */
        parms.event = A2DP_EVENT_STREAM_SECURITY_IND;
        parms.p.data = Parms->p.data;
        doCallback = TRUE;
        break;
    case AVDTP_EVENT_STREAM_SECURITY_CNF:
        /* Security Data has been confirmed */
        parms.event = A2DP_EVENT_STREAM_SECURITY_CNF;
        parms.p.data = Parms->p.data;
        doCallback = TRUE;
        break;
    case AVDTP_EVENT_STREAM_ABORTED:
        /* A stream has been aborted */
        if (parms.status == BT_STATUS_SUCCESS) {
            stream->flags |= A2DP_STRM_FLAG_IDLE;
        }
        parms.event = A2DP_EVENT_STREAM_ABORTED;
        doCallback = TRUE;
        break;
    case AVDTP_EVENT_STREAM_DATA_IND:
        /* Stream Data is incoming */
        parms.event = A2DP_EVENT_STREAM_DATA_IND;
        parms.p.data = Parms->p.data;
        doCallback = TRUE;
        break;
    case AVDTP_EVENT_STREAM_PACKET_SENT:
        /* Stream Data has been sent */
        if (Parms->p.packet->ulpContext == (void *)0) {
            parms.event = A2DP_EVENT_STREAM_PACKET_SENT;
            parms.p.btPacket = Parms->p.packet;
            doCallback = TRUE;
        } else {
            /* SBC data sent */
            sbcPacket = (A2dpSbcPacket *)GetHeadList(&stream->sbcQueue);
            Assert(&sbcPacket->packet == Parms->p.packet);
            if (sbcPacket->dataLen > sbcPacket->dataSent) {
                /* Still data to send */

                maxPacketLen = A2DP_MediaPacketSize(stream) - 13;
                if (sbcPacket->frameSize > maxPacketLen) {
                    /* Segmented SBC frame */
                    sbcPacket->packet.header[BT_PACKET_HEADER_LEN - 1]--;
                    if ((sbcPacket->packet.header[BT_PACKET_HEADER_LEN - 1] & 
                         0x0F) == 0) {
                        /* First segment */
                        sbcPacket->packet.header[BT_PACKET_HEADER_LEN - 1] = 0xC0;
                        sbcPacket->packet.header[BT_PACKET_HEADER_LEN - 1] += 
                                           sbcPacket->frameSize / maxPacketLen;
                        if (sbcPacket->frameSize % maxPacketLen) {
                            sbcPacket->packet.header[BT_PACKET_HEADER_LEN - 1]++;
                        }
                    } else if ((sbcPacket->packet.header[BT_PACKET_HEADER_LEN - 1] &
                                0x0F) > 1) {
                        /* Intermediate segement */
                        sbcPacket->packet.header[BT_PACKET_HEADER_LEN - 1] &= 0x8F;
                    } else {
                        /* Last segment */
                        sbcPacket->packet.header[BT_PACKET_HEADER_LEN - 1] = 0xA1;
                    }
                }

                status = A2dpStreamSendSbcPacket(stream, sbcPacket);
                if (status == BT_STATUS_PENDING) {
                    break;
                } else {
                    parms.error = AVDTP_ERR_NO_ERROR;
                    parms.status = status;
                }
            }
             
            /* Return the packet */
            Parms->p.packet->flags = 0;
            parms.event = A2DP_EVENT_STREAM_SBC_PACKET_SENT;
            parms.p.sbcPacket = (A2dpSbcPacket *)RemoveHeadList(&stream->sbcQueue);
            parms.len = Parms->len;
            stream->callback(stream, &parms);

            /* Send next queued packet */
            while (!IsListEmpty(&stream->sbcQueue)) {
                sbcPacket = (A2dpSbcPacket *)GetHeadList(&stream->sbcQueue);
                status = A2dpStreamSendSbcPacket(stream, sbcPacket);
                if (status != BT_STATUS_PENDING) {
                    Parms->p.packet->flags = 0;
                    parms.event = A2DP_EVENT_STREAM_SBC_PACKET_SENT;
                    parms.p.sbcPacket = (A2dpSbcPacket *)RemoveHeadList(&stream->sbcQueue);
                    parms.len = Parms->len;
                    stream->callback(stream, &parms);
                } else {
                    break;
                }
            }
        }
        break;
    }

    if (doCallback) {
        parms.len = Parms->len;
        stream->callback(stream, &parms);
    }
}

/*---------------------------------------------------------------------------
 *            A2dpCmgrCallback()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Called by device manager with link state events.
 *
 */
void A2dpCmgrCallback(CmgrHandler *cHandler, 
                      CmgrEvent Event, 
                      BtStatus Status)
{
    A2dpDevice *device = ContainingRecord(cHandler, A2dpDevice, cmgrHandler);
    
    switch (Event) {

    case CMEVENT_DATA_LINK_CON_CNF:
        if (Status == BT_STATUS_SUCCESS) {
            /* ACL is connected, start SDP Query */
            Status = A2dpStartServiceQuery(device, 
                                           device->currentStream->type, 
                                           BSQM_FIRST);
            if (Status == BT_STATUS_PENDING) {
                return;
            }
        }

        /* Disconnect and call the application */
        A2dpClose(device);
        break;
    }
}


