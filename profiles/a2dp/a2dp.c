/****************************************************************************
 *
 * File:
 *     $Workfile:a2dp.c$ for iAnywhere AV SDK, version 1.2.3
 *     $Revision:37$
 *
 * Description: This file contains the API for the Advanced Audio
 *     Distribution Profile (A2DP).
 *             
 * Created:     June 8, 2004
 *
 * Copyright 2004 - 2005 Extended Systems, Inc.

 * Portions copyright 2005-2006 iAnywhere Solutions, Inc.

 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any means, 
 * or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, Inc. 
 * This work contains confidential and proprietary information of Extended 
 * Systems, Inc. which is protected by copyright, trade secret, trademark and 
 * other intellectual property rights.
 *
 ****************************************************************************/

#include "sys/a2dpi.h"
#include "oslib.h"
#include "conmgr.h"

/*---------------------------------------------------------------------------
 *            A2DP_Init()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initialize the A2DP SDK.
 *
 * Returns:   (See header file)
 */
BOOL A2DP_Init(void)
{
    BOOL status;
    U8 i;

    status = A2dpAlloc();
    if (status == FALSE) {
        goto exit;
    }

    /* Initialize Device List */
    InitializeListHead(&A2DP(freeDeviceList));
    for (i = 0; i < NUM_BT_DEVICES; i++) {
        OS_MemSet((U8 *)&A2DP(device[i]), 0, sizeof(A2dpDevice));
        InsertTailList(&A2DP(freeDeviceList), &A2DP(device[i]).node);
    }

    /* Initialize the stream info list */
    InitializeListHead(&A2DP(freeStreamInfoList));
    for (i = 0; i < A2DP_MAX_STREAMINFOS; i++) {
        OS_MemSet((U8 *)&A2DP(streamInfos[i]), 0, sizeof(A2dpStreamInfo));
        InsertTailList(&A2DP(freeStreamInfoList), &A2DP(streamInfos[i]).node);
    }

    /* Initialize the Stream registration list */    
    InitializeListHead(&A2DP(streamList));
    InitializeListHead(&A2DP(inuseDeviceList));

    status = AVDTP_Init();
    if (status == FALSE) {
        goto exit;
    }

    exit:

    return status;
}

/*---------------------------------------------------------------------------
 *             A2DP_Register()
 *---------------------------------------------------------------------------
 *
 * Synopsis:   Registers an audio source or sink for both originating and 
 *             receiving connections.
 *
 * Returns:    (See header file)
 */
BtStatus A2DP_Register(A2dpStream *Stream, AvdtpCodec *Codec, 
                       A2dpCallback Callback)
{
    BtStatus    status = BT_STATUS_SUCCESS;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Stream != 0);
    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Codec != 0);
    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Callback != 0);

    OS_LockStack();

    if (IsNodeOnList(&A2DP(streamList), &Stream->node)) {
        status = BT_STATUS_IN_USE;
        goto exit;
    }

    /* Register a channel with AVDTP */
    if ((A2DP(snkRefCount) == 0) && (A2DP(srcRefCount) == 0)) {
        if (!IsListEmpty(&A2DP(freeDeviceList))) {
            A2DP(currentDevice) = 
            (A2dpDevice *)RemoveHeadList(&A2DP(freeDeviceList));
            status = AVDTP_Register(&A2DP(currentDevice)->channel, 
                                    A2dpAvdtpCallback);
            if (status != BT_STATUS_SUCCESS) {
                InsertTailList(&A2DP(freeDeviceList), 
                               &A2DP(currentDevice)->node);
                A2DP(currentDevice) = 0;
                goto exit;
            }
        } else {
            status = BT_STATUS_NO_RESOURCES;
            goto exit;
        }
    }

    /* Register the stream with AVDTP */
    Stream->stream.endPointType = Stream->type;
    Stream->stream.mediaType = AVDTP_MEDIA_TYPE_AUDIO;
    status = AVDTP_RegisterStream(&Stream->stream, Codec);
    if (status != BT_STATUS_SUCCESS) {
        /* Failed, deregister channel if necessary */
        if ((A2DP(snkRefCount) == 0) && (A2DP(srcRefCount) == 0)) {
            (void)AVDTP_Deregister(&A2DP(currentDevice)->channel);
            InsertTailList(&A2DP(freeDeviceList), 
                           &A2DP(currentDevice)->node);
            A2DP(currentDevice) = 0;
        }
        goto exit;
    }

    /* Register with SDP */
    if (Stream->type == A2DP_STREAM_TYPE_SOURCE) {
        if (A2DP(srcRefCount) == 0) {
            status = A2dpRegisterSdpService(A2DP_STREAM_TYPE_SOURCE);
            if (status != BT_STATUS_SUCCESS) {

                /* Failed, deregister the stream */
                (void)AVDTP_DeregisterStream(&Stream->stream);

                /* Failed, deregister channel if necessary */
                if (A2DP(snkRefCount) == 0) {
                    (void)AVDTP_Deregister(&A2DP(currentDevice)->channel);
                    InsertTailList(&A2DP(freeDeviceList), 
                                   &A2DP(currentDevice)->node);
                    A2DP(currentDevice) = 0;
                }
                goto exit;
            }
        }
        A2DP(srcRefCount)++;
    } else {
        if (A2DP(snkRefCount) == 0) {
            status = A2dpRegisterSdpService(A2DP_STREAM_TYPE_SINK);
            if (status != BT_STATUS_SUCCESS) {

                /* Failed, deregister the stream */
                (void)AVDTP_DeregisterStream(&Stream->stream);

                /* Failed, deregister channel if necessary */
                if (A2DP(srcRefCount) == 0) {
                    (void)AVDTP_Deregister(&A2DP(currentDevice)->channel);
                    InsertTailList(&A2DP(freeDeviceList), 
                                   &A2DP(currentDevice)->node);
                    A2DP(currentDevice) = 0;
                }
                goto exit;
            }
        }
        A2DP(snkRefCount)++;
    }

    /* Initialize the Stream */
    InitializeListHead(&Stream->sbcQueue);
    InitializeListHead(&Stream->streamInfoList);
    Stream->callback = Callback;
    Stream->device = 0;
    Stream->flags = 0;
    Stream->currentInfo = 0;
    OS_MemSet((U8 *)&Stream->lastParms, 0, sizeof(AvdtpCallbackParms));
    InsertTailList(&A2DP(streamList), &Stream->node);

    /* Initialize Media Packet Header for SBC */
    Stream->mediaHeader.version = 2;
    Stream->mediaHeader.padding = 0;
    Stream->mediaHeader.marker = 0;
    Stream->mediaHeader.payloadType = 101;
    Stream->mediaHeader.csrcCount = 0;
    Stream->mediaHeader.sequenceNumber = OS_Rand();
    Stream->mediaHeader.timestamp = (OS_Rand() << 16) + OS_Rand();
    Stream->mediaHeader.ssrc = (OS_Rand() << 16) + OS_Rand();

    exit:

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            A2DP_Deregister()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Deregisters the audio source or sink.
 *
 * Returns:   (See header file)
 */
BtStatus A2DP_Deregister(A2dpStream *Stream)
{
    BtStatus status = BT_STATUS_SUCCESS;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Stream != 0);

    OS_LockStack();

    if (IsNodeOnList(&A2DP(streamList), &Stream->node)) {
        /* Stream is registered */
        if (AVDTP_GetStreamState(&Stream->stream) == AVDTP_STRM_STATE_IDLE) {
            /* Stream is in the correct state to be deregistered */
            status = AVDTP_DeregisterStream(&Stream->stream);
            if (status == BT_STATUS_SUCCESS) {
                /* Deregister SDP if necessary */
                if (Stream->type == A2DP_STREAM_TYPE_SOURCE) {
                    /* Source */
                    if (A2DP(srcRefCount) == 1) {
                        A2dpDeregisterSdpService(Stream->type);
                    }
                    Assert(A2DP(srcRefCount) > 0);
                    A2DP(srcRefCount)--;
                } else {
                    /* Sink */
                    if (A2DP(snkRefCount) == 1) {
                        A2dpDeregisterSdpService(Stream->type);
                    }
                    Assert(A2DP(snkRefCount) > 0);
                    A2DP(snkRefCount)--;
                }

                /* Stream was deregistered successfully */
                RemoveEntryList(&Stream->node);
                if (IsListEmpty(&A2DP(streamList))) {
                    Assert((A2DP(srcRefCount) == 0) ||
                           (A2DP(snkRefCount) == 0));
                    /* This is the last stream, unregister the current device */
                    status = AVDTP_Deregister(&A2DP(currentDevice)->channel);
                    if (status == BT_STATUS_SUCCESS) {
                        InsertTailList(&A2DP(freeDeviceList),
                                       &A2DP(currentDevice)->node);
                        A2DP(currentDevice) = 0;
                    }
                }
            }
        } else {
            status = BT_STATUS_IN_USE;
        }
    } else {
        status = BT_STATUS_NOT_FOUND;
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            A2DP_OpenStream()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Open a stream.
 *
 * Returns:   (See header file)
 */
BtStatus A2DP_OpenStream(A2dpStream *Stream, BD_ADDR *Addr)
{
    BtStatus status = BT_STATUS_SUCCESS;
    A2dpDevice *device;
    BtRemoteDevice *remDev;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Stream != 0);

    OS_LockStack();

    if (IsNodeOnList(&A2DP(streamList), &Stream->node)) {
        if (AVDTP_GetStreamState(&Stream->stream) == AVDTP_STRM_STATE_IDLE) {

            /* See if a connection already exists */
            device = (A2dpDevice *)GetHeadList(&A2DP(inuseDeviceList));
            while (device != (A2dpDevice *)&A2DP(inuseDeviceList)) {
                if (AVDTP_IsConnected(&device->channel)) {
                    remDev = AVDTP_GetRemoteDevice(&device->channel);

                    /* The specified device is already connected.
                     * Start searching the list for a stream with matching info.
                     * Do a stream discovery.
                     */
                    if (!(Stream->flags & A2DP_STRM_FLAG_DISCOVERY)) {
                        A2dpFreeStreamInfo(Stream);

                        if (Stream->currentInfo) {
                            /* Try to query for capabilities */
                            status = AVDTP_GetCapabilities(&Stream->device->channel,
                                                           Stream->currentInfo->info.id);
                        } else {
                            status = AVDTP_DiscoverStreams(&device->channel);
                            if (status == BT_STATUS_PENDING) {
                                Stream->flags |= A2DP_STRM_FLAG_DISCOVERY;
                            }
                        }
                    } else {
                        status = BT_STATUS_BUSY;
                    }
                    break;
                } else {
                    status = BT_STATUS_BUSY;
                }
                device = (A2dpDevice *)GetNextNode(&device->node);
            }

            if (device == (A2dpDevice *)&A2DP(inuseDeviceList)) {
                if (A2DP(currentDevice)) {
                    /* No existing connection, need a new device */
                    device = A2DP(currentDevice);
                    if (!IsListEmpty(&A2DP(freeDeviceList))) {
                        /* Register a new device */
                        A2DP(currentDevice) = 
                        (A2dpDevice *)RemoveHeadList(&A2DP(freeDeviceList));
                        status = AVDTP_Register(&A2DP(currentDevice)->channel, 
                                                A2dpAvdtpCallback);
                        if (status != BT_STATUS_SUCCESS) {
                            /* Failed to register */
                            InsertTailList(&A2DP(freeDeviceList), 
                                           &A2DP(currentDevice)->node);
                            A2DP(currentDevice) = device;
                            goto exit;
                        }
                    } else {
                        /* No more devices */
                        A2DP(currentDevice) = 0;
                    }
                } else {
                    status = BT_STATUS_NO_RESOURCES;
                    goto exit;
                }

                /* Establish the ACL link */
                (void)CMGR_RegisterHandler(&device->cmgrHandler, A2dpCmgrCallback);
                device->cmgrHandler.devQuery.quality.mask = 
                                        (BDQM_DEVICE_CLASS|BDQM_DEVICE_STATUS|
                                         BDQM_SERVICE_CLASS|BDQM_SERVICE_UUID);
                device->cmgrHandler.devQuery.quality.status = BDS_IN_RANGE;
                device->cmgrHandler.devQuery.quality.deviceClass = COD_MAJOR_AUDIO;
                device->cmgrHandler.devQuery.quality.serviceUuid = SC_AUDIO_VIDEO;
                if (Stream->type == A2DP_STREAM_TYPE_SOURCE) {
                    device->cmgrHandler.devQuery.quality.serviceClass = COD_RENDERING;
                } else {
                    device->cmgrHandler.devQuery.quality.serviceClass = COD_CAPTURING;
                }
                status = CMGR_CreateDataLink(&device->cmgrHandler, Addr);
                if (status == BT_STATUS_SUCCESS) {
                    A2DP(currentDevice) = 0;
                    /* Do an SDP discovery */
                    status = A2dpStartServiceQuery(device, 
                                                   Stream->type, 
                                                   BSQM_FIRST);
                }

                if (status == BT_STATUS_PENDING) {
                    /* Connection started */
                    InsertTailList(&A2DP(inuseDeviceList), &device->node);
                } else {
                    /* Failed to connect */
                    if (A2DP(currentDevice)) {
                        /* Put the new device back on the free list */
                        (void)AVDTP_Deregister(&A2DP(currentDevice)->channel);
                        InsertTailList(&A2DP(freeDeviceList), 
                                       &A2DP(currentDevice)->node);
                    }
                    A2DP(currentDevice) = device;
                }
            }

            if (status == BT_STATUS_PENDING) {
                /* Initialize the device */
                device->currentStream = Stream;
                device->flags = A2DP_DEV_FLAG_OUTGOING;
                Stream->device = device;
                if (!(Stream->flags & A2DP_STRM_FLAG_IDLE)) {
                    A2DP(devRefCount)++;
                }
            }
        } else {
            status = BT_STATUS_IN_USE;
        }
    } else {
        status = BT_STATUS_NOT_FOUND;
    }

    exit:

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            A2DP_OpenStreamRsp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Responds to an open request.
 *
 * Returns:   (See header file)
 */
BtStatus A2DP_OpenStreamRsp(A2dpStream *Stream, A2dpError Error, 
                            AvdtpCapabilityType CapType)
{
    BtStatus status;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Stream != 0);

    OS_LockStack();

    /* Accept or reject the request to open */
    status = AVDTP_OpenStreamRsp(&Stream->device->channel, 
                                 &Stream->stream, 
                                 Error, CapType);
    if (status == BT_STATUS_PENDING) {
        Stream->device->currentStream = Stream;
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            A2DP_SetStreamConfig()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Configures the specified stream.
 *
 * Returns:   (See header file)
 */
BtStatus A2DP_SetStreamConfig(A2dpStream *Stream, AvdtpCodec *Codec, 
                              AvdtpContentProt *Cp)
{
    BtStatus status;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Stream != 0);
    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Codec != 0);

    OS_LockStack();

    if ((AVDTP_GetStreamState(&Stream->stream) == AVDTP_STRM_STATE_IDLE) &&
        (Stream->currentInfo)) {
        /* Open the stream */
        status = AVDTP_OpenStream(&Stream->device->channel,
                                  &Stream->stream,
                                  Stream->currentInfo->info.id,
                                  Codec,
                                  Cp);

        if (status == BT_STATUS_PENDING) {
            Stream->device->currentStream = Stream;
        }
    } else {
        status = BT_STATUS_FAILED;
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            A2DP_GetStreamCapabilities()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Query the remote stream for its capabilities.
 *
 * Returns:   (See header file)
 */
BtStatus A2DP_GetStreamCapabilities(A2dpStream *Stream)
{
    BtStatus status = BT_STATUS_NO_CONNECTION;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Stream != 0);

    OS_LockStack();

    if (Stream->device) {

        /* Get the capabilities of the stream */
        status = AVDTP_GetCapabilities(&Stream->device->channel, 
                                       Stream->stream.remStrmId);
        if (status == BT_STATUS_PENDING) {
            Stream->flags = A2DP_STRM_FLAG_GET_CAP;
        }
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            AwdpCloseTimer()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Closes on the timer context
 *
 * Returns:   void
 */
static void A2dpCloseTimer(EvmTimer *Timer)
{
    AvdtpCallbackParms Parms;
    A2dpStream *stream = Timer->context;

    if (stream->device) {
        Parms.error = AVDTP_ERR_NO_ERROR;
        Parms.status = BT_STATUS_SUCCESS;
        Parms.len = 0;
        A2dpStreamClose(stream, &Parms);
    }
}

/*---------------------------------------------------------------------------
 *            A2dpCloseStream()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Closes the specified stream
 *
 * Returns:   (See header file)
 */
BtStatus A2dpCloseStream(A2dpStream *Stream, BOOL discFlag)
{
    BtStatus status;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Stream != 0);

    OS_LockStack();

    if (Stream->device) {

        if (!discFlag) {
            Stream->flags |= A2DP_STRM_FLAG_IDLE;
        } else {
            Stream->flags &= ~A2DP_STRM_FLAG_IDLE;
        }

        switch (AVDTP_GetStreamState(&Stream->stream)) {
        case AVDTP_STRM_STATE_OPEN:
        case AVDTP_STRM_STATE_STREAMING:
            /* Close the open stream */
            Stream->flags |= A2DP_STRM_FLAG_CLOSE_INT;
            status = AVDTP_CloseStream(&Stream->stream);
            break;
        default:
            if (discFlag) {
                /* Stream is closed, channel is open */
                Stream->flags |= A2DP_STRM_FLAG_CLOSE_INT;
                Stream->closeTimer.context = Stream;
                Stream->closeTimer.func = A2dpCloseTimer;
                EVM_StartTimer(&Stream->closeTimer, 1);
                status = BT_STATUS_PENDING;
            } else {
                status = BT_STATUS_SUCCESS;
            }
            break;
        }
    } else {
        status = BT_STATUS_NO_CONNECTION;
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            A2DP_GetStreamState()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Returns the current stream state.
 *
 * Returns:   (See header file)
 */
A2dpStreamState A2DP_GetStreamState(A2dpStream *Stream)
{
    A2dpStreamState state;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Stream != 0);

    OS_LockStack();

    switch (AVDTP_GetStreamState(&Stream->stream)) {
    case AVDTP_STRM_STATE_IDLE:
        if (Stream->currentInfo) {
            state = A2DP_STREAM_STATE_IDLE;
        } else {
            state = A2DP_STREAM_STATE_CLOSED;
        }
        break;
    case AVDTP_STRM_STATE_CONFIGURED:
    case AVDTP_STRM_STATE_OPEN:
    case AVDTP_STRM_STATE_CLOSING:
    case AVDTP_STRM_STATE_ABORTING:
        state = A2DP_STREAM_STATE_OPEN;
        break;
    case AVDTP_STRM_STATE_STREAMING:
        state = A2DP_STREAM_STATE_STREAMING;
        break;
    default:
        state = A2DP_STREAM_STATE_UNKNOWN;
        break;
    }

    OS_UnlockStack();

    return state;
}

/*---------------------------------------------------------------------------
 *            A2DP_GetConfiguredCodec()
 *---------------------------------------------------------------------------
 *
 * Synopsis: Returns the current codec configuration.
 *
 */
AvdtpCodec * A2DP_GetConfiguredCodec(A2dpStream *Stream)
{
    AvdtpCodec *codec = 0;

    OS_LockStack();

    if (Stream != 0) {
        if (IsNodeOnList(&A2DP(streamList), &Stream->node) &&
            (AVDTP_GetStreamState(&Stream->stream) != AVDTP_STRM_STATE_IDLE)) {
            /* This is a registered stream */
            codec = &Stream->stream.codecCfg;
        }
    }

    OS_UnlockStack();

    return codec;
}

/*---------------------------------------------------------------------------
 *            A2DP_GetRegisteredCodec()
 *---------------------------------------------------------------------------
 *
 * Synopsis: Returns the registered codec.
 *
 */
AvdtpCodec * A2DP_GetRegisteredCodec(A2dpStream *Stream)
{
    AvdtpCodec *codec = 0;

    OS_LockStack();

    if (Stream != 0) {
        if (IsNodeOnList(&A2DP(streamList), &Stream->node)) {
            /* This is a registered stream */
            codec = Stream->stream.codec;
        }
    }

    OS_UnlockStack();

    return codec;
}

/*---------------------------------------------------------------------------
 *            A2DP_GetConfiguredCP()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Returns the configured content protection structure of an open
 *            stream.  If no content protection is configured, this function 
 *            returns 0.
 *
 * Returns:   (See header file)
 */
AvdtpContentProt * A2DP_GetConfiguredCP(A2dpStream *Stream)
{
    AvdtpContentProt *cp = 0;

    OS_LockStack();

    if (Stream != 0) {
        if (IsNodeOnList(&A2DP(streamList), &Stream->node) &&
            (AVDTP_GetStreamState(&Stream->stream) != AVDTP_STRM_STATE_IDLE)) {
            /* This is a registered and configured stream */
            if (Stream->stream.cpCfg.cpType != 0) {
                cp = &Stream->stream.cpCfg;
            }
        }
    }

    OS_UnlockStack();

    return cp;
}

/*---------------------------------------------------------------------------
 *            A2DP_GetRegisteredCP()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Returns the registered content protection structures.  The 
 *            first call should be made with the value of 'Cp' set to 0.  
 *            This will retrieve the first registered structure.  Subsequent 
 *            calls should be made with a pointer to the last structure 
 *            returned by this function in the 'Cp' parameter.  In this case, 
 *            the next registered structure will be returned.  If there are 
 *            no more structures, then 0 will be returned.
 *
 * Returns:   (See header file)
 */
AvdtpContentProt * A2DP_GetRegisteredCP(A2dpStream *Stream,
                                        AvdtpContentProt *Cp)
{
    AvdtpContentProt *cp;

    if (Stream == 0) {
        return 0;
    }

    OS_LockStack();

    if (IsNodeOnList(&A2DP(streamList), &Stream->node)) {
        /* This is a registered stream */
        if (!Cp) {
            /* Get the first content protection capability */
            if (!IsListEmpty(&Stream->stream.cpList)) {
                cp = (AvdtpContentProt *)GetHeadList(&Stream->stream.cpList);
            } else {
                /* No content protection */
                cp = 0;
            }
        } else {
            /* Get the next content protection */
            cp = (AvdtpContentProt *)GetNextNode(&Cp->node);
            if (cp == (AvdtpContentProt *)&Stream->stream.cpList) {
                /* No more entries */
                cp = 0;
            }
        }
    } else {
        cp = 0;
    }

    OS_UnlockStack();

    return cp;
}


/*---------------------------------------------------------------------------
 *            A2DP_StreamSendRawPacket()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Sends data on the specified stream.  The stream must be open and
 *            in an streaming state.  No checking is done on the validity of
 *            the data format.  Data is simply delivered to the stream raw.  
 *            It is assumed that the data is properly formatted using the RTP
 *            media packet format described in RFC1889 (see 
 *            A2DP_CreateMediaHeader).  It is further assumed that the payload 
 *            of the RTP media packet is in the proper format for the configured 
 *            stream.
 *
 * Returns:   (See header file)
 */
BtStatus A2DP_StreamSendRawPacket(A2dpStream *Stream, BtPacket *Packet)
{
    BtStatus status;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Stream != 0);
    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Packet != 0);
    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Packet->data != 0);

    OS_LockStack();

    if (Stream->stream.state == AVDTP_STRM_STATE_STREAMING) {
        Packet->flags &= BTP_FLAG_INUSE;
        Packet->ulpContext = 0;
        status = AVDTP_StreamSendRawPacket(&Stream->stream, Packet);
    } else {
        status = BT_STATUS_NO_CONNECTION;
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            A2DP_StreamSendSbcPacket()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Sends SBC data on the specified stream.  The stream must be open
 *            and in an streaming state.  No checking is done on the validity of
 *            the data format.  Data is delivered to the stream with the media
 *            packet header and SBC header. 
 *
 * Returns:   (See header file)
 */
BtStatus A2DP_StreamSendSbcPacket(A2dpStream *Stream, 
                                  A2dpSbcPacket *Packet,
                                  SbcStreamInfo *StreamInfo)
{
    BtStatus status;
    U16      maxPacketLen;

#if (XA_DEBUG == XA_ENABLED) || (XA_ERROR_CHECK == XA_ENABLED)
    U16      headerLen;
#endif

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Stream != 0);
    CheckUnlockedParm(BT_STATUS_INVALID_PARM, Packet != 0);
    CheckUnlockedParm(BT_STATUS_INVALID_PARM, StreamInfo != 0);

    OS_LockStack();

    if (Stream->stream.state == AVDTP_STRM_STATE_STREAMING) {
#if (XA_DEBUG == XA_ENABLED) || (XA_ERROR_CHECK == XA_ENABLED)
        headerLen = A2DP_CreateMediaHeader(&Stream->mediaHeader, 0);
        CheckLockedParm(BT_STATUS_INVALID_PARM, headerLen == 12);
#endif

        Stream->timeInc = StreamInfo->numBlocks * 
                          StreamInfo->numSubBands;

        if (IsNodeOnList(&Stream->sbcQueue, &Packet->node)) {
            status = BT_STATUS_IN_USE;
            goto exit;
        }

        maxPacketLen = A2DP_MediaPacketSize(Stream) - 13;
        if (Packet->frameSize > maxPacketLen) {
            /* Must segment an SBC frame */
            Packet->packet.header[BT_PACKET_HEADER_LEN - 1] = 0xC0;
            Packet->packet.header[BT_PACKET_HEADER_LEN - 1] += 
                Packet->frameSize / maxPacketLen;
            if (Packet->frameSize % maxPacketLen) {
                Packet->packet.header[BT_PACKET_HEADER_LEN - 1]++;
            }
        }

        Packet->dataSent = 0;
        if (IsListEmpty(&Stream->sbcQueue)) {
            status = A2dpStreamSendSbcPacket(Stream, Packet);
            if (status == BT_STATUS_PENDING) {
                InsertTailList(&Stream->sbcQueue, &Packet->node);
            }
        } else {
            InsertTailList(&Stream->sbcQueue, &Packet->node);
            status = BT_STATUS_PENDING;
        }
    } else {
        status = BT_STATUS_NO_CONNECTION;
    }

    exit:

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            A2dpStreamSendSbcPacket()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Sends SBC packets.
 */
BtStatus A2dpStreamSendSbcPacket(A2dpStream *Stream, A2dpSbcPacket *Packet)
{
    BtStatus status;
    U8       numFrames;
    U16      maxPacketLen;
    U16      dataToSend = Packet->dataLen - Packet->dataSent;
    U8      *ptr = Packet->data + Packet->dataSent;
    U8       headerLen;

    Packet->packet.flags = BTP_FLAG_INUSE;
    headerLen = A2DP_CreateMediaHeader(&Stream->mediaHeader, 
                         &Packet->packet.header[BT_PACKET_HEADER_LEN - 13]) + 1;
    Packet->packet.headerLen = headerLen;

    maxPacketLen = A2DP_MediaPacketSize(Stream) - headerLen;
    if (Packet->frameSize > maxPacketLen) {
        /* Segmented SBC frame */
        Assert(Packet->packet.header[BT_PACKET_HEADER_LEN - 1] > 0);

        if (Packet->packet.header[BT_PACKET_HEADER_LEN - 1] & 0x40) {
            /* First segment */
            Packet->frmDataSent = 0;
        }

        if ((Packet->packet.header[BT_PACKET_HEADER_LEN - 1] & 0x0F) > 1) {
            /* Frist or Intermediate segment */
            Packet->packet.dataLen = min(maxPacketLen, dataToSend);
        } else {
            /* Last segment */
            Packet->packet.dataLen = Packet->frameSize - Packet->frmDataSent;
            Stream->mediaHeader.timestamp += Stream->timeInc;
        }
        Packet->packet.data = ptr;
    } else {
        /* Can send at least one SBC frame per packet */
        Packet->packet.dataLen = min(maxPacketLen, dataToSend);
        numFrames = Packet->packet.dataLen / Packet->frameSize;
        if (numFrames) {
            Stream->mediaHeader.timestamp += Stream->timeInc * numFrames;
            Packet->packet.dataLen = numFrames * Packet->frameSize;
            Packet->packet.header[BT_PACKET_HEADER_LEN - 1] = 0;
            Packet->packet.header[BT_PACKET_HEADER_LEN - 1] += numFrames;
            Packet->packet.data = ptr;
        } else {
            status = BT_STATUS_FAILED;
            goto exit;
        }
    }

    Packet->packet.headerLen = headerLen;
    status = A2DP_StreamSendRawPacket(Stream, &Packet->packet);
    if (status == BT_STATUS_PENDING) {
        Stream->mediaHeader.sequenceNumber++;
        Packet->packet.ulpContext = (void *)1;
        Packet->dataSent += Packet->packet.dataLen;
        Packet->frmDataSent += Packet->packet.dataLen;
    }

    exit:

    return status;
}

