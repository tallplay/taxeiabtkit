/****************************************************************************
 *
 * File:
 *     $Workfile:a2alloc.c$ for iAnywhere AV SDK, version 1.2.3
 *     $Revision:5$
 *
 * Description: This file contains memory allocation code for the Advanced
 *     Audio Distribution Profile (A2DP).
 *             
 * Created:     June 8, 2004
 *
 * Copyright 2004 - 2005 Extended Systems, Inc.

 * Portions copyright 2005-2006 iAnywhere Solutions, Inc.

 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any means, 
 * or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, Inc. 
 * This work contains confidential and proprietary information of Extended 
 * Systems, Inc. which is protected by copyright, trade secret, trademark and 
 * other intellectual property rights.
 *
 ****************************************************************************/

#include "sys/a2alloc.h"

#if XA_CONTEXT_PTR == XA_ENABLED
/* The stack context is accessed through a pointer. Normally in this case
 * the stack context would be dynamically allocated. For testing purposes
 * the pointer points to a global structure.
 */
BtA2dpContext  a2Tmp;
BtA2dpContext *a2dpContext = &a2Tmp;
#else /* XA_CONTEXT_PTR == XA_ENABLED */
BtA2dpContext a2dpContext;
#endif /* XA_CONTEXT_PTR */

/*---------------------------------------------------------------------------
 * A2dpAlloc()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initializes the global context for the A2DP profile.  This 
 *            function can be replaced if necessary.
 *
 * Return:    void
 */
BOOL A2dpAlloc(void)
{
    U8* ptr; 

    /* Fill memory with 0. */
#if XA_CONTEXT_PTR == XA_ENABLED
    ptr = (U8*)a2dpContext;
#else
    ptr = (U8*)&a2dpContext;
#endif /* XA_CONTEXT_PTR */

    OS_MemSet(ptr, 0, (U32)sizeof(BtA2dpContext));

    return TRUE;
}


