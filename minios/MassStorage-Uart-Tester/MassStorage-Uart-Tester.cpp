// MassStorage-Uart-Tester.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "MassStorage-Uart-Tester.h"
#include "MassStorage-Uart-TesterDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMassStorageUartTesterApp

BEGIN_MESSAGE_MAP(CMassStorageUartTesterApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CMassStorageUartTesterApp construction

CMassStorageUartTesterApp::CMassStorageUartTesterApp()
: m_thcf_cursel(NULL)
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
	 HCCExecuteState = HCC_IDLE;
	 bHCCEventRecv = FALSE;

     for(int i = 0; i < 10; i++)
 	 {
	    bACLConnected[i] = FALSE;
        ACLHandleID[i] = 0x0000;
     }

     SettingExecuteState = 0;
	 bHCDSend = FALSE;
	 bHCDRecv = FALSE;
	 HCDTestState = 0;
}


// The one and only CMassStorageUartTesterApp object

CMassStorageUartTesterApp theApp;


// CMassStorageUartTesterApp initialization

BOOL CMassStorageUartTesterApp::InitInstance()
{
	CWinApp::InitInstance();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	// of your final executable, you should remove from the following
	// the specific initialization routines you do not need
	// Change the registry key under which our settings are stored
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization
	SetRegistryKey(_T("Local AppWizard-Generated Applications"));

	CMassStorageUartTesterDlg dlg;
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}
