// MassStorage-Uart-TesterDlg.cpp : implementation file
//

#include "stdafx.h"
#include "MassStorage-Uart-Tester.h"
#include "MassStorage-Uart-TesterDlg.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMassStorageUartTesterDlg dialog




CMassStorageUartTesterDlg::CMassStorageUartTesterDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CMassStorageUartTesterDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CMassStorageUartTesterDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO_HCICOMMAND, m_combo_hci_command);
	DDX_Control(pDX, IDC_EDIT_HCICOMMANDDATA, m_edit_hci_command_data);
	DDX_Control(pDX, IDC_LIST_MSG, m_list_msg);
	DDX_Control(pDX, IDC_EDIT_BDADDR, m_edit_bdaddr);
	DDX_Control(pDX, IDC_EDIT_BDADDR_ACCEPT, m_edit_bdaddr_accept);
}

BEGIN_MESSAGE_MAP(CMassStorageUartTesterDlg, CDialog)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDCANCEL, &CMassStorageUartTesterDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_BUTTON1, &CMassStorageUartTesterDlg::OnBnClickedButton1)
	ON_CBN_SELCHANGE(IDC_COMBO_HCICOMMAND, &CMassStorageUartTesterDlg::OnCbnSelchangeComboHcicommand)
	ON_EN_UPDATE(IDC_EDIT_HCICOMMANDDATA, &CMassStorageUartTesterDlg::OnEnUpdateEditHcicommanddata)
	ON_BN_CLICKED(IDC_BUTTON_SETADDR, &CMassStorageUartTesterDlg::OnBnClickedButtonSetaddr)
	ON_EN_UPDATE(IDC_EDIT_BDADDR, &CMassStorageUartTesterDlg::OnEnUpdateEditBdaddr)
	ON_BN_CLICKED(IDC_BUTTON_PageScan, &CMassStorageUartTesterDlg::OnBnClickedButtonPagescan)
	ON_BN_CLICKED(IDC_BUTTON_AUTO_ACCEPT, &CMassStorageUartTesterDlg::OnBnClickedButtonAutoAccept)
	ON_EN_UPDATE(IDC_EDIT_BDADDR_ACCEPT, &CMassStorageUartTesterDlg::OnEnUpdateEditBdaddrAccept)
	ON_BN_CLICKED(IDC_BUTTON_ACL1, &CMassStorageUartTesterDlg::OnBnClickedButtonAcl1)
	ON_BN_CLICKED(IDC_BUTTON_ACL2, &CMassStorageUartTesterDlg::OnBnClickedButtonAcl2)
	ON_BN_CLICKED(IDC_BUTTON_ACL3, &CMassStorageUartTesterDlg::OnBnClickedButtonAcl3)
	ON_BN_CLICKED(IDC_BUTTON_ACL4, &CMassStorageUartTesterDlg::OnBnClickedButtonAcl4)
	ON_BN_CLICKED(IDC_BUTTON_ACLTEST1, &CMassStorageUartTesterDlg::OnBnClickedButtonAcltest1)
	ON_WM_TIMER()
END_MESSAGE_MAP()


// CMassStorageUartTesterDlg message handlers




static CWnd *mycwnd; 

static DWORD FAR PASCAL osThread(LPSTR lpData);
BOOL isRun = FALSE;
HANDLE OSThread;

void HCI_DataHandler(U16 len, U8 *buffer, U8 flags, U16 HciHandle);
void HCI_EventHandler(U8 event, U8 len, U8* data);
BtPacket * HCI_DataReadyHandler(HciHandle HciHndl);
void HCI_DataDoneHandler(BtPacket *Packet, BtStatus Status, HciHandle HciHndl);

#define DATACB_START_TEST 1
#define DATACB_DATA_SENT_SUCCESS 2
#define DATACB_DATA_SENT_FAIL 3
#define DATACB_DATA_RECV 4
#define DATACB_TIMEOUT 5

void DataCallBack(CWnd *cwnd, U8 op);


static DWORD FAR PASCAL osThread(LPSTR lpData)
{
    while(isRun)
	{
	    MINIOS_Run();
	}

	OS_Deinit();
	return 0;
}

	extern "C" {
extern BOOL skip_first;
		}
BOOL CMassStorageUartTesterDlg::OnInitDialog()
{
	DWORD dwThreadID;

	CDialog::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
    skip_first = TRUE;
    if (!OS_Init()) {
		return FALSE;
    }

	StartSniffer(AfxGetInstanceHandle(), SW_SHOW);
	
	// TODO: Add extra initialization here
	mycwnd = this;

    /* Create a secondary thread to watch for an event. */
	isRun = TRUE;
    OSThread = CreateThread((LPSECURITY_ATTRIBUTES)NULL,
                              0,
                              (LPTHREAD_START_ROUTINE)osThread,
                              (LPVOID)NULL,
                              0, &dwThreadID);
    if (NULL == OSThread) {
        isRun = FALSE;
        return FALSE;
    }

    HCI_RegisterEventHandler(HCI_EventHandler);
	HCI_RegisterDataHandler(HCI_DataHandler);
	HCI_RegisterDataReadyHandler(HCI_DataReadyHandler);
	HCI_RegisterDataDoneHandler(HCI_DataDoneHandler);
	m_combo_hci_command.ResetContent();        
	m_combo_hci_command.SetItemData(0, 0);

	for(unsigned char i = 0; i < HCI_Def_GetCommandCnt(); i++)
	{
	    char tmp[100];
		T_HCI_Command_Format *thcf = HCI_Def_GetCommandFormatbyIdx(i);
		sprintf(tmp, "0x%04X %s", thcf->op, pHciCommand(HCI_Def_GetCommandOpByIdx(i)));
	    m_combo_hci_command.AddString(tmp);
		m_combo_hci_command.SetItemData(i, (DWORD_PTR)thcf);
	}
	
	m_combo_hci_command.SetCurSel(-1);
    
    m_edit_bdaddr.SetWindowTextA("112233445566");
	m_edit_bdaddr_accept.SetWindowTextA("000000e18000");
	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CMassStorageUartTesterDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CMassStorageUartTesterDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CMassStorageUartTesterDlg::OnBnClickedCancel()
{
	// TODO: Add your control notification handler code here
	OnCancel();
}

//new data present
void HCI_DataHandler(U16 len, U8 *buffer, U8 flags, U16 HciHandle)
{
    HciHandle &= 0xfff;
    theApp.bHCDRecv = TRUE;
    theApp.HCDHandle = HciHandle;
    theApp.HCDLen = len;
    theApp.HCDData = buffer;

    DataCallBack(mycwnd, DATACB_DATA_RECV); //data received    
    ((CMassStorageUartTesterDlg*)mycwnd)->UpdateHCIDebugUI();
    ((CMassStorageUartTesterDlg*)mycwnd)->UpdateACLUI();

		
    theApp.bHCDRecv = FALSE;
}

//new event present
void HCI_EventHandler(U8 event, U8 len, U8* data)
{
	theApp.HCCEvent = event;
	theApp.HCCEventLen = len;
	memcpy(theApp.HCCEventData, data, len);
    theApp.bHCCEventRecv = TRUE;

    ((CMassStorageUartTesterDlg*)mycwnd)->UpdateHCIDebugUI();
    ((CMassStorageUartTesterDlg*)mycwnd)->UpdateHCICommandUI();
    ((CMassStorageUartTesterDlg*)mycwnd)->UpdateACLUI();
	((CMassStorageUartTesterDlg*)mycwnd)->UpdateSettingUI();
	
	theApp.bHCCEventRecv = FALSE;
}

//data can be sent
BtPacket * HCI_DataReadyHandler(HciHandle HciHndl)
{    
    if(theApp.bHCDSend == TRUE)
	{
     	theApp.bHCDSend = FALSE;
	    return &theApp.HCDSendPacket;
	}
	return 0;
}

//data was sent
void HCI_DataDoneHandler(BtPacket *Packet, BtStatus Status, HciHandle HciHndl)
{
    if(Status == BT_STATUS_SUCCESS)
        DataCallBack(mycwnd, DATACB_DATA_SENT_SUCCESS);
	else
		DataCallBack(mycwnd, DATACB_DATA_SENT_FAIL);
}

void toHex(char hi, char lo, U8 *hex)
{
        char digstr[2] = "0";
        unsigned int n, m;

        digstr[0] = hi;    sscanf(digstr, "%x", &n);
        digstr[0] = lo;    sscanf(digstr, "%x", &m);
        *hex = (n << 4) + m;
}

void CMassStorageUartTesterDlg::SendHCICmd(U16 op, U8 parmlen, U8 *parm)
{
    static HciCommand cmd;
	memcpy(cmd.parms, parm, parmlen);
    HCI_SndCommand(op, parmlen, &cmd);	
}

void CMassStorageUartTesterDlg::OnBnClickedButton1()
{
//    HCI_SndCommand(HCC_ENABLE_DUT, 0, &cmd[0], HCI_ResetHandler, HCE_COMMAND_COMPLETE);
//    HCI_SndCommand(HCC_READ_LOCAL_COMMANDS, 0, &cmd, HCI_ResetHandler, HCE_COMMAND_COMPLETE);
//    HCI_SndCommand(HCC_RESET, 0, &cmd, HCI_ResetHandler, HCE_COMMAND_COMPLETE);
#if 1
    U16 op;
    U8 parmlen;
    U8 data[512];
    char text[1024];
    U8 hex;	

    theApp.HCCCMD = op = theApp.m_thcf_cursel->op;
	theApp.HCCCMDLen = parmlen = theApp.m_thcf_cursel->parmlen;

    m_edit_hci_command_data.GetWindowTextA(text, 1024);

    for(int i = 0; i < parmlen*2; i+=2)
    {
        toHex(text[i], text[i+1], &hex);    
        data[i/2] = hex;
    }

	memcpy(theApp.HCCCMDData, data, parmlen);

	
	theApp.HCCExecuteState = HCC_SEND;
	UpdateHCIDebugUI();
#if 0
	memcpy(cmd.parms, theApp.HCCCMDData, parmlen);

    HCI_SndCommand(op, parmlen, &cmd);	
#else
    SendHCICmd(op, parmlen, theApp.HCCCMDData);
#endif
	UpdateHCICommandUI();
#else
#if STEST==0
{//static tx
    cmd[0].parms[0] = 0x01;//mode, start:0x01
    cmd[0].parms[1] = 0x01;//txrx, tx:0x00
    cmd[0].parms[2] = 0x32;//chnlnumber, 2450
    cmd[0].parms[3] = 0x01;//data, "1":modulated
    cmd[0].parms[4] = 0x00;//modulationmode, basic: 0x00
    
    HCI_SndCommand(HCC_ST_STATIC_TXRX, 5, &cmd[0], HCI_EventHandler);
}
#elif STEST==1
{//txrx test
	cmd[0].parms[0] = 0x80;//RX_On_Start, unchanged:0x80
	cmd[0].parms[1] = 0x80;//Synt_On_Start, unchanged:0x80 
	cmd[0].parms[2] = 0x3f;//TX_On_Start, start on 0x3f*2 us 
	cmd[0].parms[3] = 0x80;//Phd_Off_Start, unchanged:0x80 
	cmd[0].parms[4] = 0x02;//Test_Scenario, TX_1_PATTERN:2
	cmd[0].parms[5] = 0x01;//Hopping_Mode, single:0 standard frequency:1 reduce frequency:2
	cmd[0].parms[6] = 0x32;//Frequency, 2450;
	cmd[0].parms[7] = 0x00;//Interval, number of empty frames 
	cmd[0].parms[8] = 0x84;//Packet_Type, DH1=0x84
	cmd[0].parms[9] = 0x08;//Payload_Size, 8 bytes
	cmd[0].parms[10] = 0x00;
	cmd[0].parms[11] = 0x00;//BD_Address, 
    cmd[0].parms[12] = 0x01;
    cmd[0].parms[13] = 0x02;
    cmd[0].parms[14] = 0x03;
    cmd[0].parms[15] = 0x04;
    cmd[0].parms[16] = 0x05;

	cmd[0].parms[17] = 0x00;//LT_Address, 0x00
	
	cmd[0].parms[18] = 0xff;//Channel_Map    
	cmd[0].parms[19] = 0xff;
	cmd[0].parms[20] = 0xff;
	cmd[0].parms[21] = 0xff;
	cmd[0].parms[22] = 0xff;
	cmd[0].parms[23] = 0xff;
	cmd[0].parms[24] = 0xff;
	cmd[0].parms[25] = 0xff;
	cmd[0].parms[26] = 0xff;
	cmd[0].parms[27] = 0xff;
    HCI_SndCommand(HCC_ST_TXRX_TEST, 28, &cmd[0], HCI_EventHandler);
}
#endif
#endif

}

void CMassStorageUartTesterDlg::OnCbnSelchangeComboHcicommand()
{
	// TODO: Add your control notification handler code here
	int idx;
	idx = m_combo_hci_command.GetCurSel();
    theApp.m_thcf_cursel = HCI_Def_GetCommandFormatbyIdx(idx);
	
	UpdateHCICommandUI();
}

void CMassStorageUartTesterDlg::OnEnUpdateEditHcicommanddata()
{
    static char save_data[1024] = {0};
    char new_data[1024];
    int len, i;
	int permit_len;

	permit_len = theApp.m_thcf_cursel ? theApp.m_thcf_cursel->parmlen * 2 : 0;

    m_edit_hci_command_data.GetWindowTextA(new_data, 1024);
    len = m_edit_hci_command_data.GetWindowTextLengthA();
    
    if(len > permit_len)
    {
		if(strlen(save_data) > permit_len)
		{
			save_data[permit_len] = 0x00;
		}
        m_edit_hci_command_data.SetWindowTextA(save_data);
    }
    else     
    {
        for(i = 0; i < len ; i++)
        {
            if(!isxdigit(new_data[i]))
            {
                m_edit_hci_command_data.SetWindowTextA(save_data);
                break;
            }
        }

        if(i == len && len)
            strcpy(save_data, new_data);  
    }

    UpdateHCICommandUI();
//    UpdateWriteButton();
}

void CMassStorageUartTesterDlg::ParseCmd(char *str, U16 cmd, U8 len, U8 *data)
{
    U8 i;
	sprintf(str, "%s %s", str, pHciCommand(cmd));
	switch(cmd)
	{
	    case HCC_CREATE_CONNECTION:
			sprintf(str, "%s BDADDR =", str);
			for(i = 0; i < len && i < 6; i++)
    			sprintf(str, "%s %02X", str, data[i]);
			sprintf(str, "%s PKType =", str);
			for(i = 6; i < len && i < 6+2; i++)
    			sprintf(str, "%s %02X", str, data[i]);
			sprintf(str, "%s PSRmode =", str);
			for(i = 6+2; i < len && i < 6+2+1; i++)
    			sprintf(str, "%s %02X", str, data[i]);
			sprintf(str, "%s Reserved =", str);
			for(i = 6+2+1; i < len && i < 6+2+1+1; i++)
    			sprintf(str, "%s %02X", str, data[i]);
			sprintf(str, "%s ClkOff =", str);
			for(i = 6+2+1+1; i < len && i < 6+2+1+1+2; i++)
    			sprintf(str, "%s %02X", str, data[i]);
			sprintf(str, "%s AllowRS =", str);
			for(i = 6+2+1+1+2; i < len && i < 6+2+1+1+2+1; i++)
    			sprintf(str, "%s %02X", str, data[i]);
			break;
        case HCC_DISCONNECT:
			sprintf(str, "%s Handle =", str);
			for(i = 0; i < len && i < 2; i++)
    			sprintf(str, "%s %02X", str, data[i]);
			sprintf(str, "%s Reason =", str);
			for(i = 2; i < len && i < 2+1; i++)
    			sprintf(str, "%s %s(%02X)", str, pHC_Status(data[i]), data[i]);
			break;
	    default:
			if(len > 0)
		        sprintf(str, "%s Parms=", str);
			else
				sprintf(str, "%s No Parms", str);

			for(i = 0; i < len ; i++)
				sprintf(str, "%s %02x", str, data[i]);				
		break;
	}
		
}

void CMassStorageUartTesterDlg::ParseData(char *str, U16 handle, U16 len, U8 *data)
{
    sprintf(str, "%s Handle = %04X Len = %04d Data=", str, handle, len);
	
	for(int i = 0; i < len ; i++)
		sprintf(str, "%s %02X", str, data[i]);
	
}

void CMassStorageUartTesterDlg::ParseEvent(char *str, U8 event, U8 len, U8 *data)
{
	U8 i;
	sprintf(str, "%s %s", str, pHciEvent(event));

	switch(event)
	{
	    case HCE_COMMAND_COMPLETE:
			sprintf(str, "%s #Packet=%02d Op=%02x%02x", str, data[0], data[1], data[2]);
			if(len > 3)
		    {
		        sprintf(str, "%s Data=", str);
			    for(i = 3; i < len; i++)
				    sprintf(str, "%s %02x", str, data[i]);
		    }
			break;
		case HCE_COMMAND_STATUS:
			sprintf(str, "%s Sts = %s(%02x) #Packet=%02d Op=%02x%02x", str, pHC_Status(data[0]), data[0], data[1], data[2], data[3]);
			if(len > 4)
		    {
		        sprintf(str, "%s Data=", str);
			    for(i = 4; i < len; i++)
				    sprintf(str, "%s %02x", str, data[i]);
		    }
			break;
		case HCE_CONNECT_COMPLETE:
			sprintf(str, "%s Sts = %s(%02x) ConnectHandle=%02x%02x BDADDR =", str, 
				pHC_Status(data[0]), data[0], data[1], data[2]);
			for(i = 0; i < 6 ; i++)
				sprintf(str, "%s %02x", str, data[3+i]);
			if(len > 9)
		    {
		        sprintf(str, "%s Data=", str);
			    for(i = 9; i < len; i++)
				    sprintf(str, "%s %02x", str, data[i]);
		    }
			break;
		default:
	        sprintf(str, "%s Data=", str);
			for(i = 0; i < len; i++)
				 sprintf(str, "%s %02x", str, data[i]);
		break;
	}
}

void CMassStorageUartTesterDlg::UpdateHCIDebugUI(void)
{
    char buf[1000];

    if(theApp.HCCExecuteState == HCC_SEND)
	{
	    sprintf(buf, " CMD\t:");
	    ParseCmd(buf, theApp.HCCCMD, theApp.HCCCMDLen, theApp.HCCCMDData);
		m_list_msg.AddString(buf);
	}
	
    if(theApp.bHCCEventRecv)
    {
	    sprintf(buf, "EVENT\t:");
    	ParseEvent(buf, theApp.HCCEvent, theApp.HCCEventLen, theApp.HCCEventData);
		m_list_msg.AddString(buf);
    }

	if(theApp.bHCDRecv)
	{
	    sprintf(buf, "DATA IN\t:");
		ParseData(buf, theApp.HCDHandle, theApp.HCDLen,theApp.HCDData);
		m_list_msg.AddString(buf);
	}
}

void CMassStorageUartTesterDlg::UpdateACLUI(void)
{
    U8 i,j;
    char buf[50];
	if(theApp.bHCCEventRecv == TRUE)
	{
	    switch(theApp.HCCEvent)
    	{
    	    case HCE_CONNECT_COMPLETE:
				if(theApp.HCCEventData[0] == 0x00) //status success only
				{
				    for(j = 0; j < 4; j++)
			    	{
			    	    if(theApp.bACLConnected[j] == FALSE)
			    	    {
							//ID
							theApp.ACLHandleID[j] = (theApp.HCCEventData[2] << 8) + theApp.HCCEventData[1];							
							sprintf(buf, "%04X", theApp.ACLHandleID[j]); 
		                	this->GetDlgItem(IDC_EDIT_ACL1ID+(3*j))->SetWindowTextA(buf);

							//address connected
							sprintf(buf, "");
							for(i = 0; i < 6 ; i++)
								sprintf(buf, "%s%02x ", buf, theApp.HCCEventData[3+i]);
							sprintf(buf, "%s Connected.", buf);

							this->GetDlgItem(IDC_STATIC_ACL1STS+(3*j))->SetWindowTextA(buf);
							this->GetDlgItem(IDC_BUTTON_ACL1+(3*j))->EnableWindow(TRUE);
							if(j == 0) //only allow one test button now
							this->GetDlgItem(IDC_BUTTON_ACLTEST1+j)->EnableWindow(TRUE);
							theApp.bACLConnected[j] = TRUE;
							break;
			    	    }
			    	}
						
				}
				break;
			case HCE_DISCONNECT_COMPLETE:
		    {
				if(theApp.HCCEventData[0] == 0x00) //status success only
				{
				    for(j = 0; j < 4; j++)
			    	{
			    	    if(theApp.bACLConnected[j] == TRUE && (theApp.ACLHandleID[j] == ((theApp.HCCEventData[2] << 8) + theApp.HCCEventData[1])))
			    	    {
							//ID
							theApp.ACLHandleID[j] = (theApp.HCCEventData[2] << 8) + theApp.HCCEventData[1];							
							sprintf(buf, "%04X", theApp.ACLHandleID[j]); 
		                	this->GetDlgItem(IDC_EDIT_ACL1ID+(3*j))->SetWindowTextA(buf);

							this->GetDlgItem(IDC_STATIC_ACL1STS+(3*j))->SetWindowTextA("Disconnected.");
							this->GetDlgItem(IDC_BUTTON_ACL1+(3*j))->EnableWindow(FALSE);
							if(j==0)
							this->GetDlgItem(IDC_BUTTON_ACLTEST1+j)->EnableWindow(TRUE);
							theApp.bACLConnected[j] = FALSE;
							break;
			    	    }
			    	}
						
				}
				break;				
			}
    	}
	}

	if(theApp.HCDTestState == 1) //data sent
	{
	    this->GetDlgItem(IDC_BUTTON_ACLTEST1)->EnableWindow(FALSE);
	}
	else if(theApp.HCDTestState == 0 || theApp.HCDTestState == 3 || theApp.HCDTestState == 4 || theApp.HCDTestState == 5)
	{
        this->GetDlgItem(IDC_BUTTON_ACLTEST1)->EnableWindow(TRUE);

		if(theApp.HCDTestState == 3)
    	{
		    MessageBox("Send test packet fail!", "Error");
    	}
	    else if(theApp.HCDTestState == 4)
    	{
		    MessageBox("Test Pass!", "Congradulation");
    	}
	    else if(theApp.HCDTestState == 5)
    	{
		    MessageBox("Test Fail!", "Error");
    	}
	}
}

void CMassStorageUartTesterDlg::UpdateHCICommandUI(void)
{
    char buf[500];
	int permit_len;
	int len;
	
	permit_len = theApp.m_thcf_cursel ? theApp.m_thcf_cursel->parmlen : 0xff/*on purpose*/;
    len = m_edit_hci_command_data.GetWindowTextLengthA()/2;
	
	if(theApp.m_thcf_cursel)
	    sprintf(buf, "len = %2d/%2d", len, permit_len);
	else
    	sprintf(buf, "len = ?/?");	

	//Update length static
	this->GetDlgItem(IDC_STATIC_HCICOMMANDLEN)->SetWindowTextA(buf);

	switch(theApp.HCCExecuteState)
	{
	    case HCC_IDLE:
  			/*Update Command Param Static*/
            if(theApp.m_thcf_cursel)
        	{
        	    if(theApp.m_thcf_cursel->parmlen)
    	    	{
				    char text[1024];
				    U8 hex;	

				    theApp.HCCCMD = theApp.m_thcf_cursel->op;
					theApp.HCCCMDLen = len;

				    m_edit_hci_command_data.GetWindowTextA(text, 1024);

				    for(int i = 0; i < len*2; i+=2)
				    {
				        toHex(text[i], text[i+1], &hex);    
				        theApp.HCCCMDData[i/2] = hex;
				    }
					
				    sprintf(buf, "");     	    	
    	    	    ParseCmd(buf, theApp.HCCCMD, theApp.HCCCMDLen, theApp.HCCCMDData);
					this->GetDlgItem(IDC_STATIC_HCC_PARM)->SetWindowTextA(buf);
    	    	}
				else
				{
				    this->GetDlgItem(IDC_STATIC_HCC_PARM)->SetWindowTextA("No Params");
				}
        	}
			break;
	    case HCC_SEND:
			/*Update Event Statics*/
			if(theApp.m_thcf_cursel->event_start != 0xff)
				//need start event
			    theApp.HCCExecuteState = HCC_WAIT_START_EVENT;
			else
			{
			    
            	this->GetDlgItem(IDC_STATIC_HCICOMMANDSTARTEVENT)->SetWindowTextA("No Start Event");

			    if(theApp.m_thcf_cursel->event_end != 0xff)
			    {
			        char buf[80];
					sprintf(buf, "Wait %s Event", pHciEvent(theApp.m_thcf_cursel->event_end));
	            	this->GetDlgItem(IDC_STATIC_HCICOMMANDENDEVENT)->SetWindowTextA(buf);
				    theApp.HCCExecuteState = HCC_WAIT_END_EVENT;
			    }
				else
				{
	            	this->GetDlgItem(IDC_STATIC_HCICOMMANDENDEVENT)->SetWindowTextA("No End Event");
					theApp.HCCExecuteState = HCC_IDLE;
				}
			}
			break;
	    case HCC_WAIT_START_EVENT:
		{ 
	        char buf[500];
			if(theApp.bHCCEventRecv == TRUE)
			{
			    sprintf(buf, "");
				ParseEvent(buf, theApp.HCCEvent, theApp.HCCEventLen, theApp.HCCEventData);

    			/*Update Event Statics*/
			    if(theApp.m_thcf_cursel->event_start == theApp.HCCEvent)
			    {
			        
	        	    this->GetDlgItem(IDC_STATIC_HCICOMMANDSTARTEVENT)->SetWindowTextA(buf);
				    theApp.HCCExecuteState = HCC_IDLE;
				    if(theApp.m_thcf_cursel->event_start != 0xff)
				    {
				        if(theApp.HCCEvent == HCE_COMMAND_STATUS)
				        {
				            if(theApp.HCCEventData[0] == 0x00)//command pending
							    theApp.HCCExecuteState = HCC_WAIT_END_EVENT;
							//else, HCC_IDLE
				        }
					    else
						    theApp.HCCExecuteState = HCC_WAIT_END_EVENT;
				    }
			    }
			}

			break;
    	}
	    case HCC_WAIT_END_EVENT:
		{ 
	        char buf[500];
			if(theApp.bHCCEventRecv == TRUE)
			{
    			/*Update Event Statics*/
			    sprintf(buf, "");
				ParseEvent(buf, theApp.HCCEvent, theApp.HCCEventLen, theApp.HCCEventData);

			    if(theApp.m_thcf_cursel->event_end == theApp.HCCEvent)
			    {
				    theApp.HCCExecuteState = HCC_IDLE;
	        	    this->GetDlgItem(IDC_STATIC_HCICOMMANDENDEVENT)->SetWindowTextA(buf);
			    }
			}

			break;
    	}
	}

	//disable or enable UI
    this->GetDlgItem(IDC_EDIT_HCICOMMANDDATA)->EnableWindow((theApp.HCCExecuteState == HCC_IDLE));
    this->GetDlgItem(IDC_STATIC_HCICOMMANDLEN)->EnableWindow((theApp.HCCExecuteState == HCC_IDLE));
    this->GetDlgItem(IDC_COMBO_HCICOMMAND)->EnableWindow((theApp.HCCExecuteState == HCC_IDLE));
    this->GetDlgItem(IDC_BUTTON1)->EnableWindow((permit_len == len) && (theApp.HCCExecuteState == HCC_IDLE));

}

void CMassStorageUartTesterDlg::OnBnClickedButtonSetaddr()
{
    U8 buf[8];
    char text[20];

    m_edit_bdaddr.GetWindowTextA(text, 20);
	
	buf[0] = 0xfe;//bd_user_id
	buf[1] = 0x06;//addr
	
    for(int i = 0; i < 6*2; i+=2)
        toHex(text[i], text[i+1], &buf[2+i/2]);    
	
	// TODO: Add your control notification handler code here
	SendHCICmd(HCC_ERI_ADDRESS, 8, buf);
	theApp.SettingExecuteState = 1; //set address
	UpdateSettingUI();
}

void CMassStorageUartTesterDlg::OnEnUpdateEditBdaddr()
{
    static char save_data[12] = {0};
    char new_data[13];
    int len, i;

    m_edit_bdaddr.GetWindowTextA(new_data, 20);
	
    len = m_edit_bdaddr.GetWindowTextLengthA();
    
    if(len > 6*2)
    {
        m_edit_bdaddr.SetWindowTextA(save_data);
    }
    else     
    {
        for(i = 0; i < len ; i++)
        {
            if(!isxdigit(new_data[i]))
            {
                m_edit_bdaddr.SetWindowTextA(save_data);
                break;
            }
        }

        if(i == len && len)
            strcpy(save_data, new_data);  
    }

	UpdateSettingUI();
}

void CMassStorageUartTesterDlg::UpdateSettingUI(void)
{
    this->GetDlgItem(IDC_BUTTON_SETADDR)->EnableWindow(FALSE);
    this->GetDlgItem(IDC_BUTTON_PageScan)->EnableWindow(FALSE);
    this->GetDlgItem(IDC_BUTTON_AUTO_ACCEPT)->EnableWindow(FALSE);
	
    switch(theApp.SettingExecuteState)
	{
	    case 0: //idle
       	    goto back2idle;
	        break;
	    case 1: //set address
	        if(theApp.bHCCEventRecv == TRUE && theApp.HCCEvent == HCE_COMMAND_COMPLETE)
        	{
        	    if((theApp.HCCEventData[2] << 8) + theApp.HCCEventData[1] == HCC_ERI_ADDRESS)
        	    {//op code match
        	        if(theApp.HCCEventData[3] == 0x00) //success
    	        	{
    	        	   //send reset
    	        	   SendHCICmd(HCC_RESET, 0, NULL);
					   theApp.SettingExecuteState = 2;
    	        	}
        	    }
        	}
	        break;
	    case 2: //reset
	        if(theApp.bHCCEventRecv == TRUE && theApp.HCCEvent == HCE_COMMAND_COMPLETE)
        	{
        	    if((theApp.HCCEventData[2] << 8) + theApp.HCCEventData[1] == HCC_RESET)
        	    {//op code match
        	        if(theApp.HCCEventData[3] == 0x00) //success
    	        	{
    	        	    goto back2idle;
					}
        	    }
        	}
	        break;
        case 3: //page scan enable
            if(theApp.bHCCEventRecv == TRUE && theApp.HCCEvent == HCE_COMMAND_COMPLETE)
        	{
        	    if((theApp.HCCEventData[2] << 8) + theApp.HCCEventData[1] == HCC_WRITE_SCAN_ENABLE)
        	    {//op code match
        	        if(theApp.HCCEventData[3] == 0x00) //success
    	        	{
					   char buf[50];
					   this->GetDlgItem(IDC_BUTTON_PageScan)->GetWindowTextA(buf, 50);
					   if(!strcmp(buf, "Page Scan Enable"))
    					   this->GetDlgItem(IDC_BUTTON_PageScan)->SetWindowTextA("Page Scan Disable");
					   else
                           this->GetDlgItem(IDC_BUTTON_PageScan)->SetWindowTextA("Page Scan Enable");
    	        	    goto back2idle;
    	        	}
        	    }
        	    
        	}
			break;
		case 4: //set event filter
            if(theApp.bHCCEventRecv == TRUE && theApp.HCCEvent == HCE_COMMAND_COMPLETE)
        	{
        	    if((theApp.HCCEventData[2] << 8) + theApp.HCCEventData[1] == HCC_EVENT_FILTER)
        	    {//op code match
        	        if(theApp.HCCEventData[3] == 0x00) //success
    	        	{
    	        	    goto back2idle;
    	        	}
        	    }
            }
			break;
	}
	return;

back2idle:
	   theApp.SettingExecuteState = 0;
       this->GetDlgItem(IDC_BUTTON_SETADDR)->EnableWindow(m_edit_bdaddr.GetWindowTextLengthA()/2 == 6);
       this->GetDlgItem(IDC_BUTTON_PageScan)->EnableWindow(TRUE);
       this->GetDlgItem(IDC_BUTTON_AUTO_ACCEPT)->EnableWindow(m_edit_bdaddr_accept.GetWindowTextLengthA()/2 == 6);
	return;
}



void CMassStorageUartTesterDlg::OnBnClickedButtonPagescan()
{
    char buf[50];
	U8 parm;
	// TODO: Add your control notification handler code here
	this->GetDlgItem(IDC_BUTTON_PageScan)->GetWindowTextA(buf, 50);
	
	if(!strcmp(buf, "Page Scan Enable"))
		parm = 0x02;
	else
		parm = 0x00;
	
	SendHCICmd(HCC_WRITE_SCAN_ENABLE, 1, &parm);

	theApp.SettingExecuteState = 3;
	UpdateSettingUI();
}

void CMassStorageUartTesterDlg::OnBnClickedButtonAutoAccept()
{
    U8 parm[9];
	char text[20];

    m_edit_bdaddr_accept.GetWindowTextA(text, 20);

	// TODO: Add your control notification handler code here

	#if 0
	parm[0] = 0x02; //connection setup
	parm[1] = 0x02; //Allow Connections from a device with a specific BD_ADDR
    for(int i = 0; i < 6*2; i+=2)
        toHex(text[i], text[i+1], &parm[2+i/2]);    
    parm[8] = 0x02;//do auto accept without role-switch
	SendHCICmd(HCC_EVENT_FILTER, 9, parm);
	#else
	parm[0] = 0x02; //connection setup
	parm[1] = 0x00; //Allow Connections
    parm[2] = 0x02;//do auto accept without role-switch
	SendHCICmd(HCC_EVENT_FILTER, 3, parm);
	#endif
	theApp.SettingExecuteState = 4;
	UpdateSettingUI();
}

void CMassStorageUartTesterDlg::OnEnUpdateEditBdaddrAccept()
{
    static char save_data[12] = {0};
    char new_data[13];
    int len, i;

    m_edit_bdaddr_accept.GetWindowTextA(new_data, 20);
	
    len = m_edit_bdaddr_accept.GetWindowTextLengthA();
    
    if(len > 6*2)
    {
        m_edit_bdaddr_accept.SetWindowTextA(save_data);
    }
    else     
    {
        for(i = 0; i < len ; i++)
        {
            if(!isxdigit(new_data[i]))
            {
                m_edit_bdaddr_accept.SetWindowTextA(save_data);
                break;
            }
        }

        if(i == len && len)
            strcpy(save_data, new_data);  
    }

	UpdateSettingUI();
}

void CMassStorageUartTesterDlg::OnBnClickedButtonAcl1()
{
    U8 parm[3];
	// TODO: Add your control notification handler code here
	parm[1] = theApp.ACLHandleID[0] >> 8;
	parm[0] = theApp.ACLHandleID[0] & 0xff;
	parm[2] = 0x13;
	SendHCICmd(HCC_DISCONNECT, 3, parm);
}

void CMassStorageUartTesterDlg::OnBnClickedButtonAcl2()
{
	// TODO: Add your control notification handler code here
    U8 parm[3];
	// TODO: Add your control notification handler code here
	parm[1] = theApp.ACLHandleID[1] >> 8;
	parm[0] = theApp.ACLHandleID[1] & 0xff;
	parm[2] = 0x13;
	SendHCICmd(HCC_DISCONNECT, 3, parm);
}

void CMassStorageUartTesterDlg::OnBnClickedButtonAcl3()
{
	// TODO: Add your control notification handler code here
    U8 parm[3];
	// TODO: Add your control notification handler code here
	parm[1] = theApp.ACLHandleID[2] >> 8;
	parm[0] = theApp.ACLHandleID[2] & 0xff;
	parm[2] = 0x13;
	SendHCICmd(HCC_DISCONNECT, 3, parm);
}

void CMassStorageUartTesterDlg::OnBnClickedButtonAcl4()
{
	// TODO: Add your control notification handler code here
    U8 parm[3];
	// TODO: Add your control notification handler code here
	parm[1] = theApp.ACLHandleID[3] >> 8;
	parm[0] = theApp.ACLHandleID[3] & 0xff;
	parm[2] = 0x13;
	SendHCICmd(HCC_DISCONNECT, 3, parm);
}

#define CMD_NOP 0x00
#define CMD_ECHO 0x01
#define CMD_ECHOBACK 0x02

#define TIMER_WAIT_ECHOBACK 1000
#define TIMER_FAIL 1001
#define TIMER_SUCCESS 1002

void DataCallBack(CWnd *cwnd, U8 op)
{	
	if(theApp.HCDTestState == 0)
	{
	    if(op == DATACB_DATA_RECV)
    	{
			//check if echo command received, if yes we send back the data
			if(theApp.bHCDRecv == TRUE) //double check
			{
				if(theApp.HCDLen == theApp.HCDData[0] + (theApp.HCDData[1] << 8))
				{
					if(theApp.HCDData[2] == CMD_ECHO)
			       	{
						theApp.bHCDSend = TRUE;;
						theApp.HCDSendHandle = theApp.ACLHandleID[0];

						theApp.HCDSendData[0] = theApp.HCDData[0];
						theApp.HCDSendData[1] = theApp.HCDData[1];
						theApp.HCDSendData[2] = CMD_ECHOBACK;
						theApp.HCDSendData[3] = 0x00;
						for(int i = 0; i < (theApp.HCDData[0] + (theApp.HCDData[1] << 8)) - 4 ; i++)
						{
			    			theApp.HCDSendData[i+3] = theApp.HCDData[i+3];
						}
						theApp.HCDSendPacket.dataLen = 0xe4;

						theApp.HCDSendPacket.data = theApp.HCDSendData;
						theApp.HCDSendPacket.headerLen = 0;
						theApp.HCDSendPacket.tailLen = 0;	 
						Assert(HCI_RequestToSend(theApp.ACLHandleID[0])== BT_STATUS_SUCCESS);
			       	}
				}
			}   	   
    	}
	    else if(op == DATACB_START_TEST)//start test
    	{
			theApp.bHCDSend = TRUE;;
			theApp.HCDSendHandle = theApp.ACLHandleID[0];

			theApp.HCDSendData[0] = 0xe4; //len
			theApp.HCDSendData[1] = 0x00;
			theApp.HCDSendData[2] = CMD_ECHO;
			theApp.HCDSendData[3] = 0x00;
			for(int i = 0; i < 0xe0 ; i++)
			{
    			theApp.HCDSendData[i+3] = i;
			}
			theApp.HCDSendPacket.dataLen = 0xe4;

			theApp.HCDSendPacket.data = theApp.HCDSendData;
			theApp.HCDSendPacket.headerLen = 0;
			theApp.HCDSendPacket.tailLen = 0;	 
			Assert(HCI_RequestToSend(theApp.ACLHandleID[0])== BT_STATUS_SUCCESS);
			theApp.HCDTestState = 1;
    	}
	}
	else if(theApp.HCDTestState == 1) //echo sent
	{
	    if(op == DATACB_DATA_SENT_SUCCESS)
    	{
    	    theApp.HCDTestState = 2; //wait echoback
    	    //start timer to check if receieve echo back
			cwnd->SetTimer(TIMER_WAIT_ECHOBACK, 5000, 0);
    	}
		else if(op == DATACB_DATA_SENT_FAIL)
		{
		    //send fail
		    theApp.HCDTestState = 3;
			//start timer to clear fail state
			cwnd->SetTimer(TIMER_FAIL, 100, 0);
		}
	}
	else if(theApp.HCDTestState == 2) //wait echo back
	{
	    if(op == DATACB_DATA_RECV)
	    {
	        //check if echo back	        
		    theApp.HCDTestState = 4;
			//start timer to clear success state
			cwnd->SetTimer(TIMER_SUCCESS, 100, 0);
	    }
		else if(op == DATACB_TIMEOUT)
		{
			theApp.HCDTestState = 5;//wait data fail, timout
			//start timer to clear fail state
			cwnd->SetTimer(TIMER_FAIL, 100, 0);
		}
	}
	else if(theApp.HCDTestState == 3 || theApp.HCDTestState == 4 || theApp.HCDTestState == 5)
	{
	    if(op == DATACB_TIMEOUT)
    	{
    	    //back to idle
    	    theApp.HCDTestState = 0;
    	}
	}	

}

void CMassStorageUartTesterDlg::OnBnClickedButtonAcltest1()
{
	// TODO: Add your control notification handler code here
	DataCallBack(mycwnd, 1); //start test
	UpdateACLUI();
}


void CMassStorageUartTesterDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: Add your message handler code here and/or call default

	CDialog::OnTimer(nIDEvent);
	switch(nIDEvent)
	{
	    case TIMER_WAIT_ECHOBACK:
        	this->KillTimer(nIDEvent);
        	DataCallBack(this, DATACB_TIMEOUT); //change to error state
			break;;
	    case TIMER_FAIL:
			this->KillTimer(nIDEvent);
			UpdateACLUI(); //show error
			DataCallBack(this, DATACB_TIMEOUT); //back to 0
			break;
	    case TIMER_SUCCESS:
			this->KillTimer(nIDEvent);
			DataCallBack(this, DATACB_TIMEOUT); //back to 0
			break;			
	}
}
