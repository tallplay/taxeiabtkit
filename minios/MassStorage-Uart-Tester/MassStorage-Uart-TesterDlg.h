// MassStorage-Uart-TesterDlg.h : header file
//

#pragma once
#include "afxwin.h"


// CMassStorageUartTesterDlg dialog
class CMassStorageUartTesterDlg : public CDialog
{
// Construction
public:
	CMassStorageUartTesterDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_MASSSTORAGEUARTTESTER_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedCancel();

afx_msg void OnBnClickedButton1();
CComboBox m_combo_hci_command;
CEdit m_edit_hci_command_data;
afx_msg void OnCbnSelchangeComboHcicommand();
afx_msg void OnEnUpdateEditHcicommanddata();
void UpdateHCICommandUI(void);
void UpdateHCIDebugUI(void);
void UpdateACLUI(void);
void UpdateSettingUI(void);
void ParseData(char *str, U16 handle, U16 len, U8 *data);
void ParseEvent(char *str, U8 event, U8 len, U8 *data);
void ParseCmd(char *str, U16 cmd, U8 len, U8 *data);
void SendHCICmd(U16 op, U8 parmlen, U8 *parm);

CListBox m_list_msg;
afx_msg void OnBnClickedButtonSetaddr();
afx_msg void OnEnUpdateEditBdaddr();
CEdit m_edit_bdaddr;
afx_msg void OnBnClickedButtonPagescan();
CEdit m_edit_bdaddr_accept;
afx_msg void OnBnClickedButtonAutoAccept();
afx_msg void OnEnUpdateEditBdaddrAccept();
afx_msg void OnBnClickedButtonAcl1();
afx_msg void OnBnClickedButtonAcl2();
afx_msg void OnBnClickedButtonAcl3();
afx_msg void OnBnClickedButtonAcl4();
afx_msg void OnBnClickedButtonAcltest1();
afx_msg void OnTimer(UINT_PTR nIDEvent);
};
