#include "DataLink.h"

HCI_HCEHandler eventHandler;
HCI_HCDHandler dataHandler;
HCI_DataTxReadyHandler datarHandler;
HCI_DataTxDone datadHandler;
U16 CurTask; //opcode

static T_HCI_Command_Format hci_commands[]=
{
    {HCC_INQUIRY                      ,0x05, 0xff, 0xff, HCE_COMMAND_STATUS/*, HCE_INQUIRY_RESULT, HCE_INQUIRY_COMPLETE*/}
   ,{HCC_INQUIRY_CANCEL               ,0x00, 0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_START_PERIODIC_INQ_MODE      ,0x08, 0xff, 0xff, HCE_COMMAND_COMPLETE/*, HCE_INQUIRY_RESULT, HCE_INQUIRY_COMPLETE*/}
   ,{HCC_EXIT_PERIODIC_INQ_MODE       ,0x00, 0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_CREATE_CONNECTION            ,0x0d, 0xff, 0xff, HCE_COMMAND_STATUS/*, 0xff, HCE_CONNECT_COMPLETE*/}
   ,{HCC_DISCONNECT                   ,0x03, 0xff, 0xff, HCE_COMMAND_STATUS/*, 0xff, HCE_DI;SCONNECT_COMPLETE*/}
   ,{HCC_ADD_SCO_CONNECTION           ,0x00, 0xff, 0xff, HCE_COMMAND_COMPLETE} /* Not in 1.2 */
   ,{HCC_CREATE_CONNECTION_CANCEL     ,0x06, 0xff, 0xff, HCE_COMMAND_COMPLETE} /* 1.2 */
   ,{HCC_ACCEPT_CON_REQ               ,0x07, 0xff, 0xff, HCE_COMMAND_STATUS/*, 0xff, HCE_CONNECT_COMPLETE*/}
   ,{HCC_REJECT_CON_REQ               ,0x07, 0xff, 0xff, HCE_COMMAND_STATUS/*, 0xff, HCE_CONNECT_COMPLETE*/}
   ,{HCC_LINK_KEY_REQ_REPL            ,0x16, 0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_LINK_KEY_REQ_NEG_REPL        ,0x06, 0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_PIN_CODE_REQ_REPL            ,0x17, 0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_PIN_CODE_REQ_NEG_REPL        ,0x06, 0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_CHNG_CONN_PACKET_TYPE        ,0x04, 0xff, 0xff, HCE_COMMAND_STATUS/*, 0xff, HCE_CONN_PACKET_TYPE_CHNG*/}
   ,{HCC_AUTH_REQ                     ,0x02, 0xff, 0xff, HCE_COMMAND_STATUS/*, 0xff, HCE_AUTH_COMPLETE*/}
   ,{HCC_SET_CONN_ENCRYPT             ,0x03, 0xff, 0xff, HCE_COMMAND_STATUS/*, 0xff, HCE_ENCRYPT_CHNG*/}
   ,{HCC_CHNG_CONN_LINK_KEY           ,0x02, 0xff, 0xff, HCE_COMMAND_STATUS/*, 0xff, HCE_CHNG_CONN_LINK_KEY_COMPLETE*/}
   ,{HCC_MASTER_LINK_KEY              ,0x01, 0xff, 0xff, HCE_COMMAND_STATUS/*, 0xff, HCE_MASTER_LINK_KEY_COMPLETE*/}
   ,{HCC_REM_NAME_REQ                 ,0x0a, 0xff, 0xff, HCE_COMMAND_STATUS/*, 0xff, HCE_REMOTE_NAME_REQ_COMPLETE*/}
   ,{HCC_REM_NAME_REQ_CANCEL          ,0x06, 0xff, 0xff, HCE_COMMAND_COMPLETE} /* 1.2 */
   ,{HCC_READ_REMOTE_FEATURES         ,0x02, 0xff, 0xff, HCE_COMMAND_STATUS/*, 0xff, HCE_READ_REMOTE_FEATURES_COMPLETE*/}
   ,{HCC_READ_REMOTE_EXT_FEATURES     ,0x03, 0xff, 0xff, HCE_COMMAND_STATUS/*, 0xff, HCE_READ_REMOTE_EXT_FEAT_COMPLETE*/} /* 1.2 */
   ,{HCC_READ_REMOTE_VERSION          ,0x02, 0xff, 0xff, HCE_COMMAND_STATUS/*, 0xff, HCE_READ_CLOCK_OFFSET_COMPLETE*/}
   ,{HCC_READ_CLOCK_OFFSET            ,0x02, 0xff, 0xff, HCE_COMMAND_STATUS/*, 0xff, HCE_COMMAND_COMPLETE*/}
   ,{HCC_READ_LMP_HANDLE              ,0x02, 0xff, 0xff, HCE_COMMAND_COMPLETE} /* 1.2 */
   ,{HCC_SETUP_SYNC_CONNECTION        ,0x11, 0xff, 0xff, HCC_EXCHANGE_FIXED_INFO/*, 0xff, HCE_SYNC_CONNECT_COMPLETE*/} /* 1.2 */
   ,{HCC_ACCEPT_SYNC_CON_REQ          ,0x15, 0xff, 0xff, HCE_COMMAND_STATUS/*, 0xff, HCE_SYNC_CONNECT_COMPLETE*/} /* 1.2 */
   ,{HCC_REJECT_SYNC_CON_REQ          ,0x07, 0xff, 0xff, HCE_COMMAND_STATUS/*, 0xff, HCE_SYNC_CONNECT_COMPLETE*/} /* 1.2 */
                                       
/* Group: Link policy commands */             
   ,{HCC_HOLD_MODE                    ,0x06, 0xff, 0xff, HCE_COMMAND_STATUS/*, 0xff, HCE_MODE_CHNG*/}
   ,{HCC_SNIFF_MODE                   ,0x0a, 0xff, 0xff, HCE_COMMAND_STATUS/*, 0xff, HCE_MODE_CHNG*/}
   ,{HCC_EXIT_SNIFF_MODE              ,0x02, 0xff, 0xff, HCE_COMMAND_STATUS/*, 0xff, HCE_MODE_CHNG*/}
   ,{HCC_PARK_MODE                    ,0x06, 0xff, 0xff, HCE_COMMAND_STATUS/*, 0xff, HCE_MODE_CHNG*/}
   ,{HCC_EXIT_PARK_MODE               ,0x02, 0xff, 0xff, HCE_COMMAND_STATUS/*, 0xff, HCE_MODE_CHNG*/}
   ,{HCC_QOS_SETUP                    ,0x14, 0xff, 0xff, HCE_COMMAND_STATUS/*, 0xff, HCE_QOS_SETUP_COMPLETE*/}
   ,{HCC_ROLE_DISCOVERY               ,0x02, 0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_SWITCH_ROLE                  ,0x07, 0xff, 0xff, HCE_COMMAND_STATUS/*, 0xff, HCE_ROLE_CHANGE*/}
   ,{HCC_READ_LINK_POLICY             ,0x02, 0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_WRITE_LINK_POLICY            ,0x04, 0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_READ_DEFAULT_LINK_POLICY     ,0x00, 0xff, 0xff, HCE_COMMAND_COMPLETE} /* 1.2 */
   ,{HCC_WRITE_DEFAULT_LINK_POLICY    ,0x02, 0xff, 0xff, HCE_COMMAND_COMPLETE} /* 1.2 */
   ,{HCC_FLOW_SPECIFICATION           ,0x15, 0xff, 0xff, HCE_COMMAND_STATUS/*, 0xff, HCE_FLOW_SPECIFICATION_COMPLETE*/} /* 1.2 */

/* Group: Host controller and baseband commands */
   ,{HCC_SET_EVENT_MASK               ,0x08, 0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_RESET                        ,0x00, 0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_EVENT_FILTER                 ,0x09/*or else*/, 0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_FLUSH                        ,0x02, 0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_READ_PIN_TYPE                ,0x00, 0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_WRITE_PIN_TYPE               ,0x01, 0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_CREATE_NEW_UNIT_KEY          ,0x00, 0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_READ_STORED_LINK_KEY         ,0x07, 0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_WRITE_STORED_LINK_KEY        ,0x17/*or+22n*/, 0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_DEL_STORED_LINK_KEY          ,0x07, 0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_CHNG_LOCAL_NAME              ,248,  0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_READ_LOCAL_NAME              ,0x00, 0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_READ_CONN_ACCEPT_TIMEOUT     ,0x00, 0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_WRITE_CONN_ACCEPT_TIMEOUT    ,0x02, 0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_READ_PAGE_TIMEOUT            ,0x00, 0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_WRITE_PAGE_TIMEOUT           ,0x02, 0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_READ_SCAN_ENABLE             ,0x00, 0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_WRITE_SCAN_ENABLE            ,0x01, 0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_READ_PAGE_SCAN_ACTIVITY      ,0x00, 0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_WRITE_PAGE_SCAN_ACTIVITY     ,0x04, 0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_READ_INQ_SCAN_ACTIVITY       ,0x00, 0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_WRITE_INQ_SCAN_ACTIVITY      ,0x04, 0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_READ_AUTH_ENABLE             ,0x00, 0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_WRITE_AUTH_ENABLE            ,0x01, 0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_READ_CLASS_OF_DEVICE         ,0x00, 0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_WRITE_CLASS_OF_DEVICE        ,0x03, 0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_READ_VOICE_SETTING           ,0x00, 0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_WRITE_VOICE_SETTING          ,0x02, 0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_READ_AUTO_FLUSH_TIMEOUT      ,0x02, 0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_WRITE_AUTO_FLUSH_TIMEOUT     ,0x04, 0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_READ_NUM_BC_RETRANSMIT       ,0x00, 0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_WRITE_NUM_BC_RETRANSMIT      ,0x01, 0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_READ_HOLD_MODE_ACTIVITY      ,0x00, 0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_WRITE_HOLD_MODE_ACTIVITY     ,0x01, 0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_READ_XMIT_POWER_LEVEL        ,0x03, 0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_READ_SCO_FC_ENABLE           ,0x00, 0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_WRITE_SCO_FC_ENABLE          ,0x01, 0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_SET_CTRLR_TO_HOST_FLOW_CTRL  ,0x01, 0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_HOST_BUFFER_SIZE             ,0x07, 0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_HOST_NUM_COMPLETED_PACKETS   ,0x05/*or+4n*/, 0xff, 0xff, 0xff}
   ,{HCC_READ_LINK_SUPERV_TIMEOUT     ,0x02, 0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_WRITE_LINK_SUPERV_TIMEOUT    ,0x04, 0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_READ_NUM_IAC                 ,0x00, 0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_READ_CURRENT_IAC_LAP         ,0x00, 0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_WRITE_CURRENT_IAC_LAP        ,0x04/*or+3n*/, 0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_SET_AFH_HOST_CHAN_CLASS      ,0x0a, 0xff, 0xff, HCE_COMMAND_COMPLETE} /* 1.2 */
   ,{HCC_READ_INQ_SCAN_TYPE           ,0x00, 0xff, 0xff, HCE_COMMAND_COMPLETE} /* 1.2 */
   ,{HCC_WRITE_INQ_SCAN_TYPE          ,0x01, 0xff, 0xff, HCE_COMMAND_COMPLETE} /* 1.2 */
   ,{HCC_READ_INQ_MODE                ,0x00, 0xff, 0xff, HCE_COMMAND_COMPLETE} /* 1.2 */
   ,{HCC_WRITE_INQ_MODE               ,0x01, 0xff, 0xff, HCE_COMMAND_COMPLETE} /* 1.2 */
   ,{HCC_READ_PAGE_SCAN_TYPE          ,0x00, 0xff, 0xff, HCE_COMMAND_COMPLETE} /* 1.2 */
   ,{HCC_WRITE_PAGE_SCAN_TYPE         ,0x01, 0xff, 0xff, HCE_COMMAND_COMPLETE} /* 1.2 */
   ,{HCC_READ_AFH_CHAN_ASSESS_MODE    ,0x00, 0xff, 0xff, HCE_COMMAND_COMPLETE} /* 1.2 */
   ,{HCC_WRITE_AFH_CHAN_ASSESS_MODE   ,0x01, 0xff, 0xff, HCE_COMMAND_COMPLETE} /* 1.2 */

/* Group: Informational parameters */
   ,{HCC_READ_LOCAL_VERSION           ,0x00, 0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_READ_LOCAL_COMMANDS          ,0x00, 0xff, 0xff, HCE_COMMAND_COMPLETE} /* 1.2 */
   ,{HCC_READ_LOCAL_FEATURES          ,0x00, 0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_READ_LOCAL_EXT_FEATURES      ,0x01, 0xff, 0xff, HCE_COMMAND_COMPLETE} /* 1.2 */
   ,{HCC_READ_BUFFER_SIZE             ,0x00, 0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_READ_BD_ADDR                 ,0x00, 0xff, 0xff, HCE_COMMAND_COMPLETE}

/* Group: Status parameters */
   ,{HCC_READ_FAILED_CONTACT_COUNT    ,0x02, 0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_RESET_FAILED_CONTACT_COUNT   ,0x02, 0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_GET_LINK_QUALITY             ,0x02, 0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_READ_RSSI                    ,0x02, 0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_READ_AFH_CHANNEL_MAP         ,0x02, 0xff, 0xff, HCE_COMMAND_COMPLETE} /* 1.2 */
   ,{HCC_READ_CLOCK                   ,0x03, 0xff, 0xff, HCE_COMMAND_COMPLETE} /* 1.2 */

/* Group: Testing commands */
   ,{HCC_READ_LOOPBACK_MODE           ,0x00, 0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_WRITE_LOOPBACK_MODE          ,0x01, 0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_ENABLE_DUT                   ,0x00, 0xff, 0xff, HCE_COMMAND_COMPLETE}

/* STLC2500C vendor commands */
   ,{HCC_ST_STATIC_TXRX               ,5, 0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_ST_TXRX_TEST                 ,28, 0xff, 0xff, HCE_COMMAND_COMPLETE}
   ,{HCC_ERI_ADDRESS                  ,8, 0xff, 0xff, HCE_COMMAND_COMPLETE}

};

T_HCI_Command_Format * HCI_Def_GetCommandFormatbyIdx(U8 idx)
{
    return &hci_commands[idx];
}

U16 HCI_Def_GetCommandCnt()
{
    return sizeof(hci_commands)/sizeof(T_HCI_Command_Format);
}

U16 HCI_Def_GetCommandOpByIdx(U8 idx)
{
    return hci_commands[idx].op;
}

U16 HCI_Def_GetCommandParmLenByIdx(U8 idx)
{
    return hci_commands[idx].parmlen;
}

U8 HCI_Def_GetCommandStartEventByIdx(U8 idx)
{
    return hci_commands[idx].event_start;
}

U8 HCI_Def_GetCommandEndEventByIdx(U8 idx)
{
    return hci_commands[idx].event_end;
}

U8 HCI_Def_GetCommandStartEventByOp(U16 op)
{
    U16 i;
    U16 cnt;

    cnt = sizeof(hci_commands)/sizeof(T_HCI_Command_Format); 

    for(i = 0; i < cnt ; i++)
    {
        if(hci_commands[i].op == op)
        {
           return hci_commands[i].event_start;
        }
    }

    return 0xFF;
}

U8 HCI_Def_GetCommandEndEventByOp(U16 op)
{
    U16 i;
    U16 cnt;

    cnt = sizeof(hci_commands)/sizeof(T_HCI_Command_Format); 

    for(i = 0; i < cnt ; i++)
    {
        if(hci_commands[i].op == op)
        {
           return hci_commands[i].event_end;
        }
    }

    return 0xFF;
}


void HCI_RegisterDataHandler(HCI_HCDHandler dataH)
{
    if(dataH)
    {
        dataHandler = dataH;
    }
}

void HCI_RegisterEventHandler(HCI_HCEHandler eventH)
{
    if(eventH)
    {
        eventHandler = eventH;
    }
}

void HCI_RegisterDataReadyHandler(HCI_DataTxReadyHandler datardyHandler)
{
    if(datardyHandler)
        datarHandler = datardyHandler;
}

void HCI_RegisterDataDoneHandler(HCI_DataTxDone dd)
{
    if(dd)
        datadHandler = dd;
}

BtStatus HCI_SndCommand(U16 opCode, U8 parmLen, HciCommand *cmd)
{
    CurTask = opCode;
    return HciSendCommand(opCode, parmLen, cmd, TRUE);
}

void HCI_Callback(U8 event, HciCallbackParms *parms)
{
    switch (event) {
    case HCI_DATA_IND:
        //need to consider SCO
        
        dataHandler(parms->ptr.rxBuff->len, parms->ptr.rxBuff->buffer, parms->ptr.rxBuff->flags, parms->hciHandle);
        /* Send data to L2CAP */
        //L2CAP_HciReceive(parms->ptr.rxBuff, parms->hciHandle); 
        break;

    case HCI_PACKET_HANDLED:
        //need to consider SCO

        /* Give the packet to L2CAP */
    //    L2CAP_HciTxDone(parms->ptr.packet, parms->status, parms->hciHandle); 
        datadHandler(parms->ptr.packet, parms->status, parms->hciHandle); 
        break;

    case HCI_COMMAND_HANDLED:
        break;

    case HCI_SEND_IND:  //it can send data
        //need to consider SCO

        /* Tell L2CAP it can send data */
    //    parms->ptr.packet = L2CAP_HciTxReady(parms->hciHandle); 
        parms->ptr.packet = datarHandler(parms->hciHandle);
        break;

    case HCI_CONTROLLER_EVENT:
        /* Handle the event. */
        
        //(void)MeHandleEvents(parms->ptr.hciEvent->event, 
          //                   parms->ptr.hciEvent->len, 
            //                 parms->ptr.hciEvent->parms);

        (void)eventHandler(parms->ptr.hciEvent->event, 
                             parms->ptr.hciEvent->len, 
                             parms->ptr.hciEvent->parms);
        break;

    case HCI_INIT_STATUS:
        /* HCI initializaiton is complete */
        Report(("DataLink: HCI Init complete status: %d\n", parms->status));
////        MeProcessInitStatus(parms);
        break;

    case HCI_DEINIT_STATUS:
        Report(("DataLink: HCI DeInit complete status: %d\n", parms->status));
        (void)DDB_Close();
        break;

    case HCI_TRANSPORT_ERROR:
        HCI_Deinit();
        break;
    }
}

