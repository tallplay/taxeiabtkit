//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by MassStorage-Uart-Tester.rc
//
#define IDD_MASSSTORAGEUARTTESTER_DIALOG 102
#define IDR_MAINFRAME                   128
#define IDC_EDIT_HEX                    1000
#define IDC_EDIT_CHAR                   1001
#define IDC_BUTTON1                     1002
#define IDC_COMBO_HCICOMMAND            1003
#define IDC_STATIC_HCICOMMANDLEN        1004
#define IDC_EDIT_HCICOMMANDDATA         1005
#define IDC_STATIC_HCICOMMANDSTARTEVENT 1006
#define IDC_STATIC_HCICOMMANDMIDEVENT   1007
#define IDC_STATIC_HCICOMMANDENDEVENT   1008
#define IDC_LIST_MSG                    1009
#define IDC_STATIC_HCC_PARM             1010
#define IDC_EDIT_ACL1ID                 1011
#define IDC_STATIC_ACL1STS              1012
#define IDC_BUTTON_ACL1                 1013
#define IDC_EDIT_ACL2ID                 1014
#define IDC_STATIC_ACL2STS              1015
#define IDC_BUTTON_ACL2                 1016
#define IDC_EDIT_ACL3ID                 1017
#define IDC_STATIC_ACL3STS              1018
#define IDC_BUTTON_ACL3                 1019
#define IDC_EDIT_ACL4ID                 1020
#define IDC_STATIC_ACL1STS4             1021
#define IDC_STATIC_ACL4STS              1021
#define IDC_BUTTON_ACL4                 1022
#define IDC_EDIT_BDADDR                 1023
#define IDC_BUTTON2                     1024
#define IDC_BUTTON_SETADDR              1024
#define IDC_BUTTON_PageScan             1025
#define IDC_EDIT_BDADDR_ACCEPT          1026
#define IDC_BUTTON_AUTO_ACCEPT          1027
#define IDC_BUTTON5                     1028
#define IDC_BUTTON_ACLTEST1             1028

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1029
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
