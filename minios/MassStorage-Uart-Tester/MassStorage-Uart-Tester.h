// MassStorage-Uart-Tester.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols


extern "C"{
#include "osapi.h"
#include "DataLink.h"
#include "sys/Debug.h"
#include "radiomgr.h"
void MINIOS_Run(void);
HWND StartSniffer(HINSTANCE Instance, int CmdShow);
};

// CMassStorageUartTesterApp:
// See MassStorage-Uart-Tester.cpp for the implementation of this class
//
typedef enum
{
    HCC_IDLE,
    HCC_SEND,
    HCC_WAIT_START_EVENT,
    HCC_WAIT_END_EVENT,    
}HCC_Execute_State;

class CMassStorageUartTesterApp : public CWinApp
{
public:
	CMassStorageUartTesterApp();

// Overrides
	public:
	virtual BOOL InitInstance();

// Implementation

	T_HCI_Command_Format* m_thcf_cursel;

    HCC_Execute_State HCCExecuteState;

    BOOL bHCCEventRecv;
    U8 HCCEvent;
    U8 HCCEventLen;
    U8 HCCEventData[256];

    U16 HCCCMD;
    U8 HCCCMDLen;
    U8 HCCCMDData[256];

    BOOL bACLConnected[10];
    U16 ACLHandleID[10];

    U8 SettingExecuteState; //0: idle 1: set address 2: reset

    BOOL bHCDRecv;
    U16 HCDHandle;
    U16 HCDLen;
    U8 *HCDData;

    BOOL bHCDSend;
    U16 HCDSendHandle;
    U8 HCDSendData[256];//max 20
    BtPacket HCDSendPacket;

    U8 HCDTestState; //0:idle 1: echo sent 2: wait echo back 3: fail(wait timeout or echo sent fail)

	DECLARE_MESSAGE_MAP()
};

extern CMassStorageUartTesterApp theApp;
