//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by app_ui.rc
//
#define L2CAP_APP                       101
#define LLC_Output                      1000
#define L2Client_RegisterPSM            1001
#define L2Client_ConnReq                1002
#define L2Client_DiscReq                1003
#define L2Client_ConfigReq              1004
#define L2Client_ConfigRsp              1005
#define L2Client_ConfigRej              1006
#define L2Client_SendData               1007
#define LLC_EchoReq                     1008
#define LLC_InfoReq                     1009
#define LLC_RegisterGroup               1010
#define LLC_GroupAdd                    1011
#define LLC_GroupRemove                 1012
#define LLC_GroupList                   1013
#define LLC_DestroyGroup                1014
#define LLC_SendGroupData               1015
#define L2Server_RegisterPSM            1016
#define L2Server_ConnRsp                1017
#define L2Server_ConfigReq              1018
#define L2Server_ConfigRsp              1019
#define L2Server_ConfigRej              1020
#define L2Server_SendData               1021
#define L2Server_DiscReq                1022
#define L2Server_ConnRspStatus          1023
#define LLC_AutoClient                  1024
#define LLC_AutoServer                  1025
#define LLC_ToggleCLT                   1026
#define LLC_Clear                       1027
#define LLC_EXIT                        1028
#define LLC_ACLView                     1029
#define L2Server_AutoConfig             1030
#define L2Client_AutoConfig             1031
#define LLC_ACLConnect                  1032
#define LLC_ACLDisconnect               1033

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        111
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1034
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
