#ifndef __OVERIDE_H
#define __OVERIDE_H
/***************************************************************************
 *
 * File:
 *     $Workfile:overide.h$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:33$
 *
 * Description:
 *     This file contains declarations that are used to override
 *     the configure options set in config files such as 
 *     btconfig.h. If no overrides are needed then this file 
 *     should be empty. It still must exist. 
 *
 * Created:
 *     July 15, 1999
 *
 * Copyright 1999-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

/* WARNING: The values in this overide.h file were selected specifically for
 * this sample application. If you change them, the sample application may fail
 * to compile or not work properly.
 */

/*
 * Enable the Bluetooth stack.
 */
#define BT_STACK                XA_ENABLED

/*
 * Increase the MTU just for the fun of it.
 */
#define L2CAP_MTU               1024

/*
 * L2CAP app supports one device.
 */
#define NUM_BT_DEVICES          1

/*
 * Give the user 45 seconds to respond.
 */
#define L2CAP_RTX_TIMEOUT       45

/*
 * The app needs one group.
 */
#define L2CAP_NUM_GROUPS        1

/*
 * The app uses the L2CAP_GetInfo() API.
 */
#define L2CAP_GET_INFO_SUPPORT  XA_ENABLED

/*
 * Turn on flexible config if we want to run the 
 * configuration steps manually.
 */
#define L2CAP_FLEXIBLE_CONFIG   XA_ENABLED

/*
 * The app needs one registration for each client and server.
 */
#define L2CAP_NUM_PROTOCOLS     2

/*
 * This application doesn't require RFCOMM or SDP parsing functions.
 */
#define RFCOMM_PROTOCOL         XA_DISABLED
#define SDP_PARSING_FUNCS       XA_DISABLED

/*
 * Only needed for the USB project.
 */
#define HCI_USB_TX_BUFFER_SIZE  260

#endif /* __OVERIDE_H */
