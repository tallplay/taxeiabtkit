/***************************************************************************
 *
 * File:
 *     $Workfile:l2cap_app.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:60$
 *
 * Description:
 *     Source file for l2cap sample application.
 *
 * Created:
 *     October 7, 1999
 *
 * Copyright 1999-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/
#define STRICT
#include "l2cap.h"

#include "windows.h"
#include "tchar.h"
#include "commctrl.h"
#include "stdio.h"
#include "sys/types.h"
#include "sys/timeb.h"
#include "resource.h"
#include "utils.h"
#include "me.h"
#include "sys/debug.h"
#include "bluemgr_ui.h"

/* Internal Functions */
static void AppPrint(char *format,...);
static BOOL CALLBACK AppWndProc(HWND, UINT, WPARAM, LPARAM);

static void L2Client_Event(L2capChannelId, L2capCallbackParms *);
static void L2Server_Event(L2capChannelId, L2capCallbackParms *);
static void L2Group_Event(L2capChannelId, L2capCallbackParms *);
static void L2Ping_Event(L2capChannelId, L2capCallbackParms *);
static void L2Info_Event(L2capChannelId, L2capCallbackParms *);
static void RunServer(L2capCallbackParms *);
static void RunClient(L2capCallbackParms *);
static void PrintData(const L2capCallbackParms *Info);
#if L2CAP_FLEXIBLE_CONFIG == XA_ENABLED
static void PrintConfigurationPdus(Info);
#endif
static void L2App_LinkHandler(const BtEvent *Event);
static LPTSTR GetToolTipText(int ControlId);

#define TARGET_PSM      0x0097      /* PSM registered by the server */
#define TX_DATA_LEN        256      /* Size of transmit packet data */
#define BURST_COUNT         40      /* Number of packets in a data burst */
#define PRINT_DATA_LINES     5      /* Number of Rx data lines to display. */


#if L2CAP_NUM_GROUPS > 0
/*
 * Local structure used to add space for additional group members to the
 * basic L2CAP group structure which holds 1.
 */
typedef struct _AppGroup {
   L2capGroup    group;
   BD_ADDR       members[4]; /* Five members in all */
} AppGroup;
#endif

/* Local variables for tracking PSM's used in this application */
static L2capPsm     Client = {L2Client_Event, 0x0000, L2CAP_MAXIMUM_MTU, 256};
static L2capPsm     Server = {L2Server_Event, TARGET_PSM, L2CAP_MAXIMUM_MTU, 256};
static L2capPsm     PingClient;
static L2capPsm     InfoClient;
#if L2CAP_NUM_GROUPS > 0
static AppGroup     GroupPsm;
#endif

/* Client and Server state flags */
#define L2_CONNECTED        0x01
#define L2_AUTO_CONFIG      0x02    /* Track state of AutoConfig check boxes */
#define L2_SEND_RESPONSE    0x04    /* We need to send a Config Rsp */

static U8       ClientState = L2_AUTO_CONFIG; /* Default is on */
static U8       ServerState = L2_AUTO_CONFIG; /* Default is on */

/* BtPacket data space for sending configuration options. */
static U8           ServerOptions[50];
static U8           ClientOptions[50];

static BtSelectDeviceToken  DevQuery;
static BtHandler            LinkHandler;
static BtRemoteDevice      *RemDev = 0;

/* Storage for dynamically assigned Channel identifiers, etc. */
static L2capChannelId   ClientCid, ServerCid, GroupId;
static BtPacket         ClientPkt, ServerPkt, GroupPkt, InfoPkt, PingPkt;

static U32              ClientBurst = 0;
static U32              ServerBurst = 0;
static BD_ADDR         *Members;
static U8               TxData[TX_DATA_LEN];
#if L2CAP_NUM_GROUPS > 0
static BOOL             CltEnabled = TRUE;
#endif

/* When utilizing L2CAP Flow Control, the L2CAP User must provide a reassembly
 * buffer. The buffer must be as large as the maximum MTU stated in the PSM struct.
 */
#if L2CAP_FLOW_CONTROL == XA_ENABLED
static U8           ClientRxBuffer[L2CAP_MAXIMUM_MTU];
static U8           ServerRxBuffer[L2CAP_MAXIMUM_MTU];
#endif

/* Table order matches L2ConnStatus Values */
static const char *ConnStatusTable[] = { "Accepted", "Pending", 
                    "PSM Not Supported", "Security Block", "No Resources" };

/* GUI Applicaiton internal variables, etc */
static HINSTANCE    hInst;
static HWND         AppWnd, MainWnd, BlueMgrWnd;
static BOOL         AutoClient = FALSE;
static BOOL         AutoServer = FALSE;
static char         msg[200];
static BD_ADDR      tmpBdAddr = {0};

/*
 * Main Entrypoint for our application
 */
HWND *APP_Init( HINSTANCE Inst, HINSTANCE PrevInst, LPTSTR CmdLine, int CmdShow )
{
    int      i;

    hInst = Inst;
    InitCommonControls();

    /* Create Application GUI */
    AppWnd = MainWnd = CreateDialog(Inst, MAKEINTRESOURCE(L2CAP_APP), NULL, AppWndProc);
    if (MainWnd == NULL)
        return 0;

    ShowWindow( AppWnd, CmdShow ) ;

    ME_SetClassOfDevice(COD_MAJOR_UNCLASSIFIED);

    /* Load Bluetooth Piconet Manager GUI */
    BlueMgrWnd = BMG_Initialize(Inst, AppWnd);
    if (BlueMgrWnd == 0) {
        DestroyWindow(MainWnd);
        return 0;
    }
    BMG_AddMainMenu(AppWnd);
    BMG_AddToolTips(AppWnd, GetToolTipText);

    OS_MemSet((U8 *)&ClientPkt, 0, sizeof(BtPacket));
    OS_MemSet((U8 *)&ServerPkt, 0, sizeof(BtPacket));
    OS_MemSet((U8 *)&GroupPkt, 0, sizeof(BtPacket));
    OS_MemSet((U8 *)&InfoPkt, 0, sizeof(BtPacket));
    OS_MemSet((U8 *)&PingPkt, 0, sizeof(BtPacket));

    for (i = 0; i < TX_DATA_LEN; i++) 
        TxData[i] = (U8)i;

    return &MainWnd;
}

void  APP_Deinit(void)
{
    /* Unused */
}

void APP_Thread(void)
{
    /* Unused */
}

/*
 * Handler for Window messages generated by GUI
 */
BOOL CALLBACK AppWndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    BtStatus            status;
    int                 index;
#if L2CAP_NUM_GROUPS > 0
    U8                  count;
    U8                  addr[BDADDR_NTOA_SIZE];
#endif
#if L2CAP_FLEXIBLE_CONFIG == XA_ENABLED
    U32                 btnState; 
#endif
    L2capConnSettings   connSettings = {0};

    switch (uMsg) {

    case WM_INITDIALOG:
        for (index = 0; index < 5; index++)
            SendMessage(GetDlgItem(hWnd, L2Server_ConnRspStatus), CB_ADDSTRING, 0, (LPARAM)ConnStatusTable[index]);

        SendMessage(GetDlgItem(hWnd, L2Server_ConnRspStatus), CB_SETCURSEL, 0, 0);

        /* Update the GUI to enable Flexible Configuration options. */
        SendDlgItemMessage(hWnd, L2Server_AutoConfig, BM_SETCHECK, 
                           (ServerState & L2_AUTO_CONFIG ? BST_CHECKED : BST_UNCHECKED), 0L);

        SendDlgItemMessage(hWnd, L2Client_AutoConfig, BM_SETCHECK, 
                           (ClientState & L2_AUTO_CONFIG ? BST_CHECKED : BST_UNCHECKED), 0L);

#if L2CAP_FLEXIBLE_CONFIG == XA_ENABLED
        EnableWindow(GetDlgItem(hWnd, L2Server_AutoConfig), TRUE);
        EnableWindow(GetDlgItem(hWnd, L2Client_AutoConfig), TRUE);
        EnableWindow(GetDlgItem(hWnd, L2Client_ConfigReq), TRUE);
        EnableWindow(GetDlgItem(hWnd, L2Client_ConfigRsp), TRUE);
        EnableWindow(GetDlgItem(hWnd, L2Server_ConfigReq), TRUE);
        EnableWindow(GetDlgItem(hWnd, L2Server_ConfigRsp), TRUE);
#endif
#if L2CAP_NUM_GROUPS > 0
        /* Update the GUI to enable Group functions. */
        EnableWindow(GetDlgItem(hWnd, LLC_RegisterGroup), TRUE);
        EnableWindow(GetDlgItem(hWnd, LLC_GroupAdd), TRUE);
        EnableWindow(GetDlgItem(hWnd, LLC_GroupRemove), TRUE);
        EnableWindow(GetDlgItem(hWnd, LLC_GroupList), TRUE);
        EnableWindow(GetDlgItem(hWnd, LLC_DestroyGroup), TRUE);
        EnableWindow(GetDlgItem(hWnd, LLC_SendGroupData), TRUE);
        EnableWindow(GetDlgItem(hWnd, LLC_ToggleCLT), TRUE);
#endif
        break;

    case WM_PAINT: 
        /* Draws a separator below the Blue Manager menu. */
        {
            PAINTSTRUCT     ps; 
            RECT            rect;
            HDC             hdc; 

            hdc = BeginPaint(hWnd, &ps); 
            GetClientRect(hWnd, &rect); 
            rect.left += 2;
            rect.right -= 3;
            DrawEdge(hdc, &rect, EDGE_ETCHED, BF_TOP); 
            EndPaint(hWnd, &ps); 
        }
        break;

    case WM_COMMAND:
        switch (LOWORD(wParam)) {
            /* *************************************************************
             *            L2CAP Connection Management Functions
             * *************************************************************/
        case LLC_ACLConnect:
            if (RemDev == 0) {
                DevQuery.callback = L2App_LinkHandler;
                DevQuery.quality.mask = (BDQM_DEVICE_CLASS|BDQM_DEVICE_STATUS);
                DevQuery.quality.deviceClass = COD_MAJOR_UNCLASSIFIED;
                DevQuery.quality.status = BDS_IN_RANGE;

                status = DS_SelectDevice(&DevQuery);
                if (status != BT_STATUS_PENDING)
                    AppPrint("DS_SelectDevice() Returned %s.", pBT_Status(status));
            }
            else AppPrint("A connection already exists.");
            break;

        case LLC_ACLDisconnect:
            if (RemDev) {
                status = ME_DisconnectLink(&LinkHandler, RemDev);
                if (status == BT_STATUS_SUCCESS) {
                    AppPrint("Local ACL connection disconnected.");
                    RemDev = 0;
                } else {
                    Assert(status == BT_STATUS_IN_PROGRESS);
                    AppPrint("ACL connection attempt in progress.");
                }
            }
            else AppPrint("No ACL connection exists.");
            break;

        case LLC_ACLView:
            BMG_ShowChooser(TRUE);
            break;

            /* *************************************************************
             *                  L2CAP Client Functions
             * *************************************************************/
        case L2Client_RegisterPSM:
#if L2CAP_FLOW_CONTROL == XA_ENABLED
            Client.inLinkMode = Client.outLinkMode = L2MODE_RETRANSMISSION;
            Client.inMonitorTimeout = 60000;   /* 60 seconds */
            Client.inRetransTimeout = 10000;   /* 10 seconds */
            Client.inTransmitMax = 3;
#endif /* L2CAP_FLOW_CONTROL == XA_ENABLED */
            status = L2CAP_RegisterPsm(&Client);
            AppPrint("Client: RegisterPsm() returned %s", pBT_Status(status));
            break;

        case L2Client_ConnReq:
            if (RemDev) {
#if L2CAP_FLOW_CONTROL == XA_ENABLED
                connSettings.reasmBuffer = ClientRxBuffer;
#endif
                status = L2CAP_ConnectReq(&Client, TARGET_PSM, RemDev, &connSettings, &ClientCid);
                AppPrint("Client: ConnectReq() Returned %s.", pBT_Status(status));
                
                if (status == BT_STATUS_PENDING) {
#if L2CAP_FLEXIBLE_CONFIG == XA_ENABLED
                    L2CAP_SetAutoConfigMode(ClientCid, 
                                            (ClientState & L2_AUTO_CONFIG ? TRUE : FALSE));
#endif
                    AppPrint("   L2CAP Assigned Channel Id %04x.", ClientCid);
                }

            } else {
                AppPrint("Error: You must establish and select an ACL Connection first.");
            }
            break;

        case L2Client_DiscReq:
            status = L2CAP_DisconnectReq(ClientCid);
            AppPrint("Client: DiscReq(%04x) Returned %s", ClientCid, pBT_Status(status));
            break;

#if L2CAP_FLEXIBLE_CONFIG == XA_ENABLED
        case L2Client_ConfigReq:
            if (ClientPkt.flags & BTP_FLAG_INUSE) {
                AppPrint("Client: Cannot Send Config Request, the packet is In Use.");
                break;
            }

            ClientPkt.flags = BTP_FLAG_INUSE;
            ClientPkt.headerLen = 0;
            ClientPkt.dataLen = 0;
            ClientPkt.data = ClientOptions;

            /* See what non-default options we need to send. */
            if (L2CAP_MAXIMUM_MTU != L2CAP_DEFAULT_MTU) {
                ClientOptions[0] = L2CFG_OPTION_MTU;
                ClientOptions[1] = 2;
                StoreLE16(ClientOptions+2, L2CAP_MAXIMUM_MTU);
                ClientPkt.dataLen += 4;
            }

            status = L2CAP_SendConfigReq(ClientCid, &ClientPkt, L2CFG_FLAG_NONE);
            AppPrint("Client: SendConfigReq(%04x) Returned %s", ClientCid, pBT_Status(status));
            if (status != BT_STATUS_PENDING)
                ClientPkt.flags = 0;
            break;

        case L2Client_ConfigRsp:
            if (ClientPkt.flags & BTP_FLAG_INUSE) {
                AppPrint("Client: Cannot Send Config Response, the packet is In Use.");
                ClientState |= L2_SEND_RESPONSE;
                break;
            }
            ClientPkt.flags = BTP_FLAG_INUSE;
            ClientPkt.headerLen = 0;
            ClientPkt.dataLen = 0;

            status = L2CAP_SendConfigRsp(ClientCid, &ClientPkt, L2CFG_RESULT_ACCEPTED, L2CFG_FLAG_NONE);
            AppPrint("Client: SendConfigRsp(%04x) Returned %s", ClientCid, pBT_Status(status));
            if (status != BT_STATUS_PENDING)
                ClientPkt.flags = 0;
            break;

        case L2Client_AutoConfig:
            btnState = SendDlgItemMessage(hWnd, L2Client_AutoConfig, BM_GETSTATE, 0L, 0L);
            if (btnState & BST_CHECKED)
                ClientState |= L2_AUTO_CONFIG;
            else ClientState &= ~L2_AUTO_CONFIG;
            break;
#endif

        case L2Client_SendData:
            if (ClientPkt.flags & BTP_FLAG_INUSE) {
                AppPrint("Client: Cannot Send Packet because it is In Use.");
                break;
            }

            ClientPkt.flags = BTP_FLAG_INUSE;
            ClientPkt.headerLen = 0;
            ClientPkt.data = TxData;
            ClientPkt.dataLen = TX_DATA_LEN;

            status = L2CAP_SendData(ClientCid, &ClientPkt);
            AppPrint("Client: SendData(%04x) Returned %s", ClientCid, pBT_Status(status));

            if (status != BT_STATUS_PENDING) 
                ClientPkt.flags = 0;
            break;

            /* *************************************************************
             *                  L2CAP Server Functions
             * *************************************************************/
        case L2Server_RegisterPSM:
#if L2CAP_FLOW_CONTROL == XA_ENABLED
            Server.inLinkMode = Server.outLinkMode = L2MODE_RETRANSMISSION;
            Server.inMonitorTimeout = 60000;   /* 60 seconds */
            Server.inRetransTimeout = 10000;   /* 10 seconds */
            Server.inTransmitMax = 3;
#endif /* L2CAP_FLOW_CONTROL == XA_ENABLED */
            status = L2CAP_RegisterPsm(&Server);
            AppPrint("Server: RegisterPsm() Returned %s", pBT_Status(status));
            break;

        case L2Server_ConnRsp:
            /* Indices in combo box correlate with L2CONN_Xxxx values. */
#if L2CAP_FLEXIBLE_CONFIG == XA_ENABLED
            L2CAP_SetAutoConfigMode(ServerCid, 
                                    (ServerState & L2_AUTO_CONFIG ? TRUE : FALSE));
#endif
            index = SendMessage(GetDlgItem(hWnd, L2Server_ConnRspStatus), CB_GETCURSEL, 0, 0);
            if (index == CB_ERR) index = 0;

#if L2CAP_FLOW_CONTROL == XA_ENABLED
            connSettings.reasmBuffer = ServerRxBuffer;
#endif
            status = L2CAP_ConnectRsp(ServerCid, (L2capConnStatus)index, &connSettings);
            AppPrint("Server: ConnectRsp(%04x, %s) Returned %s", ServerCid, ConnStatusTable[index], pBT_Status(status));

            break;

        case L2Server_DiscReq:
            status = L2CAP_DisconnectReq(ServerCid);
            AppPrint("Server: DiscReq(%04x) Returned %s", ServerCid, pBT_Status(status));

            break;

#if L2CAP_FLEXIBLE_CONFIG == XA_ENABLED
        case L2Server_ConfigReq:
            if (ServerPkt.flags & BTP_FLAG_INUSE) {
                AppPrint("Server: Cannot Send Config Request, the packet is In Use.");
                break;
            }

            ServerPkt.flags = BTP_FLAG_INUSE;
            ServerPkt.headerLen = 0;
            ServerPkt.dataLen = 0;
            ServerPkt.data = ServerOptions;

            /* See what non-default options we need to send. */
            if (L2CAP_MAXIMUM_MTU != L2CAP_DEFAULT_MTU) {
                ServerOptions[0] = L2CFG_OPTION_MTU;
                ServerOptions[1] = 2;
                StoreLE16(ServerOptions+2, L2CAP_MAXIMUM_MTU);
                ServerPkt.dataLen += 4;
            }

            status = L2CAP_SendConfigReq(ServerCid, &ServerPkt, L2CFG_FLAG_NONE);
            AppPrint("Server: SendConfigReq(%04x) Returned %s", ServerCid, pBT_Status(status));
            if (status != BT_STATUS_PENDING)
                ServerPkt.flags = 0;
            break;

        case L2Server_ConfigRsp:
            if (ServerPkt.flags & BTP_FLAG_INUSE) {
                AppPrint("Server: Cannot Send Config Response, the packet is In Use.");
                ServerState |= L2_SEND_RESPONSE;
                break;
            }
            ServerPkt.flags = BTP_FLAG_INUSE;
            ServerPkt.headerLen = 0;
            ServerPkt.dataLen = 0;

            status = L2CAP_SendConfigRsp(ServerCid, &ServerPkt, L2CFG_RESULT_ACCEPTED, L2CFG_FLAG_NONE);
            AppPrint("Server: SendConfigRsp(%04x) Returned %s", ServerCid, pBT_Status(status));
            if (status != BT_STATUS_PENDING)
                ServerPkt.flags = 0;
            break;

        case L2Server_AutoConfig:
            btnState = SendDlgItemMessage(hWnd, L2Server_AutoConfig, BM_GETSTATE, 0L, 0L);
            if (btnState & BST_CHECKED)
                ServerState |= L2_AUTO_CONFIG;
            else ServerState &= ~L2_AUTO_CONFIG;
            break;
#endif

        case L2Server_SendData:
            if (ServerPkt.flags & BTP_FLAG_INUSE) {
                AppPrint("Server: Cannot Send Packet because it is In Use.");
                break;
            }

            ServerPkt.flags = BTP_FLAG_INUSE;
            ServerPkt.headerLen = 0;
            ServerPkt.data = TxData;
            ServerPkt.dataLen = TX_DATA_LEN;
                  
            status = L2CAP_SendData(ServerCid, &ServerPkt);
            AppPrint("Server: SendData(%04x) Returned %s", ServerCid, pBT_Status(status));
            
            if (status != BT_STATUS_PENDING) 
                ServerPkt.flags = 0;
            break;

            /* *************************************************************
             *                  L2CAP Ping/GetInfo Functions
             * *************************************************************/

        case LLC_EchoReq:
            if (RemDev) {
                PingClient.callback = L2Ping_Event;
                status = L2CAP_Ping(&PingClient, RemDev, TxData, TX_DATA_LEN);
                AppPrint("Ping() Returned %s", pBT_Status(status));
            } else {
                AppPrint("Error: You must establish and select an ACL Connection first.");
            }
            break;

        case LLC_InfoReq:
            if (RemDev) {
                InfoClient.callback = L2Info_Event;
                status = L2CAP_GetInfo(&InfoClient, RemDev, L2INFO_CONNECTIONLESS_MTU);
                AppPrint("GetInfo() for Connectionless MTU, Returned %s", pBT_Status(status));
            } else {
                AppPrint("Error: You must establish and select an ACL Connection first.");
            }
            break;

#if L2CAP_NUM_GROUPS > 0
            /* *************************************************************
             *                    L2CAP Group Functions
             * *************************************************************/

        case LLC_RegisterGroup:
            GroupPsm.group.psm = 0xABBA;
            GroupPsm.group.callback = L2Group_Event;
            GroupPsm.group.maxMembers = 5;
            status = L2CAP_RegisterGroup(&GroupPsm.group, &GroupId);
            AppPrint("RegisterGroup() returned %s, GroupId %04x", pBT_Status(status), GroupId);
            break;

        case LLC_DestroyGroup:
            status = L2CAP_GroupDestroy(&GroupPsm.group);
            AppPrint("GroupDestroy() returned %s", pBT_Status(status));
            break;
            
        case LLC_GroupAdd:
            if (BMG_QueryUserForBdAddr(&tmpBdAddr) == FALSE) {
                /* User cancelled dialog */
                break;
            }

            status = L2CAP_GroupAddMember(GroupId, &tmpBdAddr);
            AppPrint("GroupAddMember(%s) returned %s", bdaddr_ntoa(&tmpBdAddr, addr), pBT_Status(status));
            break;

        case LLC_GroupRemove:
            if (BMG_QueryUserForBdAddr(&tmpBdAddr) == FALSE) {
                /* User cancelled dialog */
                break;
            }
            status = L2CAP_GroupRemoveMember(GroupId, &tmpBdAddr);
            AppPrint("GroupRemoveMember(%s) returned %s", bdaddr_ntoa(&tmpBdAddr, addr), pBT_Status(status));
            break;

        case LLC_GroupList:
            status = L2CAP_GroupGetMembers(GroupId, &Members, &count);
            if (status == BT_STATUS_SUCCESS) {
                AppPrint("GroupGetMembers() returned %s, %d members.", pBT_Status(status), count);

                index = 0;
                while (index < count) {
                    AppPrint("   Member %d: %s", ++index, bdaddr_ntoa(Members, addr));
                    Members++;
                }
            }
            else AppPrint("GroupGetMembers() returned %s.", pBT_Status(status));
            break;
            
        case LLC_SendGroupData:
            if (GroupPkt.flags & BTP_FLAG_INUSE) {
                AppPrint("Group: Cannot Send Packet because it is In Use.");
                break;
            }
            
            GroupPkt.flags = BTP_FLAG_INUSE;
            GroupPkt.headerLen = 0;
            GroupPkt.data = TxData;
            GroupPkt.dataLen = TX_DATA_LEN;
            
            status = L2CAP_SendData(GroupId, &GroupPkt);
            AppPrint("Group: SendData(%04x) Returned %s", GroupId, pBT_Status(status));

            if (status != BT_STATUS_PENDING) 
                GroupPkt.flags = 0;
            break;

        case LLC_ToggleCLT:
            if (CltEnabled) {
                status = L2CAP_DisableCLT(0);
                AppPrint("L2CAP_DisableCLT() Returned %s.", pBT_Status(status));
                if (status == BT_STATUS_SUCCESS) {
                    SendMessage(GetDlgItem(hWnd, LLC_ToggleCLT), WM_SETTEXT, 0, (LPARAM)"Enable CLT");
                    CltEnabled = FALSE;
                }
            } else {
                status = L2CAP_EnableCLT(0);
                AppPrint("L2CAP_EnableCLT() Returned %s.", pBT_Status(status));
                if (status == BT_STATUS_SUCCESS) {
                    SendMessage(GetDlgItem(hWnd, LLC_ToggleCLT), WM_SETTEXT, 0, (LPARAM)"Disable CLT");
                    CltEnabled = TRUE;
                }
            }
            break;
#endif

            /* *************************************************************
             *                       Misc Functions
             * *************************************************************/
            
        case LLC_Clear:
            PostMessage(GetDlgItem(AppWnd, LLC_Output), LB_RESETCONTENT, 0, 0);
            break;

        case LLC_AutoClient:
            if (AutoClient == FALSE) {
                SendMessage(GetDlgItem(hWnd, LLC_AutoClient), WM_SETTEXT, 0, (LPARAM)"Manual Client");
                AutoClient = TRUE;
            } else {
                SendMessage(GetDlgItem(hWnd, LLC_AutoClient), WM_SETTEXT, 0, (LPARAM)"Auto Client");
                AutoClient = FALSE;
            }
            break;

        case LLC_AutoServer:
            if (AutoServer == FALSE) {
                SendMessage(GetDlgItem(hWnd, LLC_AutoServer), WM_SETTEXT, 0, (LPARAM)"Manual Server");
                AutoServer = TRUE;
            } else {
                SendMessage(GetDlgItem(hWnd, LLC_AutoServer), WM_SETTEXT, 0, (LPARAM)"Auto Server");
                AutoServer = FALSE;
            }
            break;

        case LLC_EXIT:
        case WM_DESTROY:
            if (hWnd == AppWnd) {
                /* Exit the application */
                if (BMG_Shutdown() != BT_STATUS_PENDING)
                    PostQuitMessage(0);
            }
            return TRUE;

        default:
            BMG_TrackMainMenu(hWnd, wParam, lParam);
            break;
        }
        break;
    }
    return 0L;
}


static void L2App_LinkHandler(const BtEvent *Event)
{
    BtStatus    status;

    switch (Event->eType) {
    case BTEVENT_DEVICE_SELECTED:
        if (Event->errCode == BEC_NO_ERROR) {
            ME_InitHandler(&LinkHandler);
            LinkHandler.callback = L2App_LinkHandler;

            status = ME_CreateLink(&LinkHandler, &Event->p.select->result->addr, 
                                   &Event->p.select->result->psi, &RemDev);
            AppPrint("ME_CreateLink() to %s returned %s.", 
                     BMG_GetDeviceName(Event->p.select->result), pBT_Status(status));
        } else {
            AppPrint("DS_SelectDevice() completed with error: %s.", pHC_Status(Event->errCode));
            if (Event->errCode == BEC_NOT_FOUND)
                AppPrint("You must select a device in the Blue Manager.");
        }
        break;

    case BTEVENT_LINK_CONNECT_CNF:
        if (Event->errCode == BEC_NO_ERROR) {
            AppPrint("ACL connection established.");
        } else {
            AppPrint("ACL connection failed, reason = %s.", pHC_Status(Event->errCode));
            RemDev = 0;
        }
        break;

    case BTEVENT_LINK_DISCONNECT:
        RemDev = 0;
        break;
    }
}


/* ---------------------------------------------------------------------------
 *                     Client L2CAP Event handler
 * ---------------------------------------------------------------------------
 */
void L2Client_Event(U16 ChannelId, L2capCallbackParms *Info)
{
   U8 *m = msg;

   /* Display the event */
   m += sprintf(m, "Client(%04x): Received Event %s", ChannelId, pL2CAP_Event(Info->event));

   if (Info->event == L2EVENT_DISCONNECTED) {
      m += sprintf(m, " due to %s", pL2CAP_DiscReason(Info->status));
      ClientCid = L2CID_INVALID;
   }
   else if (Info->event == L2EVENT_DATA_IND) {
       m += sprintf(m, " %d bytes received", Info->dataLen);
   }
   else if (Info->event == L2EVENT_COMPLETE) {
       m += sprintf(m, " status %s", pBT_Status((BtStatus)Info->status));
   }
   AppPrint("%s.", msg);

   /* Process the event */
   switch (Info->event) {
   case L2EVENT_CONNECTED:
   case L2EVENT_RECONFIGURED:
       ClientState |= L2_CONNECTED;
       break;

   case L2EVENT_DISCONNECTED:
   case L2EVENT_CONFIGURING:
       ClientState &= ~L2_CONNECTED;
       break;

#if L2CAP_FLEXIBLE_CONFIG == XA_ENABLED
   case L2EVENT_CONFIG_IND:
   case L2EVENT_CONFIG_CNF:
       PrintConfigurationPdus(Info);
       break;
#endif

   case L2EVENT_DATA_IND:
       PrintData(Info);

#if L2CAP_FLOW_CONTROL == XA_ENABLED
       /* Accept data so packets will continue to flow. */
       AssertEval(L2CAP_AcceptSduData(ChannelId, Info->dataLen) == BT_STATUS_SUCCESS);
#endif
       break;

   case L2EVENT_PACKET_HANDLED:
      Assert(Info->ptrs.packet == (void *)&ClientPkt);
      Assert(ClientPkt.flags & BTP_FLAG_INUSE);
      ClientPkt.flags = 0;
      break;
   }
   
   if (AutoClient)
      RunClient(Info);
}


/* ---------------------------------------------------------------------------
 *                     Server L2CAP Event handler
 * ---------------------------------------------------------------------------
 */
void L2Server_Event(U16 ChannelId, L2capCallbackParms *Info)
{
    U8          *m = msg;

    /* Display the event */
    m += sprintf(m, "Server(%04x): Received Event %s", ChannelId, pL2CAP_Event(Info->event));

    if (Info->event == L2EVENT_DISCONNECTED) {
        m += sprintf(m, " due to %s", pL2CAP_DiscReason(Info->status));
        ServerCid = L2CID_INVALID;
    }
    else if (Info->event == L2EVENT_DATA_IND) {
        m += sprintf(m, " %d bytes received", Info->dataLen);
    }
    else if (Info->event == L2EVENT_COMPLETE) {
          m += sprintf(m, " status %s", pBT_Status((BtStatus)Info->status));
    }
    AppPrint("%s.", msg);

    if (Info->event == L2EVENT_CONNECT_IND) {
        /* Save server Channel Id */
        ServerCid = ChannelId;

        AppPrint("   L2CAP Assigned Channel ID %04x.", ChannelId);
    }

    /* Process the event */
    switch (Info->event) {
    case L2EVENT_CONNECTED:
    case L2EVENT_RECONFIGURED:
        ServerState |= L2_CONNECTED;
        break;

    case L2EVENT_DISCONNECTED:
    case L2EVENT_CONFIGURING:
        ServerState &= ~L2_CONNECTED;
        break;

#if L2CAP_FLEXIBLE_CONFIG == XA_ENABLED
   case L2EVENT_CONFIG_IND:
   case L2EVENT_CONFIG_CNF:
        PrintConfigurationPdus(Info);
        break;
#endif

   case L2EVENT_DATA_IND:
       PrintData(Info);

#if L2CAP_FLOW_CONTROL == XA_ENABLED
       /* Accept data so packets will continue to flow. */
       AssertEval(L2CAP_AcceptSduData(ChannelId, Info->dataLen) == BT_STATUS_SUCCESS);
#endif
       break;

   case L2EVENT_PACKET_HANDLED:
        Assert(Info->ptrs.packet == (void *)&ServerPkt);
        Assert(ServerPkt.flags & BTP_FLAG_INUSE);
        ServerPkt.flags = 0;
        break;
    }
    
    if (AutoServer)
        RunServer(Info);
}

#if L2CAP_NUM_GROUPS > 0
/* ---------------------------------------------------------------------------
 *                       L2CAP Group Event handler
 * ---------------------------------------------------------------------------
 */
static void L2Group_Event(U16 ChannelId, L2capCallbackParms *Info)
{
    AppPrint("Group %04x: Received Event %s.", ChannelId, pL2CAP_Event(Info->event));

    if (Info->event == L2EVENT_PACKET_HANDLED) {
        Assert(Info->ptrs.packet == (void *)&GroupPkt);
        GroupPkt.flags = 0;
    }
    else if (Info->event == L2EVENT_DATA_IND) {
        PrintData(Info);
    }
}
#endif

/* ---------------------------------------------------------------------------
 *                       Ping Event handler
 * ---------------------------------------------------------------------------
 */
static void L2Ping_Event(U16 ChannelId, L2capCallbackParms *Info)
{
    AppPrint("Channel %04x: Received Event %s.", ChannelId, pL2CAP_Event(Info->event));

    if (Info->event == L2EVENT_COMPLETE) {
        AppPrint("Echo response has been received,");
        AppPrint("    Status %s, with %d bytes data.", pBT_Status((U8)Info->status), Info->dataLen);
        PrintData(Info);
    }
}

/* ---------------------------------------------------------------------------
 *                       Info Request Event handler
 * ---------------------------------------------------------------------------
 */
static void L2Info_Event(U16 ChannelId, L2capCallbackParms *Info)
{
    char *m = msg;

    AppPrint("Channel %04x: Received Event %s.", ChannelId, pL2CAP_Event(Info->event));

    if (Info->event == L2EVENT_COMPLETE) {
        AppPrint("Info response has been received,");

        if (Info->status == 0) {
            m += sprintf(m, "Status = Success (%d bytes of data)", Info->dataLen);
            if (Info->dataLen == 2)
                m += sprintf(m, " value = %d", LEtoHost16(Info->ptrs.data));
            else if (Info->dataLen == 4)
                m += sprintf(m, " value = %d", LEtoHost32(Info->ptrs.data));
        }
        else m += sprintf(m, "Status = Not Supported", Info->dataLen); 

        AppPrint("    %s.", msg);
   }
}


/*
 * Handles running automatic server actions
 */
static void RunServer(L2capCallbackParms *Info)
{
    switch (Info->event) {
    case L2EVENT_CONNECT_IND:
        SendMessage(GetDlgItem(AppWnd, L2Server_ConnRspStatus), CB_SETCURSEL, 0, 0);
        PostMessage(AppWnd, WM_COMMAND, L2Server_ConnRsp, 0);
        break;

    case L2EVENT_DATA_IND:
        ServerBurst++;
        /* When we've received 80% of the burst, respond to keep data flowing. */
        if (ServerBurst > ((BURST_COUNT * 80) / 100)) {
            PostMessage(AppWnd, WM_COMMAND, L2Server_SendData, 0);
        }
        break;

    case L2EVENT_CONFIGURING:
        PostMessage(AppWnd, WM_COMMAND, L2Server_ConfigReq, 0);
        break;

    case L2EVENT_PACKET_HANDLED:
        ServerBurst = 0;

        if ((ServerState & L2_SEND_RESPONSE) == 0)
            break;
        
        ServerState &= ~L2_SEND_RESPONSE;
        /* Drop down to send ConfigRsp */

#if L2CAP_FLEXIBLE_CONFIG == XA_ENABLED
    case L2EVENT_CONFIG_IND:
        PostMessage(AppWnd, WM_COMMAND, L2Server_ConfigRsp, 0);
        break;
#endif
    }
}


/*
 * Handles running automatic client actions
 */
static void RunClient(L2capCallbackParms *Info)
{
    BtStatus status;

    switch (Info->event) {

    case L2EVENT_CONNECTED:
        /* Uncomment the following line to have data transmit automatically
         * start once the connection is established.
         *
        PostMessage(AppWnd, WM_COMMAND, L2Client_SendData, 0);
         */
        break;

    case L2EVENT_CONFIGURING:
        PostMessage(AppWnd, WM_COMMAND, L2Client_ConfigReq, 0);
        break;

    case L2EVENT_DATA_IND:
         ClientBurst = 0;
         /* Got our ack from the server, restart the client */
         if (ClientPkt.flags & BTP_FLAG_INUSE)
             break;

    case L2EVENT_PACKET_HANDLED:
        if (ClientState & L2_CONNECTED) {
            if (ClientBurst < BURST_COUNT) {
                ClientBurst++;
            
                ClientPkt.flags = BTP_FLAG_INUSE;
                ClientPkt.headerLen = 0;
                ClientPkt.data = TxData;
                ClientPkt.dataLen = TX_DATA_LEN;
                
                status = L2CAP_SendData(ClientCid, &ClientPkt);
                AppPrint("Client:SendData(%04x) Burst %d, Returned %s", ClientCid, ClientBurst, pBT_Status(status));
            }
        }
        if ((ClientState & L2_SEND_RESPONSE) == 0)
            break;

        ClientState &= ~L2_SEND_RESPONSE;
        /* Drop down to send Config Response */

#if L2CAP_FLEXIBLE_CONFIG == XA_ENABLED
    case L2EVENT_CONFIG_IND:
        PostMessage(AppWnd, WM_COMMAND, L2Client_ConfigRsp, 0);
        break;
#endif

    case L2EVENT_DISCONNECTED:
        ClientBurst = 0;
        break;
    }
}

static void PrintData(const L2capCallbackParms *Info)
{
    const U8 *p = Info->ptrs.data;
    const U8 *limit = Info->ptrs.data + Info->dataLen;
    U8       *output;
    int       offset;
    int       lines = 0;

#if PRINT_DATA_LINES > 0
    while (p < limit) {
        output = msg;
        output += sprintf(output, "     ");

        offset = 0;
        while (offset < 16) {
            if (p+offset < limit)
                output += sprintf(output, "%02x ", p[offset]);
            if (++offset == 8) output += sprintf(output, "  ");
        }
        p += offset;
        AppPrint(msg);

        if (++lines == PRINT_DATA_LINES) {
            AppPrint("     <data ouput truncated>");
            break;
        }
    }
#endif
}


#if L2CAP_FLEXIBLE_CONFIG == XA_ENABLED
static void PrintConfigurationPdus(const L2capCallbackParms *Info)
{
    U8  *options;
    U16  optionsLen;

    if (Info->event == L2EVENT_CONFIG_IND) {
        AppPrint("   Ident %d, Flags %0x, OptionsLen %d.", 
                 Info->ptrs.configInd.ident, Info->ptrs.configInd.flags, 
                 Info->ptrs.configInd.optionsLen);

        options = Info->ptrs.configInd.options;
        optionsLen = Info->ptrs.configInd.optionsLen;
    }

    if (Info->event == L2EVENT_CONFIG_CNF) {
        AppPrint("   Ident %d, Flags %0x, Result %0x, OptionsLen %d.", 
                 Info->ptrs.configCnf.ident, Info->ptrs.configCnf.flags, 
                 Info->ptrs.configCnf.result, Info->ptrs.configCnf.optionsLen);

        options = Info->ptrs.configCnf.options;
        optionsLen = Info->ptrs.configCnf.optionsLen;
    }        

    while (optionsLen) {
        switch (options[0]) {
        case L2CFG_OPTION_MTU:
            AppPrint("   Option: MTU, Value %d", LEtoHost16(options+2));
            break;
        case L2CFG_OPTION_FLUSH_TIMEOUT:
            AppPrint("   Option: Flush Timeout, Value %d", LEtoHost16(options+2));
            break;
        case L2CFG_OPTION_QOS:
            AppPrint("   Option: QoS, length %d", options[1]);
            break;
        default:
            AppPrint("   Option: %d, length %d", options[0], options[1]);
            break;
        }

        /* Advance to next Option */
        optionsLen -= options[1]+2;
        options += options[1]+2;
    }
}
#endif

/*
 * Prints messages to the UI list box 
 */
static void AppPrint(char *format,...)
{
    char     buffer[200]; /* Output Buffer */
    int      len;
    va_list  args;
    int      index;

    va_start(args, format);
    len = _vsnprintf(buffer, 200, format, args);
    va_end(args);

    index = SendMessage(GetDlgItem(AppWnd, LLC_Output), LB_ADDSTRING, 0, (LPARAM)buffer);
    SendMessage(GetDlgItem(AppWnd, LLC_Output), LB_SETCARETINDEX, index, 0);
}

/*
 * Returns tool tip text for Main UI buttons.
 */
static LPTSTR GetToolTipText(int ControlId)
{
    int i;

    struct {
        int     control;
        LPTSTR  text;
    } msgs[] = {
        {L2Server_RegisterPSM, "Registers the test server PSM. The server must be registered before any server functions can be performed including receiving an incoming connection request."},
        {L2Server_DiscReq, "Send a L2CA_DisconnectReq to the client."},
        {L2Server_SendData, "Send a L2CA_DataWriteReq to the client."},
        {L2Server_ConnRsp, "Send a L2CA_ConnectRsp to the client."},
        {L2Server_ConfigReq, "Send a L2CA_ConfigReq to the client."},
        {L2Server_ConfigRsp, "Send a L2CA_ConfigRsp to the client."},
        {L2Server_AutoConfig, "When selected, the application automatically handles the L2CAP servers configuration phase."},
        {L2Server_ConnRspStatus, "Select the response code to send in connect response."},
        {L2Client_RegisterPSM, "Registers the test client. The client must be registered before any client functions can be performed."},
        {L2Client_DiscReq, "Send a L2CA_DisconnectReq to the server."},
        {L2Client_SendData, "Send a L2CA_DataWriteReq to the server."},
        {L2Client_ConnReq, "Send a L2CA_ConnectReq to the server."},
        {L2Client_ConfigReq, "Send a L2CA_ConfigReq to the server."},
        {L2Client_ConfigRsp, "Send a L2CA_ConfigRsp to the server."},
        {L2Client_AutoConfig, "When selected, the application automatically handles the L2CAP servers configuration phase."},
        {LLC_RegisterGroup, "Registers the test group PSM. The group must be registered before any group functions can be performed."},
        {LLC_GroupAdd, "Add a member address to the group list."},
        {LLC_SendGroupData, "Send a L2CA_DataReq message to the group. The message is sent to the Piconet Broadcast addresss."},
        {LLC_DestroyGroup, "Destroy and deregister the group."},
        {LLC_GroupRemove, "Remove a member address from the group list."},
        {LLC_GroupList, "Display the members of the group."},
        {LLC_EchoReq, "Send an L2CA_EchoReq to the remote device. An ACL connection must exist to send an echo request."},
        {LLC_InfoReq, "Send an L2CA_InfoReq to the remote device. An ACL connection must exist to send an info request."},
        {LLC_ToggleCLT, "Enable/Disable reception of connectionless traffic. This affects group traffic reception only."},
        {LLC_AutoServer, "In Auto mode, the server application will automatically handle all requests received by the server."},
        {LLC_AutoClient, "In Auto mode, the client application will continuously send traffic to the server once a connection is established and the first L2CA_DataReq is sent."},
        {LLC_ACLConnect, "Open the Bluetooth device manager to select a device to establish an ACL connection with."},
        {LLC_ACLDisconnect, "Disconnect the current ACL connection."},
        {LLC_ACLView, "Open the Bluetooth device manager to view devices and connections."}, {0,""}
    };

    for (i = 0; msgs[i].control; i++) {
        if (msgs[i].control == ControlId)
            break;
    }
    return msgs[i].text;
}


/*
 * Stub out SDP entrypoints called by event manager.
 */
BtStatus SDP_Init(void)     { return BT_STATUS_SUCCESS; }
void     SDP_Deinit(void)   { }
