# Microsoft Developer Studio Project File - Name="L2CAP App" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=L2CAP App - Win32 Release
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "llc.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "llc.mak" CFG="L2CAP App - Win32 Release"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "L2CAP App - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "L2CAP App - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "L2CAP App - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "..\..\build\l2cap_app\Release"
# PROP BASE Intermediate_Dir "..\..\build\l2cap_app\Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /YX /FD /c
# ADD CPP /nologo /Zp1 /W3 /GX /O1 /I "." /I "..\.." /I "..\..\hcitrans\abcsp\include" /I "..\..\hcitrans\abcsp\config" /I "..\..\inc" /I "..\..\minios\inc" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "USB_TRANS" /FD /c
# SUBTRACT CPP /YX
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /o "NUL" /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /o "NUL" /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo /o"..\..\build\l2cap_app\Release\l2cap_app.bsc"
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib advapi32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /machine:I386
# ADD LINK32 wsock32.lib comctl32.lib advapi32.lib user32.lib gdi32.lib winmm.lib comdlg32.lib /nologo /subsystem:windows /pdb:none /map /machine:I386 /out:"Release\l2cap_app.exe"

!ELSEIF  "$(CFG)" == "L2CAP App - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "..\..\build\l2cap_app\Debug"
# PROP BASE Intermediate_Dir "..\..\build\l2cap_app\Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /YX /FD /c
# ADD CPP /nologo /Zp1 /W3 /Gm /GX /Zi /Od /I "." /I "..\.." /I "..\..\hcitrans\abcsp\include" /I "..\..\hcitrans\abcsp\config" /I "..\..\inc" /I "..\..\minios\inc" /I "..\..\hcitrans\module_init\TI" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "USB_TRANS" /Fr /FD /c
# SUBTRACT CPP /YX
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /o "NUL" /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /o "NUL" /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo /o"..\..\build\l2cap_app\Debug/l2cap_app.bsc"
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib advapi32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 wsock32.lib comctl32.lib advapi32.lib user32.lib gdi32.lib winmm.lib comdlg32.lib /nologo /subsystem:windows /pdb:none /debug /machine:I386 /out:"Debug\l2cap_app.exe"

!ENDIF 

# Begin Target

# Name "L2CAP App - Win32 Release"
# Name "L2CAP App - Win32 Debug"
# Begin Group "Base SDK"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\stack\btalloc.c
# End Source File
# Begin Source File

SOURCE=..\..\stack\btrom.c
# End Source File
# Begin Source File

SOURCE=..\..\stack\btutils.c
# End Source File
# Begin Source File

SOURCE=..\..\stack\debug.c
# End Source File
# Begin Source File

SOURCE=..\..\stack\eventmgr.c
# End Source File
# Begin Source File

SOURCE=..\..\stack\hci\hci.c
# End Source File
# Begin Source File

SOURCE=..\..\stack\hci\hci_evnt.c
# End Source File
# Begin Source File

SOURCE=..\..\stack\hci\hci_proc.c
# End Source File
# Begin Source File

SOURCE=..\..\stack\hci\hci_util.c
# End Source File
# Begin Source File

SOURCE=..\..\stack\l2cap\l2cap.c
# End Source File
# Begin Source File

SOURCE=..\..\stack\l2cap\l2cap_if.c
# End Source File
# Begin Source File

SOURCE=..\..\stack\l2cap\l2cap_sm.c
# End Source File
# Begin Source File

SOURCE=..\..\stack\l2cap\l2cap_utl.c
# End Source File
# Begin Source File

SOURCE=..\..\stack\me\me.c
# End Source File
# Begin Source File

SOURCE=..\..\stack\me\meacc.c
# End Source File
# Begin Source File

SOURCE=..\..\stack\me\mecon.c
# End Source File
# Begin Source File

SOURCE=..\..\stack\me\medev.c
# End Source File
# Begin Source File

SOURCE=..\..\stack\me\meif.c
# End Source File
# Begin Source File

SOURCE=..\..\stack\me\mesec.c
# End Source File
# Begin Source File

SOURCE=..\..\apps\common\msg_constants.c
# End Source File
# Begin Source File

SOURCE=..\..\stack\hci\prescan.c
# End Source File
# Begin Source File

SOURCE=..\..\stack\radiomgr.c
# End Source File
# Begin Source File

SOURCE=..\..\stack\rxbuff.c
# End Source File
# Begin Source File

SOURCE=..\..\stack\sdp\sdputil.c
# End Source File
# Begin Source File

SOURCE=..\..\apps\common\sniffer.c
# End Source File
# Begin Source File

SOURCE=..\..\stack\utils.c
# End Source File
# End Group
# Begin Group "Windows Integration"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\common\bluemgr_gui.c
# End Source File
# Begin Source File

SOURCE=..\ddb4w.c
# End Source File
# Begin Source File

SOURCE=..\common\fts.cpp
# End Source File
# Begin Source File

SOURCE=..\osapi.c
# End Source File
# Begin Source File

SOURCE=..\common\sniffer_ui.c
# End Source File
# Begin Source File

SOURCE=..\winmain.c
# End Source File
# End Group
# Begin Group "HCI Transports"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\hcitrans\abcsp\src\abcsp_crc.c
# End Source File
# Begin Source File

SOURCE=..\..\hcitrans\abcsp\src\abcsp_free.c
# End Source File
# Begin Source File

SOURCE=..\..\hcitrans\abcsp\src\abcsp_init.c
# End Source File
# Begin Source File

SOURCE=..\..\hcitrans\abcsp\src\abcsp_malloc.c
# End Source File
# Begin Source File

SOURCE=..\..\hcitrans\abcsp\src\abcsp_zmalloc.c
# End Source File
# Begin Source File

SOURCE=..\..\hcitrans\abcsp\abcsphci.c
# End Source File
# Begin Source File

SOURCE=..\..\hcitrans\abcsp\src\le.c
# End Source File
# Begin Source File

SOURCE=..\..\hcitrans\abcsp\src\rxbcsp.c
# End Source File
# Begin Source File

SOURCE=..\..\hcitrans\abcsp\src\rxmsg.c
# End Source File
# Begin Source File

SOURCE=..\..\hcitrans\abcsp\src\rxmsgdemux.c
# End Source File
# Begin Source File

SOURCE=..\..\hcitrans\abcsp\src\rxslip.c
# End Source File
# Begin Source File

SOURCE=..\..\hcitrans\abcsp\src\txmsg.c
# End Source File
# Begin Source File

SOURCE=..\..\hcitrans\abcsp\src\txrx.c
# End Source File
# Begin Source File

SOURCE=..\..\hcitrans\abcsp\src\txslip.c
# End Source File
# Begin Source File

SOURCE=..\..\hcitrans\uart\uarttran.c
# End Source File
# Begin Source File

SOURCE=..\..\hcitrans\usb\usbtrans.c
# End Source File
# Begin Source File

SOURCE=..\..\hcitrans\abcsp\xa_abcsp.c
# End Source File
# End Group
# Begin Group "Radio Integration"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\hcitrans\inet\hclinkmgr.c

!IF  "$(CFG)" == "L2CAP App - Win32 Release"

# ADD CPP /I "..\..\hcitrans\modinit"

!ELSEIF  "$(CFG)" == "L2CAP App - Win32 Debug"

# ADD CPP /I "..\..\hcitrans\module_init\STMicro" /I "..\..\hcitrans\modinit"
# SUBTRACT CPP /I "..\..\hcitrans\module_init\TI"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\hcitrans\inet\hostctlr.c

!IF  "$(CFG)" == "L2CAP App - Win32 Release"

# ADD CPP /I "..\..\hcitrans\modinit"

!ELSEIF  "$(CFG)" == "L2CAP App - Win32 Debug"

# ADD CPP /I "..\..\hcitrans\module_init\STMicro" /I "..\..\hcitrans\modinit"
# SUBTRACT CPP /I "..\..\hcitrans\module_init\TI"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\hcitrans\inet\inettran.c

!IF  "$(CFG)" == "L2CAP App - Win32 Release"

# ADD CPP /I "..\..\hcitrans\modinit"

!ELSEIF  "$(CFG)" == "L2CAP App - Win32 Debug"

# ADD CPP /I "..\..\hcitrans\module_init\STMicro" /I "..\..\hcitrans\modinit"
# SUBTRACT CPP /I "..\..\hcitrans\module_init\TI"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\hcitrans\modinit\radiomod.c

!IF  "$(CFG)" == "L2CAP App - Win32 Release"

# ADD CPP /I "..\..\hcitrans\modinit"

!ELSEIF  "$(CFG)" == "L2CAP App - Win32 Debug"

# ADD CPP /I "..\..\hcitrans\modinit"
# SUBTRACT CPP /I "..\..\hcitrans\module_init\TI"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\hcitrans\modinit\TI\tiinit.c

!IF  "$(CFG)" == "L2CAP App - Win32 Release"

# ADD CPP /I "..\..\hcitrans\modinit"

!ELSEIF  "$(CFG)" == "L2CAP App - Win32 Debug"

# ADD CPP /I "..\..\hcitrans\modinit"
# SUBTRACT CPP /I "..\..\hcitrans\module_init\TI"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\hcitrans\transprt.c

!IF  "$(CFG)" == "L2CAP App - Win32 Release"

# ADD CPP /I "..\..\hcitrans\modinit"

!ELSEIF  "$(CFG)" == "L2CAP App - Win32 Debug"

# ADD CPP /I "..\..\hcitrans\module_init\STMicro" /I "..\..\hcitrans\modinit"
# SUBTRACT CPP /I "..\..\hcitrans\module_init\TI"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\hcitrans\common\uart.c

!IF  "$(CFG)" == "L2CAP App - Win32 Release"

# ADD CPP /I "..\..\hcitrans\modinit"

!ELSEIF  "$(CFG)" == "L2CAP App - Win32 Debug"

# ADD CPP /I "..\..\hcitrans\module_init\STMicro" /I "..\..\hcitrans\modinit"
# SUBTRACT CPP /I "..\..\hcitrans\module_init\TI"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\hcitrans\usb\usb.c

!IF  "$(CFG)" == "L2CAP App - Win32 Release"

# ADD CPP /I "..\..\hcitrans\modinit"

!ELSEIF  "$(CFG)" == "L2CAP App - Win32 Debug"

# ADD CPP /I "..\..\hcitrans\module_init\STMicro" /I "..\..\hcitrans\modinit"
# SUBTRACT CPP /I "..\..\hcitrans\module_init\TI"

!ENDIF 

# End Source File
# End Group
# Begin Source File

SOURCE=.\app_ui.rc
# End Source File
# Begin Source File

SOURCE=.\l2cap_app.c
# End Source File
# Begin Source File

SOURCE=.\overide.h
# End Source File
# End Target
# End Project
