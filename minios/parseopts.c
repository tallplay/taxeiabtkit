/***************************************************************************
 *
 * File:
 *     $Workfile:parseopts.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:55$
 *
 * Description:
 *     This file contains the code which parses and exports the command
 *     line options for the stack.
 *
 * Created:
 *     October 7, 1999
 *
 * Copyright 1999-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "osapi.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "parseopts.h"

#include "prescan.h"


/*---------------------------------------------------------------------------
 *
 * Global variables
 */

static UserOpts opts = { 8, 115, RADIO_TYPE_ST_MICRO_UART}; /* BT-INET Emulator, COM1 @ 115 baud */
//static UserOpts opts = { 8, 115, RADIO_TYPE_MTK_6622_UART}; /* BT-INET Emulator, COM1 @ 115 baud */


static char     errMessage[256];

/*---------------------------------------------------------------------------
 * getPortNumOption()
 *     Returns the user-selected port number.
 */
U16 getPortNumOption(void)
{
    return opts.portNum;
}

/*---------------------------------------------------------------------------
 * getSpeedOption()
 *     Returns the user-selected bps rate.
 */
U16 getSpeedOption(void)
{
    return opts.speed;
}

/*---------------------------------------------------------------------------
 * getRadioType()
 *     Returns the user-selected radio type.
 */
U16 getRadioType(void)
{
    return opts.radioType;
}

void setRadioType(U16 type)
{
    opts.radioType = type;
}

/*---------------------------------------------------------------------------
 * getSnifferOption()
 *     Returns the user-selected sniffer on/off state.
 */
BOOL getSnifferOption(void)
{
    return opts.startSniffer;
}


/*---------------------------------------------------------------------------
 * getErrorMesage()
 *     Returns the user-selected bps rate.
 */
const char *getErrorMessage(void)
{
    return errMessage;
}




