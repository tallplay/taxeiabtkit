#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#include "stdarg.h"
#include "stdlib.h"
#include "stdio.h"
#include "osapi.h"
#include "eventmgr.h"
#include "utils.h"

#include "mmsystem.h"  /* You must also include winmm.lib when compiling
                        * to access the Windows multimedia subsystem. */

#if (XA_MULTITASKING != XA_DISABLED)
#warning This version of osapi.c is designed to be used with
#warning XA_MULTITASKING set to disabled
#endif

/****************************************************************************
 *
 * Local Defines
 *
 ****************************************************************************/

#define OS_MAX_NUM_THREADS 10

/****************************************************************************
 *
 * RAM Data
 *
 ****************************************************************************/


static BOOL    init = FALSE;  /* Is the stack initialized? */
static U16     randSeed;      /* Seed for random number generator */
static BOOL    systemExit = FALSE; /* TRUE when it's time to quit */
static PFV     threads[OS_MAX_NUM_THREADS];   /* Array of threads */
static HANDLE  hwMux = NULL;  /* Mutex for OS_StopHardware() */
static HANDLE  irMux = NULL;  /* Mutex for OS_IrStopHardware() */
static TimeT   skipTime = 0;    /* Amount of time we've "skipped"; testing only */

/****************************************************************************
 *
 * Prototypes
 *
 ****************************************************************************/

void MINIOS_RemoveThread(PFV thread);
BOOL MINIOS_AddThread(PFV thread);
void MINIOS_Run(void);

#if (XA_DEBUG == XA_ENABLED) || (XA_DEBUG_PRINT == XA_ENABLED)
int __cdecl _CrtDbgReport(int, const char *, int, const char *, const char *, ...);
#endif


/****************************************************************************
 *
 * Functions
 *
 ****************************************************************************/


/*---------------------------------------------------------------------------
 * Initializes the operating system.
 */
BOOL OS_Init(void)
{
    int i;
    
    if (init==FALSE) {
        
        /* Set up the thread table for later MINIOS calls */
        for (i = 0; i < OS_MAX_NUM_THREADS; i++) {
            threads[i] = 0;
        }
        
        /*------------------------------------------------------------
         * Step one: initialize the random seed
         */
        randSeed = (U16)timeGetTime();
        
        /*------------------------------------------------------------
         * Step two: prepare timer and semaphore objects
         */

        /* No timer is necessary in a non-multitasking environment.*/
        
        /* The hardware driver will allocate this mutex before it
         * can execute. In a MINIOS system, this will often be handled
         * by interrupts instead, so the Mutex object will not be
         * required.
         */
        hwMux = CreateMutex(NULL, FALSE, NULL);
        irMux = CreateMutex(NULL, FALSE, NULL);
        
        if (hwMux == NULL) {
            return FALSE;
        }

        if (irMux == NULL) {
            return FALSE;
        }
        
        /*------------------------------------------------------------
         * Step three: initialize the event manager.
         */
        if (EVM_Init() == FALSE) {
            return FALSE;
        }

        /*------------------------------------------------------------
         * Step four create the stack task.
         */        
        MINIOS_AddThread(EVM_Process);

        /* Initialization is complete so set init to TRUE */
        init = TRUE;
    }

    return TRUE;    
}


/*---------------------------------------------------------------------------
 * If de-initialization was necessary, this function could be filled out
 */
void OS_Deinit(void)
{

    /* If hwMux is non-null, we probably survived the initialization process
     * and should tear everything down.
     */
    if (hwMux) {

        /* Deinitialize the event manager */
        EVM_Deinit();

        MINIOS_RemoveThread(EVM_Process);

        init = FALSE;        
        CloseHandle(hwMux);
        CloseHandle(irMux);
        hwMux = NULL;
        irMux = NULL;
    }
}


/*---------------------------------------------------------------------------
 * Returns the system time in ticks. Called by the stack.
 */
TimeT OS_GetSystemTime(void)
{
    /* Uses a coarse (55 ms) timer */
    return (TimeT)(GetTickCount() + skipTime);
}


/*---------------------------------------------------------------------------
 * Returns a random 16-bit number.
 */
U16 OS_Rand(void)
{
    randSeed = 25173 * randSeed + 13849;
    return randSeed;
}

/*---------------------------------------------------------------------------
 * Called by the stack to prevent hardware operations from running.
 */
void OS_StopHardware(void)
{
    DWORD result;
    
    /* Because the hardware thread always takes this mutex before it
     * runs, we can take it to prevent the hardware thread from running.
     * In a typical MINIOS system, this code should be replaced with
     * a system call to disable I/O interrupts.
     */
    result = WaitForSingleObject(hwMux, INFINITE);
    Assert(result == WAIT_OBJECT_0);
}


/*---------------------------------------------------------------------------
 * Called by the stack to allow hardware operations to resume.
 */
void OS_ResumeHardware(void)
{
    BOOL result;

    /* In a typical MINIOS system, this code should re-enable whatever
     * interrupts were disabled by the previous OS_StopHardware call.
     */
    result = ReleaseMutex(hwMux);
    if (result != TRUE)
    {
        Report(("Resumption error: %d\n", GetLastError()));
    }
    Assert(result == TRUE);
}


/*---------------------------------------------------------------------------
 * Called by the stack to prevent hardware operations from running.
 */
void OS_IrStopHardware(void)
{
    DWORD result;
    
    /* Because the hardware thread always takes this mutex before it
     * runs, we can take it to prevent the hardware thread from running.
     * In a typical MINIOS system, this code should be replaced with
     * a system call to disable I/O interrupts.
     */

    result = WaitForSingleObject(irMux, INFINITE);
    Assert(result == WAIT_OBJECT_0);
}


/*---------------------------------------------------------------------------
 * Called by the stack to allow hardware operations to resume.
 */
void OS_IrResumeHardware(void)
{
    BOOL result;

    /* In a typical MINIOS system, this code should re-enable whatever
     * interrupts were disabled by the previous OS_StopHardware call.
     */
    result = ReleaseMutex(irMux);
    if (result != TRUE)
    {
        Report(("Resumption error: %d\n", GetLastError()));
    }
    Assert(result == TRUE);
}


/*---------------------------------------------------------------------------
 * Called by the stack to copy memory
 */
void OS_MemCopy(U8 *dest, const U8 *source, U32 numBytes)
{
    /* The ANSI memcpy(dest, source, numBytes) function can be
     * used, or you can use this code if ANSI routines are not present
     */

#if INTEGER_SIZE == 4
    I16   c;
    
    c = numBytes / 4;
    numBytes -= (U16)c * 4;
    
    while (c--)
        *((U32 *)dest)++ = *((U32 *)source)++;
    
#elif INTEGER_SIZE == 2
    I16   c;
    
    c = numBytes / 2;
    numBytes -= c * 2;
    
    while (c--)
        *((U16 *)dest)++ = *((U16 *)source)++;
#endif
    
    while (numBytes--)
        *dest++ = *source++;    
}


/*---------------------------------------------------------------------------
 * Called by the stack to compare two buffers
 */
BOOL OS_MemCmp(const U8 *buffer1, U16 len1, const U8 *buffer2, U16 len2)
{
    /* You can use this code, or the ANSI equivalent if it is available:
     * return (len1 != len2) ? FALSE : (0 == memcmp(buffer1, buffer2, len2));
     */
    if (len1 == len2) {
      while (len1--) {
         if (*buffer1++ != *buffer2++)
            return FALSE;
      }
      return TRUE;
   }
   return FALSE;
}


/*---------------------------------------------------------------------------
 * Called by the stack to fill memory
 */
void OS_MemSet(U8 *dest, U8 byte, U32 len)
{
    /* Use the ANSI memset(dest, byte, len); function, or you can
     * use this code if ANSI routines are not present
     */
   while (len--)
      *dest++ = byte;
    
}

/*---------------------------------------------------------------------------
 * Compares two strings.
 */
U8 OS_StrCmp(const char *Str1, const char *Str2)
{
    while (*Str1 == *Str2) {
        if (*Str1 == 0 || *Str2 == 0) {
            break;
        }
        Str1++;
        Str2++;
    }

    /* Return zero on success, just like the ANSI strcmp() */
    if (*Str1 == *Str2)
        return 0;

    return 1;
}

/*---------------------------------------------------------------------------
 * Returns the length of the string.
 */
U16 OS_StrLen(const char *Str)
{
   const char  *cp = Str;

   Assert(Str);

   while (*cp != 0) cp++;

   return (U16)(cp - Str);
}

/* Note: OS_NotifyEvm, OS_LockStack, and OS_UnlockStack functions
 * are not required when XA_MULTITASKING is disabled.
 */

/*---------------------------------------------------------------------------
 * A special function used for testing purposes only. It advances the system
 * timer forward by an arbitrary amount. This is handy when a test would
 * otherwise have to wait a long time for a timer to expire.
 */
void OS_SkipSystemTime(TimeT time)
{
    TimeT now = OS_GetSystemTime();
    skipTime += time;
}


/****************************************************************************
 *
 * MINIOS functions
 *
 ****************************************************************************/

/*---------------------------------------------------------------------------
 * Adds a MINIOS thread. This thread is a function that executes and
 * returns without blocking. It will be called repeatedly until the system
 * exist or MINIOS_RemoveThread is called.
 *
 * Returns:
 *     TRUE - thread was added.
 *
 *     FALSE - thread was not added (no resources).
 */
BOOL MINIOS_AddThread(PFV thread)
{
    int i;

    /* Look for a blank space in which to store the thread */
    for (i = 0; i < OS_MAX_NUM_THREADS; i++) {

        /* The thread must not already have been added */
        Assert(threads[i]!=thread);
        
        if (threads[i] == 0) {
            threads[i] = thread;
            return (TRUE);
        }
    }
    return FALSE;

}


/*---------------------------------------------------------------------------
 * Removes a MINIOS thread.
 */
void MINIOS_RemoveThread(PFV thread)
{
    int i;

    for (i = 0; i < OS_MAX_NUM_THREADS; i++) {
        if (threads[i] == thread) {
            threads[i] = 0;            
        }
    }
}


/*---------------------------------------------------------------------------
 * Called to perform a single execution of current threads. MINIOS_Run
 * should be called repeatedly until system exit.
 */
void MINIOS_Run(void)
{
    int   i;

    /* Call threads */
    for (i = 0; i < OS_MAX_NUM_THREADS; i++) {
        if (threads[i] != 0) {
            threads[i]();
        }
    }
}


/****************************************************************************
 *
 * Debugging functions
 *
 ****************************************************************************/


#if XA_DEBUG == XA_ENABLED

/*---------------------------------------------------------------------------
 * Called to indicate a failed condition
 */
void OS_Assert(const char *expression, const char *file, U16 line)
{
    if (_CrtDbgReport(2, file, line, NULL, expression) == 1)
    {
        /* Halt the system */
        __asm { int 3 };
    }
}


#endif /* XA_DEBUG */

#if XA_DEBUG_PRINT == XA_ENABLED
#if !defined(DEMO) || TCP_STACK == XA_ENABLED || IRDA_STACK == XA_ENABLED
/*---------------------------------------------------------------------------
 * Called to report stack information
 *
 * NOTE: On the Windows OS platform XA_DEBUG must be enabled to 
 *       print debug messages.
 */
void OS_Report(const char *format, ...)
{
    char buffer[200];  /* Output buffer */
    va_list     args;
    
    va_start(args, format);
    _vsnprintf(buffer, 200, format, args);
    va_end(args);
    //printf("%s", buffer);
    _CrtDbgReport(0, NULL, 0, NULL, "%s", buffer);
}
#endif /* DEMO */
#endif /* XA_DEBUG_PRINT */


