#ifndef __OVERIDE_H
#define __OVERIDE_H

/* WARNING: The values in this overide.h file were selected specifically for
 * this sample application. If you change them, the sample application may fail
 * to compile or not work properly.
 */
#ifdef BTSTACK_DISABLE
#define BT_STACK XA_DISABLED
#endif

#define RFCOMM_PROTOCOL          XA_DISABLED
#define XA_USE_ENDIAN_MACROS     XA_ENABLED
#define XA_SNIFFER               XA_DISABLED

/* For larger SDP Server responses */
#define SDP_SERVER_SEND_SIZE     L2CAP_MTU /* 672 default */

/* Only needed for the USB transport. */
#define HCI_USB_TX_BUFFER_SIZE  260

#endif /* __OVERIDE_H */
