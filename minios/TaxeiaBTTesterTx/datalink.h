#ifndef __DATA_LINK__
#define __DATA_LINK__

#include "bttypes.h"
#include "hcitrans.h"
#include "sys/hci.h"
#include "sys/host.h"

typedef void (*HCI_BusHandler)(U8 event, U8 status);
typedef void (*HCI_HCEHandler)(U8 event, U8 len, U8* data);
typedef void (*HCI_HCDHandler)(U16 len, U8 *buffer, U8 flags, U16 HciHandle);
typedef BtPacket * (*HCI_DataTxReadyHandler)(HciHandle HciHndl);;
typedef void (*HCI_DataTxDone)(BtPacket *Packet, BtStatus Status, HciHandle HciHndl);

BtStatus HCI_SndCommand(U16 opCode, U8 parmLen, HciCommand *cmd);
void HCI_RegisterDataHandler(HCI_HCDHandler dataHandler);
void HCI_RegisterEventHandler(HCI_HCEHandler eventHandler);
void HCI_RegisterDataReadyHandler(HCI_DataTxReadyHandler datardyHandler);
void HCI_RegisterDataDoneHandler(HCI_DataTxDone dd);
void HCI_RegisterBusHandler(HCI_BusHandler bb);

typedef struct {
   U16 op;
   U8 parmlen;
   U8 event_start; //event which indicate command was start execute,  0xFF means no end event
   U8 event_middle; //events which indicate in middle,  0xFF means no end event
   U8 event_end; //event which indicate command was stop execute. 0xFF means no end event
}T_HCI_Command_Format;

U8 HCI_Def_GetCommandEndEventByOp(U16 op);
U8 HCI_Def_GetCommandStartEventByOp(U16 op);
U8 HCI_Def_GetCommandEndEventByIdx(U8 idx);
U8 HCI_Def_GetCommandStartEventByIdx(U8 idx);
U16 HCI_Def_GetCommandParmLenByIdx(U8 idx);
U16 HCI_Def_GetCommandOpByIdx(U8 idx);
U16 HCI_Def_GetCommandCnt();
T_HCI_Command_Format * HCI_Def_GetCommandFormatbyIdx(U8 idx);
#endif
