// TaxeiaBTTesterTxDlg.cpp : implementation file
//

#include "stdafx.h"
#include "TaxeiaBTTesterTx.h"
#include "TaxeiaBTTesterTxDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CTaxeiaBTTesterTxDlg::CTaxeiaBTTesterTxDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTaxeiaBTTesterTxDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CTaxeiaBTTesterTxDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATIC_STATUS, m_static_status);
	DDX_Control(pDX, IDC_STATIC_RSSI, m_static_rssi);
	DDX_Control(pDX, IDC_STATIC_ADDR, m_static_addr);
}

BEGIN_MESSAGE_MAP(CTaxeiaBTTesterTxDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_Test_Button, &CTaxeiaBTTesterTxDlg::OnBnClickedTestButton)
	ON_WM_TIMER()
END_MESSAGE_MAP()


// CTaxeiaBTTesterTxDlg message handlers
static CTaxeiaBTTesterTxDlg *mycwnd; 
BOOL isRun = FALSE;
static DWORD FAR PASCAL osThread(LPSTR lpData);
HANDLE OSThread;

#define TEST_OP_USER_START    0
#define TEST_OP_USER_APP_INIT 1
#define TEST_OP_BUS_HANDLER 2
#define TEST_OP_EVENT_HANDLER 3
#define TEST_OP_DATA_HANDLER 4
#define TEST_OP_DATA_READY_HANDLER 5
#define TEST_OP_TIMER_HANDLER 6

#define IS_COMMAND_STATUS(wish, event, data)  (event == HCE_COMMAND_STATUS && ((data[3] << 8) + data[2]) == wish)
#define IS_CONNECT_COMPLETE(event) (event == HCE_CONNECT_COMPLETE)
#define IS_DISCONNECT_COMPLETE(event) (event == HCE_DISCONNECT_COMPLETE)
#define IS_COMMAND_COMPLETE(event) (event == HCE_COMMAND_COMPLETE)

static DWORD FAR PASCAL osThread(LPSTR lpData)
{
    while(isRun)
	{
	    MINIOS_Run();
	}

	OS_Deinit();
	return 0;
}

void CTaxeiaBTTesterTxDlg::EventHandler(U8 event, U8 len, U8* data)
{
	U8 i;
	printf(" %s",  pHciEvent(event));

	switch(event)
	{
	    case HCE_COMMAND_COMPLETE:
			printf(" #Packet=%02d Op=%02x%02x",  data[0], data[1], data[2]);
			if(len > 3)
		    {
		        printf(" Data=");
			    for(i = 3; i < len; i++)
				    printf(" %02x",  data[i]);
		    }
			break;
		case HCE_COMMAND_STATUS:
			printf(" Sts = %s(%02x) #Packet=%02d Op=%02x%02x",  pHC_Status(data[0]), data[0], data[1], data[2], data[3]);
			if(len > 4)
		    {
		        printf(" Data=");
			    for(i = 4; i < len; i++)
				    printf(" %02x",  data[i]);
		    }
			break;
		case HCE_CONNECT_COMPLETE:
			printf(" Sts = %s(%02x) ConnectHandle=%02x%02x BDADDR =",  
				pHC_Status(data[0]), data[0], data[1], data[2]);
			for(i = 0; i < 6 ; i++)
				printf(" %02x",  data[3+i]);
			if(len > 9)
		    {
		        printf(" Data=");
			    for(i = 9; i < len; i++)
				    printf(" %02x",  data[i]);
		    }
			break;
		default:
	        printf(" Data=");
			for(i = 0; i < len; i++)
				 printf(" %02x",  data[i]);
		break;
	}
	
	mycwnd->TestProcedure(TEST_OP_EVENT_HANDLER, event, len, data);
}

void CTaxeiaBTTesterTxDlg::BusHandler(U8 event, U8 status)
{
	switch(event)
	{
		case HCI_INIT_STATUS:
			mycwnd->TestProcedure(TEST_OP_BUS_HANDLER, HCI_INIT_STATUS, 1, &status);
			break;
		case HCI_DEINIT_STATUS:
			mycwnd->TestProcedure(TEST_OP_BUS_HANDLER, HCI_DEINIT_STATUS, 1, &status);
			break;
		case HCI_TRANSPORT_ERROR:
			break;
	}
}

void HCI_DataDoneHandler(BtPacket *Packet, BtStatus Status, HciHandle HciHndl)
{
}

BtPacket * CTaxeiaBTTesterTxDlg::DataReadyHandler(HciHandle HciHndl)
{    
    return (BtPacket*)mycwnd->TestProcedure(TEST_OP_DATA_READY_HANDLER, HciHndl, 0, 0);
}

void CTaxeiaBTTesterTxDlg::DataHandler(U16 len, U8 *buffer, U8 flags, U16 HciHandle)
{
    mycwnd->TestProcedure(TEST_OP_DATA_HANDLER, HciHandle, len, buffer);
}



void CTaxeiaBTTesterTxDlg::SendHCICmd(U16 op, U8 parmlen, U8 *parm)
{
    static HciCommand cmd;
	memcpy(cmd.parms, parm, parmlen);
    HCI_SndCommand(op, parmlen, &cmd);	
}

void CTaxeiaBTTesterTxDlg::SetStaticColorText(U16 staticid, COLORREF color, const char *format, ...)
{
    char buffer[500];  /* Output buffer */
    va_list     args;
    
    va_start(args, format);
    _vsnprintf(buffer, 200, format, args);
    va_end(args);

	if(staticid == IDC_STATIC_RSSI)
	    m_static_rssi.SetTextColor(color);
	else if(staticid == IDC_STATIC_STATUS)
	    m_static_status.SetTextColor(color);
	else if(staticid == IDC_STATIC_ADDR)
	    m_static_addr.SetTextColor(color);

	((CColorStatic*)this->GetDlgItem(staticid))->SetWindowTextA(buffer);

}

void *CTaxeiaBTTesterTxDlg::TestProcedure(U8 op, U16 status, U8 len, U8* data)
{
	DWORD dwThreadID;
	
	#define TESTSTATE_IDLE 0
	#define TESTSTATE_INIT 1
	#define TESTSTATE_DEINIT 2
    #define TESTSTATE_TESTING 3

	#define TESTSUBSTATE_CREATEACL 0
	#define TESTSUBSTATE_WAITCONN 1
	#define TESTSUBSTATE_READRSSI 5
	#define TESTSUBSTATE_REQUEST_SEND 2
	#define TESTSUBSTATE_WAIT_HSK 3
	#define TESTSUBSTATE_DISCONNECT 4

	#define CMD_NOP 0x00
    #define CMD_ECHO 0x01
    #define CMD_ECHOBACK 0x02
	
	static U8 testState = TESTSTATE_IDLE;
	static U8 testSubState = TESTSUBSTATE_CREATEACL;
    static U16 ACLHandle;
	static BtPacket testPacket;
	static U8 testData[256];
	static U8 timeout;
	static U8 RSSI;
	static U8 uid[8];
		
	// TODO: Add your control notification handler code here
	switch(testState)
	{
	    case TESTSTATE_IDLE: //idle
	        if(op == TEST_OP_USER_START) //start
	        {
	            int rlen;

				rlen = 8;

	            //get unique id
                MassUart_Open();
			    MassUart_Execute(0, &rlen, "\xF2\x00\x03\x00\x00\x00\x00\x00\x00\x00\x00\x00", (char*)uid);				
				MassUart_Close();

				for(int i = 0; i < 6; i++)
				{
				    uid[i] = uid[i] ^ uid[i+2];
				}

				SetStaticColorText(IDC_STATIC_ADDR, LIGHTBLUE, "%02X%02X%02X%02X%02X%02X", uid[0], uid[1],uid[2],uid[3],uid[4],uid[5]);
				
				RADIO_SetBDAddr(uid);

				HCI_RegisterBusHandler(CTaxeiaBTTesterTxDlg::BusHandler);
				HCI_RegisterEventHandler(CTaxeiaBTTesterTxDlg::EventHandler);
				HCI_RegisterDataReadyHandler(CTaxeiaBTTesterTxDlg::DataReadyHandler);
				HCI_RegisterDataDoneHandler(HCI_DataDoneHandler);
				HCI_RegisterDataHandler(CTaxeiaBTTesterTxDlg::DataHandler);
				
			    if (!OS_Init()) {
					goto initfail;
			    }

				StartSniffer(AfxGetInstanceHandle(), SW_SHOW);
				
			    /* Create a secondary thread to watch for an event. */
				isRun = TRUE;
			    OSThread = CreateThread((LPSECURITY_ATTRIBUTES)NULL,
			                              0,
			                              (LPTHREAD_START_ROUTINE)osThread,
			                              (LPVOID)NULL,
			                              0, &dwThreadID);
			    if (NULL == OSThread) {
			        isRun = FALSE;
					goto initfail;
			    }
				this->GetDlgItem(IDC_Test_Button)->EnableWindow(FALSE);
				testState = TESTSTATE_INIT;
				SetStaticColorText(IDC_STATIC_RSSI, LIGHTBLUE, "RSSI=??");
				break;
initfail:				
				//m_static_status.SetTextColor(RED);
				//m_static_status.SetWindowTextA("Init Fail(1)!!");
				SetStaticColorText(IDC_STATIC_STATUS, LIGHTRED, "Init Fail(1)!!");
				break;
	        }
			else if(op == TEST_OP_USER_APP_INIT)
			{
				CFont *m_Font1 = new CFont;
			    m_Font1->CreatePointFont(200, "Arial Bold");
			    m_static_status.SetFont(m_Font1);
			    m_static_rssi.SetFont(m_Font1);
			    m_static_addr.SetFont(m_Font1);
				
				//m_static_status.SetTextColor(BLUE);
				//m_static_status.SetWindowTextA("Wait Device In!");

				SetStaticColorText(IDC_STATIC_STATUS, LIGHTBLUE, "Wait Device In!");
				SetStaticColorText(IDC_STATIC_RSSI, LIGHTBLUE, "RSSI=??");
				SetStaticColorText(IDC_STATIC_ADDR, LIGHTBLUE, "000000000000");

				//((CColorStatic*)this->GetDlgItem(IDC_STATIC_RSSI))->SetTextColor(BLUE);
				//m_static_rssi.SetWindowTextA("RSSI=");
			}
			
		    break;			
			
		case TESTSTATE_INIT: //initializing

		    if(op == TEST_OP_BUS_HANDLER) //success
	    	{//skip
	    	    if(data[0] == 0)
			    {
				//	m_static_status.SetTextColor(LIGHTBLUE);
				//	m_static_status.SetWindowTextA("Init Success!!");
					SetStaticColorText(IDC_STATIC_STATUS, LIGHTBLUE, "Init Success!");

	    			testState = TESTSTATE_TESTING;
					testSubState = TESTSUBSTATE_CREATEACL;
					SendHCICmd(HCC_CREATE_CONNECTION, 13, (U8*)"\x11\x22\x33\x44\x55\x66\x18\xcc\x01\x00\x00\x00\x00");					
			    }
				else
				{
					//m_static_status.SetTextColor(RED);
					//m_static_status.SetWindowTextA("Init Fail(2)!!");
					SetStaticColorText(IDC_STATIC_STATUS, LIGHTRED, "Init Fail(2)!!");
       				this->GetDlgItem(IDC_Test_Button)->EnableWindow(TRUE);


				    testState = TESTSTATE_IDLE;
				}
		    }
			else
			{
			    Assert(0); //unhandled encountered
			}
		    break;
			
		case TESTSTATE_TESTING:
		{
			if(op == TEST_OP_EVENT_HANDLER && status == HCE_MAX_SLOTS_CHNG)
				return 0; //skip useless command
			
		    if(testSubState == TESTSUBSTATE_CREATEACL)
		    {
		        if(op == TEST_OP_EVENT_HANDLER && IS_COMMAND_STATUS(HCC_CREATE_CONNECTION, status, data))
	        	{
	        	    if(data[0] == 0) //success
        	    	{
						//m_static_status.SetTextColor(LIGHTBLUE);
						//m_static_status.SetWindowTextA("WAIT for Connection!");
						SetStaticColorText(IDC_STATIC_STATUS, LIGHTBLUE, "WAIT for Connection!");						
        	    	    testSubState = TESTSUBSTATE_WAITCONN;
        	    	}
					else
					{
						//m_static_status.SetTextColor(RED);
						//m_static_status.SetWindowTextA("Create ACL Fail!!");
						SetStaticColorText(IDC_STATIC_STATUS, LIGHTRED, "Create ACL Fail!!");						

						RMGR_RadioShutdown();
					    testState = TESTSTATE_DEINIT;
					}
					return 0;
	        	}
		    }
			else if(testSubState == TESTSUBSTATE_WAITCONN)
			{					
			    if(op == TEST_OP_EVENT_HANDLER && IS_CONNECT_COMPLETE(status))
			    {
			        if(data[0] == 0)
		        	{
						//m_static_status.SetTextColor(LIGHTBLUE);
						//m_static_status.SetWindowTextA("Connected!");
						SetStaticColorText(IDC_STATIC_STATUS, LIGHTBLUE, "Connected!");						
						ACLHandle = (data[2] << 8) + data[1];

						SendHCICmd(HCC_READ_RSSI, 2, (U8*)&ACLHandle);
						testSubState = TESTSUBSTATE_READRSSI;
						
		        	}
					else
					{
						//m_static_status.SetTextColor(RED);
						//m_static_status.SetWindowTextA("Fail while connecting!!");
						SetStaticColorText(IDC_STATIC_STATUS, LIGHTRED, "Fail while connecting!!");						

						RMGR_RadioShutdown();
			            testState = TESTSTATE_DEINIT;
					}
				    return 0;
			    }
			}
			else if(testSubState == TESTSUBSTATE_READRSSI)
			{
			    if(op == TEST_OP_EVENT_HANDLER && IS_COMMAND_COMPLETE(status))
		    	{
				    Assert(data[3] == 0); //success

    	    	    RSSI = data[6];

					SetStaticColorText(IDC_STATIC_RSSI, BLUE, "RSSI=%02d", (char)RSSI);

					testSubState = TESTSUBSTATE_REQUEST_SEND;
					Assert(HCI_RequestToSend(ACLHandle)== BT_STATUS_SUCCESS);
					return 0;
		    	}
			}
			else if(testSubState == TESTSUBSTATE_REQUEST_SEND)
			{
			    if(op == TEST_OP_DATA_READY_HANDLER)
		    	{
		    	    if(ACLHandle == status)
		    	    {
    	    			testData[0] = 0xe4; //len
						testData[1] = 0x00;
						testData[2] = CMD_ECHO;
						testData[3] = 0x00;
						for(int i = 0; i < 0xe0 ; i++)
						{
			    			testData[i+3] = i;
						}
						testPacket.dataLen = 0xe4;

						testPacket.data = testData;
						testPacket.headerLen = 0;
						testPacket.tailLen = 0;	 

						//m_static_status.SetTextColor(LIGHTBLUE);						
						//m_static_status.SetWindowTextA("Data Sent, Wait Hsk!!");
						SetStaticColorText(IDC_STATIC_STATUS, LIGHTBLUE, "Data Sent, Wait Hsk!!");						

						timeout = 5;
                        SetTimer(1000, 1000, 0);
		    	        testSubState = TESTSUBSTATE_WAIT_HSK;
						return &testPacket;
		    	    }
		    	}
				
				Assert(0);
			}
			else if(testSubState == TESTSUBSTATE_WAIT_HSK)
			{
        	    U8 buf[3];
			    if(op == TEST_OP_TIMER_HANDLER)
		    	{
		    	    if(!timeout)
		    	    {
						//m_static_status.SetTextColor(LIGHTRED);
						//m_static_status.SetWindowTextA("Fail wait HSK!!");
						SetStaticColorText(IDC_STATIC_STATUS, LIGHTRED, "Fail wait HSK!!");						
						
						testSubState = TESTSUBSTATE_DISCONNECT;
						buf[0] = ACLHandle & 0xff;
						buf[1] = (ACLHandle >> 8) & 0xff;
						buf[2] = 0x13;
					    SendHCICmd(HCC_DISCONNECT, 3, buf);
		    	    }
					else
					{
					    //char buf[40];
						timeout--;
                        SetTimer(1000, 1000, 0);

						//sprintf(buf, "Data Sent, Wait Hsk...%02d", timeout);
						//m_static_status.SetTextColor(LIGHTBLUE);						
						//m_static_status.SetWindowTextA(buf);
						SetStaticColorText(IDC_STATIC_STATUS, LIGHTBLUE, "Data Sent, Wait Hsk...%02d", timeout);						
					}
					return 0;
		    	}
				else if(op == TEST_OP_DATA_HANDLER)
				{
					KillTimer(1000);
					testSubState = TESTSUBSTATE_DISCONNECT;
					buf[0] = ACLHandle & 0xff;
					buf[1] = (ACLHandle >> 8) & 0xff;
					buf[2] = 0x13;
				    SendHCICmd(HCC_DISCONNECT, 3, buf);

				    if(data[2] == CMD_ECHOBACK)
			    	{
			    	    if(!memcmp(testData+3, data+3, len-4))
		    	    	{
							//m_static_status.SetTextColor(LIGHTGREEN);
							//m_static_status.SetWindowTextA("PASS!!");		    	    	    
							SetStaticColorText(IDC_STATIC_STATUS, LIGHTGREEN, "PASS!");						

		    	    	}
						else
						{
							//m_static_status.SetTextColor(LIGHTRED);
							//m_static_status.SetWindowTextA("FAIL!!");		    	    	    
							SetStaticColorText(IDC_STATIC_STATUS, LIGHTRED, "FAIL!!");													
						}
					    return 0;
			    	}
				    Assert(0);
				}
				else if(op == TEST_OP_DATA_READY_HANDLER)
				{
				    Report(("Why data ready handler happen this time!!"));
					return 0;
				}

			   // Assert(0);
			}
			else if(testSubState == TESTSUBSTATE_DISCONNECT)
			{
			    if(op == TEST_OP_EVENT_HANDLER)
			    {
				    if(IS_DISCONNECT_COMPLETE(status))
				    {
						RMGR_RadioShutdown();
			            testState = TESTSTATE_DEINIT;
				    }
				    return 0;					
			    }
				
				Assert(0);
			}
		  ////  Assert(0); //unhandled encountered
		}
			
		case TESTSTATE_DEINIT: 
			if(op == TEST_OP_BUS_HANDLER)
			{
			    isRun = FALSE;
				testState = TESTSTATE_IDLE;
  				this->GetDlgItem(IDC_Test_Button)->EnableWindow(TRUE);
				return 0;
			}
			else if(op == TEST_OP_EVENT_HANDLER)
			{
				return 0;
			}
			
			Assert(0);		
	}
}

void CTaxeiaBTTesterTxDlg::UpdateUI()
{
}

BOOL CTaxeiaBTTesterTxDlg::OnInitDialog()
{
	DWORD dwThreadID;

	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here

	// TODO: Add extra initialization here
	mycwnd = this;

    TestProcedure(TEST_OP_USER_APP_INIT, 0, 0, 0);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CTaxeiaBTTesterTxDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	CDialog::OnSysCommand(nID, lParam);
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CTaxeiaBTTesterTxDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CTaxeiaBTTesterTxDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CTaxeiaBTTesterTxDlg::OnCancel()
{
	// TODO: Add your specialized code here and/or call the base class
	isRun = FALSE;
	CDialog::OnCancel();
}


void CTaxeiaBTTesterTxDlg::OnBnClickedTestButton()
{
	TestProcedure(TEST_OP_USER_START, 0, 0, 0);
}

void CTaxeiaBTTesterTxDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: Add your message handler code here and/or call default
	KillTimer(1000);
    TestProcedure(TEST_OP_TIMER_HANDLER, 0, 0, 0);
	CDialog::OnTimer(nIDEvent);
}
