// TaxeiaBTTesterTx.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols

extern "C"{
#include "osapi.h"
#include "DataLink.h"
#include "sys/Debug.h"
#include "radiomgr.h"
#include "MassUart.h"
void MINIOS_Run(void);
HWND StartSniffer(HINSTANCE Instance, int CmdShow);
};

// CTaxeiaBTTesterTxApp:
// See TaxeiaBTTesterTx.cpp for the implementation of this class
//

class CTaxeiaBTTesterTxApp : public CWinApp
{
public:
	CTaxeiaBTTesterTxApp();

// Overrides
	public:
	virtual BOOL InitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
};

extern CTaxeiaBTTesterTxApp theApp;
