// TaxeiaBTTesterTxDlg.h : header file
//

#pragma once
#include "ColorStatic.h"


// CTaxeiaBTTesterTxDlg dialog
class CTaxeiaBTTesterTxDlg : public CDialog
{
// Construction
public:
	CTaxeiaBTTesterTxDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_TAXEIABTTESTERTX_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
	virtual void OnCancel();
    static void BusHandler(U8 event, U8 status);
    static void EventHandler(U8 event, U8 len, U8* data);
    static BtPacket* DataReadyHandler(HciHandle HciHndl);
    static void DataHandler(U16 len, U8 *buffer, U8 flags, U16 HciHandle);
    void SetStaticColorText(U16 staticid, COLORREF color, const char *format, ...);
    void *TestProcedure(U8 op, U16 status, U8 len, U8* data);
    void SendHCICmd(U16 op, U8 parmlen, U8 *parm);
    void UpdateUI();
    
public:
	afx_msg void OnBnClickedTestButton();
	CColorStatic m_static_status;
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	CColorStatic m_static_rssi;
	CColorStatic m_static_addr;
};
