/***************************************************************************
 *
 * File:
 *     $Workfile:winmain.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:65$
 *
 * Description:
 *     This file contains the main program for test applications
 *     for the stack.
 *
 * Created:
 *     October 7, 1999
 *
 * Copyright 1999-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc. 
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "osapi.h"
#include "windows.h"
#include "stdio.h"

/*
 * Bring in the options parser
 */
#include "parseopts.c" 

#ifndef APP_PARSES_OPTIONS
#define APP_PARSES_OPTIONS  XA_DISABLED
#endif

/****************************************************************************
 *
 * Globals
 *
 ****************************************************************************/

/* Handle of the Applicaiton main window. */
static HWND   ActiveWnd = 0;
static HANDLE appScheduleEvent;
static HANDLE messageThread;

/****************************************************************************
 *
 * Prototypes
 *
 ****************************************************************************/

/*---------------------------------------------------------------------------
 *
 * Function prototypes
 */
/*
 * Application functions 
 */
HWND *APP_Init(HINSTANCE Inst, HINSTANCE PrevInst, LPTSTR CmdLine, int CmdShow);
void  APP_Deinit(void);
void  APP_Thread(void);


#if APP_PARSES_OPTIONS == XA_ENABLED
BOOL APP_ParseCommandLine(const char **Option);
char *APP_GetOptionString(void);
char *APP_GetExampleString(void);
#endif

/*
 * MINIOS functions
 * If multitasking is disabled, we'll use the MINIOS for threading
 */
#if XA_MULTITASKING == XA_DISABLED
void  MINIOS_RemoveThread(PFV thread);
BOOL  MINIOS_AddThread(PFV thread);
void  MINIOS_Run(void);
#else
static DWORD WINAPI AppThread(LPVOID active);
#endif /* XA_MULTITASKING */

void PumpMessages(void);

/*
 * Internal functions
 */
static BOOL ParseCommandLine(char *cmd);
static void PrintHelp(const char *title, const char *error);


/*---------------------------------------------------------------------------
 *             main()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  The main program. Initiailizes the Mini OS which in turn 
 *            initializes the stack.  It then initializes the application, 
 *            adds the application thread to the OS, and starts the Mini OS.
 *
 * Return:    Status of the operation.
 */
int PASCAL WinMain(HINSTANCE Inst, HINSTANCE PrevInst, LPTSTR CmdLine, int CmdShow)
{
#if XA_MULTITASKING == XA_ENABLED
    HANDLE  appThreadHndl;
    DWORD   appThreadId;
    BOOL    appThreadActive;
#endif
    HWND   *appWnd;

    if (FALSE == ParseCommandLine(CmdLine)) {
        return(1);
    }
    Report(("Command line read %s\n", CmdLine));

    if (!OS_Init()) {
        Report(("Fatal Error: Could not initialize stack.\n"));
        return(1);
    }

    appScheduleEvent = CreateEvent(NULL, FALSE, TRUE, NULL);
    Assert(appScheduleEvent);

    messageThread = GetCurrentThread();

    if ((appWnd = APP_Init(Inst, PrevInst, CmdLine, CmdShow)) != NULL) {
        ActiveWnd = *appWnd;

        /* Set up regular calls to APP_Thread (one way or another) */
#if XA_MULTITASKING == XA_DISABLED
        MINIOS_AddThread(APP_Thread);
#else
        appThreadActive = TRUE;
        appThreadHndl = CreateThread(0, 4096, AppThread, &appThreadActive, 0, &appThreadId);
        Assert(appThreadHndl);
#endif


        /* Create a timer that fires 10 times a second. Hopefully, this is
         * often enough for the application.
         */
        if (0 == SetTimer(NULL, 0, 100, NULL)) {
            Report(("Fatal Error: Could not establish app timer\n"));
            return 1;
        }

        while (ActiveWnd) {

#if XA_MULTITASKING == XA_DISABLED
            /* Let the minios run all threads */
            MINIOS_Run();
#endif

            /* Wait until a message is available, then process it */
            MsgWaitForMultipleObjectsEx(0, NULL, (XA_MULTITASKING==XA_ENABLED)?500:50,
                    QS_ALLEVENTS | QS_ALLINPUT | QS_ALLPOSTMESSAGE , 0);
            /* Processes any messages currently on the queue. */
            PumpMessages();
        }
    }

    /* TODO: Bother to kill the app thread? or just exit? */

    return 0;
}

#if XA_MULTITASKING == XA_ENABLED

/*
 *  Use this thread so as not to pollute the real APP_Thread with OS-specifics.
 */
static DWORD WINAPI AppThread(LPVOID active)
{

    while (ActiveWnd) {
        APP_Thread();        
        MsgWaitForMultipleObjectsEx(1, &appScheduleEvent, 100, 0 , 0);
    }
    /* App has exited, tell main thread it's OK to exit */
    *((BOOL *)active) = FALSE;
    return 0;
}

#endif /* XA_MULTITASKING */

/*
 * Deal with request to schedule application thread
 */
void APP_Schedule()
{
    SetEvent(appScheduleEvent);   
}



/*---------------------------------------------------------------------------
 *             PumpMessages()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  This function is responsible for processing window messages.
 *
 */
void PumpMessages(void)
{
    MSG     w_msg;
    HWND    hWnd;

    while (PeekMessage(&w_msg, NULL, 0, 0, PM_NOREMOVE)) {

        if (GetMessage(&w_msg, NULL, 0, 0) == 0) {
            if ((hWnd = ActiveWnd) != 0) {
                Report(("Deinitializing\n"));
                ActiveWnd = 0;
                APP_Deinit();
                OS_Deinit();
            }
            break;
        }

        if (w_msg.message == WM_APP+100) {
            /* This message is used to tell the message pump to change the 
             * window handle to the currently active dialog. It is necessary to
             * have ActiveWnd point to the correct dialog to insure keyboard
             * selection messages (tab key, down key, etc.) work.
             */
            if (w_msg.lParam) {
                Assert(IsWindow((HWND)w_msg.lParam));
                ActiveWnd = (HWND)w_msg.lParam;
            }
            continue;
        }

        if (!ActiveWnd || !IsDialogMessage(ActiveWnd, &w_msg)) {
            TranslateMessage( &w_msg );
            DispatchMessage( &w_msg );
        }
    }
}

/*---------------------------------------------------------------------------
 * ParseCommandLine()
 *
 *     Reads options from the command line.
 */
static BOOL ParseCommandLine(char *cmd)
{
    char *str;

    while (*cmd != 0) {
        if (*cmd == ' ') {
            /* Do nothing; spaces are meaningless */
            cmd++;
            continue;
        }

        /* Parse for -h Help option */
        str = (char *)cmd;
        if (((str[0] == '-') || (str[0] == '/')) && (str[1] == 'h')) {
            if (str[2] == 0) {
                PrintHelp("Program usage:", NULL);
                return FALSE;
            }
        }

        if (ParseStandardOptions(&cmd))
            continue;

#if APP_PARSES_OPTIONS == XA_ENABLED
        if (APP_ParseCommandLine(&cmd))
            continue;
#endif
        /* Unrecognized option */
        Report(("%s", getErrorMessage()));
        PrintHelp("Command line parse failed.", getErrorMessage());
        return FALSE;
    }

    /* We survived the parse, return success */
    return TRUE;
}


/*---------------------------------------------------------------------------
 *            PrintHelp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Print a help message for the application.
 *
 * Return:    name - name of function
 */
static void PrintHelp(const char *title, const char *error)
{
    char errMsg[750];
    char *cp;
    char name[_MAX_FNAME], *pName;

    /* Get the pgmname in case we need it. */
    pName = GetCommandLine();
    cp = pName;
    while (*cp++ != ' ')
        ;
    *cp = '\0';
    _splitpath(pName, 0, 0, name, 0);

    cp  = errMsg;

    if (error) {
        cp += sprintf(cp, "%s\n\n", error);
    }

    cp += sprintf(cp, "Usage: %s [-option [-option]]\n\n", name);

    /* Now print the options */
    cp += sprintf(cp, "Options: \n");

    /* Print the standard options */
    cp += sprintf(cp, "%s", getStandardOptions());

#if APP_PARSES_OPTIONS == XA_ENABLED
    cp += sprintf(cp, "%s", APP_GetOptionString());
#endif

    /* Print the help message */
    cp += sprintf(cp, "   -h\t\tPrint this help message\n\n");

#if APP_PARSES_OPTIONS == XA_ENABLED
    /* Print the example string */
    cp += sprintf(cp, "Example: %s %s\n", name, APP_GetExampleString());
#endif

    Assert(cp < errMsg + sizeof(errMsg));
    MessageBox(NULL, errMsg, title, MB_OK);

} /* End of PrintHelp() */

