/***************************************************************************
 *
 * File:
 *     $Workfile:oslib.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:16$
 *
 * Description:
 *     API abstraction layer for the Win32 API. This allows support for
 *     Windows 95, 98, NT, and 2000.
 *
 * Copyright 2001-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions,
 * Inc.  This work contains confidential and proprietary information of
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret,
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#include "stdlib.h"
#include "stdio.h"
#include "osapi.h"
#include "oslib.h"

/****************************************************************************
 *
 * Local Defines
 *
 ****************************************************************************/
#define OS_ALLOC_BLOCK_TABLE_SIZE   150

/****************************************************************************
 *
 * RAM Data
 *
 ****************************************************************************/
static U32      allocMax = 0;
static U32      allocBytes = 0;
static void    *allocBlocks[OS_ALLOC_BLOCK_TABLE_SIZE] = {0};

/****************************************************************************
 *
 * Prototypes
 *
 ****************************************************************************/

/****************************************************************************
 *
 * Functions
 *
 ****************************************************************************/


/*---------------------------------------------------------------------------
 * Allocate a block of memory.
 */
void *OS_Malloc(U16 Size)
{
#if XA_DEBUG == XA_ENABLED
    U8 *ptr;
    U16 i;

    Assert(Size > 0);
    if (Size == 0)
        return 0;

    if ((ptr = malloc((U16)(Size+6))) == 0) {
        return 0;
    }

    *(U16 *)ptr = Size;     /* Bytes 0 & 1 */

    ptr[2] = 0xFE;
    ptr[3] = 0xED;
    ptr[Size+4] = 0xF0;
    ptr[Size+5] = 0x0D;

    for (i = 0; i < OS_ALLOC_BLOCK_TABLE_SIZE; i++) {
        if (allocBlocks[i] == 0) {
            allocBlocks[i] = ptr;
            break;
        }
    }

    allocBytes += Size;
    if (allocBytes > allocMax)
        allocMax = allocBytes;

    Report(("OS_Malloc(%d) Allocated %d bytes @ 0x%08X.\n", i, Size, (ptr+4)));
    return (void *)(ptr+4);
#else /* XA_DEBUG == XA_ENABLED */

    return malloc(Size);
#endif /* XA_DEBUG == XA_ENABLED */
}

/*---------------------------------------------------------------------------
 * Deallocates or frees a memory block.
 */
void OS_Free(void *MemBlock)
{
#if XA_DEBUG == XA_ENABLED
    U16  size, i;
    U8  *ptr = (U8 *)MemBlock - 4;

    size = *(U16 *)ptr;     /* Bytes 0 & 1 */

    if (ptr[2] != 0xFE || ptr[3] != 0xED) {
        Report(("Buffer Header corrupted.\n"));
        Assert(0);
    }

    if (ptr[size+4] != 0xF0 || ptr[size+5] != 0x0D) {
        Report(("Buffer Trailer corrupted.\n"));
        Assert(0);
    }

    for (i = 0; i < OS_ALLOC_BLOCK_TABLE_SIZE; i++) {
        if (allocBlocks[i] == ptr) {
            allocBlocks[i] = 0;
            break;
        }
    }
    Assert(i < OS_ALLOC_BLOCK_TABLE_SIZE);
    Assert(allocBytes >= size);

    allocBytes -= size;

    free(ptr);
#else /* XA_DEBUG == XA_ENABLED */

    free(MemBlock);
#endif /* XA_DEBUG == XA_ENABLED */
}

#if XA_DEBUG == XA_ENABLED
/*---------------------------------------------------------------------------
 * Check if the OS Library has any memory blocks in use.
 */
void OS_LibCheck(void)
{
    U16  i, size, blocks = 0;
    U8  *ptr;

    Report(("OS_LibCheck(): %d bytes outstanding. Peak usage %d bytes.\n", allocBytes, allocMax));
    if (allocBytes) {
        Report(("Allocator: Block List:\n"));
        for (i = 0; i < OS_ALLOC_BLOCK_TABLE_SIZE; i++) {
            if (allocBlocks[i] == 0)
                continue;

            blocks++;

            size = *(U16 *)allocBlocks[i];
            ptr = (U8 *)allocBlocks[i] + 4;

            Report(("       Block #%2d @ 0x%08X, Size %2d: \"", i, ptr, size));
            while (size--)
                Report(("%c", *ptr++));
            Report(("\"\n", ptr++));
        }
    }
    Assert(!allocBytes && !blocks);

    allocBytes = allocMax = 0;
    OS_MemSet((U8 *)&allocBlocks, 0, sizeof(allocBlocks));
}
#endif /* XA_DEBUG == XA_ENABLED */