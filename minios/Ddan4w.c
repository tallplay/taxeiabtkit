#include "bttypes.h"
#include "utils.h"
#include "ddan.h"

#include "windows.h"
#include "stdlib.h"
#include "stdio.h"
#include "stdarg.h"

/****************************************************************************
 *
 * Constants
 *
 ****************************************************************************/

#define DDAN_MAX_ENTRIES  40
#define DDAN_NOT_FOUND    0xffff

/****************************************************************************
 *
 * Data
 *
 ****************************************************************************/

/* The device database is kept in memory. */
static I16            numDdanRecs = 0;
static BtAddrNamePair DevDb[DDAN_MAX_ENTRIES];
static char           DbName[BDADDR_NTOA_SIZE + 22] = {0};

/****************************************************************************
 *
 * Functions
 *
 ****************************************************************************/

static I16  DdanFindRecord(const BD_ADDR *bdAddr);
static void DdanCreateName(const BD_ADDR *bdAddr);


/*---------------------------------------------------------------------------
 *            DDAN_Open()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Open the device database. 
 *
 * Return:    BT_STATUS_SUCCESS if success otherwise BT_STATUS_FAILED.
 */
BtStatus DDAN_Open(const BD_ADDR *bdAddr)
{
    BtStatus status;
    FILE     *fp = NULL;
    I16      count;

    /* See if the device database file exists. If it does not exist then
     * create a new one.
     */
    status = BT_STATUS_FAILED;
    numDdanRecs = 0;

    /* Create the ddb file based on the radio's BD_ADDR */
    DdanCreateName(bdAddr);
    fp = fopen(DbName, "rb");
    if (fp == NULL) {
        /* The file does not exist so we are done */
        Report(("DeviceAN: Device Database file does not exist\n"));
        status = BT_STATUS_SUCCESS;
        goto done;
    } else {
        Report(("DeviceAN: Device database file opened\n"));
    }

    /* Read in the contents of the database file. The first item in the 
     * database file is the number of items 
     */
    fread(&numDdanRecs, sizeof(numDdanRecs), 1, fp);
    if (ferror(fp) || (numDdanRecs > DDAN_MAX_ENTRIES)) {
        Report(("DeviceAN: Error reading the number of items\n"));
        /* Reset the number of records in the database, since 
         * reading the number of items failed. 
         */
        numDdanRecs = 0;
        goto close;
    }
    Report(("DeviceAN: Database contains %d records\n", numDdanRecs));

    /* Read the elements */
    for (count = 0; count < numDdanRecs; count++) {
        fread(&(DevDb[count]), sizeof(BtAddrNamePair), 1, fp);
        if (ferror(fp)) {
            Report(("DeviceAN: Error reading item %d\n",count));
            /* Set the number of records in the database to
             * the position that just failed.  Our next additional
             * record will start here.
             */
            numDdanRecs = count;
            goto close;
        }
    }
    status = BT_STATUS_SUCCESS;
close:
    fclose(fp);
done:
    return status;
}

/*---------------------------------------------------------------------------
 *            DDAN_Close()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Close the device database. 
 *
 * Return:    BT_STATUS_SUCCESS if success otherwise BT_STATUS_FAILED.
 */
BtStatus DDAN_Close(void)
{
    BtStatus status;

    status = BT_STATUS_FAILED;

    if (DDAN_Flush() == BT_STATUS_SUCCESS)
    {
        status = BT_STATUS_SUCCESS;
    }

    DbName[0] = 0;
    numDdanRecs = 0;
    return status;
}


/*---------------------------------------------------------------------------
 *            DDAN_Flush()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Force the database to be written to disk. 
 *
 * Return:    BT_STATUS_SUCCESS if success otherwise BT_STATUS_FAILED.
 */
BtStatus DDAN_Flush(void)
{
    FILE     *fp = NULL;
    BtStatus status;
    I16      count;

    status = BT_STATUS_FAILED;

    /* Create the database file and copy in the entries if any exist */
    if (DbName[0] == 0) {
        goto done;
    }
    Assert(numDdanRecs <= DDAN_MAX_ENTRIES);

    fp = fopen(DbName,"wb");
    Report(("DeviceAN: Creating Device database file %s.\n", DbName));
    if (fp == NULL) {
        Report(("Error creating %s.", DbName));
        goto done;
    }

    /* Write out the number of items */
    fwrite(&numDdanRecs, sizeof(numDdanRecs), 1, fp);
    if (ferror(fp)) {
        Report(("DeviceAN: Error writing numDdanRecs to database file\n"));
        goto close;
    }

    /* Write the entries */
    for (count = 0; count < numDdanRecs; count++) {
        fwrite(&(DevDb[count]), sizeof(BtAddrNamePair), 1, fp);
        if (ferror(fp)) {
            Report(("DeviceAN: Error writing entry %d\n", count));
            goto close;
        }
    }
    Report(("DeviceAN: Wrote %d items to database\n", count));
    status = BT_STATUS_SUCCESS;
close:
    fclose(fp);
done:
    return status;
}


/*---------------------------------------------------------------------------
 *            DDAN_AddRecord()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Add a record to the database. 
 *
 * Return:    BT_STATUS_SUCCESS if success otherwise BT_STATUS_FAILED.
 */
BtStatus DDAN_AddRecord(const BtAddrNamePair* record)
{
    I16 count;

    count = DdanFindRecord(&(record->bdAddr));
    if (count == DDAN_NOT_FOUND) {
        /* A record with this bdAddr does not exist so add it */
        count = numDdanRecs;
        numDdanRecs++;
    }
    DevDb[count] = *record;
    if (DDAN_Flush() != BT_STATUS_SUCCESS) {
        Report(("DeviceAN: Error saving record.", count));
    }

    return BT_STATUS_SUCCESS;
}



/*---------------------------------------------------------------------------
 *            DDAN_FindRecord()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Find the record that has the given BD_ADDR. 
 *
 * Return:    BT_STATUS_SUCCESS if success otherwise BT_STATUS_FAILED.
 */
BtStatus DDAN_FindRecord(const BD_ADDR *bdAddr, BtAddrNamePair* record)
{
    I16 count;

    count = DdanFindRecord(bdAddr);

    if (count != DDAN_NOT_FOUND) {
        *record = DevDb[count];
        return BT_STATUS_SUCCESS;
    }
    return BT_STATUS_FAILED;
}

/*---------------------------------------------------------------------------
 *            DDAN_DeleteRecord()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Delete a record from the database. 
 *
 * Return:    BT_STATUS_SUCCESS if success otherwise BT_STATUS_FAILED.
 */
BtStatus DDAN_DeleteRecord(const BD_ADDR *bdAddr)
{
    I16 count;

    count = DdanFindRecord(bdAddr);
    if (count != DDAN_NOT_FOUND) {
        /* If it is the last record then all we need to do is reduce
         * numDdanRecs by 1. Otherwise we need to shift the array
         */
        numDdanRecs--;
        if (count < numDdanRecs) {
            /* We need to shift the array */
            OS_MemCopy((U8*)(&DevDb[count]), (U8*)(&DevDb[count+1]),
                       (U16)((numDdanRecs - count) * sizeof(BtAddrNamePair)));
        }
        return BT_STATUS_SUCCESS;
    }
    return BT_STATUS_FAILED;
}


/*---------------------------------------------------------------------------
 *            DDAN_EnumDeviceRecords()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Enumerate the records in the device database. 
 *
 * Return:    status of the operation.
 */
BtStatus DDAN_EnumDeviceRecords(I16 i, BtAddrNamePair* record)
{
    if (i >= numDdanRecs) {
        return BT_STATUS_FAILED;
    } else {
        *record = DevDb[i];
    }
    return BT_STATUS_SUCCESS;
}

/*---------------------------------------------------------------------------
 *            DdanFindRecord()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Find the record that has the given BD_ADDR. 
 *
 * Return:    Index of BtAddrNamePair if found otherwise DDAN_NOT_FOUND.
 */
static I16 DdanFindRecord(const BD_ADDR *bdAddr)
{
    I16 count;

    for (count = 0; count < numDdanRecs; count++) {
        if (OS_MemCmp(bdAddr->addr, 6, DevDb[count].bdAddr.addr, 6)) {
            /* The record is found so return it */
            return count;
        }
    }
    return DDAN_NOT_FOUND;
}

/*---------------------------------------------------------------------------
 *            DdanCreateName()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Creates a database filename based on the radio's bluetooth
 *            device address.
 */
#define DDANNAME "\\DDAN."

static void DdanCreateName(const BD_ADDR *bdAddr)
{
    char    *bp;
    U8       u, l, i;

    strcpy(DbName, DDANNAME);
    bp = DbName + sizeof(DDANNAME)-1;

    for (i = 6; i > 0; i--) {
        u = (U8)(bdAddr->addr[i-1]/16);
        l = (U8)(bdAddr->addr[i-1]%16);
        
        if (u < 10)
            *bp++ = (U8)('0' + u);
        else *bp++ = (U8)('A' + u - 10);
        
        if (l < 10)
            *bp++ = (U8)('0' + l);
        else *bp++ = (U8)('A' + l - 10);
        *bp++ = '.';
    }
    *bp = 0;

    strcat(DbName, "ddb");
}


