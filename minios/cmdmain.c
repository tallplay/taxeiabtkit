/***************************************************************************
 *
 * File:
 *     $Workfile:cmdmain.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:30$
 *
 * Description:
 *     This file contains the main program for sample applications.
 *
 * Created:
 *     March 3, 2000
 *
 * Copyright 2000-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "osapi.h"
#include "windows.h"
#include "stdio.h"
#include "conio.h"
#include "signal.h"

/*
 * We include the Standard Options Parser here because it is an 
 * integral part of the functionality performed in this file.
 */
#include "parseopts.c"

#ifndef APP_PARSES_OPTIONS
#define APP_PARSES_OPTIONS  XA_DISABLED
#endif

/*---------------------------------------------------------------------------
 *
 * Function prototypes
 */
/*
 * Application functions 
 */
BOOL  APP_ParseCommandLine(const char **str);
BOOL  APP_Init(void);
void  APP_Deinit(void);
BOOL  APP_Thread(void);
char *APP_GetOptionString(void);
char *APP_GetExampleString(void);
void  APP_PrintTitle(void);

/*
 * MINIOS functions
 * If multitasking is disabled, we'll use the MINIOS for threading
 */
#if XA_MULTITASKING == XA_DISABLED
void MINIOS_RemoveThread(PFV thread);
BOOL MINIOS_AddThread(PFV thread);
void MINIOS_Run(void);
#endif

/*
 * Internal functions
 */
static BOOL ParseCommandLine(int argc, char *argv[]);
static void PrintHelp(const char* name);
static void ctrlchandler( int sig );
static DWORD WINAPI AppThread(LPVOID lpParameter);

void PumpMessages(void);

/*---------------------------------------------------------------------------
 *             main()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  The main program. This function sets up the com port and
 *            initiailizes the Mini OS which in turn initializes the stack.
 *            It then initializes the application, adds the application thread
 *            to the OS, and starts the Mini OS.
 *
 * Return:    Status of the operation.
 */
int main(int argc, char **argv)
{
#if XA_MULTITASKING == XA_ENABLED
    HANDLE  appThreadHndl;
    DWORD   appThreadId;
    BOOL    appThreadActive;
#endif

    APP_PrintTitle();

    /* Parse the command line */
    if (!ParseCommandLine(argc, argv)) {
        goto Exit;
    }

    /* Set up Ctrl-C Handler */
    signal(SIGINT, ctrlchandler );

    /* Initialize the operating system which initializes the stack */
    if (!OS_Init()) {
        printf("Fatal Error: Could not initialize the stack\n");
        goto Exit;
    }

#if XA_MULTITASKING == XA_ENABLED
    /*
     * In multitasking with the inet driver, we need to continue processing
     * Windows messages during APP_Deinit(); therefore, we'll handle application
     * initialization and deinitialization in AppThread().
     */
    appThreadActive = TRUE;
    appThreadHndl = CreateThread(0, 4096, AppThread, &appThreadActive, 0, &appThreadId);
    Assert(appThreadHndl);

    while (appThreadActive) {
#else

    /* Initialize the application */
    if (APP_Init()) {
        while (APP_Thread()) {
#endif

            /* Just take a quick nap to keep from running
             * the processor at 100%.
             */
            Sleep(20);
         
            /*
             * Since this main() procedure is for command-line based 
             * applications, it is not necessary to provide a windows
             * message loop. However, the INet Host controler uses a
             * hidden window to receive winsock events, so it requires
             * a message loop. Therefore when running with the INet
             * Host controller, this is necessary.
             */
            PumpMessages();

#if XA_MULTITASKING == XA_DISABLED
            /* Run the minios threads (including the Event manager). */
            MINIOS_Run();
#endif
        }

        Report(("Deinitializing\n"));

#if XA_MULTITASKING == XA_ENABLED
        if (appThreadHndl)
            CloseHandle(appThreadHndl);
#else
        APP_Deinit();
    }
#endif

    OS_Deinit();

Exit:
    printf("\nThe application has exited. Press any key to close.\n");
    while (!kbhit()) ;
    return 1;

} /* End of main() */


/*---------------------------------------------------------------------------
 *             PumpMessages()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  This function is responsible for processing window messages.
 *
 */
void PumpMessages(void)
{
    MSG     w_msg;

    while (PeekMessage(&w_msg, NULL, 0, 0, PM_NOREMOVE)) {
        if (GetMessage(&w_msg, NULL, 0, 0) == 0) {
            break;
        }
        TranslateMessage( &w_msg );
        DispatchMessage( &w_msg );
    }
}

/*---------------------------------------------------------------------------
 *            ParseCommandLine()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Parse the command line for options that are stack (framer)
 *            dependent.
 *
 * Return:    TRUE - the application should continue
 *            FALSE - an error occurred and the application should abort.
 */
static BOOL ParseCommandLine(int argc, char *argv[])
{
    char *str;
    int   i;
    char  name[_MAX_FNAME];

    _splitpath(argv[0],0,0,name,0);

    if (argc > 1) {
        /* We have some parameters */
        for (i = 1; i < argc; i++) {

            /* Parse standard command line options */
            if (ParseStandardOptions(argv+i))
                continue;

            /* Parse for -h Help option */
            str = argv[i];
            if (((str[0] == '-') || (str[0] == '/')) && (str[1] == 'h')) {
                if (str[2] == 0) {
                    PrintHelp(name);
                    return FALSE;
                }
            }
            
#if APP_PARSES_OPTIONS == XA_ENABLED
            /* Parse application specific options */
            if (APP_ParseCommandLine(&argv[i]))
                continue;
#endif
           printf("Command-line option error:\n %s.\n", getErrorMessage());
           PrintHelp(name); 
            return FALSE;
        }
    }
    return TRUE;

} /* End of ParseCommandLine() */


/*---------------------------------------------------------------------------
 *            PrintHelp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Print a help message for the application.
 *
 * Return:    name - name of function
 */
static void PrintHelp(const char *name)
{
#if APP_PARSES_OPTIONS == XA_ENABLED
    /* First print the usage line */
    printf("\nUsage: %s [-option [-option]] %s\n\n", 
           name, APP_GetOptionString());
#else
    printf("\nUsage: %s [-option [-option]]\n\n", name);
#endif
    /* Now print the options */
    printf("Options: \n");

    /* Print the standard options */
    printf(getStandardOptions());

    /* Print the help message */
    printf("   -h                   Print this help message\n\n");

#if APP_PARSES_OPTIONS == XA_ENABLED
    /* Print the example string */
    printf("Example: %s %s\n\n", name, APP_GetExampleString());
#endif
} /* End of PrintHelp() */


/*
 *  Use this thread so as not to pollute the real APP_Thread with OS-specifics.
 */
static DWORD WINAPI AppThread(LPVOID active)
{
    if (APP_Init()) {

        while (APP_Thread())  {
            // Sleep for a bit; the application probably doesn't need to run
            // that often.
            Sleep(100);
        }

        APP_Deinit();
    }
    /* App has exited, tell main thread to exit */
    *((BOOL *)active) = FALSE;
    return 0;
}


/*
 * Ctrl-C Handler 
 */
static void ctrlchandler( int sig )
{
    signal( SIGINT, SIG_IGN );
    signal( SIGINT, ctrlchandler );   

    /* Exit gracefully */
    APP_Deinit();
    OS_Deinit();
}

