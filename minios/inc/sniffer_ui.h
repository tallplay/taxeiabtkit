/***************************************************************************
 *
 * File:
 *     $Workfile:sniffer_ui.h$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:18$
 *
 * Description:
 *     This is file contains the function prototypes and definitions
 *     for the Windows Protocol Sniffer UI. It can be used for both
 *       GUI and CLI apps.
 * 
 * Copyright 1999-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#ifndef __SNIFFER_UI_H
#define __SNIFFER_UI_H

#include "config.h"


/****************************************************************************
 *
 * Function Reference
 *
 ****************************************************************************/

//#if XA_DEBUG == XA_ENABLED && HCI_ALLOW_PRESCAN == XA_ENABLED
#if HCI_ALLOW_PRESCAN == XA_ENABLED
//#if defined (_WINDOWS_)
#if defined (WIN32)
#include "windows.h"
/*---------------------------------------------------------------------------
 * StartSniffer()
 *
 *     Initializes the Bluetooth Protocol Sniffer and creates the sniffer
 *     window. This function is exists only in the Windows environment.
 *
 * Parameters:
 *     hInst - Handle to the application instance.
 *     cmdShow - Show state of the Sniffer window.
 *
 * Returns:
 *     non-zero - Handle of the sniffer window.
 *     zero     - An error occured initializing the sniffer.
 */
HWND StartSniffer(HINSTANCE hInst, int cmdShow); /* IGNORESTYLE */

/*---------------------------------------------------------------------------
 * GetSnifferWindow()
 *
 *     This function returns the sniffer window handle. If the sniffer is
 *     not open zero is returned;
 */
HWND GetSnifferWnd(void);

//#endif  /* defined (_WINDOWS_) */   tallplay mark


/*---------------------------------------------------------------------------
 * SnifferReset()
 *
 *     This function resets the state of the sniffer packet decoder and 
 *     connection tables.
 *
 * Requires:
 *     XA_DEBUG enabled.
 *
 * Parameters:
 *     Full - When TRUE the connection table is reset along with the packet
 *         decoders. When FALSE, only the packet decoders are reset.
 *         Typically FALSE is used, unless a critical sniffer error is
 *         encountered and a full reset is necessary.
 */
void SnifferReset(BOOL Full);


/*---------------------------------------------------------------------------
 * StopSniffer()
 *
 *     In the Windows environment, this function closes the Bluetooth
 *     Protocol Sniffer and destroys its output window. In the command
 *     line environment it closes the logger file.
 */
void StopSniffer(void);


/*---------------------------------------------------------------------------
 * SnifferHandleDeinit()
 *
 *     Disconnects the sniffer from the stack before OS_Deinit is to be
 *     called.
 *
 *     This is necessary if an application wants to deinitialize and then
 *     later re-initialize the stack without confusing the sniffer.
 */
void SnifferHandleDeinit(void);

/*---------------------------------------------------------------------------
 * SnifferHandleReinit()
 *
 *     Reconnects the sniffer to the stack AFTER OS_Init is called.
 */
BOOL SnifferHandleReinit(void);

/*---------------------------------------------------------------------------
 * IsSnifferOpen()
 *
 *     Returns true if the sniffer/logger is currently running.
 */
BOOL IsSnifferOpen(void);


/*---------------------------------------------------------------------------
 * StartSnifferLogging()
 *
 *     Initializes the Bluetooth Protocol Sniffer and creates the specified
 *     file. This function is exists only in the command line environment.
 *     This function is the command line equivalent of StartSniffer().
 *
 * Parameters:
 *     hInst - Handle to the application instance.
 *     cmdShow - Show state of the Sniffer window.
 *
 * Returns:
 *     non-zero - Handle of the sniffer window.
 *     zero     - An error occured initializing the sniffer.
 */
BOOL StartSnifferLogging(const char *Filename);
#else
#define StartSniffer(h, c)      ((void) 0)
#define SnifferReset(f)         ((void) 0)
#define StopSniffer()           ((void) 0)
#define IsSnifferOpen()         (FALSE)
#define StartSnifferLogging(f)  (FALSE)
#define GetSnifferWnd()         ((HWND) 0)
#endif  /* defined (_WINDOWS_) */  //tallplay add

#else
#define StartSniffer(h, c)      ((void) 0)
#define SnifferReset(f)         ((void) 0)
#define StopSniffer()           ((void) 0)
#define IsSnifferOpen()         (FALSE)
#define StartSnifferLogging(f)  (FALSE)
#define GetSnifferWnd()         ((HWND) 0)

#endif /* XA_DEBUG == XA_ENABLED */

#define antiquewhite        0xD7EBFA
#define aqua        0xffff00
#define aquamarine        0xD4FF7F
#define azure        0xFFFFF0
#define beige        0xDCF5F5
#define bisque        0xC4E4FF
#define black        0x000000
#define blanchedalmond          0xCDEBFF
#define blue        0xFF0000
#define blueviolet        0xE22B8A
#define brown        0x2A2AA5
#define burlywood        0x87B8DE
#define cadetblue        0xA09E5F
#define chartreuse        0x00FF7F
#define chocolate        0x1E69D2
#define coral        0x507FFF
#define cornflowerblue          0xED9564
#define cornsilk        0xDCF8FF
#define crimson        0x0000dc
#define cyan        0xFFFF00
#define darkblue        0x8B0000
#define darkcyan        0x8B8B00
#define darkgoldenrod          0x0B86B8
#define darkgray        0xA9A9A9
#define darkgreen        0x006400
#define darkkhaki        0x6BB7BD
#define darkmagenta        0x8B008B
#define darkolivegreen          0x2F6B55
#define darkorange        0x008CFF
#define darkorchid        0xCC3299
#define darkred        0x00008B
#define darksalmon        0x7A96E9
#define darkseagreen        0x006400
#define darkslateblue          0x8B3D48
#define darkslategray          0x4F4F2F
#define darkturquoise          0xD1CE00
#define darkviolet        0xD30094
#define deeppink        0x9314FF
#define deepskyblue        0xFFBF00
#define dimgray        0x696969
#define dodgerblue        0xFF901E
#define firebrick        0x2222B2
#define floralwhite        0xF0FAFF
#define forestgreen        0x228B22
#define Fuchsia        0xff00ff
#define gainsboro        0xDCDCDC
#define ghostwhite        0xFFF8F8
#define gold        0xAAE8EE
#define goldenrod        0x20A5DA
#define gray        0x808080
#define green        0x008000
#define greenyellow        0x2FFFAD
#define honeydew        0xF0FFF0
#define hotpink        0xB469FF
#define indianred        0x5C5CCD
#define indigo        0x800045
#define ivory        0xF0FFFF
#define khaki        0x8CE6F0
#define lavender        0xFAE6E6
#define lavenderblush          0xF5F0FF
#define lawngreen        0x00FC7C
#define lemonchiffon        0xCDFAFF
#define lightblue        0xE6D8AD
#define lightcoral        0x8080F0
#define lightcyan        0xFFFFE0
#define lightgoldenrodyellow          0xD2FAFA
#define lightgreen        0x90EE90
#define lightgrey        0xD3D3D3
#define lightpink        0xC1B6FF
#define lightsalmon        0x7AA0FF
#define lightseagreen          0xAAB220
#define lightskyblue        0xFACE87
#define lightslategray          0x998877
#define lightsteelblue          0xDEC4B0
#define lightyellow        0xE0FFFF
#define lime        0x00ff00
#define limegreen        0x32CD32
#define linen        0xE6F0FA
#define magenta        0xFF00FF
#define maroon        0x000080
#define mediumaquamarine          0xAACD66
#define mediumblue        0xCD0000
#define mediumorchid        0xD355BA
#define mediumpurple        0xDB7093
#define mediumseagreen          0x71B33C
#define mediumslateblu          0xEE687Be
#define mediumspringgreen          0x9AFA00
#define mediumturquois          0xCCD148e
#define mediumvioletre          0x8515C7d
#define midnightblue        0x701919
#define mintcream        0xFAFFF5
#define mistyrose        0xE1E4FF
#define moccasin        0xB5E4FF
#define navajowhite        0xADDEFF
#define navy        0x800000
#define oldlace        0xE6F5FD
#define olive        0x008080
#define olivedrab        0x238E6B
#define orange        0x00A5FF
#define orangered        0x0045FF
#define orchid        0xD670DA
#define palegoldenrod          0xAAE8EE
#define palegreen        0x98FB98
#define paleturquoise          0xEEEEAF
#define palevioletred          0x9370DB
#define papayawhip        0xD5EFFF
#define peachpuff        0xB9DAFF
#define peru        0x3F85CD
#define pink        0xCBC0FF
#define plum        0xDDA0DD
#define powderblue        0xE6E0B0
#define purple        0x800080
#define red        0x0000ff
#define rosybrown        0x8F8FBC
#define royalblue        0xE16941
#define saddlebrown        0x13458B
#define salmon        0x7280FA
#define sandybrown        0x60A4F4
#define seagreen        0x578B2E
#define seashell        0xEEF5FF
#define sienna        0x2D52A0
#define silver        0xc0c0c0
#define skyblue        0xEBCE87
#define slateblue        0x8B3D48
#define slategray        0x908070
#define snow        0xFAFAFF
#define springgreen        0x7FFF00
#define steelblue        0xB48246
#define tan        0x8CB4D2
#define teal        0x808000
#define thistle        0xD8BFD8
#define tomato        0x4763FF
#define turquoise        0xD0E040
#define violet        0xEE82EE
#define wheat        0xB3DEF5
#define white        0xffffff
#define whitesmoke        0xF5F5F5
#define yellow        0x00ffff
#define yellowgreen        0x32CD9A

#endif        /* __SNIFFER_UI_H */

