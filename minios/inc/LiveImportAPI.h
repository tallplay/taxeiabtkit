// LiveImportAPI.h : Defines the initialization routines for the DLL.
//


#include "stdio.h"

#include "tchar.h"
#include "DriverNotifications.h"

// Type/value definitions:
#ifndef    __cplusplus
#define    bool BYTE
#define    true ((bool) 1)
#define    false ((bool) 0)
#endif

// API function pointer definitions:
typedef HRESULT (GetDllVersion)(TCHAR** pszDllVersion, int* piSize);
typedef HRESULT (InitializeLiveImport)(const TCHAR* szMemoryName, const TCHAR* szConfiguration, bool* pboolSuccess);
typedef HRESULT (ReleaseLiveImport)(void);
typedef HRESULT (StillAlive)(bool* pboolIsAppAlive);
typedef HRESULT (IsAppReady)(bool* pboolIsAppReady);

typedef HRESULT (SendFrame)(
        int iOriginalLength,                // The "real" length of a frame. Some frames may be truncated, so this may not be the same as included length
        int iIncludedLength,                // The size of the data passed in this call
        // TBD-HACK-PER: Probably not the approved way of passing byte arrays.
        const BYTE* pbytFrame,                        // The actual bytes of the frame
        int iDrf,                            // any errors or other data related flag
        int iStream,                        // Which side this data comes from
        __int64 i64Timestamp);

typedef HRESULT (SendEvent)(BYTE bytData, int iDrf, int iStream, __int64 i64Timestamp);
typedef HRESULT (SendControlSignalChange)(int iNdrf, __int64 i64Timestamp);
typedef HRESULT (SendNondataFlagChange)(int iNdrf, __int64 i64Timestamp);
typedef HRESULT (SendBreak)(int iStream, __int64 i64Timestamp);
typedef HRESULT (SendFlowControl)(bool boolFlowControlIsOn, __int64 i64Timestamp);
typedef HRESULT (SendPower)(int iLevel, __int64 i64Timestamp);
typedef HRESULT (SendConnectionStatus)(bool boolIsConnected, __int64 i64Timestamp);
typedef HRESULT (SendConfigurationString)(const TCHAR* szConfiguration);
typedef HRESULT (SendSpecialEvent)(int iStream, int iEventNumber, __int64 i64Timestamp);
typedef HRESULT (SendStartOfFrame)(int iStream, __int64 i64Timestamp);
typedef HRESULT (SendEndOfFrame)(int iStream, __int64 i64Timestamp);
typedef HRESULT (SendAbortedFrame)(int iStream, __int64 i64Timestamp);
typedef HRESULT (SendByteSimple)(BYTE byData, __int64 i64Timestamp);
typedef HRESULT (CheckForMessages)();
typedef HRESULT (GetAppVersionNumber)(TCHAR** pszAppVersionNumber, int* piSize);
typedef HRESULT (GetAppSerialNumber)(TCHAR** pszAppSerialNumber, int* piSize);
typedef HRESULT (GetAppDisplayedConfigurationName)(TCHAR** pszAppConfigName, int* piSize);
typedef HRESULT (GetSerialNumberSectionKeyValuePairs)(TCHAR** pszKeyValuePairs, int* piSize);
typedef HRESULT (RegisterNotification)(eNotificationTypes eType, NotificationType pNotification, void* pThis);
typedef HRESULT (SendNotification)(eNotificationTypes eType);
typedef HRESULT (SendArraySimple)(BYTE* pbytData, int iLength, __int64 i64Timestamp);
typedef HRESULT (SendStringSimple)(TCHAR* szData, __int64 i64Timestamp);
typedef HRESULT (SendNumberOfLostMessages)(const int iNumberOfLostMessages);
typedef HRESULT (UpdateStat)(int iStream, int iStatNumber, __int64 i64IncrementAmount);
typedef HRESULT (FramesLost)(int iFramesLost);
typedef HRESULT (SetExePath)(TCHAR* szServerPath);

// API function pointer variable definitions:
GetDllVersion* g_pGetDllVersion= NULL;
InitializeLiveImport* g_pInitializeLiveImport= NULL;
ReleaseLiveImport* g_pReleaseLiveImport= NULL;
StillAlive* g_pStillAlive= NULL;
IsAppReady* g_pIsAppReady= NULL;
SendFrame* g_pSendFrame= NULL;
SendEvent* g_pSendEvent= NULL;
SendControlSignalChange* g_pSendControlSignalChange= NULL;
SendNondataFlagChange* g_pSendNondataFlagChange= NULL;
SendBreak* g_pSendBreak= NULL;
SendFlowControl* g_pSendFlowControl= NULL;
SendConnectionStatus* g_pSendConnectionStatus= NULL;
SendConfigurationString* g_pSendConfigurationString= NULL;
SendSpecialEvent* g_pSendSpecialEvent= NULL;
SendStartOfFrame* g_pSendStartOfFrame= NULL;
SendEndOfFrame* g_pSendEndOfFrame= NULL;
SendAbortedFrame* g_pSendAbortedFrame= NULL;
SendByteSimple* g_pSendByteSimple= NULL;
CheckForMessages* g_pCheckForMessages= NULL;
GetAppVersionNumber* g_pGetAppVersionNumber= NULL;
GetAppSerialNumber* g_pGetAppSerialNumber= NULL;
GetAppDisplayedConfigurationName* g_pGetAppDisplayedConfigurationName= NULL;
GetSerialNumberSectionKeyValuePairs* g_pGetSerialNumberSectionKeyValuePairs= NULL;
RegisterNotification* g_pRegisterNotification= NULL;
SendNotification* g_pSendNotification= NULL;
SendArraySimple* g_pSendArraySimple= NULL;
SendStringSimple* g_pSendStringSimple= NULL;
SendNumberOfLostMessages* g_pSendNumberOfLostMessages= NULL;
UpdateStat* g_pUpdateStat= NULL;
FramesLost* g_pFramesLost= NULL;
SetExePath* g_pSetExePath= NULL;

// Library definitions:
TCHAR* g_pszLibraryName= _T(".\\LiveImportAPI.dll");
HMODULE g_hLiveImportAPI= NULL;

bool ShowFailMessage(char* pszProcName)
{
    TCHAR szError[1024];
    _stprintf(szError, _T("Failed to get address of function \"%s\"."), (TCHAR*)pszProcName);
    MessageBox(NULL, szError, _T("Error"), MB_OK);
    FreeLibrary(g_hLiveImportAPI);
    return (false);
}

bool LoadLiveImportAPIFunctions(void)
{
    char* pszProcName= NULL;
    TCHAR szError[1024];
    DWORD dwError= ERROR_SUCCESS;
    
    g_hLiveImportAPI= LoadLibrary(g_pszLibraryName);
    if(NULL == g_hLiveImportAPI)
    {
        dwError= GetLastError();
        _stprintf(szError, _T("Failed to load module \"%s\" - error %d"), g_pszLibraryName, dwError);
        MessageBox(NULL, szError, _T("Error"), MB_OK);
        return (false);
    }

    pszProcName= "InitializeLiveImport";
    g_pInitializeLiveImport= (InitializeLiveImport*)GetProcAddress(g_hLiveImportAPI, pszProcName);
    if(NULL == g_pInitializeLiveImport)
        return (ShowFailMessage(pszProcName));
    
    pszProcName= "ReleaseLiveImport";
    g_pReleaseLiveImport= (ReleaseLiveImport*)GetProcAddress(g_hLiveImportAPI, pszProcName);
    if(NULL == g_pReleaseLiveImport)
        return (ShowFailMessage(pszProcName));
    
    pszProcName= "StillAlive";
    g_pStillAlive= (StillAlive*)GetProcAddress(g_hLiveImportAPI, pszProcName);
    if(NULL == g_pStillAlive)
        return (ShowFailMessage(pszProcName));

    pszProcName= "IsAppReady";
    g_pIsAppReady= (IsAppReady*)GetProcAddress(g_hLiveImportAPI, pszProcName);
    if(NULL == g_pIsAppReady)
        return (ShowFailMessage(pszProcName));
    
    pszProcName= "SendFrame";
    g_pSendFrame= (SendFrame*)GetProcAddress(g_hLiveImportAPI, pszProcName);
    if(NULL == g_pSendFrame)
        return (ShowFailMessage(pszProcName));
    
    pszProcName= "SendEvent";
    g_pSendEvent= (SendEvent*)GetProcAddress(g_hLiveImportAPI, pszProcName);
    if(NULL == g_pSendEvent)
        return (ShowFailMessage(pszProcName));
    
    pszProcName= "SendControlSignalChange";
    g_pSendControlSignalChange= (SendControlSignalChange*)GetProcAddress(g_hLiveImportAPI, pszProcName);
    if(NULL == g_pSendControlSignalChange)
        return (ShowFailMessage(pszProcName));
    
    pszProcName= "SendNondataFlagChange";
    g_pSendNondataFlagChange= (SendNondataFlagChange*)GetProcAddress(g_hLiveImportAPI, pszProcName);
    if(NULL == g_pSendNondataFlagChange)
        return (ShowFailMessage(pszProcName));
    
    pszProcName= "SendBreak";
    g_pSendBreak= (SendBreak*)GetProcAddress(g_hLiveImportAPI, pszProcName);
    if(NULL == g_pSendBreak)
        return (ShowFailMessage(pszProcName));
    
    pszProcName= "SendFlowControl";
    g_pSendFlowControl= (SendFlowControl*)GetProcAddress(g_hLiveImportAPI, pszProcName);
    if(NULL == g_pSendFlowControl)
        return (ShowFailMessage(pszProcName));
    
    pszProcName= "SendConnectionStatus";
    g_pSendConnectionStatus= (SendConnectionStatus*)GetProcAddress(g_hLiveImportAPI, pszProcName);
    if(NULL == g_pSendConnectionStatus)
        return (ShowFailMessage(pszProcName));
    
    pszProcName= "SendConfigurationString";
    g_pSendConfigurationString= (SendConfigurationString*)GetProcAddress(g_hLiveImportAPI, pszProcName);
    if(NULL == g_pSendConfigurationString)
        return (ShowFailMessage(pszProcName));
    
    pszProcName= "SendSpecialEvent";
    g_pSendSpecialEvent= (SendSpecialEvent*)GetProcAddress(g_hLiveImportAPI, pszProcName);
    if(NULL == g_pSendSpecialEvent)
        return (ShowFailMessage(pszProcName));
    
    pszProcName= "SendControlSignalChange";
    g_pSendControlSignalChange= (SendControlSignalChange*)GetProcAddress(g_hLiveImportAPI, pszProcName);
    if(NULL == g_pSendControlSignalChange)
        return (ShowFailMessage(pszProcName));
    
    pszProcName= "SendStartOfFrame";
    g_pSendStartOfFrame= (SendStartOfFrame*)GetProcAddress(g_hLiveImportAPI, pszProcName);
    if(NULL == g_pSendStartOfFrame)
        return (ShowFailMessage(pszProcName));
    
    pszProcName= "SendEndOfFrame";
    g_pSendEndOfFrame= (SendEndOfFrame*)GetProcAddress(g_hLiveImportAPI, pszProcName);
    if(NULL == g_pSendEndOfFrame)
        return (ShowFailMessage(pszProcName));
    
    pszProcName= "SendAbortedFrame";
    g_pSendAbortedFrame= (SendAbortedFrame*)GetProcAddress(g_hLiveImportAPI, pszProcName);
    if(NULL == g_pSendAbortedFrame)
        return (ShowFailMessage(pszProcName));
    
    pszProcName= "SendByteSimple";
    g_pSendByteSimple= (SendByteSimple*)GetProcAddress(g_hLiveImportAPI, pszProcName);
    if(NULL == g_pSendByteSimple)
        return (ShowFailMessage(pszProcName));
    
    pszProcName= "CheckForMessages";
    g_pCheckForMessages= (CheckForMessages*)GetProcAddress(g_hLiveImportAPI, pszProcName);
    if(NULL == g_pCheckForMessages)
        return (ShowFailMessage(pszProcName));
    
    pszProcName= "GetAppVersionNumber";
    g_pGetAppVersionNumber= (GetAppVersionNumber*)GetProcAddress(g_hLiveImportAPI, pszProcName);
    if(NULL == g_pGetAppVersionNumber)
        return (ShowFailMessage(pszProcName));
    
    pszProcName= "GetAppSerialNumber";
    g_pGetAppSerialNumber= (GetAppSerialNumber*)GetProcAddress(g_hLiveImportAPI, pszProcName);
    if(NULL == g_pGetAppSerialNumber)
        return (ShowFailMessage(pszProcName));
    
    pszProcName= "GetAppDisplayedConfigurationName";
    g_pGetAppDisplayedConfigurationName= (GetAppDisplayedConfigurationName*)GetProcAddress(g_hLiveImportAPI, pszProcName);
    if(NULL == g_pGetAppDisplayedConfigurationName)
        return (ShowFailMessage(pszProcName));
    
    pszProcName= "GetSerialNumberSectionKeyValuePairs";
    g_pGetSerialNumberSectionKeyValuePairs= (GetSerialNumberSectionKeyValuePairs*)GetProcAddress(g_hLiveImportAPI, pszProcName);
    if(NULL == g_pGetSerialNumberSectionKeyValuePairs)
        return (ShowFailMessage(pszProcName));
    
    pszProcName= "RegisterNotification";
    g_pRegisterNotification= (RegisterNotification*)GetProcAddress(g_hLiveImportAPI, pszProcName);
    if(NULL == g_pRegisterNotification)
        return (ShowFailMessage(pszProcName));
    
    pszProcName= "SendNotification";
    g_pSendNotification= (SendNotification*)GetProcAddress(g_hLiveImportAPI, pszProcName);
    if(NULL == g_pSendNotification)
        return (ShowFailMessage(pszProcName));
    
    pszProcName= "SendArraySimple";
    g_pSendArraySimple= (SendArraySimple*)GetProcAddress(g_hLiveImportAPI, pszProcName);
    if(NULL == g_pSendArraySimple)
        return (ShowFailMessage(pszProcName));
    
    pszProcName= "SendStringSimple";
    g_pSendStringSimple= (SendStringSimple*)GetProcAddress(g_hLiveImportAPI, pszProcName);
    if(NULL == g_pSendStringSimple)
        return (ShowFailMessage(pszProcName));
    
    pszProcName= "SendNumberOfLostMessages";
    g_pSendNumberOfLostMessages= (SendNumberOfLostMessages*)GetProcAddress(g_hLiveImportAPI, pszProcName);
    if(NULL == g_pSendNumberOfLostMessages)
        return (ShowFailMessage(pszProcName));
    
    pszProcName= "UpdateStat";
    g_pUpdateStat= (UpdateStat*)GetProcAddress(g_hLiveImportAPI, pszProcName);
    if(NULL == g_pUpdateStat)
        return (ShowFailMessage(pszProcName));
    
    pszProcName= "FramesLost";
    g_pFramesLost= (FramesLost*)GetProcAddress(g_hLiveImportAPI, pszProcName);
    if(NULL == g_pFramesLost)
        return (ShowFailMessage(pszProcName));
    
    pszProcName= "SetExePath";
    g_pSetExePath= (SetExePath*)GetProcAddress(g_hLiveImportAPI, pszProcName);
    if(NULL == g_pSetExePath)
        return (ShowFailMessage(pszProcName));
    
    return (true);
}

void NullLiveImportFunctionPointers(void)
{
    g_pGetDllVersion= NULL;
    g_pInitializeLiveImport= NULL;
    g_pReleaseLiveImport= NULL;
    g_pStillAlive= NULL;
    g_pIsAppReady= NULL;
    g_pSendFrame= NULL;
    g_pSendEvent= NULL;
    g_pSendControlSignalChange= NULL;
    g_pSendNondataFlagChange= NULL;
    g_pSendBreak= NULL;
    g_pSendFlowControl= NULL;
    g_pSendConnectionStatus= NULL;
    g_pSendConfigurationString= NULL;
    g_pSendSpecialEvent= NULL;
    g_pSendStartOfFrame= NULL;
    g_pSendEndOfFrame= NULL;
    g_pSendAbortedFrame= NULL;
    g_pSendByteSimple= NULL;
    g_pCheckForMessages= NULL;
    g_pGetAppVersionNumber= NULL;
    g_pGetAppSerialNumber= NULL;
    g_pGetAppDisplayedConfigurationName= NULL;
    g_pGetSerialNumberSectionKeyValuePairs= NULL;
    g_pRegisterNotification= NULL;
    g_pSendNotification= NULL;
    g_pSendArraySimple= NULL;
    g_pSendStringSimple= NULL;
    g_pSendNumberOfLostMessages= NULL;
    g_pUpdateStat= NULL;
    g_pFramesLost= NULL;
    g_pSetExePath= NULL;
}
