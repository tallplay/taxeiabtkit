/*****************************************************************************
 *
 * File:
 *     $Workfile:hostctlr.h$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:89$
 *
 * Description: This file contains structures and definitions used by
 *              the inet host controller.
 *
 * Created:     Aug 13, 1999
 *
 * Copyright 1999-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any means, 
 * or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions,
 * Inc.  This work contains confidential and proprietary information of
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret,
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/
#define STRICT
#include "windows.h"
#include "winsock.h"

#include "osapi.h"
#include "utils.h"
#include "hcitrans.h"
#include "sys/hci.h"
#include "eventmgr.h"

#ifndef FAST_INQUIRY_SCAN
#define FAST_INQUIRY_SCAN   XA_ENABLED
#endif

#ifndef ALLOW_LOOPBACK
#define ALLOW_LOOPBACK      XA_DISABLED
#endif

/* since the two devices will not come out of hold at EXACTLY the same time
 * HOLDTIMER_LEEWAY is the number of system ticks leeway between the timers
 */
#ifndef HOLDTIMER_LEEWAY
#define HOLDTIMER_LEEWAY  9
#endif

/* Callback type for handling authorization messages */
struct _BtConn;
typedef void (*AuthCallback)(U16 hcp, struct _BtConn *conn,
                             void *param1, void *param2);

/* ---------------------------------------------------------------------------
 * Host Controller ACL Connection tracking structure
 */
typedef struct _BtConn {
   SOCKET        link;        /* Endpoint of ACL link */
   U16           port;        /* Peer Bluetooth device's port number */
   SOCKADDR_IN   addr;        /* IP Address of peer (in network order) */
   RxBuffHandle  hciBuffer;   /* Current receive buffer */
   U16           hciBufferLen;/* Amount of data written to hciBuffer */
   U16           hciPacketLen;/* Size of current receive packet */
   U16           packetTypes; /* BT packet types supported on link */
   U16           packetsSent; /* Number of packets sent but not completed */
   U8            state;       /* LINK State of this connection */
   AuthCallback  authCur;     /* Authentication State of this connection */
   U8            role;          /* Master of this connection? */
   U16           secFlags;    /* Flags used during security procedures */
   BOOL          allowRoleChange; /* Used during connection setup only */
   BOOL          roleChanged;   /* Were the roles changed during setup? */
   BOOL          nameReqOnly; /* Temp connection to get Remote Name */
   U8            stage[4];    /* Used when staging receive data */
   U8            stageLen;    /* Len staged data */
   U8            linkKeyType; /* Type of key in linkKey. */
   U8            linkKey[16]; /* Link Key */
   U8            tmpRand[15]; /* Temp storage for authentication rand # */
   U8            sRes[3];     /* Expected SRES response to our AU_RAND */
   U16           modeInt;     /* Stores length of sniff/park mode */
   ListEntry     ACLDataQueue;/* Used for queueing data while in hold/park/sniff */
   EvmTimer      connTimer;   /* Hold mode timer */
   BOOL          waitModeChange;    /* Flag to indicate if we are waiting to enter hold mode */
   U16           linkPolicy;  /* Link Policy Settings */
   U16           flags;       /* Additional flags */
   U16           hostCurAclPackets; /* Current ACL packets outstanding */
} BtConn;

/* ---------------------------------------------------------------------------
 * BtConn Link States
 */
#define LINK_FREE           0    /* Entry is unused */
#define LINK_CONNECTING     1    /* TCP Connection Initiated */
#define LINK_ACCEPTING      2    /* TCP Connect Request received and Accepted */
#define LINK_SETUP          3    /* LMP-ACL Connection setup started */
#define LINK_AUTHENTICATING 4    /* ACL Connection is up but needs authentication */
#define LINK_ENCRYPTING     5    /* ACL Connection is up but needs encryption */
#define LINK_ACTIVE         6    /* ACL Connection is up */
#define LINK_HOLD           7    /* Link is Holding */
#define LINK_PARK           8    /* Link is Parked */
#define LINK_SNIFF          9

/* ---------------------------------------------------------------------------
 * BtConn Link Key Types
 */
#define LINK_KEY_NONE           0
#define LINK_KEY_INIT           1
#define LINK_KEY_COMB           2
#define LINK_KEY_LOCAL_UNIT     3
#define LINK_KEY_REMOTE_UNIT    4

/* ---------------------------------------------------------------------------
 * Link Security flags
 */
#define LINK_SEC_AUTHENTICATED   0x0001 /* We have authenticated the remote device */
#define LINK_SEC_REMOTE_COMPLETE 0x0002 /* We have received HCP_LMP_REMOTE_COMPLETE */
#define LINK_SEC_AUTH_REQUESTED  0x0004 /* Authorization was requested by host */
#define LINK_SEC_ENCRYPTED       0x0008 /* Link is currently encrypted */

/* ---------------------------------------------------------------------------
 * Regular flags 
 */
#define LINK_FLAG_NAME_REQ       0x0001 /* There is an outstanding name req */

/* ---------------------------------------------------------------------------
 * Host Controller SCO Connection tracking structure
 */
typedef struct _ScoConn {
    BtConn     *parent;       /* Managing ACL connection */
    U8          handle;       /* Connection Handle (== Index on Master) */
    U8          state;        /* Conn state (Inactive w/ parent == going down) */
    U16         packetsSent;  /* Number of packets sent but not completed */
    U8          linkType;     /* 0 == SCO, 2 == eSCO */
    BOOL        localRequest; /* TRUE when local device requested link */
    U16         hostCurScoPackets; /* Current SCO packets outstanding */
} ScoConn;

#define SCO_INACTIVE      0  /* Connection is free or going down */
#define SCO_CONNECTING    1  /* Connection is coming up */
#define SCO_ACTIVE        2  /* Connection is up */
#define SCO_CHANGING      3  /* eSCO Connection is changing */

#define NUM_BT_HANDLES    (NUM_BT_DEVICES+2) /* Add 2 handles for Broadcast */

#define MAX_IAC_CODES     2  /* Maximum number of IAC codes to handle at once. */

/* ---------------------------------------------------------------------------
 * Host Controller Variables
 */
typedef struct _InetHostController {
   U8          state;            /* HC's currently Active modes */
   BD_ADDR     myAddr;           /* Local Bluetooth address */
   SOCKET      pageSkt;          /* Used for receiving Connections */
   SOCKET      inquirySkt;       /* Used to receive Inquiry Responses */
   SOCKET      inquiryScanSkt;   /* Used to receive Inqiury Scan Requests */
   BtConn      connTab[NUM_BT_HANDLES]; /* ACL Connection Table */
#if NUM_SCO_CONNS > 0
   ScoConn     scoTab[NUM_SCO_CONNS];   /* SCO Connection Table */
#endif
   U32         inqAccess;        /* Inquiry Access code */
   U16         inqDuration;      /* Remaining Inquiry Duration */
   TimeT       inqStartTime;     /* Time of the beginning of the inquiry */
   TimeT       inqMaxTime;       /* The longest we should wait before giving up */
   U8          inqResponseCount; /* Number of Inquiry Responses received */
   U8          inqResponseMax;   /* Max Number of Inquiry Responses */
   U16         inqKey;           /* Our Unique Inquiry Key */
   U32         inqIACs[MAX_IAC_CODES]; /* IACs to which we might respond. */
   U16         inqIACCount;      /* Number of IACs in inqAccessCodes */
   U8          classOfDevice[3]; /* Our Device Class */
#if NUM_SCO_CONNS > 0
   BOOL        scoFlowOn;        /* Is SCO Flow Control Active? */
#endif

   /* Host flow control */
   U16         hostMaxAclLen;     /* Max ACL size to send to host */
   U8          hostMaxScoLen;     /* Max SCO size to send to host */
   U16         hostMaxAclPackets; /* Max ACL that host can handle */
   U16         hostMaxScoPackets; /* Max SCO that host can handle */
   U16         hostCurAclPackets; /* Current ACL packets outstanding */
   U16         hostCurScoPackets; /* Current SCO packets outstanding */

   U8          localVersion[8];  /* Local device version info */
   U8          localFeatures[8]; /* Local device features info */
   U16         localLinkPolicy;  /* Local device features info */
   U8          localName[248];   /* Local device name */
   ListEntry   eventFreeList;    /* HC Event free list */
   HWND        driverWnd;        /* HC Driver window handle */
   TranEntry   tranEntry;        /* Function pointers into transport */
   ListEntry   eventQueue;       /* Queue of events to indicate */
   U16         voiceSettings;    /* Voice Settings Bitmask (see 4.7.30) */
   U32         flags;            /* Miscellaneous flags (IHCF_) */
} InetHostController;

#define IHC(f)    (InetHC.f)
extern InetHostController  InetHC;

#define CONN_PICONET_BCAST    (NUM_BT_HANDLES-1)
#define CONN_ACTIVE_BCAST     (NUM_BT_HANDLES-2)

/* ---------------------------------------------------------------------------
 * Flags for InetHostController
 */
#define IHCF_AUTHENTICATE      0x0001  /* Authenticate all links? */
#define IHCF_ENCRYPT           0x0002  /* Automatic encrypt mode? */
#define IHCF_INQUIRY_RSSI      0x0004  /* Generate RSSI results for inq? */
#define IHCF_CTRLR_FLOW_ACL    0x0008  /* Ctrlr-to-host ACL flow on? */
#define IHCF_CTRLR_FLOW_SCO    0x0010  /* Ctrlr-to-host SCO flow on? */

/* ---------------------------------------------------------------------------
 * Host Controller States of operation
 */
#define HC_MODE_IDLE      0
#define HC_MODE_INQ_SCAN  0x01
#define HC_MODE_PAGE_SCAN 0x02
#define HC_MODE_INQUIRY   0x04
#define HC_MODE_PAGE      0x08
//efine HC_MODE_CONNECTED 0x10
#define HC_MODE_IDENTIFY  0x20
#define HC_MODE_RESETTING 0x40

/* ---------------------------------------------------------------------------
 * Well known ports for Inquiry and Paging procedures.
 */
#define INQUIRY_PORT    2048  /* UDP */
#define PAGE_PORT       2048  /* TCP */

/* ---------------------------------------------------------------------------
 * Window events handled by Driver Wnd.
 */
#define WM_SELECT       WM_USER+101
#define WM_HC_EVENT     WM_USER+102
#define WM_HC_QUIT      WM_USER+103

/* Define Pseudo-function: Number of commands HCI can send */
extern U8 HC_CmdQueueSpace(void);

#define HC_CmdQueueSpace()    10

/* Delay an event for later indication to the host. This is used when indicating
 * the event now would cause the event to be indicated out of order.
 */
#define HC_DelayEvent(_EVENT, _DATA, _DATALEN)  \
            PostMessage(IHC(driverWnd), WM_HC_EVENT, (((_DATALEN) << 8)|(_EVENT)), (LPARAM)(_DATA))


/* The QueueEvent structure is used to queue an event for indication at a 
 * later time. This is only used when there is an event buffer shortage.
 * Using this method preserves the event ordering.
 */
typedef struct _QueuedEvent {
    ListEntry   node;
    U8          event;
    U8          eventLen;
    U8          eventData[1];
} QueuedEvent;

/* ---------------------------------------------------------------------------
 * Handy macros for converting a BD_ADDR to a SOCKADDR_IN and vice versa.
 */
U16  BDA_IPPORT(const BD_ADDR *);   /* Retrieve IP Port from BD_ADDR */
U32  BDA_IPADDR(const BD_ADDR *);   /* Retrieve IP Host from BD_ADDR */
void BdAddr2SinAddr(const BD_ADDR *, struct sockaddr_in);
void ConnAddr2BdAddr(const BtConn *, BD_ADDR *);

#define BDA_IPPORT(_BD)   (*((U16 *)((_BD)->addr)))
#define BDA_IPADDR(_BD)   (*((U32 *)((_BD)->addr+2)))

#define BdAddr2SinAddr(_BD, _SIN) \
         (_SIN).sin_port = BDA_IPPORT(_BD);(_SIN).sin_addr.s_addr = BDA_IPADDR(_BD)

#define ConnAddr2BdAddr(_CONN, _BD) \
         BDA_IPPORT(_BD) = (_CONN)->port;BDA_IPADDR(_BD) = (_CONN)->addr.sin_addr.s_addr

#define HCP_ACTIVE_BCAST_BIT  0x0004
#define HCP_PICONET_BCAST_BIT 0x0008
/* ---------------------------------------------------------------------------
 * Host Controller Packet Type Identifiers
 * 
 * All packets sent over UDP and TCP ports contain this two byte identifier
 * at the beginning of the packet. The HCP_L2C_xxx types are followed by a
 * 2-byte length field and then data. The rest of the types are fixed length.
 *
 * The 4 byte header used on HCP_L2C packets makes for easy conversion
 * between HCI data commands/events and the wire format. All header fields
 * are sent in LSB order.
 *
 * All packet types are described using the following convention. The field
 * names are listed in order with the field size in bytes shown in parens 
 * after the field name. With the HCP type identifier is in the first two
 * bytes of the packet. For example, the expression:
 *
 *      #define HCP_L2C_CONTU   /* length(2), data(length)
 *
 * defines the L2CAP Continue data packet as having a U16 length field
 * after the HCP type, followed by 'length' bytes of data.
 *
 * The three Data Packet Types (Connection Oriented, Active B'Cast &
 * Piconet B'Cast) each use bits 0 & 1 to discern the first fragment
 * from the rest of the fragments (CONTU). If bit 0 is set the packet
 * is the first, otherwise bit 1 signifies its a continuation fragment.
 */
#define HCP_RESERVED    0x0000   /* Unused */
#define HCP_L2C_FIRST   0x0001   /* length(2), data(length) */
#define HCP_L2C_CONTU   0x0002   /* length(2), data(length) */
#define HCP_ACTV_FIRST  0x0005   /* length(2), data(length) */
#define HCP_ACTV_CONTU  0x0006   /* length(2), data(length) */
#define HCP_TEST_EVENT  0x0007   /* length(2), event_data(length) */
#define HCP_PNET_FIRST  0x0009   /* length(2), data(length) */
#define HCP_PNET_CONTU  0x000A   /* length(2), data(length) */
#define HCP_SCO_AUDIO   0x000B   /* length(1), handle(1), data(length) */

/* All id's starting with 0x0100 are based on LMP packets. */
#define HCP_LMP_EVENT               0x0100   /* LMP_EVENT mask, nop */
#define HCP_LMP_CONN_REQ            0x0101   /* cod(3), port(2)*/
#define HCP_LMP_ACCEPTED            0x0102   /* accepted-opcode(2) */
#define HCP_LMP_SWITCH_REQ          0x0103   /* (0) */
#define HCP_LMP_SETUP_COMPLETE      0x0104   /* (0) */
#define HCP_LMP_NOT_ACCEPTED        0x0105   /* rejected-opcode(2) reason(1) */
#define HCP_LMP_DETACH              0x0106   /* reason(1) */
#define HCP_LMP_IN_RAND             0x0107   /* random-number(15) */
#define HCP_LMP_COMB_KEY            0x0108   /* random-number(15) */
#define HCP_LMP_AU_RAND             0x0109   /* random-number(15) */
#define HCP_LMP_SRES                0x010A   /* auth-resp(3) */
#define HCP_LMP_SCO_LINK_REQ        0x010B   /* SCO-hndl(1), packets(1), mode(1) */
#define HCP_LMP_REMOVE_SCO_REQ      0x010C   /* SCO-hndl(1), reason(1) */
#define HCP_LMP_NAME_REQ            0x010D   /* (0) */
#define HCP_LMP_NAME_RSP            0x010E   /* name(248) */
#define HCP_LMP_HOLD                0x0110   /* ??? */    
#define HCP_LMP_HOLD_REQ            0x0111   /* ??? */
#define HCP_LMP_SNIFF               0x0112
#define HCP_LMP_SNIFF_REQ           0x0113
#define HCP_LMP_UNSNIFF_REQ         0x0114  
#define HCP_LMP_PARK                0x0115
#define HCP_LMP_PARK_REQ            0x0116
#define HCP_LMP_UNPARK_REQ          0x0117
#define HCP_LMP_ENC_MODE_REQ        0x0118   /* mode(1) */
#define HCP_LMP_ENC_KEY_SIZE_REQ    0x0119  /* key-size(1) */
#define HCP_LMP_ENC_START_REQ       0x011A   /* random-number(16) */
#define HCP_LMP_ENC_STOP_REQ        0x011B   /* (0) */
#define HCP_LMP_FEATURES            0x011C   /* (0) */
#define HCP_LMP_FEATURES_RSP        0x011D   /* (0) */
#define HCP_LMP_ESCO_LINK_REQ       0x011E   /* eSCO-hndl(1), pt-m->s(1), pt-s->m(1), pl-m->s(2),
                                              * pl-s->m(2), mode(1), state(1)
                                              */
#define HCP_LMP_REMOVE_ESCO_REQ     0x011F   /* eSCO-hndl(1), reason(1) */
#define HCP_LMP_ACCEPTED_EXT        0x0120   /* escape(1), accepted-opcode(2) */
#define HCP_LMP_NOT_ACCEPTED_EXT    0x0121   /* escape(1), rejected-opcode(2) reason(1) */



/* Not really LMP packets but treated like such in the INet HC. */
#define HCP_INQUIRY_REQ         0x0301   /* inquiryId(2), accessCode(4), mbz(3) ** UDP Only */
#define HCP_INQUIRY_RSP         0x0302   /* cod(3), bdAddr(6)    ** UDP Only */

/* INet HC specific packets, not part of Bluetooth Specification. */
#define HCP_IDENTITY            0x0501   /* id(2)                ** UDP Only */

#define HCP_HCI_COMMAND         0x0700   /* Mask for indication of certain 
                                          * HCC commands */
#define HCP_HCC_PIN_POS         0x0701   /* Successful PIN code. param1 is (U8 *) pin,
                                          * param2 is U16 pinLen.
                                          */
#define HCP_HCC_PIN_NEG         0x0702   /* PIN code could not be retrieved.
                                          */
#define HCP_HCC_KEY_POS         0x0703   /* Successful key code. param1 is (U8 *) key,
                                          * param2 is U16 pinLen.
                                          */
#define HCP_HCC_KEY_NEG         0x0704   /* PIN code could not be retrieved.
                                          */

#define HCP_TYPE_MASK           0x0F00   /* Used to get type (Data, LMP, etc)
/* ---------------------------------------------------------------------------  
 * Host Controller & Driver Internal Functions
 */

/* From sniffer.c */
#if XA_DEBUG == XA_ENABLED
void DumpLmpPacket(const char *RxTx, const U8 *Packet, U16 Len);
#else 
#define DumpLmpPacket(_RXTX, _P, _L)
#endif

/* From hostctlr.c */
extern HcStatus HC_Inquire(U32 type, U8 Duration, U8 NumResponses);
extern void     HC_InquiryCancel(void);
extern void     HC_SendInquiry(U32 accessCode, U16 randValue);
extern HcStatus HC_CreateAclConn(BD_ADDR Target, U16 PacketTypes, BOOL NameReqOnly, BOOL allowRoleChange);
extern HcStatus HC_Disconnect(U16 ConHndl, U8 Reason);
extern void     HC_PostEvent(U8 Event, U8 *EventData, U8 EventLen);
extern HcStatus HC_AcceptConn(BD_ADDR Addr, U8 Role);
extern HcStatus HC_RejectConn(BD_ADDR Addr, U8 Reason);
extern void     HC_StopInqScan(void);
extern void     HC_StopPageScan(void);
extern HcStatus HC_StartPageScan(void);
extern HcStatus HC_StartInqScan(void);
extern void     HC_WriteInqMode(U8 type);
extern void     HC_ReadInqMode();
extern void     HC_WriteScanEnable(U8 mode);
extern BOOL     HC_Send(SOCKET Skt, const char *Buf, int Len, int Flags);
extern BOOL     HC_SendLmp(SOCKET Skt, const char *Buf, int Len, int Flags);
extern BOOL     HC_SendTo(const char *Buf, int Len, U32 ToAddr, U16 ToPort);
extern void     HC_Clock(void);
extern void     HC_SendBroadcastData(U8 *Buffer, U8 Len);
extern HcStatus HC_OpenInquirySocket(void);
extern HcStatus HC_OpenInquiryScanSocket(void);
extern HcStatus HC_OpenPagingSocket(void);
extern LRESULT CALLBACK HC_WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
extern void     HC_GetIdentity(void);
extern HcStatus HC_Authenticate(U16 ConnHndl);
extern void     HC_LinkKeyReqReply(const BD_ADDR *BdAddr, const U8 *LinkKey);
extern void     HC_LinkKeyReqNegReply(const BD_ADDR *BdAddr);
extern void     HC_CloseLink(BtConn *Conn);
#if NUM_SCO_CONNS > 0
extern HcStatus HC_AddScoConn(U16 AclHndl, U16 PktTypes);
extern HcStatus HC_ScoAcceptConn(BtConn *Acl);
extern HcStatus HC_SetupSyncConn(U16 AclHndl, U16 pktTypes);
extern HcStatus HC_AcceptSyncConn(BtConn *Acl);
extern HcStatus HC_RejectSyncConn(BD_ADDR Addr, U8 Reason);
extern ScoConn *FindScoLinkbyHandle(U8 ScoHandle);
#endif
extern BtConn *GetConnFromBdAddr(const BD_ADDR *BdAddr);
extern HcStatus HC_GetRemoteName(BD_ADDR *remoteDevice);
extern BOOL HC_ProcessEventOverrunQueue(void);

/* From hcitrans.c */
static void     HC_CreateConnCancel(BD_ADDR *bdAddr);
static void     HC_RemoteNameCancel(BD_ADDR *bdAddr);
static void     HC_ReadBufferSize(void);
BOOL            HC_CauseLinkLoss(const BD_ADDR *addr);
static void     HC_HostNumCompletedPackets(U8 *parmBlock);
static void     HC_Reset(void);
static void     HC_SetHostFlowCtrl(U8 p);
static void     HC_HostBufferSize(U8 *parmBlock);
static void     HC_SendCommand(HciPacket *Packet);
void            HC_SendQueuedAclData(BtConn *conn);
static void     HC_SendAclData(HciPacket *Packet);
static void     HC_SendScoData(HciPacket *Packet);
static HWND     InitDriverWnd(void);
static void     HC_ReadBdAddr(void);
static void     HC_WriteScoFlowControl(BOOL FlowOn);
static void     HC_ReadScoFlowControl(void);
static void     HC_ReadLocalVersion();
static void     HC_ReadLocalFeatures();
static void     HC_ReadLocalName();
static void     HC_WriteLocalName(char *Name, U8 NameLen);
static void     HC_WriteScoSettings(U16 Setting);
static void     HC_ReadAuthEnable(void);
static void     HC_AuthEnable(BOOL enable);
static void     HC_ReadEncryptMode(void);
static void     HC_WriteEncryptMode(U8 mode);
static void     HC_ReadClassOfDevice();
static void     HC_WriteClassOfDevice(U8 *Cod);
void     HC_WriteCurrentIacLap(I8 count, U8 *buffer);
/* BT v1.2 */
static void HC_ReadDefaultLinkPolicy();
static void HC_WriteDefaultLinkPolicy(U16 linkPolicy);

/* From hclinkmgr.c */
static void HC_GenerateRandNr(U8 *RandNr);
static void HC_GenerateSRes(const U8 *AuRand, const U8 *LinkKey, BD_ADDR *Addr, U8 *SRes);
static void HC_GenerateKinit(const U8 *InRand, const U8 *Pin, U8 PinLen, const BD_ADDR *BdAddr, U8 *kInit);
extern void HC_PinCodeReqReply(const BD_ADDR *BdAddr, U8 PinLen, const U8 *Pin);
extern void HC_PinCodeReqNegReply(const BD_ADDR *BdAddr);
extern void HC_LinkKeyReqReply(const BD_ADDR *BdAddr, const U8 *LinkKey);
extern void HC_LinkKeyReqNegReply(const BD_ADDR *BdAddr);
extern void HC_ProcessLmCommand(U16 Hcp, BtConn *Conn, U16 ConnHndl);
static void HC_GenerateXorKey(U8 *KeyInOut, const U8 *KeyIn);
#if NUM_SCO_CONNS > 0
static void HC_ScoLinkRequest(BtConn *Acl, U8 *Request);
static void HC_ScoRemoveLink(U8 ScoHandle, U8 Reason);
static void HC_EScoLinkRequest(BtConn *Acl, U8 *Request);
static void HC_ScoVsetReq(U8 vset1, U8 vset2);
#endif
static void HC_SendRemoteFeatures(BtConn *Conn);
static void HC_SendLocalName(BtConn *Conn);
static void HC_ProcessNameRsp(BtConn *Conn, U8 *rxData);
static void HC_ProcessReadFeatureRsp(U16 ConnHndl, U8 *rxData);
static void StartAuthenticating(BtConn *conn);

/* Authorization States */
void AuthReady(U16 event, BtConn *conn, void *param1, void *param2);
static void AuthKeyReq_A(U16 event, BtConn *conn, void *param1, void *param2);
static void AuthPinReq_A(U16 event, BtConn *conn, void *param1, void *param2);
static void AuthCheckKey_A(U16 event, BtConn *conn, void *param1, void *param2);
static void AuthPairing_A(U16 event, BtConn *conn, void *param1, void *param2);
static void AuthCombKey_A(U16 event, BtConn *conn, void *param1, void *param2);
static void AuthPinReq_B(U16 event, BtConn *conn, void *param1, void *param2);
static void AuthCombKey_B(U16 event, BtConn *conn, void *param1, void *param2);

/* Switches directly to another state (without pushing, popping,
 * or calling the state). This is equivalent to a "normal" state transition.
 */
#define AuthSwitchState(conn, cb) AuthGetState(conn) = (cb)

/* Returns the current authorization state */
#define AuthGetState(conn)       ((conn)->authCur)

/* Sends an event to the current authorization state */
#define HandleAuthState(ev, conn, p1, p2)    (conn)->authCur(ev,conn,(void *)p1,(void *)p2)


void HC_ProcessHold(U16 ConnHndl, U16 holdInt);
void HC_ProcessHoldReq(U16 ConnHndl, U16 holdInt);
void HoldTimerFire(EvmTimer *Timer);
HcStatus HC_HoldMode(U16 ConnHndl, U16 holdMaxInt, U16 holdMinInt);
HcStatus HC_HoldModeReq(U16 ConnHndl, U16 holdMaxInt, U16 holdMinInt);

HcStatus HC_SniffMode(U16 ConnHndl, U16 sniffMaxInt, U16 sniffMinInt, 
                      U16 sniffAttempt, U16 sniffTO);
void HC_ProcessSniffReq(U16 ConnHndl, U8 timerflag, U16 DSniff, U16 TSniff, 
                   U16 Sniffatt, U16 SniffTO);
void SniffTimerFire(EvmTimer *Timer);

HcStatus HC_ExitSniffMode(U16 ConnHndl);
void HC_ProcessUnsniffReq(U16 ConnHndl);

HcStatus HC_ParkMode(U16 ConnHndl, U16 beaconMaxInt, U16 beaconMinInt);
HcStatus HC_ExitParkMode(U16 ConnHndl);
void HC_ProcessPark(U16 ConnHndl, U16 beaconInt);
void HC_ProcessParkReq(U16 ConnHndl, U16 beaconInt);
void HC_ProcessUnparkReq(U16 ConnHndl);

HcStatus HC_SetConnectionEncryption(U16 ConnHndl, U8 encenable);
void HC_ProcessEncStartReq(U16 ConnHndl);
void HC_ProcessEncStopReq(U16 ConnHndl);
HcStatus HC_ReadRemoteFeatures(U16 connHndl);

void HC_ReadLinkPolicy(U16 connHndl);
void HC_WriteLinkPolicy(U16 connHndl, U16 linkPolicy);

HcStatus HC_SwitchRole(const BD_ADDR *BdAddr, U8 Role);
void HC_RoleDiscovery(U16 ConHndl);
void HC_ProcessSwitchReq(U16 ConnHndl, U8 Role);
