// DriverNotifications.h
//
// This provides a common set of notifications between FTS & the generic
// interface, and between the generic interface and the COM interface.

#pragma once

enum eNotificationTypes 
{
    eOpenSetIO, eOpenHwDlg, eLiveModeStarted, eLiveModeStopped, eAppClosed,
    eStartCaptureToBuf, eStartCaptureToFile, ePauseResumeCapture, eClearBuffer, eCloseFile,
    eResync, eReset,
};

typedef void (*NotificationType) (void* pThis);
