#ifndef __BLUEMGR_H
#define __BLUEMGR_H
/***************************************************************************
 *
 * File:
 *     $Workfile:bluemgr_ui.h$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:21$
 *
 * Description:
 *     This is file contains a simple Bluetooth Device Manager for managing
 *     the selection of a remote Bluetooth device and for managing local
 *     Bluetooth control panel settings. This application makes extensive
 *     use of the ME API's.
 *
 * Copyright 1999-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/
#include "me.h"
#include "sec.h"
#include "medev.h"

/*---------------------------------------------------------------------------
 * Bluetooth Device Manager application
 *
 *     The Bluetooth Device Management application allows the user to inquire
 *     remote devices, manage local device settings, activate security and
 *     select the appropriate remote device for a service connection. The 
 *     application provides a graphical or a command line based user interface.
 */

/****************************************************************************
 *
 * Constants
 *
 ****************************************************************************/

/*---------------------------------------------------------------------------
 * MAX_DEVICE_NAME_SIZE constant
 *
 *     This constant defines the maximum length of a device name that can
 *     be stored in the BtDeviceExt context. The value must be at least
 *     enough to store the BD_ADDR as a string, and no larger than the maximum
 *     device name length. The format for the device name is UTF-8.
 */
#ifndef MAX_DEVICE_NAME_SIZE
#define MAX_DEVICE_NAME_SIZE 34
#endif

#if MAX_DEVICE_NAME_SIZE < BDADDR_NTOA_SIZE
#error "MAX_DEVICE_NAME_SIZE must be at least BDADDR_NTOA_SIZE bytes."
#endif
#if MAX_DEVICE_NAME_SIZE > 248
#error "MAX_DEVICE_NAME_SIZE must be no greater than 248"
#endif

/*---------------------------------------------------------------------------
 * INQUIRY_REFERSH_INTERVAL constant
 *
 *     This constant defines the interval between repeated automatic device
 *     inquiries (quiet period). The value is in milliseconds. The default
 *     inquiry quiet period is twice the default inquiry period.
 */
#ifndef INQUIRY_REFERSH_INTERVAL
#define INQUIRY_REFERSH_INTERVAL     (BT_INQ_TIME_GAP100 * 2 * 1000)
#endif

/*---------------------------------------------------------------------------
 * MAX_PIN_LEN constant
 *
 *     Defines the maximum string size of a Bluetooth Passcode (PIN).
 *     This value is defined by the Bluetooth specification as 16 bytes.
 */
#define MAX_PIN_LEN         16


/****************************************************************************
 *
 * Types
 *
 ****************************************************************************/

/*---------------------------------------------------------------------------
 * BtDeviceExtStatus type
 *
 *     These types are used to track status information about the device
 *     that is particular to the functionality of the Bluetooth Manager
 *     user interface.
 */
typedef U8 BtDeviceExtStatus;

/* The remote device name has been successfully retrieved. */
#define BDS_HAVE_NAME       0x01

/* The remote device name should be queried. */
#define BDS_NO_NAME         0x02

/* Name request failed on last attempt. */
#define BDS_NAME_FAILED     0x04

/* End of BtDeviceExtStatus */

/*---------------------------------------------------------------------------
 * BtPropertyPageId type
 *
 *     Property pages supported by the Bluetooth Manager Control panel.
 */
typedef U8 BtPropertyPageId;

#define BMG_PROPERTY_ACCESS      0
#define BMG_PROPERTY_LINKPOLICY  1
#define BMG_PROPERTY_INTERVALS   2
#define BMG_PROPERTY_BLUEMGR     3

/* End of BtPropertyPageId */


/*---------------------------------------------------------------------------
 * BtInquiryStyle type
 *
 *     Bluetooth Manager Inquiry/Remote Name Request behavior modes.
 */
typedef U8 BtInquiryStyle;

/* Do not perform automatic bluetooth name requests. */
#define BMG_NO_NAMEREQ              0x00

/* Perform automatic bluetooth name requests after inquiry is complete. */
#define BMG_DO_NAMEREQ              0x01

/* Perform automatic bluetooth name requests while inquiry is in progress. */
#define BMG_DO_NAMEREQ_INSEQ       (0x02|BMG_DO_NAMEREQ)

/* Automatically refresh the device list with inquiries. */
#define BMG_AUTO_REFRESH            0x04

/* End of BtInquiryStyle */

/****************************************************************************
 *
 * Data Structures
 *
 ****************************************************************************/

/*---------------------------------------------------------------------------
 * BtDeviceExt structure
 *
 *     This structure is stored in the BtDeviceContext->context field by the
 *     Bluetooth device manager. It tracks information about each remote
 *     device that is needed by the Bluetooth device manager for displaying
 *     and selecting remote devices.
 */
typedef struct _BtDeviceExt {
    BOOL                inUse;
    BtDeviceExtStatus   status;

    /* The device mangager must keep copies of the BtRemoteDevice fields that
     * it uses to update the user interface to prevent possible deadlock
     * scenarios. In Windows, deadlock can occur when a stack event results in
     * a request to update the displayed user interface. Since the display
     * update (window message) occurs on a different thread (message pump) than
     * the stack event, and it is blocking the stack event, the update thread
     * will be blocked from reentering the Bluetooth stack, and a deadlock will
     * result. These variables are updated before the windows update message is
     * sent, while still executing on the stack thread.
     */
    BtRemDevState       state;  /* The devices connection status. */
    BtConnectionRole    role;   /* The role of the local device (master/slave) if connected. */
    BtLinkMode          mode;   /* Current connection mode (hold/park/sniff/active). */
    BtAuthState         auth;   /* The authentication state link. */
    BtEncryptState      encrypt;/* The encryption state of the link. */

    /* The name of the remote device. If unknown, the devices Bluetooth device
     * address is used. 
     */
    char                name[MAX_DEVICE_NAME_SIZE];
} BtDeviceExt;


/*---------------------------------------------------------------------------
 * BtControlPanel structure
 *
 *     This structure maintains settings for Bluetooth SDK API functions.
 *     These settings are managed via the Bluetooth property pages provided
 *     by the Bluetooth Manager user interface.
 */
typedef struct _BtControlPanel {
    BtIac               inquiry_iac;
    BtSniffInfo         startSniff_info;
    BtAccessModeInfo    setAccessibleModeC_info;
    BtAccessModeInfo    setAccessibleModeNC_info;
    U16                 hold_min;
    U16                 hold_max;
    U16                 startPark_min;
    U16                 startPark_max;
    BtLinkPolicy        setLinkPolicy_inACL;
    BtLinkPolicy        setLinkPolicy_outACL;

    /* Group: Providing these parameters amounts to using a fixed passkey. */
    const U8           *setPin_pin;
    U8                  setPin_len;
    BtPairingType       setPin_type;

    /* Group: These parameters are specific to the Blue Manager. */
    BtInquiryStyle      inquiryMode;
    BOOL                aclNotActiveAlert;

} BtControlPanel;


/****************************************************************************
 *
 * Function Reference
 *
 ****************************************************************************/
#if defined (_WINDOWS_)     /* Graphical User Interface */

/*---------------------------------------------------------------------------
 * BMG_Initialize()
 *
 *     Initialize the Bluetooth Device manager user interface component. 
 *     This initialization includes the registration of Management entity
 *     event handlers, creation of user interface components the initialization
 *     of internal data structures and configuring optional stack settings.
 *
 * Parameters:
 *     Inst - The instance handle of the parent application.
 *         
 *     hWndParent - The window handle of the parent applications main window.
 *
 * Returns:
 *     On success, the window handle of the device chooser window. Otherwise
 *         zero.
 */
HWND BMG_Initialize(HINSTANCE Inst, HWND hWndParent);


/*---------------------------------------------------------------------------
 * BMG_Deinitialize()
 *
 *     Deinitialize the Bluetooth Device manager user interface component.
 *     The Bluetooth Device manager must be deinitialized after completing
 *     a device manager shutdown.
 *
 * Returns:
 *     BT_STATUS_SUCCESS - The device manager is deinitialized.
 */
BtStatus BMG_Deinitialize(void);


/*---------------------------------------------------------------------------
 * BMG_Shutdown()
 *
 *     Shutdown the device manager component. This function handles 
 *     asynchronous aspects of the device manager deinitialization. Once
 *     complete the device manager must be deinitialized.
 *
 * Returns:
 *     BT_STATUS_FAILED - The device manager is not initialized.
 *
 *     BT_STATUS_SUCCESS - The device manager is shut down. BMG_Deinitialize
 *         should be called immediately.
 *
 *     BT_STATUS_PENDING - The device manager is shutting down. When shutdown
 *         is complete, a WM_QUIT message is posted.
 */
BtStatus BMG_Shutdown(void);


/*---------------------------------------------------------------------------
 * BMG_AddMainMenu()
 *
 *     Adds the Bluetooth device manager main menu to the applications user
 *     interface. This menu is used to access the device chooser and control
 *     panel dialogs. The BMG_TrackMainMenu function should be used to handle
 *     mouse events for the menu.
 *
 * Parameters:
 *     hWnd -  The window handle of the parent applications main window.
 *
 * Returns:
 *     TRUE - The menu was successfully added.
 *
 *     FALSE - The menu could not be added.
 */
BOOL BMG_AddMainMenu(HWND hWnd);


/*---------------------------------------------------------------------------
 * BMG_AddToolTips()
 *
 *     Adds Bluetooth device manager tool tip support to the applications user
 *     interface. The application provides a function which maps a control Id
 *     to a tool tip message.
 *
 * Parameters:
 *     hWnd -  The handle of the window to add tool tip support to.
 *
 *     GetToolTipText - A pointer to a function that returns the tool tip text
 *         for the specified control Id. This function is called whenever a
 *         tool tip is required, so the message can be state specific. The
 *         ControlId parameter is the resource identifier of the control. If
 *         no tool tip exists for the control an empty string must be returned.
 *
 * Returns:
 *     TRUE - Tool tip support was successfully added.
 *
 *     FALSE - Tool tip support could not be added.
 */
BOOL BMG_AddToolTips(HWND hWndApp, LPTSTR (*GetToolTipText)(int ControlId));


/*---------------------------------------------------------------------------
 * BMG_TrackMainMenu()
 *
 *     This function is used to pass messages to the Bluetooth device manager 
 *     main menu for processing. The user applications window procedure should
 *     pass all unrecognized WM_COMMAND messages to this function. Messages
 *     destined for the main menu are processed before returning.
 *
 * Parameters:
 *     hWnd -  The window handle receiving this message.
 *     
 *     wParam - The WPARAM of the message.
 *
 *     lParam - The LPARAM of the message.         
 *
 * Returns:
 *     TRUE - If the message is for the main menu and was processed.
 *
 *     FALSE - The message is not for the main menu.
 */
BOOL BMG_TrackMainMenu(HWND hWnd, WPARAM wParam, LPARAM lParam);


/*---------------------------------------------------------------------------
 * BMG_GetDeviceName()
 *
 *     Retrieves a pointer to the name of the remote device.
 *
 * Parameters:
 *     Bdc - The Bluetooth device context of the device.
 *
 * Returns:
 *     A pointer to the null-terminated name of the device (UTF-8).
 */
const char *BMG_GetDeviceName(BtDeviceContext *Bdc);

#define BMG_GetDeviceName(_BDC) ((_BDC)->context ? ((BtDeviceExt *)(_BDC)->context)->name : "")


/*---------------------------------------------------------------------------
 * BMG_ShowChooser()
 *
 *     Show or hide the Bluetooth device chooser dialog. This dialog can
 *     be used to manage remote device options such as authentication and 
 *     connection mode. It also displays debug and event status messages.
 *     The chooser cannot be manually opened to select a device. Device
 *     selection only occurs as part of a DS_SelectDevice function call.
 *
 * Parameters:
 *     Show - When TRUE, the dialog is displayed. Otherwise it is hidden.
 */
void BMG_ShowChooser(BOOL Show);


/*---------------------------------------------------------------------------
 * BMG_OpenProperties()
 *
 *     Opens the Bluetooth control panel to the specified property page.
 *
 * Parameters:
 *     Page - The property page to open.
 *
 * Returns:
 *     On success, the handle of the property page window. On failure, zero.
 */
HWND BMG_OpenProperties(BtPropertyPageId Page);


#if BT_SECURITY == XA_ENABLED
/*---------------------------------------------------------------------------
 * BMG_SetSecurityMode()
 *
 *     Set the security mode for the device. This includes the registration
 *     or deregistration of pairing and security handlers with the Management
 *     Entity. This command affects future connections only.
 *
 * Parameters:
 *     SecMode - The security mode to set.
 *
 * Returns:
 *     BT_STATUS_FAILED - The Bluetooth device manager is not initialized.
 *
 *     BT_STATUS_SUCCESS - The new security mode has been set.
 *         
 *     BT_STATUS_PENDING - The security mode change has been requested. 
 *         Completion is indicated by a BTEVENT_SECURITY_EVENT to ME global
 *         handlers.
 */
BtStatus BMG_SetSecurityMode(BtSecurityMode SecMode);
#endif


/*---------------------------------------------------------------------------
 * BMG_GetControlPanelSettings()
 *
 *     Returns the Bluetooth Control panel structure used by the Blue Manager.
 *
 * Returns:
 *     Pointer to the Bluetooth Control Panel.
 */
BtControlPanel *BMG_GetControlPanelSettings(void);


/*---------------------------------------------------------------------------
 * BMG_QueryUserForBdAddr()
 *
 *     Prompts the user to enter a 48-bit Bluetooth device address.
 *
 * Parameters:
 *     Addr - A pointer to the BD_ADDR to receive the users input.
 *
 * Returns:
 *     TRUE - The user entered a valid BD_ADDR.
 *
 *     FALSE - The user cancelled the request or provided an invalid BD_ADDR.
 */
BOOL BMG_QueryUserForBdAddr(BD_ADDR *Addr);
#endif  /* _WINDOWS_ */

#if defined( __TEXTMENU_H )     /* Command Line User Interface */
/*---------------------------------------------------------------------------
 * BMI_CreateMenu()
 *
 *     Creates the command line user menu for the Bluetooth device manager.
 *
 * Returns:
 *     On success, the handle to the device manager menu. Otherwize zero.
 */
MnuTextMenu *BMI_CreateMenu(void);


/*---------------------------------------------------------------------------
 * BMI_IsRadioReady()
 *
 *     Returns the status of the radio initialization.
 *
 * Returns:
 *     TRUE - The radio is initialized.
 *
 *     FALSE - The radio has not yet been initialized.
 */
BOOL BMI_IsRadioReady(void);


/*---------------------------------------------------------------------------
 * BMI_Shutdown()
 *
 *     Shutdown the device manager component. This function handles 
 *     asynchronous aspects of the device manager deinitialization. Once
 *     complete the device manager must be deinitialized.
 *
 * Returns:
 *     BT_STATUS_FAILED - The device manager is not initialized.
 *
 *     BT_STATUS_SUCCESS - The device manager is shut down. BMG_Deinitialize
 *         should be called immediately.
 *
 *     BT_STATUS_PENDING - The device manager is shutting down.
 */
BtStatus BMI_Shutdown(void);


/*---------------------------------------------------------------------------
 * BMI_Deinitialize()
 *
 *     Deinitialize the Bluetooth Device manager user interface component.
 *     The Bluetooth Device manager must be deinitialized after completing
 *     a device manager shutdown.
 *
 * Returns:
 *     BT_STATUS_SUCCESS - The device manager is deinitialized.
 */
BtStatus BMI_Deinitialize(void);

/*---------------------------------------------------------------------------
 * BMI_SetSecurityMode()
 *
 *     Set the security mode for the device. This includes the registration
 *     or deregistration of pairing and security handlers with the Management
 *     Entity. This command affects future connections only.
 *
 * Parameters:
 *     SecMode - The security mode to set.
 *
 * Returns:
 *     BT_STATUS_FAILED - The Bluetooth device manager is not initialized.
 *
 *     BT_STATUS_SUCCESS - The new security mode has been set.
 *         
 *     BT_STATUS_PENDING - The security mode change has been requested. 
 *         Completion is indicated by a BTEVENT_SECURITY_EVENT to ME global
 *         handlers.
 */
BtStatus BMI_SetSecurityMode(BtSecurityMode SecMode);

#endif /* __TEXTMENU_H */

#endif /* __BLUEMGR_H */

