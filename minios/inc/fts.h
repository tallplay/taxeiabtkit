/***************************************************************************
 *
 * File:
 *     $Workfile:fts.h$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:15$
 *
 * Description: iAnywhere Blue FTS interface header file.
 *
 * Created:     September 10, 2002
 *
 * Copyright 1999-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/
#include "btconfig.h"

/****************************************************************************
 *
 * Constants
 *
 ***************************************************************************/

#define FTS_HCI_COMMAND     0x01
#define FTS_HCI_ACL_DATA    0x02
#define FTS_HCI_SCO_DATA    0x04
#define FTS_HCI_EVENT       0x08

#define BUF_TYPES (HCI_BUFTYPE_COMMAND|HCI_BUFTYPE_EVENT|HCI_BUFTYPE_ACL_DATA|HCI_BUFTYPE_SCO_DATA)

/****************************************************************************
 *
 * Function Reference
 *
 ****************************************************************************/

/*---------------------------------------------------------------------------
 * FTS_FindFTS4BTDirectory()
 *
 *      Extracts the DLL and INI file path names from the registry for FTS4BT.
 *
 * Parameters:
 *      pLiveImportAPIDllPath - points to an array of characters where the 
 *                              DLL path name can be written.
 *      pIniPath              - points to an array of characters where the
 *                              ini path name can be written.
 *
 * Returns:
 *      TRUE if the file path names could be extracted from the registry, 
 *      FALSE otherwise.
 */
BOOL FTS_FindFTS4BTDirectory(TCHAR *pLiveImportAPIDllPath ,
                             TCHAR *pIniPath              );

/*---------------------------------------------------------------------------
 * FTS_ConnectToLiveImport()
 *
 *      Connects the application to the FTS Live Import server. This must
 *      be the first function called to access FTS. Note: This funtion will
 *      not succeed if FTS for Bluetooth is not running on the local system.
 *
 * Parameters:
 *      None.
 *
 * Returns:
 *      TRUE for successful startup, FALSE otherwise.
 */
BOOL FTS_ConnectToLiveImport(void);

/*---------------------------------------------------------------------------
 * FTS_StartLiveImport()
 *
 *      Starts passing HCI frame buffers to FTS LiveImport. Do not call this
 *      interface if FTS_ConnectToLiveImport fails.
 *
 * Parameters:
 *      None.
 *
 * Returns:
 *      None.
 */
void FTS_StartLiveImport(void);

/*---------------------------------------------------------------------------
 * FTS_CheckLiveImportConnection()
 *
 *      Checks the FTS Live Import connection status.
 *
 * Parameters:
 *      None.
 *
 * Returns:
 *      TRUE for connected, FALSE otherwise.
 */
BOOL FTS_CheckLiveImportConnection(void);

/*---------------------------------------------------------------------------
 * FTS_SendData()
 *
 *      Sends frame data directly to FTS Live Import. Typically used for test
 *      purposes only, since FTS_StartLiveImport automatically passes HCI
 *      frames to FTS.
 *
 * Parameters:
 *      iframeLen - length of frame.
 *      pbytFrame - HCI frame data in an array of BYTEs format.
 *      iFlags - 0x01 = HCI_COMMAND
 *               0x02 = HCI_ACL_DATA
 *               0x04 = HCI_SCO_DATA
 *               0x08 = HCI_EVENT
 *      iSide - 0x00 = Host side, 0x01 = Controller side.
 *
 * Returns:
 *      None.
 */
void FTS_SendData(int iFrameLength, BYTE* pbytFrame, int iFlags, int iSide);

/*---------------------------------------------------------------------------
 * FTS_DisconnectFromLiveImport()
 *
 *      Disconnects the application from the FTS Live Import server. This must
 *      be the last function called before closing the application.
 *
 * Parameters:
 *      None.
 *
 * Returns:
 *      TRUE for successful disconnect, FALSE otherwise.
 */
BOOL FTS_DisconnectFromLiveImport(void);

/*
 * Stub out FTS API functions when HCI Prescans are disabled. 
 */
#if HCI_ALLOW_PRESCAN == XA_DISABLED
#define FTS_ConnectToLiveImport()       (FALSE)
#define FTS_StartLiveImport()           (void)0
#define FTS_CheckLiveImportConnection() (FALSE)
#define FTS_SendData(_A, _B, _C, _D)    (void)0    
#define FTS_DisconnectFromLiveImport()  (FALSE)
#endif
