#ifndef __BLUEMGRI_H
#define __BLUEMGRI_H
/***************************************************************************
 *
 * File:
 *     $Workfile:bluemgri.h$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:22$
 *
 * Description:
 *     This is file contains internal definitions for the simple Bluetooth
 *     Device manager.
 *
 * Copyright 1999-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/
#include "btconfig.h"

#if NUM_KNOWN_DEVICES == 0
#error The Bluetooth Monitor GUI requires the ME Bluetooth Device Selection Manager.
#endif

/*
 * Device List Icon indicies 
 */
#define ICON_DEVICE_OUT             0
#define ICON_DEVICE_INRANGE         1
#define ICON_DEVICE_CONNECTED       2
#define ICON_DEVICE_INPICONET       3
#define ICON_DEVICE_PREFERRED       4
#define ICON_DEVICE_PREFERREDOUT    5

/*
 * Bluetooth Manager Device Options. These options flags
 * are set by GetDeviceOption().
 */
#define PMO_AUTHENTICATE        0x0001
#define PMO_HOLD                0x0002
#define PMO_PARK                0x0004
#define PMO_UNPARK              0x0008
#define PMO_SNIFF               0x0010
#define PMO_UNSNIFF             0x0020
#define PMO_ENCRYPTION_ON       0x0040   
#define PMO_ENCRYPTION_OFF      0x0080   
#define PMO_SWITCH_ROLE         0x0100   
#define PMO_DELETE_KEY          0x0200
#define PMO_PARK_ALL            0x0400

/*
 * Bluetooth Manager Menu identifiers
 *
 * PMM values are used in conjunction with the PMO_ values as popup
 * menu identifiers. When used as menu id's the PMO & PMM values are
 * not treated as bit flags but as values
 */
#define PMM_EMPTY_LIST          0x0003
#define PMM_ADD_DEVICE          0x0005
#define PMM_BROWSE              0x0007

/*
 * Bluetooth Manager states 
 */
#define BMSTATE_IDLE            0
#define BMSTATE_INITIALIZED     1
#define BMSTATE_RADIO_READY     2
#define BMSTATE_SHUTTING_DOWN   3

/*
 * Bluetooth Manager radio activity tracking
 */
#define BMG_NO_ACTIVITY             0x00
#define BMG_INQUIRY_ACTIVE          0x01
#define BMG_NAMEREQ_ACTIVE          0x02

/*
 * Bluetooth Control Panel default settings 
 */
#define BSDK_CONTROL_DEFAULTS {                                             \
    /* ME_Inqiury() IAC */                                                  \
    BT_IAC_GIAC,                                                            \
    /* Sniff Info: 0.5 to 5 sec interval; 1 sec sniff attempt & timeout */  \
    {0x1F40, 0x0320, 0x0320, 0x0320},                                       \
    /* ME_SetAccessibleModeC() Access Mode Info */                          \
    {BT_DEFAULT_INQ_SCAN_INTERVAL, BT_DEFAULT_INQ_SCAN_WINDOW, BT_DEFAULT_PAGE_SCAN_INTERVAL, BT_DEFAULT_PAGE_SCAN_WINDOW}, \
    /* ME_AccessibleModeNC() Access Mode Info */                            \
    {BT_DEFAULT_INQ_SCAN_INTERVAL, BT_DEFAULT_INQ_SCAN_WINDOW, BT_DEFAULT_PAGE_SCAN_INTERVAL, BT_DEFAULT_PAGE_SCAN_WINDOW}, \
    /* Hold Minimum, Maximum: 10.24 seconds (=GAP100 Inquiry time) */       \
    0x4000, 0x4000,                                                         \
    /* Park Beacon Min, Max interval: 0.5 - 5.0 seconds */                  \
    0x0320, 0x1F40,                                                         \
    /* Link Policy for inbound connections */                               \
    ((BLP_MASTER_SLAVE_SWITCH|BLP_HOLD_MODE|BLP_SNIFF_MODE|BLP_PARK_MODE|BLP_SCATTER_MODE)&(~BLP_MASK)),  \
    /* Link Policy for outbound connections */                              \
    ((BLP_MASTER_SLAVE_SWITCH|BLP_HOLD_MODE|BLP_SNIFF_MODE|BLP_PARK_MODE|BLP_SCATTER_MODE)&(~BLP_MASK)),  \
    /* Fixed Pin code, its length and type */                               \
    0, 0, 0,                                                                \
    /* Bluetooth manager Inquiry Style */                                   \
    (BMG_DO_NAMEREQ|BMG_AUTO_REFRESH),                                      \
    /* Alert the user when data is sent on a parked/sniffing ACL */         \
    TRUE }

/*
 * Bluetooth Manager property pages
 */
#define PROPERTY_PAGES {                                                        \
    { "Accessibility", MAKEINTRESOURCE(BDM_ACCESS), TabDlgAccess, 0},           \
    { "Policies", MAKEINTRESOURCE(BDM_POLICY), TabDlgPolicy, 0 },               \
    { "Hold, Park, Sniff", MAKEINTRESOURCE(BDM_INTERVALS), TabDlgIntervals, 0 },\
    { "Blue Manager", MAKEINTRESOURCE(BDM_MORE), TabDlgMore, 0 },               \
    { 0, 0, 0 } }

/*
 * SDP Query for Public Service Browsing
 */
#define SERVICE_BROWSE_QUERY {                                                      \
    SDP_ATTRIB_HEADER_8BIT(3),              /* Data Element sequence, 3 bytes */    \
    SDP_UUID_16BIT(SC_PUBLIC_BROWSE_GROUP), /* Public Browse Root UUID value */     \
    0x02,0xA0,                              /* Max number of result bytes: 672 */   \
    SDP_ATTRIB_HEADER_8BIT(5),              /* Data element sequence, 5 bytes */    \
    SDP_UINT_32BIT(AID_SERVICE_CLASS_ID_LIST) /* Attrib Range = only SC Id's */     \
}


/*
 * Shortcut to dereference (void *) context pointer in BtDeviceContext. This
 * is where the BlueMgr (aka Device Selection manager) stores it's context.
 */
#define EXT(_DEV)    ((BtDeviceExt *)(_DEV)->context)

/*
 * Custom Application Windows messages
 */
#define WM_RELOAD           (WM_APP+1)   /* Reload Property page values */
#define WM_PIN_REQ          (WM_APP+2)   /* Prompt user for PIN code */
#define WM_PIN_CANCEL       (WM_APP+3)   /* Cancel PIN code request */
#define WM_AUTHORIZE_REQ    (WM_APP+4)   /* Prompt user for authorization */
#define WM_AUTHORIZE_CANCEL (WM_APP+5)   /* Cancel authorization request */
#define WM_ACTIVE_WND       (WM_APP+100) /* Active window change */

/*
 * This structure is used to pass information to the EnterPinDlgProc().
 * The Flags are the BtPairingType(s) defined in "sec.h" (BPT_).
 */
typedef struct _PinCodeRequest {
    U8          Pin[MAX_PIN_LEN];
    U8          PinLen;
    U8          Flags;
    const char *DeviceDesc;
} PinCodeReq;

typedef struct _AuthorizeReq {
   /* 0 - None, 1 - This time, 2 - Rest of ACL, 
    * 3 - forever */
    U8   level; 
    /* Name of device */
    const char *deviceDesc;
} AuthorizeReq;

/*
 * Bluetooth Manager main context.
 */
typedef struct _BlueMgrGuiContext 
{
    HWND                chooserWnd;         /* Window handles */
    HWND                snifferWnd;
    HWND                propertyWnd;
    HWND                deviceListWnd;
    HWND                debugWnd;
    HWND                statsWnd;
    HWND                browseWnd;
    HWND                toolTipWnd;
    U32                 lastInqTime;        /* Last inquiry time */
    HINSTANCE           hInstance;
    BtSelectDeviceToken *activeQuery;       /* Current SelectDevice query */
    HIMAGELIST          smallIcons;         /* Device Icon list */
    BtDeviceContext    *menuDevice;         /* Device requesting menu */
    BtDeviceExt         btDevices[NUM_KNOWN_DEVICES];   /* Device extensions */
    BtHandler           globalHandler;      /* General event handler */
    BtHandler           securityHandler;    /* Security event handler */
    BtHandler           browserHandler;     /* Browser connection handler */
    MeCommandToken      nameTok;            /* Name request token */
    BtRemoteDevice     *browseDev;          /* Currently browsing device */
    SdpQueryToken       browseToken;        /* SDP Public Browse request */
    HTREEITEM           browseTree;         /* Node of current tree */

    struct {                                /* State machine for parsing SDP */
        U8              state;              /* Public service browse results. */
        U8              stage[5];
        U32             stageLen;
        U8              stageOff;
        U32             skipLen;
    } browser;

    U8                  ftsActive;          /* FTS Connection state */
    U8                  state;              /* BMSTATE_XXX */
    BtSecurityMode      securityMode;       /* BSM level from sec.h */
    U8                  activity;           /* BMG_XXX_ACTIVE */
    U8                  numConns;           /* Number of active ACL connections */
    BOOL                chooserOpen;        /* TRUE: chooser visible */
    char                fixedPin[MAX_PIN_LEN];  /* Value of fixed PIN Code */
    char                deviceName[BT_MAX_REM_DEV_NAME];    /* Local device name */
} BlueMgrGuiContext;

/*
 * Bluetooth Manager property page tab info.
 */
typedef struct _tabInfo {
    char        *title;             /* Tab title */
    LPCTSTR      tmplt;             /* Tab window template */
    DLGPROC      dlgProc;           /* Tab dialog procedure */
    HWND         handle;            /* Tab window handle */
} tabInfo;

/*
 * Bluetooth Manager property page trackbar info.
 */
typedef struct _trackbarInfo {
    int         trackbar;
    int         buddy;
    int         label;
    int         min;
    int         max;
    int         increment;  /* in ms */
    int         ticfreq;
    int         pagesize;
    U16         btdefault;
} trackbarInfo;

/*
 * Private BlueMgrGui Functions 
 */
static void InitListView(void);
static void OnNotify(NMHDR *Nmh);
static void OnToolTip(NMHDR *Nmh);
static void PopupMenu(WPARAM wParam, LPARAM lParam);
static void UpdateChooserStatus(void);
static BOOL InitializeGui(HWND hWndParent);
static BOOL CALLBACK ChooserWndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
static BOOL CALLBACK PropertyWndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
static BOOL CALLBACK TabDlgAccess(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
static BOOL CALLBACK TabDlgIntervals(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
static BOOL CALLBACK TabDlgPolicy(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
static BOOL CALLBACK TabDlgMore(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
static BOOL CALLBACK GetBdAddrDlgProc(HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam);
static BOOL CALLBACK AboutDlgProc(HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam);
static BOOL CALLBACK EnterPinDlgProc(HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam);
static BOOL CALLBACK EnterAuthorizeDlgProc(HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam);
static BOOL CALLBACK DebugWndProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam);
static BOOL CALLBACK StatsWndProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam);
static BOOL CALLBACK BrowserWndProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam);
static BOOL CALLBACK AddTTipEnumProc(HWND hwndCtrl, LPARAM lParam);
static void TabOnNotify(NMHDR *Nmh);
static int  AddDevice(BtDeviceContext *Bdc, BtDeviceExt *Template);
static void RemoveDevice(BtDeviceContext *Device);
static BtDeviceContext *GetSelectedDeviceContext(void);
static BtDeviceContext *GetDeviceContextByIndex(U32 Index);
static U16  GetDeviceOptions(BtDeviceContext *Bdc);
static void GlobalEventHandler(const BtEvent *Event);
#if BT_SECURITY == XA_ENABLED
static void SecurityEventHandler(const BtEvent *Event);
#endif
static void BmReport(const char *format,...);
static void UpdateDeviceState(BtDeviceContext *Bdc);
static U16  BtCod2String(BtClassOfDevice Cod, char *Result, U16 Max);
static void RadioStatusChange(BtEventType Status);
static void UpdateStatusMsg(void);
static void RunNameQuery(void);
static void StatReport(const char *format,...);
#if SDP_PARSING_FUNCS == XA_ENABLED
static void BrowserEventHandler(const BtEvent *Event);
static void RunServiceBrowse(BtDeviceContext *Device);
static void BrowserParseResults(const U8 *Results, U32 Length);
static void BrowserEnd(const char *StatusMsg);
#endif

extern int __cdecl _CrtDbgReport(int, const char *, int, const char *, const char *, ...);


#endif /* __BLUEMGRI_H */

