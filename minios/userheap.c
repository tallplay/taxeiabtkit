/*****************************************************************************
 *
 * File:
 *     $Workfile:userheap.c$ for API Tester
 *     $Revision:8$
 *
 * Description:
 *     A compact, portable heap manager
 * 
 * Copyright 1999-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions,
 * Inc.  This work contains confidential and proprietary information of
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret,
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "utils.h"
#include "osapi.h"

/****************************************************************************
 *
 * Internal interface
 *
 ****************************************************************************/


/*---------------------------------------------------------------------------
 * UhBlockFlags constant
 *
 *     When enabled, collects heap statistics which can be output using
 *     UH_DumpStatistics(). XA_DEBUG must be enabled to use this feature.
 */
#ifndef UH_STATISTICS
#define UH_STATISTICS XA_DEBUG
#endif
#if (UH_STATISTICS == XA_ENABLED) && (XA_DEBUG != XA_ENABLED)
#error "UH_STATISTICS requires XA_DEBUG!"
#endif


/*---------------------------------------------------------------------------
 * UhBlockFlags type
 *
 *     Indicates how a particular block in a heap is used
 */
typedef U8 UhBlockFlags;

#define UHF_ALLOCATED      0x01 /* This memory block is in use */
#define UHF_LAST           0x02 /* This is the final block available */
#define UHF_FLAGMASK       0x03 /* This is the OR of all acceptable flags */

/* End of type */

/*---------------------------------------------------------------------------
 * UhStatistics structure
 * 
 *     Records heap information for display with UH_DumpStatistics().
 *     Requires UH_STATISTICS == XA_ENABLED
 */
typedef struct _UhStatistics
{
    /* Original size of the heap */
    U16 heapSize;

    /* Maximum outstanding memory allocation */
    U16 maxAllocated;

    /* Maximum number of (in use) segments available */
    U16 maxNumSegments;

    /* Minimum size of the biggest available block at any one point */
    U16 minAvailable;

} UhStatistics;

#define UH_STATISTICS_SIZE ALIGN(sizeof(UhBlockHeader))

/*---------------------------------------------------------------------------
 * UhBlockHeader structure
 *
 *     Describes the characteristics of a memory block in use. This
 *     information is read/written before each block of memory.
 */
typedef struct _UhBlockHeader
{
    /* Total length of this block, NOT including header length */
    U16 len;           
    
     /* Flags applied to this block */
    UhBlockFlags flags;

#if XA_DEBUG == XA_ENABLED
    U8 *heap;      /* Location of start of heap */
#endif /* XA_DEBUG */

} UhBlockHeader;

#define UH_BLOCK_HEADER_SIZE ALIGN(sizeof(UhBlockHeader))


/****************************************************************************
 *
 * Debugging routines
 *
 ****************************************************************************/

#if XA_DEBUG == XA_ENABLED

/* Prints out contents of the heap */
static void UH_ReportHeap(U8 *heap)
{
    UhBlockHeader *header;
    U8 *curPos;
    U16 count;

    Report(("UH: Heap @%lx:\n",heap));

    count = 0;
    curPos = heap;

#if UH_STATISTICS == XA_ENABLED
    curPos += UH_STATISTICS_SIZE;
#endif
   
    /* Step through blocks */
    do {
        header = (UhBlockHeader *)curPos;
        count++;
        Report(("UH:   Block %d (@%lx): Len=%d Flags=%x\n", count, curPos, header->len, header->flags));
        curPos += header->len + UH_BLOCK_HEADER_SIZE;
    } while (!(header->flags & UHF_LAST));     
}


/* Checks through heap contents to ensure they are safe. */
static BOOL UH_VerifyHeap(U8 *heap)
{
#if UH_STATISTICS == XA_ENABLED
    UhStatistics *globalStats;
    UhStatistics stats;
    U16 contig = 0;
#endif
    U32 total = 0;
    UhBlockHeader *header;
    U8 *curPos;
    
    curPos = heap;
    
#if UH_STATISTICS == XA_ENABLED
    globalStats = (UhStatistics *)curPos;
    stats.maxAllocated = 0;
    stats.maxNumSegments = 0;
    stats.minAvailable = 0;
    curPos += UH_STATISTICS_SIZE;
    total += UH_STATISTICS_SIZE;
#endif /* UH_STATISTICS */

    /* Step through blocks */
    do {
        header = (UhBlockHeader *)curPos;
        Assert(!(header->flags & ~UHF_FLAGMASK));

        /* Maximum heap is 32k; verify total size is below this */
        total += header->len + UH_BLOCK_HEADER_SIZE;
        Assert(total < 0x10000);

#if UH_STATISTICS == XA_ENABLED
        if (header->flags & UHF_ALLOCATED)
        {
            stats.maxAllocated += header->len;
            stats.maxNumSegments ++;
            contig = 0;
        }
        else
        {
            /* If we notice contiguous unallocated blocks, recon them
             * as a single allocatable block */
            contig += header->len + UH_BLOCK_HEADER_SIZE;
            stats.minAvailable = max(stats.minAvailable, contig - UH_BLOCK_HEADER_SIZE);
        }
#endif /* UH_STATISTICS */

        curPos += header->len + UH_BLOCK_HEADER_SIZE;
    } while (!(header->flags & UHF_LAST));

#if UH_STATISTICS == XA_ENABLED
    globalStats->maxAllocated = max(globalStats->maxAllocated, stats.maxAllocated);
    globalStats->maxNumSegments = max(globalStats->maxNumSegments, stats.maxNumSegments);
    globalStats->minAvailable = min(globalStats->minAvailable, stats.minAvailable);
#endif /* UH_STATISTICS */

/*    UH_ReportHeap(heap); */
    
    return TRUE;
}

#else /* XA_DEBUG */
#define UH_VerifyHeap(a) 
#define UH_ReportHeap(a)
#endif /* XA_DEBUG */

/****************************************************************************
 *
 * Public functions
 *
 ****************************************************************************/

/*---------------------------------------------------------------------------
 *            UH_CreateHeap()
 *---------------------------------------------------------------------------
 * Builds heap for alloc/free to use
 */
void UH_CreateHeap(U8 *buffer, U16 len)
{
    U8 *curPos;
    curPos = buffer;

#if UH_STATISTICS == XA_ENABLED
    Assert(len > UH_BLOCK_HEADER_SIZE + UH_STATISTICS_SIZE);
    /* Initialize the statistics block */
    ((UhStatistics *)curPos)->heapSize = len;
    ((UhStatistics *)curPos)->maxAllocated = 0;
    ((UhStatistics *)curPos)->maxNumSegments = 0;
    ((UhStatistics *)curPos)->minAvailable = len - UH_BLOCK_HEADER_SIZE - sizeof(UhStatistics);
    curPos += UH_STATISTICS_SIZE;
#endif
    Assert(len > UH_BLOCK_HEADER_SIZE);

    /* Write the initial header */
    ((UhBlockHeader *)curPos)->len = len - UH_BLOCK_HEADER_SIZE;
#if UH_STATISTICS == XA_ENABLED
    ((UhBlockHeader *)curPos)->len -= UH_STATISTICS_SIZE;
#endif

    ((UhBlockHeader *)curPos)->flags = UHF_LAST;
#if XA_DEBUG == XA_ENABLED
    ((UhBlockHeader *)curPos)->heap = buffer;
#endif /* XA_DEBUG */

    Assert(UH_VerifyHeap(buffer));
}


/*---------------------------------------------------------------------------
 *            UH_Alloc()
 *---------------------------------------------------------------------------
 * Returns a pointer to allocated memory of the specified size. 
 */
U8 *UH_Alloc(U8 *heap, U16 size)
{
    UhBlockHeader *curHeader, *nextHeader;
    U8 *curPos, success;
    
    Assert(heap);
    Assert(UH_VerifyHeap(heap));

    size = ALIGN(size);
    curPos = heap;
    success = FALSE;

#if UH_STATISTICS == XA_ENABLED
    curPos += UH_STATISTICS_SIZE;
#endif

    /* Search for a free block */
    do {
        curHeader = (UhBlockHeader *)curPos;

        /* At an unallocated block? */
        if (!(curHeader->flags & UHF_ALLOCATED))
        {
            /* Does the block allow a split? */
            if (curHeader->len >= size + UH_BLOCK_HEADER_SIZE)
            {
                /* Split block in two (allocated + unallocated) */
                nextHeader = (UhBlockHeader *)(curPos + UH_BLOCK_HEADER_SIZE + size);
                nextHeader->flags = curHeader->flags;
                nextHeader->len = curHeader->len - (UH_BLOCK_HEADER_SIZE + size);
#if XA_DEBUG == XA_ENABLED
                nextHeader->heap = curHeader->heap;
#endif /* XA_DEBUG */

                curHeader->len = size;
                curHeader->flags |= UHF_ALLOCATED;
                curHeader->flags &= ~UHF_LAST;

                success = TRUE;
                break;
            }
            /* Is the block exactly the right size? */
            else if (curHeader->len == size)
            {
                curHeader->flags |= UHF_ALLOCATED;
                success = TRUE;
                break;
            }
            /* The block is too small/wrong size, merge with next? */
            else if (!(curHeader->flags & UHF_LAST))
            {
                /* Let's look at the next block. Perhaps it is also not allocated
                 * and we can merge it.
                 */
                nextHeader = (UhBlockHeader *)(curPos + UH_BLOCK_HEADER_SIZE + curHeader->len);
                if (!(nextHeader->flags & UHF_ALLOCATED))
                {
                    curHeader->len += nextHeader->len + UH_BLOCK_HEADER_SIZE;

                    /* Absorb all flags from the next header */
                    curHeader->flags |= nextHeader->flags;
                    continue;
                }
            }
        }

        /* Are we done looking? */
        if (curHeader->flags & UHF_LAST) 
        {
            break;
        }

        /* Advance to location of next header */
        curPos += curHeader->len + UH_BLOCK_HEADER_SIZE;
    } while (!success);

    if (success)
    {
#if UH_STATISTICS == XA_ENABLED
        /* Use VerifyHeap to gather the latest statistics */
        Assert(UH_VerifyHeap(heap));
#endif /* UH_STATISTICS == XA_ENABLED */

#if XA_DEBUG == XA_ENABLED
        OS_MemSet((U8 *)(curPos + UH_BLOCK_HEADER_SIZE), 0xDD, size);
#endif
        return (U8 *)(curPos + UH_BLOCK_HEADER_SIZE);
    }


    /* Failed to find an acceptable block */
    return 0;
}

/*---------------------------------------------------------------------------
 *            UH_Free()
 *---------------------------------------------------------------------------
 * Releases heap memory back to the heap.
 */
void UH_Free(U8 *block)
{
    UhBlockHeader *header;
    Assert(block);
    header = (UhBlockHeader *)(block - UH_BLOCK_HEADER_SIZE);
    Assert(header->flags & UHF_ALLOCATED);
    Assert(UH_VerifyHeap(header->heap));

    /* Update header flag and store off header. */
    header->flags &= ~UHF_ALLOCATED;

#if UH_STATISTICS == XA_ENABLED
    Assert(UH_VerifyHeap(header->heap));
#endif /* UH_STATISTICS == XA_ENABLED */
}

#if UH_STATISTICS == XA_ENABLED
/*---------------------------------------------------------------------------
 *            UH_DumpStatistics()
 *---------------------------------------------------------------------------
 * Report() on usage of the heap
 */
void UH_DumpStatistics(U8 *heap)
{
    UhStatistics *globalStats;
    globalStats = (UhStatistics *)heap;
    
    Report(("UH: Original heap size: %d\n", globalStats->heapSize));
    Report(("UH: Maximum memory allocated: %d\n", globalStats->maxAllocated));
    Report(("UH: Maximum number of segments allocated: %d\n", globalStats->maxNumSegments));
    Report(("UH: Smallest available memory chunk: %d\n", globalStats->minAvailable));
}
#endif /* UH_STATISTICS == XA_ENABLED */
