//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by ObexGui.rc
//
#define OBEX_QBEAM                      101
#define OBEX_GET                        103
#define IDB_LOGO                        105
#define QUERY_TARGET                    108
#define OBEX_SETPATH                    109
#define QBEAM_SPLASH                    111
#define IDB_SPLASH                      114
#define IPP_PICK_TCPPORT                118
#define OBEX_SERVER_TCP                 118
#define OBEX_SET_TIMEOUT                119
#define OBEX_SNIFFER_MAIN               124
#define OBEX_USER_ID                    126
#define OBEX_PASSWORD                   127
#define ID_CONNECT                      1000
#define ID_DISCONNECT                   1001
#define ID_PUT                          1002
#define ID_GET                          1003
#define ID_EXIT                         1004
#define ID_CLIENT_CON_REQ               1005
#define ID_CLIENT_DISC_REQ              1006
#define OBEX_CLIENT_OUTPUT              1007
#define ID_CLIENT_ABORT                 1008
#define ID_SERVER_ABORT                 1009
#define IDC_CLIENT_PROGRESS             1010
#define IDC_SERVER_PROGRESS             1011
#define OBEX_SERVER_OUTPUT              1012
#define ID_PUSH                         1013
#define ID_EXCHANGE                     1014
#define ID_SETPATH                      1015
#define IPA_IPADDRESS                   1015
#define IDC_GET_OBJECT                  1016
#define IPA_OK                          1016
#define IDC_CLIENT_PROGRESS2            1016
#define IPA_CANCEL                      1017
#define IDC_SERVER_PROGRESS2            1017
#define IPA_BDADDR                      1018
#define ID_CREATE_SESSION               1018
#define IPA_PICK_BDADDR                 1019
#define ID_CLOSE_SESSION                1019
#define IPA_PICK_IPADDR                 1020
#define ID_SUSPEND_SESSION              1020
#define IPA_PICK_IRDA                   1021
#define IDC_SET_PATH                    1021
#define ID_RESUME_SESSION               1021
#define ID_SET_CLIENT_SESSION_TIMEOUT   1022
#define ID_SET_SERVER_SESSION_TIMEOUT   1023
#define IDC_NO_CREATE                   1024
#define ID_RELIABLE_PUSH                1024
#define ID_SERVER_CON_REQ               1024
#define ID_SNIFFER                      1024
#define IDC_BACKUP                      1025
#define IDB_BITMAP                      1025
#define ID_SERVER_DISC_REQ              1025
#define IDC_VERSION                     1026
#define IDC_RESET                       1026
#define ID_SET_PASS                     1026
#define ID_SET_USER_ID                  1027
#define IPA_PICK_TCPPORT                1028
#define IPA_TCP_PORT                    1029
#define IPP_PICK_TCPPORT2               1030
#define ID_CLIENT2_CHECKBOX             1033
#define IPP_TCP_PORT2                   1033
#define IPP_TCP_PORT                    1034
#define IPP_OK                          1035
#define IPP_CANCEL                      1036
#define IPP_INTRO                       1037
#define IPP_SET_TIMEOUT                 1038
#define ID_SESSION2_CHECKBOX            1040
#define ID_AUTO_RESUME_CHECKBOX         1052
#define IDC_DEFAULT_VCARD               1055
#define OBEX_SNIFFER_LISTBOX            1056
#define IPP_PASSWORD                    1057
#define IPP_USER_ID                     1058
#define ID_OBEX_AUTH                    1059
#define ID_SNIFFER_CLEAR                40008
#define ID_SNIFFER_SAVEAS               40012

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1060
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
