#ifndef __OVERIDE_H
#define __OVERIDE_H

/****************************************************************************
 *
 * File:
 *     $Workfile:overide.h$ for XTNDAccess Blue SDK, Version 1.3
 *     $Revision:62$
 *
 * Description:
 *     Configuration overrides for the qbeam project.
 * 
 * Copyright 1999-2005 Extended Systems, Inc.  ALL RIGHTS RESERVED.
 *
 * Unpublished Confidential Information of Extended Systems, Inc.
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of Extended Systems, Inc.
 * 
 * Use of this work is governed by a license granted by Extended Systems,
 * Inc.  This work contains confidential and proprietary information of
 * Extended Systems, Inc. which is protected by copyright, trade secret,
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

/* WARNING: The values in this overide.h file were selected specifically for
 * this sample application. If you change them, the sample application may fail
 * to compile or not work properly.
 */

/*
 * Enable the desired stacks to run with OBEX. 
 *
 *     Since OBEX can be configured to run over a mix of stacks. The
 *     control of which stacks are included has been moved to the
 *     individual workspaces where it can be better managed.
 */
#ifdef BTSTACK
#define BT_STACK                        XA_ENABLED
#else
#define BT_STACK                        XA_DISABLED
#endif

#ifdef TCPSTACK
#define TCP_STACK                       XA_ENABLED
#else
#define TCP_STACK                       XA_DISABLED
#endif

#ifdef IRDASTACK
#define IRDA_STACK                      XA_ENABLED
#else
#define IRDA_STACK                      XA_DISABLED
#endif

#define XA_LOAD_LIST                    XA_MODULE(OBEX)

/*
 * QBeam application specific options
 */

/* QBeam uses the Event Manager for managing suspended session timeouts
 * and to initialize OBEX. 
 */
#define XA_EVENTMGR                     XA_ENABLED

/* QBeam application name, set by the PME when Bluetooth is enabled. */
#define PME_APP_NAME                    "QBeam App"

#define BT_ALLOW_SCAN_WHILE_CON         XA_ENABLED

/*
 * IrDA specific options that QBeam requires.
 */
#if IRDA_STACK == XA_ENABLED
#include <frmconfig.h>
/* Number of bound connections to the IrDA stack.  We need space for the IAS Server, 
 * IAS Client, two OBEX Clients and two OBEX Servers.  This is a minimum of 6.
 */
#define IR_CONS_SIZE                    6
/* IAS database entries for the device name, OBEX class name, IrXfer class name, 
 * and CUSTOM class name (for the second OBEX server). This is a minimum of 4.
 */
#define IR_IAS_SERVER_SIZE              4
#define IR_ADD_MS_TTP_PATCH             XA_ENABLED
#endif

/*
 * OBEX specific options that QBeam requires.
 */
#define OBEX_DEINIT_FUNCS               XA_ENABLED

/*
 * Optional OBEX Configuration options.
 */
#define OBEX_SERVER_CONS_SIZE           2
#define OBEX_SESSION_SUPPORT            XA_ENABLED
#define OBEX_ALLOW_SERVER_TP_DISCONNECT XA_ENABLED
#define OBEX_AUTHENTICATION             XA_ENABLED

/*
 * Bluetooth optional options.
 */
#if BT_STACK == XA_ENABLED
#define XA_SNIFFER                      XA_ENABLED
#define L2CAP_MTU                       (672*3)     /* Optional: Triple L2CAP PDU size */

/* Only needed for the USB transport. */
#define HCI_USB_TX_BUFFER_SIZE          260

#endif

/*
 * IrDA optional options.
 */
#if IRDA_STACK == XA_ENABLED
#define IR_LMP_DISCONNECT               XA_ENABLED  /* Optional */
#define IR_DISC_THRESHOLD               IRDT_40     /* 40 second link disconnect time */
#endif

#if ((IRDA_STACK == XA_ENABLED) || (TCP_STACK == XA_ENABLED)) && BT_STACK == XA_DISABLED
/* This option enables the OBEX Sniffer hooks */
#define JETTEST                     XA_ENABLED
#else
/* OBEX Sniffer not used in Bluetooth since the BlueMgr has a sniffer */
#define JETTEST                     XA_DISABLED
#endif

#endif /* __OVERIDE_H */
