/*****************************************************************************
 * File:        qbeam_gui.c
 *
 * Description: This file contains GUI code for QBeam sample application.
 *
 * Created:     December 22, 1999
 *
 * $Project:XTNDAccess IrDA SDK$
 *
 * Copyright 1999-2005 Extended Systems, Inc.  ALL RIGHTS RESERVED.
 *
 * Unpublished Confidential Information of Extended Systems, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any means, 
 * or stored in a database or retrieval system, without prior written 
 * permission of Extended Systems, Inc.
 * 
 * Use of this work is governed by a license granted by Extended Systems, Inc.  
 * This work contains confidential and proprietary information of Extended 
 * Systems, Inc. which is protected by copyright, trade secret, trademark and 
 * other intellectual property rights.
 *
 ****************************************************************************/
#define STRICT
#include <windows.h>
#include <commctrl.h>
#include <stdio.h>
#include "resource.h"
#include <direct.h>  /* For _getcwd() */

#include <osapi.h>
#if BT_STACK == XA_ENABLED
#include <bttypes.h>
#include <me.h>
#include <bluemgr_ui.h>
#include <sys/debug.h>
#endif

#include <obstore.h>
#include <obex.h>
#include <qbeam.h>

#define XA_NAME             "XTNDAccess Embedded SDK"
#if XA_DEBUG == XA_ENABLED
#define XA_VERSION          "Version 1.3 Debug Build \n(� 2000-2002 Extended Systems Incorporated)"
#else
#define XA_VERSION          "Version 1.3 Release Build \n(� 2000-2002 Extended Systems Incorporated)"
#endif

/* Internal Global variables for QBeam GUI */
HWND        AppWnd;
HINSTANCE   hInst;
char        objectName[OBS_MAX_NAME_LEN];
char        lastObject[OBS_MAX_NAME_LEN];
char        pathName[64];
U8          AppQbInstance;

#if BT_STACK == XA_ENABLED
/* This structure is used by AcquireTarget() to maintain context during
 * the Blue Manager DS_SelectDevice() function call.
 */
typedef struct _QbDevQuery {
    BtSelectDeviceToken     sd;
    WPARAM                  context;
    ObexTpAddr              target;
} QbDevQuery;

static void BtEventHandler(const BtEvent *Event);
static LPTSTR QbGetToolTipText(int ControlId);
#endif

/* Internal function prototypes */
static void CALLBACK  TimerExpired(HWND hWnd, UINT uMsg, UINT idEvent, DWORD dwTime);
static BOOL CALLBACK  AppWndProc(HWND, UINT, WPARAM, LPARAM);
static BOOL CALLBACK  GetDlgProc(HWND, UINT, WPARAM, LPARAM);
static BOOL CALLBACK  SetPathDlgProc(HWND, UINT, WPARAM, LPARAM);
#if OBEX_SESSION_SUPPORT == XA_ENABLED
static BOOL CALLBACK SetSessionTimeoutDlgProc(HWND, UINT, WPARAM, LPARAM);
#endif /* OBEX_SESSION_SUPPORT == XA_ENABLED */
static BOOL CALLBACK  GetTpAddrDlgProc(HWND, UINT, WPARAM, LPARAM);
static void           AcquireTargetAddr(WPARAM);
#if OBEX_AUTHENTICATION == XA_ENABLED
static BOOL CALLBACK PasswordDlgProc(HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam);
static BOOL CALLBACK UserIdDlgProc(HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam);
#endif /* OBEX_AUTHENTICATION == XA_ENABLED */
HWND Splash_Create(HINSTANCE Inst, HWND appWnd, int res_id, int CmdShow);

char* App_GetSavedObject(void);
void App_Report(char *format,...);
void NotifyClientProgress(U8 deviceNum);
void NotifyServerProgress(U8 deviceNum);
void SetServerUIContext(ObStoreHandle object, U8 deviceNum);
void SetClientUIContext(ObStoreHandle object, U8 deviceNum);

extern BOOL IsObexSnifferOpen();
extern HWND StartObexSniffer(HINSTANCE Instance, int CmdShow);
extern void StopObexSniffer(void);

/****************************************************************************
 ** Main Entrypoint
 **/
HWND* APP_Init( HINSTANCE Inst, HINSTANCE PrevInst, LPTSTR CmdLine, int CmdShow )
{
    INITCOMMONCONTROLSEX    icc;
#if TCP_STACK == XA_ENABLED
    static U32              serverPorts;
#endif /* TCP_STACK == XA_ENABLED */
    U16                     server[2] = {0};
    U8                      i;

    hInst = Inst;

    icc.dwSize = sizeof(icc);
    icc.dwICC = 0;
    icc.dwICC |= ICC_INTERNET_CLASSES;

    if (InitCommonControlsEx(&icc) == FALSE)
        return 0;

    AppWnd = CreateDialog(Inst, MAKEINTRESOURCE(OBEX_QBEAM), NULL, AppWndProc);
    if (AppWnd == NULL)
        return 0;

#if BT_STACK == XA_ENABLED
    if (BMG_Initialize(Inst, AppWnd) == 0) {
        DestroyWindow(AppWnd);
        return 0;
    }
    BMG_AddMainMenu(AppWnd);
    BMG_AddToolTips(AppWnd, QbGetToolTipText);
#endif

    ShowWindow( AppWnd, CmdShow );

    /* Now initialize the QBeam applicaiton and Object Store. */
    for (i = 0; i < NUM_INSTANCES; i++) {
        if (Qb_Init(i) == FALSE) {
            SetTimer(AppWnd, 0, (1000*3), TimerExpired);
            break;
        }
    }
    
    Splash_Create(Inst, AppWnd, QBEAM_SPLASH, CmdShow);
    
    return &AppWnd;
}

void APP_Deinit(void)
{
    U8      i;

    for (i = 0; i < NUM_INSTANCES; i++)
        Qb_Deinit(i);
}

void APP_Thread(void)
{
    /* Unused */
}

static void CALLBACK TimerExpired(HWND hWnd, UINT uMsg, UINT idEvent, DWORD dwTime)
{
    switch (uMsg) {
    case WM_TIMER:
        KillTimer(hWnd, 0);
        APP_Deinit();
    }
}

#if OBEX_AUTHENTICATION == XA_ENABLED
BOOL App_Authenticate(void)
{
    U32 btnState;

    btnState = SendDlgItemMessage(AppWnd, ID_OBEX_AUTH, BM_GETSTATE, 0L, 0L);
    if (btnState & BST_CHECKED) {
        return TRUE;
    }
    else return FALSE;
}

BOOL App_GetPassword(char *password)
{
    /* Popup OBEX Authentication Password Dialog */
    if (DialogBoxParam(hInst, MAKEINTRESOURCE(OBEX_PASSWORD), AppWnd, 
        PasswordDlgProc, (LPARAM)password) == TRUE)
        return TRUE;
    else return FALSE;
}

BOOL App_GetUserId(char *userId)
{
    /* Popup OBEX Authentication Password Dialog */
    if (DialogBoxParam(hInst, MAKEINTRESOURCE(OBEX_USER_ID), AppWnd, 
        UserIdDlgProc, (LPARAM)userId) == TRUE)
        return TRUE;
    else return FALSE;
}
#endif /* OBEX_AUTHENTICATION == XA_ENABLED */

static BOOL CALLBACK AppWndProc( HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam )
{
    OPENFILENAME  openFile;
    char          filter[50];
    ObexTpAddr   *target;
#if NUM_INSTANCES > 1
    U32           cbState;
#endif /* NUM_INSTANCES > 1 */
#if OBEX_SESSION_SUPPORT == XA_ENABLED
    U8            role;
#endif /* OBEX_SESSION_SUPPORT == XA_ENABLED */

    switch (uMsg) {

    case WM_INITDIALOG:
        objectName[0] = 0;
        lastObject[0] = 0;
#if NUM_INSTANCES == 1
        EnableWindow(GetDlgItem(hWnd, ID_CLIENT2_CHECKBOX), FALSE);
#endif
#if OBEX_SESSION_SUPPORT != XA_ENABLED
        EnableWindow(GetDlgItem(hWnd, ID_SESSION2_CHECKBOX), FALSE);
        EnableWindow(GetDlgItem(hWnd, ID_CREATE_SESSION), FALSE);
        EnableWindow(GetDlgItem(hWnd, ID_CLOSE_SESSION), FALSE);
        EnableWindow(GetDlgItem(hWnd, ID_SUSPEND_SESSION), FALSE);
        EnableWindow(GetDlgItem(hWnd, ID_RESUME_SESSION), FALSE);
        EnableWindow(GetDlgItem(hWnd, ID_SET_CLIENT_SESSION_TIMEOUT), FALSE);
        EnableWindow(GetDlgItem(hWnd, ID_SET_SERVER_SESSION_TIMEOUT), FALSE);
#endif /* OBEX_SESSION_SUPPORT != XA_ENABLED */
#if OBEX_ALLOW_SERVER_TP_DISCONNECT != XA_ENABLED
        EnableWindow(GetDlgItem(hWnd, ID_SERVER_DISC_REQ), FALSE);
#endif /* OBEX_ALLOW_SERVER_TP_DISCONNECT != XA_ENABLED */
#if OBEX_AUTHENTICATION != XA_ENABLED
        EnableWindow(GetDlgItem(hWnd, ID_OBEX_AUTH), FALSE);
        EnableWindow(GetDlgItem(hWnd, ID_SET_PASS), FALSE);
        EnableWindow(GetDlgItem(hWnd, ID_SET_USER_ID), FALSE);
#endif /* OBEX_AUTHENTICATION != XA_ENABLED */
#if JETTEST != XA_ENABLED
        EnableWindow(GetDlgItem(hWnd, ID_SNIFFER), FALSE);
#endif /* JETTEST != XA_ENABLED */
        return TRUE;

#if BT_STACK == XA_ENABLED
    case WM_PAINT: 
        /* Draws a separator below the Blue Manager menu. */
        {
            PAINTSTRUCT     ps; 
            RECT            rect;
            HDC             hdc; 

            hdc = BeginPaint(hWnd, &ps); 
            GetClientRect(hWnd, &rect); 
            rect.left += 2;
            rect.right -= 3;
            DrawEdge(hdc, &rect, EDGE_ETCHED, BF_TOP); 
            EndPaint(hWnd, &ps); 
        }
        break;
#endif /* BT_STACK == XA_ENABLED */

    case WM_COMMAND:
        switch (LOWORD(wParam)) {

        case ID_CONNECT:
            Qb_Connect(AppQbInstance);
            break;

        case ID_DISCONNECT:
            Qb_Disconnect(AppQbInstance);
            break;

        case ID_PUT:
            /* Post dialog to get name of object to PUT */
            memset( &openFile, 0, sizeof(openFile));
            openFile.lStructSize = sizeof(openFile);
            openFile.hwndOwner = AppWnd;
            openFile.lpstrFile = lastObject;
            openFile.nMaxFile = OBS_MAX_NAME_LEN;
            openFile.lpstrFileTitle = objectName;
            openFile.nMaxFileTitle = OBS_MAX_NAME_LEN;
            openFile.lpstrTitle = TEXT("Put Object");
            openFile.Flags = OFN_FILEMUSTEXIST|OFN_PATHMUSTEXIST;
            openFile.lpstrInitialDir = "\\";

            if (GetOpenFileName(&openFile) == 0)
                break;
               
            Qb_Put(AppQbInstance, objectName);
            break;

        case ID_GET:
            /* Popup Get Dialog */
            DialogBox(hInst, MAKEINTRESOURCE(OBEX_GET), AppWnd, GetDlgProc);
            break;

        case ID_SETPATH:
            /* Popup SetPath Dialog */
            DialogBox(hInst, MAKEINTRESOURCE(OBEX_SETPATH), AppWnd, SetPathDlgProc);
            break;

#if OBEX_AUTHENTICATION == XA_ENABLED
        case ID_SET_PASS:
            Qb_SetPassword(AppQbInstance);
            break;

        case ID_SET_USER_ID:
            Qb_SetUserId(AppQbInstance);
            break;        
#endif /* OBEX_AUTHENTICATION == XA_ENABLED */

#if OBEX_SESSION_SUPPORT == XA_ENABLED
        case ID_CREATE_SESSION:
            Qb_CreateSession(AppQbInstance);
            break;

        case ID_CLOSE_SESSION:
            Qb_CloseSession(AppQbInstance);
            break;

        case ID_SUSPEND_SESSION:
            Qb_SuspendSession(AppQbInstance);
            break;
        
        case ID_RESUME_SESSION:
            Qb_ResumeSession(AppQbInstance);
            break;
        
        case ID_SET_CLIENT_SESSION_TIMEOUT:
            role = QB_CLIENT;
            goto Dialog;
        
        case ID_SET_SERVER_SESSION_TIMEOUT:
            role = QB_SERVER;
Dialog:
            /* Popup Set Session Timeout Dialog */
            DialogBoxParam(hInst, MAKEINTRESOURCE(OBEX_SET_TIMEOUT), AppWnd, 
                           SetSessionTimeoutDlgProc, (LPARAM)role);
            break;
#endif /* OBEX_SESSION_SUPPORT == XA_ENABLED */

        /* Group: All of these operations require us to find a device first. */
        case ID_PUSH:
        case ID_EXCHANGE:
        case ID_CLIENT_CON_REQ:
            AcquireTargetAddr(LOWORD(wParam)+500);
            break;

        case ID_PUSH+500:
            if ((target = (ObexTpAddr *)lParam) == 0) {
                App_Report("Client: Error: You must Select a device to Push to first.");
                break;
            }
            

            memset(filter, 0, sizeof(filter));
            strcpy(filter, "All Files (*.*)");
            strcpy(filter+16, "*.*");

            /* Post dialog to get name of object to PUT */
            memset( &openFile, 0, sizeof(openFile));
            openFile.lStructSize = sizeof(openFile);
            openFile.hwndOwner = AppWnd;
            openFile.lpstrFilter = filter;
            openFile.lpstrFileTitle = objectName;
            openFile.nMaxFileTitle = OBS_MAX_NAME_LEN;
            openFile.lpstrTitle = TEXT("Select Push Object");
            openFile.Flags = OFN_FILEMUSTEXIST|OFN_PATHMUSTEXIST;
            openFile.lpstrInitialDir = "\\";

            if (GetOpenFileName(&openFile) == 0)
                break;

            Qb_ObjectPush(AppQbInstance, objectName, target);
            break;

        case ID_EXCHANGE+500:
            if ((target = (ObexTpAddr *)lParam) == 0) {
                App_Report("Client: Error: You must Select a device to Exchange with first.");
                break;
            }
      
            memset(filter, 0, sizeof(filter));
            strcpy(filter, "Business Cards (*.vcf)");
            strcpy(filter+23, "*.vcf");

            /* Post dialog to get name of object to PUT */
            memset( &openFile, 0, sizeof(openFile));
            openFile.lStructSize = sizeof(openFile);
            openFile.hwndOwner = AppWnd;
            openFile.lpstrFilter = filter;
            openFile.lpstrFileTitle = objectName; 
            openFile.nMaxFileTitle = OBS_MAX_NAME_LEN;
            openFile.lpstrTitle = TEXT("Select Business Card");
            openFile.Flags = OFN_FILEMUSTEXIST|OFN_PATHMUSTEXIST;
            openFile.lpstrInitialDir = "\\";

            if (GetOpenFileName(&openFile) == 0)
                break;

            Qb_BizExchange(AppQbInstance, objectName, target);
            break;

        case ID_CLIENT_CON_REQ+500:
            if ((target = (ObexTpAddr *)lParam) == 0) {
                App_Report("Client: Error: You must Select a device to Connect to first.");
                break;
            }
            
            Qb_ClientTpConnect(AppQbInstance, target);
            break;

        case ID_CLIENT_DISC_REQ:
            Qb_ClientTpDisconnect(AppQbInstance);
            break;

#if OBEX_ALLOW_SERVER_TP_DISCONNECT == XA_ENABLED
        case ID_SERVER_DISC_REQ:
            Qb_ServerTpDisconnect(AppQbInstance);
            break;
#endif /* OBEX_ALLOW_SERVER_TP_DISCONNECT == XA_ENABLED */

        case ID_SERVER_ABORT:
            Qb_AbortServer(AppQbInstance);
            break;

        case ID_CLIENT_ABORT:
            Qb_AbortClient(AppQbInstance);
            break;

        case ID_CLIENT2_CHECKBOX:
#if NUM_INSTANCES > 1
            cbState = SendDlgItemMessage(hWnd, ID_CLIENT2_CHECKBOX, BM_GETSTATE, 0L, 0L);
            if (cbState & BST_CHECKED) {
                AppQbInstance = 1;
            }
            else {
                AppQbInstance = 0;
            }
#endif /* NUM_INSTANCES > 1 */
            break;

#if JETTEST == XA_ENABLED
        case ID_SNIFFER:
            if (IsObexSnifferOpen()) {
                StopObexSniffer();
                SendMessage(GetDlgItem(AppWnd, ID_SNIFFER), WM_SETTEXT, 0, (LPARAM)"OBEX Sniffer");
            } else  {
                HWND    sniffer = StartObexSniffer(hInst, 0);
                RECT    appRect;

                SendMessage(GetDlgItem(AppWnd, ID_SNIFFER), WM_SETTEXT, 0, (LPARAM)"Stop Sniffer");
                GetWindowRect(AppWnd, &appRect);
                SetWindowPos(sniffer, 0, appRect.left, appRect.bottom, 0, 0, SWP_NOSIZE | SWP_NOZORDER | SWP_SHOWWINDOW);
            }
            break;
#endif /* JETTEST == XA_ENABLED */

        case ID_EXIT:
        case WM_DESTROY:
            if (hWnd == AppWnd) {
                /* Exit the application */
#if BT_STACK == XA_ENABLED
                if (BMG_Shutdown() != BT_STATUS_PENDING)
#endif
                    PostQuitMessage(0);
                return TRUE;
            }
            break;

#if BT_STACK == XA_ENABLED
        default:
            BMG_TrackMainMenu(hWnd, wParam, lParam);
            break;
#endif
        }
        break;
    }

    return 0L;
}

/*
 * Pops up a dialog to query the user for the name of the object to GET.
 */
static BOOL CALLBACK GetDlgProc( HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam )
{
    char        buffer[256], *b;
    static BOOL defaultVCard;
    U32         btnState;

    switch (uMsg) {

    case WM_INITDIALOG:
        strcpy(buffer, "Destination Folder: ");
        b = buffer + strlen(buffer);
        _getcwd(b, 200);
        SetWindowText(hDlg, buffer);
        strcat(b, "\\*.*");
        SendDlgItemMessage(hDlg, IDC_GET_OBJECT, CB_DIR, DDL_READWRITE, (LPARAM)b);
        if (objectName[0]) {
            SendDlgItemMessage(hDlg, IDC_GET_OBJECT, CB_ADDSTRING, 0, (LPARAM)objectName);
            SendDlgItemMessage(hDlg, IDC_GET_OBJECT, WM_SETTEXT, 0, (LPARAM)objectName);
        }
        defaultVCard = FALSE;
        return TRUE;

    case WM_COMMAND:
        switch (LOWORD(wParam)) {

        case IDC_DEFAULT_VCARD:
            btnState = SendDlgItemMessage(hDlg, IDC_DEFAULT_VCARD, BM_GETSTATE, 0L, 0L);
            if (btnState & BST_CHECKED)
                defaultVCard = TRUE;
            else defaultVCard = FALSE;
            return TRUE;

        case IDOK:
            objectName[0] = 0;
            if (defaultVCard) {
                /* Get the default vCard object, ignoring any object name provided */
                Qb_Get(AppQbInstance, "");
            } else if (SendDlgItemMessage(hDlg, IDC_GET_OBJECT, WM_GETTEXT, OBS_MAX_NAME_LEN, 
                       (LPARAM)objectName) > 0) {
                /* Set to the path name entered, possibly including the backup and noCreate flags */
                Qb_Get(AppQbInstance, objectName);
            }
            
            EndDialog(hDlg, TRUE);
            return TRUE;

        case IDCANCEL:
            EndDialog(hDlg, TRUE);
            return TRUE;
        }
    }
    return 0L;
}

/*
 * Pops up a dialog to query the user for the name of the path to set
 */
static BOOL CALLBACK SetPathDlgProc( HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam )
{
    static BOOL reset;
    static BOOL backup;
    static BOOL noCreate;
    U32         btnState;

    switch (uMsg) {

    case WM_INITDIALOG:
        backup = FALSE;
        noCreate = FALSE;
        reset = FALSE;
        return TRUE;

    case WM_COMMAND:
        switch (LOWORD(wParam)) {
        case IDC_BACKUP:
            btnState = SendDlgItemMessage(hDlg, IDC_BACKUP, BM_GETSTATE, 0L, 0L);
            if (btnState & BST_CHECKED)
                backup = TRUE;
            else backup = FALSE;
            return TRUE;

        case IDC_NO_CREATE:
            btnState = SendDlgItemMessage(hDlg, IDC_NO_CREATE, BM_GETSTATE, 0L, 0L);
            if (btnState & BST_CHECKED)
                noCreate = TRUE;
            else noCreate = FALSE;
            return TRUE;

        case IDC_RESET:
            btnState = SendDlgItemMessage(hDlg, IDC_RESET, BM_GETSTATE, 0L, 0L);
            if (btnState & BST_CHECKED)
                reset = TRUE;
            else reset = FALSE;
            return TRUE;

        case IDOK:
            pathName[0] = 0;
            if (reset) {
                /* Reset to the root folder, ignoring the path name, backup flag, and noCreate flag */
                Qb_SetPath(AppQbInstance, 0, FALSE, FALSE);
            } else if (SendDlgItemMessage(hDlg, IDC_SET_PATH, WM_GETTEXT, 64, (LPARAM)pathName) > 0) {
                /* Set to the path name entered, possibly including the backup and noCreate flags */
                Qb_SetPath(AppQbInstance, pathName, backup, noCreate);
            } else if (backup || noCreate) {
                /* Backup or noCreate flags might be set */
                Qb_SetPath(AppQbInstance, 0, backup, noCreate);
            }

            EndDialog(hDlg, TRUE);
            return TRUE;

        case IDCANCEL:
            EndDialog(hDlg, TRUE);
            return TRUE;
        }
    }
    return 0L;
}

#if OBEX_AUTHENTICATION == XA_ENABLED
static BOOL CALLBACK PasswordDlgProc( HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam )
{
    static char      *password;

    switch (uMsg) {

    case WM_INITDIALOG:
        password = (char *)lParam;

        /* Load the default/current password */
        SendDlgItemMessage(hDlg, IPP_PASSWORD, WM_SETTEXT, 0, (LPARAM)lParam);
        return TRUE;

    case WM_COMMAND:
        switch (LOWORD(wParam)) {

        case IDOK:
            SendDlgItemMessage(hDlg, IPP_PASSWORD, WM_GETTEXT, 11, (LPARAM)password);
            EndDialog(hDlg, TRUE);
            return TRUE;

        case IDCANCEL:
            EndDialog(hDlg, FALSE);
            return TRUE;
        }
    }
    return 0L;
}

static BOOL CALLBACK UserIdDlgProc( HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam )
{
    static char      *userId;

    switch (uMsg) {

    case WM_INITDIALOG:
        userId = (char *)lParam;

        /* Load the default/current password */
        SendDlgItemMessage(hDlg, IPP_USER_ID, WM_SETTEXT, 0, (LPARAM)lParam);
        return TRUE;

    case WM_COMMAND:
        switch (LOWORD(wParam)) {

        case IDOK:
            SendDlgItemMessage(hDlg, IPP_USER_ID, WM_GETTEXT, 20, (LPARAM)userId);
            EndDialog(hDlg, TRUE);
            return TRUE;

        case IDCANCEL:
            EndDialog(hDlg, FALSE);
            return TRUE;
        }
    }
    return 0L;
}
#endif /* OBEX_AUTHENTICATION == XA_ENABLED */

/*
 * Pops up a dialog to query the user for the address of the device to
 * connect to. Supports Bluetooth, TCP and IrDA addresses. Completion of
 * this request is signalled to AppWnd asynchronously.
 */
static void AcquireTargetAddr(WPARAM CompletionEvent)
{
    static ObexTpAddr   target = {0};
    static U32          ipAddr = 0x7f000001;
#if BT_STACK == XA_ENABLED
    static QbDevQuery   qbQuery;
    BtStatus            status;
#endif

    if ((OBEX_EnumTransports() != OBEX_TP_IRDA) && 
        (OBEX_EnumTransports() != OBEX_TP_BLUETOOTH)) {

        /* Popup Target device picker that supports all transports. */
#if TCP_STACK == XA_ENABLED
        target.proto.tcp.addr = ipAddr;
#endif
        if (DialogBoxParam(hInst, MAKEINTRESOURCE(QUERY_TARGET), AppWnd, 
                           GetTpAddrDlgProc, (LPARAM)&target) == TRUE) {

#if TCP_STACK == XA_ENABLED
            if (target.type == OBEX_TP_TCP)
                ipAddr = ntohl(target.proto.tcp.addr); /* Save the address */
#endif
            PostMessage(AppWnd, WM_COMMAND, CompletionEvent, (LPARAM)&target);
            return;
        }

        PostMessage(AppWnd, WM_COMMAND, CompletionEvent, 0);
        return;
    }

#if BT_STACK == XA_ENABLED
    OS_MemSet((U8 *)&qbQuery, 0, sizeof(qbQuery));

    /* If its just the BT Stack then use the Blue Manager dialog.
     * We're searching for a device with the Object Transfer service.
     */
    qbQuery.context = CompletionEvent;
    qbQuery.sd.callback = BtEventHandler;
    qbQuery.sd.quality.mask = (BDQM_SERVICE_CLASS|BDQM_DEVICE_STATUS|BDQM_SERVICE_UUID);
    qbQuery.sd.quality.serviceClass = COD_OBJECT_TRANSFER;
    qbQuery.sd.quality.serviceUuid = SC_OBEX_OBJECT_PUSH;
    qbQuery.sd.quality.status = (BDS_IN_RANGE);

    status = DS_SelectDevice(&(qbQuery.sd));
    App_Report("Client: DS_SelectDevice() returned %s.", pBT_Status(status));
    if (status != BT_STATUS_PENDING) {
        if (status == BT_STATUS_SUCCESS) {
            qbQuery.target.type = OBEX_TP_BLUETOOTH;
            qbQuery.target.proto.bt.psi = qbQuery.sd.result->psi;
            OS_MemCopy(qbQuery.target.proto.bt.addr.addr, qbQuery.sd.result->addr.addr, BD_ADDR_SIZE);

            /* Complete the Acquire Target Request (Success) */
            PostMessage(AppWnd, WM_COMMAND, CompletionEvent, (LPARAM)&qbQuery.target);
        } else {
            PostMessage(AppWnd, WM_COMMAND, CompletionEvent, 0);
        }
    }
    return;
#endif

#if IRDA_STACK == XA_ENABLED
    target.type = OBEX_TP_IRDA;
    
    PostMessage(AppWnd, WM_COMMAND, CompletionEvent, (LPARAM)&target);
    return;
#endif

    PostMessage(AppWnd, WM_COMMAND, CompletionEvent, 0);
}

char* App_GetSavedObject(void)
{
    OPENFILENAME  openFile;
    char          filter[50];

    memset(filter, 0, sizeof(filter));
    strcpy(filter, "All Files (*.*)");
    strcpy(filter+16, "*.*");

    /* Post dialog to get name of saved object */
    memset( &openFile, 0, sizeof(openFile));
    openFile.lStructSize = sizeof(openFile);
    openFile.hwndOwner = AppWnd;
    openFile.lpstrFilter = filter;
    openFile.lpstrFileTitle = objectName;
    openFile.nMaxFileTitle = OBS_MAX_NAME_LEN;
    openFile.lpstrTitle = TEXT("Save Default Object As");
    openFile.Flags = OFN_PATHMUSTEXIST;
    openFile.lpstrInitialDir = "\\";

    if (GetSaveFileName(&openFile) == 0)
        return 0;
    else return objectName;   
}

#if BT_STACK == XA_ENABLED
static void BtEventHandler(const BtEvent *Event)
{
    QbDevQuery  *qbQuery;

    if (Event->eType == BTEVENT_DEVICE_SELECTED) {
        qbQuery = (QbDevQuery *)Event->p.select;

        if (Event->errCode == BEC_NO_ERROR) {

            qbQuery->target.type = OBEX_TP_BLUETOOTH;
            qbQuery->target.proto.bt.psi = qbQuery->sd.result->psi;
            OS_MemCopy(qbQuery->target.proto.bt.addr.addr, qbQuery->sd.result->addr.addr, BD_ADDR_SIZE);

            /* Complete the Acquire Target Request (Success) */
            PostMessage(AppWnd, WM_COMMAND, (WPARAM)qbQuery->context, (LPARAM)&qbQuery->target);

        } else {
            /* Complete the Acquire Target Request (Failed) */
            PostMessage(AppWnd, WM_COMMAND, (WPARAM)qbQuery->context, 0);
        }
    }
}
#endif /* BT_STACK == XA_ENABLED */

/*
 * Prints messages to the UI list box 
 */
void App_Report(char *format,...)
{
    char     buffer[200]; /* Output Buffer */
    int      index;
    va_list  args;

    va_start(args, format);
    index = _vsnprintf(buffer, 200, format, args);
    va_end(args);

    /* Direct output to Client or Server list boxes */
    if (StrnCmp("Client", 6, buffer, 6) == TRUE) {
        index = SendDlgItemMessage(AppWnd, OBEX_CLIENT_OUTPUT, LB_ADDSTRING, 0, (LPARAM)buffer);
        if (index != LB_ERR) {
            SendDlgItemMessage(AppWnd, OBEX_CLIENT_OUTPUT, LB_SETCARETINDEX, (WPARAM)index, FALSE);
        }
    } else {
        index = SendDlgItemMessage(AppWnd, OBEX_SERVER_OUTPUT, LB_ADDSTRING, 0, (LPARAM)buffer);
        if (index != LB_ERR) {
            SendDlgItemMessage(AppWnd, OBEX_SERVER_OUTPUT, LB_SETCARETINDEX, (WPARAM)index, FALSE);
        }
    }
}

/*
 * Maintains progress dialogs
 */
void App_Progress(U32 barId, U32 currPos, U32 maxPos)
{
    if (currPos == 0) {
        PostMessage(GetDlgItem(AppWnd, barId), PBM_SETRANGE32, 0, maxPos);
    }

    PostMessage(GetDlgItem(AppWnd, barId), PBM_SETPOS, currPos, 0);
}

#if OBEX_SESSION_SUPPORT == XA_ENABLED
/*
 * Pops up a dialog to query the user for the session timeout to use.
 */
static BOOL CALLBACK SetSessionTimeoutDlgProc( HWND hDlg, UINT uMsg, 
                                               WPARAM wParam, LPARAM lParam )
{
    U8          time[8];
    U32         timeout;
    char        *stopString;
    static U8   role;

    switch (uMsg) {
    case WM_INITDIALOG:
        role = (U8)lParam;
        /* Load the default session timeout */
        SendDlgItemMessage(hDlg, IPP_SET_TIMEOUT, WM_SETTEXT, 0, (LPARAM)"0");
        return TRUE;

    case WM_COMMAND:
        switch (LOWORD(wParam)) {
        case IDOK:
            time[0] = 0;
            SendDlgItemMessage(hDlg, IPP_SET_TIMEOUT, WM_GETTEXT, 8, (LPARAM)time);
            timeout = strtoul(time, &stopString, 10);
            Qb_SetSessionTimeout(AppQbInstance, timeout, role);
            EndDialog(hDlg, TRUE);
            return TRUE;

        case IDCANCEL:
            EndDialog(hDlg, TRUE);
            return TRUE;
        }
    }
    return 0L;
}
#endif /* OBEX_SESSION_SUPPORT == XA_ENABLED */

/*
 * Window procedure for processing Get TP Addr dialog.
 */
static BOOL CALLBACK GetTpAddrDlgProc(HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    static ObexTpAddr  *tpa;       /* Output TpAddr ptr */
    I32                 index = 0;
#if BT_STACK == XA_ENABLED
    BtDeviceContext    *bdc;
#endif
#if TCP_STACK == XA_ENABLED
    char                port[6];
    U16                 tcpPort;
    char               *stopString;
#endif

    switch (uMsg) {
    case WM_INITDIALOG:
        /* Save pointer to output address location. */
        tpa = (ObexTpAddr *)lParam;

        /* Load the known targets into the picker. On input, the lParam
         * contains the IP address to initialize the IP address with.
         */
#if TCP_STACK == XA_ENABLED
        SendDlgItemMessage(hDlg, IPA_IPADDRESS, IPM_SETADDRESS, 0, 
                           (LPARAM)(U32 *)tpa->proto.tcp.addr);
        SendDlgItemMessage(hDlg, IPA_TCP_PORT, WM_SETTEXT, 0, (LPARAM)"650");
#else
        EnableWindow(GetDlgItem(hDlg, IPA_PICK_IPADDR), FALSE);
        EnableWindow(GetDlgItem(hDlg, IPA_IPADDRESS), FALSE);
        EnableWindow(GetDlgItem(hDlg, IPA_PICK_TCPPORT), FALSE);
        EnableWindow(GetDlgItem(hDlg, IPA_TCP_PORT), FALSE);
#endif

#if BT_STACK == XA_ENABLED
        /* Propagate BD Addr list with devices from the Blue Manager dialog. */
        for (bdc = 0; DS_EnumDeviceList(&bdc) == BT_STATUS_SUCCESS; index++) {
            SendDlgItemMessage(hDlg, IPA_BDADDR, LB_ADDSTRING, 0, (LPARAM)((BtDeviceExt *)bdc->context)->name);
            SendDlgItemMessage(hDlg, IPA_BDADDR, LB_SETITEMDATA, index, (LPARAM)bdc);
        }
#else
        EnableWindow(GetDlgItem(hDlg, IPA_PICK_BDADDR), FALSE);
#endif

#if IRDA_STACK == XA_DISABLED
        EnableWindow(GetDlgItem(hDlg, IPA_PICK_IRDA), FALSE);
#endif
        /* Now select the default address type on the GUI */
        if (index > 0) {
            CheckRadioButton(hDlg, IPA_PICK_BDADDR, IPA_PICK_IRDA, IPA_PICK_BDADDR);
            SendDlgItemMessage(hDlg, IPA_BDADDR, LB_SETCURSEL, index-1, (LPARAM)0);
        } else {
            if (OBEX_EnumTransports() & OBEX_TP_TCP)
                CheckRadioButton(hDlg, IPA_PICK_BDADDR, IPA_PICK_IRDA, IPA_PICK_IPADDR);
            else CheckRadioButton(hDlg, IPA_PICK_BDADDR, IPA_PICK_IRDA, IPA_PICK_IRDA);
        }
        break;

    case WM_COMMAND:
        switch (LOWORD(wParam)) {
        case IPA_OK:
#if BT_STACK == XA_ENABLED
            if (IsDlgButtonChecked(hDlg, IPA_PICK_BDADDR) == BST_CHECKED) {
                /* Bluetooth address was selected, check for an address
                 * in the listbox.
                 */
                index = SendDlgItemMessage(hDlg, IPA_BDADDR, LB_GETCURSEL, 0L, 0L);
                if (index != LB_ERR) {
                    /* Found an address, return it. */
                    bdc = (BtDeviceContext *)SendDlgItemMessage(hDlg, IPA_BDADDR, LB_GETITEMDATA, index, 0);
                    Assert(bdc);
                    /* Setup the Bluetooth target */
                    tpa->type = OBEX_TP_BLUETOOTH;
                    tpa->proto.bt.addr = bdc->addr;
                    tpa->proto.bt.psi = bdc->psi;
                    EndDialog(hDlg, TRUE);
                    return TRUE;
                }
            }
#endif
#if TCP_STACK == XA_ENABLED
            if (IsDlgButtonChecked(hDlg, IPA_PICK_IPADDR) == BST_CHECKED) {
                if (SendDlgItemMessage(hDlg, IPA_IPADDRESS, IPM_GETADDRESS, 0, 
                        (LPARAM)&tpa->proto.tcp.addr) == 4) {
                    tpa->type = OBEX_TP_TCP;
                    tpa->proto.tcp.addr = htonl(tpa->proto.tcp.addr);
                    port[0] = 0;
                    SendDlgItemMessage(hDlg, IPA_TCP_PORT, WM_GETTEXT, 6, (LPARAM)port);
                    tcpPort = (U16)strtoul(port, &stopString, 10);
                    if (tcpPort > 0 && tcpPort <= 65535) {
                        tpa->proto.tcp.customPort = tcpPort;
        
                        EndDialog(hDlg, TRUE);
                        return TRUE;
                    }
                }
            }
#endif
#if IRDA_STACK == XA_ENABLED
            if (IsDlgButtonChecked(hDlg, IPA_PICK_IRDA) == BST_CHECKED) {
                tpa->type = OBEX_TP_IRDA;
                tpa->proto.ir.addr = 0;

                EndDialog(hDlg, TRUE);
                return TRUE;
            }
#endif
            /* The user didn't enter a complete address. */
            tpa->type = OBEX_TP_NONE;
            /* Drop through to Cancel case */

        case WM_DESTROY:
        case IPA_CANCEL:
            EndDialog(hDlg, FALSE);
            return TRUE;
        }
        break;
    }
    return FALSE;
}


/*---------------------------------------------------------------------------
 *            GetUIContext
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Returns the UI context handle for the specified client or
 *            server instance. The UI context is used to control the progress
 *            meter as an object is transferred.
 *
 * Return:    Progress bar resource Id.
 *
 */
U32 GetUIContext(U8 QbInst, U8 Role)
{
    if (Role == QB_CLIENT) {
        if (QbInst == 0)
            return IDC_CLIENT_PROGRESS;
        return IDC_CLIENT_PROGRESS2;
    }
    
    if (QbInst == 0)
        return IDC_SERVER_PROGRESS;
    return IDC_SERVER_PROGRESS2;
}

#if BT_STACK == XA_ENABLED
static LPTSTR QbGetToolTipText(int ControlId)
{
    switch (ControlId) {
    case ID_CONNECT: return "Send an OBEX Connect";
    case ID_DISCONNECT: return "Send an OBEX Disconnect";
    case ID_PUT: return "Send an OBEX Put";
    case ID_GET: return "Send an OBEX Get";
    case ID_CLIENT_CON_REQ: return "Establish an OBEX transport connection";
    case ID_CLIENT_DISC_REQ: return "Terminate the OBEX transport connection";
    case ID_CLIENT_ABORT: return "Abort the current client operation";
    case ID_SERVER_ABORT: return "Abort the current server operation";
    case ID_PUSH: return "Perform an Object Push sequence";
    case ID_EXCHANGE: return "Perform a vCard Exchange sequence";
    case ID_SETPATH: return "Send an OBEX SetPath";
    case IDC_DEFAULT_VCARD: return "Use the default vCard";
    case ID_CREATE_SESSION: return "Create a reliable OBEX Session";
    case ID_CLOSE_SESSION: return "Close a reliable OBEX Session";
    case ID_SUSPEND_SESSION: return "Suspend a reliable OBEX Session";
    case ID_RESUME_SESSION: return "Resume a reliable OBEX Session";
    case ID_SET_CLIENT_SESSION_TIMEOUT: return "Set the client session timeout";
    case ID_SET_SERVER_SESSION_TIMEOUT: return "Set the client session timeout";
    case ID_CLIENT2_CHECKBOX: return (AppQbInstance ? "Switch to application instance #1" : "Switch to applicaion instance #2");
    }
    return "";
}
#endif /* BT_STACK == XA_ENABLED */
