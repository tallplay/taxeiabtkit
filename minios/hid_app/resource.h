//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by hid_app.rc
//
#define MAINDIALOG                      101
#define IDC_ENTER_PIN                   104
#define LIST_OUTPUT                     1000
#define EXIT_BUTTON                     1001
#define FAX_QUERY                       1002
#define SendNONE                        1002
#define IDC_PIN_CODE                    1003
#define DISCONNECT                      1003
#define CONNECT                         1004
#define DISCOVER                        1005
#define LAN_QUERY                       1006
#define CONNECT2                        1006
#define HS_QUERY                        1007
#define CONNECT3                        1007
#define INTERCOM_QUERY                  1008
#define CONNECT4                        1008
#define CORDLESS_QUERY                  1009
#define STOPSNIFFER                     1009
#define FILE_TRANSFER_QUERY             1010
#define NON_DISCOVER                    1010
#define SYNC_QUERY                      1011
#define STOPSNIFFER2                    1011
#define STARTSNIFFER                    1011
#define HS_GW_QUERY                     1012
#define SENDA                           1012
#define SYNC_CMD_QUERY                  1013
#define DUN_QUERY                       1025
#define UUID_16                         1026
#define UUID_32                         1027
#define UUID_128                        1028
#define ACL_VIEW                        1029
#define IDACCEPT                        1030
#define IDREJECT                        1031
#define ACL_CONNECT                     1032
#define ACL_DISCONNECT                  1033
#define PIN_REQ                         1034
#define IDC_DEVICELIST                  1034
#define PIN_CANCEL                      1035

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        105
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1035
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
