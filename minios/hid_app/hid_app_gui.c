
#include "windows.h"
#include "commctrl.h"
#include "stdio.h"
#include "resource.h"
#include "btalloc.h"
#include "sys/debug.h"
#include "hid.h"
#include "ddan.h"
#include "osapi.h"
#include "avrcp.h"
#include "hid_app.h"
#include "parseopts.h"
#if XA_MULTITASKING == XA_ENABLED
#error "MULTITASKING must set DISABLE"
#endif

/****************************************************************************
 *
 * Constants
 *
 ****************************************************************************/
/* External Functions */
extern void App_Report(char *format,...);

/* Internal Functions */
HWND *APP_Init(HINSTANCE Inst, HINSTANCE PrevInst, LPTSTR CmdLine, int CmdShow);
void APP_Deinit(void);
void APP_Thread(void);
static BOOL CALLBACK AppWndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
static void HidSecurityAndDeviceHandler(const BtEvent *Event);
static BOOL CALLBACK EnterPinProc(HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam);
static void RadioStatusChange(BtEventType Status);

typedef struct _PinCodeRequest {
    U8          Pin[16];
    U8          PinLen;
    U8          Flags;
    const char *DeviceDesc;
} PinCodeReq;

typedef struct _BtDeviceExt {
    BOOL inUse;
    BtAddrNamePair  banp;
}BtDeviceExt;

#define MAX_DEVICE 20
#define MAX_DEVICE_NAME_SIZE 34
BtDeviceExt btdevext[MAX_DEVICE];

/****************************************************************************
 *
 * Globals
 *
 ****************************************************************************/

/* Handle of the Applicaiton main window. */

/* Internal Variables */
static HWND   AppWnd;
static HWND   deviceWnd;
BOOL isquit = FALSE;


/****************************************************************************
 *
 * Prototypes
 *
 ****************************************************************************/

/*---------------------------------------------------------------------------
 *
 * Function prototypes
 */
/*
 * Application functions 
 */
HWND *APP_Init(HINSTANCE Inst, HINSTANCE PrevInst, LPTSTR CmdLine, int CmdShow);
void  APP_Deinit(void);
void  APP_Thread(void);
void  MINIOS_RemoveThread(PFV thread);
BOOL  MINIOS_AddThread(PFV thread);
void  MINIOS_Run(void);

void PumpMessages(void);

static int  DWND_AddDevice(BtDeviceContext *Bdc, BtAddrNamePair *Template);
static void DWND_RemoveDevice(BtDeviceContext *Device);
static BtDeviceContext *DWND_GetDeviceContextByIndex(U32 Index);
static BtDeviceContext *DWND_GetSelectedDeviceContext();
static void DWND_OnNotify(NMHDR *Nmh);


/*
 * Retrieve the BtDeviceContext of the specified device index
 */
static BtDeviceContext *DWND_GetDeviceContextByIndex(U32 Index)
{
    LVITEM  lvItem = {0};

    lvItem.mask = LVIF_PARAM;
    lvItem.iItem = Index;

    if (ListView_GetItem(deviceWnd, &lvItem) != -1) {
        return(BtDeviceContext *)lvItem.lParam;
    }
    return 0;
}

/*
 * Retrieve the BtDeviceContext of the currently selected device
 */
static BtDeviceContext *DWND_GetSelectedDeviceContext()
{
    int     i;

    /* Walk the list of devices, looking for the selected one */
    for (i = 0; i < ListView_GetItemCount(deviceWnd); i++) {
        if (ListView_GetItemState(deviceWnd, i, LVIS_SELECTED)) {
            return DWND_GetDeviceContextByIndex(i);
        }
    }
    return 0;
}

/*
 * Add a new device to the device list
 */
static int DWND_AddDevice(BtDeviceContext *Bdc, BtAddrNamePair *Template)
{
    LVITEM           lvItem = {0};
    BtDeviceContext *bdc;
    int              i = 0;

    /* Walk the list of devices, looking for a match */
    while ((bdc = DWND_GetDeviceContextByIndex(i))) {
        if (OS_MemCmp(bdc->addr.addr, 6, Bdc->addr.addr, 6) == TRUE) {
            /* Device is already on list, return. */
            return i;
        }
        i++;
    }

    for (i = 0; i < MAX_DEVICE; i++) {
        if (btdevext[i].inUse == FALSE) {
            break;
        }
    }
    Assert(i < MAX_DEVICE);

    Bdc->context = btdevext + i;

    if (Template == 0) {
        OS_MemSet((U8 *)(btdevext+i), 0, sizeof(BtDeviceExt));
        bdaddr_ntoa(&Bdc->addr, btdevext[i].banp.name);
    } else btdevext[i].banp = *Template;

    btdevext[i].inUse = TRUE;

    App_Report(("DWND_AddDevice() %s.\n", btdevext[i].banp.name));

    /* Now add it to device view */
    lvItem.mask = LVIF_TEXT|LVIF_IMAGE|LVIF_PARAM;
    lvItem.iItem = ListView_GetItemCount(deviceWnd);
    lvItem.lParam = (LPARAM)Bdc;
    lvItem.iImage = I_IMAGECALLBACK;
    lvItem.pszText = btdevext[i].banp.name;   /* sort cannot use callback */

    if ((i = ListView_InsertItem(deviceWnd, &lvItem)) == -1)
        Assert(0);
    return i;
}


/*
 * Remove a device from the list view.
 */
static void DWND_RemoveDevice(BtDeviceContext *Device)
{
    BtDeviceContext *bdc;
    I8               i;

    /* Find the index of the device */
    for (i = 0; bdc = DWND_GetDeviceContextByIndex(i); i++) {
        if (bdc == Device) {
            App_Report("DWND_RemoveDevice() %s.\n", ((BtDeviceExt *)Device->context)->banp.name);
            ListView_DeleteItem(deviceWnd, i);
            ((BtDeviceExt *)Device->context)->inUse = FALSE;
            Device->context = 0;
            break;
        }
    }
}

/*
 * Initialize the device list view columns 
 */
static void DWND_Init(void)
{
    LVCOLUMN        lvColumn;
    int             width;
    RECT            rect;

    GetWindowRect(deviceWnd, &rect);
    width = ((rect.right - rect.left) - (GetSystemMetrics(SM_CXBORDER)*2)) / 5;

    /* Initialize the columns */
    lvColumn.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT;
    lvColumn.fmt = LVCFMT_LEFT;

    /* Deduct the border & scroll width from the "Address" column since it's the smallest. */
    lvColumn.cx = (width * 2) - (GetSystemMetrics(SM_CXBORDER)*2) - GetSystemMetrics(SM_CXVSCROLL);
    lvColumn.pszText = "Name";
    ListView_InsertColumn(deviceWnd, 0, &lvColumn);

    lvColumn.cx = width * 3;
    lvColumn.pszText = "Status";
    ListView_InsertColumn(deviceWnd, 1, &lvColumn);
}


/*
 * Process WM_NOTIFY messages for device list
 */
static void DWND_OnNotify(NMHDR *Nmh)
{
    NMLVDISPINFO       *lpdi;
    NMCLICK            *clk;
    BtDeviceContext    *bdc;
    BtDeviceExt        *ext;
    BtRemDevState   constat;
    BtDeviceStatus  secstat;


    /* DO NOT CALL Stack functions from this function! */
    switch (Nmh->code) {
    case LVN_GETDISPINFO:
        lpdi = (NMLVDISPINFO*)Nmh;
        bdc = (BtDeviceContext *)lpdi->item.lParam;
        ext = (BtDeviceExt *)bdc->context;

        /* What is the sub-item (column) of the information being requested? */
        switch (lpdi->item.iSubItem) {
        case 0:         /* Col 0: Device Name / Address */
            if (lpdi->item.mask & LVIF_TEXT) {
                Assert(lpdi->item.cchTextMax > (int)strlen(ext->banp.name));
                strcpy(lpdi->item.pszText, ext->banp.name);
            }
            break;

        case 1:         /* Col 1: Device Status */
            if (lpdi->item.mask & LVIF_TEXT) {
                Assert(lpdi->item.cchTextMax > 24);

                constat = HIDAPP_GetRemDevConState(bdc);
                    if (constat == BDS_CONNECTED) 
                    {
                         strcat(lpdi->item.pszText, "Connected");
                        
                    } 
                    else if (constat == BDS_OUT_CON || constat == BDS_IN_CON)
                    {
                        strcpy(lpdi->item.pszText, "Connecting");
                    }
                    else if (constat == BDS_DISCONNECTED)
                         strcat(lpdi->item.pszText, "Disconnected");
                    
                secstat = HIDAPP_GetRemDevSecState(bdc);

                if (secstat & BDS_PAIRED) {
                    if (strlen(lpdi->item.pszText))
                        strcat(lpdi->item.pszText, ", ");

                    strcat(lpdi->item.pszText, "Paired");

                    if (secstat & BDS_TRUSTED)
                        strcat(lpdi->item.pszText, ", Trusted");
                }
            }
            break;
        }
    case NM_CLICK:
        /* If you select a device we have to update the buttons */
        clk = (NMCLICK *)Nmh;
        break;

#if 0
    case NM_RCLICK:
        /* If you right click on a device we pop up a menu */
        clk = (NMCLICK *)Nmh;
        BMG(menuDevice) = GetDeviceContextByIndex(clk->dwItemSpec);
        break;
    case NM_DBLCLK:
        /* If you double click on a device we treat it like selecting a
         * device and clicking "Ok".
         */
        if (BMG(activeQuery))
            SendMessage(BMG(chooserWnd), WM_COMMAND, IDC_OK, 0);
        break;

    case TTN_GETDISPINFO:
        OnToolTip(Nmh);
        break;
#endif
    }
}

/*****************************************************************************
Govert!!! Must implement begin
*****************************************************************************/

BOOL MouseTest = FALSE;
U8 X;
U8 Y;

HidChannel hid_channel;
U8 mdata[6];

U8 kidx = 0;
U8 kchar = 4;
U8 kup = 0;
U8 kdata[10][10];

#define OFF 2
void MouseMove()
{
    mdata[0] = 0xA1;
    mdata[1] = 0x02;
    mdata[2] = 0x00;
    mdata[5] = 0;//X

    if(X == 0 && (Y != 0))
    {
        mdata[3] = 0;//X
        mdata[4] = -OFF;//Y-1
        Y-=OFF;
        if(HID_IntSendData(&hid_channel, 6, mdata) != BT_STATUS_PENDING)
        {
            Y += OFF;
            App_Report("BUSY");
        }
    }
    if(Y == 0xFF-OFF+1)
    {
        mdata[3] = -OFF;//X-1
        mdata[4] = 0;//Y
        X-=OFF;
        if(HID_IntSendData(&hid_channel, 6, mdata) != BT_STATUS_PENDING)
        {
            X+= OFF;
            App_Report("BUSY");
        }
    }
    else if(X == 0xFF-OFF+1)
    {
        mdata[3] = 0;//X
        mdata[4] = OFF;//Y+1
        Y+=OFF;
        if(HID_IntSendData(&hid_channel, 6, mdata) != BT_STATUS_PENDING)
        {
            Y-=OFF;
            App_Report("BUSY");
        }
    }
    else if(Y == 0)
    {
        mdata[3] = OFF;//X+1
        mdata[4] = 0;//Y
        X+=OFF;
        if(HID_IntSendData(&hid_channel, 6, mdata) != BT_STATUS_PENDING)
        {
            X-=OFF;
            App_Report("BUSY");
        }
    }

}

void KeyboardHit()
{
    kdata[kidx][0] = 0xA1;
    kdata[kidx][1] = 0x01;
    kdata[kidx][2] = 0x00;
    kdata[kidx][3] = 0x00;
    kdata[kidx][5] = kdata[kidx][6] = kdata[kidx][7] = kdata[kidx][8] = kdata[kidx][9] = 0;

    if(kup)
    {
        kdata[kidx][4] = kchar++;
        if(kchar > 4+25)
            kchar = 4;
    }
    else
        kdata[kidx][4] = 0;
    kup = !kup;
    kidx++;
    if(kidx > 10)
        kidx = 0;

    HID_IntSendData(&hid_channel, 10, &kdata[kidx]);

}

#ifdef TEST_AVRCP
AvrcpChannel avctp_channel;
U8 avctpdata[50];
AvctpRspFrame avctprspfme;
void AvrcpCB(AvrcpChannel *chnl, AvrcpCallbackParms *Parms)
{
    switch(Parms->event)
    {
        case AVRCP_EVENT_CONNECT_IND:
            AVRCP_ConnectRsp(&avctp_channel, TRUE); //always receieve
            break;
        case AVRCP_EVENT_CONNECT:
            App_Report("AVRCP connected");
            break;
        case AVRCP_EVENT_COMMAND:
        {
            U8 buf[200];
			int i;
            App_Report("AVRCP Command Received:");
            switch(Parms->p.cmdFrame->ctype)
            {
                case AVCTP_CTYPE_CONTROL:
                    sprintf(buf, "ctype = CONTROL");
                    break;
                case AVCTP_CTYPE_STATUS:
                    sprintf(buf, "ctype = STATUS");
                    break;
                case AVCTP_CTYPE_NOTIFY:
                    sprintf(buf, "ctype = NOTIFY");
                    break;
                default:
                    sprintf(buf, "ctype = %d", Parms->p.cmdFrame->ctype);
                    break;
            }

            sprintf(buf, "%s op = %02X header = ", buf , Parms->p.cmdFrame->opcode);

            for(i = 0; i < Parms->p.cmdFrame->headerLen; i++)
                sprintf(buf, "%s %02X", buf, Parms->p.cmdFrame->header[i]);

            sprintf(buf, "%s oprand = ", buf);

            for(i = 0; i < Parms->p.cmdFrame->operandLen; i++)
                sprintf(buf, "%s %02X", buf, Parms->p.cmdFrame->operands[i]);

            App_Report("%s", buf);


            switch(Parms->p.cmdFrame->opcode)
            {
                case AVCTP_OPCODE_VENDOR_DEPENDENT:
                    switch(Parms->p.cmdFrame->operands[3])
                    {
    					case 0x10: //get capability
    					avctprspfme = *(AvctpRspFrame*)Parms->p.cmdFrame;
                        avctpdata[0] = 0x00; avctpdata[1] = 0x19; avctpdata[2] = 0x58; 
                        avctpdata[3] = 0x10; avctpdata[4] = 0x00;

                        if(Parms->p.cmdFrame->operands[7] == 0x03)//eventID
                        {
                            avctprspfme.response = AVCTP_RESPONSE_IMPLEMENTED_STABLE;
                            avctpdata[5] = 0x00; avctpdata[6] = 0x02;//len = 2;
                            avctpdata[7] = 0x03; avctpdata[8] = 0;
                            avctprspfme.operandLen = 9;
                        }
                        else
                        {
                            avctprspfme.response = AVCTP_RESPONSE_NOT_IMPLEMENTED;
                            avctpdata[5] = 0x00; avctpdata[6] = 0x01;//len = 1;
                            avctpdata[7] = 0x02; //Specified parameter not found
                            avctprspfme.operandLen = 8;
                        }
                        avctprspfme.operands = avctpdata;
                        
                        AVRCP_SendResponse(chnl, &avctprspfme);                        
                        break;
                    }
                    break;
                case AVCTP_OPCODE_PASS_THROUGH:
                    break;
                default:
                    Assert(0);
                    break;
            }
            

            
            
            break;
        }
        case AVRCP_EVENT_DISCONNECT:
            App_Report("AVRCP disconnected");
            break;
		default:
            App_Report("AVRCP unknow event = %x", Parms->event);
            break;
    }
}
#endif


void HidCB (HidChannel *chnl, HidCallbackParms *Parms)
{
    int i, j;
    BtDeviceContext *bdc;
    switch(Parms->event)
    {
        case HID_EVENT_CONNECT:
             HIDAPP_SetDiscoverable(FALSE);
             App_Report(("HidCB: connected "));
            break;
        case HID_EVENT_DISCONNECT:
             App_Report(("HidCB: disconnected "));
            break;
        case HID_EVENT_INT_DATA_SENT:
           // App_Report (("HidCB: data sent "));
            return;
        default:
            App_Report(("HidCB: unknow event"));
            return;
                
    }

    InvalidateRect(deviceWnd, 0, TRUE);

    /* Retrieve index of this device for ListView function call. */
    for (i = 0; DWND_GetDeviceContextByIndex(i); i++) 
    {
        bdc = DWND_GetDeviceContextByIndex(i);

        for(j = 0; j < BD_ADDR_SIZE; j++)
        {
            if (bdc->addr.addr[j] != Parms->p.remDev->bdAddr.addr[j] ) {
                break;
            }
        }

        if(j == BD_ADDR_SIZE)
        {
            ListView_Update(deviceWnd, i);
        }
    }
    
}

static void NameResultHandler(const BtEvent *Event)
{
    BtAddrNamePair banp;
    BtDeviceContext *bdc;
    U8               len;

    Assert(Event && Event->eType == BTEVENT_NAME_RESULT);
//    Assert(Event->p.meToken == &nameTok);

    Event->p.meToken->callback = 0;

    /* Update the BtDeviceContext with the name result. */
    if ((bdc = DS_FindDevice(&Event->p.meToken->p.name.bdAddr))) {
        if ((Event->errCode == BEC_NO_ERROR) && (Event->p.meToken->p.name.io.out.len > 0)) {
            App_Report("Received name %s.\n", Event->p.meToken->p.name.io.out.name);

            /* Save the remote device name. */
            len = min(Event->p.meToken->p.name.io.out.len, MAX_DEVICE_NAME_SIZE-1);
      
            OS_MemCopy(banp.name, Event->p.meToken->p.name.io.out.name, len);            
            banp.name[len] = 0;
            banp.bdAddr = Event->p.meToken->p.name.bdAddr;

            DDAN_AddRecord(&banp);
            
            DWND_RemoveDevice(bdc);
            DWND_AddDevice(bdc, &banp);            
        } else {
            /* Record that the name request failed. Next time we'll skip it. */
            App_Report("Name request failed, status %s.\n", pHC_Status(Event->errCode));
        }
    }
}
static void HidSecurityAndDeviceHandler(const BtEvent *Event)
{
    BtDeviceContext *bdc;
    BtStatus         status;

    switch (Event->eType) {
    case BTEVENT_PIN_REQ:
        AssertEval((bdc = DS_FindDevice(&Event->p.remDev->bdAddr)));
        App_Report(("PIN Request Received from.\n"));
        if (Event->errCode == BT_STATUS_SUCCESS) {
            /* Prompt the user for a PIN code */
            PostMessage(AppWnd, PIN_REQ, 0, (LPARAM)bdc);
        } else {
            /* Cancel the PIN code request */
            EnterPinProc(0, PIN_CANCEL, 0, 0);
        }
        break;

    case BTEVENT_AUTHORIZATION_REQ:
        App_Report("!!!!!!!!BTEVENT_AUTHORIZATION_REQ\n");
        break;

    case BTEVENT_PAIRING_COMPLETE:
        AssertEval((bdc = DS_FindDevice(&Event->p.remDev->bdAddr)));
        App_Report("Pairing complete, status %s.\n", pHC_Status(Event->errCode));
        break;

    case BTEVENT_SECURITY3_COMPLETE:
        if (Event->errCode != 0) {
            /* Only need to report failure, success is reported to the Global Handler. */
            App_Report("Secuirty Mode 3 failed. Status %s\n", pHC_Status(Event->errCode));
        }
        break;

    case BTEVENT_AUTHENTICATE_CNF:
        if (Event->errCode != 0) {
            /* Only need to report failure, success is reported to the Global Handler. */
            App_Report("Authentication attempt failed. Reason %s.", pHC_Status(Event->errCode));
        }
        break;

    case BTEVENT_ENCRYPT_COMPLETE:
        if (Event->errCode != 0) {
            /* Only need to report failure, success is reported to the Global Handler. */
            App_Report("Encryption change attempt failed. Reason %s.", pHC_Status(Event->errCode));
        }
        break;
    case BTEVENT_DEVICE_ADDED:
    {
        BtAddrNamePair banp;
        Event->p.device->psi.psRepMode = 1;
        if(DDAN_FindRecord(&Event->p.device->addr, &banp) == BT_STATUS_FAILED)
        {//name not found in database
        
             DWND_AddDevice(Event->p.device, 0);

            status = HIDAPP_GetRemoteDeviceName(Event->p.device->addr, Event->p.device->psi, NameResultHandler);
            App_Report("GetRemoteDeviceName returned %s.\n", pBT_Status(status));
            Assert((status == BT_STATUS_PENDING) || (status == BT_STATUS_HCI_INIT_ERR));
         }
         else
         {
             DWND_AddDevice(Event->p.device, &banp);
         }
     }
     break;

    case BTEVENT_DEVICE_DELETED:
        App_Report("Device Removed!\n");
        break;

    case BTEVENT_HCI_INIT_ERROR:
    case BTEVENT_HCI_INITIALIZED:
	case BTEVENT_HCI_DEINITIALIZED:
        App_Report("HCI %s.\n", pME_Event(Event->eType));
        RadioStatusChange(Event->eType);
        break;
        
    default:
        App_Report("###################Unknow event received = %d##############\n", Event->eType);
        break;

    }
}

/*****************************************************************************
Govert!!! Must implement end
*****************************************************************************/

static BOOL CALLBACK EnterPinProc(HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
{

    static PinCodeReq  *pinParms = 0;
    char                heading[50];
    static HWND         saveDlg;


    switch (uMsg) {
    
    case WM_INITDIALOG:
        pinParms = (PinCodeReq *)lParam;
        sprintf(heading, "Passkey required for %s", pinParms->DeviceDesc);
        SendMessage(hDlg, WM_SETTEXT, 0, (LPARAM)heading);
        SendMessage(hDlg, EM_SETSEL, 0, (LPARAM)-1);
        SendMessage(hDlg, EM_SETLIMITTEXT, 16/*MAX_PIN_LEN*/, 0L);
        saveDlg = hDlg;
        break;

    case PIN_CANCEL:
        /* Cancel the Dialog */
        EndDialog(saveDlg, FALSE);
        return TRUE;

    case WM_COMMAND:
        switch (LOWORD(wParam)) {
        case IDREJECT:
            EndDialog(hDlg, FALSE);
            return TRUE;

        case IDACCEPT:
            /* Retrieve the Pin Code */
            *(U16 *)pinParms->Pin = 16;       /* Sets the buffer size */
            pinParms->PinLen = (U8)SendDlgItemMessage(hDlg, IDC_PIN_CODE, EM_GETLINE,
                                                      0, (LPARAM)pinParms->Pin);
            if (pinParms->PinLen == 0) {
                /* The user didn't enter a pin. */
                EndDialog(hDlg, FALSE);
                return TRUE;
            }

            /* Trusted */
             pinParms->Flags = BPT_SAVE_TRUSTED;

            EndDialog(hDlg, TRUE);
            return TRUE;

        case EN_MAXTEXT:
            heading[0] = 0;
            break;
        }
        break;
    }
    return FALSE;
}
HINSTANCE ThisInst;

HWND *APP_Init(HINSTANCE Inst, HINSTANCE PrevInst, LPTSTR CmdLine, int CmdShow)
{
    BOOL sts;
	int i;


    //initialize variables
    for(i = 0; i < MAX_DEVICE; i++)
        btdevext[i].inUse = FALSE;
    
    /* Load the common control library into the application */
    InitCommonControls();

    /* Create the Application dialog */ 
    AppWnd = CreateDialog(Inst, MAKEINTRESOURCE(MAINDIALOG), NULL, AppWndProc);
    if (AppWnd == NULL) {
        return 0;
    }
     ThisInst = Inst;
#if 0
    /* Load Bluetooth Piconet Manager GUI */
    if (BMG_Initialize(Inst, AppWnd) == 0) {
        DestroyWindow(AppWnd);
        return 0;
    }
    BMG_AddMainMenu(AppWnd);
#else
	//#if XA_DEBUG == XA_ENABLED && HCI_ALLOW_PRESCAN == XA_ENABLED
    #if HCI_ALLOW_PRESCAN == XA_ENABLED
     
    StartSniffer(Inst, SW_SHOW);
    #endif

#endif
#ifdef TEST_AVRCP
    sts = HIDAPP_Init(&avctp_channel, &hid_channel, AvrcpCB, HidCB, HidSecurityAndDeviceHandler);
#else
    sts = HIDAPP_Init(&hid_channel, HidCB, HidSecurityAndDeviceHandler);
#endif
    Assert(sts == TRUE);
    
    /* Display the Dialog box */
    ShowWindow( AppWnd, CmdShow ) ;

    return &AppWnd;
}

/*---------------------------------------------------------------------------
 *            APP_Deinit()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Deinitialize the app. 
 *
 * Return:    void 
 */
void APP_Deinit(void)
{
    ME_RadioShutdown();    
}

/*---------------------------------------------------------------------------
 *            APP_Thread()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Application thread. This function is called during idle times. 
 *
 * Return:    void 
 */
void APP_Thread(void)
{
   /* Anything you need to do during idle time can be put here. */
}

/*---------------------------------------------------------------------------
 *            AppWndProc()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle windows messages for the Dialog. 
 *
 * Return:    void 
 */
static BOOL CALLBACK AppWndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    static U8 keydown = FALSE;
//    char      msg[200];
    BtStatus  status;
 
    switch (uMsg) {

    case WM_INITDIALOG:
        deviceWnd = GetDlgItem(hWnd, IDC_DEVICELIST);
        DWND_Init();
        
        CheckRadioButton(hWnd, UUID_16, UUID_128, UUID_16);
        /* Good time to do GUI init stuff */
        break;

    case WM_PAINT:
        {
            PAINTSTRUCT     ps; 
            RECT            rect;
            HDC             hdc; 

            hdc = BeginPaint(hWnd, &ps); 
            GetClientRect(hWnd, &rect); 
            rect.left += 2;
            rect.right -= 3;
            DrawEdge(hdc, &rect, EDGE_ETCHED, BF_TOP); 
            EndPaint(hWnd, &ps); 
        }
        break;

    case WM_NOTIFY:
        DWND_OnNotify((NMHDR *)lParam);
        break;

    case WM_TIMER:
        /*
            if(keydown)
                HID_IntSendData(&hid_channel, 10, "\xa1\x01\x00\x00\x00\x00\x00\x00\x00\x00");        
            else
                HID_IntSendData(&hid_channel, 10, "\xa1\x01\x00\x00\x04\x00\x00\x00\x00\x00");        
            keydown = !keydown;
            */
//KeyboardHit();

//App_Report("time = %d", OS_GetSystemTime());

            MouseMove();

        break;
    case WM_COMMAND:
        /* All UI button clicks get dispatched here */
        switch (LOWORD(wParam)) {
        case UUID_16:
            break;

        case UUID_32:
            break;

        case UUID_128:
            break;

        case ACL_CONNECT:
            break;

        case ACL_DISCONNECT:
            break;

        case SENDA:

            #if 0
            MouseTest = TRUE;
            //SetTimer(hWnd, 0, 10, 0);
            #else
            HID_IntSendData(&hid_channel, 10, "\xa1\x01\x00\x00\x04\x00\x00\x00\x00\x00");
            #endif
            break;
        case SendNONE:
            #if 0
//            KillTimer(hWnd, 0);
MouseTest = FALSE;
#else
            HID_IntSendData(&hid_channel, 10, "\xa1\x01\x00\x00\x00\x00\x00\x00\x00\x00");
#endif
            break;
        case DISCONNECT:
            HID_Disconnect(&hid_channel);
#ifdef TEST_AVRCP
            AVRCP_Disconnect(&avctp_channel);
#endif
            
            break;
        case CONNECT:
        {
            BD_ADDR badr;
            //tallplay
            badr.addr[0] = 0x38;
            badr.addr[1] = 0xEA;
            badr.addr[2] = 0x85;
            badr.addr[3] = 0xE4;
            badr.addr[4] = 0xA3;
            badr.addr[5] = 0x78;
            HID_Connect(&hid_channel, &badr);
            break;
        }
        case CONNECT2:
        {
            BD_ADDR badr;
#if 0
//mac
            badr.addr[0] = 0x90;
            badr.addr[1] = 0x4B;
            badr.addr[2] = 0x21;
            badr.addr[3] = 0x4B;
            badr.addr[4] = 0x33;
            badr.addr[5] = 0x60;
#else
//evo
            badr.addr[5] = 0xD8;
            badr.addr[4] = 0xB3;
            badr.addr[3] = 0x77;
            badr.addr[2] = 0x42;
            badr.addr[1] = 0x46;
            badr.addr[0] = 0xD8;
#endif
            HID_Connect(&hid_channel, &badr);
            break;
        }
        case CONNECT3:
        {
            BD_ADDR badr;
//win7
            badr.addr[0] = 0xFC;
            badr.addr[1] = 0xF5;
            badr.addr[2] = 0xB7;
            badr.addr[3] = 0x08;
            badr.addr[4] = 0xDD;
            badr.addr[5] = 0x78;
            HID_Connect(&hid_channel, &badr);
            break;
        }           
        case DISCOVER:
           HIDAPP_SetDiscoverable(TRUE);
            break;
        case NON_DISCOVER:
           HIDAPP_SetDiscoverable(FALSE);
            break;
        case STARTSNIFFER:
//            StartSniffer
            break;
       case STOPSNIFFER:
        StopSniffer();
        break;

        case WM_DESTROY:
        case EXIT_BUTTON:
            if (AppWnd == hWnd) {
                /* Exit the application */
                  PostQuitMessage(0);
  
                isquit = TRUE;
            }
            
            return TRUE;
            break;
         default:
//            BMG_TrackMainMenu(hWnd, wParam, lParam);
            break;
        }
        break;
       case PIN_REQ:
        {
            PinCodeReq  parms;
            BtDeviceContext *bdc = (BtDeviceContext *)lParam;

            OS_MemSet((U8 *)&parms, 0, sizeof(parms));
            parms.DeviceDesc = "haha";

            /* Using IsWindowVisible() helps focus return to the right window. */
            if (DialogBoxParam(ThisInst, MAKEINTRESOURCE(IDC_ENTER_PIN), 
                               IsWindowVisible(hWnd) ? hWnd : GetParent(hWnd),
                               EnterPinProc, (LPARAM)&parms) == TRUE) {

                /* User entered Pin code */
                status = HIDAPP_SetPin(bdc, parms.Pin, parms.PinLen, parms.Flags);
            } else {
                /* Fail Pin code request */
                status = HIDAPP_SetPin(bdc, 0, 0, 0);
            }
            App_Report("SetPin() returned status %s.", pBT_Status(status));
        }            
        break;
    }

    return 0L;
}

/*---------------------------------------------------------------------------
 *            App_Report()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Prints messages to the UI list box 
 *
 * Return:    void 
 */
void App_Report(char *format,...) 
{ 
    char     buffer[200]; /* Output Buffer */
    int      index;
    va_list  args;

    va_start(args, format);
    index = _vsnprintf(buffer, 200, format, args);
    va_end(args);

    index = SendDlgItemMessage(AppWnd, LIST_OUTPUT, LB_ADDSTRING, 0, (LPARAM)buffer);
    if (index != LB_ERR) {
        SendDlgItemMessage(AppWnd, LIST_OUTPUT, LB_SETCARETINDEX, (WPARAM)index, FALSE);
    }
}

/*
 * Process radio status changes to update GUI
 */
static void RadioStatusChange(BtEventType Status)
{
    BD_ADDR          bdaddr;
    BtDeviceContext *bdc = 0;

    if (Status == BTEVENT_HCI_INITIALIZED) {
        if (HIDAPP_ReadLocalBdAddr(&bdaddr) == BT_STATUS_SUCCESS) {
            /*govert!!!, use for check our spi secret*/
        }

        HIDAPP_SetDiscoverable(FALSE);    
    }
    else if(Status == BTEVENT_HCI_DEINITIALIZED)
    {
        if(isquit == TRUE)
            AppWnd = 0;
    }
}

/*---------------------------------------------------------------------------
 *             main()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  The main program. Initiailizes the Mini OS which in turn 
 *            initializes the stack.  It then initializes the application, 
 *            adds the application thread to the OS, and starts the Mini OS.
 *
 * Return:    Status of the operation.
 */
int PASCAL WinMain(HINSTANCE Inst, HINSTANCE PrevInst, LPTSTR CmdLine, int CmdShow)
{
    BOOL sts;
    
    //read unique id
    U8 cmd[13] = "\xF2\x00\x03\x00\x00\x00\x00\x00\x00\x00\x00\x00";
    U8 uid[8];
    int rlen;
    rlen = 8;

    MassUart_Open();
    MassUart_Execute(0, &rlen, cmd, uid);
    MassUart_Close();
/*
    if(rlen == 8)
        RADIO_SetBDAddr(uid);
    else
    */
        RADIO_SetBDAddr("\xB8\x32\x22\x10\x15\xBB");
      setRadioType(RADIO_TYPE_MTK_6622_UART);

    
    if (!OS_Init()) {
        Report(("Fatal Error: Could not initialize stack.\n"));
        return(1);
    }

    if ((APP_Init(Inst, PrevInst, CmdLine, CmdShow)) != NULL) {

        Assert(ME_SetLocalDeviceName("tallplay", 9) ==  BT_STATUS_SUCCESS);
        /* Set up regular calls to APP_Thread (one way or another) */
        MINIOS_AddThread(APP_Thread);

        while (AppWnd) {

            /* Let the minios run all threads */
            MINIOS_Run();

            /* Wait until a message is available, then process it */
            MsgWaitForMultipleObjectsEx(0, NULL, 10,
                    QS_ALLEVENTS | QS_ALLINPUT | QS_ALLPOSTMESSAGE , 0);
            /* Processes any messages currently on the queue. */
            PumpMessages();
            if(MouseTest)
            {
                MouseMove();
            }
        }
    }

    OS_Deinit();

    /* TODO: Bother to kill the app thread? or just exit? */

    return 0;
}



/*---------------------------------------------------------------------------
 *             PumpMessages()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  This function is responsible for processing window messages.
 *
 */
void PumpMessages(void)
{
    MSG     w_msg;
    HWND    hWnd;

    while (PeekMessage(&w_msg, NULL, 0, 0, PM_NOREMOVE)) {

        if (GetMessage(&w_msg, NULL, 0, 0) == 0) {
            if ((hWnd = AppWnd) != 0) {
                Report(("Deinitializing\n"));
                //AppWnd = 0;

                APP_Deinit();
            }
            break;
        }

        if (w_msg.message == WM_APP+100) {
            /* This message is used to tell the message pump to change the 
             * window handle to the currently active dialog. It is necessary to
             * have ActiveWnd point to the correct dialog to insure keyboard
             * selection messages (tab key, down key, etc.) work.
             */
            if (w_msg.lParam) {
                Assert(IsWindow((HWND)w_msg.lParam));
                AppWnd = (HWND)w_msg.lParam;
            }
            continue;
        }

        if (!AppWnd || !IsDialogMessage(AppWnd, &w_msg)) {
            TranslateMessage( &w_msg );
            DispatchMessage( &w_msg );
        }
    }
}

