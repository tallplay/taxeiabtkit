/***************************************************************************
 *
 * File:
 *     $Workfile:sdp_app_gui.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:24$
 *
 * Description:   This is file contains the code for the OS specific
 *                SDAP user interface. 
 *
 * Created:       February 20, 2001
 *
 * Copyright 2001-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "sdp.h"

#include "windows.h"
#include "commctrl.h"
#include "stdio.h"
#include "resource.h"
#include "btalloc.h"
#include "bluemgr_ui.h"
#include "sys/debug.h"

/****************************************************************************
 *
 * Constants
 *
 ****************************************************************************/
/* External Functions */
extern BtStatus SendSdpUuidQuery(SdpQueryMode mode, U8 uuidSize);

extern void InitializeSdpServer(void);
extern void InitializeSdpClient(void);
extern void DeinitializeSdpServer(void);
extern void DeinitializeSdpClient(void);
extern void SdpEventHandler(const BtEvent* event);

extern void App_Report(char *format,...);

/* Internal Functions */
HWND *APP_Init(HINSTANCE Inst, HINSTANCE PrevInst, LPTSTR CmdLine, int CmdShow);
void APP_Deinit(void);
void APP_Thread(void);
static BOOL CALLBACK AppWndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

/* External Variables */
extern BtRemoteDevice* remDev; 
extern U16             qUuid;
extern U8              uuidSize;
extern BtHandler       devHandler;

/* Internal Variables */
static HWND          AppWnd;
static BtSelectDeviceToken  devQuery;

/*---------------------------------------------------------------------------
 *            APP_Init()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initialize the applicaiton. This is the main entr point. 
 *
 * Return:    void 
 */
HWND *APP_Init(HINSTANCE Inst, HINSTANCE PrevInst, LPTSTR CmdLine, int CmdShow)
{
    /* Load the common control library into the application */
    InitCommonControls();

    /* Create the Application dialog */ 
    AppWnd = CreateDialog(Inst, MAKEINTRESOURCE(MAINDIALOG), NULL, AppWndProc);
    if (AppWnd == NULL) {
        return 0;
    }

    /* Load Bluetooth Piconet Manager GUI */
    if (BMG_Initialize(Inst, AppWnd) == 0) {
        DestroyWindow(AppWnd);
        return 0;
    }
    BMG_AddMainMenu(AppWnd);

    /* Display the Dialog box */
    ShowWindow( AppWnd, CmdShow ) ;

    InitializeSdpServer();
    InitializeSdpClient();

    return &AppWnd;
}

/*---------------------------------------------------------------------------
 *            APP_Deinit()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Deinitialize the app. 
 *
 * Return:    void 
 */
void APP_Deinit(void)
{
    DeinitializeSdpClient();
    DeinitializeSdpServer();
}

/*---------------------------------------------------------------------------
 *            APP_Thread()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Application thread. This function is called during idle times. 
 *
 * Return:    void 
 */
void APP_Thread(void)
{
   /* Anything you need to do during idle time can be put here. */
}

/*---------------------------------------------------------------------------
 *            AppWndProc()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle windows messages for the Dialog. 
 *
 * Return:    void 
 */
static BOOL CALLBACK AppWndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    char      msg[200];
    BtStatus  status;
 
    switch (uMsg) {

    case WM_INITDIALOG:
        uuidSize = 16;
        CheckRadioButton(hWnd, UUID_16, UUID_128, UUID_16);
        /* Good time to do GUI init stuff */
        break;

    case WM_PAINT:
        {
            PAINTSTRUCT     ps; 
            RECT            rect;
            HDC             hdc; 

            hdc = BeginPaint(hWnd, &ps); 
            GetClientRect(hWnd, &rect); 
            rect.left += 2;
            rect.right -= 3;
            DrawEdge(hdc, &rect, EDGE_ETCHED, BF_TOP); 
            EndPaint(hWnd, &ps); 
        }
        break;

    case WM_COMMAND:
        /* All UI button clicks get dispatched here */
        switch (LOWORD(wParam)) {
        case UUID_16:
            uuidSize = 16;
            break;

        case UUID_32:
            uuidSize = 32;
            break;

        case UUID_128:
            uuidSize = 128;
            break;

        case ACL_CONNECT:
            devQuery.callback = SdpEventHandler;
            devQuery.quality.mask = BDQM_DEVICE_STATUS;
            devQuery.quality.status = BDS_IN_RANGE;

            status = DS_SelectDevice(&devQuery);
            App_Report("DS_SelectDevice() returned %s.", pBT_Status(status));
            break;

        case ACL_DISCONNECT:
            if (remDev) {
               status = ME_DisconnectLink(&devHandler, remDev);
               App_Report("ME_DisconnectLink() returned %s.", pBT_Status(status));
               if (status == BT_STATUS_SUCCESS)
                   remDev = 0;
            }
            break;

        case OBJECT_PUSH_QUERY:
            qUuid = SC_OBEX_OBJECT_PUSH;      /* Uuid16 Object Push */
            goto DoQuery;

        case SERIAL_QUERY:
            qUuid = SC_SERIAL_PORT;           /* Uuid16 Serial Port */
            goto DoQuery;

        case FAX_QUERY:
            qUuid = SC_FAX;                   /* Uuid16 Fax */
            goto DoQuery;

        case DUN_QUERY:
            qUuid = SC_DIALUP_NETWORKING;     /* Uuid16 DUN */
            goto DoQuery;

        case LAN_QUERY:
            qUuid = SC_LAN_ACCESS_PPP;        /* Uuid16 LAN Access using PPP */
            goto DoQuery;

        case HS_QUERY:
            qUuid = SC_HEADSET;               /* Uuid16 Headset */
            goto DoQuery;

        case HS_GW_QUERY:
            qUuid = SC_HEADSET_AUDIO_GATEWAY; /* Uuid16 Headset Audio Gateway */
            goto DoQuery;

        case INTERCOM_QUERY:
            qUuid = SC_INTERCOM;              /* Uuid16 Intercom */
            goto DoQuery;

        case CORDLESS_QUERY:
            qUuid = SC_CORDLESS_TELEPHONY;    /* Uuid16 Cordless Telephony */
            goto DoQuery;

        case FILE_TRANSFER_QUERY:
            qUuid = SC_OBEX_FILE_TRANSFER;    /* Uuid16 Obex File Transfer */
            goto DoQuery;

        case SYNC_QUERY:
            qUuid = SC_IRMC_SYNC;             /* Uuid16 IrMC Synchronization */
            goto DoQuery;

        case SYNC_CMD_QUERY:
            qUuid = SC_IRMC_SYNC_COMMAND;     /* Uuid16 IrMC Synchronization Command */
DoQuery:
                /* Perform an SDP query */
            if (remDev != 0) {
                status = SendSdpUuidQuery(BSQM_FIRST, uuidSize);
                if (status == BT_STATUS_PENDING) {
                    App_Report("Starting SDP Query");
                } else {
                    sprintf(msg, "SDP Query failed to start. Status = %d",status);
                    App_Report(msg);
                }
            } else {
                App_Report("Unable to start query - no connection");
            }
            break;

        case WM_DESTROY:
        case EXIT_BUTTON:
            if (AppWnd == hWnd) {
                /* Exit the application */
                if (BMG_Shutdown() != BT_STATUS_PENDING)
                    PostQuitMessage(0);
            }
            return TRUE;

        default:
            BMG_TrackMainMenu(hWnd, wParam, lParam);
            break;
        }
        break;
    }

    return 0L;
}

/*---------------------------------------------------------------------------
 *            App_Report()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Prints messages to the UI list box 
 *
 * Return:    void 
 */
void App_Report(char *format,...) 
{ 
    char     buffer[200]; /* Output Buffer */
    int      index;
    va_list  args;

    va_start(args, format);
    index = _vsnprintf(buffer, 200, format, args);
    va_end(args);

    index = SendDlgItemMessage(AppWnd, LIST_OUTPUT, LB_ADDSTRING, 0, (LPARAM)buffer);
    if (index != LB_ERR) {
        SendDlgItemMessage(AppWnd, LIST_OUTPUT, LB_SETCARETINDEX, (WPARAM)index, FALSE);
    }
}

