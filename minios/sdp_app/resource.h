//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by sdp_app.rc
//
#define MAINDIALOG                      101
#define LIST_OUTPUT                     1000
#define EXIT_BUTTON                     1001
#define FAX_QUERY                       1002
#define OBJECT_PUSH_QUERY               1003
#define SERIAL_QUERY                    1005
#define LAN_QUERY                       1006
#define HS_QUERY                        1007
#define INTERCOM_QUERY                  1008
#define CORDLESS_QUERY                  1009
#define FILE_TRANSFER_QUERY             1010
#define SYNC_QUERY                      1011
#define HS_GW_QUERY                     1012
#define SYNC_CMD_QUERY                  1013
#define DUN_QUERY                       1025
#define UUID_16                         1026
#define UUID_32                         1027
#define UUID_128                        1028
#define ACL_VIEW                        1029
#define ACL_CONNECT                     1032
#define ACL_DISCONNECT                  1033

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        103
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1030
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
