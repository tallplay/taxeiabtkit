#ifndef __OVERIDE_H
#define __OVERIDE_H
/***************************************************************************
 *
 * File:
 *     $Workfile:overide.h$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:19$
 *
 * Description:   This file contains declarations that are used to override
 *                the configure options set in config files such as 
 *                btconfig.h. If no overrides are needed then this file 
 *                should be empty. It still must exist. 
 * 
 * Created:       July 15, 1999
 *
 * Copyright 1999-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of Counterpoint Systems Foundry, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of Counterpoint Systems Foundry, Inc.
 * 
 * Use of this work is governed by a license granted by Counterpoint Systems 
 * Foundry, Inc.  This work contains confidential and proprietary 
 * information of Counterpoint Systems Foundry, Inc. which is protected by 
 * copyright, trade secret, trademark and other intellectual property rights.
 *
 ****************************************************************************/

/* WARNING: The values in this overide.h file were selected specifically for
 * this sample application. If you change them, the sample application may fail
 * to compile or not work properly.
 */

#define RFCOMM_PROTOCOL          XA_DISABLED
#define XA_USE_ENDIAN_MACROS     XA_ENABLED
#define XA_SNIFFER               XA_ENABLED

/* For larger SDP Server responses */
#define SDP_SERVER_SEND_SIZE     L2CAP_MTU /* 672 default */

/* Only needed for the USB transport. */
#define HCI_USB_TX_BUFFER_SIZE  260

#endif /* __OVERIDE_H */
