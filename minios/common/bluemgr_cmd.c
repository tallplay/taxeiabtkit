/***************************************************************************
 *
 * File:
 *     $Workfile:bluemgr_cmd.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:18$
 *
 * Description:
 *     This is file contains a simple application for interfacing
 *     with the Bluetooth Management Entity.
 *
 * Copyright 1999-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/
#include "utils.h"
#include "sys/debug.h"
#include "textmenu.h"
#include "bluemgr_ui.h"
#include "sys/hci.h"
#include "sys/mei.h"
#include "parseopts.h"
#include "prescan.h"
#include "sniffer_ui.h"
#include "stdio.h"
#include "stdarg.h"
#include "string.h"

extern void App_Report(char *format,...);

/***************************************************************************
 *
 * Local defines
 *
 ***************************************************************************/


/* Internal functions */
static BOOL BmiSelectDevice(U32 Index);
static BtDeviceContext *BmiGetSelectedDevice();
static BOOL BmiMenuHandler(MnuTextMenu *Menu, U16 Cmd, void *Arg);
static BOOL BmiEnterPinHandler(MnuTextMenu *Menu, U16 Cmd, void *Arg);
static void BmiEventHandler(const BtEvent *event);
static void BmiPrint(const char *format,...);

/* Piconet Manager menu constants */
#define M_BMI_DOINQUIRY         1201
#define M_BMI_AUTHENTICATE      1202
#define M_BMI_SELECTDEVICE      1203
#define M_BMI_DEVICESELECT_DONE 1204
#define M_BMI_MAINMENU          1205
#define M_BMI_FLUSHLIST         1206
#define M_BMI_PIN_ENTERED       1207
#define M_BMI_SPECIFY_ADDR      1208
#define M_BMI_SPECIFY_ADDR_DONE 1209
#define M_BMI_LOGGER            1210

/* Piconet Manager Option flags. Stored in BtDeviceContext->context */
#define PMO_SELECTED            0x00000001

/****************************************************************************
 *
 * Piconet Manager Command-line interface driver specific data
 *
 ****************************************************************************/
static MnuTextMenu          BmiMenu;

static const MnuItem BmiMenuItems[] = {
    { 'i', "Perform (or Cancel) a General Inquiry", M_BMI_DOINQUIRY, 0 },
    { 's', "Select a device", M_BMI_SELECTDEVICE, 0 },
    { 'a', "Authenticate device", M_BMI_AUTHENTICATE, 0 },
//  { 'f', "Flush Bluetooth Device List", M_BMI_FLUSHLIST, 0};
    { 'b', "Specify a Bluetooth Device Address", M_BMI_SPECIFY_ADDR, 0},
    { 'l', "Start (or Stop) Protocol Analyzer logging", M_BMI_LOGGER, 0},
    { 'x', "Switch to the Main Menu", M_BMI_MAINMENU, &BmiMenu.workspace},
    { 'q', "Quit", M_QUIT, 0 },
    { 0, 0, 0, 0 }};

typedef struct _PinCodeRequest {
    U8              Pin[MAX_PIN_LEN];
    U8              Flags;
    BtRemoteDevice *Device;
    void           *Context;
} PinCodeReq;

static PinCodeReq     EnterPinWs;
static MnuTextMenu    BmiMenu = { "Bluetooth Manager Menu", BmiMenuHandler, BmiMenuItems, 0 };
static MnuTextMenu    BmiEnterPinMenu = { 0, BmiEnterPinHandler, 0, (U32)&EnterPinWs };
static U8             SpecifyAddr[BDADDR_NTOA_SIZE];
static BtSelectDeviceToken *SelectDevice;
static BtHandler      globalHandler;
static U32            lastInqTime;
static BOOL           InquiryInProgress;
static BtSecurityMode securityMode = 0;
static BtHandler      securityHandler;

/****************************************************************************
 *
 * Public Piconet Manager Command-line interface functions
 *
 ****************************************************************************/
MnuTextMenu *BMI_CreateMenu(void)
{
    ME_InitHandler(&globalHandler);
    globalHandler.callback = BmiEventHandler;
    AssertEval(ME_RegisterGlobalHandler(&globalHandler) == BT_STATUS_SUCCESS);

    ME_SetEventMask(&globalHandler, BEM_ALL_EVENTS);

    /* Register Bluetooth monitor event handler. */
    DS_RegisterMonitorHandler(&globalHandler);

    /* Set our promiscuous link policies */
    ME_SetDefaultLinkPolicy((BLP_MASTER_SLAVE_SWITCH|BLP_HOLD_MODE|BLP_SNIFF_MODE|BLP_PARK_MODE|BLP_SCATTER_MODE),
                            (BLP_MASTER_SLAVE_SWITCH|BLP_HOLD_MODE|BLP_SNIFF_MODE|BLP_PARK_MODE|BLP_SCATTER_MODE));

    if (getSnifferOption() == TRUE) {
        if (StartSnifferLogging("BtTrace.txt"))
            BmiPrint("Logging started to 'BtTrace.txt'.\n");
    }

    return &BmiMenu;
}

BtStatus BMI_Shutdown(void)
{
    return ME_RadioShutdown();
}

BtStatus BMI_Deinitialize(void)
{
    ME_UnregisterGlobalHandler(&globalHandler);
    DS_RegisterMonitorHandler(0);

    return BT_STATUS_SUCCESS;
}

BOOL BMI_IsRadioReady(void)
{
    return (ME_GetStackInitState() == BTSS_INITIALIZED);
}

#if BT_SECURITY == XA_ENABLED
/*---------------------------------------------------------------------------
 *            BMI_SetSecurityMode()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Set the security mode for the device. This includes the
 *            registration or deregistration of pairing and security handlers
 *            with the Management Entity. This command affects future
 *            connections only.
 */
BtStatus BMI_SetSecurityMode(BtSecurityMode SecMode)
{
    BtStatus    status;

    switch (SecMode) {
    case BSM_SEC_DISABLED:
        /* Remove Authorization and Pairing handlers. */
        SEC_RegisterPairingHandler(0);
        SEC_RegisterAuthorizeHandler(0);
        BmiPrint("Deregistered Pairing & Authorization handlers.\n");

        if (securityMode < BSM_SEC_LEVEL_3) {
            /* Done downgrading security level. */
            securityMode = BSM_SEC_LEVEL_1;
            status = BT_STATUS_SUCCESS;
            goto exit;
        }
        /* Drop down to disable mode 3 */

    case BSM_SEC_LEVEL_1:
    case BSM_SEC_LEVEL_2:
        status = SEC_DisableSecurityMode3(&securityHandler);
        BmiPrint("SEC_DisableSecurityMode3() returned %s.\n", pBT_Status(status));
        break;

    case BSM_SEC_LEVEL_3:
        status = SEC_EnableSecurityMode3(&securityHandler, FALSE);
        BmiPrint("SEC_EnableSecurityMode3() (without encryption) returned %s.\n", pBT_Status(status));
        break;

    case BSM_SEC_ENCRYPT:
        status = SEC_EnableSecurityMode3(&securityHandler, TRUE);
        BmiPrint("SEC_EnableSecurityMode3() (with encryption) returned %s.\n", pBT_Status(status));
        break;
    }

    if ((status == BT_STATUS_PENDING) || (status == BT_STATUS_SUCCESS)) {
        if (securityMode < BSM_SEC_LEVEL_2) {
            /* Old security level didn't have handlers registered. 
             * Register our security event handlers now.
             */
            ME_InitHandler(&securityHandler);
            securityHandler.callback = BmiEventHandler;

            SEC_RegisterPairingHandler(&securityHandler);
            SEC_RegisterAuthorizeHandler(&securityHandler);
        }
        securityMode = SecMode;
    }

    exit:
    return status;
}
#endif /* BT_SECURITY == XA_ENABLED */


/****************************************************************************
 *
 * Internal Bmi Command specific Functions
 *
 ****************************************************************************/
static BOOL BmiMenuHandler(MnuTextMenu *Menu, U16 Cmd, void *Arg)
{
    BtDeviceContext     *bdc;
    BtDeviceContext      newDevice;
    BtSelectDeviceToken *temp;
    MnuReadInteger      *mri;
    MnuReadString       *mrs;
    U16                  index;
    BtStatus             status;
    char                 addrname[BDADDR_NTOA_SIZE];

    switch (Cmd) {
    case M_ACTIVATE:
        /* First time through, the Bmi menu remembers where it
         * came from so it can always go back to that menu.
         */
        if (Menu->workspace == 0) {
            Assert(Arg);    /* Bmi can not be the first menu loaded */
            Menu->workspace = (U32)Arg;
        }
        break;

    case M_SHOW:
        /* Show devices in range */
        bdc = 0;
        
        for (index = 0; DS_EnumDeviceList(&bdc) == BT_STATUS_SUCCESS; index++) {
            BmiPrint("Device %d  %s  %s%s\n", index, bdaddr_ntoa(&bdc->addr, addrname),
                     ((U32)bdc->context & PMO_SELECTED ? " <Selected>":""),
                     (bdc->link && bdc->link->state == BDS_CONNECTED ? " <Connected>":
                     (bdc->state & BDS_IN_RANGE ? " <In Range>":"")));
        }
        if (index > 0)
            Mnu_EchoKey('\n');
        break;

    case M_BMI_DOINQUIRY:
        if (InquiryInProgress == FALSE) {
            /* Start an inquiry */
            status = ME_Inquiry(BT_IAC_GIAC, BT_INQ_TIME_GAP100, NUM_KNOWN_DEVICES);
            BmiPrint("ME_Inquiry() returned %s.", pBT_Status(status));
            if (status == BT_STATUS_PENDING)
                InquiryInProgress = TRUE;
        } else {
            status = ME_CancelInquiry();
            BmiPrint("ME_CancelInquiry() returned %s.", pBT_Status(status));
            if (status != BT_STATUS_PENDING)
                InquiryInProgress = FALSE;
        }
        break;

    case M_BMI_AUTHENTICATE:
        if ((bdc = BmiGetSelectedDevice()) && bdc->link) {
            status = SEC_AuthenticateLink(&globalHandler, bdc->link);
            BmiPrint("SEC_AuthenticateLink() returned %s.\n", pBT_Status(status));
            break;
        }
        BmiPrint("No (or invalid) device was selected.\n");
        break;

    case M_SWITCHMENU:
        Mnu_Load((MnuTextMenu *)Arg, TRUE);
        break;

    case M_BMI_MAINMENU:
        Mnu_Load(*(MnuTextMenu **)Arg, TRUE);
        break;

    case M_BMI_SELECTDEVICE:
        /* To do select we calculate the max index and
         * auto-select if there is only one choice.
         */
        bdc = 0;
        index = 0;
        while (DS_EnumDeviceList(&bdc) == BT_STATUS_SUCCESS) index++;

        if (index == 0) {
            BmiPrint("There are no known devices, try (i)nquiry first\n");
            /* Fail menu switch */
            return FALSE;
        }
        index--;

        if (index == 0) {
            BmiSelectDevice(0);
            BmiPrint("Auto-Selected device %d.\n", 0);
            goto DeviceSelected;
        }

        /* Call the Menu to read the integer */
        BmiPrint("Enter Index of Device to Select (0 - %d): \r", index);
        Mnu_ReadInteger(M_BMI_DEVICESELECT_DONE, 0, index, 10);
        break;

    case M_BMI_DEVICESELECT_DONE:
        /* The Menu is done reading the integer */
        mri = (MnuReadInteger *)(Arg);
        if (mri->status != MRERR_SUCCESS) {
            if (mri->status == MRERR_RANGE_ERR)
                BmiPrint("Invalid Selection.\n");

            Mnu_Show();
            break;
        }
        BmiSelectDevice((U16)mri->value);
        BmiPrint("Selected device %d.\n", mri->value);

DeviceSelected:
        if ((temp = SelectDevice)) {
            /* We have a DS_SelectDevice() request in progress. 
             * Return the results and cancel the auto-inquiry.
             */
            AssertEval(bdc = BmiGetSelectedDevice());
            SelectDevice = 0;

            if (InquiryInProgress) {
                /* This will cancel the inquiry. */
                BmiMenuHandler(&BmiMenu, M_BMI_DOINQUIRY, 0);
            }

            DS_SelectDeviceResult(temp, bdc, BT_STATUS_SUCCESS);
        }
        break;

    case M_BMI_SPECIFY_ADDR:
        BmiPrint("Enter Bluetooth Device Address (ex. 01:02:03:Aa:Bb:Cc): \r");
        Mnu_ReadString(M_BMI_SPECIFY_ADDR_DONE, SpecifyAddr, BDADDR_NTOA_SIZE);
        break;

    case M_BMI_SPECIFY_ADDR_DONE:
        mrs = (MnuReadString *)Arg;
        if (mrs->status == MRERR_SUCCESS) {
            OS_MemSet((U8 *)&newDevice, 0, sizeof(BtDeviceContext));

            newDevice.addr = bdaddr_aton(SpecifyAddr);
            if (IsValidBdAddr(&newDevice.addr))
                DS_AddDevice(&newDevice, &bdc);
        }
        break;

    case M_BMI_LOGGER:
        if (!IsSnifferOpen()) {
            if (StartSnifferLogging("BtTrace.txt"))
                BmiPrint("Logging started to 'BtTrace.txt'.\n");
            else BmiPrint("Logging failed to start.\n'");
        } else {
            StopSniffer();
            BmiPrint("Logging stopped.\n");
        }
        break;

    case M_QUIT:
        BMI_Shutdown();
        break;
    }
    return TRUE;
}

static BOOL BmiEnterPinHandler(MnuTextMenu *Menu, MnuCommand Cmd, MnuArg Arg)
{
    PinCodeReq      *pin = (PinCodeReq *)(Menu->workspace);
    MnuReadString   *mrs = (MnuReadString *)Arg;
#if BT_SECURITY == XA_ENABLED
    BtStatus         status;
#endif
    
    switch (Cmd) {
    case M_ACTIVATE:
        /* Save old menu ptr */
        pin->Context = Arg;
        break;

    case M_SHOW:
        BmiPrint("Enter PIN Code (16 characters max): \r");
        Mnu_ReadString(M_BMI_PIN_ENTERED, pin->Pin, MAX_PIN_LEN);
        break;

#if BT_SECURITY == XA_ENABLED
    case M_BMI_PIN_ENTERED:
        mrs = (MnuReadString *)Arg;
        if (mrs->status == MRERR_SUCCESS)
            status = SEC_SetPin(pin->Device, mrs->buffer, (U8)mrs->length, pin->Flags);
        else status = SEC_SetPin(pin->Device, 0, 0, 0);

        BmiPrint("Set Pin returned status %s.", pBT_Status(status));

        Mnu_Load((MnuTextMenu *)pin->Context, FALSE);
        break;
#endif
        
    }

    return TRUE;
}


static BOOL BmiSelectDevice(U32 Index)
{
    BtDeviceContext    *bdc = 0;
    U32                 i;

    /* Mark all devices unselected, except the chosen one */
    for (i = 0; DS_EnumDeviceList(&bdc) == BT_STATUS_SUCCESS; i++) {
        if (i == Index)
            (U32)bdc->context |= PMO_SELECTED;
        else (U32)bdc->context &= ~PMO_SELECTED;
    }

    return TRUE;
}


/*
 * Retrieve the BtDeviceContext of the currently selected device
 */
static BtDeviceContext *BmiGetSelectedDevice()
{
    BtDeviceContext *bdc = 0;

    /* Walk the list of devices, looking for the matching addr */
    while (DS_EnumDeviceList(&bdc) == BT_STATUS_SUCCESS) {
        if ((U32)bdc->context & PMO_SELECTED) {
            return bdc;
        }
    }
    return 0;
}

/*
 * Write a message to the Blue Manager console.
 *
 * Normally this function needs to make sure that there is a \n on the
 * end of the string. To support the special case where we don't want a
 * \n to be appended, we check for a \r and if found we don't add the \n.
 */
static void BmiPrint(const char *format,...)
{
    char            buffer[205] = "BlueMgr: "; /* Output Buffer */
    int             index;
    va_list         args;

    va_start(args, format);
    index = _vsnprintf(buffer+9, 200, format, args) + 9;
    va_end(args);

    App_Report(buffer);
}


/*
 * Queries the user for the PIN code
 */
static BOOL BmiEnterPinCode(BtRemoteDevice *Device)
{ 
    char    addr[BDADDR_NTOA_SIZE];

    bdaddr_ntoa(&Device->bdAddr, addr);
    BmiPrint("Authentication requested for device %s\n", addr);
    EnterPinWs.Device = Device;
    EnterPinWs.Flags = BPT_SAVE_TRUSTED;

    /* We need to switch to a PIN Code specific menu because
     * we don't know which menu is active.
     */
    Mnu_Load(&BmiEnterPinMenu, TRUE);
    return TRUE;
}


/*
 * Processes Bluetooth Management entity events in order to perform 
 * Command interface driver specific actions.
 */
static void BmiEventHandler(const BtEvent *event)
{
    BtDeviceContext     *bdc = 0;
    U32                  index;
    char                 addrname[BDADDR_NTOA_SIZE];

    switch (event->eType) {
    case BTEVENT_INQUIRY_RESULT:
        BmiPrint("Inquiry found device %s.\n", bdaddr_ntoa(&event->p.inqResult.bdAddr, addrname));
        break;

    case BTEVENT_INQUIRY_COMPLETE:
    case BTEVENT_INQUIRY_CANCELED:
        InquiryInProgress = FALSE;
        BmiPrint("Inquiry Complete.\n");
        /* Mark first 'In Range' device as selected */
        for (index = 0; DS_EnumDeviceList(&bdc) == BT_STATUS_SUCCESS; index++) {
            if (bdc->state == BDS_IN_RANGE) {
                BmiSelectDevice(index);
                break;
            }
        }
        lastInqTime = OS_GetSystemTime();
        break;

    case BTEVENT_SELECT_DEVICE_REQ:
        if (SelectDevice != 0) {
            /* Call completion function with failure. */
            DS_SelectDeviceResult(event->p.select, 0, BEC_UNSPECIFIED_ERR);
            break;
        }

        /* If we have a selected device, return it. */
        if (bdc = BmiGetSelectedDevice()) {
            DS_SelectDeviceResult(event->p.select, bdc, BEC_NO_ERROR);
            break;
        }

        /* Initiate the request */
        SelectDevice = event->p.select;
        if (InquiryInProgress == FALSE) {
            if ((lastInqTime + MS_TO_TICKS(INQUIRY_REFERSH_INTERVAL)) < OS_GetSystemTime()) {
                /* Run a new inquiry */
                BmiMenuHandler(&BmiMenu, M_BMI_DOINQUIRY, 0);
                Mnu_Load(&BmiMenu, TRUE);
            }
        }
        break;

    case BTEVENT_LINK_CONNECT_IND:
        if (event->errCode == BEC_NO_ERROR) {
            for (index = 0; DS_EnumDeviceList(&bdc) == BT_STATUS_SUCCESS; index++) {
                if (OS_MemCmp(bdc->addr.addr, 6, event->p.remDev->bdAddr.addr, 6) == 0) {
                    /* If no device is currently selected, select this newly connected one. */
                    if (!BmiGetSelectedDevice())
                        BmiSelectDevice(index);
                }
            }
        }
        break;

    case BTEVENT_PIN_REQ:
        BmiEnterPinCode(event->p.remDev);
        break;

    case BTEVENT_HCI_FAILED:
        BmiPrint("Radio Offline.\n");
        break;
    }
}
