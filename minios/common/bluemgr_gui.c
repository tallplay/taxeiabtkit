/***************************************************************************
 *
 * File:
 *     $Workfile:bluemgr_gui.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:86$
 *
 * Description:
 *     This is file contains a simple application for interfacing
 *     with the Bluetooth Management Entity.
 *
 * Created:
 *     October 18, 1999
 *
 * Copyright 1999-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/
#include "windows.h"
#include "commctrl.h"
#include "bluemgrrc.h"
#include "stdio.h"
#include "winreg.h"
#include "utils.h"
#include "medev.h"
#include "sniffer_ui.h"
#include "parseopts.h"
#include "sys/mei.h"
#include "fts.h"
#include "bluemgr_ui.h"
#include "bluemgri.h"
#include "sys/btstats.h"
#include "sys/debug.h"

/****************************************************************************
 *
 * Bluetooth Piconet Manager Windows GUI RAM
 *
 ****************************************************************************/

/* Bluetooth Control Panel settings */
static BtControlPanel BlueSdkControl = BSDK_CONTROL_DEFAULTS;
#define BCC(f)  (BlueSdkControl.f)

/* Blue Manager GUI Context */
static BlueMgrGuiContext BlueMgrGui = {0};
#define BMG(f)  (BlueMgrGui.f)

/* Bluetooth & Blue Manager Property pages */
static tabInfo Tabs[] = PROPERTY_PAGES;

/* Service Search Attribute Request */
static U8 BrowseServiceSearchAttribReq[] = SERVICE_BROWSE_QUERY;

/****************************************************************************
 *
 * Bluetooth Piconet Manager Windows GUI functions
 *
 ****************************************************************************/
/*---------------------------------------------------------------------------
 *            BMG_Initialize()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initialize the Bluetooth Device manager user interface component. 
 *            This initialization includes the registration of Management
 *            entity event handlers, creation of user interface components the
 *            initialization of internal data structures and configuring
 *            optional stack settings.
 */
HWND BMG_Initialize(HINSTANCE Inst, HWND hWndParent)
{
    U32         type, nameLen, len = 0;
    HKEY        hKey;
    BtStatus    status;

    if (BMG(state) != BMSTATE_IDLE) {
        /* Already initialized. */
        return BMG(chooserWnd);
    }
    OS_MemSet((U8 *)&BlueMgrGui, 0, sizeof(BlueMgrGui));

    BMG(hInstance) = Inst;

    /* Initialize the GUI components. */
    if (InitializeGui(hWndParent) == FALSE) {
        return 0;
    }
    BMG(state) = BMSTATE_INITIALIZED;

    /* Register Global Bluetooth stack event handler */
    ME_InitHandler(&BMG(globalHandler));
    BMG(globalHandler).callback = GlobalEventHandler;

    status = ME_RegisterGlobalHandler(&BMG(globalHandler));
    Assert(status == BT_STATUS_SUCCESS);

    ME_SetEventMask(&BMG(globalHandler), BEM_ALL_EVENTS);

#if BT_SECURITY == XA_ENABLED
    /* Initialize the security event handler callback. */
    BMG(securityHandler).callback = SecurityEventHandler;
#endif
    /* Register Bluetooth monitor event handler. */
    DS_RegisterMonitorHandler(&BMG(globalHandler));

    if (ME_GetStackInitState() == BTSS_INITIALIZED) {
        /* Stack is already up, call RadioStatusChange() to run radio
         * specific initialization code. 
         */
        RadioStatusChange(BTEVENT_HCI_INITIALIZED);
    }

    /* Set our promiscuous link policies */
    status = ME_SetDefaultLinkPolicy(BCC(setLinkPolicy_inACL), BCC(setLinkPolicy_outACL));
    Assert(status == BT_STATUS_SUCCESS);

    /* Set the local name. The Windows Computer name is first, then the default name is appended. */
    if (RegOpenKey(HKEY_LOCAL_MACHINE, TEXT("System\\CurrentControlSet\\Control\\ComputerName\\ComputerName"), &hKey) == ERROR_SUCCESS) {
        nameLen = BT_MAX_REM_DEV_NAME;

        if (RegQueryValueEx(hKey, TEXT("ComputerName"), NULL, &type, (U8 *)&BMG(deviceName), &nameLen) == ERROR_SUCCESS) {
            /* Have the computer name, now add the application name. */
#ifdef PME_APP_NAME
            if (nameLen > 0)
                BMG(deviceName)[nameLen - 1] = ' ';

            len = min((BT_MAX_REM_DEV_NAME - nameLen), sizeof(PME_APP_NAME));
            OS_MemCopy(BMG(deviceName) + nameLen, PME_APP_NAME, len); /* Includes null-terminator */
#endif
            status = ME_SetLocalDeviceName(BMG(deviceName), (U8)(nameLen + len));
            BmReport("ME_SetLocalDeviceName() to %s returned %s.\n", BMG(deviceName), pBT_Status(status));
        }
        RegCloseKey(hKey);
    }

    if (getRadioType() == RADIO_TYPE_ERICSSON_UART) {
        /* Older Ericssons do now allow role switch! So we must workaround this by
         * setting our connection role to MASTER for outbound connections and register
         * a Link Accept Handler to override the role on inbound connections.
         */
        ME_SetConnectionRole(BCR_MASTER);               /* Outbound */
        ME_RegisterAcceptHandler(&BMG(globalHandler));  /* Inbound */
    }

    Report(("BMG_Initialize() complete.\n"));
    return BMG(chooserWnd);
}


/*---------------------------------------------------------------------------
 *            BMG_Deinitialize()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Deinitialize the Bluetooth Device manager user interface
 *            component. The Bluetooth Device manager must be deinitialized
 *            after completing a device manager shutdown.
 */
BtStatus BMG_Deinitialize()
{
    if (BMG(activeQuery)) {
        DS_SelectDeviceResult(BMG(activeQuery), 0, BEC_POWER_OFF);
        BMG(activeQuery) = 0;
    }

    FTS_DisconnectFromLiveImport();

    /* Unregister handlers and disable security. */
    ME_UnregisterGlobalHandler(&BMG(globalHandler));
    DS_RegisterMonitorHandler(0);

#if BT_SECURITY == XA_ENABLED
    BMG_SetSecurityMode(BSM_SEC_LEVEL_1);
#endif

    BMG(state) = BMSTATE_IDLE;
    Report(("BMG_Deinitialize() complete.\n"));

    return BT_STATUS_SUCCESS;
}


/*---------------------------------------------------------------------------
 *            BMG_Shutdown()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Shutdown the device manager component. This function handles 
 *            asynchronous aspects of the device manager deinitialization.
 *            Once complete the device manager must be deinitialized.
 */
BtStatus BMG_Shutdown(void)
{
    if ((BMG(state) != BMSTATE_INITIALIZED) &&
        (BMG(state) != BMSTATE_RADIO_READY)) {
        return BT_STATUS_FAILED;
    }

    /* Tell the ME to shutdown radio, a BTEVENT_HCI_DEINITIALIZED completes
     * this request. Allow ME up to 1 second to shutdown.
     */
    BMG(state) = BMSTATE_SHUTTING_DOWN;
    return ME_RadioShutdown();
}


/*---------------------------------------------------------------------------
 *            BMG_QueryUserForBdAddr()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Prompts the user to enter a 48-bit Bluetooth device address.
 *            
 *            
 */
BOOL BMG_QueryUserForBdAddr(BD_ADDR *Addr)
{
    BOOL    rc;

    if (BMG(chooserWnd) == 0) {
        return FALSE;
    }

    /* Prompt dialog requesting user to enter a Bluetooth Device Address. */
    rc = DialogBoxParam(BMG(hInstance), MAKEINTRESOURCE(BDM_SPECIFY_BDADDR), 
                        BMG(chooserWnd), GetBdAddrDlgProc, (LPARAM)Addr);
    return rc;
}


/*---------------------------------------------------------------------------
 *            BMG_AddMainMenu()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Adds the Bluetooth device manager main menu to the applications
 *            user interface. This menu is used to access the device chooser
 *            and control panel dialogs. The BMG_TrackMainMenu function should
 *            be used to handle mouse events for the menu.
 */
BOOL BMG_AddMainMenu(HWND hWnd)
{
    HMENU           hMenu;
    RECT            rect;
    WINDOWPLACEMENT winPlace;
    MENUITEMINFO    mii;
    BOOL            FTS4BT_Usable;
    TCHAR           testLiveImportAPIDllPath[_MAX_PATH];
    TCHAR           testIniPath[1024];
    FILE            *filePtrDll;
    FILE            *filePtrIni;

    FTS4BT_Usable = FALSE;
    hMenu = LoadMenu(BMG(hInstance), MAKEINTRESOURCE(BDM_MENU));
    if (hMenu) {
        if (SetMenu(hWnd, hMenu) == TRUE) {
            /* Enlarge the window to contain the menu. */
            if (GetMenuItemRect(hWnd, hMenu, 0, &rect)) {
                GetWindowPlacement(hWnd, &winPlace);
                winPlace.showCmd = (IsWindowVisible(hWnd) ? SW_SHOW : SW_HIDE);
                winPlace.rcNormalPosition.bottom += (rect.bottom - rect.top);
                SetWindowPlacement(hWnd, &winPlace);
            }

            /* Disable menu items for excluded features. */
            mii.cbSize = sizeof(mii);
            mii.fMask = MIIM_STATE;
            mii.fState = MFS_DISABLED;

#ifndef DEMO
            /* Disable the demo window option on the menu */
            SetMenuItemInfo(hMenu, ID_VIEW_DEMO, FALSE, &mii);
#endif
#if XA_STATISTICS != XA_ENABLED
            /* Disable the statistics window option on the menu */
            SetMenuItemInfo(hMenu, ID_VIEW_STATS, FALSE, &mii);
#endif
#if XA_DEBUG != XA_ENABLED || HCI_ALLOW_PRESCAN != XA_ENABLED
            /* Disable the sniffer window option on the menu */
            SetMenuItemInfo(hMenu, ID_VIEW_SNIFFER, FALSE, &mii);
#endif
#if HCI_ALLOW_PRESCAN == XA_ENABLED
            if (FTS_FindFTS4BTDirectory(&testLiveImportAPIDllPath[0], 
                                        &testIniPath[0]             )){
                filePtrDll = fopen(testLiveImportAPIDllPath, "r");
                if (NULL != filePtrDll){
                    fclose(filePtrDll);
                    filePtrIni = fopen(testIniPath, "r");
                    if (NULL != filePtrIni){
                        fclose(filePtrIni);
                        FTS4BT_Usable = TRUE;
                    }
                }
            }
#endif
            if (!FTS4BT_Usable){
                /* Disable the FTS analyzer option on the menu */
                SetMenuItemInfo(hMenu, ID_ENBL_FTS, FALSE, &mii);
            }
            return TRUE;
        }
        DestroyMenu(hMenu);
    }
    return FALSE;
}


/*---------------------------------------------------------------------------
 *            BMG_TrackMainMenu()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  This function is used to pass messages to the Bluetooth device
 *            manager main menu for processing. The user applications window
 *            procedure should pass all unrecognized WM_COMMAND messages to
 *            this function. Messages destined for the main menu are processed
 *            before returning.
 */
BOOL BMG_TrackMainMenu(HWND hWnd, WPARAM wParam, LPARAM lParam)
{
    MENUITEMINFO mii;

    switch (LOWORD(wParam)) {
    case ID_PROP_ACCESSIBILITY:
        BMG_OpenProperties(BMG_PROPERTY_ACCESS);
        return TRUE;

    case ID_PROP_LINKPOLICY:
        BMG_OpenProperties(BMG_PROPERTY_LINKPOLICY);
        return TRUE;

    case ID_PROP_INTERVALS:
        BMG_OpenProperties(BMG_PROPERTY_INTERVALS);
        return TRUE;

    case ID_PROP_BLUEMGR:
        BMG_OpenProperties(BMG_PROPERTY_BLUEMGR);
        return TRUE;

    case ID_VIEW_SNIFFER:
        mii.cbSize = sizeof(mii);
        mii.fMask = MIIM_STATE;
        /* Preset expected 'fState' for case when menu doesn't exist. */
        mii.fState = (IsSnifferOpen() ? MFS_CHECKED : MFS_UNCHECKED);
        GetMenuItemInfo(GetMenu(hWnd), ID_VIEW_SNIFFER, FALSE, &mii);

        if (mii.fState == MFS_UNCHECKED) {
            if ((BMG(snifferWnd) = StartSniffer(BMG(hInstance), SW_SHOW)))
                mii.fState = MFS_CHECKED;
        } else {
            StopSniffer();
            mii.fState = MFS_UNCHECKED;
        }
        SetMenuItemInfo(GetMenu(hWnd), ID_VIEW_SNIFFER, FALSE, &mii);
        return TRUE;

    case ID_ENBL_FTS:
        mii.cbSize = sizeof(mii);
        mii.fMask = MIIM_STATE;
        GetMenuItemInfo(GetMenu(hWnd), ID_ENBL_FTS, FALSE, &mii);

        if (mii.fState == MFS_UNCHECKED) {

            /* Enable the FTS4BT Connection */

            if (!BMG(ftsActive)) {
                BMG(ftsActive) = FTS_ConnectToLiveImport();
            }
            if (BMG(ftsActive)) {

                /* If FTS4BT is active, give     */
                /* it a few seconds to connect.  */
                FTS_CheckLiveImportConnection();
            }

            /* Start sending data to the FTS4BT DLL by registering 
             * the prescan callback routines.  If the FTS4BT       
             * application is running, the DLL will pass the       
             * data on to it.  If the FTS4BT application is not    
             * running, the DLL will drop the data on the floor.   
             */
            FTS_StartLiveImport();

            /* Put a checkmark next to the menu item to show 
             * that data is being sent to the FTS4BT DLL.    
             */
            mii.fState = MFS_CHECKED;
            SetMenuItemInfo(GetMenu(hWnd), ID_ENBL_FTS, FALSE, &mii);

        } else {

            /* Disable the FTS4BT Connection */

            /* Stop sending data to the FTS4BT DLL by unregistering  
             * the prescan callback routines.  Then inform           
             * the FTS4BT application that it should                 
             * disconnect.  Lastly, free up the FTS4BT DLL library.  
             */
            FTS_DisconnectFromLiveImport();

            BMG(ftsActive) = FALSE;

            /* Remove the checkmark next to the menu item to show 
             * that data is not being sent to the FTS4BT DLL.     
             */
            mii.fState = MFS_UNCHECKED;
            SetMenuItemInfo(GetMenu(hWnd), ID_ENBL_FTS, FALSE, &mii);

        }

        return TRUE;

#ifdef DEMO
    case ID_VIEW_DEMO:
        mii.cbSize = sizeof(mii);
        mii.fMask = MIIM_STATE;
        GetMenuItemInfo(GetMenu(hWnd), ID_VIEW_DEMO, FALSE, &mii);

        if (mii.fState == MFS_UNCHECKED) {
            if (BMG(debugWnd) == 0) {
                BMG(debugWnd) = CreateDialog(BMG(hInstance), MAKEINTRESOURCE(DEBUG_OUTPUT), hWnd, DebugWndProc);

                if (BMG(debugWnd) == 0) {
                    mii.fState = MFS_DISABLED;
                    SetMenuItemInfo(GetMenu(hWnd), ID_VIEW_DEMO, FALSE, &mii);
                    break;
                }
            }
            ShowWindow(BMG(debugWnd), SW_SHOW);

            mii.fState = MFS_CHECKED;
            SetMenuItemInfo(GetMenu(hWnd), ID_VIEW_DEMO, FALSE, &mii);
        } else {
            mii.fState = MFS_UNCHECKED;
            SetMenuItemInfo(GetMenu(hWnd), ID_VIEW_DEMO, FALSE, &mii);

            ShowWindow(BMG(debugWnd), SW_HIDE);
        }
        return TRUE;
#endif

    case ID_VIEW_MANAGER:
        mii.cbSize = sizeof(mii);
        mii.fMask = MIIM_STATE;
        /* Preset expected 'fState' for case when menu doesn't exist. */
        mii.fState = (BMG(chooserOpen) ? MFS_CHECKED : MFS_UNCHECKED);
        GetMenuItemInfo(GetMenu(hWnd), ID_VIEW_MANAGER, FALSE, &mii);

        if (mii.fState == MFS_UNCHECKED) {
            BMG(chooserOpen) = TRUE;
            mii.fState = MFS_CHECKED;
        } else {
            BMG(chooserOpen) = FALSE;
            mii.fState = MFS_UNCHECKED;
        }
        SetMenuItemInfo(GetMenu(hWnd), ID_VIEW_MANAGER, FALSE, &mii);

        UpdateChooserStatus();
        return TRUE;

#if XA_STATISTICS == XA_ENABLED
    case ID_VIEW_STATS:
        mii.cbSize = sizeof(mii);
        mii.fMask = MIIM_STATE;
        GetMenuItemInfo(GetMenu(hWnd), ID_VIEW_STATS, FALSE, &mii);

        if (mii.fState == MFS_UNCHECKED) {
            if (BMG(statsWnd) == 0) {
                BMG(statsWnd) = CreateDialog(BMG(hInstance), MAKEINTRESOURCE(STATS_OUTPUT), hWnd, StatsWndProc);

                if (BMG(statsWnd) == 0) {
                    mii.fState = MFS_DISABLED;
                    SetMenuItemInfo(GetMenu(hWnd), ID_VIEW_STATS, FALSE, &mii);
                    break;
                }
            }
            ShowWindow(BMG(statsWnd), SW_SHOW);

            mii.fState = MFS_CHECKED;
            SetMenuItemInfo(GetMenu(hWnd), ID_VIEW_STATS, FALSE, &mii);
        } else {
            mii.fState = MFS_UNCHECKED;
            SetMenuItemInfo(GetMenu(hWnd), ID_VIEW_STATS, FALSE, &mii);

            ShowWindow(BMG(statsWnd), SW_HIDE);
        }
        return TRUE;
#endif

    case ID_HELP_ABOUT:
        DialogBox(BMG(hInstance), MAKEINTRESOURCE(BDM_ABOUT), GetParent(BMG(chooserWnd)), AboutDlgProc);
        return TRUE;
    }

    return FALSE;
}


/*---------------------------------------------------------------------------
 *            BMG_ShowChooser()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Show or hide the Bluetooth device chooser dialog. This dialog
 *            can be used to manage remote device options such as
 *            authentication and connection mode. It also displays debug and
 *            event status messages. The chooser cannot be manually opened to
 *            select a device. Device selection only occurs as part of a
 *            DS_SelectDevice function call which generates the event
 *            BTEVENT_SELECT_DEVICE_REQ to the GlobalEventHandler.
 */
void BMG_ShowChooser(BOOL Show)
{
    HMENU           hMenu = GetMenu(GetParent(BMG(chooserWnd)));
    MENUITEMINFO    mii;

    if (Show == TRUE) {
        BMG(chooserOpen) = TRUE;
        mii.fState = MFS_CHECKED;
    } else {
        BMG(chooserOpen) = FALSE;
        mii.fState = MFS_UNCHECKED;
    }

    if (hMenu) {
        mii.cbSize = sizeof(mii);
        mii.fMask = MIIM_STATE;

        SetMenuItemInfo(hMenu, ID_VIEW_MANAGER, FALSE, &mii);
    }
    UpdateChooserStatus();
}


#if BT_SECURITY == XA_ENABLED
/*---------------------------------------------------------------------------
 *            BMG_SetSecurityMode()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Set the security mode for the device. This includes the
 *            registration or deregistration of pairing and security handlers
 *            with the Management Entity. This command affects future
 *            connections only.
 */
BtStatus BMG_SetSecurityMode(BtSecurityMode SecMode)
{
    BtStatus    status;

    CheckParmExit(BT_STATUS_FAILED, BMG(state) != BMSTATE_IDLE);

    switch (SecMode) {
    case BSM_SEC_DISABLED:
        /* Remove Authorization and Pairing handlers. */
        SEC_RegisterPairingHandler(0);
        SEC_RegisterAuthorizeHandler(0);
        BmReport("Deregistered Pairing & Authorization handlers.\n");

        if (BMG(securityMode) < BSM_SEC_LEVEL_3) {
            /* Done downgrading security level. */
            BMG(securityMode) = BSM_SEC_LEVEL_1;
            status = BT_STATUS_SUCCESS;
            goto exit;
        }
        /* Drop down to disable mode 3 */

    case BSM_SEC_LEVEL_1:
    case BSM_SEC_LEVEL_2:
        status = SEC_DisableSecurityMode3(&BMG(securityHandler));
        BmReport("SEC_DisableSecurityMode3() returned %s.\n", pBT_Status(status));
        break;

    case BSM_SEC_LEVEL_3:
        status = SEC_EnableSecurityMode3(&BMG(securityHandler), FALSE);
        BmReport("SEC_EnableSecurityMode3() (without encryption) returned %s.\n", pBT_Status(status));
        break;

    case BSM_SEC_ENCRYPT:
        status = SEC_EnableSecurityMode3(&BMG(securityHandler), TRUE);
        BmReport("SEC_EnableSecurityMode3() (with encryption) returned %s.\n", pBT_Status(status));
        break;
    }

    if ((status == BT_STATUS_PENDING) || (status == BT_STATUS_SUCCESS)) {
        if (BMG(securityMode) < BSM_SEC_LEVEL_2) {
            /* Old security level didn't have handlers registered. 
             * Register our security event handlers now.
             */
            ME_InitHandler(&BMG(securityHandler));
            BMG(securityHandler).callback = SecurityEventHandler;

            SEC_RegisterPairingHandler(&BMG(securityHandler));
            SEC_RegisterAuthorizeHandler(&BMG(securityHandler));
        }
        BMG(securityMode) = SecMode;
    }

    exit:
    return status;
}
#endif /* BT_SECURITY == XA_ENABLED */


/*---------------------------------------------------------------------------
 *            BMG_AddToolTips()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Creates a tooltip control for the application by enumerating
 *            the child control windows, and registering the tool tip handler.
 */
BOOL BMG_AddToolTips(HWND hWnd, LPTSTR (*GetToolTipText)(int))
{
    if (BMG(toolTipWnd) == 0) {
        /* Bluetooth Manager GUI must be initialized first. */
        return FALSE;
    }

    /* Enumerate the child windows to register them with the tooltip control. */
    if (!EnumChildWindows(hWnd, AddTTipEnumProc, (LPARAM)GetToolTipText)) {
        return FALSE; 
    }
    return TRUE; 
} 


/*---------------------------------------------------------------------------
 *            BMG_OpenProperties()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Opens the Bluetooth property pages window.
 *
 */
HWND BMG_OpenProperties(BtPropertyPageId CurrTab)
{
    int     i;

    if (BMG(propertyWnd) == 0) {
        /* BlueMgrGui must be initialized first. */
        return 0;
    }

    if (IsWindowVisible(BMG(propertyWnd)) == FALSE) {
        /* Reinitialize all the dialogs with the current settings. */
        for (i = 0; Tabs[i].title; i++)
            SendMessage(Tabs[i].handle, WM_RELOAD, 0, 0);
    }

    TabCtrl_SetCurFocus(GetDlgItem(BMG(propertyWnd), PROP_TABS), CurrTab);
    ShowWindow(BMG(propertyWnd), SW_SHOW);
    ShowWindow(Tabs[CurrTab].handle, SW_SHOW);

    return BMG(propertyWnd);
}


/*---------------------------------------------------------------------------
 *            BMG_GetControlPanelSettings()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Returns a pointer to the Bluetooth control panel structure.
 *
 */
BtControlPanel *BMG_GetControlPanelSettings(void)
{
    return &BlueSdkControl;
}


/*****************************************************************************
 *                                                                           *
 *                   Internal Bluetooth Manager Functions                    *
 *                                                                           *
 *****************************************************************************/
/*---------------------------------------------------------------------------
 *            GlobalEventHandler()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  This function serves as the main stack event handler. It is
 *            responsible for updating the GUI based on the stack status.
 */
static void GlobalEventHandler(const BtEvent *Event)
{
    const char      *msg = "";
    const char      *name = "unknown";
    BtDeviceContext *bdc = 0;
    BtDeviceContext  newDevice;
    BtDeviceStatus   check;
    int              i;
    BtStatus         status;

    switch (Event->eType) {
    case BTEVENT_INQUIRY_RESULT:
        if (BMG(activeQuery)) {
            /* Recalculate the automated best choice since we may know more
             * about this device now (like the ClassOfDevice bits).
             */
            if (DS_EnumDevicesByQuality(&bdc, &BMG(activeQuery)->quality) == BT_STATUS_SUCCESS) {
                BMG(activeQuery)->result = bdc;
                InvalidateRect(BMG(deviceListWnd), 0, TRUE);
            }
        }
        bdc = DS_FindDevice(&Event->p.inqResult.bdAddr);
        break;

    case BTEVENT_INQUIRY_COMPLETE:
    case BTEVENT_INQUIRY_CANCELED:
        if (Event->errCode == BEC_NO_ERROR)
            BMG(lastInqTime) = OS_GetSystemTime();

        EnableWindow(GetDlgItem(BMG(chooserWnd), IDC_INQUIRE), TRUE);
        BMG(activity) &= ~BMG_INQUIRY_ACTIVE;
        RunNameQuery();
        UpdateStatusMsg();
        break;

    case BTEVENT_ACCESSIBLE_CHANGE:
        switch (Event->p.aMode) {
        case BAM_NOT_ACCESSIBLE: msg = "Not Accessible"; break;
        case BAM_GENERAL_ACCESSIBLE: msg = "General Accessible"; break;
        case BAM_LIMITED_ACCESSIBLE: msg = "Limited Accessible"; break;
        case BAM_CONNECTABLE_ONLY: msg = "Connectable Only"; break;
        default: msg = "Unknown"; break;
        }
        SendMessage(GetDlgItem(BMG(chooserWnd), IDC_ACCESSIBILITY), WM_SETTEXT, 0, (LPARAM)msg);
        break;

    case BTEVENT_HCI_FAILED:
    case BTEVENT_HCI_DEINITIALIZED:
        /* Return any outstanding select device requests. */
        if (BMG(activeQuery) != 0) {
            /* Call completion function with failure. */
            DS_SelectDeviceResult(BMG(activeQuery), 0, BEC_REQUEST_CANCELLED);
            BMG(activeQuery) = 0;
        }
        /* Drop into next case. */

    case BTEVENT_HCI_INITIALIZED:
    case BTEVENT_HCI_FATAL_ERROR:
    case BTEVENT_HCI_INIT_ERROR:
        RadioStatusChange(Event->eType);
        break; 

    case BTEVENT_HCI_COMMAND_SENT:
        if (Event->p.hciCmd.type == HCC_INQUIRY) {
            /* Force a full update get the correct status printed and clear any selections */
            BMG(activity) |= BMG_INQUIRY_ACTIVE;
            UpdateStatusMsg();

            /* Tag devices without a name for lookup. */
            if (BCC(inquiryMode) & BMG_DO_NAMEREQ) {
                while (DS_EnumDeviceList(&bdc) == BT_STATUS_SUCCESS) {
                    if ((EXT(bdc)->status & BDS_HAVE_NAME) == 0) {
                        /* If the name request failed last time skip it this time. */
                        if ((EXT(bdc)->status & BDS_NAME_FAILED) == 0)
                            EXT(bdc)->status |= BDS_NO_NAME;
                        else EXT(bdc)->status &= ~BDS_NAME_FAILED;
                    }
                }
            }

            InvalidateRect(BMG(deviceListWnd), 0, TRUE);
            RunNameQuery();
        } else if (Event->p.hciCmd.type == HCC_REM_NAME_REQ) {
            BMG(activity) |= BMG_NAMEREQ_ACTIVE;
            UpdateStatusMsg();
        } else if (Event->p.hciCmd.type == HCC_CREATE_CONNECTION) {
            /* This will cause a GUI status update. */
            bdc = DS_FindDevice((BD_ADDR *)Event->p.hciCmd.parms);
        }
        break;

    case BTEVENT_DEVICE_ADDED:
        /* Add new device and flag need name. */
        Assert(Event->p.device->context == 0);
        AddDevice(Event->p.device, 0);

        Event->p.device->psi.psRepMode = 1;

        if (BCC(inquiryMode) & BMG_DO_NAMEREQ)
            ((BtDeviceExt *)Event->p.device->context)->status |= BDS_NO_NAME;

        if (BMG(activeQuery)) {
            /* Recalculate the automated choice since we know of more devices now. */
            if (DS_EnumDevicesByQuality(&bdc, &BMG(activeQuery)->quality) == BT_STATUS_SUCCESS) {
                BMG(activeQuery)->result = bdc;
                InvalidateRect(BMG(deviceListWnd), 0, TRUE);
            }
        }
        RunNameQuery();
        /* Set BDC for updating below. */
        bdc = Event->p.device;
        break;

    case BTEVENT_DEVICE_DELETED:
        Assert(Event->p.device->context && ((BtDeviceExt *)Event->p.device->context)->inUse);
        RemoveDevice(Event->p.device);
        break;

    case BTEVENT_SELECT_DEVICE_REQ:
        /* The application has requested the chooser select a device */
        if (BMG(chooserWnd) == 0) {
            /* Call completion function with failure. */
            DS_SelectDeviceResult(Event->p.select, 0, BEC_UNSPECIFIED_ERR);
            break;
        }

        if (BMG(activeQuery) != 0) {
            /* Call completion function with failure. */
            DS_SelectDeviceResult(Event->p.select, 0, BEC_LIMITED_RESOURCE);
            break;
        }

        /* Initiate the request */
        BMG(activeQuery) = Event->p.select;
        if (((BMG(activity) & BMG_INQUIRY_ACTIVE) == 0) && (BCC(inquiryMode) & BMG_AUTO_REFRESH)) {
            if ((OS_GetSystemTime() - BMG(lastInqTime)) > MS_TO_TICKS(INQUIRY_REFERSH_INTERVAL)) {
                /* Run a new inquiry */
                PostMessage(BMG(chooserWnd), WM_COMMAND, IDC_INQUIRE, 0);
            }
        }
        /* Open the device chooser window */
        UpdateChooserStatus();
        break;

    case BTEVENT_ACL_DATA_NOT_ACTIVE:
        if (BCC(aclNotActiveAlert) == FALSE)
            break;

        /* In park, prompt user to restore active mode. */
        if (ME_GetCurrentMode(Event->p.remDev) == BLM_PARK_MODE) {
            i = MessageBox(BMG(chooserWnd), "An internal request to send data has been "
                           "received while the ACL connection\nis in park "
                           "mode. Should the device exit park mode?", 
                           "Data While Not Active", MB_YESNO|MB_ICONQUESTION);
            if (i == IDYES) {
                status = ME_StopPark(Event->p.remDev);
                BmReport("ME_StopPark() returned %s.", pBT_Status(status));
            }
        }
        /* Remove message for sniff mode. It is a problem in many apps.
         *
         * else if (ME_GetCurrentMode(Event->p.remDev) == BLM_SNIFF_MODE) {
         *   i = MessageBox(BMG(chooserWnd), "An internal request to send data has been "
         *                                   "received while the ACL connection\nis in sniff "
         *                                   "mode. Should the device exit sniff mode?", 
         *                                   "Data While Not Active", MB_YESNO|MB_ICONQUESTION);
         *   if (i == IDYES) {
         *       status = ME_StopSniff(Event->p.remDev);
         *       BmReport("ME_StopSniff() returned %s.", pBT_Status(status));
         *   }
         *}
         */
        break;

    case BTEVENT_LINK_CONNECT_REQ:
        /* This event is only registered for when the radio is an Ericsson. We
         * use the ME_AcceptIncomingLink() to override the connection role setting
         * as part of the workaround for radios that don't support role change.
         */
        status = ME_AcceptIncomingLink(Event->p.remDev, BCR_SLAVE);
        BmReport("ME_AcceptIncomingLink() returned %s.", pBT_Status(status));
        break;

    case BTEVENT_LINK_CONNECT_CNF:
    case BTEVENT_LINK_CONNECT_IND:
        /* Track of the number of active connections. */
        if (Event->errCode == BEC_NO_ERROR) {
            BMG(numConns)++;
            UpdateStatusMsg();
        }
        if (Event->p.remDev) {
            if ((bdc = DS_FindDevice(&Event->p.remDev->bdAddr)) == 0) {
                /* Device table must be full. Find one to delete. */
                OS_MemSet((U8 *)&newDevice, 0, sizeof(BtDeviceContext));

                check = (BDS_IN_RANGE|BDS_PAIRED|BDS_TRUSTED);
                for (i = 1; check; i++) {

                    while ((status = DS_EnumDeviceList(&bdc)) == BT_STATUS_SUCCESS) {
                        if ((bdc->state & check) == 0) {
                            /* This device is out of range and or unpaired. */
                            DS_DeleteDevice(&bdc->addr);
                            newDevice.addr = Event->p.remDev->bdAddr;
                            DS_AddDevice(&newDevice, &bdc);
                            check = 0;
                            break;
                        }
                    }
                    check = (check >> i) << i;
                }
            }
        }
        break;

    case BTEVENT_LINK_DISCONNECT:
        /* Track of the number of active connections. */
        BMG(numConns)--;
        UpdateStatusMsg();

        if (Event->p.remDev)
            bdc = DS_FindDevice(&Event->p.remDev->bdAddr);
        break;

#if 0
    case BTEVENT_CANCEL_SELECT_DEVICE:
        /* Simulate user clicking the cancel button to cancel the operation. */
        Assert(BMG(activeQuery));
        PostMessage(BMG(chooserWnd), WM_COMMAND, IDC_CANCEL, 0);
        break;
#endif
        /* Group: The following global events are caught so that we can lookup
         *        the BtDeviceContext and update the devices status.
         */
    case BTEVENT_LINK_CON_RESTRICT:
    case BTEVENT_AUTHENTICATED:
        if (Event->p.remDev)
            bdc = DS_FindDevice(&Event->p.remDev->bdAddr);
        break;

    case BTEVENT_SCO_DISCONNECT:
    case BTEVENT_SCO_CONNECT_CNF:
    case BTEVENT_SCO_CONNECT_IND:
        if (Event->p.scoConnect.remDev)
            bdc = DS_FindDevice(&Event->p.scoConnect.remDev->bdAddr);
        break;

    case BTEVENT_ENCRYPTION_CHANGE:
        bdc = DS_FindDevice(&Event->p.encrypt.remDev->bdAddr);
        break;

    case BTEVENT_ROLE_CHANGE:
        bdc = DS_FindDevice(&Event->p.roleChange.remDev->bdAddr);
        break;

    case BTEVENT_MODE_CHANGE:
        bdc = DS_FindDevice(&Event->p.modeChange.remDev->bdAddr);
        break;

    default:
        /* Unhandled events come here. */
        Assert((Event->eType == BTEVENT_SECURITY_CHANGE) || 
               (Event->eType == BTEVENT_LINK_ACCEPT_RSP));
        break;
    }

    /* Always update the devices status in the display. */
    if (bdc) {
        UpdateDeviceState(bdc);

        /* Retrieve index of this device for ListView function call. */
        for (i = 0; GetDeviceContextByIndex(i); i++) {
            if (bdc == GetDeviceContextByIndex(i)) {
                ListView_Update(BMG(deviceListWnd), i);
                break;
            }
        }
    }

    /* Write a message to the log window in the chooser dialog. */
    if (bdc) name = EXT(bdc)->name;

    switch (Event->eType) {
    case BTEVENT_LINK_CONNECT_CNF:
        if (Event->errCode == BEC_NO_ERROR)
            BmReport("Established Connection with %s.\n", name);
        else BmReport("Connection to %s failed, reason %s.\n", name, pHC_Status(Event->errCode));
        break;

    case BTEVENT_LINK_CONNECT_IND:
        if (Event->errCode == BEC_NO_ERROR)
            BmReport("Accepted Connection with %s.\n", name);
        else BmReport("Connection to %s failed, reason %s.\n", name, pHC_Status(Event->errCode));
        break;

    case BTEVENT_LINK_DISCONNECT:
        BmReport("Connection to %s was Disconnected, reason %s\n", name, pHC_Status(Event->errCode));
        break;

    case BTEVENT_ACCESSIBLE_CHANGE:
        /* 'Msg' was determined earlier. */
        BmReport("Accessible Mode change to %s completed.\n", msg);
        break;

    case BTEVENT_INQUIRY_RESULT:
        BmReport("Inquiry: Found device %s, ClassOfDevice %.8lX\n", name, Event->p.inqResult.classOfDevice);
        break;

    case BTEVENT_INQUIRY_COMPLETE:
    case BTEVENT_INQUIRY_CANCELED:
        BmReport("Inquiry: Completed with status %s.\n", pHC_Status(Event->errCode));
        break;

    case BTEVENT_MODE_CHANGE:
        if (Event->errCode != BEC_NO_ERROR)
            BmReport("Mode change for %s failed, reason %s.", name, pHC_Status(Event->errCode));

        switch (Event->p.modeChange.curMode) {
        case BLM_ACTIVE_MODE: msg = "Active"; break;
        case BLM_HOLD_MODE: msg = "Hold"; break;
        case BLM_SNIFF_MODE: msg = "Sniff"; break;
        case BLM_PARK_MODE: msg = "Park"; break;
        }
        BmReport("Mode change to %s for device %s complete.\n", msg, name);
        break;

    case BTEVENT_AUTHENTICATED:
        BmReport("Authentication for %s completed, status %s.\n", name, pHC_Status(Event->errCode));
        break;

    case BTEVENT_SECURITY_CHANGE:
        BmReport("Security Mode changed to level %d, %s encryption.\n", Event->p.secMode.mode,
                 (Event->p.secMode.encrypt ? "with" : "without"));
        break;

    case BTEVENT_ENCRYPTION_CHANGE:
        BmReport("Encryption change for %s, status %s.\n", name, pHC_Status(Event->errCode));
        break;

    case BTEVENT_ROLE_CHANGE:
        BmReport("Role change for %s, status %s.\n", name, pHC_Status(Event->errCode));
        break;
    }
}

#if BT_SECURITY == XA_ENABLED
/*---------------------------------------------------------------------------
 *            SecurityEventHandler()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handles the Bluetooth Stack Security events (from the 
 *            Management Entity).
 */
static void SecurityEventHandler(const BtEvent *Event)
{
    BtDeviceContext *bdc;
    BtStatus         status;

    switch (Event->eType) {
    case BTEVENT_PIN_REQ:
        AssertEval((bdc = DS_FindDevice(&Event->p.remDev->bdAddr)));
        Report(("PIN Request Received from %s.\n", EXT(bdc)->name));

        if (BCC(setPin_len) > 0) {
            /* We have a fixed pin, use it. */
            status = SEC_SetPin(Event->p.remDev, BCC(setPin_pin), 
                                BCC(setPin_len), BCC(setPin_type));
            BmReport("SEC_SetPin() [FIXED PIN] returned status %s.", pBT_Status(status));
        } else {
            if (Event->errCode == BT_STATUS_SUCCESS) {
                /* Prompt the user for a PIN code */
                PostMessage(BMG(chooserWnd), WM_PIN_REQ, 0, (LPARAM)bdc);
            } else {
                /* Cancel the PIN code request */
                EnterPinDlgProc(0, WM_PIN_CANCEL, 0, 0);
            }
        }
        break;

    case BTEVENT_AUTHORIZATION_REQ:
        AssertEval((bdc = DS_FindDevice(&Event->p.remDev->bdAddr)));
        if (Event->errCode == BT_STATUS_SUCCESS) {
            Report(("Authorization request received from %s.\n", EXT(bdc)->name));
            /* Check in with user to see if service request is authorized */
            PostMessage(BMG(chooserWnd), WM_AUTHORIZE_REQ, 0, (LPARAM)bdc);
        } else {
            Report(("Authorization request from %s cancelled.\n", EXT(bdc)->name));
            /* Cancel the AUTHORIZE request */
            EnterAuthorizeDlgProc(0, WM_AUTHORIZE_CANCEL, 0, 0);            
        }
        break;

    case BTEVENT_PAIRING_COMPLETE:
        AssertEval((bdc = DS_FindDevice(&Event->p.remDev->bdAddr)));
        BmReport("Pairing with %s complete, status %s.\n", EXT(bdc)->name, pHC_Status(Event->errCode));
        break;

    case BTEVENT_SECURITY3_COMPLETE:
        if (Event->errCode != 0) {
            /* Only need to report failure, success is reported to the Global Handler. */
            Report(("Secuirty Mode 3 failed. Status %s\n", pHC_Status(Event->errCode)));
        }
        break;

    case BTEVENT_AUTHENTICATE_CNF:
        if (Event->errCode != 0) {
            /* Only need to report failure, success is reported to the Global Handler. */
            BmReport("Authentication attempt failed. Reason %s.", pHC_Status(Event->errCode));
        }
        break;

    case BTEVENT_ENCRYPT_COMPLETE:
        if (Event->errCode != 0) {
            /* Only need to report failure, success is reported to the Global Handler. */
            BmReport("Encryption change attempt failed. Reason %s.", pHC_Status(Event->errCode));
        }
        break;

    }
}
#endif /* BT_SECURITY == XA_ENABLED */


/*
 * Window procedure for Device Chooser dialog
 */
static BOOL CALLBACK ChooserWndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    static BOOL         firstTime;
    BtSelectDeviceToken *temp;
    RECT                rect, rc1, rc2;
    PAINTSTRUCT         ps; 
    HDC                 hdc;
    BtStatus            status;
    BtDeviceContext    *bdc;
    WINDOWPLACEMENT     winPlace;
    char                msg[20];
    int                 adjust;
    HWND                hWndList;
#if BT_SECURITY == XA_ENABLED
    BtRemoteDevice     *remDev;
#endif /* BT_SECURITY == XA_ENABLED */

    switch (uMsg) {
    case WM_INITDIALOG:
        BMG(deviceListWnd) = GetDlgItem(hWnd, BDM_DEVICE_LIST);
        InitListView();
        /* Initially we're in the "More" view, force a switch to the compact view. */
        PostMessage(hWnd, WM_COMMAND, IDC_MORE, 0);
        firstTime = TRUE;
        break;

    case WM_SHOWWINDOW:
        if (firstTime && wParam) {
            /* The first time we open the chooser, center it over the parent. */
            firstTime = FALSE;
            GetWindowRect(hWnd, &rect);
            adjust = rect.right - rect.left;
            GetWindowRect(GetParent(hWnd), &rect);
            if ((rect.right - rect.left) > adjust)
                adjust = rect.right - rect.left - adjust;
            else adjust = 16;
            SetWindowPos(hWnd, 0, rect.left + (adjust/2), rect.top + 42, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
        }
        break;

    case WM_NOTIFY:
        OnNotify((NMHDR *)lParam);
        break;

    case WM_CONTEXTMENU:
        PopupMenu(wParam, lParam);
        break;

    case WM_ACTIVATE:
        /* Tell winmain() that the active window is changing. */
        if (LOWORD(wParam) == WA_INACTIVE)
            PostMessage(hWnd, WM_ACTIVE_WND, 0, lParam);
        else PostMessage(hWnd, WM_ACTIVE_WND, 0, (LPARAM)hWnd);
        break;

    case WM_PAINT: 
        hdc = BeginPaint(hWnd, &ps); 
        GetClientRect(GetDlgItem(hWnd, IDC_DIVIDER), &rect);
        rect.left += 3;
        DrawEdge(hdc, &rect, EDGE_ETCHED, BF_BOTTOM);
        EndPaint(hWnd, &ps); 
        break;

    case WM_COMMAND:
        switch (LOWORD(wParam)) {
        case IDC_MORE:
            /* Expand or compress the main view. */
            hWndList = GetDlgItem(BMG(chooserWnd), BDM_MONITOR);
            GetClientRect(hWndList, &rect);
            GetWindowRect(hWndList, &rc1);
            GetWindowRect(GetDlgItem(hWnd, IDC_DIVIDER), &rc2);
            rect.bottom += rc1.top - rc2.bottom;

            SendMessage(GetDlgItem(hWnd, IDC_MORE), WM_GETTEXT, 20, (LPARAM)msg);
            if (strcmp(msg, "More >>") == 0) {
                /* Expand window to show report log. */
                ShowWindow(hWndList, SW_SHOW);
                adjust = rect.bottom;
                SendMessage(GetDlgItem(hWnd, IDC_MORE), WM_SETTEXT, 0, (LPARAM)"<< Less");
            } else {
                /* Compact window by hiding the report log. */
                ShowWindow(hWndList, SW_HIDE);
                adjust = 0 - rect.bottom;
                SendMessage(GetDlgItem(hWnd, IDC_MORE), WM_SETTEXT, 0, (LPARAM)"More >>");
            }
            GetWindowPlacement(hWnd, &winPlace);
            winPlace.showCmd = (IsWindowVisible(hWnd) ? SW_SHOW : SW_HIDE);
            winPlace.rcNormalPosition.bottom += adjust;
            SetWindowPlacement(hWnd, &winPlace);

            GetWindowPlacement(GetDlgItem(hWnd, IDC_ACCESSIBILITY), &winPlace);
            winPlace.rcNormalPosition.top += adjust;
            winPlace.rcNormalPosition.bottom += adjust;
            SetWindowPlacement(GetDlgItem(hWnd, IDC_ACCESSIBILITY), &winPlace);

            GetWindowPlacement(GetDlgItem(hWnd, IDC_INQSTATE), &winPlace);
            winPlace.rcNormalPosition.top += adjust;
            winPlace.rcNormalPosition.bottom += adjust;
            SetWindowPlacement(GetDlgItem(hWnd, IDC_INQSTATE), &winPlace);

            GetWindowPlacement(GetDlgItem(hWnd, IDC_RADIO_STATUS), &winPlace);
            winPlace.rcNormalPosition.top += adjust;
            winPlace.rcNormalPosition.bottom += adjust;
            SetWindowPlacement(GetDlgItem(hWnd, IDC_RADIO_STATUS), &winPlace);

            GetWindowPlacement(GetDlgItem(hWnd, IDC_BDADDR), &winPlace);
            winPlace.rcNormalPosition.top += adjust;
            winPlace.rcNormalPosition.bottom += adjust;
            SetWindowPlacement(GetDlgItem(hWnd, IDC_BDADDR), &winPlace);
            break;

        case IDC_INQUIRE:
            if ((BMG(activity) & (BMG_INQUIRY_ACTIVE|BMG_NAMEREQ_ACTIVE)) == 0) {
                /* Start an inquiry (unlimited # of responses) */
                status = ME_Inquiry(BCC(inquiry_iac), BT_INQ_TIME_GAP100, 0);
                if (status == BT_STATUS_PENDING)
                    BMG(activity) |= BMG_INQUIRY_ACTIVE;

                BmReport("ME_Inquiry() returned %s.", pBT_Status(status));
            } else if (BMG(activity) & BMG_INQUIRY_ACTIVE) {
                /* Cancel the inquiry */
                status = ME_CancelInquiry();
                if (status == BT_STATUS_PENDING)
                    EnableWindow(GetDlgItem(hWnd, IDC_INQUIRE), FALSE);
                else if (status != BT_STATUS_IN_PROGRESS)
                    BMG(activity) &= ~BMG_INQUIRY_ACTIVE;

                BmReport("ME_CancelInquiry() returned %s.", pBT_Status(status));
            } else {
                /* Cancel the Name Lookup. The best we can do is stop future lookups. */
                bdc = 0;
                while (DS_EnumDeviceList(&bdc) == BT_STATUS_SUCCESS)
                    EXT(bdc)->status &= ~BDS_NO_NAME;
            }
            UpdateStatusMsg();
            break;

        case IDC_OK:
        case IDC_CANCEL:
            if ((temp = BMG(activeQuery)) != 0) {
                BMG(activeQuery) = 0;
                /* Was the "Ok" or "Cancel" button clicked. */
                if (LOWORD(wParam) == IDC_OK)
                    DS_SelectDeviceResult(temp, GetSelectedDeviceContext(), BEC_NO_ERROR);
                else DS_SelectDeviceResult(temp, 0, BEC_REQUEST_CANCELLED);

                EnableWindow(GetParent(BMG(chooserWnd)), TRUE);
            } else {
                /* "Close" button clicked. Deselect option on menu. */
                BMG_TrackMainMenu(GetParent(hWnd), ID_VIEW_MANAGER, 0);
            }

            /* To free up radio bandwidth (we may be about to page a device).
             * Cancel the active inquiry and name lookup processes.
             */
            if (BMG(activity) & BMG_INQUIRY_ACTIVE)
                SendMessage(BMG(chooserWnd), WM_COMMAND, IDC_INQUIRE, 0);

            for (bdc = 0; DS_EnumDeviceList(&bdc) == BT_STATUS_SUCCESS; )
                EXT(bdc)->status &= ~BDS_NO_NAME;

            UpdateChooserStatus();
            break;

        case WM_DESTROY:    
            /* Sent by RadioStatusChange() when an async radio shutdown is complete. */
            if (lParam == BMSTATE_SHUTTING_DOWN)
                PostQuitMessage(0);
            break;
        }
        break;

#if BT_SECURITY == XA_ENABLED
    case WM_PIN_REQ: {
            PinCodeReq  parms;
            BtDeviceContext *bdc = (BtDeviceContext *)lParam;

            OS_MemSet((U8 *)&parms, 0, sizeof(parms));
            parms.DeviceDesc = EXT(bdc)->name;
            remDev = ME_FindRemoteDevice(&bdc->addr);

            /* Using IsWindowVisible() helps focus return to the right window. */
            if (DialogBoxParam(BMG(hInstance), MAKEINTRESOURCE(BDM_ENTER_PIN), 
                               IsWindowVisible(hWnd) ? hWnd : GetParent(hWnd),
                               EnterPinDlgProc, (LPARAM)&parms) == TRUE) {

                /* User entered Pin code */
                status = SEC_SetPin(remDev, parms.Pin, parms.PinLen, parms.Flags);
            } else {
                /* Fail Pin code request */
                status = SEC_SetPin(remDev, 0, 0, 0);
            }
            BmReport("SEC_SetPin() returned status %s.", pBT_Status(status));
        }
        break;

    case WM_AUTHORIZE_REQ: {
            AuthorizeReq  parms;
            BtDeviceContext *bdc = (BtDeviceContext *)lParam;

            OS_MemSet((U8 *)&parms, 0, sizeof(parms));
            parms.deviceDesc = EXT(bdc)->name;
            remDev = ME_FindRemoteDevice(&bdc->addr);

            /* Using IsWindowVisible() helps focus return to the right window. */
            if (DialogBoxParam(BMG(hInstance), MAKEINTRESOURCE(BDM_AUTHORIZE), 
                               IsWindowVisible(hWnd) ? hWnd : GetParent(hWnd),
                               EnterAuthorizeDlgProc, (LPARAM)&parms) == TRUE) {

                /* User accepted authorization */
                switch (parms.level) {
                case 1: // This time
                    status = SEC_AuthorizeService(remDev, TRUE);
                    break;
                case 2: // This ACL
                    status = SEC_Authorize(remDev, TRUE, FALSE);
                    break;
                case 3: // Future
                    status = SEC_Authorize(remDev, TRUE, TRUE);
                    break;
                default:
                    Assert(0);
                    status = SEC_Authorize(remDev, 0, 0);
                    break;
                }
            } else {
                /* Fail Authorize request */
                status = SEC_Authorize(remDev, 0, 0);
            }
            BmReport("SEC_Authorize[Service]() returned status %s.", pBT_Status(status));
        }
        break;
#endif
    }

    return 0;
}


/*
 * Initialize the main GUI components: Chooser, Property pages & Tool tip windows.
 */
static BOOL InitializeGui(HWND hWndParent)
{
    INITCOMMONCONTROLSEX icc;
    RECT                 rect, parent;
    U32                  i;

    icc.dwSize = sizeof(icc);
    icc.dwICC = (ICC_BAR_CLASSES|ICC_LISTVIEW_CLASSES|ICC_PROGRESS_CLASS|ICC_TAB_CLASSES);
    if (InitCommonControlsEx(&icc) == FALSE) {
        return FALSE;
    }

    BMG(chooserWnd) = CreateDialog(BMG(hInstance), MAKEINTRESOURCE(BDM_MAINDETAILS), hWndParent, ChooserWndProc);
    if (BMG(chooserWnd) == 0) {
        return FALSE;
    }

    BMG(propertyWnd) = CreateDialog(BMG(hInstance), MAKEINTRESOURCE(BDM_PROPPAGE), hWndParent, PropertyWndProc);
    if (BMG(propertyWnd) == 0) {
        DestroyWindow(BMG(chooserWnd));
        BMG(chooserWnd) = 0;
        return FALSE;
    }

    /* Create and position the property page dialogs. */
    GetWindowRect(BMG(propertyWnd), &parent);
    GetWindowRect(GetDlgItem(BMG(propertyWnd), PROP_TABS), &rect);

    i = 0;
    rect.left -= parent.left;
    rect.top -= parent.top;
    while (Tabs[i].title) {
        Tabs[i].handle = CreateDialog(BMG(hInstance), Tabs[i].tmplt, BMG(propertyWnd), Tabs[i].dlgProc);
        if (Tabs[i].handle == 0) {
            DestroyWindow(BMG(propertyWnd));
            DestroyWindow(BMG(chooserWnd));
            BMG(propertyWnd) = BMG(chooserWnd) = 0;
            return FALSE;
        }
        SetWindowPos(Tabs[i].handle, HWND_TOP, rect.left, rect.top, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
        i++;
    }

    /* Create the Tool Tip window and add the Blue Manager chooser window controls */
    BMG(toolTipWnd) = CreateWindowEx(WS_EX_TOPMOST, TOOLTIPS_CLASS, 0, TTS_ALWAYSTIP, CW_USEDEFAULT,
                                     CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,
                                     BMG(chooserWnd), 0, BMG(hInstance), 0); 
    if (BMG(toolTipWnd) != 0) {
        /* Add tool tip support for chooser and property pages. */
        EnumChildWindows(BMG(chooserWnd), AddTTipEnumProc, 0);
        EnumChildWindows(BMG(propertyWnd), AddTTipEnumProc, 0);
    }

    UpdateStatusMsg();
    SendMessage(GetDlgItem(BMG(chooserWnd), IDC_ACCESSIBILITY), WM_SETTEXT, 0, (LPARAM)"");
    SendMessage(GetDlgItem(BMG(chooserWnd), IDC_BDADDR), WM_SETTEXT, 0, (LPARAM)"");

#if XA_DEBUG == XA_ENABLED && HCI_ALLOW_PRESCAN == XA_ENABLED
    /* Command line request to open sniffer at startup. */
    if (getSnifferOption())
        BMG_TrackMainMenu(GetParent(BMG(chooserWnd)), ID_VIEW_SNIFFER, 0);
#endif

    return TRUE;
}


/*
 * Initialize the device list view columns 
 */
static void InitListView(void)
{
    LVCOLUMN        lvColumn;
    HICON           hIcon;
    int             cx, cy, width;
    RECT            rect;

    GetWindowRect(BMG(deviceListWnd), &rect);
    width = ((rect.right - rect.left) - (GetSystemMetrics(SM_CXBORDER)*2)) / 5;

    /* Initialize the columns */
    lvColumn.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT;
    lvColumn.fmt = LVCFMT_LEFT;

    /* Deduct the border & scroll width from the "Address" column since it's the smallest. */
    lvColumn.cx = (width * 2) - (GetSystemMetrics(SM_CXBORDER)*2) - GetSystemMetrics(SM_CXVSCROLL);
    lvColumn.pszText = "Name or Address";
    ListView_InsertColumn(BMG(deviceListWnd), 0, &lvColumn);

    lvColumn.cx = width;
    lvColumn.pszText = "Status";
    ListView_InsertColumn(BMG(deviceListWnd), 1, &lvColumn);

    lvColumn.cx = (width * 2);
    lvColumn.pszText = "Device Class: Services";
    ListView_InsertColumn(BMG(deviceListWnd), 2, &lvColumn);

    cx = GetSystemMetrics(SM_CXSMICON);
    cy = GetSystemMetrics(SM_CYSMICON);

    /* Load the small icon image list */
    BMG(smallIcons) = ImageList_Create(cx, cy, ILC_COLORDDB | ILC_MASK, 6, 0);
    if (BMG(smallIcons)) {
        /* Add the device icons */
        hIcon = (HICON)LoadImage(BMG(hInstance), MAKEINTRESOURCE(PME_DEVICEOUT), IMAGE_ICON, cx, cy, LR_DEFAULTCOLOR|LR_LOADTRANSPARENT);
        ImageList_AddIcon(BMG(smallIcons), hIcon);
        hIcon = (HICON)LoadImage(BMG(hInstance), MAKEINTRESOURCE(PME_DEVICEINRANGE), IMAGE_ICON, cx, cy, LR_DEFAULTCOLOR|LR_LOADTRANSPARENT);
        ImageList_AddIcon(BMG(smallIcons), hIcon);
        hIcon = (HICON)LoadImage(BMG(hInstance), MAKEINTRESOURCE(PME_DEVICECONNECTED), IMAGE_ICON, cx, cy, LR_DEFAULTCOLOR|LR_LOADTRANSPARENT);
        ImageList_AddIcon(BMG(smallIcons), hIcon);
        hIcon = (HICON)LoadImage(BMG(hInstance), MAKEINTRESOURCE(PME_DEVICEINPICONET), IMAGE_ICON, cx, cy, LR_DEFAULTCOLOR|LR_LOADTRANSPARENT);
        ImageList_AddIcon(BMG(smallIcons), hIcon);
        hIcon = (HICON)LoadImage(BMG(hInstance), MAKEINTRESOURCE(PME_DEVICEPREFERRED), IMAGE_ICON, cx, cy, LR_DEFAULTCOLOR|LR_LOADTRANSPARENT);
        ImageList_AddIcon(BMG(smallIcons), hIcon);
        hIcon = (HICON)LoadImage(BMG(hInstance), MAKEINTRESOURCE(PME_DEVICEPREFERREDOUT), IMAGE_ICON, cx, cy, LR_DEFAULTCOLOR|LR_LOADTRANSPARENT);
        ImageList_AddIcon(BMG(smallIcons), hIcon);
        ListView_SetImageList(BMG(deviceListWnd), BMG(smallIcons), LVSIL_SMALL);
    }
}

/*
 * Process WM_NOTIFY messages for device list
 */
static void OnNotify(NMHDR *Nmh)
{
    NMLVDISPINFO       *lpdi;
    NMCLICK            *clk;
    BtDeviceContext    *bdc;
    BtDeviceExt        *ext;
    int                 i, len, addLen;

    /* DO NOT CALL Stack functions from this function! */
    switch (Nmh->code) {
    case LVN_GETDISPINFO:
        lpdi = (NMLVDISPINFO*)Nmh;
        bdc = (BtDeviceContext *)lpdi->item.lParam;
        ext = (BtDeviceExt *)bdc->context;

        /* What is the sub-item (column) of the information being requested? */
        switch (lpdi->item.iSubItem) {
        case 0:         /* Col 0: Device Name / Address */
            if (lpdi->item.mask & LVIF_TEXT) {
                Assert(lpdi->item.cchTextMax > (int)strlen(ext->name));
                strcpy(lpdi->item.pszText, ext->name);
            }
            if (lpdi->item.mask & LVIF_IMAGE) {
                if (ext->state == BDS_CONNECTED) {
                    if (ext->mode != BLM_ACTIVE_MODE)
                        lpdi->item.iImage = ICON_DEVICE_INPICONET;
                    else lpdi->item.iImage = ICON_DEVICE_CONNECTED;
                } else if (BMG(activeQuery) && BMG(activeQuery)->result == bdc) {
                    if (bdc->state & BDS_IN_RANGE)
                        lpdi->item.iImage = ICON_DEVICE_PREFERRED;
                    else lpdi->item.iImage = ICON_DEVICE_PREFERREDOUT;
                } else if (bdc->state & BDS_IN_RANGE)
                    lpdi->item.iImage = ICON_DEVICE_INRANGE;
                else lpdi->item.iImage = ICON_DEVICE_OUT;
            }
            break;

        case 1:         /* Col 1: Device Status */
            if (lpdi->item.mask & LVIF_TEXT) {
                Assert(lpdi->item.cchTextMax > 24);

                if (bdc->state & BDS_IN_RANGE)
                    strcpy(lpdi->item.pszText, "In Range");

                if (ext->state == BDS_CONNECTED) {
                    if (ext->role == BCR_MASTER)
                        strcpy(lpdi->item.pszText, "Master");
                    else strcpy(lpdi->item.pszText, "Slave");

                    if (BMG(browseDev) && (bdc->link == BMG(browseDev)))
                        strcat(lpdi->item.pszText, ", Browsing");

                    if (ext->mode == BLM_PARK_MODE) {
                        strcat(lpdi->item.pszText, ", Parked");
                    } else if (ext->mode == BLM_HOLD_MODE) {
                        strcat(lpdi->item.pszText, ", Holding");
                    } else if (ext->mode == BLM_SNIFF_MODE) {
                        strcat(lpdi->item.pszText, ", Sniffing");
                    }
                    if (ext->auth == BAS_AUTHENTICATED)
                        strcat(lpdi->item.pszText, ", Authenticated");

                    if (ext->encrypt == BES_ENCRYPTED)
                        strcat(lpdi->item.pszText, ", Encrypted");
                } else if (ext->state == BDS_OUT_CON || ext->state == BDS_IN_CON)
                    strcpy(lpdi->item.pszText, "Connecting");

                if (bdc->state & BDS_PAIRED) {
                    if (strlen(lpdi->item.pszText))
                        strcat(lpdi->item.pszText, ", ");

                    strcat(lpdi->item.pszText, "Paired");

                    if (bdc->state & BDS_TRUSTED)
                        strcat(lpdi->item.pszText, ", Trusted");
                }
            }
            break;

        case 2:         /* Col 2: Known Services */
            if (lpdi->item.mask & LVIF_TEXT) {
                len = addLen = 0;

                /* Services string starts with the Major Class of Device */
                if ((len = strlen(pCodMajorClass(bdc->classOfDevice))) > 0) {
                    strcpy(lpdi->item.pszText, pCodMajorClass(bdc->classOfDevice));
                    strcat(lpdi->item.pszText, ": ");
                    len += 2;
                } else lpdi->item.pszText[0] = 0;

                /* Now append known services to the list. */
#if DS_NUM_SERVICES > 0
                for (i = 0; i < DS_NUM_SERVICES; i++) {
                    if (bdc->services[i]) {
                        addLen = strlen(pSdpService(bdc->services[i]));
                        if (len > 0 && (lpdi->item.pszText[len-1] != ' ')) {
                            addLen += 2;
                            if ((len + addLen) > lpdi->item.cchTextMax)
                                break;

                            strcat(lpdi->item.pszText, ", ");
                        }
                        strcat(lpdi->item.pszText, pSdpService(bdc->services[i]));
                        len += addLen;
                    }
                }
#endif

                if (len > 0 && (lpdi->item.pszText[len-1] != ' ')) {
                    strcat(lpdi->item.pszText, "  ");
                    len += 2;
                }

                /* Finally, append service class bits from the ClassOfDevice. */
                if (BtCod2ServiceString(bdc->classOfDevice, lpdi->item.pszText + len, 
                                        (U16)(lpdi->item.cchTextMax - len)))
                    if (len > 1 && (lpdi->item.pszText[len-2] == ' '))
                        lpdi->item.pszText[len-2] = ',';
            }
            break;
        }
        break;

    case NM_CLICK:
        /* If you select a device we have to update the buttons */
        clk = (NMCLICK *)Nmh;
        break;

    case NM_RCLICK:
        /* If you right click on a device we pop up a menu */
        clk = (NMCLICK *)Nmh;
        BMG(menuDevice) = GetDeviceContextByIndex(clk->dwItemSpec);
        break;

    case NM_DBLCLK:
        /* If you double click on a device we treat it like selecting a
         * device and clicking "Ok".
         */
        if (BMG(activeQuery))
            SendMessage(BMG(chooserWnd), WM_COMMAND, IDC_OK, 0);
        break;

    case TTN_GETDISPINFO:
        OnToolTip(Nmh);
        break;
    }
}


/*
 * Builds and pops up the context (right click) device menu.
 */
static void PopupMenu(WPARAM wParam, LPARAM lParam)
{
    static HMENU     hMenu;
    BOOL             selection;
#if BT_SECURITY == XA_ENABLED
    U32              i;
#endif
    MENUITEMINFO     mii = {0};
    UINT             pos = 0;
    U16              options = 0;
    BtStatus         status;
    BtDeviceContext *bdc = 0;
    BtDeviceContext  newDevice;
    char             addrStr[BDADDR_NTOA_SIZE];
    U8               num;

    if ((HWND)wParam != BMG(deviceListWnd)) {
        return;
    }

    if ((hMenu = CreatePopupMenu()) == 0) {
        return;
    }

    if (BMG(menuDevice)) {
        options = GetDeviceOptions(BMG(menuDevice));

        mii.cbSize = sizeof(mii);
        mii.fMask = MIIM_TYPE | MIIM_ID | MIIM_STATE;
        mii.fType = MFT_STRING;

        mii.fState = MF_DISABLED;   /* Disabled but not greyed */
        mii.dwTypeData = bdaddr_ntoa(&BMG(menuDevice)->addr, addrStr);
        mii.wID = 0;
        InsertMenuItem(hMenu, pos, TRUE, &mii);
        pos++;

        mii.fType = MFT_SEPARATOR;
        mii.fState = MFS_ENABLED;
        mii.wID = 0;
        InsertMenuItem(hMenu, pos, TRUE, &mii);
        pos++;

#if BT_SECURITY == XA_ENABLED
        mii.fType = MFT_STRING;
        mii.fState = (options & PMO_AUTHENTICATE ? MFS_ENABLED : MFS_DISABLED);
        mii.dwTypeData = "Authenticate";
        mii.wID = PMO_AUTHENTICATE;
        InsertMenuItem(hMenu, pos, TRUE, &mii);
        pos++;

        mii.fMask = MIIM_TYPE | MIIM_ID | MIIM_STATE | MIIM_CHECKMARKS;
        mii.fState = (options & (PMO_ENCRYPTION_ON|PMO_ENCRYPTION_OFF) ? MFS_ENABLED : MFS_DISABLED);
        mii.fState |= (options & PMO_ENCRYPTION_OFF ? MFS_CHECKED : MFS_UNCHECKED);
        mii.dwTypeData = "Encryption";
        mii.wID = PMO_ENCRYPTION_ON;
        InsertMenuItem(hMenu, pos, TRUE, &mii);
        pos++;

        mii.fMask = MIIM_TYPE | MIIM_ID | MIIM_STATE;
        mii.fState = (options & PMO_DELETE_KEY ? MFS_ENABLED : MFS_DISABLED);
        mii.dwTypeData = "Delete Link Key";
        mii.wID = PMO_DELETE_KEY;
        InsertMenuItem(hMenu, pos, TRUE, &mii);
        pos++;

        mii.fMask = MIIM_TYPE | MIIM_ID | MIIM_STATE;
        mii.fType = MFT_SEPARATOR;
        mii.fState = MFS_ENABLED;
        mii.wID = 0;
        InsertMenuItem(hMenu, pos, TRUE, &mii);
        pos++;
#endif /* BT_SECURITY == XA_ENABLED */

#if SDP_PARSING_FUNCS == XA_ENABLED
        mii.fMask = MIIM_TYPE | MIIM_ID | MIIM_STATE | MIIM_CHECKMARKS;
        mii.fType = MFT_STRING;
        mii.fState = (BMG(browseDev) == 0 ? MFS_ENABLED : MFS_DISABLED);
        mii.dwTypeData = "Browse Public Services";
        mii.wID = PMM_BROWSE;
        InsertMenuItem(hMenu, pos, TRUE, &mii);
        pos++;

        mii.fType = MFT_SEPARATOR;
        mii.fState = MFS_ENABLED;
        mii.wID = 0;
        InsertMenuItem(hMenu, pos, TRUE, &mii);
        pos++;
#endif /* SDP_PARSING_FUNCS == XA_ENABLED */

        mii.fMask = MIIM_TYPE | MIIM_ID | MIIM_STATE | MIIM_CHECKMARKS;
        mii.fType = MFT_STRING;
        mii.fState = (options & (PMO_PARK|PMO_UNPARK) ? MFS_ENABLED : MFS_DISABLED);
        mii.fState |= (options & (PMO_UNPARK) ? MFS_CHECKED : MFS_UNCHECKED);
        mii.dwTypeData = "Park";
        mii.wID = PMO_PARK;
        InsertMenuItem(hMenu, pos, TRUE, &mii);
        pos++;

        mii.fMask = MIIM_TYPE | MIIM_ID | MIIM_STATE;
        mii.fState = (options & PMO_HOLD ? MFS_ENABLED : MFS_DISABLED);
        mii.dwTypeData = "Hold";
        mii.wID = PMO_HOLD;
        InsertMenuItem(hMenu, pos, TRUE, &mii);
        pos++;

        mii.fMask = MIIM_TYPE | MIIM_ID | MIIM_STATE | MIIM_CHECKMARKS;
        mii.fType = MFT_STRING;
        mii.fState = (options & (PMO_SNIFF|PMO_UNSNIFF) ? MFS_ENABLED : MFS_DISABLED);
        mii.fState |= (options & (PMO_UNSNIFF) ? MFS_CHECKED : MFS_UNCHECKED);
        mii.dwTypeData = "Sniff";
        mii.wID = PMO_SNIFF;
        InsertMenuItem(hMenu, pos, TRUE, &mii);
        pos++;

        mii.fMask = MIIM_TYPE | MIIM_ID | MIIM_STATE;
        mii.fType = MFT_SEPARATOR;
        mii.fState = MFS_ENABLED;
        mii.wID = 0;
        InsertMenuItem(hMenu, pos, TRUE, &mii);
        pos++;

        mii.fMask = MIIM_TYPE | MIIM_ID | MIIM_STATE;
        mii.fType = MFT_STRING;
        mii.fState = (options & PMO_SWITCH_ROLE ? MFS_ENABLED : MFS_DISABLED);
        if (options & PMO_SWITCH_ROLE)
            if (((BtDeviceExt *)BMG(menuDevice)->context)->role == BCR_MASTER)
                mii.dwTypeData = "Switch Role (to Slave)";
            else mii.dwTypeData = "Switch Role (to Master)";
        else mii.dwTypeData = "Switch Role";
        mii.wID = PMO_SWITCH_ROLE;
        InsertMenuItem(hMenu, pos, TRUE, &mii);
        pos++;

    } else {

        mii.cbSize = sizeof(mii);

        mii.fMask = MIIM_TYPE | MIIM_ID | MIIM_STATE;
        mii.fType = MFT_STRING;
        mii.fState = MFS_ENABLED;
        mii.dwTypeData = "Add Device to List";
        mii.wID = PMM_ADD_DEVICE;
        InsertMenuItem(hMenu, pos, TRUE, &mii);
        pos++;

        mii.fState = MFS_ENABLED;
        mii.fMask = MIIM_TYPE | MIIM_ID | MIIM_STATE;
        mii.fType = MFT_SEPARATOR;
        mii.fState = MFS_ENABLED;
        mii.wID = 0;
        InsertMenuItem(hMenu, pos, TRUE, &mii);
        pos++;

        mii.fMask = MIIM_TYPE | MIIM_ID | MIIM_STATE;
        mii.fType = MFT_STRING;
        mii.dwTypeData = "Empty List";
        mii.wID = PMM_EMPTY_LIST;
        InsertMenuItem(hMenu, pos, TRUE, &mii);
        pos++;

        mii.fState = MFS_ENABLED;
        mii.fMask = MIIM_TYPE | MIIM_ID | MIIM_STATE;
        mii.fType = MFT_SEPARATOR;
        mii.fState = MFS_ENABLED;
        mii.wID = 0;
        InsertMenuItem(hMenu, pos, TRUE, &mii);
        pos++;

        options = 0;
        for (bdc = 0; DS_EnumDeviceList(&bdc) == BT_STATUS_SUCCESS; ) {
            if (bdc->link) {
                options = (PMO_PARK_ALL);
                break;
            }
        }

        mii.fMask = MIIM_TYPE | MIIM_ID | MIIM_STATE | MIIM_CHECKMARKS;
        mii.fType = MFT_STRING;
        mii.fState = (options & PMO_PARK_ALL ? MFS_ENABLED : MFS_DISABLED);
        mii.dwTypeData = "Park All Devices";
        mii.wID = PMO_PARK_ALL;
        InsertMenuItem(hMenu, pos, TRUE, &mii);
        pos++;
    }

    selection = TrackPopupMenuEx(hMenu, TPM_LEFTALIGN|TPM_RETURNCMD, LOWORD(lParam),
                                 HIWORD(lParam), (HWND)wParam, NULL);
    switch (selection) {
#if BT_SECURITY == XA_ENABLED
    case PMO_AUTHENTICATE:
        status = SEC_AuthenticateLink(&BMG(securityHandler), BMG(menuDevice)->link);
        BmReport("SEC_AuthenticateLink() returned %s.\n", pBT_Status(status));
        if (status == BT_STATUS_SUCCESS)
            goto UpdateDeviceStatus;
        break;

    case PMO_ENCRYPTION_ON:
        Assert(BMG(menuDevice));
        mii.fMask = MIIM_STATE;
        GetMenuItemInfo(hMenu, PMO_ENCRYPTION_ON, FALSE, &mii);
        if (mii.fState & MFS_CHECKED)
            status = SEC_SetLinkEncryption(&BMG(securityHandler), BMG(menuDevice)->link, BECM_ENCRYPT_DISABLE);
        else status = SEC_SetLinkEncryption(&BMG(securityHandler), BMG(menuDevice)->link, BECM_ENCRYPT_ENABLE);
        BmReport("SEC_SetLinkEncryption() returned %s.\n", pBT_Status(status));
        break;

    case PMO_DELETE_KEY:
        Assert(BMG(menuDevice));
        SEC_DeleteDeviceRecord(&BMG(menuDevice)->addr);

        UpdateDeviceStatus:
        /* Update status in chooser */
        for (i = 0; GetDeviceContextByIndex(i); i++) {
            if (BMG(menuDevice) == GetDeviceContextByIndex(i)) {
                ListView_Update(BMG(deviceListWnd), i);
                break;
            }
        }
        break;
#endif /* BT_SECURITY */

    case PMO_HOLD:
        Assert(BMG(menuDevice) && BMG(menuDevice)->link);

        status = ME_Hold(BMG(menuDevice)->link, BCC(hold_max), BCC(hold_min));
        BmReport("ME_Hold() for %s returned %s.\n", EXT(BMG(menuDevice))->name, pBT_Status(status));
        break;

    case PMO_PARK:
        Assert(BMG(menuDevice) && BMG(menuDevice)->link);

        mii.fMask = MIIM_STATE;
        GetMenuItemInfo(hMenu, PMO_PARK, FALSE, &mii);

        if (mii.fState & MFS_CHECKED) {
            /* Device is Parked, unpark it */
            status = ME_StopPark(BMG(menuDevice)->link);
            BmReport("ME_StopPark() for %s returned %s.\n", EXT(BMG(menuDevice))->name, pBT_Status(status));
        } else {
            status = ME_StartPark(BMG(menuDevice)->link, BCC(startPark_max), BCC(startPark_min));
            BmReport("ME_StartPark() for %s returned %s.\n", EXT(BMG(menuDevice))->name, pBT_Status(status));
        }
        break;

    case PMO_SNIFF:
        Assert(BMG(menuDevice) && BMG(menuDevice)->link);

        mii.fMask = MIIM_STATE;
        GetMenuItemInfo(hMenu, PMO_SNIFF, FALSE, &mii);
        if (mii.fState & MFS_CHECKED) {
            /* Stop sniff mode */
            status = ME_StopSniff(BMG(menuDevice)->link);
            BmReport("ME_StopSniff() for %s returned %s.\n", EXT(BMG(menuDevice))->name, pBT_Status(status));
        } else {
            /* Start sniff mode */
            status = ME_StartSniff(BMG(menuDevice)->link, &BCC(startSniff_info));
            BmReport("ME_StartSniff() for %s returned %s.\n", EXT(BMG(menuDevice))->name, pBT_Status(status));
        }
        break;

    case PMO_SWITCH_ROLE:
        status = ME_SwitchRole(BMG(menuDevice)->link);
        BmReport("ME_SwitchRole() for %s returned %s.\n", EXT(BMG(menuDevice))->name, pBT_Status(status));
        break;

    case PMM_ADD_DEVICE:
        OS_MemSet((U8 *)&newDevice, 0, sizeof(BtDeviceContext));

        if (BMG_QueryUserForBdAddr(&newDevice.addr) == TRUE) {
            /* User entered valid address. */
            DS_AddDevice(&newDevice, &bdc);
        }
        break;

    case PMM_EMPTY_LIST:
        /* Clear List view */
        for (bdc = 0; DS_EnumDeviceList(&bdc) == BT_STATUS_SUCCESS; ) {
            /* Remove all non-connected devices */
            if (((BtDeviceExt *)bdc->context)->state == BDS_DISCONNECTED) {
                AssertEval(DS_DeleteDevice(&bdc->addr) == BT_STATUS_SUCCESS);
                bdc = 0;
            }
        }
        BMG(lastInqTime) = 0;

        /* If there's a select device query in progress, clear out the
         * default device because it's been deleted. 
         */
        if (BMG(activeQuery))
            BMG(activeQuery)->result = 0;
        break;

    case PMO_PARK_ALL:
        status = ME_StartParkAll(BCC(startPark_max), BCC(startPark_min), &num);
        BmReport("ME_StartParkAll() returned %s (%d devices).\n", pBT_Status(status), num);
        break;

#if SDP_PARSING_FUNCS == XA_ENABLED
    case PMM_BROWSE:
        RunServiceBrowse(BMG(menuDevice));
        break;
#endif /* SDP_PARSING_FUNCS == XA_ENABLED */

    default:
        Assert(selection == 0);
        break;
    }

    DestroyMenu(hMenu);
}


/*
 * Controls the showing and hiding of the chooser dialog 
 */
static void UpdateChooserStatus(void)
{
    if (BMG(activeQuery)) {
        /* Open the device chooser dialog and activate the OK button. */
        EnableWindow(GetDlgItem(BMG(chooserWnd), IDC_OK), TRUE);
        SendMessage(GetDlgItem(BMG(chooserWnd), IDC_CANCEL), WM_SETTEXT, 0, (LPARAM)"Cancel");
        ShowWindow(BMG(chooserWnd), SW_SHOWNORMAL);
        SetActiveWindow(BMG(chooserWnd));
        EnableWindow(GetParent(BMG(chooserWnd)), FALSE);
        InvalidateRect(BMG(deviceListWnd), 0, TRUE);
    } else if (BMG(chooserOpen)) {
        /* Open the device chooser dialog and deactivate the OK button. */
        EnableWindow(GetDlgItem(BMG(chooserWnd), IDC_OK), FALSE);
        SendMessage(GetDlgItem(BMG(chooserWnd), IDC_CANCEL), WM_SETTEXT, 0, (LPARAM)"Close");
        ShowWindow(BMG(chooserWnd), SW_SHOWNORMAL);
        InvalidateRect(BMG(deviceListWnd), 0, TRUE);
    } else {
        /* Close the device chooser dialog. */
        ShowWindow(BMG(chooserWnd), SW_HIDE);
    }
}


/*
 * Window procedure for processing Pin Code Request Dialog.
 */
static BOOL CALLBACK EnterPinDlgProc(HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    static PinCodeReq  *pinParms = 0;
    char                heading[50];
    static HWND         saveDlg;


    switch (uMsg) {
    
    case WM_INITDIALOG:
        pinParms = (PinCodeReq *)lParam;
        sprintf(heading, "Passkey required for %s", pinParms->DeviceDesc);
        SendMessage(hDlg, WM_SETTEXT, 0, (LPARAM)heading);
        SendMessage(hDlg, EM_SETSEL, 0, (LPARAM)-1);
        SendMessage(hDlg, EM_SETLIMITTEXT, MAX_PIN_LEN, 0L);
        CheckRadioButton(hDlg, IDC_ONCE, IDC_TRUSTED, IDC_ONCE);
        saveDlg = hDlg;
        break;

    case WM_PIN_CANCEL:
        /* Cancel the Dialog */
        EndDialog(saveDlg, FALSE);
        return TRUE;

    case WM_COMMAND:
        switch (LOWORD(wParam)) {
        case PIN_CANCEL:
            EndDialog(hDlg, FALSE);
            return TRUE;

        case PIN_OK:
            /* Retrieve the Pin Code */
            *(U16 *)pinParms->Pin = MAX_PIN_LEN;       /* Sets the buffer size */
            pinParms->PinLen = (U8)SendDlgItemMessage(hDlg, IDC_PIN_CODE, EM_GETLINE,
                                                      0, (LPARAM)pinParms->Pin);
            if (pinParms->PinLen == 0) {
                /* The user didn't enter a pin. */
                EndDialog(hDlg, FALSE);
                return TRUE;
            }

            /* Retrieve the Security setting */
            if (IsDlgButtonChecked(hDlg, IDC_ONCE) == BST_CHECKED)
                pinParms->Flags = BPT_NOT_SAVED;
            else if (IsDlgButtonChecked(hDlg, IDC_AUTHORIZED) == BST_CHECKED)
                pinParms->Flags = BPT_SAVE_NOT_TRUSTED;
            else if (IsDlgButtonChecked(hDlg, IDC_TRUSTED) == BST_CHECKED)
                pinParms->Flags = BPT_SAVE_TRUSTED;
            else Assert(0);

            EndDialog(hDlg, TRUE);
            return TRUE;

        case EN_MAXTEXT:
            heading[0] = 0;
            break;
        }
        break;
    }
    return FALSE;
}

/*
 * Window procedure for processing Authorize Requests
 */
static BOOL CALLBACK EnterAuthorizeDlgProc(HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    static AuthorizeReq *authParms = 0;
    char                heading[50];
    static HWND         saveDlg;


    switch (uMsg) {
    
    case WM_INITDIALOG:
        authParms = (AuthorizeReq *)lParam;
        sprintf(heading, "Authorize %.40s?", authParms->deviceDesc);
        SendMessage(hDlg, WM_SETTEXT, 0, (LPARAM)heading);
        SendMessage(hDlg, EM_SETSEL, 0, (LPARAM)-1);
        CheckRadioButton(hDlg, IDC_ONCE, IDC_TRUSTED, IDC_ONCE);
        saveDlg = hDlg;
        break;

    case WM_AUTHORIZE_CANCEL:
        /* Cancel the Dialog */
        EndDialog(saveDlg, FALSE);
        return TRUE;

    case WM_COMMAND:
        switch (LOWORD(wParam)) {
        case AUTHORIZE_CANCEL:
            EndDialog(hDlg, FALSE);
            return TRUE;

        case AUTHORIZE_OK:

            /* Retrieve the Security setting */
            if (IsDlgButtonChecked(hDlg, IDC_ONCE) == BST_CHECKED)
                authParms->level = 1;
            else if (IsDlgButtonChecked(hDlg, IDC_FOR_CONN) == BST_CHECKED)
                authParms->level = 2;
            else if (IsDlgButtonChecked(hDlg, IDC_FOR_FUTURE) == BST_CHECKED)
                authParms->level = 3;
            else Assert(0);

            EndDialog(hDlg, TRUE);
            return TRUE;

        case EN_MAXTEXT:
            heading[0] = 0;
            break;
        }
        break;
    }
    return FALSE;
}

/*
 * Window procedure for manually entering a Bluetooth device address
 */
static BOOL CALLBACK GetBdAddrDlgProc(HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    static BD_ADDR *addr;
    char            bdAddr[BDADDR_NTOA_SIZE];
    I32             len;

    switch (uMsg) {
    case WM_INITDIALOG:
        /* Save pointer to output address location. */
        addr = (BD_ADDR *)lParam;
        if (IsValidBdAddr(addr)) {
            SendDlgItemMessage(hDlg, PSB_BDADDR, WM_SETTEXT, 0, (LPARAM)bdaddr_ntoa(addr, bdAddr));
            SendDlgItemMessage(hDlg, PSB_BDADDR, EM_SETSEL, 0, MAKELONG(0,0xffff));
        }
        break;

    case WM_COMMAND:
        switch (LOWORD(wParam)) {
        case PSB_OK:
            /* Check for an address in the listbox. */
            if (SendDlgItemMessage(hDlg, PSB_BDADDR, LB_GETCURSEL, 0L, 0L) != LB_ERR) {
                /* Found an address, retrieve it. */
                *((U16 *)bdAddr) = BDADDR_NTOA_SIZE-1;
                len = SendDlgItemMessage(hDlg, PSB_BDADDR, EM_GETLINE, 0, (LPARAM)bdAddr);
                if (len > 16) {
                    bdAddr[len] = 0;
                    *addr = bdaddr_aton(bdAddr);

                    EndDialog(hDlg, TRUE);
                    return TRUE;
                }
            }
            /* The user didn't enter a complete address. */
            /* Drop through to Cancel case */

        case WM_DESTROY:
        case PSB_CANCEL:
            EndDialog(hDlg, FALSE);
            return TRUE;
        }
        break;
    }
    return FALSE;
}

/*
 * Shows application ABOUT dialog.
 */
static BOOL CALLBACK AboutDlgProc(HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    char        msg[50], addr[BDADDR_NTOA_SIZE], *mptr;
    BD_ADDR     bdaddr;
    BtStatus    status;

    switch (uMsg) {
    case WM_INITDIALOG:
        if (ME_ReadLocalBdAddr(&bdaddr) == BT_STATUS_SUCCESS) {
            bdaddr_ntoa(&bdaddr, addr);
            sprintf(msg, "Local Bluetooth Device Address is %s", addr);
            SendMessage(GetDlgItem(hDlg, IDC_ABOUT_ADDR), WM_SETTEXT, 0, (LPARAM)msg);
        }
        switch (getRadioType()) {
        case RADIO_TYPE_ERICSSON_UART:  mptr = "Ericsson Radio"; break;
        case RADIO_TYPE_SIW_UART:
        case RADIO_TYPE_SIW_USB:        mptr = "Silicon Wave Radio"; break;
        case RADIO_TYPE_TI_UART:        mptr = "Texas Instruments Radio"; break;
        case RADIO_TYPE_INFINEON_UART:  mptr = "Infineon Radio"; break;
        case RADIO_TYPE_CSR_UART:
        case RADIO_TYPE_CSR_USB:
        case RADIO_TYPE_CSR_BCSP:       mptr = "Cambridge Silicon Research Radio"; break;
        case RADIO_TYPE_MOTOROLA_UART:  mptr = "Motorolla Radio"; break;
        case RADIO_TYPE_IA_INET:       mptr = "iAnywhere Inet Radio emulator"; break;
        default:                        mptr = "Unrecognized Radio"; break;
        }
        SendMessage(GetDlgItem(hDlg, IDC_ABOUT_RADIO), WM_SETTEXT, 0, (LPARAM)mptr);
        break;

    case WM_COMMAND:
        switch (LOWORD(wParam)) {
        
        case IDC_ABOUT_RESET:
            if (MessageBox(hDlg, "Are you sure you want to reset the radio?","Warning", MB_OKCANCEL|MB_ICONWARNING) == IDOK) {
                status = RMGR_HciReset();
                BmReport("RMGR_HciReset() returned %s.", pBT_Status(status));
            }
            break;

        case IDOK:
            EndDialog(hDlg, FALSE);
            return TRUE;
        }
        break;
    }
    return FALSE;
}


/*
 * Retrieve the BtDeviceContext of the specified device index
 */
static BtDeviceContext *GetDeviceContextByIndex(U32 Index)
{
    LVITEM  lvItem = {0};

    lvItem.mask = LVIF_PARAM;
    lvItem.iItem = Index;

    if (ListView_GetItem(BMG(deviceListWnd), &lvItem) != -1) {
        return(BtDeviceContext *)lvItem.lParam;
    }
    return 0;
}

/*
 * Retrieve the BtDeviceContext of the currently selected device
 */
static BtDeviceContext *GetSelectedDeviceContext()
{
    int     i;

    /* Walk the list of devices, looking for the selected one */
    for (i = 0; i < ListView_GetItemCount(BMG(deviceListWnd)); i++) {
        if (ListView_GetItemState(BMG(deviceListWnd), i, LVIS_SELECTED)) {
            return GetDeviceContextByIndex(i);
        }
    }
    return 0;
}

/*
 * Add a new device to the device list
 */
static int AddDevice(BtDeviceContext *Bdc, BtDeviceExt *Template)
{
    LVITEM           lvItem = {0};
    BtDeviceContext *bdc;
    int              i = 0;

    /* Walk the list of devices, looking for a match */
    while ((bdc = GetDeviceContextByIndex(i))) {
        if (OS_MemCmp(bdc->addr.addr, 6, Bdc->addr.addr, 6) == TRUE) {
            /* Device is already on list, return. */
            return i;
        }
        i++;
    }

    for (i = 0; i < NUM_KNOWN_DEVICES; i++) {
        if (BMG(btDevices)[i].inUse == FALSE) {
            break;
        }
    }
    Assert(i < NUM_KNOWN_DEVICES);

    Bdc->context = BMG(btDevices) + i;

    if (Template == 0) {
        OS_MemSet((U8 *)(BMG(btDevices)+i), 0, sizeof(BtDeviceExt));
        bdaddr_ntoa(&Bdc->addr, BMG(btDevices)[i].name);
    } else BMG(btDevices)[i] = *Template;

    BMG(btDevices)[i].inUse = TRUE;

    UpdateDeviceState(Bdc);

    Report(("BMG: AddDevice() %s.\n", BMG(btDevices)[i].name));

    /* Now add it to device view */
    lvItem.mask = LVIF_TEXT|LVIF_PARAM;
    lvItem.iItem = ListView_GetItemCount(BMG(deviceListWnd));
    lvItem.lParam = (LPARAM)Bdc;
    lvItem.pszText = EXT(Bdc)->name;   /* sort cannot use callback */

    if ((i = ListView_InsertItem(BMG(deviceListWnd), &lvItem)) == -1)
        Assert(0);

    return i;
}

/*
 * Remove a device from the list view.
 */
static void RemoveDevice(BtDeviceContext *Device)
{
    BtDeviceContext *bdc;
    I8               i;

    /* Find the index of the device */
    for (i = 0; bdc = GetDeviceContextByIndex(i); i++) {
        if (bdc == Device) {
            Report(("BMG: RemoveDevice() %s.\n", ((BtDeviceExt *)Device->context)->name));
            ListView_DeleteItem(BMG(deviceListWnd), i);
            ((BtDeviceExt *)Device->context)->inUse = FALSE;
            Device->context = 0;
            break;
        }
    }
}

/*
 * Updates the device state from the remdev pointer.
 */
static void UpdateDeviceState(BtDeviceContext *Bdc)
{
    BtDeviceExt     *ext = (BtDeviceExt *)Bdc->context;
    BtRemoteDevice  *link;

    Assert(ext);

    if (((link = Bdc->link) || (link = ME_FindRemoteDevice(&Bdc->addr))) &&
        (ME_GetRemDevState(link) != BDS_OUT_DISC)) {
        ext->mode = ME_GetCurrentMode(link);
        ext->state = ME_GetRemDevState(link);
        ext->role = ME_GetCurrentRole(link);
        ext->auth = link->authState;
        ext->encrypt = ME_GetRemDevEncryptState(link);
    } else ext->state = BDS_DISCONNECTED;
}


/*
 * Returns a bitmask that represents all the piconet manager functions/options
 * that are available to the specified device.
 */
static U16 GetDeviceOptions(BtDeviceContext *Bdc)
{
    BtDeviceExt *ext;
    U16          opts = 0;

    Assert(Bdc && Bdc->context);

    ext = (BtDeviceExt *)Bdc->context;

    if (ext->state == BDS_CONNECTED) {
        if (ext->mode == BLM_ACTIVE_MODE) {
            opts |= (PMO_PARK|PMO_SNIFF|PMO_HOLD|PMO_SWITCH_ROLE);
            /* If were not parked or holding and security is on, we can handle these. */
            if (BMG(securityMode) > BSM_SEC_LEVEL_1) {
                if (ext->auth == BAS_NOT_AUTHENTICATED)
                    opts |= PMO_AUTHENTICATE;
                else if (ext->encrypt == BES_NOT_ENCRYPTED)
                    opts |= PMO_ENCRYPTION_ON;
                else opts |= PMO_ENCRYPTION_OFF;
            }
        } else if (ext->mode == BLM_PARK_MODE)
            opts |= PMO_UNPARK;
        else if (ext->mode == BLM_SNIFF_MODE)
            opts |= PMO_UNSNIFF;
    }
    if (Bdc->state & BDS_PAIRED)
        opts |= PMO_DELETE_KEY;

    /* If security is disabled, clear security based options. */
#if BT_SECURITY == XA_DISABLED
    opts &= ~(PMO_AUTHENTICATE | PMO_ENCRYPTION_ON | PMO_ENCRYPTION_OFF | PMO_DELETE_KEY);
#endif

    return opts;
}

/*
 * Process radio status changes to update GUI
 */
static void RadioStatusChange(BtEventType Status)
{
    U8               oldState = BMG(state);
    char             addrmsg[50], addr[BDADDR_NTOA_SIZE];
    BD_ADDR          bdaddr;
    BtDeviceContext *bdc = 0;

    Assert(BMG(state) > BMSTATE_IDLE);

    if (Status == BTEVENT_HCI_INITIALIZED) {
        BMG(state) = BMSTATE_RADIO_READY;
        Report(("RadioStatusChange() RADIO Online.\n"));
        SendDlgItemMessage(BMG(chooserWnd), IDC_RADIO_STATUS, WM_SETTEXT, 0, (LPARAM)"Radio Online");

        /* Read our Bluetooth address for display in the GUI. */
        if (ME_ReadLocalBdAddr(&bdaddr) == BT_STATUS_SUCCESS) {
            bdaddr_ntoa(&bdaddr, addr);
            sprintf(addrmsg, "Local Bluetooth Address is %s", addr);
            SendMessage(GetDlgItem(BMG(chooserWnd), IDC_BDADDR), WM_SETTEXT, 0, (LPARAM)addrmsg);
        }

        /* Add any known devices into the device selection dialog. */
        while (DS_EnumDeviceList(&bdc) == BT_STATUS_SUCCESS)
        {
            AddDevice(bdc, 0);
       //     DPrintf("aa");
         ///   BlueInt.BtIntInformNumber(P_GENERAL, REMOTE_DEVS_LIST, INFORM_NUMBER_CHANGE);
        }
        
        /* Get an inquiry started before we need to present the device list. */
        if (BCC(inquiryMode) & BMG_AUTO_REFRESH)
            PostMessage(BMG(chooserWnd), WM_COMMAND, IDC_INQUIRE, 0);

    } else {
        if (Status == BTEVENT_HCI_INIT_ERROR)
            BmReport("Received HCI Initialization error event.");
        else if (Status == BTEVENT_HCI_DEINITIALIZED)
            BmReport("Received HCI Deinitialized event.");
        else if (Status == BTEVENT_HCI_FAILED)
            BmReport("Received HCI Failed event.");
        else BmReport("Received HCI Fatal error event.");

        SendMessage(GetDlgItem(BMG(chooserWnd), IDC_RADIO_STATUS), WM_SETTEXT, 0, (LPARAM)"Radio Offline");
        SendMessage(GetDlgItem(BMG(chooserWnd), IDC_ACCESSIBILITY), WM_SETTEXT, 0, (LPARAM)"");
        SendMessage(GetDlgItem(BMG(chooserWnd), IDC_BDADDR), WM_SETTEXT, 0, (LPARAM)"");

        BMG(state) = BMSTATE_INITIALIZED;
        BMG(activity) = BMG_NO_ACTIVITY;

        if (oldState == BMSTATE_SHUTTING_DOWN)
            SendMessage(BMG(chooserWnd), WM_COMMAND, WM_DESTROY, (LPARAM)oldState);
    }
}

/*
 * Processes results of a ME_GetRemoteDeviceName() request.
 */
static void NameResultHandler(const BtEvent *Event)
{
    BtDeviceContext *bdc;
    BtDeviceExt      old;
    U8               len;
    int              i;

    Assert(Event && Event->eType == BTEVENT_NAME_RESULT);
    Assert(Event->p.meToken == &BMG(nameTok));

    BMG(nameTok).callback = 0;

    /* Update the BtDeviceContext with the name result. */
    if ((bdc = DS_FindDevice(&Event->p.meToken->p.name.bdAddr))) {
        EXT(bdc)->status &= ~BDS_NO_NAME;

        if ((Event->errCode == BEC_NO_ERROR) && (Event->p.meToken->p.name.io.out.len > 0)) {
            BmReport("Received name %s for device %s.\n", BMG(nameTok).p.name.io.out.name, EXT(bdc)->name);

            /* Save the remote device name. */
            len = min(BMG(nameTok).p.name.io.out.len, MAX_DEVICE_NAME_SIZE-1);
            OS_MemCopy(EXT(bdc)->name, BMG(nameTok).p.name.io.out.name, len);
            EXT(bdc)->name[len] = 0;
            EXT(bdc)->status |= BDS_HAVE_NAME;

            /* To keep the list properly sorted, we need to delete and then
             * re-insert the item. First save the Device Extension data. 
             */
            old = *(BtDeviceExt *)bdc->context;
            RemoveDevice(bdc);
            i = AddDevice(bdc, &old);
        } else {
            /* Record that the name request failed. Next time we'll skip it. */
            BmReport("Name request for %s failed, status %s.\n", EXT(bdc)->name, pHC_Status(Event->errCode));
            EXT(bdc)->status |= BDS_NAME_FAILED;
        }
    }

    BMG(activity) &= ~BMG_NAMEREQ_ACTIVE;
    UpdateStatusMsg();
    RunNameQuery();
}

/*
 * Issues a ME_GetRemoteDeviceName() request.
 */
static void RunNameQuery(void)
{
    BtDeviceContext *bdc = 0;
    BtStatus         status;

    if (BMG(nameTok).callback != 0) {
        /* A RemoteDeviceName query is already active. */
        return;
    }

    if ((BCC(inquiryMode) & BMG_DO_NAMEREQ) == 0) {
        return;
    }

    if (((BCC(inquiryMode) & BMG_DO_NAMEREQ_INSEQ) != BMG_DO_NAMEREQ_INSEQ) &&
        (BMG(activity) & BMG_INQUIRY_ACTIVE)) {
        return;
    }

    while (DS_EnumDeviceList(&bdc) == BT_STATUS_SUCCESS) {
        if ((((BtDeviceExt *)bdc->context)->status & BDS_NO_NAME) == 0)
            continue;

        /* Start a name query for this device */
        BMG(nameTok).callback = NameResultHandler;
        BMG(nameTok).p.name.bdAddr = bdc->addr;
        BMG(nameTok).p.name.io.in.psi = bdc->psi;

        status = ME_GetRemoteDeviceName(&BMG(nameTok));
        BmReport("ME_GetRemoteDeviceName(%s) returned %s.\n", EXT(bdc)->name, pBT_Status(status));
        Assert((status == BT_STATUS_PENDING) || (status == BT_STATUS_HCI_INIT_ERR));
        break;
    }
}


/*
 * Sends a message to the Bluetooth Manager GUI Listbox.
 * This function sets the proper horizontal extent for the scroll bar.
 */
void BmReport(const char *format,...)
{
    static DWORD    extent = 0;
    char            buffer[200]; /* Output Buffer */
    int             index;
    va_list         args;
    SIZE            size;  
    DWORD           newExtent;
    HDC             hDCListBox;
    HWND            hWndList = GetDlgItem(BMG(chooserWnd), BDM_MONITOR);
    HFONT           hFontOld, hFontNew;
    TEXTMETRIC      tm;

    va_start(args, format);
    index = _vsnprintf(buffer, 200, format, args);
    va_end(args);

    /* Skim off accidental newline characters, add punctuation. */
    if (buffer[index-1] == '\n') {
        if (buffer[index-2] == '.') {
            buffer[index-1] = 0;
            index--;
        } else buffer[index-1] = '.';
    }

    /* See if we need to adjust the horizontal scrolling area */
    hDCListBox = GetDC(hWndList);
    hFontNew = (HFONT)SendMessage(hWndList, WM_GETFONT, 0, 0);
    hFontOld = SelectObject(hDCListBox, hFontNew);
    GetTextMetrics(hDCListBox, &tm);
    if (GetTextExtentPoint32(hDCListBox, buffer, strlen(buffer), &size))
        newExtent = size.cx + tm.tmAveCharWidth;

    SelectObject(hDCListBox, hFontOld);
    ReleaseDC(hWndList, hDCListBox);

    /* Is this line longer then are current width? */
    if (newExtent > extent) {
        extent = newExtent;
        SendMessage(hWndList, LB_SETHORIZONTALEXTENT, (WPARAM)extent, 0);
    }

    /* Now write the text out to the listbox. */
    index = SendMessage(hWndList, LB_ADDSTRING, 0, (LPARAM)buffer);
    SendMessage(hWndList, LB_SETCARETINDEX, index, 0);
}

/*
 * Updates the chooser status bar.
 */
static void UpdateStatusMsg(void)
{
    char *msg, msgStr[20];

    if (BMG(activity) & BMG_NAMEREQ_ACTIVE)
        msg = "Name Request";
    else if (BMG(activity) & BMG_INQUIRY_ACTIVE)
        msg = "Inquiry";
    else if (BMG(numConns) > 0) {
        msg = msgStr;
        sprintf(msgStr, "%d Active ACL%s", BMG(numConns), (BMG(numConns) > 1 ? "s":""));
    } else msg = "";

    SendMessage(GetDlgItem(BMG(chooserWnd), IDC_INQSTATE), WM_SETTEXT, 0, (LPARAM)msg);

    if (BMG(activity) & BMG_INQUIRY_ACTIVE)
        SendMessage(GetDlgItem(BMG(chooserWnd), IDC_INQUIRE), WM_SETTEXT, 0, (LPARAM)"Stop Inquiry");
    else if (BMG(activity) & BMG_NAMEREQ_ACTIVE)
        SendMessage(GetDlgItem(BMG(chooserWnd), IDC_INQUIRE), WM_SETTEXT, 0, (LPARAM)"Stop Names");
    else SendMessage(GetDlgItem(BMG(chooserWnd), IDC_INQUIRE), WM_SETTEXT, 0, (LPARAM)"Refresh");
}

#ifdef DEMO
/*
 * Window procedure for processing Debug Window.
 */
static BOOL CALLBACK DebugWndProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam)
{
    static LONG     wOffset, hOffset;
    LONG            width, height;
    RECT            w_rect, l_rect;
    MENUITEMINFO    mii;

    switch (Msg) {
    
    case WM_INITDIALOG:
        break;

    case WM_SETFOCUS:
        GetWindowRect(hWnd, &w_rect);
        GetWindowRect(GetDlgItem(hWnd, STACK_OUTPUT), &l_rect);
        wOffset = (w_rect.right - l_rect.right);
        hOffset = (w_rect.bottom - l_rect.bottom);
        break;

    case WM_SIZING:
    case WM_EXITSIZEMOVE:       /* Msg for Win95 */
        GetClientRect(hWnd, &w_rect);
        width = (w_rect.right - w_rect.left - wOffset);
        height = (w_rect.bottom - w_rect.top - hOffset);
        MoveWindow(GetDlgItem(hWnd, STACK_OUTPUT), 5, 6, width, height, TRUE); /* Origin (x,y) = 5,6 */
        break;

    case WM_COMMAND:
        switch (LOWORD(wParam)) {
        case WM_DESTROY:
            if (hWnd == BMG(debugWnd)) {
                mii.cbSize = sizeof(mii);
                mii.fMask = MIIM_STATE;
                mii.fState = MFS_UNCHECKED;
                SetMenuItemInfo(GetMenu(GetParent(hWnd)), ID_VIEW_DEMO, FALSE, &mii);
            }
            ShowWindow(hWnd, SW_HIDE);
            return TRUE;
            break;
        }
        break;
    }

    return 0;
}

/*
 * Redirects report messages to the Demo GUI.
 */
void OS_Report(const char *format,...)
{
    char     buffer[200]; /* Output Buffer */
    int      index;
    va_list  args;

    if (BMG(debugWnd) == 0) return;

    va_start(args, format);
    index = _vsnprintf(buffer, 200, format, args);
    va_end(args);

    if (buffer[index-1] == '\n') {
        if (buffer[index-2] == '.') {
            buffer[index-1] = 0;
            index--;
        } else buffer[index-1] = '.';
    }

    index = SendDlgItemMessage(BMG(debugWnd), STACK_OUTPUT, LB_ADDSTRING, 0, (LPARAM)buffer);
    SendDlgItemMessage(BMG(debugWnd), STACK_OUTPUT, LB_SETCARETINDEX, index, 0);

} /* End of OS_Report() */
#endif


#if XA_STATISTICS == XA_ENABLED
#include "btalloc.h"
/*
 * Window procedure for processing Statistics Window.
 */
static BOOL CALLBACK StatsWndProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam)
{
    static LONG     wOffset, hOffset;
    MENUITEMINFO    mii;
    LONG            width, height;
    RECT            w_rect, l_rect;
    static BOOL     firstTime = TRUE;

    switch (Msg) {
    case WM_INITDIALOG:
        break;

    case WM_ACTIVATE:
        /* Tell winmain() that the active window is changing. */
        if (LOWORD(wParam) == WA_INACTIVE)
            PostMessage(hWnd, WM_ACTIVE_WND, 0, lParam);
        else PostMessage(hWnd, WM_ACTIVE_WND, 0, (LPARAM)hWnd);
        break;

    case WM_SHOWWINDOW:
        if (firstTime == TRUE) {
            firstTime = FALSE;
            BTSTAT_Report(&BTC(stats), StatReport);
        }
        break;

    case WM_SETFOCUS:
        GetWindowRect(hWnd, &w_rect);
        GetWindowRect(GetDlgItem(hWnd, BDM_STATISTICS), &l_rect);
        wOffset = (w_rect.right - l_rect.right);
        hOffset = (w_rect.bottom - l_rect.bottom);
        break;

    case WM_SIZING:
    case WM_EXITSIZEMOVE:       /* Msg for Win95 */
        /* Resize the child windows, based on the parents new size. */
        GetClientRect(hWnd, &w_rect);
        width = (w_rect.right - w_rect.left - wOffset);
        height = (w_rect.bottom - w_rect.top - hOffset);
        MoveWindow(GetDlgItem(hWnd, BDM_STATISTICS), 5, 6, width, height, TRUE); /* Origin (x,y) = 5,6 */
        GetWindowRect(GetDlgItem(hWnd, BDM_REFRESH), &l_rect);
        MoveWindow(GetDlgItem(hWnd, BDM_REFRESH), w_rect.right - 194, w_rect.bottom - 26, 
                   l_rect.right-l_rect.left, l_rect.bottom-l_rect.top, TRUE); /* Origin (x,y) = -194,-26 */
        GetWindowRect(GetDlgItem(hWnd, BDM_RESET), &l_rect);
        MoveWindow(GetDlgItem(hWnd, BDM_RESET), w_rect.right - 90, w_rect.bottom - 26, 
                   l_rect.right-l_rect.left, l_rect.bottom-l_rect.top, TRUE); /* Origin (x,y) = -90, -26 */
        break;

    case WM_COMMAND:
        switch (LOWORD(wParam)) {
        case BDM_REFRESH:
            BTSTAT_Report(&BTC(stats), StatReport);
            break;

        case BDM_RESET:
            OS_MemSet((U8 *)&BTC(stats), 0, sizeof(BtStatistics));
            break;

        case WM_DESTROY:
            if (hWnd == BMG(statsWnd)) {
                mii.cbSize = sizeof(mii);
                mii.fMask = MIIM_STATE;
                mii.fState = MFS_UNCHECKED;
                SetMenuItemInfo(GetMenu(GetParent(hWnd)), ID_VIEW_STATS, FALSE, &mii);
            }
            ShowWindow(hWnd, SW_HIDE);
            return TRUE;
            break;
        }
        break;
    }

    return 0;
}

/*
 * Sends a message to the Bluetooth Manager Statistics Listbox.
 */
static void StatReport(const char *format,...)
{
    char            buffer[200]; /* Output Buffer */
    int             index;
    va_list         args;
    HWND            hWndList = GetDlgItem(BMG(statsWnd), BDM_STATISTICS);

    va_start(args, format);
    index = _vsnprintf(buffer, 200, format, args);
    va_end(args);

    /* Skim off newline characters. */
    if (buffer[index-1] == '\n')
        buffer[index-1] = 0;

    /* Now write the text out to the listbox. */
    index = SendMessage(hWndList, LB_ADDSTRING, 0, (LPARAM)buffer+1);
    SendMessage(hWndList, LB_SETCARETINDEX, index, 0);
}
#endif


#if SDP_PARSING_FUNCS == XA_ENABLED
#include "sys/sdpi.h"
/*
 * Searches the Public Browse group for the specified device
 */
static void RunServiceBrowse(BtDeviceContext *Device)
{
    BtStatus    status;
    BtEvent     e;
    char        msg[80];

    if (BMG(browseDev) != 0) {
        /* A public service browse is already in progress. */
        return;
    }

    if (BMG(browseWnd) == 0) {
        BMG(browseWnd) = CreateDialog(BMG(hInstance), MAKEINTRESOURCE(BDM_BROWSESDP), BMG(chooserWnd), BrowserWndProc);
        if (BMG(browseWnd) == 0) {
            return;
        }
    }
    sprintf(msg, "Connecting to %s...", EXT(Device)->name);
    SendDlgItemMessage(BMG(browseWnd), IDC_BROWSER_STATUS, WM_SETTEXT, 0, (LPARAM)msg);

    /* Prepare the SDP Query and Parse Attributes fields for later use. */
    BMG(browseToken).parms = BrowseServiceSearchAttribReq;
    BMG(browseToken).plen = sizeof(BrowseServiceSearchAttribReq);
    BMG(browseToken).type = BSQT_SERVICE_SEARCH_ATTRIB_REQ;
    BMG(browseToken).callback = BrowserEventHandler;
    BMG(browseToken).attribId = AID_SERVICE_CLASS_ID_LIST;
    BMG(browseToken).uuid = 0;
    BMG(browseToken).mode = BSPM_BEGINNING;

    BMG(browser).state = 0;
    BMG(browser).stageLen = 1;
    BMG(browser).stageOff = 0;

    /* Need to establish an ACL connection to run the query. */
    BMG(browserHandler).callback = BrowserEventHandler;

    status = ME_CreateLink(&BMG(browserHandler), &Device->addr, &Device->psi, &BMG(browseDev));
    if (status != BT_STATUS_PENDING) {
        if (status == BT_STATUS_SUCCESS) {
            /* On immediate success, deliver completion event to handler. */
            e.eType = BTEVENT_LINK_CONNECT_CNF;
            e.p.remDev = BMG(browseDev);
            e.errCode = BEC_NO_ERROR;

            BrowserEventHandler(&e);
            return;
        }
        sprintf(msg, "Could not initiate an ACL connection, reason %s.\n", pBT_Status(status));
        BrowserEnd(msg);
    }
}

/*
 * Add either a root level device, or a service sub-group to the tree.
 */
static HTREEITEM TreeView_AddBranch(HTREEITEM Parent, LPARAM Context)
{
    HWND            hWndTv = GetDlgItem(BMG(browseWnd), IDC_BROWSETREE);
    TVITEM          item;
    HTREEITEM       node;
    TVINSERTSTRUCT  tvins = {0};

    if (Parent == TVI_ROOT)
        node = TreeView_GetRoot(hWndTv);
    else node = TreeView_GetChild(hWndTv, Parent);

    /* Search the branches of this parent for a match. */
    item.mask = TVIF_PARAM;

    while (item.hItem = node) {
        TreeView_GetItem(hWndTv, &item);
        if (item.lParam == Context) {
            /* This branch exists, delete it. */
            TreeView_DeleteItem(hWndTv, node);
            break;
        }
        node = TreeView_GetNextSibling(hWndTv, node);
    }

    /* Create a new branch. */
    tvins.hParent = Parent;
    tvins.hInsertAfter = TVI_SORT;
    tvins.item.mask = TVIF_TEXT | TVIF_PARAM | TVIF_CHILDREN;
    tvins.item.cChildren = 0;
    tvins.item.pszText = EXT((BtDeviceContext *)Context)->name;
    tvins.item.cchTextMax = strlen(tvins.item.pszText);
    tvins.item.lParam = Context;

    /* Add the branch or root item to the tree view control. */
    node = TreeView_InsertItem(hWndTv, &tvins);

    Assert(node);
    return node;
}

/*
 * Add a service to the parent group or device.
 */
static HTREEITEM TreeView_AddLeaf(HTREEITEM Parent, U32 Service)
{
    HWND            hWndTv = GetDlgItem(BMG(browseWnd), IDC_BROWSETREE);
    TVITEM          item;
    HTREEITEM       node;
    TVINSERTSTRUCT  tvins = {0};
    char            servStr[25];

    /* Search this branch for a child which matches the one were about to add. */
    node = TreeView_GetChild(hWndTv, Parent);
    item.mask = TVIF_PARAM;

    while (item.hItem = node) {
        TreeView_GetItem(hWndTv, &item);
        if (item.lParam == (LPARAM)Service) {
            /* This service is already in the list. */
            return node;
        }
        node = TreeView_GetNextSibling(hWndTv, node);
    }

    /* Unique service, add it. */
    tvins.hParent = Parent;
    tvins.hInsertAfter = TVI_SORT;
    tvins.item.mask = TVIF_TEXT | TVIF_PARAM | TVIF_CHILDREN;
    tvins.item.cChildren = 0;
    tvins.item.pszText = (LPSTR)pSdpService(Service);
    if ((tvins.item.cchTextMax = strlen(pSdpService(Service))) == 0) {
        /* Unknown service, just show the raw UUID. */
        sprintf(servStr, "Unrecognized (%x)", Service);
        tvins.item.pszText = (LPSTR)servStr;
        tvins.item.cchTextMax = strlen(servStr);
    }
    tvins.item.lParam = (LPARAM)Service;

    /* Add the service item to the tree view control. */
    node = TreeView_InsertItem(hWndTv, &tvins);
    Assert(node);

    /* Set the parent to indicate that is has children. */
    item.mask = TVIF_CHILDREN|TVIF_HANDLE;
    item.hItem = Parent;
    item.cChildren = 1;
    TreeView_SetItem(hWndTv, &item);

    return node;
}

/*
 * Called to end the browsing session and display the resutls. 
 */
static void BrowserEnd(const char *StatusMsg)
{
    HWND            hWndTv = GetDlgItem(BMG(browseWnd), IDC_BROWSETREE);
    HTREEITEM       node;
    TVITEM          item;

    if (StatusMsg == 0) {
        /* Normal completion, set the status message. */
        if (TreeView_GetChild(hWndTv, BMG(browseTree)) == 0)
            StatusMsg = "Public service browsing not supported.";
        else StatusMsg = "Service browse successful.";
    }

    SendDlgItemMessage(BMG(browseWnd), IDC_BROWSER_STATUS, WM_SETTEXT, 0, (LPARAM)StatusMsg);

    if (BMG(browseDev)) {
        /* An ACL exists, disconnect it. */
        ME_DisconnectLink(&BMG(browserHandler), BMG(browseDev));
        BMG(browseDev) = 0;
    }

    /* Walk the list of trees to collapse and unbold all trees except
     * for the current tree which we expand and bold.
     */
    node = TreeView_GetRoot(hWndTv);

    while (node) {
        item.mask = TVIF_STATE|TVIF_HANDLE;
        item.hItem = node;
        item.stateMask = 0x00FF;

        TreeView_GetItem(hWndTv, &item);

        if (node == BMG(browseTree))
            item.state |= TVIS_BOLD;
        else item.state &= ~TVIS_BOLD;

        TreeView_SetItem(hWndTv, &item);
        TreeView_Expand(hWndTv, node, (node == BMG(browseTree) ? TVE_EXPAND : TVE_COLLAPSE));

        node = TreeView_GetNextSibling(hWndTv, node);
    }

    if (BMG(browseTree)) {
        /* Rotate the current tree into view. */
        TreeView_Select(hWndTv, BMG(browseTree), TVGN_FIRSTVISIBLE);
        BMG(browseTree) = 0;
    }

    ShowWindow(BMG(browseWnd), SW_SHOWNORMAL);
    SetForegroundWindow(BMG(browseWnd));
}

/*
 * Receives ACL connection and SDP result events from the Bluetooth stack.
 */
static void BrowserEventHandler(const BtEvent *Event)
{
    BtStatus    status;
    char        msg[80];

    switch (Event->eType) {
    case BTEVENT_LINK_CONNECT_CNF:
        Assert(Event->p.remDev == BMG(browseDev));
        if (Event->errCode != BEC_NO_ERROR) {
            sprintf(msg, "ACL Connect request failed: %s.", pHC_Status(Event->errCode));
            BrowserEnd(msg);
            break;
        }

        BMG(browseTree) = TreeView_AddBranch(TVI_ROOT, (LPARAM)DS_FindDevice(&BMG(browseDev)->bdAddr));

        /* Connection is up! Start Public Browse query. */
        BMG(browseToken).rm = BMG(browseDev);

        status = SDP_Query(&BMG(browseToken), BSQM_FIRST);
        if (status != BT_STATUS_PENDING) {
            BrowserEnd("Failed to initiate SDP Query.");
            break;
        }
        break;

    case SDEVENT_QUERY_RSP:
        /* Process the browse group service UUID list. */
        while ((status = SDP_ParseAttributes(&BMG(browseToken))) == BT_STATUS_SUCCESS) {

            BrowserParseResults(BMG(browseToken).valueBuff, BMG(browseToken).availValueLen);

            BMG(browseToken).mode = BSPM_RESUME;
            BMG(browseToken).results = BMG(browseToken).remainBuff;
            BMG(browseToken).rLen = BMG(browseToken).remainLen;
        }

        if (status == BT_STATUS_SDP_CONT_STATE) {
            Report(("BMG: SDP_ParseAttributes - Continuation State. Query Again!\n"));

            /* Check for partial results. */
            BrowserParseResults(BMG(browseToken).valueBuff, BMG(browseToken).availValueLen);

            BMG(browseToken).mode = BSPM_CONT_STATE;

            status = SDP_Query(&BMG(browseToken), BSQM_CONTINUE);
            if (status == BT_STATUS_PENDING) {
                break;
            }
            Report(("BMG: Could not continue SDP Query, reason %s.\n", pBT_Status(status)));
        }
        /* Done, drop into next case. */

    case SDEVENT_QUERY_ERR:
    case SDEVENT_QUERY_FAILED:
        BrowserEnd(0);
        break;
    }
}

/*
 * Mini state machine for parsing UUID lists. While the parsing is generally
 * trivial, a state machine is necessary because the results could be split
 * across multiple responses. This parser is also probably a bit more robust
 * than is minimally necessary, but we don't want invalid responses to trip
 * up our parsing.
 */
static void BrowserParseResults(const U8 *Results, U32 Length)
{
    U8      i;
    U32     n, service;
    U16     offset;

    while (Length > 0) {
        Assert(BMG(browser).stageLen <= 5); 
        Assert(BMG(browser).stageOff <= BMG(browser).stageLen);

        n = min((BMG(browser).stageLen - BMG(browser).stageOff), Length);
        Assert(n <= 5);

        Length -= n;
        i = BMG(browser).stageOff;
        BMG(browser).stageOff += (U8)n;

        /* Stage the data */
        while (i < BMG(browser).stageOff)
            BMG(browser).stage[i++] = *Results++;

        /* If the stage is incomplete, exit. */
        if (BMG(browser).stageOff != BMG(browser).stageLen) {
            Assert(Length == 0);
            return;
        }
        BMG(browser).stageOff = 0;

        switch (BMG(browser).state) {
        case 0:
            /* Read the staged element header length */
            BMG(browser).stageLen = SdpParseElementHeader(BMG(browser).stage[0]);
            BMG(browser).stageOff = 1;  /* Leave the header type in the stage. */
            BMG(browser).state++;
            break;

        case 1:
            /* Read the staged element value length */
            BMG(browser).stageLen = SDP_ParseDataElement(BMG(browser).stage, &offset);
            BMG(browser).state++;

            /* Value must be a UUID of 2 or 4 bytes to be processed. */
            if (((BMG(browser).stage[0] & DETD_MASK) != DETD_UUID) || (BMG(browser).stageLen > 4)) {
                /* Flush the value bytes */
                BMG(browser).skipLen = BMG(browser).stageLen;
                BMG(browser).stageLen = 0;
            }
            break;

        case 2:
            if (BMG(browser).skipLen > 0) {
                /* Skip over header value (not staged) */
                n = min(BMG(browser).skipLen, Length);
                BMG(browser).skipLen -= n;
                Length -= n;
                Results += n;
            } else {
                /* Extract staged service UUID */
                if (BMG(browser).stageLen == 2)
                    service = BEtoHost16(BMG(browser).stage);
                else service = BEtoHost32(BMG(browser).stage);

                Report(("FOUND UUID: %x (%s)\n", service, pSdpService(service)));
                TreeView_AddLeaf(BMG(browseTree), service);
            }

            if (BMG(browser).skipLen == 0) {
                /* Reset parser for next element. */
                BMG(browser).state = 0;
                BMG(browser).stageLen = 1;
            }
            break;
        }
    }
}


/*
 * Window procedure for Service Browsing window.
 */
static BOOL CALLBACK BrowserWndProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam)
{
    PAINTSTRUCT     ps; 
    RECT            rect, rect2;
    HDC             hdc;
    int             margin;

    switch (Msg) {
    case WM_SHOWWINDOW:
        if (wParam == TRUE) {
            /* Center the dialog over the main application window. */
            GetWindowRect(hWnd, &rect);
            margin = rect.right - rect.left;
            GetWindowRect(GetParent(hWnd), &rect);
            if ((rect.right - rect.left) > margin)
                margin = rect.right - rect.left - margin;
            else margin = 16;
            SetWindowPos(hWnd, HWND_TOP, rect.left + (margin/2), rect.top + 42, 0, 0, SWP_NOSIZE|SWP_SHOWWINDOW);
        }
        break;

    case WM_PAINT:
        /* Draw line above browser status window. */
        hdc = BeginPaint(hWnd, &ps); 
        GetWindowRect(hWnd, &rect);
        GetWindowRect(GetDlgItem(hWnd, IDC_BROWSER_STATUS), &rect2);
        margin = rect.bottom - rect2.top;
        GetClientRect(hWnd, &rect); 
        rect.left += 3;
        rect.right -= 4;
        rect.bottom -= margin;
        DrawEdge(hdc, &rect, EDGE_ETCHED, BF_BOTTOM); 
        EndPaint(hWnd, &ps); 
        break;

    case WM_COMMAND:
        switch (LOWORD(wParam)) {
        
        case IDOK:
        case WM_DESTROY:
            if (hWnd == BMG(browseWnd)) {
                ShowWindow(hWnd, SW_HIDE);
            }
            return TRUE;
        }
        break;
    }

    return 0;
}

#endif /* SDP_PARSING_FUNCS == XA_ENABLED */

/* ------------------------------------------------------------------------- *
 *                     Bluetooth Manager Property Pages                      *
 * ------------------------------------------------------------------------- */

/*
 * Main window procedure for the Bluetooth property pages.
 */
static BOOL CALLBACK PropertyWndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    int             i;
    TCITEM          tcItem;
    RECT            rect;

    switch (uMsg) {
    case WM_INITDIALOG:
        /* Initialize the property tabs */
        tcItem.mask = TCIF_TEXT;
        for (i = 0; Tabs[i].title; i++) {
            tcItem.pszText = Tabs[i].title;

            if (TabCtrl_InsertItem(GetDlgItem(hWnd, PROP_TABS), i, &tcItem) == -1)
                break;
        }
        break;

    case WM_ACTIVATE:
        /* Tell winmain() that the active window is changing. */
        if (LOWORD(wParam) == WA_INACTIVE)
            PostMessage(hWnd, WM_ACTIVE_WND, 0, lParam);
        else PostMessage(hWnd, WM_ACTIVE_WND, 0, (LPARAM)hWnd);
        break;

    case WM_SHOWWINDOW:
        if (wParam == TRUE) {
            /* Position the property page centered over the parent. */
            GetWindowRect(hWnd, &rect);
            i = rect.right - rect.left;
            GetWindowRect(GetParent(hWnd), &rect);
            if ((rect.right - rect.left) > i)
                i = rect.right - rect.left - i;
            else i = 16;
            SetWindowPos(hWnd, 0, rect.left + (i/2), rect.top + 42, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
        }
        break;

    case WM_COMMAND:
        switch (LOWORD(wParam)) {
        case IDOK:
            i = TabCtrl_GetCurSel(GetDlgItem(BMG(propertyWnd), PROP_TABS));
            SendMessage(Tabs[i].handle, WM_COMMAND, IDOK, 0);
            /* Drop into next case */

        case IDCANCEL:
            ShowWindow(hWnd, SW_HIDE);
            break;

        case BDM_APPLY:
            i = TabCtrl_GetCurSel(GetDlgItem(BMG(propertyWnd), PROP_TABS));
            SendMessage(Tabs[i].handle, WM_COMMAND, BDM_APPLY, 0);
            break;
        }
        break;

    case WM_NOTIFY:
        TabOnNotify((NMHDR *)lParam);
        break;

    case WM_DESTROY:
        if (hWnd == BMG(propertyWnd))
            BMG(propertyWnd) = 0;
        break;
    }
    return 0;
}

/*
 * Window procedure for the Access property page.
 */
static BOOL CALLBACK TabDlgAccess(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    const trackbarInfo tb[] = {
        /* trackbar id       buddy id      label id            min     max     inc  ticfreq pagesz  btdefault */
        { IDC_INQ_SPIN_INT,  IDC_INQ_INT,  IDC_INQ_LABEL_INT,  0x0012, 0x1000, 625, 0x0100, 0x0032, BT_DEFAULT_INQ_SCAN_INTERVAL},
        { IDC_INQ_SPIN_WND,  IDC_INQ_WND,  IDC_INQ_LABEL_WND,  0x0012, 0x1000, 625, 0x0100, 0x0032, BT_DEFAULT_INQ_SCAN_WINDOW},
        { IDC_PAGE_SPIN_INT, IDC_PAGE_INT, IDC_PAGE_LABEL_INT, 0x0012, 0x1000, 625, 0x0100, 0x0032, BT_DEFAULT_PAGE_SCAN_INTERVAL},
        { IDC_PAGE_SPIN_WND, IDC_PAGE_WND, IDC_PAGE_LABEL_WND, 0x0012, 0x1000, 625, 0x0100, 0x0032, BT_DEFAULT_PAGE_SCAN_WINDOW}
    };
    enum tb_id {
        TBACC_INQ_INT, TBACC_INQ_WND, TBACC_PAGE_INT, TBACC_PAGE_WND, TBACC_COUNT
    };

    static BtAccessibleMode  accC, accN, *curacc, tempacc;
    static U16  posC[TBACC_COUNT], posN[TBACC_COUNT], *curpos;
    long        value, pos;
    char        valstr[10];
    int         i;
    BOOL        modified = FALSE;
    BtStatus    status;

    switch (uMsg) {
    case WM_INITDIALOG:
        for (i = 0; i < TBACC_COUNT; i++) {
            SendDlgItemMessage(hWnd, tb[i].trackbar, TBM_SETRANGEMIN, 0, tb[i].min);
            SendDlgItemMessage(hWnd, tb[i].trackbar, TBM_SETRANGEMAX, 0, tb[i].max);
            SendDlgItemMessage(hWnd, tb[i].trackbar, TBM_SETTICFREQ, tb[i].ticfreq, 0);
            SendDlgItemMessage(hWnd, tb[i].trackbar, TBM_SETPAGESIZE, 0, tb[i].pagesize);
            SetWindowLong(GetDlgItem(hWnd, tb[i].trackbar), GWL_USERDATA, i);

            /* Disable the option if the ME won't allow us to set it. */
            if (tb[i].btdefault == 0) {
                EnableWindow(GetDlgItem(hWnd, tb[i].trackbar), FALSE);
                EnableWindow(GetDlgItem(hWnd, tb[i].buddy), FALSE);
                EnableWindow(GetDlgItem(hWnd, tb[i].label), FALSE);
                posC[i] = posN[i] = 0;
            }
        }
        break;

    case WM_RELOAD:
        /* Load existing values for both connected and not-connected modes. */
        posC[TBACC_INQ_INT] = BCC(setAccessibleModeC_info).inqInterval;
        posN[TBACC_INQ_INT] = BCC(setAccessibleModeNC_info).inqInterval;
        posC[TBACC_INQ_WND] = BCC(setAccessibleModeC_info).inqWindow;
        posN[TBACC_INQ_WND] = BCC(setAccessibleModeNC_info).inqWindow;
        posC[TBACC_PAGE_INT] = BCC(setAccessibleModeC_info).pageInterval;
        posN[TBACC_PAGE_INT] = BCC(setAccessibleModeNC_info).pageInterval;
        posC[TBACC_PAGE_WND] = BCC(setAccessibleModeC_info).pageWindow;
        posN[TBACC_PAGE_WND] = BCC(setAccessibleModeNC_info).pageWindow;

#if BT_ALLOW_SCAN_WHILE_CON == XA_ENABLED
        ME_GetAccessibleModeC(&accC, 0);
#endif
        ME_GetAccessibleModeNC(&accN, 0);
        /* Always show "Not Connected" mode initially. */
        curpos = posN;
        curacc = &accN;
        SetWindowText(GetDlgItem(hWnd, IDC_CONN_MODE), "When Not Connected");

        /* Drop into SHOWWINDOW handling */

    case WM_SHOWWINDOW:
        for (i = 0; i < TBACC_COUNT; i++) {
            if (tb[i].btdefault == 0)
                continue;

            SendMessage(GetDlgItem(hWnd, tb[i].trackbar), TBM_SETPOS, TRUE, curpos[i]);
            SendMessage(hWnd, WM_HSCROLL, 0, (LPARAM)GetDlgItem(hWnd, tb[i].trackbar));
        }

        switch (*curacc) {
        case BAM_NOT_ACCESSIBLE:
            CheckRadioButton(hWnd, PME_ACC_1, PME_ACC_4, PME_ACC_1);
            break;
        case BAM_GENERAL_ACCESSIBLE: 
            CheckRadioButton(hWnd, PME_ACC_1, PME_ACC_4, PME_ACC_2);
            break;
        case BAM_CONNECTABLE_ONLY:
            CheckRadioButton(hWnd, PME_ACC_1, PME_ACC_4, PME_ACC_3);
            break;
        case BAM_LIMITED_ACCESSIBLE: 
            CheckRadioButton(hWnd, PME_ACC_1, PME_ACC_4, PME_ACC_4);
            break;
        }
        goto UpdateApplyButton;

    case WM_HSCROLL:
        i = GetWindowLong((HWND)lParam, GWL_USERDATA);
        pos = SendMessage((HWND)lParam, TBM_GETPOS, 0, 0);

        value = pos * tb[i].increment;

        sprintf(valstr, "%d.%06d", (value / 1000000), (value % 1000000));
        SendDlgItemMessage(hWnd, tb[i].buddy, WM_SETTEXT, 0, (LPARAM)valstr);

        /* Update the locally cached value */
        curpos[i] = (U16)pos;

        if ((i == TBACC_INQ_INT) || (i == TBACC_PAGE_INT)) {
            /* If the interval is less than the window, move the window down too. */
            if (curpos[i] < curpos[i+1]) {
                SendMessage(GetDlgItem(hWnd, tb[i+1].trackbar), TBM_SETPOS, TRUE, curpos[i]);
                SendMessage(hWnd, WM_HSCROLL, 0, (LPARAM)GetDlgItem(hWnd, tb[i+1].trackbar));
            }
        } else {
            /* If the window is greater than the interval, move the interval up too. */
            if (curpos[i] > curpos[i-1]) {
                SendMessage(GetDlgItem(hWnd, tb[i-1].trackbar), TBM_SETPOS, TRUE, curpos[i]);
                SendMessage(hWnd, WM_HSCROLL, 0, (LPARAM)GetDlgItem(hWnd, tb[i-1].trackbar));
            }
        }
        goto UpdateApplyButton;

    case WM_NOTIFY:
        TabOnNotify((NMHDR *)lParam);
        break;

    case WM_COMMAND:
        switch (LOWORD(wParam)) {
        case PME_ACC_1:
            *curacc = BAM_NOT_ACCESSIBLE;
            goto UpdateApplyButton;

        case PME_ACC_2:
            *curacc = BAM_GENERAL_ACCESSIBLE;
            goto UpdateApplyButton;

        case PME_ACC_3:
            *curacc = BAM_CONNECTABLE_ONLY;
            goto UpdateApplyButton;

        case PME_ACC_4:
            *curacc = BAM_LIMITED_ACCESSIBLE;
            goto UpdateApplyButton;

        case IDC_CONN_MODE:
#if BT_ALLOW_SCAN_WHILE_CON == XA_ENABLED
            if (curpos == posC) {
                SetWindowText(GetDlgItem(hWnd, IDC_CONN_MODE), "When Not Connected");
                curpos = posN;
                curacc = &accN;
            } else {
                SetWindowText(GetDlgItem(hWnd, IDC_CONN_MODE), "When Connected");
                curpos = posC;
                curacc = &accC;
            }
            /* Refresh the dialog settings */
            SendMessage(hWnd, WM_SHOWWINDOW, TRUE, 0);
#endif
            break;

        case IDOK:
            if (IsWindowEnabled(GetDlgItem(BMG(propertyWnd), BDM_APPLY)) == FALSE)
                break;
            /* Drop down to apply changes */

        case BDM_APPLY:
            BCC(setAccessibleModeC_info).inqInterval = posC[TBACC_INQ_INT];
            BCC(setAccessibleModeNC_info).inqInterval = posN[TBACC_INQ_INT];
            BCC(setAccessibleModeC_info).inqWindow = posC[TBACC_INQ_WND];
            BCC(setAccessibleModeNC_info).inqWindow = posN[TBACC_INQ_WND];
            BCC(setAccessibleModeC_info).pageInterval = posC[TBACC_PAGE_INT];

            BCC(setAccessibleModeNC_info).pageInterval = posN[TBACC_PAGE_INT];
            BCC(setAccessibleModeC_info).pageWindow = posC[TBACC_PAGE_WND];
            BCC(setAccessibleModeNC_info).pageWindow = posN[TBACC_PAGE_WND];

            status = ME_SetAccessibleModeNC(accN, &BCC(setAccessibleModeNC_info));
            BmReport("ME_SetAccessibleMode() when not-connected returned %s.\n", pBT_Status(status));

#if BT_ALLOW_SCAN_WHILE_CON == XA_ENABLED
            status = ME_SetAccessibleModeC(accC, &BCC(setAccessibleModeC_info));
            BmReport("ME_SetAccessibleMode() when connected returned %s.\n", pBT_Status(status));
#endif /* BT_ALLOW_SCAN_WHILE_CON == XA_ENABLED */

            EnableWindow(GetDlgItem(BMG(propertyWnd), BDM_APPLY), FALSE);
            break;
        }
        break;
    }
    return 0;

    UpdateApplyButton:
    /* Enable the "Apply" button if any changes have occurred. */
    if ((posC[TBACC_INQ_INT] != BCC(setAccessibleModeC_info).inqInterval) ||
        (posN[TBACC_INQ_INT] != BCC(setAccessibleModeNC_info).inqInterval) ||
        (posC[TBACC_INQ_WND] != BCC(setAccessibleModeC_info).inqWindow) ||
        (posN[TBACC_INQ_WND] != BCC(setAccessibleModeNC_info).inqWindow) ||
        (posC[TBACC_PAGE_INT] != BCC(setAccessibleModeC_info).pageInterval) ||
        (posN[TBACC_PAGE_INT] != BCC(setAccessibleModeNC_info).pageInterval) ||
        (posC[TBACC_PAGE_WND] != BCC(setAccessibleModeC_info).pageWindow) ||
        (posN[TBACC_PAGE_WND] != BCC(setAccessibleModeNC_info).pageWindow))
        modified = TRUE;

    if (curacc == &accN)
        ME_GetAccessibleModeNC(&tempacc, 0);
#if BT_ALLOW_SCAN_WHILE_CON == XA_ENABLED
    else ME_GetAccessibleModeC(&tempacc, 0);
#endif
    if (tempacc != *curacc)
        modified = TRUE;

    EnableWindow(GetDlgItem(BMG(propertyWnd), BDM_APPLY), modified);
    return 0;
}


/*
 * Window procedure for the Hold/Park/Sniff property page.
 */
static BOOL CALLBACK TabDlgIntervals(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    const trackbarInfo tb[] = {
        /* trackbar id       buddy id      label id            min     max     inc  ticfreq pagesz  btdefault */
        { IDC_HOLD_SPIN_MIN, IDC_HOLD_MIN, IDC_HOLD_LABEL_MIN, 0x0001, 0xFFFF, 625, 0x0FFE, 0x0320, 0},  /* Hold Interval Min */
        { IDC_HOLD_SPIN_MAX, IDC_HOLD_MAX, IDC_HOLD_LABEL_MAX, 0x0001, 0xFFFF, 625, 0x0FFE, 0x0320, 0},  /* Hold Interval Max */
        { IDC_PARK_SPIN_MIN, IDC_PARK_MIN, IDC_PARK_LABEL_MIN, 0x0001, 0xFFFF, 625, 0x0FFE, 0x0320, 0},  /* Park Interval Min */
        { IDC_PARK_SPIN_MAX, IDC_PARK_MAX, IDC_PARK_LABEL_MAX, 0x0001, 0xFFFF, 625, 0x0FFE, 0x0320, 0},  /* Park Interval Max */
        { IDC_SNIF_SPIN_MIN, IDC_SNIF_MIN, IDC_SNIF_LABEL_MIN, 0x0001, 0xFFFF, 625, 0x0FFE, 0x0320, 0},  /* Sniff Interval Min */
        { IDC_SNIF_SPIN_MAX, IDC_SNIF_MAX, IDC_SNIF_LABEL_MAX, 0x0001, 0xFFFF, 625, 0x0FFE, 0x0320, 0},  /* Sinff Interval Max */
        { IDC_SNIF_SPIN_ATT, IDC_SNIF_ATT, IDC_SNIF_LABEL_ATT, 0x0001, 0x7FFF, 625, 0x07FE, 0x0320, 0},  /* Sniff Interval Min */
        { IDC_SNIF_SPIN_TO , IDC_SNIF_TO , IDC_SNIF_LABEL_TO , 0x0000, 0x7FFF, 625, 0x07FE, 0x0320, 0},  /* Sinff Interval Max */
    };
    enum tb_id {
        TBINT_HOLD_MIN, TBINT_HOLD_MAX, TBINT_PARK_MIN, TBINT_PARK_MAX, TBINT_SNIFF_MIN, TBINT_SNIFF_MAX, TBINT_SNIFF_ATT, TBINT_SNIFF_TO, TBINT_COUNT
    };

    static U16  cur[TBINT_COUNT];
    long        value, pos;
    char        valstr[10];
    int         i;
    BOOL        modified = FALSE;

    switch (uMsg) {
    case WM_INITDIALOG:
        for (i = 0; i < TBINT_COUNT; i++) {
            SendDlgItemMessage(hWnd, tb[i].trackbar, TBM_SETRANGEMIN, 0, tb[i].min);
            SendDlgItemMessage(hWnd, tb[i].trackbar, TBM_SETRANGEMAX, 0, tb[i].max);
            SendDlgItemMessage(hWnd, tb[i].trackbar, TBM_SETTICFREQ, tb[i].ticfreq, 0);
            SendDlgItemMessage(hWnd, tb[i].trackbar, TBM_SETPAGESIZE, 0, tb[i].pagesize);
            SetWindowLong(GetDlgItem(hWnd, tb[i].trackbar), GWL_USERDATA, i);
        }
        break;

    case WM_RELOAD:
        /* Load current values for all modes. */
        cur[TBINT_HOLD_MIN] = BCC(hold_min);
        cur[TBINT_HOLD_MAX] = BCC(hold_max);
        cur[TBINT_PARK_MIN] = BCC(startPark_min);
        cur[TBINT_PARK_MAX] = BCC(startPark_max);
        cur[TBINT_SNIFF_MIN] = BCC(startSniff_info).minInterval;
        cur[TBINT_SNIFF_MAX] = BCC(startSniff_info).maxInterval;
        cur[TBINT_SNIFF_ATT] = BCC(startSniff_info).attempt;
        cur[TBINT_SNIFF_TO] = BCC(startSniff_info).timeout;
        /* Drop into SHOWWINDOW handling */

    case WM_SHOWWINDOW:
        for (i = 0; i < TBINT_COUNT; i++) {
            SendMessage(GetDlgItem(hWnd, tb[i].trackbar), TBM_SETPOS, TRUE, cur[i]);
            SendMessage(hWnd, WM_HSCROLL, 0, (LPARAM)GetDlgItem(hWnd, tb[i].trackbar));
        }
        goto UpdateApplyButton;

    case WM_HSCROLL:
        i = GetWindowLong((HWND)lParam, GWL_USERDATA);
        pos = SendMessage((HWND)lParam, TBM_GETPOS, 0, 0);

        value = pos * tb[i].increment;
        if (i == TBINT_SNIFF_ATT)
            value = ((pos * 2) - 1) * tb[i].increment;
        if (i == TBINT_SNIFF_TO)
            value = (pos > 0 ? ((pos * 2) - 1) * tb[i].increment : 0);
        sprintf(valstr, "%d.%06d", (value / 1000000), (value % 1000000));
        SendDlgItemMessage(hWnd, tb[i].buddy, WM_SETTEXT, 0, (LPARAM)valstr);

        /* Update the locally cached value */
        cur[i] = (U16)pos;

        if ((i == TBINT_HOLD_MIN) || (i == TBINT_PARK_MIN) || (i == TBINT_SNIFF_MIN)) {
            /* If the minimum is greater than the maximum, move the maximum up too. */
            if (cur[i] > cur[i+1]) {
                SendMessage(GetDlgItem(hWnd, tb[i+1].trackbar), TBM_SETPOS, TRUE, cur[i]);
                SendMessage(hWnd, WM_HSCROLL, 0, (LPARAM)GetDlgItem(hWnd, tb[i+1].trackbar));
            }
        } else if ((i == TBINT_HOLD_MAX) || (i == TBINT_PARK_MAX) || (i == TBINT_SNIFF_MAX)) {
            /* If the maximum is less than the minimum, move the minimum down too. */
            if (cur[i] < cur[i-1]) {
                SendMessage(GetDlgItem(hWnd, tb[i-1].trackbar), TBM_SETPOS, TRUE, cur[i]);
                SendMessage(hWnd, WM_HSCROLL, 0, (LPARAM)GetDlgItem(hWnd, tb[i-1].trackbar));
            }
        }

        UpdateApplyButton:
        /* Enable the "Apply" button if any changes have occurred. */
        if ((cur[TBINT_HOLD_MIN] != BCC(hold_min)) || (cur[TBINT_HOLD_MAX] != BCC(hold_max)) ||
            (cur[TBINT_PARK_MIN] != BCC(startPark_min)) || (cur[TBINT_PARK_MAX] != BCC(startPark_max)) ||
            (cur[TBINT_SNIFF_MIN] != BCC(startSniff_info).minInterval) ||
            (cur[TBINT_SNIFF_MAX] != BCC(startSniff_info).maxInterval) ||
            (cur[TBINT_SNIFF_ATT] != BCC(startSniff_info).attempt) || 
            (cur[TBINT_SNIFF_TO] != BCC(startSniff_info).timeout))
            modified = TRUE;

        EnableWindow(GetDlgItem(BMG(propertyWnd), BDM_APPLY), modified);
        break;

    case WM_NOTIFY:
        TabOnNotify((NMHDR *)lParam);
        break;

    case WM_COMMAND:
        switch (LOWORD(wParam)) {
        case IDOK:
            if (IsWindowEnabled(GetDlgItem(BMG(propertyWnd), BDM_APPLY)) == FALSE)
                break;
            /* Drop down to apply changes */

        case BDM_APPLY:
            BCC(hold_min) = cur[TBINT_HOLD_MIN];
            BCC(hold_max) = cur[TBINT_HOLD_MAX];
            BCC(startPark_min) = cur[TBINT_PARK_MIN];
            BCC(startPark_max) = cur[TBINT_PARK_MAX];
            BCC(startSniff_info).minInterval = cur[TBINT_SNIFF_MIN];
            BCC(startSniff_info).maxInterval = cur[TBINT_SNIFF_MAX];
            BCC(startSniff_info).attempt = cur[TBINT_SNIFF_ATT];
            BCC(startSniff_info).timeout = cur[TBINT_SNIFF_TO];

            EnableWindow(GetDlgItem(BMG(propertyWnd), BDM_APPLY), FALSE);
            break;
        }
        break;
    }
    return 0;
}

/*
 * Window procedure for the Link policy property page.
 */
static BOOL CALLBACK TabDlgPolicy(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    const char *secmsg[] = {"No Bluetooth Security\nAuthentication and Encryption are disabled.",
        "Bluetooth Security Level 1\nRespond only. Do not initiate Security.",
        "Bluetooth Security Level 2\nAuthentication is performed if user "
        "requested or if a Security Record is present for the service.",
        "Bluetooth Security Level 3\nAuthentication required to establish "
        "a device connection.",
        "Bluetooth Security Level 3\nAuthentication required to establish a "
        "device connection. All communications between devices are encrypted."};

    static BtSecurityMode secmode;
    static BtLinkPolicy   inpolicy, outpolicy, *curpolicy, option;
    BtStatus    status;
    long        pos;
    BOOL        modified;

    switch (uMsg) {
    case WM_INITDIALOG:
        SendDlgItemMessage(hWnd, IDC_SECURITY_SPIN, TBM_SETRANGEMIN, 0, 1);
        SendDlgItemMessage(hWnd, IDC_SECURITY_SPIN, TBM_SETRANGEMAX, 0, 5);
        SendDlgItemMessage(hWnd, IDC_SECURITY_SPIN, TBM_SETTICFREQ, 1, 0);
        SendDlgItemMessage(hWnd, IDC_SECURITY_SPIN, TBM_SETPAGESIZE, 0, 1);
#if BT_SECURITY == XA_DISABLED
        EnableWindow(GetDlgItem(hWnd, IDC_SECURITY_SPIN), FALSE);
#endif
        if (BLP_MASK & BLP_SCATTER_MODE) {
            EnableWindow(GetDlgItem(hWnd, IDC_SCATTER), FALSE);
        }
        break;

    case WM_RELOAD:
        /* Collect current settings */
        secmode = BMG(securityMode);
        inpolicy = BCC(setLinkPolicy_inACL);
        outpolicy = BCC(setLinkPolicy_outACL);
        curpolicy = &inpolicy;

        CheckRadioButton(hWnd, IDC_INBOUND, IDC_OUTBOUND, IDC_INBOUND);
        /* Drop into SHOWWINDOW handling */

    case WM_SHOWWINDOW:
        SendDlgItemMessage(hWnd, IDC_SECURITY_SPIN, TBM_SETPOS, TRUE, secmode);
        SendMessage(hWnd, WM_HSCROLL, 0, (LPARAM)GetDlgItem(hWnd, IDC_SECURITY_SPIN));

        SendDlgItemMessage(hWnd, IDC_MSSWITCH, BM_SETCHECK, 
                           (*curpolicy & BLP_MASTER_SLAVE_SWITCH ? BST_CHECKED : BST_UNCHECKED), 0L);
        SendDlgItemMessage(hWnd, IDC_HOLD, BM_SETCHECK, 
                           (*curpolicy & BLP_HOLD_MODE ? BST_CHECKED : BST_UNCHECKED), 0L);
        SendDlgItemMessage(hWnd, IDC_PARK, BM_SETCHECK, 
                           (*curpolicy & BLP_PARK_MODE ? BST_CHECKED : BST_UNCHECKED), 0L);
        SendDlgItemMessage(hWnd, IDC_SNIFF, BM_SETCHECK, 
                           (*curpolicy & BLP_SNIFF_MODE ? BST_CHECKED : BST_UNCHECKED), 0L);
        SendDlgItemMessage(hWnd, IDC_SCATTER, BM_SETCHECK, 
                           (*curpolicy & BLP_SCATTER_MODE ? BST_CHECKED : BST_UNCHECKED), 0L);
        goto UpdateApplyButton;

    case WM_HSCROLL:
        pos = SendMessage((HWND)lParam, TBM_GETPOS, 0, 0);
        SendDlgItemMessage(hWnd, IDC_SECURITY_DESC, WM_SETTEXT, 0, (LPARAM)secmsg[pos-1]);
        secmode = (BtSecurityMode)pos;
        goto UpdateApplyButton;

    case WM_NOTIFY:
        TabOnNotify((NMHDR *)lParam);
        break;

    case WM_COMMAND:
        switch (LOWORD(wParam)) {
        case IDC_MSSWITCH:
            option = BLP_MASTER_SLAVE_SWITCH;
            goto updatePolicy;

        case IDC_HOLD:
            option = BLP_HOLD_MODE;
            goto updatePolicy;

        case IDC_PARK:
            option = BLP_PARK_MODE;
            goto updatePolicy;

        case IDC_SNIFF:
            option = BLP_SNIFF_MODE;

        case IDC_SCATTER:
            option = BLP_SCATTER_MODE;

            updatePolicy:
            if (IsDlgButtonChecked(hWnd, LOWORD(wParam)) == BST_CHECKED)
                *curpolicy |= option;
            else *curpolicy &= ~option;
            goto UpdateApplyButton;

        case IDC_INBOUND:
            curpolicy = &inpolicy;
            /* Refresh the dialog settings */
            SendMessage(hWnd, WM_SHOWWINDOW, TRUE, 0);
            break;

        case IDC_OUTBOUND:
            curpolicy = &outpolicy;
            /* Refresh the dialog settings */
            SendMessage(hWnd, WM_SHOWWINDOW, TRUE, 0);
            break;

        case IDOK:
            if (IsWindowEnabled(GetDlgItem(BMG(propertyWnd), BDM_APPLY)) == FALSE)
                break;
            /* Drop down to apply changes */

        case BDM_APPLY:
#if BT_SECURITY == XA_ENABLED
            if (secmode != BMG(securityMode)) {
                status = BMG_SetSecurityMode(secmode);
                BmReport("BMG_SetSecurityMode() to level %d returned %s.\n", secmode, pBT_Status(status));
            }
#endif
            if ((inpolicy != BCC(setLinkPolicy_inACL)) || (outpolicy != BCC(setLinkPolicy_outACL))) {
                /* Apply the link policy change */
                status = ME_SetDefaultLinkPolicy(inpolicy, outpolicy);
                BmReport("ME_SetDefaultLinkPolicy() returned %s.\n", pBT_Status(status));
                if (status == BT_STATUS_SUCCESS) {
                    BCC(setLinkPolicy_outACL) = outpolicy;
                    BCC(setLinkPolicy_inACL) = inpolicy;
                }
            }
            EnableWindow(GetDlgItem(BMG(propertyWnd), BDM_APPLY), FALSE);
            break;
        }
        break;
    }
    return 0;

    UpdateApplyButton:
    modified = FALSE;

    /* Enable the "Apply" button if any changes have occurred. */
    if ((secmode != BMG(securityMode)) || (inpolicy != BCC(setLinkPolicy_inACL)) ||
        (outpolicy != BCC(setLinkPolicy_outACL)))
        modified = TRUE;

    EnableWindow(GetDlgItem(BMG(propertyWnd), BDM_APPLY), modified);
    return 0;
}


/*
 * Window procedure for the Bluetooth manager property page.
 */
static BOOL CALLBACK TabDlgMore(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    const char *iacstr[] = { "General/Unlimited (0x009E8B33)", "Limited/Dedicated (0x009E8B00)", 0};
    const BtIac iacval[] = { BT_IAC_GIAC, BT_IAC_LIAC};

    const char *inqstr[] = { "No Remote Device Name Lookup", "Lookup remote device names after Inquiry completes", "Lookup remote device names while Inquiry is in progress", 0};
    const U8    inqval[] = { BMG_NO_NAMEREQ, BMG_DO_NAMEREQ, BMG_DO_NAMEREQ_INSEQ};

    static BtConnectionRole   connpolicy, curpolicy;
    static BOOL     fixedpin, pinchanged;
    static U8       inqmode, refresh;
    static BtIac    iac;
    long            i;
    BOOL            modified;

    switch (uMsg) {
    case WM_INITDIALOG:
        for (i = 0; iacstr[i]; i++)
            SendDlgItemMessage(hWnd, IDC_IAC, CB_ADDSTRING, 0, (LPARAM)iacstr[i]);
        for (i = 0; inqstr[i]; i++)
            SendDlgItemMessage(hWnd, IDC_INQMODE, CB_ADDSTRING, 0, (LPARAM)inqstr[i]);
        break;

    case WM_RELOAD:
        /* Collect current settings */
        /* Set the conn policy to get the existing value, then restore it. */
        curpolicy = connpolicy = ME_SetConnectionRole(BCR_ANY);
        ME_SetConnectionRole(curpolicy);
        inqmode = (BCC(inquiryMode) & ~BMG_AUTO_REFRESH);
        refresh = (BCC(inquiryMode) & BMG_AUTO_REFRESH);
        iac = BCC(inquiry_iac);
        fixedpin = (BCC(setPin_len) != 0);
        pinchanged = FALSE;

        CheckDlgButton(hWnd, IDC_FIXED_PIN_CHECK, (fixedpin ? BST_CHECKED : BST_UNCHECKED));
        SendDlgItemMessage(hWnd, IDC_PIN_CODE, WM_SETTEXT, 0, (LPARAM)BCC(setPin_pin));
        EnableWindow(GetDlgItem(hWnd, IDC_PIN_CODE), (fixedpin ? TRUE : FALSE));

        CheckDlgButton(hWnd, IDC_AUTO_REFRESH, (refresh ? BST_CHECKED : BST_UNCHECKED));
        CheckDlgButton(hWnd, IDC_MASTER_ONLY, (connpolicy == BCR_MASTER ? BST_CHECKED : BST_UNCHECKED));

        for (i = 0; iacstr[i]; i++) {
            if (iacval[i] == iac)
                SendDlgItemMessage(hWnd, IDC_IAC, CB_SETCURSEL, i, 0);
        }
        for (i = 0; inqstr[i]; i++) {
            if (inqval[i] == inqmode)
                SendDlgItemMessage(hWnd, IDC_INQMODE, CB_SETCURSEL, i, 0);
        }
        /* Drop into SHOWWINDOW handling */

    case WM_SHOWWINDOW:
        goto UpdateApplyButton;

    case WM_NOTIFY:
        TabOnNotify((NMHDR *)lParam);
        break;

    case WM_COMMAND:
        switch (LOWORD(wParam)) {
        case IDC_FIXED_PIN_CHECK:
            if (IsDlgButtonChecked(hWnd, LOWORD(wParam)) == BST_CHECKED) {
                fixedpin = TRUE;
                EnableWindow(GetDlgItem(hWnd, IDC_PIN_CODE), TRUE);
            } else {
                fixedpin = FALSE;
                EnableWindow(GetDlgItem(hWnd, IDC_PIN_CODE), FALSE);
            }
            goto UpdateApplyButton;

        case IDC_MASTER_ONLY:
            if (IsDlgButtonChecked(hWnd, LOWORD(wParam)) == BST_CHECKED) {
                connpolicy = BCR_MASTER;
            } else {
                connpolicy = BCR_ANY;
            }
            goto UpdateApplyButton;

        case IDC_AUTO_REFRESH:
            if (IsDlgButtonChecked(hWnd, LOWORD(wParam)) == BST_CHECKED)
                refresh = BMG_AUTO_REFRESH;
            else refresh = 0;
            goto UpdateApplyButton;

        case IDC_IAC:
            i = SendDlgItemMessage(hWnd, IDC_IAC, CB_GETCURSEL, 0, 0);
            iac = iacval[i];
            goto UpdateApplyButton;

        case IDC_INQMODE:
            i = SendDlgItemMessage(hWnd, IDC_INQMODE, CB_GETCURSEL, 0, 0);
            inqmode = inqval[i];
            goto UpdateApplyButton;

        case IDC_PIN_CODE:
            if (HIWORD(wParam) != EN_CHANGE)
                break;

            pinchanged = TRUE;
            goto UpdateApplyButton;

        case IDOK:
            if (IsWindowEnabled(GetDlgItem(BMG(propertyWnd), BDM_APPLY)) == FALSE)
                break;
            /* Drop down to apply changes */

        case BDM_APPLY:
            if (fixedpin && pinchanged) {
                BMG(fixedPin)[0] = MAX_PIN_LEN;
                SendDlgItemMessage(hWnd, IDC_PIN_CODE, EM_GETLINE, 0, (LPARAM)BMG(fixedPin));
                BCC(setPin_pin) = BMG(fixedPin);
                BCC(setPin_len) = (U8)OS_StrLen(BMG(fixedPin));
                pinchanged = FALSE;
            } else if (fixedpin == FALSE)
                BCC(setPin_len) = 0;

            BCC(inquiryMode) = (inqmode|refresh);
            BCC(inquiry_iac) = iac;
            ME_SetConnectionRole(connpolicy);
            curpolicy = connpolicy;

            EnableWindow(GetDlgItem(BMG(propertyWnd), BDM_APPLY), FALSE);
            break;
        }
        break;
    }
    return 0;

    UpdateApplyButton:
    modified = FALSE;

    /* Enable the "Apply" button if any changes have occurred. */
    if ((connpolicy != curpolicy) || (fixedpin && pinchanged) ||
        ((inqmode|refresh) != BCC(inquiryMode)) || (iac != BCC(inquiry_iac)) || 
        (fixedpin != (BCC(setPin_len) != 0)))
        modified = TRUE;

    EnableWindow(GetDlgItem(BMG(propertyWnd), BDM_APPLY), modified);
    return 0;
}


/*
 * Process WM_NOTIFY messages for the property pages.
 */
static void TabOnNotify(NMHDR *Nmh)
{
    int     i;

    switch (Nmh->code) {
    case TCN_SELCHANGING:
        i = TabCtrl_GetCurSel(GetDlgItem(BMG(propertyWnd), PROP_TABS));
        ShowWindow(Tabs[i].handle, SW_HIDE);
        break;

    case TCN_SELCHANGE: 
        i = TabCtrl_GetCurSel(GetDlgItem(BMG(propertyWnd), PROP_TABS));
        ShowWindow(Tabs[i].handle, SW_SHOW);
        break;
    }
}

/*
 * Registers the control with the tooltip control. The lParam is a ponter to the
 * applications GetToolTipText() function.
 */
static BOOL CALLBACK AddTTipEnumProc(HWND hwndCtrl, LPARAM lParam) 
{ 
    TOOLINFO ti = {0}; 

    /* Skip controls that don't have an identifier (typically Group Boxes). */
    if (GetDlgCtrlID((HWND)hwndCtrl) > 0) {
/*        {
            char     szClass[64]; 
            GetClassName(hwndCtrl, szClass, sizeof(szClass)); 
            Report(("AddTTip: CtrlId %d (%s), lParam %x\n", GetDlgCtrlID((HWND)hwndCtrl), szClass, lParam));
        }
*/
        ti.cbSize = sizeof(TOOLINFO); 
        ti.uFlags = TTF_IDISHWND|TTF_SUBCLASS;
        ti.hwnd = BMG(chooserWnd);
        ti.uId = (UINT)hwndCtrl; 
        ti.lpszText = LPSTR_TEXTCALLBACK;
        ti.lParam = lParam;         /* Application's tool tip text function */
        AssertEval(SendMessage(BMG(toolTipWnd), TTM_ADDTOOL, 0, (LPARAM)&ti) == TRUE);
    }
    return TRUE; 
} 

/* 
 * Provides the tooltip control with the appropriate text to  display for a
 * control. This function is called by the chooser dialog procedure in 
 * response to a WM_NOTIFY message. 
 */
static void OnToolTip(NMHDR *Nmh)
{ 
    TOOLTIPTEXT *lpttt; 
    int          idCtrl; 

    Assert(Nmh->code == TTN_GETDISPINFO);

    idCtrl = GetDlgCtrlID((HWND)(Nmh->idFrom));
    lpttt = (TOOLTIPTEXT *)Nmh; 

    if (lpttt->lParam == 0) {
        /* The request is for a Bluetooth Manager GUI control. */
        switch (idCtrl) {
        /* Chooser Window controls */
        case IDC_OK: 
            lpttt->lpszText = "Ok"; 
            break;
        case IDC_CANCEL: 
            SendMessage(GetDlgItem(BMG(chooserWnd), IDC_CANCEL), WM_GETTEXT, sizeof(lpttt->szText), (LPARAM)lpttt->szText);
            break;
        case IDC_INQUIRE: 
            lpttt->lpszText = "Start or Stop a Bluetooth device inquiry."; 
            break;
        case IDC_MORE:
            SendMessage(GetDlgItem(BMG(chooserWnd), IDC_MORE), WM_GETTEXT, sizeof(lpttt->szText), (LPARAM)lpttt->szText);
            if (strcmp(lpttt->szText, "More >>") == 0)
                lpttt->lpszText = "Show function call status and event message window.";
            else lpttt->lpszText = "Hide function call status and event message window.";
            *lpttt->szText = 0;
            break;
        case IDC_ACCESSIBILITY:
            lpttt->lpszText = "Bluetooth Radio Accessibility (Page & Inquiry Scan) mode";
            break;
        case IDC_RADIO_STATUS:
            lpttt->lpszText = "Bluetooth Radio status";
            break;
        case BDM_DEVICE_LIST:
            lpttt->lpszText = "List of known devices. Right click on a device to view the device options menu.";
            break;
            /* Property page Controls */
        case IDC_MSSWITCH:
            lpttt->lpszText = "Allow the Link Manager to accept a Master/Slave role switch.";
            break;
        case BDM_APPLY:
            lpttt->lpszText = "Apply the changes to this property page. Does not apply changes to any other pages.";
            break;
        }
    } else {
        /* Window specific GetToolTipText() function is in lpttt->lParam */
        lpttt->lpszText = ((LPTSTR (*)(int))lpttt->lParam)(idCtrl);
    } 
    return; 
}

