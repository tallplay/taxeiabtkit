/***************************************************************************
 *
 * File:
 *     $Workfile:sniffer_ui.c$ for iAnywhere Blue SDK, Version 2.1.1
 *
 * Description:
 *     Source code for the sniffer user interface in windows.
 *
 * Copyright 1999-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#define STRICT
#include "hcitrans.h"
#include "sys/l2cap_i.h"
#include "sniffer.h"
#include "sniffer_ui.h"

#include "windows.h"
#include "commctrl.h"
#include "commdlg.h"
#include "stdio.h"
#include "bluemgrrc.h"

//#if XA_DEBUG == XA_ENABLED && HCI_ALLOW_PRESCAN == XA_ENABLED
#if HCI_ALLOW_PRESCAN == XA_ENABLED
static void SnifferMsg(const char *Msg);
static BOOL CALLBACK SnifferWndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
static BOOL ProcessHciPacket(const HciPacket *Packet);
static BOOL ProcessHciBuffer(const HciBuffer *Buffer);


typedef struct _MiniSniffer {
    /* Sniffer Window options */
    BOOL            FollowTail;

    /* Sniffer output options */
    BOOL            ShowRawL2Cap;
    BOOL            ShowHciEvents;
    BOOL            ShowHciCommands;
    BOOL            ShowAclRxData;
    BOOL            ShowAclTxData;
    BOOL            ShowScoRxData;
    BOOL            ShowScoTxData;

    HciPrescanHandler hciPrescan;

    BOOL            On;

    HWND            wnd;
    int             listBox;
    LPCSTR          wndTemplate;
    char            filename[_MAX_PATH];

    HANDLE          file;

} MiniSniffer;

/* Global Sniffer MiniOS UI Context and Protocol decode state. */
MiniSniffer         Sniffer = {0};

static  char *pMonth[] = { "", "January", "February", "March", "April", "May", 
                           "June", "July", "August", "September", "October",
                           "November", "December" };

#define SNIFF_COLORS XA_ENABLED

/*****************************************************************************
 * 
 *                Sniffer Dialog box management functions
 *
 *****************************************************************************/
/*---------------------------------------------------------------------------
 * StartSniffer
 *
 *     This function is called to start sniffing. The sniffer stores all
 *     output in the file ".bttrace". The data is read from this file and
 *     displayed in the sniffer listbox by the sniffer window handler
 *     WM_DRAWITEM handler.
 */
HWND StartSniffer(HINSTANCE Instance, int CmdShow)
{
    char                buffer[250], *name;
    DWORD               written;
    SYSTEMTIME          time;
    SniffDispHandler    handler = SnifferMsg;
    HANDLE              file = INVALID_HANDLE_VALUE;

    if (Sniffer.On == TRUE) {
        return Sniffer.wnd;
    }
    Assert(Sniffer.file == 0);

    /* Create the sniffer window or reset it if one exists. */
    if (Sniffer.wnd == NULL) {
        Sniffer.wnd = CreateDialog(Instance, MAKEINTRESOURCE(SNIFFER_MAIN), NULL, SnifferWndProc);
    } else {
        SendDlgItemMessage(Sniffer.wnd, SNIFFER_LISTBOX, LB_RESETCONTENT, 0, 0);
    }

    if (Sniffer.wnd == NULL) {
        /* Failed to start the sniffer window. */
        return 0;
    }

    SNIFFER_Reset(FALSE);
    SNIFFER_SetOption(SO_DISPLAY_HANDLER, &handler);

    /* Open a temporary file in the users TEMP directory for writing. */
    while (file == INVALID_HANDLE_VALUE) {
        name = _tempnam(0, "BtSniff");
        strcpy(Sniffer.filename, name);
        strcat(Sniffer.filename, ".tmp");
        free(name);

        /* This Win32 call is similar to fopen() but with more control. */
        file = CreateFile(Sniffer.filename, GENERIC_READ|GENERIC_WRITE, 
                          FILE_SHARE_READ, 0, CREATE_ALWAYS, 
                          FILE_ATTRIBUTE_NORMAL|FILE_FLAG_RANDOM_ACCESS, 0);
    }
    Sniffer.file = file;

    /* Write sniffer start date & time */
    GetLocalTime(&time);
    sprintf(buffer, "Trace Generated at %02d:%02d:%02d%s on %s %d, %04d.\r\n\r\n",
            (time.wHour > 12 ? time.wHour - 12 : time.wHour),
            time.wMinute, time.wSecond, (time.wHour < 12 ? "am" : "pm"),
            pMonth[time.wMonth], time.wDay, time.wYear);

    WriteFile(Sniffer.file, buffer, strlen(buffer), &written, 0);   /* fwrite() */

    /* Register a prescan handler */
    Sniffer.hciPrescan.hciTxPrescan = ProcessHciPacket;
    Sniffer.hciPrescan.hciTxDone = NULL;
    Sniffer.hciPrescan.hciRxPrescan = ProcessHciBuffer;
    Sniffer.hciPrescan.hciRxDone = NULL;

    if (HCI_RegisterPrescan(&Sniffer.hciPrescan, PRESCAN_FLAG_FIRST) != XA_STATUS_SUCCESS) {
        CloseHandle(Sniffer.file);
        Sniffer.file = 0;
        return 0;
    }

    Sniffer.On = TRUE;

    Sniffer.FollowTail = TRUE;
    Sniffer.ShowRawL2Cap = FALSE;
    Sniffer.ShowHciEvents = TRUE;
    Sniffer.ShowHciCommands = TRUE;
    Sniffer.ShowAclRxData = TRUE;
    Sniffer.ShowAclTxData = TRUE;
    Sniffer.ShowScoRxData = TRUE;
    Sniffer.ShowScoTxData = TRUE;

    ShowWindow(Sniffer.wnd, CmdShow);

    return Sniffer.wnd;
}

/*---------------------------------------------------------------------------
 * Detatches the sniffer from the stack in advance of the stack being de-
 * initialized.
 */
void SnifferHandleDeinit(void)
{
    Assert(Sniffer.On);
    Assert(Sniffer.hciPrescan.node.Blink);

    /* Unregister prescan handler */
    HCI_UnregisterPrescan(&Sniffer.hciPrescan);

    /* Use the Blink member to indicate whether Prescan is registered */
    Sniffer.hciPrescan.node.Blink = NULL;
}

/*---------------------------------------------------------------------------
 * Re-attaches the sniffer to the stack in the case the stack was
 * re-initialized.
 */
BOOL SnifferHandleReinit(void)
{
    Assert(Sniffer.On);
    Assert(!Sniffer.hciPrescan.node.Blink);
    
    /* Re-register the prescan structure we created earlier. */
    if (HCI_RegisterPrescan(&Sniffer.hciPrescan, PRESCAN_FLAG_FIRST) != XA_STATUS_SUCCESS) {
        return FALSE;
    }
    SnifferReset(TRUE);
    return TRUE;
}

/*---------------------------------------------------------------------------
 * StopSniffer
 *
 *     This function stops the sniffer, closes the output file and hides
 *     the window.
 */
void StopSniffer(void)
{
    if (Sniffer.wnd != NULL)
        ShowWindow(Sniffer.wnd, SW_HIDE);

    /* Unregister prescan handler */
    if (Sniffer.hciPrescan.node.Blink) {
        HCI_UnregisterPrescan(&Sniffer.hciPrescan);
    }

    if (Sniffer.file) {
        CloseHandle(Sniffer.file);
        Sniffer.file = 0;
    }

    Sniffer.On = FALSE;
}


/*---------------------------------------------------------------------------
 * SnifferReset
 *
 *     This function resets the state of the sniffer packet decoder and
 *     if "Full == TRUE" the connection table is also reset.
 */
void SnifferReset(BOOL Full)
{
    SNIFFER_Reset(Full);
}


/*---------------------------------------------------------------------------
 * IsSnifferOpen
 *
 *     This function returns TRUE if the sniffer is active.
 */
BOOL IsSnifferOpen()
{
    return Sniffer.On;
}

/*---------------------------------------------------------------------------
 * GetSnifferWindow
 *
 *     This function returns the sniffer window handle.
 */
HWND GetSnifferWnd()
{
    if (Sniffer.On == TRUE)
        return Sniffer.wnd;

    return 0;
}

/*---------------------------------------------------------------------------
 * StartSnifferLogging
 *
 *     This function initializes the Sniffer in log file mode. It is used
 *     with command line and other non Windows GUI based applications.
 */
BOOL StartSnifferLogging(const char *Filename)
{
    char                buffer[250];
    SYSTEMTIME          time;
    SniffDispHandler    handler = SnifferMsg;
    DWORD               written;

    if (Sniffer.On == TRUE) {
        /* Sniffer already started. */
        return TRUE;
    }
    Assert(Sniffer.file == 0);

    SNIFFER_Reset(TRUE);
    SNIFFER_SetOption(SO_DISPLAY_HANDLER, &handler);

    /* This Win32 call is similar to fopen() but with more control. */
    Sniffer.file = CreateFile(Filename, GENERIC_READ|GENERIC_WRITE, 
                              FILE_SHARE_READ, 0, CREATE_ALWAYS, 
                              FILE_ATTRIBUTE_NORMAL|FILE_FLAG_RANDOM_ACCESS, 0);
    if (Sniffer.file == INVALID_HANDLE_VALUE) {
        Sniffer.file = 0;
        return FALSE;
    }

    /* Write test execution date & time */   
    GetLocalTime(&time);
    sprintf(buffer, "Trace Generated at %02d:%02d:%02d%s on %s %d, %04d.\r\n\r\n",
            (time.wHour > 12 ? time.wHour - 12 : time.wHour),
            time.wMinute, time.wSecond, (time.wHour < 12 ? "am" : "pm"),
            pMonth[time.wMonth], time.wDay, time.wYear);

    WriteFile(Sniffer.file, buffer, strlen(buffer), &written, 0);    /* fwrite() */

    /* Register a prescan handler */
    Sniffer.hciPrescan.hciTxPrescan = ProcessHciPacket;
    Sniffer.hciPrescan.hciTxDone = NULL;
    Sniffer.hciPrescan.hciRxPrescan = ProcessHciBuffer;
    Sniffer.hciPrescan.hciRxDone = NULL;

    if (HCI_RegisterPrescan(&Sniffer.hciPrescan, PRESCAN_FLAG_FIRST) != XA_STATUS_SUCCESS) {
        CloseHandle(Sniffer.file);
        Sniffer.file = 0;
        return 0;
    }

    Sniffer.On = TRUE;

    Sniffer.ShowRawL2Cap = FALSE;
    Sniffer.ShowHciEvents = TRUE;
    Sniffer.ShowHciCommands = TRUE;
    Sniffer.ShowAclRxData = TRUE;
    Sniffer.ShowAclTxData = TRUE;
    Sniffer.ShowScoRxData = TRUE;
    Sniffer.ShowScoTxData = TRUE;

    return TRUE;
}

#if XA_SNIFFER == XA_ENABLED
/*---------------------------------------------------------------------------
 * SnifferRegisterEndpoint
 *
 *     This function is called by protocol modules (RFCOMM, SDP, OBEX, etc.)
 *     to register new connections with the sniffer. The sniffer keeps track
 *     of all connections in order to properly decode data packets.
 */
void SnifferRegisterEndpoint(SniffProtocolId EndpointType, void *Endpoint, 
                             BtRemoteDevice *remDev, SniffProtocolId Protocol)
{
    SniffEndpoint se = {0};

    switch (EndpointType) {
    case SPI_L2CAP:
    case SPI_L2CAP_FEC:
        se.proto = EndpointType;
        se.l2cap = *(L2capChannelId *)Endpoint; /* CID */
        se.l2cap_remote = L2Cap_GetValidChannel(se.l2cap)->remoteCid;
        break;

    case SPI_RFCOMM:
        se.proto = SPI_RFCOMM;
        se.rfcomm = *(U8 *)Endpoint;   /* DLCI */
        break;
    }

    /* All endpoint types carry the HCI handle LMP-Id attribute */
    se.proto |= SPI_LM;
    se.lmp = remDev->hciHandle;

    se.type = Protocol;

    /* Register this endpoint with the sniffer. */
    SNIFFER_RegisterEndpoint(&se);
}
#endif

/*---------------------------------------------------------------------------
 * SnifferMsg
 *
 *     Write a formatted line of ouput to the sniffer log. This is the output
 *     function used by the decode functions in the core sniffer.
 */
void SnifferMsg(const char *Msg)
{
   DWORD    offset, len, written;
   int      nItem;

   Assert(Sniffer.file != 0);

   len = strlen(Msg);

   if (len > LINE_LENGTH)
       len = LINE_LENGTH;

   /* Seek to end of the file and write the message with a cr-lf terminator. */
   offset = SetFilePointer(Sniffer.file, 0, 0, FILE_END);   /* fseek() */
   WriteFile(Sniffer.file, Msg, len, &written, 0);          /* fwrite() */
   WriteFile(Sniffer.file, "\r\n", 2, &written, 0);

   /* Add the message (item) to the listbox. The file offset is automatically 
    * stored in the new item's "itemData" field.
    */
   if (Sniffer.wnd) {
       nItem = SendMessage(GetDlgItem(Sniffer.wnd, SNIFFER_LISTBOX), LB_ADDSTRING, 0, (LPARAM)offset);
       if (Sniffer.FollowTail == TRUE)
           SendMessage(GetDlgItem(Sniffer.wnd, SNIFFER_LISTBOX), LB_SETCARETINDEX, nItem, 0);
   }
}


/*---------------------------------------------------------------------------
 * ProcessHciPacket
 *
 *     This function passes a HCI transmit packet to the sniffer.
 */
BOOL ProcessHciPacket(const HciPacket *Packet)
{
    if (!Sniffer.On) return FALSE;
    
    if (((Packet->flags & HCI_BUFTYPE_COMMAND) && Sniffer.ShowHciCommands) ||
        ((Packet->flags & HCI_BUFTYPE_ACL_DATA) && Sniffer.ShowAclTxData) ||
        ((Packet->flags & HCI_BUFTYPE_SCO_DATA) && Sniffer.ShowScoTxData)) {

        SNIFFER_DisplayHciPacket(Packet, OS_GetSystemTime());
    }
    return FALSE;
}


/*---------------------------------------------------------------------------
 * ProcessHciBuffer
 *
 *     This function passes a HCI receive buffer to the sniffer.
 */
BOOL ProcessHciBuffer(const HciBuffer *Buffer)
{
    if (!Sniffer.On) return FALSE;
    
    if (((Buffer->flags & HCI_BUFTYPE_EVENT) && Sniffer.ShowHciEvents) ||
        ((Buffer->flags & HCI_BUFTYPE_ACL_DATA) && Sniffer.ShowAclRxData) ||
        ((Buffer->flags & HCI_BUFTYPE_SCO_DATA) && Sniffer.ShowScoRxData)) {

        SNIFFER_DisplayHciBuffer(Buffer, OS_GetSystemTime());
    }
    return FALSE;
}


/*---------------------------------------------------------------------------
 * DumpLmpPacket
 *
 *     This function dumps an INET-LMP packet.
 */
void DumpLmpPacket(const char *RxTx, const U8 *Packet, U16 Len)
{
    SNIFFER_DisplayLmpPacket(RxTx, Packet, Len, OS_GetSystemTime());
}


/*---------------------------------------------------------------------------
 * SnifferWndProc
 *
 *     Window procedure for managing the Sniffer GUI.
 */
BOOL CALLBACK SnifferWndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    static HWND     listWnd;
    static LONG     wOffset, hOffset, tmHeight;
    LONG            width, height;
    RECT            w_rect, l_rect;
    BOOL            checked;

    switch (uMsg) {

    case WM_SETFONT:
        {
            HDC         hdc = GetDC(GetDlgItem(hWnd, SNIFFER_LISTBOX));
            TEXTMETRIC  tm;
            HGDIOBJ     oldObj;

            /* Retrieve the font height for later. */
            oldObj = SelectObject(hdc, (void *)wParam);
            GetTextMetrics(hdc, &tm);
            SelectObject(hdc, (void *)oldObj);
            ReleaseDC(hWnd, hdc);

            tmHeight = tm.tmHeight;
        }
        break;

    case WM_SHOWWINDOW:
        if (wParam == FALSE)
            break;

    case WM_SETFOCUS:
        listWnd = GetDlgItem(hWnd, SNIFFER_LISTBOX);
        GetWindowRect(hWnd, &w_rect);
        GetWindowRect(listWnd, &l_rect);
        wOffset = (w_rect.right - l_rect.right);
        hOffset = (w_rect.bottom - l_rect.bottom);

        /* Initialize display options in menu */
        if (Sniffer.ShowRawL2Cap)
            CheckMenuItem(GetMenu(hWnd), ID_OPTIONS_RAWL2CAP, MF_BYCOMMAND|MF_CHECKED);
        if (Sniffer.ShowHciEvents)
            CheckMenuItem(GetMenu(hWnd), ID_OPTIONS_HCIEVENTS, MF_BYCOMMAND|MF_CHECKED);
        if (Sniffer.ShowHciCommands)
            CheckMenuItem(GetMenu(hWnd), ID_OPTIONS_HCICOMMANDS, MF_BYCOMMAND|MF_CHECKED);
        if (Sniffer.ShowAclRxData)
            CheckMenuItem(GetMenu(hWnd), ID_OPTIONS_ACLRX, MF_BYCOMMAND|MF_CHECKED);
        if (Sniffer.ShowAclTxData)
            CheckMenuItem(GetMenu(hWnd), ID_OPTIONS_ACLTX, MF_BYCOMMAND|MF_CHECKED);
        if (Sniffer.ShowScoRxData)
            CheckMenuItem(GetMenu(hWnd), ID_OPTIONS_SCORX, MF_BYCOMMAND|MF_CHECKED);
        if (Sniffer.ShowScoTxData)
            CheckMenuItem(GetMenu(hWnd), ID_OPTIONS_SCOTX, MF_BYCOMMAND|MF_CHECKED);
        if (Sniffer.FollowTail)
            CheckMenuItem(GetMenu(hWnd), ID_OPTIONS_TAIL, MF_BYCOMMAND|MF_CHECKED);
        break;

    case WM_SIZING:
    case WM_EXITSIZEMOVE:       /* Msg for Win95 */
        GetClientRect(hWnd, &w_rect);
        width = (w_rect.right - w_rect.left - wOffset);
        height = (w_rect.bottom - w_rect.top - hOffset);
        MoveWindow(listWnd, 5, 6, width, height, TRUE);     /* Origin (x,y) = 5,6 */
        break;
        
    case WM_MEASUREITEM: 
        {
            LPMEASUREITEMSTRUCT lpmis = (LPMEASUREITEMSTRUCT)lParam;

            lpmis->itemHeight = tmHeight;
        }
        return FALSE;

    case WM_DRAWITEM:
        {
            LPDRAWITEMSTRUCT    lpdis = (LPDRAWITEMSTRUCT)lParam;
            int                 y, len;
            DWORD               bkColor, textColor, read;
            char                data[LINE_LENGTH+2], ch = LINE_LENGTH;
            DWORD               hash = 0, color = 0x808080;

            /* If there are no list box items, skip this message. */
            if (lpdis->itemID == -1) {
                return TRUE;
            }
            
            /* Draw the text for the list box item. */
            if (lpdis->itemAction & (ODA_DRAWENTIRE|ODA_SELECT)) {
   
                /* Seek to offset in file of line start and count line length.
                 * These Win32 calls work like ANSI fseek() and fread().
                 */
                if (SetFilePointer(Sniffer.file, lpdis->itemData, 0, FILE_BEGIN) == 0xFFFFFFFF) return FALSE;
                if (ReadFile(Sniffer.file, data, LINE_LENGTH+2, &read, 0) == FALSE) return FALSE;

                /* Count length of the string (up to the <CR>).
                 *
                 * While were doing this, generate a hash for the protocol name
                 * so we can determine what color to ouput the text as.
                 */
                for (len = 0; (data[len] != '\r') && len < LINE_LENGTH+2; len++) {
#if SNIFF_COLORS == XA_ENABLED
                    if ((data[len] != ' ') && (color == 0x808080)) {
                        hash += data[len];

                        if (data[len] == ':') {
                            switch (hash) {
                            case 0x01FE:    // RFCOMM
                                color = 0x701919;       // Midnight Blue 
                                break;
                            case 0x018C:    // L2CAP
                                color = 0x2222B2;       // Firebrick
                                break;
                            case 0x01E3:    // ACL-TX
                            case 0x01E1:    // ACL-RX
                            case 0x020F:    // HCI-CMD
                            case 0x022A:    // HCI-EVT
                                color = 0x000000;       // Black
                                break;
                            case 0x0121:    // SDP
                                color = 0x808000;
                                break;
                            case 0x0168:    // OBEX
                                color = 0x71B33C;       // Medium Sea Green    
                                break;
                            case 0x0124:    // TCS
                                color = 0x13458B;       // Saddle Brown
                                break;
                            default:        // Usually Hex dump
                                color = 0x808081;       // Grey
                                break;
                            }
                        }
                    }
#endif
                }

                y = (lpdis->rcItem.bottom + lpdis->rcItem.top - tmHeight) / 2; 
 
                if (lpdis->itemState & ODS_SELECTED) {
                    /* Set the selected background and text colors. */
                    bkColor = SetBkColor(lpdis->hDC, GetSysColor(COLOR_HIGHLIGHT));
                    textColor = SetTextColor(lpdis->hDC, GetSysColor(COLOR_HIGHLIGHTTEXT));
#if SNIFF_COLORS == XA_ENABLED
                } else {
                    SetTextColor(lpdis->hDC, color);
#endif
                }

                ExtTextOut(lpdis->hDC, 0, y, (ETO_CLIPPED|ETO_OPAQUE), &lpdis->rcItem, data, len, 0);

                if (lpdis->itemState & ODS_SELECTED) {
                    /* Restore normal background color. */
                    bkColor = SetBkColor(lpdis->hDC, bkColor);
                }
            }
            return TRUE;
        }
        break;

    case WM_INITDIALOG:
        break;

    case WM_COMMAND:
        switch (LOWORD(wParam)) {
        case ID_SNIFFER_SAVEAS:
            /* Save the sniffer output */
            {
                OPENFILENAME   saveFile;
                char           filename[_MAX_PATH];

                strcpy(filename, "BtTrace.txt");
                
                /* Post dialog to get filename to save results in */
                memset( &saveFile, 0, sizeof(saveFile));
                saveFile.lStructSize = sizeof(saveFile);
                saveFile.hwndOwner = hWnd;
                saveFile.lpstrFile = filename;
                saveFile.lpstrFilter = "Text Files(*.txt)\0*.TXT\0All Files(*.*)\0*.*\0";
                saveFile.nMaxFile = _MAX_PATH;
                saveFile.Flags = OFN_OVERWRITEPROMPT;
                saveFile.lpstrTitle = TEXT("Save Results");

                if (GetSaveFileName(&saveFile) == 0)
                    break;
                
                /* Concatenates ".txt" to the file name if it has no extension */
                if (strchr(filename, '.') == 0)
                    strcat(filename, ".txt");

                if (CopyFile(Sniffer.filename, filename, FALSE) == 0)
                    MessageBox(hWnd, "File copy failed!", "Error", (MB_OK|MB_ICONEXCLAMATION));

            }
            break;

        case ID_SNIFFER_CLEAR:
            SendDlgItemMessage(Sniffer.wnd, SNIFFER_LISTBOX, LB_RESETCONTENT, 0, 0);
            break;

        case ID_OPTIONS_ACLTX:
        case ID_OPTIONS_ACLRX:
        case ID_OPTIONS_HCIEVENTS:
        case ID_OPTIONS_HCICOMMANDS:
        case ID_OPTIONS_RAWL2CAP:
        case ID_OPTIONS_SCORX:
        case ID_OPTIONS_SCOTX:
        case ID_OPTIONS_TAIL:
        case ID_OPTIONS_LMPPACKETS:
            if (GetMenuState(GetMenu(hWnd), LOWORD(wParam), MF_BYCOMMAND) & MFS_CHECKED) {
                // Toggle to unchecked
                CheckMenuItem(GetMenu(hWnd), LOWORD(wParam), MF_BYCOMMAND|MF_UNCHECKED);
                checked = FALSE;
            } else {
                // Toggle to checked
                CheckMenuItem(GetMenu(hWnd), LOWORD(wParam), MF_BYCOMMAND|MF_CHECKED);
                checked = TRUE;
            }

            switch (LOWORD(wParam)) {
            case ID_OPTIONS_ACLTX:
                Sniffer.ShowAclTxData = checked;
                break;
            case ID_OPTIONS_ACLRX:
                Sniffer.ShowAclRxData = checked;
                break;
            case ID_OPTIONS_HCIEVENTS:
                Sniffer.ShowHciEvents = checked;
                break;
            case ID_OPTIONS_HCICOMMANDS:
                Sniffer.ShowHciCommands = checked;
                break;
            case ID_OPTIONS_RAWL2CAP:
                Sniffer.ShowRawL2Cap = checked;

                /* This option state must be conveyed to the sniffer library. */
                {
                    SniffDispFilter filter;

                    SNIFFER_GetOption(SO_DISPLAY_FILTER, &filter);
                    if (checked)
                        filter |= SDF_DISPLAY_RAW;
                    else filter &= ~SDF_DISPLAY_RAW;

                    SNIFFER_SetOption(SO_DISPLAY_FILTER, &filter);
                }
                break;
            case ID_OPTIONS_SCORX:
                Sniffer.ShowScoRxData = checked;
                break;
            case ID_OPTIONS_SCOTX:
                Sniffer.ShowScoTxData = checked;
                break;
            case ID_OPTIONS_TAIL:
                Sniffer.FollowTail = checked;
                break;
            }
            break;
        }
    }
    return 0L;
}

#endif /* XA_DEBUG == XA_ENABLED && HCI_ALLOW_PRESCAN == XA_ENABLED */

