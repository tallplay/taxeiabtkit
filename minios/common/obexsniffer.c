/*****************************************************************************
 *
 * File:
 *     obexsniffer.c
 *
 * Description: 
 *     This file contains code for the OBEX Protocol Sniffer.
 *
 * Copyright 2002-2005 Extended Systems, Inc.  ALL RIGHTS RESERVED.
 *
 * Unpublished Confidential Information of Extended Systems, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any means, 
 * or stored in a database or retrieval system, without prior written 
 * permission of Extended Systems, Inc.
 * 
 * Use of this work is governed by a license granted by Extended Systems, Inc.  
 * This work contains confidential and proprietary information of Extended 
 * Systems, Inc. which is protected by copyright, trade secret, trademark and 
 * other intellectual property rights.
 *
 ****************************************************************************/

#define STRICT
#include <windows.h>
#include <commctrl.h>
#include "resource.h"
#include <stdio.h>
#include <sys/obxalloc.h>


/****************************************************************************
 *
 * Internal & External Function Prototypes
 *
 ****************************************************************************/
static void SaveResults(const char *filename);
static void SnifferMsg(const char *Msg);
static const char *pTimeOffset(const char *Msg);
static BOOL CALLBACK ObexSnifferWndProc( HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam );
#if JETTEST == XA_ENABLED
static void ObexSniffer(void *, ObexSniffType, const U8 *, U16 );
#endif /* JETTEST == XA_ENABLED */

/****************************************************************************
 *
 * RAM and ROM Data
 *
 ****************************************************************************/
#define LINE_WIDTH          16

#undef OBSC_RX_WAIT
#define OBSC_RX_WAIT        0
#define OBSC_RX_STAGED      1
#define OBSC_RX_IGNORE      2

static HWND     SniffWnd = NULL;
static int      SnifferListBox = OBEX_SNIFFER_LISTBOX;
static LPCSTR   SnifferWndTemplate = MAKEINTRESOURCE(OBEX_SNIFFER_MAIN);
       BOOL     SnifferOn = FALSE;

static SYSTEMTIME   StartTime;
static FILE    *SniffFile = 0;
static DWORD   localStart = 0;

static char    *output = NULL;


/* Type used to track a particular OBEX connection */
typedef struct _ObexContext {
    U16     packetLen;          /* Size of current OBEX packet */
    U16     curPacketLen;       /* Amount of current packet data received */
    U16     savedCurPacketLen;  /* Saved current packet length, in case 
                                 * a client abort is sent during a response
                                 */
    U16     savedPacketLen;     /* Saved OBEX packet length */
    U8      stageBuff[6];       /* OBEX Header stage buffer */
    U8      stageLen;           /* Num bytes to read into stageBuff */
    U8      stageOffset;        /* Num bytes in stageBuff */
    U8      rxState;            /* Receive parser state */
    U8      opcode;             /* Current Opcode */
    U8      rsp;                /* Current Response */
    BOOL    isCommand;          /* Is the packet a Command or Response */
    BOOL    inSync;             /* Is the parser sync'ed with the data */
    BOOL    seqNumHeader;       /* Do we have a Session Sequence Number Header */

    void    *appContext;        /* Used to match sniffer context with real OBEX context. */
} ObexContext;

ObexContext Op[50] = {0}; 

/*
 * Initialize an OBEX parser
 */
static void InitObexParser(ObexContext *obp)
{
    OS_MemSet((U8 *)obp, 0, sizeof(ObexContext));

    obp->rxState = OBSC_RX_WAIT;
    obp->seqNumHeader = FALSE;
    /* Stage the packet length and opcode */
    obp->stageLen = 3;
}

/****************************************************************************
 *
 * Public Functions
 *
 ****************************************************************************/

/*---------------------------------------------------------------------------
 *            StartObexSniffer()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Called to start sniffing. If "instance" is NULL, no window 
 *            is opened
 *
 * Return:    HWND
 *
 */
HWND StartObexSniffer(HINSTANCE Instance, int CmdShow)
{
    /* JetTest must be enabled to hook the sniffer. */
#if JETTEST == XA_ENABLED

    I8 i;

    if (Instance != NULL) {
        if (SniffWnd == NULL) {
            SniffWnd = CreateDialog(Instance, SnifferWndTemplate, NULL, ObexSnifferWndProc);
        }
        else SendDlgItemMessage(SniffWnd, SnifferListBox, LB_RESETCONTENT, 0, 0);
    }

    if ((Instance == NULL) || (SniffWnd != NULL)) {
        /* Re-initialize time delta */
        localStart = 0;

        for (i = 0; i < 50; i++)
            InitObexParser(&Op[i]);

        if (SniffWnd != NULL) {
            ShowWindow( SniffWnd, CmdShow );
        }
        SnifferOn = TRUE;
    }

    /* Hook the sniffer */
    OBD(sniffer) = ObexSniffer;
#endif /* JETTEST == XA_ENABLED */

    return SniffWnd;
}

/*---------------------------------------------------------------------------
 *            StopObexSniffer()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Called to stop sniffing.
 *
 * Return:    void
 *
 */
void StopObexSniffer(void)
{
    /* JetTest must be enabled to hook the sniffer. */
#if JETTEST == XA_ENABLED
    
    if (SniffWnd != NULL) {
        ShowWindow(SniffWnd, SW_HIDE);
    }

    if (SniffFile) {
        fclose(SniffFile);
        SniffFile = 0;
    }
    SnifferOn = FALSE;

    /* Unhook the sniffer */
    OBD(sniffer) = 0;
#endif /* JETTEST == XA_ENABLED */
}

/*---------------------------------------------------------------------------
 *            IsObexSnifferOpen()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Determines if the OBEX sniffer is currently enabled or disabled.
 *
 * Return:    TRUE or FALSE
 *
 */
BOOL IsObexSnifferOpen()
{
    return SnifferOn;
}


static BOOL CALLBACK ObexSnifferWndProc( HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam )
{
    static HWND     listWnd;
    static LONG     wOffset, hOffset;
    LONG            width, height;
    RECT            w_rect, l_rect;
    U16             i;

    switch (uMsg) {

    case WM_SETFOCUS:
        listWnd = GetDlgItem(hWnd, OBEX_SNIFFER_LISTBOX);
        GetWindowRect(hWnd, &w_rect);
        GetWindowRect(listWnd, &l_rect);
        wOffset = (w_rect.right - l_rect.right);
        hOffset = (w_rect.bottom - l_rect.bottom);

        /* Initialize display options in menu */
        break;

    case WM_SIZING:
    case WM_EXITSIZEMOVE:       // Msg for Win95
        GetClientRect(hWnd, &w_rect);
        width = (w_rect.right - w_rect.left - wOffset);
        height = (w_rect.bottom - w_rect.top - hOffset);
        MoveWindow(listWnd, 5, 6, width, height, TRUE);     // Origin (x,y) = 5,6
        break;
        
    case WM_COMMAND:
        switch (LOWORD(wParam)) {
        case ID_SNIFFER_SAVEAS:
            /* Save the sniffer output */
            {
                OPENFILENAME   saveFile;
                char           filename[256];

                strcpy(filename, "ObexTrace.txt");
                
                /* Post dialog to get filename to save results in */
                memset( &saveFile, 0, sizeof(saveFile));
                saveFile.lStructSize = sizeof(saveFile);
                saveFile.hwndOwner = hWnd;
                saveFile.lpstrFile = filename;
                saveFile.lpstrFilter = "Text Files(*.txt)\0*.TXT\0All Files(*.*)\0*.*\0";
                saveFile.nMaxFile = 256;
                saveFile.Flags = OFN_OVERWRITEPROMPT;
                saveFile.lpstrTitle = TEXT("Save Results");

                if (GetSaveFileName(&saveFile) == 0)
                    break;
                
                /* RWW: concatenates ".txt" to the file name has no extension */
                for (i = 0; i < strlen(filename); i++) {
                    if (filename[i] == '.') {
                        break;
                    }
                }
                if (i == strlen(filename)) {
                    strcat(filename, ".txt");
                }

                SaveResults(filename);
            }
            break;

        case ID_SNIFFER_CLEAR:
            SendDlgItemMessage(SniffWnd, SnifferListBox, LB_RESETCONTENT, 0, 0);
            break;
       }
    }
    return 0L;
}

void SnifferMsg(const char *Msg)
{
    char     buffer[200]; /* Output Buffer */
    int      index, len;
    va_list  args;

    /* Redirect output to the "output" string if it exists */
    if (output != NULL) {       
        strcat(output, Msg);
        strcat(output, "\n");
        return;
    }

    Assert((SniffWnd || SniffFile) && !(SniffWnd && SniffFile));

    if (SniffWnd) {
        va_start(args, Msg);
        index = _vsnprintf(buffer, 200, Msg, args);
        va_end(args);

        index = SendDlgItemMessage(SniffWnd, SnifferListBox, LB_ADDSTRING, 0, (LPARAM)buffer);
        if (index != LB_ERR) {
            SendDlgItemMessage(SniffWnd, SnifferListBox, LB_SETCARETINDEX, (WPARAM)index, FALSE);
        }
    }

    len = strlen(Msg);

    if (SniffFile) {
        fwrite(Msg, 1, len, SniffFile);
        fwrite("\r\n", 1, 2, SniffFile);
    }
}


/*
 * Functions for converting OBEX Opcodes and common response values into strings
 */
static const char *pObexRsp(U8 RespCode)
{
    static char buff[20];

    switch (RespCode & 0x7F) {
    case 0x10: return "Continue";
    case 0x20: return "Success";
    case 0x21: return "Created";
    case 0x22: return "Accepted";
    case 0x23: return "Non-Authoritative Info";
    case 0x24: return "No Content";
    case 0x25: return "Reset Content";
    case 0x26: return "Partial Content";
    case 0x30: return "Multiple Choices";
    case 0x31: return "Moved Permanently";
    case 0x32: return "Moved Temporarily";
    case 0x33: return "See Other";
    case 0x34: return "Not Modified";
    case 0x35: return "Use Proxy";
    case 0x40: return "Bad Request";
    case 0x41: return "Unauthorized";
    case 0x42: return "Payment Required";
    case 0x43: return "Forbidden";
    case 0x44: return "Not Found";
    case 0x45: return "Method Not Allowed";
    case 0x46: return "Not Acceptable";
    case 0x47: return "Proxy Authentication Required";
    case 0x48: return "Request Timeout";
    case 0x49: return "Conflict";
    case 0x4A: return "Gone";
    case 0x4B: return "Length Required";
    case 0x4C: return "Precondition Failed";
    case 0x4D: return "Entity Too Large";
    case 0x4E: return "URL Too Large";
    case 0x4F: return "Unsupported Media Type";
    case 0x50: return "Internal Server Error";
    case 0x51: return "Not Implemented";
    case 0x52: return "Bad Gateway";
    case 0x53: return "Service Unavailable";
    case 0x54: return "Gateway Timeout";
    case 0x55: return "HTTP Version Not Supported";
    case 0x60: return "Database Full";
    case 0x61: return "Database Locked";
    }
    sprintf (buff, "Status (0x%x)", (RespCode & 0x7F));
    return buff;
}

static const char *pObexSessOpcode(U8 Opcode)
{
    static char buff[20];

    switch (Opcode) {
    case 0: return "Create Session";
    case 1: return "Close Session";
    case 2: return "Suspend Session";
    case 3: return "Resume Session";
    case 4: return "Set Session Timeout";
    }
    sprintf (buff, "Unknown (%x)", (Opcode));
    return buff;
}

static const char *pObexOpcode(U8 Opcode)
{
    static char buff[20];

    switch (Opcode & 0x7F) {
    case 0: return "Connect";
    case 1: return "Disconnect";
    case 2: return "Put";
    case 3: return "Get";
    case 5: return "Set Path";
    case 7: return "Session";
    case 0x7F: return "Abort";
    }
    sprintf (buff, "Unknown (%x)", (Opcode & 0x7F));
    return buff;
}

static  char *pMonth[] = { "", "January", "February", "March", "April", "May", 
                           "June", "July", "August", "September", "October",
                           "November", "December" };


/* ------------------------------------------------------------------------
 * Save Sniffer data
 */
static void SaveResults(const char *filename)
{
    FILE   *fp;
    int     len, count, index = 0;
    char    buffer[250];

    count = SendDlgItemMessage(SniffWnd, SnifferListBox, LB_GETCOUNT, 0, 0);
    if (count == LB_ERR || count == 0)
        return;

    fp = fopen(filename, "wb");
    if (fp == 0) {
        Report(("Sniffer: Could not open file %s.\n", filename));
        return;
    }

    /* Write test execution date & time */
    sprintf(buffer, "Trace Generated at %02d:%02d:%02d%s on %s %d, %04d.\r\n\r\n",
            (StartTime.wHour > 12 ? StartTime.wHour - 12 : StartTime.wHour),
            StartTime.wMinute, StartTime.wSecond, (StartTime.wHour < 12 ? "am" : "pm"),
            pMonth[StartTime.wMonth], StartTime.wDay, StartTime.wYear);

    len = strlen(buffer);
    if (fwrite(buffer, 1, len, fp) != (unsigned)len)
        goto Error;

    for (index = 0 ;index < count; index++) {
        len = SendDlgItemMessage(SniffWnd, SnifferListBox, LB_GETTEXT, index, (LPARAM)buffer);
        if (len == LB_ERR)
            break;

        strcat(buffer, "\r\n");
        len += 2;

        if (fwrite(buffer, 1, len, fp) != (unsigned)len)
            break;
    }

Error:
    if (index < count)
        Report(("Sniffer: Save Results encountered error %d records written.\n", index));

    fclose(fp);
}

static const char *pTimeOffset(const char *Msg)
{
    static char   buffer[20];
    DWORD         now;

    if (localStart == 0) {
        localStart = OS_GetSystemTime();
        GetLocalTime(&StartTime);
    }
    now = OS_GetSystemTime();

    sprintf(buffer, "%s+%02ld.%03lds", Msg, (now-localStart)/1000, (now-localStart)%1000);

    return buffer;
}

/*
 * Parses OBEX Packet headers.
 */
static const char *OBEX_Parse(ObexContext *obp, const U8 *RxBuff, U16 RxLen)
{
    U16     n, packetLen;
    U8      i, opcode;
    char    msg[100];

    /* Process the data in obp->rxBuff of length obp->rxLen. */
    while (RxLen > 0) {
        /* Restore the prior packet, if we got an Abort command during a response packet and we
         * aren't currently processing a sequence number header for the Abort command. 
         */
        if ((obp->savedCurPacketLen > 0) && (!obp->seqNumHeader)) {
            /* Restore the saved packet lengths */
            obp->curPacketLen = obp->savedCurPacketLen;
            obp->packetLen = obp->savedPacketLen;
            /* Restore the RX state */
            obp->rxState = OBSC_RX_IGNORE;
            /* Clear the saved lengths */
            obp->savedCurPacketLen = 0;
            obp->savedPacketLen = 0;
        }

        Assert(obp->stageLen <= 6); 
        Assert((obp->stageOffset == 0) || (obp->stageOffset < obp->stageLen));

        if (obp->rxState == OBSC_RX_IGNORE) {
            /* It's possible to send an Abort command while a response is being 
             * received. 
             */
            if ((RxBuff[0] == 0xFF) && (BEtoHost16(RxBuff+1) == RxLen) && ((RxLen == 3) || (RxLen == 5))) {
                /* Save the current packet */
                obp->savedCurPacketLen = obp->curPacketLen;
                obp->savedPacketLen = obp->packetLen;
                /* Process the Abort command */
                obp->seqNumHeader = FALSE;
                obp->rxState = OBSC_RX_WAIT;
                /* Looks like a command, lets parse it! */
                obp->inSync = TRUE;
                /* Stage the packet length and opcode */
                obp->stageLen = 3;
                goto Stage;
            }

            Assert (RxLen <= (obp->packetLen - obp->curPacketLen));

            obp->curPacketLen += RxLen;
            /* In this state we do not need to do any further processing */
            break;
        }

Stage:
        n = min((U16)(obp->stageLen - obp->stageOffset), RxLen);
        Assert(n <= 6);

        /* Stage the data */
        for (i = obp->stageOffset; n > 0; n--, i++) {
            obp->stageBuff[i] = *RxBuff;
            RxBuff++;
            RxLen--;
            obp->stageOffset++;
        }

        /* Only call the state machine if the data has been completely staged. */
        if (obp->stageOffset == obp->stageLen) {

            switch (obp->rxState) {
            case OBSC_RX_WAIT:
                /* We have what is likely the start of an OBEX packet. Assume
                 * we're in sync and retrieve the packet length and opcode.
                 */
                packetLen = BEtoHost16(obp->stageBuff + 1);
                opcode = (obp->stageBuff[0] & 0x7F);

                if (!obp->inSync) {
                    /* We've staged what might be an OBEX Command let's see. */
                    if (opcode > 7 || opcode == 4 || opcode == 6 || (RxLen > packetLen)) {
                        /* Not the start of a command, ignore it. */
                        InitObexParser(obp);
                        return RxBuff-obp->stageLen;
                    }
                    /* Looks like a command, lets parse it! */
                    obp->inSync = TRUE;
                }
                obp->packetLen = packetLen;

                /* Autodetect if the packet is a command or a response. */
                if (opcode <= 0x0F || opcode == 0x7F) {
                    /* This is a command so get the packet opcode */
                    obp->opcode = obp->stageBuff[0];
                    obp->isCommand = TRUE;
                } else {
                    /* This a response so get the packet response */
                    obp->rsp = obp->stageBuff[0];
                    obp->isCommand = FALSE;
                }

                /* Some opcodes like CONNECT and SetPath have 
                 * values beyond the length that are not headers. 
                 */
                switch (obp->opcode & 0x7F) {
                case 0: 
                    /* CONNECT: We need to get the OBEX version, flags and
                     * Maximum OBEX packet length.
                     */
                    obp->stageLen = 4;
                    obp->curPacketLen = 7;
                    obp->rxState = OBSC_RX_STAGED;
                    /* See if we have a Session Sequence Number Header */
                    goto SeqNumHeader;

                case 7:
                    /* SESSION: We need to get the Session opcode out of the 
                     * Session Parameters Header */

                    /* The Session opcode has a length followed by headers. */
                    obp->stageLen = 0;
                    obp->curPacketLen = 3;
                    if (obp->isCommand) {
                        /* We have the expected Session Parameters header (0x52) and
                         * the Session Opcode tag (0x05) */
                        Assert ((RxBuff[0] == 0x52) && (RxBuff[3] == 0x05));
                        goto ShowObexPacket;
                    }
                    /* Drop down for SESSION Response */

                case 5:
                    /* SETPATH: The flags are only sent in the request packet 
                     * (received by the server). The response does not include
                     * the extra 2 bytes. In this case drop into the default case.
                     */
                    if (obp->isCommand) {
                        obp->stageLen = 2;
                        obp->curPacketLen = 5;
                        obp->rxState = OBSC_RX_STAGED;
                        /* See if we have a Session Sequence Number Header */
                        goto SeqNumHeader;
                    }
                    /* Drop down for SETPATH Response */

                default:
                    /* The other opcodes only have length followed by headers. */
                    obp->stageLen = 0;
                    obp->curPacketLen = 3;

                    if ((obp->packetLen >= (obp->curPacketLen+2)) && (RxLen > 0) &&
                        (RxBuff[obp->stageLen] == 0x93)) {
                        /* Looks like we have a Session Sequence Number Header */
                        obp->seqNumHeader = TRUE;
                        obp->stageLen += 2;
                        obp->curPacketLen += 2;
                        obp->rxState = OBSC_RX_STAGED;
                        break;
                    }
                    else {
                        /* Jump down to print the packet header */
                        goto ShowObexPacket;
                    }
                }
SeqNumHeader:
                if ((obp->packetLen >= (obp->curPacketLen+2)) && (RxLen > 0) && 
                    (RxBuff[obp->stageLen] == 0x93)) {
                    /* Looks like we have a Session Sequence Number Header */
                    obp->seqNumHeader = TRUE;
                    obp->stageLen += 2;
                    obp->curPacketLen += 2;
                }
                break;

            case OBSC_RX_STAGED:
                /* If Connect, the parameters are in the stage buffer. */
                /* If SetPath Command, the flags & constants are in the stage buffer */
                /* Otherwise, the opcode and length are in the stage buffer. */
ShowObexPacket:
                if (obp->isCommand) {
                    if ((obp->opcode & 0x7F) == 7) /* Session */
                        sprintf(msg, "    OBEX: %s Operation%s, Length %d", pObexSessOpcode(RxBuff[5]), ((obp->opcode & 0x80) ? " +Final Bit" : ""), obp->packetLen);
                    else sprintf(msg, "    OBEX: %s Operation%s, Length %d", pObexOpcode(obp->opcode), ((obp->opcode & 0x80) ? " +Final Bit" : ""), obp->packetLen);
                }
                else sprintf(msg, "    OBEX: %s Response%s, Length %d", pObexRsp(obp->rsp), ((obp->rsp & 0x80) ? " +Final Bit" : ""), obp->packetLen);
                SnifferMsg(msg);

                if ((obp->opcode & 0x7F) == 0) { /* Connect */
                    sprintf(msg, "    OBEX: Ver %d.%d, OBEX Max Packet %d, Flags 0x%02x", ((obp->stageBuff[0] & 0xF0) >> 4), (obp->stageBuff[0] & 0x0F), BEtoHost16(obp->stageBuff + 2), obp->stageBuff[1]);
                    SnifferMsg(msg);
                } else if (((obp->opcode & 0x7F) == 5) && obp->isCommand) { /* Set Path */
                    sprintf(msg, "    OBEX: Flags 0x%02x, Constants 0x%02x", obp->stageBuff[0], obp->stageBuff[1]);
                    SnifferMsg(msg);
                }

                if (obp->seqNumHeader) {
                    /* Sequence Number is the last byte in the stage buffer */
                    sprintf(msg, "    OBEX: Sequence Number %d", obp->stageBuff[obp->stageLen-1]);
                    SnifferMsg(msg);
                }
                /* We ignore the data part of the packet */
                obp->rxState = OBSC_RX_IGNORE;
                break;
            } /* End switch(rxState) */
            
            obp->stageOffset = 0;
        
        } /* End if (stageOffset == stageLen) */
    
    } /* End While() */

    if (obp->curPacketLen == obp->packetLen) {
        /* The packet is done. Prep for a new one. */
        obp->rxState = OBSC_RX_WAIT;
        obp->seqNumHeader = FALSE;
        obp->stageLen = 0;
        obp->stageLen = 3;
    }
    return RxBuff;
}


/*
 * Displays hex output of provided data.
 */
static void SnifferDumpRaw(const char *Msg, const U8 *Buffer, U16 Len)
{
    const U8 *p = Buffer;
    char     *o, output[(LINE_WIDTH * 4)+20]; /* 12 bytes padding */
    int       po;

    /* 
     * This part dumps any bytes not consumed by the Header Parsers.
     */

    /* This will check for illegal lengths caused by rollovers */
    Assert(Len < 0x8000);

    po = 0;
    while (p < Buffer+Len) {
        o = output;

        /* Append proper line heading msg */
        if (p == Buffer)
            o += sprintf(o, "%s ", Msg);
        else o += sprintf(o, "    ");

        /* Dumps the packet in basic ASCII dump format */
        po = 0;
        while (po < LINE_WIDTH) {
            if (p+po < Buffer+Len)
                o += sprintf(o, "%02x ", p[po]);
            else o += sprintf(o, "   ");
            if (++po == LINE_WIDTH/2) o += sprintf(o, "  ");
        }
        o += sprintf(o, "    ");

        po = 0;
        while (po < LINE_WIDTH) {
            if (p+po < Buffer+Len)
                o += sprintf(o, "%1c", ((p[po] > ' ' && p[po] < '~') ? p[po] : '.'));
            else break;
            po++;
        }

        SnifferMsg(output);
        p += po;
    }
}

#if JETTEST == XA_ENABLED
static void ObexSniffer(void *ObexHandle, ObexSniffType Type, const U8 *Buffer, U16 Length)
{
    char        output[100];
    I8          i, j = 50;
    const U8   *dump;

    if (!SnifferOn)
        return;

    for (i = 0; i < 50; i++) {
        if (Op[i].appContext == ObexHandle)
            break;

        if (Op[i].appContext == 0 && j == 50)
            j = i;
    }

    if (i == 50) {
        /* Assign unknown parser to slot "j". */
        Assert (j < 50);
        i = j;
        Op[i].appContext = ObexHandle;
    }

    sprintf(output, "%s: Data %s %s [Length %d]", (Type == OBSNIFF_TX ? "TX" : "RX"),
            (Op[i].rxState == OBSC_RX_WAIT ? "First" : "Continue"), pTimeOffset(""), Length);
    SnifferMsg(output);

    dump = OBEX_Parse(&Op[i], Buffer, Length);

    SnifferDumpRaw("   ", dump, (U16)(Length - (dump - Buffer)));
}
#endif /* JETTEST == XA_ENABLED */

