/***************************************************************************
 *
 * File:
 *     $Workfile:$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:$
 *
 * Description: iAnywhere Blue FTS interface source file.
 *
 * Created:     September 10, 2002
 *
 * Copyright 2002-2005 Extended Systems, Inc.

 * Portions copyright 2005 iAnywhere Solutions, Inc.

 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/
#include "comdef.h"         /* COM definitions */
#include "LiveImportAPI.h"  /* Definitions of function pointers. */

extern "C" {
#include "fts.h"
#include "osapi.h"
#include "sys/hci.h"
#include "hcitrans.h"

    BOOL FTS_ProcessHciPacket(HciPacket *Packet);
    BOOL FTS_ProcessHciBuffer(HciBuffer *Buffer);

#if HCI_ALLOW_PRESCAN == XA_ENABLED
    HciPrescanHandler   hciPrescan;
    long                frameSequence;

/*---------------------------------------------------------------------------
 * FTS_StartLiveImport
 */
    void FTS_StartLiveImport(void)
    {
        /* Initialize the Frame sequence number */
        frameSequence = 0;

        /* Register a prescan handler */
        hciPrescan.hciTxPrescan = FTS_ProcessHciPacket;
        hciPrescan.hciTxDone = NULL;
        hciPrescan.hciRxPrescan = FTS_ProcessHciBuffer;
        hciPrescan.hciRxDone = NULL;

        if (HCI_RegisterPrescan(&hciPrescan, PRESCAN_FLAG_FIRST) != XA_STATUS_SUCCESS) {
            Report(("Failed to register prescan data\n"));
        }
    }

/*---------------------------------------------------------------------------
 * FTS_CheckLiveImportConnection()
 */
    BOOL FTS_CheckLiveImportConnection()
    {
        char *message= NULL;
        bool boolConnectionIsRunning(false);
        int iCount(0);
        DWORD dwStatus(0);

        /* Walk through the variables and return the state */
        while (!boolConnectionIsRunning && (iCount < 2)) { /* Wait no more than 2 seconds */
            dwStatus= g_pIsAppReady(&boolConnectionIsRunning);
            if (FAILED(dwStatus)) {
                message= "Unspecified internal error in Live Import interface.";
                break;
            }

            if (!boolConnectionIsRunning) {
                Sleep(1000);
                iCount++;
            }
        }

        if ((2 <= iCount) && !boolConnectionIsRunning)
            message= "FTS is not ready to receive data via Live Import.";

        if (NULL != message)
            if (0 != strlen(message))
                Report(("%s\n", message));

        return(message == 0);    /* Return true for "all is well" */
    }

/*---------------------------------------------------------------------------
 * FTS_DisconnectFromLiveImport()
 */
    BOOL FTS_DisconnectFromLiveImport()
    {
        /* Stop sending data to the FTS4BT DLL */

        /* First, unregister the prescan callback routines.  */
        if (XA_STATUS_SUCCESS != HCI_UnregisterPrescan(&hciPrescan)) {
            Report(("Failed to unregister prescan data\n"));
        }

        /* Secondly, if the FTS4BT callbacks are not NULLED out,         
         * then the FTS4BT application is still connected.  In that      
         * case, the FTS4BT application needs to be told to disconnect.  
         */
        if (NULL != g_pReleaseLiveImport) {
            /* Inform the FTS4BT application that it should disconnect.  */
            g_pReleaseLiveImport();

            /* NULL out the FTS4BT DLL callbacks.  */
            NullLiveImportFunctionPointers();
        }

        /* Third, free up the FTS4BT DLL library.  */
        FreeLibrary(g_hLiveImportAPI);

        return(TRUE);
    } 

/*---------------------------------------------------------------------------
 * FTS_FindFTS4BTDirectory()
 */

    BOOL FTS_FindFTS4BTDirectory( TCHAR *pLiveImportAPIDllPath ,
                                  TCHAR *pIniPath              )
    {

        /* Find the FTS for Bluetooth directory */
        HKEY hkFTE= NULL;
        DWORD 
            dwRegStatus(
                RegOpenKeyEx(
                    HKEY_LOCAL_MACHINE,
                    "Software\\FTE\\DataCom Analyzer\\Installations\\", 
                    0, 
                    KEY_READ, 
                    &hkFTE));
        if (ERROR_SUCCESS != dwRegStatus) {
            return(FALSE);
        }

        /* Locate the FTS4BT or FTS4BT Virtual executable.
         * This code is dangerous as written, because it will only locate the 
         * "first" instance of FTS4BT or FTS4BT Virtual, and the ordering is 
         * random. If multiple versions of FTS4BT or FTS4BT Virtual are 
         * installed, or one of each, the installation located could not be 
         * the one desired.
         *
         * To avoid that installation-location uncertainty, a third registry 
         * key should be checked (like the serial number), or "forward 
         * launch" should be employed.  If a third parameter is to be used, 
         * it can be provided to the datasource as a command-line parameter, 
         * in an ini file or at worst hard-coded.
         *
         * Forward launch is the better way to solve the problem. In that 
         * case, FTS is told what datasource to launch and where it is. FTS 
         * then creates the connection data, which are sent to the 
         * datasource.
         */

        int iEntry(0);
        TCHAR tcSubKey[256];
        DWORD dwSubKeySize(256);
        HKEY hkSub= NULL;
        DWORD dwBufferSize(1024);
        TCHAR tcProductId[1024];  /* Overkill.... */
        bool boolFound(false);
        do {
            dwRegStatus = RegEnumKeyEx(hkFTE, 
                                       iEntry++, 
                                       tcSubKey, 
                                       &dwSubKeySize, 
                                       0, 
                                       0, 
                                       0, 
                                       NULL);
            dwSubKeySize= 256;
            if (ERROR_SUCCESS != RegOpenKeyEx(hkFTE, 
                                              tcSubKey, 
                                              0, 
                                              KEY_EXECUTE, 
                                              &hkSub))
                continue;

            dwBufferSize= 1024;
            if (ERROR_SUCCESS == RegQueryValueEx(hkSub, 
                                                 _T("Path"), 
                                                 0, 
                                                 0, 
                                                 (BYTE*)(&pIniPath[0]), 
                                                 &dwBufferSize)) {
                dwBufferSize= 1024;
                if (ERROR_SUCCESS == RegQueryValueEx(hkSub, 
                                                     _T("Product ID"), 
                                                     0, 
                                                     0, 
                                                     (BYTE*)tcProductId, 
                                                     &dwBufferSize)) {
                    if (  (0 == _tcscmp(tcProductId, _T("16"))) 
                       || (0 == _tcscmp(tcProductId, _T("19")))) {

                        /* Found an installation of 
                         * FTS4BT or FTS4BT Virtual. 
                         */
                        boolFound= true;  
                    }
                }
            }

            if (ERROR_SUCCESS != RegCloseKey(hkSub))
                break;

        } while ((ERROR_SUCCESS == dwRegStatus) && !boolFound);

        if (  (ERROR_SUCCESS != RegCloseKey(hkFTE)) 
           || (ERROR_SUCCESS != dwRegStatus       )
           || !boolFound                          ) {
            return(FALSE);
        }

        /* This code assumes LiveImportAPI.dll is located in a subdirectory 
         * under the directory where the liveimport.ini file is located.
         * This code can be removed and g_pszLibraryName can hard-coded here. 
         */
        _tcscpy((char *)(&pLiveImportAPIDllPath[0]), (char *)(&pIniPath[0]));
        _tcscat((char *)(&pLiveImportAPIDllPath[0]), 
                _T("\\Executables\\Core\\LiveImportAPI.dll"));

        _tcscat((char *)(&pIniPath[0]), _T("\\liveimport.ini"));
        return(TRUE);
    }

/*---------------------------------------------------------------------------
 * FTS_ConnectToLiveImport()
 */

    BOOL FTS_ConnectToLiveImport(void)
    {

        TCHAR tcLiveImportAPIDllPath[_MAX_PATH];
        TCHAR tcIniPath[1024];
        
        if (!FTS_FindFTS4BTDirectory(&tcLiveImportAPIDllPath[0], &tcIniPath[0])){
            FTS_DisconnectFromLiveImport();
            return(FALSE);
        }

        g_pszLibraryName= tcLiveImportAPIDllPath;
        if (!LoadLiveImportAPIFunctions())
            return(FALSE);

        /* Get the configuration out of the liveimport.ini file. */
        
        TCHAR tcConnect[1024];
        TCHAR tcConfig[1024];
        DWORD dwRead(GetPrivateProfileString(_T("General"), _T("ConnectionString"), 0, tcConnect, 1024, tcIniPath));
        if (0 != dwRead)
            dwRead= GetPrivateProfileSection(_T("Configuration"), tcConfig, 1024, tcIniPath);

        if (0 == dwRead) {
            FTS_DisconnectFromLiveImport();
            return(FALSE);
        }

        /* Convert nulls to newlines in configuration text */
        TCHAR* ptc= tcConfig;
        int iLength(_tcslen(ptc));
        while (0 != iLength) {
            ptc[iLength]= _T('\n');  /* Change null character to new line */
            ptc += (iLength + 1);  /* Move to the next string */
            iLength = _tcslen(ptc);
        }

        bool boolSuccess(true);  /* Only set false if failure in liveimport class. */
        HRESULT hr= g_pInitializeLiveImport(tcConnect, tcConfig, &boolSuccess);

        if (FAILED(hr)) {  /* Status flag from the server itself */
            if (!boolSuccess)
                hr= HRESULT_FROM_WIN32(ERROR_INVALID_FUNCTION);

            FTS_DisconnectFromLiveImport();
            return(FALSE);
        }

        return(TRUE);
    }

/*---------------------------------------------------------------------------
 * FTS_SendData
 */

    void FTS_SendData(int iFrameLength, BYTE* pbytFrame, int iFlags, int iSide)
    {
        if (0 == iFrameLength)  /* No data to send.... */
            return;

        if (!FTS_CheckLiveImportConnection())
            return;

        /* Generate a timestamp. */
        SYSTEMTIME systemtime;
        GetLocalTime(&systemtime);
        __int64 i64Timestamp(0);
        SystemTimeToFileTime(&systemtime, (FILETIME*)&i64Timestamp);

        /* Send the data. */
        HRESULT hStatus= g_pSendFrame(
                                     iFrameLength,   /* Original size of the frame */
                                     iFrameLength,   /* "Sliced" size of the frame */
                                     pbytFrame,
                                     iFlags,         /* DRFs */
                                     iSide,          /* What side did the data come from? */
                                     i64Timestamp);  /* Timestamp */

        if (FAILED(hStatus))
            Report(("Failed to send data: Error %d\n", hStatus));
    }

/*---------------------------------------------------------------------------
 * FTS_ProcessHciPacket
 *
 *     This function passes a HCI transmit packet to the sniffer.
 */
    BOOL FTS_ProcessHciPacket(HciPacket *packet)
    {
        int         j, flags, hLen, frameLen;
        char       *p;
        SAFEARRAY  *liFrame;

        /* Set up the flags and the Header length. */
        switch (packet->flags & BUF_TYPES) {
        case HCI_BUFTYPE_COMMAND:
            flags = FTS_HCI_COMMAND;
            hLen = frameLen = 3;
            break;

        case HCI_BUFTYPE_ACL_DATA:
            flags = FTS_HCI_ACL_DATA;
            hLen = frameLen = 4;
            break;

        case HCI_BUFTYPE_SCO_DATA:
            flags = FTS_HCI_SCO_DATA;
            hLen = frameLen = 3;
            break;

        default:
            Report(("ERROR: Unknown HCI Packet Type!\n"));
            return FALSE;
            break;
        }

        /* Calculate the packet length. */
        for (j = 0; j < packet->fCount; j++) {
            frameLen += packet->f[j].len;
        }

        /* Create a SafeArray */
        liFrame = SafeArrayCreateVector(VT_UI1, 0, frameLen);
        SafeArrayAccessData(liFrame, (void **)&p);

        /* Copy the header */
        memcpy(p, packet->header, hLen);
        p += hLen;

        /* Copy the fragments (if any) */
        for (j = 0; j < packet->fCount; j++) {
            memcpy(p, packet->f[j].buffer, packet->f[j].len);
            p += packet->f[j].len;
        }
        SafeArrayUnaccessData(liFrame);

        /* Send the frame */
        FTS_SendData(frameLen, (BYTE *)liFrame->pvData, flags, 1);

        return FALSE;
    }


/*---------------------------------------------------------------------------
 * FTS_ProcessHciBuffer
 *
 *     This function passes a HCI receive buffer to the sniffer.
 */
    BOOL FTS_ProcessHciBuffer(HciBuffer *buffer)
    {
        char       *p;
        SAFEARRAY  *liFrame;
        int         flags;

        /* Set up the flags. */
        switch (buffer->flags & BUF_TYPES) {
        case HCI_BUFTYPE_EVENT:
            flags = FTS_HCI_EVENT;
            break;

        case HCI_BUFTYPE_ACL_DATA:
            flags = FTS_HCI_ACL_DATA;
            break;

        case HCI_BUFTYPE_SCO_DATA:
            flags = FTS_HCI_SCO_DATA;
            break;

        default:
            break;
        }

        /* Create a SafeArray */
        liFrame = SafeArrayCreateVector(VT_UI1, 0, buffer->len);
        SafeArrayAccessData(liFrame, (void **)&p);

        /* Copy the buffer */
        memcpy(p, buffer->buffer, buffer->len);
        SafeArrayUnaccessData(liFrame);

        /* Send the frame */
        FTS_SendData(buffer->len, (unsigned char *)liFrame->pvData, flags, 0);

        return FALSE;
    }
#endif /* HCI_ALLOW_PRESCAN == XA_ENABLED */

} /* extern "C" */
