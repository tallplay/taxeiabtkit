/***************************************************************************
 *
 * File:
 *     $Workfile:inettran.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:126$
 *
 * Description:
 *     This file contains code for inet driver.
 *
 * Created:
 *     Aug 13, 1999
 *
 * Copyright 1999-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "hostctlr.h"
#include "me.h" /* To get BT_IAC_GIAC */
#include "sys/debug.h"
#include "time.h"

#ifdef DEMO
#undef Report
#define Report(S)
#endif

/* Local functions */
static void INETTRAN_SendData(HciPacket *hciPacket);
static void INETTRAN_BufferAvailable(HciBufFlags buffType);
static void INETTRAN_SetSpeed(U16 speed);

/* Host Controller Global data */
InetHostController  InetHC = {0};
extern SOCKET       Wakeup;

#define INET_HC_CLASS_NAME "Inet HC Driver Class"

#define PACKET_TYPE_BROADCAST (ACTIVE_BROADCAST|PICONET_BROADCAST)

void HC_DataInd(SOCKET S);


/* ---------------------------------------------------------------------------
 * INETTRAN_Init
 * 
 */
BtStatus INETTRAN_Init(TranCallback tranCallback)
{
    I16          i;
    WSADATA     wsaData;
    HWND        driverWnd;

    /* Save window in case this is a reinit. */
    driverWnd = IHC(driverWnd);
    /* Clear the context structure */
    OS_MemSet((U8 *)&InetHC, 0, sizeof(InetHostController));

    /* Initialize the Bluetooth sniffer */
    /*   if (SNIFFER_Init() == BT_STATUS_FAILED) {
     *       return BT_STATUS_FAILED;
     *   }
     */
    /* Init the Winsock Layer */
    if (WSAStartup( 0x0101, &wsaData))
        Assert(0);

    /* Create the driver window handle for receiving network events. */
    if (driverWnd) {
        /* Window already exists, don't recreate it. */
        IHC(driverWnd) = driverWnd;
    }
    else {
        IHC(driverWnd) = InitDriverWnd();
    }
    Assert(IHC(driverWnd));

    /* Init the Inet Host Controller data */
    IHC(pageSkt) = INVALID_SOCKET;
    IHC(inquirySkt) = INVALID_SOCKET;
    IHC(inquiryScanSkt) = INVALID_SOCKET;
    IHC(inqIACCount) = 1;
    IHC(inqIACs[0])  = BT_IAC_GIAC;
    IHC(classOfDevice)[0] = 0x04;    /* Minor Computer Class: Desktop 00000100b */
    IHC(classOfDevice)[1] = 0x01;    /* Major Class: Computer 00000001b */
    IHC(classOfDevice)[2] = 0x50;    /* Major Service Class: Object-Transfer & Telephony 01010000b */
#if NUM_SCO_CONNS > 0
    IHC(scoFlowOn) = FALSE;          /* Default: SCO flow control is disabled */
#endif
    //IHC(pageScanInterval) = 0x0800;  /* Defaults from HCI 4.7.19 */
    //IHC(pageScanWindow) = 0x0012;    /* Defaults from HCI 4.7.19 */

    IHC(localVersion)[0] = 0x02;    /* HCI version: 0x00 = 1.0B, 0x01 = 1.1, 0x02 = 1.2 */
    IHC(localVersion)[1] = 0x00;    /* HCI revision Hi byte: */
    IHC(localVersion)[2] = 0x00;    /* HCI revision Lo byte: */
    IHC(localVersion)[3] = 0x02;    /* LMP version: 0x00 = 1.0B, 0x01 = 1.1, 0x02 = 1.2 */
    IHC(localVersion)[4] = 0xFF;    /* Manufacturer Name Hi byte */
    IHC(localVersion)[5] = 0xFF;    /* Manufacturer Name Lo byte: 0xFFFF = Testing */
    IHC(localVersion)[6] = 0x00;    /* LMP subversion Hi byte: */
    IHC(localVersion)[7] = 0x00;    /* LMP subversion Lo byte: */

    IHC(localFeatures)[0] = 0xFF;    /*  */
    IHC(localFeatures)[1] = 0xFF;    /*  */
    IHC(localFeatures)[2] = 0x0F;    /*  */
    IHC(localFeatures)[3] = 0x80;    /* eSCO */
    IHC(localFeatures)[4] = 0x00;    /*  */
    IHC(localFeatures)[5] = 0x00;    /*  */
    IHC(localFeatures)[6] = 0x00;    /*  */
    IHC(localFeatures)[7] = 0x00;    /*  */

    IHC(localLinkPolicy) = BLP_MASK; /*  */

    OS_MemCopy(IHC(localName), "iAnywhere Blue SDK", 21);

    /* Setup the link connection table */
#if NUM_SCO_CONNS > 0
    OS_MemSet((U8 *)&IHC(scoTab), 0, sizeof(IHC(scoTab)));
#endif
    OS_MemSet((U8 *)&IHC(connTab), 0, sizeof(IHC(connTab)));
    for (i = 0; i < NUM_BT_HANDLES; i++) {
        IHC(connTab)[i].link = INVALID_SOCKET;
    }

    /* Start the Host Controler Clock, it 'ticks' every 40ms */
    if (SetTimer(IHC(driverWnd), 1, 40, NULL) == 0)
        Assert(0);

    if (HC_OpenPagingSocket() != HC_STATUS_SUCCESS)
        return BT_STATUS_FAILED;

    if (HC_OpenInquirySocket() == HC_STATUS_SUCCESS)
        HC_GetIdentity();

    InitializeListHead(&IHC(eventQueue));

    srand(time(0));

    /* If the total exceeds 255, internal SCO connection handles need
     * to be modified so they can be two bytes long.
     */
    Assert((NUM_SCO_CONNS == 0) || (NUM_BT_HANDLES + NUM_SCO_CONNS < 0x00FF));

    IHC(tranEntry).sendData = INETTRAN_SendData;
    IHC(tranEntry).buffAvail = INETTRAN_BufferAvailable;
    IHC(tranEntry).setSpeed = INETTRAN_SetSpeed;
    HCI_RegisterTransport(&IHC(tranEntry));

    return BT_STATUS_SUCCESS;
}

/* ---------------------------------------------------------------------------
 * INETTRAN_Shutdown
 * 
 */
BtStatus INETTRAN_Shutdown(void)
{
    U8          i;
    BtConn     *conn;

    OS_StopHardware();

    /* Denitialize the Bluetooth sniffer */
    /*    SNIFFER_Deinit();*/
   
    IHC(state) = HC_MODE_IDLE;

    for (i=0; i < NUM_BT_HANDLES; i++) {
        conn = &IHC(connTab)[i];
        EVM_CancelTimer(&(conn->connTimer));
    }

    KillTimer(IHC(driverWnd), 1);

    /* Close down any open links */
    for (i = 0; i < NUM_BT_DEVICES; i++) {
        if (IHC(connTab)[i].link != INVALID_SOCKET)
            closesocket( IHC(connTab)[i].link );
    }

    /* Close down inquiry socket */
    if (IHC(inquirySkt) != INVALID_SOCKET)
        closesocket( IHC(inquirySkt) );

    /* Close down page socket */
    if (IHC(pageSkt) = INVALID_SOCKET)
        closesocket( IHC(pageSkt) );

   if (IHC(inquiryScanSkt) == INVALID_SOCKET)
       closesocket( IHC(inquiryScanSkt) );

    IHC(pageSkt) = INVALID_SOCKET;
    IHC(inquirySkt) = INVALID_SOCKET;
    IHC(inquiryScanSkt) = INVALID_SOCKET;

    if (IHC(driverWnd)) {
        /* Force the driver window to close (must be on its own thread). */
        /* Removed force close for Reinitialization testing. [benb: 4/18/03]
        SendMessage(IHC(driverWnd), WM_HC_QUIT, 0, 0); */
    }

    if (!UnregisterClass(INET_HC_CLASS_NAME, 0)) {
        Report(("HC: Unregister failed: %d\n",GetLastError()));
    }

    /* Shutdown winsock */
    WSACleanup();

    OS_ResumeHardware();
    
    return BT_STATUS_SUCCESS;
}

#define HCI_BUFTYPE_MASK   0x0F
/* ---------------------------------------------------------------------------
 * INETTRAN_Send
 * 
 */
void INETTRAN_SendData(HciPacket *Packet)
{
    BtConn     *conn;
    HciHandle   hciHandle;

#if NUM_SCO_CONNS > 0
    ScoConn    *sco;
#endif

    OS_StopHardware();


    switch (Packet->flags & HCI_BUFTYPE_MASK) {
        
    case HCI_BUFTYPE_ACL_DATA:
        hciHandle = LEtoHost16(Packet->header);
        if (hciHandle & PACKET_TYPE_BROADCAST) {
            if (hciHandle & ACTIVE_BROADCAST)
                conn = &IHC(connTab)[CONN_ACTIVE_BCAST];
            else conn = &IHC(connTab)[CONN_PICONET_BCAST];
            conn->port = hciHandle;

        } else {
            Assert((hciHandle & 0x0EFF) < NUM_BT_DEVICES);
            conn = &IHC(connTab)[hciHandle & 0x0EFF];
        }
        /* only send data if the link is active, OR if the link is parked and
         * it is broadcast data, otherwise just queue the data for later.
         */
        if ((conn->state <= LINK_ACTIVE) && (hciHandle & PACKET_TYPE_BROADCAST)) {
            HC_SendAclData(Packet); 
        }
        else if ((conn->state > LINK_ACTIVE) || (conn->waitModeChange == TRUE)) {
            /* queue data */
            InsertTailList(&(conn->ACLDataQueue), &(Packet->node) );
            OS_ResumeHardware();
            return;
        }
        else {
            HC_SendAclData(Packet); 
        }
        break;
        
    case HCI_BUFTYPE_SCO_DATA:
        #if NUM_SCO_CONNS > 0
            /* dump data */
            sco = &IHC(scoTab)[Packet->header[0] - NUM_BT_HANDLES];
            conn = sco->parent;
            if (conn->state > LINK_HOLD) {
                sco->packetsSent++;
            }
            else {
                HC_SendScoData(Packet);
            }

        #else
            Assert(0);
        #endif
        break;
        
    case HCI_BUFTYPE_COMMAND:

        /* commands are ALWAYS sent no matter what mode we are in */
        HC_SendCommand(Packet);
        break;
        
    default:
        Assert(0);
        break;
    }
    
    /* Packet was sent, Return it to the HCI packet pool. */
    HCI_PacketSent(Packet);
    
    OS_ResumeHardware();
}

/* ---------------------------------------------------------------------------
 * INETTRAN_BufferAvailable
 */
static void INETTRAN_BufferAvailable(HciBufFlags buffType)
{
}

/* ---------------------------------------------------------------------------
 * INETTRAN_SetSpeed
 */
void INETTRAN_SetSpeed(U16 speed)
{
}


/* ---------------------------------------------------------------------------
 * HC_SendQueuedAclData
 * 
 */
void HC_SendQueuedAclData(BtConn *conn)
{
    HciPacket *Packet;

    while (!IsListEmpty(&(conn->ACLDataQueue)))
    {
        Packet = (HciPacket *)RemoveHeadList(&(conn->ACLDataQueue));
        HC_SendAclData(Packet); 

        /* Packet was sent, Return it to the HCI packet pool. */
        HCI_PacketSent(Packet);
    }
}


/* ---------------------------------------------------------------------------
 * HC_SendAclData
 * 
 */
static void HC_SendAclData(HciPacket *Packet)
{
    BtConn     *conn;
    U8          buff[4];    /* ACL header */
    U16         hcp;
    BOOL        sent = TRUE;
    I8          i, c;
    HciHandle   hciHandle;

    hciHandle = LEtoHost16(Packet->header);
    if (hciHandle & PACKET_TYPE_BROADCAST) {
        if (hciHandle & ACTIVE_BROADCAST)
            conn = &IHC(connTab)[CONN_ACTIVE_BCAST];
        else conn = &IHC(connTab)[CONN_PICONET_BCAST];

        /* This technique is not too impressive, but its Bluetooth! 
         * Keep track of what handle the Host picked. We'll need this when
         * generating NumCompletedPackets events. We just overload the port
         * field since it is unused in the broadcast cases.
         */
        conn->port = hciHandle;

    } else {
        Assert((hciHandle & 0x0EFF) < NUM_BT_DEVICES);
        conn = &IHC(connTab)[hciHandle & 0x0EFF];
    }

    buff[2] = Packet->header[2];
    buff[3] = Packet->header[3];

    /*
     * To better emulate the host controller, we keep track of the Number
     * of Complete Packets but don't generate the Event until our HC_Clock
     * runs. This way we can complete multiple packets in one event.
     */
    conn->packetsSent++;

    if (hciHandle & FIRST_PACKET)
        hcp = HCP_L2C_FIRST;
    else hcp = HCP_L2C_CONTU;

    if (hciHandle & PACKET_TYPE_BROADCAST) {
        /* Set proper broadcast bit in the HCP. We unicast 
         * the broadcast packet to each of the connected devices. 
         */
        if (hciHandle & ACTIVE_BROADCAST)
            hcp |= HCP_ACTIVE_BCAST_BIT;
        else hcp |= HCP_PICONET_BCAST_BIT;

        StoreLE16(buff, hcp);

        for (c = 0; c < NUM_BT_HANDLES; c++) {
            if ((IHC(connTab)[c].state != LINK_ACTIVE) &&
                (!(IHC(connTab)[c].state == LINK_PARK) && (hciHandle & PICONET_BROADCAST)))
               continue;

            conn = &IHC(connTab)[c];
            Assert(conn->link != INVALID_SOCKET);

            /* Send HCP Inet header first, then actual data packet */
            sent = HC_Send(conn->link, buff, 4, 0);

            /* Send all segments of the HCI packet */
            for (i = 0; i < Packet->fCount; i++) {
                if (sent) 
                    sent = HC_Send(conn->link, Packet->f[i].buffer, Packet->f[i].len, 0);
            }
        }

        //Report(("HC: Sent broadcast packet.\n"));
        if (sent == FALSE)
            conn->packetsSent--;

        return;
    }

    /* Connection oriented (Unicast) sends */
    Assert(conn);
    StoreLE16(buff, hcp);

    /* Send HCP Inet header first, then actual data packet */
    sent = HC_Send(conn->link, buff, 4, 0);

    /* Send all segments of the HCI packet */
    for (i = 0; i < Packet->fCount; i++) {
        if (sent)
            sent = HC_Send(conn->link, Packet->f[i].buffer, Packet->f[i].len, 0);
    }

    //Report(("HC: Sent unicast packet.\n"));
    if (sent == FALSE)
        conn->packetsSent--;
//  else Report(("INETTRAN: Sent %d bytes.\n", LEtoHost16(Packet->header+2)));

    return;
}


/* ---------------------------------------------------------------------------
 * HC_SendScoData
 * 
 */
static void HC_SendScoData(HciPacket *Packet)
{
#if NUM_SCO_CONNS > 0
    ScoConn    *sco;
    BtConn     *acl;
    I8          i;
    U8          buff[4];    /* ACL header */
    BOOL        sent;

    Assert(Packet->header[0] >= NUM_BT_HANDLES);
    Assert(Packet->header[0] < NUM_BT_HANDLES + NUM_SCO_CONNS);
    Assert(Packet->header[1] == 0);

    sco = &IHC(scoTab)[Packet->header[0] - NUM_BT_HANDLES];
    acl = sco->parent;

    StoreLE16(buff, HCP_SCO_AUDIO);
    buff[2] = Packet->header[2];        /* Length */
    buff[3] = sco->handle;

    /* Send HCP-SCO INet header first, then actual audio packet */
    sent = HC_Send(acl->link, buff, 4, 0);

    /* Send all segments of the HCI packet */
    for (i = 0; i < Packet->fCount; i++) {
        if (sent)
            sent = HC_Send(acl->link, Packet->f[i].buffer, Packet->f[i].len, 0);
    }

    if (sent)
        sco->packetsSent++;

    Report(("HC: Sent voice/audio packet.\n"));
    return;
#else
    Assert(0);
#endif
}

/* ---------------------------------------------------------------------------
 * HC_SendCommand
 * 
 * All Host commands are dispatched here. If the command returns a command
 * status event, then the event is sent from here. Functions for commands
 * that return a status event must return a HcStatus value. They must also
 * 'break' out of their case so the status is sent. Commands which do not 
 * send status must 'return' from their case to avoid sending a command
 * status event. 
 *
 * If an invalid parameter is detected, functions which return a command
 * status should still 'break' from their case. Functions which do not
 * return status jump to 'SendCmdCompleteErr' which sends a command complete
 * with an error code (and no status event).
 */
void HC_SendCommand(HciPacket *Packet)
{
    U16         opcode = LEtoHost16(Packet->header);
    U8          parmBlockLen = Packet->header[2];
    U8         *parmBlock = (U8 *)(Packet->f[0].buffer);
    HcStatus    status = HC_STATUS_INVAL_HCI_PARM_VAL;
    U8          cmdStatus[11], complete[4];
#if NUM_SCO_CONNS > 0
    U8          i;
    BD_ADDR     tmpAddr;
    BtConn     *Acl;
    U16         pktType;
#endif

    switch (opcode) {

        /* Note: Commands are listed in this switch statement in the same order
         * they appear in the HCI specs. Keep it this way!
         */

    case HCC_INQUIRY:   /* 4.5.1 Inquiry */
        if (parmBlockLen >= 5) {
            /* Parameters: Inq_length+3 & Num Responses+4 */
            status = HC_Inquire(LEtoHost16(parmBlock) + (parmBlock[2]<<16),
                parmBlock[3], parmBlock[4]);
        } 
        break;

    case HCC_INQUIRY_CANCEL:   /* 4.5.2 Inquiry Cancel */
        if (parmBlockLen >= 0) {
            HC_InquiryCancel();
            return;
        }
        goto SendCmdCompleteErr;
        break;

    case HCC_CREATE_CONNECTION:   /* 4.5.5 Create Connection */
        if (parmBlockLen >= 12) {
            U16 pkts = LEtoHost16(parmBlock+6);
            BOOL  allowRoleChange = (BOOL)parmBlock[12];
            /* need to add allowrolechange parm */
            status = HC_CreateAclConn(*(BD_ADDR *)parmBlock, pkts, FALSE, allowRoleChange);
        }
        break;

    case HCC_DISCONNECT:   /* 4.5.6 Disconnect */
        if (parmBlockLen >= 3) {
            U16 connHndl = LEtoHost16(parmBlock) & 0x0FFF;
            U8  reason = parmBlock[2];
            
            status = HC_Disconnect(connHndl, reason);
        }
        break;

#if NUM_SCO_CONNS > 0
    case HCC_ADD_SCO_CONNECTION:    /* 4.5.7 Add_SCO_Connection */
        if (parmBlockLen >= 4) {
            U16 connHndl = LEtoHost16(parmBlock) & 0x0FFF;

            status = HC_AddScoConn(connHndl, LEtoHost16(parmBlock+2));
        }
        break;

    case HCC_SETUP_SYNC_CONNECTION: /* v1.2 7.1.33 Setup_Sync_Connection */
        if (parmBlockLen >= 17) {
            U16 connHndl = LEtoHost16(parmBlock) & 0x0FFF;
            pktType = LEtoHost16(parmBlock+15);

            if (pktType & 0x0038) {
                /* Try to setup an eSCO. */
                status = HC_SetupSyncConn(connHndl, pktType);
            }
            else if (pktType & 0x0007) {
                /* Setup a SCO. */
                status = HC_AddScoConn(connHndl, (U16)(pktType<<5));
            }
        }
        break;

    case HCC_ACCEPT_SYNC_CON_REQ:   /* v1.2 7.1.34 Accept_Sync_Conn_Req */
        if (parmBlockLen >= 4) {
            /* Find a matching connection entry */
            Acl = 0;
            for (i = 0; i < NUM_BT_DEVICES; i++) {
                if ((IHC(connTab)[i].state == LINK_ACCEPTING) ||
                    (IHC(connTab)[i].state == LINK_ACTIVE)) {
                    ConnAddr2BdAddr(&IHC(connTab)[i], &tmpAddr);

                    if (AreBdAddrsEqual(&tmpAddr, (BD_ADDR *)parmBlock)) {
                        Acl = &IHC(connTab)[i];
                        break;
                    }
                }
            }
            status = HC_AcceptSyncConn(Acl);
        }
        break;

    case HCC_REJECT_SYNC_CON_REQ:   /* v1.2 7.1.35 Reject_Sync_Conn_Req */
        if (parmBlockLen >= 4) {
            status = HC_RejectSyncConn(*(BD_ADDR *)parmBlock, parmBlock[6]);
        }
        break;
#endif

    case HCC_ACCEPT_CON_REQ:   /* 4.5.8 Accept_Connection_Request */
        if (parmBlockLen >= 7) {
            U8  role = parmBlock[6];
            
            status = HC_AcceptConn(*(BD_ADDR *)parmBlock, role);
        }
        break;

    case HCC_REJECT_CON_REQ:   /* 4.5.9 Reject_Connection_Request */
        if (parmBlockLen >= 7) {
            U8  reason = parmBlock[6];
            
            status = HC_RejectConn(*(BD_ADDR *)parmBlock, reason);
        }
        break;

    case HCC_LINK_KEY_REQ_REPL:       /* 4.5.10 Link_Key_Request_Reply */
        if (parmBlockLen >= 22) {
            /* Command Parameters: BD_ADDR(6), LinkKey(16) */
            HC_LinkKeyReqReply((BD_ADDR *)parmBlock, parmBlock+6);
            return;
        }
        goto SendCmdCompleteErr;
        break;

    case HCC_LINK_KEY_REQ_NEG_REPL:   /* 4.5.11 Link_Key_Request_Negative_Reply */
        if (parmBlockLen >= 6) {
            /* Command Parameters: BD_ADDR(6) */
            HC_LinkKeyReqNegReply((BD_ADDR *)parmBlock);
            return;
        }
        goto SendCmdCompleteErr;
        break;

    case HCC_PIN_CODE_REQ_REPL:       /* 4.5.12 PIN_Code_Request_Reply */
        if (parmBlockLen >= 23) {
            /* Command Parameters: BD_ADDR(6), PinLen(1) PinCode(16) */
            if (parmBlock[6] < 1 || parmBlock[6] > 16)  /* Pin Len 1-16 */
                goto SendCmdCompleteErr;

            HC_PinCodeReqReply((BD_ADDR *)parmBlock, parmBlock[6], parmBlock+7);
            return;
        }
        goto SendCmdCompleteErr;
        break;

    case HCC_PIN_CODE_REQ_NEG_REPL:   /* 4.5.13 PIN_Code_Request_Negative_Reply */
        if (parmBlockLen >= 6) {
            HC_PinCodeReqNegReply((BD_ADDR *)parmBlock);
            return;
        }
        goto SendCmdCompleteErr;
        break;

    case HCC_AUTH_REQ:                /* 4.5.15 Authentication_Requested */
        if (parmBlockLen >= 2) {
            U16 connHndl = LEtoHost16(parmBlock) & 0x0FFF;            
            status = HC_Authenticate(connHndl);
        } 
        break;

    case HCC_SET_CONN_ENCRYPT:
        if (parmBlockLen >= 3) {        /* 4.5.16 Set_Connection_Encryption */
            U16 connHndl = LEtoHost16(parmBlock) & 0x0FFF;
            U8  encenable = parmBlock[2];

            status = HC_SetConnectionEncryption(connHndl, encenable);
        }
        break;

    case HCC_REM_NAME_REQ:
        if (parmBlockLen >= 10) {   /* 4.5.19 Remote_Name_Request */
            /* We ignore the last 4 parameter bytes (timing stuff) */
            status = HC_GetRemoteName((BD_ADDR *)parmBlock);
        }
        break;

    case HCC_READ_REMOTE_FEATURES:
        if (parmBlockLen >= 2) {        /* 4.5.20 Read_Remote_Supported_Features */
            U16 connHndl = LEtoHost16(parmBlock) & 0x0FFF;
            status = HC_ReadRemoteFeatures(connHndl);
        }
        break;

    case HCC_READ_LOCAL_VERSION:        /* 4.8.1 Read_Local_Version */
        HC_ReadLocalVersion();
        return;

    case HCC_READ_LOCAL_FEATURES:       /* 4.8.2 Read_Local_Version */
        HC_ReadLocalFeatures();
        return;

    case HCC_CREATE_CONNECTION_CANCEL:  /* v1.2 7.1.7 Create_Connection_Cancel */
        if (parmBlockLen >= 6) {
            HC_CreateConnCancel((BD_ADDR *)parmBlock);
        }
        return;

    case HCC_REM_NAME_REQ_CANCEL:       /* v1.2 7.1.20 Remote_Name_Request_Cancel */
        if (parmBlockLen >= 6) {
            HC_RemoteNameCancel((BD_ADDR *)parmBlock);
        }
        return;

    case HCC_HOLD_MODE:           /* 4.6.1 Hold_Mode */
        if (parmBlockLen >= 6) {
            U16 connHndl = LEtoHost16(parmBlock) & 0x0FFF;
            U16 holdMaxInt = LEtoHost16(parmBlock+2);
            U16 holdMinInt = LEtoHost16(parmBlock+4);
            
            status = HC_HoldModeReq(connHndl, holdMaxInt, holdMinInt);
        }
        break;

    case HCC_SNIFF_MODE:          /* 4.6.2 Sniff_Mode */
        if (parmBlockLen >= 10) {
            U16 connHndl = LEtoHost16(parmBlock) & 0x0FFF;
            U16 sniffMaxInt = LEtoHost16(parmBlock+2);
            U16 sniffMinInt = LEtoHost16(parmBlock+4);
            U16 sniffAttempt = LEtoHost16(parmBlock+6);
            U16 sniffTO = LEtoHost16(parmBlock+8);

            status = HC_SniffMode(connHndl, sniffMaxInt, sniffMinInt, 
                                    sniffAttempt, sniffTO);
        }
        break;

    case HCC_EXIT_SNIFF_MODE:    /* 4.6.3 Exit_Sniff_Mode */
        if (parmBlockLen >= 2) {
            U16 connHndl = LEtoHost16(parmBlock) & 0x0FFF;

            status = HC_ExitSniffMode(connHndl);
        }
        break;

    case HCC_PARK_MODE:          /* 4.6.4 Park_Mode */
        if (parmBlockLen >= 6) {
            U16 connHndl = LEtoHost16(parmBlock) & 0x0FFF;
            U16 beaconMaxInt = LEtoHost16(parmBlock+2);
            U16 beaconMinInt = LEtoHost16(parmBlock+4);
            status = HC_ParkMode(connHndl, beaconMaxInt, beaconMinInt);
        }
        break;

    case HCC_EXIT_PARK_MODE:     /* 4.6.5 Exit_Park_Mode */
        if (parmBlockLen >= 2) {
            U16 connHndl = LEtoHost16(parmBlock) & 0x0FFF;

            status = HC_ExitParkMode(connHndl);
        }
        break;

    case HCC_SWITCH_ROLE:           /* 4.6.8 Switch_Role */
        if (parmBlockLen >= 7) {
            U8  role = parmBlock[6];
            
            status = HC_SwitchRole((BD_ADDR *)parmBlock, role);
        }
        break;

    case HCC_ROLE_DISCOVERY:
        if (parmBlockLen >= 2)
        {
            U16 connHndl = LEtoHost16(parmBlock) & 0x0FFF;
            HC_RoleDiscovery(connHndl);
            return;
        }
        goto SendCmdCompleteErr;


    case HCC_READ_LINK_POLICY:      /* 4.6.9 Read_Link_Policy_Settings */
        if (parmBlockLen >= 2) {
            U16 connHndl = LEtoHost16(parmBlock) & 0x0FFF;
            HC_ReadLinkPolicy(connHndl);
            return;
        }
        break;
        
    case HCC_WRITE_LINK_POLICY:     /* 4.6.10 Write_Link_Policy_Settings */
        if (parmBlockLen >= 4) {
            U16 connHndl = LEtoHost16(parmBlock) & 0x0FFF;
            U16 linkPolicy = LEtoHost16(parmBlock+2);
            HC_WriteLinkPolicy(connHndl, linkPolicy);
            return;
        }
        break;
        
    case HCC_RESET:              /* 4.7.2 Rest */
        if (parmBlockLen >= 0) {
            HC_Reset();
            return;
        }
        goto SendCmdCompleteErr;
        break;

    case HCC_READ_LOCAL_NAME:       /* 4.7.11 Change_Local_Name */
        HC_ReadLocalName();
        return;

    case HCC_CHNG_LOCAL_NAME:       /* 4.7.12 Read_Local_Name */
      if (parmBlockLen >= 248) {
            HC_WriteLocalName(parmBlock, parmBlockLen);
            return;
      }
      goto SendCmdCompleteErr;
      break;

    case HCC_WRITE_SCAN_ENABLE:     /* 4.7.17 Write_Scan_Enable */
        if (parmBlockLen >= 1) {
            U8 mode = parmBlock[0];

            HC_WriteScanEnable(mode);
            return;
        }
        goto SendCmdCompleteErr;
        break;

    case HCC_READ_INQ_MODE:             /* 7.3.53 Read Inquiry Mode Command */
        HC_ReadInqMode();
        return;

    case HCC_WRITE_INQ_MODE:            /* 7.3.54 Write Inquiry Mode Command */
        if (parmBlockLen >= 1) {
            HC_WriteInqMode(parmBlock[0]);
            return;
        }
        goto SendCmdCompleteErr;
        break;


    case HCC_WRITE_INQ_SCAN_ACTIVITY:   /* 4.7.21 Write_InquiryScan_Activity */
    case HCC_WRITE_PAGE_SCAN_ACTIVITY:  /* 4.7.19 Write_PageScan_Activity */
        /* These commands do nothing at this time but need to act as if
         * they succeeded.
         */
        status = HC_STATUS_SUCCESS;
        cmdStatus[0] = HC_CmdQueueSpace();  /* Number of commands HCI can send */
        StoreLE16(cmdStatus+1, opcode);    /* Command were acknowledging */
        cmdStatus[3] = status;
        HC_PostEvent(HCE_COMMAND_COMPLETE, cmdStatus, 4);
        return;

    case HCC_READ_AUTH_ENABLE:        /* 4.7.23 Read_Authentication_Enable */
        HC_ReadAuthEnable();
        return;

    case HCC_WRITE_AUTH_ENABLE:       /* 4.7.24 Write_Authentication_Enable */
        if (parmBlockLen >=1 ) {
            HC_AuthEnable(parmBlock[0]);
            return;
        }
        goto SendCmdCompleteErr;
        break;

    case HCC_READ_ENCRYPT_MODE:        /* 4.7.25 Read_Encryption_Mode */
        HC_ReadEncryptMode();
        return;

    case HCC_WRITE_ENCRYPT_MODE:       /* 4.7.26 Write_Encryption_Mode */
        if (parmBlockLen >=1 ) {
            HC_WriteEncryptMode(parmBlock[0]);
            return;
        }
        goto SendCmdCompleteErr;
        break;

    case HCC_READ_CLASS_OF_DEVICE:      /* 4.7.27 Read_Class_Of_Device */
        HC_ReadClassOfDevice();
        return;

    case HCC_WRITE_CLASS_OF_DEVICE:     /* 4.7.28 Write_Class_Of_Device */
        if (parmBlockLen >= 3) {
            HC_WriteClassOfDevice(parmBlock);
            return;
        }
        goto SendCmdCompleteErr;
        break;

#if NUM_SCO_CONNS > 0
    case HCC_WRITE_VOICE_SETTING:   /* 4.7.29 Write_Voice_Setting */
        if (parmBlockLen >= 2) {

            HC_WriteScoSettings(LEtoHost16(parmBlock));
            return;
        }
        break;
#endif

    case HCC_SET_CTRLR_TO_HOST_FLOW_CTRL:  /* 4.7.37 Set_Host_Controller_To_Host_Flow_Control */
        if (parmBlockLen >= 1) {
            HC_SetHostFlowCtrl(parmBlock[0]);
            return;
        }
        goto SendCmdCompleteErr;
        break;

    case HCC_READ_SCO_FC_ENABLE:    /* 4.7.38 Read_SCO_Flow_Control_Enable */
        HC_ReadScoFlowControl();
        return;

    case HCC_WRITE_SCO_FC_ENABLE:   /* 4.7.39 Write_SCO_Flow_Control_Enable */
        if (parmBlockLen >= 1) {
            HC_WriteScoFlowControl(parmBlock[0]);
            return;
        }
        goto SendCmdCompleteErr;
        break;

    case HCC_HOST_BUFFER_SIZE:      /* 4.7.41 Host_Buffer_Size */
        if (parmBlockLen >= 7) {
            HC_HostBufferSize(parmBlock);
            return;
        }
        goto SendCmdCompleteErr;
        break;

    case HCC_HOST_NUM_COMPLETED_PACKETS:  /* 4.7.42 Host_Number_Of_Completed_Packets */
        if ((parmBlockLen >= 1) &&
            (parmBlockLen >= 1 + (parmBlock[0] * 4))) {
            HC_HostNumCompletedPackets(parmBlock);
            return;
        }
        goto SendCmdCompleteErr;
        return;

    case HCC_WRITE_CURRENT_IAC_LAP:     /* 4.7.44 Write_Current_IAC_LAP */
        if ((parmBlockLen >= 1) &&
            (parmBlockLen >= 1 + (parmBlock[0]*3))) {
            HC_WriteCurrentIacLap(parmBlock[0], parmBlock+1);
            return;
        }
        goto SendCmdCompleteErr;
        break;

    case HCC_READ_BUFFER_SIZE:   /* 4.8.3 Read_Buffer_Size */
        if (parmBlockLen >= 0) {
            HC_ReadBufferSize();
            return;
        }
        goto SendCmdCompleteErr;
        break;

    case HCC_READ_BD_ADDR:          /* 4.8.5 Read_BD_ADDR */
        if (parmBlockLen >= 0) {
            HC_ReadBdAddr();
            return;
        }
        break;


    /* BT v1.2 commands follow: */
    case HCC_READ_DEFAULT_LINK_POLICY:
        HC_ReadDefaultLinkPolicy();
        return;

    case HCC_WRITE_DEFAULT_LINK_POLICY:
        HC_WriteDefaultLinkPolicy(LEtoHost16(parmBlock));
        return;

    default:
        status = HC_STATUS_UNSUPP_FEATUR_PARM_VAL;
        break;
    }

    /* Post the command status to the Host */
    cmdStatus[0] = status;              /* Command Status */
    cmdStatus[1] = HC_CmdQueueSpace();  /* Number of commands HCI can send */
    StoreLE16(cmdStatus+2, opcode);    /* Command were acknowledging */

    HC_PostEvent(HCE_COMMAND_STATUS, cmdStatus, 4);
    return;

 SendCmdCompleteErr:

    /* Post a Command Complete to the Host (only for error cases) */
    complete[0] = HC_CmdQueueSpace();  /* Number of commands HCI can send */
    StoreLE16(complete+1, opcode);
    complete[3] = status;

    HC_PostEvent(HCE_COMMAND_COMPLETE, complete, 4);
    return;
}


/* ---------------------------------------------------------------------------
 * HC_ReadBufferSize
 * 
 */
void HC_ReadBufferSize(void)
{
    U8 complete[11];

    /* Respond with size and number of buffers */
    complete[0] = HC_CmdQueueSpace();  /* Number of commands HCI can send */
    StoreLE16(complete + 1, HCC_READ_BUFFER_SIZE); 
    complete[3] = HC_STATUS_SUCCESS;
    StoreLE16(&(complete[4]), HCI_ACL_DATA_SIZE);
    complete[6] = HCI_SCO_DATA_SIZE;

    /* We can only store HCI_NUM_PACKETS of ACL packets. This is because we borrow
     * HCI's actual "HciPacket" structures
     */
    StoreLE16(&(complete[7]), HCI_NUM_PACKETS);

    /* This is the number of SCO packets outstanding. TODO: Hard coded to 2,
     * which can't be right.
     */
    StoreLE16(&(complete[9]), 2);

    HC_PostEvent(HCE_COMMAND_COMPLETE, complete, 11);
}

/* ---------------------------------------------------------------------------
 * HC_CauseLinkLoss
 * 
 *     Close the TCP link suddenly and send the appropriate events to
 *     our Host to indicate that the link timed out
 */
BOOL HC_CauseLinkLoss(const BD_ADDR *BdAddr)
{
    U8       discEvent[4];
#if NUM_SCO_CONNS > 0
    U16      i;
#endif
    U16      aclHandle;
    BD_ADDR  tmpAddr;

    discEvent[0] = HC_STATUS_SUCCESS;
    /* discEvent 1-2 contain the handle */
    discEvent[3] = HC_STATUS_CONN_TIMEOUT;

    /* Look up the handle that corresponds to this address */
    for (aclHandle = 0; aclHandle < NUM_BT_DEVICES; aclHandle++) {
        ConnAddr2BdAddr(&IHC(connTab)[aclHandle], &tmpAddr);
        if (AreBdAddrsEqual(&tmpAddr, BdAddr)) {
            HC_CloseLink(&IHC(connTab)[aclHandle]);
            break;
        }
    }
    
    /* Hopefully we found a match */
    Assert(aclHandle < NUM_BT_DEVICES);

#if NUM_SCO_CONNS > 0
    /* Find and remove any SCO connections for this address. */
    for (i = 0; i < NUM_SCO_CONNS; i++) {

        if (IHC(scoTab)[i].parent == 0) continue;

        Assert(IHC(scoTab)[i].state != SCO_INACTIVE);

        ConnAddr2BdAddr(IHC(scoTab)[i].parent, &tmpAddr);
        if (AreBdAddrsEqual(&tmpAddr, BdAddr)) {
            IHC(scoTab)[i].parent = 0;
            
            /* Send a disconnect message for the scoHandle */
            StoreLE16(discEvent+1, (U16)(i + NUM_BT_HANDLES));
            HC_PostEvent(HCE_DISCONNECT_COMPLETE, discEvent, 4);
            
            /* Absolve any outstanding SCO packets considered "in use" */
            IHC(hostCurScoPackets) -= IHC(scoTab)[i].hostCurScoPackets;
        }
    }
#endif

    /* Finally send the ACL disconnect event */
    StoreLE16(discEvent+1, (U16)aclHandle);
    HC_PostEvent(HCE_DISCONNECT_COMPLETE, discEvent, 4);
    
    /* Absolve any outstanding ACL packets considered "in use" */
    IHC(hostCurAclPackets) -= IHC(connTab)[aclHandle].hostCurAclPackets;

    return TRUE;
}


/* ---------------------------------------------------------------------------
 * HC_Reset
 * 
 */
void HC_Reset(void)
{
    U8 complete[4];

    complete[0] = HC_CmdQueueSpace();  /* Number of commands HCI can send */
    StoreLE16(complete + 1, HCC_RESET);
    complete[3] = HC_STATUS_SUCCESS;

    if (IHC(state) == HC_MODE_IDENTIFY) {
        IHC(state) |= HC_MODE_RESETTING;
        return;
    }

    HC_PostEvent(HCE_COMMAND_COMPLETE, complete, 4);
}

/* ---------------------------------------------------------------------------
 * HC_SetHostFlowCtrl
 * 
 */
void HC_SetHostFlowCtrl(U8 flow)
{
    U8 i;
    U8 complete[4];

    complete[0] = HC_CmdQueueSpace();  /* Number of commands HCI can send */
    StoreLE16(complete + 1, HCC_SET_CTRLR_TO_HOST_FLOW_CTRL); 
    complete[3] = HC_STATUS_SUCCESS;

    /* Decode the flow bit */
    if (flow & 0x01) {
        IHC(flags) |= IHCF_CTRLR_FLOW_ACL;
        IHC(hostCurAclPackets) = 0;
        for (i = 0; i < NUM_BT_HANDLES; i++) {
            IHC(connTab)[i].hostCurAclPackets = 0;
        }
        
    } else {
        IHC(flags) &= ~IHCF_CTRLR_FLOW_ACL;
    }

    if (flow & 0x02) {
        IHC(flags) |= IHCF_CTRLR_FLOW_SCO;
#if NUM_SCO_CONNS > 0
        IHC(hostCurScoPackets) = 0;
        for (i = 0; i < NUM_SCO_CONNS; i++) {
            IHC(scoTab)[i].hostCurScoPackets = 0;
        }
#endif /* NUM_SCO_CONNS */
    } else {
        IHC(flags) &= ~IHCF_CTRLR_FLOW_SCO;
    }

    HC_PostEvent(HCE_COMMAND_COMPLETE, complete, 4);
}

/* ---------------------------------------------------------------------------
 * HC_HostBufferSize
 * 
 */
void HC_HostBufferSize(U8 *parmBlock)
{
    U8 complete[4];

    /* Respond with size and number of buffers */
    complete[0] = HC_CmdQueueSpace();  /* Number of commands HCI can send */
    StoreLE16(complete + 1, HCC_HOST_BUFFER_SIZE); 
    complete[3] = HC_STATUS_SUCCESS;

    /* Read out the important parameters */
    IHC(hostMaxAclLen) = LEtoHost16(parmBlock);
    IHC(hostMaxScoLen) = parmBlock[2];
    IHC(hostMaxAclPackets) = LEtoHost16(parmBlock+3);
    IHC(hostMaxScoPackets) = LEtoHost16(parmBlock+5);

    HC_PostEvent(HCE_COMMAND_COMPLETE, complete, 4);
}

/* ---------------------------------------------------------------------------
 * HC_HostNumCompletedPackets
 * 
 * Host indicates how many received packets are now to be considered 
 * "handled".
 */
void HC_HostNumCompletedPackets(U8 *parmBlock)
{
    U8 i;
    U16 connHandle;
    U16 numPackets;

    /* Iterate through each handle */
    for (i = 0; i < parmBlock[0]; i++) {
        connHandle = LEtoHost16(parmBlock + 1 + i*4);
        numPackets = LEtoHost16(parmBlock + 1 + i*4 + 2);
        if (connHandle < NUM_BT_HANDLES) {
            Report(("HC: %d more ACL packets available\n", numPackets));
            IHC(hostCurAclPackets) -= numPackets;
            IHC(connTab)[connHandle].hostCurAclPackets -= numPackets;
        } else {
#if NUM_SCO_CONNS > 0
            IHC(hostCurScoPackets) -= numPackets;
            IHC(scoTab)[connHandle - NUM_BT_HANDLES].hostCurScoPackets -= numPackets;
#endif /* NUM_SCO_CONNS */
        }
    }

    /* Do something horribly inefficient and poll each outstanding
     * link to see if it has a complete HCI buffer outstanding.
     */
    for (i = 0; i < NUM_BT_HANDLES; i++) {
       HC_DataInd(IHC(connTab)[i].link);
    }

    /* NOTE: There is no command-complete event corresponding to this
     * command! Odd.
     */
}

/* ---------------------------------------------------------------------------
 * HC_ReadBdAddr
 * 
 */
void HC_ReadBdAddr(void)
{
    U8  complete[10];

    complete[0] = HC_CmdQueueSpace();  /* Number of commands HCI can send */
    StoreLE16(complete+1, HCC_READ_BD_ADDR);
    complete[3] = HC_STATUS_SUCCESS;
    OS_MemCopy(complete+4, IHC(myAddr).addr, 6);

    HC_PostEvent(HCE_COMMAND_COMPLETE, complete, 10);
}

/* ---------------------------------------------------------------------------
 * HC_ReadScoFlowControl()
 * 
 */
static void HC_ReadScoFlowControl(void)
{
    U8  complete[5];

    complete[0] = HC_CmdQueueSpace();  /* Number of commands HCI can send */
    StoreLE16(complete+1, HCC_READ_SCO_FC_ENABLE);

#if NUM_SCO_CONNS > 0
    complete[3] = HC_STATUS_SUCCESS;
    complete[4] = IHC(scoFlowOn);
#else
    complete[3] = HC_STATUS_CMD_DISALLOWED;
    complete[4] = 0;
#endif

    HC_PostEvent(HCE_COMMAND_COMPLETE, complete, 5);
}

/* ---------------------------------------------------------------------------
 * HC_WriteScoFlowControl()
 * 
 */
static void HC_WriteScoFlowControl(BOOL FlowOn)
{
    U8  complete[4];

#if NUM_SCO_CONNS > 0
    I8  i;

    /* If there are any active SCO conns, reject the request. */
    for (i = 0; i < NUM_SCO_CONNS; i++) {
        if (IHC(scoTab)[i].parent != 0) {
            complete[3] = HC_STATUS_UNSPECIFIED_ERROR;
            goto PostComplete;
        }
    }
       
    /* Success */
    IHC(scoFlowOn) = FlowOn;
    complete[3] = HC_STATUS_SUCCESS;

PostComplete:
#else
    complete[3] = HC_STATUS_CMD_DISALLOWED;
#endif

    complete[0] = HC_CmdQueueSpace();  /* Number of commands HCI can send */
    StoreLE16(complete+1, HCC_WRITE_SCO_FC_ENABLE);

    HC_PostEvent(HCE_COMMAND_COMPLETE, complete, 4);
}

/* ---------------------------------------------------------------------------
 * HC_ReadLocalVersion
 * 
 */
static void HC_ReadLocalVersion()
{
    U8  complete[248+4];

    complete[0] = HC_CmdQueueSpace();  /* Number of commands HCI can send */
    StoreLE16(complete+1, HCC_READ_LOCAL_VERSION);
    complete[3] = HC_STATUS_SUCCESS;
    OS_MemCopy(complete+4, IHC(localVersion), 8);

    HC_PostEvent(HCE_COMMAND_COMPLETE, complete, 12);
}

/* ---------------------------------------------------------------------------
 * HC_ReadLocalFeatures
 * 
 */
static void HC_ReadLocalFeatures()
{
    U8  complete[248+4];

    complete[0] = HC_CmdQueueSpace();  /* Number of commands HCI can send */
    StoreLE16(complete+1, HCC_READ_LOCAL_FEATURES);
    complete[3] = HC_STATUS_SUCCESS;
    OS_MemCopy(complete+4, IHC(localFeatures), 8);

    HC_PostEvent(HCE_COMMAND_COMPLETE, complete, 12);
}

/* ---------------------------------------------------------------------------
 * HC_CreateConnCancel
 * 
 */
static void HC_CreateConnCancel(BD_ADDR *bdAddr)
{
    BtConn   *conn = 0;
    U8  complete[10];
    U8  connComplete[11];

    /* Build Command complete w/o status */
    complete[0] = HC_CmdQueueSpace();
    StoreLE16(complete+1, HCC_CREATE_CONNECTION_CANCEL);
    /* status goes here (+3) */
    OS_MemCopy(complete+4, bdAddr->addr, 6);

    /* Find a matching connection entry */
    conn = GetConnFromBdAddr(bdAddr);

    if (!conn || conn->link == INVALID_SOCKET) {
        complete[3] = HC_STATUS_NO_CONNECTION;
    }
    else if (conn->state > LINK_AUTHENTICATING) {
        complete[3] = HC_STATUS_ACL_ALREADY_EXISTS;
    }
    else {
        complete[3] = HC_STATUS_SUCCESS;
        HC_CloseLink(conn);
    }

    HC_PostEvent(HCE_COMMAND_COMPLETE, complete, 10);

    /* If we canceled the create, we need to complete it with an error. */
    if (complete[3] == HC_STATUS_SUCCESS) {
        connComplete[0] = HC_STATUS_NO_CONNECTION;
        StoreLE16(connComplete+1, 0xFFFF);  /* Invalid Conn Handle */
        OS_MemCopy(connComplete+3, bdAddr->addr, 6);
        HC_PostEvent(HCE_CONNECT_COMPLETE, connComplete, 11);
    }
}

/* ---------------------------------------------------------------------------
 * HC_RemoteNameCancel
 * 
 */
static void HC_RemoteNameCancel(BD_ADDR *bdAddr)
{
    BtConn   *conn = 0;
    U8  complete[10];
    U8  nameComplete[7];

    /* Build Command complete w/o status */
    complete[0] = HC_CmdQueueSpace();
    StoreLE16(complete+1, HCC_REM_NAME_REQ_CANCEL);
    /* status goes here (+3) */
    OS_MemCopy(complete+4, bdAddr->addr, 6);

    /* Find a matching connection entry */
    conn = GetConnFromBdAddr(bdAddr);

    if (!conn || conn->link == INVALID_SOCKET ||
        !((conn->flags & LINK_FLAG_NAME_REQ) || conn->nameReqOnly)) {
        complete[3] = HC_STATUS_INVAL_HCI_PARM_VAL;
    }
    else {
        complete[3] = HC_STATUS_SUCCESS;

        /* No longer waiting for a name response */
        conn->flags &= ~LINK_FLAG_NAME_REQ;
        
        /* Close the link if it is temporary. */
        if (conn->nameReqOnly) {
            /* Tear down connection */
            HC_CloseLink(conn);
        }
    }

    HC_PostEvent(HCE_COMMAND_COMPLETE, complete, 10);

    /* If we canceled the get name, we need to complete it with an error. */
    if (complete[3] == HC_STATUS_SUCCESS) {
        nameComplete[0] = HC_STATUS_NO_CONNECTION;
        OS_MemCopy(nameComplete+1, bdAddr->addr, 6);
        HC_PostEvent(HCE_REMOTE_NAME_REQ_COMPLETE, nameComplete, 7);
    }
}

/* ---------------------------------------------------------------------------
 * HC_ReadLocalName
 * 
 */
static void HC_ReadLocalName()
{
    U8  complete[248+4];

    complete[0] = HC_CmdQueueSpace();  /* Number of commands HCI can send */
    StoreLE16(complete+1, HCC_READ_LOCAL_NAME);
    complete[3] = HC_STATUS_SUCCESS;
    OS_MemCopy(complete+4, IHC(localName), 248);

    HC_PostEvent(HCE_COMMAND_COMPLETE, complete, 252);
}

/* ---------------------------------------------------------------------------
 * HC_WriteLocalName
 * 
 */
static void HC_WriteLocalName(char *Name, U8 NameLen)
{
    U8  complete[4];

    OS_MemSet(IHC(localName), 0, 248);
    OS_MemCopy(IHC(localName), Name, NameLen);

    complete[0] = HC_CmdQueueSpace();  /* Number of commands HCI can send */
    StoreLE16(complete+1, HCC_CHNG_LOCAL_NAME);
    complete[3] = HC_STATUS_SUCCESS;

    HC_PostEvent(HCE_COMMAND_COMPLETE, complete, 4);
}

/* ---------------------------------------------------------------------------
 * HC_WriteScoSettings
 * 
 */
static void HC_WriteScoSettings(U16 Setting)
{
    U8  complete[4];

    IHC(voiceSettings) = Setting;

    complete[0] = HC_CmdQueueSpace();  /* Number of commands HCI can send */
    StoreLE16(complete+1, HCC_WRITE_VOICE_SETTING);
    complete[3] = HC_STATUS_SUCCESS;

    HC_PostEvent(HCE_COMMAND_COMPLETE, complete, 4);
}

/* ---------------------------------------------------------------------------
 * HC_ReadAuthEnable
 * 
 */
void HC_ReadAuthEnable()
{
    U8  complete[5];

    complete[0] = HC_CmdQueueSpace();  /* Number of commands HCI can send */
    StoreLE16(complete+1, HCC_READ_AUTH_ENABLE);
    complete[3] = HC_STATUS_SUCCESS;
    complete[4] = (U8)IHC(flags) & IHCF_AUTHENTICATE;
    HC_PostEvent(HCE_COMMAND_COMPLETE, complete, 5);
}


/* ---------------------------------------------------------------------------
 * HC_AuthEnable()
 *
 * Host has requested to enable/disable automatic authentication
 *
 */
void HC_AuthEnable(BOOL enable)
{
    U8 complete[4];
    if (enable) {
        IHC(flags) |= IHCF_AUTHENTICATE;
    } else {
        IHC(flags) &= ~IHCF_AUTHENTICATE;
    }
    /* Always return success */
    complete[0] = HC_CmdQueueSpace();              /* Number of commands HCI can send */
    StoreLE16(complete+1, HCC_WRITE_AUTH_ENABLE);  /* Command we're acknowledging */
    complete[3] = HC_STATUS_SUCCESS;
    HC_PostEvent(HCE_COMMAND_COMPLETE, complete, 4);
}

/* ---------------------------------------------------------------------------
 * HC_ReadAuthEnable
 * 
 */
void HC_ReadEncryptMode()
{
    U8  complete[5];

    complete[0] = HC_CmdQueueSpace();  /* Number of commands HCI can send */
    StoreLE16(complete+1, HCC_READ_ENCRYPT_MODE);
    complete[3] = HC_STATUS_SUCCESS;
    if (IHC(flags) & IHCF_ENCRYPT)
        complete[4] = 1;
    else
        complete[4] = 0;
    HC_PostEvent(HCE_COMMAND_COMPLETE, complete, 5);
}


/* ---------------------------------------------------------------------------
 * HC_EncryptEnable()
 *
 * Host has requested to enable/disable automatic encryption
 *
 */
void HC_WriteEncryptMode(U8 mode)
{
    U8 complete[4];
    if (mode == 0x00) {
        IHC(flags) &= ~IHCF_ENCRYPT;
    } else if (mode == 0x01) {
        IHC(flags) |= IHCF_ENCRYPT;
    } else {   
        /* Inet currently doe snot support encrypted broadcast packets */
        complete[0] = HC_CmdQueueSpace();              /* Number of commands HCI can send */
        StoreLE16(complete+1, HCC_WRITE_ENCRYPT_MODE);  /* Command we're acknowledging */
        complete[3] = HC_STATUS_UNSUPP_FEATUR_PARM_VAL;
        HC_PostEvent(HCE_COMMAND_COMPLETE, complete, 4);
        return;
    }

    /* Always return success */
    complete[0] = HC_CmdQueueSpace();              /* Number of commands HCI can send */
    StoreLE16(complete+1, HCC_WRITE_ENCRYPT_MODE);  /* Command we're acknowledging */
    complete[3] = HC_STATUS_SUCCESS;
    HC_PostEvent(HCE_COMMAND_COMPLETE, complete, 4);
}

/* ---------------------------------------------------------------------------
 * HC_ReadClassOfDevice
 * 
 */
void HC_ReadClassOfDevice()
{
    U8  complete[7];

    complete[0] = HC_CmdQueueSpace();  /* Number of commands HCI can send */
    StoreLE16(complete+1, HCC_READ_CLASS_OF_DEVICE);
    complete[3] = HC_STATUS_SUCCESS;
    complete[4] = IHC(classOfDevice)[0];
    complete[5] = IHC(classOfDevice)[1];
    complete[6] = IHC(classOfDevice)[2];
    HC_PostEvent(HCE_COMMAND_COMPLETE, complete, 7);
}

/* ---------------------------------------------------------------------------
 * HC_WriteClassOfDevice
 * 
 */
void HC_WriteClassOfDevice(U8 *Cod)
{
    U8  complete[4];

    IHC(classOfDevice)[0] = Cod[0];
    IHC(classOfDevice)[1] = Cod[1];
    IHC(classOfDevice)[2] = Cod[2];

    complete[0] = HC_CmdQueueSpace();  /* Number of commands HCI can send */
    StoreLE16(complete+1, HCC_WRITE_CLASS_OF_DEVICE);
    complete[3] = HC_STATUS_SUCCESS;
    HC_PostEvent(HCE_COMMAND_COMPLETE, complete, 4);
}

/* ---------------------------------------------------------------------------
 * HC_ReadDefaultLinkPolicy (BT v1.2)
 * 
 */
static void HC_ReadDefaultLinkPolicy()
{
    U8  complete[6];

    complete[0] = HC_CmdQueueSpace();  /* Number of commands HCI can send */
    StoreLE16(complete+1, HCC_READ_DEFAULT_LINK_POLICY);
    complete[3] = HC_STATUS_SUCCESS;
    StoreLE16(complete+4, IHC(localLinkPolicy));

    HC_PostEvent(HCE_COMMAND_COMPLETE, complete, 6);
}

/* ---------------------------------------------------------------------------
 * HC_WriteDefaultLinkPolicy (BT v1.2)
 * 
 */
static void HC_WriteDefaultLinkPolicy(U16 linkPolicy)
{
    U8  complete[4];

    StoreLE16((U8 *)&IHC(localLinkPolicy), linkPolicy);

    complete[0] = HC_CmdQueueSpace();  /* Number of commands HCI can send */
    StoreLE16(complete+1, HCC_WRITE_DEFAULT_LINK_POLICY);
    complete[3] = HC_STATUS_SUCCESS;

    HC_PostEvent(HCE_COMMAND_COMPLETE, complete, 4);
}



/* ---------------------------------------------------------------------------
 * HC_RoleDiscovery
 * 
 */
void HC_RoleDiscovery(U16 connHndl)
{
    BtConn *conn;
    U8      complete[8];

    conn = &IHC(connTab)[connHndl];
    Assert(conn);

    /* Always return success */
    complete[0] = HC_CmdQueueSpace();              /* Number of commands HCI can send */
    StoreLE16(complete+1, HCC_ROLE_DISCOVERY);   /* Command we're acknowledging */
    complete[3] = HC_STATUS_SUCCESS;
    StoreLE16(complete+4, connHndl);
    complete[6] = conn->role; 
    HC_PostEvent(HCE_COMMAND_COMPLETE, complete, 7);
}

/* ---------------------------------------------------------------------------
 * HC_Clock
 * 
 */
void HC_Clock(void)
{
    static U16 ClockState = 1;
    U16      i;
    U8       j;
    U8       complete[((NUM_BT_HANDLES+NUM_SCO_CONNS) * 4) + 1];
    //   static U32      oldClock;
    //   U32 currClock;

    struct PacketsCompletedArray {
        U16   handle;
        U16   count;
    } *pca;

    //   currClock = GetTickCount();
    //   Report(("Clock: state %d, time %d (%d interval)\n", ClockState, currClock, currClock - oldClock));
    //   oldClock = currClock;

    switch (ClockState) {
    case 1:
        /* Generates Inquiry every 120 ms (10 per 1.20 sec interval) */
        /* Send an Inquiry Request? */
        if (IHC(inqDuration)) {

            /* Check for completion by num responses and by elapsed interval. */
            if (IHC(inqResponseMax) && (IHC(inqResponseCount) == IHC(inqResponseMax)))
                IHC(inqDuration) = 1;

            if (IHC(inqMaxTime) < (OS_GetSystemTime() - IHC(inqStartTime)))
                IHC(inqDuration) = 1;

            if (--IHC(inqDuration) == 0) {
                /* Indicate inquiry complete event */
                U8 inqComplete[2];

                inqComplete[0] = HC_STATUS_SUCCESS;
                inqComplete[1] = IHC(inqResponseCount);

                HC_PostEvent(HCE_INQUIRY_COMPLETE, inqComplete, 2);
                break;
            }
                     
            HC_SendInquiry(IHC(inqAccess),IHC(inqKey));
        }
        /* Fall through so that Packets Complete is always checked. */
        
    case 2: 
        /* Restart logic for flow controled links */
        {
            SOCKET w = Wakeup;
    
            Wakeup = INVALID_SOCKET;
            if (w != INVALID_SOCKET)
                PostMessage(IHC(driverWnd), WM_SELECT, w, FD_READ);
        }
        /* Check to see if we have any events waiting to be indicated */
        HC_ProcessEventOverrunQueue();

        /* Fall through so that Packets Complete is always checked. */

    case 3: 
        /* Checks every 40 ms */
        /* Are any sent packets to complete? */
        for (i = 0; i < NUM_BT_HANDLES; i++) {
            if (IHC(connTab)[i].packetsSent > 0)
                break;
        }
        if (i == NUM_BT_HANDLES) {
#if NUM_SCO_CONNS > 0
            if (IHC(scoFlowOn) == FALSE)
                break;

            /* No ACL packets to complete. Check for SCO packets. */
            for (j = 0; j < NUM_SCO_CONNS; j++) {
                if (IHC(scoTab)[j].packetsSent > 0)
                    break;
            }
            if (j == NUM_SCO_CONNS)
                break;
#else
            break;
#endif
        }

        /* Yes, Build and post PacketsCompletedEvent */
        pca = (struct PacketsCompletedArray *)(complete+1);

        /* Process ACL handles, note that we start with the old value of 'i' */
        for (j = 0; i < NUM_BT_HANDLES; i++) {
            if (IHC(connTab)[i].packetsSent) {
                //Report(("HC: Completing %d packets on Conn %d.\n", IHC(connTab)[i].packetsSent, i));
                /* If the connection is a broadcast connection, replace
                 * our hciHandle with the one the Host gave use.
                 */
                if (i == CONN_PICONET_BCAST || i == CONN_ACTIVE_BCAST)
                    StoreLE16((U8 *)(&pca[j].handle), IHC(connTab)[i].port);
                else StoreLE16((U8 *)(&pca[j].handle), i);

                StoreLE16((U8 *)(&pca[j].count), IHC(connTab)[i].packetsSent);
                IHC(connTab)[i].packetsSent = 0;
                j++;
            }
        }

#if NUM_SCO_CONNS > 0
        /* Now process the SCO handles */
        if (IHC(scoFlowOn) == TRUE) {
            for (i = 0; i < NUM_SCO_CONNS; i++) {
                if (IHC(scoTab)[i].packetsSent) {
                    StoreLE16((U8 *)(&pca[j].handle), (U16)(i+NUM_BT_HANDLES));
                    StoreLE16((U8 *)(&pca[j].count), IHC(scoTab)[i].packetsSent);
                    IHC(scoTab)[i].packetsSent = 0;
                    j++;
                }
            }
        }
#endif
        complete[0] = j;

        HC_PostEvent(HCE_NUM_COMPLETED_PACKETS, complete, (U8)((j*4)+1));
        break;

    default:
        Assert(0);
        break;

    } /* switch() */

    if (++ClockState > 3)
        ClockState = 1;
}

/* ---------------------------------------------------------------------------
 * InitDriverWnd
 * 
 */
static HWND InitDriverWnd(void)
{
    WNDCLASS    wc;
    HWND        hWnd;

    if (!GetClassInfo(0, INET_HC_CLASS_NAME, &wc)) {

        wc.style = 0;
        wc.lpfnWndProc = (WNDPROC)HC_WndProc;
        wc.cbClsExtra = 0;
        wc.cbWndExtra = sizeof(LONG);
        wc.hIcon = 0;
        wc.hInstance = 0;
        wc.hCursor = 0;
        wc.hbrBackground = 0;
        wc.lpszMenuName =  0;
        wc.lpszClassName = INET_HC_CLASS_NAME;

        if (!RegisterClass(&wc)) {
            Report(("HC: Select Window Registration failed, %x\n", GetLastError()));
            return NULL;
        }
    }

    hWnd = CreateWindow( "Inet HC Driver Class", NULL, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL);
    return hWnd;
}
