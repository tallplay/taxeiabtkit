/***************************************************************************
 *
 * File:
 *     $Workfile:hclinkmgr.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:93$
 *
 * Description:
 *    This file contains most of the Host Controller Link Mgmt code.
 *
 * Created:
 *     Jan 29, 2000
 *
 * Copyright 2000-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "osapi.h"
#include "hostctlr.h"
#include "eventmgr.h"
#include "btalloc.h"
#include "sys/debug.h"

#ifdef DEMO
#undef Report
#define Report(S)
#endif

#if XA_DEBUG_PRINT == XA_ENABLED
static char *pHcp(U16 hcp);
#endif

extern void HC_DisconnectInd(SOCKET S);
static void ConnectComplete(BtConn *Conn,U16 ConnHndl);

/* ---------------------------------------------------------------------------
 * HcGetHandleFromCon
 *
 * Gets the handle value from a conn pointer
 */
static U16 HcGetHandleFromConn(BtConn *conn)
{
    U16 i;
    for (i = 0; i < NUM_BT_DEVICES; i++) {
        if (&(IHC(connTab)[i]) == conn) return i;
    }
    Assert(0);
    return 0;
}


/* ---------------------------------------------------------------------------
 * HC_Authenticate 
 *
 */
HcStatus HC_Authenticate(U16 ConnHndl)
{
    BtConn *conn;

    if (ConnHndl > NUM_BT_DEVICES)
        return HC_STATUS_NO_CONNECTION;

    conn = &IHC(connTab)[ConnHndl];

    if (conn->link == INVALID_SOCKET)
        return HC_STATUS_NO_CONNECTION;

    conn->secFlags |= LINK_SEC_AUTH_REQUESTED;
    StartAuthenticating(conn);

    return HC_STATUS_SUCCESS;
}


/* ---------------------------------------------------------------------------
 * HC_SetConnectionEncryption
 *
 */
HcStatus HC_SetConnectionEncryption(U16 ConnHndl, U8 encenable)
{
    BtConn *conn;
    U8       setenc[2];

    if (ConnHndl > NUM_BT_DEVICES)
        return HC_STATUS_NO_CONNECTION;

    conn = &IHC(connTab)[ConnHndl];

    if (conn->link == INVALID_SOCKET)
        return HC_STATUS_NO_CONNECTION;

    if (conn->authCur != AuthReady)
        return HC_STATUS_KEY_MISSING;

    /* turn encryption on */
    if (encenable) {
        StoreLE16(setenc, HCP_LMP_ENC_START_REQ);
        
        HC_SendLmp(conn->link, setenc, 2, 0); 
    }

    /* turn encryption off */
    else {
        StoreLE16(setenc, HCP_LMP_ENC_STOP_REQ);
        
        HC_SendLmp(conn->link, setenc, 2, 0); 
    }

    return HC_STATUS_SUCCESS;
}


/* ---------------------------------------------------------------------------
 * GetConnFromBdAddr
 *
 * Finds a BtConn match for the specified address
 */
BtConn *GetConnFromBdAddr(const BD_ADDR *BdAddr)
{
    int       i;
    BD_ADDR   tmpAddr;
    for (i = 0; i < NUM_BT_DEVICES; i++) {
        ConnAddr2BdAddr(&IHC(connTab)[i], &tmpAddr);
        if (AreBdAddrsEqual(&tmpAddr, BdAddr)) {
            return &IHC(connTab)[i];
        }
    }
    return 0;
}
        

/* ---------------------------------------------------------------------------
 * HC_LinkKeyReqReply
 * 
 */
void HC_LinkKeyReqReply(const BD_ADDR *BdAddr, const U8 *LinkKey)
{
    BtConn   *conn = 0;
    U8        complete[10];

    /* Build Command complete w/o status */
    complete[0] = HC_CmdQueueSpace();
    StoreLE16(complete+1, HCC_LINK_KEY_REQ_REPL);
    /* status goes here (+3) */
    OS_MemCopy(complete+4, BdAddr->addr, 6);

    /* Find an matching connection entry */
    conn = GetConnFromBdAddr(BdAddr);
    
    if (!conn || conn->link == INVALID_SOCKET) {
        /* We failed to find a match, send and invalid connection completion */
        complete[3] = HC_STATUS_NO_CONNECTION;
        HC_PostEvent(HCE_COMMAND_COMPLETE, complete, 10);
        return;
    }

    /* Send command complete status */
    complete[3] = HC_STATUS_SUCCESS;
    HC_PostEvent(HCE_COMMAND_COMPLETE, complete, 10);

    /* Store the link key and continue authorization */
    OS_MemCopy(conn->linkKey, LinkKey, 16);
    conn->linkKeyType = LINK_KEY_COMB;

    HandleAuthState(HCP_HCC_KEY_POS, conn, 0, 0 );

} /* End HC_LinkKeyReqReply() */


/* ---------------------------------------------------------------------------
 * HC_LinkKeyReqNegReply
 * 
 */
void HC_LinkKeyReqNegReply(const BD_ADDR *BdAddr)
{
    BtConn   *conn = 0;
    U8        complete[10];

    /* Build Command complete w/o status */
    complete[0] = HC_CmdQueueSpace();
    StoreLE16(complete+1, HCC_LINK_KEY_REQ_NEG_REPL);
    /* status goes here (+3) */
    OS_MemCopy(complete+4, BdAddr->addr, 6);

    /* Find an matching connection entry */
    conn = GetConnFromBdAddr(BdAddr);
    
    if (!conn || conn->link == INVALID_SOCKET) {
        /* Indicate negative command complete for Link_Key_Request_Negative_Reply */
        complete[3] = HC_STATUS_NO_CONNECTION;
        HC_PostEvent(HCE_COMMAND_COMPLETE, complete, 10);
        return;
    }
    /* Removing assert to allow mutual authentication
     * Assert(conn->linkKeyType == LINK_KEY_NONE);
     */

    /* Indicate positive command complete for Link_Key_Request_Negative_Reply */
    complete[3] = HC_STATUS_SUCCESS;
    HC_PostEvent(HCE_COMMAND_COMPLETE, complete, 10);

    HandleAuthState(HCP_HCC_KEY_NEG, conn, 0, 0);

} /* End HC_LinkKeyReqNegReply() */

/* ---------------------------------------------------------------------------
 * HC_PinCodeReqReply
 * 
 */
void HC_PinCodeReqReply(const BD_ADDR *BdAddr, U8 PinLen, const U8 *Pin)
{
    BtConn   *conn = 0;
    U8        complete[10];

    /* Build Command complete w/o status */
    complete[0] = HC_CmdQueueSpace();
    StoreLE16(complete+1, HCC_PIN_CODE_REQ_REPL);
    /* status goes here (+3) */
    OS_MemCopy(complete+4, BdAddr->addr, 6);

    /* Find an matching connection entry */
    conn = GetConnFromBdAddr(BdAddr);

    if (!conn || conn->link == INVALID_SOCKET) {
        /* Indicate negative PinCodeReqReply command complete */
        complete[3] = HC_STATUS_NO_CONNECTION;
        HC_PostEvent(HCE_COMMAND_COMPLETE, complete, 10);
        return;
    }

    /* Indicate successful PinCodeReqReply command */
    complete[3] = HC_STATUS_SUCCESS;
    HC_PostEvent(HCE_COMMAND_COMPLETE, complete, 10);
    conn->linkKeyType = LINK_KEY_INIT;


    /* Hand the PIN code to the current authentication state */
    HandleAuthState(HCP_HCC_PIN_POS, conn, (void *)Pin, (void *)PinLen);

} /* End HC_PinCodeReqReply() */

/* ---------------------------------------------------------------------------
 * HC_PinCodeReqNegReply
 * 
 */
void HC_PinCodeReqNegReply(const BD_ADDR *BdAddr)
{
    BtConn   *conn = 0;
    U8        complete[10];

    /* Build Command complete w/o status */
    complete[0] = HC_CmdQueueSpace();
    StoreLE16(complete+1, HCC_PIN_CODE_REQ_NEG_REPL);
    /* status goes here (+3) */
    OS_MemCopy(complete+4, BdAddr->addr, 6);

    /* Find an matching connection entry */
    conn = GetConnFromBdAddr(BdAddr);
    
    if (!conn || conn->link == INVALID_SOCKET) {
        /* Indicate negative PinCodeReqNegReply command complete */
        complete[3] = HC_STATUS_NO_CONNECTION;
        HC_PostEvent(HCE_COMMAND_COMPLETE, complete, 10);
        return;
    }

    /* Indicate successful PinCodeReqNegReply command */
    complete[3] = HC_STATUS_SUCCESS;
    HC_PostEvent(HCE_COMMAND_COMPLETE, complete, 10);


    /* Hand the PIN request failed. */
    HandleAuthState(HCP_HCC_PIN_NEG, conn, 0, 0);

} /* End HC_PinCodeReqNegReply() */

/* ---------------------------------------------------------------------------
 * HC_WriteCurrentIacLap
 * 
 * Writes the set of acceptable IACs that we will respond to during inquiry
 * scan.
 */
void HC_WriteCurrentIacLap(I8 count, U8 *buffer)
{
    I8 i;
    U8 complete[4];
    Assert(count <= MAX_IAC_CODES);

    IHC(inqIACCount) = (U16)count;
    for (i = 0; i < count; i++) {
        IHC(inqIACs)[i]  = LEtoHost16(buffer);
        IHC(inqIACs)[i] += (buffer[2] << 16);
        buffer += 3;
    }

    /* Always return success */
    complete[0] = HC_CmdQueueSpace();  /* Number of commands HCI can send */
    StoreLE16(complete+1, HCC_WRITE_CURRENT_IAC_LAP);  /* Command we're acknowledging */
    complete[3] = HC_STATUS_SUCCESS;
    HC_PostEvent(HCE_COMMAND_COMPLETE, complete, 4);
}


/* ---------------------------------------------------------------------------
 * HC_ProcessLmpAccepted
 * 
 */
static void HC_ProcessLmpAccepted(BtConn *Conn, U16 Opcode, U16 ConnHndl)
{
    U8          complete[17];
    U8          lmpmsg[6];
    U8          lmpaccepted[8];
    U8            setup[2];


#if NUM_SCO_CONNS > 0
    ScoConn    *sco;
    I8          i;
#endif

    switch(Opcode) {

    case HCP_LMP_CONN_REQ:
        /* Our connection request was accepted */
        //Assert(Conn->role == BCR_MASTER);
        Conn->state = LINK_SETUP;

        if ((Conn->roleChanged == TRUE) && (Conn->role == BCR_SLAVE)) {
            
            /* Post Role Change event */
            complete[0] = HC_STATUS_SUCCESS;
            ConnAddr2BdAddr(Conn, (BD_ADDR *)(complete+1));
            complete[7] = Conn->role;
               HC_PostEvent(HCE_ROLE_CHANGE, complete, 8);
        }

        /* Should send features request, but we just skip to setup complete. */

        /* Actually, pass this event on to the authentication state
         * machine for handling
         */
        HandleAuthState(HCP_LMP_CONN_REQ, Conn, 0, 0);
        break;

    case HCP_LMP_IN_RAND:
        /* Pairing accepted, pass this on to authentication states */
        HandleAuthState(HCP_LMP_ACCEPTED, Conn, 0, 0);
        break;

#if NUM_SCO_CONNS > 0
    case HCP_LMP_SCO_LINK_REQ:
        /* Indicate the Connection to the Host */

        for (i = 0; i < NUM_SCO_CONNS; i++) {
            if (IHC(scoTab)[i].parent == Conn && IHC(scoTab)[i].state == SCO_CONNECTING) {
                break;
            }
        }
        Assert (i < NUM_SCO_CONNS);

        sco = &IHC(scoTab)[i];
        sco->state = SCO_ACTIVE;

        /* Indicate CONNECT Complete (success) to HCI */
        complete[0]  = HC_STATUS_SUCCESS;
        /* SCO Handle = SCO index + NumBtHandles Offset */
        complete[1]  = (U8)(sco - IHC(scoTab)) + NUM_BT_HANDLES;
        complete[2]  = 0;
        ConnAddr2BdAddr(Conn, (BD_ADDR *)(complete+3));
        complete[9]  = 0;   /* SCO Link */
        complete[10] = 0;   /* No encryption */

        HC_PostEvent(HCE_CONNECT_COMPLETE, complete, 11);
        break;

    case HCP_LMP_REMOVE_SCO_REQ:
    case HCP_LMP_REMOVE_ESCO_REQ:
        for (i = 0; i < NUM_SCO_CONNS; i++) {
            if (IHC(scoTab)[i].parent == Conn && IHC(scoTab)[i].state == SCO_INACTIVE) {
                break;
            }
        }
        Assert (i < NUM_SCO_CONNS);

        sco = &IHC(scoTab)[i];
        /* We already indicated the disconnect complete to the Host.
         * Just Free the connection structure.
         */
        OS_MemSet((U8 *)sco, 0, sizeof(ScoConn));
        break;

    case HCP_LMP_ESCO_LINK_REQ:
        /* Check for Conn Changed */
        for (i = 0; i < NUM_SCO_CONNS; i++) {
            if (IHC(scoTab)[i].parent == Conn && IHC(scoTab)[i].state == SCO_CHANGING) {
                /* Indicate SCO CHANGED Request to HCI */
                IHC(scoTab)[i].state = SCO_ACTIVE;
                OS_MemSet(complete, 0, 9);
                sco = &IHC(scoTab)[i];
                complete[0] = HC_STATUS_SUCCESS;
                complete[1] = (U8)(sco - IHC(scoTab)) + NUM_BT_HANDLES;

                HC_PostEvent(HCE_SYNC_CONN_CHANGED, complete, 9);
                return;
            }
        }

        /* Indicate the Connection to the Host */
        for (i = 0; i < NUM_SCO_CONNS; i++) {
            if (IHC(scoTab)[i].parent == Conn && IHC(scoTab)[i].state == SCO_CONNECTING) {
                break;
            }
        }
        Assert (i < NUM_SCO_CONNS);

        sco = &IHC(scoTab)[i];
        sco->state = SCO_ACTIVE;

        /* Indicate CONNECT Complete (success) to HCI */
        complete[0]  = HC_STATUS_SUCCESS;
        /* SCO Handle = SCO index + NumBtHandles Offset */
        complete[1]  = (U8)(sco - IHC(scoTab)) + NUM_BT_HANDLES;
        complete[2]  = 0;
        ConnAddr2BdAddr(Conn, (BD_ADDR *)(complete+3));
        complete[9]  = sco->linkType;
        complete[10]  = 0;          /* Tx interval */
        complete[11] = 0;           /* rTx window */
        StoreLE16(complete+12, 30); /* Rx packet len */
        StoreLE16(complete+14, 30); /* Tx packet len */
        complete[16] = 2;           /* Air Mode */

        HC_PostEvent(HCE_SYNC_CONNECT_COMPLETE, complete, 17);
        break;
#endif

    case HCP_LMP_SWITCH_REQ:
        Conn->role = (U8)Conn->modeInt;

        if (Conn->state == LINK_ACCEPTING) {
            StoreLE16(lmpmsg, HCP_LMP_ACCEPTED);
            StoreLE16(lmpmsg+2, HCP_LMP_CONN_REQ);    /* accepted-opcode */
            HC_SendLmp(Conn->link, lmpmsg, 4, 0);
             
            Conn->state = LINK_SETUP;

        }
        else {
            /* If any ACL Data packets have queued up, send them */
            HC_SendQueuedAclData(Conn);
        }
        Conn->waitModeChange = FALSE;

        /* Post ModeChange event */
        lmpaccepted[0] = HC_STATUS_SUCCESS;
        ConnAddr2BdAddr(Conn, (BD_ADDR *)(lmpaccepted+1));
        lmpaccepted[7] = Conn->role;
        HC_PostEvent(HCE_ROLE_CHANGE, lmpaccepted, 8);
        break;

    case HCP_LMP_HOLD_REQ:
        /* Set mode to Hold */
        Conn->state = LINK_HOLD;

        /* Post ModeChange event */
        complete[0] = HC_STATUS_SUCCESS;
        StoreLE16(complete+1, ConnHndl);
        complete[3] = BLM_HOLD_MODE;
        StoreLE16(complete+4, Conn->modeInt);

        HC_PostEvent(HCE_MODE_CHNG, complete, 6);

        /* Set up Timer Structure and call EVM_StartTimer */
        Conn->connTimer.func = HoldTimerFire;

        /* HoldTimer.context is used to store the ConnHndl so that when the timer
        * is fired, the correct ConnHndl can be set active again 
        */
        Conn->connTimer.context = (void *)ConnHndl;

        /* modeInt is measured in # of baseband slots which are .625 msec */
        EVM_StartTimer(&(Conn->connTimer), (TimeT)(Conn->modeInt*.625));
        break;

    case HCP_LMP_SNIFF_REQ:

        /* See Figure 6.1 (sub-scenario 2) on page 1063 of BT spec 1.0b */
        Conn->state = LINK_SNIFF;

        /* Set up Timer Structure and call EVM_StartTimer */
        Conn->connTimer.func = SniffTimerFire;

        /* HoldTimer.context is used to store the ConnHndl */
        Conn->connTimer.context = (void *)ConnHndl;

        /* sniffInt is measured in # of baseband slots which are .625 msec */
        EVM_StartTimer(&(Conn->connTimer), (TimeT)(Conn->modeInt*.625));


        /* Post ModeChange event */
        complete[0] = HC_STATUS_SUCCESS;
        StoreLE16(complete+1, ConnHndl);
        complete[3] = BLM_SNIFF_MODE;
        StoreLE16(complete+4, Conn->modeInt);

        HC_PostEvent(HCE_MODE_CHNG, complete, 6);
        break;

    case HCP_LMP_UNSNIFF_REQ:
        /* See Figure 6.1 (sub-scenario 3) on page 1063 of BT spec 1.0b */
        /* Set connection state to active */
        
        /* If any ACL Data packets have queued up while in hold, send them */
        HC_SendQueuedAclData(Conn);

        Conn->state = LINK_ACTIVE;
        Conn->waitModeChange = FALSE;

        /* Post ModeChange event */
        complete[0] = HC_STATUS_SUCCESS;
        StoreLE16(complete+1, ConnHndl);
        complete[3] = BLM_ACTIVE_MODE;
        StoreLE16(complete+4, 0);

        HC_PostEvent(HCE_MODE_CHNG, complete, 6);

        /* If any ACL Data packets have queued up while in sniff, send them */
        EVM_CancelTimer(&(Conn->connTimer));
        HC_SendQueuedAclData(Conn);

        break;

    case HCP_LMP_PARK_REQ:
        Conn->state = LINK_PARK;

        /* Send a Park command to Slave */
        StoreLE16(lmpmsg, HCP_LMP_PARK);
        StoreLE16(lmpmsg+2, Conn->modeInt);
        HC_SendLmp(Conn->link, lmpmsg, 4, 0); 

        /* Post ModeChange event */
        complete[0] = HC_STATUS_SUCCESS;
        StoreLE16(complete+1, ConnHndl);
        complete[3] = BLM_PARK_MODE;
        StoreLE16(complete+4, Conn->modeInt);
    
        HC_PostEvent(HCE_MODE_CHNG, complete, 6);
        break;

    case HCP_LMP_UNPARK_REQ:
        /* See Figure 6.4 (all sub scenarios) on page 1067 of BT spec 1.0b */
        
        /* If any ACL Data packets have queued up while in hold, send them */
        HC_SendQueuedAclData(Conn);
        
        Conn->state = LINK_ACTIVE;
        Conn->waitModeChange = FALSE;

        /* Post ModeChange event */
        complete[0] = HC_STATUS_SUCCESS;
        StoreLE16(complete+1, ConnHndl);
        complete[3] = BLM_ACTIVE_MODE;
        StoreLE16(complete+4, 0);

        HC_PostEvent(HCE_MODE_CHNG, complete, 6);

        /* If any ACL Data packets have queued up while in park, send them */
        HC_SendQueuedAclData(Conn);
        break;

    case HCP_LMP_ENC_START_REQ:
        Conn->secFlags |= LINK_SEC_ENCRYPTED;

        /* If link is pending encryption then complete link. */ 
        if (Conn->state == LINK_ENCRYPTING)
        {            
            /* Issue SETUP_COMPLETE to other side */
            StoreLE16(setup, HCP_LMP_SETUP_COMPLETE);
            HC_SendLmp(Conn->link, setup, 2, 0);

            /* If we've already received complete from remote, 
             * indicate connect complete */
            if (Conn->secFlags & LINK_SEC_REMOTE_COMPLETE) {
                ConnectComplete(Conn, HcGetHandleFromConn(Conn));
            }
        }
        else {
            /* Otherwise just send out a normal ENCRYPT_CHNG event */
            complete[0] = HC_STATUS_SUCCESS;
            StoreLE16(complete+1, ConnHndl);
            complete[3] = TRUE;
            StoreLE16(complete+4, 0);
            HC_PostEvent(HCE_ENCRYPT_CHNG, complete, 6);
        }
        break;

    case HCP_LMP_ENC_STOP_REQ:
        Conn->secFlags &= ~LINK_SEC_ENCRYPTED;

        complete[0] = HC_STATUS_SUCCESS;
        StoreLE16(complete+1, ConnHndl);
        complete[3] = FALSE;
        StoreLE16(complete+4, 0);
        HC_PostEvent(HCE_ENCRYPT_CHNG, complete, 6);
        break;

    default:
        Assert(0);
    }
}

/* ---------------------------------------------------------------------------
 * HC_ProcessLmpNotAccepted 
 * 
 */
static void HC_ProcessLmpNotAccepted(BtConn *Conn, U16 Opcode, U8 Reason, U16 ConnHndl)
{
    U8  complete[18];
    U8  pinReq[6];
    U8  lmpmsg[6];

#if NUM_SCO_CONNS > 0
    I8  i;
#endif

    switch(Opcode) {
    case HCP_LMP_CONN_REQ:
        Assert(Conn->role == BCR_MASTER);

        /* Indicate CONNECT Complete (failure) to HCI */
        complete[0]  = Reason;
        StoreLE16(complete+1, 0xFFFF);  /* Invalid Conn Handle */
        ConnAddr2BdAddr(Conn, (BD_ADDR *)(complete+3));
        complete[9]  = 0x01;   /* ACL Link Type */
        complete[10] = 0x00;   /* No encryption */

        HC_PostEvent(HCE_CONNECT_COMPLETE, complete, 11);

        /* Close down link */
        HC_CloseLink(Conn);
        break;

    case HCP_LMP_AU_RAND:
        /* The requesting device lost it's link key, try to get
         * a PIN number and pair again.
         */
        ConnAddr2BdAddr(Conn, (BD_ADDR *)pinReq);
        HC_PostEvent(HCE_PIN_CODE_REQ, pinReq, 6);

        AuthSwitchState(Conn, AuthPinReq_A);
        break;

    case HCP_LMP_IN_RAND:
        /* Peer rejected our Authentication attempt */
        Assert(Reason != HC_STATUS_SUCCESS);
        HandleAuthState(HCP_LMP_NOT_ACCEPTED, Conn, 0, 0);
        break;

#if NUM_SCO_CONNS > 0
    case HCP_LMP_SCO_LINK_REQ:
        /* Indicate the Connection to the Host */

        for (i = 0; i < NUM_SCO_CONNS; i++) {
            if (IHC(scoTab)[i].parent == Conn && IHC(scoTab)[i].state == SCO_CONNECTING) {
                break;
            }
        }
        Assert (i < NUM_SCO_CONNS);

        /* Free the connection */
        OS_MemSet((U8 *)&(IHC(scoTab)[i]), 0, sizeof(ScoConn));

        /* Indicate CONNECT Complete (failure) to HCI */
        complete[0]  = Reason;
        StoreLE16(complete+1, 0xFFFF);  /* Invalid Conn Handle */
        ConnAddr2BdAddr(Conn, (BD_ADDR *)(complete+3));
        complete[9]  = 0;   /* SCO Link Type */
        complete[10] = 0;   /* No encryption */

        HC_PostEvent(HCE_CONNECT_COMPLETE, complete, 11);
        break;

    case HCP_LMP_ESCO_LINK_REQ:
        /* Indicate the Connection to the Host */

        for (i = 0; i < NUM_SCO_CONNS; i++) {
            if (IHC(scoTab)[i].parent == Conn && IHC(scoTab)[i].state == SCO_CONNECTING) {
                break;
            }
        }
        Assert (i < NUM_SCO_CONNS);

        /* Indicate CONNECT Complete (failure) to HCI */
        complete[0]  = Reason;
        StoreLE16(complete+1, 0xFFFF);  /* Invalid Conn Handle */
        ConnAddr2BdAddr(Conn, (BD_ADDR *)(complete+3));
        complete[9]  = IHC(scoTab)[i].linkType;
        complete[10]  = 0;          /* Tx interval */
        complete[11] = 0;           /* rTx window */
        StoreLE16(complete+12, 30); /* Rx packet len */
        StoreLE16(complete+14, 30); /* Tx packet len */
        complete[16] = 2;           /* Air Mode */

        /* Free the connection */
        OS_MemSet((U8 *)&(IHC(scoTab)[i]), 0, sizeof(ScoConn));

        HC_PostEvent(HCE_SYNC_CONNECT_COMPLETE, complete, 17);
        break;

#endif

    case HCP_LMP_HOLD_REQ:
    case HCP_LMP_SNIFF_REQ:
    case HCP_LMP_PARK_REQ:
        /* If any ACL Data packets have queued up while in hold, send them */
        HC_SendQueuedAclData(Conn);
        
        Conn->state = LINK_ACTIVE;        
        Conn->waitModeChange = FALSE;

        /* Post ModeChange event */
        complete[0] = Reason;
        StoreLE16(complete+1, ConnHndl);
        complete[3] = BLM_ACTIVE_MODE;
        StoreLE16(complete+4, 0);

        HC_PostEvent(HCE_MODE_CHNG, complete, 6);

        /* If any ACL Data packets have queued up while in park, send them */
        HC_SendQueuedAclData(Conn);
        break;

    case HCP_LMP_SWITCH_REQ:

        /* is this switch request during connection setup? */
        if (Conn->state == LINK_ACCEPTING) {
            Conn->waitModeChange = FALSE;
            Conn->roleChanged = FALSE;

            /* Post ModeChange event */
            complete[0] = HC_STATUS_ROLE_CHANGE_DISALLOWED;
            ConnAddr2BdAddr(Conn, (BD_ADDR *)(complete+1));
            complete[7] = Conn->role;
            HC_PostEvent(HCE_ROLE_CHANGE, complete, 8);

            /* Post LMP event */
            Conn->role = (Conn->modeInt == BCR_MASTER) ? BCR_SLAVE : BCR_MASTER;
            StoreLE16(lmpmsg, HCP_LMP_ACCEPTED);
            StoreLE16(lmpmsg+2, HCP_LMP_CONN_REQ);    /* accepted-opcode */
            HC_SendLmp(Conn->link, lmpmsg, 4, 0);
             
            Conn->state = LINK_SETUP;
        }
        else {
            /* If any ACL Data packets have queued up while in hold, send them */
            HC_SendQueuedAclData(Conn);
            
            Conn->state = LINK_ACTIVE;        
            Conn->waitModeChange = FALSE;

            /* Post ModeChange event */
            complete[0] = HC_STATUS_UNSUPP_REMOTE_FEATURE;
            ConnAddr2BdAddr(Conn, (BD_ADDR *)(complete+1));
            complete[7] = Conn->role;
            HC_PostEvent(HCE_ROLE_CHANGE, complete, 8);
        }
        break;        

    default:
        Assert(0);
    }
}


/* ---------------------------------------------------------------------------
 * ConnectComplete
 * 
 *    Prepare a connection for activity.
 */
static void ConnectComplete(BtConn *Conn,U16 ConnHndl)
{
    U8 complete[11];

    Conn->state = LINK_ACTIVE;

    /* Indicate CONNECT Complete (success) to HCI */
    complete[0]  = HC_STATUS_SUCCESS;
    StoreLE16(complete+1, ConnHndl);       /* Conn Handle */
    ConnAddr2BdAddr(Conn, (BD_ADDR *)(complete+3));
    complete[9]  = 0x01;   /* ACL Link */
    complete[10] = (Conn->secFlags & LINK_SEC_ENCRYPTED?1:0);

    HC_PostEvent(HCE_CONNECT_COMPLETE, complete, 11);

    /* Open inquiry socket to receive potential broadcast data */
    HC_OpenInquirySocket();
}

/* ---------------------------------------------------------------------------
 * SendSRes
 *
 * Sends an authentication response based on the current link key and random
 * number.
 */
static void SendSRes(BtConn *conn)
{
    U8 sres[5];
    StoreLE16(sres, HCP_LMP_SRES);
    HC_GenerateSRes(conn->tmpRand, conn->linkKey, &IHC(myAddr), sres+2);       
    HC_SendLmp(conn->link, sres, 5, 0);
}

/* ---------------------------------------------------------------------------
 * SendAuRand
 *
 * Sends an authentication challenge based on current link key
 */
static void SendAuRand(BtConn *conn)
{
    U8      auRand[17];
    BD_ADDR claimant;

    StoreLE16(auRand, HCP_LMP_AU_RAND);
    HC_GenerateRandNr(auRand+2);
    HC_SendLmp(conn->link, auRand, 17, 0);

    /* Generate the anticipated SRES response value */
    ConnAddr2BdAddr(conn, &claimant);
    HC_GenerateSRes(auRand+2, conn->linkKey, &claimant, conn->sRes);
}

/* ---------------------------------------------------------------------------
 * NotifyNewKey
 *
 * Inform host about a new link key for this connection.
 */
static void NotifyNewKey(BtConn *conn)
{
    U8 newKey[23];

    /* We now have a new Link Key, inform the Host */
    ConnAddr2BdAddr(conn, (BD_ADDR *)newKey);
    OS_MemCopy(newKey+6, conn->linkKey, 16);
    newKey[22] = conn->linkKeyType - 2;

    HC_PostEvent(HCE_LINK_KEY_NOTIFY, newKey, 23);
}

/* ---------------------------------------------------------------------------
 * StartAuthenticating
 *
 * Kicks off the authentication process as the verifier.
 */
static void StartAuthenticating(BtConn *conn)
{
    U8 *keyReq;

    /* Wait for authorization response */
    AuthSwitchState(conn, AuthKeyReq_A);

    /* Query Host for Link Key */
    keyReq = malloc(sizeof(BD_ADDR));
    Assert(keyReq);
    ConnAddr2BdAddr(conn, (BD_ADDR *)keyReq);
    HC_DelayEvent(HCE_LINK_KEY_REQ, keyReq, 6);
}

/* ---------------------------------------------------------------------------
 * AuthenticationComplete
 *
 * Handle completion of the Authentication process
 */
static void AuthenticationComplete(BtConn *conn, BOOL success)
{
    U8 complete[11], setup[3];

    U16 connHandle = HcGetHandleFromConn(conn);

    /* Back to initial state */
    AuthSwitchState(conn, AuthReady);

    if (success) {
        Report(("HC: Authentication was successful\n"));

        /* Indicate that the link is authenticated */
        conn->secFlags |= LINK_SEC_AUTHENTICATED;

        /* If connection was pending auth., complete connection */
        if (conn->state == LINK_AUTHENTICATING) {

            /* Waiting for encryption as well? */
            if (IHC(flags) & IHCF_ENCRYPT)
            {
                /* Start the encryption process. When complete,
                 * send SETUP_COMP in return. 
                 */
                conn->state = LINK_ENCRYPTING;
                HC_SetConnectionEncryption(connHandle, TRUE);
            }
            else
            {
                /* Send LMP_Setup_Complete since authentication is over. */
                StoreLE16(setup, HCP_LMP_SETUP_COMPLETE);
                HC_SendLmp(conn->link, setup, 2, 0);

                /* If we've already received SETUP_COMPLETE then indicate
                 * that the connection is ready.
                 */
                if (conn->secFlags & LINK_SEC_REMOTE_COMPLETE) {
                    ConnectComplete(conn, HcGetHandleFromConn(conn));
                }
                /* Otherwise, we'll wait for the SETUP_COMPLETE message.
                 */
            }
        } else {

            /* Indicate success to host if this was a host-initiated 
             * authentication
             */
            if (conn->secFlags & LINK_SEC_AUTH_REQUESTED) {
                complete[0] = HC_STATUS_SUCCESS;
                StoreLE16(complete+1, connHandle);                    
                HC_PostEvent(HCE_AUTH_COMPLETE, complete, 3);
                conn->secFlags &= ~LINK_SEC_AUTH_REQUESTED;
            }
        }
    } else {
        /* Authentication failed */
        Report(("HC: Authentication was unsuccessful\n"));

        /* If connection was pending auth., indicate failure */
        if (conn->state == LINK_AUTHENTICATING) {

            // RWW - 2001.11.29
            /* Send LMP_Detach to peer, then close the TCP connection. */
            StoreLE16(setup, HCP_LMP_DETACH);
            setup[2] = BEC_AUTHENTICATE_FAILURE;
            HC_SendLmp(conn->link, setup, 3, 0);
            
            
            complete[0]  = HC_STATUS_AUTH_FAILURE;
            StoreLE16(complete+1, 0xFFFF);  /* Invalid Conn Handle */
            ConnAddr2BdAddr(conn, (BD_ADDR *)(complete+3));
            complete[9]  = 0x01;   /* ACL Link Type */
            complete[10] = 0x00;   /* No encryption */

            HC_PostEvent(HCE_CONNECT_COMPLETE, complete, 11);


            
            /* Close down link */
            HC_CloseLink(conn);

        } else {
            /* Otherwise just tell the Host about the fail */
            complete[0] = HC_STATUS_AUTH_FAILURE;
            StoreLE16(complete+1, connHandle);
            HC_PostEvent(HCE_AUTH_COMPLETE, complete, 3);
        }

        /* Reset link key */
        conn->linkKeyType = LINK_KEY_NONE;
        OS_MemSet(conn->linkKey, 0, 16);

    }


}

/* ---------------------------------------------------------------------------
 * AuthReady
 *
 * (Authentication State) Ready for authentication 
 */
void AuthReady(U16 event, BtConn *conn, void *param1, void *param2)
{
    U8  rsp[5], pinReq[6], setup[2];
    U8 *keyReq;

    U16 connHandle = HcGetHandleFromConn(conn);

    Report(("HC: AuthReady processing event %s\n", pHcp(event)));
    switch(event) {

    case HCP_LMP_AU_RAND:
        OS_MemCopy(conn->tmpRand, ((U8 *)param1)+2, 15);
        /* Request for authorization of the current link key */
        if (conn->linkKeyType == LINK_KEY_NONE) {

            /* Query Host for Link Key */
            keyReq = malloc(sizeof(BD_ADDR));
            Assert(keyReq);
            ConnAddr2BdAddr(conn, (BD_ADDR *)keyReq);
            HC_DelayEvent(HCE_LINK_KEY_REQ, keyReq, 6);

            /* Wait around in this state for Host's response */
        } else {
            SendSRes(conn);
        }                
        break;

    case HCP_HCC_KEY_POS:
        /* Host has provided a key. Assume we are authenticated and
         * respond to the authentication request.
         */
        SendSRes(conn);
        break;

    case HCP_HCC_KEY_NEG:
        /* Uh-oh. We don't have the key. Authentication fails. */
        StoreLE16(rsp, HCP_LMP_NOT_ACCEPTED);
        StoreLE16(rsp+2, HCP_LMP_AU_RAND);
        rsp[4] = HC_STATUS_KEY_MISSING;       
        HC_SendLmp(conn->link, rsp, 5, 0);
        break;

    case HCP_LMP_IN_RAND:
        /* This is a pairing request. Store off the random seed and 
         * query for the PIN */
        OS_MemCopy(conn->tmpRand, ((U8 *)param1)+2, 15);
        ConnAddr2BdAddr(conn, (BD_ADDR *)pinReq);
        AuthSwitchState(conn, AuthPinReq_B);
        HC_PostEvent(HCE_PIN_CODE_REQ, pinReq, 6);
        break;

    case HCP_LMP_SETUP_COMPLETE:
        /* Is mode3 authentication/encryption required? (For slaves, this is the only
         * chance to start security)
         */
        if ((((conn->roleChanged == FALSE) && (conn->role == BCR_SLAVE)) ||
            ((conn->roleChanged == TRUE) && (conn->role == BCR_MASTER))) &&
            (IHC(flags) & IHCF_AUTHENTICATE)) {

            if (conn->secFlags & LINK_SEC_AUTHENTICATED) {

                /* If we have already completed authentication then
                 * we're (probably) finished. Check encryption flag.
                 */
                if (IHC(flags) & IHCF_ENCRYPT)
                {
                    /* Enable encryption. When complete, send SETUP_COMPLETE. */
                    conn->state = LINK_ENCRYPTING;
                    HC_SetConnectionEncryption(connHandle, TRUE);
                }
                else
                {
                    StoreLE16(setup, HCP_LMP_SETUP_COMPLETE);            
                    HC_SendLmp(conn->link, setup, 2, 0);
                    ConnectComplete(conn, connHandle);
                }
            } else {

                /* Start slave mode-3 authentication! */
                conn->state = LINK_AUTHENTICATING;
                StartAuthenticating(conn);
            }
        } else {
            /* Only the slave sends back LMP_Setup_Complete,
             * unless they switched roles, then the master needs
             * to send the LMP_Setup_complete back to the slave 
             */
            if (((conn->roleChanged == FALSE) && (conn->role == BCR_SLAVE)) ||
                ((conn->roleChanged == TRUE) && (conn->role == BCR_MASTER))) {
                StoreLE16(setup, HCP_LMP_SETUP_COMPLETE);            
                HC_SendLmp(conn->link, setup, 2, 0);
                conn->roleChanged = FALSE;
            }
            ConnectComplete(conn, connHandle);
        }
        break;

    case HCP_LMP_CONN_REQ:

        /* Is mode3 authentication required? */
        if (IHC(flags) & IHCF_AUTHENTICATE) {
            conn->state = LINK_AUTHENTICATING;
            Assert(conn->linkKeyType == LINK_KEY_NONE);

            StartAuthenticating(conn);

        } else {
            /* Allow the connection to continue normally */

            /* Send LMP_Setup_Complete */
            StoreLE16(setup, HCP_LMP_SETUP_COMPLETE);
            HC_SendLmp(conn->link, setup, 2, 0);

        }
        break;

    default:
        Assert(0);
    }
}


/* ---------------------------------------------------------------------------
 * AuthKeyReq_A
 *
 * (Authentication State) Waiting for a key from the Host
 */
static void AuthKeyReq_A(U16 event, BtConn *conn, void *param1, void *param2)
{
    U8      pinReq[6];

    Report(("HC: AuthKeyReq_A processing event %s\n", pHcp(event)));
    switch(event) {

    case HCP_HCC_KEY_POS:
        /* Send AU_RAND to claimant */
        SendAuRand(conn);

        /* Next state: CheckKey_A */
        AuthSwitchState(conn, AuthCheckKey_A);
        break;

    case HCP_HCC_KEY_NEG:
        // If the key doesn't exist, try to get a PIN number.
        ConnAddr2BdAddr(conn, (BD_ADDR *)pinReq);
        AuthSwitchState(conn, AuthPinReq_A);
        HC_PostEvent(HCE_PIN_CODE_REQ, pinReq, 6);
        break;

    default:
        Assert(0);
    }
}


/* ---------------------------------------------------------------------------
 * AuthCheckKey_A
 *
 * Verify the remote device's key. If the response matches, authentication
 * was successful.
 */
static void AuthCheckKey_A(U16 event, BtConn *conn, void *param1, void *param2)
{
    Report(("HC: AuthCheckKey_A processing event %s\n", pHcp(event)));
    switch(event) {
    case HCP_LMP_NOT_ACCEPTED:
        /* The remote device does not support authentication */
        AuthenticationComplete(conn, FALSE);
        break;

    case HCP_LMP_SRES:
        /* Verify the three-byte authentication response */
        if (OS_MemCmp(((U8 *)param1)+2, 3, conn->sRes, 3) == TRUE) {
            /* The response matched. */
            AuthenticationComplete(conn, TRUE);
        } else {
            /* The response failed. */
            AuthenticationComplete(conn, FALSE);
        }
        break;
    default:
        Assert(0);

    }
}

/* ---------------------------------------------------------------------------
 * AuthPinReq_A
 *
 * Wait for PIN response so that we can continue with pairing.
 */
static void AuthPinReq_A(U16 event, BtConn *conn, void *param1, void *param2)
{
    U8        inRand[17];
    BD_ADDR   tmpAddr;

    Report(("HC: AuthPinReq_A processing event %s\n", pHcp(event)));
    switch(event) {

    case HCP_HCC_PIN_POS:
        /* param1 (U8 *)Pin, param2 (U8)PinLen */

        /* As the verifier we use the claimant's BD_ADDR to generate an INIT key. */
        HC_GenerateRandNr(conn->tmpRand);
        ConnAddr2BdAddr(conn, &tmpAddr);
        HC_GenerateKinit(conn->tmpRand, (U8 *)param1, (U8)param2,
            &tmpAddr, conn->linkKey);
        conn->linkKeyType = LINK_KEY_INIT;

        /* Initiate authentication by sending an IN_RAND to the claimant. */
        StoreLE16(inRand, HCP_LMP_IN_RAND);
        OS_MemCopy(inRand+2, conn->tmpRand, 15);
        HC_SendLmp(conn->link, inRand, 17, 0);

        AuthSwitchState(conn, AuthPairing_A);
        break;

    case HCP_HCC_PIN_NEG:
        /* No PIN, no authentication! */
        AuthenticationComplete(conn, FALSE);
        break;

    default:
        Assert(0);
        break;
    }
}


/* ---------------------------------------------------------------------------
 * AuthPairing_A
 *
 * Wait for response to pairing request (LMP_in_rand) or initial key 
 * authentication.
 */
static void AuthPairing_A(U16 event, BtConn *conn, void *param1, void *param2)
{
    Report(("HC: AuthPairing_A processing event %s\n", pHcp(event)));
    switch(event) {

    case HCP_LMP_NOT_ACCEPTED:
        AuthenticationComplete(conn, FALSE);    
        break;

    case HCP_LMP_IN_RAND:
        /* Whoops! The other device wants to initiate pairing. */
        
        Assert(0);
        /* We can't handle this because we need the pin to regenerate
         * the proper init key. And guess what, we didn't store it.
         */
#ifdef PIN_STORED
        /* Now that we're the claimant, we have to use _my_ BD_ADDR, the current
         * PIN, and the received random number to create the init key.
         */
        conn->linkKeyType = LINK_KEY_INIT;
        OS_MemCopy(conn->tmpRand, ((U8 *)param1)+2, 15);
        HC_GenerateKinit(conn->tmpRand, Pin, PinLen, &IHC(myAddr), conn->linkKey);

        /* Send the accept message back to the other device */
        StoreLE16(accepted, HCP_LMP_ACCEPTED);
        StoreLE16(accepted+2, HCP_LMP_IN_RAND);
        HC_SendLmp(conn->link, accepted, 4, 0);

        /* Now switch over to B mode */
        AuthSwitchState(conn, AuthPairing_B);
        /* TODO: Make sure this is right when AuthPairing_B is written */
#endif /* PIN_STORED */

        break;

    case HCP_LMP_ACCEPTED:
        /* The remote device has accepted our ownership of the pairing process.
         * So verify the current link (init) key.
         */
        SendAuRand(conn);

        /* Wait for the correct response */
        break;

    case HCP_LMP_SRES:
        Assert(conn->linkKeyType == LINK_KEY_INIT);

        /* Verify the three-byte authentication response to our init key. */
        if (OS_MemCmp(((U8 *)param1)+2, 3, conn->sRes, 3)) {
            /* The response matched. Go into combkey_a state and wait for
             * reverse authentication of the init key.
             */
            AuthSwitchState(conn, AuthCombKey_A); 
        } else {
            /* The response failed so quit everything. */
            AuthenticationComplete(conn, FALSE);
        }
        break;

    default:
        Assert(0);
        break;
    }
}

#ifdef gwd
static void printCurrentKey(BtConn *conn)
{
    char buffer[200];  /* Output buffer */
    int i;
    sprintf(buffer, "NEW KEY: ");
    for (i=0;i<16;i++) {
        sprintf(buffer, "%s %02X", buffer, conn->linkKey[i]);
    }
    sprintf(buffer, "%s\n", buffer);
    MessageBox(NULL, buffer, "Key change.", MB_OK);

    Report(("HC: Link key is -"));
    for (i = 0; i < 16; i++) {
        Report(("%02x-", LinkKey[i]));
    }
    Report((". SRES is -"));
    for (i = 0; i < 3; i++) {
        Report(("%02x-", SRes[i]));
    }
    Report(("\n"));

}
#endif 


/* ---------------------------------------------------------------------------
 * AuthCombKey_A
 *
 * We're about to authenticate the init key so we can continue with 
 * the new combination key.
 */
static void AuthCombKey_A(U16 event, BtConn *conn, void *param1, void *param2)
{
    U8 combKey[17];

    Report(("HC: AuthCombKey_A processing event %s\n", pHcp(event)));
    switch(event) {

    case HCP_LMP_AU_RAND:
        OS_MemCopy(conn->tmpRand, ((U8 *)param1)+2, 15);

        /* Send an authentication response based on this number and the 
         * current key
         */
        SendSRes(conn);

        /* Create and send the combination key */

        if (conn->linkKeyType == LINK_KEY_INIT) {
            /* Note: I simplifed this calculation by omitting 
             *       the BD_ADDR component.
             */
            StoreLE16(combKey, HCP_LMP_COMB_KEY);
            HC_GenerateRandNr(conn->tmpRand);
            OS_MemCopy(combKey+2, conn->tmpRand, 15);
            HC_GenerateXorKey(combKey+2, conn->linkKey);
            HC_SendLmp(conn->link, combKey, 17, 0);
            /* Wait for response */
        } else {
            Assert(conn->linkKeyType == LINK_KEY_COMB);
            /* This was an authentication request for the 
             * combination key. Let's indicate the new key 
             * to our host and we're done.
             */
            NotifyNewKey(conn);
            AuthenticationComplete(conn, TRUE);
        }
        break;

    case HCP_LMP_COMB_KEY:
        /* We've received a COMB_KEY back from the other side, which
         * should give us enough data to generate the new combination key. */

        /* First retrieve the peer's RandNr by XORing the received Comb Key
         * with the shared Kinit.
         */
        HC_GenerateXorKey(((U8 *)param1)+2, conn->linkKey);
        /* Now XOR our RandNr with their RandNr to get the key. */
        HC_GenerateXorKey(((U8 *)param1)+2, conn->tmpRand);
        /* Now store it and fix the 16th byte */
        OS_MemCopy(conn->linkKey, ((U8 *)param1)+2, 15);
        conn->linkKey[15] = 0x00;
        conn->linkKeyType = LINK_KEY_COMB;

        /* Now authenticate on this new key. */
        SendAuRand(conn);
        break;

    case HCP_LMP_SRES:
        /* Verify the new combination key */
        if (!OS_MemCmp(((U8 *)param1)+2, 3, conn->sRes, 3)) {
            AuthenticationComplete(conn, FALSE);
        }
        /* Don't do anything; wait for reverse authentication on comb key */
        break;

    default:
        Assert(0);
        break;
    }
}


/* ---------------------------------------------------------------------------
 * AuthPinReq_B
 *
 * The remote device has requested pairing so handle everything up to
 * creation of the combination key.
 */
static void AuthPinReq_B(U16 event, BtConn *conn, void *param1, void *param2)
{
    U8 accepted[4], rsp[5],combKey[17];

    Report(("HC: AuthPinReq_B processing event %s\n", pHcp(event)));

    switch(event) {

    case HCP_HCC_PIN_POS:
        /* param1 (U8 *)Pin, param2 (U8)PinLen */

        /* As the claimant we use our own BD_ADDR to generate an INIT key. */
        HC_GenerateKinit(conn->tmpRand, (U8 *)param1, (U8)param2,
            &IHC(myAddr), conn->linkKey);
        conn->linkKeyType = LINK_KEY_INIT;

        /* Tell the verifier we accept authentication */
        StoreLE16(accepted, HCP_LMP_ACCEPTED);
        StoreLE16(accepted+2, HCP_LMP_IN_RAND);
        HC_SendLmp(conn->link, accepted, 4, 0);

        /* Wait around for mutual init key authentication */
        break;

    case HCP_HCC_PIN_NEG:
        /* No PIN, we must reject the request. */
        StoreLE16(rsp, HCP_LMP_NOT_ACCEPTED);
        StoreLE16(rsp+2, HCP_LMP_IN_RAND);
        rsp[4] = HC_STATUS_PAIRING_DISALLOWED;       
        HC_SendLmp(conn->link, rsp, 5, 0);
        AuthSwitchState(conn, AuthReady);
        break;

    case HCP_LMP_AU_RAND:
        /* Let's verify the INIT key. */
        OS_MemCopy(conn->tmpRand, ((U8 *)param1)+2, 15);
        SendSRes(conn);
        SendAuRand(conn);
        break;

    case HCP_LMP_NOT_ACCEPTED:
        /* Something we did ticked off the other device. Let's quit. */
        conn->linkKeyType = LINK_KEY_NONE;
        OS_MemSet(conn->linkKey, 0, 16);
        AuthSwitchState(conn, AuthReady);        
        break;

    case HCP_LMP_SRES:
        /* Check for successful verification */
        if (!OS_MemCmp(((U8 *)param1)+2, 3, conn->sRes, 3)) {
            /* The other device has the wrong link key. So give up on
             * this authentication process.
             */
            conn->linkKeyType = LINK_KEY_INIT;
            AuthSwitchState(conn, AuthReady);

            /* TODO: Should we tell the host about this authentication failure? 
             * After all, our host didn't initiate it....
             */
        }

        /* Don't do anything, wait for COMB_KEY */
        break;

    case HCP_LMP_COMB_KEY:
        if (conn->linkKeyType != LINK_KEY_INIT) {
            /* Remote device is trying to pull a fast one. */
            Assert(0);
        }

        /* Send back an appropriate response */
        StoreLE16(combKey, HCP_LMP_COMB_KEY);
        /* Note: I simplifed this calculation by omitting 
         *       the BD_ADDR component.
         */
        HC_GenerateRandNr(conn->tmpRand);
        OS_MemCopy(combKey+2, conn->tmpRand, 15);
        HC_GenerateXorKey(combKey+2, conn->linkKey);
        HC_SendLmp(conn->link, combKey, 17, 0);

        /* Finally, compute the actual combination link key: */
        HC_GenerateXorKey(((U8 *)param1)+2, conn->linkKey);
        /* Now XOR our RandNr with their RandNr to get the key. */
        HC_GenerateXorKey(((U8 *)param1)+2, conn->tmpRand);

        /* Now store it and fix the 16th byte */
        OS_MemCopy(conn->linkKey, ((U8 *)param1)+2, 15);
        conn->linkKey[15] = 0x00;
        conn->linkKeyType = LINK_KEY_COMB;

        /* Wait for authentication of this new key */
        AuthSwitchState(conn, AuthCombKey_B);
        break;

    default:
        Assert(0);
        break;
    }
}


/* ---------------------------------------------------------------------------
 * AuthCombKey_B
 *
 * Handle authentication of a new combination key.
 */
static void AuthCombKey_B(U16 event, BtConn *conn, void *param1, void *param2)
{
    Report(("HC: AuthCombKey_B processing event %s\n", pHcp(event)));
    switch(event) {

    case HCP_LMP_AU_RAND:
        OS_MemCopy(conn->tmpRand, ((U8 *)param1)+2, 15);
        SendSRes(conn);
        SendAuRand(conn);
        /* Wait for response to our AuRand. */
        break;

    case HCP_LMP_SRES:
        if (OS_MemCmp(((U8 *)param1)+2, 3, conn->sRes, 3)) {
            /* The combination key has been verified. Tell
             * the host about it. 
             */
            NotifyNewKey(conn);
            AuthSwitchState(conn, AuthReady);
            AuthenticationComplete(conn, TRUE);
        } else {
            /* Key verification step failed */
            Assert(0);
        }
        break;

    default:
        Assert(0);
        break;
    }
}


/* ---------------------------------------------------------------------------
 * HC_ProcessLmCommand
 * 
 */
void HC_ProcessLmCommand(U16 Hcp, BtConn *Conn, U16 ConnHndl)
{
    int      len, dataLen;
    U8       rxData[250], connReq[10], complete[11];
    
    /* First, receive the rest of the packet, 'len' includes 2-byte opcode */
    switch(Hcp) {
    case HCP_LMP_NAME_RSP:
        len = 250;
        break;
        
    case HCP_LMP_IN_RAND:
    case HCP_LMP_COMB_KEY:
    case HCP_LMP_AU_RAND:
        len = 17;
        break;
        
    case HCP_LMP_ESCO_LINK_REQ:
        len = 12;
        break;

    case HCP_LMP_SNIFF:
    case HCP_LMP_SNIFF_REQ:
        len = 11;
        break;
        
    case HCP_LMP_CONN_REQ:
        len = 7;
        break;

    case HCP_LMP_FEATURES_RSP:
        len = 10;
        break;
        
    case HCP_LMP_NOT_ACCEPTED:
    case HCP_LMP_SRES:
    case HCP_LMP_SCO_LINK_REQ:
    case HCP_LMP_NOT_ACCEPTED_EXT:
        len = 5;
        break;
        
    case HCP_LMP_ACCEPTED:
    case HCP_LMP_REMOVE_SCO_REQ:
    case HCP_LMP_HOLD:
    case HCP_LMP_HOLD_REQ:
    case HCP_LMP_PARK:
    case HCP_LMP_PARK_REQ:
    case HCP_LMP_REMOVE_ESCO_REQ:
    case HCP_LMP_ACCEPTED_EXT:
        len = 4;
        break;
        
    case HCP_LMP_SWITCH_REQ:
    case HCP_LMP_DETACH:
        len = 3;
        break;
        
    case HCP_LMP_SETUP_COMPLETE:
    case HCP_LMP_NAME_REQ:
    case HCP_LMP_UNSNIFF_REQ:
    case HCP_LMP_UNPARK_REQ:
    case HCP_LMP_ENC_START_REQ:
    case HCP_LMP_ENC_STOP_REQ:
        len = 2;
        break;
        
    default:
       len=0;
    }
    StoreLE16(rxData, Hcp);
    
    if (len > 2) {
        dataLen = recv(Conn->link, rxData+2, len-2, 0);
        if (dataLen == SOCKET_ERROR) {
            Assert(WSAGetLastError() == WSAEWOULDBLOCK);
            Assert(0);     /* We can't drop an LM Command!! */
            return;
        }
    }
#if XA_DEBUG == XA_ENABLED && HCI_ALLOW_PRESCAN == XA_ENABLED && XA_SNIFFER == XA_ENABLED
    DumpLmpPacket("RX", rxData, (U16)len);
#endif
    /* 
     * Dispatch the LM Command
     */
    switch(Hcp) {

    case HCP_LMP_CONN_REQ:
        Report(("HC: ProcessLmCommand(%x) LMP_Conn_Req\n", ConnHndl));
        Assert(Conn->role == BCR_SLAVE);
        
        Conn->port = LEtoHost16(rxData+5);
        
        /* Indicate CONNECT Request to HCI */
        OS_MemSet(connReq, 0, 10);
        ConnAddr2BdAddr(Conn, (BD_ADDR *)(connReq)); 
        connReq[6] = rxData[2];   /* Class of device 3-bytes */
        connReq[7] = rxData[3];
        connReq[8] = rxData[4];
        connReq[9] = TRUE;        /* ACL Connection */
        
        HC_PostEvent(HCE_CONNECT_REQUEST, connReq, 10);
        break;

    case HCP_LMP_SETUP_COMPLETE:
        Report(("HC: ProcessLmCommand(%x) LMP_Setup_Complete\n", ConnHndl));
        Conn->secFlags |= LINK_SEC_REMOTE_COMPLETE;

        /* Let the authentication handler decide whether to send back
         * SETUP_COMPLETE yet.
         */
        HandleAuthState(Hcp, Conn, 0, 0);    
        break;
    

        
    case HCP_LMP_DETACH:
        Report(("HC: ProcessLmCommand(%x) LMP_Detach\n", ConnHndl));
        HC_DisconnectInd(Conn->link);
        break;
    
    case HCP_LMP_ACCEPTED:
        Report(("HC: ProcessLmCommand(%x) LMP_Accepted: Opcode %04x\n", ConnHndl, LEtoHost16(rxData+2)));
        
        HC_ProcessLmpAccepted(Conn, LEtoHost16(rxData+2), ConnHndl);
        break;
    
    case HCP_LMP_NOT_ACCEPTED:
        Report(("HC: ProcessLmCommand(%x) LMP_Not_Accepted: Opcode %04x\n", ConnHndl, LEtoHost16(rxData+2)));
        
        HC_ProcessLmpNotAccepted(Conn, LEtoHost16(rxData+2), rxData[4], ConnHndl);
        break;

    case HCP_LMP_ACCEPTED_EXT:
        Report(("HC: ProcessLmCommand(%x) LMP_AcceptedExt: Opcode %04x\n", ConnHndl, LEtoHost16(rxData+3)));
        
        HC_ProcessLmpAccepted(Conn, LEtoHost16(rxData+2), ConnHndl);
        break;
    
    case HCP_LMP_NOT_ACCEPTED_EXT:
        Report(("HC: ProcessLmCommand(%x) LMP_Not_AcceptedExt: Opcode %04x\n", ConnHndl, LEtoHost16(rxData+3)));
        
        HC_ProcessLmpNotAccepted(Conn, LEtoHost16(rxData+2), rxData[4], ConnHndl);
        break;

    case HCP_LMP_IN_RAND:
    case HCP_LMP_AU_RAND:
    case HCP_LMP_SRES:
    case HCP_LMP_COMB_KEY:
        HandleAuthState(Hcp, Conn, rxData, len);
        break;
   

#if NUM_SCO_CONNS > 0
    case HCP_LMP_SCO_LINK_REQ:
        Report(("HC: ProcessLmCommand(%x) LMP_SCO_Link_Req\n", ConnHndl));
        
        HC_ScoLinkRequest(Conn, rxData+2);
        break;
    
    case HCP_LMP_REMOVE_SCO_REQ:
    case HCP_LMP_REMOVE_ESCO_REQ:
        Report(("HC: ProcessLmCommand(%x) LMP_Remove_SCO_Req\n", ConnHndl));
        
        HC_ScoRemoveLink(rxData[2], rxData[3]);
        break;

    case HCP_LMP_ESCO_LINK_REQ:
        Report(("HC: ProcessLmCommand(%x) LMP_SCO_Link_Req\n", ConnHndl));
        
        HC_EScoLinkRequest(Conn, rxData+2);
        break;
#endif
    
    case HCP_LMP_NAME_REQ:
        HC_SendLocalName(Conn);
        break;
    
    case HCP_LMP_NAME_RSP:
        HC_ProcessNameRsp(Conn, rxData+2);
        break;
    
    case HCP_LMP_SWITCH_REQ:
        HC_ProcessSwitchReq(ConnHndl, rxData[2]);
        break;
        
    case HCP_LMP_HOLD:
        Assert(0);
        break;

    case HCP_LMP_HOLD_REQ:
        HC_ProcessHoldReq(ConnHndl, LEtoHost16(rxData+2));
        break;
    
    case HCP_LMP_SNIFF_REQ:
        {
            U8   timerflag = rxData[2];
            U16  DSniff = LEtoHost16(rxData+3);
            U16  TSniff = LEtoHost16(rxData+5);
            U16  Sniffatt = LEtoHost16(rxData+7);
            U16  SniffTO = LEtoHost16(rxData+9);
            HC_ProcessSniffReq(ConnHndl, timerflag, DSniff, TSniff, Sniffatt, SniffTO);
        }
        break;
    
    case HCP_LMP_SNIFF:
        /* This command is never used */
        Assert(0);
        break;
    
    case HCP_LMP_UNSNIFF_REQ:
        HC_ProcessUnsniffReq(ConnHndl);
        break;
    
    case HCP_LMP_PARK:
        HC_ProcessPark(ConnHndl, LEtoHost16(rxData+2));
        break;
    
    case HCP_LMP_PARK_REQ:
        HC_ProcessParkReq(ConnHndl, LEtoHost16(rxData+2));
        break;
    
    case HCP_LMP_UNPARK_REQ:
        HC_ProcessUnparkReq(ConnHndl);
        break;

    case HCP_LMP_ENC_START_REQ:
        HC_ProcessEncStartReq(ConnHndl);
        break;

    case HCP_LMP_ENC_STOP_REQ:
        HC_ProcessEncStopReq(ConnHndl);
        break;

    case HCP_LMP_FEATURES:
        HC_SendRemoteFeatures(Conn);
        break;

    case HCP_LMP_FEATURES_RSP:
        HC_ProcessReadFeatureRsp(ConnHndl, rxData+2);
        break;

    default:
        StoreLE16(complete, HCP_LMP_NOT_ACCEPTED);
        StoreLE16(complete+2, Hcp);
        complete[4] = HC_STATUS_UNKNOWN_HCI_CMD;
        HC_SendLmp(Conn->link, complete, 5, 0); 
    }
    return;
}

#if NUM_SCO_CONNS > 0
/* ---------------------------------------------------------------------------
 * HC_ScoLinkRequest
 * 
 */
static void HC_ScoLinkRequest(BtConn *Acl, U8 *rxData)
{
    HcStatus    status;
    I8          i;
    U8          connReq[10], rsp[5];
    ScoConn    *sco = 0;

    /* Find an available SCO handle, looking for an existing 1/2 open one. */
    for (i = 0; i < NUM_SCO_CONNS; i++) {
        if (IHC(scoTab)[i].parent == 0) {
            if (sco == 0) 
                sco = &IHC(scoTab)[i];
            continue;
        }

        if (IHC(scoTab)[i].parent == Acl) {
            /* If we are the slave, we may have initiated this SCO connection.
             * If so, call HC_ScoAcceptConn(), since we don't want to indicate
             * it to the Host.
             */
            if ((Acl->role == BCR_SLAVE) && (IHC(scoTab)[i].state == SCO_CONNECTING) &&
                IHC(scoTab)[i].localRequest) {
                IHC(scoTab)[i].handle = rxData[0];
                HC_ScoAcceptConn(Acl);
                return;
            }
        }
    }

    if (sco == 0) {
        /* No more SCO Links available */
        status = HC_STATUS_HOST_REJ_NO_RESOURCES;
        goto RejectConn;
    }

    sco->parent = Acl;
    sco->state = SCO_CONNECTING;
    sco->localRequest = FALSE;

    /* Master Assigns the SCO handle */
    if (Acl->role == BCR_MASTER) {
        Assert(rxData[0] == 0);
        sco->handle = (U8)(sco - IHC(scoTab))+1;
    } 
    else sco->handle = rxData[0];

    /* Indicate SCO CONNECT Request to HCI */
    OS_MemSet(connReq, 0, 10);
    ConnAddr2BdAddr(Acl, (BD_ADDR *)(connReq)); 
    connReq[6] = 0;   /* Class of device 3-bytes */
    connReq[7] = 0;
    connReq[8] = 0;
    connReq[9] = 0;  /* SCO Connection */

    HC_PostEvent(HCE_CONNECT_REQUEST, connReq, 10);
    return;

RejectConn:
    /* The INet driver is unable to accept the SCO connection */
    StoreLE16(rsp, HCP_LMP_NOT_ACCEPTED);
    StoreLE16(rsp+2, HCP_LMP_SCO_LINK_REQ);
    rsp[4] = status;
        
    HC_SendLmp(Acl->link, rsp, 5, 0);
}

/* ---------------------------------------------------------------------------
 * HC_ScoRemoveLink
 * 
 */
static void HC_ScoRemoveLink(U8 ScoHandle, U8 Reason)
{
    ScoConn *sco = FindScoLinkbyHandle(ScoHandle);
    U8       discEvent[4], rsp[4];

    Assert(sco);

    /* Indicate disconnect to Host */
    discEvent[0] = HC_STATUS_SUCCESS;
    discEvent[1] = (U8)(sco - IHC(scoTab)) + NUM_BT_HANDLES;
    discEvent[2] = 0;
    discEvent[3] = Reason;

    HC_PostEvent(HCE_DISCONNECT_COMPLETE, discEvent, 4);

    /* Absolve any outstanding SCO packets considered "in use" */
    IHC(hostCurScoPackets) -= sco->hostCurScoPackets;
    
    /* Send response to peer */
    StoreLE16(rsp, HCP_LMP_ACCEPTED);
    StoreLE16(rsp+2, HCP_LMP_REMOVE_SCO_REQ);
        
    HC_SendLmp(sco->parent->link, rsp, 4, 0);

    /* Free the connection */
    OS_MemSet((U8 *)sco, 0, sizeof(ScoConn));
    return;
}

/* ---------------------------------------------------------------------------
 * HC_ScoLinkRequest
 * 
 */
static void HC_EScoLinkRequest(BtConn *Acl, U8 *rxData)
{
    HcStatus    status;
    I8          i;
    U8          connReq[10], rsp[5];
    ScoConn       *sco = 0;
    
    if (rxData[0] != 0) {
        sco = FindScoLinkbyHandle(rxData[0]);
    }

    OS_MemSet(connReq, 0, 10);

    if (sco != 0) {
        /* Indicate SCO CHANGED Request to HCI */
        connReq[0] = HC_STATUS_SUCCESS;
        connReq[1] = (U8)(sco - IHC(scoTab)) + NUM_BT_HANDLES;

        HC_PostEvent(HCE_SYNC_CONN_CHANGED, connReq, 9);

        /* Accept the changes */
        StoreLE16(rsp, HCP_LMP_ACCEPTED_EXT);
        StoreLE16(rsp+2, HCP_LMP_ESCO_LINK_REQ);

        HC_SendLmp(Acl->link, rsp, 4, 0);

        return;
    }

    /* Find an available SCO handle, looking for an existing 1/2 open one. */
    for (i = 0; i < NUM_SCO_CONNS; i++) {
        if (IHC(scoTab)[i].parent == 0) {
            if (sco == 0) 
                sco = &IHC(scoTab)[i];
            continue;
        }

        if (IHC(scoTab)[i].parent == Acl) {
            /* If we are the slave, we may have initiated this SCO connection.
             * If so, call HC_ScoAcceptConn(), since we don't want to indicate
             * it to the Host.
             */
            if ((Acl->role == BCR_SLAVE) && (IHC(scoTab)[i].state == SCO_CONNECTING) &&
                IHC(scoTab)[i].localRequest) {
                IHC(scoTab)[i].handle = rxData[0];
                HC_AcceptSyncConn(Acl);
                return;
            }
        }
    }

    if (sco == 0) {
        /* No more SCO Links available */
        status = HC_STATUS_HOST_REJ_NO_RESOURCES;
        goto RejectConn;
    }

    sco->parent = Acl;
    sco->state = SCO_CONNECTING;
    sco->localRequest = FALSE;

    /* Master Assigns the SCO handle */
    if (Acl->role == BCR_MASTER) {
        Assert(rxData[0] == 0);
        sco->handle = (U8)(sco - IHC(scoTab))+1;
    } 
    else sco->handle = rxData[0];

    /* Indicate SCO CONNECT Request to HCI */
    ConnAddr2BdAddr(Acl, (BD_ADDR *)(connReq)); 
    connReq[6] = 0;   /* Class of device 3-bytes */
    connReq[7] = 0;
    connReq[8] = 0;
    connReq[9] = 2;   /* eSCO Connection */

    HC_PostEvent(HCE_CONNECT_REQUEST, connReq, 10);
    return;

RejectConn:
    /* The INet driver is unable to accept the SCO connection */
    StoreLE16(rsp, HCP_LMP_NOT_ACCEPTED_EXT);
    StoreLE16(rsp+2, HCP_LMP_ESCO_LINK_REQ);
    rsp[4] = status;
        
    HC_SendLmp(Acl->link, rsp, 5, 0);
}
#endif

/* ---------------------------------------------------------------------------
 * HC_GetRemoteName
 * 
 */
HcStatus HC_GetRemoteName(BD_ADDR *remoteDevice)
{
    I8       i;
    U8       nameReq[2];
    BtConn  *conn = 0;
    BD_ADDR  tmpAddr;

    /* Find an matching connection entry */
    for (i = 0; i < NUM_BT_DEVICES; i++) {
        if (IHC(connTab)[i].state == LINK_ACTIVE) {
            ConnAddr2BdAddr(&IHC(connTab)[i], &tmpAddr);

            if (AreBdAddrsEqual(&tmpAddr, remoteDevice)) {
                conn = &IHC(connTab)[i];
                break;
            }
        }
    }

    if (conn && conn->link != INVALID_SOCKET) {
        /* An ACL exists, send a request */
        StoreLE16(nameReq, HCP_LMP_NAME_REQ);

        HC_SendLmp(conn->link, nameReq, 2, 0); 
        conn->flags |= LINK_FLAG_NAME_REQ;
        return HC_STATUS_SUCCESS;
    }

    /* We need to create a temporary ACL go get the name */
    return HC_CreateAclConn(*remoteDevice, 0, TRUE, FALSE);
}

/* ---------------------------------------------------------------------------
 * HC_ReadRemoteSupportedFeatures
 * 
 */
HcStatus HC_ReadRemoteFeatures(U16 ConnHndl)
{
    BtConn *conn;
    U8      readFeatures[2];

    if (ConnHndl > NUM_BT_DEVICES)
        return HC_STATUS_NO_CONNECTION;

    conn = &IHC(connTab)[ConnHndl];

    if (conn->link == INVALID_SOCKET)
        return HC_STATUS_NO_CONNECTION;

    /* Send the request */
    StoreLE16(readFeatures, HCP_LMP_FEATURES);
        
    HC_SendLmp(conn->link, readFeatures, 2, 0); 

    return HC_STATUS_SUCCESS;
}

/* ---------------------------------------------------------------------------
 * HC_SendRemoteFeatures
 * 
 */
static void HC_SendRemoteFeatures(BtConn *Conn)
{
    U8  featureRsp[10];

    StoreLE16(featureRsp, HCP_LMP_FEATURES_RSP);
    OS_MemSet(featureRsp+2, 0, 8);

    HC_SendLmp(Conn->link, featureRsp, 10, 0);

    return;
}

/* ---------------------------------------------------------------------------
 * HC_SendLocalName
 * 
 */
static void HC_SendLocalName(BtConn *Conn)
{
    U8  nameRsp[250];

    Sleep(1000);    /* Simulate BT response time by sleeping a bit. */

    StoreLE16(nameRsp, HCP_LMP_NAME_RSP);
    OS_MemCopy(nameRsp+2, IHC(localName), 248);

    HC_SendLmp(Conn->link, nameRsp, 250, 0);

    return;
}


/* ---------------------------------------------------------------------------
 * HC_HoldModeReq
 * 
 */
HcStatus HC_HoldModeReq(U16 ConnHndl, U16 holdMaxInt, U16 holdMinInt)
{
    BtConn *conn;
    U8      hold[4];
    U16     holdInt = (holdMaxInt + holdMinInt) / 2;
    
    if (ConnHndl > NUM_BT_DEVICES)
        return HC_STATUS_NO_CONNECTION;

    conn = &IHC(connTab)[ConnHndl];

    if (conn->link == INVALID_SOCKET)
        return HC_STATUS_NO_CONNECTION;

    if ((conn->linkPolicy & BLP_HOLD_MODE) != BLP_HOLD_MODE)
        return HC_STATUS_UNSUPP_FEATUR_PARM_VAL;

    /* Send LMP Packet (see def on p. 226 of BT spec 1.0b) */
    StoreLE16(hold, HCP_LMP_HOLD_REQ);
    StoreLE16(hold+2, holdInt);    
    HC_SendLmp(conn->link, hold, 4, 0); 

    conn->waitModeChange = TRUE;

    conn->modeInt = holdInt;

    /* return status */
    return HC_STATUS_SUCCESS;
}


/* ---------------------------------------------------------------------------
 * HoldTimerFire
 * 
 * HoldTimerFire is fired when the hold time has expired.  The device mode is
 * returned to active a ModeChange event is posted to the HC.
 */
void HoldTimerFire(EvmTimer *Timer)
{
    BtConn *conn;
    U16     ConnHndl = (U16)Timer->context;  /* connection handle is stored in timer context */
    U8      modechng[6];        

    OS_StopHardware();

    /* Hold Mode timer has expired, so change mode back to active 
     * and send HCE_MODE_CHNG event to HCI
     */
    conn = &IHC(connTab)[ConnHndl];

    if (conn->state == LINK_HOLD) {
        /* Call HC_PostEvent(ModeChangeEvent) */
        modechng[0] = HC_STATUS_SUCCESS;
        StoreLE16(modechng+1, ConnHndl);
        modechng[3] = BLM_ACTIVE_MODE;
        StoreLE16(modechng+4, 0);

        HC_PostEvent(HCE_MODE_CHNG, modechng, 6);

        /* If any ACL Data packets have queued up while in hold, send them */
        HC_SendQueuedAclData(conn);
    }

    /* Set connection mode to active */
    conn->state = LINK_ACTIVE;
    conn->waitModeChange = FALSE;

    OS_ResumeHardware();
}


/* ---------------------------------------------------------------------------
 * HC_SniffMode
 * 
 */
HcStatus HC_SniffMode(U16 ConnHndl, U16 sniffMaxInt, U16 sniffMinInt, 
                      U16 sniffAttempt, U16 sniffTO) 
{
    BtConn *conn;
    U8      sniff[11];
    U16     sniffInt = (sniffMaxInt + sniffMinInt) / 2;    /* average of max and min */
    
    if (ConnHndl > NUM_BT_DEVICES)
        return HC_STATUS_NO_CONNECTION;

    conn = &IHC(connTab)[ConnHndl];

    if (conn->link == INVALID_SOCKET)
        return HC_STATUS_NO_CONNECTION;

    if ((conn->linkPolicy & BLP_SNIFF_MODE) != BLP_SNIFF_MODE)
      return HC_STATUS_UNSUPP_FEATUR_PARM_VAL;

    conn->waitModeChange = TRUE;

    /* Send LMP Packet (see def on p. 229 of BT spec 1.0b) */
    conn->modeInt = sniffInt;
    StoreLE16(sniff, HCP_LMP_SNIFF_REQ);
    sniff[2] = 0x0;                     /* use Initialization 1 */
    StoreLE16(sniff+3, 0);                /* DSniff */
    StoreLE16(sniff+5, sniffInt);       /* TSniff */
    StoreLE16(sniff+7, sniffAttempt);
    StoreLE16(sniff+9, sniffTO);

    HC_SendLmp(conn->link, sniff, 11, 0); 

    return HC_STATUS_SUCCESS;
}


/* ---------------------------------------------------------------------------
 * SniffTimerFire
 * 
 */
void SniffTimerFire(EvmTimer *Timer)
{
    BtConn *conn;
    U16     ConnHndl = (U16)Timer->context;  /* connection handle is stored in timer context */

    OS_StopHardware();

    conn = &IHC(connTab)[ConnHndl];

    if (conn->state == LINK_SNIFF) {

        /* If any ACL Data packets have queued up while in sniff, send them */
        HC_SendQueuedAclData(conn);
        EVM_StartTimer(Timer, (TimeT)(conn->modeInt*.625));
    }

    OS_ResumeHardware();
}


/* ---------------------------------------------------------------------------
 * HC_ExitSniffMode
 * 
 */
HcStatus HC_ExitSniffMode(U16 ConnHndl) 
{

    BtConn  *conn;
    U8       unsniff[2];
  
    if (ConnHndl > NUM_BT_DEVICES)
        return HC_STATUS_NO_CONNECTION;

    conn = &IHC(connTab)[ConnHndl];

    if (conn->link == INVALID_SOCKET)
        return HC_STATUS_NO_CONNECTION;

    /* Send LMP Packet (see def on p. 230 of BT spec 1.0b) */
    StoreLE16(unsniff, HCP_LMP_UNSNIFF_REQ);
    HC_SendLmp(conn->link, unsniff, 2, 0); 

    return HC_STATUS_SUCCESS;
}

/* ---------------------------------------------------------------------------
 * HC_ParkMode
 * 
 */
HcStatus HC_ParkMode(U16 ConnHndl, U16 beaconMaxInt, U16 beaconMinInt) 
{
    BtConn *conn;
    U8       park[4];
    U16     beaconInt = (beaconMaxInt + beaconMinInt) / 2;
    
    if (ConnHndl > NUM_BT_DEVICES)
        return HC_STATUS_NO_CONNECTION;

    conn = &IHC(connTab)[ConnHndl];

    if (conn->link == INVALID_SOCKET)
        return HC_STATUS_NO_CONNECTION;

  if ((conn->linkPolicy & BLP_PARK_MODE) != BLP_PARK_MODE)
      return HC_STATUS_UNSUPP_FEATUR_PARM_VAL;

    conn->waitModeChange = TRUE;
    conn->modeInt = beaconInt;

        
    /* Send LMP Packet (see def on p. 228 of BT spec 1.0b) */
    conn->modeInt = beaconInt;
    StoreLE16(park, HCP_LMP_PARK_REQ);
    StoreLE16(park+2, beaconInt);
    HC_SendLmp(conn->link, park, 4, 0); 

    return HC_STATUS_SUCCESS;
}

/* ---------------------------------------------------------------------------
 * HC_ExitParkMode
 * 
 */
HcStatus HC_ExitParkMode(U16 ConnHndl) 
{

    BtConn  *conn;
    U8       unsniff[2];
  
    if (ConnHndl > NUM_BT_DEVICES)
        return HC_STATUS_NO_CONNECTION;

    conn = &IHC(connTab)[ConnHndl];

    if (conn->link == INVALID_SOCKET)
        return HC_STATUS_NO_CONNECTION;

    /* Send LMP Packet */
    StoreLE16(unsniff, HCP_LMP_UNSNIFF_REQ);
    HC_SendLmp(conn->link, unsniff, 2, 0); 

    return HC_STATUS_SUCCESS;
}

/* ---------------------------------------------------------------------------
 * HC_ProcessNameRsp
 * 
 */
static void HC_ProcessNameRsp(BtConn *Conn, U8 *rxData)
{
    U8 nameRsp[255];

    if (!(Conn->flags & LINK_FLAG_NAME_REQ)) {
        /* We never asked for a name response so do nothing. */
        Assert(!Conn->nameReqOnly);
        return;
    }

    /* No longer waiting for a name response */
    Conn->flags &= ~LINK_FLAG_NAME_REQ;

    nameRsp[0] = HC_STATUS_SUCCESS;
    ConnAddr2BdAddr(Conn, (BD_ADDR *)(nameRsp+1)); 
    OS_MemCopy(nameRsp+7, rxData, 248);

    HC_PostEvent(HCE_REMOTE_NAME_REQ_COMPLETE, nameRsp, 255);

    if (Conn->nameReqOnly) {
        /* Tear down connection */
        HC_CloseLink( Conn );
    }

    return;
}


/* ---------------------------------------------------------------------------
 * HC_ProcessReadFeatureRsp
 * 
 */
static void HC_ProcessReadFeatureRsp(U16 ConnHndl, U8 *rxData)
{
    U8 featureRsp[11];

    featureRsp[0] = HC_STATUS_SUCCESS;
    StoreLE16(featureRsp+1, ConnHndl);
    OS_MemCopy(featureRsp+3, rxData, 8);

    HC_PostEvent(HCE_READ_REMOTE_FEATURES_COMPLETE, featureRsp, 11);

    return;
}


/* ---------------------------------------------------------------------------
 * HC_ProcessHoldReq
 * 
 */
static void HC_ProcessHoldReq(U16 ConnHndl, U16 holdInt)
{
    BtConn *conn;
    U8      accept[5];
    U8      modechng[6];        

    conn = &IHC(connTab)[ConnHndl];

    if ((conn->linkPolicy & BLP_HOLD_MODE) != BLP_HOLD_MODE) {
        StoreLE16(accept, HCP_LMP_NOT_ACCEPTED);
        StoreLE16(accept+2, HCP_LMP_HOLD_REQ);
        accept[4] = HC_STATUS_UNSUPP_FEATUR_PARM_VAL;
        HC_SendLmp(conn->link, accept, 5, 0); 

        return;
    }


    /* Send LMP Packet (see def on p. 226 of BT spec 1.0b) */
    StoreLE16(accept, HCP_LMP_ACCEPTED);
    StoreLE16(accept+2, HCP_LMP_HOLD_REQ);
    HC_SendLmp(conn->link, accept, 4, 0); 

    /* Set mode to Hold */
    conn->state = LINK_HOLD;

    /* Post ModeChange event */
    modechng[0] = HC_STATUS_SUCCESS;
    StoreLE16(modechng+1, ConnHndl);
    modechng[3] = BLM_HOLD_MODE;
    StoreLE16(modechng+4, holdInt);
    
    HC_PostEvent(HCE_MODE_CHNG, modechng, 6);

    /* Set up Timer Structure and call EVM_StartTimer */
    conn->connTimer.func = HoldTimerFire;

    /* HoldTimer.context is used to store the ConnHndl so that when the timer
     * is fired, the correct ConnHndl can be set active again 
     */
    conn->connTimer.context = (void *)ConnHndl;

    /* holdInt is measured in # of baseband slots which are .625 msec */
    EVM_StartTimer(&(conn->connTimer), (TimeT)(holdInt*.625));
}


/* ---------------------------------------------------------------------------
 * HC_ProcessSniffReq
 * 
 */
void HC_ProcessSniffReq(U16 ConnHndl, U8 timerflag, U16 DSniff, U16 TSniff, 
                   U16 Sniffatt, U16 SniffTO)
{

    BtConn *conn;
    U8      accept[5];
    U8      modechng[6];        

    conn = &IHC(connTab)[ConnHndl];

    if ((conn->linkPolicy & BLP_SNIFF_MODE) != BLP_SNIFF_MODE) {
        StoreLE16(accept, HCP_LMP_NOT_ACCEPTED);
        StoreLE16(accept+2, HCP_LMP_SNIFF_REQ);
        accept[4] = HC_STATUS_UNSUPP_FEATUR_PARM_VAL;
        HC_SendLmp(conn->link, accept, 5, 0); 

        return;
    }

    /* Send LMP Packet (see def on p. 226 of BT spec 1.0b) */
    StoreLE16(accept, HCP_LMP_ACCEPTED);
    StoreLE16(accept+2, HCP_LMP_SNIFF_REQ);
    HC_SendLmp(conn->link, accept, 4, 0); 

    conn->state = LINK_SNIFF;
    conn->modeInt = TSniff;

    /* Set up Timer Structure and call EVM_StartTimer */
    conn->connTimer.func = SniffTimerFire;

    /* HoldTimer.context is used to store the ConnHndl */
    conn->connTimer.context = (void *)ConnHndl;
    
    /* sniffInt is measured in # of baseband slots which are .625 msec */
    EVM_StartTimer(&(conn->connTimer), (TimeT)(TSniff*.625));


    /* Post ModeChange event */
    modechng[0] = HC_STATUS_SUCCESS;
    StoreLE16(modechng+1, ConnHndl);
    modechng[3] = BLM_SNIFF_MODE;
    StoreLE16(modechng+4, DSniff);

    HC_PostEvent(HCE_MODE_CHNG, modechng, 6);
}


/* ---------------------------------------------------------------------------
 * HC_ProcessUnsniffReq
 * 
 */
void HC_ProcessUnsniffReq(U16 ConnHndl)
{
    BtConn *conn;
    U8      accept[4];
    U8      modechng[6];        

    conn = &IHC(connTab)[ConnHndl];

    StoreLE16(accept, HCP_LMP_ACCEPTED);
    StoreLE16(accept+2, HCP_LMP_UNSNIFF_REQ);
    HC_SendLmp(conn->link, accept, 4, 0); 

    /* If any ACL Data packets have queued up while in hold, send them */
    HC_SendQueuedAclData(conn);

    conn->state = LINK_ACTIVE;
    conn->waitModeChange = FALSE;

    /* Post ModeChange event */
    modechng[0] = HC_STATUS_SUCCESS;
    StoreLE16(modechng+1, ConnHndl);
    modechng[3] = BLM_ACTIVE_MODE;
    StoreLE16(modechng+4, 0);

    HC_PostEvent(HCE_MODE_CHNG, modechng, 6);

    /* If any ACL Data packets have queued up while in sniff, send them */
    EVM_CancelTimer(&(conn->connTimer));
    HC_SendQueuedAclData(conn);
}


/* ---------------------------------------------------------------------------
 * HC_ProcessPark
 * 
 */
void HC_ProcessPark(U16 ConnHndl, U16 beaconInt)
{
    BtConn *conn;
    U8      modechng[6];        

    conn = &IHC(connTab)[ConnHndl];

    /* Check whether we are Master or Slave */
    if (conn->role == BCR_MASTER) {
        /* Master should never receive this command */
        return;
    }

    conn->state = LINK_PARK;

    /* Post ModeChange event */
    modechng[0] = HC_STATUS_SUCCESS;
    StoreLE16(modechng+1, ConnHndl);
    modechng[3] = BLM_PARK_MODE;
    StoreLE16(modechng+4, beaconInt);

    HC_PostEvent(HCE_MODE_CHNG, modechng, 6);

}

/* ---------------------------------------------------------------------------
 * HC_ProcessParkReq
 * 
 */
void HC_ProcessParkReq(U16 ConnHndl, U16 beaconInt)
{
    BtConn *conn;
    U8      park[5];
    U8      modechng[6];        

    conn = &IHC(connTab)[ConnHndl];

    if ((conn->linkPolicy & BLP_PARK_MODE) != BLP_PARK_MODE) {
        StoreLE16(park, HCP_LMP_NOT_ACCEPTED);
        StoreLE16(park+2, HCP_LMP_PARK_REQ);
        park[4] = HC_STATUS_UNSUPP_FEATUR_PARM_VAL;
        HC_SendLmp(conn->link, park, 5, 0); 

        return;
    }

    if (conn->role == BCR_MASTER) {
        StoreLE16(park, HCP_LMP_PARK);
        StoreLE16(park+2, beaconInt);
        /* Due to the complicated nature of the Park command
        * and our limited LM capabilities, the parameters 
        * for Park are not being calculated.
        */
        HC_SendLmp(conn->link, park, 4, 0); 

        conn->state = LINK_PARK;

        /* Post ModeChange event */
        modechng[0] = HC_STATUS_SUCCESS;
        StoreLE16(modechng+1, ConnHndl);
        modechng[3] = BLM_PARK_MODE;
        StoreLE16(modechng+4, beaconInt);
    
        HC_PostEvent(HCE_MODE_CHNG, modechng, 6);
    }
    else {
        /* If any ACL Data packets have queued up, send them */
        HC_SendQueuedAclData(conn);
        
        conn->waitModeChange = FALSE;
    
        StoreLE16(park, HCP_LMP_ACCEPTED);
        StoreLE16(park+2, HCP_LMP_PARK_REQ);
        HC_SendLmp(conn->link, park, 4, 0);     
    }
}

/* ---------------------------------------------------------------------------
 * HC_ProcessUnparkReq
 * 
 */
void HC_ProcessUnparkReq(U16 ConnHndl)
{
    BtConn *conn;
    U8      accept[4];
    U8      modechng[6];        

    conn = &IHC(connTab)[ConnHndl];

    StoreLE16(accept, HCP_LMP_ACCEPTED);
    StoreLE16(accept+2, HCP_LMP_UNPARK_REQ);
    HC_SendLmp(conn->link, accept, 4, 0); 

    /* If any ACL Data packets have queued up while in hold, send them */
    HC_SendQueuedAclData(conn);

    conn->state = LINK_ACTIVE;
    conn->waitModeChange = FALSE;

    /* Post ModeChange event */
    modechng[0] = HC_STATUS_SUCCESS;
    StoreLE16(modechng+1, ConnHndl);
    modechng[3] = BLM_ACTIVE_MODE;
    StoreLE16(modechng+4, 0);

    HC_PostEvent(HCE_MODE_CHNG, modechng, 6);

    /* If any ACL Data packets have queued up while in park, send them */
    HC_SendQueuedAclData(conn);
}

/* ---------------------------------------------------------------------------
 * HC_ProcessEncStartReq
 * 
 */
void HC_ProcessEncStartReq(U16 ConnHndl)
{
    BtConn *conn;
    U8      accept[4];
    U8      encchng[4];        

    conn = &IHC(connTab)[ConnHndl];

    StoreLE16(accept, HCP_LMP_ACCEPTED);
    StoreLE16(accept+2, HCP_LMP_ENC_START_REQ);
    HC_SendLmp(conn->link, accept, 4, 0); 

    conn->secFlags |= LINK_SEC_ENCRYPTED;

    /* The remote device has requested encryption, which we automatically
     * allow (not per spec). Indicate this ONLY if we are connected.
     */
    if (conn->state >= LINK_ACTIVE) {
        encchng[0] = HC_STATUS_SUCCESS;
        StoreLE16(encchng+1, ConnHndl);
        encchng[3] = TRUE;

        HC_PostEvent(HCE_ENCRYPT_CHNG, encchng, 4);
    }
}

/* ---------------------------------------------------------------------------
 * HC_ProcessEncStopReq
 * 
 */
void HC_ProcessEncStopReq(U16 ConnHndl)
{
    BtConn *conn;
    U8      accept[4];
    U8      encchng[4];        

    conn = &IHC(connTab)[ConnHndl];

    StoreLE16(accept, HCP_LMP_ACCEPTED);
    StoreLE16(accept+2, HCP_LMP_ENC_STOP_REQ);
    HC_SendLmp(conn->link, accept, 4, 0); 

    conn->secFlags &= ~LINK_SEC_ENCRYPTED;

    encchng[0] = HC_STATUS_SUCCESS;
    StoreLE16(encchng+1, ConnHndl);
    encchng[3] = FALSE;

    HC_PostEvent(HCE_ENCRYPT_CHNG, encchng, 4);
}


/* ---------------------------------------------------------------------------
 * HC_ReadLinkPolicy
 * 
 */
void HC_ReadLinkPolicy(U16 connHndl)
{
    BtConn *conn;
    U8      complete[8];

    conn = &IHC(connTab)[connHndl];

    /* Always return success */
    complete[0] = HC_CmdQueueSpace();              /* Number of commands HCI can send */
    StoreLE16(complete+1, HCC_READ_LINK_POLICY);  /* Command we're acknowledging */
    complete[3] = HC_STATUS_SUCCESS;
    StoreLE16(complete+4, connHndl);
    StoreLE16(complete+6, conn->linkPolicy);
    HC_PostEvent(HCE_COMMAND_COMPLETE, complete, 8);
}

/* ---------------------------------------------------------------------------
 * HC_WriteLinkPolicy
 * 
 */
void HC_WriteLinkPolicy(U16 connHndl, U16 linkPolicy)
{
    BtConn *conn;
    U8      complete[6];

    conn = &IHC(connTab)[connHndl];

    conn->linkPolicy = linkPolicy;

    /* Always return success */
    complete[0] = HC_CmdQueueSpace();              /* Number of commands HCI can send */
    StoreLE16(complete+1, HCC_WRITE_LINK_POLICY);  /* Command we're acknowledging */
    complete[3] = HC_STATUS_SUCCESS;
    StoreLE16(complete+4, connHndl);
    HC_PostEvent(HCE_COMMAND_COMPLETE, complete, 6);
}


/* ---------------------------------------------------------------------------
 * HC_SwitchRole
 * 
 */
HcStatus HC_SwitchRole(const BD_ADDR *BdAddr, U8 Role)
{
    BtConn *conn;
    U8       role[3];


    /* Find an matching connection entry */
    conn = GetConnFromBdAddr(BdAddr);
    
    if (!conn || conn->link == INVALID_SOCKET)
        return HC_STATUS_NO_CONNECTION;

    if ((conn->linkPolicy & BLP_MASTER_SLAVE_SWITCH) != BLP_MASTER_SLAVE_SWITCH)
        return HC_STATUS_ROLE_CHANGE_DISALLOWED;

    conn->waitModeChange = TRUE;
        
    StoreLE16(role, HCP_LMP_SWITCH_REQ);

    /* store what role we are switching to */
    conn->modeInt = Role;
    if (Role == BCR_MASTER) 
        role[2] = BCR_SLAVE;
    else 
        role[2] = BCR_MASTER;

    HC_SendLmp(conn->link, role, 3, 0); 

    return HC_STATUS_SUCCESS;
}

/* ---------------------------------------------------------------------------
 * HC_ProcessSwitchReq
 * 
 */
void HC_ProcessSwitchReq(U16 ConnHndl, U8 Role)
{
    BtConn *conn;
    U8      accept[5];
    U8      rolechng[8];

    conn = &IHC(connTab)[ConnHndl];

    if ((conn->state == LINK_CONNECTING) && (conn->allowRoleChange == FALSE)) {
        StoreLE16(accept, HCP_LMP_NOT_ACCEPTED);
        StoreLE16(accept+2, HCP_LMP_SWITCH_REQ);
        accept[4] = HC_STATUS_ROLE_CHANGE_DISALLOWED;
        HC_SendLmp(conn->link, accept, 5, 0); 
        return;
    }
    else {
        conn->roleChanged = TRUE;
    }
    if ((conn->state == LINK_ACTIVE) && 
        ((conn->linkPolicy & BLP_MASTER_SLAVE_SWITCH) != BLP_MASTER_SLAVE_SWITCH)) {
        StoreLE16(accept, HCP_LMP_NOT_ACCEPTED);
        StoreLE16(accept+2, HCP_LMP_SWITCH_REQ);
        accept[4] = HC_STATUS_UNSUPP_FEATUR_PARM_VAL;
        HC_SendLmp(conn->link, accept, 5, 0); 


        /* I think I need to send an event for the connection failure */
        return;
    }

    /* switch roles */
    conn->role = Role;
    conn->modeInt = Role;

    /* Send LMP Packet (see def on p. 226 of BT spec 1.0b) */
    StoreLE16(accept, HCP_LMP_ACCEPTED);
    StoreLE16(accept+2, HCP_LMP_SWITCH_REQ);
    HC_SendLmp(conn->link, accept, 4, 0); 

    if (conn->state == LINK_ACTIVE) {
        /* Post RoleChange event */
        rolechng[0] = HC_STATUS_SUCCESS;
        ConnAddr2BdAddr(conn, (BD_ADDR *)(rolechng+1));
        rolechng[7] = conn->role;
           HC_PostEvent(HCE_ROLE_CHANGE, rolechng, 8);
    }
}

/* ---------------------------------------------------------------------------
 * HC_GenerateKinit
 * 
 */
static void HC_GenerateKinit(const U8 *InRand, const U8 *Pin, U8 PinLen, 
                             const BD_ADDR *BdAddr, U8 *kInit /* Out */)
{
    U8  i;

    Assert(PinLen > 0 && PinLen < 17);

    kInit[15] = 0;
    OS_MemCopy(kInit, InRand, 15);

    for (i = 0; i < PinLen; i++)
        kInit[i] += Pin[i];

    for (i = 0; i < sizeof(BD_ADDR); i++)
        kInit[i*2] += BdAddr->addr[i];
}

/* ---------------------------------------------------------------------------
 * HC_GenerateSRes
 * 
 */
static void HC_GenerateSRes(const U8 *AuRand, const U8 *LinkKey, BD_ADDR *Addr,
                            U8 *SRes /* Out */)
{
    U32     sres;

    /* How's this for a mumbo-jumbo SRES algorithm :-) */
    sres = BDA_IPADDR(Addr) + BDA_IPPORT(Addr) + *(U32 *)(AuRand) + *(U32 *)(AuRand+4);
    sres += *(U32 *)(AuRand+8) + *(U16 *)(AuRand+12) + AuRand[14];
    sres += *(U32 *)(LinkKey) + *(U32 *)(LinkKey+4);
    sres += *(U32 *)(LinkKey+8) + *(U32 *)(LinkKey+12);

    while (sres > 0x00FFFFFF)
        sres = (sres & 0x00FFFFFF) + ((sres & 0xFF000000) >> 24);

    /* Finally, the result! */
    OS_MemCopy(SRes, (U8 *)&sres, 3);
}

/* ---------------------------------------------------------------------------
 * HC_GenerateRandNr
 * 
 */
static void HC_GenerateRandNr(U8 *Rand)
{
    U8  offset;

    /* Build 15-byte random number from 8 2-byte random values */
    for (offset = 0; offset < 14; offset += 2)
        StoreLE16((Rand+offset), OS_Rand());

    *(Rand+14) = (U8)OS_Rand();
}

/* ---------------------------------------------------------------------------
 * HC_GenerateXorKey
 * 
 */
static void HC_GenerateXorKey(U8 *KeyInOut, const U8 *KeyIn)
{
    I8  i;

    for (i = 0; i < 15; i++)
        KeyInOut[i] = KeyInOut[i] ^ KeyIn[i];
}

#if XA_DEBUG_PRINT == XA_ENABLED

static char *pHcp(U16 hcp)
{
    switch(hcp) {
    case HCP_LMP_EVENT           : return "HCP_LMP_EVENT";
    case HCP_LMP_CONN_REQ        : return "HCP_LMP_CONN_REQ";
    case HCP_LMP_ACCEPTED        : return "HCP_LMP_ACCEPTED";
    case HCP_LMP_SWITCH_REQ      : return "HCP_LMP_SWITCH_REQ";
    case HCP_LMP_SETUP_COMPLETE  : return "HCP_LMP_SETUP_COMPLETE";
    case HCP_LMP_NOT_ACCEPTED    : return "HCP_LMP_NOT_ACCEPTED";
    case HCP_LMP_DETACH          : return "HCP_LMP_DETACH";
    case HCP_LMP_IN_RAND         : return "HCP_LMP_IN_RAND";
    case HCP_LMP_COMB_KEY        : return "HCP_LMP_COMB_KEY";
    case HCP_LMP_AU_RAND         : return "HCP_LMP_AU_RAND";
    case HCP_LMP_SRES            : return "HCP_LMP_SRES";
    case HCP_LMP_SCO_LINK_REQ    : return "HCP_LMP_SCO_LINK_REQ";
    case HCP_LMP_REMOVE_SCO_REQ  : return "HCP_LMP_REMOVE_SCO_REQ";
    case HCP_LMP_NAME_REQ        : return "HCP_LMP_NAME_REQ";
    case HCP_LMP_NAME_RSP        : return "HCP_LMP_NAME_RSP";
    case HCP_LMP_HOLD            : return "HCP_LMP_HOLD";
    case HCP_LMP_HOLD_REQ        : return "HCP_LMP_HOLD_REQ";
    case HCP_LMP_SNIFF           : return "HCP_LMP_SNIFF";
    case HCP_LMP_SNIFF_REQ       : return "HCP_LMP_SNIFF_REQ";
    case HCP_LMP_UNSNIFF_REQ     : return "HCP_LMP_UNSNIFF_REQ";
    case HCP_LMP_PARK            : return "HCP_LMP_PARK";
    case HCP_LMP_PARK_REQ        : return "HCP_LMP_PARK_REQ";
    case HCP_LMP_UNPARK_REQ      : return "HCP_LMP_UNPARK_REQ";
    case HCP_LMP_ENC_MODE_REQ    : return "HCP_LMP_ENC_MODE_REQ";
    case HCP_LMP_ENC_KEY_SIZE_REQ: return "HCP_LMP_ENC_KEY_SIZE_REQ";
    case HCP_LMP_ENC_START_REQ   : return "HCP_LMP_ENC_START_REQ";
    case HCP_LMP_ENC_STOP_REQ    : return "HCP_LMP_ENC_STOP_REQ";
    case HCP_INQUIRY_REQ         : return "HCP_INQUIRY_REQ";
    case HCP_INQUIRY_RSP         : return "HCP_INQUIRY_RSP";
    case HCP_IDENTITY            : return "HCP_IDENTITY";
    case HCP_HCI_COMMAND         : return "HCP_HCI_COMMAND";
    case HCP_HCC_PIN_POS         : return "HCP_HCC_PIN_POS";
    case HCP_HCC_PIN_NEG         : return "HCP_HCC_PIN_NEG";
    case HCP_HCC_KEY_POS         : return "HCP_HCC_KEY_POS";
    case HCP_HCC_KEY_NEG         : return "HCP_HCC_KEY_NEG";
    default: return "UNKNOWN";
    }
}

#endif /* XA_DEBUG_PRINT */
