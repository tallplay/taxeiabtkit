/***************************************************************************
 *
 * File:
 *     $Workfile:hostctlr.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:121$
 *
 * Description:
 *     This file contains code for INet host controller.
 *
 * Created:
 *     Aug 13, 1999
 *
 * Copyright 1999-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "osapi.h"
#include "hostctlr.h"
#include "sys/evmxp.h"
#include "me.h"
#include "sys/debug.h"
#if NUM_SCO_CONNS > 0
#include "mesco.h"
#endif

#ifdef DEMO
#undef Report
#define Report(S)
#endif

/* Internal function prototypes */
static void HC_InquiryPacket(SOCKET S, U16 opcode);
static void HC_ConnectStatus(SOCKET S, BOOL Success);
static void HC_IdentityPacket(void);
static void HandleEventOverrun(U8 Event, U8 *EventData, U8 EventLen);
#if NUM_SCO_CONNS > 0
static HcStatus HC_ScoRejectConn(BtConn *Acl, U8 Reason);
#endif

#if XA_DEBUG_PRINT == XA_ENABLED
static char *WSA_LastError(void);
static char *WSA_Error(int error);
#endif


SOCKET      Wakeup = INVALID_SOCKET;
static U16  lastInquiry = 0;


/* ---------------------------------------------------------------------------
 * HC_InitConn
 *
 * 
 */
void HC_InitConn(BtConn *conn) 
{
    conn->roleChanged = FALSE;
    conn->allowRoleChange = FALSE;
}


/* ---------------------------------------------------------------------------
 * HC_Inquire
 *
 * Duration in 100 ms units.
 */
HcStatus HC_Inquire(U32 type, U8 Duration, U8 NumResponses)
{
   HcStatus status;
   /*
    * Setup inquiry process. Every 100 mseconds we ping the inquiry port
    * looking for a device. The duration parameter indicates number of
    * seconds to inquire for. A Value of 1 == once, 65535 == until stopped.
    */
   status = HC_OpenInquirySocket();

   if (status == HC_STATUS_SUCCESS) {

      IHC(inqDuration) = Duration * 10;
      IHC(inqStartTime) = OS_GetSystemTime();
      IHC(inqMaxTime) = Duration * 1280;
      IHC(inqResponseMax) = NumResponses;
      IHC(inqResponseCount) = 0;
      IHC(inqAccess) = type;
      IHC(inqKey) = OS_Rand();
      IHC(state) |= HC_MODE_INQUIRY;
   }
   return status;
}

/* ---------------------------------------------------------------------------
 * HC_OpenInquiryScanSocket
 *
 * There are two inquiry sockets. One for Inquiry and one for Inquiry Scan. The
 * Inquiry Scan socket is well known and is only open when the device is scanning.
 * 
 */
HcStatus HC_OpenInquiryScanSocket(void)
{
   int         rcode;
   SOCKADDR_IN myAddr = {AF_INET, htons(INQUIRY_PORT), INADDR_ANY, 0};
   BOOL        on = TRUE;

   if (IHC(inquiryScanSkt) == INVALID_SOCKET) {
      OS_MemSet(myAddr.sin_zero, 0, 8);

      /* Open UDP Receive ONLY Port */
      IHC(inquiryScanSkt) = socket(AF_INET, SOCK_DGRAM, 0);

      if (IHC(inquiryScanSkt) == INVALID_SOCKET)
         return HC_STATUS_HOST_REJ_NO_RESOURCES;

      rcode = setsockopt(IHC(inquiryScanSkt), SOL_SOCKET, SO_REUSEADDR, (char *)&on, sizeof(BOOL));
      if (rcode != SOCKET_ERROR) {

         rcode = bind(IHC(inquiryScanSkt), (struct sockaddr *)&myAddr, sizeof(SOCKADDR_IN));
         if (rcode != SOCKET_ERROR) {

            rcode = WSAAsyncSelect(IHC(inquiryScanSkt), IHC(driverWnd), WM_SELECT, FD_READ);
            if (rcode != SOCKET_ERROR) {

               return HC_STATUS_SUCCESS;
            }
         }
      }
   
      /* setsockopt, bind, select or setsockopt calls failed */
      closesocket( IHC(inquiryScanSkt) );
      IHC(inquiryScanSkt) = INVALID_SOCKET;
      return HC_STATUS_UNSPECIFIED_ERROR;
   }

   return HC_STATUS_SUCCESS;
}

/* ---------------------------------------------------------------------------
 * HC_OpenInquirySocket
 *
 * The Inquiry socket is dynamic and is only open when the device is inquiring.
 * 
 */
HcStatus HC_OpenInquirySocket(void)
{
   int         rcode;
   SOCKADDR_IN myAddr = {AF_INET, 0, INADDR_ANY, 0};
   BOOL        on = TRUE;

   if (IHC(inquirySkt) == INVALID_SOCKET) {
      OS_MemSet(myAddr.sin_zero, 0, 8);

      /* Open UDP Port */
      IHC(inquirySkt) = socket(AF_INET, SOCK_DGRAM, 0);

      if (IHC(inquirySkt) == INVALID_SOCKET)
         return HC_STATUS_HOST_REJ_NO_RESOURCES;

      rcode = bind(IHC(inquirySkt), (struct sockaddr *)&myAddr, sizeof(SOCKADDR_IN));
      if (rcode != SOCKET_ERROR) {

         rcode = WSAAsyncSelect(IHC(inquirySkt), IHC(driverWnd), WM_SELECT, FD_READ);
         if (rcode != SOCKET_ERROR) {

            rcode = setsockopt(IHC(inquirySkt), SOL_SOCKET, SO_BROADCAST, (char *)&on, sizeof(BOOL));
            if (rcode != SOCKET_ERROR) {

               return HC_STATUS_SUCCESS;
            }
         }
      }
   
      /* setsockopt, bind, select or setsockopt calls failed */
      closesocket( IHC(inquirySkt) );
      IHC(inquirySkt) = INVALID_SOCKET;
      return HC_STATUS_UNSPECIFIED_ERROR;
   }

   return HC_STATUS_SUCCESS;
}


/* ---------------------------------------------------------------------------
 * HC_CreateAclConn
 * 
 */
HcStatus HC_CreateAclConn(BD_ADDR Target, U16 PacketTypes, BOOL NameReqOnly, BOOL allowRoleChange)
{
    int      rcode, i;
    BtConn  *conn = 0;

    /* Establish connection to specified device */
    for (i = 0; i < NUM_BT_DEVICES; i++) {
        if (IHC(connTab)[i].link == INVALID_SOCKET) {
            conn = &IHC(connTab)[i];
            break;
        }
    }
    if (conn == 0)
        return HC_STATUS_HOST_REJ_NO_RESOURCES;

    /* Clear all memory */
    OS_MemSet((U8 *)conn, 0, sizeof(BtConn));

    /* Initialize the authorization states */
    conn->authCur = AuthReady;
    HC_InitConn(conn);
    conn->allowRoleChange = allowRoleChange;

    InitializeListHead(&(conn->ACLDataQueue) );

   conn->link = socket(AF_INET, SOCK_STREAM, 0);
   if (conn->link == INVALID_SOCKET)
      return HC_STATUS_HOST_REJ_NO_RESOURCES;

   rcode = WSAAsyncSelect( conn->link, IHC(driverWnd), WM_SELECT, FD_READ|FD_WRITE|FD_CONNECT|FD_CLOSE);
   if (rcode == SOCKET_ERROR) {
      HC_CloseLink( conn );
      return HC_STATUS_UNSPECIFIED_ERROR;
   }

   OS_MemSet(conn->addr.sin_zero, 0, 8);
   conn->addr.sin_family = AF_INET;
   BdAddr2SinAddr(&Target, conn->addr);

   conn->port = BDA_IPPORT(&Target);

   rcode = connect(conn->link, (struct sockaddr *)&conn->addr, sizeof(SOCKADDR_IN));
   if (rcode == SOCKET_ERROR) {
      if (WSAGetLastError() != WSAEWOULDBLOCK) {
         Report(("HC: CreateAclConn() Connect failed %s\n.", WSA_LastError()));
         HC_CloseLink( conn );
         return HC_STATUS_UNSPECIFIED_ERROR;
      }
   }

   conn->role = BCR_MASTER;
   conn->state = LINK_CONNECTING;
   conn->nameReqOnly = NameReqOnly;

   return HC_STATUS_SUCCESS;
}

#if NUM_SCO_CONNS > 0
/* ---------------------------------------------------------------------------
 * HC_AddScoConn
 * 
 */
HcStatus HC_AddScoConn(U16 AclHndl, U16 PktTypes)
{
    ScoConn *sco = 0;
    BtConn  *acl;
    I8       i;
    U8       scoReq[5];

    if (AclHndl > NUM_BT_DEVICES)
        return HC_STATUS_NO_CONNECTION;

    acl = &IHC(connTab)[AclHndl];
    if (acl->link == INVALID_SOCKET)
        return HC_STATUS_NO_CONNECTION;

    /* Make sure there are no SCO connections coming up on 
     * this same connection */
    for (i = 0; i < NUM_SCO_CONNS; i++) {
        if (IHC(scoTab)[i].parent == acl &&
            IHC(scoTab)[i].state == SCO_CONNECTING) {
            return HC_STATUS_HOST_REJ_NO_RESOURCES;
        }
    }

    /* Find an available SCO handle */
    for (i = 0; i < NUM_SCO_CONNS; i++) {
        if (IHC(scoTab)[i].parent == 0) {
            break;
        }
    }
    if (i == NUM_SCO_CONNS) {
        /* No more SCO Links available */
        return HC_STATUS_HOST_REJ_NO_RESOURCES;
    }
    sco = &IHC(scoTab)[i];

    /* We found a SCO handle, initiate the SCO connection */
    OS_MemSet((U8 *)sco, 0, sizeof(ScoConn));
    sco->parent = acl;
    sco->state = SCO_CONNECTING;
    sco->handle = ((acl->role == BCR_MASTER) ? (U8)(i+1) : 0); /* Only Master can assign */
    sco->localRequest = TRUE;

    StoreLE16(scoReq, HCP_LMP_SCO_LINK_REQ);
    scoReq[2] = sco->handle;
    /* These values should come from checking the bits in the 
     * IHC(VoiceSetting) value.
     */
    scoReq[3] = (U8) PktTypes;  /* Packet types */
    scoReq[4] = 2;              /* Air Mode: 2 = CVSD  */

    HC_SendLmp(acl->link, scoReq, 5, 0);

    return HC_STATUS_SUCCESS;
}

/* ---------------------------------------------------------------------------
 * HC_SetupSyncConn
 * 
 */
HcStatus HC_SetupSyncConn(U16 ConHndl, U16 pktTypes)
{
    ScoConn *sco = 0;
    BtConn  *acl;
    I8       i;
    U8       scoReq[12];

    if (ConHndl > NUM_BT_DEVICES) {
        if (ConHndl > NUM_BT_HANDLES + NUM_SCO_CONNS) {
            return HC_STATUS_NO_CONNECTION;
        }

        /* Modify an existing eSCO connnection. */

        sco = &IHC(scoTab)[ConHndl-NUM_BT_HANDLES];
        if (sco->state != SCO_ACTIVE) {
            return HC_STATUS_NO_CONNECTION;
        }
        sco->state = SCO_CHANGING;
        acl = sco->parent;
    }
    else {
        /* Set up an new eSCO connection */

        acl = &IHC(connTab)[ConHndl];

        if (acl->link == INVALID_SOCKET)
            return HC_STATUS_NO_CONNECTION;

        /* Make sure there are no SCO connections coming up on 
         * this same connection */
        for (i = 0; i < NUM_SCO_CONNS; i++) {
            if (IHC(scoTab)[i].parent == acl &&
                IHC(scoTab)[i].state == SCO_CONNECTING) {
                return HC_STATUS_HOST_REJ_NO_RESOURCES;
            }
        }

        /* Find an available SCO handle */
        for (i = 0; i < NUM_SCO_CONNS; i++) {
            if (IHC(scoTab)[i].parent == 0) {
                break;
            }
        }
        if (i == NUM_SCO_CONNS) {
            /* No more SCO Links available */
            return HC_STATUS_HOST_REJ_NO_RESOURCES;
        }
        sco = &IHC(scoTab)[i];

        /* We found a SCO handle, initiate the SCO connection */
        OS_MemSet((U8 *)sco, 0, sizeof(ScoConn));
        sco->parent = acl;
        sco->state = SCO_CONNECTING;
        sco->handle = ((acl->role == BCR_MASTER) ? (U8)(i+1) : 0); /* Only Master can assign */
        sco->linkType = 2; /* eSCO link */
        sco->localRequest = TRUE;
    }

    StoreLE16(scoReq, HCP_LMP_ESCO_LINK_REQ);
    scoReq[2] = sco->handle;
    /* We should negotiate the following, but won't for now. */
    scoReq[3] = 0x07;           /* EV3 M->S */
    scoReq[4] = 0x07;           /* EV3 S->M */
    StoreLE16(scoReq+5, 30);    /* Packet size M->S */
    StoreLE16(scoReq+7, 30);    /* Packet size S->M */
    scoReq[10] = 2;             /* Air Mode: 2 = CVSD  */
    scoReq[11] = 0;             /* Negotiation state */

    HC_SendLmp(acl->link, scoReq, 12, 0);

    return HC_STATUS_SUCCESS;
}
#endif

/* ---------------------------------------------------------------------------
 * HC_StartInqScan
 * 
 */
HcStatus HC_StartInqScan(void)
{
   HcStatus status;

   if (IHC(state) & HC_MODE_INQ_SCAN)
      return HC_STATUS_SUCCESS;

   status = HC_OpenInquiryScanSocket();
   if (status == HC_STATUS_SUCCESS) {

      Report(("HC: Inquiry Scanning socket %x\n", IHC(inquiryScanSkt)));
      IHC(state) |= HC_MODE_INQ_SCAN;
   }
   return status;
}

/* ---------------------------------------------------------------------------
 * HC_OpenPagingSocket()
 * 
 */
HcStatus HC_OpenPagingSocket(void)
{
   int         rcode;
   SOCKADDR_IN myAddr = {AF_INET, htons(PAGE_PORT), INADDR_ANY, 0};

   if (IHC(pageSkt) != INVALID_SOCKET)
      return HC_STATUS_SUCCESS;

   OS_MemSet(myAddr.sin_zero, 0, 8);

   /* Open TCP Socket */
   IHC(pageSkt) = socket(AF_INET, SOCK_STREAM, 0);

   if (IHC(pageSkt) != INVALID_SOCKET) {

      while (myAddr.sin_port < htons(PAGE_PORT + 0x00FF)) {
         /* Bind to dynamic-fixed Port */
         rcode = bind(IHC(pageSkt), (struct sockaddr *)&myAddr, sizeof(SOCKADDR_IN));
         if (rcode != SOCKET_ERROR) {
            BDA_IPPORT(&IHC(myAddr)) = myAddr.sin_port;
            BDA_IPADDR(&IHC(myAddr)) = 0;     /* This is figured out soon */
//            Report(("HC: Opened Page Scanning socket %x [%04x].\n", IHC(pageSkt), ntohs(myAddr.sin_port)));
            return HC_STATUS_SUCCESS;
         }
         /* increment by 1 (network order) and try for next port */
         myAddr.sin_port += 0x0100;
      }

      /* open or bind calls failed */
      closesocket( IHC(pageSkt) );
      IHC(pageSkt) = INVALID_SOCKET;
   }

   return HC_STATUS_UNSPECIFIED_ERROR;
}



/* ---------------------------------------------------------------------------
 * HC_StartPageScan
 * 
 */
HcStatus HC_StartPageScan(void)
{
   int         rcode;

   if (IHC(state) & HC_MODE_PAGE_SCAN)
      return HC_STATUS_SUCCESS;

   if (IHC(pageSkt) == INVALID_SOCKET)
      return HC_STATUS_UNSPECIFIED_ERROR;


   rcode = listen(IHC(pageSkt), 1);
   if (rcode != SOCKET_ERROR) {

      rcode = WSAAsyncSelect(IHC(pageSkt), IHC(driverWnd), WM_SELECT, FD_ACCEPT|FD_CLOSE);
      if (rcode != SOCKET_ERROR) {

         IHC(state) |= HC_MODE_PAGE_SCAN; 
         Report(("HC: Page Scanning socket %x\n", IHC(pageSkt)));
         return HC_STATUS_SUCCESS;
      }
   }
   return HC_STATUS_UNSPECIFIED_ERROR;
}


/* ---------------------------------------------------------------------------
 * HC_StopInqScan
 * 
 */
void HC_StopInqScan(void)
{
   if ((IHC(state) & HC_MODE_INQ_SCAN) == 0)
      return;

   IHC(state) &= ~HC_MODE_INQ_SCAN;

   Assert(IHC(inquiryScanSkt) != INVALID_SOCKET);

   closesocket(IHC(inquiryScanSkt));
   IHC(inquiryScanSkt) = INVALID_SOCKET;
   return;
}


/* ---------------------------------------------------------------------------
 * HC_StopPageScan
 * 
 */
void HC_StopPageScan(void)
{
   if ((IHC(state) & HC_MODE_PAGE_SCAN) == 0)
      return;

   Assert(IHC(pageSkt) != INVALID_SOCKET);

//   closesocket(IHC(pageSkt));

//   IHC(pageSkt) = INVALID_SOCKET;

   IHC(state) &= ~HC_MODE_PAGE_SCAN;

   return;
}

/* ---------------------------------------------------------------------------
 * HC_WriteScanEnable
 * 
 */
void HC_WriteScanEnable(U8 Mode)
{
   U8       complete[4];
   HcStatus status;

   /* First we stop any disabled scans */
   if ((Mode & 0x01) == 0)
      HC_StopInqScan();

   if ((Mode & 0x02) == 0)
      HC_StopPageScan();

   /* Now we enable the scans, one at a time */
   if (Mode & 0x01)
      status = HC_StartInqScan();
   else status = HC_STATUS_SUCCESS;

   if ((Mode & 0x02) && status == HC_STATUS_SUCCESS) {
      
      status = HC_StartPageScan();
      if (status != HC_STATUS_SUCCESS)
         HC_StopInqScan();
   }

   complete[0] = HC_CmdQueueSpace();  /* Number of commands HCI can send */
   StoreLE16(complete+1, HCC_WRITE_SCAN_ENABLE);    /* Command were acknowledging */
   complete[3] = status;
   
   HC_PostEvent(HCE_COMMAND_COMPLETE, complete, 4);
}

/* ---------------------------------------------------------------------------
 * HC_InquiryCancel
 * 
 */
void HC_InquiryCancel(void)
{
   U8    complete[4];   /* Command Complete event buffer */

   complete[0] = HC_CmdQueueSpace();  /* Number of commands HCI can send */
   StoreLE16(complete+1, HCC_INQUIRY_CANCEL);    /* Command were acknowledging */

   if (IHC(state) & HC_MODE_INQUIRY) {
      Assert(IHC(inquirySkt) != INVALID_SOCKET);

      if ((IHC(state) & HC_MODE_INQ_SCAN) == 0) {

         closesocket(IHC(inquiryScanSkt));
         IHC(inquiryScanSkt) = INVALID_SOCKET;
      }

      IHC(state) &= ~HC_MODE_INQUIRY;
      IHC(inqDuration) = 0;

      complete[3] = HC_STATUS_SUCCESS;
   } else {
      /* No inquiry in progress */
      complete[3] = HC_STATUS_UNSPECIFIED_ERROR;
   }

   /* Post the command complete to the HCI */
   HC_PostEvent(HCE_COMMAND_COMPLETE, complete, 4);
   return;
}


/* ---------------------------------------------------------------------------
 * HC_SendInquiry
 *
 *  Called periodically to send inquiry request from Driver thread.
 */
void HC_SendInquiry(U32 accessCode, U16 keyValue)
{
   U8    request[11] = {0};

   if ((IHC(state) & HC_MODE_INQUIRY) == 0)
      return;

   StoreLE16(request, HCP_INQUIRY_REQ);    /* Inquiry_Request */
   StoreLE16(request+2, keyValue);
   StoreLE32(request+4, accessCode);

   HC_SendTo(request, 11, htonl(INADDR_BROADCAST), htons(INQUIRY_PORT));
}

/* ---------------------------------------------------------------------------
 * HC_WriteInqMode
 *
 *  Writes the current mode for inquiries. Not too many modes defined as
 *  of yet.
 */
void HC_WriteInqMode(U8 inqMode)
{
    HcStatus status = HC_STATUS_INVAL_HCI_PARM_VAL;
    U8       complete[4];

    switch (inqMode) {
    case 0:
        /* Clear RSSI-inquiry mode */
        IHC(flags) &= ~IHCF_INQUIRY_RSSI;
        status = HC_STATUS_SUCCESS;
        break;

    case 1:
        /* Set RSSI-inquiry mode */
        IHC(flags) |= IHCF_INQUIRY_RSSI;
        status = HC_STATUS_SUCCESS;
        break;

    default:
        /* Fail the cmd */
        break;
    }    

   complete[0] = HC_CmdQueueSpace();  
   StoreLE16(complete+1, HCC_WRITE_INQ_MODE);    /* Command to acknowledge */
   complete[3] = status;
   
   HC_PostEvent(HCE_COMMAND_COMPLETE, complete, 4);
}

/* ---------------------------------------------------------------------------
 * HC_ReadInqMode
 *
 *  Reads the current mode for inquiries back to the host.
 */
void HC_ReadInqMode()
{
    U8 mode = 0;
    U8 complete[5];

    /* Determine mode value */
    if (IHC(flags) & IHCF_INQUIRY_RSSI) mode = 1;

    /* Create and deliver response event */
    complete[0] = HC_CmdQueueSpace();
    StoreLE16(complete+1, HCC_READ_INQ_MODE);
    complete[3] = HC_STATUS_SUCCESS;
    complete[4] = mode;
    HC_PostEvent(HCE_COMMAND_COMPLETE, complete, 5);
}

/* ---------------------------------------------------------------------------
 * HC_ProcessBroadcast
 * 
 * This function is called when a packet arrives on the UDP (broadcast) port.
 */
static void HC_ProcessBroadcast(SOCKET S)
{
   U8          packet[4];
   U16         opcode;
   int         rcode;

   /* Peek at HCP Type to dispatch data to proper receive function. */
   rcode = recvfrom(S, packet, 4, MSG_PEEK, 0, 0);
   if (rcode == SOCKET_ERROR) {
      //Report(("recvfrom() Failed, reason %s\n",WSA_LastError()));
      if (WSAGetLastError() == WSAEMSGSIZE) {
         rcode = 4;
      } else {
         Assert(WSAGetLastError() == WSAEWOULDBLOCK);
         return;
      }
   }

   if (rcode != 4) {
       /* Not our packet. Flush it from the input stream. */
       recvfrom(S, packet, 4, 0, 0, 0);
       return;
   }
   opcode = LEtoHost16(packet);

   switch (opcode) {
      case HCP_INQUIRY_REQ:
      case HCP_INQUIRY_RSP:
         HC_InquiryPacket(S, opcode);
         break;

      case HCP_IDENTITY:
         HC_IdentityPacket();
         break;

      default:
          /* Unrecognized packet. Flush it from the input stream. */
          recvfrom(S, packet, 4, 0, 0, 0);
          break;
   }
}

/* ---------------------------------------------------------------------------
 * HC_GetIdentity
 * 
 * This function is called at init time to send an Identity Packet.
 * The Identity packet is an INet specific packet that is used to figure out
 * what our local IP address is. From this, our local BD_ADDR can be computed.
 */
void HC_GetIdentity(void)
{
   U8    idPacket[4];
   int   nameLen = sizeof(struct sockaddr_in);
   struct sockaddr_in   name;

   /* Get the port # of our dynamic InquirySkt */
   getsockname(IHC(inquirySkt), (struct sockaddr*)&name, &nameLen);

   IHC(state) = HC_MODE_IDENTIFY;
   
   StoreLE16(idPacket, HCP_IDENTITY);
   StoreLE16(idPacket+2, (U16)IHC(inquirySkt));

   if (!HC_SendTo(idPacket, 4, htonl(INADDR_BROADCAST), name.sin_port)) {

#if XA_DEBUG_PRINT == XA_ENABLED && !defined DEMO
       char        addr[BDADDR_NTOA_SIZE];
#endif
       IN_ADDR myIP;

       /* If this attempt fails, let's just assume our address is 
        * 127.0.0.1
        */
       myIP.S_un.S_un_b.s_b1 = 127;
       myIP.S_un.S_un_b.s_b2 = 0;
       myIP.S_un.S_un_b.s_b3 = 0;
       myIP.S_un.S_un_b.s_b4 = 1;
       BDA_IPADDR(&IHC(myAddr)) = myIP.s_addr;

       /* I copied all of this code out of the handler function
        * for HCP_IDENTITY. This is bad, so TODO: Normalize this 
        * process into a brand new function.
        */

      /* Complete the reset command */
      if (IHC(state) & HC_MODE_RESETTING) {
          U8 complete[4];

          complete[0] = HC_CmdQueueSpace();  /* Number of commands HCI can send */
          StoreLE16(complete + 1, HCC_RESET);
          complete[3] = HC_STATUS_SUCCESS;

          HC_PostEvent(HCE_COMMAND_COMPLETE, complete, 4);
      }

      IHC(state) = HC_MODE_IDLE;
      Report(("HC: Bluetooth device address is %s.\n", bdaddr_ntoa(&IHC(myAddr), addr)));
   }

   return;
}


/* ---------------------------------------------------------------------------
 * HC_IdentityPacket
 * 
 * This function processes a received INet Identity Packet.
 */
static void HC_IdentityPacket(void)
{
   U8          packet[4];
   SOCKADDR_IN fromAddr;
   int         fromLen = sizeof(SOCKADDR_IN);
   int         rcode;
#if XA_DEBUG_PRINT == XA_ENABLED && !defined DEMO
   char        addr[BDADDR_NTOA_SIZE];
#endif

   /* First remove the packet from the receive queue */
   rcode = recvfrom(IHC(inquirySkt), packet, 4, 0, (struct sockaddr *)&fromAddr, &fromLen);
   if (rcode == SOCKET_ERROR) {
      if (WSAGetLastError() == WSAEMSGSIZE) {
         rcode = 4;
      } else {
         Assert(WSAGetLastError() == WSAEWOULDBLOCK);
         return;
      }
   }
   Assert(rcode == 4);

   /* Now if we don't know who we are, this will tell us! */
   if (((IHC(state) & ~HC_MODE_RESETTING) == HC_MODE_IDENTIFY) && 
       (BDA_IPADDR(&IHC(myAddr)) == 0) &&
       (LEtoHost16(packet+2) == IHC(inquirySkt)) ) {

      BDA_IPADDR(&IHC(myAddr)) = fromAddr.sin_addr.s_addr;

      /* Complete the reset command */
      if (IHC(state) & HC_MODE_RESETTING) {
          U8 complete[4];

          complete[0] = HC_CmdQueueSpace();  /* Number of commands HCI can send */
          StoreLE16(complete + 1, HCC_RESET);
          complete[3] = HC_STATUS_SUCCESS;

          HC_PostEvent(HCE_COMMAND_COMPLETE, complete, 4);
      }

      IHC(state) = HC_MODE_IDLE;
      Report(("HC: Bluetooth device address is %s.\n", bdaddr_ntoa(&IHC(myAddr), addr)));
   }
}

/* ---------------------------------------------------------------------------
 * HC_InquiryPacket
 * 
 */
static void HC_InquiryPacket(SOCKET S, U16 opcode)
{
    SOCKADDR_IN from;
    I8          i;
    U8          request[11]; /* Used for rx & tx */
    U16         reqKey;
    U32         accessCode;
    int         fromLen = sizeof(SOCKADDR_IN);
    int         rcode;
    U8          inqResult[1 + 6 + 1 + 1 + 3 + 2 + 1];
    U8          pos;

    /*
     * Called when a packet is received on the inquiry socket.
     * Sends a response to the requestor.
     */

    /* receive the Inquiry or broadcast packet */
    rcode = recvfrom(S, request, 11, 0, (struct sockaddr *)&from, &fromLen);
    if (rcode == SOCKET_ERROR) {
        Assert(WSAGetLastError() == WSAEWOULDBLOCK);
        return;
    }
    Assert(rcode == 11);

    switch (opcode) {
    case HCP_INQUIRY_REQ:   /* Inquiry Request */
        /* If were not scaning for inquiries ignore the request. */
        if ((IHC(state) & HC_MODE_INQ_SCAN) == 0) {
            return;
        }

        /* Inquiry data is a unique 16 bit number (key) */
        reqKey = LEtoHost16(request+2);
        accessCode = LEtoHost32(request+4);

        /* Check to see if the access code is listed */
        for (i = 0; i < IHC(inqIACCount); i++) {
            if (accessCode == IHC(inqIACs)[i]) break;
        }

        if (i == IHC(inqIACCount)) {
            /* No match was found, ignore */
            return;
        }

        if (lastInquiry == reqKey) {
            /* We've already responded, don't respond again. */
            return;
        }

#if ALLOW_LOOPBACK == XA_DISABLED
        /* Don't respond to our own inquiries */
        if (IHC(inqKey) == reqKey) {
            return;
        }
#endif

#if FAST_INQUIRY_SCAN == XA_DISABLED
        /* Only respond if our unique value matches the key */
        if ((OS_Rand() & 0x000F) != (reqKey & 0x000F)) {
            return;
        }
#endif
        /* Remember the inquiry we're responding to. */
        lastInquiry = reqKey;

        /* Now respond to request */
        Report(("HC: Received Inquiry Request, sending Response.\n"));
        StoreLE16(request, HCP_INQUIRY_RSP); /* Inquiry_Response */
        request[2] = IHC(classOfDevice)[0];    /* Send our Class of device */
        request[3] = IHC(classOfDevice)[1];
        request[4] = IHC(classOfDevice)[2];
        *(BD_ADDR *)&request[5] = IHC(myAddr);

        HC_SendTo(request, 11, from.sin_addr.s_addr, from.sin_port);
        break;

    case HCP_INQUIRY_RSP:   /* Inquiry Response to our request */

        /* If were not inquiring ignore the response */
        if ((IHC(state) & HC_MODE_INQUIRY) == 0) {
            return;
        }

        if ((IHC(inqResponseMax) == 0) || (IHC(inqResponseCount) < IHC(inqResponseMax))) {
            
            IHC(inqResponseCount)++;
            Report(("HC: Received Inquiry Response #%d (max %d).\n", IHC(inqResponseCount), IHC(inqResponseMax)));
            
            inqResult[0] = 1;       /* Num responses */
            OS_MemCopy(inqResult + 1, request + 5, 6); /* BD_ADDR */
            /* Overwrite (possibly zero) IP addr part */
            BDA_IPADDR((BD_ADDR *)(inqResult + 1)) = from.sin_addr.s_addr;
            inqResult[7] = 0x00;    /* Scan Repetition */
            inqResult[8] = 0x00;    /* Scan Period/Mode */
            pos=9;

            if (!(IHC(flags) & IHCF_INQUIRY_RSSI)) {
                inqResult[pos++] = 0x00; /* Reserved byte only in non-RSSI */
            }

            OS_MemCopy(inqResult + pos, request + 2, 3); /* Class of Device */
            pos += 3;
            StoreLE16(inqResult + pos, 0);   /* Clock offset */
            pos += 2;

            /* Include RSSI in result? */
            if (IHC(flags) & IHCF_INQUIRY_RSSI) {
                inqResult[pos++] = 0x9B;           /* RSSI */
                HC_PostEvent(HCE_INQUIRY_RESULT_WITH_RSSI, inqResult, pos);
            } else {
                HC_PostEvent(HCE_INQUIRY_RESULT, inqResult, pos);
            }
        }
        break;

    default:
        Assert(0);
        break;
    }

    return;
}


/* ---------------------------------------------------------------------------
 * HC_ConnectRequest
 * 
 */
static void HC_ConnectRequest(void)
{
   BtConn   *conn = 0;
   SOCKADDR_IN incomingAddr;
   int       fromLen = sizeof(SOCKADDR_IN);
   int       i, rcode;
   SOCKET    incoming;

   if ((IHC(state) & HC_MODE_PAGE_SCAN) == 0) {
DumpConnection:
      /* Accept connection and immediately close it */
      incoming = accept(IHC(pageSkt), (SOCKADDR *)&incomingAddr, &fromLen);
      if (incoming != INVALID_SOCKET) {
          Sleep(200); /* Allow any stragglers to come through */
          closesocket( incoming );
      }      

      return;
   }

   /*
    * Called when a connection attempt is received by the page socket.
    * Accepts connection if possible. Indicates connection request to HCI
    * when TCP connection is complete.
    */
   for (i = 0; i < NUM_BT_DEVICES; i++) {
      if (IHC(connTab)[i].link == INVALID_SOCKET) {
         conn = &IHC(connTab)[i];
         break;
      }
   }

   if (conn == 0) {
       Report(("HC: TCP Connect request failed, no more connections.\n"));
       goto DumpConnection;
   }

   OS_MemSet((U8 *)conn, 0, sizeof(BtConn));

   /* Initialize the authorization states */
   conn->authCur = AuthReady;
   
   /* Initialize other connection variables */
   HC_InitConn(conn);

   /* Accept connection */
   conn->link = accept(IHC(pageSkt), (struct sockaddr *)&conn->addr, &fromLen);
   if (conn->link != INVALID_SOCKET) {

       rcode = WSAAsyncSelect(conn->link, IHC(driverWnd), WM_SELECT, FD_WRITE|FD_READ|FD_CLOSE);
       if (rcode != SOCKET_ERROR) {
           /* Write event on conn->link confirms connection */
           conn->role = BCR_SLAVE;
           conn->state = LINK_ACCEPTING;
           conn->stageLen = 0;
           Report(("HC: Accepted TCP Connection from %s, on socket %x.\n", inet_ntoa(conn->addr.sin_addr), conn->link));
           return;
       }
       
       HC_CloseLink( conn );
   }

   /* Reject page request, no more connections or internal error */
   Report(("HC: TCP Connect request failed internal error.\n"));
}


#if NUM_SCO_CONNS > 0
/* ---------------------------------------------------------------------------
 * HC_Disconnect
 * Does an emergency close of any outstanding SCO connections.
 */
static void HC_CloseScoCons(BtConn *conn, U8 reason)
{
    U8       discEvent[4];
    int      i;
    U8       *RejConnCompletionEvent;
    ScoConn *sco;

    /* Hunt for SCO links and disconnect them. */
    for (i = 0; i < NUM_SCO_CONNS; i++) {
        if (IHC(scoTab)[i].parent == conn) {
            sco = &IHC(scoTab)[i];
            Assert(sco->state != SCO_INACTIVE);
            
            if (sco->state > SCO_CONNECTING)
            {
                /* Indicate SCO Disconnect Completion event to HCI */
                discEvent[0] = HC_STATUS_SUCCESS;
                discEvent[1] = i + NUM_BT_HANDLES;
                discEvent[2] = 0;
                discEvent[3] = reason;
            
                HC_PostEvent(HCE_DISCONNECT_COMPLETE, discEvent, 4);

                /* Absolve any outstanding SCO packets considered "in use" */
                IHC(hostCurScoPackets) -= sco->hostCurScoPackets;

                /* Free pending SCO connection */
                OS_MemSet((U8 *)sco, 0, sizeof(ScoConn));

            }
            else if (sco->state == SCO_CONNECTING)
            {
                /* Indicate setup failed? Guess it depends if we created this. */
                if (sco->localRequest)
                {
                    RejConnCompletionEvent = malloc(11);
                    RejConnCompletionEvent = malloc(17);
                    RejConnCompletionEvent[0]  = HC_STATUS_CONN_TERM_LOCAL_HOST;
                    StoreLE16(RejConnCompletionEvent+1, 0xFFFF);       /* Invalid Conn Handle */
                    ConnAddr2BdAddr(conn, (BD_ADDR *)(RejConnCompletionEvent+3));
                    RejConnCompletionEvent[9]  = sco->linkType;
                    RejConnCompletionEvent[10]  = 0;          /* Tx interval */
                    RejConnCompletionEvent[11] = 0;           /* rTx window */
                    StoreLE16(RejConnCompletionEvent+12, 30); /* Rx packet len */
                    StoreLE16(RejConnCompletionEvent+14, 30); /* Tx packet len */
                    RejConnCompletionEvent[16] = 2;           /* Air Mode */

                    HC_DelayEvent(HCE_SYNC_CONNECT_COMPLETE, RejConnCompletionEvent, 17);
                }
                /* Free pending SCO connection */
                OS_MemSet((U8 *)sco, 0, sizeof(ScoConn));
            }
        }
    }
}
#endif

/* ---------------------------------------------------------------------------
 * HC_Disconnect
 * 
 */
HcStatus HC_Disconnect(U16 ConHndl, U8 Reason)
{
   U8       discReq[4];
   U8      *DiscCompletionEvent;
   ScoConn *sco = 0;
   BtConn  *conn;
   BOOL     sent;

    switch (Reason) {
    case BEC_AUTHENTICATE_FAILURE:
    case BEC_USER_TERMINATED:
    case BEC_LOW_RESOURCES:
    case BEC_POWER_OFF:
    case BEC_UNSUPPORTED_REMOTE:
    case BEC_PAIR_UNITKEY_NO_SUPP:
        break;
    default:
        return HC_STATUS_INVAL_HCI_PARM_VAL;
        break;
    }
    
   if (ConHndl > NUM_BT_DEVICES) {

       if (ConHndl > NUM_BT_HANDLES + NUM_SCO_CONNS) {
           return HC_STATUS_NO_CONNECTION;
       }

#if NUM_SCO_CONNS > 0
       /* This is likely a SCO connection handle. */
       sco = &IHC(scoTab)[ConHndl-NUM_BT_HANDLES];
       if (sco->state != SCO_ACTIVE) {
           return HC_STATUS_NO_CONNECTION;
       }
       conn = sco->parent;
#endif
   } else {
       conn = &IHC(connTab)[ConHndl];
   }

   /* Make sure it's OK to disconnect */
   if (conn->flags & LINK_FLAG_NAME_REQ) {
       /* Disconnect happened at a bad time. Acknowledge the disconnect
        * request with a SUCCESS message but follow up with a
        * DISCONNECT_COMPLETE event bearing an error.
        */
        DiscCompletionEvent = malloc(4);
        StoreLE16(DiscCompletionEvent+1, ConHndl);
        DiscCompletionEvent[3] = HC_STATUS_CMD_DISALLOWED;
        HC_DelayEvent(HCE_DISCONNECT_COMPLETE, DiscCompletionEvent, 4);
        
        /* Absolve any outstanding SCO packets considered "in use" */
        IHC(hostCurAclPackets) -= conn->hostCurAclPackets;
        conn->hostCurAclPackets = 0;
    
        return HC_STATUS_SUCCESS;
   }

   if (conn->state == LINK_HOLD ||
       conn->state == LINK_PARK ||
       conn->state == LINK_SNIFF) {             /* Cancel any hold timers */
        EVM_CancelTimer(&(conn->connTimer));

        /* If any ACL Data packets have queued up while in hold, send them */
        HC_SendQueuedAclData(conn);

        /* Set connection mode to active */
        conn->state = LINK_ACTIVE;
        conn->waitModeChange = FALSE;
        HoldTimerFire(&(conn->connTimer));
   }

    /* If we were in the middle of an incoming buffer then get rid of it */
    if (conn->hciBuffer != 0) {
        HCI_RxBufferInd(conn->hciBuffer, BT_STATUS_FAILED);
    }

   /* Disconnect link to specified device. */
    if (conn->link == INVALID_SOCKET) {
        if (sco) {
            /* If our user is trying to disconnect an SCO link _after_ its
             * ACL link is closed, that's bad. Bad enough to assert, but
             * not until we've completely addressed this in ME.
             * Assert(sco==0);
             */

            /* Try to recover */
            sco->parent = 0;
            sco->state = SCO_INACTIVE;
        }
        return HC_STATUS_NO_CONNECTION;
    }


   if (sco) {
       /* Send a SCO Disconnect */
       StoreLE16(discReq, HCP_LMP_REMOVE_SCO_REQ);
       discReq[2] = sco->handle;
       discReq[3] = Reason;

       sent = HC_SendLmp(conn->link, discReq, 4, 0);
   } else {
       /* Send LMP_Detach to peer, then close the TCP connection. */
       StoreLE16(discReq, HCP_LMP_DETACH);
       discReq[2] = Reason;

       sent = HC_SendLmp(conn->link, discReq, 3, 0);

#if NUM_SCO_CONNS > 0
       HC_CloseScoCons(conn, HC_STATUS_CONN_TERM_LOCAL_HOST);
#endif
       HC_CloseLink( conn );

   }

   /* Queue Disconnect Completion event for posting. The send can
    * fail if the other side closes the connection before we get 
    * to send our disconnect. In this case the close would have
    * already generated a DISCONNECT_COMPLETE event.
    */
    DiscCompletionEvent = malloc(4);
    if (sent) {
        if (sco)
            sco->state = SCO_INACTIVE;
        DiscCompletionEvent[0] = HC_STATUS_SUCCESS;
    }
    else DiscCompletionEvent[0] = HC_STATUS_HARDWARE_FAILURE;

    StoreLE16(DiscCompletionEvent+1, ConHndl);
    DiscCompletionEvent[3] = HC_STATUS_CONN_TERM_LOCAL_HOST;

    HC_DelayEvent(HCE_DISCONNECT_COMPLETE, DiscCompletionEvent, 4);
    
    /* Absolve any outstanding ACL packets considered "in use" */
    IHC(hostCurAclPackets) -= conn->hostCurAclPackets;
    conn->hostCurAclPackets = 0;
    return HC_STATUS_SUCCESS;
}

/* ---------------------------------------------------------------------------
 * HC_DisconnectInd
 * 
 */
void HC_DisconnectInd(SOCKET S)
{
    BtConn  *conn = 0;
    int      i;
    int      connHandle;
    U8       *discEvent;

    /*
     * Called when the link is disconnected. Indicates disconnect to HCI.
     */
    for (i = 0; i < NUM_BT_DEVICES; i++) {
        if (IHC(connTab)[i].link == S) {
            conn = &IHC(connTab)[i];
            connHandle = i;
            break;
        }
    }

    if (conn) {
        Assert(conn->link != INVALID_SOCKET);
        Assert(conn->state != LINK_FREE);

        /* If we were in the middle of an incoming buffer then get rid of it */
        if (conn->hciBuffer != 0) {
            HCI_RxBufferInd(conn->hciBuffer, BT_STATUS_FAILED);
        }

        if (conn->state < LINK_ACTIVE) {
            /* The connection isn't up yet. Indicate CONNECT_COMPLETE:Failed */
            HC_ConnectStatus(S, FALSE);
            return;
        }

       if (conn->state == LINK_HOLD ||
           conn->state == LINK_PARK ||
           conn->state == LINK_SNIFF) {            /* Cancel any hold timers */
            EVM_CancelTimer(&(conn->connTimer));

            /* If any ACL Data packets have queued up while in hold, send them */
            HC_SendQueuedAclData(conn);
            
            /* Set connection mode to active */
            conn->state = LINK_ACTIVE;
            conn->waitModeChange = FALSE;
            HoldTimerFire(&(conn->connTimer));
        }

#if NUM_SCO_CONNS > 0
        HC_CloseScoCons(conn, HC_STATUS_CONN_TIMEOUT);
#endif
        /* Close endpoint */
        HC_CloseLink( conn );

        /* Indicate Disconnect Completion (ACL) event to HCI. Delay it
         * just in case SCO discon events were queued previously. */
        discEvent = malloc(4);
        discEvent[0] = HC_STATUS_SUCCESS;
        StoreLE16(discEvent+1, (U16)connHandle);
        discEvent[3] = HC_STATUS_CONN_TIMEOUT;

        HC_DelayEvent(HCE_DISCONNECT_COMPLETE, discEvent, 4);

        /* Absolve any outstanding ACL packets considered "in use" */
        IHC(hostCurAclPackets) -= conn->hostCurAclPackets;

    }        
    return;
}


/* ---------------------------------------------------------------------------
 * HC_ConnectStatus
 * 
 */
static void HC_ConnectStatus(SOCKET S, BOOL Success)
{
   BtConn  *conn = 0;
   int      i;
   U8       complete[11];
   U8       connReq[8], nameReq[2];

   /*
    * Called when a TCP connection we initiated is complete. Reports both
    * success and failure cases.
    */
   for (i = 0; i < NUM_BT_DEVICES; i++) {
      if (IHC(connTab)[i].link == S) {
         conn = &IHC(connTab)[i];
         break;
      }
   }
   Assert(conn);
   Assert(conn->link != INVALID_SOCKET);

   /* This can happen if we get flow controlled while connected */
   if (conn->state == LINK_ACTIVE)
      return;

   if (Success == TRUE) {
      Report(("HC: TCP Connection complete, ConnHndl %d.\n", i));

      if (conn->role == BCR_MASTER) {

          /* Is this a temp connection, for getting the Remote Name? */
          if (conn->nameReqOnly) {
              StoreLE16(nameReq, HCP_LMP_NAME_REQ);
              conn->flags |= LINK_FLAG_NAME_REQ;
              HC_SendLmp(conn->link, nameReq, 2, 0); 
              return;
          }

         /* Send LMP Connect Req to slave */
         StoreLE16(connReq, HCP_LMP_CONN_REQ);
         connReq[2] = IHC(classOfDevice)[0];    /* Send our Class of Device */
         connReq[3] = IHC(classOfDevice)[1];
         connReq[4] = IHC(classOfDevice)[2];
         connReq[5] = (U8)(BDA_IPPORT(&IHC(myAddr)));
         connReq[6] = (U8)(BDA_IPPORT(&IHC(myAddr)) >> 8);

         HC_SendLmp(conn->link, connReq, 7, 0);
      }
      return;
 
   } else {
       Report(("HC: TCP Connection failed, ConnHndl %d.\n", i));
       
       conn->link = INVALID_SOCKET;

       if (conn->role == BCR_MASTER) {

           /* If this was supposed to get the remote device's name then
            * report failure appropriately.
            */
           if (conn->nameReqOnly) {
               complete[0] = HC_STATUS_CONN_TIMEOUT;
               ConnAddr2BdAddr(conn, (BD_ADDR *)(complete+1));
               /* Null-terminate the remote name string */
               complete[7] = 0;
               HC_PostEvent(HCE_REMOTE_NAME_REQ_COMPLETE, complete, 8);
               return;
           }

           /* Connection Completion failure values */
           complete[0]  = HC_STATUS_CONN_TIMEOUT;
           StoreLE16(complete+1, 0xFFFF);       /* Invalid Conn Handle */
           ConnAddr2BdAddr(conn, (BD_ADDR *)(complete+3));
           complete[9]  = 0x01;     /* ACL Link Type */
           complete[10] = 0x00;

           HC_PostEvent(HCE_CONNECT_COMPLETE, complete, 11);
       }
   }

   return;
}

/* ---------------------------------------------------------------------------
 * HC_RejectConn
 * 
 */
HcStatus HC_RejectConn(BD_ADDR Addr, U8 Reason)
{
    U8        connRsp[5];
    int       i;
    BtConn   *conn = 0;
    U8       *RejConnCompletionEvent;
    BD_ADDR   tmpAddr;
    
    if (Reason < HC_STATUS_HOST_REJ_NO_RESOURCES ||
        Reason > HC_STATUS_HOST_REJ_PERSONAL_DEV) {
        return HC_STATUS_INVAL_HCI_PARM_VAL;
    }
    
    /* Find an matching connection entry */
    for (i = 0; i < NUM_BT_DEVICES; i++) {
        if ((IHC(connTab)[i].state == LINK_ACCEPTING) ||
            (IHC(connTab)[i].state == LINK_ACTIVE)) {
            ConnAddr2BdAddr(&IHC(connTab)[i], &tmpAddr);
            
            if (AreBdAddrsEqual(&tmpAddr, &Addr)) {
                conn = &IHC(connTab)[i];
                break;
            }
        }
    }
    
    if (!conn || conn->link == INVALID_SOCKET) {
        return HC_STATUS_NO_CONNECTION;
    }
    
#if NUM_SCO_CONNS > 0
    if (conn->state == LINK_ACTIVE) {
        /* The ACL connection is already up. It must be a SCO conn coming up. */
        return HC_ScoRejectConn(conn, Reason);
    }
#endif
    
    /* Send the LMP_Not_Accepted */
    StoreLE16(connRsp, HCP_LMP_NOT_ACCEPTED);
    StoreLE16(connRsp+2, HCP_LMP_CONN_REQ);
    connRsp[4] = Reason;
    
    HC_SendLmp(conn->link, connRsp, 5, 0);
    
    /* Close down link */
    HC_CloseLink( conn );
    
    /* Connection Completion failure values */
    RejConnCompletionEvent = malloc(11);
    RejConnCompletionEvent[0]  = HC_STATUS_CONN_TERM_LOCAL_HOST;
    StoreLE16(RejConnCompletionEvent+1, 0xFFFF);       /* Invalid Conn Handle */
    ConnAddr2BdAddr(conn, (BD_ADDR *)(RejConnCompletionEvent+3));
    RejConnCompletionEvent[9]  = 0x01;   /* ACL Link Type */
    RejConnCompletionEvent[10] = 0x00;   /* No encryption */
    
    HC_DelayEvent(HCE_CONNECT_COMPLETE, RejConnCompletionEvent, 11);
    
    return HC_STATUS_SUCCESS;
}

/* ---------------------------------------------------------------------------
 * HC_AcceptConn
 * 
 */
HcStatus HC_AcceptConn(BD_ADDR Addr, U8 Role)
{
   BtConn  *conn = 0;
   int      i;
   U8       connRsp[4];
   BD_ADDR  tmpAddr;

   /* Find a matching connection entry */
   for (i = 0; i < NUM_BT_DEVICES; i++) {
      if ((IHC(connTab)[i].state == LINK_ACCEPTING) ||
          (IHC(connTab)[i].state == LINK_ACTIVE)) {
         ConnAddr2BdAddr(&IHC(connTab)[i], &tmpAddr);

         if (AreBdAddrsEqual(&tmpAddr, &Addr)) {
            conn = &IHC(connTab)[i];
            break;
         }
      }
   }

   if (!conn || conn->link == INVALID_SOCKET) {
      return HC_STATUS_NO_CONNECTION;
   }

#if NUM_SCO_CONNS > 0
   if (conn->state == LINK_ACTIVE) {
       /* The ACL connection is already up. It must be a SCO conn coming up. */
       return HC_ScoAcceptConn(conn);
   }
#endif

   /* Send connect response to Master */
   if (Role == 0x01) {
      /* Remain the slave, send LMP_Accpeted */
      StoreLE16(connRsp, HCP_LMP_ACCEPTED);
      StoreLE16(connRsp+2, HCP_LMP_CONN_REQ);    /* accepted-opcode */

      HC_SendLmp(conn->link, connRsp, 4, 0);

       conn->state = LINK_SETUP;
   } else {
      /* Request to become the Master, send LMP_switch_req */

      conn->roleChanged = TRUE;

      StoreLE16(connRsp, HCP_LMP_SWITCH_REQ);
      /* request the other guy to be slave */
      connRsp[2] = BCR_SLAVE;
      HC_SendLmp(conn->link, connRsp, 3, 0);

      /* conn->state should still be LINK_CONNECTING until the role change is 
       * finished, then we can send the HCP_LMP_CONN_REQ and change the state
       * to LINK_SETUP
       */
   }


   InitializeListHead(&(conn->ACLDataQueue));

   return HC_STATUS_SUCCESS;
}


#if NUM_SCO_CONNS > 0
/* ---------------------------------------------------------------------------
 * HC_ScoAcceptConn
 * 
 */
HcStatus HC_ScoAcceptConn(BtConn *Acl)
{
    ScoConn *sco;
    I8       i;
    U8       scoReq[5], accepted[4];
    U8      *complete;
    
    for (i = 0; i < NUM_SCO_CONNS; i++) {
        if (IHC(scoTab)[i].parent == Acl && IHC(scoTab)[i].state == SCO_CONNECTING) {
            break;
        }
    }
       
    if (i == NUM_SCO_CONNS) {
        return HC_STATUS_NO_CONNECTION;
    }
    sco = &IHC(scoTab)[i];

    /* We found the matching SCO connection. If we are the Master then we
     * have to re-initiate the SCO connection.
     */
    if (Acl->role == BCR_MASTER) {
        StoreLE16(scoReq, HCP_LMP_SCO_LINK_REQ);
        scoReq[2] = sco->handle;  /* SCO Handle, only Master can assign */
        scoReq[3] = 2;  /* Packet types: 0 = HV1, 1 = HV2, 2 = HV3 */
        scoReq[4] = 2;  /* Air Mode: 2 = CVSD */

        HC_SendLmp(Acl->link, scoReq, 5, 0);

        return HC_STATUS_SUCCESS;
    }
    
    /* Accept the SCO link */
    sco->state = SCO_ACTIVE;

    StoreLE16(accepted, HCP_LMP_ACCEPTED);
    StoreLE16(accepted+2, HCP_LMP_SCO_LINK_REQ);

    HC_SendLmp(Acl->link, accepted, 4, 0);

    /* Indicate CONNECT Complete (success) to HCI */
    complete = malloc(11);
    complete[0]  = HC_STATUS_SUCCESS;
    /* SCO Handle = SCO index + NumBtHandles Offset */
    complete[1]  = (U8)(sco - IHC(scoTab)) + NUM_BT_HANDLES;
    complete[2]  = 0;
    ConnAddr2BdAddr(Acl, (BD_ADDR *)(complete+3));
    complete[9]  = 0;   /* SCO Link */
    complete[10] = 0;   /* No encryption */

    HC_DelayEvent(HCE_CONNECT_COMPLETE, complete, 11);

    return HC_STATUS_SUCCESS;
}

/* ---------------------------------------------------------------------------
 * HC_ScoRejectConn
 * 
 */
static HcStatus HC_ScoRejectConn(BtConn *Acl, U8 Reason)
{
    ScoConn *sco;
    I8       i;
    U8       connRsp[5];
    U8      *RejConnCompletionEvent;
    
    for (i = 0; i < NUM_SCO_CONNS; i++) {
        if (IHC(scoTab)[i].parent == Acl && IHC(scoTab)[i].state == SCO_CONNECTING) {
            break;
        }
    }
       
    if (i == NUM_SCO_CONNS) {
        return HC_STATUS_NO_CONNECTION;
    }
    sco = &IHC(scoTab)[i];

    /* We found the matching SCO connection. Send the LMP_Not_Accepted */
    StoreLE16(connRsp, HCP_LMP_NOT_ACCEPTED);
    StoreLE16(connRsp+2, HCP_LMP_SCO_LINK_REQ);
    connRsp[4] = Reason;

    HC_SendLmp(Acl->link, connRsp, 5, 0);

    /* Connection Completion failure values */
    RejConnCompletionEvent = malloc(11);
    RejConnCompletionEvent[0]  = HC_STATUS_CONN_TERM_LOCAL_HOST;
    StoreLE16(RejConnCompletionEvent+1, 0xFFFF);       /* Invalid Conn Handle */
    ConnAddr2BdAddr(Acl, (BD_ADDR *)(RejConnCompletionEvent+3));
    RejConnCompletionEvent[9]  = 0x00;   /* SCO Link Type */
    RejConnCompletionEvent[10] = 0x00;   /* No encryption */

    HC_DelayEvent(HCE_CONNECT_COMPLETE, RejConnCompletionEvent, 11);

    /* Free pending SCO connection */
    OS_MemSet((U8 *)sco, 0, sizeof(ScoConn));

    return HC_STATUS_SUCCESS;
}

/* ---------------------------------------------------------------------------
 * HC_AcceptSyncConn
 * 
 */
HcStatus HC_AcceptSyncConn(BtConn *Acl)
{
    ScoConn *sco;
    I8       i;
    U8       scoReq[12], accepted[4];
    U8      *complete;
    
    if (!Acl || Acl->link == INVALID_SOCKET) {
        return HC_STATUS_NO_CONNECTION;
    }

    for (i = 0; i < NUM_SCO_CONNS; i++) {
        if (IHC(scoTab)[i].parent == Acl && IHC(scoTab)[i].state == SCO_CONNECTING) {
            break;
        }
    }
       
    if (i == NUM_SCO_CONNS) {
        return HC_STATUS_NO_CONNECTION;
    }
    sco = &IHC(scoTab)[i];

    /* We found the matching SCO connection. If we are the Master then we
     * have to re-initiate the SCO connection.
     */
    if (Acl->role == BCR_MASTER) {
        StoreLE16(scoReq, HCP_LMP_ESCO_LINK_REQ);
        scoReq[2] = sco->handle;  /* SCO Handle, only Master can assign */
        /* We should negotiate the following, but won't for now. */
        scoReq[3] = 0x07;           /* EV3 M->S */
        scoReq[4] = 0x07;           /* EV3 S->M */
        StoreLE16(scoReq+5, 30);    /* Packet size M->S */
        StoreLE16(scoReq+7, 30);    /* Packet size S->M */
        scoReq[10] = 2;             /* Air Mode: 2 = CVSD  */
        scoReq[11] = 0;             /* Negotiation state */

        HC_SendLmp(Acl->link, scoReq, 12, 0);

        return HC_STATUS_SUCCESS;
    }
    
    /* Accept the SCO link */
    sco->state = SCO_ACTIVE;

    StoreLE16(accepted, HCP_LMP_ACCEPTED_EXT);
    StoreLE16(accepted+2, HCP_LMP_ESCO_LINK_REQ);

    HC_SendLmp(Acl->link, accepted, 4, 0);

    /* Indicate SYNC CONNECT Complete (success) to HCI */
    complete = malloc(17);
    complete[0]  = HC_STATUS_SUCCESS;
    /* SCO Handle = SCO index + NumBtHandles Offset */
    complete[1]  = (U8)(sco - IHC(scoTab)) + NUM_BT_HANDLES;
    complete[2]  = 0;
    ConnAddr2BdAddr(Acl, (BD_ADDR *)(complete+3));
    complete[9]  = sco->linkType;
    complete[10]  = 0;          /* Tx interval */
    complete[11] = 0;           /* rTx window */
    StoreLE16(complete+12, 30); /* Rx packet len */
    StoreLE16(complete+14, 30); /* Tx packet len */
    complete[16] = 2;           /* Air Mode */

    HC_DelayEvent(HCE_SYNC_CONNECT_COMPLETE, complete, 17);

    return HC_STATUS_SUCCESS;
}

/* ---------------------------------------------------------------------------
 * HC_RejectSyncConn
 * 
 */
HcStatus HC_RejectSyncConn(BD_ADDR Addr, U8 Reason)
{
    BtConn  *Acl = 0;
    ScoConn *sco;
    I8       i;
    U8       connRsp[5];
    U8      *RejConnCompletionEvent;
    BD_ADDR  tmpAddr;
    
    /* Find a matching connection entry */
    for (i = 0; i < NUM_BT_DEVICES; i++) {
       if ((IHC(connTab)[i].state == LINK_ACCEPTING) ||
           (IHC(connTab)[i].state == LINK_ACTIVE)) {
          ConnAddr2BdAddr(&IHC(connTab)[i], &tmpAddr);
  
          if (AreBdAddrsEqual(&tmpAddr, &Addr)) {
             Acl = &IHC(connTab)[i];
             break;
          }
       }
    }
  
    if (!Acl || Acl->link == INVALID_SOCKET) {
       return HC_STATUS_NO_CONNECTION;
    }

    for (i = 0; i < NUM_SCO_CONNS; i++) {
        if (IHC(scoTab)[i].parent == Acl && IHC(scoTab)[i].state == SCO_CONNECTING) {
            break;
        }
    }
       
    if (i == NUM_SCO_CONNS) {
        return HC_STATUS_NO_CONNECTION;
    }
    sco = &IHC(scoTab)[i];

    /* We found the matching SCO connection. Send the LMP_Not_Accepted */
    StoreLE16(connRsp, HCP_LMP_NOT_ACCEPTED_EXT);
    StoreLE16(connRsp+2, HCP_LMP_ESCO_LINK_REQ);
    connRsp[4] = Reason;

    HC_SendLmp(Acl->link, connRsp, 5, 0);

    /* Connection Completion failure values */
    RejConnCompletionEvent = malloc(17);
    RejConnCompletionEvent[0]  = HC_STATUS_CONN_TERM_LOCAL_HOST;
    StoreLE16(RejConnCompletionEvent+1, 0xFFFF);       /* Invalid Conn Handle */
    ConnAddr2BdAddr(Acl, (BD_ADDR *)(RejConnCompletionEvent+3));
    RejConnCompletionEvent[9]  = sco->linkType;
    RejConnCompletionEvent[10]  = 0;          /* Tx interval */
    RejConnCompletionEvent[11] = 0;           /* rTx window */
    StoreLE16(RejConnCompletionEvent+12, 30); /* Rx packet len */
    StoreLE16(RejConnCompletionEvent+14, 30); /* Tx packet len */
    RejConnCompletionEvent[16] = 2;           /* Air Mode */

    HC_DelayEvent(HCE_SYNC_CONNECT_COMPLETE, RejConnCompletionEvent, 17);

    /* Free pending SCO connection */
    OS_MemSet((U8 *)sco, 0, sizeof(ScoConn));

    return HC_STATUS_SUCCESS;
}
#endif

/* ---------------------------------------------------------------------------
 * HC_DataInd
 * 
 */
void HC_DataInd(SOCKET S)
{
    int         dataLen;
    U16         i, hcp, hndl;
    U8         *buffer, headerSize;
    BtConn     *conn = 0;
    ScoConn    *sco = 0;
    BtStatus    status;
    
    /*
     * Called when data arrives on a connected socket.
     */
    for (i = 0; i < NUM_BT_DEVICES; i++) {
        if (IHC(connTab)[i].link == S) {
            conn = &IHC(connTab)[i];
            break;
        }
    }
    
    if (!conn) return;
    
    /* Receive data until pipe runs dry. It may take multiple receive 
     * calls to get the complete BT Packet.
     */
    while (1) {
        
        if (conn->hciBuffer == 0) {
            
            /* NEW Packet: Read the HCP Type to dispatch data to proper receiver. */
            if (conn->stageLen < 2) {
                dataLen = recv(S, conn->stage+conn->stageLen, 2-conn->stageLen, 0);
                if (dataLen == SOCKET_ERROR) {
                    if (WSAGetLastError() != WSAEWOULDBLOCK)
                        Report(("HC: DataInd() recv Failed, reason %s.\n",WSA_LastError()));
                    return;
                }
                conn->stageLen += dataLen;
            }
            
            if (conn->stageLen < 2)
                return;
            
            /* Send all LM Events to the ProcessLmCommand function. */
            hcp = LEtoHost16(conn->stage);
            if ((hcp & HCP_TYPE_MASK) == HCP_LMP_EVENT) {
                HC_ProcessLmCommand(hcp, conn, i);
                conn->stageLen = 0;
                return;
            }
            
            /* It's a new data packet. */
            
            if (conn->state == LINK_HOLD) {
                /* The HOLD timer should be about to expire. Just a small
                 * difference between master & slave timing can cause us to
                 * receive data while in hold. Just assert that this is what
                 * is happening, and force us out of hold mode.
                 */
                Assert((conn->connTimer.startTime + conn->connTimer.time) < 
                       (OS_GetSystemTime() + HOLDTIMER_LEEWAY));
                
                EVM_CancelTimer(&(conn->connTimer));
                HoldTimerFire(&(conn->connTimer));
            }

            /* This assert needs to check if the packet is a piconet broadcast if
             * the connection state is LINK_PARK, but I haven't figured out which
             * paramter containts that information yet */
            Assert((conn->state == LINK_ACTIVE) || (conn->state == LINK_SNIFF) ||
                    ((conn->state == LINK_PARK)));/* && ((hcp & 0x0f) >= HCP_ACTV_FIRST))); */

            /* It's a new data packet. We need to stage 2 more bytes */
            if (conn->stageLen < 4) {
                dataLen = recv(S, conn->stage+conn->stageLen, 4-conn->stageLen, 0);
                if (dataLen == SOCKET_ERROR) {
                    if (WSAGetLastError() != WSAEWOULDBLOCK)
                        Report(("HC: DataInd() recv Failed, reason %s.\n",WSA_LastError()));
                    return;
                }
                conn->stageLen += dataLen;
            }
            
            if (conn->stageLen < 4)
                return;
            
            /* Before we try to request an RxBuffer, send up any outstanding events */
            if (!HC_ProcessEventOverrunQueue()) {
                /* The data is coming in faster than we can handle it! */
                /* There many not be any event buffers available right now.
                 * That's OK, as soon as the stack returns them we will handle
                 * outstanding events.
                 */
                Report(("HC: Ran out of buffers for indicating events\n"));
                 /* Assert(0); */
            }
            headerSize = 4;

            /* We have enough data staged to request an rxBuffer */
            /* Generate the Connection Handle, flag as FIRST or CONTINUE */
            switch (LEtoHost16(conn->stage)) {
            case HCP_ACTV_FIRST:
                hndl = (ACTIVE_BROADCAST|FIRST_PACKET);
                break;
            case HCP_ACTV_CONTU:
                hndl = (ACTIVE_BROADCAST|CONTINUING_PACKET);
                break;
            case HCP_PNET_FIRST:
                hndl = (PICONET_BROADCAST|FIRST_PACKET);
                break;
            case HCP_PNET_CONTU:
                hndl = (PICONET_BROADCAST|CONTINUING_PACKET);
                break;
            case HCP_L2C_FIRST:
                hndl = (U16)(i | FIRST_PACKET);
                break;
            case HCP_L2C_CONTU:
                hndl = (U16)(i | CONTINUING_PACKET);
                break;
#if NUM_SCO_CONNS > 0
            case HCP_SCO_AUDIO:
                /* SCO Link conn Handle is in byte 4 of SCO packet. */
                sco = FindScoLinkbyHandle(conn->stage[3]);
                Assert(sco);
                hndl = (U16)(sco - IHC(scoTab)) + NUM_BT_HANDLES;
                headerSize = 3;
#endif
                break;
                
            default:
                Report(("HC: Unrecognized connection handle: %d\n",
                    LEtoHost16(conn->stage)));
                conn->stageLen = 0;
                return;
            }
            
            /* SCO packets have a 1 byte data len field, ACL is 2 bytes. */
            if (LEtoHost16(conn->stage) == HCP_SCO_AUDIO) {
                conn->hciPacketLen = conn->stage[2];
                status = HCI_GetScoBuffer(&conn->hciBuffer, hndl, (U16)(conn->hciPacketLen+3));
            } else {
                conn->hciPacketLen = LEtoHost16(conn->stage+2);
                status = HCI_GetAclBuffer(&conn->hciBuffer, hndl, (U16)(conn->hciPacketLen+4));
            }
            
            /* The 'Wakeup' socket is used to restart the recv process
             * when buffers are available.
             */
            if (status == BT_STATUS_NO_RESOURCES) {
                Report(("HC: DataInd() NO RESOURCES Socket %d (Wakeup %d).\n", S, Wakeup));
                Wakeup = S;
                return;
            }
            Assert(status == BT_STATUS_SUCCESS);
            
            buffer = HCI_GetRxPtr(conn->hciBuffer);
            
            /* Copy connection handle and packet length into buffer */
            StoreLE16(buffer, hndl);
            if (LEtoHost16(conn->stage) == HCP_SCO_AUDIO) {
                buffer[2] = (U8)conn->hciPacketLen;
                conn->hciBufferLen = 3;
            } else {
                StoreLE16(buffer+2, conn->hciPacketLen);
                conn->hciBufferLen = 4;
            }
            conn->stageLen = 0;

      } else {
          
          /* Receive was already in progress */
          if (LEtoHost16(conn->stage) == HCP_SCO_AUDIO) {
              headerSize = 3;
          } else {
              headerSize = 4;
          }

          Assert(conn->hciBufferLen > headerSize - 1);

          if (conn->hciPacketLen + headerSize > conn->hciBufferLen) {

              buffer = HCI_GetRxPtr(conn->hciBuffer);
          
              dataLen = recv(S, buffer + conn->hciBufferLen, 
                  conn->hciPacketLen - conn->hciBufferLen + headerSize, 0);
          
              if (dataLen == SOCKET_ERROR) {
                  if (WSAGetLastError() != WSAEWOULDBLOCK)
                      Report(("HC: DataInd() Recv failed reason %s.\n",WSA_LastError()));
              
                  //          Assert(WSAGetLastError() == WSAEWOULDBLOCK);
                  break;
              }
          
              conn->hciBufferLen += dataLen;
          }
      }
      
        /* If the received len == buffer len in header, were done */
        if (conn->hciBufferLen == conn->hciPacketLen + headerSize) {

            /* Increment number of ACL/SCO packets outstanding,
             * in case we are tracking this for controller-to-host flow 
             */
            if ((conn->hciBuffer->flags & HCI_BUFTYPE_ACL_DATA) &&
                (IHC(flags) & IHCF_CTRLR_FLOW_ACL)) {
                if (IHC(hostCurAclPackets) >= IHC(hostMaxAclPackets)) {
                    Report(("HC: Holding up received ACL packet due to flow control.\n"));
                    Wakeup = S;
                    return;
                }

                /* Increment both global and local outstanding packet count */
                IHC(hostCurAclPackets)++;
                conn->hostCurAclPackets++;

            } else if ((conn->hciBuffer->flags & HCI_BUFTYPE_SCO_DATA) &&
                (IHC(flags) & IHCF_CTRLR_FLOW_SCO)) {
                if (IHC(hostCurScoPackets) >= IHC(hostMaxScoPackets)) {
                    Report(("HC: Holding up received SCO packet due to flow control.\n"));
                    Wakeup = S;
                    return;
                }

#if NUM_SCO_CONNS > 0
                /* Increment both global and local outstanding packet count */
                IHC(hostCurScoPackets)++;
                sco = FindScoLinkbyHandle(conn->stage[3]);
                Assert(sco);
                sco->hostCurScoPackets++;
#endif /* NUM_SCO_CONS */
            }

            /* Done receiving, Indicate the buffer to the HCI */
            Report(("HC: Indicating %d byte unicast packet.\n", conn->hciBufferLen));
            HCI_RxBufferInd(conn->hciBuffer, BT_STATUS_SUCCESS);

            conn->hciBuffer = 0;
        }
        else Assert(conn->hciBufferLen < conn->hciPacketLen+headerSize);
    }
   
    return;
}

#if NUM_SCO_CONNS > 0
/* ---------------------------------------------------------------------------
 * FindScoLinkbyHandle
 * 
 */
ScoConn *FindScoLinkbyHandle(U8 ScoHandle)
{
    I8  i;

    for (i = 0; i < NUM_SCO_CONNS; i++) {
        if (IHC(scoTab)[i].handle == ScoHandle) {
            Assert(IHC(scoTab)[i].state == SCO_ACTIVE);
            return &IHC(scoTab)[i];
        }
    }
    return 0;
}
#endif

/* ---------------------------------------------------------------------------
 * HC_PostEvent
 * 
 */
void HC_PostEvent(U8 Event, U8 *EventData, U8 EventLen)
{
    RxBuffHandle    rxBuffHandle;
    U8             *buffer;

    /* Indicate any queued events first */
    HC_ProcessEventOverrunQueue();

    if (HCI_GetEventBuffer(&rxBuffHandle, (U16)(EventLen+2)) != BT_STATUS_SUCCESS) {
        /* No Events currently available, queue this one */
        HandleEventOverrun(Event, EventData, EventLen);
        return;
    }
    buffer = HCI_GetRxPtr(rxBuffHandle);

    /* Indicate the current event */
    buffer[0] = Event;
    buffer[1] = EventLen;
    OS_MemCopy(buffer+2, EventData, EventLen);

    /* Pass the event buffer up to the Host */
    HCI_RxBufferInd(rxBuffHandle, BT_STATUS_SUCCESS);
    return;
}

/* ---------------------------------------------------------------------------
 * HC_HandleEventOverrun
 * 
 */
static void HandleEventOverrun(U8 Event, U8 *EventData, U8 EventLen)
{
    QueuedEvent     *event;

    event = (QueuedEvent *)malloc(sizeof(QueuedEvent)+EventLen);
    Assert(event);

    event->event = Event;
    event->eventLen = EventLen;
    OS_MemCopy(event->eventData, EventData, EventLen);

    InsertTailList(&IHC(eventQueue), (ListEntry *)event);
    return;
}

/* ---------------------------------------------------------------------------
 * ProcessEventOverrunQueue
 * 
 * Returns:
 *     TRUE if there are no outstanding events to be sent.
 * 
 *     FALSE if some events have not yet been sent.
 */
BOOL HC_ProcessEventOverrunQueue(void)
{
    QueuedEvent    *event;
    RxBuffHandle    rxBuffHandle;
    U8             *buffer;

    while (!IsListEmpty(&IHC(eventQueue))) {
        event = (QueuedEvent *)RemoveHeadList(&IHC(eventQueue));

        if (HCI_GetEventBuffer(&rxBuffHandle, (U16)(event->eventLen+2)) != BT_STATUS_SUCCESS) {
            /* Still no buffers, requeue the event */
            InsertHeadList(&IHC(eventQueue), (ListEntry *)event);
            return FALSE;
        }
        buffer = HCI_GetRxPtr(rxBuffHandle);

        buffer[0] = event->event;
        buffer[1] = event->eventLen;
        OS_MemCopy(buffer+2, event->eventData, event->eventLen);

        /* Pass the event buffer up to the Host */
        HCI_RxBufferInd(rxBuffHandle, BT_STATUS_SUCCESS);

        free(event);
    }
    return TRUE;
}

/* ---------------------------------------------------------------------------
 * HC_CloseLink
 * 
 */
void HC_CloseLink(BtConn *Conn)
{
    I16  i;

    closesocket(Conn->link);

    Conn->link = INVALID_SOCKET;
    Conn->state = LINK_FREE;
    Conn->packetsSent = 0;

    /* Check if all the links are closed. If so then clear the broadcast
     * packets Sent counts. We don't want to indicate a packets sent event
     * with no links up.
     */
    for (i = 0; i < NUM_BT_HANDLES; i++) {
        if (IHC(connTab)[i].link != INVALID_SOCKET) {
            return;
        }
    }
    /* we're completely shut down */
    IHC(connTab)[CONN_ACTIVE_BCAST].packetsSent = 0;
    IHC(connTab)[CONN_PICONET_BCAST].packetsSent = 0;
}

/* ---------------------------------------------------------------------------
 * HC_Send
 * 
 */
BOOL HC_Send(SOCKET Skt, const char *buf, int len, int flags)
{
   int   rcode;

   while (len > 0) {

      rcode = send(Skt, buf, len, flags);
      if (rcode == SOCKET_ERROR) {
         Report(("HC: Send() Failed, reason %s.\n",WSA_LastError()));
         /* Don't know why occasionally I get these errors that aren't even
          * in the WSA... table. But if I ignore them things still work fine.
          */
         if ((WSAGetLastError() == WSAEWOULDBLOCK) ||
             (WSAGetLastError() < WSABASEERR)) {

            Sleep(500);
            continue;
         }

         /* This can happen if the link goes down while the HCI has
          * transmit data queue'd, because it may try to send the data
          * before it realizes the link is down.
          */
         return FALSE;
      }
      len -= rcode;
      buf += rcode;
   }

   return TRUE;
}

/* ---------------------------------------------------------------------------
 * HC_SendTo
 *
 * Always sends from (on) the Inquiry Socket.
 */
BOOL HC_SendTo(const char *Buffer, int BufferLen, U32 ToAddr, U16 ToPort)
{
   SOCKADDR_IN target = {AF_INET, 0, 0, 0};
   int         rcode;

   Assert(IHC(inquirySkt) != INVALID_SOCKET);

   target.sin_addr.s_addr = ToAddr;
   target.sin_port = ToPort;

   rcode = sendto(IHC(inquirySkt), Buffer, BufferLen, 0, 
                  (struct sockaddr *)&target, sizeof(SOCKADDR_IN));

   if (rcode == SOCKET_ERROR) {
       /* Failure is not critical */
       Report(("HC: SendTo() Failed, reason %s\n",WSA_LastError()));
       return FALSE;
   }
   return TRUE;
}

/* ---------------------------------------------------------------------------
 * HC_SendLmp
 *
 * Works just like HC_Send() plus it dumps the packet to the sniffer. 
 * Only for LMP packets.
 */
BOOL HC_SendLmp(SOCKET Skt, const char *buf, int len, int flags)
{
#if XA_DEBUG == XA_ENABLED && HCI_ALLOW_PRESCAN == XA_ENABLED && XA_SNIFFER == XA_ENABLED
    DumpLmpPacket("TX", buf, (U16)len);
#endif
    return HC_Send(Skt, buf, len, flags);
}


/* ---------------------------------------------------------------------------
 * HC_WndProc
 * 
 */
LRESULT CALLBACK HC_WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    static BOOL busy = 0;

    /* Only call StopHardware() if the message is one that will 
     * cause us to enter the HC.
     */
    if ((uMsg == WM_SELECT) || (uMsg == WM_HC_EVENT) || (uMsg == WM_TIMER)) {
        OS_StopHardware();

        if (busy > 0) {
            if (busy == 1) {
                /* The usual case for this message is that an Assertion was hit in code
                 * that runs off this thread (hostctlr.c or sniffer.c). In this case it
                 * is not a problem. If this message shows up in other cases it's a concern.
                 */
                Report(("HC: ** WARNING ** Ignoring messages because WndProc is BUSY!\n"));
                busy++;
            }
            OS_ResumeHardware();
            return 0;
        }
        busy = 1;
    }

    switch (uMsg) {

    case WM_SELECT:
        Assert(busy);
        switch (WSAGETSELECTEVENT(lParam)) {

        case FD_ACCEPT:
            Report(("HC: WndProc() Socket %x received message: ", wParam));
            if (WSAGETSELECTERROR( lParam ) == 0) {
                /* Connect Indication */
                Report(("Accept.\n"));
                HC_ConnectRequest();
            }
            else {
                Report(("Accept failed %s.\n", WSA_Error(WSAGETSELECTERROR(lParam))));
            }
            break;

        case FD_READ:
            //Report(("HC: WndProc() Socket %x received message: ", wParam));
            if (WSAGETSELECTERROR( lParam ) == 0) {
                /* receive data ... */
                //Report(("Read.\n"));
                if (wParam == IHC(inquirySkt) || wParam == IHC(inquiryScanSkt))
                    HC_ProcessBroadcast(wParam);
                else HC_DataInd(wParam);
            }
            else Report(("Read failed %s.\n", WSA_Error(WSAGETSELECTERROR(lParam))));
            break;

        case FD_WRITE:
            Report(("HC: WndProc() Socket %x received message: ", wParam));
            if (WSAGETSELECTERROR( lParam ) == 0) {
                /* Connection established */
                Report(("Write (Conn Accepted).\n"));
                HC_ConnectStatus(wParam, TRUE);
            } else {
                /* Connection failed */
                Report(("Write (Conn Accept failed %s).\n", WSA_Error(WSAGETSELECTERROR(lParam))));
                HC_ConnectStatus(wParam, FALSE);
            }       
            break;
            
        case FD_CONNECT:
            Report(("HC: WndProc() Socket %x received message: ", wParam));
            if (WSAGETSELECTERROR( lParam ) == 0) {
                Report(("Connected.\n"));
                /* Connection established, we ignore this event waiting for
                 * the FD_WRITE event when the other side completes the conn.
                 */
                // HC_ConnectStatus(wParam, TRUE);
            } else {
                Report(("Connect failed %s.\n", WSA_Error(WSAGETSELECTERROR(lParam))));
                /* Connection failed */
                HC_ConnectStatus(wParam, FALSE);
            }       
            break;
            
        case FD_CLOSE:
            Report(("HC: WndProc() Socket %x received message: ", wParam));
            Report(("Close.\n"));
            HC_DisconnectInd(wParam);
            break;

        default:
            Assert(0);
            break;
        }
        break;

    case WM_HC_EVENT:
        Assert(busy);
        //Report(("WndProc: Received WM HC Event msg.\n"));
        HC_PostEvent((U8)wParam, (U8 *)lParam, (U8)(wParam >> 8));
        free((void *)lParam);
        break;

    case WM_HC_QUIT:
        if (!DestroyWindow(IHC(driverWnd)))
            Report(("HC: Window close failed: %d\n", GetLastError()));
        else IHC(driverWnd) = 0;
        break;

    case WM_TIMER:
        //Report(("WndProc: Received WM Timer msg.\n"));
        Assert(busy);
        HC_Clock();
        break;

    case WM_CREATE:
        //Report(("WndProc: Received WM Create msg.\n"));
        break;

    case WM_DESTROY:
        //Report(("WndProc: Received WM Destroy msg.\n"));
        break;

    default:
        //Report(("DriverWnd: Window Received UNKNOWN msg %#4.4x.\n", uMsg));
        return DefWindowProc(hWnd, uMsg, wParam, lParam);
    }

    if (busy > 0) {
        busy = 0;
        OS_ResumeHardware();
    }
    return 0;
}


#if XA_DEBUG_PRINT == XA_ENABLED
/* ---------------------------------------------------------------------------
 * WSA_LastError
 * 
 */
static char *WSA_LastError(void)
{
   return ( WSA_Error(WSAGetLastError()) );
}

/* ---------------------------------------------------------------------------
 * WSA_Error
 * 
 */
static char *WSA_Error(int error)
{
   switch (error) {
      case WSAEINTR: return "WSAEINTR";
      case WSAEBADF: return "WSAEBADF";
      case WSAEACCES: return "WSAEACCES";
      case WSAEFAULT: return "WSAEFAULT";
      case WSAEINVAL: return "WSAEINVAL";
      case WSAEMFILE: return "WSAEMFILE";
      case WSAEWOULDBLOCK: return "WSAEWOULDBLOCK";
      case WSAEINPROGRESS: return "WSAEINPROGRESS";
      case WSAEALREADY: return "WSAEALREADY";
      case WSAENOTSOCK: return "WSAENOTSOCK";
      case WSAEDESTADDRREQ: return "WSAEDESTADDRREQ";
      case WSAEMSGSIZE: return "WSAEMSGSIZE";
      case WSAEPROTOTYPE: return "WSAEPROTOTYPE";
      case WSAENOPROTOOPT: return "WSAENOPROTOOPT";
      case WSAEPROTONOSUPPORT: return "WSAEPROTONOSUPPORT";
      case WSAESOCKTNOSUPPORT: return "WSAESOCKTNOSUPPORT";
      case WSAEOPNOTSUPP: return "WSAEOPNOTSUPP";
      case WSAEPFNOSUPPORT: return "WSAEPFNOSUPPORT";
      case WSAEAFNOSUPPORT: return "WSAEAFNOSUPPORT";
      case WSAEADDRINUSE: return "WSAEADDRINUSE";
      case WSAEADDRNOTAVAIL: return "WSAEADDRNOTAVAIL";
      case WSAENETDOWN: return "WSAENETDOWN";
      case WSAENETUNREACH: return "WSAENETUNREACH";
      case WSAENETRESET: return "WSAENETRESET";
      case WSAECONNABORTED: return "WSAECONNABORTED";
      case WSAECONNRESET: return "WSAECONNRESET";
      case WSAENOBUFS: return "WSAENOBUFS";
      case WSAEISCONN: return "WSAEISCONN";
      case WSAENOTCONN: return "WSAENOTCONN";
      case WSAESHUTDOWN: return "WSAESHUTDOWN";
      case WSAETOOMANYREFS: return "WSAETOOMANYREFS";
      case WSAETIMEDOUT: return "WSAETIMEDOUT";
      case WSAECONNREFUSED: return "WSAECONNREFUSED";
      case WSAELOOP: return "WSAELOOP";
      case WSAENAMETOOLONG: return "WSAENAMETOOLONG";
      case WSAEHOSTDOWN: return "WSAEHOSTDOWN";
      case WSAEHOSTUNREACH: return "WSAEHOSTUNREACH";
      case WSAENOTEMPTY: return "WSAENOTEMPTY";
      case WSAEPROCLIM: return "WSAEPROCLIM";
      case WSAEUSERS: return "WSAUSERS";
      case WSAEDQUOT: return "WSAEDQUOT";
      case WSAESTALE: return "WSAESTALE";
      case WSAEREMOTE: return "WSAEREMOTE";
      case WSASYSNOTREADY: return "WSASYSNOTREADY";
      case WSAVERNOTSUPPORTED: return "WSAVERNOTSUPPORTED";
      case WSANOTINITIALISED: return "WSANOTINITIALISED";
      case WSAEDISCON: return "WSAEDISCON";
   }
   return "Unknown Error";
}
#endif /* XA_DEBUG_PRINT */

