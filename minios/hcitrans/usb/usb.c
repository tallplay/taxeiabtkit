/***************************************************************************
 *
 * File:
 *     $Workfile:usb.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:63$
 *
 * Description:
 *     HCI Transport for USB.
 *     This file lies between the HCI Driver and a controller with a USB
 *     interface.
 *      
 *     Implementations may be device-unique in some ways. For example, 
 *     One manufacturer may provide a symbolic name while another may
 *     require that PnP calls be made to locate the device.
 *       
 *     RADIO_TYPE_SIW
 *     Tested with Silicon Wave System and drivers.
 *     Hardware: WDS Rev. 3.0
 *     Firmware: 
 *     Driver: ncusb.sys
 *       Provider: Silicon Wave 
 *       Date: 08/29/2001
 *       Windows 98 only
 *
 *     RADIO_TYPE_CSR
 *       This code was tested on Alps Bluetooth modules which use CSR
 *       radios and drivers. Details below:
 *       Alps BT modules P/N: UGTZ4XA22A
 *       Firmware version:    10.2
 *       USB driver:
 *      Provider:           Cambridge Silicon Radio
 *        File name:            CSRBC01.SYS
 *      Version:            1.3.0.0
 *           File Version:        5.00.2001.802
 *
 * Copyright 1999-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

/* Made the USB receive buffer size variable to try 
   work around apparent bug in CSRBC01.SYS driver. Default size is
   64, but larger buffer recommended for CSR.
 */
#define BULKBUFSIZE                1024

#include "btalloc.h"
#include "windows.h"
#include "setupapi.h"
#include "hcitrans.h"
#include "parseopts.h"
#include "usb.h"
#include "sys/usbtrans.h"
#include "winioctl.h"


/*
 * CSR (Casira only?) have a problem with the HCI RESET command. The unit
 * never recovers. Defining CSR_RESET_PROBLEM enables a workaround. This
 * workaround is not necessary when using ESI Bluetooth dongles.
 */
 //#define CSR_RESET_PROBLEM 

/* UsbReport controls verboseness */
#define UsbReport(S)
/* #define UsbReport        Report */

/* Defines for DeviceIoControl() access to SiW's control endpoint */
#define I82930_IOCTL_INDEX  0x0000 

#define IOCTL_NCUSB_CONTROL_WRITE_SETUP  CTL_CODE(FILE_DEVICE_UNKNOWN,  \
                                                  I82930_IOCTL_INDEX+16,\
                                                  METHOD_BUFFERED,  \
                                                  FILE_ANY_ACCESS) 

#define IOCTL_NCUSB_CONTROL_WRITE_DATA  CTL_CODE(FILE_DEVICE_UNKNOWN,  \
                                                 I82930_IOCTL_INDEX+17,\
                                                 METHOD_BUFFERED,  \
                                                 FILE_ANY_ACCESS) 


/* Defines for DeviceIoControl() access to Alps' control endpoint */
#define CSRBC01_IOCTL_INDEX  0x0000

#define IOCTL_CSRBC01_SEND_HCI_COMMAND          CTL_CODE(FILE_DEVICE_UNKNOWN, \
                                                CSRBC01_IOCTL_INDEX, \
                                                METHOD_OUT_DIRECT, \
                                                FILE_ANY_ACCESS)

#define IOCTL_CSRBC01_GET_HCI_EVENT             CTL_CODE(FILE_DEVICE_UNKNOWN, \
                                                CSRBC01_IOCTL_INDEX+1, \
                                                METHOD_BUFFERED, \
                                                FILE_ANY_ACCESS)

#define IOCTL_CSRBC01_GET_VERSION               CTL_CODE(FILE_DEVICE_UNKNOWN, \
                                                CSRBC01_IOCTL_INDEX+2, \
                                                METHOD_BUFFERED, \
                                                FILE_ANY_ACCESS)

#define IOCTL_CSRBC01_GET_DRIVER_NAME           CTL_CODE(FILE_DEVICE_UNKNOWN, \
                                                CSRBC01_IOCTL_INDEX+3, \
                                                METHOD_BUFFERED, \
                                                FILE_ANY_ACCESS)

#define IOCTL_CSRBC01_GET_CONFIG_DESCRIPTOR     CTL_CODE(FILE_DEVICE_UNKNOWN,  \
                                                CSRBC01_IOCTL_INDEX+4,\
                                                METHOD_BUFFERED,  \
                                                FILE_ANY_ACCESS)

#define IOCTL_CSRBC01_GET_DEVICE_DESCRIPTOR     CTL_CODE(FILE_DEVICE_UNKNOWN,  \
                                                CSRBC01_IOCTL_INDEX+5,\
                                                METHOD_BUFFERED,  \
                                                FILE_ANY_ACCESS)

#define IOCTL_CSRBC01_RESET_DEVICE              CTL_CODE(FILE_DEVICE_UNKNOWN,  \
                                                CSRBC01_IOCTL_INDEX+6,\
                                                METHOD_BUFFERED,  \
                                                FILE_ANY_ACCESS)                                                              

#define IOCTL_CSRBC01_RESET_PIPE                CTL_CODE(FILE_DEVICE_UNKNOWN,  \
                                                CSRBC01_IOCTL_INDEX+7,\
                                                METHOD_BUFFERED,  \
                                                FILE_ANY_ACCESS)                                                           

#define IOCTL_CSRBC01_BLOCK_HCI_EVENT           CTL_CODE(FILE_DEVICE_UNKNOWN, \
                                                CSRBC01_IOCTL_INDEX+12, \
                                                METHOD_BUFFERED, \
                                                FILE_ANY_ACCESS)

#define IOCTL_CSRBC01_BLOCK_HCI_DATA            CTL_CODE(FILE_DEVICE_UNKNOWN, \
                                                CSRBC01_IOCTL_INDEX+13, \
                                                METHOD_BUFFERED, \
                                                FILE_ANY_ACCESS)

#define IOCTL_CSRBC01_SEND_CONTROL_TRANSFER     CTL_CODE(FILE_DEVICE_UNKNOWN, \
                                                CSRBC01_IOCTL_INDEX+14,\
                                                METHOD_BUFFERED, \
                                                FILE_ANY_ACCESS)

#define IOCTL_CSRBC01_SELECT_ALTERNATE_INTERFACE CTL_CODE(FILE_DEVICE_UNKNOWN, \
                                                CSRBC01_IOCTL_INDEX+15, \
                                                METHOD_BUFFERED, \
                                                FILE_ANY_ACCESS)

#define IOCTL_CSRBC01_START_SCO_DATA            CTL_CODE(FILE_DEVICE_UNKNOWN, \
                                                CSRBC01_IOCTL_INDEX+16, \
                                                METHOD_BUFFERED, \
                                                FILE_ANY_ACCESS)

#define IOCTL_CSRBC01_SEND_SCO_DATA             CTL_CODE(FILE_DEVICE_UNKNOWN, \
                                                CSRBC01_IOCTL_INDEX+17, \
                                                METHOD_BUFFERED, \
                                                FILE_ANY_ACCESS)

#define IOCTL_CSRBC01_RECV_SCO_DATA             CTL_CODE(FILE_DEVICE_UNKNOWN, \
                                                CSRBC01_IOCTL_INDEX+18, \
                                                METHOD_BUFFERED, \
                                                FILE_ANY_ACCESS)

/*---------------------------------------------------------------------------
 * UsbPipe structure
 *
 *     A structure used to manage each USB data pipe. 
 */
typedef struct _UsbPipe {
    HANDLE        handle;
    CONST char    *name;               /* Pipe name, "PIPE00", e.g. */
    UsbEndPoint   type;                /* control, interrupt, bulk, iso */
    BOOL          notify;              /* call callback() on event */
    UsbCallback   callback;
    OVERLAPPED    ovrLap;              /* For overlapped IO */
    U8            *inBuf;              /* For overlapped read */
    U16           inBufLen;            /* size of inBuf */
    U32           readLen;             /* Length of data in readBuffer */
    U32           readPos;             /* Current position in readBuffer */
    BOOL          triggerArmed;        /* TRUE when waiting for a signal */
    BOOL          signalPending;       /* TRUE when an event is signalled but
                                          cannot be serviced. */
} UsbPipe;


typedef unsigned char FAR * physAddr;   /* physical addresses */


/****************************************************************************
 *
 * Global Data
 *
 ***************************************************************************/

static U16 radioType;

/* An array of usb endpoint offsets that indicates the correct priority
 * order for handling read/writes. Total number of entries must equal
 * MAX_PIPES.
 *
 * Writes are to be handled BEFORE reads so that the stack does not
 * get confused in the case where a USB driver delivers a read event
 * very very quickly.
 */
static UsbEndPoint notifyOrder[] = {
    USB_CONTROL_WRITE,
    USB_BULK_WRITE,     
    USB_INTERRUPT_READ,
#if (BT_SCO_HCI_DATA == XA_ENABLED) && (NUM_SCO_CONNS > 0)
    USB_BULK_READ,
    USB_ISO_WRITE,
    USB_ISO_READ,
#else
    USB_BULK_READ
#endif
};

/*
 * Device-unique names given to endpoints. The order must correspond to the
 * enumerated UsbEndPoint in trans.h and also match the order of the pipes
 * according to the USB hardware manufacturer. The number of endpoints must
 * be >= MAX_PIPES. That is, if BT_SCO_HCI_DATA is enabled, then there must
 * be 6 endpoints.
 */
#if 1 //tallplay
static CONST char *siwEndpointName[] = {
    "",                       /* control */
    "\\PIPE00",               /* interrupt */
    "\\PIPE01",               /* bulk read */
#if (BT_SCO_HCI_DATA == XA_ENABLED) && (NUM_SCO_CONNS > 0)
    "\\PIPE02",               /* bulk write */
    "\\PIPE03",               /* ISO read - NOT SUPPORTED ON SIW */
    "\\PIPE04"                /* ISO write - NOT SUPPORTED ON SIW */
#else
    "\\PIPE02"                /* bulk write */
#endif
};
#else
static CONST char *siwEndpointName[] = {
    "",                       /* control */
    "",               /* interrupt */
    "",               /* bulk read */
#if (BT_SCO_HCI_DATA == XA_ENABLED) && (NUM_SCO_CONNS > 0)
    "",               /* bulk write */
    "",               /* ISO read - NOT SUPPORTED ON SIW */
    ""                /* ISO write - NOT SUPPORTED ON SIW */
#else
    ""                /* bulk write */
#endif
};
#endif

/* Alps driver just needs the devic name "\\.\CSR0", not pipe names. */
static CONST char *alpsEndpointName[] = {
    "",                     /* control */
    "",                     /* interrupt */
    "",                     /* bulk read */
#if (BT_SCO_HCI_DATA == XA_ENABLED) && (NUM_SCO_CONNS > 0)
    "",                     /* bulk write */
    "",                     /* ISO read - NOT SUPPORTED ON CSR */
    ""                      /* ISO write - NOT SUPPORTED ON CSR */
#else
    ""                      /* bulk write */
#endif
};

static CONST char **endpointName;

/* Generated from the GUID registered by the driver itself */
char completedevName[256] = "";  

/* Indicates whether the port and thread are active */
static BOOL         isActive = FALSE;

static UsbPipe      usbPipe[MAX_PIPES];

/* Input pipes require a buffer. Note that these are only to cover latency
 * in servicing a read event rather than a buffer for max packet size. That is,
 * ReadFile() causes an event to be sent up when data arrives. The event in turn
 * results in reading the endpoint into an stack-supplied buffer. The sizes 
 * below could be empirically adjusted.
 */
static U8 interruptBuf[16];         /* Alps requires 16 bytes exactly! */
static U8 bulkBuf[BULKBUFSIZE];

#if (BT_SCO_HCI_DATA == XA_ENABLED) && (NUM_SCO_CONNS > 0)
static U8 isoBuf[64];
#endif

#ifdef CSR_RESET_PROBLEM
static BOOL readingReset;
#endif

HANDLE usbThread;

/*---------------------------------------------------------------------------
 * Event Handle.
 *
 *     Define handles for internal CSR-USB event handling.
 */
static HANDLE   readHciEvntDone;
static HANDLE   readAclDataDone;
static HANDLE   eventThreadSignal;

/****************************************************************************
 *
 * Function Prototypes
 *
 ***************************************************************************/

static DWORD FAR PASCAL eventThread(UsbPipe *pipe);
static BOOL pipeClose(UsbPipe *pipe);
static void closeUsbDevice(void);
static BOOL pipeOpen(char *baseName, UsbPipe *pipe, UsbEndPoint type);
static BOOL controlWrite(UsbPipe *pipe, const UCHAR * const buf, UINT len); 
static void pipeRead(UsbPipe *pipe);
static BOOL eventAsyncRead(UsbPipe *pipe);
static BOOL aclAsyncRead(UsbPipe *pipe);
static void initCsrInputPipe(UsbPipe *pipe);
static void wakeupEventThread();
#ifdef CSR_RESET_PROBLEM
static void resetCsr(char *devName);
static void fakeResetCmd(UsbPipe *);
#endif



/*---------------------------------------------------------------------------
 * USB_Init() 
 *      Initialize the USB interface.
 *      
 * Requires:
 * Parameters:
 * Returns:
 *      BtStatus
 */
BOOL NEAR USB_Init( void )
{
    int i;
    char *devRoot;
    char devName[200];  
    DWORD dwThreadID;

    radioType = getRadioType();

    /* Device base name 
     *      e.g., \\.\MYUSBDEV\
     */
    switch (radioType) {
    case RADIO_TYPE_SIW_USB:
        devRoot = "NCUSB-";
        devRoot = "CSR";
        endpointName = siwEndpointName;
        Assert(((sizeof(siwEndpointName) / sizeof(*siwEndpointName)) >= MAX_PIPES));
        break;
    
    case RADIO_TYPE_CSR_USB:
        devRoot = "CSR";
        
        endpointName = alpsEndpointName;
        Assert(((sizeof(alpsEndpointName) / sizeof(*alpsEndpointName)) >= MAX_PIPES));
        break;
    
    default:
        MessageBox(NULL, "Radio type must be SIW_USB or CSR_USB.\nUse -r option (e.g. -rCSR_USB).", 
                   "USB_Init Failed", MB_OK);
        return FALSE;
    };

    wsprintf(devName, "\\\\.\\%s%d", devRoot, getPortNumOption() - 1);
///    wsprintf(devName, "\\\\.\\%s", devRoot, getPortNumOption() - 1);

    switch (radioType) {
    case RADIO_TYPE_SIW_USB:
        break;

    case RADIO_TYPE_CSR_USB:
#ifdef CSR_RESET_PROBLEM
        resetCsr(devName);
        readingReset = FALSE;
#endif
        break;
    };

    /* Open all the pipes */
    for (i = 0; i < MAX_PIPES; i++) {
        /* Ensure no callbacks are made before they are registered. */
        usbPipe[i].notify = FALSE;
        /* Open this pipe. */
        if (! pipeOpen(devName, &usbPipe[i], (UsbEndPoint)i)) {
            return FALSE;
        }
    }

    /* Add a special event used to wake up the event thread */
    eventThreadSignal = CreateEvent(NULL, TRUE, FALSE, NULL);
    if (!eventThreadSignal) return FALSE;


    /* When the last pipe has been opened, create our thread */
    isActive = TRUE;

    if (! (usbThread = CreateThread((LPSECURITY_ATTRIBUTES)NULL, 
                       0,
                       (LPTHREAD_START_ROUTINE)eventThread,
                       (LPVOID)usbPipe,
                       0, &dwThreadID))) {
        return FALSE;
    }

    switch (radioType) {
    case RADIO_TYPE_SIW_USB:
        /* Start an asynchronous read for input pipes. */
        pipeRead(&usbPipe[USB_INTERRUPT_READ]);
        pipeRead(&usbPipe[USB_BULK_READ]);
#if (BT_SCO_HCI_DATA == XA_ENABLED) && (NUM_SCO_CONNS > 0)
        pipeRead(&usbPipe[USB_ISO_READ]);
#endif
        break;

    case RADIO_TYPE_CSR_USB:
        /* Initialise read pipes. */
        initCsrInputPipe(&usbPipe[USB_INTERRUPT_READ]);
        initCsrInputPipe(&usbPipe[USB_BULK_READ]);
        break;
    };

    return TRUE;
} /* USB_Init() */


/*---------------------------------------------------------------------------
 * USB_Deinit() 
 *      Deinit the USB interface returning it to a benign state.
 *      
 * Requires:
 * Parameters:
 * Returns:
 */
void NEAR USB_Deinit(void)
{
    /* If not already active, ignore */
    if (isActive == FALSE) {
        return;
    }

    /* Tell the event thread to close, then destroy the signal */
    isActive = FALSE;
    wakeupEventThread();
    WaitForMultipleObjects(1, &eventThreadSignal, FALSE, INFINITE);
    CloseHandle(eventThreadSignal);
    eventThreadSignal = NULL;

    closeUsbDevice();
} /* End of DeinitCom() */


/*---------------------------------------------------------------------------
 * USB_Read() 
 *  Read a block of data from the USB port. The caller will continue
 *  calling this function until we return less than the number of bytes
 *  the caller expects to read or the caller has no more buffers.
 *
 *  This is touchy with Alps. Here are some scenarios:
 *      1. No data in our buffer.
 *      2. Data is already in our buffer.
 *      3. Caller quits calling because it can't get a buffer.
 *
 *  Note that we as long as we have data in our buffer, we cannot call
 *  setTriggerForHciEvent().     
 *
 * Requires:
 *     
 * Parameters:
 *        type   - type of pipe to read
 *      buffer - allocated buffer in which to receive data
 *      length - max bytes to read
 *  Return: 
 *      number of bytes read into buffer
 */
int NEAR USB_Read(UsbEndPoint type, U8 *buffer, I16 length)
{
    DWORD       bytesRead = 0;
    DWORD       error;
    UsbPipe    *pipe;

    Assert(type < MAX_PIPES);

    pipe = &usbPipe[type];

    switch (radioType) {
    case RADIO_TYPE_SIW_USB:
        if (pipe->readPos == pipe->readLen) {
            /* There have been no bytes read */
            if (GetOverlappedResult(pipe->handle, &pipe->ovrLap,
                                    &pipe->readLen, FALSE) == FALSE) {
                /* and there are none pending */
                error = GetLastError();
                break;
            }
        }

        bytesRead = min(length, pipe->readLen - pipe->readPos);
        memcpy(buffer, pipe->inBuf + pipe->readPos, bytesRead);
        pipe->readPos += bytesRead;

        if (pipe->readPos == pipe->readLen) {
            pipeRead(pipe);
        }
        break;

    case RADIO_TYPE_CSR_USB:
        switch (type) {
        
        case USB_INTERRUPT_READ:
#ifdef CSR_RESET_PROBLEM
            if (readingReset) {
                UsbReport(("CSRUSB: Sending Command Complete event for reset\n"));
                readingReset = FALSE;
                pipe->inBuf[0] = HCE_COMMAND_COMPLETE;
                pipe->inBuf[1] = 4;     /* len */
                pipe->inBuf[2] = 1;
                StoreLE16(&pipe->inBuf[3], HCC_RESET);
                pipe->inBuf[5] = 0;
                pipe->readLen += 6;
            } else
#endif
            if (pipe->triggerArmed && (pipe->readLen == pipe->readPos)) {
                /* The event is signalled but we have not read data. */
                pipe->triggerArmed = FALSE;
                pipeRead(pipe);
            }

            /* Copy to caller's buffer and advance pointers. */
            bytesRead = min(length, pipe->readLen - pipe->readPos);
            Assert(bytesRead <= BULKBUFSIZE); 
            memcpy(buffer, pipe->inBuf + pipe->readPos, bytesRead);
            pipe->readPos += bytesRead; 

            if ((bytesRead < length) && (pipe->readPos == pipe->readLen)) {
                /* We have less than the number requested, set our event trigger. */
                eventAsyncRead(pipe);
            }
            UsbReport(("USB: intRead: req %d, ret %d bytes\n", length, bytesRead));
            break;


        case USB_BULK_READ:
            if (pipe->triggerArmed && (pipe->readPos == pipe->readLen)) {
                /* The event is signalled but we have not read data. */
                pipe->triggerArmed = FALSE;
                pipeRead(pipe);
            }

            /* Copy to caller's buffer and advance pointers. */
            bytesRead = min(length, pipe->readLen - pipe->readPos);
            Assert(bytesRead <= BULKBUFSIZE); 
            memcpy(buffer, pipe->inBuf + pipe->readPos, bytesRead);
            pipe->readPos += bytesRead; 

            if ((bytesRead < length) && (pipe->readPos == pipe->readLen)) {
                /* We have less than the number requested, set our event trigger. */
                aclAsyncRead(pipe);
            }
            UsbReport(("USB: bulkRead: req %d, ret %d bytes\n", length, bytesRead));
            break;

        default:
            Assert((type == USB_INTERRUPT_READ) || (type == USB_BULK_READ));
            return 0;

        }
        break;
    };

    return bytesRead;
} /* end of USB_Read() */


/*---------------------------------------------------------------------------
 * USB_Write() 
 *      Write a block of data to a USB pipe.
 *
 * Requires:
 *     
 * Parameters:
 *        type     -   type of pipe to write
 *      buffer  -   buffer to write out
 *      length  -   # bytes in buffer
 *      notify  -   NOTIFY (call EventHandler on write completion)
 *                  NO_NOTIFY (do not notify on completion)
 * Returns:
 *      number of bytes written
 */
DWORD NEAR USB_Write(UsbEndPoint type, U8 *buffer , DWORD length, U8 notify)
{
    DWORD       bytesWritten = 0;
    DWORD       errorFlags;
    DWORD       error = 0;
    COMSTAT     comStat;
    UsbPipe     *pipe;

    Assert(type < MAX_PIPES);

    pipe = &usbPipe[type];

    pipe->notify = notify;

#ifdef CSR_RESET_PROBLEM
    if ((LEtoHost16(buffer) == HCC_RESET) && (radioType == RADIO_TYPE_CSR_USB)) {
        fakeResetCmd(pipe);
        return length;
    }
#endif

    if (type == USB_CONTROL_WRITE) {
        if (controlWrite(pipe, buffer, length)) {
            UsbReport(("USB: ctrlWrite: req %d, ret %d bytes\n", length, bytesWritten));
            bytesWritten = length;
        }
    } else {

        /* Write the data asynchronously. WriteFile may return before completion. */
        if (! WriteFile(pipe->handle, buffer, length, &bytesWritten, 
                        &pipe->ovrLap)) {
            UsbReport(("USB: bulkWrite: req %d, ret %d bytes\n", length, bytesWritten));

            if ((error = GetLastError()) == ERROR_IO_PENDING) {
                GetOverlappedResult(pipe->handle, &pipe->ovrLap, &bytesWritten, TRUE);
            } else {
                SetEvent(pipe->ovrLap.hEvent);
            }

        } else {
            /* A write error has occurred */
            ClearCommError(pipe->handle, &errorFlags, &comStat);
        }
    }

    return bytesWritten;
} /* End of USB_Write() */



/*---------------------------------------------------------------------------
 * USB_RegisterCallback() 
 *      
 * Requires:
 *     
 * Parameters:
 *        type     -   Type of USB pipe to register with
 *        func    -     Callback function
 *      notify  -   TRUE/FALSE: notify callback on event
 *
 * Returns:
 */
void USB_RegisterCallback(UsbEndPoint type, UsbCallback func, BOOL notify)
{
    Assert(type < MAX_PIPES);
    Assert(func != NULL);

    usbPipe[type].callback = func;
    usbPipe[type].notify   = notify;
}



/*---------------------------------------------------------------------------
 * pipeOpen() 
 *      Open the USB pipe.
 * Requires:
 *     
 * Parameters:
 *      baseName device name base
 *      pipe     pointer to existing UsbPipe structure
 *      type     type of pipe to open
 *
 * Return:  
 *      TRUE    success
 *      FALSE   failed
 */
static BOOL NEAR pipeOpen(char *baseName, UsbPipe *pipe, UsbEndPoint type)
{
    char devName[256];  

    Assert(pipe);
    Assert(type < MAX_PIPES);

    /* Fill in pipe structure */
    pipe->handle    = 0;
    pipe->name      = endpointName[type];
    pipe->notify    = FALSE;
    pipe->type      = type;
    pipe->signalPending = FALSE;

    /* Input pipes need a buffer */
    if (type == USB_INTERRUPT_READ) {
        pipe->inBuf = interruptBuf;
        pipe->inBufLen = sizeof(interruptBuf) / sizeof(*interruptBuf);
    } else if (type == USB_BULK_READ) {
        pipe->inBuf = bulkBuf;
        pipe->inBufLen = sizeof(bulkBuf) / sizeof(*bulkBuf);
#if (BT_SCO_HCI_DATA == XA_ENABLED) && (NUM_SCO_CONNS > 0)
    } else if (type == USB_ISO_READ) {
        pipe->inBuf = isoBuf;
        pipe->inBufLen = sizeof(isoBuf) / sizeof(*isoBuf);
#endif
    }

    strcpy(devName, baseName);

    /* Append pipe name
     *      e.g., \\.\MYUSBDEV\PIPE00
     */
    strcat(devName, pipe->name);                      

    if (((radioType == RADIO_TYPE_CSR_USB) && (type == 0))
        || (radioType != RADIO_TYPE_CSR_USB)) {
        /* Open device read/write, no sharing, no inheritance, no template */
        pipe->handle = CreateFile(devName,
                                  GENERIC_WRITE | GENERIC_READ,
                                  0,            /* no sharing*/
                                  NULL,
                                  OPEN_EXISTING,
                                  FILE_FLAG_OVERLAPPED,
                                  NULL);

        if (pipe->handle == INVALID_HANDLE_VALUE) {
            Report(("Failed to open (%s) = %d\n", devName, GetLastError()));
            pipe->handle = 0;
            return FALSE;
        }
    } else if (radioType == RADIO_TYPE_CSR_USB) {
        /* Alps && type != 0
         * All pipes have the same handle. 
         */
        pipe->handle = usbPipe[0].handle;
    }

    /* Cancel any pending I/Os on this thread. */
    CancelIo(pipe->handle);

    /* Initialize the OVERLAPPED structures */
    memset(&pipe->ovrLap, 0, sizeof(OVERLAPPED));

    /* Create I/O event used for overlapped reads/writes */
    pipe->ovrLap.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
    if (pipe->ovrLap.hEvent == NULL) {
        return FALSE;
    }

    switch (radioType) {
    case RADIO_TYPE_SIW_USB:
        break;

    case RADIO_TYPE_CSR_USB:

        /* Reset this pipe, or at least try to. This does not appear to
         * reset any pending I/Os, even though the command seems to get
         * completed.
         */
        DeviceIoControl(pipe->handle,
                        IOCTL_CSRBC01_RESET_PIPE,
                        0, 0, 0, 0,
                        &pipe->readLen,
                        0);

        if (GetOverlappedResult(pipe->handle,
                                &pipe->ovrLap,
                                &pipe->readLen,
                                TRUE) == TRUE) {
            /* Successfully reset the pipe. Any other sanity
             * check code can go here.
             */
            pipe->readLen = 0;
            ResetEvent(pipe->ovrLap.hEvent);

            UsbReport(("\nPipe %d reset.\n", type));
        }

        /* Finally, do this... */
        if (type == (MAX_PIPES-1)) {
            /* Once pipes have been created, create two more 
             * Event structures for internal use.
             */
            readHciEvntDone = CreateEvent(NULL, TRUE, FALSE, NULL);
            readAclDataDone = CreateEvent(NULL, TRUE, FALSE, NULL);
        }
        break;
    };



    return TRUE;
}



/*---------------------------------------------------------------------------
 * initCsrInputPipe() 
 *
 * Initialise the read pipes, by clearing out all remnant data. 
 * Ideally this would be done through a pipe clear command, but this does
 * not appear to work. The reset command appears to do this, but it
 * also breaks the connection.
 */
static void initCsrInputPipe(UsbPipe *pipe)
{

    switch (pipe->type) {
    
    case USB_INTERRUPT_READ:
        /* Clear out all previous HCI Events. */
        do {
            pipeRead(&usbPipe[USB_INTERRUPT_READ]);
        } while (pipe->readLen);

        /* Set up to pend more events. */

        eventAsyncRead(&usbPipe[USB_INTERRUPT_READ]);
        break;

    case USB_BULK_READ:
        /* Clear out all previous ACL data. */
        do {
            pipeRead(&usbPipe[USB_BULK_READ]);
        } while (pipe->readLen);
        /* Set up to pend more data. */
        aclAsyncRead(&usbPipe[USB_BULK_READ]);
        break;

#if 0    /* USB CSR does not handle ISO data at this time. */
#if (BT_SCO_HCI_DATA == XA_ENABLED) && (NUM_SCO_CONNS > 0)
    case USB_BULK_READ:
        do {
            pipeRead(&usbPipe[USB_ISO_READ]);
        } while (pipe->readLen);
        break;
#endif
#endif

    default:
        Assert((pipe->type == USB_INTERRUPT_READ) || 
               (pipe->type == USB_BULK_READ));
        break;
    }

} /* initCsrInputPipe() */


/*---------------------------------------------------------------------------
 * pipeRead() 
 *
 *  Read from the pipe. We have been signalled that data is ready.
 */
static void pipeRead(UsbPipe *pipe)
{
    static OVERLAPPED ovrReadHciEvnt = {0, 0, 0, 0, 0};
    static OVERLAPPED ovrReadAclData = {0, 0, 0, 0, 0};
    ULONG written = 0x00;
    ULONG error = 0x00;
    BOOL rc = FALSE;

    /* Reset this particular endpoint's parameters. */
    pipe->readPos = 0;
    pipe->readLen = 0;

    switch (radioType) {
    case RADIO_TYPE_SIW_USB:
        if (ReadFile(pipe->handle, pipe->inBuf, 
                     pipe->inBufLen, &pipe->readLen, &pipe->ovrLap)) {
            /* If ReadFile returned true, then the read was 
             * successful (no wait is required). Immediately
             * request a read event to be sent to the transport.
             */
            SetEvent(pipe->ovrLap.hEvent);
        } else {
            /* Otherwise, this was an overlapped read, and the
             * hEvent will be fired when bytes show up.
             */
            error = GetLastError();
            Assert(error == ERROR_IO_PENDING);
        }
        break;

    case RADIO_TYPE_CSR_USB:
        switch (pipe->type) {
        
        case USB_INTERRUPT_READ:    /* HCI Event read.. */
            /* Prepare this event for use. */
            ovrReadHciEvnt.hEvent = readHciEvntDone;
            ResetEvent(readHciEvntDone);

            /* If we got here, we must have had an HCI Event to read. */
            rc = DeviceIoControl(pipe->handle,
                                 IOCTL_CSRBC01_GET_HCI_EVENT, 
                                 NULL, 
                                 16,            /* CSR requires 16 */
                                 pipe->inBuf, 
                                 pipe->inBufLen,
                                 &pipe->readLen, 
                                 &ovrReadHciEvnt);

            /* See how many bytes were read. */
            if (GetOverlappedResult(pipe->handle, 
                                    &ovrReadHciEvnt, 
                                    &pipe->readLen, FALSE) == FALSE) {
                error = GetLastError();
                Assert(error == ERROR_IO_INCOMPLETE);

            } else {
                /* Manual reset if wait completed. This is an internal
                   event signaling, so do it rightaway once we've got it. */
                ResetEvent(readHciEvntDone);

            }
            break;


        case USB_BULK_READ:     /* ACL Data read.. */
            Assert((pipe->handle != INVALID_HANDLE_VALUE) && (pipe->handle != 0));

            /* Prepare this event for use. */
            ovrReadAclData.hEvent = readAclDataDone;
            /* Ensure we reset to non-signaled. */
            ResetEvent(readAclDataDone);

            /* If we got here, we must have had data to read. */
            rc = ReadFile(pipe->handle, pipe->inBuf, 
                          pipe->inBufLen, &pipe->readLen, &ovrReadAclData);

            /* Wait till it completes or forever. */
            if (GetOverlappedResult(pipe->handle, 
                                    &ovrReadAclData, 
                                    &pipe->readLen, FALSE) == FALSE) {

                /* Otherwise, this was an overlapped read, and the
                 * hEvent will be fired when bytes show up.
                 */
                error = GetLastError();
                Assert(error == ERROR_IO_INCOMPLETE);

            } else {
                /* Manual reset if wait completed. This is an internal
                   event signaling, so do it rightaway once we've got it. */
                ResetEvent(readAclDataDone);

            }

            Assert(pipe->readLen <= BULKBUFSIZE); 
            Assert((pipe->handle != INVALID_HANDLE_VALUE) && (pipe->handle != 0));
            break;

        default:
            Assert((pipe->type != USB_INTERRUPT_READ) || (pipe->type != USB_BULK_READ));
            break;
        } /* switch(pipe->type) */
        break;
    };
} /* pipeRead() */


/*---------------------------------------------------------------------------
 * eventAsyncRead() 
 *
 *  Start an asynchronous read for an HCI event. If an event is available
 *  to read, signal eventThread.
 */
static BOOL eventAsyncRead(UsbPipe *pipe)
{
    ULONG  written = 0x00;
    BOOL   rc = FALSE;
    DWORD  error;

    Assert((pipe->handle != INVALID_HANDLE_VALUE) && (pipe->handle != 0));

    /* Explicitly reset the event. */
    ResetEvent(pipe->ovrLap.hEvent);

    /* Block till the next HCI Event occurs. */
    rc = DeviceIoControl(pipe->handle,
                         IOCTL_CSRBC01_BLOCK_HCI_EVENT, 
                         0, 0, 0, 0,
                         &written, 
                         &pipe->ovrLap);
    if (!rc) {
        /* I/O pending is expected. Not sure why we get Access Denied, 
         * but we can recover. 
         */
        error = GetLastError();
        Assert((error == ERROR_IO_PENDING) || 
               (error == ERROR_ACCESS_DENIED));
    } else {
        /* An event is already available so signal eventThread. */
        SetEvent(pipe->ovrLap.hEvent);
    }
    pipe->triggerArmed = TRUE;

    return rc;

} /* eventAsyncRead() */



/*---------------------------------------------------------------------------
 * aclAsyncRead() 
 *
 *      Waits for next ACL RX event. This routine will block
 *        until the USB driver UsbReports data was available.
 */
static BOOL aclAsyncRead(UsbPipe *pipe)
{
    ULONG  written = 0x00;
    ULONG  error = 0x00;
    BOOL   rc = FALSE;

    /* Explicitly reset the event. */
    ResetEvent(pipe->ovrLap.hEvent);

    /* Block till the next HCI Event occurs. */
    rc = DeviceIoControl(pipe->handle,
                         IOCTL_CSRBC01_BLOCK_HCI_DATA, 
                         0, 0, 0, 0,
                         &written, 
                         &pipe->ovrLap);

    if (! rc) {
        /* I/O pending is expected. Not sure why we get Access Denied, 
         * but we can recover. Return and let eventThread() unblock when
         * this event is signaled.
         */
        error = GetLastError();
        Assert((error == ERROR_IO_PENDING) || 
               (error == ERROR_ACCESS_DENIED));
    } else {
        /* Not documented, but seems we get here if we tried
         * to do this operation when we are already pending
         * the signal or device has remnant data to read.
         */
        SetEvent(pipe->ovrLap.hEvent);
    }

    pipe->triggerArmed = TRUE;
    return rc;

} /* aclAsyncRead() */



/*---------------------------------------------------------------------------
 * pipeClose() 
 *      Close the USB pipe.
 * Requires:
 *     
 * Parameters:
 *      pipe    pointer to existing UsbPipe structure
 *
 * Return:  
 *      TRUE    success
 *      FALSE   failed
 */
static BOOL pipeClose(UsbPipe *pipe)
{
    Assert(pipe);

    CloseHandle(pipe->handle);
    return TRUE;
}



/*---------------------------------------------------------------------------
 * closeUsbDevice()
 *      Close the USB interface.
 *
 * Requires:
 * Parameters: 
 *  Return: 
 */
static void NEAR closeUsbDevice(void)
{
    I8 i; 

    switch (radioType) {
    case RADIO_TYPE_SIW_USB:
        for (i = 0; i < MAX_PIPES; i++) {
            CancelIo(usbPipe[i].handle);
            pipeClose(&usbPipe[i]); 
        }
        break;

    case RADIO_TYPE_CSR_USB:
        CancelIo(usbPipe[0].handle);
        pipeClose(&usbPipe[0]);
        break;
    };
} 



/*---------------------------------------------------------------------------
 * controlWrite() 
 *      Write data to Endpoint 0 (Control Endpoint)
 *      This function was supplied by Silicon Wave. Unfortunately, I think
 *      the code to access the Control Endpoint may be implementation-
 *      specific.
 *
 * Requires:
 *     
 * Parameters:
 *      buf - buffer to be written
 *      len - length of buf
 *
 * Returns:
 *      TRUE  - write was successful
 *      FALSE - write failed
 */
static BOOL controlWrite(UsbPipe *pipe, const UCHAR * const buf, UINT len) 
{ 
    DWORD     bytesRet; 
    physAddr  pSetupBuf; 
    physAddr  pDataBuf; 
    BOOL      rc;
    static    UCHAR sBuf[8] = {0,0,0,0,0,0,0,0}; 
    DWORD     error;

    sBuf[6]   = (unsigned char)len; 
    pSetupBuf = (physAddr)sBuf; 
    pDataBuf  = (physAddr)buf; 

    switch (radioType) {
    case RADIO_TYPE_SIW_USB:
        /* Setup for the actual write */
        if ((rc = DeviceIoControl(pipe->handle, 
                                  IOCTL_NCUSB_CONTROL_WRITE_SETUP, 
                                  (UCHAR *)pSetupBuf, 8, 
                                  (UCHAR *)pSetupBuf, 8, 
                                  &bytesRet, NULL))) {

            /* Now write the data */
            rc = DeviceIoControl(pipe->handle,
                                 IOCTL_NCUSB_CONTROL_WRITE_DATA, 
                                 pDataBuf,  *(WORD *)((physAddr)pSetupBuf+6), 
                                 pDataBuf,  *(WORD *)((physAddr)pSetupBuf+6), 
                                 &bytesRet, NULL);

            if (rc == TRUE) {
                /* Signal thread that the write is complete. */
                SetEvent(pipe->ovrLap.hEvent);
            }
        }
        break;

    case RADIO_TYPE_CSR_USB:
        rc = DeviceIoControl(pipe->handle,
                             IOCTL_CSRBC01_SEND_HCI_COMMAND, 
                             (physAddr)buf, len,
                             0,  0,
                             &bytesRet, 
                             &pipe->ovrLap);

        if (! rc) {
            /* I/O pending is expected. Not sure why we get Access Denied, 
             * but we can recover. 
             */
            error = GetLastError();
            Assert((error == ERROR_IO_PENDING) || 
                   (error == ERROR_ACCESS_DENIED));
        } else {
            SetEvent(pipe->ovrLap.hEvent);
            rc = TRUE;
        }
        break;

    default:
        Assert(0);
        break;
    };

    return rc; 


} /* controlWrite() */

/*---------------------------------------------------------------------------
 * Causes the main thread to wake up immediately
 */
static void wakeupEventThread()
{
    Assert(eventThreadSignal);
    SetEvent(eventThreadSignal);
}


/*---------------------------------------------------------------------------
 * Watch for USB events.
 * This is a thread that pends waiting for driver events. When an event
 * occurs, it calls the handler.
 *
 * Context: interrupt/task
 *   Args:  pipes - pointer to an array of pipes
 *
 * Return:    TRUE
 */
static DWORD FAR PASCAL eventThread(UsbPipe *pipes)
{
    DWORD       index;
    HANDLE      events[MAX_PIPES+1];
    UsbPipe    *pipe;
    Assert(pipes);

    /*
     * Build an array of event handles for which we're going to wait for
     * the individual events.
     */
    for (index = 0; index < MAX_PIPES; index++) {
        events[index] = pipes[index].ovrLap.hEvent;
    }
    events[MAX_PIPES] = eventThreadSignal;

    /* Without this it's possible to miss a signaled event when 
     * eventThread() starts up.
     */
    while ((! pipes[USB_INTERRUPT_READ].notify) ||
           (! pipes[USB_BULK_READ].notify)) {
        /* Avert race condition that occurs when we try to
           invoke callback below before the callback has been
           registered. */
    }

    /* Stay in this thread while still connected */
    while (isActive) {

        /* We will run whenever any pipe has activity. Don't wait
         * infinitely in case USB_Deinit() is called.
         */
        index = WaitForMultipleObjects(MAX_PIPES + 1,
                                       events,
                                       FALSE,       /* wait on any handle */
                                       10000);

        OS_StopHardware();

        if (index == WAIT_FAILED) {
            /* Wait should never fail so assert if it does */
            DWORD error = GetLastError();
            Assert(0);

        } else if (index != WAIT_TIMEOUT) {

            /* Make 'index' an index of which handle had activity */
            index -= WAIT_OBJECT_0;

            /* At least on W98 the event never returned to non-signalled
             * so we can do it here. !NOTE!: we could reset the event only
             * when the callback was successful (see below); however, with
             * high-volume data this can starve the stack task so that we
             * run out of buffers; therefore, we will always reset.
             */
            ResetEvent(events[index]);

            if (index < MAX_PIPES) {
                /* pipe = the one that caused us to run. So set signal pending. */
                pipes[index].signalPending = TRUE;
            }
        } 

//        if (index == WAIT_TIMEOUT) { Report(("USB: Wait timeout\n")); }
        /* WAIT_ABANDONED_n won't happen, and WAIT_TIMEOUT just drops through to
         * the routine below.
         */

        /* Now run through all pipes to see if we need to do any notifications */
        for (index = 0; index < MAX_PIPES; index++) {
            pipe = &pipes[notifyOrder[index]];
            if ((pipe->signalPending == TRUE) &&
                (pipe->notify == TRUE)) {
                pipe->signalPending = !pipe->callback();
            }
        }

        OS_ResumeHardware();

    } /* while(USB_Connected) */

    /* Close all events */
    for (index = 0; index < MAX_PIPES; index++) {
        CloseHandle(events[index]);
    }

    /* Send the event back to notify that we have successfully exited. */
    SetEvent(eventThreadSignal);

    return(TRUE);

} /* End of eventThread() */



#ifdef CSR_RESET_PROBLEM
/*---------------------------------------------------------------------------
 * resetCsr() 
 *      This function resets the CSR radios.    
 *     
 * Parameters:
 *      devName     device name of controller
 */
static void resetCsr(char *devName)
{
    DWORD status;
    UsbPipe *pipe;
    unsigned long written;

    pipe = &usbPipe[0];

    pipe->notify = FALSE;
    /* Open this pipe. */
    if (! pipeOpen(devName, pipe, (UsbEndPoint)0)) {
        return;
    }

    status = DeviceIoControl(
                            pipe->handle,
                            IOCTL_CSRBC01_RESET_DEVICE,
                            0,
                            0,
                            0,
                            0,
                            &written,
                            0
                            );
    Assert((status));

    Sleep(500);
    closeUsbDevice();
}


/*---------------------------------------------------------------------------
 * fakeResetCmd() 
 *      Fool the upper layer into thinking we transmitted the Reset 
 *      command. Signal eventThread that we have sent the packet.
 *      Set a timer upon whose expiration we will send a Command Complete
 *      event up the stack.
 *     
 * Parameters:
 *      pipe    pointer to Control pipe
 */
static void fakeResetCmd(UsbPipe *pipe)
{
    readingReset = TRUE;

    /* Signal eventThread that we sent the buffer. */
    SetEvent(pipe->ovrLap.hEvent);
    SetEvent(usbPipe[USB_INTERRUPT_READ].ovrLap.hEvent);
}
#endif /* CSR_RESET_PROBLEM */


