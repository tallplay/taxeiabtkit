//#include "stdafx.h"
#ifndef __STDFFF__
#define __STDFFF__

#ifndef _SECURE_ATL
#define _SECURE_ATL 1
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers
#endif

// Modify the following defines if you have to target a platform prior to the ones specified below.
// Refer to MSDN for the latest info on corresponding values for different platforms.
#ifndef WINVER				// Allow use of features specific to Windows XP or later.
#define WINVER 0x0501		// Change this to the appropriate value to target other versions of Windows.
#endif

#ifndef _WIN32_WINNT		// Allow use of features specific to Windows XP or later.                   
#define _WIN32_WINNT 0x0501	// Change this to the appropriate value to target other versions of Windows.
#endif						

#ifndef _WIN32_WINDOWS		// Allow use of features specific to Windows 98 or later.
#define _WIN32_WINDOWS 0x0410 // Change this to the appropriate value to target Windows Me or later.
#endif

#ifndef _WIN32_IE			// Allow use of features specific to IE 6.0 or later.
#define _WIN32_IE 0x0600	// Change this to the appropriate value to target other versions of IE.
#endif

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// some CString constructors will be explicit

// turns off MFC's hiding of some common and often safely ignored warning messages
#define _AFX_ALL_WARNINGS

//#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions

#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#endif

#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

#endif

#include "bulkusb.h"
extern "C" {

#include "MassUart.h"

#define BULKIN 0
#define BULKOUT 1

CBulkUSB mydevice;

#define MASS_UART_ID2  "Taxeia  BT KM share     1.00bt168kms"
#define MASS_UART_ID1  "Taxeia  MtkBT Presentor 1.00bt168mtk"

int MassUart_Execute(int bulkinout, int *transferlength, char *cdb, char *databuf)
{
    return mydevice.Execute(bulkinout, transferlength, cdb, databuf);
}

int MassUart_Close()
{
    mydevice.close_device();

    return RETCODE_SUCCESS;
}

BOOL skip_first = FALSE;
BOOL IsFujitsu = FALSE;

int MassUart_Test()
{
    char inq[512];
    return mydevice.Inquiry(inq);
}

int MassUart_Open()
{
    BOOL        r_value;
    int         i;

    char        drivebuf[MAX_PATH];
	char		inq[512];

    DWORD       iLogicalDrives  = ::GetLogicalDrives();
    
    for (i = 3; i < 26; i++) 
    {
        if (((1 << i) & iLogicalDrives) != 0) 
        {
            sprintf(drivebuf, "%c:\\\\", i + 'A');

            r_value = mydevice.open_device(i + 'A');
            if (!r_value) 
            {
                continue;
            }
	
			r_value = mydevice.Inquiry(inq);
			if(!r_value)
            {
				mydevice.close_device();
				continue;
			}

IsFujitsu = FALSE;
            if (!memcmp(inq + 8, MASS_UART_ID1, 24))
            {
IsFujitsu = TRUE;
				if(skip_first == TRUE)
		    	{
		    	    skip_first = FALSE;
		    	    continue;
		    	}
			    sprintf(inq, "Open %s success", drivebuf);
			    OutputDebugString(drivebuf);
				return 0; //success
            }
			else if(!memcmp(inq + 8, MASS_UART_ID2, 24))
			{
				if(skip_first == TRUE)
		    	{
		    	    skip_first = FALSE;
		    	    continue;
		    	}
			    sprintf(inq, "Open %s success", drivebuf);
			    OutputDebugString(drivebuf);
				return 0; //success
			}
			else
			{
                mydevice.close_device();
                continue;
			}
        }
    }

    return 1;//fail
}

int MassUart_Recv(U8 *buf, U8 *len)
{
    char cmd[13] = "\xF1\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00";
    int rlen;
	rlen = 64;
	
    if(!mydevice.Execute(BULKIN, &rlen, cmd, (char*)buf)) //tallplay
    {
    	*len = 0;
        return RETCODE_COMMAND_INVALID;
    }
	
    *len = rlen;
    return RETCODE_SUCCESS;
}

int MassUart_TxCnt(U16 *cnt)
{
    char cmd[13] = "\xF3\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00";
    int rlen;
	rlen = 2;
	
    if(!mydevice.Execute(BULKIN, &rlen, cmd, (char*)cnt)) //tallplay
    {
        return RETCODE_COMMAND_INVALID;
    }
	
    return RETCODE_SUCCESS;
}

int MassUart_RecvLen(U8 *buf, U8 *len)
{
    char cmd[13] = "\xF1\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00";
    int rlen;
	rlen = *len;
	
    if(!mydevice.Execute(BULKIN, &rlen, cmd, (char*)buf)) //tallplay
    {
    	*len = 0;
        return RETCODE_COMMAND_INVALID;
    }
	
    *len = rlen;
    return RETCODE_SUCCESS;
}

int MassUart_Send(U8 *buf, U8 len)
{
    char cmd[13] = "\xF0\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00";
    int ret;
	U16 xp_len;

    cmd[8] = len;
	
    xp_len = (len == 1) ? 2 : len;

    //execute
    ret = mydevice.Execute(BULKOUT, xp_len, (char*)cmd, (char*)buf);
    if(ret)
        return RETCODE_SUCCESS;
    
    return RETCODE_DEVICE_FAIL; 
}

int MassUart_Send2(U8 *buf, U16 *len)
{
    char cmd[13] = "\xF0\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00";
    int ret;
	int xp_len;
	U16 cnt;

	if(IsFujitsu == TRUE)
	{

	    MassUart_TxCnt(&cnt);

		cnt = cnt > *len ? *len : cnt;
		
	    //two bytes length
	    cmd[8] = (cnt >> 8) & 0xff;
	    cmd[9] = cnt & 0xff;
		
	    xp_len = (cnt == 1) ? 2 : cnt;

	    //execute
	    if(!mydevice.Execute(BULKOUT, &xp_len, (char*)cmd, (char*)buf))
	    {
	        *len = 0;
	        return RETCODE_COMMAND_INVALID;
	    }

		*len = cnt;
	    return RETCODE_SUCCESS; 
	}
	else
	{
	    cmd[8] = *len;		
	    xp_len = (*len == 1) ? 2 : *len;
	    //execute
	    ret = mydevice.Execute(BULKOUT, xp_len, (char*)cmd, (char*)buf);
	    if(ret)
	        return RETCODE_SUCCESS;
	    
	    return RETCODE_DEVICE_FAIL; 
		
	}
}

} //C linkage
