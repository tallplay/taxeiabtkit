#include "osapi.h"
#include "uart.h"
#include "windows.h"

#include "parseopts.h"
#include "MassUart.h"

#ifdef MASSUARTTEST
#define OS_StopHardware()       (void)0
#define OS_ResumeHardware()     (void)0
#undef Report
#define Report(S)
#undef Assert
#define Assert(e)
#endif
/****************************************************************************
 *
 * Constants
 *
 ****************************************************************************/
#ifndef DUMP_UART_DATA
    #define DUMP_UART_DATA   XA_ENABLED
#endif

#ifndef DUMP_THREADING
    #define DUMP_THREADING XA_ENABLED
#endif

#if DUMP_THREADING == XA_DISABLED
   #define ReportThread(s) Report(s)
#else
   #define ReportThread(s)
#endif

/* Size of our read buffer. This can be any size larger than one. */
#define MAX_READ_BUFF   100

/* Mappings to NT API */
#define _fmemset   memset
#define _fmemmove  memmove

/****************************************************************************
 *
 * Global Data
 *
 ***************************************************************************/
/* Indicates whether the port and thread are active */
static BOOL         isActive = FALSE;

/* Callback provided by the stack */
static UartCallback callback = NULL;

/* Number times the UART has been initialized */
static U8           initCount = 0;

/* Overlap structures for asynchronous Win32 file access */
static OVERLAPPED   osWrite = { 0};
static OVERLAPPED   osRead  = { 0};

/* Number of bytes written in last UART_Write operation */
static DWORD        lastWritten;

/* Input buffer to use when we're not sure if more data is coming */
static U8           readBuffer[MAX_READ_BUFF];
static DWORD        readLen; /* Length of data in readBuffer */
static DWORD        readPos; /* Current position in readBuffer */

/* Error message detected during initialization */
static char        *errMessage;

/* Handle for UART free running thread */
static HANDLE uartThread = NULL;
static HANDLE readThread = NULL;
static HANDLE writeThread = NULL;


/* The Motorola radios require some special handling for power up and
 * the HCI RESET command. To initialize the stack reliably, the following
 * must be done:
 *
 * 1. Wait at least 2 seconds before issuing the first reset command.
 * 2. Discard all data from the serial port until the reset command
 *    has been issued.
 * 3. Discard all data from the serial port after a reset command until
 *    the reset command complete event packet is received.
 *
 * Step 2 and 3 are useful for other radios as well, so they are enabled
 * for all radios.
 */
static U8   resetOffset = 0;
static U8   resetComplete[] = { 0x04, 0x0E, 0x04 };
static enum {
    DISCARD_ALL, RESET, RESET_DONE
} resetState = DISCARD_ALL;

/* Used to indicate asynchronous shutdown */
static EvmTimer notifyShutdown;

/****************************************************************************
 *
 * Local Prototypes
 *
 ***************************************************************************/

static DWORD FAR PASCAL eventThread(LPSTR lpData);
static DWORD FAR PASCAL readThreadFunc(LPSTR lpData);
static DWORD FAR PASCAL writeThreadFunc(LPSTR lpData);
static BOOL openConnection(void);
static BOOL setupConnection(U16 speed);
static void restartRead(void);
static BOOL internalInit(void);
static BOOL isRadioPresent(void);
void NotifyShutdown(EvmTimer *Timer);

#if (DUMP_UART_DATA == XA_ENABLED) && (XA_DEBUG == XA_ENABLED)
static void DisplayHex(const char *Source, const U8 *Buffer, U16 Len);
#else
    #define DisplayHex(S, B, L) (0)
#endif

typedef struct __a
{
    U8 buffer[256];
    U16 length;
}writeThreadParam ;

writeThreadParam wtp;

static HANDLE  uartMux = NULL;  /* Mutex for Uart resource */
static HANDLE  readMux = NULL;  /* Mutex for read  */
static HANDLE  writeMux = NULL;  /* Mutex for write */

/****************************************************************************
 *
 * Public Functions
 *
 ***************************************************************************/

/*---------------------------------------------------------------------------
 * UART_Init()
 *
 *     Called by the stack (during EVM_Init) to intialize the UART hardware
 *     driver.
 *
 * Returns:
 *     BT_STATUS_PENDING - Initialization is pending.
 *     BT_STATUS_FAILED - Initialization failed.
 */
BtStatus UART_Init(UartCallback func)
{
    initCount++;
    /* Note that we may issue the HCI RESET command multiple times
     * if the first attempt fails. Therefore, start the read process
     * all over again.
     */
    resetState = DISCARD_ALL;
    resetOffset = 0;


    /* Initialize the uart */
    callback = func;
    if (internalInit()) {
        Report(("UART: Initializing\n"));
        return BT_STATUS_PENDING;
    } else {
        /* Display an error message for the user */
        UART_Shutdown();
        return BT_STATUS_FAILED;
    }
}

/* Helper function for UART_Init */
BOOL internalInit(void)
{
    /* Initialize the OVERLAPPED structures */
    memset(&osWrite, 0, sizeof(OVERLAPPED));
    memset(&osRead, 0, sizeof(OVERLAPPED));

    uartMux = CreateMutex(NULL, FALSE, NULL);
    if (uartMux == NULL) {
        return FALSE;
    }

    readMux = CreateMutex(NULL, FALSE, NULL);
    if (readMux == NULL) {
        return FALSE;
    }
    writeMux = CreateMutex(NULL, FALSE, NULL);
    if (writeMux == NULL) {
        return FALSE;
    }

    /* Create I/O events used for overlapped reads/writes */
    osRead.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
    if (osRead.hEvent == NULL) {
        errMessage = "Couldn't allocate resources (read semaphore)";
        return FALSE;
    }

    osWrite.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
    if (NULL == osWrite.hEvent) {
        errMessage = "Couldn't allocate resources (write semaphore)";
        return FALSE;
    }

    /* Now open the port */
    if (FALSE == openConnection()) {
        return FALSE;
    }

    /* Start the first read operation so that we'll be informed
     * of read events.
     */
    restartRead();

    return TRUE;
}


/*---------------------------------------------------------------------------
 * UART_Shutdown() 
 *      Release any resources, close the connection if open
 *
 * Requires:
 *     
 * Parameters:
 *
 * Returns:
 */
BtStatus NEAR UART_Shutdown(void)
{
    Report(("UART: Deinit start\n"));

    /* Force the connection to close */
    if (isActive) {
        isActive = FALSE;
    }

    Report(("UART: Deinitialized\n"));
    return BT_STATUS_PENDING;
}

/*---------------------------------------------------------------------------
 * UART_Read() 
 *      Read bytes from the comm port
 *
 * Requires:
 *     
 * Parameters:
 *      buffer  allocated buffer to receive the bytes
 *      length  max number of bytes to read
 *
 * Returns:
 *      number of bytes read
 */
U16 UART_Read(U8 *buffer, U16 length)
{
    U16 bytesToCopy;
    U16 result;

    /* If UART_Read is called and the readBuffer is empty, we know
     * there's a pending read operation (it may or may not be complete).
     */
     #if 1
    if (readPos == readLen) {
        #if 0
        result = GetOverlappedResult(commPort, &osRead, &readLen, FALSE);
        if (result==FALSE) {
            return 0;
        }
        #endif
        if(readThread)
            return 0;
    }
    #endif


   /* Discard all data until after HCI RESET has been sent. */
    if ((resetState == RESET)
        &&  (memcmp(readBuffer + readPos, resetComplete + resetOffset,
                    min(readLen - readPos, sizeof(resetComplete) - resetOffset)) == 0)) {

        /* Found HCI RESET COMMAND COMPLETE event packet. Send
         * it up to the stack.
         */
        resetOffset += length;
        if (resetOffset >= sizeof(resetComplete)) {

            /* Complete RESET COMMAND COMPLETE event packet has been
             * sent up the stack. Can now proceed as normal.
             */
            resetState = RESET_DONE;
        }
    }
    #ifndef MASSUARTTEST
    else if (resetState != RESET_DONE) {

        /* Just discard all input until we've actually sent the HCI
         * RESET COMMAND and matched the expected event packet reply.
         */
        restartRead();
        return 0;
    }
    #endif

    /* Handle any available data */
    bytesToCopy = (U16)(min(readLen-readPos, length));
    memcpy(buffer, readBuffer+readPos, bytesToCopy);
    readPos += bytesToCopy;

    DisplayHex("RX:", buffer, (U16)bytesToCopy);
    if (readPos == readLen) {
        /* If the readBuffer is empty, wait for more data */
        restartRead();
    }


    // Report(("UART: Read %d bytes\n", bytesToCopy));

    return bytesToCopy;
} /* end of UART_Read() */


/*---------------------------------------------------------------------------
 * UART_Write() 
 *      Write a buffer to the comm port
 *
 * Requires:
 *     
 * Parameters:
 *      buffer  data to send
 *      length  bytes in buffer
 *
 * Returns:
 *      Number of bytes written. If an error occurs, 0 bytes are returned.
 */
U16 UART_Write(const U8 *buffer, U16 length)
{
    DWORD         dwThreadID;
    DWORD       bytesWritten;
    DWORD       error = 0;

    // Report(("UART: Write() %d bytes.\n", length));

    DisplayHex("TX:", buffer, (U16)length);
    /* Do a quick check to see if the radio is present */
    if (isRadioPresent()==FALSE) {
        return 0;
    }

    if (resetState == DISCARD_ALL) {
        /* First write will be for the HCI RESET command so also begin
         * to search for the HCI RESET COMMAND COMPLETE event packet.
         */
        resetState = RESET;
    }

    /* Write the data asynchronously.
     * WriteFile may return before completion.
     */
    /* Create a secondary thread to watch for an event. */
    Assert(writeThread == NULL);
    //    Assert(length < 256);

    bytesWritten = min(length, 64);
    
    memcpy(wtp.buffer, buffer, bytesWritten);
    wtp.length = bytesWritten;
    ReportThread(("============CreatewriteThread+\n"));
    
    writeThread = CreateThread((LPSECURITY_ATTRIBUTES)NULL,
                              0,
                              (LPTHREAD_START_ROUTINE)writeThreadFunc,
                              (LPVOID)&wtp,
                              0, &dwThreadID);
    Assert(writeThread);
    
    ReportThread(("============CreatewriteThread-\n"));

    lastWritten = bytesWritten;
    // Report(("UART: Wrote %d bytes\n", length));
    /* bytesWritten will be zero since the WriteFile has only
     * just started.
     */
    return bytesWritten;
} /* End of UART_Write() */


/*---------------------------------------------------------------------------
 * UART_SetSpeed() 
 *     Called to change the speed of the UART.
 *
 * Requires:
 *
 * Parameters:
 *     speed - the speed (in KBps) at which the UART should operate (57 or 115)
 *
 * Returns:
 */
void UART_SetSpeed(U16 speed)
{
}

/****************************************************************************
 *
 * Private Functions
 *
 ***************************************************************************/

/*---------------------------------------------------------------------------
 * eventThread() 
 *      This thread watches for COMM events.  When data is available 
 *      to be read, the interrupt handler is called with the appropriate
 *      event.
 *
 * Requires:
 *     
 * Parameters:
 *
 * Returns:
 *      TRUE    success
 *      FALSE   failed
 */
void OS_CatchUart();
void OS_ReleaseUart();

void OS_CatchUart()
{
    DWORD result;
    
    /* Because the hardware thread always takes this mutex before it
     * runs, we can take it to prevent the hardware thread from running.
     * In a typical MINIOS system, this code should be replaced with
     * a system call to disable I/O interrupts.
     */
    result = WaitForSingleObject(uartMux, INFINITE);
    Assert(result == WAIT_OBJECT_0);
}


void OS_ReleaseUart()
{
    BOOL result;

    /* In a typical MINIOS system, this code should re-enable whatever
     * interrupts were disabled by the previous OS_StopHardware call.
     */
    result = ReleaseMutex(uartMux);
    if (result != TRUE)
    {
        Report(("Resumption error: %d\n", GetLastError()));
    }
    Assert(result == TRUE);
}

void OS_CatchRead()
{
#if 0
    DWORD result;
    
    /* Because the hardware thread always takes this mutex before it
     * runs, we can take it to prevent the hardware thread from running.
     * In a typical MINIOS system, this code should be replaced with
     * a system call to disable I/O interrupts.
     */
    result = WaitForSingleObject(readMux, INFINITE);
    Assert(result == WAIT_OBJECT_0);
#endif
}


void OS_ReleaseRead()
{
#if 0
    BOOL result;

    /* In a typical MINIOS system, this code should re-enable whatever
     * interrupts were disabled by the previous OS_StopHardware call.
     */
    result = ReleaseMutex(readMux);
    if (result != TRUE)
    {
        Report(("Resumption error: %d\n", GetLastError()));
    }
    Assert(result == TRUE);
#endif
}

void OS_CatchWrite()
{
#if 0
    DWORD result;
    
    /* Because the hardware thread always takes this mutex before it
     * runs, we can take it to prevent the hardware thread from running.
     * In a typical MINIOS system, this code should be replaced with
     * a system call to disable I/O interrupts.
     */
    result = WaitForSingleObject(writeMux, INFINITE);
    Assert(result == WAIT_OBJECT_0);
#endif
}


void OS_ReleaseWrite()
{
#if 0
    BOOL result;

    /* In a typical MINIOS system, this code should re-enable whatever
     * interrupts were disabled by the previous OS_StopHardware call.
     */
    result = ReleaseMutex(writeMux);
    if (result != TRUE)
    {
        Report(("Resumption error: %d\n", GetLastError()));
    }
    Assert(result == TRUE);
#endif
}

static DWORD FAR PASCAL readThreadFunc(LPSTR lpData)
{

    ReportThread(("============readThread+\n"));
    OS_CatchRead();
    while(readLen == 0)
    {
        OS_CatchUart();
        MassUart_Recv(readBuffer, &readLen);        
        OS_ReleaseUart();
        if(!isActive)
        {
            readThread = NULL;
            return 0;
        }
        /*
        if(readLen == 0)
            Sleep(10);
            */
    }
    readPos = 0;
    readThread = NULL;

    SetEvent(osRead.hEvent);
    OS_ReleaseRead();
    ReportThread(("============readThread-\n"));

    return 0;
}

static DWORD FAR PASCAL writeThreadFunc(LPSTR lpData)
{
    DWORD bytesWritten;
    writeThreadParam *ttt;
    ttt = (writeThreadParam *) lpData;
    OS_CatchWrite();
    ReportThread(("============writeThread+\n"));

    OS_CatchUart();

    if (MassUart_Send(ttt->buffer, ttt->length)) {

        Assert(0);

    }
    
    OS_ReleaseUart();

    writeThread = NULL;
    SetEvent(osWrite.hEvent);
    OS_ReleaseWrite();

    ReportThread(("============writeThread-\n"));
    return 0;
}
 
static DWORD FAR PASCAL eventThread(LPSTR lpData)
{
    DWORD result;
    HANDLE eventArray[2];

    /* Fill the event array with possible events */
    eventArray[0] = osWrite.hEvent;
    eventArray[1] = osRead.hEvent;

    /* Notify the transport that the UART is initialized */
    callback(UE_INIT_COMPLETE);

    /* Stay in this thread while still connected */
    while (isActive) {

        /* Wake up every now and then just in case a Deinit is requested */
        result = WaitForMultipleObjects(2, eventArray, FALSE, 100);

        /* Do not process the event until we are sure that a higher
         * layer is not between OS_StopHardware and OS_ResumeHardware. We
         * accomplish this by calling OS_StopHardware and OS_ResumeHardware
         * ourselves.
         */
        OS_StopHardware();

        if (callback == NULL) {
            return FALSE;
        }

        if (result == WAIT_OBJECT_0+1) {
            /* Data was received, notify upper layer */

            //Report(("UART: Read event\n"));
            /* Note: we do not reset the event, UART_Read code does this */
			ReportThread(("============callr+\n"));
			OS_CatchRead();
            callback(UE_DATA_TO_READ);
			OS_ReleaseRead();
			ReportThread(("============callr-\n"));

        } else if (result == WAIT_OBJECT_0) {
            /* The write is done, notify upper layer */

            /* We could check for errors using GetOverlappedResult, but there
             * are two problems with this. 1) It doesn't work at all in Win95
             * (other Win32 platforms OK) 2) Sometimes a write will complete
             * successfully even if the other device is not present. (Perhaps
             * due to certain null-modem cables automatically holding RTS high?)
             * So we're going to allow the transport layer to deal with this.
             */
            // Report(("============UART: Write event\n"));
            ResetEvent(osWrite.hEvent);
			ReportThread(("============callw+\n"));
            OS_CatchWrite();
            callback(UE_WRITE_COMPLETE);
            OS_ReleaseWrite();
			ReportThread(("============callw-\n"));
        }

        OS_ResumeHardware();
    }

    MassUart_Close();
    
    /* Close read and write event handles if necessary */
    if (osWrite.hEvent != NULL) {
        CloseHandle(osWrite.hEvent);
        osWrite.hEvent = NULL;
    }

    if (osRead.hEvent != NULL) {
        CloseHandle(osRead.hEvent);
        osRead.hEvent = NULL;
    }

    CloseHandle(readMux);
    CloseHandle(writeMux);
    CloseHandle(uartMux);
    readMux = NULL;
    writeMux = NULL;
    uartMux = NULL;

    /* Start timer to send event to indicate shutdown complete */
    #ifndef MASSUARTTEST
    notifyShutdown.func = NotifyShutdown;
    notifyShutdown.context = (void *)1;
    EVM_StartTimer(&notifyShutdown, 1);
    #endif
    return TRUE;

} /* End of eventThread() */

/*---------------------------------------------------------------------------
 * NotifyShutdown() 
 *      This timer handler notifies the transport that shutdown has completed.
 *      This event is scheduled when the I/O thread exits.
 *
 * Parameters:
 *      Event Manager timer structure.
 *
 * Returns:
 *      TRUE    success
 *      FALSE   failed
 */
void NotifyShutdown(EvmTimer *Timer)
{
    Assert(callback != NULL);
    callback(UE_SHUTDOWN_COMPLETE);
}

/*---------------------------------------------------------------------------
 * isRadioPresent()
 *
 *     Determines whether there is a device connected to our serial port.
 *
 * Returns:
 *      TRUE    Yes, a device was detected
 *      FALSE   No device is present
 */
static BOOL isRadioPresent(void)
{
    /* We could check the receive-line-signal-detect signal to determine
     * whether any hardware is connected. The signal should be high when the
     * connection is live. However, some null-modem cables don't seem to 
     * support this so we'll just return TRUE.
     *
     * DWORD lineStatus;
     * GetCommModemStatus(commPort, &lineStatus);
     * if ((lineStatus & MS_RLSD_ON)==0) {
     *    return FALSE;
     * }
     */
    return TRUE;
}

/*---------------------------------------------------------------------------
 * openConnection()
 *
 *     Opens the UART port and creates a thread to manage it.
 *
 * Returns:
 *      TRUE    success
 *      FALSE   failed; errMessage is set with a description of the error.
 */
static BOOL openConnection(void)
{
    DWORD         dwThreadID;
    DWORD bytesWritten;

    if(MassUart_Open())
        return FALSE;

    isActive = TRUE;

    /* Create a secondary thread to watch for an event. */
    uartThread = CreateThread((LPSECURITY_ATTRIBUTES)NULL,
                              0,
                              (LPTHREAD_START_ROUTINE)eventThread,
                              (LPVOID)NULL,
                              0, &dwThreadID);
    if (NULL == uartThread) {
        isActive = FALSE;
        MassUart_Close();
        errMessage = "Couldn't create driver thread";
        return FALSE;
    }

    return TRUE;

} /* openConnection() */

/*---------------------------------------------------------------------------
 * restartRead() 
 *
 *      Begins the read process. The event handle in osRead will be
 *      posted when the operation is complete.
 */
static void restartRead(void)
{
    DWORD         dwThreadID;


    readLen = readPos = 0;

    ResetEvent(osRead.hEvent);


    Assert(readThread == NULL);
    ReportThread(("============CreatereadThread+\n"));

    readThread = CreateThread((LPSECURITY_ATTRIBUTES)NULL,
                              0,
                              (LPTHREAD_START_ROUTINE)readThreadFunc,
                              (LPVOID)NULL,
                              0, &dwThreadID);
    Assert(readThread);
    ReportThread(("============CreatereadThread-\n"));

}


#if (DUMP_UART_DATA == XA_ENABLED) && (XA_DEBUG == XA_ENABLED)
    #include "stdio.h"
    #define LINE_WIDTH 16
/*---------------------------------------------------------------------------
 * DisplayHex()
 *
 *     Debug output tool for displaying data bytes read from and written
 *     to the UART port.
 */
void DisplayHex(const char *Source, const U8 *Buffer, U16 Len)
{
    const U8 *p = Buffer;
    char     *o, output[(LINE_WIDTH * 4)+20]; /* 12 bytes padding */
    int       po;

    po = 0;
    while (p < Buffer+Len) {
        o = output;

        /* Append proper line advance spaces */
        o += sprintf(o, " %s   ", Source);

        /* Dumps the packet in basic ASCII dump format */
        po = 0;
        while (po < LINE_WIDTH) {
            if (p+po < Buffer+Len)
                o += sprintf(o, "%02x ", p[po]);
            else o += sprintf(o, "   ");
            if (++po == LINE_WIDTH/2) o += sprintf(o, "  ");
        }
        o += sprintf(o, "    ");

        po = 0;
        while (po < LINE_WIDTH) {
            if (p+po < Buffer+Len)
                o += sprintf(o, "%1c", ((p[po] >= ' ' && p[po] <= '~') ? p[po] : '.'));
            else break;
            po++;
        }

        Report(("%s\n", output));
        p += po;
    }
}
#endif /* (DUMP_UART_DATA == XA_ENABLED) && (XA_DEBUG == XA_ENABLED) */
