/***************************************************************************
 *
 * File:
 *     $Workfile:uart.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:88$
 *
 * Description:
 *      HCI Transport - UART physical driver.
 *      This file provides the interface between the UART HCI Transport 
 *      Driver that is platform independent and the controller or driver
 *      for the controller.
 *
 *      This particular driver uses Microsoft's VCOMM driver for accessing
 *      the Bluetooth controller.
 *
 * Copyright 1999-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "osapi.h"
#include "uart.h"
#include "windows.h"

#include "parseopts.h"

/****************************************************************************
 *
 * Constants
 *
 ****************************************************************************/

#ifdef DEMO
    #undef Report
    #define Report(S)
#endif

#ifndef DUMP_UART_DATA
    #define DUMP_UART_DATA   XA_ENABLED
#endif

/* ASCII character definitions */
#define ASCII_XON       0x11
#define ASCII_XOFF      0x13

/* Size of our read buffer. This can be any size larger than one. */
#define MAX_READ_BUFF   100

/* Mappings to NT API */
#define _fmemset   memset
#define _fmemmove  memmove

/****************************************************************************
 *
 * Global Data
 *
 ***************************************************************************/

/* Handle for communications port */
static HANDLE       commPort = NULL;

/* Indicates whether the port and thread are active */
static BOOL         isActive = FALSE;

/* Callback provided by the stack */
static UartCallback callback = NULL;

/* Number times the UART has been initialized */
static U8           initCount = 0;

/* Overlap structures for asynchronous Win32 file access */
static OVERLAPPED   osWrite = { 0};
static OVERLAPPED   osRead  = { 0};

/* Number of bytes written in last UART_Write operation */
static DWORD        lastWritten;

/* Input buffer to use when we're not sure if more data is coming */
static U8           readBuffer[MAX_READ_BUFF];
static DWORD        readLen; /* Length of data in readBuffer */
static DWORD        readPos; /* Current position in readBuffer */

/* Error message detected during initialization */
static char        *errMessage;

/* Handle for UART free running thread */
static HANDLE uartThread = NULL;

/* The Motorola radios require some special handling for power up and
 * the HCI RESET command. To initialize the stack reliably, the following
 * must be done:
 *
 * 1. Wait at least 2 seconds before issuing the first reset command.
 * 2. Discard all data from the serial port until the reset command
 *    has been issued.
 * 3. Discard all data from the serial port after a reset command until
 *    the reset command complete event packet is received.
 *
 * Step 2 and 3 are useful for other radios as well, so they are enabled
 * for all radios.
 */
static U8   resetOffset = 0;
static U8   resetComplete[] = { 0x04, 0x0E, 0x04 };
static enum {
    DISCARD_ALL, RESET, RESET_DONE
} resetState = DISCARD_ALL;

/* Used to indicate asynchronous shutdown */
static EvmTimer notifyShutdown;

/****************************************************************************
 *
 * Local Prototypes
 *
 ***************************************************************************/

static DWORD FAR PASCAL eventThread(LPSTR lpData);
static BOOL openConnection(void);
static BOOL setupConnection(U16 speed);
static void restartRead(void);
static BOOL internalInit(void);
static BOOL isRadioPresent(void);
void NotifyShutdown(EvmTimer *Timer);

#if (DUMP_UART_DATA == XA_ENABLED) && (XA_DEBUG == XA_ENABLED)
static void DisplayHex(const char *Source, const U8 *Buffer, U16 Len);
#else
    #define DisplayHex(S, B, L) (0)
#endif



/****************************************************************************
 *
 * Public Functions
 *
 ***************************************************************************/

/*---------------------------------------------------------------------------
 * UART_Init()
 *
 *     Called by the stack (during EVM_Init) to intialize the UART hardware
 *     driver.
 *
 * Returns:
 *     BT_STATUS_PENDING - Initialization is pending.
 *     BT_STATUS_FAILED - Initialization failed.
 */
BtStatus UART_Init(UartCallback func)
{
    initCount++;

    /* Setup special UART transport handling */
    if (getRadioType() != RADIO_TYPE_CSR_BCSP) {

        /* Note that we may issue the HCI RESET command multiple times
         * if the first attempt fails. Therefore, start the read process
         * all over again.
         */
        resetState = DISCARD_ALL;
        resetOffset = 0;

    } else {
        /* BCSP  doesn't need a this special handling. */
        resetState = RESET_DONE;
    }

    /* Initialize the uart */
    callback = func;
    if (internalInit()) {
        Report(("UART: Initializing\n"));
        return BT_STATUS_PENDING;
    } else {
        /* Display an error message for the user */
        MessageBox(NULL, errMessage, "UART Initialization Failed", MB_OK);
        UART_Shutdown();
        return BT_STATUS_FAILED;
    }
}

/* Helper function for UART_Init */
BOOL internalInit(void)
{
    /* Initialize the OVERLAPPED structures */
    memset(&osWrite, 0, sizeof(OVERLAPPED));
    memset(&osRead, 0, sizeof(OVERLAPPED));

    /* Create I/O events used for overlapped reads/writes */
    osRead.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
    if (osRead.hEvent == NULL) {
        errMessage = "Couldn't allocate resources (read semaphore)";
        return FALSE;
    }

    osWrite.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
    if (NULL == osWrite.hEvent) {
        errMessage = "Couldn't allocate resources (write semaphore)";
        return FALSE;
    }

    /* Now open the port */
    if (FALSE == openConnection()) {
        commPort = NULL;
        return FALSE;
    }

    /* Start the first read operation so that we'll be informed
     * of read events.
     */
    restartRead();

    return TRUE;
}


/*---------------------------------------------------------------------------
 * UART_Shutdown() 
 *      Release any resources, close the connection if open
 *
 * Requires:
 *     
 * Parameters:
 *
 * Returns:
 */
BtStatus NEAR UART_Shutdown(void)
{
    Report(("UART: Deinit start\n"));

    /* Force the connection to close */
    if (isActive) {
        isActive = FALSE;
    }

    Report(("UART: Deinitialized\n"));
    return BT_STATUS_PENDING;
}

/*---------------------------------------------------------------------------
 * UART_Read() 
 *      Read bytes from the comm port
 *
 * Requires:
 *     
 * Parameters:
 *      buffer  allocated buffer to receive the bytes
 *      length  max number of bytes to read
 *
 * Returns:
 *      number of bytes read
 */
U16 UART_Read(U8 *buffer, U16 length)
{
    U16 bytesToCopy;
    U16 result;

    /* If UART_Read is called and the readBuffer is empty, we know
     * there's a pending read operation (it may or may not be complete).
     */
    if (readPos == readLen) {
        result = GetOverlappedResult(commPort, &osRead, &readLen, FALSE);
        if (result==FALSE) {
            return 0;
        }
    }


    if (getRadioType() != RADIO_TYPE_CSR_BCSP) {

        /* Discard all data until after HCI RESET has been sent. */
        if ((resetState == RESET)
            &&  (memcmp(readBuffer + readPos, resetComplete + resetOffset,
                        min(readLen - readPos, sizeof(resetComplete) - resetOffset)) == 0)) {

            /* Found HCI RESET COMMAND COMPLETE event packet. Send
             * it up to the stack.
             */
            resetOffset += length;
            if (resetOffset >= sizeof(resetComplete)) {

                /* Complete RESET COMMAND COMPLETE event packet has been
                 * sent up the stack. Can now proceed as normal.
                 */
                resetState = RESET_DONE;
            }
        } else if (resetState != RESET_DONE) {

            /* Just discard all input until we've actually sent the HCI
             * RESET COMMAND and matched the expected event packet reply.
             */
            restartRead();
            return 0;
        }
    }

    /* Handle any available data */
    bytesToCopy = (U16)(min(readLen-readPos, length));
    memcpy(buffer, readBuffer+readPos, bytesToCopy);
    readPos += bytesToCopy;

    DisplayHex("RX:", buffer, (U16)bytesToCopy);

    if (readPos == readLen) {
        /* If the readBuffer is empty, wait for more data */
        restartRead();
    }


    // Report(("UART: Read %d bytes\n", bytesToCopy));

    return bytesToCopy;
} /* end of UART_Read() */


/*---------------------------------------------------------------------------
 * UART_Write() 
 *      Write a buffer to the comm port
 *
 * Requires:
 *     
 * Parameters:
 *      buffer  data to send
 *      length  bytes in buffer
 *
 * Returns:
 *      Number of bytes written. If an error occurs, 0 bytes are returned.
 */
U16 UART_Write(const U8 *buffer, U16 length)
{
    DWORD       bytesWritten;
    DWORD       error = 0;

    // Report(("UART: Write() %d bytes.\n", length));

    DisplayHex("TX:", buffer, (U16)length);

    /* Do a quick check to see if the radio is present */
    if (isRadioPresent()==FALSE) {
        return 0;
    }

    if (getRadioType() != RADIO_TYPE_CSR_BCSP) {
        if (resetState == DISCARD_ALL) {
            /* First write will be for the HCI RESET command so also begin
             * to search for the HCI RESET COMMAND COMPLETE event packet.
             */
            resetState = RESET;
        }
    }

    /* Write the data asynchronously.
     * WriteFile may return before completion.
     */
    //    Sleep(3);
    if (FALSE == WriteFile(commPort, buffer, length, &bytesWritten, 
                           &osWrite) ) {

        /* We expect FALSE because it's an overlapped operation */
        Assert(GetLastError()==ERROR_IO_PENDING);

    } else {
        error = GetLastError();
        return 0;
    }

    lastWritten = length;
    // Report(("UART: Wrote %d bytes\n", length));
    /* bytesWritten will be zero since the WriteFile has only
     * just started.
     */
    return length;
} /* End of UART_Write() */


/*---------------------------------------------------------------------------
 * UART_SetSpeed() 
 *     Called to change the speed of the UART.
 *
 * Requires:
 *
 * Parameters:
 *     speed - the speed (in KBps) at which the UART should operate (57 or 115)
 *
 * Returns:
 */
void UART_SetSpeed(U16 speed)
{
    setupConnection(speed);
}

/****************************************************************************
 *
 * Private Functions
 *
 ***************************************************************************/

/*---------------------------------------------------------------------------
 * eventThread() 
 *      This thread watches for COMM events.  When data is available 
 *      to be read, the interrupt handler is called with the appropriate
 *      event.
 *
 * Requires:
 *     
 * Parameters:
 *
 * Returns:
 *      TRUE    success
 *      FALSE   failed
 */
static DWORD FAR PASCAL eventThread(LPSTR lpData)
{
    DWORD result;
    HANDLE eventArray[2];

    /* Fill the event array with possible events */
    eventArray[0] = osWrite.hEvent;
    eventArray[1] = osRead.hEvent;

    /* Notify the transport that the UART is initialized */
    callback(UE_INIT_COMPLETE);

    /* Stay in this thread while still connected */
    while (isActive) {

        /* Wake up every now and then just in case a Deinit is requested */
        result = WaitForMultipleObjects(2, eventArray, FALSE, 100);

        /* Do not process the event until we are sure that a higher
         * layer is not between OS_StopHardware and OS_ResumeHardware. We
         * accomplish this by calling OS_StopHardware and OS_ResumeHardware
         * ourselves.
         */
        OS_StopHardware();

        if (callback == NULL) {
            return FALSE;
        }

        if (result == WAIT_OBJECT_0+1) {
            /* Data was received, notify upper layer */

            //Report(("UART: Read event\n"));
            /* Note: we do not reset the event, UART_Read code does this */
            callback(UE_DATA_TO_READ);

        } else if (result == WAIT_OBJECT_0) {
            /* The write is done, notify upper layer */

            /* We could check for errors using GetOverlappedResult, but there
             * are two problems with this. 1) It doesn't work at all in Win95
             * (other Win32 platforms OK) 2) Sometimes a write will complete
             * successfully even if the other device is not present. (Perhaps
             * due to certain null-modem cables automatically holding RTS high?)
             * So we're going to allow the transport layer to deal with this.
             */
            // Report(("UART: Write event\n"));
            ResetEvent(osWrite.hEvent);
            callback(UE_WRITE_COMPLETE);
        }

        OS_ResumeHardware();
    }

    /* Drop DTR */
    EscapeCommFunction(commPort, CLRDTR);

    /* Purge any outstanding reads/writes and close handles */
    PurgeComm(commPort, PURGE_TXABORT | PURGE_RXABORT |
              PURGE_TXCLEAR | PURGE_RXCLEAR);
    CloseHandle(commPort);    
    commPort = NULL;

    /* Close read and write event handles if necessary */
    if (osWrite.hEvent != NULL) {
        CloseHandle(osWrite.hEvent);
        osWrite.hEvent = NULL;
    }

    if (osRead.hEvent != NULL) {
        CloseHandle(osRead.hEvent);
        osRead.hEvent = NULL;
    }

    /* Start timer to send event to indicate shutdown complete */
    notifyShutdown.func = NotifyShutdown;
    notifyShutdown.context = (void *)1;
    EVM_StartTimer(&notifyShutdown, 1);
    return TRUE;

} /* End of eventThread() */

/*---------------------------------------------------------------------------
 * NotifyShutdown() 
 *      This timer handler notifies the transport that shutdown has completed.
 *      This event is scheduled when the I/O thread exits.
 *
 * Parameters:
 *      Event Manager timer structure.
 *
 * Returns:
 *      TRUE    success
 *      FALSE   failed
 */
void NotifyShutdown(EvmTimer *Timer)
{
    Assert(callback != NULL);
    callback(UE_SHUTDOWN_COMPLETE);
}

/*---------------------------------------------------------------------------
 * isRadioPresent()
 *
 *     Determines whether there is a device connected to our serial port.
 *
 * Returns:
 *      TRUE    Yes, a device was detected
 *      FALSE   No device is present
 */
static BOOL isRadioPresent(void)
{
    /* We could check the receive-line-signal-detect signal to determine
     * whether any hardware is connected. The signal should be high when the
     * connection is live. However, some null-modem cables don't seem to 
     * support this so we'll just return TRUE.
     *
     * DWORD lineStatus;
     * GetCommModemStatus(commPort, &lineStatus);
     * if ((lineStatus & MS_RLSD_ON)==0) {
     *    return FALSE;
     * }
     */
    return TRUE;
}

/*---------------------------------------------------------------------------
 * openConnection()
 *
 *     Opens the UART port and creates a thread to manage it.
 *
 * Returns:
 *      TRUE    success
 *      FALSE   failed; errMessage is set with a description of the error.
 */
static BOOL openConnection(void)
{
    char       portName[9];
    DWORD         dwThreadID;
    COMMTIMEOUTS  CommTimeOuts;

    /* Create the Comm port name */
    wsprintf(portName, "\\\\.\\COM%d", getPortNumOption());

    /* Open Comm port */
    if ((commPort =
         CreateFile(portName, GENERIC_READ | GENERIC_WRITE,
                    0,
                    NULL,
                    OPEN_EXISTING,
                    FILE_ATTRIBUTE_NORMAL |
                    FILE_FLAG_OVERLAPPED,
                    NULL)) == (HANDLE)-1) {

        errMessage = "Could not open port";
        return FALSE;
    } else {

        /* Get any early notifications */
        SetCommMask(commPort, EV_RXCHAR | EV_TXEMPTY);

        /* Setup device buffers */
        SetupComm(commPort, 4096, 4096);

        /* Purge any information in the buffer */
        PurgeComm(commPort, PURGE_TXABORT | PURGE_RXABORT |
                  PURGE_TXCLEAR | PURGE_RXCLEAR);

        /* Set up for overlapped I/O */
        CommTimeOuts.ReadIntervalTimeout = 10;
        CommTimeOuts.ReadTotalTimeoutMultiplier = 0;
        CommTimeOuts.ReadTotalTimeoutConstant = 0;
        CommTimeOuts.WriteTotalTimeoutMultiplier = 0;
        /* If a write operation takes more than 3 seconds, it's probably
         * an error.
         */
        CommTimeOuts.WriteTotalTimeoutConstant = 750; 
        SetCommTimeouts(commPort, &CommTimeOuts);
    }


    /* Try to apply appropriate port settings */
    if (FALSE==setupConnection(0)) {
        isActive = FALSE;
        CloseHandle(commPort);
        errMessage = "Could not apply port settings";
        return FALSE;
    }

    /* Make sure there's actually a radio present */
    if (isRadioPresent()==FALSE) {
        errMessage = "Port opened but no radio detected";
        return FALSE;
    }

    isActive = TRUE;

    /* Create a secondary thread to watch for an event. */
    uartThread = CreateThread((LPSECURITY_ATTRIBUTES)NULL,
                              0,
                              (LPTHREAD_START_ROUTINE)eventThread,
                              (LPVOID)NULL,
                              0, &dwThreadID);
    if (NULL == uartThread) {
        isActive = FALSE;
        CloseHandle(commPort);
        errMessage = "Couldn't create driver thread";
        return FALSE;
    }

    /* Set DTR */
    EscapeCommFunction(commPort, SETDTR);

    if (getRadioType() == RADIO_TYPE_MOTOROLA_UART) {

        /* Pause for at least 2 seconds to allow the Motorola radios
         * to complete their automatic powerup reset. Then purge any
         * data in the serial port buffers. This avoids a possible HCI
         * initialization failure due to the stack receiving extra HCI
         * event packets.
         */
        Sleep(2000);
        PurgeComm(commPort, PURGE_TXABORT | PURGE_RXABORT |
                  PURGE_TXCLEAR | PURGE_RXCLEAR);
    }

    return TRUE;

} /* openConnection() */


/*---------------------------------------------------------------------------
 * setupConnection() 
 *      Setup the DCB
 *
 * Requires:
 *     
 * Parameters:
 *
 * Returns:
 *      TRUE    success
 *      FALSE   failed
 */
static BOOL NEAR setupConnection(U16 speed)
{
    DCB        dcb;

    dcb.DCBlength = sizeof(DCB);
    GetCommState(commPort, &dcb);

    if (speed == 0) {
        /* Set the default speed (at initialization time) */
        if (!(initCount % 2)) {
            /* Since we don't know under what circumstances the UART
             * was initialized, the baud rate of the UART may have been
             * set back to the default, or left as specified on the
             * command line.  Alternate speed settings between the
             * default and the speed indicated on the command line.
             */
            speed = getSpeedOption();
            if (speed == 0) {
                speed = 115;
            }
        } else {
            switch (getRadioType()) {
            case RADIO_TYPE_MOTOROLA_UART:
                speed = 9;
                break;
            case RADIO_TYPE_ERICSSON_UART:
                speed = 57;
                break;
            case RADIO_TYPE_ST_MICRO_UART:
            case RADIO_TYPE_BROADCOM_UART:
            case RADIO_TYPE_INFINEON_UART:
                speed = 115;
                break;
            default:
                speed = getSpeedOption();
                if (speed == 0) {
                    speed = 115;
                }
                break;
            }
        }
    }

    /* Set the baud rate and data format */
    switch (speed) {
    
    case 9: /* Motorola radios powerup at 9600 baud. */
        dcb.BaudRate = CBR_9600;
        break;
    case 38:
        dcb.BaudRate = CBR_38400;
        break;
    case 57:
        dcb.BaudRate = CBR_57600;
        break;
    case 115:
        dcb.BaudRate = CBR_115200;
        break;
    case 128:
        dcb.BaudRate = CBR_128000;
        break;
    case 256:
        dcb.BaudRate = CBR_256000;
        break;
    default:
        dcb.BaudRate = speed * 100;
    } 

    Report(("UART:  Setting speed to %d\n", speed));

    if (getRadioType() == RADIO_TYPE_CSR_BCSP) {
        dcb.ByteSize = 8;
        dcb.Parity = EVENPARITY;
        dcb.StopBits = ONESTOPBIT;

        dcb.fOutxDsrFlow = 0;
        dcb.fDtrControl = DTR_CONTROL_ENABLE;
        dcb.fOutxCtsFlow = 0;
        dcb.fRtsControl = RTS_CONTROL_ENABLE;
        dcb.XonLim = 100;
        dcb.XoffLim = 100;

        /* Set the software flow control */
        dcb.fInX = dcb.fOutX = 0;
        dcb.XonChar = ASCII_XON;
        dcb.XoffChar = ASCII_XOFF;

        /* Set other stuff */
        dcb.fBinary = TRUE;
        dcb.fParity = TRUE;

        dcb.fDsrSensitivity = 0;
        dcb.fNull = 0;
        dcb.fAbortOnError = 0;

    } else {

        dcb.ByteSize = 8;
        dcb.Parity = NOPARITY;
        dcb.StopBits = ONESTOPBIT;

        dcb.fOutxDsrFlow = 0;
        dcb.fDtrControl = DTR_CONTROL_ENABLE;
        dcb.fOutxCtsFlow = 1;
        dcb.fRtsControl = RTS_CONTROL_HANDSHAKE;
        dcb.XonLim = 100;
        dcb.XoffLim = 100;

        /* Set the software flow control */
        dcb.fInX = dcb.fOutX = 0;
        dcb.XonChar = ASCII_XON;
        dcb.XoffChar = ASCII_XOFF;

        /* Set other stuff */
        dcb.fBinary = TRUE;
        dcb.fParity = TRUE;

        dcb.fDsrSensitivity = 0;
        dcb.fNull = 0;
        dcb.fAbortOnError = 0;
    }

    return SetCommState(commPort, &dcb);
}

/*---------------------------------------------------------------------------
 * restartRead() 
 *
 *      Begins the read process. The event handle in osRead will be
 *      posted when the operation is complete.
 */
static void restartRead(void)
{
    readLen = readPos = 0;

    ResetEvent(osRead.hEvent);

    if (ReadFile(commPort, readBuffer, 
                 MAX_READ_BUFF, &readLen, &osRead)) {
        /* If ReadFile returned true, then the read was 
         * successful (no wait is required). Immediately
         * request a read event to be sent to the transport.
         */
        SetEvent(osRead.hEvent);
        readPos = 0;
    } else {
        /* Otherwise, this was an overlapped read, and the
         * hEvent will be fired when bytes show up.
         */
        Assert(GetLastError() == ERROR_IO_PENDING);
    }
}


#if (DUMP_UART_DATA == XA_ENABLED) && (XA_DEBUG == XA_ENABLED)
    #include "stdio.h"
    #define LINE_WIDTH 16
/*---------------------------------------------------------------------------
 * DisplayHex()
 *
 *     Debug output tool for displaying data bytes read from and written
 *     to the UART port.
 */
void DisplayHex(const char *Source, const U8 *Buffer, U16 Len)
{
    const U8 *p = Buffer;
    char     *o, output[(LINE_WIDTH * 4)+20]; /* 12 bytes padding */
    int       po;

    po = 0;
    while (p < Buffer+Len) {
        o = output;

        /* Append proper line advance spaces */
        o += sprintf(o, " %s   ", Source);

        /* Dumps the packet in basic ASCII dump format */
        po = 0;
        while (po < LINE_WIDTH) {
            if (p+po < Buffer+Len)
                o += sprintf(o, "%02x ", p[po]);
            else o += sprintf(o, "   ");
            if (++po == LINE_WIDTH/2) o += sprintf(o, "  ");
        }
        o += sprintf(o, "    ");

        po = 0;
        while (po < LINE_WIDTH) {
            if (p+po < Buffer+Len)
                o += sprintf(o, "%1c", ((p[po] >= ' ' && p[po] <= '~') ? p[po] : '.'));
            else break;
            po++;
        }

        Report(("%s\n", output));
        p += po;
    }
}
#endif /* (DUMP_UART_DATA == XA_ENABLED) && (XA_DEBUG == XA_ENABLED) */
