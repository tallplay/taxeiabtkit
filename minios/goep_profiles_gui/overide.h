#ifndef __OVERIDE_H
#define __OVERIDE_H

/****************************************************************************
 *
 * File:
 *     $Workfile$ for XTNDAccess Blue SDK, Version 1.0
 *     $Revision$
 *
 * Description:
 *     Configuration overrides for the qbeam project.
 * 
 * Copyright 1999-2005 Extended Systems, Inc.  ALL RIGHTS RESERVED.
 *
 * Unpublished Confidential Information of Extended Systems, Inc.
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of Extended Systems, Inc.
 * 
 * Use of this work is governed by a license granted by Extended Systems,
 * Inc.  This work contains confidential and proprietary information of
 * Extended Systems, Inc. which is protected by copyright, trade secret,
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

/*
 * Enable the desired stacks to run with OBEX. 
 *
 *     Since OBEX can be configured to run over a mix of stacks. The
 *     control of which stacks are included has been moved to the
 *     individual workspaces where it can be better managed.
 */
#ifdef BTSTACK
d
#define BT_STACK                        XA_ENABLED
#else
d
#define BT_STACK                        XA_DISABLED
#endif

#ifdef TCPSTACK
#define TCP_STACK                       XA_ENABLED
#else
#define TCP_STACK                       XA_DISABLED
#endif

#ifdef IRDASTACK
#define IRDA_STACK                      XA_ENABLED
#else
#define IRDA_STACK                      XA_DISABLED
#endif

#define XA_LOAD_LIST                    XA_MODULE(OBEX) XA_MODULE(GOEP) XA_MODULE(OPUSH) XA_MODULE(FTP)

#define OPUSH_EXPANDED_API              XA_ENABLED
#define FTP_EXPANDED_API                XA_ENABLED
#define OBEX_SERVER_CONS_SIZE           1

/* GOEP Profiles uses the Event Manager to initialize OBEX. 
 */
#define XA_EVENTMGR                     XA_ENABLED

/*
 * Enable Server Header build funcs so the server can send a LENGTH header
 * in response to get request. Enable Client Header parse funcs so the Client
 * can parse the received LENGTH header.
 */
#define OBEX_SERVER_HEADER_BUILD        XA_ENABLED
#define OBEX_CLIENT_HEADER_PARSE        XA_ENABLED

#define OBEX_DEINIT_FUNCS               XA_ENABLED


#define OBEX_PACKET_FLOW_CONTROL        XA_ENABLED

#define OBEX_AUTHENTICATION             XA_ENABLED
#define OBEX_MAX_REALM_LEN              20
#define OBEX_DYNAMIC_OBJECT_SUPPORT     XA_ENABLED
#define OBEX_DEFERRED_PROVIDE_OBJECT    XA_ENABLED

#define GOEP_DOES_UNICODE_CONVERSIONS   XA_ENABLED /* Optional */

/*
 * IrDA specific options required.
 */
#if IRDA_STACK == XA_ENABLED
#include <frmconfig.h>
#define IR_CONS_SIZE                    6       /* Space for 2 OBEX clients/servers */
#define IR_IAS_SERVER_SIZE              5       /* IAS object space for both servers */
#define IR_ADD_MS_TTP_PATCH             XA_ENABLED
#define IR_DISC_THRESHOLD               IRDT_40 /* 40 second link disconnect time */
#define IR_LMP_DISCONNECT               XA_ENABLED
#endif

/*
 * Bluetooth optional options.
 */
#if BT_STACK == XA_ENABLED
#define XA_SNIFFER                      XA_ENABLED
#define L2CAP_MTU                       (672*3)     /* Optional: Triple L2CAP PDU size */

/* Only needed for the USB transport. */
#define HCI_USB_TX_BUFFER_SIZE          260

#endif

#endif /* __OVERIDE_H */
