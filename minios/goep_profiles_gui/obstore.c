/****************************************************************************
 *
 * File:        obstore.c
 *
 * Description: This file contains the code for the Object store used in 
 *              the GOEP Profiles application. 
 *
 * Created:     May 9, 1997
 *
 * $Project:XTNDAccess IrDA SDK$
 *
 * Copyright 1997-2005 Extended Systems, Inc.  ALL RIGHTS RESERVED.
 *
 * Unpublished Confidential Information of Extended Systems, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of Extended Systems, Inc.
 * 
 * Use of this work is governed by a license granted by Extended Systems,
 * Inc.  This work contains confidential and proprietary information of
 * Extended Systems, Inc. which is protected by copyright, trade secret,
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include <io.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <osapi.h>
#include <obstore.h>
#include <io.h>
#include <direct.h>

extern void App_Progress(U32 barId, U32 currPos, U32 maxPos);
/****************************************************************************
 *
 * Function prototypes
 *
 ****************************************************************************/

static char *GetTmpFile(void);
static void  FreeTmpFile(const char* file);
static BOOL  SwitchToInbox(void);

#define SwitchBack()   _chdir(curDir)

/****************************************************************************
 *
 * Local ROM data
 *
 ****************************************************************************/

static const char* tmpFile1 = "obp15378.tmp";
static const char* tmpFile2 = "obp15379.tmp";
static const char* inboxDir = "\\qbinbox";
static const char* defaultName = "index.txt";
static const char* defaultRxName = "rxqbeam.dft";
static const char* defaultVCardName = "mycard.vcf";

/****************************************************************************
 *
 * Local RAM data
 *
 ****************************************************************************/
static ObStoreEntry obsEntries[OBS_MAX_NUM_ENTRIES];
static ListEntry    obsList;
 
/* QBeam Inbox directory. Can be changed by calls to Update/ApplyPath */
static char         obsInbox[OBS_MAX_PATH_LEN];
static BOOL         obsInboxUnicodeOdd;
static U16          obsInboxLen;
static U16          obsInboxOldLen;

/* Temporary directory used to keep track of current dir when switching */
static char         curDir[OBS_MAX_PATH_LEN];

/* Flags indicating which temp files are in use */
static U8           tmpInUse;

/* Flags that define each temp file */
#define OB_TMP_FILE1 0x01
#define OB_TMP_FILE2 0x02

/*---------------------------------------------------------------------------
 *            OBSTORE_Init()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initialize the object store.
 *
 * Return:    TRUE - object store initialized.
 *            FALSE - could not create inbox.
 *
 */
BOOL OBSTORE_Init(void)
{
    U8 i;

    /* If the inbox does not exist then create it */
    if (_chdir("\\QbInbox") && _mkdir("\\QbInbox")) {
        printf("QBeam Server: Unable to set initial directory, error %d.\n", errno);
        _chdir("\temp");
    }
  
    /* Initialize the inbox directory */
    strcpy( obsInbox, inboxDir );
    obsInboxLen = (U16)strlen( obsInbox );
    obsInboxUnicodeOdd = FALSE;

    /* Initialize the object store entries */
    InitializeListHead(&obsList);

    for (i = 0; i < OBS_MAX_NUM_ENTRIES; i++) {
        InsertTailList(&obsList, &(obsEntries[i].node));
    }

    tmpInUse = 0;
    return TRUE;

} /* End of Obs_Init() */


/*---------------------------------------------------------------------------
 *            OBSTORE_New()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Obtain an object store item.
 *
 * Return:    Handle of object store or 0 if no more objects store entries.
 *
 */
ObStoreHandle OBSTORE_New(void)
{
    ObStoreHandle obs;

    obs = 0;
    if (!IsListEmpty(&obsList)) {
        obs = (ObStoreHandle) RemoveHeadList(&obsList);
        obs->fp = 0;
        obs->flags = 0;
        obs->nameLen = 0;
        obs->name[0] = 0;
        obs->typeLen = 0;
        obs->type[0] = 0;
        obs->curNamePos = obs->name;
        obs->fileLen = 0;
        obs->amount = 0;
        obs->tmpFile = 0;
    } else {
        DebugPrint(("OBSTORE_New: Out of entries\n"));
    }
    return obs;

} /* End of OBSTORE_New() */


/*---------------------------------------------------------------------------
 *            OBSTORE_Create()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Open an object store item for writing.
 *
 * Return:    Success or failure of create operation.
 *
 */
ObexRespCode OBSTORE_Create(ObStoreHandle obs, BOOL useInbox)
{
    char *fileName;

    Assert( obs );
    
    if (useInbox) {
        if (!SwitchToInbox()) {
            return OBRC_NOT_FOUND;
        }
        obs->flags |= OBSEF_IN_INBOX;
    }
        
    if (strlen(obs->name) == 0) {
        /* Create a tmp file */
        obs->tmpFile = GetTmpFile();
        fileName = obs->tmpFile;
    } else {
        /* Create file with the specified name */
        fileName = obs->name;
    }
    
    obs->fp = fopen(fileName, "wb");
    
    if (useInbox) {
        SwitchBack();
    }
        
    if (obs->fp == 0) {
        printf("OBSTORE:Error creating %s\n", fileName);
        obs->flags = 0;
        return OBRC_NOT_FOUND;
    }
    
    return OBRC_SUCCESS;
} /* End of OBSTORE_Create() */

/*---------------------------------------------------------------------------
 *            OBSTORE_Open()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Open and object store item for reading. If a name is not set
 *            the default object is opened.
 *
 * Return:    OBRC_SUCCESS - Open is successful. 
 *            OBRC_NOT_FOUND - Failed to open file.
 *
 */
ObexRespCode OBSTORE_Open(ObStoreHandle obs, BOOL useInbox)
{
    int handle;
    
    Assert( obs );

    if (strlen(obs->name) == 0) {
        /* There is no name so set the name to the default object */
        if (strlen(obs->type)) {
            /* Use type to determine local name for object */
            if (_stricmp(obs->type, "text/x-vCard") == 0) {
                /* Open the default vCard */
                OBSTORE_AppendNameAscii(obs, defaultVCardName, (U16)strlen(defaultVCardName));
            } else {
                /* Default nameless, typeless object. */
                OBSTORE_AppendNameAscii(obs, defaultName, (U16)strlen(defaultName));
            }
        } else {
            /* Default nameless, typeless object. */
            OBSTORE_AppendNameAscii(obs, defaultName, (U16)strlen(defaultName));
        }

        obs->flags |= OBSEF_DEFAULT_OBJECT;
    }

    if (useInbox) {
        /* File is in the inbox, save where we are currently */
        if (!SwitchToInbox()) {
            return OBRC_NOT_FOUND;
        }
        obs->flags |= OBSEF_IN_INBOX;
    }
    obs->fp = 0;

    /* First obtain the file length. */
    handle = _open(obs->name, _O_RDONLY);
    if (handle == -1) {
        goto open_err;
    }

    /* Get the length of the file */
    obs->fileLen = _filelength(handle);

    /* Close the handle */
    _close(handle);

    if (obs->fileLen == -1L) {
        goto open_err;
    }

    /* Open the file for reading */
    obs->fp = fopen(obs->name, "a+b");      /* Read & Append at end */
    
open_err:
    if (useInbox) {
        SwitchBack();
    }
    
    if (obs->fp == 0) {
        return OBRC_NOT_FOUND;
    } 
    
    return OBRC_SUCCESS;

} /* End of OBSTORE_Open() */

#if GOEP_DOES_UNICODE_CONVERSIONS == XA_DISABLED
/*---------------------------------------------------------------------------
 *            OBSTORE_AppendNameUnicode()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Append the unicode string to the name. A state machine is used
 *            so the unicode string does not have to be an even number of
 *            bytes.
 *
 * Return:    OBRC_SUCCESS      - Appended Name successfully.
 *            OBRC_UNAUTHORIZED - Unicode Name did not fit in buffer.
 *
 */
ObexRespCode OBSTORE_AppendNameUnicode(ObStoreHandle obs, const U8 *name, U16 len)
{
    U16 finalLen;
    U16 i;

    Assert( obs );

    if (len == 0) {
        return OBRC_SUCCESS;
    }
    
    /* Check if the appended name would overflow our buffer space. */
    if ((obs->nameLen + (len/2)) > (OBS_MAX_NAME_LEN - 1)) {
        return OBRC_UNAUTHORIZED;
    }

    /* Append the Unicode string to the existing name. We need to
     * remove the first byte of each byte pair.
     */
    finalLen = min((OBS_MAX_NAME_LEN - 1) - obs->nameLen, len);
    for (i = obs->nameLen; finalLen > 0; finalLen--) {
        if (obs->flags & OBSEF_ANU_UNICODE_ODD) {
            /* This is the byte to store */
            obs->name[i] = *name;
            i++;
            obs->flags &= ~OBSEF_ANU_UNICODE_ODD;
        } else {
            /* Skip the 0 */
            obs->flags |= OBSEF_ANU_UNICODE_ODD;
        }
        name++;
    }

    if (i > 0) {

        /* Point i to the last character written */
        i--;

        /* Put a 0 at the end of the name if one does not exists */
        if (obs->name[i] != 0) {
            i++;
            obs->name[i] = 0;
        }
        /* Update nameLen. It does not include the 0 */
        obs->nameLen = i;
    }    
    
    return OBRC_SUCCESS;
} /* End of OBSTORE_AppendNameUnicode() */
#endif /* GOEP_DOES_UNICODE_CONVERSIONS == XA_DISABLED */

/*---------------------------------------------------------------------------
 *            OBSTORE_AppendNameAscii()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Append the ASCII string to the name. 
 *
 * Return:    void
 *
 */
void OBSTORE_AppendNameAscii(ObStoreHandle obs, const char* name, U16 len)
{
    U16 finalLen;
    U16 i;

    if ((obs == 0) || (len == 0)) {
        return;
    }
  
    /* Append the ASCII string to the existing ASCII name. */
    finalLen = min((OBS_MAX_NAME_LEN - 1) - obs->nameLen, len);
    OS_MemCopy((U8 *)(obs->name + obs->nameLen), (U8 *)name, finalLen);
    
    i = obs->nameLen + finalLen;

    /* Put a 0 at the end of the name if one does not exists */
    if (obs->name[i-1] != 0) {
        obs->name[i] = 0;
        i++;
    }
    /* Update nameLen. It does not include the 0 */
    obs->nameLen = (i - 1);
} /* End of OBSTORE_AppendNameAscii() */

/*---------------------------------------------------------------------------
 *            OBSTORE_AppendType()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Append the ASCII string to the object type field.
 *
 * Return:    void
 *
 */
void OBSTORE_AppendType(ObStoreHandle obs, const U8 *type, U16 len)
{
    U16 copyLen;
    U16 i;

    if ((obs == 0) || (len == 0)) {
        return;
    }
  
    /* Append the ASCII string to the existing ASCII name. */
    copyLen = min((OBS_MAX_TYPE_LEN - 1) - obs->typeLen, len);
    OS_MemCopy((U8 *)(obs->type + obs->typeLen), type, copyLen);
    
    i = obs->typeLen + copyLen;

    /* Put a 0 at the end of the type if one does not exist */
    if (obs->type[i-1] != 0) {
        obs->type[i] = 0;
        i++;
    }
    /* Update typeLen. It does not include the 0 */
    obs->typeLen = (i - 1);
} /* End of OBSTORE_AppendType() */

/*---------------------------------------------------------------------------
 *            OBSTORE_GetObjectLen()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Get the length of an object store item.
 *
 * Return:    The length of the object in bytes.
 *
 */
U32 OBSTORE_GetObjectLen(void *obs)
{
    return ((ObStoreHandle)obs)->fileLen;
} /* End of OBSTORE_GetObjectLen() */

/*---------------------------------------------------------------------------
 *            OBSTORE_Write()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Write data to the given object store item.
 *
 * Return:    OBRC_SUCCESS -      Buffer was written successfully.
 *            OBRC_UNAUTHORIZED - Buffer was not written.
 *
 */
ObexRespCode OBSTORE_Write(void *obs, U8 *buff, U16 len)
{
    ObStoreHandle obsh = (ObStoreHandle)obs;
    ObexRespCode ret = OBRC_UNAUTHORIZED;

    if (obsh != 0) {
        Assert(obsh->fp != 0);
        if (fwrite(buff, 1, len, obsh->fp) == len) {
            if (obsh->amount == 0) {
                App_Progress(obsh->uiContext, obsh->amount, obsh->fileLen);
                printf("Writing %10lu ",obsh->amount);
            }
            obsh->amount += len;
            printf("\b\b\b\b\b\b\b\b\b\b\b%10lu ", obsh->amount);
            ret = OBRC_SUCCESS;
        } else {
            printf("Error writing to file\n");
        }
    }

    App_Progress(obsh->uiContext, obsh->amount, obsh->fileLen);
    return ret;

} /* End of OBSTORE_Write() */

/*---------------------------------------------------------------------------
 *            OBSTORE_Read()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Read data from the given object store item.
 *
 * Return:    OBRC_SUCCESS -      Buffer was read successfully.
 *            OBRC_UNAUTHORIZED - Buffer was not read.
 *
 */
ObexRespCode OBSTORE_Read(void *obs, U8* buff, U16 len)
{
    ObStoreHandle obsh = (ObStoreHandle)obs;
    ObexRespCode ret = OBRC_UNAUTHORIZED;

    if (obsh != 0) {
        Assert(obsh->fp != 0);
        if (fread(buff, 1, len, obsh->fp) == len) {
            if (obsh->amount == 0) {
                App_Progress(obsh->uiContext, obsh->amount, obsh->fileLen);
                printf("Reading %10lu ",obsh->amount);
            }
            obsh->amount += len;
            printf("\b\b\b\b\b\b\b\b\b\b\b%10lu ", obsh->amount);
            ret = OBRC_SUCCESS;
        } else {
            printf("Error reading from file! len %d\n", len);
        }
    }

    App_Progress(obsh->uiContext, obsh->amount, obsh->fileLen);
    return ret;

} /* End of OBSTORE_Read() */

/*---------------------------------------------------------------------------
 *            OBSTORE_Delete()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Delete the given object store item. Item can be in the 
 *            current OBEX directory or the Inbox. The Object store
 *            handle is then returned to the pool.
 *
 * Return:    OBRC_SUCCESS -   File was deleted.
 *            OBRC_NOT_FOUND - File was not deleted.
 *
 */
ObexRespCode OBSTORE_Delete(ObStoreHandle *obsPtr, BOOL useInbox)
{
    ObexRespCode  ret = OBRC_NOT_FOUND;
    ObStoreHandle obs = *obsPtr;

    Assert (obs);
    
    /* Close the file if is open */
    if (obs->fp != 0) {
        fclose( obs->fp );

        /* Print a newline */
        printf("\n");
    }

    if (useInbox || obs->flags & OBSEF_IN_INBOX) {
        /* File is not in the inbox, save where we are currently */
        if (!SwitchToInbox()) {
            return OBRC_NOT_FOUND;
        }
    }
    
    if (obs->tmpFile != 0) {
        /* The Object Store entry has the tmp file name. So delete that file.*/
        if (remove(obs->tmpFile) == 0)
            ret = OBRC_SUCCESS;
    } else {
        if (remove(obs->name) == 0)
            ret = OBRC_SUCCESS;
    }
    
    if (useInbox || obs->flags & OBSEF_IN_INBOX) {
        SwitchBack();
    }
    
    /* If the item has a tmp file then free it */
    FreeTmpFile(obs->tmpFile);

    /* Free the object */
    InsertTailList(&obsList, &(obs->node));

    *obsPtr = 0;
    return ret;
} /* End of OBSTORE_Delete() */

/*---------------------------------------------------------------------------
 *            OBSTORE_Close()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Close the object store item and return the handle to the pool.
 *
 * Return:    void 
 *
 */
ObexRespCode OBSTORE_Close(ObStoreHandle *obsPtr)
{
    ObexRespCode  rcode = OBRC_SUCCESS;
    ObStoreHandle obs = *obsPtr;

    Assert(obs);

    if (obs->fp != 0) {
        fclose(obs->fp);
        obs->fp = 0;

        /* Print a newline */
        printf("\n");

        /* If the file is a temporary and a new name is set, rename it. */
        if ((obs->tmpFile != 0) && (obs->nameLen > 0)) {
    
            if (obs->flags & OBSEF_IN_INBOX) {
                /* File is in the inbox */
                SwitchToInbox();
            }
    
            /* If the new file already exists remove it then rename the tmp file */
            remove(obs->name);
            
            if (rename(obs->tmpFile, obs->name) != 0) {
                
                printf("Unable to rename file from %s to %s\n", obs->tmpFile, 
                                                                obs->name);
                /* Remove the tempfile and return an error */
                remove(obs->tmpFile);
                
                rcode = OBRC_UNAUTHORIZED;
            }
            
            if (obs->flags & OBSEF_IN_INBOX) {
                SwitchBack();
            }
        }
    }
    FreeTmpFile(obs->tmpFile);
    
    /* Return the ObStoreHandle to the pool */
    InsertTailList(&obsList, &(obs->node));
    
    *obsPtr = 0;
    return rcode;
} /* End of OBSTORE_Close() */

/*---------------------------------------------------------------------------
 *            OBSTORE_SetNameDefault()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Set the name of the Object store entry to the default name
 *            for a received object that has no name.
 *
 * Return:    void 
 *
 */
void OBSTORE_SetNameDefault(ObStoreHandle obs)
{
    /* Set the default name */
    OBSTORE_AppendNameAscii(obs, defaultRxName, (U16)strlen(defaultRxName));
    obs->flags |= OBSEF_DEFAULT_OBJECT;
} /* End of OBSTORE_SetNameDefault() */

#if GOEP_DOES_UNICODE_CONVERSIONS == XA_DISABLED
/*---------------------------------------------------------------------------
 *            OBSTORE_ReadNameUnicode()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Copy the Object Store item's name into the provided buffer
 *            in Unicode. Copied name is null terminated. If parameter
 *            First is TRUE copy is started at beginning of name, otherwise
 *            the copy continues where it left off.
 *
 * Return:    Number of bytes copied including the null terminator.
 *
 */
U16 OBSTORE_ReadNameUnicode(ObStoreHandle obs, U8 *buffer, U16 len, BOOL first)
{
   U16  i = 0;

   if ((obs == 0) || (len == 0)) {
      return 0;
   }

   Assert( buffer != NULL );
   
   if (first) {
      obs->curNamePos = obs->name;
      obs->flags &= ~OBSEF_RNU_UNICODE_ODD;
   }
   
   while ( i < len ) {
      
      if (obs->flags & OBSEF_RNU_UNICODE_ODD) {
         
         /* Check if were at the end of the name yet */
         if (*obs->curNamePos == 0) {
            /* Were at the end of the name and we've written the even byte
             * of the Unicode Null Terminator, write the odd byte and exit.
             */
            buffer[i++] = 0;
            break;
         }

         /* Unicode odd bytes are the ASCII byte. */
         buffer[i++] = *obs->curNamePos++;
         obs->flags &= ~OBSEF_RNU_UNICODE_ODD;
      } else {
         /* Unicode even bytes are Zero in ASCII */
         obs->flags |= OBSEF_RNU_UNICODE_ODD;
         buffer[i++] = 0;
      }
      
   }
   
   return i;
} /* End of OBSTORE_ReadNameUnicode() */
#endif /* GOEP_DOES_UNICODE_CONVERSIONS == XA_DISABLED */

/*---------------------------------------------------------------------------
 *            OBSTORE_AppendPathUnicode()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Append the path provided to the current OBEX Server path. 
 *            The path provided is in Unicode.
 *
 * Return:    OBRC_SUCCESS      - The path was successfully appended.
 *            OBRC_UNAUTHORIZED - Unable to append the path.
 *
 */
#if GOEP_DOES_UNICODE_CONVERSIONS == XA_DISABLED
ObexRespCode OBSTORE_AppendPathUnicode(const U8 *path, U16 len, BOOL first)
{
    /* Each new path must be separated from the current path by '\' 
     * Record the current inbox length in case we need to roll back
     * requested changes.
     */
    if (first) {
        obsInboxOldLen = obsInboxLen;
        obsInbox[obsInboxLen++] = '\\';
        obsInbox[obsInboxLen] = 0;
    }
    
    if (len == 0) {
        return OBRC_SUCCESS;
    }

    /* Check if the appended name is going to overflow our buffer. */
    if ((obsInboxLen + len) > (OBS_MAX_PATH_LEN - 1)) {
        return OBRC_UNAUTHORIZED;
    }
    
    /* Convert Unicode path to ASCII. We ignore the odd Unicode
     * byte, it should be 0. The final string is null terminated.
     */
    while (len > 0) {
        if (obsInboxUnicodeOdd == TRUE) {
            obsInbox[obsInboxLen] = *path;
            obsInboxLen++;
            obsInboxUnicodeOdd = FALSE;
        } else {
            obsInboxUnicodeOdd = TRUE;
        }
        path++;
        len--;
    }
    
    /* Put a NULL at the end of the inbox if one does not exist. */
    if (obsInbox[obsInboxLen-1] != 0) {
        obsInbox[obsInboxLen] = 0;
    } else obsInboxLen--;
    
    return OBRC_SUCCESS;
} /* End of OBSTORE_AppendPathUnicode() */
#else /* GOEP_DOES_UNICODE_CONVERSIONS == XA_DISABLED */
/*---------------------------------------------------------------------------
 *            OBSTORE_AppendPathAscii()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Append the path provided to the current OBEX Server path. 
 *            The path provided is in Ascii.
 *
 * Return:    OBRC_SUCCESS      - The path was successfully appended.
 *            OBRC_UNAUTHORIZED - Unable to append the path.
 *
 */
ObexRespCode OBSTORE_AppendPathAscii(const U8 *path, U16 len, BOOL first)
{
    /* Each new path must be separated from the current path by '\' 
     * Record the current inbox length in case we need to roll back
     * requested changes.
     */
    if (first) {
        obsInboxOldLen = obsInboxLen;
        obsInbox[obsInboxLen++] = '\\';
        obsInbox[obsInboxLen] = 0;
    }
    
    if (len == 0) {
        return OBRC_SUCCESS;
    }

    /* Check if the appended name is going to overflow our buffer. */
    if ((obsInboxLen + len) > (OBS_MAX_PATH_LEN - 1)) {
        return OBRC_UNAUTHORIZED;
    }
    
    /* The final string is null terminated. */
    OS_MemCopy(obsInbox+obsInboxLen, path, len);
    obsInboxLen += len;

    /* Put a NULL at the end of the inbox if one does not exist. */
    if (obsInbox[obsInboxLen-1] != 0) {
        obsInbox[obsInboxLen] = 0;
    } else obsInboxLen--;
    
    return OBRC_SUCCESS;
} /* End of OBSTORE_AppendPathAscii() */
#endif /* GOEP_DOES_UNICODE_CONVERSIONS == XA_DISABLED */

/*---------------------------------------------------------------------------
 *            OBSTORE_UpdatePath()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Apply path changes.
 *
 * Return:    OBRC_SUCCESS      - The path was successfully updated.
 *            OBRC_NOT_FOUND    - Unable to change to updated path.
 *            OBRC_UNAUTHORIZED - Refused to change to updated path.
 *
 */
ObexRespCode OBSTORE_UpdatePath(U8 flags, BOOL update)
{
    
    /* The Unicode conversion should be in a stable state. */
    if (obsInboxUnicodeOdd == TRUE) {
        printf("OBSTORE: Received Unicode Pathname is malformed.\n");
        return OBRC_UNAUTHORIZED;
    }
    
    /* Apply backup command if present. */
    if (flags & OBS_UPU_BACKUP) {
        
        if (obsInboxLen == strlen(inboxDir)) {
            printf("OBSTORE: Unable to backup, currently at inbox root.\n");
            return OBRC_UNAUTHORIZED;
        }
        
        while( TRUE ) {
            --obsInboxLen;
            if (obsInbox[obsInboxLen] == '\\') {
                obsInbox[obsInboxLen] = 0;
                break;
            }
        }
    }
    
    /* Apply new directory if requested */
    if (update) {
       /* Save off current directory, we just apply the change to test
         * its validity, then we reset the directory to the current one.
         */
        if (_getcwd(curDir, OBS_MAX_PATH_LEN) != 0) {
            
            /* Try to change dirs, if it fails try to create the dir */
            if (_chdir(obsInbox) != 0) {

                if ((flags & OBS_UPU_DONT_CREATE) ||
                    (_mkdir(obsInbox) != 0 || _chdir(obsInbox) != 0)) {

                    printf("OBSTORE: Unable to change to directory \"%s\".\n", obsInbox);
                    
                    /* Reset to prior inbox */
                    obsInboxLen = obsInboxOldLen;
                    obsInbox[obsInboxLen] = 0;
                    
                    return OBRC_NOT_FOUND;
                }
            }
            /* Return to current directory */
            SwitchBack();
        
        } else {
            /* Unable to save current directory, path was not applied */

            obsInboxLen = obsInboxOldLen;
            obsInbox[obsInboxLen] = 0;
            return OBRC_UNAUTHORIZED;
        }
    }
    
    printf("OBSTORE: Current directory \"%s\"\n", obsInbox);
    return OBRC_SUCCESS;
} /* End of OBSTORE_UpdatePath() */

/*---------------------------------------------------------------------------
 *            OBSTORE_ResetPath()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Reset the inbox to its root.
 *
 * Return:    void
 *
 */
void OBSTORE_ResetPath(void)
{
    /* Reset to the default inbox dir. */
    strcpy( obsInbox, inboxDir );
    obsInboxLen = (U16)strlen( obsInbox );
    obsInboxUnicodeOdd = FALSE;
} /* End of OBSTORE_ResetPath() */

/*---------------------------------------------------------------------------
 *            OBSTORE_CancelPathUpdate()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Reset the inbox to its pre-AppendPathUnicode() setting.
 *
 * Return:    void
 *
 */
void OBSTORE_CancelPathUpdate(void)
{
    /* Reset to prior inbox */
    obsInboxLen = obsInboxOldLen;
    obsInbox[obsInboxLen] = 0;
} /* End of OBSTORE_CancelPathUpdate() */

/*---------------------------------------------------------------------------
 *            SwitchToInbox()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Change the current directory to the Inbox.
 *
 * Return:    TRUE  - Change dir was successful.
 *            FALSE - Unable to change dir.
 *
 */
static BOOL SwitchToInbox(void)
{
    /* File is in the inbox, save where we are currently */
    if (_getcwd(curDir, OBS_MAX_PATH_LEN) == 0) {
        return FALSE;
    }
            
    /* and reset to the inbox. */        
    if (_chdir(obsInbox) != 0) {
        return FALSE;
    }
    
    return TRUE;
} /* End of SwitchToInbox() */

/*---------------------------------------------------------------------------
 *            GetTmpFile()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Return a pointer to the name of a temporary file in the
 *            in-box.
 *
 * Return:    void 
 *
 */
static char* GetTmpFile(void)
{
    if (!(tmpInUse & OB_TMP_FILE1)) {
        tmpInUse |= OB_TMP_FILE1;
        return ((char*) tmpFile1);
    } else if (!(tmpInUse & OB_TMP_FILE2)) {
        tmpInUse |= OB_TMP_FILE2;
        return ((char*) tmpFile2);
    }

    Assert(0);
    return 0;
} /* End of GetTmpFile() */


/*---------------------------------------------------------------------------
 *            FreeTmpFile()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Free the temporary file.
 *
 * Return:    void 
 *
 */
static void FreeTmpFile(const char *file)
{
    if (file == tmpFile1) {
        Assert(tmpInUse & OB_TMP_FILE1);
        tmpInUse &= ~OB_TMP_FILE1;
    } else if (file == tmpFile2) {
        Assert(tmpInUse & OB_TMP_FILE2);
        tmpInUse &= ~OB_TMP_FILE2;
    }
} /* End of FreeTmpFile() */


