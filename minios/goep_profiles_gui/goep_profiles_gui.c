/*****************************************************************************
 * File:        goep_gui.c
 *
 * Description: This file contains GUI code for the Goep Profiles sample 
 *              application.
 *
 * Created:     July 25, 2002
 *
 * $Project:XTNDAccess IrDA SDK$
 *
 * Copyright 1999-2005 Extended Systems, Inc.  ALL RIGHTS RESERVED.
 *
 * Unpublished Confidential Information of Extended Systems, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any means, 
 * or stored in a database or retrieval system, without prior written 
 * permission of Extended Systems, Inc.
 * 
 * Use of this work is governed by a license granted by Extended Systems, Inc.  
 * This work contains confidential and proprietary information of Extended 
 * Systems, Inc. which is protected by copyright, trade secret, trademark and 
 * other intellectual property rights.
 *
 ****************************************************************************/
#define STRICT
#include <windows.h>
#include <commctrl.h>
#include <stdio.h>
#include "resource.h"
#include <direct.h>  /* For _getcwd() */

#include <osapi.h>
#if BT_STACK == XA_ENABLED
#include <bttypes.h>
#include <me.h>
#include <bluemgr_ui.h>
#include <sys/debug.h>
#endif

#include <obstore.h>
#include <opush.h>
#include <ftp.h>
#include <goep_profiles.h>

static BOOL ftpRegistered;
static BOOL opushRegistered;

/* Internal Global variables for Goep Profile GUI */
HWND        AppWnd;
HINSTANCE   hInst;
char        objectName[64];
char        folderName[64];
char        lastObject[64];
char        pathName[64];

#if BT_STACK == XA_ENABLED
static LPTSTR GpaGetToolTipText(int ControlId);
#endif

/* Internal function prototypes */
static BOOL CALLBACK  AppWndProc(HWND, UINT, WPARAM, LPARAM);
static BOOL CALLBACK  GetDlgProc(HWND, UINT, WPARAM, LPARAM);
static BOOL CALLBACK  SetPathDlgProc(HWND, UINT, WPARAM, LPARAM);
static BOOL CALLBACK  GetTpAddrDlgProc(HWND, UINT, WPARAM, LPARAM);
static void           AcquireTargetAddr(WPARAM);

#if OBEX_AUTHENTICATION == XA_ENABLED
static BOOL CALLBACK PasswordDlgProc(HWND, UINT, WPARAM, LPARAM);
static BOOL CALLBACK UserIdDlgProc(HWND, UINT, WPARAM, LPARAM);
BOOL App_GetPassword(char *password);
BOOL App_GetUserId(char *userId);
BOOL App_Authenticate(void);
char* App_GetSaveObject(void);
#endif /* OBEX_AUTHENTICATION == XA_ENABLED */

extern U8       currentGpaProfile;

void App_Report(char *format,...);

/****************************************************************************
 ** Main Entrypoint
 **/
HWND *APP_Init( HINSTANCE Inst, HINSTANCE PrevInst, LPTSTR CmdLine, int CmdShow )
{
    INITCOMMONCONTROLSEX    icc;

    hInst = Inst;

    icc.dwSize = sizeof(icc);
    icc.dwICC = 0;
#if TCP_STACK == XA_ENABLED
    icc.dwICC |= ICC_INTERNET_CLASSES;
#endif
    if (InitCommonControlsEx(&icc) == FALSE)
        return 0;

    AppWnd = CreateDialog(Inst, MAKEINTRESOURCE(OBEX_GOEP), NULL, AppWndProc);
    if (AppWnd == NULL)
        return 0;

#if BT_STACK == XA_ENABLED
    if (BMG_Initialize(Inst, AppWnd) == 0) {
        DestroyWindow(AppWnd);
        return 0;
    }
    BMG_AddMainMenu(AppWnd);
    BMG_AddToolTips(AppWnd, GpaGetToolTipText);
#endif

    ShowWindow( AppWnd, CmdShow );

    opushRegistered = FALSE;
    ftpRegistered = FALSE;

    return &AppWnd;
}

void  APP_Deinit(void)
{
    GPA_Deinit();
}

void APP_Thread(void)
{
}

static BOOL CALLBACK AppWndProc( HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam )
{
    OPENFILENAME  openFile;
    char          filter[50];
    ObexTpAddr   *target;

    switch (uMsg) {

    case WM_INITDIALOG:
        objectName[0] = 0;
        folderName[0] = 0;
        lastObject[0] = 0;
#if OBEX_AUTHENTICATION == XA_ENABLED
        SendDlgItemMessage(hWnd, IDC_CLIENT_AUTH, BM_SETCHECK, BST_CHECKED, 0L);
#else /* OBEX_AUTHENTICATION == XA_ENABLED */
        EnableWindow(GetDlgItem(hWnd, IDC_SET_USERID), FALSE);
        EnableWindow(GetDlgItem(hWnd, IDC_SET_PASSWORD), FALSE);
        EnableWindow(GetDlgItem(hWnd, IDC_CLIENT_AUTH), FALSE);
#endif /* OBEX_AUTHENTICATION == XA_ENABLED */
        SendDlgItemMessage(hWnd, IDC_OPUSH_REGISTER, WM_SETTEXT, 0, (LPARAM)"Register OPUSH");
        SendDlgItemMessage(hWnd, IDC_FTP_REGISTER, WM_SETTEXT, 0, (LPARAM)"Register FTP");
        break;

#if BT_STACK == XA_ENABLED
    case WM_PAINT: 
        /* Draws a separator below the Blue Manager menu. */
        {
            PAINTSTRUCT     ps; 
            RECT            rect;
            HDC             hdc; 

            hdc = BeginPaint(hWnd, &ps); 
            GetClientRect(hWnd, &rect); 
            rect.left += 2;
            rect.right -= 3;
            DrawEdge(hdc, &rect, EDGE_ETCHED, BF_TOP); 
            EndPaint(hWnd, &ps); 
        }
        break;
#endif /* BT_STACK == XA_ENABLED */

    case WM_COMMAND:
        switch (LOWORD(wParam)) {

        case ID_FTP_CONNECT:
            if (currentGpaProfile != 0x01) break;
            AcquireTargetAddr(LOWORD(wParam)+500);
            break;

        case ID_OPUSH_CONNECT:
        case ID_OBJECT_PUSH:
        case ID_EXCHANGE:
            if (currentGpaProfile != 0x00) break;
            AcquireTargetAddr(LOWORD(wParam)+500);
            break;

        case ID_OPUSH_CONNECT+500:
        case ID_FTP_CONNECT+500:
            if ((target = (ObexTpAddr *)lParam) == 0) {
                App_Report("Client Error: You must Select a device to Connect to first.");
                break;
            }
            
            GPA_Connect(target);
            break;

        case ID_OPUSH_DISCONNECT:
            if (currentGpaProfile != 0x00) break;
            GPA_Disconnect();
            break;

        case ID_FTP_DISCONNECT:            
            if (currentGpaProfile != 0x01) break;
            GPA_Disconnect();
            break;

        case ID_OPUSH_PUSH:
            if (currentGpaProfile != 0x00) break;
            goto Push;
            break;

        case ID_FTP_PUSH:
            if (currentGpaProfile != 0x01) break;
            goto Push;
            break;

Push:
            /* Post dialog to get name of object to PUT */
            memset( &openFile, 0, sizeof(openFile));
            openFile.lStructSize = sizeof(openFile);
            openFile.hwndOwner = AppWnd;
            openFile.lpstrFile = lastObject;
            openFile.nMaxFile = OBS_MAX_NAME_LEN;
            openFile.lpstrFileTitle = objectName;
            openFile.nMaxFileTitle = OBS_MAX_NAME_LEN;
            openFile.lpstrTitle = TEXT("Push Object");
            openFile.Flags = OFN_FILEMUSTEXIST|OFN_PATHMUSTEXIST;
            openFile.lpstrInitialDir = "\\";

            if (GetOpenFileName(&openFile) == 0)
                break;
               
            GPA_Push(objectName);
            break;

        case ID_OPUSH_PULL:
            if (currentGpaProfile != 0x00) break;
            /* Get the default VCard object */
            GPA_Pull("", "text/x-vCard");
            break;

        case ID_FTP_PULL:
            if (currentGpaProfile != 0x01) break;
            /* Popup Get Dialog */
            DialogBox(hInst, MAKEINTRESOURCE(OBEX_GET), AppWnd, GetDlgProc);
            break;

        case ID_FTP_SETFOLDER:
            if (currentGpaProfile != 0x01) break;
            /* Popup SetPath Dialog */
            DialogBox(hInst, MAKEINTRESOURCE(OBEX_SETPATH), AppWnd, SetPathDlgProc);
            break;

        case ID_OBJECT_PUSH+500:
            if ((target = (ObexTpAddr *)lParam) == 0) {
                App_Report("Client Error: You must Select a device to Push to first.");
                break;
            }

            memset(filter, 0, sizeof(filter));
            strcpy(filter, "All Files (*.*)");
            strcpy(filter+16, "*.*");

            /* Post dialog to get name of object to PUT */
            memset( &openFile, 0, sizeof(openFile));
            openFile.lStructSize = sizeof(openFile);
            openFile.hwndOwner = AppWnd;
            openFile.lpstrFilter = filter;
            openFile.lpstrFileTitle = objectName;
            openFile.nMaxFileTitle = OBS_MAX_NAME_LEN;
            openFile.lpstrTitle = TEXT("Select Push Object");
            openFile.Flags = OFN_FILEMUSTEXIST|OFN_PATHMUSTEXIST;
            openFile.lpstrInitialDir = "\\";

            if (GetOpenFileName(&openFile) == 0)
                break;

            GPA_ObjectPush(objectName, target);
            break;

        case ID_EXCHANGE+500:
            if ((target = (ObexTpAddr *)lParam) == 0) {
                App_Report("Client Error: You must Select a device to Exchange with first.");
                break;
            }
      
            memset(filter, 0, sizeof(filter));
            strcpy(filter, "Business Cards (*.vcf)");
            strcpy(filter+23, "*.vcf");

            /* Post dialog to get name of object to PUT */
            memset( &openFile, 0, sizeof(openFile));
            openFile.lStructSize = sizeof(openFile);
            openFile.hwndOwner = AppWnd;
            openFile.lpstrFilter = filter;
            openFile.lpstrFileTitle = objectName; 
            openFile.nMaxFileTitle = OBS_MAX_NAME_LEN;
            openFile.lpstrTitle = TEXT("Select Business Card");
            openFile.Flags = OFN_FILEMUSTEXIST|OFN_PATHMUSTEXIST;
            openFile.lpstrInitialDir = "\\";

            if (GetOpenFileName(&openFile) == 0)
                break;

            GPA_BizExchange(objectName, target);
            break;

        case ID_OPUSH_SERVER_ABORT:
            if (currentGpaProfile != 0x00) break;
            GPA_AbortServer();
            break;

        case ID_OPUSH_CLIENT_ABORT:
            if (currentGpaProfile != 0x00) break;
            GPA_AbortClient();
            break;

        case ID_FTP_CLIENT_ABORT:
            if (currentGpaProfile != 0x01) break;
            GPA_AbortClient();
            break;

        case ID_FTP_PULL_FOLDER_LISTING:
            if (currentGpaProfile != 0x01) break;
            /* Get the current folder listing */
            GPA_PullFolder(0);
            break;

        case IDC_OPUSH_REGISTER:
            if (opushRegistered == FALSE) {
                if (GPA_Init(0)) {
                    opushRegistered = TRUE;
                    SendDlgItemMessage(AppWnd, IDC_OPUSH_REGISTER, WM_SETTEXT, 0, (LPARAM)"Deregister OPUSH");
                }
            } else {
                if (GPA_Deinit()) {
                    opushRegistered = FALSE;
                    SendDlgItemMessage(AppWnd, IDC_OPUSH_REGISTER, WM_SETTEXT, 0, (LPARAM)"Register OPUSH");
                }
            }
            break;

        case IDC_FTP_REGISTER:
            if (ftpRegistered == FALSE) {
                if (GPA_Init(1)) {
                    ftpRegistered = TRUE;
                    SendDlgItemMessage(AppWnd, IDC_FTP_REGISTER, WM_SETTEXT, 0, (LPARAM)"Deregister FTP");
                }
            } else {
                if (GPA_Deinit()) {
                    ftpRegistered = FALSE;
                    SendDlgItemMessage(AppWnd, IDC_FTP_REGISTER, WM_SETTEXT, 0, (LPARAM)"Register FTP");
                }
            }
            break;

#if OBEX_AUTHENTICATION == XA_ENABLED
        case IDC_SET_PASSWORD:
            if (currentGpaProfile != 0x01) break;
            GPA_SetPassword();
            break;

        case IDC_SET_USERID:
            if (currentGpaProfile != 0x01) break;
            GPA_SetUserId();
            break;
#endif /* OBEX_AUTHENTICATION == XA_ENABLED */

        case ID_EXIT:
        case WM_DESTROY:
            if (hWnd == AppWnd) {
                /* Exit the application */
#if BT_STACK == XA_ENABLED
                if (BMG_Shutdown() != BT_STATUS_PENDING)
#endif
                    PostQuitMessage(0);
                return TRUE;
            }
            break;

#if BT_STACK == XA_ENABLED
        default:
            BMG_TrackMainMenu(hWnd, wParam, lParam);
            break;
#endif
        }
        break;
    }
    return 0L;
}


/*
 * Pops up a dialog to query the user for the name of the object to GET.
 */
static BOOL CALLBACK GetDlgProc( HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam )
{
    char         buffer[256], *b;

    switch (uMsg) {

    case WM_INITDIALOG:
        strcpy(buffer, "Destination Folder: ");
        b = buffer + strlen(buffer);
        _getcwd(b, 200);
        SetWindowText(hWnd, buffer);
        strcat(b, "\\*.*");
        SendDlgItemMessage(hWnd, IDC_GET_OBJECT, CB_DIR, DDL_READWRITE, (LPARAM)b);
        if (objectName[0]) {
            SendDlgItemMessage(hWnd, IDC_GET_OBJECT, CB_ADDSTRING, 0, (LPARAM)objectName);
            SendDlgItemMessage(hWnd, IDC_GET_OBJECT, WM_SETTEXT, 0, (LPARAM)objectName);
        }
        break;

    case WM_COMMAND:
        switch (LOWORD(wParam)) {

        case IDOK:
            objectName[0] = 0;
            if (SendDlgItemMessage(hWnd, IDC_GET_OBJECT, WM_GETTEXT, 64, (LPARAM)objectName) > 0) {
                /* Get the object provided.  No type header provided */
                GPA_Pull(objectName, "");
            } 
            EndDialog(hWnd, TRUE);
            break;

        case IDCANCEL:
            EndDialog(hWnd, TRUE);
            break;
        }
    }

    return 0L;
}

/*
 * Pops up a dialog to query the user for the name of the path to set
 */
static BOOL CALLBACK SetPathDlgProc( HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam )
{
    U32         btnState;
    static BOOL reset;
    static BOOL backup;
    static BOOL noCreate;

    switch (uMsg) {

    case WM_INITDIALOG:
        backup = FALSE;
        reset = FALSE;
        noCreate = FALSE;
        break;

    case WM_COMMAND:
        switch (LOWORD(wParam)) {
        case IDC_BACKUP:
            btnState = SendDlgItemMessage(hWnd, IDC_BACKUP, BM_GETSTATE, 0L, 0L);
            if (btnState & BST_CHECKED)
                backup = TRUE;
            else backup = FALSE;
            break;

        case IDC_RESET:
            btnState = SendDlgItemMessage(hWnd, IDC_RESET, BM_GETSTATE, 0L, 0L);
            if (btnState & BST_CHECKED) 
                reset = TRUE;
            else reset = FALSE;
            break;

        case IDC_DONT_CREATE:
            btnState = SendDlgItemMessage(hWnd, IDC_DONT_CREATE, BM_GETSTATE, 0L, 0L);
            if (btnState & BST_CHECKED) 
                noCreate = TRUE;
            else noCreate = FALSE;
            break;

        case IDOK:
            pathName[0] = 0;
            if (reset) {
                /* Reset to the root folder, ignoring the path name, backup flag, and noCreate flag */
                GPA_SetFolder("", FALSE, FALSE, TRUE);
            } else if (SendDlgItemMessage(hWnd, IDC_SET_PATH, WM_GETTEXT, 64, (LPARAM)pathName) > 0) {
                /* Set to the path name entered, possibly including the backup and noCreate flags */
                GPA_SetFolder(pathName, backup, noCreate, FALSE);
            } else if (backup || noCreate) {
                /* Backup or noCreate flags might be set */
                GPA_SetFolder("", backup, noCreate, FALSE);
            }

            EndDialog(hWnd, TRUE);
            break;

        case IDCANCEL:
            EndDialog(hWnd, TRUE);
            break;
        }
    }

    return 0L;
}

#if OBEX_AUTHENTICATION == XA_ENABLED
BOOL App_Authenticate(void)
{
    U32 btnState;

    btnState = SendDlgItemMessage(AppWnd, IDC_CLIENT_AUTH, BM_GETSTATE, 0L, 0L);
    if (btnState & BST_CHECKED)
        return TRUE;
    else return FALSE;
}

BOOL App_GetPassword(char *password)
{
    /* Popup OBEX Authentication Password Dialog */
    if (DialogBoxParam(hInst, MAKEINTRESOURCE(OBEX_PASSWORD), AppWnd, 
        PasswordDlgProc, (LPARAM)password) == TRUE)
        return TRUE;
    else return FALSE;
}

BOOL App_GetUserId(char *userId)
{
    /* Popup OBEX Authentication Password Dialog */
    if (DialogBoxParam(hInst, MAKEINTRESOURCE(OBEX_USERID), AppWnd, 
        UserIdDlgProc, (LPARAM)userId) == TRUE)
        return TRUE;
    else return FALSE;
}

char* App_GetSavedObject(void) 
{
    OPENFILENAME  openFile;
    char          filter[50];

    memset(filter, 0, sizeof(filter));
    strcpy(filter, "All Files (*.*)");
    strcpy(filter+16, "*.*");

    /* Post dialog to get name of saved object */
    memset( &openFile, 0, sizeof(openFile));
    openFile.lStructSize = sizeof(openFile);
    openFile.hwndOwner = AppWnd;
    openFile.lpstrFilter = filter;
    openFile.lpstrFileTitle = objectName;
    openFile.nMaxFileTitle = OBS_MAX_NAME_LEN;
    openFile.lpstrTitle = TEXT("Save Default Object As");
    openFile.Flags = OFN_PATHMUSTEXIST;
    openFile.lpstrInitialDir = "\\";

    if (GetSaveFileName(&openFile) == 0)
        return 0;
    else return objectName;   
}

static BOOL CALLBACK PasswordDlgProc( HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam )
{
    static char      *password;

    switch (uMsg) {

    case WM_INITDIALOG:
        password = (char *)lParam;

        /* Load the default password */
        SendDlgItemMessage(hDlg, IPP_PASSWORD, WM_SETTEXT, 0, (LPARAM)lParam);
        return TRUE;

    case WM_COMMAND:
        switch (LOWORD(wParam)) {

        case IDOK:
            SendDlgItemMessage(hDlg, IPP_PASSWORD, WM_GETTEXT, 11, (LPARAM)password);
            EndDialog(hDlg, TRUE);
            return TRUE;

        case IDCANCEL:
            EndDialog(hDlg, FALSE);
            return TRUE;
        }
    }
    return 0L;
}

static BOOL CALLBACK UserIdDlgProc( HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam )
{
    static char      *userId;

    switch (uMsg) {

    case WM_INITDIALOG:
        userId = (char *)lParam;

        /* Load the default password */
        SendDlgItemMessage(hDlg, IPP_USERID, WM_SETTEXT, 0, (LPARAM)lParam);
        return TRUE;

    case WM_COMMAND:
        switch (LOWORD(wParam)) {

        case IDOK:
            SendDlgItemMessage(hDlg, IPP_USERID, WM_GETTEXT, 20, (LPARAM)userId);
            EndDialog(hDlg, TRUE);
            return TRUE;

        case IDCANCEL:
            EndDialog(hDlg, FALSE);
            return TRUE;
        }
    }
    return 0L;
}
#endif /* OBEX_AUTHENTICATION == XA_ENABLED */

/*
 * Pops up a dialog to query the user for the address of the device to
 * connect to. Supports Bluetooth, TCP and IrDA addresses. Completion of
 * this request is signalled to AppWnd asynchronously.
 */
static void AcquireTargetAddr(WPARAM CompletionEvent)
{
    static ObexTpAddr   target = {0};
    static U32          ipAddr = 0x7f000001;

    if ((OBEX_EnumTransports() != OBEX_TP_IRDA) && 
        (OBEX_EnumTransports() != OBEX_TP_BLUETOOTH)) {

        /* Popup Target device picker that supports all transports. */
#if TCP_STACK == XA_ENABLED
        target.proto.tcp.addr = ipAddr;
#endif
        if (DialogBoxParam(hInst, MAKEINTRESOURCE(QUERY_TARGET), AppWnd, 
                           GetTpAddrDlgProc, (LPARAM)&target) == TRUE) {

#if TCP_STACK == XA_ENABLED
            if (target.type == OBEX_TP_TCP)
                ipAddr = ntohl(target.proto.tcp.addr); /* Save the address */
#endif
            PostMessage(AppWnd, WM_COMMAND, CompletionEvent, (LPARAM)&target);
            return;
        }

        PostMessage(AppWnd, WM_COMMAND, CompletionEvent, 0);
        return;
    }

#if BT_STACK == XA_ENABLED
    target.type = OBEX_TP_BLUETOOTH;
    
    PostMessage(AppWnd, WM_COMMAND, CompletionEvent, (LPARAM)&target);
    return;
#endif

#if IRDA_STACK == XA_ENABLED
    target.type = OBEX_TP_IRDA;
    
    PostMessage(AppWnd, WM_COMMAND, CompletionEvent, (LPARAM)&target);
    return;
#endif

    PostMessage(AppWnd, WM_COMMAND, CompletionEvent, 0);
}

/*
 * Exported to goep.c for writing a string to the UI list boxes.
 */
void App_Report(char *format,...)
{
    char     buffer[200]; /* Output Buffer */
    int      index;
    va_list  args;

    va_start(args, format);
    index = _vsnprintf(buffer, 200, format, args);
    va_end(args);

    /* Direct output to Client or Server list boxes */
    if (StrnCmp("Client", 6, buffer, 6) == TRUE) {
        index = SendDlgItemMessage(AppWnd, OBEX_CLIENT_OUTPUT, LB_ADDSTRING, 0, (LPARAM)buffer);
        if (index != LB_ERR) {
            SendDlgItemMessage(AppWnd, OBEX_CLIENT_OUTPUT, LB_SETCARETINDEX, (WPARAM)index, FALSE);
        }
    } else {
        index = SendDlgItemMessage(AppWnd, OBEX_SERVER_OUTPUT, LB_ADDSTRING, 0, (LPARAM)buffer);
        if (index != LB_ERR) {
            SendDlgItemMessage(AppWnd, OBEX_SERVER_OUTPUT, LB_SETCARETINDEX, (WPARAM)index, FALSE);
        }
    }
}

/*
 * Exported to goep.c for maintaining progress dialogs
 */
void App_Progress(U32 barId, U32 currPos, U32 maxPos)
{
    if (currPos == 0) {
        SendDlgItemMessage(AppWnd, barId, PBM_SETRANGE32, 0, maxPos);
    }

    SendDlgItemMessage(AppWnd, barId, PBM_SETPOS, currPos, 0);
}

/*
 * Window procedure for processing Get TP Addr dialog.
 */
static BOOL CALLBACK GetTpAddrDlgProc(HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    static ObexTpAddr  *tpa;       /* Output TpAddr ptr */
    I32                 index = 0;
#if BT_STACK == XA_ENABLED
    BtDeviceContext    *bdc;

#endif
#if TCP_STACK == XA_ENABLED
    char                port[6];
    U16                 tcpPort;
    char               *stopString;
#endif

    switch (uMsg) {
    case WM_INITDIALOG:
        /* Save pointer to output address location. */
        tpa = (ObexTpAddr *)lParam;

        /* Load the known targets into the picker. On input, the lParam
         * contains the IP address to initialize the IP address with.
         */
#if TCP_STACK == XA_ENABLED
        SendDlgItemMessage(hDlg, IPA_IPADDRESS, IPM_SETADDRESS, 0, 
                           (LPARAM)(U32 *)tpa->proto.tcp.addr);
        SendDlgItemMessage(hDlg, IPA_TCP_PORT, WM_SETTEXT, 0, (LPARAM)"650");
#else
        EnableWindow(GetDlgItem(hDlg, IPA_PICK_IPADDR), FALSE);
        EnableWindow(GetDlgItem(hDlg, IPA_IPADDRESS), FALSE);
        EnableWindow(GetDlgItem(hDlg, IPA_PICK_TCPPORT), FALSE);
        EnableWindow(GetDlgItem(hDlg, IPA_TCP_PORT), FALSE);
#endif

#if BT_STACK == XA_ENABLED
        /* Propagate BD Addr list with devices from the Blue Manager dialog. */
        for (bdc = 0; DS_EnumDeviceList(&bdc) == BT_STATUS_SUCCESS; index++) {
            SendDlgItemMessage(hDlg, IPA_BDADDR, LB_ADDSTRING, 0, (LPARAM)((BtDeviceExt *)bdc->context)->name);
            SendDlgItemMessage(hDlg, IPA_BDADDR, LB_SETITEMDATA, index, (LPARAM)bdc);
        }
#else
        EnableWindow(GetDlgItem(hDlg, IPA_PICK_IPADDR), FALSE);
#endif

#if IRDA_STACK == XA_DISABLED
        EnableWindow(GetDlgItem(hDlg, IPA_PICK_IRDA), FALSE);
#endif
        /* Now select the default address type on the GUI */
        if (index > 0) {
            CheckRadioButton(hDlg, IPA_PICK_BDADDR, IPA_PICK_IPADDR, IPA_PICK_BDADDR);
            SendDlgItemMessage(hDlg, IPA_BDADDR, LB_SETCURSEL, index-1, (LPARAM)0);
        } else {
            CheckRadioButton(hDlg, IPA_PICK_BDADDR, IPA_PICK_IPADDR, IPA_PICK_IPADDR);
        }
        break;

    case WM_COMMAND:
        switch (LOWORD(wParam)) {
        case IPA_OK:
#if BT_STACK == XA_ENABLED
            if (IsDlgButtonChecked(hDlg, IPA_PICK_BDADDR) == BST_CHECKED) {
                /* Bluetooth address was selected, check for an address
                 * in the listbox.
                 */
                index = SendDlgItemMessage(hDlg, IPA_BDADDR, LB_GETCURSEL, 0L, 0L);
                if (index != LB_ERR) {
                    /* Found an address, return it. */
                    bdc = (BtDeviceContext *)SendDlgItemMessage(hDlg, IPA_BDADDR, LB_GETITEMDATA, index, 0);
                    Assert(bdc);
                    /* Setup the Bluetooth target */
                    tpa->type = OBEX_TP_BLUETOOTH;
                    tpa->proto.bt.addr = bdc->addr;
                    tpa->proto.bt.psi = bdc->psi;
                    EndDialog(hDlg, TRUE);
                    return TRUE;
                }
            }
#endif
#if TCP_STACK == XA_ENABLED
            if (IsDlgButtonChecked(hDlg, IPA_PICK_IPADDR) == BST_CHECKED) {
                if (SendDlgItemMessage(hDlg, IPA_IPADDRESS, IPM_GETADDRESS, 0, 
                        (LPARAM)&tpa->proto.tcp.addr) == 4) {
                    tpa->type = OBEX_TP_TCP;
                    tpa->proto.tcp.addr = htonl(tpa->proto.tcp.addr);
                    port[0] = 0;
                    SendDlgItemMessage(hDlg, IPA_TCP_PORT, WM_GETTEXT, 6, (LPARAM)port);
                    tcpPort = (U16)strtoul(port, &stopString, 10);
                    if (tcpPort > 0 && tcpPort <= 65535) {
                        tpa->proto.tcp.customPort = tcpPort;
        
                        EndDialog(hDlg, TRUE);
                        return TRUE;
                    }
                }
            }
#endif
#if IRDA_STACK == XA_ENABLED
            if (IsDlgButtonChecked(hDlg, IPA_PICK_IRDA) == BST_CHECKED) {
                tpa->type = OBEX_TP_IRDA;
                tpa->proto.ir.addr = 0;

                EndDialog(hDlg, TRUE);
                return TRUE;
            }
#endif
            /* The user didn't enter a complete address. */
            tpa->type = OBEX_TP_NONE;
            /* Drop through to Cancel case */

        case WM_DESTROY:
        case IPA_CANCEL:
            EndDialog(hDlg, FALSE);
            return TRUE;
        }
        break;
    }
    return FALSE;
}

/*---------------------------------------------------------------------------
 *            GetClientUIContext
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Returns the UI context handle for the client instance.  The UI 
 *            context is used to control the progress meter as an object is 
 *            transferred.
 *
 * Return:    Progress bar resource Id.
 *
 */
U32 GetClientUIContext(void)
{
    return IDC_CLIENT_PROGRESS;
}

/*---------------------------------------------------------------------------
 *            GetServerUIContext
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Returns the UI context handle for the server instance.  The UI 
 *            context is used to control the progress meter as an object is 
 *            transferred.
 *
 * Return:    Progress bar resource Id.
 *
 */
U32 GetServerUIContext(void)
{
    return IDC_SERVER_PROGRESS;
}

#if BT_STACK == XA_ENABLED
static LPTSTR GpaGetToolTipText(int ControlId)
{
    switch (ControlId) {
    /* OPush Profile */
    case IDC_OPUSH_REGISTER: return "Register/Deregister the OPush Profile";
    case ID_OPUSH_CONNECT: return "Send an OPush Profile Connect";
    case ID_OPUSH_DISCONNECT: return "Send an OPush Profile Disconnect";
    case ID_OPUSH_PUSH: return "Send an OBEX Put";
    case ID_OPUSH_PULL: return "Send an OBEX Get for the Default Object";
    case ID_OPUSH_CLIENT_ABORT: return "Abort the current client operation";
    case ID_OPUSH_SERVER_ABORT: return "Abort the current server operation";
    case ID_EXCHANGE: return "Perform a vCard Exchange sequence";
    case ID_OBJECT_PUSH: return "Perform an Object Push sequence";
    /* FTP Profile */
    case IDC_FTP_REGISTER: return "Register/Deregister the File Transfer Profile";
    case ID_FTP_CONNECT: return "Send a File Transfer Profile Connect";
    case ID_FTP_DISCONNECT: return "Send a File Transfer Profile Disconnect";
    case ID_FTP_PUSH: return "Send an OBEX Put";
    case ID_FTP_PULL: return "Send an OBEX Get";
    case ID_FTP_PULL_FOLDER_LISTING: return "Send an OBEX Get for the current folder listing";
    case ID_FTP_SETFOLDER: return "Send an OBEX SetPath";
    case ID_FTP_CLIENT_ABORT: return "Abort the current client operation";
    case IDC_SET_USERID: return "Set the OBEX Authentication UserId";
    case IDC_SET_PASSWORD: return "Set the OBEX Authentication Password";
    case IDC_CLIENT_AUTH: return "Toggle the requirement for OBEX Client Authentication";
    }
    return "";
}
#endif /* BT_STACK == XA_ENABLED */
