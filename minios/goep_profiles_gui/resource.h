//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by ObexGui.rc
//
#define OBEX_QBEAM                      101
#define OBEX_GOEP                       101
#define OBEX_GET                        103
#define IDB_LOGO                        105
#define QUERY_TARGET                    108
#define OBEX_SETPATH                    109
#define OBEX_PASSWORD                   111
#define OBEX_USERID                     112
#define ID_CONNECT                      1000
#define ID_OPUSH_CONNECT                1000
#define ID_DISCONNECT                   1001
#define ID_OPUSH_DISCONNECT             1001
#define ID_PUT                          1002
#define ID_GET                          1003
#define ID_OPUSH_PULL                   1003
#define ID_EXIT                         1004
#define ID_CON_REQ                      1005
#define ID_DISC_REQ                     1006
#define OBEX_CLIENT_OUTPUT              1007
#define ID_CLIENT_ABORT                 1008
#define ID_SERVER_ABORT                 1009
#define IDC_CLIENT_PROGRESS             1010
#define IDC_SERVER_PROGRESS             1011
#define OBEX_SERVER_OUTPUT              1012
#define ID_PUSH                         1013
#define ID_OPUSH_PUSH                   1013
#define ID_EXCHANGE                     1014
#define ID_SETPATH                      1015
#define ID_CLIENT_SETPATH               1015
#define IPA_IPADDRESS                   1015
#define ID_OPUSH_SETFOLDER              1015
#define ID_FTP_SETFOLDER                1015
#define IDC_GET_OBJECT                  1016
#define IPA_OK                          1016
#define ID_CONNECT_FTP                  1016
#define IPA_CANCEL                      1017
#define ID_CON_REQ_FTP                  1017
#define IPA_BDADDR                      1018
#define ID_DISC_REQ_FTP                 1018
#define IPA_PICK_BDADDR                 1019
#define IPA_PICK_IPADDR                 1020
#define IPA_PICK_IRDA                   1021
#define IDC_SET_PATH                    1021
#define ID_OBJECT_PUSH                  1022
#define ID_OPUSH_SERVER_ABORT           1023
#define IDC_NO_CREATE                   1024
#define ID_OPUSH_CLIENT_ABORT           1024
#define IDC_RESET                       1024
#define IDC_BACKUP                      1025
#define IDC_FTP_REGISTER                1025
#define IDC_CLIENT_AUTH                 1026
#define IDC_DONT_CREATE                 1026
#define IDC_SERVER_AUTH                 1027
#define IPP_PASSWORD                    1028
#define IDC_OPUSH_REGISTER              1028
#define IPP_USERID                      1029
#define ID_FTP_SERVER_ABORT             1029
#define ID_FTP_CLIENT_ABORT             1030
#define ID_FTP_DISCONNECT               1031
#define ID_FTP_CONNECT                  1032
#define IPA_TCP_PORT                    1033
#define IPA_PICK_TCPPORT                1034
#define IDC_SET_USERID                  1035
#define IDC_SET_PASSWORD                1036
#define ID_FTP_PUSH                     1037
#define ID_FTP_PULL                     1038
#define ID_FTP_PULL_FOLDER_LISTING      1039

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        114
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1035
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
