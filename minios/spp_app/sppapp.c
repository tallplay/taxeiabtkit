/***************************************************************************
 *
 * File:
 *     $Workfile:sppapp.c$  for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:26$
 *
 * Description:
 *     Sample Port Emulation Entity for RFCOMM.
 *
 *
 * Copyright 1999-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "windows.h"
#include "stdio.h"
#include "conio.h"
#include "io.h"
#include "fcntl.h"
#include "sppdrv.h"
#include "spp.h"
#include "osapi.h"
#include "textmenu.h"
#if XA_STATISTICS == XA_ENABLED
#include "sys/btstats.h"
#include "btalloc.h"
#endif
#include "bluemgr_ui.h"
#include "sys/debug.h"
#if BT_ALLOW_SCAN_WHILE_CON == XA_ENABLED
#include "me.h"
#endif

#if XA_MULTITASKING == XA_DISABLED
/*
 * Technically this application should run without multitasking; however,
 * it has not been tested with a single thread.
 */
#   error !!! This demo requires XA_MULTITASKING to be enabled.
#endif


/* Special key values for state machine */
#define IDLE                      0
#ifndef ESC
#   define ESC                 0x1b
#endif
#ifndef ACK
#   define ACK                 0x06
#endif
#ifndef CR
#   define CR                  0x0d
#endif
#ifndef LF
#   define LF                  0x0a
#endif
#ifndef BS
#   define BS                  0x08
#endif
#ifndef BEL
#   define BEL                 0x07
#endif


/* Structure to keep track of port information */
typedef struct {
    int fd;
    int modemStatus;
    int lineStatus;
    int baud;
    int dataFormat;
    int rxCnt;
    const char *name;       /* device name */
} CommonPort;

/* Client port attributes */
typedef struct {
    CommonPort           port;       /* Client port for that service */
    SdpServiceClassUuid  uuid;       /* UUID of service on device */
    BtHandler            devHandler; /* Handler for device */
    BtRemoteDevice       *remDev;     /* Remote device connected to */
} ClientPort;

typedef CommonPort ServerPort;

/* Structure for data format tests */
typedef struct {
    int  ioctlArg;
    BOOL result;        /* expected result (pass/fail) */
} testRecord;

#define PASS        TRUE
#define FAIL        FALSE

/* Delay function from testmain.c */
void APP_Delay(int ms);

/* Function Prototypes */
void App_Report(char *format,...);
static BOOL mhMain(MnuTextMenu *menu, MnuCommand cmd, MnuArg arg);
static BOOL mhClientMain(MnuTextMenu *menu, MnuCommand cmd, MnuArg arg);
static BOOL mhClientServerFound(MnuTextMenu *menu, MnuCommand cmd, MnuArg arg);
static BOOL mhServerMain(MnuTextMenu *menu, MnuCommand cmd, MnuArg arg);
static BOOL mhStandard(MnuTextMenu *menu, MnuCommand cmd, MnuArg arg);
static void checkInputForAllPorts(void);
static void checkInput(CommonPort *p);
static void echoBack(int fd, unsigned char *buf, size_t len);
static BOOL topLevel(int key);
static BOOL serverOpened(int key);
static BOOL getFileName(int key);
static void sendFile(int fd);
static void testPortSettings(CommonPort *);
static void displayDataFormat(int val);
static void displayBaud(int val);
static void displayModemStatus(int val);
static void displayLineStatus(int val);
static void getAndShowPortSettings(CommonPort *, BOOL);
static void toggleEchoMode(void);
static void checkPortSettings(void);
static void clientDeviceSelected(ClientPort *);
static void quitBlueMgr(MnuTextMenu *menu);
static void devEventHandler(const BtEvent *Event);
static void displayClientPorts(void);
static void openAllServerDevices(void);



static testRecord dataFormatTest[] = {
    {STOP_BITS_1   | CHAR_SIZE_5 | PAR_NONE   | PAR_ODD | FLOW_NONE,      PASS},
    /* Next test should fail as an example */
    {STOP_BITS_2   | CHAR_SIZE_5 | PAR_NONE   | PAR_ODD | FLOW_NONE,      FAIL},
    {STOP_BITS_1_5 | CHAR_SIZE_5 | PAR_NONE   | PAR_ODD | FLOW_NONE,      PASS},
    {STOP_BITS_1   | CHAR_SIZE_6 | PAR_NONE   | PAR_ODD | FLOW_NONE,      PASS},
    {STOP_BITS_1   | CHAR_SIZE_7 | PAR_NONE   | PAR_ODD | FLOW_NONE,      PASS},
    {STOP_BITS_1   | CHAR_SIZE_8 | PAR_NONE   | PAR_ODD | FLOW_NONE,      PASS},
    {STOP_BITS_1   | CHAR_SIZE_6 | PAR_ENABLE | PAR_ODD | FLOW_NONE,      PASS},
    {STOP_BITS_1   | CHAR_SIZE_6 | PAR_ENABLE | PAR_EVEN | FLOW_NONE,     PASS},
    {STOP_BITS_1   | CHAR_SIZE_6 | PAR_ENABLE | PAR_MARK | FLOW_NONE,     PASS},
    {STOP_BITS_1   | CHAR_SIZE_6 | PAR_ENABLE | PAR_SPACE| FLOW_NONE,     PASS},
    {STOP_BITS_1   | CHAR_SIZE_6 | PAR_ENABLE | PAR_EVEN | FLOW_RTS_CTS,  PASS},
    {STOP_BITS_1   | CHAR_SIZE_6 | PAR_ENABLE | PAR_EVEN | FLOW_DTR_DSR,  PASS},
    {STOP_BITS_1   | CHAR_SIZE_6 | PAR_ENABLE | PAR_EVEN | FLOW_XON_XOFF, PASS}
};

#define NUM_FORMAT_TESTS    (sizeof(dataFormatTest)/sizeof(*dataFormatTest))


/* Menu options */
#define M_CLIENT          M_MENUBASE+0
#define M_SERVER          M_MENUBASE+1
#define M_ECHO            M_MENUBASE+3
#define M_GETPORTSETTINGS M_MENUBASE+4
#define M_TESTDATAFORMAT  M_MENUBASE+5
#define M_OPEN            M_MENUBASE+6
#define M_WRITE           M_MENUBASE+7
#define M_GOTFILENAME     M_MENUBASE+8
#define M_SENDFILE        M_MENUBASE+9
#define M_CLOSE           M_MENUBASE+10
#define M_RTSCTS          M_MENUBASE+11
#define M_PREVMENU        M_MENUBASE+12
#define M_BLUEMGR         M_MENUBASE+13
#define M_QUERY           M_MENUBASE+14
#define M_ZERO            M_MENUBASE+15
#define M_PAUSE           M_MENUBASE+16
#define M_DISCONNECT      M_MENUBASE+17
#define M_TARGET          M_MENUBASE+18
#define M_GET_ACL M_MENUBASE+19

static const MnuItem mainMenuItems[] = {
        { 'c', "I want to be a client", M_CLIENT },
        { 's', "I want to be a server", M_SERVER },
        { 'q', "Quit",           M_QUIT },
        { 0, 0, 0 }
};

static MnuTextMenu mainMenu = { "Application Main Menu", 
    mhMain, mainMenuItems, 0 };


/*
 * Start of client menus
 */
static const MnuItem clientMainMenuItems[] = {
        { 'p', "Select client device to access", M_TARGET },
        { 'o', "Open the client device", M_OPEN },
        { 'w', "Write canned string to the device", M_WRITE },
        { 'g', "Get device settings", M_GETPORTSETTINGS },
        { 's', "Send a file", M_SENDFILE },
        { 'c', "Close the device", M_CLOSE },
        { 'r', "toggle RTS/CTS flow control", M_RTSCTS },
        { 'f', "test Data format settings", M_TESTDATAFORMAT },
        { 'p', "Toggle reading input", M_PAUSE },
        { 'e', "toggle Echo on/off", M_ECHO },
        { 'd', "Disconnect the ACL Connection", M_DISCONNECT, 0 },
        { 'x', "go to Bluetooth Manager", M_BLUEMGR },
        { 'q', "Quit", M_QUIT },
        { ESC, NULL, M_PREVMENU },
        { 0, 0, 0 }
};

static MnuTextMenu clientMainMenu = { "Client Menu", 
    mhClientMain, clientMainMenuItems, 0 };

/* End client menus */


/*
 * Start of server menus
 */
static const MnuItem ServerMainMenuItems[] = {
        { 'o', "Open the server device", M_OPEN },
        { 'c', "Close the device", M_CLOSE },
        { 'g', "Get device settings", M_GETPORTSETTINGS },
        { 'z', "Clear RX count for all ports", M_ZERO },
        { 'p', "Toggle reading input", M_PAUSE },
        { 'e', "toggle Echo on/off", M_ECHO },
        { 'x', "go to Bluetooth Manager", M_BLUEMGR },
        { 'q', "Quit", M_QUIT },
        { ESC, NULL, M_PREVMENU },
        { 0, 0, 0 }
};

static MnuTextMenu ServerMainMenu = { "Server Main Menu", 
    mhServerMain, ServerMainMenuItems, 0 };

/* end server menus */


/* Always list server ports first. These must agree with the ports as
 * defined in sppdrv.c.
 */
static ServerPort serverPort[] = {
    {-1, 0, -1, 0, 0, 0, "/dev/spp/sDialup"}, 
    {-1, 0, -1, 0, 0, 0, "/dev/spp/sLanAccess0"}, 
    {-1, 0, -1, 0, 0, 0, "/dev/spp/sLanAccess1"},
    {-1, 0, -1, 0, 0, 0, "/dev/spp/sFax"},
    {-1, 0, -1, 0, 0, 0, "/dev/spp/sSerial"}
};
#define NUM_SERVERS       (sizeof(serverPort)/sizeof(*serverPort))

static ClientPort clientPort[] = {
    {{-1, 0, -1, 0, 0, 0,"/dev/spp/cDialup"}, 
     SC_DIALUP_NETWORKING, 0, 0}, 
    {{-1, 0, -1, 0, 0, 0, "/dev/spp/cLanAccess"}, 
     SC_LAN_ACCESS_PPP, 0, 0}, 
    {{-1, 0, -1, 0, 0, 0, "/dev/spp/cFax"},
     SC_FAX, 0, 0}, 
    {{-1, 0, -1, 0, 0, 0, "/dev/spp/cSerial"},
     SC_SERIAL_PORT, 0, 0} 
};
#define NUM_CLIENTS       (sizeof(clientPort)/sizeof(*clientPort))

/* Determines whether to automatically send incoming data back */
static BOOL echoOn;

/* Flag for communicating between eventhandler and thread. When the ACL
 * is established, the event handler receives confirmation. Since we
 * want the local device opened by the app thread, the event handler sets
 * the flag true signalling the app thread to call open.
 */
static BOOL openClientNow;

/* External function client uses to check if service is on the remote device. */
static BtDeviceContext *dev;

/* Structure passed to Blue Manager when requesting the selected device */
static BtSelectDeviceToken devQuery = {0};

/* Currently selected client */
static ClientPort *selectedClient;

/* scratch buf for displaying bdaddr */
static char bdaddrString[BDADDR_NTOA_SIZE];     

/* 
 * Variables for sending/receiving files
 */
static char fileName[64];
#define FILENAMELEN         sizeof(fileName)
static int fileFd;
static unsigned char workBuf[1024];
#define WORKBUFLEN          sizeof(workBuf)
static int totalBytesRead;
static int totalBytesWritten;
static U16 txCrc;
static BOOL appState;
#define APP_STATE_NOT_READY     0
#define APP_STATE_READY         1
#define APP_STATE_EXITING       2

static MnuTextMenu  *BlueMgrMenu;/* Pointer to Bluetooth Manager's menu */
static int openPorts;            /* Number open ports. */
static BOOL readRx;              /* Automatically read received data */


/*---------------------------------------------------------------------------
 * APP_Init() 
 *      Initialize Bluetooth driver. Normally this would be done
 *      during boot.
 *
 * Requires:
 *     
 * Parameters:
 *
 * Returns:
 *      0   failed
 *      1   success
 */
BOOL  APP_Init(void)
{
    int i;

    for (i = 0; i < NUM_CLIENTS; i++) {
        ME_InitHandler(&clientPort[i].devHandler);
        clientPort[i].devHandler.callback = devEventHandler;
        clientPort[i].remDev = 0;
    }

    selectedClient = &clientPort[0];
    openPorts = 0;
    appState = APP_STATE_NOT_READY;
    readRx = TRUE;
    openClientNow = FALSE;
    Mnu_Load((MnuTextMenu *)&mainMenu, FALSE);
    BlueMgrMenu = BMI_CreateMenu();
    Assert(BlueMgrMenu);
    BMI_SetSecurityMode(BSM_SEC_LEVEL_2);
    return TRUE;
}


/*
 * Exported for writing messages to the application's main console.
 *
 * Normally this function needs to make sure that there is a \n on the
 * end of the string. To support the special case where we don't want a
 * \n to be appended, we check for a \r and if found we don't add the \n.
 */
void App_Report(char *format,...)
{
    int         index;
    char        workBuf[1000]; /* Output workBuf */
    va_list     args;

    va_start(args, format);
    index = _vsnprintf(workBuf, 1000, format, args);
    va_end(args);

    /* Add missing newline character */
    if (workBuf[index-1] != '\n') {
        if (workBuf[index-1] == '\r') {
            workBuf[index-1] = 0;
        } else {
            workBuf[index] = '\n';
            workBuf[index+1] = 0;
            index++;
        }
    }

    printf(workBuf);
}


void APP_Delay(int ms)
{
    Sleep(ms);
}


/*---------------------------------------------------------------------------
 * APP_ParseCommandLine() 
 *      Called by testmain.c to make command line available.
 *     
 * Parameters:
 *      str - pointer to command-line string
 *
 * Returns:
 */
BOOL  APP_ParseCommandLine(char *str)
{
    return TRUE;
}


/*---------------------------------------------------------------------------
 * APP_PrintTitle() 
 *      Called by testmain.c
 */
void APP_PrintTitle(void)
{
    App_Report("RFCOMM Serial API Demo\n");    
}


/*---------------------------------------------------------------------------
 * APP_GetOptionString() 
 *      Called by testmain.c
 */
char* APP_GetOptionString(void)
{
    return "";
}


/*---------------------------------------------------------------------------
 * APP_GetExampleString() 
 *      Called by testmain.c
 */
char* APP_GetExampleString(void)
{
    return "";
}


/*---------------------------------------------------------------------------
 * APP_Deinit() 
 *      This function is probably only required in a demo in which
 *      the entire stack and driver are abandoned.
 */
void  APP_Deinit(void)
{
    int rc;
    int i;

    for (i = 0; i < NUM_SERVERS; i++) {
        if (serverPort[i].fd != -1) {
            if ((rc = Close(serverPort[i].fd)) != OSRC_SUCCESS) {
                App_Report("Close() failed for %s: %d\n", serverPort[i].name, rc);
            }
        }
    }

    for (i = 0; i < NUM_CLIENTS; i++) {
        if (clientPort[i].port.fd != -1) {
            if ((rc = Close(clientPort[i].port.fd)) != OSRC_SUCCESS) {
                App_Report("Close() failed for %s: %d\n", clientPort[i].port.name, rc);
            }
        }
    }

    removeSppDriver();
    BMI_Deinitialize();

#if XA_STATISTICS == XA_ENABLED
    BTSTAT_Report(&BTC(stats), OS_Report);
#endif

}



/*---------------------------------------------------------------------------
 * APP_Thread() 
 *      Called once by testmain.c. This is the main loop for this sample
 *      app.
 */
BOOL APP_Thread(void)
{
    char ch = 0;
    BOOL quit = FALSE;

    while (1) {

        /*
         * In this main loop we could block on getch() without calling
         * kbhit(); however, for demo purposes we avoid blocking here so
         * that we can check for incoming data on open ports.
         */
        if (kbhit()) {
            ch = getch();
        }
            
        if (appState == APP_STATE_NOT_READY) {
            /* Wait for the radio to initialize before proceeding. */
            if (! BMI_IsRadioReady()) {
                if (ch == 'q') {
                    return FALSE;
                }
            } else {
                /* The radio is ready */
                appState = APP_STATE_READY;
                Mnu_Show();
            }
        } else if (appState == APP_STATE_EXITING) {
            if (! BMI_IsRadioReady()) {
                /* BlueMgr has done any deinit for the radio. */
                return FALSE;
            }
        } else {
            
            if (openClientNow == TRUE) {
                /* ACL was created and our event handler set this flag
                 * so that the file can be opened by the app thread.
                 */
                Mnu_ProcessKey('o');
                openClientNow = FALSE;
            }

            if (ch) {
                if (Mnu_ProcessKey(ch) == M_QUIT) {
                    /* User requested to quit application */
                    appState = APP_STATE_EXITING;
                }
                ch = 0;
            } else {
                /* No key input */
                if (readRx == TRUE) {
                    checkInputForAllPorts();
                }

                APP_Delay(20);
            }
        }
    }

    return TRUE;
}



/*---------------------------------------------------------------------------
 * mhMain
 *      Main Menu Handler
 */
static BOOL mhMain(MnuTextMenu *menu, MnuCommand cmd, MnuArg arg)
{
    BtStatus status;

    switch (cmd) {

    case M_SERVER:
        /* The application is used as a server. */
#if BT_ALLOW_SCAN_WHILE_CON == XA_ENABLED
        status = ME_SetAccessibleModeC(BAM_GENERAL_ACCESSIBLE, 0);
        if (status != BT_STATUS_SUCCESS) {
            App_Report("ME_SetAccessibleModeC failed: %d\n", status);
        }
#endif
        openAllServerDevices();
        Mnu_Load(&ServerMainMenu, TRUE);
        break;

    case M_CLIENT:
        /* The application is used as a client. */
        App_Report("\n\nClient is active.\n");
        
        AssertEval(ME_SetDefaultLinkPolicy(BLP_DISABLE_ALL, 
            BLP_MASTER_SLAVE_SWITCH) == BT_STATUS_SUCCESS);
        /* Load the client menu without displaying it then load and
         * display BlueMgr. When BlueMgr exits it will return to
         * clientMainMenu instead of here.
         */
        Mnu_Load((MnuTextMenu *)&clientMainMenu, FALSE);
        /* BlueMgr remembers the 1st menu from whence it was first called. 
         * Clearing workspace causes it to return to the current menu.
         */
        BlueMgrMenu->workspace = 0;
        Mnu_Load(BlueMgrMenu, TRUE);
        break;

    case M_ECHO:
        toggleEchoMode();
        break;

    case M_QUIT:
        quitBlueMgr(menu);
    }

    return TRUE;
}


/*---------------------------------------------------------------------------
 * mhServerMain 
 *      Server main menu handler.
 */
static BOOL mhServerMain(MnuTextMenu *menu, MnuCommand cmd, MnuArg arg)
{
    ServerPort *p;
    int i;
    int status;

    switch (cmd) {

    case M_OPEN:
        openAllServerDevices();
        break;

    case M_CLOSE:
        /* Close all the servers */
        for (i = 0; i < NUM_SERVERS; i++) {
            p = &serverPort[i];
            if ((status = Close(p->fd)) == 0) {
                App_Report("Server port %s closed.\n", p->name);
            } else {
                App_Report("Server port %s close failed: %d\n", p->name, status);
            }
            p->fd = -1;
        }
        break;

    case M_GETPORTSETTINGS:
        /* Get the port settings */
        for (i = 0; i < NUM_SERVERS; i++) {
            p = &serverPort[i];
            if (p->fd >= 0) {
                getAndShowPortSettings((CommonPort *)p, TRUE);
            }
        }
        break;

    case M_TESTDATAFORMAT:
        /* Test dataformat settings */
        for (i = 0; i < NUM_SERVERS; i++) {
            p = &serverPort[i];
            if (p->fd >= 0) {
                testPortSettings((CommonPort *)p);
            }
        }
        break;
        
    case M_ZERO:
        /* Clear the rx count */
        for (i = 0; i < NUM_SERVERS; i++) {
            p = &serverPort[i];
            if (p->fd >= 0) {
                App_Report("Port %s rx count: %d\n", p->name, p->rxCnt);
                p->rxCnt = 0;
            }
        }
        break;

    default:
        mhStandard(menu, cmd, arg);
    }

    return TRUE;
}


/*---------------------------------------------------------------------------
 * mhClientMain() 
 *      Client menu. A server has been found by SDP.
 */
static BOOL mhClientMain(MnuTextMenu *menu, MnuCommand cmd, MnuArg arg)
{
    BtStatus status;
    int fd;

    switch (cmd) {
    case M_TARGET:
        displayClientPorts();
        App_Report("\nTo which service do you want to connect? \r");
        Mnu_ReadInteger(M_GET_ACL, 0, NUM_CLIENTS - 1, 10);
        break;

    case M_GET_ACL:
        {
            MnuReadInteger *mnuInt;
            Assert(arg);
            mnuInt = (MnuReadInteger *)arg;

            if (mnuInt->status == MRERR_SUCCESS) {
                Assert((mnuInt->value >= 0));
                Assert((mnuInt->value < NUM_CLIENTS));
                clientDeviceSelected(&clientPort[mnuInt->value]);
            }
        }
        break;
    
    case M_WRITE:
        if (Write(selectedClient->port.fd, "Hello World!", 12) == 12) {
            App_Report("\n12 bytes written to port\n");
        } else {
            App_Report("\nWrite failed\n");
        }
        break;

    case M_GETPORTSETTINGS:
        /* Get the port settings */
        getAndShowPortSettings(&selectedClient->port, TRUE);
        break;

    case M_SENDFILE:
        fileFd = 0;
        App_Report("\nEnter File name: \r");
        Mnu_ReadString(M_GOTFILENAME, fileName, FILENAMELEN);
        break;

    case M_OPEN:
        if (selectedClient->remDev) {
            fd = Open(selectedClient->port.name, 0, (int)selectedClient->remDev);
            if (fd >= 0) {
                selectedClient->port.fd = fd;
                selectedClient->port.rxCnt = 0;
                openPorts++;
                App_Report("Client port opened\n");
            } else {
                App_Report("\nClient open failed: %d\n", fd);
            }
        } else {
            /* Need to create the ACL first. */
            clientDeviceSelected(selectedClient);
        }
        break;

    case M_CLOSE:
        if (Close(selectedClient->port.fd) != OSRC_SUCCESS) {
            openPorts--;
            App_Report("\nClose() failed\n");
        } else {
            App_Report("\nClient port closed\n");
        }
        selectedClient->port.fd = -1;
        break;

    case M_TESTDATAFORMAT:
        /* Test dataformat settings */
        testPortSettings(&selectedClient->port);
        break;

    case M_ECHO:
        toggleEchoMode();
        break;

    case M_GOTFILENAME:
        sendFile(selectedClient->port.fd);

        Ioctl(selectedClient->port.fd, IOC_GET_LINE_STATUS, &status);
        if (status) {
            App_Report("Line status error = %d\n", status);
        }
        break;

    case M_RTSCTS:
        /* Toggle RTS/CTS flow control */
        {
            static int arg = FLOW_NONE;

            if (arg == FLOW_NONE) {
                arg = FLOW_RTS_CTS;
                if (Ioctl(selectedClient->port.fd, IOC_SET_FLOWCONTROL, &arg)
                    == OSRC_SUCCESS) {
                    App_Report("RTS/CTS enabled\n");
                } else {
                    App_Report("Ioctl() failed\n");
                    arg = FLOW_NONE;
                }
            } else {
                arg = FLOW_NONE;
                if (Ioctl(selectedClient->port.fd, IOC_SET_FLOWCONTROL, &arg)
                    == OSRC_SUCCESS) {
                    App_Report("RTS/CTS disabled\n");
                } else {
                    App_Report("Ioctl() failed\n");
                    arg = FLOW_RTS_CTS;
                }
            }
        }
        break;
        
    case M_DISCONNECT:
        if (selectedClient->port.fd == -1) {
            ME_DisconnectLink(&selectedClient->devHandler, selectedClient->remDev);
            App_Report("ACL Connection disconnected.\n");
        } else {
            App_Report("Cannot disconnect because the client port is open.\n");
        }
        break;

    case M_SHOW:
        Assert(selectedClient);
        App_Report("  Selected client port: %s\n\n", selectedClient->port.name);
        break;

    default:
        mhStandard(menu, cmd, arg);
    }

    return TRUE;
}


/*---------------------------------------------------------------------------
 * mhStandard() 
 *      Menu handler for common or standard commands.
 */
static BOOL mhStandard(MnuTextMenu *menu, MnuCommand cmd, MnuArg arg)
{
    switch (cmd) {

    case M_ACTIVATE:
        /* If we have been activated from a higher menu, workspace is 0. 
         * If a lower menu has returned to us, workspace is non-0.
         */
        if (! menu->workspace) {
            menu->workspace = (U32)arg;
        }
        break;

    case M_PAUSE:
        readRx = !readRx;
        App_Report("Reading input is %s.\n", readRx == TRUE ? "enabled" :
            "disabled");
        break;

    case M_ECHO:
        toggleEchoMode();
        break;

    case M_BLUEMGR:
        /* PME remembers the 1st menu from whence it was first called. 
         * Clearing workspace causes it to return to the current menu.
         */
        BlueMgrMenu->workspace = 0;
        Mnu_Load(BlueMgrMenu, TRUE);
        break;

    case M_QUIT:
        quitBlueMgr(menu);
        break;

    case M_PREVMENU:
        if (menu->workspace) {
            MnuTextMenu *prevMenu = (MnuTextMenu *)menu->workspace;

            menu->workspace = 0;
            Mnu_Load((MnuTextMenu *)prevMenu, TRUE);
        }
        break;
    }
    return TRUE;
}



/*---------------------------------------------------------------------------
 * sendFile() 
 *      Open a file and write it to the Serial API.
 */
static void sendFile(int port)
{
    int nRead, nWrite, tWrite;
    int fileFd;
    char *pBuf;
    int key;

    fileFd = _open(fileName, _O_RDONLY | _O_BINARY, 0);
    if (fileFd < 0) {
        App_Report("_open() failed: %d\n", fileFd);
        return;
    }
    
    App_Report("\n\nSending %s: %8d\r", fileName, 0);
    tWrite = 0;

    while ((nRead = _read(fileFd, workBuf, sizeof(workBuf))) > 0) {

        nWrite = 0;
        pBuf   = workBuf;

        while (nRead > 0) {
            nWrite = Write(port, (char *)pBuf, nRead);

            if (nWrite > 0) {
                tWrite += nWrite;
                App_Report("\b\b\b\b\b\b\b\b%8d\r", tWrite);
                pBuf   += nWrite;
                nRead  -= nWrite;
            } else if (nWrite < 0) {
                App_Report("Write() failed: %d\n", nWrite);
                break;
            } else {
                APP_Delay(5);
            }

            /* See if there is rx data, too */
            checkInputForAllPorts();

            /* Give us a chance to bail out. */
            if (kbhit() && (((key = getch()) == 'q') || (key == ESC))) {
                App_Report("User break -\r");
                break;
            }
        }
    }

    App_Report(" done\n");
    _close(fileFd);
}



/*---------------------------------------------------------------------------
 * testPortSettings() 
 *      Send various settings in dataFormatTest[] and see if the results
 *      are correct.
 */
static void testPortSettings(CommonPort *p)
{
    static int i;
    int passed = 0;
    int rc;

    for (i = 0; i < NUM_FORMAT_TESTS; i++) {
        rc = Ioctl(p->fd, IOC_SET_DATAFORMAT, &dataFormatTest[i].ioctlArg);
        if (rc == OSRC_ERROR) {
            /* Some settings in the table may be invalid */
            App_Report("Ioctl(SET) error on test #%d\n", i);
            continue;
        }

        rc = Ioctl(p->fd, IOC_GET_DATAFORMAT, &p->dataFormat);
        if (rc == OSRC_ERROR) {
            App_Report("Ioctl(GET) error on test #%d\n", i);
            continue;
        }

        if ( ((dataFormatTest[i].ioctlArg == p->dataFormat)
               && (dataFormatTest[i].result == PASS))
             || ((dataFormatTest[i].ioctlArg != p->dataFormat)
             && (dataFormatTest[i].result == FAIL)) ) {
            passed++;
        } else {
            App_Report("!!! Test #%d failed !!!\n", i);
            App_Report("Set dataformat to:\n");
            displayDataFormat(dataFormatTest[i].ioctlArg);
            App_Report("Received:\n");
            displayDataFormat(p->dataFormat);
        }
    }

    App_Report("\n\nTest complete: %d of %d passed.\n\n", passed, i);
}


/*---------------------------------------------------------------------------
 * getAndShowPortSettings() 
 *      Retrieve data format and line settings for the open port and 
 *      display the results.
 */
static void getAndShowPortSettings(CommonPort *port, BOOL all)
{
    int val;
    int rc;
    BOOL title = FALSE;

    rc = Ioctl(port->fd, IOC_GET_DATAFORMAT, &val);
    if (rc == OSRC_SUCCESS) {
        if ((all == TRUE) || (val != port->dataFormat)) {
            App_Report("\n\n------- Port Settings -------\n");
            title = TRUE;    
            port->dataFormat = val;
            displayDataFormat(val);
        }
    } else {
        App_Report("Ioctl: IOC_GET_DATAFORMAT failed: %d\n", rc); 
    }

    rc  = Ioctl(port->fd, IOC_GETBAUD, &val);
    if (rc == OSRC_SUCCESS) {
        if ((all == TRUE) || (val != port->baud)) {
            if (title == FALSE) {
                App_Report("\n\n------- Port Settings -------\n");
                title = TRUE;    
            }
            port->baud = val;
            displayBaud(val);
        }
    } else {
        App_Report("Ioctl: IOC_GETBAUD failed: %d\n", rc); 
    }

    rc  = Ioctl(port->fd, IOC_GET_MODEM_STATUS, &val);
    if (rc == OSRC_SUCCESS) {
        if ((all == TRUE) || (val != port->modemStatus)) {
            if (title == FALSE) {
                App_Report("\n\n------- Port Settings -------\n");
                title = TRUE;    
            }
            port->modemStatus = val;
            displayModemStatus(val);
        }
    } else {
        App_Report("Ioctl: IOC_GET_MODEM_STATUS failed: %d\n", rc); 
    }

    rc  = Ioctl(port->fd, IOC_GET_LINE_STATUS, &val);
    if (rc == OSRC_SUCCESS) {
        if ((all == TRUE) || (val != port->lineStatus)) {
            if (title == FALSE) {
                App_Report("\n\n------- Port Settings -------\n");
                title = TRUE;    
            }
            port->lineStatus = val;
            displayLineStatus(val);
        }
    } else {
        App_Report("Ioctl: IOC_GET_LINE_STATUS failed: %d\n", rc); 
    }

    if (title == TRUE) {
        App_Report("\n-----------------------------\n\n");
    }
}


/*---------------------------------------------------------------------------
 * displayDataFormat() 
 *      Display textual interpretation of data format.
 */
static void displayDataFormat(int val)
{
    const char *parity[] = {"ODD", "EVEN", "MARK", "SPACE"};
    const char *enabled[]= {"Disabled", "Enabled"};
    const char *stops[]  = {"1", "2", "1 1/2"};
    const char *flow[]   = {"NONE", "RTS/CTS", "DTR/DSR", "XON/XOFF"};
    const char *chars[]  = {"5", "6", "7", "8"};
    
    App_Report("    StopBits: %s\n", stops[val & STOP_MASK]);
    App_Report("    CharSize: %s\n", chars[(val & CHAR_MASK) >> 2]);
    App_Report("      Parity: %s - %s\n", 
            parity[(val & PARITY_TYPE_MASK) >> 5], 
            enabled[(val & PARITY_ENABLE_MASK) >> 4]);
    App_Report("        Flow: %s\n", flow[(val & FLOW_MASK) >> 8]);
}


/*---------------------------------------------------------------------------
 * displayBaud() 
 *      Display baud in text form.
 */
static void displayBaud(int val)
{
    const int   baud[]   = {2400, 4800, 9600, 19200, 38400, 56700, 115200};

    App_Report("        Baud: %d\n", baud[val]);
}


/*---------------------------------------------------------------------------
 * displayModemStatus() 
 *      Display text version of modem status.
 */
static void displayModemStatus(int val)
{
    const char *dtr[]    = {"dtr", "DTR"};
    const char *cts[]    = {"cts", "CTS"};
    const char *ri[]     = {"ri", "RI"};
    const char *dcd[]    = {"dcd", "DCD"};

    App_Report("Modem Status: %s %s %s %s\n", dtr[val & MODEM_STATUS_DTR],
            cts[(val & MODEM_STATUS_CTS) >> 1],
            ri[(val & MODEM_STATUS_RI)  >> 2],
            dcd[(val & MODEM_STATUS_DCD) >> 3]);
}


/*---------------------------------------------------------------------------
 * displayLineStatus() 
 *      Display text version of line status.
 */
static void displayLineStatus(int val)
{
    const char *ovr[]     = {"", "overrun error"};
    const char *parity[]  = {"", "parity error"};
    const char *frame[]   = {"", "framing error"};

    App_Report(" Line Status: %s %s %s", ovr[val & LINE_STATUS_OVR],
            parity[(val & LINE_STATUS_PARITY) >> 1],
            frame[(val & LINE_STATUS_FRAMING)  >> 2]);
}


/*---------------------------------------------------------------------------
 * checkInpu() 
 *      For each open port read any pending rx data.
 */
static void checkInputForAllPorts(void)
{
    int i;

    for (i = 0; i < NUM_SERVERS; i++) {
        checkInput((CommonPort *)&serverPort[i]);
    }

    for (i = 0; i < NUM_CLIENTS; i++) {
        checkInput((CommonPort *)&clientPort[i].port);
    }
}


static void checkInput(CommonPort *p)
{
    int nRead;
    char buf[80];

    if (p->fd >= 0) {

        if ((Ioctl(p->fd, IOC_NREAD, &nRead) == OSRC_SUCCESS) && (nRead > 0)) {

            if (openPorts > 1) {
                App_Report("\n");
            } else {
                App_Report("\r\r");
            }

            App_Report("Receiving on port %s: %4d %8d%\r", p->name, 0, p->rxCnt);

            do {
                nRead = sizeof(buf);
                nRead = Read(p->fd, (char *)buf, nRead);
                if (nRead < 0) {
                    App_Report("Read() failure for %s\n", p->name);

                } else if (nRead > 0) {
                        p->rxCnt += nRead;
                        App_Report("\b\b\b\b\b\b\b\b\b\b\b\b\b%4d %8d\r", 
                                   nRead, p->rxCnt);

                        if (echoOn == TRUE) {
                            echoBack(p->fd, buf, nRead);
                        }
                    }
            } while (nRead > 0);
        }
    }
}


/*---------------------------------------------------------------------------
 * echoBack() 
 *      If the echo back option is set, send received data back to sender.
 */
static void echoBack(int fd, unsigned char *buf, size_t len)
{
    int key;
    size_t bytes;

    while (len > 0) {
        bytes = Write(fd, (char *)buf, len);

        if (bytes >= 0) {
            len -= bytes;
            buf += bytes;
        } else {
            App_Report("echoBack(): Write() returned %d\n", bytes);
        }

        /* Give us a chance to bail out. */
        if (kbhit() && (((key = getch()) == 'q') || (key == ESC))) {
            App_Report("User break -\r");
            toggleEchoMode();
            break;
        }
    }
}


/*---------------------------------------------------------------------------
 * toggleEchoMode() 
 *      When toggle mode is enabled, any data received is automatically
 *      sent back to the sender.
 */
static void toggleEchoMode(void)
{
    echoOn = !echoOn;
    App_Report("Echo is \r");
    if (echoOn == TRUE) {
        App_Report("ON\n");
    } else {
        App_Report("OFF\n");
    }
}


/*---------------------------------------------------------------------------
 * checkPortSettings() 
 *      For all open ports get and display port settings.
 */
static void checkPortSettings(void)
{
    int i;

    for (i = 0; i < NUM_SERVERS; i++) {
        if (serverPort[i].fd >= 0) {
            /* The port is open */
            getAndShowPortSettings(&serverPort[i], FALSE);
        }
    }

    for (i = 0; i < NUM_CLIENTS; i++) {
        if (clientPort[i].port.fd >= 0) {
            /* The port is open */
            getAndShowPortSettings(&clientPort[i].port, FALSE);
        }
    }
}


/*---------------------------------------------------------------------------
 * clientDeviceSelected() 
 *      Called when the user has selected a client device. If an ACL does
 *      not already exist between the local and remote devices, one is
 *      created. 
 */
static void clientDeviceSelected(ClientPort *port)
{
    BtStatus status;

    selectedClient = port;

    if ((selectedClient->remDev == 0) ||
        (! ME_FindRemoteDevice(&selectedClient->remDev->bdAddr))) {
        /* No ACL exists to server or one exists but this client device 
         * has not registered a handler.
         */
        App_Report("Creating an ACL...");
        devQuery.callback = devEventHandler;
        devQuery.quality.mask = (BDQM_DEVICE_STATUS|BDQM_SERVICE_UUID);
        devQuery.quality.status = BDS_IN_RANGE;
        devQuery.quality.serviceUuid = selectedClient->uuid;

        status = DS_SelectDevice(&devQuery);
        if (status == BT_STATUS_SUCCESS) {
            status = ME_CreateLink(&selectedClient->devHandler, 
                                   &devQuery.result->addr, 
                                   &devQuery.result->psi, 
                                   (BtRemoteDevice **)&selectedClient->remDev);

            if (status == BT_STATUS_SUCCESS) {
                App_Report("Handler added to %s", 
                           bdaddr_ntoa(&selectedClient->remDev->bdAddr, bdaddrString));
                openClientNow = TRUE;
            }
        } else if (status == BT_STATUS_PENDING)
            App_Report("Creating ACL to remote device...");
        else
            App_Report("No remote device matching client service is available.\n");
    }
}


/*-----------------------------------------------------------------------------
 * quitBlueMgr()
 *      Tell Bluetooth Manager to quit. It starts radio deinitialization.
 *      We can wait for that to complete.
 */
static void quitBlueMgr(MnuTextMenu *menu)
{
    (*BlueMgrMenu->handler)(BlueMgrMenu, M_QUIT, 0);
}


/*---------------------------------------------------------------------------
 * devEventHandler() 
 *      Handler for receiving ME events. Receives asynchronous events and
 *      is executed in stack thread context.
 */
static void devEventHandler(const BtEvent *Event)
{
    switch (Event->eType) {
    case BTEVENT_DEVICE_SELECTED:
        if (Event->errCode == BEC_NO_ERROR) {
            App_Report("Device selected.");
        } else {
            App_Report("DS_SelectDevice() completed with error: %s.", 
                       pHC_Status(Event->errCode));
            if (Event->errCode == BEC_NOT_FOUND)
                App_Report("You must select a device in the Blue Manager.");
        }
        break;

    case BTEVENT_LINK_CONNECT_CNF:
        if (Event->errCode == BEC_NO_ERROR) {
            App_Report("ACL created to %s. %s now can be opened.", 
                       bdaddr_ntoa(&selectedClient->remDev->bdAddr, bdaddrString),
                       selectedClient->port.name);
            openClientNow = TRUE;
        } else {
            App_Report("Connection failed, reason %s.", pHC_Status(Event->errCode));
        }
        break;

    case BTEVENT_LINK_DISCONNECT:
        selectedClient->remDev = 0;
        if (Event->errCode == BEC_NO_ERROR) {
            App_Report("ACL disconnected to %s", 
                       bdaddr_ntoa(&Event->p.bdAddr, bdaddrString));
        }
        break;
    }
}


/*---------------------------------------------------------------------------
 * openAllServerDevices() 
 *      Opens all server devices.
 */
static void openAllServerDevices(void)
{
    int i;
    ServerPort *p;

    /* Open all the servers */
    for (i = 0; i < NUM_SERVERS; i++) {
        p = &serverPort[i];
        if ((p->fd = Open(p->name, 0, 0)) >= 0) {
            App_Report("Server port %s is open.\n", p->name);
        } else {
            App_Report("Server port %s open failed: %d\n", p->name, p->fd);
        }
    }
}


/*---------------------------------------------------------------------------
 * displayClientPorts() 
 *      Display information about all client ports.
 */
static void displayClientPorts(void)
{
    int i;
    ClientPort *p;

    App_Report("");

    for (i = 0; i < NUM_CLIENTS; i++) {
        p = &clientPort[i];

        App_Report("%d. %-20s\r", i, p->port.name);
        if (p->remDev) {
            App_Report(" %-12s\r", bdaddr_ntoa(&p->remDev->bdAddr, 
                                              bdaddrString));
            if (ME_FindRemoteDevice(&p->remDev->bdAddr)) {
                App_Report(" ACL up\r");
            } else {
                App_Report(" no ACL\r");
            }
        } else {
            App_Report(" %12s no ACL\r", "00:00:00:00:00:00");
        }

        App_Report(" %d\r", p->port.rxCnt);
        if (p == selectedClient) {
            App_Report(" selected");
        } else {
            App_Report("");
        }
    }
}
