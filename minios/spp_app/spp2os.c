/***************************************************************************
 *
 * File:
 *     $Workfile:spp2os.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:16$
 *
 * Description:
 *     Sample Port Emulation Entity for RFCOMM.
 *     This file provides conversion routines between OS-specific codes
 *     and Bluetooth codes.
 *
 *
 * Copyright 1999-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "spp2os.h"
#include "sppdrv.h"


/***********************************************************************
 *
 * Defines, Typedefs and Structures
 *
 ***********************************************************************/

#define NOT_SUPPORTED       -1


/*
 * Group: macros to aid in converting to/from RFCOMM codes and OS codes.
 */
#define rfToOs(table, pOs, rf)  \
            convertRfToOs(table, (int *)pOs, (U16)(rf), \
            (RfToOsConversion *)((U8 *)(table) + sizeof(table)))

#define osToRf(table, pRf, os)  \
            convertOsToRf(table, (U16 *)pRf, (int)(os), \
            (RfToOsConversion *)((U8 *)(table) + sizeof(table)))

/*
 * Macro to clear and set bits. Bits are cleared before bits are set.
 */
#define setBits(tgt, clr, set)  \
                do { \
                    (tgt) &= ~(clr); \
                    (tgt) |= set; \
                } while (0)


/*
 *  Structure that relates OS-specific ioctl arguments to RFCOMM equivalent.
 */
struct _RfToOsConversion {
    U16 rfArg;
    int osArg;
};



/***********************************************************************
 *
 * Function Prototypes
 *
 ***********************************************************************/
static BOOL convertRfToOs(RfToOsConversion *table, int *osVal, U16 rfVal,
                          RfToOsConversion *end);
static BOOL convertOsToRf(RfToOsConversion *table, U16 *rfVal, int osVal,
                          RfToOsConversion *end);
 
 


/***********************************************************************
 *
 * ROM Variables
 *
 ***********************************************************************/

/*
 *  This is a cross-reference of RFCOMM defines to OS defines.
 *  If a define or setting is not supported on one side or the other,
 *  the value must be omitted altogether. 
 *
 *  NOTE: that the typedef for RfToOsConversion specifies 'const' to make
 *        it easier to change all references.
 */
static RfToOsConversion rfOsBaud[] = {
    {RF_BAUD_2400,   BPS_2400},
    {RF_BAUD_4800,   BPS_4800},
/*    {RF_BAUD_7200,   NOT_SUPPORTED}, */
    {RF_BAUD_9600,   BPS_9600},
    {RF_BAUD_19200,  BPS_19200},
    {RF_BAUD_38400,  BPS_38400},
    {RF_BAUD_57600,  BPS_57600},
    {RF_BAUD_115200, BPS_115200}
/*    {RF_BAUD_230400, NOT_SUPPORTED} */
};

static RfToOsConversion rfOsDataSize[] = {
    {RF_DATA_BITS_8,   CHAR_SIZE_8},
    {RF_DATA_BITS_7,   CHAR_SIZE_7},
    {RF_DATA_BITS_6,   CHAR_SIZE_6},
    {RF_DATA_BITS_5,   CHAR_SIZE_5}
};

static RfToOsConversion rfOsStopBits[] = {
    {RF_STOP_BITS_1,    STOP_BITS_1},
/*    {NOT_SUPPORTED,     STOP_BITS_2}, */
    {RF_STOP_BITS_1_5,  STOP_BITS_1_5}
};

static RfToOsConversion rfOsParity[] = {
    {RF_PARITY_TYPE_EVEN,    PAR_EVEN},
    {RF_PARITY_TYPE_ODD,     PAR_ODD},
    {RF_PARITY_TYPE_MARK,    PAR_MARK},
    {RF_PARITY_TYPE_SPACE,   PAR_SPACE},
    {RF_PARITY_ON,           PAR_ENABLE}
};

/*
 * It's important to have the OR'd values precede single values where
 * there are duplicate entries. E.g,. FLOW_XON_XOFF has 3 entries. When
 * converting RF->OS, XON/XOFF corresponds to any value. When going the 
 * opposite direction, setting XON/XOFF will enable XON/XOFF for both
 * input and output.
 */
static RfToOsConversion rfOsFlowControl[] = {
    {RF_FLOW_CTRL_NONE,                    FLOW_NONE},
    {RF_XON_ON_INPUT | RF_XON_ON_OUTPUT,   FLOW_XON_XOFF},
    {RF_XON_ON_INPUT,                      FLOW_XON_XOFF},
    {RF_XON_ON_OUTPUT,                     FLOW_XON_XOFF},
    {RF_RTR_ON_INPUT | RF_RTR_ON_OUTPUT,   FLOW_RTS_CTS},
    {RF_RTR_ON_INPUT,                      FLOW_RTS_CTS},
    {RF_RTR_ON_OUTPUT,                     FLOW_RTS_CTS},
    {RF_RTC_ON_INPUT | RF_RTC_ON_OUTPUT,   FLOW_DTR_DSR},
    {RF_RTC_ON_INPUT,                      FLOW_DTR_DSR},
    {RF_RTC_ON_OUTPUT,                     FLOW_DTR_DSR}
};

/* 
 * Modem Status is somewhat different. The bit corresponding to each signal
 * is either set or clear. The table is scanned once per bit with the results
 * OR'd together. Therefore, we want zero to be valid.
 */
static RfToOsConversion rfOsModemStatus[] = {
    {RF_DSR,    MODEM_STATUS_DSR},
    {RF_CTS,    MODEM_STATUS_CTS},
    {RF_RI,     MODEM_STATUS_RI},
    {RF_CD,     MODEM_STATUS_DCD},
    {RF_DTR,    MODEM_STATUS_DTR},
    {RF_RTS,    MODEM_STATUS_RTS},
    {0,         0}
};

static RfToOsConversion rfOsLineStatus[] = {
    {RF_OVERRUN,    LINE_STATUS_OVR},
    {RF_PARITY,        LINE_STATUS_PARITY},
    {RF_FRAMING,     LINE_STATUS_FRAMING},
    {0,         0}
};


/***********************************************************************
 *
 * Local RAM Variables
 *
 ***********************************************************************/
 


 

/*---------------------------------------------------------------------------
 * osToRfModemStatus() 
 *      Convert OS-specified baud to Bluetooth RFCOMM. Note that the 
 *      application may, or may not, be using a hardware signal for flow
 *      control. This function assumes that the Receive-buffer manager will
 *      actually enforce flow control (not the application). That is, the
 *      app may say "can't receive any more data"; but, as long as our
 *      buffer allows, we can receive it.
 */
BOOL osToRfModemStatus(RfSignals *rfSignals, int status)
{
    int rc;
    U16 rfModemStatus = 0;         /* Need U16 for size conflict */

    rc = osToRf(rfOsModemStatus, &rfModemStatus, status);

    if (rc == TRUE) {
        *rfSignals = (RfSignals)rfModemStatus;
    }

    return rc;
}



/*---------------------------------------------------------------------------
 * osToRfBaud() 
 */
BOOL osToRfBaud(U8 *rfBaud, int osBaud)
{
    int rc;
    U16 baud = 0;         /* Need U16 for size conflict */

    rc = osToRf(rfOsBaud, &baud, osBaud);

    if ( rc == TRUE) {
        *rfBaud = (U8)baud;
    }

    return rc;
}
 



/*---------------------------------------------------------------------------
 * osToRfParity() 
 */
BOOL osToRfParity(RfDataFormat *rfFormat, int parity)
{
    int rc;
    U16 rfParity = 0;         /* Need U16 for size conflict */

    rc = osToRf(rfOsParity, &rfParity, parity & PARITY_TYPE_MASK);

    if (rc == TRUE) {
        setBits(*rfFormat, 
                RF_PARITY_TYPE_MASK | RF_PARITY_MASK,
                (U8)(rfParity & 0xff));
        if (parity & PAR_ENABLE) {
            *rfFormat |= RF_PARITY_ON;
        }
    }

    return rc;
}



/*---------------------------------------------------------------------------
 * osToRfStopBits() 
 */
BOOL osToRfStopBits(RfDataFormat *rfFormat, int stopBits)
{
    int rc;
    U16 rfStopBits = 0;         /* Need U16 for size conflict */

    rc = osToRf(rfOsStopBits, &rfStopBits, stopBits & STOP_MASK);

    if (rc == TRUE) {
        setBits(*rfFormat, RF_STOP_BITS_MASK, (U8)(rfStopBits & 0xff));

    }

    return rc;
}



/*---------------------------------------------------------------------------
 * osToRfDataBits() 
 */
BOOL osToRfDataBits(RfDataFormat *rfFormat, int osDataSize)
{
    int rc;
    U16 rfDataSize = 0;         /* Need U16 for size conflict */

    rc = osToRf(rfOsDataSize, &rfDataSize, osDataSize & CHAR_MASK);

    if (rc == TRUE) {
        setBits(*rfFormat, RF_DATA_BITS_MASK, (U8)(rfDataSize & 0xff));
    }

    return rc;
}



/*---------------------------------------------------------------------------
 * osToRfFlowControl() 
 */
BOOL osToRfFlowControl(RfFlowControl *rfFlow, int flow)
{
    int rc;
    U16 rfFlowControl = 0;         /* Need U16 for size conflict */

    rc = osToRf(rfOsFlowControl, &rfFlowControl, flow & FLOW_MASK);

    if (rc == TRUE) {
        setBits(*rfFlow, 0x3f, (U8)(rfFlowControl & 0xff));
    }

    return rc;
}


/*---------------------------------------------------------------------------
 * btToOsRc() 
 */
OsReturnCode btToOsRc(BtStatus bts)
{
    OsReturnCode rc;

    switch (bts) {
    case BT_STATUS_SUCCESS:
    case BT_STATUS_PENDING:
        rc = OSRC_SUCCESS;
        break;
    case BT_STATUS_FAILED:
        rc = OSRC_PROTOCOL_FAILED;
        break;
    case BT_STATUS_INVALID_PARM:
        rc = OSRC_BAD_PARM;
        break;
    default:
        /* Need to add return code */
        Report(("btToOsRc(%d) default path.\n", (int)bts));
        rc = OSRC_ERROR;
    }

    return rc;
}



/*---------------------------------------------------------------------------
 * rfToOsDataBits() 
 */
BOOL rfToOsDataBits(RfDataFormat rfFormat, int *osSettings)
{
    int rc;

    rc = rfToOs(rfOsDataSize, osSettings, 
                (U16)(rfFormat & RF_DATA_BITS_MASK));

    return rc;
}



/*---------------------------------------------------------------------------
 * rfToOsStopBits() 
 */
BOOL rfToOsStopBits(RfDataFormat rfStopBits, int *osSettings)
{
    int rc;

    rc = rfToOs(rfOsStopBits, osSettings, 
                rfStopBits & RF_STOP_BITS_MASK);

    return rc;
}



/*---------------------------------------------------------------------------
 * rfToOsParity() 
 */
BOOL rfToOsParity(RfDataFormat rfParity, int *osSettings)
{
    int rc;

    rc = rfToOs(rfOsParity, osSettings, 
                rfParity & RF_PARITY_TYPE_MASK);
    if (rc == TRUE) {
        if (rfParity & RF_PARITY_MASK & RF_PARITY_ON) {
            *osSettings |= PAR_ENABLE;
        } else {
            *osSettings |= PAR_NONE;
        }
    }

    return rc;
}



/*---------------------------------------------------------------------------
 * rfToOsBaud() 
 */
BOOL rfToOsBaud(RfBaudRate rfBaud, int *osBaud)
{
    return rfToOs(rfOsBaud, osBaud, rfBaud);
}



/*---------------------------------------------------------------------------
 * rfToOsFlowControl() 
 */
BOOL rfToOsFlowControl(RfDataFormat rfFlow, int *osFlow)
{
    return rfToOs(rfOsFlowControl, osFlow, rfFlow);
}



/*---------------------------------------------------------------------------
 * rfToOsModemStatus() 
 */
BOOL rfToOsModemStatus(U8 rfStatus, int *osStatus, BOOL flowControl)
{
    BOOL rc;

    *osStatus = 0;

    /* Since DTR==DSR and RTS==CTS, don't need to do both */
    rc =  rfToOs(rfOsModemStatus, osStatus, rfStatus & RF_DSR);
    rc |= rfToOs(rfOsModemStatus, osStatus, rfStatus & RF_CTS);
    rc |= rfToOs(rfOsModemStatus, osStatus, rfStatus & RF_RI);
    rc |= rfToOs(rfOsModemStatus, osStatus, rfStatus & RF_CD);

    if (rfStatus & RF_FLOW) {
        /* The other device flowed off the link */
        if(flowControl & RF_FLOW_RTS_CTS) {
            /* RTS/CTS is in use. */
            *osStatus &= ~MODEM_STATUS_CTS;
        } else if (flowControl & RF_FLOW_DTR_DSR) {
            *osStatus &= ~MODEM_STATUS_DSR;
        }
    }

    return rc;
}



/*---------------------------------------------------------------------------
 * rfToOsLineStatus() 
 */
BOOL rfToOsLineStatus(RfLineStatus *signals, int *osStatus)
{
    *osStatus = 0;
    return rfToOs(rfOsLineStatus, osStatus, *signals);
}


/*---------------------------------------------------------------------------
 * convertRfToOs() 
 *      Converts an RFCOMM parameter to its OS equivalent.
 *      
 * Requires:
 *      *osVal is NOT cleared unless baud rate is being converted. This
 *      allows values to be OR'd together.
 *
 * Parameters:
 *      table   pointer to conversion table to use
 *      osVal   pointer to variable to receive OS equivalent code
 *      rfVal   RFCOMM code
 *      end     pointer past last entry of 'table'
 *
 * Returns:
 *      TRUE    conversion succeeded
 *      FALSE   failed - no equivalent code
 */
static BOOL convertRfToOs(RfToOsConversion *table, int *osVal, U16 rfVal,
                          RfToOsConversion *end)
{
    BOOL rc = FALSE;

    if (table == rfOsBaud) {
         /* Conversion is 1-to-1 replacement */
        for ( ; table < end; table++) {
            if (table->rfArg == rfVal) {
                *osVal = table->osArg;
                rc = TRUE;
                break;
            }
        }
    } else {
        /* Conversion is accumulated; i.e., there may be >1 match */
        for ( ; table < end; table++) {
            /* rfVal may be 0 in which case a simple & won't work */
            if (rfVal == table->rfArg) {
                *osVal |= table->osArg;
                rc = TRUE;
            }
        }
    }
    
    return rc;
}


/*---------------------------------------------------------------------------
 * convertOsToRf() 
 *      Converts an OS parameter to its RFCOMM equivalent.
 *
 * Requires:
 *
 * Parameters:
 *      table   pointer to conversion table to use
 *      rfVal   pointer to variable to receive RFCOMM equivalent code
 *      osVal   OS code
 *      end     pointer past last entry of 'table'
 *
 * Returns:
 *      TRUE    conversion succeeded
 *      FALSE   failed - no equivalent code
 */
static BOOL convertOsToRf(RfToOsConversion *table, U16 *rfVal, int osVal,
                          RfToOsConversion *end)
{
    BOOL rc = FALSE;

    *rfVal = 0;

    if (table == rfOsBaud) {
         /* Conversion is 1-to-1 replacement */
        for ( ; table < end; table++) {
            if (table->osArg == osVal) {
                *rfVal = table->rfArg;
                rc = TRUE;
                break;
            }
        }
    } else {
        /* Conversion is by bit and may involve >1 element */
        for ( ; table < end; table++) {
            if (table->osArg == osVal) {
                *rfVal |= table->rfArg;
                rc = TRUE;
            }
        }
    }

    return rc;
}

