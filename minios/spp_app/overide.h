#ifndef __OVERIDE_H
#define __OVERIDE_H
/****************************************************************************
 *
 * File:
 *     $Workfile:overide.h$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:25$
 *
 * Description:
 *     Configuration overrides for the Serial API project.
 * 
 * Copyright 1999-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions,
 * Inc.  This work contains confidential and proprietary information of
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret,
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

/* WARNING: The values in this overide.h file were selected specifically for
 * this sample application. If you change them, the sample application may fail
 * to compile or not work properly.
 */

#define BT_STACK                 XA_ENABLED

/* Required if your device is a server allowing multiple simultaneous incoming
 * connections.
 */
#define BT_ALLOW_SCAN_WHILE_CON  XA_ENABLED

/* Only needed for the USB transport. */
#define HCI_USB_TX_BUFFER_SIZE  260

/* Initialize SPP driver */
#define XA_LOAD_LIST    XA_MODULE(SPPDRV)

#define BT_SECURITY     XA_ENABLED
#define XA_SNIFFER      XA_ENABLED

#endif /* __OVERIDE_H */
