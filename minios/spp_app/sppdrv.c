/***************************************************************************
 *
 * File:
 *     $Workfile:sppdrv.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:30$
 *
 * Description:
 *      This is a little glue layer that performs functions two functions:
 *
 *      1) Initialize the serial devices.
 *
 *      2) Provides a thin layer normally provided by an OS. It implements 
 *      a POSIX-like API for the application to call strictly for 
 *      demonstration purposes. When porting to an OS, these functions
 *      can probably be eliminated.
 *
 * Copyright 1999-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "memory.h"
#include "windows.h"
#include "sppdrv.h"
#include "sppos.h"
#include "spp2os.h"
#include "ring.h"
#include "sdp.h"

 
/****************************************************************************
 *
 * Types and Constants
 *
 ****************************************************************************/

#define NUM_SPP_DEVS        9
#define SPP_BASENAME        "/dev/spp/"
#define SPP_BASENAME_LEN    (sizeof(SPP_BASENAME) - 1)   /* exclude trailing 0 */

/*
 *  Ring buffer sizes.
 *  127 is the default RFCOMM buffer size. For tx, we'll arbitrarily make
 *  it 3x.
 */
#define RING_RX_BUFSIZE     (RF_MAX_FRAME_SIZE * 8)
#define RING_TX_BUFSIZE     (RF_MAX_FRAME_SIZE * 3)



/****************************************************************************
 *
 * Local function prototypes
 *
 ****************************************************************************/
static int getDev(const char *devName);



/****************************************************************************
 *
 * ROM Data
 *
 ****************************************************************************/

#if SPP_SERVER == XA_ENABLED

/*
 * Public Browse Group Attribute
 *
 *     Any SdpAttribute structure can include this attribute to add itself
 *     to the root level of the Public Browse Group.
 */
static const U8 publicBrowseGroup[] = {
    SDP_ATTRIB_HEADER_8BIT(3),          /* 3 bytes */
    SDP_UUID_16BIT(SC_PUBLIC_BROWSE_GROUP),  /* Public Browse Group */
};

/* Attribute value of Language Base. This can be common to all/any endpoint. */
static const U8 languageBase[] = {
    SDP_ATTRIB_HEADER_8BIT(9),   /* Data element sequence, 9 bytes */
    SDP_UINT_16BIT(0x656E),      /* Uint16 "en" (English) */
    SDP_UINT_16BIT(0x006A),      /* Uint16 UTF-8 encoding */
    SDP_UINT_16BIT(0x0100)       /* Uint16 primary language base ID */
};


/*-------------------------------------------------------------------------
 * Serial Port Entity SDP attributes
 *
 * Value of service class ID is a data element sequence of UUIDs
 */
static const U8 serialPortServiceClassIdList[] = {
    SDP_ATTRIB_HEADER_8BIT(3),
    SDP_UUID_16BIT(SC_SERIAL_PORT)
};

/* Value of the protocol descriptor list.
 * Note: this is not ROM since it will be modified but put here to keep
 * SDP attributes together. 
 */
static U8 serialPortProtocolDescriptorList[] = {
    SDP_ATTRIB_HEADER_8BIT(12),
    SDP_ATTRIB_HEADER_8BIT(3),         /* L2CAP descriptor */
    SDP_UUID_16BIT(PROT_L2CAP),
    SDP_ATTRIB_HEADER_8BIT(5),         /* RFCOMM descriptor */
    SDP_UUID_16BIT(PROT_RFCOMM),
    SDP_UINT_8BIT(0xa5)                /* RFCOMM Channel: set at run time */
};

/* Attribute value of Serial Port Service Name */
static const U8 serialPortServiceName[] = {
    SDP_TEXT_8BIT(12),
    'S','e','r','i','a','l',' ','P','o','r','t', 0x00
};

/* Attribute IDs in ascending order. */
static SdpAttribute serialPortSdpAttribs[] = {
    /* Service Class ID List attribute */
    SDP_ATTRIBUTE(AID_SERVICE_CLASS_ID_LIST, serialPortServiceClassIdList),
    /* Protocol Descriptor List attribute */
    SDP_ATTRIBUTE(AID_PROTOCOL_DESC_LIST, serialPortProtocolDescriptorList),
    /* Public Browse Group Service */
    SDP_ATTRIBUTE(AID_BROWSE_GROUP_LIST, publicBrowseGroup), 
    /* Language Base ID List attribute */
    SDP_ATTRIBUTE(AID_LANG_BASE_ID_LIST, languageBase),
    /* Serial Port Profile Service Name */
    SDP_ATTRIBUTE(AID_SERVICE_NAME + 0x0100, serialPortServiceName)
};


/*-------------------------------------------------------------------------
 *
 * Dialup Networking SDP record attributes
 */

/* Attribute value of Dial-up Networking Service class ID list */
static const U8 dialupServiceClass[] = {
    SDP_ATTRIB_HEADER_8BIT(6),            /* Data element sequence, 6 bytes */
    /* ServiceClassIdList - most specific to most general class */
    SDP_UUID_16BIT(SC_DIALUP_NETWORKING), /* Uuid16 Dialup Networking */
    SDP_UUID_16BIT(SC_GENERIC_NETWORKING) /* Uuid16 Generic Networking */
};

/* Attribute value of Dial-up Netwokring Protocol descriptor list.
 * Note: this is not ROM since it will be modified but put here to keep
 * SDP attributes together. 
 */
static U8 dialupProtocolDescriptor[] = {
    SDP_ATTRIB_HEADER_8BIT(12),  /* Data element sequence, 12 bytes */
    SDP_ATTRIB_HEADER_8BIT(3),   /* Data element sequence for L2CAP, 3 bytes */
    SDP_UUID_16BIT(PROT_L2CAP),  /* Uuid16 L2CAP */
    SDP_ATTRIB_HEADER_8BIT(5),   /* Data element sequence for RFCOMM, 5 bytes */
    SDP_UUID_16BIT(PROT_RFCOMM), /* Uuid16 RFCOMM */
    SDP_UINT_8BIT(0xa5)          /* Uint8 RFCOMM channel number - value can vary */
};

/* Attribute value of Dial-up networking profile descriptor list */
static const U8 dialupProfileDescriptor[] = {
    SDP_ATTRIB_HEADER_8BIT(8),            /* Data element sequence, 8 bytes */
    SDP_ATTRIB_HEADER_8BIT(6),            /* Data element sequence for ProfileDescriptor, 6 bytes */
    SDP_UUID_16BIT(SC_DIALUP_NETWORKING), /* Uuid16 Dialup networking */
    SDP_UINT_16BIT(0x0100)                /* Uint16 version number */
};

/* Attribute value of Dialup networking service name */
static const U8 dialupServiceName[] = {
    SDP_TEXT_8BIT(19),           /* Null terminated text string, 19 bytes */
    'D','i','a','l','-','u','p',' ',
    'N','e','t','w','o','r','k','i','n','g',0x00
};

/* Attribute value of Dialup networking audio feedback support */
static const U8 dialupAudioFeedback[] = {
    SDP_BOOL(0)                  /* No Audio Feedback Support */
};

static SdpAttribute dialupSdpAttribs[] = {
    /* Dialup networking service class ID list */
    SDP_ATTRIBUTE(AID_SERVICE_CLASS_ID_LIST, dialupServiceClass),
    /* Dialup networking protocol descriptor list */
    SDP_ATTRIBUTE(AID_PROTOCOL_DESC_LIST, dialupProtocolDescriptor),
    /* Public Browse Group Service */
    SDP_ATTRIBUTE(AID_BROWSE_GROUP_LIST, publicBrowseGroup),
    /* Dialup networking language base attribute ID list */
    SDP_ATTRIBUTE(AID_LANG_BASE_ID_LIST, languageBase),
    /* Dialup networking Bluetooth profile descriptor list */
    SDP_ATTRIBUTE(AID_BT_PROFILE_DESC_LIST, dialupProfileDescriptor),
    /* Dialup networking service name */
    SDP_ATTRIBUTE((AID_SERVICE_NAME + 0x0100), dialupServiceName),
    /* Audio Feedback Support - where is this attribute defined? */
    SDP_ATTRIBUTE(AID_AUDIO_FEEDBACK_SUPPORT, dialupAudioFeedback)
};


/*-------------------------------------------------------------------------
 *
 * Fax SDP record attributes
 */

/* Attribute value of Fax Service class ID list */
static const U8 faxServiceClass[] = {
    SDP_ATTRIB_HEADER_8BIT(6),           /* Data element sequence, 6 bytes */
    /* ServiceClassIdList - most specific to most general class */
    SDP_UUID_16BIT(SC_FAX),              /* Uuid16 Fax */
    SDP_UUID_16BIT(SC_GENERIC_TELEPHONY) /* Uuid16 Generic telephony */
};

/* Attribute value of Fax protocol descriptor list */
static U8 faxProtocolDescriptor[] = {
    SDP_ATTRIB_HEADER_8BIT(12),   /* Data element sequence, 12 bytes */
    SDP_ATTRIB_HEADER_8BIT(3),    /* Data element sequence for L2CAP, 3 bytes */
    SDP_UUID_16BIT(PROT_L2CAP),   /* Uuid16 L2CAP */
    SDP_ATTRIB_HEADER_8BIT(5),    /* Data element sequence for RFCOMM, 5 bytes */
    SDP_UUID_16BIT(PROT_RFCOMM),  /* Uuid16 RFCOMM */
    SDP_UINT_8BIT(0xa5)           /* Uint8 RFCOMM channel number - value can vary */
};

/* Attribute value of Fax Bluetooth profile descriptor list */
static const U8 faxProfileDescriptor[] = {
    SDP_ATTRIB_HEADER_8BIT(8), /* Data element sequence, 8 bytes */
    SDP_ATTRIB_HEADER_8BIT(6), /* Data element sequence for ProfileDescriptor, 6 bytes */
    SDP_UUID_16BIT(SC_FAX),    /* Uuid16 fax */
    SDP_UINT_16BIT(0x0100)     /* Uint16 version number */
};

/* Attribute value of Fax Service name */
static const U8 faxServiceName[] = {
    SDP_TEXT_8BIT(4),       /* Null terminated text string, 4 bytes */
    'F','a','x',0x00        /* Text string */
};

/* Attribute value of Fax Audio Feeback */
static const U8 faxAudioFeedbackSupport[] = {
    SDP_BOOL(0)                  /* No Audio Feedback Support */
};

/* Attribute value of Fax Class 1 Support */
static const U8 faxClass1Support[] = {
    SDP_BOOL(0)                  /* No Fax Class 1 Support */
};

/* Attribute value of Fax Class 2.0 Support */
static const U8 faxClass20Support[] = {
    SDP_BOOL(0)                  /* No Fax Class 2.0 Support */
};

/* Attribute value of Fax Class 2 Support */
static const U8 faxClass2Support[] = {
    SDP_BOOL(0)                  /* No Fax Class 2 Support */
};

static SdpAttribute faxSdpAttribs[] = {
    /* Fax service class ID list */
    SDP_ATTRIBUTE(AID_SERVICE_CLASS_ID_LIST, faxServiceClass),
    /* Fax protocol descriptor list */
    SDP_ATTRIBUTE(AID_PROTOCOL_DESC_LIST, faxProtocolDescriptor),
    /* Public Browse Group Service */
    SDP_ATTRIBUTE(AID_BROWSE_GROUP_LIST, publicBrowseGroup), 
    /* Fax language base attribute ID list */
    SDP_ATTRIBUTE(AID_LANG_BASE_ID_LIST, languageBase),
    /* Fax Bluetooth profile descriptor list */
    SDP_ATTRIBUTE(AID_BT_PROFILE_DESC_LIST, faxProfileDescriptor),
    /* Fax service name */
    SDP_ATTRIBUTE((AID_SERVICE_NAME + 0x0100), faxServiceName),
    /* Fax class 1 support */
    SDP_ATTRIBUTE(AID_FAX_CLASS_1_SUPPORT, faxClass1Support),
    /* Fax class 2.0 support */
    SDP_ATTRIBUTE(AID_FAX_CLASS_20_SUPPORT, faxClass20Support),
    /* Fax class 2 support */
    SDP_ATTRIBUTE(AID_FAX_CLASS_2_SUPPORT, faxClass2Support),
    /* Audio feedback support */
    SDP_ATTRIBUTE(AID_AUDIO_FEEDBACK_SUPPORT, faxAudioFeedbackSupport)
};



/*-------------------------------------------------------------------------
 *
 * LAN Access SDP record attributes
 */

/*
 * begin LAN service common attributes (common to all LAN servers)
 */

/* Attribute value of LAN Service Name */
static const U8 lanServiceName[] = {
    SDP_TEXT_8BIT(21),           /* Null terminated text string, 21 bytes */
    'L','A','N',' ',             /* Text String */
    'A','c','c','e','s','s',' ',
    'u','s','i','n','g',' ',
    'P','P','P',0x00
};

/* Attribute value of LAN Service Description */
static const U8 lanServiceDescription[] = {
    SDP_TEXT_8BIT(21),           /* Null terminated text string, 21 bytes */
    'L','A','N',' ',             /* Text String */
    'A','c','c','e','s','s',' ',
    'u','s','i','n','g',' ',
    'P','P','P',0x00
};

/* Attribute value of LAN Service Class Id List */ 
static const U8 lanServiceClass[] = {
    SDP_ATTRIB_HEADER_8BIT(3),          /* Data element sequence, 3 bytes */
    SDP_UUID_16BIT(SC_LAN_ACCESS_PPP)   /* Uuid16 LAN Access using PPP */
};

/* Attribute value of LAN Bluetooth Profile Descriptor List */
static const U8 lanProfileDescriptor[] = {
    SDP_ATTRIB_HEADER_8BIT(8),           /* Data element sequence, 8 bytes */
    SDP_ATTRIB_HEADER_8BIT(6),           /* Data element sequence for ProfileDescriptor, 6 bytes */
    SDP_UUID_16BIT(SC_LAN_ACCESS_PPP),   /* Uuid16 LAN Access using PPP */
    SDP_UINT_16BIT(0x0100)               /* Uint16 version number */
};
/* end LAN service common attributes (common to all LAN servers) */

/*
 * Begin LAN Server Device 0
 */

/* Attribute value of LAN Protocol Descriptor List. */
static U8 lanProtocolDescriptor0[] = {
    SDP_ATTRIB_HEADER_8BIT(12),  /* Data element sequence, 12 bytes */
    SDP_ATTRIB_HEADER_8BIT(3),   /* Data element sequence for L2CAP, 3 bytes */     
    SDP_UUID_16BIT(PROT_L2CAP),  /* Uuid16 L2CAP */
    SDP_ATTRIB_HEADER_8BIT(5),   /* Data element sequence for RFCOMM, 5 bytes */
    SDP_UUID_16BIT(PROT_RFCOMM), /* Uuid16 RFCOMM */
    SDP_UINT_8BIT(0xa5)          /* Uint8 RFCOMM channel number - value can vary */
};

/* Attribute value of LAN Service Availability */
static const U8 LanServiceAvailability[] = {
    SDP_UINT_8BIT(0xFF)          /* Service not in use, accepting new clients 
                                  * This value can vary 
                                  */
};

/* Attribute list for LAN device 0 */
static SdpAttribute lanSdpAttribs0[] = {
    /* Service Class ID List attribute */
    SDP_ATTRIBUTE(AID_SERVICE_CLASS_ID_LIST, lanServiceClass),
    /* Protocol Descriptor List attribute */
    SDP_ATTRIBUTE(AID_PROTOCOL_DESC_LIST, lanProtocolDescriptor0),
    /* Public Browse Group Service */
    SDP_ATTRIBUTE(AID_BROWSE_GROUP_LIST, publicBrowseGroup), 
    /* Language Base ID List attribute */
    SDP_ATTRIBUTE(AID_LANG_BASE_ID_LIST, languageBase),
    /* Service Availability attribute */
    SDP_ATTRIBUTE(AID_SERVICE_AVAILABILITY, LanServiceAvailability),
    /* Bluetooth Profile Descriptor List attribute */
    SDP_ATTRIBUTE(AID_BT_PROFILE_DESC_LIST, lanProfileDescriptor),
    /* Serial Port Profile Service Name */
    SDP_ATTRIBUTE((AID_SERVICE_NAME + 0x0100), lanServiceName),
    /* Service Description attribute */
    SDP_ATTRIBUTE((AID_SERVICE_DESCRIPTION + 0x0100), lanServiceDescription)
};

/* end LAN Device 0 */

/*
 * Begin LAN Server Device 1
 */

/* Attribute value of LAN Protocol Descriptor List. */
static U8 lanProtocolDescriptor1[] = {
    SDP_ATTRIB_HEADER_8BIT(12),  /* Data element sequence, 12 bytes */
    SDP_ATTRIB_HEADER_8BIT(3),   /* Data element sequence for L2CAP, 3 bytes */     
    SDP_UUID_16BIT(PROT_L2CAP),  /* Uuid16 L2CAP */
    SDP_ATTRIB_HEADER_8BIT(5),   /* Data element sequence for RFCOMM, 5 bytes */
    SDP_UUID_16BIT(PROT_RFCOMM), /* Uuid16 RFCOMM */
    SDP_UINT_8BIT(0xa5)          /* Uint8 RFCOMM channel number - value can vary */
};

/* Attribute list for LAN device 1 */
static SdpAttribute lanSdpAttribs1[] = {
    /* Service Class ID List attribute */
    SDP_ATTRIBUTE(AID_SERVICE_CLASS_ID_LIST, lanServiceClass),
    /* Protocol Descriptor List attribute */
    SDP_ATTRIBUTE(AID_PROTOCOL_DESC_LIST, lanProtocolDescriptor1),
    /* Public Browse Group Service */
    SDP_ATTRIBUTE(AID_BROWSE_GROUP_LIST, publicBrowseGroup), 
    /* Language Base ID List attribute */
    SDP_ATTRIBUTE(AID_LANG_BASE_ID_LIST, languageBase),
    /* Service Availability attribute */
    SDP_ATTRIBUTE(AID_SERVICE_AVAILABILITY, LanServiceAvailability),
    /* Bluetooth Profile Descriptor List attribute */
    SDP_ATTRIBUTE(AID_BT_PROFILE_DESC_LIST, lanProfileDescriptor),
    /* Serial Port Profile Service Name */
    SDP_ATTRIBUTE((AID_SERVICE_NAME + 0x0100), lanServiceName),
    /* Service Description attribute */
    SDP_ATTRIBUTE((AID_SERVICE_DESCRIPTION + 0x0100), lanServiceDescription)
};
/* end LAN Server Device 1 */

/* end of SDP data base records */
#endif /* SPP_SERVER == XA_ENABLED */


#if SPP_CLIENT == XA_ENABLED

/*-------------------------------------------------------------------------
 *
 * SDP query info for client ports.
 * A separate service search request is needed for each service. That is,
 * if more than one client is connecting to the same service type, only
 * one search request is required for those two clients.
 */
static const U8 lanAccessSdpServiceSearchReq[] = {
    /* 
     * Data Element Sequence of Services 
     */
    DETD_SEQ + DESD_ADD_8BITS,  /* Header for attribute ID list */
    9,                          /* Size of the list */

    /* Serial Port */
    DETD_UUID + DESD_2BYTES,                   /* type & size index 0x19 */
    (U8)(((SC_LAN_ACCESS_PPP) & 0xff00) >> 8), /* Bits[15:8] of UUID */    
    (U8)((SC_LAN_ACCESS_PPP) & 0x00ff),        /* Bits[7:0] of UUID */
    
    /* RFCOMM */
    DETD_UUID + DESD_2BYTES,    /* Header for RFCOMM UUID */
    0x00, 0x03,                 /* UUID for RFCOMM in Big Endian */

    /* L2CAP */
    DETD_UUID + DESD_2BYTES,     /* UUID header of size 2 bytes */
    0x01, 0x00,                  /* L2CAP UUID */

    /* 
     * Max number of service record handles to return 
     */
    0x00, 0x64,

    /* 
     * Data Element Sequence of Attribute List 
     */
    DETD_SEQ + DESD_ADD_8BITS,  /* Header for attribute ID list */
    3,                          /* Size of the list */
    DETD_UINT + DESD_2BYTES,    /* Header for Protocol descriptor list ID */
    0x00, 0x04                  /* Value of protocol descriptor list ID */
};

static const U8 dunSdpServiceSearchReq[] = {
    /* 
     * Data Element Sequence of Services 
     */
    DETD_SEQ + DESD_ADD_8BITS,  /* Header for attribute ID list */
    9,                          /* Size of the list */

    /* Serial Port */
    DETD_UUID + DESD_2BYTES,                   /* type & size index 0x19 */
    (U8)(((SC_DIALUP_NETWORKING) & 0xff00) >> 8), /* Bits[15:8] of UUID */    
    (U8)((SC_DIALUP_NETWORKING) & 0x00ff),        /* Bits[7:0] of UUID */
    
    /* RFCOMM */
    DETD_UUID + DESD_2BYTES,    /* Header for RFCOMM UUID */
    0x00, 0x03,                 /* UUID for RFCOMM in Big Endian */

    /* L2CAP */
    DETD_UUID + DESD_2BYTES,     /* UUID header of size 2 bytes */
    0x01, 0x00,                  /* L2CAP UUID */

    /* 
     * Max number of service record handles to return 
     */
    0x00, 0x64,

    /* 
     * Data Element Sequence of Attribute List 
     */
    DETD_SEQ + DESD_ADD_8BITS,  /* Header for attribute ID list */
    3,                          /* Size of the list */
    DETD_UINT + DESD_2BYTES,    /* Header for Protocol descriptor list ID */
    0x00, 0x04                  /* Value of protocol descriptor list ID */
};

static const U8 faxAccessSdpServiceSearchReq[] = {
    /* 
     * Data Element Sequence of Services 
     */
    DETD_SEQ + DESD_ADD_8BITS,  /* Header for attribute ID list */
    9,                          /* Size of the list */

    /* Serial Port */
    DETD_UUID + DESD_2BYTES,        /* type & size index 0x19 */
    (U8)(((SC_FAX) & 0xff00) >> 8), /* Bits[15:8] of UUID */    
    (U8)((SC_FAX) & 0x00ff),        /* Bits[7:0] of UUID */
    
    /* RFCOMM */
    DETD_UUID + DESD_2BYTES,    /* Header for RFCOMM UUID */
    0x00, 0x03,                 /* UUID for RFCOMM in Big Endian */

    /* L2CAP */
    DETD_UUID + DESD_2BYTES,     /* UUID header of size 2 bytes */
    0x01, 0x00,                  /* L2CAP UUID */

    /* 
     * Max number of service record handles to return 
     */
    0x00, 0x64,

    /* 
     * Data Element Sequence of Attribute List 
     */
    DETD_SEQ + DESD_ADD_8BITS,  /* Header for attribute ID list */
    3,                          /* Size of the list */
    DETD_UINT + DESD_2BYTES,    /* Header for Protocol descriptor list ID */
    0x00, 0x04                  /* Value of protocol descriptor list ID */
};


/*-------------------------------------------------------------------------
 *
 * SDP query info
 */
static const U8 serialPortSdpServiceSearchReq[] = {
    /* 
     * Data Element Sequence of Services 
     */
    DETD_SEQ + DESD_ADD_8BITS,  /* Header for attribute ID list */
    9,                          /* Size of the list */

    /* Serial Port */
    DETD_UUID + DESD_2BYTES,                /* type & size index 0x19 */
    (U8)(((SC_SERIAL_PORT) & 0xff00) >> 8), /* Bits[15:8] of UUID */    
    (U8)((SC_SERIAL_PORT) & 0x00ff),        /* Bits[7:0] of UUID */
    
    /* RFCOMM */
    DETD_UUID + DESD_2BYTES,    /* Header for RFCOMM UUID */
    0x00, 0x03,                 /* UUID for RFCOMM in Big Endian */

    /* L2CAP */
    DETD_UUID + DESD_2BYTES,     /* UUID header of size 2 bytes */
    0x01, 0x00,                  /* L2CAP UUID */

    /* 
     * Max number of service record handles to return 
     */
    0x00, 0x64,

    /* 
     * Data Element Sequence of Attribute List 
     */
    DETD_SEQ + DESD_ADD_8BITS,  /* Header for attribute ID list */
    3,                          /* Size of the list */
    DETD_UINT + DESD_2BYTES,    /* Header for Protocol descriptor list ID */
    0x00, 0x04                  /* Value of protocol descriptor list ID */
};

#endif /* SPP_CLIENT == XA_ENABLED */



/****************************************************************************
 *
 * RAM Data
 *
 ****************************************************************************/

/* 
 *  Structures for our read/write buffers. These would probably be 
 *  allocated by the OS using whatever services and structures available
 *  for buffer management.
 */
static RingBuf ringRBuf[NUM_SPP_DEVS];
static RingBuf ringWBuf[NUM_SPP_DEVS];

/*
 *  Similar to the structures above, the actual data buffers would surely
 *  be dynamically allocated and reallocated. We'll allocate read and write
 *  buffers for each device. Note 'read' and 'write' are from the local
 *  perspective: receive data into 'read', send data via 'write'.
 *
 *  We'll make the rx buffer 6x for flow-control reasons. When we flow
 *  off rx, there still could be packets in the pipe. We'll flow off when
 *  there is room for an additional 3 packets. Note that it has not been
 *  imperically determined how many packets may be in the pipeline.
 */
static U8 rBuffer[NUM_SPP_DEVS][RING_RX_BUFSIZE];
static U8 wBuffer[NUM_SPP_DEVS][RING_TX_BUFSIZE];

/* Device structures */
static SPP_DEV_OS sppOsDev[NUM_SPP_DEVS];

/* Tx Packets for each device. The number is arbitrary. >1 is recommended
 * for high rates of outbound data. Note: because this sample does not
 * copy data from the ring buffer to txpacket, it can only use 1 for number
 * of tx packets.
 */
static BtPacket txPacket[NUM_SPP_DEVS][1];

#if SPP_SERVER == XA_ENABLED
/* RFCOMM service structures server devices */
static SppService dialupService;
static SppService serialPortService;
static SppService lanService;
static SppService faxService;

/* Server SDP records */
static SdpRecord   serialPortSdpRecord; 
static SdpRecord   dialupSdpRecord; 
static SdpRecord   lanSdpRecord0; 
static SdpRecord   lanSdpRecord1; 
static SdpRecord   faxSdpRecord; 
#endif /* SPP_SERVER == XA_ENABLED */




/*---------------------------------------------------------------------------
 * SPPDRV_Init() 
 *      This function is OS-specific. It should be called during system
 *      init. It would probably initialize the driver and return pointers
 *      to the driver entry points. It might also create the devices 
 *      themselves.
 *
 * Requires:
 *     
 * Parameters:
 *
 * Returns:
 */
BOOL SPPDRV_Init(void)
{
    int i;
    SPP_DEV_OS *osDev;

#if SPP_SERVER == XA_ENABLED
    /* Initialize dialup service structure. */
    dialupService.service.serviceId = 0;
    dialupService.name              = dialupServiceName;
    dialupService.nameLen           = sizeof(dialupServiceName);
    dialupService.numPorts          = 0;

    /* Dev0 is a DUN server */
    dialupSdpRecord.attribs              = dialupSdpAttribs;
    dialupSdpRecord.num                  = sizeof(dialupSdpAttribs) / 
                                           sizeof(*(dialupSdpAttribs));
    dialupSdpRecord.classOfDevice        = COD_TELEPHONY | COD_NETWORKING; 
    osDev                                = &sppOsDev[0];
    osDev->name                          = "sDialup";
    osDev->sppDev.portType               = SPP_SERVER_PORT;
    osDev->sppDev.type.sppService        = &dialupService;
    osDev->sppDev.type.sppService->sdpRecord = &dialupSdpRecord;

    /* Initialize dialup service structure. */
    lanService.service.serviceId = 0;
    lanService.name              = lanServiceName;
    lanService.nameLen           = sizeof(lanServiceName);
    lanService.numPorts          = 0;

    /* Dev1 is a LAN server */
    lanSdpRecord0.attribs                = lanSdpAttribs0;
    lanSdpRecord0.num                    = sizeof(lanSdpAttribs0) / 
                                           sizeof(*(lanSdpAttribs0));
    lanSdpRecord0.classOfDevice          = COD_TELEPHONY | COD_NETWORKING; 
    osDev                                = &sppOsDev[1];
    osDev->name                          = "sLanAccess0";
    osDev->sppDev.portType               = SPP_SERVER_PORT;
    osDev->sppDev.type.sppService        = &lanService;
    osDev->sppDev.type.sppService->sdpRecord = &lanSdpRecord0;

    /* Dev2 is also a LAN server */
    lanSdpRecord1.attribs                = lanSdpAttribs1;
    lanSdpRecord1.num                    = sizeof(lanSdpAttribs1) / 
                                           sizeof(*(lanSdpAttribs1));
    lanSdpRecord1.classOfDevice          = COD_TELEPHONY | COD_NETWORKING; 
    osDev                                = &sppOsDev[2];
    osDev->name                          = "sLanAccess1";
    osDev->sppDev.portType               = SPP_SERVER_PORT;
    osDev->sppDev.type.sppService        = &lanService;
    osDev->sppDev.type.sppService->sdpRecord = &lanSdpRecord1;

    /* Dev3 is a generic FAX server */
    faxSdpRecord.attribs                 = faxSdpAttribs;
    faxSdpRecord.num                     = sizeof(faxSdpAttribs) / 
                                           sizeof(*(faxSdpAttribs));
    faxSdpRecord.classOfDevice           = COD_TELEPHONY | COD_NETWORKING; 
    osDev                                = &sppOsDev[3];
    osDev->name                          = "sFax";
    osDev->sppDev.portType               = SPP_SERVER_PORT;
    osDev->sppDev.type.sppService        = &faxService;
    osDev->sppDev.type.sppService->sdpRecord = &faxSdpRecord;

    /* Dev4 is a generic serialPort server */
    serialPortSdpRecord.attribs          = serialPortSdpAttribs;
    serialPortSdpRecord.num              = sizeof(serialPortSdpAttribs) / 
                                           sizeof(*(serialPortSdpAttribs));
    serialPortSdpRecord.classOfDevice    = COD_TELEPHONY | COD_NETWORKING; 
    osDev                                = &sppOsDev[4];
    osDev->name                          = "sSerial";
    osDev->sppDev.portType               = SPP_SERVER_PORT;
    osDev->sppDev.type.sppService        = &serialPortService;
    osDev->sppDev.type.sppService->sdpRecord = &serialPortSdpRecord;

#endif /* SPP_SERVER == XA_ENABLED */

#if SPP_CLIENT == XA_ENABLED
    /* Dev5 is a client port for DUN. */
    osDev = &sppOsDev[5];
    osDev->name            = "cDialup";
    osDev->sppDev.portType = SPP_CLIENT_PORT;

    osDev->sppDev.type.client.sdpToken.type  = BSQT_SERVICE_SEARCH_ATTRIB_REQ;
    osDev->sppDev.type.client.sdpToken.parms = dunSdpServiceSearchReq;
    osDev->sppDev.type.client.sdpToken.plen  = sizeof(dunSdpServiceSearchReq);

    /* Dev5 is a client port for LAN Access. */
    osDev = &sppOsDev[6];
    osDev->name            = "cLanAccess";
    osDev->sppDev.portType = SPP_CLIENT_PORT;

    osDev->sppDev.type.client.sdpToken.type  = BSQT_SERVICE_SEARCH_ATTRIB_REQ;
    osDev->sppDev.type.client.sdpToken.parms = lanAccessSdpServiceSearchReq;
    osDev->sppDev.type.client.sdpToken.plen  = sizeof(lanAccessSdpServiceSearchReq);

    /* Dev6 is a client port for FAX. */
    osDev = &sppOsDev[7];
    osDev->name            = "cFax";
    osDev->sppDev.portType = SPP_CLIENT_PORT;

    osDev->sppDev.type.client.sdpToken.type  = BSQT_SERVICE_SEARCH_ATTRIB_REQ;
    osDev->sppDev.type.client.sdpToken.parms = faxAccessSdpServiceSearchReq;
    osDev->sppDev.type.client.sdpToken.plen  = sizeof(faxAccessSdpServiceSearchReq);

    /* Dev7 is a client port for generic Serial Port. */
    osDev = &sppOsDev[8];
    osDev->name            = "cSerial";
    osDev->sppDev.portType = SPP_CLIENT_PORT;

    osDev->sppDev.type.client.sdpToken.type  = BSQT_SERVICE_SEARCH_ATTRIB_REQ;
    osDev->sppDev.type.client.sdpToken.parms = serialPortSdpServiceSearchReq;
    osDev->sppDev.type.client.sdpToken.plen  = sizeof(serialPortSdpServiceSearchReq);

#endif /* SPP_CLIENT == XA_ENABLED */

    /* Do initialization common to all of the ports. */
    for (i = 0; i < NUM_SPP_DEVS; i++) {
        osDev = &sppOsDev[i];
        SPP_InitDevice(&osDev->sppDev, txPacket[i], sizeof(txPacket[i])/sizeof(BtPacket));

        osDev->sppDev.osDev    = &sppOsDev[i];
        osDev->threadId        = 0;

        /* Create mutexes and events with unique names */
        osDev->mutex = CreateMutex(NULL, FALSE, NULL);
        Assert(osDev->mutex);
        osDev->wait  = CreateSemaphore(0, 0, 1, "SPP");
        Assert(osDev->wait);

        /* Setup read buffer */
        osDev->rRing = &ringRBuf[i];
        RING_BufInit(osDev->rRing, rBuffer[i], sizeof(rBuffer[i]));

        /* Setup write buffer */
        osDev->wRing = &ringWBuf[i];
        RING_BufInit(osDev->wRing, wBuffer[i], sizeof(wBuffer[i]));


        if (sppOsDev[i].mutex == NULL) {
            for (i = 0; i < NUM_SPP_DEVS; i++) {
                CloseHandle(sppOsDev[i].mutex);
            }

            return FALSE;
        }
    }

    return TRUE;
}
 

/*---------------------------------------------------------------------------
 * removeDriver() 
 *      Remove driver and release resources.
 *      NOTE that this is only needed since we're not really loading as
 *      a driver.
 *
 * Requires:
 *     
 * Parameters:
 *
 * Returns:
 */
void removeSppDriver(void)
{
    int i;

    for (i = 0; i < NUM_SPP_DEVS; i++) {
        if (sppOsDev[i].sppDev.state != 0) {
            Close(i);
        }
        ReleaseMutex(sppOsDev[i].mutex);
        CloseHandle(sppOsDev[i].mutex);
    }
}



/*---------------------------------------------------------------------------
 * Creat() 
 *      
 * Requires:
 *     
 * Parameters:
 *
 * Returns:
 */
OsReturnCode Creat(const char *devName, int flag)
{
    return OSRC_NOTSUPPORTED;
}

 
 
/*---------------------------------------------------------------------------
 * Remove() 
 *      
 * Requires:
 *     
 * Parameters:
 *
 * Returns:
 */
OsReturnCode Remove(const char *devName)
{
    return OSRC_NOTSUPPORTED;
}
 
 
/*---------------------------------------------------------------------------
 * Open() 
 *      
 * Requires:
 *     
 * Parameters:
 *
 * Returns:
 */
OsReturnCode Open(const char *devName, int flags, int mode)
{
    OsReturnCode  fd;
    BtStatus status = BT_STATUS_FAILED;

    if ((fd = getDev(devName)) >= 0) {
        if (sppOsDev[fd].threadId == 0) {
            status = SPP_Open(&sppOsDev[fd].sppDev, (BtRemoteDevice *)mode);
            if (status == BT_STATUS_SUCCESS) {
                sppOsDev[fd].threadId = GetCurrentThreadId();
            }
        }
    }

    if (status == BT_STATUS_SUCCESS) {
        return fd;
    } else {
        return btToOsRc(status);
    }
}

 
 
/*---------------------------------------------------------------------------
 * Close() 
 *      
 * Requires:
 *     
 * Parameters:
 *
 * Returns:
 */
OsReturnCode Close(int fd)
{
    if ((fd < 0) || (fd >= NUM_SPP_DEVS) 
        || (sppOsDev[fd].threadId != GetCurrentThreadId())) {
        return OSRC_INVALID_HANDLE;
    }

    /* Regardless of whether SPP_Close fails, it's closed as far as this
     * level is concerned.
     */
    sppOsDev[fd].threadId = 0;

    return btToOsRc(SPP_Close(&sppOsDev[fd].sppDev));
}

 
 
/*---------------------------------------------------------------------------
 * Read() 
 *      
 * Requires:
 *     
 * Parameters:
 *
 * Returns:
 */
int Read(int fd, char *buffer, int maxBytes)
{
    BtStatus status;

    if ((fd < 0) || (fd >= NUM_SPP_DEVS)
        || (sppOsDev[fd].threadId != GetCurrentThreadId())) {
        return OSRC_INVALID_HANDLE;
    }

    status = SPP_Read(&sppOsDev[fd].sppDev, buffer, (U16 *)&maxBytes);
    if (status != BT_STATUS_SUCCESS) {
        maxBytes = btToOsRc(status);
    }

    return maxBytes;
}

 
 
 /*---------------------------------------------------------------------------
 * Write() 
 *      
 * Returns:
 */
int Write(int fd, char *buffer, int nBytes)
{
    BtStatus status;

    if ((fd < 0) || (fd >= NUM_SPP_DEVS)
        || (sppOsDev[fd].threadId != GetCurrentThreadId())) {
        return OSRC_INVALID_HANDLE;
    }

    status = SPP_Write(&sppOsDev[fd].sppDev, buffer, (U16* )&nBytes);
    if (status != BT_STATUS_SUCCESS) {
        nBytes = btToOsRc(status);
    }

    return nBytes;
}

 
 
 /*---------------------------------------------------------------------------
 * Ioctl() 
 *      
 * Parameters:
 *
 *      fd - file descriptor for open file
 *      function - ioctl function to perform
 *      arg - argument appropriate for the function; many functions update
 *            the contents of *arg on success
 *
 * Returns:
 *      OS return code
 */
int Ioctl(int fd, int function, void *arg)
{
    BtStatus status;

    if ((fd < 0) || (fd >= NUM_SPP_DEVS) 
        || (sppOsDev[fd].threadId != GetCurrentThreadId())) {
        return OSRC_INVALID_HANDLE;
    }

    status = SPP_Ioctl(&sppOsDev[fd].sppDev, (U16)function, arg);

    return btToOsRc(status);
}



/*---------------------------------------------------------------------------
 * getDev() 
 *      Verify and retrieve the unique device number.
 *      Given a form "/dev/spp/sLanAccess0", return 2.
 *     
 * Parameters:
 *      devName     complete device name (e.g., "/dev/spp/sLanAccess0")
 *
 * Returns:
 *      >=0         device number (e.g., 2)
 *      <0          failed; bad devName
 */
static int getDev(const char *devName)
{
    int  fd;

    if (memcmp(devName, SPP_BASENAME, SPP_BASENAME_LEN) == 0) {
        /* devName starts "/dev/spp/" */
        devName += SPP_BASENAME_LEN;
        for (fd = 0; fd < NUM_SPP_DEVS; fd++) {
            if (memcmp(devName, sppOsDev[fd].name, 
                       strlen(sppOsDev[fd].name)) == 0) {
                break;
            }
        }
    }
    
    if (fd >= NUM_SPP_DEVS) {
        fd = -1;
    }
    return fd;
}

