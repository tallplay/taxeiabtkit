/****************************************************************************
 *
 * File:        sppos.c
 *     $Workfile:sppos.h$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:16$
 *
 * Description: 
 *      This the Windows-specific OS SPP Services layer.
 *      It is at a similar level as a BSP (Board Support Package) include
 *      file might be. That is, it is utilized by a higher layer - in this
 *      case, the Serial Port Profile.
 *
 *      Serial Port Profile Layers:
 *          
 *          application
 *          OS Device API
 *          SPP 
 *          OS SPP Services (including this file)
 *          hardware
 *
 * $Project:XTNDAccess Blue SDK$
 *
 * Copyright 1997-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions,
 * Inc.  This work contains confidential and proprietary information of
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret,
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/
#include "osapi.h"
#include "spp.h"
#include "sppdrv.h"
/* Platform dependent includes */
#include "windows.h"
           


/****************************************************************************
 *
 * Types and Constants
 *
 ****************************************************************************/



/*---------------------------------------------------------------------------
 * Serial Port Profile Configuration API layer
 *
 *     The constants in this layer provide general configuration to
 *     communication protocol stacks compiled into the system. To change
 *     a constant, simply #define it in your overide.h include file.
 *
 *     Configuration constants here and in other configuration API files
 *     are used to make the stack more appropriate for a particular
 *     environment. Constants can be modified to allow tradeoffs
 *     between code size, RAM usage, functionality, and throughput.
 *
 *     Some constants are numeric, and others indicate whether a feature
 *     is enabled (defined as XA_ENABLED) or disabled (defined as
 *     XA_DISABLED).
 */

/****************************************************************************
 *
 * Constants
 *
 ****************************************************************************/



/****************************************************************************
 *
 * Types and Constants
 *
 ****************************************************************************/


/*----------------------------------------------------------------------
 * SPP_DEV_OS type
 *      Serial Port Profile Device structure for Windows implementation.
 *      This structure contains all the fields required for a serial 
 *      device as implemented by the Serial Port Profile. The first member
 *      MUST be of type SppDev. Any other members are for use by 
 *      implementation or OS-specific code.
 */
typedef struct _SPP_DEV_OS {
    SppDev         sppDev;             /* MUST be the first member */

    /* OS-specific members */
    char            *name;          /* Unique device name */
    HANDLE          mutex;          /* mutual exclusion semaphore for lock */
    HANDLE          wait;           /* mutual exclusion semaphore for wait */
    HANDLE          event;          /* event used to block caller */
    DWORD           threadId;       /* Owner of device */
    RingBuf         *rRing;         /* pointer to read buffer */
    RingBuf         *wRing;         /* pointer to write buffer */
} SPP_DEV_OS;
/* End of SPP_DEV_OS */



/****************************************************************************
 *
 * Function Prototypes
 *
 ****************************************************************************/
