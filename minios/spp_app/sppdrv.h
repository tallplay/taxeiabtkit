/***************************************************************************
 *
 * File:
 *     $Workfile:sppdrv.h$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:12$
 *
 * Description:
 *      Header file for sample Port Emulation Entity interface to RFCOMM.
 *      This file is included by programs at the application level.
 *
 * Copyright 1999-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/
#ifndef __SPPDRV_H
#define __SPPDRV_H
 
#include "rfcomm.h"


/*---------------------------------------------------------------------------
 *  Serial API layer
 *
 *  The Serial API layer is an example of an API that can be placed on top
 *  of RFCOMM. This particular example uses the basic I/O function: open,
 *  close, read, write, and ioctl.
 *
 *  The API converts the above OS calls to RFCOMM's own API. Conversely,
 *  events and data that come up from RFCOMM are handled in a way compatible
 *  with the above routines.
 *
 *  If used, it is expected that this sample would be ported to the host's
 *  native environment.
 *
 *  This example allows a calling application to open either a service or
 *  client port (/dev/rfc0 and /dev/rfc1 respectively).
 */
 

/***********************************************************************
 *
 * Constants
 *
 ***********************************************************************/
  
       


/****************************************************************************
 *
 * Types
 *
 ****************************************************************************/
           
/*---------------------------------------------------------------------------
 * OsReturnCode type
 *
 *     These should be replaced by your operating system's basic IO error
 *     codes. If there is an error peculiar to this driver, it must be 
 *     assigned a unique number.
 */ 
typedef int OsReturnCode;

#define OSRC_SUCCESS                 0
#define OSRC_ERROR                  -1
#define OSRC_INVALID_HANDLE         -2
#define OSRC_NOTSUPPORTED           -3
#define OSRC_NOACCESS               -4
#define OSRC_PROTOCOL_FAILED        -6
#define OSRC_BAD_PARM               -7

/* End of OsReturnCode */



/*---------------------------------------------------------------------------
 * IoctlFunction type
 *
 *     Symbolics for your OS could be used instead of, or in addition to,
 *     the ones below.
 */
typedef int IoctlFunction;

#define IOC_NREAD              0    /* Returns number of bytes in read buffer */
#define IOC_NWRITE             1    /* Returns number of bytes in write buffer */
#define IOC_FLUSH              2    /* Discards all bytes in read and write buffers */
#define IOC_RFLUSH             3    /* Discards all bytes in read buffer */
#define IOC_WFLUSH             4    /* Discards all bytes in write buffer */
#define IOC_CANCEL             5    /* Cancels a blocked read or write */
#define IOC_RBUFSET            6    /* Sets size of receive buffer */
#define IOC_WBUFSET            7    /* Sets size of transmit buffer */
#define IOC_SETBAUD            8    /* Set the baud rate */
#define IOC_GETBAUD            9    /* Get the baud rate */
#define IOC_GET_DATAFORMAT     10   /* Get stopbits, charsize, parity, flow control */
#define IOC_SET_DATAFORMAT     11   /* Set stopbits, charsize, parity, flow control */
#define IOC_STOPBITS           12   /* Set number of stop bits */
#define IOC_CHARSIZE           13   /* Set character size */
#define IOC_PARITY             14   /* Set parity */
#define IOC_GET_MODEM_STATUS   15   /* Get modem status */
#define IOC_SET_MODEM_CONTROL  16   /* Set modem control */
#define IOC_GET_LINE_STATUS    17   /* Get line status */
#define IOC_GET_FLOWCONTROL    18   /* Get flow control settings */
#define IOC_SET_FLOWCONTROL    19   /* Set flow control settings */
#define IOC_SET_DEVICEID       20   /* Set Remote DeviceId for client port */

/* End of IoctlFunction */



/*---------------------------------------------------------------------------
 * BaudRate type
 *
 *     Symbolics for your OS could be used instead of, or in addition to,
 *     the ones below.
 */
typedef int BaudRate;

#define BPS_2400      0
#define BPS_4800      1
#define BPS_9600      2
#define BPS_19200     3
#define BPS_38400     4
#define BPS_57600     5
#define BPS_115200    6

/* End of BaudRate */



/*---------------------------------------------------------------------------
 * DataFormat type
 *
 *    The following bit definitions are for data format.
 */
typedef int DataFormat;

#define STOP_MASK               0x0003
#define     STOP_BITS_1         0x0000
#define     STOP_BITS_2         0x0001
#define     STOP_BITS_1_5       0x0002
#define CHAR_MASK               0x000c
#define     CHAR_SIZE_5         0x0000
#define     CHAR_SIZE_6         0x0004
#define     CHAR_SIZE_7         0x0008
#define     CHAR_SIZE_8         0x000c
#define PARITY_ENABLE_MASK      0x0010
#define     PAR_NONE            0x0000
#define     PAR_ENABLE          0x0010
#define PARITY_TYPE_MASK        0x0060
#define     PAR_ODD             0x0000
#define     PAR_EVEN            0x0020
#define     PAR_MARK            0x0040
#define     PAR_SPACE           0x0060
#define FLOW_MASK               0x0300
#define     FLOW_NONE           0x0000
#define     FLOW_RTS_CTS        0x0100
#define     FLOW_DTR_DSR        0x0200
#define     FLOW_XON_XOFF       0x0300

/* End of DataFormat */



/*---------------------------------------------------------------------------
 * ModemStatus type
 *
 *     Ioctl 'arg' points to this type when setting/getting modem status.
 */
typedef int ModemStatus;

#define MODEM_STATUS_DSR    0x01
#define MODEM_STATUS_CTS    0x02
#define MODEM_STATUS_RI     0x04
#define MODEM_STATUS_DCD    0x08

/* Group: Bits for modem status (DTE->DCE) */
#define MODEM_STATUS_DTR    MODEM_STATUS_DSR
#define MODEM_STATUS_RTS    MODEM_STATUS_CTS

/* End of ModemStatus */



/*---------------------------------------------------------------------------
 * LineStatus type
 *
 *     Ioctl 'arg' points to this type when setting/getting line status.
 */
typedef int LineStatus;

#define LINE_STATUS_OVR     0x01 
#define LINE_STATUS_PARITY  0x02
#define LINE_STATUS_FRAMING 0x04

/* End of LineStatus type */



/****************************************************************************
 *
 * Data Structures
 *
 ****************************************************************************/

/*---------------------------------------------------------------------------
 * RemoteDevInfo structure
 *
 *     Structure required when opening a client port. Note that 'void *'
 *     is used so that Bluetooth internals don't have to be dragged around.
 */
typedef struct _RemoteDevInfo {
    void      *bdAddr;       /* pointer to BD_ADDR */
    void      *remoteDev;    /* pointer to BtRemoteDevice */
    U8        rChannel;      /* remote server channel */
    RfChannel lChannel;      /* local channel */
} RemoteDevInfo;




/****************************************************************************
 *
 * Function Reference
 *
 ****************************************************************************/

/*---------------------------------------------------------------------------
 * installSppDriver()
 *     Initializes the driver. All this functionality would normally be done
 *     at boot time by system initialization code. 
 */
BOOL installSppDriver(void);
 

/*---------------------------------------------------------------------------
 * removeSppDriver() 
 *      Remove driver and release resources.
 *      Note that this is only needed since we're not really loading as
 *      a driver.
 */
void removeSppDriver(void);

 
/*---------------------------------------------------------------------------
 * Creat() 
 *     Create a file/device. This function is not supported in the sample API.
 *     
 * Parameters:
 *      
 * Returns:
 *     OSRC_NOTSUPPORTED
 */
OsReturnCode Creat(const char *filename, int flag);
 
 
/*---------------------------------------------------------------------------
 * Remove() 
 * Remove a file/device. This function is not supported in the sample API.
 *      
 * Parameters:
 *
 * Returns:
 *          OSRC_NOTSUPPORTED
 */
int Remove(const char *filename);

 
/*---------------------------------------------------------------------------
 * Open() 
 *     Opens a device.
 *
 * Parameters:
 *     filename  - pointer to serial device (e.g., (/dev/rfc0")
 *     flags     - ignored
 *     mode      - not used when opening a local server port; when opening
 *                 local client ports, 'mode' is actually a pointer to 
 *                 a data object of type RemoteDevInfo which describes the
 *                 target device for RFCOMM; the RemoteDevInfo would normally
 *                 be obtained by a "Bluetooth Advisor"-type software entity.
 *
 * Returns:
 *     File descriptor on success or OSRC_ERROR on failure.
 */
int Open(const char *filename, int flags, int mode);

 
/*---------------------------------------------------------------------------
 * Close() 
 *     Close a device.
 *     
 * Parameters:
 *     fd - valid file descriptor returned by Open.
 *
 * Returns:
 *     OSRC_SUCCESS - Device closed successfully.
 *
 *     OSRC_ERROR - Invalid descriptor.
 */
int Close(int fd);

 
/*---------------------------------------------------------------------------
 * Read() 
 * Read data from an open device.
 *     
 * Parameters:
 *     fd        - valid file descriptor returned by Open
 *     buffer    - existing buffer in which to receive data
 *     maxBytes  - maximum number of bytes to place in buffer;
 *
 * Returns:
 *      OS-specific error or number of bytes written on success    
 */
int Read(int fd, char *buffer, int maxBytes);

 
/*---------------------------------------------------------------------------
 * Write() 
 *     Write data to the device. Note that this function actually hands off
 *     data to the driver (serial API) which has its own buffers.
 *     
 * Parameters:
 *     fd        - valid file descriptor returned by Open
 *     buffer    - buffer containing data to be written
 *     nBytes    - number of bytes to write
 *
 * Returns:
 *     Number of bytes written on success or an OS-specific error code
 *     (less than 0) if an error occurs.
 */
int Write(int fd, char *buffer, int nBytes);

 
/*---------------------------------------------------------------------------
 * Ioctl()
 *     Performs various functions on an open file. 
 *      
 * Parameters:
 *     fd       - valid file descriptor returned by Open
 *     function - function to be performed
 *     arg      - pointer to function-specific argument
 *                 (see types BaudRate, ModemStatus, LineStatus & DataFormat)
 *
 * Returns:
 *     OSRC_SUCCESS - An OS-specific error code (less than 0) if an error
 *          occurs.
 */
int Ioctl(int fd, IoctlFunction function, void *arg);


#endif /* __SPPDRV_H */




