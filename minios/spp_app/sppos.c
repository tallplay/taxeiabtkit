/****************************************************************************
 *
 * File:        bpposapi.c
 *     $Workfile:sppos.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:20$
 *
 * Description: This file contains platform specific code for the  
 *              Serial API application. 
 *
 * $Project:XTNDAccess Blue SDK$
 *
 * Copyright 1997-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions,
 * Inc.  This work contains confidential and proprietary information of
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret,
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/
#include "osapi.h"
#include "sppos.h"
#include "spp2os.h"

/* Platform dependent includes */
#include "windows.h"


/****************************************************************************
 *
 * Types and Constants
 *
 ****************************************************************************/

/* Additional ioctl return code */
#define SPP_NOT_SUPPORTED



/****************************************************************************
 *
 * Function Prototypes
 *
 ****************************************************************************/

static BtStatus ioctlPortSettings(SppDev *dev, U16 function, void *arg);



/****************************************************************************
 *
 * RAM Data
 *
 ****************************************************************************/



/****************************************************************************
 *
 * The following functions are APIs that the OS layer (this layer) must
 * provide to the portable SPP.
 *
 ***************************************************************************/

/*---------------------------------------------------------------------------
 */
void SPPOS_LockDev(SppDev *dev)
{
    DWORD status;

    status = WaitForSingleObject(((SPP_DEV_OS *)dev)->mutex, 10000);
}


/*---------------------------------------------------------------------------
 */
void SPPOS_UnlockDev(SppDev *dev)
{
    ReleaseMutex(((SPP_DEV_OS *)dev)->mutex);
}


/*---------------------------------------------------------------------------
 */
BtStatus SPPOS_Wait(SppDev *dev, U16 ms)
{
    Report(("Semtake %d\n", OS_GetSystemTime()));
    
    if (WaitForSingleObject(((SPP_DEV_OS *)dev)->wait, ms) == WAIT_TIMEOUT) {
        Report(("SPPOS: wait timeout\n"));
        return BT_STATUS_FAILED;
    }
    
    return BT_STATUS_SUCCESS;
}


/*---------------------------------------------------------------------------
 */
void SPPOS_Resume(SppDev *dev)
{
    Report(("Semgive %d\n", OS_GetSystemTime()));
    /* Increment the semaphore count by 1. If the count is already at the max,
     * the call returns FALSE and count is not updated.
     */
    ReleaseSemaphore(((SPP_DEV_OS *)dev)->wait, 1, 0);
}


BtStatus SPPOS_Ioctl(SppDev *dev, U16 function, void *arg)
{
    BtStatus rc = BT_STATUS_SUCCESS;

    SPPOS_LockDev(dev);

    switch (function) {
     
    case IOC_NREAD:          /* Returns number of bytes in read buffer */
        *(int *)arg = SPPOS_RxBytes(dev);
        break;

    case IOC_NWRITE:
        /* 
         *  Returns number of bytes in write buffer. Note that we don't
         *  decrement datalen until a packet is actually sent via Bluetooth
         *  so the caller can be certain that all data has left the local
         *  machine.
         */
        *(U16 *)arg = RING_DataLen(((SPP_DEV_OS *)dev)->wRing);
        break;

    case IOC_GETBAUD:
        {
            U8 rfBaud;

            rfBaud = SPP_GetBaud(dev);
            if (rfToOsBaud(rfBaud, arg) == FALSE) {
                rc = BT_STATUS_FAILED;
            }
        }
        break;

    case IOC_GET_DATAFORMAT:
        *(int *)arg = 0;
        rfToOsDataBits(SPP_GetDataBits(dev), arg);
        rfToOsStopBits(SPP_GetStopBits(dev), arg);
        rfToOsFlowControl(SPP_GetFlowControl(dev), arg);
        rfToOsParity(SPP_GetParity(dev), arg);
        if ((rfToOsDataBits(SPP_GetDataBits(dev), arg) == TRUE)
            && (rfToOsStopBits(SPP_GetStopBits(dev), arg) == TRUE)
            && (rfToOsFlowControl(SPP_GetFlowControl(dev), arg) == TRUE)
            && (rfToOsParity(SPP_GetParity(dev), arg) == TRUE)) {
            rc = BT_STATUS_SUCCESS;
        }
        break;

    case IOC_GET_MODEM_STATUS:
        /* arg is a pointer to an S16 in which the result is returned.
         * Convert the RF constant to an OS value.
         */
        Assert(arg);
        if (rfToOsModemStatus(SPP_GetModemStatus(dev), arg, 
                              dev->portSettings.flowControl) == TRUE) {
            rc = BT_STATUS_SUCCESS;
        }
        break;

    case IOC_SET_MODEM_CONTROL:
        {
            RfSignals signals;

            Assert(arg);
            if (osToRfModemStatus(&signals, *(int *)arg) == TRUE) {
                SPP_SetModemControl(dev, signals);
                rc = BT_STATUS_SUCCESS;
            }
        }
        break;

    case IOC_GET_LINE_STATUS:
        if (rfToOsLineStatus(&dev->lineStatus, arg) == TRUE) {
            rc = BT_STATUS_SUCCESS;
        }
        /* Reading line status clears it. */
        dev->lineStatus = 0;
        break;

    case IOC_FLUSH:          /* Discards all bytes in read and write buffers */
        SPPOS_FlushRx(dev);
        SPPOS_FlushTx(dev);
        rc = BT_STATUS_SUCCESS;
        break;

    case IOC_RFLUSH:         /* Discards all bytes in read buffer */
        SPPOS_FlushRx(dev);
        rc = BT_STATUS_SUCCESS;
        break;

    case IOC_WFLUSH:         /* Discards all bytes in write buffer */
        SPPOS_FlushTx(dev);
        rc = BT_STATUS_SUCCESS;
        break;

    default:
        rc = ioctlPortSettings(dev, function, arg);
    }

    SPPOS_UnlockDev(dev);
    return rc;
}


/*---------------------------------------------------------------------------
 *  This function is called by SPP for when the read function is called.
 */
BtStatus SPPOS_ReadBuf(SppDev *dev, char buf[], U16 *len)
{
    BtStatus rc = BT_STATUS_SUCCESS;

    *len = RING_ReadData(((SPP_DEV_OS *)dev)->rRing, buf, *len);
    return rc;
}


/*---------------------------------------------------------------------------
 *  This function is called by SPP when the write function is called.
 *  Puts bytes in the device ring buffer.
 */
BtStatus SPPOS_WriteBuf(SppDev *dev, char buf[], U16 *len)
{
    BtStatus rc = BT_STATUS_SUCCESS;

    *len = RING_WriteData(((SPP_DEV_OS *)dev)->wRing, buf, *len);
    return rc;
}


/*---------------------------------------------------------------------------
 *  This function is called by SPP for when characters have been received
 *  from RFCOMM.
 *
 *  Put bytes in the device ring buffer.
 */
BtStatus SPPOS_DevRx(SppDev *dev, char buf[], U16 *len)
{
    BtStatus rc = BT_STATUS_SUCCESS;

    *len = RING_WriteData(((SPP_DEV_OS *)dev)->rRing, buf, *len);
    return rc;
}


/*---------------------------------------------------------------------------
 * Return a buffer of data to be transmitted via RFCOMM
 * Instead of allocating a buffer and copying data from the ring
 * buffer, return a pointer into the ring buffer. This assumes
 * that SPPOS_DevTx will not be called again without an interceding
 * call to SPPOS_ReturnBuf.
 *
 * Note that buf will be returned to us via SPPOS_ReturnBuf.   
 */
BtStatus SPPOS_DevTx(SppDev *dev, char **buf, U16 *len)
{
    BtStatus rc = BT_STATUS_SUCCESS;

    RING_GetDataPtr(((SPP_DEV_OS *)dev)->wRing, buf, len);
    return rc;
}


/*---------------------------------------------------------------------------
 * Return number of bytes received from RFCOMM that have not been read.
 */
U16 SPPOS_RxBytes(SppDev *dev)
{
    return RING_DataLen(((SPP_DEV_OS *)dev)->rRing);
}


/*---------------------------------------------------------------------------
 *  Report number of free bytes in ring buffer.
 */
U16 SPPOS_RxFree(SppDev *dev)
{
    return RING_FreeSpace(((SPP_DEV_OS *)dev)->rRing);
}


/*---------------------------------------------------------------------------
 */
void SPPOS_FlushRx(SppDev *dev)
{
    RING_BufFlush(((SPP_DEV_OS *)dev)->rRing);
}


/*---------------------------------------------------------------------------
 */
void SPPOS_FlushTx(SppDev *dev)
{
    RING_BufFlush(((SPP_DEV_OS *)dev)->wRing);
}


/*---------------------------------------------------------------------------
 *  Called when the buffer returned by SPPOS_DevTx is relinguished by the
 *  driver.
 */
void SPPOS_ReturnBuf(SppDev *dev, char *buf, U16 len)
{
    RING_BufDelete(((SPP_DEV_OS *)dev)->wRing, len);
}


/*---------------------------------------------------------------------------
 * ioctlPortSettings() 
 *      This is part of ioctl(). The portsettings are handled simarily
 *      so they are segregated here to reduce code.
 *
 * Requires:
 *     
 * Parameters:
 *      dev         pointer to device structure 
 *      function    ioctl function
 *      arg         arg appropriate for 'function'
 *
 * Returns:
 *      BT_STATUS_SUCCESS
 *      BT_STATUS_FAILED
 *      SPPOS_Ioctl is allowed to return other values more meaningful
 */
static BtStatus ioctlPortSettings(SppDev *dev, U16 function, void *arg)
{
    BtStatus status = BT_STATUS_FAILED;
    RfDataFormat  rfDataFormat;
    RfFlowControl rfFlowControl;
    RfBaudRate    rfBaud;

    switch(function) {

    case IOC_SET_DATAFORMAT:
        /* Set the RFCOMM values to the OS value in arg */
        rfDataFormat = SPP_GetDataFormat(dev);
        rfFlowControl = SPP_GetFlowControl(dev);
        if ((osToRfDataBits(&rfDataFormat, *(int *)arg) == TRUE)
            && (osToRfStopBits(&rfDataFormat, *(int *)arg) == TRUE)
            && (osToRfFlowControl(&rfFlowControl, *(int *)arg) == TRUE)
            && (osToRfParity(&rfDataFormat, *(int *)arg) == TRUE)) {
            status = BT_STATUS_SUCCESS;
            SPP_SetDataFormat(dev, rfDataFormat);
            SPP_SetFlowControl(dev, rfFlowControl);
        }

        break;
    
    case IOC_SETBAUD:
        rfBaud = SPP_GetBaud(dev);
        if (osToRfBaud(&rfBaud, *(int *)arg) == TRUE) {
            SPP_SetBaud(dev, rfBaud);
            status = BT_STATUS_SUCCESS;
        }
        break;

    case IOC_SET_FLOWCONTROL:
        rfFlowControl = SPP_GetFlowControl(dev);
        if (osToRfFlowControl(&rfFlowControl, *(int *)arg) == TRUE) {
            SPP_SetFlowControl(dev, rfFlowControl);
            status = BT_STATUS_SUCCESS;
        }
        break;
    
    default:
        break;
    }

    return status;
}
