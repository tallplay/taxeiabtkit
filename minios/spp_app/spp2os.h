/***************************************************************************
 *
 * File:
 *     $Workfile:spp2os.h$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:14$
 *
 * Description:
 *      Header file for sample Port Emulation Entity interface to RFCOMM.
 *      Note that nearly all of this file may not be required if you are
 *      actually implementing a driver under your operating system.
 *
 *
 * Copyright 1999-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#ifndef __SPP2OS_H
#define __SPP2OS_H


#include "bttypes.h"
#include "spp.h"

typedef const struct _RfToOsConversion RfToOsConversion;


/***********************************************************************
 *
 * Function Prototypes
 *
 ***********************************************************************/
 

/*---------------------------------------------------------------------------
 * osToRfBaud() 
 *      Convert OS-specified baud to Bluetooth RFCOMM
 *
 * Requires:
 *     
 * Parameters:
 *      rfBaud  pointer to RFCOMM baud code
 *      osBaud    OS baud rate code
 *
 * Returns:
 *      TRUE    successful; *rfBaud contains the RFCOMM baud code equivalent
 *              to osBaud
 */
BOOL osToRfBaud(U8 *rfBaud, int osBaud);
 

/*---------------------------------------------------------------------------
 * osToRfModemStatus() 
 *      Convert OS-specified baud to Bluetooth RFCOMM
 *
 * Requires:
 *     
 * Parameters:
 *      signals pointer to RFCOMM modem signals
 *      status  OS modem status code
 *
 * Returns:
 *      TRUE    successful; *signals is updated with the RFCOMM-equivalent
 *              to 'status'
 */
BOOL osToRfModemStatus(RfSignals *signals, int status);


/*---------------------------------------------------------------------------
 * osToRfDataBits() 
 *      Convert OS-specified code to Bluetooth RFCOMM
 *
 * Requires:
 *     
 * Parameters:
 *      rfFormat  pointer to RFCOMM dataformat
 *      stopBits  OS stopBits code; non-stopbit related bits are ignored
 *
 * Returns:
 *      TRUE    successful; the appropriate value in the device structure is
 *      updated with Bluetooth values.
 */
BOOL osToRfDataBits(RfDataFormat *rfFormat, int osDataSize);


/*---------------------------------------------------------------------------
 * osToRfStopBits() 
 *      Convert OS-specified code to Bluetooth RFCOMM
 *
 * Requires:
 *     
 * Parameters:
 *      rfFormat  pointer to RFCOMM dataformat
 *      stopBits  OS stopBits code; non-stopbit related bits are ignored
 *
 * Returns:
 *      TRUE    successful; the appropriate value in the device structure is
 *      updated with Bluetooth values.
 */
BOOL osToRfStopBits(RfDataFormat *rfFormat, int stopBits);


/*---------------------------------------------------------------------------
 * osToRfParity() 
 *      Convert OS-specified baud to Bluetooth RFCOMM
 *
 * Requires:
 *     
 * Parameters:
 *      rfFormat  pointer to RFCOMM dataformat
 *      parity    OS parity code; non-parity related bits are ignored
 *
 * Returns:
 *      TRUE    successful; the appropriate value in the device structure is
 *      updated with Bluetooth values.
 */
BOOL osToRfParity(RfDataFormat *rfFormat, int parity);



/*---------------------------------------------------------------------------
 * osToRfFlowControl() 
 *      Convert OS-specified code to Bluetooth RFCOMM
 *
 * Requires:
 *     
 * Parameters:
 *      rfFlow    pointer to RFCOMM flow control
 *      flow      OS FlowControl code; non-flow related bits are ignored
 *
 * Returns:
 *      TRUE    successful; the appropriate value in the device structure is
 *      updated with Bluetooth values.
 */
BOOL osToRfFlowControl(RfFlowControl *RfFlow, int flow);



/*---------------------------------------------------------------------------
 * rfToOsDataBits() 
 *      Convert Bluetooth RFCOMM databits to OS format
 *
 * Requires:
 *     
 * Parameters:
 *       rfFormat       RFCOMM-formatted databits
 *       osSettings     pointer to OS-specific variable to receive
 *                      equivalent OS settings
 * Returns:
 *      TRUE    successful conversion
 *      FALSE   failed (non-extent parameter setting)
 */
BOOL rfToOsDataBits(RfDataFormat rfFormat, int *osSettings);



/*---------------------------------------------------------------------------
 * rfToOsStopBits() 
 *      Convert Bluetooth RFCOMM stopbits to OS format
 *
 * Requires:
 *     
 * Parameters:
 *       rfStopBits     RFCOMM-formatted stopbits
 *       osSettings     pointer to OS-specific variable to receive
 *                      equivalent OS settings
 * Returns:
 *      TRUE    successful conversion
 *      FALSE   failed (non-extent parameter setting)
 */
BOOL rfToOsStopBits(RfDataFormat rfStopBits, int *osSettings);



/*---------------------------------------------------------------------------
 * rfToOsParity() 
 *      Convert Bluetooth RFCOMM parity to OS format
 *     
 * Parameters:
 *       rfParity       RFCOMM-formatted parity
 *       osSettings     pointer to OS-specific variable to receive
 *                      equivalent OS settings
 * Returns:
 *      TRUE    successful conversion
 *      FALSE   failed (non-extent parameter setting)
 */
BOOL rfToOsParity(RfDataFormat rfParity, int *osSettings);
;


/*---------------------------------------------------------------------------
 * rfToOsBaud() 
 *      Convert Bluetooth RFCOMM baud rate to OS format
 *
 * Parameters:
 *       rfBaud         RFCOMM-formatted baud rate
 *       osSettings     pointer to OS-specific variable to receive
 *                      equivalent OS settings
 * Returns:
 *      TRUE    successful conversion
 *      FALSE   failed (non-extent parameter setting)
 */
BOOL rfToOsBaud(RfBaudRate rfBaud, int *osBaud);



/*---------------------------------------------------------------------------
 * rfToOsFlowControl() 
 *      Convert Bluetooth RFCOMM flow control to OS format
 *
 * Parameters:
 *       rfFlow         RFCOMM-formatted flow control
 *       osSettings     pointer to OS-specific variable to receive
 *                      equivalent OS settings
 * Returns:
 *      TRUE    successful conversion
 *      FALSE   failed (non-extent parameter setting)
 */
BOOL rfToOsFlowControl(RfDataFormat rfFlow, int *osFlow);



/*---------------------------------------------------------------------------
 * rfToOsModemStatus() 
 *      Convert Bluetooth RFCOMM portsettings to OS data format
 *
 * Requires:
 *     
 * Parameters:
 *
 * Returns:
 */
BOOL rfToOsModemStatus(RfSignals rfSignals, int *osStatus, BOOL flowNone);



/*---------------------------------------------------------------------------
 * rfToOsLineStatus() 
 *      Convert Bluetooth RFCOMM portsettings to OS data format
 *
 * Requires:
 *     
 * Parameters:
 *
 * Returns:
 */
BOOL rfToOsLineStatus(RfLineStatus *rfStatus, int *osStatus);


/*---------------------------------------------------------------------------
 * btToOsRc() 
 *      Convert Bluetooth return code to OS return code.
 *
 * Requires:
 *     
 * Parameters:
 *
 * Returns:
 */
int btToOsRc(BtStatus bts);
  

#endif /* __SPP2OS_H */

