# Microsoft Developer Studio Project File - Name="Serial API" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Console Application" 0x0103

CFG=Serial API - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "Serial API.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "Serial API.mak" CFG="Serial API - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "Serial API - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "Serial API - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "Serial API - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /W3 /GX /O2 /I "." /I "..\.." /I "..\..\hcitrans\abcsp\include" /I "..\..\hcitrans\abcsp\config" /I "..\..\inc" /I "..\..\minios\inc" /D "NDEBUG" /D "WIN32" /D "_CONSOLE" /D "_MBCS" /D "USB_TRANS" /YX /FD /c
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo /o"Release/serialApp.bsc"
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib advapi32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib advapi32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 wsock32.lib comctl32.lib kernel32.lib advapi32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib winmm.lib /nologo /subsystem:console /machine:I386 /out:"Release\serial.exe"

!ELSEIF  "$(CFG)" == "Serial API - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /GZ /c
# ADD CPP /nologo /W3 /Gm /GX /ZI /Od /I "." /I "..\.." /I "..\..\hcitrans\abcsp\include" /I "..\..\hcitrans\abcsp\config" /I "..\..\inc" /I "..\..\minios\inc" /I "..\..\hcitrans\module_init\TI" /D "_DEBUG" /D "WIN32" /D "_CONSOLE" /D "_MBCS" /D "USB_TRANS" /FR /YX /FD /GZ /c
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo /o"Debug\serialApp.bsc"
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib advapi32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib advapi32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept
# ADD LINK32 wsock32.lib comctl32.lib kernel32.lib advapi32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib winmm.lib /nologo /subsystem:console /debug /machine:I386 /out:"Debug\serial.exe" /pdbtype:sept

!ENDIF 

# Begin Target

# Name "Serial API - Win32 Release"
# Name "Serial API - Win32 Debug"
# Begin Group "Base SDK"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\stack\btalloc.c
# End Source File
# Begin Source File

SOURCE=..\..\stack\btrom.c
# End Source File
# Begin Source File

SOURCE=..\..\stack\btutils.c
# End Source File
# Begin Source File

SOURCE=..\..\stack\debug.c
# End Source File
# Begin Source File

SOURCE=..\..\stack\eventmgr.c
# End Source File
# Begin Source File

SOURCE=..\..\stack\hci\hci.c
# End Source File
# Begin Source File

SOURCE=..\..\stack\hci\hci_evnt.c
# End Source File
# Begin Source File

SOURCE=..\..\stack\hci\hci_proc.c
# End Source File
# Begin Source File

SOURCE=..\..\stack\hci\hci_util.c
# End Source File
# Begin Source File

SOURCE=..\..\stack\l2cap\l2cap.c
# End Source File
# Begin Source File

SOURCE=..\..\stack\l2cap\l2cap_if.c
# End Source File
# Begin Source File

SOURCE=..\..\stack\l2cap\l2cap_sm.c
# End Source File
# Begin Source File

SOURCE=..\..\stack\l2cap\l2cap_utl.c
# End Source File
# Begin Source File

SOURCE=..\..\stack\me\me.c
# End Source File
# Begin Source File

SOURCE=..\..\stack\me\meacc.c
# End Source File
# Begin Source File

SOURCE=..\..\stack\me\mecon.c
# End Source File
# Begin Source File

SOURCE=..\..\stack\me\medev.c
# End Source File
# Begin Source File

SOURCE=..\..\stack\me\meif.c
# End Source File
# Begin Source File

SOURCE=..\..\stack\me\mesec.c
# End Source File
# Begin Source File

SOURCE=..\..\apps\common\msg_constants.c
# End Source File
# Begin Source File

SOURCE=..\..\stack\hci\prescan.c
# End Source File
# Begin Source File

SOURCE=..\..\stack\radiomgr.c
# End Source File
# Begin Source File

SOURCE=..\..\stack\rfcomm\rfc_ctl.c
# End Source File
# Begin Source File

SOURCE=..\..\stack\rfcomm\rfc_fcs.c
# End Source File
# Begin Source File

SOURCE=..\..\stack\rfcomm\rfc_sm.c
# End Source File
# Begin Source File

SOURCE=..\..\stack\rfcomm\rfc_util.c
# End Source File
# Begin Source File

SOURCE=..\..\stack\rfcomm\rfcomm.c
# End Source File
# Begin Source File

SOURCE=..\..\apps\common\ring.c
# End Source File
# Begin Source File

SOURCE=..\..\stack\rxbuff.c
# End Source File
# Begin Source File

SOURCE=..\..\stack\sdp\sdp.c
# End Source File
# Begin Source File

SOURCE=..\..\stack\sdp\sdpclient.c
# End Source File
# Begin Source File

SOURCE=..\..\stack\sdp\sdpserv.c
# End Source File
# Begin Source File

SOURCE=..\..\stack\sdp\sdputil.c
# End Source File
# Begin Source File

SOURCE=..\..\apps\common\sniffer.c
# End Source File
# Begin Source File

SOURCE=..\..\stack\utils.c
# End Source File
# End Group
# Begin Group "Windows Integration"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\common\bluemgr_cmd.c
# End Source File
# Begin Source File

SOURCE=..\cmdmain.c
# End Source File
# Begin Source File

SOURCE=..\ddb4w.c
# End Source File
# Begin Source File

SOURCE=..\osapi.c
# End Source File
# Begin Source File

SOURCE=..\common\sniffer_ui.c
# End Source File
# Begin Source File

SOURCE=..\..\apps\common\textmenu.c
# End Source File
# End Group
# Begin Group "SPP Profile"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\profiles\spp\spp.c
# End Source File
# Begin Source File

SOURCE=..\..\inc\spp.h
# End Source File
# End Group
# Begin Group "HCI Transports"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\hcitrans\abcsp\src\abcsp_crc.c
# End Source File
# Begin Source File

SOURCE=..\..\hcitrans\abcsp\src\abcsp_free.c
# End Source File
# Begin Source File

SOURCE=..\..\hcitrans\abcsp\src\abcsp_init.c
# End Source File
# Begin Source File

SOURCE=..\..\hcitrans\abcsp\src\abcsp_malloc.c
# End Source File
# Begin Source File

SOURCE=..\..\hcitrans\abcsp\src\abcsp_zmalloc.c
# End Source File
# Begin Source File

SOURCE=..\..\hcitrans\abcsp\abcsphci.c
# End Source File
# Begin Source File

SOURCE=..\..\hcitrans\abcsp\src\le.c
# End Source File
# Begin Source File

SOURCE=..\..\hcitrans\abcsp\src\rxbcsp.c
# End Source File
# Begin Source File

SOURCE=..\..\hcitrans\abcsp\src\rxmsg.c
# End Source File
# Begin Source File

SOURCE=..\..\hcitrans\abcsp\src\rxmsgdemux.c
# End Source File
# Begin Source File

SOURCE=..\..\hcitrans\abcsp\src\rxslip.c
# End Source File
# Begin Source File

SOURCE=..\..\hcitrans\abcsp\src\txmsg.c
# End Source File
# Begin Source File

SOURCE=..\..\hcitrans\abcsp\src\txrx.c
# End Source File
# Begin Source File

SOURCE=..\..\hcitrans\abcsp\src\txslip.c
# End Source File
# Begin Source File

SOURCE=..\..\hcitrans\uart\uarttran.c
# End Source File
# Begin Source File

SOURCE=..\..\hcitrans\usb\usbtrans.c
# End Source File
# Begin Source File

SOURCE=..\..\hcitrans\abcsp\xa_abcsp.c
# End Source File
# End Group
# Begin Group "Radio Integration"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\hcitrans\inet\hclinkmgr.c

!IF  "$(CFG)" == "Serial API - Win32 Release"

# ADD CPP /I "..\..\hcitrans\modinit"

!ELSEIF  "$(CFG)" == "Serial API - Win32 Debug"

# ADD CPP /I "..\..\hcitrans\modinit"
# SUBTRACT CPP /I "..\..\hcitrans\module_init\TI"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\hcitrans\inet\hostctlr.c

!IF  "$(CFG)" == "Serial API - Win32 Release"

# ADD CPP /I "..\..\hcitrans\modinit"

!ELSEIF  "$(CFG)" == "Serial API - Win32 Debug"

# ADD CPP /I "..\..\hcitrans\modinit"
# SUBTRACT CPP /I "..\..\hcitrans\module_init\TI"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\hcitrans\inet\inettran.c

!IF  "$(CFG)" == "Serial API - Win32 Release"

# ADD CPP /I "..\..\hcitrans\modinit"

!ELSEIF  "$(CFG)" == "Serial API - Win32 Debug"

# ADD CPP /I "..\..\hcitrans\modinit"
# SUBTRACT CPP /I "..\..\hcitrans\module_init\TI"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\hcitrans\modinit\radiomod.c

!IF  "$(CFG)" == "Serial API - Win32 Release"

# ADD CPP /I "..\..\hcitrans\modinit"

!ELSEIF  "$(CFG)" == "Serial API - Win32 Debug"

# ADD CPP /I "..\..\hcitrans\modinit"
# SUBTRACT CPP /I "..\..\hcitrans\module_init\TI"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\hcitrans\modinit\TI\tiinit.c

!IF  "$(CFG)" == "Serial API - Win32 Release"

# ADD CPP /I "..\..\hcitrans\modinit"

!ELSEIF  "$(CFG)" == "Serial API - Win32 Debug"

# ADD CPP /I "..\..\hcitrans\modinit"
# SUBTRACT CPP /I "..\..\hcitrans\module_init\TI"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\hcitrans\transprt.c

!IF  "$(CFG)" == "Serial API - Win32 Release"

# ADD CPP /I "..\..\hcitrans\modinit"

!ELSEIF  "$(CFG)" == "Serial API - Win32 Debug"

# ADD CPP /I "..\..\hcitrans\modinit"
# SUBTRACT CPP /I "..\..\hcitrans\module_init\TI"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\hcitrans\common\uart.c

!IF  "$(CFG)" == "Serial API - Win32 Release"

# ADD CPP /I "..\..\hcitrans\modinit"

!ELSEIF  "$(CFG)" == "Serial API - Win32 Debug"

# ADD CPP /I "..\..\hcitrans\modinit"
# SUBTRACT CPP /I "..\..\hcitrans\module_init\TI"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\hcitrans\usb\usb.c

!IF  "$(CFG)" == "Serial API - Win32 Release"

# ADD CPP /I "..\..\hcitrans\modinit"

!ELSEIF  "$(CFG)" == "Serial API - Win32 Debug"

# ADD CPP /I "..\..\hcitrans\modinit"
# SUBTRACT CPP /I "..\..\hcitrans\module_init\TI"

!ENDIF 

# End Source File
# End Group
# Begin Source File

SOURCE=.\overide.h
# End Source File
# Begin Source File

SOURCE=.\spp2os.c
# End Source File
# Begin Source File

SOURCE=.\spp2os.h
# End Source File
# Begin Source File

SOURCE=.\sppapp.c
# End Source File
# Begin Source File

SOURCE=.\sppdrv.c
# End Source File
# Begin Source File

SOURCE=.\sppdrv.h
# End Source File
# Begin Source File

SOURCE=.\sppos.c
# End Source File
# End Target
# End Project
