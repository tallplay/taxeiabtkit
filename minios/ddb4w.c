#include "bttypes.h"
#include "utils.h"
#include "ddb.h"
#include "ddan.h"

#include "windows.h"
#include "stdlib.h"
#include "stdio.h"
#include "stdarg.h"

/****************************************************************************
 *
 * Constants
 *
 ****************************************************************************/

#define DDB_MAX_ENTRIES  40
#define DDB_NOT_FOUND    0xffff

/****************************************************************************
 *
 * Data
 *
 ****************************************************************************/

/* The device database is kept in memory. */
static I16            numDdbRecs = 0;
static BtDeviceRecord devDb[DDB_MAX_ENTRIES];
static char           dbName[BDADDR_NTOA_SIZE + 22] = {0};

/****************************************************************************
 *
 * Functions
 *
 ****************************************************************************/

static I16  DdbFindRecord(const BD_ADDR *bdAddr);
static void DdbCreateName(const BD_ADDR *bdAddr);


/*---------------------------------------------------------------------------
 *            DDB_Open()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Open the device database. 
 *
 * Return:    BT_STATUS_SUCCESS if success otherwise BT_STATUS_FAILED.
 */
BtStatus DDB_Open(const BD_ADDR *bdAddr)
{
    BtStatus status;
    FILE     *fp = NULL;
    I16      count;

    DDAN_Open(bdAddr); //tallplay add
    
    /* See if the device database file exists. If it does not exist then
     * create a new one.
     */
    status = BT_STATUS_FAILED;
    numDdbRecs = 0;

    /* Create the ddb file based on the radio's BD_ADDR */
    DdbCreateName(bdAddr);
    fp = fopen(dbName, "rb");
    if (fp == NULL) {
        /* The file does not exist so we are done */
        Report(("DeviceDB: Device Database file does not exist\n"));
        status = BT_STATUS_SUCCESS;
        goto done;
    } else {
        Report(("DeviceDB: Device database file opened\n"));
    }

    /* Read in the contents of the database file. The first item in the 
     * database file is the number of items 
     */
    fread(&numDdbRecs, sizeof(numDdbRecs), 1, fp);
    if (ferror(fp) || (numDdbRecs > DDB_MAX_ENTRIES)) {
        Report(("DeviceDB: Error reading the number of items\n"));
        /* Reset the number of records in the database, since 
         * reading the number of items failed. 
         */
        numDdbRecs = 0;
        goto close;
    }
    Report(("DeviceDB: Database contains %d records\n", numDdbRecs));

    /* Read the elements */
    for (count = 0; count < numDdbRecs; count++) {
        fread(&(devDb[count]), sizeof(BtDeviceRecord), 1, fp);
        if (ferror(fp)) {
            Report(("DeviceDB: Error reading item %d\n",count));
            /* Set the number of records in the database to
             * the position that just failed.  Our next additional
             * record will start here.
             */
            numDdbRecs = count;
            goto close;
        }
    }
    status = BT_STATUS_SUCCESS;
close:
    fclose(fp);
done:
    return status;
}

/*---------------------------------------------------------------------------
 *            DDB_Close()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Close the device database. 
 *
 * Return:    BT_STATUS_SUCCESS if success otherwise BT_STATUS_FAILED.
 */
BtStatus DDB_Close(void)
{
    BtStatus status;

    DDAN_Close();
    status = BT_STATUS_FAILED;

    if (DDB_Flush() == BT_STATUS_SUCCESS)
    {
        status = BT_STATUS_SUCCESS;
    }

    dbName[0] = 0;
    numDdbRecs = 0;
    return status;
}


/*---------------------------------------------------------------------------
 *            DDB_Flush()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Force the database to be written to disk. 
 *
 * Return:    BT_STATUS_SUCCESS if success otherwise BT_STATUS_FAILED.
 */
BtStatus DDB_Flush(void)
{
    FILE     *fp = NULL;
    BtStatus status;
    I16      count;

    status = BT_STATUS_FAILED;

    /* Create the database file and copy in the entries if any exist */
    if (dbName[0] == 0) {
        goto done;
    }
    Assert(numDdbRecs <= DDB_MAX_ENTRIES);

    fp = fopen(dbName,"wb");
    Report(("DeviceDB: Creating Device database file %s.\n", dbName));
    if (fp == NULL) {
        Report(("Error creating %s.", dbName));
        goto done;
    }

    /* Write out the number of items */
    fwrite(&numDdbRecs, sizeof(numDdbRecs), 1, fp);
    if (ferror(fp)) {
        Report(("DeviceDB: Error writing numDdbRecs to database file\n"));
        goto close;
    }

    /* Write the entries */
    for (count = 0; count < numDdbRecs; count++) {
        fwrite(&(devDb[count]), sizeof(BtDeviceRecord), 1, fp);
        if (ferror(fp)) {
            Report(("DeviceDB: Error writing entry %d\n", count));
            goto close;
        }
    }
    Report(("DeviceDB: Wrote %d items to database\n", count));
    status = BT_STATUS_SUCCESS;
close:
    fclose(fp);
done:
    return status;
}


/*---------------------------------------------------------------------------
 *            DDB_AddRecord()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Add a record to the database. 
 *
 * Return:    BT_STATUS_SUCCESS if success otherwise BT_STATUS_FAILED.
 */
BtStatus DDB_AddRecord(const BtDeviceRecord* record)
{
    I16 count;

    count = DdbFindRecord(&(record->bdAddr));
    if (count == DDB_NOT_FOUND) {
        /* A record with this bdAddr does not exist so add it */
        count = numDdbRecs;
        numDdbRecs++;
    }
    devDb[count] = *record;
    if (DDB_Flush() != BT_STATUS_SUCCESS) {
        Report(("DeviceDB: Error saving record.", count));
    }

    return BT_STATUS_SUCCESS;
}



/*---------------------------------------------------------------------------
 *            DDB_FindRecord()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Find the record that has the given BD_ADDR. 
 *
 * Return:    BT_STATUS_SUCCESS if success otherwise BT_STATUS_FAILED.
 */
BtStatus DDB_FindRecord(const BD_ADDR *bdAddr, BtDeviceRecord* record)
{
    I16 count;

    count = DdbFindRecord(bdAddr);

    if (count != DDB_NOT_FOUND) {
        *record = devDb[count];
        return BT_STATUS_SUCCESS;
    }
    return BT_STATUS_FAILED;
}

/*---------------------------------------------------------------------------
 *            DDB_DeleteRecord()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Delete a record from the database. 
 *
 * Return:    BT_STATUS_SUCCESS if success otherwise BT_STATUS_FAILED.
 */
BtStatus DDB_DeleteRecord(const BD_ADDR *bdAddr)
{
    I16 count;

    count = DdbFindRecord(bdAddr);
    if (count != DDB_NOT_FOUND) {
        /* If it is the last record then all we need to do is reduce
         * numDdbRecs by 1. Otherwise we need to shift the array
         */
        numDdbRecs--;
        if (count < numDdbRecs) {
            /* We need to shift the array */
            OS_MemCopy((U8*)(&devDb[count]), (U8*)(&devDb[count+1]),
                       (U16)((numDdbRecs - count) * sizeof(BtDeviceRecord)));
        }
        return BT_STATUS_SUCCESS;
    }
    return BT_STATUS_FAILED;
}


/*---------------------------------------------------------------------------
 *            DDB_EnumDeviceRecords()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Enumerate the records in the device database. 
 *
 * Return:    status of the operation.
 */
BtStatus DDB_EnumDeviceRecords(I16 i, BtDeviceRecord* record)
{
    if (i >= numDdbRecs) {
        return BT_STATUS_FAILED;
    } else {
        *record = devDb[i];
    }
    return BT_STATUS_SUCCESS;
}

/*---------------------------------------------------------------------------
 *            DdbFindRecord()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Find the record that has the given BD_ADDR. 
 *
 * Return:    Index of BtDeviceRecord if found otherwise DDB_NOT_FOUND.
 */
static I16 DdbFindRecord(const BD_ADDR *bdAddr)
{
    I16 count;

    for (count = 0; count < numDdbRecs; count++) {
        if (OS_MemCmp(bdAddr->addr, 6, devDb[count].bdAddr.addr, 6)) {
            /* The record is found so return it */
            return count;
        }
    }
    return DDB_NOT_FOUND;
}

/*---------------------------------------------------------------------------
 *            DdbCreateName()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Creates a database filename based on the radio's bluetooth
 *            device address.
 */
#define DDBNAME "\\DDB."

static void DdbCreateName(const BD_ADDR *bdAddr)
{
    char    *bp;
    U8       u, l, i;

    strcpy(dbName, DDBNAME);
    bp = dbName + sizeof(DDBNAME)-1;

    for (i = 6; i > 0; i--) {
        u = (U8)(bdAddr->addr[i-1]/16);
        l = (U8)(bdAddr->addr[i-1]%16);
        
        if (u < 10)
            *bp++ = (U8)('0' + u);
        else *bp++ = (U8)('A' + u - 10);
        
        if (l < 10)
            *bp++ = (U8)('0' + l);
        else *bp++ = (U8)('A' + l - 10);
        *bp++ = '.';
    }
    *bp = 0;

    strcat(dbName, "ddb");
}

