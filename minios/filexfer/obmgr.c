/****************************************************************************
 *
 * File:        obmgr.c
 *
 * Description: This file contains the code for the Object Store Manager
 *              used in the FileXfer application. 
 *
 * Created:     May 9, 1997
 *
 * $Project:XTNDAccess IrDA SDK$
 *
 * Copyright 1997-2005 Extended Systems, Inc.  ALL RIGHTS RESERVED.
 *
 * Unpublished Confidential Information of Extended Systems, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of Extended Systems, Inc.
 * 
 * Use of this work is governed by a license granted by Extended Systems,
 * Inc.  This work contains confidential and proprietary information of
 * Extended Systems, Inc. which is protected by copyright, trade secret,
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "obmgr.h"
#include "utils.h"

#include <string.h>

extern void App_Progress(U32 barId, U32 currPos, U32 maxPos);

/****************************************************************************
 *
 * Local ROM data
 *
 ****************************************************************************/
static const char *defaultVCardName = "mycard.vcf";

static const ObStore ObmTypeTab[] = {
            { "x-obex/folder-listing", OBMT_BROWSER },
            { "text/x-vCard", OBMT_VSTORE },
            { "text/x-vMsg", OBMT_VSTORE },
            { 0, OBMT_GENERIC } };

/****************************************************************************
 *
 * Local RAM data
 *
 ****************************************************************************/
static ObStoreMgr ObmEntries[OBM_NUM_ENTRIES];

/*---------------------------------------------------------------------------
 *            OBM_Init()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initialize the object store.
 *
 * Return:    TRUE - object store initialized.
 *            FALSE - could not create inbox.
 *
 */
BOOL OBM_Init()
{
    if (FSTORE_Init() == FALSE)
        return FALSE;

    if (PATHMGR_Init() == FALSE) {
        return FALSE;
    }
    return TRUE;
}

/*---------------------------------------------------------------------------
 *            OBM_New()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Obtain an object manager item.
 *
 * Return:    Handle of object manager item or 0 if no more handles.
 *
 */
ObStoreMgr *OBM_New(void)
{
    I8  i;

    for (i = 0; i < OBM_NUM_ENTRIES; i++) {
        if ((ObmEntries[i].flags & OBMF_INUSE) == 0) {
            /* Found available entry */
            OS_MemSet((U8 *)(ObmEntries+i), 0, sizeof(ObStoreMgr));
            ObmEntries[i].flags = OBMF_INUSE;
            return ObmEntries+i;
        }
    }

    Report(("OBM_New: Out of entries.\n"));
    return 0;
}

#define OBM_Free(_OBM)  (Assert((_OBM)->flags & OBMF_INUSE),(_OBM)->flags = 0)

/*---------------------------------------------------------------------------
 *            OBM_AppendNameUni()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Append the unicode string to the name. A state machine is used
 *            so the unicode string does not have to be an even number of
 *            bytes.
 *
 * Return:    OBRC_SUCCESS      - Appended Name successfully.
 *            OBRC_UNAUTHORIZED - Unicode Name did not fit in buffer.
 *
 */
ObexRespCode OBM_AppendNameUni(ObStoreMgr *Obm, const U8 *Name, U16 Len)
{
    U16 finalLen;
    U16 i;

    Assert( Obm );

    if (Len == 0) {
        return OBRC_SUCCESS;
    }
    
    /* Check if the appended name would overflow our buffer space. */
    if ((Obm->nameLen + (Len/2)) > (FILENAME_MAX - 1)) {
        return OBRC_UNAUTHORIZED;
    }

    /* Append the Unicode string to the existing name. We need to
     * remove the first byte of each byte pair.
     */
    finalLen = min((FILENAME_MAX - 1) - Obm->nameLen, Len);
    for (i = Obm->nameLen; finalLen > 0; finalLen--) {
        if (Obm->flags & OBMF_UNICODE_ODD) {
            /* This is the byte to store */
            Obm->name[i] = *Name;
            i++;
            Obm->flags &= ~OBMF_UNICODE_ODD;
        } else {
            /* Skip the 0 */
            Obm->flags |= OBMF_UNICODE_ODD;
        }
        Name++;
    }

    if (i > 0) {
        /* Point i to the last character written */
        i--;

        /* Put a 0 at the end of the name if one does not exists */
        if (Obm->name[i] != 0) {
            i++;
            Obm->name[i] = 0;
        }
        /* Update nameLen. It does not include the 0 */
        Obm->nameLen = i;
    }    
    
    return OBRC_SUCCESS;
}

/*---------------------------------------------------------------------------
 *            OBM_AppendType()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Append the ASCII string to the object type field.
 *
 * Return:    void
 *
 */
BOOL OBM_AppendType(ObStoreMgr *Obm, const char *Type, U16 Len)
{
    Assert( Obm );

    if ((Len + Obm->typeLen) > (OBM_MAX_TYPE_LEN - 1)) {
        return FALSE;
    }

    if (Len > 0) {
        /* Append the ASCII string to the existing ASCII type name. */
        OS_MemCopy((U8 *)(Obm->type + Obm->typeLen), Type, Len);
        Obm->typeLen += Len;

        /* Put a null at the end of the type if one does not exists */
        if (Obm->type[Obm->typeLen] != 0) {
            Obm->type[Obm->typeLen] = 0;
            Obm->typeLen++;
        }
    }
    return TRUE;
}

/*---------------------------------------------------------------------------
 *            OBM_AsciiToUnicode()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Convert an ASCII input string to UNICODE.
 *            The copied name is null terminated.
 *
 * Return:    Number of bytes copied including the null terminator.
 *
 */
U16 OBM_AsciiToUnicode(U8 *Unicode, const char *Ascii)
{
    U16     len = 0;

    Assert(Ascii);

    if (Unicode == 0) {
        /* Just calculate the required UNICODE buffer length */
        while (*Ascii++)
            len += 2;

        return len + 2;   /* Add 2 for null-terminator */
    }

    while (*Ascii) {
        *Unicode++ = 0;
        *Unicode++ = *Ascii++;
        len += 2;
    }

    *Unicode++ = 0; /* Null terminator */
    *Unicode++ = 0;
    len += 2;

    return len;
}

/*---------------------------------------------------------------------------
 *            OBM_Open()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Open and object store item for reading.
 *
 * Return:    OBRC_SUCCESS - Open is successful. 
 *            OBRC_NOT_FOUND - Failed to open file.
 */
ObexRespCode OBM_Open(ObStoreMgr *Obm)
{
    const ObStore  *type;

    if (Obm->store == OBMT_UNSPECIFIED) {
        Obm->store = OBMT_GENERIC;

        if (Obm->typeLen) {
            /* Dispatch to the proper object store */
            type = ObmTypeTab;
            while (type->name) {
                if (_stricmp(Obm->type, type->name) == 0) {
                    Obm->store = type->obmt;
                    break;
                }
                type++;
            }
        }
    }
        
    switch (Obm->store) {
    case OBMT_VSTORE:
        /*
         * On this platform, the VSTORE just maps to the generic filesystem. 
         * However, we need to handle any VSTORE specific behaviors first.
         */
        if (Obm->nameLen == 0) {
            strcpy(Obm->name, defaultVCardName);
        }
        /* Drop into generic store */

    case OBMT_GENERIC:
        return FSTORE_Open(&Obm->handle.file, Obm->name);

    case OBMT_BROWSER:
        return BROWSER_Open(Obm->name);
    }

    return OBRC_NOT_FOUND;
}

/*---------------------------------------------------------------------------
 *            OBM_Create()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Open an object store item for writing.
 *
 * Return:    Success or failure of create operation.
 *
 */
ObexRespCode OBM_Create(ObStoreMgr *obm)
{
    const ObStore  *type;

    if (obm->store == OBMT_UNSPECIFIED) {
        obm->store = OBMT_GENERIC;

        if (obm->typeLen) {
            /* Dispatch to the proper object store */
            type = ObmTypeTab;
            while (type->name) {
                if (_stricmp(obm->type, type->name) == 0) {
                    obm->store = type->obmt;
                    break;
                }
                type++;
            }
        }
    }
        
    switch (obm->store) {
    case OBMT_VSTORE:
        /*
         * On this platform, the VSTORE just maps to the generic filesystem. 
         */
        /* Drop into generic store */

    case OBMT_GENERIC:
        return FSTORE_Create(&obm->handle.file, obm->name);

    case OBMT_BROWSER:
        return OBRC_FORBIDDEN;
    }

    return OBRC_NOT_FOUND;
}

/*---------------------------------------------------------------------------
 *            OBM_Close()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Close the object store item and return the handle to the pool.
 *
 * Return:    Success or failure of create operation.
 *
 */
ObexRespCode OBM_Close(ObStoreMgr **ObmPtr)
{
    ObexRespCode    rcode = OBRC_SUCCESS;

    switch ((*ObmPtr)->store) {
    case OBMT_VSTORE:
        /*
         * On this platform, the VSTORE just maps to the generic filesystem. 
         */
        /* Drop into generic store */

    case OBMT_GENERIC:
        if ((*ObmPtr)->handle.file)
            rcode = FSTORE_Close(&(*ObmPtr)->handle.file);
        break;
    }

    OBM_Free(*ObmPtr);
    *ObmPtr = 0;

    return rcode;
}

/*---------------------------------------------------------------------------
 *            OBM_Delete()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Delete the given object store item. The Object store
 *            handle is then returned to the pool.
 *
 * Return:    OBRC_SUCCESS -   File was deleted.
 *            OBRC_NOT_FOUND - File was not deleted.
 *
 */
ObexRespCode OBM_Delete(ObStoreMgr **obmPtr)
{
    ObexRespCode    rcode = OBRC_NOT_FOUND;
    const ObStore  *type;

    Assert (obmPtr && *obmPtr);

    if ((*obmPtr)->store == OBMT_UNSPECIFIED) {
        (*obmPtr)->store = OBMT_GENERIC;

        if ((*obmPtr)->typeLen) {
            /* Dispatch to the proper object store */
            type = ObmTypeTab;
            while (type->name) {
                if (_stricmp((*obmPtr)->type, type->name) == 0) {
                    (*obmPtr)->store = type->obmt;
                    break;
                }
                type++;
            }
        }
    }

    switch ((*obmPtr)->store) {
    case OBMT_VSTORE:
        /* Drop into generic store */
    case OBMT_GENERIC:
        rcode = FSTORE_Delete(&(*obmPtr)->handle.file, (*obmPtr)->name);
        break;
    }
    
    OBM_Free(*obmPtr);
    *obmPtr = 0;

    return rcode;
}

/*---------------------------------------------------------------------------
 *            OBM_Read()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Read from the given object store item.
 *
 * Return:    OBRC_SUCCESS -  Read was successful.
 *            OBRC <other> -  Read failed.
 *
 */
ObexRespCode OBM_Read(ObStoreMgr *Obm, U8 *Buff, U16 Len)
{
    ObexRespCode    rcode = OBRC_FORBIDDEN;

    Assert(Obm->store != OBMT_UNSPECIFIED);

    switch (Obm->store) {
    case OBMT_VSTORE:
        /* Drop into generic store */
    case OBMT_GENERIC:
        rcode = FSTORE_Read(Obm->handle.file, Buff, Len);
        break;
    }
    
    if (Obm->amount == 0)
        App_Progress(Obm->uiContext, Obm->amount, Obm->objectLen);

    Obm->amount += Len;

    App_Progress(Obm->uiContext, Obm->amount, Obm->objectLen);
    return rcode;
}

#if OBEX_DYNAMIC_OBJECT_SUPPORT == XA_ENABLED
/*---------------------------------------------------------------------------
 *            OBM_ReadFlex()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Read from the given object store item.
 *
 * Return:    OBRC_SUCCESS -  Read was successful.
 *            OBRC <other> -  Read failed.
 *
 */
ObexRespCode OBM_ReadFlex(ObStoreMgr *Obm, U8 *Buff, U16 *Len, BOOL *More)
{
    ObexRespCode    rcode = OBRC_FORBIDDEN;

    Assert(Obm->store != OBMT_UNSPECIFIED);

    switch (Obm->store) {
    case OBMT_BROWSER:
        rcode = BROWSER_ReadFlex(Buff, Len, More);
        break;
    }

    if (Obm->amount == 0)
        App_Progress(Obm->uiContext, Obm->amount, Obm->objectLen);

    Obm->amount += *Len;

    App_Progress(Obm->uiContext, Obm->amount, Obm->objectLen);
    return rcode;
}
#endif

/*---------------------------------------------------------------------------
 *            OBM_Write()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Write to the given object manager item.
 *
 * Return:    OBRC_SUCCESS -  Write was successful.
 *            OBRC <other> -  Write failed.
 *
 */
ObexRespCode OBM_Write(ObStoreMgr *Obm, U8 *Buff, U16 Len)
{
    ObexRespCode    rcode = OBRC_FORBIDDEN;

    Assert(Obm->store != OBMT_UNSPECIFIED);

    switch (Obm->store) {
    case OBMT_VSTORE:
        /* Drop into generic store */
    case OBMT_GENERIC:
        rcode = FSTORE_Write(Obm->handle.file, Buff, Len);
        break;
    }

    if (Obm->amount == 0)
        App_Progress(Obm->uiContext, Obm->amount, Obm->objectLen);

    Obm->amount += Len;

    App_Progress(Obm->uiContext, Obm->amount, Obm->objectLen);
    return rcode;
}

/*---------------------------------------------------------------------------
 *            OBM_GetObjectLen()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Get the length of an object manager item.
 *
 * Return:    The length of the object in bytes.
 *
 */
U32 OBM_GetObjectLen(ObStoreMgr *Obm)
{
    Assert(Obm->store != OBMT_UNSPECIFIED);

    switch (Obm->store) {
    case OBMT_BROWSER:
        Obm->objectLen = BROWSER_GetObjectLen();
        break;
    case OBMT_VSTORE:
        /* Drop into generic store */
    case OBMT_GENERIC:
        Obm->objectLen = FSTORE_GetObjectLen(Obm->handle.file);
        break;
    }

    return Obm->objectLen;
}
