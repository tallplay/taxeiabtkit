#ifndef __FSTOREH
#define __FSTOREH

/****************************************************************************
 *
 * File:          fstore.h
 *
 * Description:   This file specifies the file based object store interface.
 * 
 * Created:       May 15, 1997
 *
 * $Project:XTNDAccess IrDA SDK$
 *
 * Copyright 1997-2005 Extended Systems, Inc.  ALL RIGHTS RESERVED.
 *
 * Unpublished Confidential Information of Extended Systems, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of Extended Systems, Inc.
 * 
 * Use of this work is governed by a license granted by Extended Systems,
 * Inc.  This work contains confidential and proprietary information of
 * Extended Systems, Inc. which is protected by copyright, trade secret,
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/
#include <stdio.h>

#include <obexerr.h>
#include <utils.h>

/****************************************************************************
 *
 * These structure definitions, functions and constants are used by the 
 * OBEX Protocol and must be provided by all Object Store implementations.
 *
 ****************************************************************************/
 
/****************************************************************************
 *
 * Structure and Constants Reference
 *
 ****************************************************************************/
typedef struct _ObStoreEntry *ObStoreHandle;

/****************************************************************************
 *
 * Function Reference 
 *
 ****************************************************************************/

/*---------------------------------------------------------------------------
 *
 * Prototype:     ObexRespCode FSTORE_Close(ObStoreHandle *ObsPtr)
 *
 * Description:   Close an object store item.
 *
 * Parameters:    obsPtr - handle of object store entry.
 *
 * Returns:       OBRC_SUCCESS - Operation was successful.
 *                OBRC_UNAUTHORIZED - Could not rename object store item.
 */
ObexRespCode FSTORE_Close(ObStoreHandle *ObsPtr);

/*---------------------------------------------------------------------------
 *
 * Prototype:     ObexRespCode FSTORE_Create(ObStoreHandle  *ObsPtr, 
 *                                           const char     *Name)
 *
 * Description:   Open the specified object store item for writing. ObsPtr
 *                is set to the newly created ObStoreHandle on success.
 *
 * Parameters:    ObsPtr - Handle of object store item to create.
 *                Name   - ASCII name of object.
 *
 * Returns:       OBRC_SUCCESS - Operation was successful.
 *                OBRC_NOT_FOUND - Could not create object store item.
 */
ObexRespCode FSTORE_Create(ObStoreHandle *ObsPtr, const char *Name);

/*---------------------------------------------------------------------------
 *
 * Prototype:     ObexRespCode FSTORE_Delete(ObStoreHandle *ObsPtr,
 *                                           const char    *Name)
 *
 * Description:   Delete the object and free the storage. The object
 *                can be deleted by providing its ObStoreHandle if it
 *                is open, or by providing its Name if it is not open.
 *
 * Parameters:    ObsPtr - handle to object store entry to delete. 
 *                Name   - name of object to delete.
 *
 * Returns:       OBRC_NOT_FOUND - File could not be deleted.
 *                OBRC_SUCCESS   - File was deleted.
 */
ObexRespCode FSTORE_Delete(ObStoreHandle *obsPtr, const char *Name);

/*---------------------------------------------------------------------------
 *
 * Prototype:     U32 FSTORE_GetObjectLen(ObStoreHandle obs)
 *
 * Description:   Get the length of an object store item.
 *                This function is REQUIRED by the OBEX Protocol.
 *
 * Parameters:    obs - handle of object store entry.
 *
 * Returns:       The object length
 */
U32 FSTORE_GetObjectLen(ObStoreHandle obs);

/*---------------------------------------------------------------------------
 *
 * Prototype:     BOOL FSTORE_Init()
 *
 * Description:   Initialize the file system object store.
 *
 * Parameters:    none 
 *
 * Returns:       TRUE - object store was initialized successfully. 
 *                FALSE - Unable to init.
 */
BOOL FSTORE_Init(void);

/*---------------------------------------------------------------------------
 *
 * Prototype:     ObexRespCode FSTORE_Open(ObStoreHandle *ObsPtr, 
 *                                         const char    *Name )
 *
 * Description:   Open an object store item for reading. If Name is NULL
 *                then open the default object. ObsPtr is set to the newly
 *                opened ObStoreHandle on success.
 *
 * Parameters:    ObsPtr - Pointer to object store entry
 *                Name   - Name of object to open
 *
 * Returns:       OBRC_SUCCESS indicates that the open is successful. 
 *                OBRC_NOT_FOUND indicates failure. 
 */
ObexRespCode FSTORE_Open(ObStoreHandle *ObsPtr, const char *Name);

/*---------------------------------------------------------------------------
 *
 * Prototype:     ObexRespCode FSTORE_Read(ObStoreHandle    Obs, 
 *                                         U8              *Buff,
 *                                         U16              Len )
 *
 * Description:   Read data from the object store entry.
 *                This function is REQUIRED by the OBEX Protocol.
 *
 * Parameters:    Obs - handle of object store entry.
 *                Buff - pointer to location where data is read into.
 *                Len - length in bytes of data to read
 *
 * Returns:       OBRC_SUCCESS      - Read was successful.
 *                OBRC_UNAUTHORIZED - Read failed. 
 */
ObexRespCode FSTORE_Read(ObStoreHandle Obs, U8 *Buff, U16 Len);

/*---------------------------------------------------------------------------
 *
 * Prototype:     ObexRespCode FSTORE_Write(ObStoreHandle   Obs, 
 *                                          U8             *Buff,
 *                                          U16             Len )
 *
 * Description:   Write data to the object store entry.
 *                This function is REQUIRED by the OBEX Protocol.
 *
 * Parameters:    Obs - handle of object store entry.
 *                Buff - pointer to data to write
 *                Len - length in bytes of data to write
 *
 * Returns:       OBRC_SUCCESS      - Write was successful.
 *                OBRC_UNAUTHORIZED - Write failed. 
 */
ObexRespCode FSTORE_Write(ObStoreHandle Obs, U8 *Buff, U16 Len);

/****************************************************************************
 *
 * Object Store Definitions
 *
 * Definitions used internally by the object store.
 *
 ****************************************************************************/

/****************************************************************************
 *
 * Structure and Constants Reference
 *
 ****************************************************************************/

/*---------------------------------------------------------------------------
 *
 * Size Constants used by ObStoreEntry
 */
#define OBS_MAX_NUM_ENTRIES 2

/****************************************************************************
 *
 * Data structures 
 *
 ****************************************************************************/

/*---------------------------------------------------------------------------
 *
 * ObStoreEntry Structure
 */
typedef struct _ObStoreEntry {
    ListEntry   node;
    FILE       *fp;
    const char *name;
    char        tmpName[L_tmpnam];
    U32         fileLen;
} ObStoreEntry;


#endif /* __FSTOREH */

