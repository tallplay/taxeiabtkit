/****************************************************************************
 *
 * File:        filexfer.c
 *
 * Description: This file contains the code for the OBEX File Transfer
 *              application. 
 *
 * Created:     May 17, 2000
 *
 * $Project:XTNDAccess Blue SDK$
 *
 * Copyright 1998-2005 Extended Systems, Inc.  ALL RIGHTS RESERVED.
 *
 * Unpublished Confidential Information of Extended Systems, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of Extended Systems, Inc.
 * 
 * Use of this work is governed by a license granted by Extended Systems,
 * Inc.  This work contains confidential and proprietary information of
 * Extended Systems, Inc. which is protected by copyright, trade secret,
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/
#include <osapi.h>

#if BT_STACK == XA_ENABLED
#include <me.h>
#include <bttypes.h>
#endif

#include "obmgr.h"
#include <obex.h>
#include <opush.h>
#include <ftp.h>
#include "filexfer.h"
#include "resource.h"

#define FOLDER_LISTING_OBJECT_TYPE      "x-obex/folder-listing"

/* These functions are exported by the UI for application output */
extern void App_Report(char *format,...);
extern void App_Progress(U32 barId, U32 currPos, U32 maxPos);
extern void App_UpdateUiPath();
extern BOOL App_AcceptRequest(const char *Name, const char *Type, const char *Oper);
#if OBEX_AUTHENTICATION == XA_ENABLED
extern BOOL App_GetPassword(char *password);
extern BOOL App_GetUserId(char *userId);
extern BOOL App_Authenticate(void);
#endif /* OBEX_AUTHENTICATION == XA_ENABLED */

/****************************************************************************
 *
 * ROM data
 *
 ****************************************************************************/

/*---------------------------------------------------------------------------
 *
 * Object Store Function call table
 */
static const ObStoreFuncTable FileXferFuncTable = {
    OBM_Read, OBM_Write, OBM_GetObjectLen,
#if OBEX_DYNAMIC_OBJECT_SUPPORT == XA_ENABLED
    OBM_ReadFlex,
#endif /* OBEX_DYNAMIC_OBJECT_SUPPORT == XA_ENABLED */
};


#if IRDA_STACK == XA_ENABLED
/*---------------------------------------------------------------------------
 *
 * OBEX IAS Service LSAP result. This value is in RAM because the LSAP 
 * value is not guaranteed to be fixed and may need to be updated after 
 * the server binds. It can be placed in ROM if the LSAP is known and fixed.
 */

U8 obexResult[] = {
    0x01,                    /* Type for Integer is 1 */
    0x00,0x00,0x00,0x02      /* Assumed Lsap */
};

U8 irXferResult[] = {
    0x01,                    /* Type for Integer is 1 */
    0x00,0x00,0x00,0x02      /* Assumed Lsap */
};

/*---------------------------------------------------------------------------
 *
 * IAS objects registered by Local OBEX Server.
 */
/* OBEX LSAP attribute */
const IrIasAttribute obexAttribs = {
   (U8*)"IrDA:TinyTP:LsapSel", 19, (U8*)obexResult, sizeof(obexResult)
};

/* OBEX Class Object */
IrIasObject obexObject = {
    (U8*)"OBEX", 4, 1, (IrIasAttribute*)&obexAttribs
};

/* OBEX:IrXfer LSAP attribute */
const IrIasAttribute irXferAttribs = {
   (U8*)"IrDA:TinyTP:LsapSel", 19, (U8*)irXferResult, sizeof(irXferResult)
};

/* OBEX:IrXfer Class Object */
IrIasObject irXferObject = {
    (U8*)"OBEX:IrXfer", 11, 1, (IrIasAttribute*)&irXferAttribs
};

#endif /* IRDA_STACK == XA_ENABLED */

/****************************************************************************
 *
 * RAM data
 *
 ****************************************************************************/
struct _FTXServerData {
   ObStoreMgr       *object;       /* Current Object's handle */
   GoepServerApp     ftp;          /* File transfer registration handle */
   GoepServerApp     push;         /* Object Push registration handle */
   GoepServerApp    *currApp;      /* Current server app handle */
#if OBEX_AUTHENTICATION == XA_ENABLED
   U8                authFlags;    /* OBEX Authentication flags */
   char              password[11]; /* OBEX Authentication password */
   char              userId[20];   /* OBEX Authentication userId */
   BOOL              setPassword;  /* Have we set the password yet? */
   BOOL              setUserId;    /* Have we set the userId yet? */
#endif /* OBEX_AUTHENTICATION == XA_ENABLED */
} FtxServer = {0};

/*---------------------------------------------------------------------------
 *            Ftx_Init
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initialize the OBEX Server and Object Store.
 *
 * Return:    TRUE if initialized, FALSE otherwise
 *
 */
BOOL Ftx_Init(void)
{
    ObStatus    status;
    ObexServerApp       *serverApp;
#if TCP_STACK == XA_ENABLED
    U16         serverPort;
#endif /* TCP_STACK == XA_ENABLED */
#if IRDA_STACK == XA_ENABLED
    U8          lsap;
#endif /* IRDA_STACK == XA_ENABLED */
#if OBEX_AUTHENTICATION == XA_ENABLED
    U8          i;
#endif /* OBEX_AUTHENTICATION == XA_ENABLED */
    /* Setup the Object Store function table */
    FtxServer.ftp.callback = FtxServerCallback;
    status = FTP_RegisterServer(&FtxServer.ftp, &FileXferFuncTable);
    if (status != OB_STATUS_SUCCESS) {
        App_Report("Server: FTP server initialization failed");
        return FALSE;
    }

    /* Setup the Object Store function table */
    FtxServer.push.callback = FtxServerCallback;
    status = OPUSH_RegisterServer(&FtxServer.push, &FileXferFuncTable);
    if (status != OB_STATUS_SUCCESS) {
        App_Report("Server: OPUSH server initialization failed");
        return FALSE;
    }

    /* Retrieve the FileXfer OBEX Server */
    serverApp = GOEP_GetObexServer(&FtxServer.ftp);

#if TCP_STACK == XA_ENABLED
    /* Assign the one TCP server port. GOEP can multiplex two 
     * profiles over the same OBEX connection 
     */
    serverPort = OBEX_ServerGetTcpPort(serverApp);
    App_Report("Server Listening on TCP Port %i", serverPort);
#endif /* TCP_STACK == XA_ENABLED */

#if IRDA_STACK == XA_ENABLED
    /* Register Obex and IrXfer IAS entries */
    status = OBEX_ServerGetIasLsap(serverApp, &lsap);
    if (status != OB_STATUS_SUCCESS) goto done;
        
    obexObject.attribs->value[4] = lsap;
    irXferObject.attribs->value[4] = lsap;

    /* Register the "OBEX" IAS Class name */
    status = IRIAS_Add(&obexObject);
    if (status != OB_STATUS_SUCCESS) goto done;
    /* Register the "IrXfer" IAS Class name */
    status = IRIAS_Add(&irXferObject);
    if (status != OB_STATUS_SUCCESS) {
done:
        App_Report("Server: FTP server initialization failed!");
        return FALSE;
    }    
#endif /* IRDA_STACK == XA_ENABLED */
 
    /* Initialize the current server app handle, assume FTP, since this
     * is a File Transfer sample application.  This value will change 
     * based on the callback events that are received.
     */
    FtxServer.currApp = &FtxServer.ftp;
#if OBEX_AUTHENTICATION == XA_ENABLED
    FtxServer.authFlags = 0;
    /* No user-defined password, just a default */
    FtxServer.setPassword = FALSE;
    /* No user-defined userId, just a default */
    FtxServer.setUserId = FALSE;
    /* Set default password */
    FtxServer.password[0] = 'M'; FtxServer.password[1] = 'y';
    FtxServer.password[2] = 'P'; FtxServer.password[3] = 'a';
    FtxServer.password[4] = 's'; FtxServer.password[5] = 's';
    FtxServer.password[6] = 'w'; FtxServer.password[7] = 'o';
    FtxServer.password[8] = 'r'; FtxServer.password[9] = 'd';
    FtxServer.password[10] = 0;
    /* Set default userId */
    FtxServer.userId[0] = 'M'; FtxServer.userId[1] = 'y'; 
    FtxServer.userId[2] = 'U'; FtxServer.userId[3] = 's';
    FtxServer.userId[4] = 'e'; FtxServer.userId[5] = 'r';
    FtxServer.userId[6] = 'I'; FtxServer.userId[7] = 'd';
    for (i = 8; i < 20; i++)
        FtxServer.userId[i] = 0;
#endif /* OBEX_AUTHENTICATION == XA_ENABLED */

    /* Now initialize the Object Store Component */
    if (OBM_Init() == FALSE) {
        App_Report("Object Store Initialization Failed!");
        return FALSE;
    }
    App_UpdateUiPath();

    return TRUE;
}

/*---------------------------------------------------------------------------
 *            Ftx_Deinit
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Deinitialize the OBEX protocol.
 *
 * Return:    void
 *
 */
void Ftx_Deinit(void)
{
#if OBEX_DEINIT_FUNCS == XA_ENABLED
   ObStatus status;

#if IRDA_STACK == XA_ENABLED
   /* Remove Server's OBEX and IrXfer IAS entries */
   status = IRIAS_Remove(&obexObject);
   Assert(status == IR_STATUS_SUCCESS);
   status = IRIAS_Remove(&irXferObject);
   Assert(status == IR_STATUS_SUCCESS);
#endif /* IRDA_STACK == XA_ENABLED */

   status = FTP_DeregisterServer(&FtxServer.ftp);

   status = OPUSH_DeregisterServer(&FtxServer.push);

#endif

   OBM_Deinit();
}

/*---------------------------------------------------------------------------
 *            Ftx_AbortServer
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Requests the current operation be aborted.
 *
 * Return:    void
 *
 */
void Ftx_AbortServer(void)
{
    ObStatus    status;

    status = FTP_ServerAbort(FtxServer.currApp, OBRC_CONFLICT);
    App_Report("Server: Abort request returned %s.", pObStatus(status));
}

#if OBEX_AUTHENTICATION == XA_ENABLED
/*---------------------------------------------------------------------------
 *            Ftx_SetPassword
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Set the password for OBEX Server Authentication.
 *
 * Return:    void
 *
 */
void Ftx_SetPassword(void)
{
    if (App_GetPassword(FtxServer.password)) {
        /* User-defined password set */
        FtxServer.setPassword = TRUE;  
        App_Report("Server: Password Set."); 
    }
    else {
        if (FtxServer.setPassword)
            App_Report("Server: Using Current Password: %s.", FtxServer.password);
        else App_Report("Server: Using Default Password: %s.", FtxServer.password);
    }
}

/*---------------------------------------------------------------------------
 *            Ftx_SetUserId
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Set the userId for OBEX Server Authentication.
 *
 * Return:    void
 *
 */
void Ftx_SetUserId(void)
{
    if (App_GetUserId(FtxServer.userId)) {
        /* User-defined userId set */
        FtxServer.setUserId = TRUE;
        App_Report("Server: UserId Set."); 
    }
    else {
        if (FtxServer.setUserId)
            App_Report("Server: Using Current UserId: %s.", FtxServer.userId);
        else App_Report("Server: Using Default UserId: %s.", FtxServer.userId);
    }
}
#endif /* OBEX_AUTHENTICATION == XA_ENABLED */

/*---------------------------------------------------------------------------
 *            FtxServerCallback
 *---------------------------------------------------------------------------
 *
 * Synopsis:  This function processes OBEX Server protocol events.
 *
 * Return:    void
 *
 */
static void FtxServerCallback(GoepServerEvent *Event)
{
    ObexRespCode    rcode;
    const char     *serv;
    ObStatus        status;
    BOOL            value;            

    /* Keep track of the current server application handle */
    FtxServer.currApp = Event->handler;

    if (Event->handler == &FtxServer.push) 
        serv = "PUSH: ";
    else
        serv = "FTP: ";


    /* First process only events that are not "object" related. */
    switch ( Event->event ) {
    case GOEP_EVENT_TP_CONNECTED:
        App_Report("Server: %s Transport Connection has come up.", serv);
        /* A new connection is established. Resetting to the Root folder
         * is for the Folder Browsing service. The object push service
         * always moves to the inbox before accessing any objects.
         */
        value = PATHMGR_Switch2Root();
        Assert(value == TRUE);
        App_UpdateUiPath();
        break;

    case GOEP_EVENT_TP_DISCONNECTED:
        App_Report("Server: %s Transport Connection has been disconnected.", serv);
        App_Progress(FTX_SERVER_PROGRESS, 0, 0);
#if OBEX_AUTHENTICATION == XA_ENABLED
        FtxServer.authFlags = 0;
#endif /* OBEX_AUTHENTICATION == XA_ENABLED */
        break;

    case GOEP_EVENT_CONTINUE:
#if OBEX_AUTHENTICATION == XA_ENABLED
        /* Build authentication headers if necessary. */
        ServerDoAuthentication(Event);
#endif /* OBEX_AUTHENTICATION == XA_ENABLED */
        /* Always call continue to keep the requests flowing. */
        status = FTP_ServerContinue(Event->handler);
        Assert(status == OB_STATUS_SUCCESS);
        break;

#if OBEX_AUTHENTICATION == XA_ENABLED
    case GOEP_EVENT_AUTH_CHALLENGE:
        App_Report("Server: %s Received Authenticate Challenge for %s operation.", serv, 
            GoepOpName(Event->oper));
#if OBEX_MAX_REALM_LEN > 0
        App_Report("Server: %s Realm: %s, charset %d , Options %d.", serv, 
            Event->challenge.realm+1, Event->challenge.realm[0], Event->challenge.options);
#endif /* OBEX_MAX_REALM_LEN > 0 */
        FtxServer.authFlags |= AUTH_RECEIVED_CHAL;
        break;        

    case GOEP_EVENT_AUTH_RESPONSE:
        App_Report("Server: %s Received Authenticate Response for %s operation.", serv, 
            GoepOpName(Event->oper));
        if (FTP_ServerVerifyAuthResponse(Event->handler, FtxServer.password, 
            (U8)StrLen(FtxServer.password))) {
            FtxServer.authFlags |= AUTH_AUTHENTICATED;
            App_Report("Server: %s UserId: %s, Verified.", serv, Event->response.userId);
        }
        else {
            FTP_ServerAbort(Event->handler, OBRC_UNAUTHORIZED);
            App_Report("Server: %s UserId: %s, Failed Verification.", serv, 
                Event->response.userId);
        }
        break;        
#endif /* OBEX_AUTHENTICATION == XA_ENABLED */

    default:
        /* Any events which come here may result in an Object Manager
         * call. So we must be sure that we are in the correct folder.
         */
        if (Event->handler == &FtxServer.push) {
            /* Need to make sure we're in the inbox (Object Push Service) */
            PATHMGR_Push();
            value = PATHMGR_Switch2Inbox();
            Assert(value == TRUE);
            App_UpdateUiPath();
        }

        switch ( Event->event ) {
        case GOEP_EVENT_START:
            App_Report("Server: %s Receiving a %s operation.", serv, GoepOpName(Event->oper));
            break;

        case GOEP_EVENT_DELETE_OBJECT:
        case GOEP_EVENT_PROVIDE_OBJECT:
            if (Event->oper == GOEP_OPER_PUSH || Event->oper == GOEP_OPER_PULL ||
                Event->oper == GOEP_OPER_DELETE) {
                /* Need Object to complete this request. */
                ServerObjectRequest(Event);
                break;
            }
            break;

        case GOEP_EVENT_ABORTED:
            App_Report("Server: %s %s operation aborted.", serv, GoepOpName(Event->oper));
            switch (Event->oper) {
            case GOEP_OPER_SETFOLDER:
                /* Cleanup any path changes that were started */
                PATHMGR_FlushNewPath();
                break;
        
            case GOEP_OPER_PUSH:
                /* Delete the file we created */
                if (FtxServer.object)
                    OBM_Delete(&FtxServer.object);
                break;

            case GOEP_OPER_PULL:
                if (FtxServer.object)
                    OBM_Close(&FtxServer.object);
                break;
            }
            break;

        case GOEP_EVENT_PRECOMPLETE:
            /* We want to process the Set Folder and Push commands now. This
             * way we can report the status of the request back to the client.
             */
            if (Event->oper == GOEP_OPER_SETFOLDER) {
                /* 
                 * Now apply the new path.
                */
                if (Event->info.setfolder.reset == FALSE) {
                    PATHMGR_AppendNewAscii( Event->info.setfolder.name, 
                                            Event->info.setfolder.nameLen);
                    PATHMGR_SetFlags(Event->info.setfolder.flags);

                    if (Event->info.setfolder.flags & OSPF_BACKUP)
                        App_Report("Server: %s Folder Backup requested.", serv);
                    if (Event->info.setfolder.nameLen)
                        App_Report("Server: %s Switching to Folder: %s.", serv, PATHMGR_GetNewPath());

                    if (PATHMGR_Switch2NewFolder() == FALSE) {
                        /* Change dir failed, return error to client */
                        FTP_ServerAbort(Event->handler, OBRC_NOT_FOUND);
                    }
                } else {
                    /* Reset to Root */
                    App_Report("Server: %s Folder Reset requested.", serv);
                    if (PATHMGR_Switch2Root() == FALSE) {
                        /* Change dir failed, return error to client */
                        FTP_ServerAbort(Event->handler, OBRC_NOT_FOUND);
                    }
                }
                App_UpdateUiPath();

            }
            else if (Event->oper == GOEP_OPER_PUSH && FtxServer.object) {
                /* See if we can successfully close the file we've received */
                rcode = OBM_Close( &FtxServer.object );
                if (rcode != OBRC_SUCCESS)
                    FTP_ServerAbort(Event->handler, rcode);
            }
            break;

        case GOEP_EVENT_COMPLETE:
            App_Report("Server: %s %s operation complete.", serv, GoepOpName(Event->oper));

            if (FtxServer.object) {  /* only GET still needs to return the object */
                rcode = OBM_Close( &FtxServer.object );
                Assert(rcode == OBRC_SUCCESS);
            }

            App_Progress(FTX_SERVER_PROGRESS, 0, 0);
            break;
        }

    }
}

/*---------------------------------------------------------------------------
 *            ServerObjectRequest
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Process the parsers request to open/create/delete an object.
 *
 * Return:    void
 *
 */
static void ServerObjectRequest(GoepServerEvent *Event)
{
    ObexRespCode rcode;
    
    FtxServer.object = OBM_New();
    Assert(FtxServer.object);

    OBM_AppendNameAscii( FtxServer.object, Event->info.pushpull.name,
                         Event->info.pushpull.nameLen);

    OBM_AppendType( FtxServer.object, Event->info.pushpull.type,
                    Event->info.pushpull.typeLen);

    if (App_AcceptRequest(OBM_GetNameAscii(FtxServer.object),
                          OBM_GetTypeAscii(FtxServer.object),
                          GoepOpName(Event->oper)) == FALSE) {
        App_Report("Server: User rejected object request.");
        FTP_ServerAbort(Event->handler, OBRC_NOT_FOUND);
        return;
    }

    switch (Event->oper) {

    case GOEP_OPER_DELETE:

        rcode = OBM_Delete(&FtxServer.object);
        if (rcode != OBRC_SUCCESS)
            FTP_ServerAbort(Event->handler, rcode);
        /* No need to accept and deleting killed the object pointer. */
        return;

    case GOEP_OPER_PULL:

        rcode = OBM_Open(FtxServer.object);
        if (rcode == OBRC_SUCCESS && 
            (OBM_GetObjectLen(FtxServer.object) != UNKNOWN_OBJECT_LENGTH)) {
            /* Be nice, build a LENGTH header for the response */
            Event->info.pushpull.objectLen = OBM_GetObjectLen(FtxServer.object);
        }
        break;

    case GOEP_OPER_PUSH:

        rcode = OBM_Create(FtxServer.object);
        OBM_SetObjectLen(FtxServer.object, Event->info.pushpull.objectLen);
        break;
    }

    if (rcode == OBRC_SUCCESS) {
        FTP_ServerAccept(Event->handler, FtxServer.object);
        OBM_SetUIContext(FtxServer.object, FTX_SERVER_PROGRESS);
    }

    if (StrLen(OBM_GetNameAscii(FtxServer.object)))
        App_Report("Server: Object Name \"%s\".", OBM_GetNameAscii(FtxServer.object));
    if (StrLen(OBM_GetTypeAscii(FtxServer.object)))
        App_Report("Server: Object Type \"%s\".", OBM_GetTypeAscii(FtxServer.object));
}

/*---------------------------------------------------------------------------
 *            GoepOpName
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Return a pointer to the name of the current server operation.
 *
 * Return:    ASCII String pointer.
 *
 */
static const char *GoepOpName(GoepOperation Op)
{
   switch (Op) {
   case GOEP_OPER_PUSH:
       return "Push";
   case GOEP_OPER_PULL:
       return "Pull";
   case GOEP_OPER_SETFOLDER:
       return "Set Folder";
   case GOEP_OPER_CONNECT:
       return "Connect";
   case GOEP_OPER_DISCONNECT:
       return "Disconnect";
   case GOEP_OPER_DELETE:
       return "Delete";
   case GOEP_OPER_ABORT:
       return "Abort";
   }
   return "Unknown";
}

/*---------------------------------------------------------------------------
 *            pObStatus
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Return a pointer to a description of the OBEX status code.
 *
 * Return:    ASCII String pointer.
 *
 */
static const char *pObStatus(ObStatus status)
{
   switch (status) {
      case OB_STATUS_SUCCESS:
         return "Success";
      case OB_STATUS_FAILED:
         return "Failed";
      case OB_STATUS_PENDING:
         return "Pending";
      case OB_STATUS_DISCONNECT:
         return "Disconnect";
      case OB_STATUS_NO_CONNECT:
         return "No Connection";
      case OB_STATUS_MEDIA_BUSY:
         return "Media Busy";
      case OB_STATUS_INVALID_PARM:
          return "Invalid Parameter";
      case OB_STATUS_INVALID_HANDLE:
         return "Invalid Handle";
      case OB_STATUS_PACKET_TOO_SMALL:
         return "Packet Too Small";
      case OB_STATUS_BUSY:
         return "Status Busy";
   }
   return "UNKNOWN";
}

/*---------------------------------------------------------------------------
 *            StrLen
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Counts the length of a string.  
 *
 * Return:    Length of the string in bytes.
 *
 */
U16 StrLen(const char *String)
{
   const char  *cp = String;

   Assert(String);

   while (*cp != 0) cp++;

   return (U16)(cp - String);
}

#if OBEX_AUTHENTICATION == XA_ENABLED
static void ServerDoAuthentication(GoepServerEvent *Event)
{
    ObexAuthResponse    rsp;
    ObexAuthChallenge   chal;
    U8                  chalStr[20];
    ObStatus            status;
    TimeT               time;


    if (Event->handler == &FtxServer.push) return;

    /* Only authenticate the client if the UI indicates we should */
    if (App_Authenticate() && !(FtxServer.authFlags & AUTH_AUTHENTICATED)) {
        /* First, authenticate the client if we haven't already. */
        chal.challenge = chalStr;
        chal.challengeLen = 15;

        time = OS_GetSystemTime();
        chalStr[0] = (U8)(time);
        chalStr[1] = (U8)(time >> 8);
        chalStr[2] = (U8)(time >> 16);
        chalStr[3] = (U8)(time >> 24);
        chalStr[4] = ':';
        OS_MemCopy(chalStr+5, FtxServer.password, StrLen(FtxServer.password));

        chal.options = 0x01;  /* Must send the UserId */
        chal.realm = "\0Network";  /* Use the Network UserId and Password */
        chal.realmLen = sizeof("\0Network");

        status = FTP_ServerAuthenticate(Event->handler, 0, &chal);
        Assert(status == OB_STATUS_SUCCESS);
        App_Report("Server: Built Authentication Challenge header.");
        return;
    }

    /* Otherwise, see if we need to respond to a challenge. */
    if (FtxServer.authFlags & AUTH_RECEIVED_CHAL) {
        FtxServer.authFlags &= ~AUTH_RECEIVED_CHAL;

        rsp.password = FtxServer.password;
        rsp.passwordLen = (U8)StrLen(FtxServer.password);
        rsp.userId = FtxServer.userId;
        rsp.userIdLen = (U8)StrLen(FtxServer.userId);

        status = FTP_ServerAuthenticate(Event->handler, &rsp, 0);
        Assert(status == OB_STATUS_SUCCESS);
        App_Report("Server: Built Authentication Response header.");
        return;
    }
}
#endif /* OBEX_AUTHENTICATION == XA_ENABLED */
