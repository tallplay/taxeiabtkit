/****************************************************************************
 *
 * File:        pathmgr.c
 *
 * Description: This file contains the code for managing the current folder
 *              for the client and server(s).
 *
 * Created:     May 9, 1997
 *
 * $Project:XTNDAccess IrDA SDK$
 *
 * Copyright 1997-2005 Extended Systems, Inc.  ALL RIGHTS RESERVED.
 *
 * Unpublished Confidential Information of Extended Systems, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of Extended Systems, Inc.
 * 
 * Use of this work is governed by a license granted by Extended Systems,
 * Inc.  This work contains confidential and proprietary information of
 * Extended Systems, Inc. which is protected by copyright, trade secret,
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/
#include <stdio.h>
#include <direct.h>

#include "pathmgr.h"

/****************************************************************************
 *
 * Local ROM data
 *
 ****************************************************************************/
static const char *inboxFolder = "\\QbInbox";  /* Used by Object Push server */
static const char *rootFolder = "\\";          /* Used by File Transfer server */

/****************************************************************************
 *
 * Local RAM data
 *
 ****************************************************************************/
/* 
 * In the file transfer application, the path manager is responsible for 
 * tracking the correct path/folder for each component of the application.
 * The three components are; the client, the object push server and the file
 * transfer server. The client folder is fixed at the location from which
 * the application was executed. The inbox server folder is fixed in the 
 * inbox. The file transfer server folder varies as set path requests are 
 * received. This folder is the application's current folder.
 */
static char clientFolder[PATHMGR_MAX_PATH];    /* Holds client folder */
static char saveFolder[PATHMGR_MAX_PATH];      /* Temp folder storage */
static char newFolder[PATHMGR_MAX_PATH];       /* New sub-folder request */
static U8   newFolderFlags;                    /* Options & state for newFolder */
static U16  newFolderLen;

/* New Folder Flags */
#define NFF_NONE                0x00    /* == OSPF_NONE */
#define NFF_BACKUP              0x01    /* == OSPF_BACKUP */
#define NFF_DONT_CREATE         0x02    /* == OSPF_DONT_CREATE */    
#define NFF_UNICODE_ODD         0x04    /* Used by AppendUni() */

/****************************************************************************
 *
 * Functions
 *
 ****************************************************************************/
/*---------------------------------------------------------------------------
 *            PATHMGR_Init()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initialize the path manager.
 *
 * Return:    TRUE - The path manager is initialized.
 *            FALSE - Could not create the inbox.
 *
 */
BOOL PATHMGR_Init()
{
    char    *p;

    /* Save Clients folder */
    p = _getcwd(clientFolder, PATHMGR_MAX_PATH);
    Assert(p == clientFolder);

    /* Create Inbox */
    if (_chdir(inboxFolder) && _mkdir(inboxFolder)) {
        Report(("PATHMGR: Unable to create Inbox.\n"));
        return FALSE;
    }

    newFolderFlags = NFF_NONE;
    newFolder[0] = 0;
    newFolderLen = 0;

    return TRUE;
}

/*---------------------------------------------------------------------------
 *            PATHMGR_Push
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Save the current folder.
 *
 */
void PATHMGR_Push()
{
    char    *p;

    p = _getcwd(saveFolder, PATHMGR_MAX_PATH);
    Assert(p == saveFolder);
}

/*---------------------------------------------------------------------------
 *            PATHMGR_Pop
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Restore the previously saved (pushed) folder.
 *
 */
void PATHMGR_Pop()
{
    int     status;

    status = _chdir(saveFolder);
    Assert(status == 0);
}

/*---------------------------------------------------------------------------
 *            PATHMGR_Set
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Set the current folder to the specified location.
 *
 * Return:    TRUE - success
 *            FALSE - failed
 *
 */
#define PATHMGR_Set(_FOLDER)    (_chdir((_FOLDER)) == 0)

/*---------------------------------------------------------------------------
 *            PATHMGR_Switch2Client
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Set current folder to clients folder
 *
 * Return:    TRUE - success
 *            FALSE - failed
 *
 */
BOOL PATHMGR_Switch2Client(void)
{
    return PATHMGR_Set(clientFolder);
}

/*---------------------------------------------------------------------------
 *            PATHMGR_Switch2Inbox
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Set current folder to clients folder.
 *
 * Return:    TRUE - success
 *            FALSE - failed
 *
 */
BOOL PATHMGR_Switch2Inbox(void)
{
    return PATHMGR_Set(inboxFolder);
}

/*---------------------------------------------------------------------------
 *            PATHMGR_Switch2Root
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Set current folder to clients folder.
 *
 * Return:    TRUE - success
 *            FALSE - failed
 *
 */
BOOL PATHMGR_Switch2Root(void)
{
    return PATHMGR_Set(rootFolder);
}

/*---------------------------------------------------------------------------
 *            PATHMGR_Switch2NewFolder
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Set file transfer server folder to new folder.
 *
 * Return:    TRUE - success
 *            FALSE - failed
 *
 */
BOOL PATHMGR_Switch2NewFolder(void)
{
    int     status = 0;

    if (newFolderFlags & NFF_UNICODE_ODD) {
        printf("PATHMGR: Received Unicode Pathname is malformed.\n");
        return FALSE;
    }

    if (newFolderFlags & NFF_BACKUP) {
        status = _chdir("..");
        if (status != 0)
            goto exit;
    }

    if (newFolderLen) {
        status = _chdir(newFolder);
        if (status != 0) {
            /* Couldn't change to newFolder, if we can create it, try that. */
            if (newFolderFlags & NFF_DONT_CREATE) {
                goto exit;
            }
            if (_mkdir(newFolder) || _chdir(newFolder)) {
                /* Couldn't create newFolder either, status is already set. */
                goto exit;
            }
            /* Successfully created new folder. */
            status = 0;
        }
    }

exit:
    newFolder[0] = 0;
    newFolderLen = 0;
    newFolderFlags = NFF_NONE;

    return (status == 0);
}


/*---------------------------------------------------------------------------
 *            PATHMGR_AppendUni
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Store incoming UNICODE path in ASCII.
 *
 * Return:    TRUE - success
 *            FALSE - failed
 *
 */
BOOL PATHMGR_AppendNewUni(const U8 *Path, U16 Len)
{
    /* Check if the appended name is going to overflow our buffer. */
    if ((newFolderLen + (Len/2)) > (PATHMGR_MAX_PATH - 1)) {
        return FALSE;
    }

    /* Convert Unicode path to ASCII. We ignore the odd Unicode
     * byte, it should be 0. The final string is null terminated.
     */
    while (Len > 0) {
        if (newFolderFlags & NFF_UNICODE_ODD) {
            newFolder[newFolderLen++] = *Path;
            newFolderFlags &= ~NFF_UNICODE_ODD;
        } else {
            newFolderFlags |= NFF_UNICODE_ODD;
        }
        Path++;
        Len--;
    }

    /* Put a NULL at the end if one does not exist. */
    if (newFolderLen > 0) {
        if (newFolder[newFolderLen-1] != 0)
            newFolder[newFolderLen] = 0;
        else newFolderLen--;
    }    
    return TRUE;
}

BOOL PATHMGR_AppendNewAscii(const U8 *Path, U16 Len)
{
    /* Check if the appended name is going to overflow our buffer. */
    if ((newFolderLen + Len) > (PATHMGR_MAX_PATH - 1)) {
        return FALSE;
    }
    Assert ((newFolderFlags & NFF_UNICODE_ODD) == 0);

    OS_MemCopy(newFolder+newFolderLen, Path, Len);
    newFolderLen += Len;

    return TRUE;
}

/*---------------------------------------------------------------------------
 *            PATHMGR_SetFlags()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Sets optional backup and don't create flags before applying
 *            new folder.
 *
 */
void PATHMGR_SetFlags(U8 setPathFlags)
{
    newFolderFlags |= (setPathFlags & (NFF_BACKUP|NFF_DONT_CREATE));
}

/*---------------------------------------------------------------------------
 *            PATHMGR_FlushNewPath()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Flush the stored new path information.
 *
 */
void PATHMGR_FlushNewPath()
{
    newFolder[0] = 0;
    newFolderLen = 0;
    newFolderFlags = NFF_NONE;
}

/*---------------------------------------------------------------------------
 *
 * Prototype:     const char *PATHMGR_GetNewPath()
 *
 * Description:   Returns a pointer to the current newPath if it is valid.
 *
 */
const char *PATHMGR_GetNewPath(void)
{
    if (newFolderFlags & NFF_UNICODE_ODD) 
        return 0;

    return newFolder;
}
