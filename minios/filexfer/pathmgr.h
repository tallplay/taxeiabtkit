#ifndef __PATHMGRH
#define __PATHMGRH
/****************************************************************************
 *
 * File:          pathmgr.h
 *
 * Description:   This file specifies the interface to the folder manager.
 * 
 * Created:       May 15, 1997
 *
 * $Project: XTNDAccess Blue SDK$
 *
 * Copyright 1997-2005 Extended Systems, Inc.  ALL RIGHTS RESERVED.
 *
 * Unpublished Confidential Information of Extended Systems, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of Extended Systems, Inc.
 * 
 * Use of this work is governed by a license granted by Extended Systems,
 * Inc.  This work contains confidential and proprietary information of
 * Extended Systems, Inc. which is protected by copyright, trade secret,
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/
#include <osapi.h>

#define PATHMGR_MAX_PATH   128
 
/*---------------------------------------------------------------------------
 *
 * Prototype:     BOOL PATHMGR_AppendNewUni(const U8 *Path, U16 Len )
 *
 * Description:   Append the Unicode path to the new ASCII path.
 *
 * Parameters:    Path  - Unicode path string.
 *                Len   - Length of the path.
 *
 * Returns:       TRUE  - The path was successfully appended.
 *                FALSE - Insufficient space to append the path.
 */
BOOL PATHMGR_AppendNewUni(const U8 *Path, U16 Len);
BOOL PATHMGR_AppendNewAscii(const U8 *Path, U16 Len);

/*---------------------------------------------------------------------------
 *
 * Prototype:     void PATHMGR_FlushNewPath()
 *
 * Description:   Cancel a path update in progress.
 *
 */
void PATHMGR_FlushNewPath(void);

/*---------------------------------------------------------------------------
 *
 * Prototype:     BOOL PATHMGR_Init()
 *
 * Description:   Initialize the folder manager.
 *
 * Parameters:    none 
 *
 * Returns:       TRUE - folder manager was initialized successfully. 
 *                FALSE - Unable to init.
 */
BOOL PATHMGR_Init(void);

/*---------------------------------------------------------------------------
 *
 * Prototype:     void PATHMGR_Switch2Root()
 *
 * Description:   Reset the path to the file transfer server root.
 *
 */
BOOL PATHMGR_Switch2Root(void);

/*---------------------------------------------------------------------------
 *
 * Prototype:     BOOL PATHMGR_Switch2NewFolder()
 *
 * Description:   Apply the path flags and the new path.
 *
 * Returns:       TRUE  - Path changes were successfully applied.
 *                FALSE - Path changes failed.
 */
BOOL PATHMGR_Switch2NewFolder(void);

/*---------------------------------------------------------------------------
 *
 * Prototype:     void PATHMGR_Switch2Inbox()
 *
 * Description:   Set the path to the inbox.
 *
 */
BOOL PATHMGR_Switch2Inbox(void);

/*---------------------------------------------------------------------------
 *
 * Prototype:     void PATHMGR_Switch2Client()
 *
 * Description:   Set the path to the application client.
 *
 */
BOOL PATHMGR_Switch2Client(void);

/*---------------------------------------------------------------------------
 *
 * Prototype:     void PATHMGR_SetFlags
 *
 * Description:   Set the flags for the setpath command.
 *
 */
void PATHMGR_SetFlags(U8 setPathFlags);

/*---------------------------------------------------------------------------
 *
 * Prototype:     void PATHMGR_Push()
 *
 * Description:   Save the current path.
 *
 */
void PATHMGR_Push(void);

/*---------------------------------------------------------------------------
 *
 * Prototype:     void PATHMGR_Pop()
 *
 * Description:   Restore to the previously Pushed folder.
 *
 */
void PATHMGR_Pop(void);

/*---------------------------------------------------------------------------
 *
 * Prototype:     const char *PATHMGR_GetNewPath()
 *
 * Description:   Returns a pointer to the current newPath if it is valid.
 *
 */
const char *PATHMGR_GetNewPath(void);

#endif /* __PATHMGRH */
