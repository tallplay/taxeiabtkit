/****************************************************************************
 *
 * File:        fstore.c
 *
 * Description: This file contains the code for an ANSI Standard Library
 *              based File System Object Store. It is  used in the FileXfer
 *              application to access the native filesystem. 
 *
 * Created:     May 9, 1997
 *
 * $Project:XTNDAccess IrDA SDK$
 *
 * Copyright 1997-2005 Extended Systems, Inc.  ALL RIGHTS RESERVED.
 *
 * Unpublished Confidential Information of Extended Systems, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of Extended Systems, Inc.
 * 
 * Use of this work is governed by a license granted by Extended Systems,
 * Inc.  This work contains confidential and proprietary information of
 * Extended Systems, Inc. which is protected by copyright, trade secret,
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/
#define _POSIX_
#include <io.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <osapi.h>
#include "fstore.h"

/****************************************************************************
 *
 * Internal Function prototypes
 *
 ****************************************************************************/
static ObStoreHandle ObsNew(void);

/****************************************************************************
 *
 * Local ROM data
 *
 ****************************************************************************/

static const char *defaultOpen = "index.txt";
static const char *defaultRxName = "rxqbeam.dft";

/****************************************************************************
 *
 * Local RAM data
 *
 ****************************************************************************/
static ObStoreEntry obsEntries[OBS_MAX_NUM_ENTRIES];
static ListEntry    obsList;
 
/*---------------------------------------------------------------------------
 *            FSTORE_Init()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initialize the object store.
 *
 * Return:    TRUE - object store initialized.
 *            FALSE - could not init.
 *
 */
BOOL FSTORE_Init(void)
{
    U8 i;

    /* Initialize the object store entries */
    InitializeListHead(&obsList);

    for (i = 0; i < OBS_MAX_NUM_ENTRIES; i++) {
        InsertTailList(&obsList, &(obsEntries[i].node));
    }

    return TRUE;

}


/*---------------------------------------------------------------------------
 *            FSTORE_Create()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Open an object store item for writing.
 *
 * Return:    Success or failure of create operation.
 *
 */
ObexRespCode FSTORE_Create(ObStoreHandle *ObsPtr, const char *Name)
{
    char tmpName[FILENAME_MAX];

    Assert( ObsPtr );
    
    if ((*ObsPtr = ObsNew()) == 0) {
        return OBRC_NOT_FOUND;
    }

    /* We open a temp file to avoid accidentally clobbering an existing
     * file that already exists (if the operation doesn't complete 
     * successfully). We maintain a pointer to the real filename, which
     * is used when the file is closed.
     */
    (*ObsPtr)->name = Name;
    tmpnam(tmpName);

    /* Remove path prefix from tmpName */
    strcpy((*ObsPtr)->tmpName, tmpName+strlen(_P_tmpdir));
    
    /* Open the file */
    (*ObsPtr)->fp = fopen((*ObsPtr)->tmpName, "wb");

    if ((*ObsPtr)->fp) {
        return OBRC_SUCCESS;
    }

    Report(("FSTORE: Couldn't Create %s for writing.\n", (*ObsPtr)->name));
    FSTORE_Close( ObsPtr );

    return OBRC_NOT_FOUND;
}

/*---------------------------------------------------------------------------
 *            FSTORE_Open()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Open and object store item for reading. If a name is not set
 *            the default object is opened.
 *
 * Return:    OBRC_SUCCESS - Open is successful. 
 *            OBRC_NOT_FOUND - Failed to open file.
 *
 */
ObexRespCode FSTORE_Open(ObStoreHandle *obs, const char *Name)
{
    Assert( obs );

    if ((*obs = ObsNew()) == 0) {
        return OBRC_NOT_FOUND;
    }

    if (strlen(Name) == 0) {
        /* There is no name so set the name to the default object */
        (*obs)->name = defaultOpen;
    } else {
        (*obs)->name = Name;
    }

    /* Open the file for reading */
    (*obs)->fp = fopen((*obs)->name, "rb");
    if ((*obs)->fp == 0) {
        goto open_err;
    }
    
    /* Seek to the end to get the length */
    if (fseek((*obs)->fp, 0, SEEK_END) != 0) {
        goto open_err;
    }

    (*obs)->fileLen = ftell((*obs)->fp);
    if ((signed)(*obs)->fileLen == -1L) {
        goto open_err;
    }

    /* Seek back to the beginning */
    if (fseek((*obs)->fp, 0, SEEK_SET) != 0) {
        goto open_err;
    }

    return OBRC_SUCCESS;

open_err:
    Report(("FSTORE: Couldn't Open %s for reading.\n", (*obs)->name));
    FSTORE_Close( obs );

    return OBRC_NOT_FOUND;

}


/*---------------------------------------------------------------------------
 *            FSTORE_GetObjectLen()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Get the length of an object store item.
 *
 * Return:    The length of the object in bytes.
 *
 */
U32 FSTORE_GetObjectLen(ObStoreHandle obs)
{
    Assert(obs);
    return obs->fileLen;
}

/*---------------------------------------------------------------------------
 *            FSTORE_Write()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Write data to the given object store item.
 *
 * Return:    OBRC_SUCCESS -      Buffer was written successfully.
 *            OBRC_UNAUTHORIZED - Buffer was not written.
 *
 */
ObexRespCode FSTORE_Write(ObStoreHandle obs, U8 *buff, U16 len)
{
    ObexRespCode ret = OBRC_UNAUTHORIZED;

    if (obs != 0) {
        Assert(obs->fp != 0);
        if (fwrite(buff, 1, len, obs->fp) == len) {
            ret = OBRC_SUCCESS;
        } else {
            printf("Error writing to file\n");
        }
    }

    return ret;

}

/*---------------------------------------------------------------------------
 *            FSTORE_Read()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Read data from the given object store item.
 *
 * Return:    OBRC_SUCCESS -      Buffer was read successfully.
 *            OBRC_UNAUTHORIZED - Buffer was not read.
 *
 */
ObexRespCode FSTORE_Read(ObStoreHandle obs, U8* buff, U16 len)
{
    ObexRespCode ret = OBRC_UNAUTHORIZED;

    if (obs != 0) {
        Assert(obs->fp != 0);
        if (fread(buff, 1, len, obs->fp) == len) {
            ret = OBRC_SUCCESS;
        } else {
            printf("Error reading from file! len %d\n", len);
        }
    }

    return ret;

}


#if OBEX_DYNAMIC_OBJECT_SUPPORT == XA_ENABLED
/*---------------------------------------------------------------------------
 *            FSTORE_ReadFlex()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Read data from the given object store item.
 *
 * Return:    OBRC_SUCCESS -      Buffer was read successfully.
 *            OBRC_UNAUTHORIZED - Buffer was not read.
 *
 */
ObexRespCode FSTORE_ReadFlex(ObStoreHandle obs, U8 *buff, U16 *len, BOOL *more)
{
    ObexRespCode ret = OBRC_UNAUTHORIZED;
    U16          rLen;

    if (obs != 0) {
        Assert(obs->fp != 0);
        rLen = fread(buff, 1, *len, obs->fp);

        if (ferror(obs->fp) == 0)
            ret = OBRC_SUCCESS;
    }

    if (rLen < *len)
        *more = FALSE;
    else *more = TRUE;

    *len = rLen;
    return ret;
}
#endif
    
/*---------------------------------------------------------------------------
 *            FSTORE_Delete()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Delete the given object store item. If the object is deleted
 *            by Name, then it must be in the current OBEX directory. The 
 *            Object store handle (if provided) is returned to the pool.
 *
 * Return:    OBRC_SUCCESS -   File was deleted.
 *            OBRC_NOT_FOUND - File was not deleted.
 */
ObexRespCode FSTORE_Delete(ObStoreHandle *ObsPtr, const char *Name)
{

    if (ObsPtr && *ObsPtr) {
        /* We're deleting an open file, close it first */
        Assert ((*ObsPtr)->fp);

        fclose( (*ObsPtr)->fp );
        (*ObsPtr)->fp = 0;

        /* Since it's an open file, we must have created it (as a temp). */
        Assert ((*ObsPtr)->tmpName[0] != 0);

        remove((*ObsPtr)->tmpName);
    
        /* Free the object handle */
        InsertTailList(&obsList, &((*ObsPtr)->node));
        *ObsPtr = 0;

        return OBRC_SUCCESS;
    }

    /* We're deleting a file which we haven't opened or created */
    Assert(Name);

    if (remove(Name))
        return OBRC_NOT_FOUND;

    return OBRC_SUCCESS;
}

/*---------------------------------------------------------------------------
 *            FSTORE_Close()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Close the object store item and return the handle to the pool.
 *
 * Return:    Success or failure of create operation.
 *
 */
ObexRespCode FSTORE_Close(ObStoreHandle *obsPtr)
{
    ObexRespCode  rcode = OBRC_SUCCESS;
    ObStoreHandle obs = *obsPtr;

    Assert(obs);

    if (obs->fp != 0) {
        fclose(obs->fp);
        obs->fp = 0;

        /* If the file is a temporary and a new name is set, rename it. */
        if ((obs->tmpName[0] != 0) && (obs->name[0] != 0)) {
    
            /* Remove the (possibly) existing file before renaming it */
            remove(obs->name);

            if (rename(obs->tmpName, obs->name)) {
                Report(("FSTORE: Unable to rename file from %s to %s\n", 
                        obs->tmpName, obs->name));

                /* Remove the temp file and return an error */
                remove(obs->tmpName);                
                rcode = OBRC_UNAUTHORIZED;
                goto close_done;
            }
        }
    }

close_done:
    /* Return the ObStoreHandle to the pool */
    InsertTailList(&obsList, &(obs->node));
    
    *obsPtr = 0;
    return rcode;
}

/*---------------------------------------------------------------------------
 *            ObsNew()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Obtain an object store item.
 *
 * Return:    Handle of object store or 0 if no more objects store entries.
 *
 */
static ObStoreHandle ObsNew(void)
{
    ObStoreHandle obs;

    if (!IsListEmpty(&obsList)) {
        obs = (ObStoreHandle)RemoveHeadList(&obsList);
        obs->fp = 0;
        obs->name = 0;
        obs->fileLen = 0;
        obs->tmpName[0] = 0;
    } else {
        obs = 0;
        Report(("ObsNew: Out of entries\n"));
    }
    return obs;

}

