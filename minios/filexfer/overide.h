#ifndef __OVERIDE_H
#define __OVERIDE_H

/****************************************************************************
 *
 * File:
 *     $Workfile:overide.h$ for XTNDAccess Blue SDK, Version 2.0.2
 *     $Revision:23$
 *
 * Description:
 *     Configuration overrides for the FileXfer project.
 * 
 * Copyright 1999-2005 Extended Systems, Inc.  ALL RIGHTS RESERVED.
 *
 * Unpublished Confidential Information of Extended Systems, Inc.
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of Extended Systems, Inc.
 * 
 * Use of this work is governed by a license granted by Extended Systems,
 * Inc.  This work contains confidential and proprietary information of
 * Extended Systems, Inc. which is protected by copyright, trade secret,
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

/* WARNING: The values in this overide.h file were selected specifically for
 * this sample application. If you change them, the sample application may fail
 * to compile or not work properly.
 */

/*
 * Enable the desired stacks to run with OBEX. 
 *
 *     Since OBEX can be configured to run over a mix of stacks. The
 *     control of which stacks are included has been moved to the
 *     individual workspaces where it can be better managed.
 */
#ifdef BTSTACK
#define BT_STACK                    XA_ENABLED
#else
#define BT_STACK                    XA_DISABLED
#endif

#ifdef IRDASTACK
#define IRDA_STACK                  XA_ENABLED
#else
#define IRDA_STACK                  XA_DISABLED
#endif

#ifdef TCPSTACK
#define TCP_STACK                   XA_ENABLED
#else
#define TCP_STACK                   XA_DISABLED
#endif

#define XA_LOAD_LIST                XA_MODULE(OBEX) XA_MODULE(GOEP) XA_MODULE(OPUSH) XA_MODULE(FTP)

/*
 * IrDA specific options that FileXfer requires.
 */
#if IRDA_STACK == XA_ENABLED
#include <frmconfig.h>
#define IR_CONS_SIZE                4   
#define IR_IAS_SERVER_SIZE          3    
#define IR_ADD_MS_TTP_PATCH         XA_ENABLED
#define IR_LMP_DISCONNECT           XA_ENABLED
#endif

/*
 * Bluetooth specific options that FileXfer requires.
 */
#if BT_STACK == XA_ENABLED
#define XA_SNIFFER                  XA_ENABLED

/* Only needed for the USB transport. */
#define HCI_USB_TX_BUFFER_SIZE      260
#endif

#define OBEX_DEINIT_FUNCS           XA_ENABLED

/* FileXfer uses the Event Manager to initialize OBEX. 
 */
#define XA_EVENTMGR                 XA_ENABLED

/* Required for File Transfer application because it builds the folder
 * listing objects dynamically.
 */
#define OBEX_DYNAMIC_OBJECT_SUPPORT XA_ENABLED
#define GOEP_MAX_TYPE_LEN           25
#define OBEX_SERVER_CONS_SIZE       1

#define FTP_EXPANDED_API            XA_ENABLED
#define OPUSH_EXPANDED_API          XA_ENABLED
#define OBEX_PACKET_FLOW_CONTROL    XA_ENABLED
#define OBEX_ROLE_CLIENT            XA_DISABLED

/* Enable if you want to require OBEX Authentication */
#define OBEX_AUTHENTICATION         XA_ENABLED 

#if (IRDA_STACK == XA_ENABLED) || (TCP_STACK == XA_ENABLED)
/* This option enables the OBEX Sniffer hooks */
#define JETTEST                     XA_ENABLED
#else
/* OBEX Sniffer not used in Bluetooth since the BlueMgr has a sniffer */
#define JETTEST                     XA_DISABLED
#endif


#endif /* __OVERIDE_H */
