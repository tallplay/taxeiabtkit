//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by FtxGui.rc
//
#define FTX_FILEXFER                    101
#define FTX_ACCEPT                      102
#define FTX_SERVER_TCP                  103
#define FTX_PASSWORD                    104
#define FTX_USER_ID                     105
#define OBEX_SNIFFER_MAIN               106
#define FTX_EXIT                        1001
#define FTX_SNIFFER                     1002
#define FTX_SERVER_ABORT                1003
#define FTX_SERVER_PROGRESS             1004
#define FTX_SERVER_OUTPUT               1005
#define FTX_CURRENT_FOLDER              1006
#define IDC_NAME                        1007
#define IDC_TYPE                        1008
#define IDC_PROMPT_USER                 1009
#define FTX_INTRO                       1010
#define IPP_PICK_TCPPORT                1011
#define IPP_OK                          1013
#define IPP_CANCEL                      1014
#define IPP_TCP_PORT                    1016
#define IDC_AUTH                        1018
#define FTX_SET_PASS                    1019
#define FTX_SET_USER_ID                 1020
#define IPP_PASSWORD                    1021
#define IPP_USER_ID                     1022
#define ID_SNIFFER                      1023
#define OBEX_SNIFFER_LISTBOX            1024
#define ID_SNIFFER_CLEAR                40008
#define ID_SNIFFER_SAVEAS               40012

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        108
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1025
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
