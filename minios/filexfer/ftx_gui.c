/*****************************************************************************
 * File:        ftx_gui.c
 *
 * Description: This file contains GUI code for FileXfer sample application.
 *
 * Created:     May 17, 2000
 *
 * $Project:XTNDAccess Blue SDK$
 *
 * Copyright 1999-2005 Extended Systems, Inc.  ALL RIGHTS RESERVED.
 *
 * Unpublished Confidential Information of Extended Systems, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any means, 
 * or stored in a database or retrieval system, without prior written 
 * permission of Extended Systems, Inc.
 * 
 * Use of this work is governed by a license granted by Extended Systems, Inc.  
 * This work contains confidential and proprietary information of Extended 
 * Systems, Inc. which is protected by copyright, trade secret, trademark and 
 * other intellectual property rights.
 *
 ****************************************************************************/
#define STRICT
#include <windows.h>
#include <commctrl.h>
#include <stdio.h>
#include "resource.h"
#include <direct.h>  /* For _getcwd() */
#include <winreg.h>

#include <osapi.h>
#if BT_STACK == XA_ENABLED
#include <bttypes.h>
#include <me.h>
#include <bluemgr_ui.h>
#endif

#include <parseopts.h>
#include "obmgr.h"
#include <opush.h>
#include <ftp.h>
#include "filexfer.h"

/* Internal Global variables for FileXfer GUI */
HWND                    AppWnd;
HINSTANCE               hInst;
char                    objectName[64];
char                    lastObject[64];


struct _AcceptReq {
    const char     *Oper;
    const char     *Name;
    const char     *Type;
} AcceptReq;

/* Internal function prototypes */
static void CALLBACK TimerExpired(HWND hWnd, UINT uMsg, UINT idEvent, DWORD dwTime);
#if OBEX_AUTHENTICATION == XA_ENABLED
static BOOL CALLBACK PasswordDlgProc(HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam);
static BOOL CALLBACK UserIdDlgProc(HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam);
#endif /* OBEX_AUTHENTICATION == XA_ENABLED */
static BOOL CALLBACK AppWndProc(HWND, UINT, WPARAM, LPARAM);
       void          App_Report(char *format,...);
#if BT_STACK == XA_ENABLED
static LPTSTR GpaGetToolTipText(int ControlId);
#endif /* BT_STACK == XA_ENABLED */

extern BOOL IsObexSnifferOpen();
extern HWND StartObexSniffer(HINSTANCE Instance, int CmdShow);
extern void StopObexSniffer(void);

/****************************************************************************
 ** Main Entrypoint
 **/
HWND* APP_Init( HINSTANCE Inst, HINSTANCE PrevInst, LPTSTR CmdLine, int CmdShow )
{
    hInst = Inst;

    InitCommonControls();

    AppWnd = CreateDialog(Inst, MAKEINTRESOURCE(FTX_FILEXFER), NULL, AppWndProc);
    if (AppWnd == NULL)
        return 0;

#if BT_STACK == XA_ENABLED
    if (BMG_Initialize(Inst, AppWnd) == 0) {
        DestroyWindow(AppWnd);
        return 0;
    }
    BMG_AddMainMenu(AppWnd);
    BMG_AddToolTips(AppWnd, GpaGetToolTipText);
#endif /* BT_STACK == XA_ENABLED */

    ShowWindow( AppWnd, CmdShow );

    if (Ftx_Init() == FALSE) {
        SetTimer(AppWnd, 0, (1000*3), TimerExpired);
    }

    return &AppWnd;
}

void APP_Deinit(void)
{
   Ftx_Deinit();
}

void APP_Thread(void)
{

}

static void CALLBACK TimerExpired(HWND hWnd, UINT uMsg, UINT idEvent, DWORD dwTime)
{
    switch (uMsg) {
    case WM_TIMER:
        KillTimer(hWnd, 0);
        APP_Deinit();
    }
}

#if OBEX_AUTHENTICATION == XA_ENABLED
BOOL App_Authenticate(void)
{
    U32 btnState;

    btnState = SendDlgItemMessage(AppWnd, IDC_AUTH, BM_GETSTATE, 0L, 0L);
    if (btnState & BST_CHECKED) {
        return TRUE;
    }
    else return FALSE;
}

BOOL App_GetPassword(char *password)
{
    /* Popup OBEX Authentication Password Dialog */
    if (DialogBoxParam(hInst, MAKEINTRESOURCE(FTX_PASSWORD), AppWnd, 
        PasswordDlgProc, (LPARAM)password) == TRUE)
        return TRUE;
    else return FALSE;
}

BOOL App_GetUserId(char *userId)
{
    /* Popup OBEX Authentication Password Dialog */
    if (DialogBoxParam(hInst, MAKEINTRESOURCE(FTX_USER_ID), AppWnd, 
        UserIdDlgProc, (LPARAM)userId) == TRUE)
        return TRUE;
    else return FALSE;
}
#endif /* OBEX_AUTHENTICATION == XA_ENABLED */

static BOOL CALLBACK AppWndProc( HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam )
{

    switch (uMsg) {
    case WM_INITDIALOG:
#if JETTEST != XA_ENABLED
        EnableWindow(GetDlgItem(hWnd, ID_SNIFFER), FALSE);
#endif /* JETTEST != XA_ENABLED */
        objectName[0] = 0;
        lastObject[0] = 0;
        break;

#if BT_STACK == XA_ENABLED
    case WM_PAINT: 
        /* Draws a separator below the Blue Manager menu. */
        {
            PAINTSTRUCT     ps; 
            RECT            rect;
            HDC             hdc; 

            hdc = BeginPaint(hWnd, &ps); 
            GetClientRect(hWnd, &rect); 
            rect.left += 2;
            rect.right -= 3;
            DrawEdge(hdc, &rect, EDGE_ETCHED, BF_TOP); 
            EndPaint(hWnd, &ps); 
        }
        break;
#endif /* BT_STACK == XA_ENABLED */

    case WM_SETFOCUS:
#if TCP_STACK == XA_ENABLED || IRDA_STACK == XA_ENABLED
        /* Disable prompting the user to accept the item, since
         * this blocks the thread and potentially can cause problems
         * for the TCP transport
         */
        EnableWindow(GetDlgItem(AppWnd, IDC_PROMPT_USER), FALSE);
#else
        SendDlgItemMessage(AppWnd, IDC_PROMPT_USER, BM_SETCHECK, BST_UNCHECKED, 0L);
#endif
#if OBEX_AUTHENTICATION != XA_ENABLED
        EnableWindow(GetDlgItem(AppWnd, IDC_AUTH), FALSE);
        EnableWindow(GetDlgItem(AppWnd, FTX_SET_PASS), FALSE);
        EnableWindow(GetDlgItem(AppWnd, FTX_SET_USER_ID), FALSE);
#endif
        break;

    case WM_COMMAND:
        switch (LOWORD(wParam)) {

        case FTX_SERVER_ABORT:
            Ftx_AbortServer();
            break;

#if OBEX_AUTHENTICATION == XA_ENABLED
        case FTX_SET_PASS:
            Ftx_SetPassword();
            break;

        case FTX_SET_USER_ID:
            Ftx_SetUserId();
            break;        
#endif 

#if JETTEST == XA_ENABLED
        case ID_SNIFFER:
            if (IsObexSnifferOpen()) {
                StopObexSniffer();
                SendMessage(GetDlgItem(AppWnd, ID_SNIFFER), WM_SETTEXT, 0, (LPARAM)"OBEX Sniffer");
            } else  {
                HWND    sniffer = StartObexSniffer(hInst, 0);
                RECT    appRect;

                SendMessage(GetDlgItem(AppWnd, ID_SNIFFER), WM_SETTEXT, 0, (LPARAM)"Stop Sniffer");
                GetWindowRect(AppWnd, &appRect);
                SetWindowPos(sniffer, 0, appRect.left, appRect.bottom, 0, 0, SWP_NOSIZE | SWP_NOZORDER | SWP_SHOWWINDOW);
            }
            break;
#endif /* JETTEST == XA_ENABLED */

        case FTX_EXIT:
        case WM_DESTROY:
            if (hWnd == AppWnd) {
                /* Exit the application */
#if BT_STACK == XA_ENABLED
                if (BMG_Shutdown() != BT_STATUS_PENDING)
#endif
                    PostQuitMessage(0);
                return TRUE;
            }
            break;

#if BT_STACK == XA_ENABLED
        default:
            BMG_TrackMainMenu(hWnd, wParam, lParam);
            break;
#endif
        }
        break;
    }
    return 0L;
}

#if OBEX_AUTHENTICATION == XA_ENABLED
static BOOL CALLBACK PasswordDlgProc( HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam )
{
    static char      *password;

    switch (uMsg) {

    case WM_INITDIALOG:
        password = (char *)lParam;

        /* Load the default/current password */
        SendDlgItemMessage(hDlg, IPP_PASSWORD, WM_SETTEXT, 0, (LPARAM)lParam);
        return TRUE;

    case WM_COMMAND:
        switch (LOWORD(wParam)) {

        case IDOK:
            SendDlgItemMessage(hDlg, IPP_PASSWORD, WM_GETTEXT, 11, (LPARAM)password);
            EndDialog(hDlg, TRUE);
            return TRUE;

        case IDCANCEL:
            EndDialog(hDlg, FALSE);
            return TRUE;
        }
    }
    return 0L;
}

static BOOL CALLBACK UserIdDlgProc( HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam )
{
    static char      *userId;

    switch (uMsg) {

    case WM_INITDIALOG:
        userId = (char *)lParam;

        /* Load the default/current password */
        SendDlgItemMessage(hDlg, IPP_USER_ID, WM_SETTEXT, 0, (LPARAM)lParam);
        return TRUE;

    case WM_COMMAND:
        switch (LOWORD(wParam)) {

        case IDOK:
            SendDlgItemMessage(hDlg, IPP_USER_ID, WM_GETTEXT, 20, (LPARAM)userId);
            EndDialog(hDlg, TRUE);
            return TRUE;

        case IDCANCEL:
            EndDialog(hDlg, FALSE);
            return TRUE;
        }
    }
    return 0L;
}
#endif /* OBEX_AUTHENTICATION == XA_ENABLED */
    
static BOOL CALLBACK AcceptObjectProc( HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam )
{
    char  buffer[256];

    switch (uMsg) {

    case WM_INITDIALOG:
        sprintf(buffer, "Accept %s Request?", AcceptReq.Oper);
        SetWindowText(hDlg, buffer);
        if (AcceptReq.Name)
            SendDlgItemMessage(hDlg, IDC_NAME, WM_SETTEXT, 0, (LPARAM)AcceptReq.Name);
        if (AcceptReq.Type)
            SendDlgItemMessage(hDlg, IDC_TYPE, WM_SETTEXT, 0, (LPARAM)AcceptReq.Type);
        return TRUE;

    case WM_COMMAND:
        switch (LOWORD(wParam)) {

        case IDOK:
            EndDialog(hDlg, TRUE);
            return TRUE;

        case IDCANCEL:
            EndDialog(hDlg, FALSE);
            return TRUE;
        }
    }
    return 0L;
}

/*
 * Exported to filexfer.c for writing a string to the UI list boxes.
 */
void App_Report(char *format,...)
{
   char     buffer[200]; /* Output Buffer */
   int      index;
   va_list  args;

   va_start(args, format);
   index = _vsnprintf(buffer, 200, format, args);
   va_end(args);

   /* Strip 'Server:' from messages */
   if (StrnCmp("Server: ", 8, buffer, 8) == TRUE) {
        index = SendDlgItemMessage(AppWnd, FTX_SERVER_OUTPUT, LB_ADDSTRING, 0, (LPARAM)buffer+8);
        if (index != LB_ERR) {
            SendDlgItemMessage(AppWnd, FTX_SERVER_OUTPUT, LB_SETCARETINDEX, (WPARAM)index, FALSE);
        }
    } else {
        index = SendDlgItemMessage(AppWnd, FTX_SERVER_OUTPUT, LB_ADDSTRING, 0, (LPARAM)buffer);
        if (index != LB_ERR) {
            SendDlgItemMessage(AppWnd, FTX_SERVER_OUTPUT, LB_SETCARETINDEX, (WPARAM)index, FALSE);
        }
    }
}

/*
 * Exported to filexfer.c for maintaining progress dialogs
 */
void App_Progress(U32 barId, U32 currPos, U32 maxPos)
{
    if (currPos == 0) {
        PostMessage(GetDlgItem(AppWnd, barId), PBM_SETRANGE32, 0, maxPos);
    }
    PostMessage(GetDlgItem(AppWnd, barId), PBM_SETPOS, currPos, 0);
}

/*
 * Exported to filexfer.c for displaying the current folder
 */
void App_UpdateUiPath()
{
    static char path[PATHMGR_MAX_PATH];

    if (_getcwd(path, PATHMGR_MAX_PATH))
        SendDlgItemMessage(AppWnd, FTX_CURRENT_FOLDER, WM_SETTEXT, 0, (LPARAM)path);
}

BOOL App_AcceptRequest(const char *Name, const char *Type, const char *Oper)
{
    U32 btnState;

    btnState = SendDlgItemMessage(AppWnd, IDC_PROMPT_USER, BM_GETSTATE, 0L, 0L);
    if ((btnState & BST_CHECKED) == 0) {
        return TRUE;
    }

    AcceptReq.Name = Name;
    AcceptReq.Type = Type;
    AcceptReq.Oper = Oper;

    return DialogBox(hInst, MAKEINTRESOURCE(FTX_ACCEPT), AppWnd, AcceptObjectProc);
}

#if BT_STACK == XA_ENABLED
static LPTSTR GpaGetToolTipText(int ControlId)
{
    switch (ControlId) {
    case FTX_SERVER_ABORT: return "Abort the current server operation";
    case FTX_SET_PASS: return "Set the OBEX Authentication Password";
    case FTX_SET_USER_ID: return "Set the OBEX Authentication UserID";
    case IDC_PROMPT_USER: return "Toggle the requirement to prompt the user to accept an object";
    case IDC_AUTH: return "Toggle the requirement for OBEX Server Authentication";
    }
    return "";
}
#endif /* BT_STACK == XA_ENABLED */


