#ifndef __BROWSERH
#define __BROWSERH
/****************************************************************************
 *
 * File:        browser.h
 *
 * Description: This file contains the interface for the folder browser.
 *
 * Created:     May 17, 2000
 *
 * $Project: XTNDAccess Blue SDK$
 *
 * Copyright 1998-2005 Extended Systems, Inc.  ALL RIGHTS RESERVED.
 *
 * Unpublished Confidential Information of Extended Systems, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of Extended Systems, Inc.
 * 
 * Use of this work is governed by a license granted by Extended Systems,
 * Inc.  This work contains confidential and proprietary information of
 * Extended Systems, Inc. which is protected by copyright, trade secret,
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/
#include <osapi.h>
#include <obexerr.h>

/*---------------------------------------------------------------------------
 *
 * Prototype:     ObexRespCode BROWSER_Open(const char *Folder)
 *
 * Description:   Initialize the folder browser to read the specified folder
 *                listing.
 *
 * Parameters:    Folder - name of folder to read.
 *
 * Returns:       OBRC_SUCCESS - Browser is ready.
 */
ObexRespCode BROWSER_Open(const char *Folder);

#if OBEX_DYNAMIC_OBJECT_SUPPORT == XA_ENABLED
/*---------------------------------------------------------------------------
 *
 * Prototype:     ObexRespCode BROWSER_ReadFlex(U8           *Buff,
 *                                              U16          *Len,
 *                                              BOOL         *More )
 *
 * Description:   Read the folder object data.
 *
 * Parameters:    Buff - pointer to location where data is read into.
 *                Len - maximum number of bytes of data to read
 *                More - callee sets to TRUE if it has more data to read.
 *
 * Returns:       OBRC_SUCCESS      - Read was successful.
 *                OBRC_UNAUTHORIZED - Read failed. 
 */
ObexRespCode BROWSER_ReadFlex(U8 *Buff, U16 *Len, BOOL *More);
#endif

/*---------------------------------------------------------------------------
 *
 * Prototype:     U32 BROWSER_GetObjectLen(void)
 *
 * Description:   Get the length of an folder listing object.
 *
 * Parameters:    Obm - pointer to the object manager entry.
 *
 * Returns:       The object length
 */
U32 BROWSER_GetObjectLen(void);

#define BROWSER_GetObjectLen()  UNKNOWN_OBJECT_LENGTH

#endif /* __BROWSERH */
