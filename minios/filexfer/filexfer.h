#ifndef __FILEXFER_H
#define __FILEXFER_H
/****************************************************************************
 *
 * File:          filexfer.h
 *
 * Description:   This file specifies defines and function prototypes for the
 *                QBeam application.
 * 
 * Created:       May 17, 2000
 *
 * $Project:XTNDAccess IrDA SDK$
 *
 * Copyright 1999-2005 Extended Systems, Inc.  ALL RIGHTS RESERVED.
 *
 * Unpublished Confidential Information of Extended Systems, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of Extended Systems, Inc.
 * 
 * Use of this work is governed by a license granted by Extended Systems,
 * Inc.  This work contains confidential and proprietary information of
 * Extended Systems, Inc. which is protected by copyright, trade secret,
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#define AUTH_RECEIVED_CHAL  0x02
#define AUTH_AUTHENTICATED  0x04

 /****************************************************************************
 *
 * FileXfer Exported Function prototypes
 *
 ***************************************************************************/
extern BOOL Ftx_Init(void);
extern void Ftx_Deinit(void);
extern void Ftx_AbortServer(void);
extern void Ftx_SetPassword(void);
extern void Ftx_SetUserId(void);

/****************************************************************************
 *
 * FileXfer Internal Function prototypes
 *
 ***************************************************************************/
static void  FtxServerCallback(GoepServerEvent *Event);

static void  ServerObjectRequest(GoepServerEvent *Event);
static void  ServerDoAuthentication(GoepServerEvent *Event);

static const char *GoepOpName(GoepOperation Op);
static const char *pObStatus(ObStatus status);
static const char *ServerOpName(void);

U16      StrLen(const char *String);

#endif /* __QBEAM_H */

