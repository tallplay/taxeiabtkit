#ifndef __OBMGRH
#define __OBMGRH

/****************************************************************************
 *
 * File:          obmgr.h
 *
 * Description:   This file specifies the OBEX Object Manager interface.
 * 
 * Created:       May 15, 1997
 *
 * $Project: XTNDAccess Blue SDK$
 *
 * Copyright 1997-2005 Extended Systems, Inc.  ALL RIGHTS RESERVED.
 *
 * Unpublished Confidential Information of Extended Systems, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of Extended Systems, Inc.
 * 
 * Use of this work is governed by a license granted by Extended Systems,
 * Inc.  This work contains confidential and proprietary information of
 * Extended Systems, Inc. which is protected by copyright, trade secret,
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include <osapi.h>
#include <obexerr.h>

#include "fstore.h"
#include "pathmgr.h"
#include "browser.h"

/*---------------------------------------------------------------------------
 *
 * ObStoreHandle Structure
 */
//typedef struct _ObStoreMgr *ObStoreHandle;
//#define ObStoreHandle ObStoreMgr *


#define OBM_NUM_ENTRIES     2
#define OBM_MAX_TYPE_LEN    32

#define OBMF_INUSE          0x01   /* Entry is inuse */
#define OBMF_UNICODE_ODD    0x02   /* AppendNameUni() position flag */
#define OBMF_DELETE         0x03   /* Delete object on close */

typedef struct _ObStoreMgr {
    U8          flags;
    U8          store;
    U16         nameLen;
    char        name[FILENAME_MAX];
    U8          typeLen;
    char        type[OBM_MAX_TYPE_LEN];
    U32         objectLen;
    U32         amount;
    U32         uiContext;
    union {
        ObStoreHandle file;
    } handle;
} ObStoreMgr;

/* Open, Create, Close, Read, Write, Readflex, GetObjectLen */

typedef struct _ObStoreType {
    const char     *name;
    U8              obmt;
} ObStore;

/* Object Store Type */
#define OBMT_UNSPECIFIED    0
#define OBMT_GENERIC        1
#define OBMT_BROWSER        2
#define OBMT_VSTORE         3

#if OBEX_DYNAMIC_OBJECT_SUPPORT == XA_ENABLED
/*---------------------------------------------------------------------------
 *
 * Constant indicates that the object store item has an unknown length
 */
#define UNKNOWN_OBJECT_LENGTH   0xFFFFFFFF
#endif
 
/****************************************************************************
 *
 * Function Reference 
 *
 ****************************************************************************/
/*---------------------------------------------------------------------------
 *
 * Prototype:     ObexRespCode OBM_Create(ObStoreMgr *Obm)
 *
 * Description:   Open the specified object store item for writing. An
 *                appropriate object store is determined automatically or
 *                it can be overridden by specifying Obm->store before
 *                making this call.
 *
 * Parameters:    Obm - pointer to the object manager entry.
 *
 * Returns:       OBRC_SUCCESS - Operation was successful.
 *                OBRC_NOT_FOUND - Could not create object store item.
 */
ObexRespCode OBM_Create(ObStoreMgr *obm);

/*---------------------------------------------------------------------------
 *
 * Prototype:     ObexRespCode OBM_Open(ObStoreMgr *Obm)
 *
 * Description:   Open an object manager object for reading. An appropriate
 *                object store is determined automatically or it can be
 *                overridden by specifying Obm->store before making this call.
 *
 * Parameters:    Obm - pointer to the object manager entry.
 *
 * Returns:       OBRC_SUCCESS indicates that the open is successful. 
 *                OBRC_NOT_FOUND indicates failure. 
 */
ObexRespCode OBM_Open(ObStoreMgr *obm);

/*---------------------------------------------------------------------------
 *
 * Prototype:     ObexRespCode OBM_Close(ObStoreMgr **ObmPtr)
 *
 * Description:   Close an object manager item.
 *
 * Parameters:    ObmPtr - pointer to the object manager entry.
 *
 * Returns:       OBRC_SUCCESS - Operation was successful.
 *                OBRC <other> - Operation failed.
 */
ObexRespCode OBM_Close(ObStoreMgr **ObmPtr);

/*---------------------------------------------------------------------------
 *
 * Prototype:     ObexRespCode OBM_Delete(ObStoreMgr **ObmPtr)
 *
 * Description:   Delete the object and free the storage.
 *
 * Parameters:    ObmPtr - Handle to object manager entry to delete. 
 *
 * Returns:       OBRC_NOT_FOUND - Object could not be deleted.
 *                OBRC_SUCCESS   - Object was deleted.
 */
ObexRespCode OBM_Delete(ObStoreMgr **ObmPtr);
 
/*---------------------------------------------------------------------------
 *
 * Prototype:     U32 OBM_GetObjectLen(ObStoreMgr *Obm)
 *
 * Description:   Get the length of an object manager item.
 *                This function is REQUIRED by the OBEX Protocol.
 *
 * Parameters:    Obm - pointer to the object manager entry.
 *
 * Returns:       The object length
 */
U32 OBM_GetObjectLen(ObStoreMgr *Obm);

/*---------------------------------------------------------------------------
 *
 * Prototype:     ObStoreMgr *OBM_New()
 *
 * Description:   Get a handle to a new object manager record.
 *
 * Parameters:    none 
 *
 * Returns:       Handle of an object manager record. 0 means out of objects. 
 */
ObStoreMgr *OBM_New(void);

/*---------------------------------------------------------------------------
 *
 * Prototype:     ObexRespCode OBM_Read(ObStoreMgr   *Obm, 
 *                                      U8           *Buff,
 *                                      U16           Len )
 *
 * Description:   Read data from the object manager entry.
 *                This function is REQUIRED by the OBEX Protocol.
 *
 * Parameters:    Obm - handle of object manager entry.
 *                Buff - pointer to location where data is read into.
 *                Len - length in bytes of data to read
 *
 * Returns:       OBRC_SUCCESS      - Read was successful.
 *                OBRC_UNAUTHORIZED - Read failed. 
 */
ObexRespCode OBM_Read(ObStoreMgr *Obm, U8 *Buff, U16 Len);

#if OBEX_DYNAMIC_OBJECT_SUPPORT == XA_ENABLED
/*---------------------------------------------------------------------------
 *
 * Prototype:     ObexRespCode OBM_ReadFlex(ObStoreMgr   *Obm, 
 *                                          U8           *Buff,
 *                                          U16          *Len,
 *                                          BOOL         *More )
 *
 * Description:   Read data from the object manager entry.
 *                This function is REQUIRED by the OBEX Protocol if
 *                OBEX_DYNAMIC_OBJECT_SUPPORT is enabled.
 *
 * Parameters:    Obm - handle of object manager entry.
 *                Buff - pointer to location where data is read into.
 *                Len - maximum number of bytes of data to read
 *                More - callee sets to TRUE if it has more data to read.
 *
 * Returns:       OBRC_SUCCESS      - Read was successful.
 *                OBRC_UNAUTHORIZED - Read failed. 
 */
ObexRespCode OBM_ReadFlex(ObStoreMgr *Obm, U8 *Buff, U16 *Len, BOOL *More);
#endif

/*---------------------------------------------------------------------------
 *
 * Prototype:     ObexRespCode OBM_Write(ObStoreMgr *Obm, 
 *                                       U8         *Buff,
 *                                       U16         Len )
 *
 * Description:   Write data to the object managre entry.
 *                This function is REQUIRED by the OBEX Protocol.
 *
 * Parameters:    Obm - handle of object manager entry.
 *                Buff - pointer to data to write
 *                Len - length in bytes of data to write
 *
 * Returns:       OBRC_SUCCESS      - Write was successful.
 *                OBRC_UNAUTHORIZED - Write failed. 
 */
ObexRespCode OBM_Write(ObStoreMgr *Obm, U8 *Buff, U16 Len);

/*---------------------------------------------------------------------------
 *
 * Prototype:     ObexRespCode OBM_AppendNameUni( ObStoreMgr  Obm, 
 *                                                const U8   *Name, 
 *                                                U16         Len )
 *
 * Description:   Append the unicode string to the objects name. A state
 *                machine is used so the unicode string does not have to
 *                be an even number of bytes.
 *
 * Parameters:    Obm - handle of object manager entry.
 *                Name - pointer to Unicode string to append
 *                Len - length in bytes of string to append
 *
 * Returns:       OBRC_SUCCESS      - Appended Name successfully.
 *                OBRC_UNAUTHORIZED - Unicode Name did not fit in buffer.
 */
ObexRespCode OBM_AppendNameUni(ObStoreMgr *Obm, const U8 *Name, U16 Len);

#define OBM_AppendNameAscii(_OBM, _NAME, _LEN)     \
                    OS_MemCopy((_OBM)->name, (_NAME), (_LEN))

#define OBM_GetNameAscii(_OBM)  ((_OBM)->name)

/*---------------------------------------------------------------------------
 *
 * Prototype:     BOOL OBM_AppendType(ObStoreMgr    Obm, 
 *                                    const char   *Type,
 *                                    U16           Len )
 *
 * Description:   Append the ASCII string to the object type. 
 *
 * Parameters:    Obm - handle of object manager entry.
 *                Type- pointer to ASCII string to append
 *                Len - length in bytes of string to append
 *
 * Returns:       TRUE  - String was successfully appended
 *                FALSE - Insufficient buffer space to append string.
 */
BOOL OBM_AppendType(ObStoreMgr *Obm, const char *Type, U16 Len);

#define OBM_GetTypeLen(_OBM)    ((_OBM)->typeLen)

#define OBM_GetTypeAscii(_OBM)  ((_OBM)->type)

/*---------------------------------------------------------------------------
 *
 * Prototype:     void OBM_SetObjectLen(ObStoreMgr *Obm, U32 Len)
 *
 * Description:   Set the length of a created object manager object.
 *
 * Parameters:    Obm - handle of object manager entry.
 *                Len - length of the object.
 *
 * Returns:       void
 */
#define OBM_SetObjectLen(Obm, Len) ((Obm)->objectLen = (Len))

/*---------------------------------------------------------------------------
 *
 * Prototype:     void OBM_SetUiContext(ObStoreMgr *Obm, U32 Context)
 *
 * Description:   Set the UI context handle for the object. This context
 *                handle is passed back to to the UI to so it can update the
 *                progress as an object is transferred.
 *
 * Parameters:    Obm - handle of object manager entry.
 *                Context - handle of UI object
 *
 * Returns:       void
 */
void OBM_SetUIContext(ObStoreMgr Obm, U32 UIContext);
#define OBM_SetUIContext(_OBS, _UIC)  (_OBS)->uiContext = (_UIC)

/*---------------------------------------------------------------------------
 *
 * Prototype:     U16 OBM_Ascii2Unicode(U8 *Unicode, const char *Ascii)
 *
 * Description:   Convert the ASCII string to a UNICODE string. May be
 *                called with a null destination string. In which case
 *                the function returns the necessary size for the Unicode
 *                destination string.
 *
 * Parameters:    Unicode - destination string buffer (null-terminated)
 *                Ascii   - source string buffer (null-terminated)
 *
 * Returns:       length of result UNICODE string (including terminator)
 */
U16 OBM_AsciiToUnicode(U8 *Unicode, const char *Ascii);

/*---------------------------------------------------------------------------
 *
 * Prototype:     BOOL OBM_Init()
 *
 * Description:   Initialize the object manager.
 *
 * Parameters:    none 
 *
 * Returns:       TRUE - object manager was initialized successfully. 
 *                FALSE - Unable to init.
 */
BOOL OBM_Init(void);

/*---------------------------------------------------------------------------
 *
 * Prototype:     void OBM_Deinit()
 *
 * Description:   Deinitialize the object store manager.
 *
 * Parameters:    none 
 *
 * Returns:       void
 */
void OBM_Deinit(void);
#define OBM_Deinit()

#endif
