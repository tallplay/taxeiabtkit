//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by TaxeiaStackTester.rc
//
#define IDD_TAXEIASTACKTESTER_DIALOG    102
#define IDR_MAINFRAME                   128
#define IDD_PIN_DIALOG                  132
#define IDC_BUTTON_STACKSWITCH          1000
#define IDC_EDIT_CMDSTATUS              1001
#define IDC_BUTTON_CMD_HCI_RESET        1002
#define IDC_BUTTON_CMD_SEND             1002
#define IDC_BUTTON_CMD_WRITE_SCAN_ENABLE 1004
#define IDC_BUTTON_CMD_WRITE_SCAN_ENABLE2 1005
#define IDC_BUTTON_CMD_WRITE_COD        1005
#define IDC_COMBO_SCAN_ENABLE           1006
#define IDC_STATIC_SCAN_ENABLE          1007
#define IDC_STATIC_COD                  1008
#define IDC_EDIT_COD                    1009
#define IDC_EDIT_STACK_STATUS           1010
#define IDC_EDIT_LOCAL_STATUS           1011
#define IDC_BUTTON1                     1012
#define IDC_BUTTON_CMD_WRITE_AUTH_ENABLE 1013
#define IDC_RADIO_AUTH_ENABLE           1014
#define IDC_BUTTON_UART_TEST            1017
#define IDC_BUTTON_UART_TEST_RX         1018
#define IDC_BUTTON2                     1019
#define IDC_BUTTON_ENSSP                1019
#define IDC_EDIT_PIN                    1020
#define IDC_LED1                        1022
#define IDC_LED2                        1023
#define IDC_LED3                        1024
#define IDC_LED4                        1025

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        133
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1023
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
