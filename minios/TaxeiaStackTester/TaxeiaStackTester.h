// TaxeiaStackTester.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"// main symbols

#define NEW_SDK

extern "C" {
#include "CORE\BTCore.h"
#include "GAP\LOCAL_DEV\Bt__LocalDevice.h"
#include "TaxeiaHciLog\TaxeiaHCILog.h"
#ifdef NEW_SDK
#include "sdkapi\bt_hid_hfp_api.h"
#else
#include "sdkapi\bt_hid_api.h"
#endif
#include "pad_data\pad-data.h"
unsigned int MassUart_GetUniqueID(void);
DWORD AssignBDAddr(void);
void AssignLinkKeyFileName(char *name);
}

// CTaxeiaStackTesterApp:
// See TaxeiaStackTester.cpp for the implementation of this class
//

class CTaxeiaStackTesterApp : public CWinApp
{
public:
	CTaxeiaStackTesterApp();

// Overrides
	public:
	virtual BOOL InitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
};

extern CTaxeiaStackTesterApp theApp;
