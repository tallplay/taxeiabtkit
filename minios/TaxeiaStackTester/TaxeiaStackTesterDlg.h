// TaxeiaStackTesterDlg.h : header file
//

#pragma once
#include "afxwin.h"
#include "ColorButton.h"


// CTaxeiaStackTesterDlg dialog
class CTaxeiaStackTesterDlg : public CDialog
{
// Construction
public:
    CTaxeiaStackTesterDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
    enum { IDD = IDD_TAXEIASTACKTESTER_DIALOG };

protected:
    virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
    HICON m_hIcon;

    // Generated message map functions
    virtual BOOL OnInitDialog();
    afx_msg void OnPaint();
    virtual afx_msg void OnOK();
    afx_msg HCURSOR OnQueryDragIcon();
    DECLARE_MESSAGE_MAP()
public:
    void SetDlgText(U16 staticid, const char *format, ...);
    afx_msg void OnTimer(UINT_PTR nIDEvent);

    virtual BOOL PreTranslateMessage(MSG* pMsg);
    virtual LRESULT DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam);

    CColorButton m_led[4];

};
