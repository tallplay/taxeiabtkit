// ManufactureDemoApp.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols

extern "C" {
#include "CORE\BTCore.h"
#include "TaxeiaHciLog\TaxeiaHCILog.h"
#include "GAP\SECURITY\Bt__SecurityManager.h"
#include "GAP\LOCAL_DEV\Bt__LocalDevice.h"
#include "GAP\LL\Bt__RemoteDevice.h"
unsigned int MassUart_GetUniqueID(void);
DWORD AssignBDAddr(void);
void AssignLinkKeyFileName(char *name);
}


// CManufactureDemoAppApp:
// See ManufactureDemoApp.cpp for the implementation of this class
//

class CManufactureDemoAppApp : public CWinApp
{
public:
	CManufactureDemoAppApp();

// Overrides
	public:
	virtual BOOL InitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
};

extern CManufactureDemoAppApp theApp;
