//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by ManufactureDemoApp.rc
//
#define IDD_MANUFACTUREDEMOAPP_DIALOG   102
#define IDR_MAINFRAME                   128
#define IDD_DIALOG_VDMESSAGE            130
#define IDC_BUTTON_START_STOP           1001
#define IDC_EDIT_TEST_STATE             1002
#define IDC_EDIT_BT_STATE2              1003
#define IDC_EDIT_BT_STATE               1003
#define IDC_EDIT_TEST_RESULT            1004
#define IDC_EDIT_MAC                    1005
#define IDC_EDIT1                       1007
#define IDC_EDIT_ATE_ID                 1007
#define IDC_EDIT_STATUS                 1008
#define IDC_BUTTON_ADD                  1009
#define IDC_BUTTON_SUB                  1010
#define IDC_BUTTON_LOW                  1011
#define IDC_EDIT_RESULT                 1012
#define IDC_BUTTON_MIDDLE               1012
#define IDC_EDIT_ACTION                 1013
#define IDC_BUTTON_HIGH                 1013
#define IDC_BUTTON_PADD                 1014
#define IDC_BUTTON_PSUB                 1015
#define IDC_BUTTON_HIGH2                1016
#define IDC_BUTTON_SINGLE               1016

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        136
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1012
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
