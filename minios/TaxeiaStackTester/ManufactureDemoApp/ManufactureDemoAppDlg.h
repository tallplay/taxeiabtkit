// ManufactureDemoAppDlg.h : header file
//

#pragma once
#include "afxwin.h"

#include "ColorEdit.h"

// CManufactureDemoAppDlg dialog
class CManufactureDemoAppDlg : public CDialog
{
// Construction
public:
	CManufactureDemoAppDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_MANUFACTUREDEMOAPP_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support

    void UpdateStatus(void);
    void ControlCenter(void);
// Implementation
protected:
    HICON m_hIcon;

    // Generated message map functions
    virtual BOOL OnInitDialog();
    afx_msg void OnPaint();	
    afx_msg BOOL OnDeviceChange(UINT nEventType, DWORD_PTR dwData);
	LRESULT	OnHotKey(WPARAM wParam, LPARAM lParam);
    afx_msg HCURSOR OnQueryDragIcon();
    DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnBnClickedButtonStartStop();
	CEdit m_control_ate_id;
	afx_msg void OnBnClickedCancel();
	CColorEdit m_control_status;
	afx_msg void OnBnClickedButtonAdd();
	afx_msg void OnBnClickedButtonSub();
	afx_msg void OnBnClickedButtonLow();
	afx_msg void OnBnClickedButtonMiddle();
	afx_msg void OnBnClickedButtonHigh();
	afx_msg void OnBnClickedButtonPsub();
	afx_msg void OnBnClickedButtonPadd();	
};
