// ManufactureDemoAppDlg.cpp : implementation file
//
// MT6622_PATCH;


#include "stdafx.h"
#include "ManufactureDemoApp.h"
#include "ManufactureDemoAppDlg.h"
#include "DlgVDMessage.h"

#include "KeyProWriter.h"
#include "rijndael-api.h"

#include <dbt.h>
#include "sdkapi/bt_hid_hfp_api.h"

#if defined(__cplusplus)
extern "C" {
#endif
int MassUart_Execute(int bulkinout, int *transferlength, char *cdb, char *databuf);
#if defined(__cplusplus)
}
#endif 

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define m_nHotKeyID 100

#define IDT_TIMEOUT	   9999
#define FIND_ATE_COUNT 5
int		find_ate;

#define TEST_PATTERN_LEN 62
U8 test_pattern[TEST_PATTERN_LEN]=
{
'0','1','2','3','4','5','6','7',
'8','9','a','b','c','d','e','f',
'g','h','i','j','k','l','m','n',
'o','p','q','r','s','t','u','v',
'w','x','y','z','A','B','C','D',
'E','F','G','H','I','J','K','L',
'M','N','O','P','Q','R','S','T',
'U','V','W','X','Y','Z'
};

U8 fail_pattern[TEST_PATTERN_LEN]=
{
'x','x','x','x','x','x','x','x',
'x','x','x','x','x','x','x','x',
'x','x','x','x','x','x','x','x',
'x','x','x','x','x','x','x','x',
'x','x','x','x','x','x','x','x',
'x','x','x','x','x','x','x','x',
'x','x','x','x','x','x','x','x',
'x','x','x','x','x','x',
};

const U8 secret_key[16] = {0x20, 0x11, 0x11, 0x18, 0x17, 0x23, 0x19, 0x76, 0x01, 0x08, 0x00, 0x00, 0x19, 0x75, 0x12, 0x23};
const U8 secret_tag[13] = {'T', 'a', 'x', 'e', 'i', 'a', ' ', 'B', 'T', ' ', 'K', 'i', 't'};

U8 activate_data[32];
U8 encrypted_data[32];


//#define CHECK_MASS_PRODUCTION_KEY
#define KEYPRO_KEY "PS3BTKIT"

CDlgVDMessage       *dlg;

// CManufactureDemoAppDlg dialog

CManufactureDemoAppDlg::CManufactureDemoAppDlg(CWnd *pParent /*=NULL*/) :
    CDialog(CManufactureDemoAppDlg::IDD, pParent)
{
    m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void
CManufactureDemoAppDlg::DoDataExchange(CDataExchange *pDX) {
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_ATE_ID, m_control_ate_id);
	DDX_Control(pDX, IDC_EDIT_STATUS, m_control_status);
}

BEGIN_MESSAGE_MAP(CManufactureDemoAppDlg, CDialog)
        ON_WM_PAINT()
        ON_WM_QUERYDRAGICON()
    ON_WM_DEVICECHANGE()        
        //}}AFX_MSG_MAP
		ON_MESSAGE(WM_HOTKEY,OnHotKey)
        ON_WM_TIMER()
        ON_BN_CLICKED(IDC_BUTTON_START_STOP, &CManufactureDemoAppDlg::OnBnClickedButtonStartStop)
		ON_BN_CLICKED(IDCANCEL, &CManufactureDemoAppDlg::OnBnClickedCancel)
		ON_BN_CLICKED(IDC_BUTTON_ADD, &CManufactureDemoAppDlg::OnBnClickedButtonAdd)
		ON_BN_CLICKED(IDC_BUTTON_SUB, &CManufactureDemoAppDlg::OnBnClickedButtonSub)
		ON_BN_CLICKED(IDC_BUTTON_LOW, &CManufactureDemoAppDlg::OnBnClickedButtonLow)
		ON_BN_CLICKED(IDC_BUTTON_MIDDLE, &CManufactureDemoAppDlg::OnBnClickedButtonMiddle)
		ON_BN_CLICKED(IDC_BUTTON_HIGH, &CManufactureDemoAppDlg::OnBnClickedButtonHigh)
		ON_BN_CLICKED(IDC_BUTTON_PSUB, &CManufactureDemoAppDlg::OnBnClickedButtonPsub)
		ON_BN_CLICKED(IDC_BUTTON_PADD, &CManufactureDemoAppDlg::OnBnClickedButtonPadd)	
END_MESSAGE_MAP()


// CManufactureDemoAppDlg message handlers
CManufactureDemoAppDlg  *manuDlg                = NULL;

U8                test_identifiler        = 1;

U8 RunBtThread(BYTE execute);

U8 bIsRfTesting;
U8 Bt_Test_Freq;
U8 Bt_Test_Power;
U8 Bt_Is_SingleTone;
U16 Bt_Test_Len;

extern "C" {
    void ate_data_cb(U16 len, U8 *dat);
    void dut_data_cb(U16 len, U8 *dat);
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void
CManufactureDemoAppDlg::OnPaint() {
    if (IsIconic()) {
        CPaintDC        dc      (this); // device context for painting

        SendMessage(WM_ICONERASEBKGND,
                    reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

        // Center icon in client rectangle
        int     cxIcon  = GetSystemMetrics(SM_CXICON);
        int     cyIcon  = GetSystemMetrics(SM_CYICON);
        CRect   rect;
        GetClientRect(&rect);
        int     x       = (rect.Width() - cxIcon + 1) / 2;
        int     y       = (rect.Height() - cyIcon + 1) / 2;

        // Draw the icon
        dc.DrawIcon(x, y, m_hIcon);
    } else {
        CDialog::OnPaint();
    }
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR
CManufactureDemoAppDlg::OnQueryDragIcon() {
    return static_cast<HCURSOR>(m_hIcon);
}

void
AssignBDName(char *name) {
    //BD name
    sprintf((char *) name, "ManufactureDongle");
}

#define INQ_STATE_START 1
#define INQ_STATE_DEVICE_FOUND 3
#define INQ_STATE_END 2
#define INQ_STATE_END_FOUND 4

U8      RemoteAddr[6];
U8      InqState        = INQ_STATE_END;
U8      TestStart       = 0;

#ifdef ATE
#define ATE_APP_STATE_IDLE 0
#define ATE_APP_STATE_START 1
#define ATE_APP_STATE_END 2
#define ATE_APP_STATE_END_RESULT 3

U8      ate_test_result = 0xff;
U8      ate_app_state   = ATE_APP_STATE_IDLE;
U8      ate_allow_send  = 1;
#else
#define DUT_APP_STATE_IDLE 0
#define DUT_APP_STATE_START 1
#define DUT_APP_STATE_END 2
U8      dut_app_state   = DUT_APP_STATE_IDLE;
U8      dut_test_result = 0xff;
#endif


HWND    hwnd;
BOOL
CManufactureDemoAppDlg::OnInitDialog() {
        USES_CONVERSION;
    CDialog::OnInitDialog();

    // Set the icon for this dialog.  The framework does this automatically
    //  when the application's main window is not a dialog
    SetIcon(m_hIcon, TRUE);                     // Set big icon
    SetIcon(m_hIcon, FALSE);            // Set small icon

    // TODO: Add extra initialization here

    SetWindowPos(&wndTopMost, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
    CenterWindow();

#ifdef TAXEIA_UI_LOG
    hwnd = StartHciLog(AfxGetInstanceHandle(), SW_HIDE);
#endif

    //char name[20];
#ifdef ATE
    //sprintf(name, "ATE %d demo", TEST_IDENTIFIER);
    //this->SetWindowText(name);
#ifdef RF_TEST
	
	//this->SetWindowText("RF Tester - Modulation");
	this->SetWindowText("RF Tester - Hopping");
	//GetDlgItem(IDC_BUTTON_ADD)->ShowWindow(SW_SHOW);
	//GetDlgItem(IDC_BUTTON_SUB)->ShowWindow(SW_SHOW);
	GetDlgItem(IDC_BUTTON_PADD)->ShowWindow(SW_SHOW);
	GetDlgItem(IDC_BUTTON_PSUB)->ShowWindow(SW_SHOW);
	//GetDlgItem(IDC_BUTTON_LOW)->ShowWindow(SW_SHOW);
	//GetDlgItem(IDC_BUTTON_HIGH)->ShowWindow(SW_SHOW);
	//GetDlgItem(IDC_BUTTON_MIDDLE)->ShowWindow(SW_SHOW);


	//this->SetWindowText("RF Tester - 2480 Single Tone");
#else
    this->SetWindowText("ATE Tester");
#endif

#else //DUT
    //sprintf(name, "DUT %d demo", TEST_IDENTIFIER);
    //this->SetWindowText(name);
    this->SetWindowText("DUT Tester");
#endif

	m_control_ate_id.SetWindowText("1");
	GetDlgItem(IDC_EDIT_MAC)->SetWindowText("00:00:00:00:00:00");

    manuDlg = this;
    bt_init();
#ifdef ATE
    bt_ate_register_callback(&ate_data_cb);
#else
    bt_dut_register_callback(&dut_data_cb);
#endif
    SetTimer(0x1723, 1000, 0);

	dlg = new CDlgVDMessage;
    dlg->Create(IDD_DIALOG_VDMESSAGE, this);    
    dlg->ShowWindow(SW_HIDE);

#ifdef ATE
	bIsRfTesting = 0;
	Bt_Test_Freq = 0x00;
	Bt_Test_Power = 0x05;
	m_control_status.SetTextColor(GREEN);
	m_control_status.SetWindowText("");	
	Bt_Is_SingleTone = 0;
	Bt_Test_Len = 0x0FF3;

#ifdef PS3_ACTIVATE
	test_identifiler = 49;
	GetDlgItem(IDC_EDIT_ATE_ID)->SetWindowText("49");
	GetDlgItem(IDC_EDIT_ATE_ID)->EnableWindow(FALSE);

	this->SetWindowText("Activator");

#ifdef CHECK_MASS_PRODUCTION_KEY
	BOOL		r_value;
	int			n_device;

	n_device = FindKey();
	if(n_device == 0){
		MessageBox("Activation Key Not Found!!");
		EndDialog(0);
		return TRUE;
	}

	r_value = OpenKey();
	if(r_value){
		MessageBox("Activation Key Not Found!!");
		EndDialog(0);
		return TRUE;
	}

	r_value = ValideateKey();
	switch (r_value){
		case TESTRESULT_VALID_KEY:
			break;
		
	    case TESTRESULT_GETUID_FAIL:
		   //"Fail!! Get UID Fail!!";			
        case TESTRESULT_AUTH_FAIL:
		    //"Fail!! Authenticate Fail!!";
        case TESTRESULT_ENC_TEST_FAIL:
    		//"Fail!! Encrypt Fail!!";
        case TESTRESULT_DEC_TEST_FAIL:
    		//"Fail!! Descrypt Fail!!";
        case TESTRESULT_INVALID_KEY:
        	//"InValid Key Pro!!";
		default:
			MessageBox("Activation Key Invalid(-1)!!");
			EndDialog(0);
			return TRUE;		
	}

	unsigned char pid[9];
	unsigned char kid[9];

	memset(pid, 0x00, sizeof(pid));
	memset(kid, 0x00, sizeof(kid));

	GetKeyProductIDs(pid, kid);

	if(memcmp(pid, KEYPRO_KEY, 8)){
		MessageBox("Activation Key Invalid(-2)!!");
		EndDialog(0);
		return TRUE;	
	} 

	int nLeft = TestKeyTimes();
	char buf[MAX_PATH];

	sprintf(buf, "Activation Cnt:%d", nLeft);
	m_control_status.SetWindowText(buf);
#endif

#endif

#endif

    if (RunBtThread(2)) {
        RunBtThread(0);

#ifdef ATE
#else
		dlg->ShowWindow(SW_SHOW);	
		dlg->SetResult(TEXT("[wait]"));    		
		dlg->InvalidateRect(NULL, FALSE);
		dlg->UpdateWindow();      
		dut_test_result = 0;
#endif
    }

	RegisterHotKey(this->GetSafeHwnd(), m_nHotKeyID + 0, MOD_CONTROL|MOD_ALT, 'H');

	m_control_status.SetBigFont();



    return TRUE;  // return TRUE  unless you set the focus to a control
}

void
InquiryCb(U8 ret, U8 *dat) {
    if (ret == 1) //command fail!!
    {
        InqState = INQ_STATE_END;
    } else if (ret == 0) //timeout or maximun result
    {
        if (InqState == INQ_STATE_DEVICE_FOUND) {
            InqState = INQ_STATE_END_FOUND;
            Bt__ConnRst(RemoteAddr, 0);
        } else {
            InqState = INQ_STATE_END;
        }
    } else  //found, result
    {
        memcpy(RemoteAddr, dat + 1, 6);
        InqState = INQ_STATE_DEVICE_FOUND;
    }
}

void
GapCallBack(U16 parm1, void *parm2) {
    switch (parm1) {
      case 0:
        //initilized
        //update scan mode
        break;
#if 0                   
            case 1: //scan change
                TheDlg->ScanMode = (U8) parm2;
                        TheDlg->SendMessage(WM_UPDATE_DATA, 0, 0);
                break;
#endif
      case 2:
        //get name
        AssignBDName((char *) parm2);
        break;
    }
}

U8
RunBtThread(BYTE execute) {
    static HANDLE       OSThread        = (HANDLE) - 1;

    if (execute == 0) //Start Thread
    {
        if (OSThread != (HANDLE) - 1) //already runing
        {
            return 2;
        }

        DWORD   dwThreadID;
#ifdef ATE
#ifdef RF_TEST
		bt_start(GetTickCount(), BT_WMODE_FREQ_TEST | test_identifiler, 0);
#else
        bt_start(GetTickCount(), BT_WMODE_ATE | test_identifiler, 0);
		ate_allow_send = 1;
#endif
#else
        bt_start(GetTickCount(), BT_WMODE_NO_SCAN, 0);
#endif
        OSThread = CreateThread((LPSECURITY_ATTRIBUTES) NULL, 0,
                                (LPTHREAD_START_ROUTINE) RunBtThread,
                                (LPVOID) 3, 0, &dwThreadID);

        return 0;
    } else if (execute == 1) //Stop Thread
    {
        if (OSThread != (HANDLE) - 1) {
            bt_stop();
            TerminateThread(OSThread, 0);
            CloseHandle(OSThread);
            OSThread = (HANDLE) - 1;                    
            return 0;
        }
        return 1;
    } else if (execute == 2) //test if device unplug
    {
        return BTCoreHwHealth();
    }

    /////Thread Body/////////////////////
    while (1) {
        if (!bt_task(GetTickCount())) {
            Sleep(4);
        }
    }
    return 0;
}


void
AssignLinkKeyFileName(char *name) {
#ifdef ATE
#ifdef PS3_ACTIVATE
	char dirtmp[MAX_PATH];
	::GetEnvironmentVariable("TEMP", dirtmp, MAX_PATH);
	sprintf(name, "%s\\ATELink", dirtmp);
#else
    strcpy(name, "ATELink");
#endif
#else
    strcpy(name, "DUTLink");
#endif
}

DWORD
AssignBDAddr(void) {
    srand(GetTickCount());

    return rand();
}


void
CManufactureDemoAppDlg::OnTimer(UINT_PTR nIDEvent) {
    // TODO: Add your message handler code here and/or call default
    if (nIDEvent == 0x1118) //inqury check
    {
        if (InqState == INQ_STATE_END) {
            Bt__Inquiry(0x33, InquiryCb);
            InqState = INQ_STATE_START;
        }
    } else if (nIDEvent == 0x1723) //update status
    {
        ControlCenter();
        UpdateStatus();
    } 

#ifdef DUT
	else if (nIDEvent == IDT_TIMEOUT){
		KillTimer(IDT_TIMEOUT);
		RunBtThread(1);

		dlg->SetAction("Timeout.");		
		dlg->SetResult(TEXT("[FAIL]"));
		m_control_status.SetTextColor(RED);
		m_control_status.SetWindowText("FAIL!!");		
	}
#endif
	
#ifdef PS3_ACTIVATE
	else if (nIDEvent == IDT_TIMEOUT){
		KillTimer(IDT_TIMEOUT);		

		dlg->SetAction("Timeout.");		
		dlg->SetResult(TEXT("[fail]"));

		ate_app_state = ATE_APP_STATE_END;
		
	}
#endif


    //CDialog::OnTimer(nIDEvent);
}


LRESULT
CManufactureDemoAppDlg::OnHotKey(WPARAM wParam, LPARAM lParam) {    
	U8          show;

    if (::IsWindowVisible(hwnd)) {
        show = SW_HIDE;
    } else {
        show = SW_SHOW;
    }

    // TODO: Add your control notification handler code here
    //StartHciLog(AfxGetInstanceHandle(), show);
    ::ShowWindow(hwnd, show);

    return 0;
}



char    bt_stk_str[][30]        = {
    "BT_STA_STOP", "BT_STA_POWERUP", "BT_STA_BOOT", "BT_STA_INITILIZED",
    "BT_STA_PAIRING", "BT_STA_CONNECTING", "BT_STA_CONNECTED",
    "BT_STA_HID_CONNECTED", "BT_STA_HFP_CONNECTED",
    "BT_STA_HID_HFP_CONNECTED", "BT_STA_DISCONNECTING", "BT_STA_DISCONNECTED",
    "BT_STA_BT_FAIL", "BT_STA_DISCONNECT_RETRY", "BT_STA_LOCK",
    "BT_STA_ATE_IDLE", "BT_STA_ATE_DISCONNECTTING", "BT_STA_ATE_CONNECTTED",
    "BT_STA_DUT_FINDING_ATE", "BT_STA_DUT_FOUND_ATE",
    "BT_STA_DUT_CONNECTING_ATE", "BT_STA_DUT_CONNECTTED_ATE"
};

#ifdef ATE
void
ate_data_cb(U16 len, U8 *dat) {
	#ifdef PS3_ACTIVATE
	
	if (ate_app_state == ATE_APP_STATE_START) {
		if(len == 32){
			
			U8 databuf[32];
			keyInstance         keyInst;     
			rijndaelInit(&keyInst, 128, (U8*)secret_key);
			rijndael_ecb_decrypt(&keyInst, dat, 32, 0, databuf);
		
			if(memcmp(databuf, activate_data, 32)==0){
				ate_test_result = 1;
				dlg->SetResult(TEXT("[pass]"));
			} else {
				ate_test_result = 0;
				dlg->SetResult(TEXT("[fail]"));
			}		
		} else {
			ate_test_result = 0;
			dlg->SetResult(TEXT("[fail]"));
		}        
        ate_app_state = ATE_APP_STATE_END;	
	}

	#else
    if (ate_app_state == ATE_APP_STATE_IDLE) //waiting start response
    {
        if (!strcmp((char *) dat, "start res")) {
            ate_app_state = ATE_APP_STATE_START;
            ate_allow_send = 1;
        }
    } else if (ate_app_state == ATE_APP_STATE_START) {

		/*
        if (!strcmp((char *) dat, "ok")) {
            ate_test_result = 1;
        } else {
            ate_test_result = 0;
        }
		*/
		if(len == TEST_PATTERN_LEN){
			if(memcmp(dat, test_pattern, TEST_PATTERN_LEN)==0){
				ate_test_result = 1;
			} else {
				ate_test_result = 0;
			}		
		} else {
			ate_test_result = 0;
		}

        bt_ate_send_data(strlen("endo") + 1,
                         ate_test_result ? (U8 *) "endo" : (U8 *) "endx");

        ate_app_state = ATE_APP_STATE_END;
    }
	#endif
}
#else
void
dut_data_cb(U16 len, U8 *dat) {

#if 0
    if (dut_app_state == DUT_APP_STATE_IDLE) //waiting start response
    {
        if (!strcmp((char *) dat, "start req")) {
            bt_dut_send_data((U16)strlen("start res") + 1, (U8 *) "start res");
            dut_app_state = DUT_APP_STATE_START;
        } else {
            OutputDebugStringA("shit");
        }
    } else if (dut_app_state == DUT_APP_STATE_START) //wait instruction
    {		
		if(len == TEST_PATTERN_LEN){
			if(memcmp(dat, test_pattern, TEST_PATTERN_LEN)==0){
				bt_dut_send_data(TEST_PATTERN_LEN,test_pattern);
			} else {
				bt_dut_send_data(TEST_PATTERN_LEN, fail_pattern);
			}

		} else if (!strncmp((char *) dat, "end", 3)) {
            dut_test_result = dat[3] == 'o' ? 1 : 0;
            dut_app_state = DUT_APP_STATE_END;
        }

    } else {
        OutputDebugStringA("invalid state recived data error!!!");
    }
#else
	if (dut_app_state == DUT_APP_STATE_IDLE) //waiting start response
	{
			U8 databuf[32];
			keyInstance         keyInst;     
			rijndaelInit(&keyInst, 128, (U8*)secret_key);
			rijndael_ecb_decrypt(&keyInst, dat, 32, 0, databuf);

		if(memcmp(databuf, secret_tag, 13)==0){

			bt_dut_send_data(32,dat);

			dut_test_result = 1;
			dut_app_state = DUT_APP_STATE_END;
		}
	
	}

#endif


}
#endif



void
CManufactureDemoAppDlg::ControlCenter(void) {
    static U8           stk_state       = 0xff;
    static U8           app_state       = 0xff;
    U8                  stk_new_state;

    stk_new_state = bt_state();

    //  if(stk_state != stk_new_state) //state change
     {
#ifdef ATE

	#ifdef RF_TEST
		if(stk_new_state == BT_STA_FREQ_TEST){
			if(!bIsRfTesting){
Bt_Test_Power = 7;
				char buf[MAX_PATH];
				//sprintf(buf, "24%02d (%d/7)", Bt_Test_Freq+2, Bt_Test_Power);
				sprintf(buf, "Hopping (%d/7)", Bt_Test_Freq+2, Bt_Test_Power);
				m_control_status.SetWindowText(buf);

				Bt__SetPower(Bt_Test_Power);
				bt_set_freq_test(Bt_Test_Freq, 0x04 /*DH1*/, 0x02 /*1111*/, Bt_Test_Len);


				//m_control_status.SetWindowText("SingleTone 2480");
				//m_control_status.UpdateWindow();
				//bt_set_freq_test(39, 0, 0x0A, 0);

				bIsRfTesting = 1;
			}
		}	
	#else
        if (stk_new_state == BT_STA_ATE_CONNECTTED) {

#ifdef PS3_ACTIVATE

			if (ate_app_state == ATE_APP_STATE_IDLE) {
				if (ate_allow_send) {
#ifdef CHECK_MASS_PRODUCTION_KEY
					int nLeft = TestKeyTimes();
					if(nLeft == 0){
						MessageBox("Available Count Exceeded!!");
						EndDialog(0);
						return;
					}
#endif
					srand((unsigned int)time(NULL));

					memset(activate_data, 0x00, 32);
					memcpy(activate_data, secret_tag, 13);

					int i;
					for(i=13;i<32;i++){
						activate_data[i] = rand()%256;
					}



					keyInstance         keyInst;     
					rijndaelInit(&keyInst, 128, (U8*)secret_key);
					rijndael_ecb_encrypt(&keyInst, activate_data, 32, 0, encrypted_data);

                    bt_ate_send_data(32,
                                     (U8 *) encrypted_data);
                    ate_allow_send = 0;

					ate_app_state = ATE_APP_STATE_START;

					dlg->SetResult("[wait]");        
					dlg->SetAction("Activating Device...");
					dlg->ShowWindow(SW_SHOW);
#ifdef CHECK_MASS_PRODUCTION_KEY
					MinusKeyTimes();
#endif
                }

			}  else if (ate_app_state == ATE_APP_STATE_END) {
                bt_ate_disconnect_dut(); //==> go to disconnected
                ate_app_state = ATE_APP_STATE_END_RESULT;
				ate_app_state = ATE_APP_STATE_IDLE;
				ate_allow_send = 1;

				Sleep(1000);
				dlg->ShowWindow(SW_HIDE);

				int nLeft = TestKeyTimes();
				char buf[MAX_PATH];

				//sprintf(buf, "Activation Cnt:%d", nLeft);
				sprintf(buf, "(%d)%02X:%02X:%02X", nLeft, activate_data[13], activate_data[14], activate_data[15]);
				m_control_status.SetWindowText(buf);

				
			}  /*else if (ate_app_state == ATE_APP_STATE_END_RESULT) {
				ate_app_state = ATE_APP_STATE_IDLE;
				ate_allow_send = 1;

				Sleep(1000);
				dlg->ShowWindow(SW_HIDE);
			}*/
#else

            if (ate_app_state == ATE_APP_STATE_IDLE) {
                if (ate_allow_send) {
                    bt_ate_send_data(strlen("start req") + 1,
                                     (U8 *) "start req");
                    ate_allow_send = 0;
                }
            } else if (ate_app_state == ATE_APP_STATE_START) {
                if (ate_allow_send) {

					bt_ate_send_data(TEST_PATTERN_LEN,
                                     (U8 *) test_pattern);

                    ate_allow_send = 0;
                }
            } else if (ate_app_state == ATE_APP_STATE_END) {
                bt_ate_disconnect_dut(); //==> go to disconnected
                ate_app_state = ATE_APP_STATE_END_RESULT;
            } else if (ate_app_state == ATE_APP_STATE_END_RESULT) //next life, connected, change from end_result to idle
            {
                ate_app_state = ATE_APP_STATE_IDLE;
                ate_allow_send = 1;
            }
#endif

        }
		else
		{
	        ate_allow_send = 1;
		}


	#endif

#else
        if (stk_new_state == BT_STA_DISCONNECTED) {
            if (dut_app_state == DUT_APP_STATE_IDLE) {
				
				if(find_ate > 0){
					bt_dut_find_ate(test_identifiler);
					find_ate--;

					// Light On Led
					int         transferlength;
					char        cdb[16];
					char        buf[512];

					transferlength = 0x00;

					memset(cdb, 0x00, sizeof(cdb));
					memset(buf, 0x00, sizeof(buf));

					cdb[0] = (char)0xF4;
					cdb[2] = 1;

					MassUart_Execute(0, &transferlength, cdb, buf);
			
				} else {
					SetTimer(IDT_TIMEOUT, 1000, NULL);
				}
            }
        } else if (stk_new_state == BT_STA_DUT_FOUND_ATE) {
            bt_dut_connect_ate();
			SetTimer(IDT_TIMEOUT, 20000, NULL);
        } else if (stk_new_state == BT_STA_DUT_CONNECTTED_ATE) {
			SetTimer(IDT_TIMEOUT, 20000, NULL);
            //app_state = dut_app_state;
        }
#endif  
        //   stk_state = stk_new_state;
    }
}
void
CManufactureDemoAppDlg::UpdateStatus(void) {
    char        test_state_str[100];
    char        test_result_str[50];
    U8          stk_state;
	int			bt_id;

	sprintf(test_state_str, "");

    stk_state = bt_state();

    //Update BT STATE
    GetDlgItem(IDC_EDIT_BT_STATE)->SetWindowText(bt_stk_str[stk_state]);

    //UPDATE TEST STATE
    switch (stk_state) {

#ifdef ATE
      case BT_STA_STOP:
        sprintf(test_state_str, "Please plug ATE");
        break;
#else
      case BT_STA_STOP:
        sprintf(test_state_str, "Please plug DUT");
        break;
#endif
      case BT_STA_POWERUP:
        m_control_status.SetWindowText("Wait");
      case BT_STA_BOOT:
      case BT_STA_INITILIZED:
        sprintf(test_state_str, "Initializating");

		char name[MAX_PATH];	
		bt_id = MassUart_GetUniqueID();
		sprintf(name, "%02X:%02X:%02X:%02X:%02X:%02X", 0x00, 0x0C, 0xE7,
            (bt_id & 0xFF0000) >> 16, (bt_id & 0xFF00) >> 8, (bt_id & 0xFF));
		
		GetDlgItem(IDC_EDIT_MAC)->SetWindowText(name);

		find_ate = FIND_ATE_COUNT;

        break;

      case BT_STA_PAIRING:
      case BT_STA_CONNECTING:
      case BT_STA_CONNECTED:
      case BT_STA_HID_CONNECTED:
      case BT_STA_HFP_CONNECTED:
      case BT_STA_HID_HFP_CONNECTED:
      case BT_STA_DISCONNECT_RETRY:
      case BT_STA_DISCONNECTING:

#ifdef ATE
      case BT_STA_DISCONNECTED:
      case BT_STA_DUT_FINDING_ATE:
      case BT_STA_DUT_FOUND_ATE:
      case BT_STA_DUT_CONNECTING_ATE:
      case BT_STA_DUT_CONNECTTED_ATE:
#else                   
      case BT_STA_ATE_IDLE:
      case BT_STA_ATE_CONNECTTED:
#endif                  
        sprintf(test_state_str, "Unknown error");
        break;

#ifdef ATE
      case BT_STA_ATE_IDLE:
        sprintf(test_state_str, "Waiting for DUT connected");		
        break;
      case BT_STA_ATE_CONNECTTED:
        sprintf(test_state_str, "DUT connected");
        break;
      case BT_STA_ATE_DISCONNECTTING:
        sprintf(test_state_str, "Disconnecting DUT");
        break;

	  case BT_STA_FREQ_TEST:
		sprintf(test_state_str, "Freq Test");
		//m_control_status.SetWindowText("Ready");
        break;
#else                   
      case BT_STA_DISCONNECTED:
        sprintf(test_state_str, "Ready to Search ATE");
        break;
      case BT_STA_DUT_FINDING_ATE:
        sprintf(test_state_str, "Finding ATE...");
        break;
      case BT_STA_DUT_FOUND_ATE:
        sprintf(test_state_str, "ATE Found");
        break;
      case BT_STA_DUT_CONNECTING_ATE:
        sprintf(test_state_str, "Connecting ATE");
        break;
      case BT_STA_DUT_CONNECTTED_ATE:
        sprintf(test_state_str, "ATE connected, and testing");
        break;
#endif
      case BT_STA_BT_FAIL:
        sprintf(test_state_str, "BT error");
        break;
      case BT_STA_LOCK:
        sprintf(test_state_str, "Authentication has been expired");
        break;
    }

#ifndef RF_TEST

#ifndef PS3_ACTIVATE
    GetDlgItem(IDC_EDIT_TEST_STATE)->SetWindowText(test_state_str);

	if(stk_state > BT_STA_STOP){
		dlg->SetResult(TEXT("[wait]"));        
		dlg->SetAction(test_state_str);
	}
#endif

#endif



#ifdef ATE
#else
	if(BT_STA_DISCONNECTED == stk_state){

		KillTimer(IDT_TIMEOUT);

		/*
		dlg->SetAction("Remove DUT.");
		if(dut_test_result == 0){
			dlg->SetResult(TEXT("[FAIL]"));
			m_control_status.SetTextColor(RED);
			m_control_status.SetWindowText("FAIL!!");
		} else {
			dlg->SetResult(TEXT("[PASS]"));
			m_control_status.SetTextColor(GREEN);
			m_control_status.SetWindowText("PASS!!");
		}
		*/
	
		if(dut_test_result == 1){
			dlg->SetAction("Remove DUT.");
			dlg->SetResult(TEXT("[PASS]"));
			m_control_status.SetTextColor(GREEN);
			m_control_status.SetWindowText("PASS!!");
		} else if(find_ate==0){
			dlg->SetAction("Remove DUT.");
			dlg->SetResult(TEXT("[FAIL]"));
			m_control_status.SetTextColor(RED);
			m_control_status.SetWindowText("FAIL!!");
		}

	}
#endif

    //dlg->InvalidateRect(NULL, FALSE);
    //dlg->UpdateWindow();      

    //UPDATE test result

#ifdef ATE

  #ifdef RF_TEST
  #else
    if (ate_app_state == ATE_APP_STATE_IDLE) {
        if (stk_state == BT_STA_ATE_CONNECTTED) {
            sprintf(test_result_str, "DUT connected, test start");
        } else {
            sprintf(test_result_str, "");
        }
    } else if (ate_app_state == ATE_APP_STATE_START) {
        sprintf(test_result_str, "Testing DUT");
    } else if (ate_app_state == ATE_APP_STATE_END) {
        sprintf(test_result_str, "Testing END");
    } else if (ate_app_state == ATE_APP_STATE_END_RESULT) {
		
        sprintf(test_result_str, ate_test_result ? "PASS" : "FAIL");
		/*
		if(ate_test_result){
			m_control_status.SetTextColor(GREEN);
			m_control_status.SetWindowText("PASS!!");		
		} else {
			m_control_status.SetTextColor(RED);
			m_control_status.SetWindowText("FAIL!!");		
		}
		*/
    }
  #endif

#else

    if (dut_app_state == DUT_APP_STATE_IDLE) {
        if (stk_state == BT_STA_DUT_CONNECTTED_ATE) {
            sprintf(test_result_str, "ATE Connected, waiting for testing");
        } else {
            sprintf(test_result_str, "");
        }
    } else if (dut_app_state == DUT_APP_STATE_START) {
        sprintf(test_result_str, "Being testing");
    } else if (dut_app_state == DUT_APP_STATE_END) {
        sprintf(test_result_str, dut_test_result ? "PASS" : "FAIL");
    }

#endif

#ifndef RF_TEST
#ifndef PS3_ACTIVATE
    GetDlgItem(IDC_EDIT_TEST_RESULT)->SetWindowText(test_result_str);
#endif
#endif

}

BOOL
CManufactureDemoAppDlg::OnDeviceChange(UINT nEventType, DWORD_PTR dwData) {
    PDEV_BROADCAST_HDR          pHdr    = (PDEV_BROADCAST_HDR) dwData;


	
	char buf[MAX_PATH];
	m_control_ate_id.GetWindowTextA(buf, MAX_PATH);
	if(!strlen(buf)){
		//MessageBox("ATE Identifier Invalid!!");
		test_identifiler = 1;
	}
	test_identifiler = atoi(buf);
	if(test_identifiler > 64){
		test_identifiler = 1;
	}
	if(test_identifiler < 0){
		test_identifiler = 1;
	}

    if (nEventType == DBT_DEVICEARRIVAL) {
        //SetTimer(IDT_ADD_DEVICE, 100, NULL);
        OutputDebugStringA("DBT_DEVICEARRIVAL\n");

        if (pHdr->dbch_devicetype == DBT_DEVTYP_DEVICEINTERFACE) {
            OutputDebugStringA("DBT_DEVTYP_DEVICEINTERFACE\n");
        } else if (pHdr->dbch_devicetype == DBT_DEVTYP_VOLUME) {
            OutputDebugStringA("DBT_DEVTYP_VOLUME\n");
        }

        if (pHdr->dbch_devicetype == DBT_DEVTYP_VOLUME) {
            if (RunBtThread(2)) {
                RunBtThread(0);
            }
#ifdef ATE
			bIsRfTesting = 0;	
#else
			dlg->ShowWindow(SW_SHOW);
			dut_test_result = 0;
#endif
        }
    } else if (nEventType == DBT_DEVICEREMOVECOMPLETE) {
        OutputDebugStringA("DBT_DEVICEREMOVECOMPLETE\n");

        if (pHdr->dbch_devicetype == DBT_DEVTYP_DEVICEINTERFACE) {
            OutputDebugStringA("DBT_DEVTYP_DEVICEINTERFACE\n");
        } else if (pHdr->dbch_devicetype == DBT_DEVTYP_VOLUME) {
            OutputDebugStringA("DBT_DEVTYP_VOLUME\n");
            if (!RunBtThread(2)) //if our hw unplug, it could be someone else
            {
				m_control_status.SetWindowText("");
				GetDlgItem(IDC_EDIT_MAC)->SetWindowText("00:00:00:00:00:00");

#ifdef ATE
                ate_app_state = ATE_APP_STATE_IDLE;
				ate_allow_send = 1;
				bIsRfTesting = 0;
#else
                dut_app_state = DUT_APP_STATE_IDLE;
				dlg->ShowWindow(SW_HIDE);
#endif
                RunBtThread(1); //try stop BT
            }
        }

#ifdef PS3_ACTIVATE
	int nLeft = TestKeyTimes();
	if(nLeft == 0){
		MessageBox("Available Count Exceeded!!");
		EndDialog(0);
        return TRUE;
	}
#endif

    } else {
        OutputDebugStringA("OnDeviceChange\n");
    }

    return true;
    return TRUE;
}


void CManufactureDemoAppDlg::OnBnClickedCancel()
{
	// TODO: Add your control notification handler code here
	RunBtThread(1); //try stop BT

	UnregisterHotKey(this->GetSafeHwnd(), m_nHotKeyID + 0);
	OnCancel();
}

// void bt_set_freq_test(U8 freq, U8 type, U8 pattern, U8 packet_length)

void CManufactureDemoAppDlg::OnBnClickedButtonAdd()
{

	if(Bt_Test_Freq<78){
		
		Bt_Test_Freq++;
		
		char buf[MAX_PATH];
		sprintf(buf, "24%02d (%d/7)", Bt_Test_Freq+2, Bt_Test_Power);
		m_control_status.SetWindowText(buf);
		
		Bt__SetPower(Bt_Test_Power);
		bt_set_freq_test(Bt_Test_Freq, 0x04 /*DH1*/, 0x02 /*1111*/, Bt_Test_Len);
	}

}

void CManufactureDemoAppDlg::OnBnClickedButtonSub()
{

	if(Bt_Test_Freq>0){

		Bt_Test_Freq--;

		char buf[MAX_PATH];
		sprintf(buf, "24%02d (%d/7)", Bt_Test_Freq+2, Bt_Test_Power);
		m_control_status.SetWindowText(buf);

		Bt__SetPower(Bt_Test_Power);
		bt_set_freq_test(Bt_Test_Freq, 0x04 /*DH1*/, 0x02 /*1111*/, Bt_Test_Len);
	}
}

void CManufactureDemoAppDlg::OnBnClickedButtonLow()
{
	Bt_Test_Freq = 0x00;

	char buf[MAX_PATH];
	sprintf(buf, "24%02d (%d/7)", Bt_Test_Freq+2, Bt_Test_Power);
	m_control_status.SetWindowText(buf);

	Bt__SetPower(Bt_Test_Power);
	bt_set_freq_test(Bt_Test_Freq, 0x04 /*DH1*/, 0x02 /*1111*/, Bt_Test_Len);
}

void CManufactureDemoAppDlg::OnBnClickedButtonMiddle()
{
	Bt_Test_Freq = 38;

	char buf[MAX_PATH];
	sprintf(buf, "24%02d (%d/7)", Bt_Test_Freq+2, Bt_Test_Power);
	m_control_status.SetWindowText(buf);

	Bt__SetPower(Bt_Test_Power);
	bt_set_freq_test(Bt_Test_Freq, 0x04 /*DH1*/, 0x02 /*1111*/, Bt_Test_Len);
}

void CManufactureDemoAppDlg::OnBnClickedButtonHigh()
{
	Bt_Test_Freq = 76;

	char buf[MAX_PATH];
	sprintf(buf, "24%02d (%d/7)", Bt_Test_Freq+2, Bt_Test_Power);
	m_control_status.SetWindowText(buf);

	Bt__SetPower(Bt_Test_Power);
	bt_set_freq_test(Bt_Test_Freq, 0x04 /*DH1*/, 0x02 /*1111*/, Bt_Test_Len);
}

void CManufactureDemoAppDlg::OnBnClickedButtonPsub()
{
	if(Bt_Test_Power>0){
		char buf[MAX_PATH];

		Bt_Test_Power--;

		sprintf(buf, "24%02d (%d/7)", Bt_Test_Freq+2, Bt_Test_Power);
		m_control_status.SetWindowText(buf);

		Bt__SetPower(Bt_Test_Power);
		bt_set_freq_test(Bt_Test_Freq, 0x04 /*DH1*/, 0x02 /*1111*/, Bt_Test_Len);	
	}	
}

void CManufactureDemoAppDlg::OnBnClickedButtonPadd()
{
	if(Bt_Test_Power<7){
		char buf[MAX_PATH];

		Bt_Test_Power++;

		sprintf(buf, "24%02d (%d/7)", Bt_Test_Freq+2, Bt_Test_Power);
		m_control_status.SetWindowText(buf);

		Bt__SetPower(Bt_Test_Power);		
		bt_set_freq_test(Bt_Test_Freq, 0x04 /*DH1*/, 0x02 /*1111*/, Bt_Test_Len);
	}		
}


void
CManufactureDemoAppDlg::OnBnClickedButtonStartStop() {
	
	//RunBtThread(1);

	//bt_set_freq_test(20);

//	Bt__SetPower((power--)&0x07);
//	bt_set_freq_test(Bt_Test_Freq, 0x04 /*DH1*/, 0x02 /*1111*/, 128);
	Bt_Is_SingleTone = 1;
	Bt_Test_Power = 0;
	Bt_Test_Freq = 13;
	//Bt__SetPower(Bt_Test_Power);
	bt_set_freq_test(Bt_Test_Freq, 0, 0x0A, 0);
   // Bt__SetPower(Bt_Test_Power);
}
