#ifndef __PAD_DATA_HH__
#define __PAD_DATA_HH__
#ifdef WIN32
#include "bttype.h"
#else
#include "common.h"
#endif

#define BT_FREQ_TESTING 0
#define USB_CONNECTING 1
#define BT_CONNECTING 2
#define USB_DISCONNECTED 3
#define BT_DISCONNECTED 4
#define BT_PAIRING 5
#define USB_CONNECTED 6
#define BT_CONNECTED 7
#define BT_ACTIVATION_START 8
#define BT_ACTIVATION_SUCCESS 9
#define ENTER_HOT_SWAP_MODE 10
#define LEAVE_HOT_SWAP_MODE 11
#define MODE1_SHOW 12
#define MODE2_SHOW (MODE1_SHOW + 1)
#define MODE3_SHOW (MODE2_SHOW + 1)


//hid mode
#define PAD_HMODE_JOYSTICK 0
#define PAD_HMODE_KEYBOARD 1
#define PAD_HMODE_KEYBOARD_MOUSE 2
#define PAD_HMODE_PS3 3

void pad_data_init(U8 battery_level, U8 charging_state);
void pad_data_state_notify(U8 connecting_state);

void pad_data_key_set(U8 *dat);
void pad_data_pointer_set(U8 *dat);
U8 pad_data_is_start_pressed(void);
U8 pad_data_is_select_pressed(void);
U8 pad_data_is_ps_pressed(void);
void pad_data_set_battery(U8 level, U8 charging_status);

void pad_data_set_report_enabled(void);
U8 pad_data_is_report_enabled(void);
U8 pad_data_is_report_enabled2disabled(void);
U8 pad_data_key_change(void);
U8 pad_data_pointer_change(void);
U8 pad_data_get_report(U8 report_id, U8 len, U8 *dat);
void pad_data_set_report(U8 *dat, U8 len);
U8 pad_data_led_byte(void);
U8 pad_get_mode(void);
void pad_set_mode(U8 mode);


//ps3
U8 ps3_data_set_report(U8 type, U8 report, const U8 *buf, int len);
U8 ps3_data_get_report(U8 type, U8 report, U8 *buf, int maxlen);


#endif
