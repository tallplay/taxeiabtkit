#include "ps3-data.h"
#include <string.h>
#include <stdio.h>
#include "sdkapi\bt_hid_hfp_api.h"

#ifdef WIN32
#include "../HAL/LocalStorage.h"
int __cdecl _CrtDbgReport(int, const char *, int, const char *, const char *, ...);
#define PRINTF(...) _CrtDbgReport(0, NULL, 0, NULL, ##__VA_ARGS__);
#else
#include "localstorage-ucomm-ps3.h"
#include "debug.h"
#endif

enum axis_index {
    sa_lstick_x = 0, sa_lstick_y,
    sa_rstick_x, sa_rstick_y,
    sa_acc_x,
    sa_acc_y,
    sa_acc_z,
    sa_gyro,
    sa_select, sa_start, sa_ps,
    sa_up, sa_right, sa_down, sa_left,
    sa_triangle, sa_circle, sa_cross, sa_square,
    sa_l1, sa_r1,
    sa_l2, sa_r2,
    sa_l3, sa_r3,
    SA_MAX
};

struct global_state {
    U8 reporting_enable2disable;
    U8 reporting_enabled;
    U8 feature_ef_byte_6;
    U8 battery_level; //0~3
    U8 battery_charging; //0: no charging 1: charging 2:charging complete
    U8 isPS3;
    U8 led_byte;
    U8 local_addr[6];
    U8 axis[SA_MAX];
    U8 key_changed;
} ps3_state;

struct sixaxis_assemble_t {
    int type;
    U8 report;
    int (*func)(U8 *buf, int maxlen);
};

struct sixaxis_process_t {
    int type;
    U8 report;
    int (*func)(const U8 *buf, int len);
};

void ps3_data_get_local_addr(void);

const int digital_order[17] = {
    sa_select, sa_l3, sa_r3, sa_start,
    sa_up, sa_right, sa_down, sa_left,
    sa_l2, sa_r2, sa_l1, sa_r1,
    sa_triangle, sa_circle, sa_cross, sa_square,
    sa_ps };
    
/* Main input report from Sixaxis -- assemble it */
int assemble_input_01(U8 *buf, int maxlen)
{
    int i;

    if (maxlen < 48) return -1;
    memset(buf, 0, 48);

    /* Digital button state */
    for (i = 0; i < 17; i++) 
    {
        int byte = 1 + (i / 8);
        int offset = i % 8;
        if (ps3_state.axis[digital_order[i]])
            buf[byte] |= (1 << offset);
    }

    /* Axes */
    buf[5] = ps3_state.axis[sa_lstick_x];
    buf[6] = ps3_state.axis[sa_lstick_y];
    buf[7] = ps3_state.axis[sa_rstick_x];
    buf[8] = ps3_state.axis[sa_rstick_y];

    /* Analog button state */
    buf[13] = ps3_state.axis[sa_up];
    buf[14] = ps3_state.axis[sa_right];
    buf[15] = ps3_state.axis[sa_down];
    buf[16] = ps3_state.axis[sa_left];
    //different from UCOMM provided spec begin !!!
    buf[17] = ps3_state.axis[sa_l2];
    buf[18] = ps3_state.axis[sa_r2];
    buf[19] = ps3_state.axis[sa_l1];
    buf[20] = ps3_state.axis[sa_r1];
    buf[21] = ps3_state.axis[sa_triangle];
    buf[22] = ps3_state.axis[sa_circle];
    buf[23] = ps3_state.axis[sa_cross];
    buf[24] = ps3_state.axis[sa_square];
    //different from UCOMM provided spec end!!!
    

    /* Battery */
    if(ps3_state.battery_charging == 0) //no charging
    {
        buf[28] = 0x03;
        buf[29] = ps3_state.battery_level + 2;
        buf[30] = 0x16;        
    }
    else if(ps3_state.battery_charging == 1) //charging
    {
        buf[28] = ps3_state.battery_level + 1;
        buf[29] = 0xee;
        buf[30] = 0x12;        
    }
    else if(ps3_state.battery_charging == 2) //charging complete
    {
        buf[28] = 0x03;
        buf[29] = 0xef;
        buf[30] = 0x14;        
    }

    /*
    0x03,0x02,0x16: level 0
    0x03,0x03,0x16: level 1
    0x03,0x04,0x16: level 2
    0x03,0x05,0x16: level 3
    0x01,0xee,0x12: level 0 charging
    0x02,0xee,0x12: level 1 charging
    0x03,0xee,0x12: level 2 charging
    0x04,0xee,0x12: level 3 charging
    0x03,0xef,0x14: charging complete
    */
    
    /* Battery comsumption*/
    buf[35] = 0x33;
    buf[36] = 0x02;
    buf[37] = 0x77;
    buf[38] = 0x01;
    buf[39] = 0x81; 

    /* Accelerometers */

    ps3_state.key_changed = 0;
    return 48;
}


/* Unknown */
int assemble_feature_01(U8 *buf, int maxlen)
{
    int len;
    U8 data[] = {
        0x01, 0x03, 0x00, 0x05, 0x0c, 0x01, 0x02, 0x18,
        0x18, 0x18, 0x18, 0x09, 0x0a, 0x10, 0x11, 0x12,
        0x13, 0x00, 0x00, 0x00, 0x00, 0x04, 0x00, 0x02,
        0x02, 0x02, 0x02, 0x00, 0x00, 0x00, 0x04, 0x04,
        0x04, 0x04, 0x00, 0x00, 0x02, 0x01, 0x02, 0x00,
        0x64, 0x00, 0x17, 0x00, 0x00, 0x00, 0x00, 0x00
    };

    len = sizeof(data);
     if (len > maxlen)
        memcpy(buf, data, maxlen);
    else
        memcpy(buf, data, len);

    return len;
}

/* Unknown */
int assemble_feature_ef(U8 *buf, int maxlen)
{
    int len;
    const U8 data[] = {
        0xef, 0x04, 0x00, 0x05, 0x03, 0x01, 0xb0, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x02, 0x00, 0x00, 0x00, /* acc_x center & radius? */
        0x02, 0x00, 0x03, 0xff, /* acc_y center & radius? */
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04
    };
    len = sizeof(data);
    if (len > maxlen)
        memcpy(buf, data, maxlen);
    else
        memcpy(buf, data, len);

    /* Byte 6 must match the byte set by the PS3 */
    buf[6] = ps3_state.feature_ef_byte_6;

    return len;
}

/* Unknown */
int assemble_feature_f2(U8 *buf, int maxlen)
{
    int len;
    U8 i;
    U8 data[] = {
        0xff, 0xff, 0x00,
        0x00, 0x1e, 0x3d, 0x24, 0x97, 0xde, /* device bdaddr */
        0x00, 0x03, 0x50, 0x81, 0xd8, 0x01, 0x8a,
        0x13
    };

    len = sizeof(data);

    ps3_data_get_local_addr();

    ps3_state.isPS3 = 1;
    //usb need to reverse the address order
    for(i = 0; i < 6; i++)
        data[3+i] = ps3_state.local_addr[5-i];
    
    if (len > maxlen)
        memcpy(buf, data, maxlen);
    else
        memcpy(buf, data, len);

    return len;
}

/* Unknown */
int assemble_feature_f5(U8 *buf, int maxlen)
{
    int len;
    U8 tmp[6];
    U8 data[] = {
        0x00,
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, /* ps3 bdaddr */
        0x97, 0xde /*device bdaddr 2 bytes*/
    };

    ps3_data_get_paired_host(tmp);
    
    memcpy(data+1, tmp, 6);

    data[7] = ps3_state.local_addr[5-4];
    data[8] = ps3_state.local_addr[5-5];
    len = sizeof(data);
    
    if (len > maxlen)
        memcpy(buf, data, maxlen);
    else
        memcpy(buf, data, len);

    return len;
}

/* Unknown */
int assemble_feature_f7(U8 *buf, int maxlen)
{
    const U8 data[] = {
        /*0x00, */0x02, 0xe4, 0x02, 0xa9, 0x01, 0x05, 0xff, 0x14, 0x23, 0x00
    };
    int len = sizeof(data);
     if (len > maxlen)
        memcpy(buf, data, maxlen);
    else
        memcpy(buf, data, len);


    return len;
}

/* Unknown */
int assemble_feature_f8(U8 *buf, int maxlen)
{
    const U8 data[] = {
        0x01, 0x00, 0x00, 0x00
    };
    int len = sizeof(data);
    if (len > maxlen)
        memcpy(buf, data, maxlen);
    else
        memcpy(buf, data, len);

    memcpy(buf, data, len);

    return len;
}

/* Main output report from PS3 including rumble and LEDs */
int process_output_01(const U8 *buf, int len)
{
    ps3_state.led_byte |= buf[9];
    if (len < 48)
        return -1;
    return 0;
}

/* Unknown */
int process_feature_ef(const U8 *buf, int len)
{
    if (len < 7)
        return -1;
    /* Need to remember byte 6 for assemble_feature_ef */
    ps3_state.feature_ef_byte_6 = buf[6];
    return 0;
}

/* Enable reporting */
int process_feature_f4(const U8 *buf, int len)
{
    /* Enable event reporting */
    if(buf[0] == 0x42)
    {
        PRINTF("f41=0x%02x\r\n", buf[1]);
        if(buf[1] == 0x0a || buf[1] == 0x0c) //enable 0xa: bt, 0xc: usb
        {
            ps3_state.reporting_enabled = 1;
            ps3_state.led_byte &= ~0xC0;
        }
        else
        {
            ps3_state.reporting_enable2disable = 1;
            ps3_state.reporting_enabled = 0;
            ps3_state.led_byte |= 0x80; //slow
        }
    }
    else
        PRINTF("f40=%02x%02x%02x%02x\r\n", buf[0], buf[1], buf[2], buf[3]);
    return 0;
}

int process_feature_f5(const U8 *buf, int len)
{
    int adr;
    U8 tmpff[6] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff};
    U8 data[6];
    if(len < 8) return -1;

    adr = 0; //8K flash
    do
    {
        StorageGetBytes(adr, 6, data);
        if(!memcmp(data, tmpff, 6)) //ff
            break;
        adr += 32;
        
    }while(adr < 8192);


    if(adr != 8192)
        StorageSetBytes(adr, 6, (U8*)buf+2);
    else
    {
        StorageClear();
        StorageSetBytes(0, 6, (U8*)buf+2);
    }
    
    return 0;
}

void ps3_data_init(U8 level, U8 charging)
{
    ps3_data_state_notify(USB_DISCONNECTED);
    ps3_data_set_battery(level , charging);
}

void ps3_data_state_notify(U8 connecting_state)
{
    if(connecting_state != USB_CONNECTING)
    {//reset data except USB_CONNECTING, because upper level found usb connecting after data was exchanged
     //and because BT was connected while usb plugged
        memset(&ps3_state, 0, sizeof(struct global_state));

        ps3_state.reporting_enabled = 0;
        ps3_state.feature_ef_byte_6 = 0xb0;
        ps3_state.reporting_enable2disable = 0;
        ps3_state.isPS3 = 0;
        ps3_state.led_byte = 0;
        ps3_state.key_changed = 0;
        ps3_state.axis[sa_lstick_x] = 128;
        ps3_state.axis[sa_lstick_y] = 128;
        ps3_state.axis[sa_rstick_x] = 128;
        ps3_state.axis[sa_rstick_y] = 128;
    }

    switch(connecting_state)
    {
        case USB_CONNECTING:
             ps3_state.led_byte |= 0x80; //slow            
            break;
        case BT_CONNECTING:
             ps3_state.led_byte |= 0x40; //fast            
            break;
        default:
            ps3_state.led_byte = 0;
            break;
    }
}

void ps3_data_get_local_addr(void)
{
/* do not modified!!!! */
    bt_gap_get_mac_addr(ps3_state.local_addr);
/* do not modified!!!! */
}

void ps3_data_get_paired_host(U8 *bdaddr)
{
    int adr;
    U8 tmpff[6] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff};
    adr = 8192; //8K flash
    do
    {
        adr -= 32;
        StorageGetBytes(adr, 6, bdaddr);
        if(memcmp(bdaddr, tmpff, 6)) //not ff
            break;
        
    }while(adr != 0);
}

U8 ps3_data_is_report_enabled(void)
{
    return ps3_state.reporting_enabled;
}

U8 ps3_data_is_report_enabled2disabled(void)
{
    U8 val;

    val = ps3_state.reporting_enable2disable;
    if(val)   ps3_state.reporting_enable2disable = 0;
    return val;
}

U8 ps3_data_led_byte(void)
{
    return ps3_state.led_byte;
}

U8 ps3_data_is_ps3(void)
{
    return ps3_state.isPS3;
}

U8 ps3_data_is_pskey_pressed(void)
{        
    if(ps3_state.axis[sa_ps])
    {
        if(!ps3_state.reporting_enabled && !ps3_state.isPS3) //is PC and ps key pressed
        {
            ps3_state.reporting_enabled = 1;
        }            
 
    }
    
    return ps3_state.axis[sa_ps];
}

U8 ps3_data_key_change(void)
{
    return ps3_state.key_changed;
}

int ps3_data_get_report(U8 type, U8 report,
        U8 *buf, int maxlen)
{
    int len = 0;

    if(type == 0x01)
    {
        if(report == 0x01) len = assemble_input_01((U8*)buf, maxlen);
            
    }
    else if(type == 0x03)
    {
        if(report == 0x01) len = assemble_feature_01((U8*)buf, maxlen);
        else if(report == 0xef) len = assemble_feature_ef((U8*)buf, maxlen);
        else if(report == 0xf2) len = assemble_feature_f2((U8*)buf, maxlen);
        else if(report == 0xf5) len = assemble_feature_f5((U8*)buf, maxlen);
        else if(report == 0xf7) len = assemble_feature_f7((U8*)buf, maxlen);
        else if(report == 0xf8) len = assemble_feature_f8((U8*)buf, maxlen);
    }

    if (len < 0) 
    {
        len = 0;
    }

    return len;
}


int ps3_data_set_report(U8 type, U8 report, const U8 *buf, int len)
{
    int ret = 0;

    if(type == 0x02)
    {
        if(report == 0x01)//0x201
            ret = process_output_01(buf, len);
    }
    else if(type == 0x03) //03ef, 03f4, 03f5
    {
        if(report == 0xef)
            ret = process_feature_ef(buf, len);
        else if(report == 0xf4)
            ret = process_feature_f4(buf, len);
        else if(report == 0xf5)
            ret = process_feature_f5(buf, len);
    }
    
    return ret;
}

void ps3_data_set_battery(U8 level, U8 charging_status)
{
    ps3_state.battery_level = level > 3 ? 3 : level;
    ps3_state.battery_charging = charging_status;
}

void ps3_data_set_button(U8 *dat)
{
    //byte 0
    ps3_state.axis[sa_select] = dat[0] & 0x1;    
    ps3_state.axis[sa_start] = dat[0] & 0x2;    
    ps3_state.axis[sa_l3] = dat[0] & 0x4;    
    ps3_state.axis[sa_r3] = dat[0] & 0x8;    
    ps3_state.axis[sa_ps] = dat[0] & 0x10;    

    //byte 1~byte 4
    ps3_state.axis[sa_lstick_x] = dat[1];
    ps3_state.axis[sa_lstick_y] = dat[2]; 
    ps3_state.axis[sa_rstick_x] = dat[3];
    ps3_state.axis[sa_rstick_y] = dat[4]; 

    //byte5~byte8
    ps3_state.axis[sa_right] = dat[5];    
    ps3_state.axis[sa_left] = dat[6];    
    ps3_state.axis[sa_up] = dat[7];    
    ps3_state.axis[sa_down] = dat[8];    

    //byte9~byte12
    ps3_state.axis[sa_triangle] = dat[9];    
    ps3_state.axis[sa_circle] = dat[10];    
    ps3_state.axis[sa_cross] = dat[11];    
    ps3_state.axis[sa_square] = dat[12];    

    //byte13~byte16
    ps3_state.axis[sa_l1] = dat[13];    
    ps3_state.axis[sa_r1] = dat[14];    
    ps3_state.axis[sa_l2] = dat[15];    
    ps3_state.axis[sa_r2] = dat[16];      

    ps3_state.key_changed = 1;

}
