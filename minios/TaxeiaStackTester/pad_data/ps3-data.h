#ifndef PS3_DATA_H
#define PS3_DATA_H
#ifdef WIN32
#include "bttype.h"
#else
//#include "common.h"
#endif

#define USB_CONNECTING 1
#define BT_CONNECTING 2
#define USB_DISCONNECTED 3
#define BT_DISCONNECTED 4

void ps3_data_init(U8 level, U8 charging);
void ps3_data_state_notify(U8 connecting_state);

int ps3_data_set_report(U8 type, U8 report, const U8 *buf, int len);
int ps3_data_get_report(U8 type, U8 report, U8 *buf, int maxlen);

U8 ps3_data_key_change(void);
U8 ps3_data_is_ps3(void);
U8 ps3_data_led_byte(void);
U8 ps3_data_is_pskey_pressed(void);
U8 ps3_data_is_report_enabled(void);
U8 ps3_data_is_report_enabled2disabled(void);
void ps3_data_get_paired_host(U8 *);
void ps3_data_set_button(U8 *dat);
void ps3_data_set_battery(U8 level, U8 charging_status);

#endif
