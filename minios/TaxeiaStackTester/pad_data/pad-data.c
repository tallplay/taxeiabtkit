#include "pad-data.h"
#include <string.h>
#include "sdkapi/bt_hid_hfp_api.h"

#ifdef WIN32
#include "../HAL/LocalStorage.h"
int __cdecl _CrtDbgReport(int, const char *, int, const char *, const char *, ...);
#define PRINTF(...) _CrtDbgReport(0, NULL, 0, NULL, ##__VA_ARGS__);
#else
#include "localstorage-ucomm-ps3.h"
#include "debug.h"
#endif

enum axis_index {
    sa_lstick_x = 0, sa_lstick_y,
    sa_rstick_x, sa_rstick_y,
    sa_acc_x,
    sa_acc_y,
    sa_acc_z,
    sa_gyro,
    sa_select, sa_start, sa_ps,
    sa_up, sa_right, sa_down, sa_left,
    sa_triangle, sa_circle, sa_cross, sa_square,
    sa_l1, sa_r1,
    sa_l2, sa_r2,
    sa_l3, sa_r3,
    sa_pointer_x,
    sa_pointer_y,
    SA_MAX
};

struct global_state {
    U8 reporting_enable2disable;
    U8 reporting_enabled;
    U8 feature_ef_byte_6;
    U8 battery_level; //0~3
    U8 battery_charging; //0: no charging 1: charging 2:charging complete
    U8 led_byte;
    U8 hid_mode;
    U8 last_hid_mode;
    U8 connecting_state;
    U8 local_addr[6];
    U8 enter_hot_swap : 1;
    U8 leave_hot_swap : 1;
    U8 key_changed : 1;
    U8 pointer_changed : 1;
    U8 reserved : 4;
    U8 axis[SA_MAX];
} pad_state;

U8 pad_data[20] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

const U8 digital_order[17] = {
    sa_select, sa_l3, sa_r3, sa_start,
    sa_up, sa_right, sa_down, sa_left,
    sa_l2, sa_r2, sa_l1, sa_r1,
    sa_triangle, sa_circle, sa_cross, sa_square,
    sa_ps };


void pad_data_init(U8 battery_level, U8 charging_state)
{
    pad_data_state_notify(USB_DISCONNECTED);
    pad_data_set_battery(battery_level, charging_state);

}

void pad_data_state_notify(U8 connecting_state)
{
    if(connecting_state == BT_CONNECTING || connecting_state == BT_PAIRING || connecting_state == BT_DISCONNECTED)
    {//reset data
#if (PAD_TYPE == 4) //BT pad always enabled
        pad_state.reporting_enabled = 1;
        pad_state.hid_mode = PAD_HMODE_JOYSTICK;
#else
        pad_state.reporting_enabled = 0;
        pad_state.hid_mode = PAD_HMODE_PS3;
#endif
        pad_state.enter_hot_swap = 0;
        pad_state.leave_hot_swap = 0;
        pad_state.reporting_enable2disable = 0;        
        pad_state.led_byte = 0;
        pad_state.key_changed = 0;
        pad_state.pointer_changed = 0;
        pad_state.axis[sa_lstick_x] = 128;
        pad_state.axis[sa_lstick_y] = 128;
        pad_state.axis[sa_rstick_x] = 128;
        pad_state.axis[sa_rstick_y] = 128;
        pad_state.feature_ef_byte_6 = 0xb0;
    }

    if(connecting_state < ENTER_HOT_SWAP_MODE)
        pad_state.connecting_state = connecting_state;
    else if(connecting_state == ENTER_HOT_SWAP_MODE)
    {
        pad_state.enter_hot_swap = 1;
        pad_state.last_hid_mode = pad_state.hid_mode;
    }
    else if(connecting_state == LEAVE_HOT_SWAP_MODE)
    {
        pad_state.leave_hot_swap = 1;
        connecting_state = pad_state.connecting_state;
    }

    switch(connecting_state)
    {
        case BT_FREQ_TESTING:
            pad_state.led_byte = 0x12; //1,4 on
            break;
        case BT_ACTIVATION_START:
            pad_state.led_byte = 0x8c; //2,3 slowblink
            break;
        case BT_ACTIVATION_SUCCESS:
            pad_state.led_byte = 0x4c; //2,3 fast blink
            break;
        case USB_CONNECTING:
            pad_state.led_byte = 0x80 | 0x1e; //slow            
            break;
        case BT_CONNECTING:
            pad_state.led_byte = 0x40 | 0x1e; //fast            
            break;
        case ENTER_HOT_SWAP_MODE:
            pad_state.led_byte = 0x20 | 0x1e; //very fast            
            break;
        case BT_PAIRING:
            pad_state.led_byte = 0x1e; //all on
            break;
        case BT_CONNECTED:
            pad_state.led_byte = 0x0; //need to find who am I
            break;
        case USB_CONNECTED:
            break;
        case MODE1_SHOW:
        case MODE2_SHOW:
        case MODE3_SHOW:
            pad_state.led_byte = 0x20 | (0x2 <<(connecting_state - MODE1_SHOW)); //very fast            
            break;
        default:
            pad_state.led_byte = 0;
            break;
    }    
}

void pad_data_pointer_set(U8 *dat)
{
    pad_state.axis[sa_pointer_x] = dat[0];
    pad_state.axis[sa_pointer_y] = dat[1];
    pad_state.pointer_changed = 1;
}

void pad_data_key_set(U8 *dat)
{
    //byte 0
    pad_state.axis[sa_select] = dat[0] & 0x1;    
    pad_state.axis[sa_start] = dat[0] & 0x2;    
    pad_state.axis[sa_l3] = dat[0] & 0x4;    
    pad_state.axis[sa_r3] = dat[0] & 0x8;    
    pad_state.axis[sa_ps] = dat[0] & 0x10;    

    //byte 1~byte 4
    pad_state.axis[sa_lstick_x] = dat[1];
    pad_state.axis[sa_lstick_y] = dat[2]; 
    pad_state.axis[sa_rstick_x] = dat[3];
    pad_state.axis[sa_rstick_y] = dat[4]; 

    //byte5~byte8
    pad_state.axis[sa_right] = dat[5];    
    pad_state.axis[sa_left] = dat[6];    
    pad_state.axis[sa_up] = dat[7];    
    pad_state.axis[sa_down] = dat[8];    

    //byte9~byte12
    pad_state.axis[sa_triangle] = dat[9];    
    pad_state.axis[sa_circle] = dat[10];    
    pad_state.axis[sa_cross] = dat[11];    
    pad_state.axis[sa_square] = dat[12];    

    //byte13~byte16
    pad_state.axis[sa_l1] = dat[13];    
    pad_state.axis[sa_r1] = dat[14];    
    pad_state.axis[sa_l2] = dat[15];    
    pad_state.axis[sa_r2] = dat[16];      

    pad_state.key_changed = 1;
}

U8 pad_data_is_start_pressed(void)
{
    return pad_state.axis[sa_start];
}

U8 pad_data_is_select_pressed(void)
{
    return pad_state.axis[sa_select];
}

U8 pad_data_is_ps_pressed(void)
{        
    return pad_state.axis[sa_ps];
}

void pad_data_set_battery(U8 level, U8 charging_status)
{
    pad_state.battery_level = level > 3 ? 3 : level;
    pad_state.battery_charging = charging_status;
}

void pad_data_set_report_enabled(void)
{
    pad_state.reporting_enabled = 1;
    pad_state.led_byte &= ~0xC0;    
}

U8 pad_data_is_report_enabled(void)
{
    return pad_state.reporting_enabled;
}

U8 pad_data_is_report_enabled2disabled(void)
{
    U8 val;

    val = pad_state.reporting_enable2disable;
    if(val)   pad_state.reporting_enable2disable = 0;
    return val;
}

U8 pad_data_key_change(void)
{
    if(pad_state.key_changed)
    {
        pad_state.key_changed = 0;
        return 1;
    }
    return 0;
}

U8 pad_data_pointer_change(void)
{
    if(pad_state.pointer_changed)
    {
        pad_state.pointer_changed = 0;
        return 1;
    }
    return 0;
}

void pad_data_set_report(U8 *dat, U8 len)
{
//2.led interrupt out edp2:
//  01 08 led 00 00 00 00 00
//4.motor interupt out edp2:
//  02 08 sm bm tt 00 00 00
//  sm:small motor 
//  lm:big   motor
//  tt:0~ff  rumble time
//     0:stop
//     1~fe:20ms per unit
//     ff:no stop

    if(len >= 8)
    {
        if(dat[0] == 0x01 && dat[1] == 0x08) //led
        {
            pad_state.led_byte = dat[2];
        }
    }
}

U8 pad_data_get_report(U8 report_id, U8 len, U8 *dat)
{
    if(report_id == 4) //game pad
    {
        dat[0] = ((pad_state.axis[sa_triangle] != 0) << 3) | ((pad_state.axis[sa_circle] != 0) << 2) | 
                 ((pad_state.axis[sa_cross] != 0) << 1) | ((pad_state.axis[sa_square] != 0) << 0);//|r2|l2|r1|l1|tri|cir|cro|squ|
        dat[1] =  ((pad_state.axis[sa_ps] != 0) << 4) | 
                 ((pad_state.axis[sa_start] != 0) << 1) | ((pad_state.axis[sa_select] != 0) << 0);;//|ps|r3|l3|st|sel|
        dat[2] = 0x08; //hat
        dat[3] = pad_state.axis[sa_lstick_x]; //lx
        dat[4] = pad_state.axis[sa_lstick_y]; //ly
        dat[5] = pad_state.axis[sa_rstick_x]; //rx
        dat[6] = pad_state.axis[sa_rstick_y]; //ry
        dat[7] = pad_state.axis[sa_right]; //right
        dat[8] = pad_state.axis[sa_left]; //left
        dat[9] = pad_state.axis[sa_up]; //up
        dat[10] = pad_state.axis[sa_down]; //down
        dat[11] = pad_state.axis[sa_triangle]; //tri
        dat[12] = pad_state.axis[sa_circle]; //cir
        dat[13] = pad_state.axis[sa_cross]; //cro
        dat[14] = pad_state.axis[sa_square]; //squ
        dat[15] = 0; //l1
        dat[16] = 0; //l2
        dat[17] = 0; //r1
        dat[18] = 0; //r2
        dat[19] = 0; //acclx_l
        dat[20] = 0; //acclx_h
        dat[21] = 0; //acclz_l
        dat[22] = 0; //acclz_h
        dat[23] = 0; //accly_l
        dat[24] = 0; //accly_h
        dat[25] = 0; //acclw_l
        dat[26] = 0; //acclw_h
        return 27;
    }
    else if(report_id == 1) //keyboard
    {
        dat[0] = 0;  //modifier
        dat[1] = 0;  //reseved
        dat[2] = 0;  //keycode 0
        dat[3] = (pad_state.axis[sa_ps] != 0) ? 0x13 : 0x00; //'P' //keycode 1
        dat[4] = 0;   //keycode 2
        dat[5] = 0;   //keycode 3
        dat[6] = 0;   //keycode 4
        dat[7] = 0;   //keycode 5
        return 8;
    }
    else if(report_id == 2) //mouse
    {
        U8 z;

        z = 0;        
 
        if(pad_state.axis[sa_down])
            z = 1;
        else if(pad_state.axis[sa_up])
            z = -1;      

        dat[0] = 0; //buttons
        dat[1] = pad_state.axis[sa_pointer_x]; //x
        dat[2] = pad_state.axis[sa_pointer_y]; //y
        dat[3] = z; //z
        return 4;
    }

    return 0;
}

U8 pad_data_led_byte(void)
{
    return pad_state.led_byte;
}

U8 pad_get_mode(void)
{
    if(pad_state.enter_hot_swap)
    {

/*        if(pad_state.leave_hot_swap)*/
        {
            pad_state.enter_hot_swap = pad_state.leave_hot_swap = 0;
        }

        return pad_state.last_hid_mode;
    }
    
    return pad_state.hid_mode;
}

void pad_set_mode(U8 mode)
{
    if(mode > PAD_HMODE_KEYBOARD_MOUSE)
        mode = PAD_HMODE_JOYSTICK;
    pad_state.hid_mode = mode;
}

///PS3/////////////////////////////
/* Main input report from Sixaxis -- assemble it */
int assemble_input_01(U8 *buf, int maxlen)
{
    int i;

    if (maxlen < 48) return -1;
    memset(buf, 0, 48);

    /* Digital button state */
    for (i = 0; i < 17; i++) 
    {
        int byte = 1 + (i / 8);
        int offset = i % 8;
        if (pad_state.axis[digital_order[i]])
            buf[byte] |= (1 << offset);
    }

    /* Axes */
    buf[5] = pad_state.axis[sa_lstick_x];
    buf[6] = pad_state.axis[sa_lstick_y];
    buf[7] = pad_state.axis[sa_rstick_x];
    buf[8] = pad_state.axis[sa_rstick_y];

    /* Analog button state */
    buf[13] = pad_state.axis[sa_up];
    buf[14] = pad_state.axis[sa_right];
    buf[15] = pad_state.axis[sa_down];
    buf[16] = pad_state.axis[sa_left];
    //different from UCOMM provided spec begin !!!
    buf[17] = pad_state.axis[sa_l2];
    buf[18] = pad_state.axis[sa_r2];
    buf[19] = pad_state.axis[sa_l1];
    buf[20] = pad_state.axis[sa_r1];
    buf[21] = pad_state.axis[sa_triangle];
    buf[22] = pad_state.axis[sa_circle];
    buf[23] = pad_state.axis[sa_cross];
    buf[24] = pad_state.axis[sa_square];
    //different from UCOMM provided spec end!!!
    

    /* Battery */
    if(pad_state.battery_charging == 0) //no charging
    {
        buf[28] = 0x03;
        buf[29] = pad_state.battery_level + 2;
        buf[30] = 0x16;        
    }
    else if(pad_state.battery_charging == 1) //charging
    {
        buf[28] = pad_state.battery_level + 1;
        buf[29] = 0xee;
        buf[30] = 0x12;        
    }
    else if(pad_state.battery_charging == 2) //charging complete
    {
        buf[28] = 0x03;
        buf[29] = 0xef;
        buf[30] = 0x14;        
    }

    /*
    0x03,0x02,0x16: level 0
    0x03,0x03,0x16: level 1
    0x03,0x04,0x16: level 2
    0x03,0x05,0x16: level 3
    0x01,0xee,0x12: level 0 charging
    0x02,0xee,0x12: level 1 charging
    0x03,0xee,0x12: level 2 charging
    0x04,0xee,0x12: level 3 charging
    0x03,0xef,0x14: charging complete
    */
    
    /* Battery comsumption*/
    buf[35] = 0x33;
    buf[36] = 0x02;
    buf[37] = 0x77;
    buf[38] = 0x01;
    buf[39] = 0x81; 

    /* Accelerometers */

    pad_state.key_changed = 0;
    return 48;
}


/* Unknown */
int assemble_feature_01(U8 *buf, int maxlen)
{
    int len;
    U8 data[] = {
        0x01, 0x03, 0x00, 0x05, 0x0c, 0x01, 0x02, 0x18,
        0x18, 0x18, 0x18, 0x09, 0x0a, 0x10, 0x11, 0x12,
        0x13, 0x00, 0x00, 0x00, 0x00, 0x04, 0x00, 0x02,
        0x02, 0x02, 0x02, 0x00, 0x00, 0x00, 0x04, 0x04,
        0x04, 0x04, 0x00, 0x00, 0x02, 0x01, 0x02, 0x00,
        0x64, 0x00, 0x17, 0x00, 0x00, 0x00, 0x00, 0x00
    };

    len = sizeof(data);
     if (len > maxlen)
        memcpy(buf, data, maxlen);
    else
        memcpy(buf, data, len);

    return len;
}

/* Unknown */
int assemble_feature_ef(U8 *buf, int maxlen)
{
    int len;
    const U8 data[] = {
        0xef, 0x04, 0x00, 0x05, 0x03, 0x01, 0xb0, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x02, 0x00, 0x00, 0x00, /* acc_x center & radius? */
        0x02, 0x00, 0x03, 0xff, /* acc_y center & radius? */
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04
    };
    len = sizeof(data);
    if (len > maxlen)
        memcpy(buf, data, maxlen);
    else
        memcpy(buf, data, len);

    /* Byte 6 must match the byte set by the PS3 */
    buf[6] = pad_state.feature_ef_byte_6;

    return len;
}

/* Unknown */
int assemble_feature_f2(U8 *buf, int maxlen)
{
    int len;
    U8 i;
    U8 data[] = {
        0xff, 0xff, 0x00,
        0x00, 0x1e, 0x3d, 0x24, 0x97, 0xde, /* device bdaddr */
        0x00, 0x03, 0x50, 0x81, 0xd8, 0x01, 0x8a,
        0x13
    };

    len = sizeof(data);

    bt_gap_get_mac_addr(pad_state.local_addr);

    //usb need to reverse the address order
    for(i = 0; i < 6; i++)
        data[3+i] = pad_state.local_addr[5-i];
    
    if (len > maxlen)
        memcpy(buf, data, maxlen);
    else
        memcpy(buf, data, len);

    return len;
}

/* Unknown */
int assemble_feature_f5(U8 *buf, int maxlen)
{
    int len;
    U8 tmp[6], i;
    U8 data[] = {
        0x00,
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, /* ps3 bdaddr */
        0x97, 0xde /*device bdaddr 2 bytes*/
    };

    bt_gap_is_paired_exist(tmp);

    for(i = 0; i < 6; i++)
        data[i+1] = tmp[5-i];

    //memcpy(data+1, tmp, 6);

    data[7] = pad_state.local_addr[5-4];
    data[8] = pad_state.local_addr[5-5];
    len = sizeof(data);
    
    if (len > maxlen)
        memcpy(buf, data, maxlen);
    else
        memcpy(buf, data, len);

    return len;
}

/* Unknown */
int assemble_feature_f7(U8 *buf, int maxlen)
{
    const U8 data[] = {
        /*0x00, */0x02, 0xe4, 0x02, 0xa9, 0x01, 0x05, 0xff, 0x14, 0x23, 0x00
    };
    int len = sizeof(data);
     if (len > maxlen)
        memcpy(buf, data, maxlen);
    else
        memcpy(buf, data, len);


    return len;
}

/* Unknown */
int assemble_feature_f8(U8 *buf, int maxlen)
{
    const U8 data[] = {
        0x01, 0x00, 0x00, 0x00
    };
    int len = sizeof(data);
    if (len > maxlen)
        memcpy(buf, data, maxlen);
    else
        memcpy(buf, data, len);

    memcpy(buf, data, len);

    return len;
}

/* Main output report from PS3 including rumble and LEDs */
int process_output_01(const U8 *buf, int len)
{
    if(buf[9])
        pad_state.led_byte = buf[9];
    
    if (len < 48)
        return -1;
    return 0;
}

/* Unknown */
int process_feature_ef(const U8 *buf, int len)
{
    if (len < 7)
        return -1;
    /* Need to remember byte 6 for assemble_feature_ef */
    pad_state.feature_ef_byte_6 = buf[6];
    return 0;
}

/* Enable reporting */
int process_feature_f4(const U8 *buf, int len)
{
    /* Enable event reporting */
    if(buf[0] == 0x42)
    {
        PRINTF("f41=0x%02x\r\n", buf[1]);
        if(buf[1] == 0x0a || buf[1] == 0x0c) //enable 0xa: bt, 0xc: usb
        {
            pad_state.reporting_enabled = 1;
            pad_state.led_byte &= ~0xC0;
        }
        else
        {
            pad_state.reporting_enable2disable = 1;
            pad_state.reporting_enabled = 0;
            pad_state.led_byte = 0x80 | 0x1e; //slow
        }
    }
    else
        PRINTF("f40=%02x%02x%02x%02x\r\n", buf[0], buf[1], buf[2], buf[3]);
    return 0;
}

int process_feature_f5(const U8 *buf, int len)
{
    int adr;
    U8 tmpff[6] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff};
    U8 data[6], i;
    if(len < 8) return -1;

        
    for(i = 0; i < 6; i++)
        data[i] = buf[7-i];

    bt_gap_storage_link_key(data, 0, 0);
    
    return 0;
}

U8 ps3_data_get_report(U8 type, U8 report,
        U8 *buf, int maxlen)
{
    U8 len = 0;

    if(type == 0x01)
    {
        if(report == 0x01) len = assemble_input_01((U8*)buf, maxlen);
            
    }
    else if(type == 0x03)
    {
        if(report == 0x01) len = assemble_feature_01((U8*)buf, maxlen);
        else if(report == 0xef) len = assemble_feature_ef((U8*)buf, maxlen);
        else if(report == 0xf2) len = assemble_feature_f2((U8*)buf, maxlen);
        else if(report == 0xf5) len = assemble_feature_f5((U8*)buf, maxlen);
        else if(report == 0xf7) len = assemble_feature_f7((U8*)buf, maxlen);
        else if(report == 0xf8) len = assemble_feature_f8((U8*)buf, maxlen);
    }

    if (len < 0) 
    {
        len = 0;
    }

    return len;
}


U8 ps3_data_set_report(U8 type, U8 report, const U8 *buf, int len)
{
    U8 ret = 0;

    if(type == 0x02)
    {
        if(report == 0x01)//0x201
            ret = process_output_01(buf, len);
    }
    else if(type == 0x03) //03ef, 03f4, 03f5
    {
        if(report == 0xef)
            ret = process_feature_ef(buf, len);
        else if(report == 0xf4)
            ret = process_feature_f4(buf, len);
        else if(report == 0xf5)
            ret = process_feature_f5(buf, len);
    }
    
    return ret;
}
