#pragma once
#include "afxwin.h"


// CPinDialog dialog

class CPinDialog : public CDialog
{
	DECLARE_DYNAMIC(CPinDialog)

public:
	CPinDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~CPinDialog();

	unsigned char pin[11];

// Dialog Data
	enum { IDD = IDD_PIN_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CEdit m_edit_pin;
	afx_msg void OnEnUpdateEditPin();
    unsigned char GetPinCode(unsigned char *pin);
};
