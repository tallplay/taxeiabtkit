#include <string.h>
#include <stdarg.h>
#include <stdio.h>
#include "TaxeiaConsoledebug.h"
#include "..\GAP\HCI\BtCmdEvt.h"
#ifdef WIN32
#include "..\HAL\LocalOutput.h"
#else
#include "..\btHAL\LocalOutput.h"
#endif

//#ifdef WIN32
//#else
/*mode*/
#define  NORMAL    0
#define  BRIGHT    1
#define  BLINK     5
#define  REVERSE   7
#define  INVISIBLE 8

/* basic colors */
#define  BLACK     0
#define  RED       1
#define  GREEN     2
#define  BROWN     3
#define  BLUE      4
#define  MAGENTA   5
#define  CYAN      6
#define  WHITE     7

/* shift values */
#define  FOREGROUND  30
#define  BACKGROUND  40

#define ANSI_SGR(a)    printf("\x1B[%dm",a)
#define ANSI_CUF(n)    printf("\x1B[%dC", n)
//#endif

void Taxeia_Assert(const char *expression, const char *file, U16 line)
{
    LocalBreak(file, line, expression);
}

#ifdef TAXEIA_DEBUG
#define LINE_WIDTH 16

void HexDisplay(const char *Source, const U8 *Buffer, U16 Len)
{
    const U8 *p = Buffer;
    char     *o, output[(LINE_WIDTH * 4)+20]; /* 12 bytes padding */
    int       po;

    po = 0;
    while (p < Buffer+Len) {
        o = output;

        /* Append proper line advance spaces */
        o += sprintf(o, " %s   ", Source);

        /* Dumps the packet in basic ASCII dump format */
        po = 0;
        while (po < LINE_WIDTH) {
            if (p+po < Buffer+Len)
                o += sprintf(o, "%02x ", p[po]);
            else 
                o += sprintf(o, "   ");
            if (++po == LINE_WIDTH/2) o += sprintf(o, "  ");
        }
        o += sprintf(o, "    ");

        po = 0;
        while (po < LINE_WIDTH) {
            if (p+po < Buffer+Len)
                o += sprintf(o, "%1c", ((p[po] >= ' ' && p[po] <= '~') ? p[po] : '.'));
            else break;
            po++;
        }

        Taxeia_Report("%s\n", output);
        p += po;
    }
}

void EdrCmdShow(U16 cmd)
{
    ANSI_SGR(FOREGROUND+RED);
    Taxeia_Report("[%s]\n", HciCommandStr(cmd));
    ANSI_SGR(NORMAL);
}

void EdrEvtShow(U8 evt, U8 len, U8 *dat)
{
    char output[50];
    ANSI_SGR(FOREGROUND+BLUE);
    Taxeia_Report("[%s", HciEventStr(evt));
    HciEventParseStr(evt, len, dat, output);
    ANSI_SGR(FOREGROUND+BROWN);
    Taxeia_Report(" %s ", output);
    Taxeia_Report("]\n");
    ANSI_SGR(NORMAL);
}


#ifdef TAXEIA_L2_DEBUG
const char *L2CAPSignalOpcodeStr(U8 Opcode)
{
    switch (Opcode) {
        case LLC_COMMAND_REJ:
            return "Command Reject";
        case LLC_CONN_REQ:
            return "Connect Request";
        case LLC_CONN_RSP:
            return "Connect Response";
        case LLC_CONFIG_REQ:
            return "Config Request";
        case LLC_CONFIG_RSP:
            return "Config Response";
        case LLC_DISC_REQ:
            return "Disconnect Request";
        case LLC_DISC_RSP:
            return "Disconnect Response";
        case LLC_ECHO_REQ:
            return "Echo Request";
        case LLC_ECHO_RSP:
            return "Echo Response";
        case LLC_INFO_REQ:
            return "Info Request";
        case LLC_INFO_RSP:
            return "Info Response";
        case LLC_TEST_REQ:
            return "Test Channel";
    }
    return "UNKNOWN Signal Opcode";
}
#endif

void HciEventParseStr(U8 Event, U8 len, U8 *dat, char *output)
{
    switch(Event)
    {
        case HCE_COMMAND_COMPLETE:
        {
            sprintf(output, "<%s", HciCommandStr(dat[1] + (dat[2] << 8)));
            sprintf(output, "%s %s>", output, dat[3] ? "FAIL": "OK");
            return;
        }
           
    }
    output[0] = 0;    
}

const char *HciEventStr(U8 Event)
{
    switch (Event) {
        case HCE_INQUIRY_COMPLETE:
            return "INQUIRY_COMPLETE";
        case HCE_INQUIRY_RESULT:
            return "INQUIRY_RESULT";
        case HCE_CONNECT_COMPLETE:
            return "CONNECT_COMPLETE";
        case HCE_CONNECT_REQUEST:
            return "CONNECT_REQUEST";
        case HCE_DISCONNECT_COMPLETE:
            return "DISCONNECT_COMPLETE";
        case HCE_AUTH_COMPLETE:
            return "AUTH_COMPLETE";
        case HCE_REMOTE_NAME_REQ_COMPLETE:
            return "REMOTE_NAME_REQ_COMPLETE";
        case HCE_ENCRYPT_CHNG:
            return "ENCRYPT_CHNG";
        case HCE_CHNG_CONN_LINK_KEY_COMPLETE:
            return "CHNG_CONN_LINK_KEY_COMPLETE";
        case HCE_MASTER_LINK_KEY_COMPLETE:
            return "MASTER_LINK_KEY_COMPLETE";
        case HCE_READ_REMOTE_FEATURES_COMPLETE:
            return "READ_REMOTE_FEATURES_COMPLETE";
        case HCE_READ_REMOTE_VERSION_COMPLETE:
            return "READ_REMOTE_VERSION_COMPLETE";
        case HCE_QOS_SETUP_COMPLETE:
            return "QOS_SETUP_COMPLETE";
        case HCE_COMMAND_COMPLETE:
            return "COMMAND_COMPLETE";
        case HCE_COMMAND_STATUS:
            return "COMMAND_STATUS";
        case HCE_HARDWARE_ERROR:
            return "HARDWARE_ERROR";
        case HCE_FLUSH_OCCURRED:
            return "FLUSH_OCCURRED";
        case HCE_ROLE_CHANGE:
            return "ROLE_CHANGE";
        case HCE_NUM_COMPLETED_PACKETS:
            return "NUM_COMPLETED_PACKETS";
        case HCE_MODE_CHNG:
            return "MODE_CHNG";
        case HCE_RETURN_LINK_KEYS:
            return "RETURN_LINK_KEYS";
        case HCE_PIN_CODE_REQ:
            return "PIN_CODE_REQ";
        case HCE_LINK_KEY_REQ:
            return "LINK_KEY_REQ";
        case HCE_LINK_KEY_NOTIFY:
            return "LINK_KEY_NOTIFY";
        case HCE_LOOPBACK_COMMAND:
            return "LOOPBACK_COMMAND";
        case HCE_DATA_BUFFER_OVERFLOW:
            return "DATA_BUFFER_OVERFLOW";
        case HCE_MAX_SLOTS_CHNG:
            return "MAX_SLOTS_CHNG";
        case HCE_READ_CLOCK_OFFSET_COMPLETE:
            return "READ_CLOCK_OFFSET_COMPLETE";
        case HCE_CONN_PACKET_TYPE_CHNG:
            return "CONN_PACKET_TYPE_CHNG";
        case HCE_QOS_VIOLATION:
            return "QOS_VIOLATION";
        case HCE_PAGE_SCAN_MODE_CHANGE:
            return "PAGE_SCAN_MODE_CHANGE";
        case HCE_PAGE_SCAN_REPETITION_MODE:
            return "PAGE_SCAN_REPETITION_MODE";
        case HCE_VENDOR_SPECIFIC:
            return "VENDOR SPECIFIC";
        case HCE_BLUETOOTH_LOGO:
            return "LOGO TESTING";
        case HCE_FLOW_SPECIFICATION_COMPLETE:
            return "FLOW_SPECIFICATION_COMPLETE";
        case HCE_INQUIRY_RESULT_WITH_RSSI:
            return "INQUIRY_RESULT_WITH_RSSI";
        case HCE_READ_REMOTE_EXT_FEAT_COMPLETE:
            return "READ_REMOTE_EXT_FEAT_COMPLETE";
        case HCE_FIXED_ADDRESS:
            return "FIXED_ADDRESS";
        case HCE_ALIAS_ADDRESS:
            return "ALIAS_ADDRESS";
        case HCE_GENERATE_ALIAS_REQ:
            return "GENERATE_ALIAS_REQ";
        case HCE_ACTIVE_ADDRESS:
            return "ACTIVE_ADDRESS";
        case HCE_ALLOW_PRIVATE_PAIRING:
            return "ALLOW_PRIVATE_PAIRING";
        case HCE_ALIAS_ADDRESS_REQ:
            return "ALIAS_ADDRESS_REQ";
        case HCE_ALIAS_NOT_RECOGNIZED:
            return "ALIAS_NOT_RECOGNIZED";
        case HCE_FIXED_ADDRESS_ATTEMPT:
            return "FIXED_ADDRESS_ATTEMPT";
        case HCE_SYNC_CONNECT_COMPLETE:
            return "SYNC_CONNECT_COMPLETE";
        case HCE_SYNC_CONN_CHANGED:
            return "SYNC_CONN_CHANGED";
        case HCE_IOCAP_REQUEST:
            return "IOCAP_REQUEST";
        case HCE_SSP_COMPLETE:
            return "SSP_COMPLETE";
        case HCE_IOCAP_RESPONSE:
            return "IOCAP_RESPONSE";
        case HCE_USER_COMFIRM_REQ:
            return "USER_COMFIRM_REQ";
        case HCE_USER_PASSKEY_REQ:
            return "USER_PASSKEY_REQ";
        case HCE_REMOTE_OOB_DATA_REQ:
            return "REMOTE_OOB_DATA_REQ";
        case HCE_LINK_SUPER_TOUT_CHG:
            return "LINK_SUPERVISION_TIMEOUT_CHANGE";

    }
    return "UNKNOWN";
}

const char *HciStatusStr(U8 Status)
{
    const char *hcStatus[] = {
        "Success",
        "Unknown HCI Cmd",
        "No Connection",
        "Hardware Failure",
        "Page Timeout",
        "Auth Failure",
        "Key Missing",
        "Memory Full",
        "Conn Timeout",
        "Max Num Conns",
        "Max SCO Conns",
        "Max ACL Conns",
        "Cmd Disallowed",
        "Host Rej No Resources",
        "Host Rej Security",
        "Host Rej Personal Dev",
        "Host Timeout",
        "Unsupp Feature/Parm Val",
        "Invalid HCI Parm Val",
        "Conn Term User Req",
        "Conn Term Low Resources",
        "Conn Term Power Off",
        "Conn Term Local Host",
        "Repeated Attempts",
        "Pairing Disallowed",
        "Unknown LMP PDU",
        "Unsupp Remote Feature",
        "SCO Offset Rejected",
        "SCO Interval Rejected",
        "SCO Air Mode Rejected",
        "Invalid LMP Parm",
        "Unspecified Error",
        "Unsupp LMP Parm",
        "Role Change Disallowed",
        "Lmp Response Timedout",
        "Lmp Err Transaction Coll",
        "Lmp PDU Disallowed",
        "Encryption Mode Unacceptable",
        "Unit Key Used",
        "QoS Not Supported",
        "Instant Passed",
        "Pairing w/Unit Key Unsupp"
        "Different Transaction Collision", //0x2A
        "QoS Unacceptable Parameter", //0X2C
        "QoS Rejected", //0X2D
        "Channel Assessment  Not Supported", //0X2E
        "Insufficient Security", //0X2F
        "Parameter out of Mandatory Range", //0X30
        "unknown error", //0X31
        "Role Switch Pending", //0X32
        "unknown error", //0X33
        "Reserved Slot Violation", //0X34
        "Role Switch Failed", //0X35
        "Extended Inquiry Response Too Large", //0x36
        "Simple Pairing Not Supported By Host", //0X37
        "Host Busy�VPairing", //0X38
        "Connection Rejected Due To No Suitable Channel Found", //0X39
        "Controller Busy", //0X3A
        "Unacceptable Connection Interval", //0X3B
        "Directed Advertising Timeout", //0X3C
        "Connection Terminated Due To MIC Failure", //0X3D
        "Connection Failed To Be Established", //0X3E
        "MAC Connection Failed", //0x3F

    };
#if 0
    if (Status > HC_STATUS_PAIRING_W_UNIT_KEY_UNSUPP) {
        /* Handle out of sequence BtErrorCode values */
        switch (Status) {
            case BEC_STORE_LINK_KEY_ERR:
                return "Store Link Key Error";
            case BEC_REQUEST_CANCELLED:
                return "Request cancelled";
            case BEC_NOT_FOUND:
                return "Not Found";
            default:
                return "UNKNOWN";
        }
    }
#endif

    return hcStatus[Status];
}


const char *HciCommandStr(U16 cmd)
{
    switch (cmd) {
        case HCC_INQUIRY:
            return "INQUIRY";
        case HCC_INQUIRY_CANCEL:
            return "INQUIRY_CANCEL";
        case HCC_START_PERIODIC_INQ_MODE:
            return "START_PERIODIC_INQ_MODE";
        case HCC_EXIT_PERIODIC_INQ_MODE:
            return "EXIT_PERIODIC_INQ_MODE";
        case HCC_CREATE_CONNECTION:
            return "CREATE_CONNECTION";
        case HCC_DISCONNECT:
            return "DISCONNECT";
        case HCC_ADD_SCO_CONNECTION:
            return "ADD_SCO_CONNECTION";
        case HCC_ACCEPT_CON_REQ:
            return "ACCEPT_CON_REQ";
        case HCC_REJECT_CON_REQ:
            return "REJECT_CON_REQ";
        case HCC_LINK_KEY_REQ_REPL:
            return "LINK_KEY_REQ_REPL";
        case HCC_LINK_KEY_REQ_NEG_REPL:
            return "LINK_KEY_REQ_NEG_REPL";
        case HCC_PIN_CODE_REQ_REPL:
            return "PIN_CODE_REQ_REPL";
        case HCC_PIN_CODE_REQ_NEG_REPL:
            return "PIN_CODE_REQ_NEG_REPL";
        case HCC_CHNG_CONN_PACKET_TYPE:
            return "CHNG_CONN_PACKET_TYPE";
        case HCC_AUTH_REQ:
            return "AUTH_REQ";
        case HCC_SET_CONN_ENCRYPT:
            return "SET_CONN_ENCRYPT";
        case HCC_CHNG_CONN_LINK_KEY:
            return "CHNG_CONN_LINK_KEY";
        case HCC_MASTER_LINK_KEY:
            return "MASTER_LINK_KEY";
        case HCC_REM_NAME_REQ:
            return "REM_NAME_REQ";
        case HCC_READ_REMOTE_FEATURES:
            return "READ_REMOTE_FEATURES";
        case HCC_READ_REMOTE_VERSION:
            return "READ_REMOTE_VERSION";
        case HCC_READ_CLOCK_OFFSET:
            return "READ_CLOCK_OFFSET";
        case HCC_HOLD_MODE:
            return "HOLD_MODE";
        case HCC_SNIFF_MODE:
            return "SNIFF_MODE";
        case HCC_EXIT_SNIFF_MODE:
            return "EXIT_SNIFF_MODE";
        case HCC_PARK_MODE:
            return "PARK_MODE";
        case HCC_EXIT_PARK_MODE:
            return "EXIT_PARK_MODE";
        case HCC_QOS_SETUP:
            return "QOS_SETUP";
        case HCC_ROLE_DISCOVERY:
            return "ROLE_DISCOVERY";
        case HCC_SWITCH_ROLE:
            return "SWITCH_ROLE";
        case HCC_READ_LINK_POLICY:
            return "READ_LINK_POLICY";
        case HCC_WRITE_LINK_POLICY:
            return "WRITE_LINK_POLICY";
        case HCC_SET_EVENT_MASK:
            return "SET_EVENT_MASK";
        case HCC_RESET:
            return "RESET";
        case HCC_EVENT_FILTER:
            return "EVENT_FILTER";
        case HCC_FLUSH:
            return "FLUSH";
        case HCC_READ_PIN_TYPE:
            return "READ_PIN_TYPE";
        case HCC_WRITE_PIN_TYPE:
            return "WRITE_PIN_TYPE";
        case HCC_CREATE_NEW_UNIT_KEY:
            return "CREATE_NEW_UNIT_KEY";
        case HCC_READ_STORED_LINK_KEY:
            return "READ_STORED_LINK_KEY";
        case HCC_WRITE_STORED_LINK_KEY:
            return "WRITE_STORED_LINK_KEY";
        case HCC_DEL_STORED_LINK_KEY:
            return "DEL_STORED_LINK_KEY";
        case HCC_CHNG_LOCAL_NAME:
            return "CHNG_LOCAL_NAME";
        case HCC_READ_LOCAL_NAME:
            return "READ_LOCAL_NAME";
        case HCC_READ_CONN_ACCEPT_TIMEOUT:
            return "READ_CONN_ACCEPT_TIMEOUT";
        case HCC_WRITE_CONN_ACCEPT_TIMEOUT:
            return "WRITE_CONN_ACCEPT_TIMEOUT";
        case HCC_READ_PAGE_TIMEOUT:
            return "READ_PAGE_TIMEOUT";
        case HCC_WRITE_PAGE_TIMEOUT:
            return "WRITE_PAGE_TIMEOUT";
        case HCC_READ_SCAN_ENABLE:
            return "READ_SCAN_ENABLE";
        case HCC_WRITE_SCAN_ENABLE:
            return "WRITE_SCAN_ENABLE";
        case HCC_READ_PAGE_SCAN_ACTIVITY:
            return "READ_PAGE_SCAN_ACTIVITY";
        case HCC_WRITE_PAGE_SCAN_ACTIVITY:
            return "WRITE_PAGE_SCAN_ACTIVITY";
        case HCC_READ_INQ_SCAN_ACTIVITY:
            return "READ_INQ_SCAN_ACTIVITY";
        case HCC_WRITE_INQ_SCAN_ACTIVITY:
            return "WRITE_INQ_SCAN_ACTIVITY";
        case HCC_READ_AUTH_ENABLE:
            return "READ_AUTH_ENABLE";
        case HCC_WRITE_AUTH_ENABLE:
            return "WRITE_AUTH_ENABLE";
        case HCC_READ_ENCRYPT_MODE:
            return "READ_ENCRYPT_MODE";
        case HCC_WRITE_ENCRYPT_MODE:
            return "WRITE_ENCRYPT_MODE";
        case HCC_READ_CLASS_OF_DEVICE:
            return "READ_CLASS_OF_DEVICE";
        case HCC_WRITE_CLASS_OF_DEVICE:
            return "WRITE_CLASS_OF_DEVICE";
        case HCC_READ_VOICE_SETTING:
            return "READ_VOICE_SETTING";
        case HCC_WRITE_VOICE_SETTING:
            return "WRITE_VOICE_SETTING";
        case HCC_READ_AUTO_FLUSH_TIMEOUT:
            return "READ_AUTO_FLUSH_TIMEOUT";
        case HCC_WRITE_AUTO_FLUSH_TIMEOUT:
            return "WRITE_AUTO_FLUSH_TIMEOUT";
        case HCC_READ_NUM_BC_RETRANSMIT:
            return "READ_NUM_BC_RETRANSMIT";
        case HCC_WRITE_NUM_BC_RETRANSMIT:
            return "WRITE_NUM_BC_RETRANSMIT";
        case HCC_READ_HOLD_MODE_ACTIVITY:
            return "READ_HOLD_MODE_ACTIVITY";
        case HCC_WRITE_HOLD_MODE_ACTIVITY:
            return "WRITE_HOLD_MODE_ACTIVITY";
        case HCC_READ_XMIT_POWER_LEVEL:
            return "READ_XMIT_POWER_LEVEL";
        case HCC_READ_SCO_FC_ENABLE:
            return "READ_SCO_FC_ENABLE";
        case HCC_WRITE_SCO_FC_ENABLE:
            return "WRITE_SCO_FC_ENABLE";
        case HCC_SET_CTRLR_TO_HOST_FLOW_CTRL:
            return "SET_CTRLR_TO_HOST_FLOW_CTRL";
        case HCC_HOST_BUFFER_SIZE:
            return "HOST_BUFFER_SIZE";
        case HCC_HOST_NUM_COMPLETED_PACKETS:
            return "HOST_NUM_COMPLETED_PACKETS";
        case HCC_READ_LINK_SUPERV_TIMEOUT:
            return "READ_LINK_SUPERV_TIMEOUT";
        case HCC_WRITE_LINK_SUPERV_TIMEOUT:
            return "WRITE_LINK_SUPERV_TIMEOUT";
        case HCC_READ_NUM_IAC:
            return "READ_NUM_IAC";
        case HCC_READ_CURRENT_IAC_LAP:
            return "READ_CURRENT_IAC_LAP";
        case HCC_WRITE_CURRENT_IAC_LAP:
            return "WRITE_CURRENT_IAC_LAP";
        case HCC_READ_PAGE_SCAN_PERIOD_MODE:
            return "READ_PAGE_SCAN_PERIOD_MODE";
        case HCC_WRITE_PAGE_SCAN_PERIOD_MODE:
            return "WRITE_PAGE_SCAN_PERIOD_MODE";
        case HCC_READ_PAGE_SCAN_MODE:
            return "READ_PAGE_SCAN_MODE";
        case HCC_WRITE_PAGE_SCAN_MODE:
            return "WRITE_PAGE_SCAN_MODE";
        case HCC_READ_LOCAL_VERSION:
            return "READ_LOCAL_VERSION";
        case HCC_READ_LOCAL_FEATURES:
            return "READ_LOCAL_FEATURES";
        case HCC_READ_BUFFER_SIZE:
            return "READ_BUFFER_SIZE";
        case HCC_READ_COUNTRY_CODE:
            return "READ_COUNTRY_CODE";
        case HCC_READ_BD_ADDR:
            return "READ_BD_ADDR";
        case HCC_READ_FAILED_CONTACT_COUNT:
            return "READ_FAILED_CONTACT_COUNT";
        case HCC_RESET_FAILED_CONTACT_COUNT:
            return "RESET_FAILED_CONTACT_COUNT";
        case HCC_GET_LINK_QUALITY:
            return "GET_LINK_QUALITY";
        case HCC_READ_RSSI:
            return "READ_RSSI";
        case HCC_READ_LOOPBACK_MODE:
            return "READ_LOOPBACK_MODE";
        case HCC_WRITE_LOOPBACK_MODE:
            return "WRITE_LOOPBACK_MODE";
        case HCC_ENABLE_DUT:
            return "ENABLE_DUT";
        case HCC_CREATE_CONNECTION_CANCEL:
            return "CREATE_CONNECTION_CANCEL";
        case HCC_REM_NAME_REQ_CANCEL:
            return "REM_NAME_REQ_CANCEL";
        case HCC_READ_REMOTE_EXT_FEATURES:
            return "READ_REMOTE_EXT_FEATURES";
        case HCC_READ_LMP_HANDLE:
            return "READ_LMP_HANDLE";
        case HCC_EXCHANGE_FIXED_INFO:
            return "EXCHANGE_FIXED_INFO";
        case HCC_EXCHANGE_ALIAS_INFO:
            return "EXCHANGE_ALIAS_INFO";
        case HCC_PRIVATE_PAIRING_REQ_REPL:
            return "PRIVATE_PAIRING_REQ_REPL";
        case HCC_PRIVATE_PAIRING_REQ_NEG_REPL:
            return "PRIVATE_PAIRING_REQ_NEG_REPL";
        case HCC_GENERATED_ALIAS:
            return "GENERATED_ALIAS";
        case HCC_ALIAS_ADDRESS_REQ_REPL:
            return "ALIAS_ADDRESS_REQ_REPL";
        case HCC_ALIAS_ADDRESS_REQ_NEG_REPL:
            return "ALIAS_ADDRESS_REQ_NEG_REPL";
        case HCC_SETUP_SYNC_CONNECTION:
            return "SETUP_SYNC_CONNECTION";
        case HCC_ACCEPT_SYNC_CON_REQ:
            return "ACCEPT_SYNC_CON_REQ";
        case HCC_REJECT_SYNC_CON_REQ:
            return "REJECT_SYNC_CON_REQ";
        case HCC_READ_DEFAULT_LINK_POLICY:
            return "READ_DEFAULT_LINK_POLICY";
        case HCC_WRITE_DEFAULT_LINK_POLICY:
            return "WRITE_DEFAULT_LINK_POLICY";
        case HCC_FLOW_SPECIFICATION:
            return "FLOW_SPECIFICATION";
        case HCC_SET_AFH_HOST_CHAN_CLASS:
            return "SET_AFH_HOST_CHAN_CLASS";
        case HCC_READ_INQ_SCAN_TYPE:
            return "READ_INQ_SCAN_TYPE";
        case HCC_WRITE_INQ_SCAN_TYPE:
            return "WRITE_INQ_SCAN_TYPE";
        case HCC_READ_INQ_MODE:
            return "READ_INQ_MODE";
        case HCC_WRITE_INQ_MODE:
            return "WRITE_INQ_MODE";
        case HCC_READ_PAGE_SCAN_TYPE:
            return "READ_PAGE_SCAN_TYPE";
        case HCC_WRITE_PAGE_SCAN_TYPE:
            return "WRITE_PAGE_SCAN_TYPE";
        case HCC_READ_AFH_CHAN_ASSESS_MODE:
            return "READ_AFH_CHAN_ASSESS_MODE";
        case HCC_WRITE_AFH_CHAN_ASSESS_MODE:
            return "WRITE_AFH_CHAN_ASSESS_MODE";
        case HCC_READ_SIMPLE_PAIRING_MODE:
            return "HCC_READ_SIMPLE_PAIRING_MODE";
        case HCC_WRITE_SIMPLE_PAIRING_MODE:
            return "HCC_WRITE_SIMPLE_PAIRING_MODE";
        case HCC_READ_LOCAL_COMMANDS:
            return "READ_LOCAL_COMMANDS";
        case HCC_READ_LOCAL_EXT_FEATURES:
            return "READ_LOCAL_EXT_FEATURES";
        case HCC_READ_AFH_CHANNEL_MAP:
            return "READ_AFH_CHANNEL_MAP";
        case HCC_READ_CLOCK:
            return "READ_CLOCK";
        case HCC_MTK6622_SET_BDADDR:
            return "HCC_MTK6622_SET_BDADDR";
        case HCC_MTK6622_SET_RADIO:    
            return "HCC_MTK6622_SET_RADIO";
        case HCC_MTK6622_SET_SLEEP:    
            return "HCC_MTK6622_SET_SLEEP";
        case HCC_MTK6622_WAKEUP:       
            return "HCC_MTK6622_WAKEUP";
        case HCC_MTK6622_HOST_WAKEUP:  
            return "HCC_MTK6622_HOST_WAKEUP";
        case HCC_MTK6622_SET_BAUD:     
            return "HCC_MTK6622_SET_BAUD";
        case HCC_MTK6622_SET_PCM:
            return "HCC_MTK6622_SET_PCM";
        case HCC_MTK6622_TEST_CONTROL: 
            return "HCC_MTK6622_TEST_CONTROL";
        case HCC_MTK6622_FW_PATCH:     
            return "HCC_MTK6622_FW_PATCH";
        case HCC_MTK6622_FW_PATCH_MODE:
            return "HCC_MTK6622_FW_PATCH_MODE";
        case HCC_MTK6622_SET_XO_TRIM:
            return "HCC_MTK6622_SET_XO_TRIM";
    }

    return "unknown command";
}

const char *L2CAPConnStatusStr(U16 Status)
{
    switch (Status) {
        case L2CONN_ACCEPTED:
            return "Accepted";

        case L2CONN_PENDING:
            return "Pending";

        case L2CONN_REJECT_PSM_NOT_SUPPORTED:
            return "Rejected Psm Not Supported";

        case L2CONN_REJECT_SECURITY_BLOCK:
            return "Rejected Security Block";

        case L2CONN_REJECT_NO_RESOURCES:
            return "Rejected No Resources";
    }
    return "UNKNOWN";
}


#endif
