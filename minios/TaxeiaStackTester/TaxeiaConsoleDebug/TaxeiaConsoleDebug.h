#ifndef __DEBUG_H_
#define __DEBUG_H_
#include "HAL/LocalOutput.h"
#include "Btconfig.h"
#include "BTType.h"

void Taxeia_Assert(const char *expression, const char *file, U16 line);
#define ASSERT(exp)  (((exp) != 0) ? (void)0 : Taxeia_Assert(#exp,__FILE__,(U16)__LINE__))

#ifdef TAXEIA_DEBUG
void HexDisplay(const char *Source, const U8 *Buffer, U16 Len);
void EdrCmdShow(U16 cmd);
void EdrEvtShow(U8 evt, U8 len, U8 *dat);

//#define Taxeia_Report(...) do { LocalOutput(##__VA_ARGS__);} while(0)
#define Taxeia_Report LocalOutput

const char *HciCommandStr(U16 cmd);
const char *HciEventStr(U8 Event);
void HciEventParseStr(U8 Event, U8 len, U8 *dat, char *output);
const char *L2CAPSignalOpcodeStr(U8 Opcode);
const char *L2CAPConnStatusStr(U16 Status);
const char *HciStatusStr(U8 Status);

#define CMD_SHOW(x) EdrCmdShow(x)
#define EVT_SHOW(x,y,z) EdrEvtShow(x,y,z)

    #ifdef TAXEIA_RAW_DEBUG
    #define HEXDISPLAY(x, y, z) HexDisplay(x, y, z)
    #else
    #define HEXDISPLAY(x, y, z)
    #endif

#else

#define CMD_SHOW(x)
#define EVT_SHOW(x,y,z)
#define HEXDISPLAY(x, y, z)
#define Taxeia_Report(str, ...)

//#define HciCommandStr(x)
//#define HciEventStr(x)
//#define L2CAPSignalOpcodeStr(x)
//#define L2CAPConnStatusStr(x)
//#define HciStatusStr(x)

#endif

#endif

