#ifndef __RFCOMM_HH__
#define __RFCOMM_HH__
#include "../BTType.h"
#include "../GAP/LOCAL_DEV/Bt__LocalDevice.h"

struct rfcomm_session {
    unsigned short    state;
    /* Default DLC parameters */
    unsigned short aclHandle : 12;
    unsigned short RFCOMM_CFC_ENABLED : 1;
    unsigned short initiator : 1;
    unsigned short reserved : 2;
};

struct rfcomm_dlc {
    unsigned char state;
    unsigned char dlci;
    unsigned char rx_credits;
    unsigned char tx_credits;
    unsigned char v24_sig;
    unsigned char tx_len;

    struct rfcomm_session *session;
    unsigned char *tx_dat;
    
    void (*data_ready)(struct rfcomm_dlc *d, U16 len, U8 *dat);
    void (*state_change)(struct rfcomm_dlc *d, int err);
    void (*modem_status)(struct rfcomm_dlc *d, U8 v24_sig);

    unsigned short mtu;

    unsigned short RFCOMM_CFC_ENABLED : 1;
    unsigned short RFCOMM_MSCEX_RX : 1;
    unsigned short RFCOMM_MSCEX_TX : 1;
    unsigned short RFCOMM_MSCEX_NEED_TX : 1;
    unsigned short  RFCOMM_RX_THROTTLED : 1;
    unsigned short  RFCOMM_TX_THROTTLED : 1;
    unsigned short  RFCOMM_TIMED_OUT : 1;
    unsigned short  RFCOMM_MSC_PENDING : 1;
    unsigned short  RFCOMM_SEC_PENDING : 1;
    unsigned short  RFCOMM_AUTH_PENDING : 1;
    unsigned short  RFCOMM_AUTH_ACCEPT : 1;
    unsigned short  RFCOMM_AUTH_REJECT : 1;
    unsigned short  RFCOMM_DEFER_SETUP : 1;
    unsigned short  RFCOMM_ENC_DROP : 1;
};

/* Connection and socket states */
enum {
    BT_CONNECTED = 1, /* Equal to TCP_ESTABLISHED to make net code happy */
    BT_OPEN,
    BT_BOUND,
    BT_LISTEN,
    BT_CONNECT,
    BT_CONNECT2,
    BT_CONFIG, //outgoing, wait to send pn
    BT_DISCONN,
    BT_CLOSED
};

#define RFCOMM_DEFAULT_MTU (LL_MTU-5)
#define RFCOMM_DEFAULT_CREDITS 1

#define RFCOMM_MAX_L2CAP_MTU	1013
#define RFCOMM_MAX_CREDITS	40

#define RFCOMM_SABM	0x2f
#define RFCOMM_DISC	0x43
#define RFCOMM_UA	0x63
#define RFCOMM_DM	0x0f
#define RFCOMM_UIH	0xef

#define RFCOMM_TEST	0x08
#define RFCOMM_FCON	0x28
#define RFCOMM_FCOFF	0x18
#define RFCOMM_MSC	0x38
#define RFCOMM_RPN	0x24
#define RFCOMM_RLS	0x14
#define RFCOMM_PN	0x20
#define RFCOMM_NSC	0x04

#define RFCOMM_V24_FC	0x02
#define RFCOMM_V24_RTC	0x04
#define RFCOMM_V24_RTR	0x08
#define RFCOMM_V24_IC	0x40
#define RFCOMM_V24_DV	0x80

#define RFCOMM_RPN_BR_2400	0x0
#define RFCOMM_RPN_BR_4800	0x1
#define RFCOMM_RPN_BR_7200	0x2
#define RFCOMM_RPN_BR_9600	0x3
#define RFCOMM_RPN_BR_19200	0x4
#define RFCOMM_RPN_BR_38400	0x5
#define RFCOMM_RPN_BR_57600	0x6
#define RFCOMM_RPN_BR_115200	0x7
#define RFCOMM_RPN_BR_230400	0x8

#define RFCOMM_RPN_DATA_5	0x0
#define RFCOMM_RPN_DATA_6	0x1
#define RFCOMM_RPN_DATA_7	0x2
#define RFCOMM_RPN_DATA_8	0x3

#define RFCOMM_RPN_STOP_1	0
#define RFCOMM_RPN_STOP_15	1

#define RFCOMM_RPN_PARITY_NONE	0x0
#define RFCOMM_RPN_PARITY_ODD	0x1
#define RFCOMM_RPN_PARITY_EVEN	0x3
#define RFCOMM_RPN_PARITY_MARK	0x5
#define RFCOMM_RPN_PARITY_SPACE	0x7

#define RFCOMM_RPN_FLOW_NONE	0x00

#define RFCOMM_RPN_XON_CHAR	0x11
#define RFCOMM_RPN_XOFF_CHAR	0x13

#define RFCOMM_RPN_PM_BITRATE		0x0001
#define RFCOMM_RPN_PM_DATA		0x0002
#define RFCOMM_RPN_PM_STOP		0x0004
#define RFCOMM_RPN_PM_PARITY		0x0008
#define RFCOMM_RPN_PM_PARITY_TYPE	0x0010
#define RFCOMM_RPN_PM_XON		0x0020
#define RFCOMM_RPN_PM_XOFF		0x0040
#define RFCOMM_RPN_PM_FLOW		0x3F00

#define RFCOMM_RPN_PM_ALL		0x3F7F

/* MSC exchange flags */
//#define RFCOMM_MSCEX_TX     1
//#define RFCOMM_MSCEX_RX     2
#define RFCOMM_MSCEX_OK     (RFCOMM_MSCEX_TX + RFCOMM_MSCEX_RX)

/* CFC states */
//#define RFCOMM_CFC_UNKNOWN  -1
//#define RFCOMM_CFC_DISABLED 0
//#define RFCOMM_CFC_ENABLED  RFCOMM_MAX_CREDITS

struct rfcomm_hdr {
    unsigned char addr;
    unsigned char ctrl;
    unsigned char len;    /* Actual size can be 2 bytes */
} ;

struct rfcomm_cmd {
    unsigned char addr;
    unsigned char ctrl;
    unsigned char len;
    unsigned char fcs;
} ;

struct rfcomm_mcc {
    unsigned char type;
    unsigned char len;
} ;

struct rfcomm_pn {
    unsigned char  dlci;
    unsigned char  flow_ctrl;
    unsigned char  priority;
    unsigned char  ack_timer;
    unsigned char mtul;
    unsigned char mtuh;
    unsigned char  max_retrans;
    unsigned char  credits;
} ;

struct rfcomm_rpn {
    unsigned char  dlci;
    unsigned char  bit_rate;
    unsigned char  line_settings;
    unsigned char  flow_ctrl;
    unsigned char  xon_char;
    unsigned char  xoff_char;
    unsigned char param_maskl;
    unsigned char param_maskh;
} ;

struct rfcomm_rls {
    unsigned char  dlci;
    unsigned char  status;
} ;

struct rfcomm_msc {
    unsigned char  dlci;
    unsigned char  v24_sig;
} ;

typedef void (*RFCOMM_DATARDY)(struct rfcomm_dlc *d, U16 len, U8 *dat);
typedef void (*RFCOMM_STSCHG)(struct rfcomm_dlc *d, int err);


void Bt_RfCommChannelRegister(U8 channel, RFCOMM_DATARDY data_rdy, RFCOMM_STSCHG sts_chg);
void Bt__RfCommInit(void);
void Bt__RfCommDeInit(void);
void Bt__RfCommDisconnect(struct rfcomm_dlc *d);
void Bt__RfCommConnect(U16 aclHandle, U8 chanel_number,RFCOMM_DATARDY data_ready, RFCOMM_STSCHG status_change);
void Bt__RfCommCB(FinishNotifyCallback fc);
void Bt_RfCommScheduler(void);


#endif
