#include "BtConfig.h"
#include "../TaxeiaConsoleDebug/TaxeiaConsoledebug.h"
#include "../GAP/L2CAP/Bt__LLCAP_V2.h"
#include "../CORE/BTCore_V2.h"
#include "../rfcomm/rfcomm_V2.h"
#include <stdio.h>
#include <string.h>

#if  (PROFILE_TYPE == PROFILE_TYPE_HID_HFP  || PROFILE_TYPE == PROFILE_TYPE_HFP || PROFILE_TYPE == PROFILE_TYPE_SPP || PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
extern void LLSignalConReq(U16 aclHandle, U16 psm);
extern void LLSignalDisConReq(U16 aclHandle, U8 id, U16 tcid, U16 scid);
extern U16 L2ChannelSearchAcidByTcid(U16 cid);

const unsigned char rfcomm_crc_table[256] = {
    0x00, 0x91, 0xe3, 0x72, 0x07, 0x96, 0xe4, 0x75,
    0x0e, 0x9f, 0xed, 0x7c, 0x09, 0x98, 0xea, 0x7b,
    0x1c, 0x8d, 0xff, 0x6e, 0x1b, 0x8a, 0xf8, 0x69,
    0x12, 0x83, 0xf1, 0x60, 0x15, 0x84, 0xf6, 0x67,

    0x38, 0xa9, 0xdb, 0x4a, 0x3f, 0xae, 0xdc, 0x4d,
    0x36, 0xa7, 0xd5, 0x44, 0x31, 0xa0, 0xd2, 0x43,
    0x24, 0xb5, 0xc7, 0x56, 0x23, 0xb2, 0xc0, 0x51,
    0x2a, 0xbb, 0xc9, 0x58, 0x2d, 0xbc, 0xce, 0x5f,

    0x70, 0xe1, 0x93, 0x02, 0x77, 0xe6, 0x94, 0x05,
    0x7e, 0xef, 0x9d, 0x0c, 0x79, 0xe8, 0x9a, 0x0b,
    0x6c, 0xfd, 0x8f, 0x1e, 0x6b, 0xfa, 0x88, 0x19,
    0x62, 0xf3, 0x81, 0x10, 0x65, 0xf4, 0x86, 0x17,

    0x48, 0xd9, 0xab, 0x3a, 0x4f, 0xde, 0xac, 0x3d,
    0x46, 0xd7, 0xa5, 0x34, 0x41, 0xd0, 0xa2, 0x33,
    0x54, 0xc5, 0xb7, 0x26, 0x53, 0xc2, 0xb0, 0x21,
    0x5a, 0xcb, 0xb9, 0x28, 0x5d, 0xcc, 0xbe, 0x2f,

    0xe0, 0x71, 0x03, 0x92, 0xe7, 0x76, 0x04, 0x95,
    0xee, 0x7f, 0x0d, 0x9c, 0xe9, 0x78, 0x0a, 0x9b,
    0xfc, 0x6d, 0x1f, 0x8e, 0xfb, 0x6a, 0x18, 0x89,
    0xf2, 0x63, 0x11, 0x80, 0xf5, 0x64, 0x16, 0x87,

    0xd8, 0x49, 0x3b, 0xaa, 0xdf, 0x4e, 0x3c, 0xad,
    0xd6, 0x47, 0x35, 0xa4, 0xd1, 0x40, 0x32, 0xa3,
    0xc4, 0x55, 0x27, 0xb6, 0xc3, 0x52, 0x20, 0xb1,
    0xca, 0x5b, 0x29, 0xb8, 0xcd, 0x5c, 0x2e, 0xbf,

    0x90, 0x01, 0x73, 0xe2, 0x97, 0x06, 0x74, 0xe5,
    0x9e, 0x0f, 0x7d, 0xec, 0x99, 0x08, 0x7a, 0xeb,
    0x8c, 0x1d, 0x6f, 0xfe, 0x8b, 0x1a, 0x68, 0xf9,
    0x82, 0x13, 0x61, 0xf0, 0x85, 0x14, 0x66, 0xf7,

    0xa8, 0x39, 0x4b, 0xda, 0xaf, 0x3e, 0x4c, 0xdd,
    0xa6, 0x37, 0x45, 0xd4, 0xa1, 0x30, 0x42, 0xd3,
    0xb4, 0x25, 0x57, 0xc6, 0xb3, 0x22, 0x50, 0xc1,
    0xba, 0x2b, 0x59, 0xc8, 0xbd, 0x2c, 0x5e, 0xcf
};

/* CRC on 2 bytes */
#define __crc(data) (rfcomm_crc_table[rfcomm_crc_table[0xff ^ data[0]] ^ data[1]])

/* FCS on 2 bytes */
static U8 __fcs(U8 *data)
{
    return 0xff - __crc(data);
}

/* FCS on 3 bytes */
static U8 __fcs2(U8 *data)
{
    return 0xff - rfcomm_crc_table[__crc(data) ^ data[2]];
}

/* Check FCS */
static int __check_fcs(U8 *data, int type, U8 fcs)
{
    U8 f = __crc(data);

    if (type != RFCOMM_UIH)
        f = rfcomm_crc_table[f ^ data[2]];

    return rfcomm_crc_table[f ^ fcs] != 0xcf;
}

#define __get_dlci(b)     ((b & 0xfc) >> 2)
#define __get_channel(b)  ((b & 0xf8) >> 3)
#define __get_dir(b)      ((b & 0x04) >> 2)
#define __get_type(b)     ((b & 0xef))

#define __test_ea(b)      ((b & 0x01))
#define __test_cr(b)      ((b & 0x02))
#define __test_pf(b)      ((b & 0x10))

#define __addr(cr, dlci)       (((dlci & 0x3f) << 2) | (cr << 1) | 0x01)
#define __ctrl(type, pf)       (((type & 0xef) | (pf << 4)))
#define __dlci(dir, chn)       (((chn & 0x1f) << 1) | dir)
#define __srv_channel(dlci)    (dlci >> 1)
#define __dir(dlci)            (dlci & 0x01)

#define __len8(len)       (((len) << 1) | 1)
#define __len16(len)      ((len) << 1)

#define __mcc_type(cr, type)   (((type << 2) | (cr << 1) | 0x01))
#define __get_mcc_type(b) ((b & 0xfc) >> 2)
#define __get_mcc_len(b)  ((b & 0xfe) >> 1)

/* RPN macros */
#define __rpn_line_settings(data, stop, parity)  ((data & 0x3) | ((stop & 0x1) << 2) | ((parity & 0x7) << 3))
#define __get_rpn_data_bits(line) ((line) & 0x3)
#define __get_rpn_stop_bits(line) (((line) >> 2) & 0x1)
#define __get_rpn_parity(line)    (((line) >> 3) & 0x7)

extern void L2ChannelSearchTCidHdlByPsm(U16 psm, U16 *cid, U16 *acl);
extern void L2ChannelSendData(U16 aclHandle, U16 cid, U16 len, U8 *dat);

typedef struct
{
    U16 aclHdl;
    U16 cid; //target cid
//parm[0]: 1: connect sucess 0: disconnect or connect fail
//parm[1]: 1: hidc 0:hidi
    FinishNotifyCallback rffc;
}RFCOMM_ST;

RFCOMM_ST rfst;

#define MAX_SESSION 1
struct rfcomm_session rf_session[MAX_SESSION];

#define MAX_DLC 1
struct rfcomm_dlc rf_dlc[MAX_DLC];

#define MAX_CHANNEL 1

struct rfcomm_channel
{
    U8 id;
    RFCOMM_DATARDY data_ready;
    RFCOMM_STSCHG status_change;
};

struct rfcomm_channel server_channel[MAX_CHANNEL];

static void rfcomm_send_frame(struct rfcomm_session *s, U8 *data, int len)
{    
    L2ChannelSendData(rfst.aclHdl, rfst.cid, len, data);       
}

void rfcomm_send_frame2(U8 hdr_len, U8 *hdr, U16 dat_len, U8 *dat, U8 fcs)
{
    U16 i;
    U8 *vdat;


    vdat = (U8*)malloc(hdr_len + dat_len + 1);
//    printf("vdat = %x\n", vdat);
    ASSERT(vdat);
    
    for(i = 0; i < hdr_len; i++)
        vdat[i] = hdr[i];
    
    for(i = 0; i < dat_len; i++)
        vdat[i+hdr_len] =  dat[i];

    vdat[hdr_len + dat_len] = fcs;
    
    
    L2ChannelSendData(rfst.aclHdl, rfst.cid, hdr_len + dat_len + 1, vdat);       

    free(vdat);
}

static void rfcomm_send_cmd(struct rfcomm_session *s, struct rfcomm_cmd *cmd)
{
    rfcomm_send_frame(s, (void *) cmd, sizeof(*cmd));
}


static void rfcomm_send_ua(struct rfcomm_session *s, U8 dlci)
{
    struct rfcomm_cmd cmd;

    cmd.addr = __addr(!s->initiator, dlci);
    cmd.ctrl = __ctrl(RFCOMM_UA, 1);
    cmd.len  = __len8(0);
    cmd.fcs  = __fcs2((U8 *) &cmd);

    rfcomm_send_cmd(s, &cmd);
}


static void rfcomm_send_sabm(struct rfcomm_session *s, U8 dlci)
{
    struct rfcomm_cmd cmd;

    cmd.addr = __addr(s->initiator, dlci);
    cmd.ctrl = __ctrl(RFCOMM_SABM, 1);
    cmd.len  = __len8(0);
    cmd.fcs  = __fcs2((U8 *) &cmd);

    rfcomm_send_cmd(s, &cmd);
}

static void rfcomm_send_disc(struct rfcomm_session *s, U8 dlci)
{
    struct rfcomm_cmd cmd;

    cmd.addr = __addr(s->initiator, dlci);
    cmd.ctrl = __ctrl(RFCOMM_DISC, 1);
    cmd.len  = __len8(0);
    cmd.fcs  = __fcs2((U8 *) &cmd);

    rfcomm_send_cmd(s, &cmd);
}

static void rfcomm_send_dm(struct rfcomm_session *s, U8 dlci)
{
    struct rfcomm_cmd cmd;

    cmd.addr = __addr(!s->initiator, dlci);
    cmd.ctrl = __ctrl(RFCOMM_DM, 1);
    cmd.len  = __len8(0);
    cmd.fcs  = __fcs2((U8 *) &cmd);

    rfcomm_send_cmd(s, &cmd);
}

static struct rfcomm_dlc *rfcomm_dlc_get(struct rfcomm_session *s, U8 dlci)
{
    U8 i;

    for(i = 0; i < MAX_DLC; i++)
    {
        if(rf_dlc[i].session == s && rf_dlc[i].dlci == dlci && rf_dlc[i].state != BT_CLOSED) 
        {
            return &rf_dlc[i];
        }
    }

    return 0;
}


static void rfcomm_dlc_clear_state(struct rfcomm_dlc *d)
{
    d->state      = BT_OPEN;
    d->mtu        = RFCOMM_DEFAULT_MTU;
    d->rx_credits = 0;//RFCOMM_DEFAULT_CREDITS;
    d->tx_dat = 0;
    d->tx_len = 0;
    d->RFCOMM_CFC_ENABLED = 0;
    d->v24_sig = 0x8D;
    d->RFCOMM_MSCEX_RX = 0;
    d->RFCOMM_MSCEX_TX = 0;
    d->RFCOMM_MSCEX_NEED_TX = 0;
    d->RFCOMM_RX_THROTTLED = 0;
    d->RFCOMM_TX_THROTTLED = 0;
    d->RFCOMM_TIMED_OUT = 0;
    d->RFCOMM_MSC_PENDING = 0;
    d->RFCOMM_SEC_PENDING = 0;
    d->RFCOMM_AUTH_PENDING = 0;
    d->RFCOMM_AUTH_ACCEPT = 0;
    d->RFCOMM_AUTH_REJECT = 0;
    d->RFCOMM_DEFER_SETUP = 0;
    d->RFCOMM_ENC_DROP = 0;
    d->data_ready = 0;
    d->state_change = 0;
    d->modem_status = 0;
}

static struct rfcomm_dlc *rfcomm_dlc_alloc()
{
    U8 i;

    for(i = 0; i < MAX_DLC; i++)
    {
        if(rf_dlc[i].state == BT_CLOSED) 
        {
            rfcomm_dlc_clear_state(&rf_dlc[i]);

            return &rf_dlc[i];
        }
    }

    return 0;
}

static void rfcomm_dlc_all_close(void)
{
    U8 i;

    for(i = 0; i < MAX_DLC; i++)
    {
        rf_dlc[i].state = BT_CLOSED; 
        if(rf_dlc[i].state_change)
            rf_dlc[i].state_change(&rf_dlc[i], 0);
    }
}

static void __rfcomm_dlc_close(struct rfcomm_dlc *d, int err)
{
    struct rfcomm_session *s = d->session;
    if (!s)
        return;

    switch(d->state)
    {
        case BT_CONNECT:
            break;
        case BT_CONNECTED:
            d->state = BT_DISCONN;
            rfcomm_send_disc(s, d->dlci);
            break;
        case BT_OPEN:
            break;
        default:
            d->state = BT_CLOSED;
            if(d->state_change)
                d->state_change(d, 0);
            break;
    }

    return;
}

struct rfcomm_session *rfcomm_session_alloc(U16 aclHandle, U8 initiator, U8 state)
{
	U8 i;
    for(i = 0; i < MAX_SESSION; i++)
        if(rf_session[i].state == BT_CLOSED)
        {
            rf_session[i].initiator = initiator;
            rf_session[i].state = state;
            rf_session[i].aclHandle = aclHandle;
            return &rf_session[i];
        }

    return 0;
}

struct rfcomm_session *rfcomm_session_search(U16 aclHandle)
{
	U8 i; 
    for(i = 0; i < MAX_SESSION; i++)
        if(rf_session[i].aclHandle == aclHandle)
        {
            return &rf_session[i];
        }

    return 0;

}

static void rfcomm_recv_dm(struct rfcomm_session *s, U8 dlci)
{
    if (dlci) 
    {
        /* Data DLC */
        struct rfcomm_dlc *d = rfcomm_dlc_get(s, dlci);
        d->state = BT_CLOSED;
        __rfcomm_dlc_close(d, 0);
        s->state = BT_DISCONN;
        rfcomm_send_disc(s, 0);

    } 
    else 
        s->state = BT_CLOSED;
    return;
}

void rfcomm_dlc_accept(struct rfcomm_dlc *d)
{
    rfcomm_send_ua(d->session, d->dlci);

    d->state = BT_CONNECTED;
    if(d->state_change)
        d->state_change(d, 0);

    d->RFCOMM_MSCEX_NEED_TX = 1;
}

static void rfcomm_check_accept(struct rfcomm_dlc *d)
{
    rfcomm_dlc_accept(d);
}

/* ---- RFCOMM core layer callbacks ----
 *
 * called under rfcomm_lock()
 */
U8 rfcomm_connect_ind(struct rfcomm_session *s, U8 channel, struct rfcomm_dlc **d)
{
    U8 i;
    U8 result = 0;

    for(i = 0; i < MAX_CHANNEL; i++)
    {
        if(server_channel[i].id == channel)
        {
            //assign data_ready and state_change here
            /* Accept connection and return socket DLC */
            *d = rfcomm_dlc_alloc();

            if(*d)
            {
                (*d)->session = s;
                (*d)->data_ready = server_channel[i].data_ready;
                (*d)->state_change = server_channel[i].status_change;
                result = 1;
            }
        }
     }

    return result;
}

///////////////////////////////////////////////////////////
//TBD
static void rfcomm_send_pn(struct rfcomm_session *s, int cr, struct rfcomm_dlc *d);

static void rfcomm_process_connect(struct rfcomm_session *s)
{
    U8 i;
    for(i = 0; i < MAX_DLC; i++)
    {
        if(rf_dlc[i].session == s && rf_dlc[i].state == BT_CONFIG) 
        {
            rfcomm_send_pn(s, 1, &rf_dlc[i]);
            return;
        }
    }

}
///////////////////////////////////////////////////////////

static void rfcomm_recv_sabm(struct rfcomm_session *s, U8 dlci)
{
    struct rfcomm_dlc *d;

    if (!dlci) {
        rfcomm_send_ua(s, 0);

        if (s->state == BT_OPEN) {
            s->state = BT_CONNECTED;
            rfcomm_process_connect(s);
        }
        return ;
    }

    /* Check if DLC exists */
    d = rfcomm_dlc_get(s, dlci);
    if (d) {
        if (d->state == BT_OPEN) {
            /* DLC was previously opened by PN request */
            rfcomm_check_accept(d);
        }
        return ;
    }

    /* Notify socket layer about incoming connection */
    if (rfcomm_connect_ind(s, __srv_channel(dlci), &d)) {
        d->dlci = dlci;
        rfcomm_check_accept(d);
    } else {
        rfcomm_send_dm(s, dlci);
    }

    return;
}

static int rfcomm_apply_pn(struct rfcomm_dlc *d, int cr, struct rfcomm_pn *pn)
{
    struct rfcomm_session *s = d->session;

    if ((pn->flow_ctrl == 0xf0 && s->RFCOMM_CFC_ENABLED) ||
            pn->flow_ctrl == 0xe0) {
        d->RFCOMM_CFC_ENABLED = 1;
        d->tx_credits = pn->credits;
    } else {
        d->RFCOMM_CFC_ENABLED = 0;
        d->RFCOMM_TX_THROTTLED = 1;
    }

    d->mtu = pn->mtul + (pn->mtuh << 8);

    if (cr && d->mtu > RFCOMM_DEFAULT_MTU)
        d->mtu = RFCOMM_DEFAULT_MTU;

    return 0;
}

static void rfcomm_send_pn(struct rfcomm_session *s, int cr, struct rfcomm_dlc *d)
{
    struct rfcomm_hdr *hdr;
    struct rfcomm_mcc *mcc;
    struct rfcomm_pn  *pn;
    U8 buf[16];
    U8 *ptr = buf;

    hdr = (void *) ptr;
    ptr += sizeof(*hdr);
    hdr->addr = __addr(s->initiator, 0);
    hdr->ctrl = __ctrl(RFCOMM_UIH, 0);
    hdr->len  = __len8(sizeof(*mcc) + sizeof(*pn));

    mcc = (void *) ptr;
    ptr += sizeof(*mcc);
    mcc->type = __mcc_type(cr, RFCOMM_PN);
    mcc->len  = __len8(sizeof(*pn));

    pn = (void *) ptr;
    ptr += sizeof(*pn);
    pn->dlci        = d->dlci;
    pn->priority    = 7;
    pn->ack_timer   = 0;
    pn->max_retrans = 0;

    if (s->RFCOMM_CFC_ENABLED) {
        pn->flow_ctrl = cr ? 0xf0 : 0xe0;
        pn->credits = 0;//RFCOMM_DEFAULT_CREDITS;
    } else {
        pn->flow_ctrl = 0;
        pn->credits   = 0;
    }

    pn->mtul = d->mtu & 0xff;
    pn->mtuh = d->mtu >> 8;

    *ptr = __fcs(buf);
    ptr++;

    rfcomm_send_frame(s, buf, (int)(ptr - buf));
}


static void rfcomm_send_nsc(struct rfcomm_session *s, int cr, U8 type)
{
    struct rfcomm_hdr *hdr;
    struct rfcomm_mcc *mcc;
    U8 buf[16];
    U8 *ptr = buf;

    hdr = (void *) ptr;
    ptr += sizeof(*hdr);
    hdr->addr = __addr(s->initiator, 0);
    hdr->ctrl = __ctrl(RFCOMM_UIH, 0);
    hdr->len  = __len8(sizeof(*mcc) + 1);

    mcc = (void *) ptr;
    ptr += sizeof(*mcc);
    mcc->type = __mcc_type(cr, RFCOMM_NSC);
    mcc->len  = __len8(1);

    /* Type that we didn't like */
    *ptr = __mcc_type(cr, type);
    ptr++;

    *ptr = __fcs(buf);
    ptr++;

    rfcomm_send_frame(s, buf, (int)(ptr - buf));
}


static void rfcomm_send_msc(struct rfcomm_session *s, int cr, U8 dlci, U8 v24_sig)
{
    struct rfcomm_hdr *hdr;
    struct rfcomm_mcc *mcc;
    struct rfcomm_msc *msc;
    U8 buf[16];
    U8 *ptr = buf;

    hdr = (void *) ptr;
    ptr += sizeof(*hdr);
    hdr->addr = __addr(s->initiator, 0);
    hdr->ctrl = __ctrl(RFCOMM_UIH, 0);
    hdr->len  = __len8(sizeof(*mcc) + sizeof(*msc));

    mcc = (void *) ptr;
    ptr += sizeof(*mcc);
    mcc->type = __mcc_type(cr, RFCOMM_MSC);
    mcc->len  = __len8(sizeof(*msc));

    msc = (void *) ptr;
    ptr += sizeof(*msc);
    msc->dlci    = __addr(1, dlci);
    msc->v24_sig = v24_sig | 0x01;

    *ptr = __fcs(buf);
    ptr++;

    rfcomm_send_frame(s, buf, (int)(ptr - buf));
}


static void rfcomm_send_credits(struct rfcomm_dlc *d, U8 credits)
{
    struct rfcomm_session *s;
    struct rfcomm_hdr *hdr;
    U8 buf[16], *ptr = buf;

    s = d->session;
    hdr = (void *) ptr;
    ptr += sizeof(*hdr);
    
    hdr->addr = __addr(s->initiator, d->dlci);
    hdr->ctrl = __ctrl(RFCOMM_UIH, 1);
    hdr->len  = __len8(0);

    *ptr = credits;
    ptr++;

    *ptr = __fcs(buf);
    ptr++;

    rfcomm_send_frame(s, buf, (int)(ptr - buf));
}

void rfcomm_dlc_close(struct rfcomm_dlc *d, int err)
{
    __rfcomm_dlc_close(d, err);

    return;
}

int rfcomm_dlc_send(struct rfcomm_dlc *d, U8 *dat, U16 len)
{
    U8 buf[4];
    
    struct rfcomm_hdr *hdr;
    U8 hdr_len;

    if (d->state != BT_CONNECTED)
        return 1;

    if (len > d->mtu)
        return 2;

    hdr = (struct rfcomm_hdr *)buf;
    
    if (len > 127) {
        *(U16*)&hdr->len = __len16(len);
        hdr_len = 4;
    } else {
        hdr->len = __len8(len);
        hdr_len = 3;
    }

    hdr->addr = __addr(d->session->initiator, d->dlci);
    hdr->ctrl = __ctrl(RFCOMM_UIH, 0);
    
    rfcomm_send_frame2(hdr_len, (U8*)hdr, len, dat, __fcs(buf));
    return 0;
}

int rfcomm_dlc_send_with_credit(struct rfcomm_dlc *d, U8 *dat, U16 len)
{
    U8 buf[5];
    
    struct rfcomm_hdr *hdr;
    U8 hdr_len;

    if (d->state != BT_CONNECTED)
        return 1;

    if (len > d->mtu)
        return 2;

    hdr = (struct rfcomm_hdr *)buf;
    
    if (len > 127) {
        *(U16*)&hdr->len = __len16(len);
        hdr_len = 5;
    } else {
        hdr->len = __len8(len);
        hdr_len = 4;
    }

    hdr->addr = __addr(d->session->initiator, d->dlci);
    hdr->ctrl = __ctrl(RFCOMM_UIH, 1);
    buf[hdr_len - 1] = RFCOMM_DEFAULT_CREDITS;
    
    rfcomm_send_frame2(hdr_len, (U8*)hdr, len, dat, __fcs(buf));
    return 0;
}


static int rfcomm_recv_ua(struct rfcomm_session *s, U8 dlci)
{
    if (dlci) 
    {
        /* Data channel */
        struct rfcomm_dlc *d = rfcomm_dlc_get(s, dlci);
        if (!d) {
            rfcomm_send_dm(s, dlci);
            return 0;
        }

        switch (d->state) {
            case BT_CONNECT:
                d->state = BT_CONNECTED;
                d->state_change(d, 0);
                //rfcomm_send_msc(s, 1, dlci, d->v24_sig);
                d->RFCOMM_MSCEX_NEED_TX = 1;
                break;

            case BT_DISCONN:
                d->state = BT_CLOSED;
                __rfcomm_dlc_close(d, 0);

                s->state = BT_DISCONN;
                rfcomm_send_disc(s, 0);

                break;
        }
    } 
    else 
    {
        if(s->state == BT_DISCONN)
        {
            s->state = BT_CLOSED;
            s->initiator = 0;
            LLSignalDisConReq(rfst.aclHdl, 0, rfst.cid, L2ChannelSearchAcidByTcid(rfst.cid));
        }
        else if(s->state == BT_CONNECT)
        {
            s->state = BT_CONNECTED;
            rfcomm_process_connect(s);
        }
        #if 0
        /* Control channel */
        switch (s->state) {
            case BT_CONNECT:
                s->state = BT_CONNECTED;
                rfcomm_process_connect(s);
                break;

            case BT_DISCONN:
                break;
        }
        #endif
    }
    return 0;
}

/* ---- RFCOMM frame reception ---- */
static int rfcomm_recv_msc(struct rfcomm_session *s, int cr, U8 *dat)
{
    struct rfcomm_msc *msc = (void *) dat;
    struct rfcomm_dlc *d;
    U8 dlci = __get_dlci(msc->dlci);

    d = rfcomm_dlc_get(s, dlci);
    if (!d)
        return 0;

    if (cr) {
        if ((msc->v24_sig & RFCOMM_V24_FC) && !d->RFCOMM_CFC_ENABLED)
            d->RFCOMM_TX_THROTTLED = 1;
        else
            d->RFCOMM_TX_THROTTLED = 0;

        if (d->modem_status)
            d->modem_status(d, msc->v24_sig);


        rfcomm_send_msc(s, 0, dlci, msc->v24_sig);

        d->RFCOMM_MSCEX_RX = 1;
    }
    else
    {
        d->RFCOMM_MSCEX_TX = 1;
    }
    return 0;
}

void rfcomm_send_rpn(struct rfcomm_session *s, int cr, U8 dlci,
                    U8 bit_rate, U8 data_bits, U8 stop_bits,
                    U8 parity, U8 flow_ctrl_settings,
                    U8 xon_char, U8 xoff_char, U16 param_mask)
{
    struct rfcomm_hdr *hdr;
    struct rfcomm_mcc *mcc;
    struct rfcomm_rpn *rpn;
    U8 buf[16], *ptr = buf;

    hdr = (void *) ptr;
    ptr += sizeof(*hdr);
    hdr->addr = __addr(s->initiator, 0);
    hdr->ctrl = __ctrl(RFCOMM_UIH, 0);
    hdr->len  = __len8(sizeof(*mcc) + sizeof(*rpn));

    mcc = (void *) ptr;
    ptr += sizeof(*mcc);
    mcc->type = __mcc_type(cr, RFCOMM_RPN);
    mcc->len  = __len8(sizeof(*rpn));

    rpn = (void *) ptr;
    ptr += sizeof(*rpn);
    rpn->dlci          = __addr(1, dlci);
    rpn->bit_rate      = bit_rate;
    rpn->line_settings = __rpn_line_settings(data_bits, stop_bits, parity);
    rpn->flow_ctrl     = flow_ctrl_settings;
    rpn->xon_char      = xon_char;
    rpn->xoff_char     = xoff_char;
    rpn->param_maskl    = param_mask & 0xff;
    rpn->param_maskh    = param_mask >> 8;

    *ptr = __fcs(buf);
    ptr++;

    rfcomm_send_frame(s, buf, (int)(ptr - buf));
}

static void rfcomm_recv_rpn(struct rfcomm_session *s, int cr, U8 len, U8 *dat)
{
    struct rfcomm_rpn *rpn = (void *) dat;
    U8 dlci = __get_dlci(rpn->dlci);

    U8 bit_rate  = 0;
    U8 data_bits = 0;
    U8 stop_bits = 0;
    U8 parity    = 0;
    U8 flow_ctrl = 0;
    U8 xon_char  = 0;
    U8 xoff_char = 0;
    U16 rpn_mask = RFCOMM_RPN_PM_ALL;

    if (!cr)
        return;

    if (len == 1) {
        /* This is a request, return default (according to ETSI TS 07.10) settings */
        bit_rate  = RFCOMM_RPN_BR_9600;
        data_bits = RFCOMM_RPN_DATA_8;
        stop_bits = RFCOMM_RPN_STOP_1;
        parity    = RFCOMM_RPN_PARITY_NONE;
        flow_ctrl = RFCOMM_RPN_FLOW_NONE;
        xon_char  = RFCOMM_RPN_XON_CHAR;
        xoff_char = RFCOMM_RPN_XOFF_CHAR;
        goto rpn_out;
    }

    /* Check for sane values, ignore/accept bit_rate, 8 bits, 1 stop bit,
     * no parity, no flow control lines, normal XON/XOFF chars */

    if (rpn->param_maskl & RFCOMM_RPN_PM_BITRATE) {
        bit_rate = rpn->bit_rate;
        if (bit_rate > RFCOMM_RPN_BR_230400) {
            bit_rate = RFCOMM_RPN_BR_9600;
            rpn_mask ^= RFCOMM_RPN_PM_BITRATE;
        }
    }

    if (rpn->param_maskl & RFCOMM_RPN_PM_DATA) {
        data_bits = __get_rpn_data_bits(rpn->line_settings);
        if (data_bits != RFCOMM_RPN_DATA_8) {
            data_bits = RFCOMM_RPN_DATA_8;
            rpn_mask ^= RFCOMM_RPN_PM_DATA;
        }
    }

    if (rpn->param_maskl & (RFCOMM_RPN_PM_STOP)) {
        stop_bits = __get_rpn_stop_bits(rpn->line_settings);
        if (stop_bits != RFCOMM_RPN_STOP_1) {
            stop_bits = RFCOMM_RPN_STOP_1;
            rpn_mask ^= RFCOMM_RPN_PM_STOP;
        }
    }

    if (rpn->param_maskl & (RFCOMM_RPN_PM_PARITY)) {
        parity = __get_rpn_parity(rpn->line_settings);
        if (parity != RFCOMM_RPN_PARITY_NONE) {
            parity = RFCOMM_RPN_PARITY_NONE;
            rpn_mask ^= RFCOMM_RPN_PM_PARITY;
        }
    }

    if (rpn->param_maskh & (RFCOMM_RPN_PM_FLOW >>8)) {
        flow_ctrl = rpn->flow_ctrl;
        if (flow_ctrl != RFCOMM_RPN_FLOW_NONE) {
            flow_ctrl = RFCOMM_RPN_FLOW_NONE;
            rpn_mask ^= RFCOMM_RPN_PM_FLOW;
        }
    }

    if (rpn->param_maskl & (RFCOMM_RPN_PM_XON)) {
        xon_char = rpn->xon_char;
        if (xon_char != RFCOMM_RPN_XON_CHAR) {
            xon_char = RFCOMM_RPN_XON_CHAR;
            rpn_mask ^= RFCOMM_RPN_PM_XON;
        }
    }

    if (rpn->param_maskl & (RFCOMM_RPN_PM_XOFF)) {
        xoff_char = rpn->xoff_char;
        if (xoff_char != RFCOMM_RPN_XOFF_CHAR) {
            xoff_char = RFCOMM_RPN_XOFF_CHAR;
            rpn_mask ^= RFCOMM_RPN_PM_XOFF;
        }
    }

rpn_out:
    rfcomm_send_rpn(s, 0, dlci, bit_rate, data_bits, stop_bits,
                    parity, flow_ctrl, xon_char, xoff_char, rpn_mask);

    return;
}

static int rfcomm_recv_pn(struct rfcomm_session *s, int cr, U8 *dat)
{
    struct rfcomm_pn *pn = (void *) dat;
    struct rfcomm_dlc *d;
    
    U8 dlci = pn->dlci;

    if (!dlci)
        return 0;

    d = rfcomm_dlc_get(s, dlci);
    if (d) 
    {
        if (cr) 
        {
            /* PN request */
            rfcomm_apply_pn(d, cr, pn);
            rfcomm_send_pn(s, 0, d);
        } 
        else
        {
            /* PN response */
            switch (d->state) 
            {
                case BT_CONFIG:
                    rfcomm_apply_pn(d, cr, pn);

                    d->state = BT_CONNECT;
                    rfcomm_send_sabm(s, d->dlci);
                    break;
            }
        }
    } 
    else 
    {
        if (!cr)
            return 0;

        /* PN request for non existing DLC.
         * Assume incoming connection. */
        if (rfcomm_connect_ind(s, __srv_channel(dlci), &d)) 
        {
            d->dlci = dlci;

            rfcomm_apply_pn(d, cr, pn);

            rfcomm_send_pn(s, 0, d);
        } 
        else 
        {
            rfcomm_send_dm(s, dlci);
        }
    }
    return 0;
}

static void rfcomm_recv_disc(struct rfcomm_session *s, U8 dlci)
{
    int err = 0;


    if (dlci) {
        struct rfcomm_dlc *d = rfcomm_dlc_get(s, dlci);
        if (d) {
            rfcomm_send_ua(s, dlci);
            d->state = BT_CLOSED;
            __rfcomm_dlc_close(d, err);
        } else
            rfcomm_send_dm(s, dlci);

    } else {
        rfcomm_send_ua(s, 0);

        s->state = BT_CLOSED;
    }

    return;
}


static int rfcomm_recv_data(struct rfcomm_session *s, U8 dlci, int pf, U16 len, U8 *dat)
{
    struct rfcomm_dlc *d;

    d = rfcomm_dlc_get(s, dlci);
    if (!d) {
        rfcomm_send_dm(s, dlci);
        goto drop;
    }

    if (pf && d->RFCOMM_CFC_ENABLED) {
        U8 credits = *dat;
        dat++;
        len--;

        d->tx_credits += credits;
        if (d->tx_credits)
            d->RFCOMM_TX_THROTTLED = 0;
    }

    if (len && d->state == BT_CONNECTED) {
        
        d->rx_credits--;
        if(d->data_ready)
        {
            d->data_ready(d, len, dat);
        }
        return 0;
    }

drop:
    return 0;
}

static int rfcomm_recv_mcc(struct rfcomm_session *s, U8 *dat)
{
    U8 type, cr, len;
    struct rfcomm_mcc *mcc = (void *) dat;

    cr   = __test_cr(mcc->type);
    type = __get_mcc_type(mcc->type);
    len  = __get_mcc_len(mcc->len);

    dat += 2;
    switch (type) {
        case RFCOMM_PN:
            rfcomm_recv_pn(s, cr, dat);
            break;

        case RFCOMM_RPN:
            rfcomm_recv_rpn(s, cr, len, dat);
            break;
/*
        case RFCOMM_RLS:
            rfcomm_recv_rls(s, cr, skb);
            break;
*/
        case RFCOMM_MSC:
            rfcomm_recv_msc(s, cr, dat);
            break;
/*
        case RFCOMM_FCOFF:
            if (cr) {
                set_bit(RFCOMM_TX_THROTTLED, &s->flags);
                rfcomm_send_fcoff(s, 0);
            }
            break;

        case RFCOMM_FCON:
            if (cr) {
                clear_bit(RFCOMM_TX_THROTTLED, &s->flags);
                rfcomm_send_fcon(s, 0);
            }
            break;

        case RFCOMM_TEST:
            if (cr)
                rfcomm_send_test(s, 0, skb->data, skb->len);
            break;

        case RFCOMM_NSC:
            break;
*/
        default:
            ASSERT(0);
            rfcomm_send_nsc(s, cr, type);
            break;
    }
    return 0;
}

U8 RfcommPsmHandler(U16 aclHandle, U16 cid, U16 len ,U8 *dat)
{
    U16 tmpHdl;
    U16 tmpCid;
    struct rfcomm_hdr *hdr;
    struct rfcomm_session *s;
    U8 type, dlci, fcs;

    if(aclHandle == 0) //update channel status
    {

        if(cid == 0)
        {
            L2ChannelSearchTCidHdlByPsm(0x0003, &tmpCid, &tmpHdl);

            if(rfst.cid) //currently connected
            {
                if(!tmpHdl || !tmpCid)
                {
                    s = rfcomm_session_search(rfst.aclHdl);
                    if(s)
                    {
                        s->initiator = 0;
                        s->aclHandle = 0;
                        s->state = BT_CLOSED;
                    }

                    rfst.cid = 0; //disconnected update
                    rfst.aclHdl = 0;   
                    rfcomm_dlc_all_close();
                    if(rfst.rffc)
                        rfst.rffc(0+2); //disconnect inform     

                }
            }
            else //currently disconnected
            {
                if(tmpHdl && tmpCid)
                {
                    rfst.cid = tmpCid; //connected update
                    rfst.aclHdl = tmpHdl;      
                    if(rfst.rffc)
                        rfst.rffc(1+2); //connect inform

                    s = rfcomm_session_search(tmpHdl);

                    if(!s)
                    {
                        s = rfcomm_session_alloc(tmpHdl, 0, BT_OPEN);
                        ASSERT(s); //not enough session!!!!
                    }
                    else
                    {
                        if(s->state == BT_CONNECT)
                        {
                            rfcomm_send_sabm(s, 0);
                        }
                    }

                }
            }
        }
        else if(cid == 1) //normal tx complete
        {
        }

        return 0;
    }

    //process session rx
    if(s = rfcomm_session_search(aclHandle))
    {
        hdr = (void*)dat;
        dlci = __get_dlci(hdr->addr);
        type = __get_type(hdr->ctrl);

        fcs = dat[len-1];

        if (__check_fcs(dat, type, fcs)) {
            ASSERT(0);
            return 0;
        }

        if (__test_ea(hdr->len))
        {
            len -= 3;
            dat += 3;
        }
        else
        {
            len -= 4;
            dat += 4;
        }

        switch (type) 
        {
            case RFCOMM_SABM:
                if (__test_pf(hdr->ctrl))
                    rfcomm_recv_sabm(s, dlci);
                break;
            case RFCOMM_DISC:
                if (__test_pf(hdr->ctrl))
                    rfcomm_recv_disc(s, dlci);
                break;
            case RFCOMM_UA:
                if (__test_pf(hdr->ctrl))
                    rfcomm_recv_ua(s, dlci);
                break;
            case RFCOMM_DM:
                if (__test_pf(hdr->ctrl))
                    rfcomm_recv_dm(s, dlci);
                break;
            case RFCOMM_UIH:
                if (dlci)
                    rfcomm_recv_data(s, dlci, __test_pf(hdr->ctrl), len-1/*except fcs*/, dat);  
                else
                    rfcomm_recv_mcc(s, dat);
                break;
            default:
                ASSERT(0);
                break;
        }
    }
    
    return 0;
}


///////////////////////////////////Public API///////////////////////////////////////////

void Bt__RfCommInit()
{
    U8 i;
    
    rfst.aclHdl = 0;
    rfst.cid = 0;
    rfst.rffc = 0;

    for(i = 0; i < MAX_SESSION; i++)
    {
        rf_session[i].state = BT_CLOSED;
        rf_session[i].RFCOMM_CFC_ENABLED = 1;
        rf_session[i].aclHandle = 0;
        rf_session[i].initiator = 0;
    }
    
    for(i = 0; i < MAX_DLC; i++)
        rf_dlc[i].state = BT_CLOSED;

    for(i = 0; i < MAX_CHANNEL; i++)
    {
        server_channel[i].id = 0;
        server_channel[i].data_ready = 0;
        server_channel[i].status_change = 0;
    }
        
    //rfcomm psm = 0x0003
    ASSERT(RegisterPsmHandler(0x0003, RfcommPsmHandler));
}


void Bt__RfCommDeInit()
{
    DeregisterPsmHandler(0x0003); //do not check
}

void Bt_RfCommChannelRegister(U8 channel, RFCOMM_DATARDY data_rdy, RFCOMM_STSCHG sts_chg)
{
    server_channel[0].id = channel;
    server_channel[0].data_ready = data_rdy;
    server_channel[0].status_change = sts_chg;
}

void Bt__RfCommDisconnect(struct rfcomm_dlc *d)
{
    rfcomm_dlc_close(d, 0);
}


void Bt__RfCommConnect(U16 aclHandle, U8 chanel_number,RFCOMM_DATARDY data_ready, RFCOMM_STSCHG status_change)
{
    struct rfcomm_dlc *d;
    struct rfcomm_session *s;
    
    LLSignalConReq(aclHandle, 0x0003);

    //dlc open    
    s = rfcomm_session_alloc(aclHandle, 1, BT_CONNECT);

    if(s)
    {
        d = rfcomm_dlc_alloc();
        d->session = s;
        d->state = BT_CONFIG;
        d->dlci = __dlci(!s->initiator,  chanel_number/*server_channel[0].id*/);
        d->data_ready = data_ready;
        d->state_change = status_change;
    }
   
    
    
}


void Bt__RfCommCB(FinishNotifyCallback fc)
{
   rfst.rffc = fc;
}

//we don't need process_session, to complicated
static void rfcomm_process_dlcs(void)
{
    U8 i;
    struct rfcomm_dlc *d;
    for(i = 0; i < MAX_DLC; i++)
    {
        d = &rf_dlc[i];
        if(d->state == BT_CONNECTED)
        {
            if(Bt_AclReady())
            {
                if(d->RFCOMM_MSCEX_NEED_TX)
                {
                    d->RFCOMM_MSCEX_NEED_TX = 0;
                    rfcomm_send_msc(d->session, 1, d->dlci, d->v24_sig);
                }
                else if(d->RFCOMM_MSCEX_TX)
                {
                    if (d->RFCOMM_CFC_ENABLED) 
                    {
                        /* CFC enabled.
                         * Give them some credits */
                        if (!d->RFCOMM_RX_THROTTLED && d->rx_credits == 0) 
                        {
                            rfcomm_send_credits(d, RFCOMM_DEFAULT_CREDITS);
                            d->rx_credits = RFCOMM_DEFAULT_CREDITS;
                            return; //once send, we need to get out
                        }
                    } 
                    else 
                    {
                        /* CFC disabled.
                         * Give ourselves some credits */
                        d->tx_credits = 5;
                    }

                    if (d->RFCOMM_TX_THROTTLED)
                        continue; //next tx

                    if (d->tx_credits && d->tx_len) {
                        rfcomm_dlc_send(d, d->tx_dat, d->tx_len);
                        d->tx_credits--;
                        d->tx_len = 0;
                        if (d->RFCOMM_CFC_ENABLED && !d->tx_credits) {
                            d->RFCOMM_TX_THROTTLED = 1;
                        }                
                        return; //once send, we need to get out
                    }

                }
            }
        }
        //else if BT_OPEN //outgoing, tallplay think
    }
}

void rfcomm_dlc_send_data_direct(struct rfcomm_dlc *d)
{
    if (d->tx_credits && d->tx_len) {
        if (!d->RFCOMM_RX_THROTTLED && d->rx_credits == 0) 
        {
            rfcomm_dlc_send_with_credit(d, d->tx_dat, d->tx_len);
            d->rx_credits = RFCOMM_DEFAULT_CREDITS;
        }
        else
            rfcomm_dlc_send(d, d->tx_dat, d->tx_len);
        d->tx_credits--;
        d->tx_len = 0;
        if (d->RFCOMM_CFC_ENABLED && !d->tx_credits) {
            d->RFCOMM_TX_THROTTLED = 1;
        }                
    }
}

void Bt_RfCommScheduler(void)
{
    rfcomm_process_dlcs();
}

#endif

