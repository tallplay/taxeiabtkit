#include "BTCore.h"
#include "../GAP/HCI/BTCmdEvt.h"
#include "../TaxeiaConsoleDebug/TaxeiaConsoleDebug.h"
#include "../HAL/MassUart.h"
#include "DataQueue.h"
#include "../GAP/LOCAL_DEV/Bt__LocalDevice.h"
#include "../GAP/LL/Bt__RemoteDevice.h"
#include "../GAP/L2CAP/Bt__LLCAP.h"
#include "../TaxeiaHciLog/TaxeiaHCILog.h"
#include "../HAL/LocalStorage.h"
#include <string.h>

#include "../HIDPD/Bt__Profile_HIDD.h"
#include "../SDP/SDPS/Bt__Profile_SDPS.h"
#include "../rfcomm/rfcomm.h"
#include "../HFP/Bt__Profile_HFP.h"
#include "../SPP/Bt__Profile_SPP.h"

#ifdef DEBUG
#include <stdio.h>
#endif

typedef enum
{
    IDLE = 0,
    PERMIT_SEND_CMD = 1,
    SENDING_CMD = 2,
    WAITEND = 3,
    PERMIT_SEND_DATA =4,
    SENDING_DATA = 5
}HCITXSTATE;

#ifndef NEW_BT_LOOP
typedef enum
{
    RX_IDLE = 0,
    EVT_RECVING = 1,
    DAT_RECVING = 2,
}HCIRXSTATE;
#else
#define RX_STATE_IDLE 0
#define RX_STATE_EVT 1
#define RX_STATE_DATA 2
#define RX_STATE_EVT_BODY 3
#define RX_STATE_EVT_END 4
#define RX_STATE_DATA_BODY 5
#define RX_STATE_DATA_END 6
#endif

//BT stack status
STACKSTATE BtState = STOP;
HCITXSTATE TxState = IDLE;
#ifndef NEW_BT_LOOP
HCIRXSTATE RxState = RX_IDLE;
#else
U8 rx_state = RX_STATE_IDLE;
U8 datbuf[64];
U16 databuf_len = 0;
#endif

U8 TxDataPresent = 0;
U8 TxCmdPresent = 0;

#if (PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
U8 profile_type = PROFILE_TYPE_NOTHING;
#endif

static void UartTxTask(void);
static void UartRxTask(void);
static void CmdTxDataTask(void);
static void EvtRxDataTask(void);
extern void Bt__ProfileHFPScheduler(void);
static void BtCoreProfileDeInit(void);
static void BtCoreProfileLoop(void);
///////////////////////////////////Public API///////////////////////////////////////////
void BTCoreLoop(void)
{   
    if(BtState != STOP)
    {
#ifndef NEW_BT_LOOP2
        //Uart Tx Task
        UartTxTask();
#endif

#ifndef NEW_BT_LOOP
        //Uart Rx Task
        UartRxTask();
#endif
        //CMD/TData Task
        CmdTxDataTask();

        //EVT/RData Task
        EvtRxDataTask();   
        
        if(BtState == INITIALIZING)
        {
            if(Bt__IsLocalDeviceInited() != 0)
                BtState = INITIALIZED;
        }
        else if(BtState == INITIALIZED)
        {
            BtCoreProfileLoop();
        }
    }

}

void BTCoreStop(void)
{
    Bt__LocalDeviceDeInit();
    Bt__RemoteDeviceDeInit();
    Bt__LLCAPDeInit();

    BtCoreProfileDeInit();

    //DeInit Process
    MassUart_Close();
    StorageDeInit();
    BtState = STOP;
}

U8 BTCoreHwHealth(void)
{
    return !MassUart_Open();
}

//success : 0 fail: 1
U8 BTCoreStart(U8 profile_type)
{
    //Init Process
#ifndef NEW_BT_LOOP2
    TxQueueInit();
#endif
#ifndef NEW_BT_LOOP
    RxQueueInit();
#endif    
    TxState = IDLE;
#ifndef NEW_BT_LOOP
    RxState = RX_IDLE;
#endif
    TxDataPresent = 0;
    TxCmdPresent = 0;
    if(MassUart_Open())
    {
        BtState = STOP;
        return 1;
    }
    else
    {
        StorageInit();
        BtState = INITIALIZING;
        Bt__LocalDeviceInit(profile_type);
        Bt__RemoteDeviceInit();
        Bt__LLCAPInit();

        BtCoreProfileInit(profile_type);

    }
    return 0;
}

STACKSTATE BTCoreState()
{
    if(BtState == INITIALIZED)
    {
        if(TxState != IDLE || TxDataPresent || TxCmdPresent)
            return BUSY;
        
        #ifndef NEW_BT_LOOP

        else if(RxState != RX_IDLE || !RxQueueIsEmpty())
            return RXBUSY;

        #else
        
        else if(rx_state != RX_STATE_IDLE || databuf_len)
            return RXBUSY;
        #endif
    }
    
    return BtState;
}


#ifdef STATUS_DEBUG
void BtUartRxTest(U8 *buf, U16 *len)
{
   U8 rlen;
   rlen = *len > 256 ? 255 : *len;
   
   MassUart_Open();
   if(MassUart_RecvLen(buf, &rlen) == RETCODE_COMMAND_INVALID)
        Taxeia_Report("!!!!!!!!!!!!!!!!!!Fuck!!!!!!!!!!!!!!!!!!\n");

   *len = rlen;
   MassUart_Close();
}

void BtUartTxTest()
{
#define BUFSIZE 25600
#define LINESIZE 80
#define TIMES 10
    U8 *buf;
    U16 i;
    U16 idx;
    U16 len;
    U16 times;

    buf = (U8*) malloc(BUFSIZE);
    
    for(i = 0; i < BUFSIZE; i++)
    {
        buf[i] = '0' + (i % LINESIZE);

        if((i%LINESIZE) == LINESIZE-2)
            buf[i] = '\r';
        
        if((i%LINESIZE) == LINESIZE-1)
            buf[i] = '\n';        
            
    }
    times = TIMES;
    MassUart_Open();

    while(times --)
    {
        idx = 0;
        len = BUFSIZE;
        i = len;
        do
        {
            MassUart_Send(buf+idx, &i);
            len -= i;
            idx += i;
            i = len;
        }
        while(len > 0);
    }
    
    MassUart_Close();
    free(buf);
}

void GetBtStackStatus(U8 *buf, U8 len)
{
    char stackstr[][20] = {"Stop", "Initializing", "Initialized"};
    
    if(len > 40)
    {    
        sprintf(buf, "Stack\t: %s\r\n", stackstr[BtState]);
        sprintf(buf, "%sHciTx\t: %d\r\n", buf, TxState);
#ifndef NEW_BT_LOOP        
        sprintf(buf, "%sHciRx\t: %d\r\n", buf, RxState);
#endif
    }
}
#endif

void BtCoreProfileInit(U8 type)
{

#if (PROFILE_TYPE == PROFILE_TYPE_HID)
        Bt__PreSetCod(Bt__ProfileSDPSInit(type));
        Bt__ProfileHIDDInit(type);
#elif (PROFILE_TYPE == PROFILE_TYPE_HFP)
        Bt__PreSetCod(Bt__ProfileSDPSInit(type));
        Bt__RfCommInit();
        Bt__ProfileHFPInit();
#elif (PROFILE_TYPE == PROFILE_TYPE_HID_HFP)
        Bt__PreSetCod(Bt__ProfileSDPSInit(type));
        Bt__ProfileHIDDInit(type);
        Bt__RfCommInit();
        Bt__ProfileHFPInit();
#elif (PROFILE_TYPE == PROFILE_TYPE_SPP)
        Bt__PreSetCod(Bt__ProfileSDPSInit(type));
        Bt__RfCommInit();
        Bt__ProfileSPPInit();
#elif (PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
    switch(type)
    {
        case PROFILE_TYPE_HID:
            Bt__PreSetCod(Bt__ProfileSDPSInit(PROFILE_TYPE_HID));
            Bt__ProfileHIDDInit(type);
            profile_type = type;
            break;
        case PROFILE_TYPE_HFP:
            Bt__PreSetCod(Bt__ProfileSDPSInit(PROFILE_TYPE_HFP));
            Bt__RfCommInit();
            Bt__ProfileHFPInit();
            profile_type = type;
            break;
        case PROFILE_TYPE_HID_HFP:
            Bt__PreSetCod(Bt__ProfileSDPSInit(PROFILE_TYPE_HID_HFP));
            Bt__ProfileHIDDInit(type);
            Bt__RfCommInit();
            Bt__ProfileHFPInit();
            profile_type = type;            
            break;
        case PROFILE_TYPE_SPP:
            Bt__PreSetCod(Bt__ProfileSDPSInit(PROFILE_TYPE_SPP));
            Bt__RfCommInit();
            Bt__ProfileSPPInit();
            profile_type = type;
            break;
        case PROFILE_TYPE_NOTHING:
        case PROFILE_TYPE_DYNAMIC:
            break;
        default:
            ASSERT(0);
            break;
    }

#endif
}

///////////////////////////////////Private Functions/////////////////////////////////////

static void BtCoreProfileDeInit(void)
{
#if (PROFILE_TYPE != PROFILE_TYPE_DYNAMIC)

    #if (PROFILE_TYPE != PROFILE_TYPE_NOTHING)
        Bt__ProfileSDPSDeInit();
    #endif

    #if (PROFILE_TYPE == PROFILE_TYPE_HID || PROFILE_TYPE == PROFILE_TYPE_HID_HFP)            
        Bt__ProfileHIDDDeInit();
    #endif
    
    #if (PROFILE_TYPE == PROFILE_TYPE_HFP || PROFILE_TYPE == PROFILE_TYPE_SPP || PROFILE_TYPE == PROFILE_TYPE_HID_HFP)            
        Bt__RfCommDeInit();
    #endif
    
    #if (PROFILE_TYPE == PROFILE_TYPE_HFP || PROFILE_TYPE == PROFILE_TYPE_HID_HFP)            
        Bt__ProfileHFPDeInit();
    #endif

    #if (PROFILE_TYPE == PROFILE_TYPE_SPP)            
        Bt__ProfileSPPDeInit();
    #endif
    
#else //dynamic from upper layer
    switch(profile_type)
    {
        case PROFILE_TYPE_HID:
            Bt__ProfileSDPSDeInit();
            Bt__ProfileHIDDDeInit();
            break;
        case PROFILE_TYPE_HFP:
            Bt__ProfileSDPSDeInit();
            Bt__RfCommDeInit();
            Bt__ProfileHFPDeInit();
            break;
        case PROFILE_TYPE_HID_HFP:
            Bt__ProfileSDPSDeInit();
            Bt__ProfileHIDDDeInit();
            Bt__RfCommDeInit();
            Bt__ProfileHFPDeInit();
            break;
        case PROFILE_TYPE_SPP:
            Bt__ProfileSDPSDeInit();
            Bt__RfCommDeInit();
            Bt__ProfileSPPDeInit();
            break;
    }
#endif
}

static void BtCoreProfileLoop()
{
#if (PROFILE_TYPE != PROFILE_TYPE_DYNAMIC)
    #if (PROFILE_TYPE == PROFILE_TYPE_HFP || PROFILE_TYPE == PROFILE_TYPE_SPP || PROFILE_TYPE == PROFILE_TYPE_HID_HFP)            
            Bt_RfCommScheduler();
    #endif
    
    #if (PROFILE_TYPE == PROFILE_TYPE_HFP || PROFILE_TYPE == PROFILE_TYPE_HID_HFP)            
        Bt__ProfileHFPScheduler();
    #endif
    
    #if (PROFILE_TYPE == PROFILE_TYPE_SPP)            
        Bt__ProfileSPPScheduler();
    #endif
    
#else  //dynamic from upper layer
    if(profile_type == PROFILE_TYPE_HFP || profile_type == PROFILE_TYPE_HID_HFP)
    {
        Bt_RfCommScheduler();
        Bt__ProfileHFPScheduler();
    }
    else if(profile_type == PROFILE_TYPE_SPP)
    {
        Bt_RfCommScheduler();
        Bt__ProfileSPPScheduler();
    }
#endif
}

#ifndef NEW_BT_LOOP2
void UartTxTask()
{
    static U8 len = 0;
    static U8 *ptr;
    U16 sendlen;
    
    if(len == 0)
    {
        TxQueuePrePop(&ptr, &len);
    }

    if(len > 0)
    {
        sendlen = len;
        if(MassUart_Send(ptr, &sendlen) == RETCODE_COMMAND_INVALID)
            Taxeia_Report("!!!!!!!!!!!!!!!!!!����!!!!!!!!!!!!!!!!!!%d\n", sendlen);
        TxQueuePostPop((U8)sendlen);

        HEXDISPLAY("=>", ptr, sendlen);
        TaxeiaHCILog(0x55, ptr, (U8)sendlen);        
        len = 0;
    }
}
#endif

#ifdef NEW_BT_LOOP

#else
void UartRxTask()
{   
    U8 *ptr;
    U8 len;
	U16 rlen;
    RxQueuePrePush(&ptr, &len);
    
    if(len > 0)
    {
		rlen = len;
        if(MassUart_RecvLen(ptr, &rlen) == RETCODE_COMMAND_INVALID)
            Taxeia_Report("!!!!!!!!!!!!!!!!!!Fuck!!!!!!!!!!!!!!!!!!\n");
		len = rlen;
        RxQueuePostPush(len);
        HEXDISPLAY("<=", ptr, rlen);
        TaxeiaHCILog(0xAA, ptr, rlen);
    }
}
#endif

BOOL CmdRequest()
{
    if(TxCmdPresent || TxState != IDLE)
    {
       // ASSERT(0);
        return FALSE;
    }

    TxCmdPresent = 1;
    TxState = PERMIT_SEND_CMD;

    return TRUE;
}

BOOL DataRequest()
{
#if 0
    if(TxState != IDLE)
    {
        ASSERT(0);
        return FALSE;
    }

    TxState = PERMIT_SEND_DATA;
#else
    if(TxDataPresent)
    {
    #ifdef EGIS_NO_ASSERT
    #else
        ASSERT(0);
    #endif
        return FALSE;
    }
    TxDataPresent = 1;
#endif
    return TRUE;
}

extern void CheckSavedCmd(void);

void CmdResponsed()
{
    /////ASSERT(TxCmdPresent); //do not remove
    TxCmdPresent = 0;

    CheckSavedCmd();
}

#ifdef NEW_BT_LOOP2
U16 UartTxSendData(U8 *buf, U16 len)
{   
    U16 slen;
    
    slen = len;
    
    if(MassUart_Send(buf, &slen) == RETCODE_COMMAND_INVALID)
        Taxeia_Report("!!!!!!!!!!!!!!!!!!����!!!!!!!!!!!!!!!!!!%d\n", len);

    ASSERT(slen == len);

    HEXDISPLAY("=>", buf, len);
    TaxeiaHCILog(0x55, buf, len);
    return slen;
}

void CmdTxDataTask()
{
    U16 len;
    U8 *ptr;
    U16 zero_padd_len;
txstart:
    switch(TxState)
    {
        case IDLE:
            if(TxDataPresent)
            {
                TxState = PERMIT_SEND_DATA;
                goto txstart;
            }
            break;
        case PERMIT_SEND_CMD:         
            len = 1;
            UartTxSendData("\x01", len);//cmd prefix            

            len = CmdSendCallback(&ptr, &zero_padd_len);
            UartTxSendData(ptr, len);

            memset(ptr, 0, len);
            while(zero_padd_len)
            {
                if(zero_padd_len < len)
                    len = zero_padd_len;

                UartTxSendData(ptr, len);                
                zero_padd_len -= len;
            }
            
            TxState = IDLE;
            break;
        case PERMIT_SEND_DATA:            
            len = 1;
            UartTxSendData("\x02", len);//cmd prefix            
            TxState = SENDING_DATA;
            len = DataSendCallback(&ptr);
            UartTxSendData(ptr, len);
            TxDataPresent = 0;
            TxState = IDLE;
            
            break;
        case WAITEND:
            break;
    }
}


//Its dangurous, do not use this!!!
U8 TxSendSepcial(U8 *dat, U8 tlen)
{
    U16 len;    
    if(TxState == IDLE) //blocked in long wait command
    {
        len = tlen;
        UartTxSendData(dat, len);
        return 0;
    }

    return 2;
}

#else
void CmdTxDataTask()
{
txstart:
    switch(TxState)
    {
        case IDLE:
            if(TxDataPresent)
            {
                TxState = PERMIT_SEND_DATA;
                goto txstart;
            }
            break;
        case PERMIT_SEND_CMD:
            if(TxQueueIsFull())
                return;
            
            TxQueuePush(0x01);//cmd prefix
            
            TxState = SENDING_CMD;   
            break;
        case PERMIT_SEND_DATA:
            if(TxQueueIsFull())
                return;
            
            TxQueuePush(0x02);//data prefix
            
            TxState = SENDING_DATA;
            goto txstart;
            break;
        case SENDING_CMD:
            if(CmdSendCallback())
            {
                TxState = IDLE;
            }
            break;
        case SENDING_DATA:
            if(DataSendCallback())
            {
                TxDataPresent = 0;
                TxState = IDLE;
            }
            break;
        case WAITEND:
            break;
    }
}

//Its dangurous, do not use this!!!
U8 TxSendSepcial(U8 *dat, U8 tlen)
{
    U8 *ptr;
    U8 len;
    
    if(TxState == IDLE) //blocked in long wait command
    {
        TxQueuePrePush(&ptr, &len);
        ASSERT(len > tlen);
        TxQueuePostPush(tlen);
        memcpy(ptr, dat, tlen);
        return 0;
    }

    return 2;
}

#endif


#ifdef NEW_BT_LOOP
extern U8 EvtBuffer[];
extern U8 RxDataBuffer[];
extern U8 RxDataHeader[];

U16 UartRxGetData(U8 *dat)
{
    U16 len;
    len = 64;
    if(MassUart_RecvLen(dat, &len) == RETCODE_COMMAND_INVALID)
        Taxeia_Report("!!!!!!!!!!!!!!!!!!Fuck!!!!!!!!!!!!!!!!!!\n");
    HEXDISPLAY("<=", dat, len);
    TaxeiaHCILog(0xAA, dat, len);

    return len;
}



void EvtRxDataTask()
{
    static U16 rx_len;
    static U16 rx_evt_idx;
    static U16 rx_dat_idx = 0;
    static U8 rx_dat_header_idx;
    static U8 ptr;

    if(!databuf_len) 
    {
        databuf_len = UartRxGetData(datbuf);
        ptr = 0;
    }
    
    if(databuf_len)
    {
        while(databuf_len)
        {
            databuf_len--;

            switch(rx_state)
            {
                case RX_STATE_IDLE:
                    {
                        if(datbuf[ptr] == 0x04) //event
                        {
                            rx_state = RX_STATE_EVT;
                            rx_len = 2;
                            rx_evt_idx = 0;
                        }
                        else if(datbuf[ptr] == 0x02) //data
                        {
                            rx_state = RX_STATE_DATA;
                            rx_len = 4;
                            rx_dat_header_idx = 0;
                            ////rx_dat_idx = 0;, because of continue packet
                        }
                        else
                            ASSERT(0);

                        ptr++;
                    }
                    break;
                case RX_STATE_DATA:
                {
                    RxDataHeader[rx_dat_header_idx++] = datbuf[ptr++];
                    rx_len --;
                        
                    if(rx_len == 0)
                    {
                        if(((RxDataHeader[1] >> 4) & 0x3) != 0x01) //!= continue PB_bit
                        {
                            memcpy(RxDataBuffer, RxDataHeader, 4);
                            rx_dat_idx = 4;
                        }
                        else
                        {
                            U16 tmp_len;
                            /*assume handle is same*/
                            /*add length to previous one*/
                            tmp_len = (RxDataHeader[2] | (RxDataHeader[3] << 8)) + (RxDataBuffer[2] | (RxDataBuffer[3] << 8));
                            RxDataBuffer[2] = tmp_len & 0xff;
                            RxDataBuffer[3] = tmp_len >> 8;
                            //rx_dat_idx = last one;
                        }
                        
                        rx_state = RX_STATE_DATA_BODY;
                        rx_len = RxDataHeader[2] | (RxDataHeader[3] << 8);
                    }

                    break;
                }         
                case RX_STATE_DATA_BODY:
                {
                    RxDataBuffer[rx_dat_idx++] = datbuf[ptr++];
                    rx_len --;
                    
                    if(rx_len == 0)
                    {
                        RxDataCallBack(RxDataBuffer);
                        rx_state = RX_STATE_IDLE;
                        return; //important
                    }
                    break;
                }

                case RX_STATE_EVT:
                {
                    EvtBuffer[rx_evt_idx++] = datbuf[ptr++];
                    rx_len --;
                    
                    if(rx_len == 0)
                    {
                        rx_state = RX_STATE_EVT_BODY;
                        rx_len = EvtBuffer[1];
                    }

                    break;
                }
                case RX_STATE_EVT_BODY:
                {
                    if(rx_evt_idx < 66)
                        EvtBuffer[rx_evt_idx] = datbuf[ptr];
                    
                    rx_len --;
                    rx_evt_idx++;
                    ptr++;

                    if(rx_len == 0)
                    {
                        EvtCallBack(EvtBuffer);
                        rx_state = RX_STATE_IDLE;
                        return; //important
                    }
                    break;
                }
            }//switch

        }//while
    } //if(len = ...
}

#else
void EvtRxDataTask(void)
{
    U8 dat;
    switch(RxState)
    {
        case RX_IDLE:
            if(!RxQueueIsEmpty())
            {
                RxQueuePop(&dat);
                if(dat == 0x04) //event
                {
                    RxState = EVT_RECVING;
                }
                else if(dat == 0x02)
                {
                    RxState = DAT_RECVING;
                }
                else
                    ASSERT(0);
            }
            else
                break;
            break;
        case EVT_RECVING:
            if(EvtCallBack())
            {
                RxState = RX_IDLE;
            }
            break;
        case DAT_RECVING:
            if(RxDataCallBack())
            {
                RxState = RX_IDLE;
            }
            break;
    }
    
}
#endif
