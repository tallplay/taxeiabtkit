#include "DataQueue.h"
#include "../TaxeiaConsoleDebug/TaxeiaConsoledebug.h"

#define QUEUECHECK
//#define QUEUETXDEBUG

#define TXCAP 65
#define RXCAP 65

U8 TxData[TXCAP], TxIn, TxOut;
U8 RxData[RXCAP], RxIn, RxOut;

#ifdef QUEUECHECK
U8 cr = 0;
void EnterCritical()
{
    if(cr == 1)
        ASSERT(0);
    cr = 1;
}

void LeaveCritical()
{
    cr = 0;
}
#define ENTER() EnterCritical();
#define RETURN(exp)    do{LeaveCritical(); return exp;}while(0)

#else
#define ENTER()
#define RETURN(exp) return exp;
#endif

void TxQueueInit()
{
    ENTER();
    TxIn = 0; //index to push
    TxOut = 0; //index to pop
    RETURN(;);
}

BOOL TxQueueIsEmpty()
{
    ENTER();
    RETURN(TxOut == TxIn);
}

BOOL TxQueueIsFull()
{
    ENTER();
    //In catch up Out    
    if(TxOut == TxIn + 1)
        RETURN(TRUE);
    
    if(TxOut == 0 && TxIn == TXCAP - 1)
        RETURN(TRUE);
    
    RETURN(FALSE);
}

BOOL TxQueuePush(U8 dat)
{
    ENTER();
    #ifdef QUEUETXDEBUG
    Taxeia_Report("TxPush len = 1 txin %d =>", TxIn);
    #endif

    //full
    if(TxOut == TxIn + 1)
        RETURN(FALSE);
    //full
    if(TxOut == 0 && TxIn == TXCAP - 1)
        RETURN(FALSE);

    TxData[TxIn++] = dat;
    if(TxIn == TXCAP)
        TxIn = 0;
    
    #ifdef QUEUETXDEBUG
    Taxeia_Report("%d\n", TxIn);
    #endif
    
    RETURN(TRUE);
}

BOOL TxQueuePop(U8 *dat)
{
    ENTER();
    if(TxOut == TxIn)
        RETURN(FALSE);
    *dat = TxData[TxOut++];
    if(TxOut == TXCAP)
        TxOut = 0;
    RETURN(TRUE);
}

void TxQueuePrePop(U8 **dat, U8 *len)
{
    ENTER();

    if(TxIn > TxOut)
    {
        *dat = &TxData[TxOut];
        *len = TxIn - TxOut;
    }
    else if(TxOut > TxIn)
    {
        *dat = &TxData[TxOut];
        *len = TXCAP - TxOut;
    }
    else
        *len = 0;    
    RETURN(;);
}           

void TxQueuePostPop(U8 len)
{
    ENTER();
    #ifdef QUEUETXDEBUG
    Taxeia_Report("TxPostPop len = %2d tout %2d =>", len, TxOut);
    #endif
    
    TxOut+= len;
    if(TxOut == TXCAP)
        TxOut = 0;
    ASSERT(TxOut < TXCAP);
    
    #ifdef QUEUETXDEBUG    
    Taxeia_Report("%2d\n", TxOut);
    #endif
    RETURN(;);
}

void TxQueuePrePush(U8 **dat, U8 *len)
{
    ENTER();
    if(TxIn == TxOut) //empty
    {
        TxIn = TxOut = 0;
        
        *dat = &TxData[0];
        *len = TXCAP - 1;
    }
    else if((TxIn + 1) < TxOut) //not full
    {
        *dat = &TxData[TxIn];
        *len = TxOut - (TxIn + 1);
    }
    else if(TxIn > TxOut)
    {
        *dat = &TxData[TxIn];
        *len = TXCAP - TxIn;

        if(TxOut == 0) //this will cause data lost, because TxIn will set to 0 
            *len -= 1;
    }
    else
        *len = 0;
//    Taxeia_Report("TxPrePush *len = %2d\n", *len);
    RETURN(;);
}

void TxQueuePostPush(U8 len)
{
    ENTER();
    #ifdef QUEUETXDEBUG
    Taxeia_Report("TxPostPush *len = %2d txin %2d => ", len, TxIn);
    #endif
    TxIn += len;
    if(TxIn == TXCAP)
        TxIn = 0;
    ASSERT(TxIn < TXCAP);
    #ifdef QUEUETXDEBUG    
    Taxeia_Report("%2d\n", TxIn);
    #endif
    RETURN(;);
}


void RxQueueInit()
{
    ENTER();
    RxIn = 0;
    RxOut = 0;
    RETURN(;);
}

BOOL RxQueueIsEmpty()
{
   return RxOut == RxIn;
}

BOOL RxQueueIsFull()
{
    ENTER();
    //In catch up Out    
    if(RxOut == RxIn + 1)
        RETURN(TRUE);
    
    if(RxOut == 0 && RxIn == RXCAP - 1)
        RETURN(TRUE);
    
    RETURN(FALSE);
}

BOOL RxQueuePush(U8 dat)
{
    ENTER();

    
    //full
    if(RxOut == RxIn + 1)
        RETURN(FALSE);
    //full
    if(RxOut == 0 && RxIn == RXCAP - 1)
        RETURN(FALSE);

    RxData[RxIn++] = dat;
    if(RxIn == RXCAP)
        RxIn = 0;
    RETURN(TRUE);
}

BOOL RxQueuePop(U8 *dat)
{
    ENTER();
    if(RxOut == RxIn)
        RETURN(FALSE);
    *dat = RxData[RxOut++];
    if(RxOut == RXCAP)
        RxOut = 0;
    RETURN(TRUE);
}

void RxQueuePrePop(U8 **dat, U8 *len)
{
    ENTER();
    
    if(RxIn > RxOut)
    {
        *dat = &RxData[RxOut];
        *len = RxIn - RxOut;
    }
    else if(RxOut > RxIn)
    {
        *dat = &RxData[RxOut];
        *len = RXCAP - RxOut;
    }
    else
        *len = 0;
    RETURN(;);
}           

void RxQueuePostPop(U8 len)
{
    ENTER();
    RxOut+= len;
    if(RxOut == RXCAP)
        RxOut = 0;
    ASSERT(RxOut < RXCAP);
    RETURN(;);
}

void RxQueuePrePush(U8 **dat, U8 *len)
{
    ENTER();
    if(RxIn == RxOut) //empty
    {
        RxIn = RxOut = 0; //prevent bug

        *dat = &RxData[0];
        *len = RXCAP - 1;
    }
    else if((RxIn + 1) < RxOut) //not full
    {
        *dat = &RxData[RxIn];
        *len = RxOut - (RxIn + 1);
    }
    else if(RxIn > RxOut)
    {
        *dat = &RxData[RxIn];
        *len = RXCAP - RxIn;

        if(RxOut == 0)
        {
           *len -= 1;
        }
    }
    else
        *len = 0;
    RETURN(;);
}

void RxQueuePostPush( U8 len)
{
    ENTER();
    RxIn += len;
    if(RxIn == RXCAP)
        RxIn = 0;
    ASSERT(RxIn < RXCAP);
    RETURN(;);
}

