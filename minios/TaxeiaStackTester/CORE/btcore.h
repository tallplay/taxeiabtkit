#ifndef __BTCORE__
#define __BTCORE__
#include "BTType.h"
#include "BtConfig.h"

///////////////////////////////////Public API///////////////////////////////////////////
typedef enum
{
    STOP = 0,
    INITIALIZING = 1,
    INITIALIZED = 2,
    BUSY = 3,
    RXBUSY = 4
}STACKSTATE;

//loop control
void BTCoreLoop(void);
void BTCoreStop(void);
U8 BTCoreHwHealth(void);
U8 BTCoreStart(U8);
STACKSTATE BTCoreState(void);
void BtCoreProfileInit(U8 type);

#ifdef STATUS_DEBUG
void BtUartTxTest(void);
void BtUartRxTest(U8 *buf, U16 *len);

//debug
void GetBtStackStatus(U8 *buf, U8 len);
#else

#define GetBtStackStatus(x, y) ;

#endif

////////////////////////////////////////////////////////////////////////////////////////
//API for upper layer
/*Request to send command*/
BOOL CmdRequest(void);
void CmdResponsed(void);
BOOL DataRequest(void);

#endif
