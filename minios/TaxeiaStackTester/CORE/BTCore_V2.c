#include "BTCore_V2.h"
#include "../GAP/HCI/BTCmdEvt_V2.h"
#include "../TaxeiaConsoleDebug/TaxeiaConsoleDebug.h"
#include "../HAL/MassUart.h"
#include "../GAP/LOCAL_DEV/Bt__LocalDevice_V2.h"
#include "../GAP/LL/Bt__RemoteDevice_V2.h"
#include "../GAP/L2CAP/Bt__LLCAP_V2.h"
#include "../TaxeiaHciLog/TaxeiaHCILog.h"
#include "../HAL/LocalStorage.h"
#include <string.h>

#include "../HIDPD/Bt__Profile_HIDD_V2.h"
#include "../SDP/SDPS/Bt__Profile_SDPS_V2.h"
#include "../rfcomm/rfcomm_V2.h"
#include "../HFP/Bt__Profile_HFP_V2.h"
#include "../SPP/Bt__Profile_SPP_V2.h"

#ifdef DEBUG
#include <stdio.h>
#endif

#define RX_STATE_IDLE 0
#define RX_STATE_EVT 1
#define RX_STATE_DATA 2
#define RX_STATE_EVT_BODY 3
#define RX_STATE_EVT_END 4
#define RX_STATE_DATA_BODY 5
#define RX_STATE_DATA_END 6

//BT stack status
STACKSTATE BtState = STOP;
U8 rxstate = RX_STATE_IDLE;
U8 datbuf[64];
U16 databuf_len = 0;

#define CMD_PRESET_CHECK_TIMEOUT 100
#define DATA_PRESET_CHECK_TIMEOUT 100

U8 TxDataPresent = DATA_PRESET_CHECK_TIMEOUT;
U8 TxCmdPresent = CMD_PRESET_CHECK_TIMEOUT;

#if (PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
U8 profile_type = PROFILE_TYPE_NOTHING;
#endif

static U8 TxDataTask(void);
static void EvtRxDataTask(void);
extern void Bt__ProfileHFPScheduler(void);
static void BtCoreProfileDeInit(void);
static void BtCoreProfileLoop(void);
static U8 TxCmdTask();

///////////////////////////////////Public API///////////////////////////////////////////
void BTCoreLoop(void)
{   
    if(BtState != STOP)
    {

        //TCmd Task
        if(!TxCmdTask())
        {
            if(TxCmdPresent)
                TxCmdPresent--;
        }
        else
            TxCmdPresent = CMD_PRESET_CHECK_TIMEOUT;
        
        //TData Task
        if(!TxDataTask())
        {
            if(TxDataPresent)
                TxDataPresent--;
        }
        else
            TxDataPresent = DATA_PRESET_CHECK_TIMEOUT;

        //EVT/RData Task
        EvtRxDataTask();   
        
        if(BtState == INITIALIZING)
        {
            if(Bt__IsLocalDeviceInited() != 0)
                BtState = INITIALIZED;
        }
        else if(BtState == INITIALIZED)
        {
            BtCoreProfileLoop();
        }
    }

}

void BTCoreStop(void)
{
    Bt__LocalDeviceDeInit();
    Bt__RemoteDeviceDeInit();
    Bt__LLCAPDeInit();

    BtCoreProfileDeInit();

    //DeInit Process
    MassUart_Close();
    StorageDeInit();
    BtState = STOP;
}

U8 BTCoreHwHealth(void)
{
    return !MassUart_Open();
}

//success : 0 fail: 1
U8 BTCoreStart(U8 profile_type)
{
    //Init Process
    TxDataPresent = 0;
    TxCmdPresent = 0;
    if(MassUart_Open())
    {
        BtState = STOP;
        return 1;
    }
    else
    {
        BtCmdEvtInit();
        StorageInit();
        BtState = INITIALIZING;
        Bt__LocalDeviceInit(profile_type);
        Bt__RemoteDeviceInit();
        Bt__LLCAPInit();

        BtCoreProfileInit(profile_type);

    }
    return 0;
}

STACKSTATE BTCoreState()
{
    if(BtState == INITIALIZED)
    {
/*        if(TxDataPresent || TxCmdPresent)
            return BUSY;
                
        else*/ if(rxstate != RX_STATE_IDLE || databuf_len)
            return RXBUSY;
    }
    
    return BtState;
}


#ifdef STATUS_DEBUG
void BtUartRxTest(U8 *buf, U16 *len)
{
   U8 rlen;
   rlen = *len > 256 ? 255 : *len;
   
   MassUart_Open();
   if(MassUart_RecvLen(buf, &rlen) == RETCODE_COMMAND_INVALID)
        Taxeia_Report("!!!!!!!!!!!!!!!!!!Fuck!!!!!!!!!!!!!!!!!!\n");

   *len = rlen;
   MassUart_Close();
}

void BtUartTxTest()
{
#define BUFSIZE 25600
#define LINESIZE 80
#define TIMES 10
    U8 *buf;
    U16 i;
    U16 idx;
    U16 len;
    U16 times;

    buf = (U8*) malloc(BUFSIZE);
    
    for(i = 0; i < BUFSIZE; i++)
    {
        buf[i] = '0' + (i % LINESIZE);

        if((i%LINESIZE) == LINESIZE-2)
            buf[i] = '\r';
        
        if((i%LINESIZE) == LINESIZE-1)
            buf[i] = '\n';        
            
    }
    times = TIMES;
    MassUart_Open();

    while(times --)
    {
        idx = 0;
        len = BUFSIZE;
        i = len;
        do
        {
            MassUart_Send(buf+idx, &i);
            len -= i;
            idx += i;
            i = len;
        }
        while(len > 0);
    }
    
    MassUart_Close();
    free(buf);
}

void GetBtStackStatus(U8 *buf, U8 len)
{
    char stackstr[][20] = {"Stop", "Initializing", "Initialized"};
    
    if(len > 40)
    {    
        sprintf(buf, "Stack\t: %s\r\n", stackstr[BtState]);
#ifndef NEW_BT_LOOP        
        sprintf(buf, "%sHciRx\t: %d\r\n", buf, RxState);
#endif
    }
}
#endif

void BtCoreProfileInit(U8 type)
{

#if (PROFILE_TYPE == PROFILE_TYPE_HID)
        Bt__PreSetCod(Bt__ProfileSDPSInit(type));
        Bt__ProfileHIDDInit(type);
#elif (PROFILE_TYPE == PROFILE_TYPE_HFP)
        Bt__PreSetCod(Bt__ProfileSDPSInit(type));
        Bt__RfCommInit();
        Bt__ProfileHFPInit();
#elif (PROFILE_TYPE == PROFILE_TYPE_HID_HFP)
        Bt__PreSetCod(Bt__ProfileSDPSInit(type));
        Bt__ProfileHIDDInit(type);
        Bt__RfCommInit();
        Bt__ProfileHFPInit();
#elif (PROFILE_TYPE == PROFILE_TYPE_SPP)
        Bt__PreSetCod(Bt__ProfileSDPSInit(type));
        Bt__RfCommInit();
        Bt__ProfileSPPInit();
#elif (PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
    switch(type)
    {
        case PROFILE_TYPE_HID:
            Bt__PreSetCod(Bt__ProfileSDPSInit(PROFILE_TYPE_HID));
            Bt__ProfileHIDDInit(type);
            profile_type = type;
            break;
        case PROFILE_TYPE_HFP:
            Bt__PreSetCod(Bt__ProfileSDPSInit(PROFILE_TYPE_HFP));
            Bt__RfCommInit();
            Bt__ProfileHFPInit();
            profile_type = type;
            break;
        case PROFILE_TYPE_HID_HFP:
            Bt__PreSetCod(Bt__ProfileSDPSInit(PROFILE_TYPE_HID_HFP));
            Bt__ProfileHIDDInit(type);
            Bt__RfCommInit();
            Bt__ProfileHFPInit();
            profile_type = type;            
            break;
        case PROFILE_TYPE_SPP:
            Bt__PreSetCod(Bt__ProfileSDPSInit(PROFILE_TYPE_SPP));
            Bt__RfCommInit();
            Bt__ProfileSPPInit();
            profile_type = type;
            break;
        case PROFILE_TYPE_NOTHING:
        case PROFILE_TYPE_DYNAMIC:
            break;
        default:
            ASSERT(0);
            break;
    }

#endif
}

///////////////////////////////////Private Functions/////////////////////////////////////

static void BtCoreProfileDeInit(void)
{
#if (PROFILE_TYPE != PROFILE_TYPE_DYNAMIC)

    #if (PROFILE_TYPE != PROFILE_TYPE_NOTHING)
        Bt__ProfileSDPSDeInit();
    #endif

    #if (PROFILE_TYPE == PROFILE_TYPE_HID || PROFILE_TYPE == PROFILE_TYPE_HID_HFP)            
        Bt__ProfileHIDDDeInit();
    #endif
    
    #if (PROFILE_TYPE == PROFILE_TYPE_HFP || PROFILE_TYPE == PROFILE_TYPE_SPP || PROFILE_TYPE == PROFILE_TYPE_HID_HFP)            
        Bt__RfCommDeInit();
    #endif
    
    #if (PROFILE_TYPE == PROFILE_TYPE_HFP || PROFILE_TYPE == PROFILE_TYPE_HID_HFP)            
        Bt__ProfileHFPDeInit();
    #endif

    #if (PROFILE_TYPE == PROFILE_TYPE_SPP)            
        Bt__ProfileSPPDeInit();
    #endif
    
#else //dynamic from upper layer
    switch(profile_type)
    {
        case PROFILE_TYPE_HID:
            Bt__ProfileSDPSDeInit();
            Bt__ProfileHIDDDeInit();
            break;
        case PROFILE_TYPE_HFP:
            Bt__ProfileSDPSDeInit();
            Bt__RfCommDeInit();
            Bt__ProfileHFPDeInit();
            break;
        case PROFILE_TYPE_HID_HFP:
            Bt__ProfileSDPSDeInit();
            Bt__ProfileHIDDDeInit();
            Bt__RfCommDeInit();
            Bt__ProfileHFPDeInit();
            break;
        case PROFILE_TYPE_SPP:
            Bt__ProfileSDPSDeInit();
            Bt__RfCommDeInit();
            Bt__ProfileSPPDeInit();
            break;
    }
#endif
}

static void BtCoreProfileLoop()
{
#if (PROFILE_TYPE != PROFILE_TYPE_DYNAMIC)
    #if (PROFILE_TYPE == PROFILE_TYPE_HFP || PROFILE_TYPE == PROFILE_TYPE_SPP || PROFILE_TYPE == PROFILE_TYPE_HID_HFP)            
            Bt_RfCommScheduler();
    #endif
    
    #if (PROFILE_TYPE == PROFILE_TYPE_HFP || PROFILE_TYPE == PROFILE_TYPE_HID_HFP)            
        Bt__ProfileHFPScheduler();
    #endif
    
    #if (PROFILE_TYPE == PROFILE_TYPE_SPP)            
        Bt__ProfileSPPScheduler();
    #endif
    
#else  //dynamic from upper layer
    if(profile_type == PROFILE_TYPE_HFP || profile_type == PROFILE_TYPE_HID_HFP)
    {
        Bt_RfCommScheduler();
        Bt__ProfileHFPScheduler();
    }
    else if(profile_type == PROFILE_TYPE_SPP)
    {
        Bt_RfCommScheduler();
        Bt__ProfileSPPScheduler();
    }
#endif
}

extern U8 BtCmdWaitFetch(void (*SendCmd)(U8 *));
extern U8 BTDataWaitFetch(void (*SendData)(U8 *));

U16 UartTxSendData(U8 *buf, U16 len);

static void TxSendCmd(U8 *buf)
{
    UartTxSendData(buf, buf[3] + 4);

    HEXDISPLAY("=>", buf, buf[3] + 4);

    CMD_SHOW(buf[1] + (buf[2] <<8));

}

static void TxSendData(U8 *buf)
{
    UartTxSendData(buf, (buf[3] + (buf[4] << 8)) + 5);

    HEXDISPLAY("=>", buf, (buf[3] + (buf[4] << 8)) + 5);

}

//return busy or not
static U8 TxCmdTask(void)
{
    return !BtCmdWaitFetch(TxSendCmd);
}

static U8 TxDataTask(void)
{
    return !BTDataWaitFetch(TxSendData);
}

U16 UartTxSendData(U8 *buf, U16 len)
{   
    U16 slen;
    
    slen = len;
    
    if(MassUart_Send(buf, &slen) == RETCODE_COMMAND_INVALID)
        Taxeia_Report("MassUartSend error!!!\n");

    ASSERT(slen == len);

    TaxeiaHCILog(0x55, buf, len);
    return slen;
}

//Its dangurous, do not use this!!!
U8 TxSendSepcial(U8 *dat, U8 tlen)
{
    U16 len;    
    len = tlen;
    UartTxSendData(dat, len);
    return 0;
}

extern U8 Evt_Buffer[];
extern U8 Rx_DataBuffer[];
extern U8 Rx_DataHeader[];

U16 UartRxGetData(U8 *dat)
{
    U16 len;
    len = 64;
    if(MassUart_RecvLen(dat, &len) == RETCODE_COMMAND_INVALID)
        Taxeia_Report("!!!!!!!!!!!!!!!!!!Fuck!!!!!!!!!!!!!!!!!!\n");
    HEXDISPLAY("<=", dat, len);
    TaxeiaHCILog(0xAA, dat, len);

    ASSERT(len <= 64);
    return len;
}



void EvtRxDataTask()
{
    static U16 rx_len;
    static U16 rx_evt_idx;
    static U16 rx_dat_idx = 0;
    static U8 rx_dat_header_idx;
    static U8 ptr;

    if(!databuf_len) 
    {
        databuf_len = UartRxGetData(datbuf);
        ptr = 0;        
    }
    
    if(databuf_len)
    {
        while(databuf_len)
        {
            databuf_len--;

            switch(rxstate)
            {
                case RX_STATE_IDLE:
                    {
                        if(datbuf[ptr] == 0x04) //event
                        {
                            rxstate = RX_STATE_EVT;
                            rx_len = 2;
                            rx_evt_idx = 0;
                        }
                        else if(datbuf[ptr] == 0x02) //data
                        {
                            rxstate = RX_STATE_DATA;
                            rx_len = 4;
                            rx_dat_header_idx = 0;
                            ////rx_dat_idx = 0;, because of continue packet
                        }
                        else
                            ASSERT(0);

                        ptr++;
                    }
                    break;
                case RX_STATE_DATA:
                {
                    Rx_DataHeader[rx_dat_header_idx++] = datbuf[ptr++];
                    rx_len --;
                        
                    if(rx_len == 0)
                    {
                        if(((Rx_DataHeader[1] >> 4) & 0x3) != 0x01) //!= continue PB_bit
                        {
                            memcpy(Rx_DataBuffer, Rx_DataHeader, 4);
                            rx_dat_idx = 4;
                        }
                        else
                        {
                            U16 tmp_len;
                            /*assume handle is same*/
                            /*add length to previous one*/
                            tmp_len = (Rx_DataHeader[2] | (Rx_DataHeader[3] << 8)) + (Rx_DataBuffer[2] | (Rx_DataBuffer[3] << 8));
                            Rx_DataBuffer[2] = tmp_len & 0xff;
                            Rx_DataBuffer[3] = tmp_len >> 8;
                            //rx_dat_idx = last one;
                        }
                        
                        rxstate = RX_STATE_DATA_BODY;
                        rx_len = Rx_DataHeader[2] | (Rx_DataHeader[3] << 8);

                        ASSERT(rx_len <= ACL_MAX_PACKET_LEN);
                    }


                    break;
                }         
                case RX_STATE_DATA_BODY:
                {
                    Rx_DataBuffer[rx_dat_idx++] = datbuf[ptr++];
                    rx_len --;
                    
                    if(rx_len == 0)
                    {
                        Rx_DataCallBack(Rx_DataBuffer);
                        rxstate = RX_STATE_IDLE;
                        return; //important
                    }
                    break;
                }

                case RX_STATE_EVT:
                {
                    Evt_Buffer[rx_evt_idx++] = datbuf[ptr++];
                    rx_len --;
                    
                    if(rx_len == 0)
                    {
                        rxstate = RX_STATE_EVT_BODY;
                        rx_len = Evt_Buffer[1];
                    }

                    break;
                }
                case RX_STATE_EVT_BODY:
                {
                    if(rx_evt_idx < 66)

                    Evt_Buffer[rx_evt_idx] = datbuf[ptr];
                    rx_len --;
                    rx_evt_idx++;
                    ptr++;

                    if(rx_len == 0)
                    {
                        Evt_CallBack(Evt_Buffer);
                        rxstate = RX_STATE_IDLE;
                        return; //important
                    }
                    break;
                }
            }//switch

        }//while
    } //if(len = ...
}

