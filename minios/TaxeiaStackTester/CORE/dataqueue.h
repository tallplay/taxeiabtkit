#ifndef __DATAQUEUE__
#define __DATAQUEUE__
#include "BTType.h"

void TxQueueInit(void);
BOOL TxQueueIsEmpty(void);
BOOL TxQueueIsFull(void);
BOOL TxQueuePush(U8 dat);
BOOL TxQueuePop(U8 *dat);
void TxQueuePrePop(U8 **dat, U8 *len);
void TxQueuePostPop(U8 len);
void TxQueuePrePush(U8 **dat, U8 *len);
void TxQueuePostPush(U8 len);

void RxQueueInit(void);
BOOL RxQueueIsEmpty(void);
BOOL RxQueueIsFull(void);
BOOL RxQueuePush(U8 dat);
BOOL RxQueuePop(U8 *dat);
void RxQueuePrePop(U8 **dat, U8 *len);
void RxQueuePostPop(U8 len);
void RxQueuePrePush(U8 **dat, U8 *len);
void RxQueuePostPush(U8 len);

void Taxeia_Assert(const char *expression, const char *file, U16 line);

#endif
