#include "bt_hid_hfp_api.h"
#include "btconfig.h"
#include "CORE\BTCore.h"
#include "GAP\LOCAL_DEV\Bt__LocalDevice.h"
#include "GAP\LL\Bt__RemoteDevice.h"
#include "hidpd\Bt__Profile_HIDD.h"
#include "spp\Bt__Profile_SPP.h"
#include "GAP\SECURITY\Bt__SecurityManager.h"
#include "TaxeiaConsoleDebug\TaxeiaConsoledebug.h"
#include "HFP\Bt__Profile_HFP.h"


#ifdef WIN32
#include "..\HAL\LocalOutput.h"
#include "..\HAL\MassUart.h"
#include "..\HAL\LocalStorage.h"
#else
#include "..\btHAL\LocalOutput.h"
#include "..\btHAL\MassUart.h"
#include "..\btHAL\LocalStorage.h"
#endif

#include <stdio.h>
#include <string.h>

extern U8 pad_data_get_report(U8 report_id, U8 len, U8 *dat);
extern U8 ps3_data_get_report(U8 type, U8 report, U8 *buf, int maxlen);
extern U8 SppRfcommAva(void);

#ifdef WIN32
void MtkPinInit(void)
{
    Taxeia_Report("Please Implement MtkPinInit function!!!\r\n");
}

U8 MtkWakeUpPin()
{
    return 0;
}

#define POWER_UP_TIME 1000

#else

#define POWER_UP_TIME 1000
#endif


#ifndef RECONNECT_TIME
#define RECONNECT_TIME 2
#endif

#ifdef MT6622_PATCH
#define BOOT_TIME 25000
#else
#define BOOT_TIME 5000
#endif

#ifndef PAIR_TIME 
#define PAIR_TIME 200000
#endif

struct
{
    U8 bt_state;
    U8 hidcconnect : 1;
    U8 phbook : 1;
    U8 ate_found : 1;
    U8 pin_code_req : 1;
    U8 is_ps3 : 1;
    U8 sco_on : 1;
    U8 reserved : 2;

    U8 act_afterboot;
    U8 profile_type;
    U16 aclhandle;
    U8 reconnect_times;

    U32 bt_cur_tick;
    U32 bt_start_tick ;
    U32 bt_powerup_tick;
    U32 bt_pairing_tick;
    U32 bt_connect_limit;
#if (PROFILE_TYPE == PROFILE_TYPE_HID_HFP || PROFILE_TYPE == PROFILE_TYPE_HFP || PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)    
    U16 phbooknum;
    U16 phbookidx;    
    U8 callingnumber[20];
#endif    
    U8 Local_BdName[20];
    U8 Remote_Bdaddr[6];    
    U16 Remote_Bdaddr_clk;
    U8 Remote_BdName[26];

} btContext;


#if (PROFILE_TYPE == PROFILE_TYPE_HID_HFP || PROFILE_TYPE == PROFILE_TYPE_HID || PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)

//parm[0]: 1: connect sucess 0: disconnect or connect fail
//parm[1]: 1: hidc 0:hidi
U8 *UpdateHidInfo(U16 parm)
{
    switch(parm & 0x3)
    {
        case 0: //hidi disconnect
            if(btContext.bt_state == BT_STA_DISCONNECTING)
            {
                Bt__HIDDCDisconnect();
            }                
            break;
        case 1: //hidi connect
            btContext.bt_state = BT_STA_HID_CONNECTED;
#if  (PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
            if(btContext.profile_type == PROFILE_TYPE_HID_HFP)
            {            
                Bt__HFPConnect(btContext.aclhandle);
            }
            else
#elif (PROFILE_TYPE == PROFILE_TYPE_HID_HFP)
            Bt__HFPConnect(btContext.aclhandle);
            break;
#endif                
            Bt__WriteScanEanbled(0); //no scan
            break;
        case 2: //hidc disconnect
            btContext.hidcconnect = 0;
            if(btContext.bt_state == BT_STA_DISCONNECTING)
            {
                Bt__DisconnRst(btContext.aclhandle, 0x13, 0);
            }
            break;
        case 3: //hidc connect
        {
            btContext.hidcconnect = 1;
            if(btContext.bt_state == BT_STA_CONNECTED)
            {
                Bt__HIDDIConnect(btContext.aclhandle); //create intr l2cap link
            }
        }
            break;
    }
        
    return 0;
}
#endif

#if (PROFILE_TYPE == PROFILE_TYPE_HID_HFP || PROFILE_TYPE == PROFILE_TYPE_HFP || PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)

static void(*PhoneBookCB)(U16 idx, U8* num, U8 *name);

static U8 *HFPCB (U16 parm)
{
    if(parm == HFPCB_PBITEM)
    {
        U8 *ptr;
        int idx;
        U8 *num;
        U8 *name;
        ptr = Bt__ProfileHFPGetCallBackBuf();

        idx = 0x1;
        sscanf((char*)ptr, "%d,", &idx);

        /* mark number */
        while(*ptr++ != '"');//find begin "
        num = ptr;
        while(*++ptr != '"');//find end "
        *ptr = 0; //mark end

        /* mark name */
        while(*ptr++ != '"');//find begin "
        name = ptr;
        if(name[0] == '"') //null
        {
            name[0] = 0;
        }
        else
        {
            while(*++ptr != '"');//find end "
            *ptr = 0; //mark end
        }

        if(PhoneBookCB)
        {
            btContext.phbookidx = idx;
            PhoneBookCB((U16)idx, num, name);
            if(!Bt__ProfileHFPGetPhoneBook((U16)idx+1))
            {
                btContext.phbook = 0;
                PhoneBookCB(0xffff, 0, 0); //end, no phone book
            }
        }
        
    }

    if(parm == HFPCB_PBNUMBER)
    {
        strcpy((char*)btContext.callingnumber, (char*)Bt__ProfileHFPGetCallBackBuf());
    }

    if(parm == 2) //l2cap disconnected
    {
        if(btContext.bt_state == BT_STA_DISCONNECTING)
        {
            Bt__DisconnRst(btContext.aclhandle, 0x13, 0);
        }
    }
    
    return 0;
}
#endif

#if (PROFILE_TYPE == PROFILE_TYPE_SPP || PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
static void (*sppcallback)(U16 len, U8 *dat) = 0;

static void SPPCB(U8 evt, U16 len, U8 *dat)
{
    if(evt == 2) //l2cap disconnected
    {
        if(btContext.bt_state == BT_STA_DISCONNECTING)
        {
            Bt__DisconnRst(btContext.aclhandle, 0x13, 0);
        }
    }
    else if(evt == 0) //data incomming
    {
        if(sppcallback)
        sppcallback(len, dat);
    }    
}
#endif

BT_LOCAL_INFO *local_info;

U8 *LocalDeviceInfo(U16 parm)
{
    if(parm == LOCALCB_INITIALIZED)//initilized
    {
        btContext.bt_state = BT_STA_INITILIZED;
    }

    return 0;
}

extern void RegisterLLTestHandler(LLDataHandler lldh);

U8 * ACLConnectCB(U16 parm)
{
    U8 aclconnect;
    if(parm & 0x80) //sco
    {
        parm &= ~0x80;

        btContext.sco_on = parm ? 1 : 0;        
    }
    else
    {
        aclconnect = parm ? 1: 0;
        btContext.aclhandle = parm;
        
        if(aclconnect)
        {
            if(btContext.bt_connect_limit) btContext.bt_connect_limit--;
            Taxeia_Report("%d left\n", btContext.bt_connect_limit);

            if(btContext.bt_state == BT_STA_CONNECTING)
            {
                btContext.bt_state = BT_STA_CONNECTED;
            }
        }
        else
        {
            Taxeia_Report("acl disconnected\r\n");
            btContext.pin_code_req = 0;        
            btContext.hidcconnect = 0;
            btContext.bt_state = BT_STA_DISCONNECTED;
        }        
    }    
    return 0;
}

void InquiryCB(U8 ret, U8 * dat)
{
    if(ret == 1) //command fail!!
        btContext.bt_state = BT_STA_DISCONNECTED;
    else if(ret == 0) //timeout or maximun result
    {
        if(btContext.ate_found)
        {
            btContext.bt_state = BT_STA_DUT_FOUND_ATE;
        }
        else
            btContext.bt_state = BT_STA_DISCONNECTED;
    }
    else
    {
        memcpy(btContext.Remote_Bdaddr, dat+1, 6);
        memcpy(&btContext.Remote_Bdaddr_clk, dat+13, 2);
        btContext.ate_found = 1;
    }
}

#ifdef BT_LOCK
U8 activated(U8*dat)
{
    U8 pout[32];
    extern U8 Bt__VerifyData(U8 *dat, U8 *pout);

    if(local_info->isAddressLocked)
    {
        if(!Bt__VerifyData(dat, pout))
        {
           StorageClear();
           StorageSetBytes(SM_SIZE, 32, dat);
           return 0;
        }
    }
    return 1;
}
#endif
void activation_cb(U16 len, U8 *dat)
{
    if(len == 32)
    {
#ifdef BT_LOCK
        if(!activated(dat)) //success
           bt_dut_send_data(32, dat);
        btContext.bt_state = BT_STA_ACTIVATON_REBOOT;
#endif
    }
}

void bt_dut_register_callback(void (*cb)(U16 len, U8 *dat))
{
    Bt__RemoteDeviceTestCB(cb);
}

void bt_dut_send_data(U16 len, U8 *dat)
{
    Bt__RemoteDeviceTest(len, dat);
}

void bt_ate_register_callback(void (*cb)(U16 len, U8 *dat))
{
    Bt__RemoteDeviceTestCB(cb);
}

void bt_ate_send_data(U16 len, U8 *dat)
{
    Bt__RemoteDeviceTest(len, dat);
}

U8 bt_dut_find_ate(U8 test_eq)
{
    if(btContext.bt_state == BT_STA_DISCONNECTED)
    {
        Bt__Inquiry(test_eq, InquiryCB);
        btContext.bt_state = BT_STA_DUT_FINDING_ATE;
        btContext.ate_found = 0;
        return 0;
    }
    return 1;
}


void bt_gap_writesupervision_timeout(U16 timeout)
{
    Bt__WriteLinkSupervisionTimeout(btContext.aclhandle, timeout); //1s
}

U8 bt_gap_is_paired_exist(U8 *addr)
{
    return Bt__EnumPairedDevice(addr);
}

void bt_gap_storage_link_key(U8 *bdaddr, U8 *link_key, U8 type)
{
    extern void Bt__StoreLinkKey(U8 *bdaddr, U8 *link_key, U8 type);

    Bt__StoreLinkKey(bdaddr, link_key, type);
}

U8 bt_gap_is_pincode_req(void)
{
    if(btContext.pin_code_req)
    {
        btContext.pin_code_req = 0;
        return 1;
    }

    return 0;
}

U8 bt_gap_disconnect(void)
{    
    if(!Bt_AclReady())
    {
        btContext.bt_state = BT_STA_DISCONNECT_RETRY;
        return 2;
    }

#if (PROFILE_TYPE == PROFILE_TYPE_HID || PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
    if(btContext.profile_type == PROFILE_TYPE_HID)
    {
        if(btContext.bt_state == BT_STA_HID_CONNECTED || btContext.bt_state == BT_STA_DISCONNECT_RETRY)
        {
            btContext.bt_state = BT_STA_DISCONNECTING;
            Bt__HIDDIDisconnect();
            return 0;
        }
        else if(btContext.bt_state == BT_STA_CONNECTED)
        {
            btContext.bt_state = BT_STA_DISCONNECTING;
            Bt__DisconnRst(btContext.aclhandle, 0x05, 0); //auth fail(reason);
            return 0;
        }
    }
#endif    
#if (PROFILE_TYPE == PROFILE_TYPE_HFP || PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
    else if(btContext.profile_type == PROFILE_TYPE_HFP)
    {
        if(btContext.bt_state == BT_STA_HFP_CONNECTED || btContext.bt_state == BT_STA_DISCONNECT_RETRY)
        {
            btContext.bt_state = BT_STA_DISCONNECTING;
            Bt__HFPDisconnect();
            return 0;
        }
        else if(btContext.bt_state == BT_STA_CONNECTED)
        {
            btContext.bt_state = BT_STA_DISCONNECTING;
            Bt__DisconnRst(btContext.aclhandle, 0x05, 0); //auth fail(reason);
            return 0;
        }
    }
#endif
#if (PROFILE_TYPE == PROFILE_TYPE_SPP || PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
    else if(btContext.profile_type == PROFILE_TYPE_SPP)
    {
        if(btContext.bt_state == BT_STA_SPP_CONNECTED || btContext.bt_state == BT_STA_DISCONNECT_RETRY)
        {
            btContext.bt_state = BT_STA_DISCONNECTING;
            Bt__SPPDisconnect();
            return 0;
        }
        else if(btContext.bt_state == BT_STA_CONNECTED || btContext.bt_state == BT_STA_CONNECTED_INCOMING)
        {
            btContext.bt_state = BT_STA_DISCONNECTING;
            Bt__DisconnRst(btContext.aclhandle, 0x05, 0); //auth fail(reason);
            return 0;
        }
    }
#endif
#if (PROFILE_TYPE == PROFILE_TYPE_DYNAMIC || PROFILE_TYPE == PROFILE_TYPE_HID_HFP)
    else if(btContext.profile_type == PROFILE_TYPE_HID_HFP)
    {
        if(btContext.bt_state == BT_STA_HID_CONNECTED || btContext.bt_state == BT_STA_HID_HFP_CONNECTED || btContext.bt_state == BT_STA_DISCONNECT_RETRY)
        {
            btContext.bt_state = BT_STA_DISCONNECTING;
            Bt__HIDDIDisconnect();
            return 0;
        }
        else if(btContext.bt_state == BT_STA_CONNECTED)
        {
            btContext.bt_state = BT_STA_DISCONNECTING;
            Bt__DisconnRst(btContext.aclhandle, 0x05, 0); //auth fail(reason);
            return 0;
        }
    }
#endif    
    
    return 1;
}

U8 bt_gap_is_address_locked(void)
{
    U8 addr[6];
    U8 locked;
    locked = Bt__GetMacAddress(addr);
    return locked;
}

#ifndef BT_LOCK
    #ifndef BT_UNLOCK_CONNECT_LIMIT
        #define BT_UNLOCK_CONNECT_LIMIT 0xffffffff
    #endif
#endif

void bt_init(void)
{
    MtkPinInit();

    btContext.bt_state = BT_STA_STOP;
    btContext.hidcconnect = 0;
    btContext.reconnect_times = 0;
    btContext.act_afterboot = 0;
    btContext.is_ps3 = 0;
#ifdef BT_LOCK
    btContext.bt_connect_limit = 10;
#else
    btContext.bt_connect_limit = BT_UNLOCK_CONNECT_LIMIT;
#endif
    btContext.Local_BdName[0] = 0;
    btContext.pin_code_req = 0;

    Bt__GetLocalInfo(&local_info);      
}

void bt_start(U32 CurTick, U8 mode, U8 profile_type)
{

    if(btContext.bt_state == BT_STA_STOP)
    {
        /*mtk power on*/
        MtkPower(1);
        btContext.bt_state = BT_STA_POWERUP;

        btContext.bt_powerup_tick = CurTick;
        btContext.act_afterboot = mode;
        btContext.profile_type = profile_type;
    }
}

void bt_stop(void)
{
    BTCoreStop();

    MtkPower(0); //bt power off

    btContext.bt_state = BT_STA_STOP;
    btContext.hidcconnect = 0;
}

static void bt_lock(void)
{
   btContext.bt_state = BT_STA_LOCK;
   BTCoreStop();
   MtkPower(0); //bt power off
}

U8 bt_state(void)
{
    return btContext.bt_state;
}


U8 bt_task(U32 curTick)
{
    static U8 last_wakeup_pin = 0;
        
    STACKSTATE sta;
    U8 time_more = 0;

    btContext.bt_cur_tick = curTick;
    
    BTCoreLoop();
    sta = BTCoreState();
    
    if( sta == BUSY || sta == RXBUSY)
    {
        time_more = 1;
    }

    if(btContext.bt_state == BT_STA_POWERUP)
    {
        //1 second to initial
        if(curTick > btContext.bt_powerup_tick + POWER_UP_TIME)
        {                      
                BTCoreStart(PROFILE_TYPE);
            

            Bt__LocalDeviceCB(LocalDeviceInfo);
            Bt__RemoteDeviceCB(ACLConnectCB);
            

            btContext.bt_state = BT_STA_BOOT;
            btContext.bt_start_tick = curTick;

        }

        time_more = 1;
    }    
    else if(btContext.bt_state == BT_STA_BOOT)
    {
        //5 second to timeout
        if(curTick > btContext.bt_start_tick + (BOOT_TIME))
        {            
            btContext.bt_state = BT_STA_BT_FAIL;
        }            

        time_more = 1;

    }
    else if(btContext.bt_state == BT_STA_INITILIZED)
    {
        btContext.bt_state = BT_STA_DISCONNECTED;
    }
    else if(btContext.bt_state == BT_STA_DISCONNECT_RETRY)
    {
        bt_gap_disconnect();
    }
    else if(btContext.bt_state == BT_STA_CONNECTING)
    {
        time_more = 0; //release time for ui task
    }
    else if(btContext.bt_state == BT_STA_BT_FAIL)
    {
    }
    else if(btContext.bt_state != BT_STA_STOP && btContext.bt_state != BT_STA_LOCK)
    {

    }

    return time_more;
}


void bt_gap_get_mac_addr(U8 *addr)
{
    Bt__GetMacAddress(addr);
}

void bt_gap_preset_local_name(U8 *str)
{
    if(strlen((char*)str) < 20)
        strcpy((char*)btContext.Local_BdName, (char*)str);
}


