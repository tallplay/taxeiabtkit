#include "bt_hid_hfp_api.h"
#include "btconfig.h"
#include "CORE\BTCore.h"
#include "GAP\LOCAL_DEV\Bt__LocalDevice.h"
#include "GAP\LL\Bt__RemoteDevice.h"
#include "hidpd\Bt__Profile_HIDD.h"
#include "spp\Bt__Profile_SPP.h"
#include "GAP\SECURITY\Bt__SecurityManager.h"
#include "TaxeiaConsoleDebug\TaxeiaConsoledebug.h"
#include "HFP\Bt__Profile_HFP.h"


#ifdef WIN32
#include "..\HAL\LocalOutput.h"
#include "..\HAL\MassUart.h"
#include "..\HAL\LocalStorage.h"
#else
#include "..\btHAL\LocalOutput.h"
#include "..\btHAL\MassUart.h"
#include "..\btHAL\LocalStorage.h"
#endif

#include <stdio.h>
#include <string.h>

extern U8 pad_data_get_report(U8 report_id, U8 len, U8 *dat);
extern U8 ps3_data_get_report(U8 type, U8 report, U8 *buf, int maxlen);
extern U8 SppRfcommAva(void);

#ifdef WIN32
void MtkPinInit(void)
{
    Taxeia_Report("Please Implement MtkPinInit function!!!\r\n");
}

U8 MtkWakeUpPin()
{
    return 0;
}

#define POWER_UP_TIME 1000

#else

#define POWER_UP_TIME 1000
#endif

#ifndef RECONNECT_TIME
#define RECONNECT_TIME 2
#endif

#define STRING2(x) #x
#define STRING(x) #x##" = "##STRING2(x)


#pragma message("======================================================================")
#pragma message(STRING(RECONNECT_TIME))
#pragma message("======================================================================")


#ifdef MT6622_PATCH
#define BOOT_TIME 25000
#else
#define BOOT_TIME 5000
#endif

#ifndef PAIR_TIME 
#define PAIR_TIME 200000
#endif

struct
{
    U8 bt_state;
    U8 hidcconnect : 1;
    U8 phbook : 1;
    U8 ate_found : 1;
    U8 pin_code_req : 1;
    U8 is_ps3 : 1;
    U8 sco_on : 1;
    U8 reserved : 2;

    U8 act_afterboot;
    U8 profile_type;
    U16 aclhandle;
    U8 reconnect_times;

    U32 bt_cur_tick;
    U32 bt_start_tick ;
    U32 bt_powerup_tick;
    U32 bt_pairing_tick;
    U32 bt_connect_limit;
#if (PROFILE_TYPE == PROFILE_TYPE_HID_HFP || PROFILE_TYPE == PROFILE_TYPE_HFP || PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)    
    U16 phbooknum;
    U16 phbookidx;    
    U8 callingnumber[20];
#endif    
    U8 Local_BdName[20];
    U8 Remote_Bdaddr[6];    
    U16 Remote_Bdaddr_clk;
    U8 Remote_BdName[26];
#ifdef  INIT_SET_AUTO_HOST_SLEEP
    U8 IsSniff;
#endif
} btContext;


#if (PROFILE_TYPE == PROFILE_TYPE_HID_HFP || PROFILE_TYPE == PROFILE_TYPE_HID || PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)

//parm[0]: 1: connect sucess 0: disconnect or connect fail
//parm[1]: 1: hidc 0:hidi
U8 *UpdateHidInfo(U16 parm)
{
    switch(parm & 0x3)
    {
        case 0: //hidi disconnect
            if(btContext.bt_state == BT_STA_DISCONNECTING)
            {
                Bt__HIDDCDisconnect();
            }                
            break;
        case 1: //hidi connect
            btContext.bt_state = BT_STA_HID_CONNECTED;
#if  (PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
            if(btContext.profile_type == PROFILE_TYPE_HID_HFP)
            {            
                Bt__HFPConnect(btContext.aclhandle);
            }
            else
#elif (PROFILE_TYPE == PROFILE_TYPE_HID_HFP)
            Bt__HFPConnect(btContext.aclhandle);
            break;
#endif      

            Bt__WriteScanEanbled(0); //no scan
            break;
        case 2: //hidc disconnect
            btContext.hidcconnect = 0;
            if(btContext.bt_state == BT_STA_DISCONNECTING)
            {
                Bt__DisconnRst(btContext.aclhandle, 0x13, 0);
            }
            break;
        case 3: //hidc connect
        {
            btContext.hidcconnect = 1;
            if(btContext.bt_state == BT_STA_CONNECTED)
            {
                Bt__HIDDIConnect(btContext.aclhandle); //create intr l2cap link
            }
        }
            break;
    }
        
    return 0;
}
#endif

#if (PROFILE_TYPE == PROFILE_TYPE_HID_HFP || PROFILE_TYPE == PROFILE_TYPE_HFP || PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)

static void(*PhoneBookCB)(U16 idx, U8* num, U8 *name);

static U8 *HFPCB (U16 parm)
{
    if(parm == HFPCB_PBITEM)
    {
        U8 *ptr;
        int idx;
        U8 *num;
        U8 *name;
        ptr = Bt__ProfileHFPGetCallBackBuf();

        idx = 0x1;
        sscanf((char*)ptr, "%d,", &idx);

        /* mark number */
        while(*ptr++ != '"');//find begin "
        num = ptr;
        while(*++ptr != '"');//find end "
        *ptr = 0; //mark end

        /* mark name */
        while(*ptr++ != '"');//find begin "
        name = ptr;
        if(name[0] == '"') //null
        {
            name[0] = 0;
        }
        else
        {
            while(*++ptr != '"');//find end "
            *ptr = 0; //mark end
        }

        if(PhoneBookCB)
        {
            btContext.phbookidx = idx;
            PhoneBookCB((U16)idx, num, name);
            if(!Bt__ProfileHFPGetPhoneBook((U16)idx+1))
            {
                btContext.phbook = 0;
                PhoneBookCB(0xffff, 0, 0); //end, no phone book
            }
        }
        
    }

    if(parm == HFPCB_PBNUMBER)
    {
        strcpy((char*)btContext.callingnumber, (char*)Bt__ProfileHFPGetCallBackBuf());
    }

    if(parm == 2) //l2cap disconnected
    {
        if(btContext.bt_state == BT_STA_DISCONNECTING)
        {
            Bt__DisconnRst(btContext.aclhandle, 0x13, 0);
        }
    }
    
    return 0;
}
#endif

#if (PROFILE_TYPE == PROFILE_TYPE_SPP || PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
static void (*sppcallback)(U16 len, U8 *dat) = 0;

static void SPPCB(U8 evt, U16 len, U8 *dat)
{
    if(evt == 2) //l2cap disconnected
    {
#ifdef FORCE_ACL_DISCONNECT_AFTER_RFCOMM_DISCONNECT
        if(btContext.bt_state == BT_STA_DISCONNECTING || btContext.bt_state == BT_STA_CONNECTED_INCOMING)
#else
        if(btContext.bt_state == BT_STA_DISCONNECTING)
#endif
        {
            Bt__DisconnRst(btContext.aclhandle, 0x13, 0);
        }
    }
    else if(evt == 0) //data incomming
    {
        if(sppcallback)
        sppcallback(len, dat);
    }    
}
#endif

BT_LOCAL_INFO *local_info;

#ifdef EXTEND_INQUIRY_RESP

static U8 Bt__WriteNameCB(U16 CmdOp, U8 EvtOp, U8 *dat)
{
    ASSERT(EvtOp == HCE_COMMAND_COMPLETE);
    if(dat[0] == HC_STATUS_SUCCESS)
    {
        if(CmdOp == HCC_CHNG_LOCAL_NAME) 
        {//success, then read back to update variable
            Bt_WriteExtInquiryRsp((U8*)btContext.Local_BdName, (U8)strlen((char*)btContext.Local_BdName));
        }
    }
    return 0;
}
#endif

U8 *LocalDeviceInfo(U16 parm)
{
    if(parm == LOCALCB_INITIALIZED)//initilized
    {
        btContext.bt_state = BT_STA_INITILIZED;
        //Bt__SetHostSleep(1);

        if(btContext.Local_BdName[0])
        {
#ifdef EXTEND_INQUIRY_RESP
            Bt__WriteLocalName((U8*)btContext.Local_BdName, (U8)strlen((char*)btContext.Local_BdName)+1, Bt__WriteNameCB);            
#else
            Bt__WriteLocalName((U8*)btContext.Local_BdName, (U8)strlen((char*)btContext.Local_BdName)+1);            
#endif
        }
    }

    if(parm == LOCALCB_SET_SLEEP) //5s
    {
        //btContext.bt_state = BT_STA_INITILIZED;
    }

    if(parm == LOCALCB_ENTER_SNIFF)
    {
#ifdef  INIT_SET_AUTO_HOST_SLEEP
        btContext.IsSniff = 1;
#endif
        Taxeia_Report("enter sniff\n");
    }

    if(parm == LOCALCB_SCAN_CHANGE)
    {
        Taxeia_Report("scan=%d\n", local_info->ScanEnabled);

        if(local_info->ScanEnabled == 3) //inquiry&page scan
        {
            btContext.bt_state = BT_STA_PAIRING;
        }
        else if(local_info->ScanEnabled == 0) //no scan
        {
            #ifdef ROLE_CHANGE_ON_ACCEPT  //redifined, role change on connected!!!
                if(btContext.bt_state == BT_STA_HID_CONNECTED) //only support HID now
                    Bt__WriteLinkSupervisionTimeout(btContext.aclhandle, 0x640*2); //1s
                #ifdef ROLE_CHANGE_ON_ACCEPT
                else
                #endif
            #endif

            #ifdef ROLE_CHANGE_ON_ACCEPT
                if(btContext.bt_state == BT_STA_SPP_CONNECTED) //only support HID now
                    Bt__WriteLinkSupervisionTimeout(btContext.aclhandle, 0x640); //1s
            #endif


        }
    }

    else if(parm == LOCALCB_PIN_CODE_REQ)
    {
        btContext.pin_code_req = 1;
    }

    if(btContext.bt_state == BT_STA_CONNECTED)
    {
        if(parm == LOCALCB_AUTH_SUCCESS)
        {
           Bt__SetConnectionEncryption(btContext.aclhandle, 1);
        }
        else if(parm == LOCALCB_ENCRYPTION_CHANGE)
        {
#if (PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
            if(btContext.profile_type == PROFILE_TYPE_HID)
                Bt__HIDDCConnect(btContext.aclhandle);
            else if(btContext.profile_type == PROFILE_TYPE_HFP)
                Bt__HFPConnect(btContext.aclhandle);
            else if(btContext.profile_type == PROFILE_TYPE_HID_HFP)
                Bt__HIDDCConnect(btContext.aclhandle);
            else if(btContext.profile_type == PROFILE_TYPE_SPP)
                Bt__SPPConnect(btContext.aclhandle);
#elif (PROFILE_TYPE == PROFILE_TYPE_HID)
                Bt__HIDDCConnect(btContext.aclhandle);
#elif (PROFILE_TYPE == PROFILE_TYPE_HFP)
                Bt__HFPConnect(btContext.aclhandle);
#elif (PROFILE_TYPE == PROFILE_TYPE_HID_HFP)
                Bt__HIDDCConnect(btContext.aclhandle);
#elif (PROFILE_TYPE == PROFILE_TYPE_SPP)
                Bt__SPPConnect(btContext.aclhandle);
#endif            
        }
        else if(parm == LOCALCB_AUTH_FAIL)
        {
            Bt__DisconnRst(btContext.aclhandle, 0x05, 0); //auth fail(reason);
        }
    }

    return 0;
}

extern void RegisterLLTestHandler(LLDataHandler lldh);

U8 * ACLConnectCB(U16 parm)
{
    U8 aclconnect;
    if(parm & 0x80) //sco
    {
        parm &= ~0x80;

        btContext.sco_on = parm ? 1 : 0;        
    }
    else
    {
        aclconnect = parm ? 1: 0;
        btContext.aclhandle = parm;
        
        if(aclconnect)
        {
            if(btContext.bt_connect_limit) btContext.bt_connect_limit--;
            Taxeia_Report("%d left\n", btContext.bt_connect_limit);

            if(btContext.bt_state == BT_STA_CONNECTING)
            {
                btContext.bt_state = BT_STA_CONNECTED;
#if (PROFILE_TYPE == PROFILE_TYPE_HID || PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
                if(btContext.is_ps3) //no security
                    Bt__HIDDCConnect(btContext.aclhandle);
                else
#endif                    
                    Bt__WriteAuthReq(btContext.aclhandle);
            }
            else if(btContext.bt_state == BT_STA_ATE_IDLE)
            {
                btContext.bt_state = BT_STA_ATE_CONNECTTED;
            }
            else if(btContext.bt_state == BT_STA_DUT_CONNECTING_ATE)
            {
                btContext.bt_state = BT_STA_DUT_CONNECTTED_ATE;
                Bt__WriteLinkSupervisionTimeout(btContext.aclhandle, 0x640); //1s
            }        
#ifdef EGIS_NO_ASSERT   

            else if(btContext.bt_state == BT_STA_PAIRING)
            {
                btContext.bt_state = BT_STA_CONNECTED_INCOMING;
            }
#endif            
        }
        else
        {
            Taxeia_Report("acl disconnected\r\n");
            btContext.pin_code_req = 0;        
            btContext.hidcconnect = 0;
            if(btContext.bt_state == BT_STA_CONNECTING)
            {
               btContext.bt_state = BT_STA_DISCONNECTED;

               if(btContext.reconnect_times < RECONNECT_TIME)
               {
                   Taxeia_Report("retry!!\r\n");
                   btContext.reconnect_times++;
                   bt_gap_connect(btContext.Remote_Bdaddr);
               }
            }
            else if(btContext.bt_state == BT_STA_ATE_CONNECTTED || btContext.bt_state == BT_STA_ATE_DISCONNECTTING)
                btContext.bt_state = BT_STA_ATE_IDLE;
            else if((btContext.bt_state != BT_STA_PAIRING) && (btContext.bt_state != BT_STA_ACTIVATON_REBOOT)) //in pairing, we don't want to tell upper disconnected happen, it will cause misunderstanding
            {
                btContext.bt_state = BT_STA_DISCONNECTED;
            }

        }        
    }    
    return 0;
}

void InquiryCB(U8 ret, U8 * dat)
{
    if(ret == 1) //command fail!!
        btContext.bt_state = BT_STA_DISCONNECTED;
    else if(ret == 0) //timeout or maximun result
    {
        if(btContext.ate_found)
        {
            btContext.bt_state = BT_STA_DUT_FOUND_ATE;
        }
        else
            btContext.bt_state = BT_STA_DISCONNECTED;
    }
    else
    {
        memcpy(btContext.Remote_Bdaddr, dat+1, 6);
        memcpy(&btContext.Remote_Bdaddr_clk, dat+13, 2);
        btContext.ate_found = 1;
    }
}

#ifdef BT_LOCK
U8 activated(U8*dat)
{
    U8 pout[32];
    extern U8 Bt__VerifyData(U8 *dat, U8 *pout);

    if(local_info->isAddressLocked)
    {
        if(!Bt__VerifyData(dat, pout))
        {
           StorageClear();
           StorageSetBytes(SM_SIZE, 32, dat);
           return 0;
        }
    }
    return 1;
}
#endif
void activation_cb(U16 len, U8 *dat)
{
    if(len == 32)
    {
#ifdef BT_LOCK
        if(!activated(dat)) //success
           bt_dut_send_data(32, dat);
        btContext.bt_state = BT_STA_ACTIVATON_REBOOT;
#endif
    }
}

void bt_dut_register_callback(void (*cb)(U16 len, U8 *dat))
{
    Bt__RemoteDeviceTestCB(cb);
}

void bt_dut_send_data(U16 len, U8 *dat)
{
    Bt__RemoteDeviceTest(len, dat);
}

void bt_ate_register_callback(void (*cb)(U16 len, U8 *dat))
{
    Bt__RemoteDeviceTestCB(cb);
}

void bt_ate_send_data(U16 len, U8 *dat)
{
    Bt__RemoteDeviceTest(len, dat);
}

U8 bt_dut_find_ate(U8 test_eq)
{
    if(btContext.bt_state == BT_STA_DISCONNECTED)
    {
        Bt__Inquiry(test_eq, InquiryCB);
        btContext.bt_state = BT_STA_DUT_FINDING_ATE;
        btContext.ate_found = 0;
        return 0;
    }
    return 1;
}

U8 bt_dut_connect_ate(void)
{
    if(btContext.bt_state == BT_STA_DUT_FOUND_ATE)
    {
        extern void Bt__ConnRstClkOffset(U8 *bdaddr, U16 clkoffset, U8);
        Bt__ConnRstClkOffset(btContext.Remote_Bdaddr, btContext.Remote_Bdaddr_clk | 0x8000, 0);
        btContext.bt_state = BT_STA_DUT_CONNECTING_ATE;
        return 0;
    }
    return 1;
}

U8 bt_ate_disconnect_dut(void)
{
    if(btContext.bt_state == BT_STA_ATE_CONNECTTED)
    {
        btContext.bt_state = BT_STA_ATE_DISCONNECTTING;
        Bt__DisconnRst(btContext.aclhandle, 0x13, 0);
        return 0;
    }
    return 1;
}

void bt_gap_read_link_rssi(void (*cb)(U8 CmdOp, U8 EvtOp, U8 *dat))
{
    Bt__ReadRSSI(btContext.aclhandle, cb);
}

void bt_gap_writesupervision_timeout(U16 timeout)
{
    Bt__WriteLinkSupervisionTimeout(btContext.aclhandle, timeout); //1s
}

U8 bt_gap_is_paired_exist(U8 *addr)
{
    return Bt__EnumPairedDevice(addr);
}

void bt_gap_storage_link_key(U8 *bdaddr, U8 *link_key, U8 type)
{
    extern void Bt__StoreLinkKey(U8 *bdaddr, U8 *link_key, U8 type);

    Bt__StoreLinkKey(bdaddr, link_key, type);
}

U8 bt_gap_is_pincode_req(void)
{
    if(btContext.pin_code_req)
    {
        btContext.pin_code_req = 0;
        return 1;
    }

    return 0;
}

U8 bt_gap_pincode_reply( U8 *pin, U8 len)
{
    return Bt__PinCodeReply(btContext.Remote_Bdaddr, pin, len);
}

//0: success else: error
U8 bt_gap_connect(U8 *bt_addr)
{
    U8 addr[6];
    U8 device_find = 0;

    if(!btContext.bt_connect_limit)
        return 3;

    if(!Bt_AclReady())
        return 2;

    if(btContext.bt_state == BT_STA_DISCONNECTED || btContext.bt_state == BT_STA_PAIRING)
    {
        device_find = 1;
        if(!bt_addr)
        {
            if(!Bt__EnumPairedDevice(addr))
            {
                device_find = 0;
            }
        }
        else
            memcpy(addr, bt_addr, 6);

        if(device_find)
        {
            btContext.bt_state = BT_STA_CONNECTING;
            Bt__ConnRst(addr, 0);
            memcpy(btContext.Remote_Bdaddr, addr, 6);

            return 0;
        }

        return 4;        
    }

    return 1;
}

U8 bt_gap_disconnect(void)
{    
    if(!Bt_AclReady())
    {
        btContext.bt_state = BT_STA_DISCONNECT_RETRY;
        return 2;
    }

#if (PROFILE_TYPE == PROFILE_TYPE_HID || PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
    if(btContext.profile_type == PROFILE_TYPE_HID)
    {
        if(btContext.bt_state == BT_STA_HID_CONNECTED || btContext.bt_state == BT_STA_DISCONNECT_RETRY)
        {
            btContext.bt_state = BT_STA_DISCONNECTING;
            Bt__HIDDIDisconnect();
            return 0;
        }
        else if(btContext.bt_state == BT_STA_CONNECTED)
        {
            btContext.bt_state = BT_STA_DISCONNECTING;
            Bt__DisconnRst(btContext.aclhandle, 0x05, 0); //auth fail(reason);
            return 0;
        }
    }
#endif    
#if (PROFILE_TYPE == PROFILE_TYPE_HFP || PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
    else if(btContext.profile_type == PROFILE_TYPE_HFP)
    {
        if(btContext.bt_state == BT_STA_HFP_CONNECTED || btContext.bt_state == BT_STA_DISCONNECT_RETRY)
        {
            btContext.bt_state = BT_STA_DISCONNECTING;
            Bt__HFPDisconnect();
            return 0;
        }
        else if(btContext.bt_state == BT_STA_CONNECTED)
        {
            btContext.bt_state = BT_STA_DISCONNECTING;
            Bt__DisconnRst(btContext.aclhandle, 0x05, 0); //auth fail(reason);
            return 0;
        }
    }
#endif
#if (PROFILE_TYPE == PROFILE_TYPE_SPP || PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
    else if(btContext.profile_type == PROFILE_TYPE_SPP)
    {
        if(btContext.bt_state == BT_STA_SPP_CONNECTED || btContext.bt_state == BT_STA_DISCONNECT_RETRY)
        {
            btContext.bt_state = BT_STA_DISCONNECTING;
            Bt__SPPDisconnect();
            return 0;
        }
        else if(btContext.bt_state == BT_STA_CONNECTED || btContext.bt_state == BT_STA_CONNECTED_INCOMING)
        {
            btContext.bt_state = BT_STA_DISCONNECTING;
            Bt__DisconnRst(btContext.aclhandle, 0x05, 0); //auth fail(reason);
            return 0;
        }
    }
#endif
#if (PROFILE_TYPE == PROFILE_TYPE_DYNAMIC || PROFILE_TYPE == PROFILE_TYPE_HID_HFP)
    else if(btContext.profile_type == PROFILE_TYPE_HID_HFP)
    {
        if(btContext.bt_state == BT_STA_HID_CONNECTED || btContext.bt_state == BT_STA_HID_HFP_CONNECTED || btContext.bt_state == BT_STA_DISCONNECT_RETRY)
        {
            btContext.bt_state = BT_STA_DISCONNECTING;
            Bt__HIDDIDisconnect();
            return 0;
        }
        else if(btContext.bt_state == BT_STA_CONNECTED)
        {
            btContext.bt_state = BT_STA_DISCONNECTING;
            Bt__DisconnRst(btContext.aclhandle, 0x05, 0); //auth fail(reason);
            return 0;
        }
    }
#endif    
    
    return 1;
}

#ifdef INIT_SET_AUTO_HOST_SLEEP
void bt_reset_connect_retry()
{
    btContext.reconnect_times = 0;
}

U8 bt_set_mcu_sleep_time(U32 ms)
{
   //ms 5000=>8000
    if(!Bt_AclReady())
        return 2;
    
    Bt__SetHostSleepTime(1, ms);
    return 0;
}

U8 bt_reset_mcu_sleep_time()
{
    if(!Bt_AclReady())
        return 2;
    
    Bt__SetHostSleep(0);
    return 0;
}

void bt_set_sniff()
{
    if(btContext.IsSniff == 0)
    {
        if(btContext.bt_state == BT_STA_HID_CONNECTED)
        {
            Bt_EnableLinkMode(btContext.aclhandle, 0x05);

            Bt__SniffRst(btContext.aclhandle, 200, 28);
            btContext.IsSniff = 1; //trick, haha
        }
    }
}

void bt_leave_sniff()
{
    if(btContext.IsSniff == 1)
    {
        if(btContext.bt_state == BT_STA_HID_CONNECTED)
        {
            Bt__SniffExit(btContext.aclhandle);
            Bt_EnableLinkMode(btContext.aclhandle, 0x00);

            btContext.IsSniff = 0;
        }
    }
}
#endif


U8 bt_gap_is_address_locked(void)
{
    U8 addr[6];
    U8 locked;
    locked = Bt__GetMacAddress(addr);
    return locked;
}

#ifndef BT_LOCK
    #ifndef BT_UNLOCK_CONNECT_LIMIT
        #define BT_UNLOCK_CONNECT_LIMIT 0xffffffff
    #endif
#endif

void bt_init(void)
{
    MtkPinInit();

    btContext.bt_state = BT_STA_STOP;
    btContext.hidcconnect = 0;
    btContext.reconnect_times = 0;
    btContext.act_afterboot = 0;
    btContext.is_ps3 = 0;
#ifdef  INIT_SET_AUTO_HOST_SLEEP
    btContext.IsSniff = 0;
#endif
    
#ifdef BT_LOCK
d
    btContext.bt_connect_limit = 10;
#else
    btContext.bt_connect_limit = BT_UNLOCK_CONNECT_LIMIT;
#pragma message("======================================================================")
#pragma message(STRING(BT_UNLOCK_CONNECT_LIMIT))
#pragma message("======================================================================")


#endif
    btContext.Local_BdName[0] = 0;
    btContext.pin_code_req = 0;

    Bt__GetLocalInfo(&local_info);      
}

U8 bt_start(U32 CurTick, U8 mode, U8 profile_type)
{

    if(btContext.bt_state == BT_STA_STOP)
    {
        /*mtk power on*/
        MtkPower(1);
        btContext.bt_state = BT_STA_POWERUP;

        btContext.bt_powerup_tick = CurTick;
        btContext.act_afterboot = mode;
        btContext.profile_type = profile_type;
        return 0;
    }

    return 1;
#if 0    
    else if(btContext.bt_state == BT_STA_DISCONNECTED) //quick connect
    {
        if(!bt_gap_connect(0))
        {
            btContext.reconnect_times = 0;
        }
    }
#endif    
}

void bt_stop(void)
{
    BTCoreStop();

    MtkPower(0); //bt power off

    btContext.bt_state = BT_STA_STOP;
    btContext.hidcconnect = 0;
}

static void bt_lock(void)
{
   btContext.bt_state = BT_STA_LOCK;
   BTCoreStop();
   MtkPower(0); //bt power off
}

void bt_gap_set_scan_mode(U32 curTick, U8 mode)
{
    if(mode == BT_WMODE_INQUIRY_SCAN)
    {
        if(btContext.bt_state == BT_STA_INITILIZED || btContext.bt_state == BT_STA_DISCONNECTED)
        {
            btContext.bt_state = BT_STA_PAIRING;
            btContext.bt_pairing_tick = curTick;
            Bt__WriteScanEanbled(3);
        }
    }
    else
    {
        if(btContext.bt_state == BT_STA_PAIRING)
            btContext.bt_state = BT_STA_DISCONNECTED;

         if(mode == BT_WMODE_PAGING_SCAN)
            Bt__WriteScanEanbled(2);
         else if(mode == BT_WMODE_NO_SCAN)
            Bt__WriteScanEanbled(0);
    }
}

void bt_set_freq_test(U8 freq, U8 type, U8 pattern, U16 packet_length)
{
    Bt__FreqTest(freq, freq, type, pattern, packet_length);
}

void bt_set_ate(U8 test_mode) //from 0x01~0x32
{
    if(btContext.bt_state == BT_STA_HID_CONNECTED)
    {
        btContext.bt_state = BT_STA_ATE_IDLE;
        Bt__SetIAC(test_mode);
    }
}

U8 bt_state(void)
{
    return btContext.bt_state;
}


U8 bt_task(U32 curTick)
{
    static U8 last_wakeup_pin = 0;
        
    STACKSTATE sta;
    U8 time_more = 0;

    btContext.bt_cur_tick = curTick;
    
    BTCoreLoop();
    sta = BTCoreState();
    
    if( sta == BUSY || sta == RXBUSY)
    {
        time_more = 1;
    }

//monitor HFP
#if (PROFILE_TYPE == PROFILE_TYPE_HFP || PROFILE_TYPE == PROFILE_TYPE_HID_HFP || PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
        if(Bt__ProfileHFPConnectStatus() == 1)
        {
            if(btContext.bt_state == BT_STA_HID_CONNECTED)
                btContext.bt_state = BT_STA_HID_HFP_CONNECTED;                
            else if(btContext.bt_state == BT_STA_PAIRING || btContext.bt_state == BT_STA_CONNECTED)
                btContext.bt_state = BT_STA_HFP_CONNECTED;

        }
        else
        {
            if(btContext.bt_state == BT_STA_HID_HFP_CONNECTED)
                btContext.bt_state = BT_STA_HID_CONNECTED;
        }
#endif        
//monitor SPP
#if (PROFILE_TYPE == PROFILE_TYPE_SPP || PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
        if(Bt__ProfileSPPConnectStatus() == 1)
        {
            btContext.bt_state = BT_STA_SPP_CONNECTED;
        }
        else if(btContext.bt_state == BT_STA_SPP_CONNECTED)
        {
            btContext.bt_state = BT_STA_CONNECTED_INCOMING;
        }
#endif        

    if(btContext.bt_state == BT_STA_POWERUP)
    {
       //1 second to initial
        if(curTick > btContext.bt_powerup_tick + POWER_UP_TIME)
        {                      
            if((btContext.act_afterboot & 0x40) != BT_WMODE_FREQ_TEST)
            {
            #if (PROFILE_TYPE == PROFILE_TYPE_HID)
                if(btContext.is_ps3)  
                    BTCoreStart(PROFILE_TYPE_PS3);
                else
                    BTCoreStart(PROFILE_TYPE_HID);
            #else
                BTCoreStart(PROFILE_TYPE);
            #endif

            #if (PROFILE_TYPE == PROFILE_TYPE_HID)
                Bt__ProfileHIDDCB(UpdateHidInfo);
            #elif (PROFILE_TYPE == PROFILE_TYPE_HFP)
                Bt__ProfileHFPCB(HFPCB);
            #elif (PROFILE_TYPE == PROFILE_TYPE_SPP) 
                Bt__ProfileSPPCB(SPPCB);
            #elif (PROFILE_TYPE == PROFILE_TYPE_HID_HFP)
                Bt__ProfileHIDDCB(UpdateHidInfo);
                Bt__ProfileHFPCB(HFPCB);
            #elif (PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
                if(btContext.profile_type == PROFILE_TYPE_HID_HFP) //two profiles
                {
                    BtCoreProfileInit(btContext.profile_type);
                    Bt__ProfileHIDDCB(UpdateHidInfo);
                    Bt__ProfileHFPCB(HFPCB);
                }
                else
                {
                    BtCoreProfileInit(btContext.profile_type);
                    if(btContext.profile_type == PROFILE_TYPE_HID)
                        Bt__ProfileHIDDCB(UpdateHidInfo);
                    else if(btContext.profile_type == PROFILE_TYPE_HFP)
                        Bt__ProfileHFPCB(HFPCB);
                    else if(btContext.profile_type == PROFILE_TYPE_SPP)
                        Bt__ProfileSPPCB(SPPCB);
                }
            #endif
                Bt__RemoteBdaddrReg(btContext.Remote_Bdaddr);
                Bt__RemoteBdNameReg(btContext.Remote_BdName);
            
            }
            else //freq_test
            {
                BTCoreStart(PROFILE_TYPE_TEST);
                Bt__RemoteBdaddrReg(0);
                Bt__RemoteBdNameReg(0);
            }

            Bt__LocalDeviceCB(LocalDeviceInfo);
            Bt__RemoteDeviceCB(ACLConnectCB);
            

            btContext.bt_state = BT_STA_BOOT;
            btContext.bt_start_tick = curTick;

            #ifdef BT_LOCK
                Taxeia_Report("bt_lock = %d\n", local_info->isAddressLocked);
                if(!local_info->isAddressLocked)
                    btContext.bt_connect_limit = 0xffffffff;
            #endif

        }

        time_more = 1;
    }    
    else if(btContext.bt_state == BT_STA_BOOT)
    {

#ifdef BT_CUSTOMIZE_COD
            U32 Bt__CustomizeCOD(void);

            local_info->COD = Bt__CustomizeCOD();
#endif
        //5 second to timeout
        if(curTick > btContext.bt_start_tick + (BOOT_TIME))
        {            
            bt_stop();
            btContext.bt_state = BT_STA_BT_FAIL;
        }            

        time_more = 1;

    }
    else if(btContext.bt_state == BT_STA_INITILIZED)
    {       
        btContext.bt_state = BT_STA_DISCONNECTED;

        switch(btContext.act_afterboot)
        {
            case BT_WMODE_INQUIRY_SCAN://inquriy scan
            {
                btContext.bt_state = BT_STA_PAIRING;
                btContext.bt_pairing_tick = curTick;
                Bt__WriteScanEanbled(3);
                break;
            }
            case BT_WMODE_PAGING://paging
            {
                U8 ret;
                ret = bt_gap_connect(0);
                if(!ret)
                {
                    btContext.reconnect_times = 0;
                }
                else if(ret == 4 || ret == 3) //no paired device, limit
                {
#ifdef WMODE_PAGING_NO_STOP
#else
                    bt_stop();
#endif
                }
                else if(ret == 2)//hci busy
                    btContext.bt_state = BT_STA_INITILIZED; //keeyp retry
                //else keep retry
                break;
            }
            case BT_WMODE_PAGING_SCAN://paging scan
            {
                Bt__WriteScanEanbled(2);
                break;
            }
            case BT_WMODE_ACTIVATION_ENABLED:
            {
                bt_dut_register_callback(activation_cb);
                bt_dut_find_ate(0x31);
                break;
            }               
            default:
            {
                if((btContext.act_afterboot & 0x80) == BT_WMODE_ATE)
                {
                    btContext.bt_state = BT_STA_ATE_IDLE;
                    Bt__SetIAC(btContext.act_afterboot & 0x7f);
                }
                else if((btContext.act_afterboot & 0x40) == BT_WMODE_FREQ_TEST)
                {
                   // Bt__FreqTest(39, 39, 0x00, 0x0a/*1111*/, 00);
                    btContext.bt_state = BT_STA_FREQ_TEST;
                }
                break;
            }
        }//switch
    }
    else if(btContext.bt_state == BT_STA_DISCONNECT_RETRY)
    {
        bt_gap_disconnect();
    }
    else if(btContext.bt_state == BT_STA_PAIRING)
    {
        U8 tmp;

        if(curTick > btContext.bt_pairing_tick + (PAIR_TIME))
        {
            //bt_stop
            bt_gap_set_scan_mode(curTick, 0);
        }


        tmp = MtkWakeUpPin();
        if(!last_wakeup_pin && tmp)
        {
            Taxeia_Report("wkup\r\n");
            Bt_SetHostWakeup();
            time_more = 1;
        }

        last_wakeup_pin = tmp;

    }
    else if(btContext.bt_state == BT_STA_DUT_FINDING_ATE || btContext.bt_state == BT_STA_DUT_CONNECTING_ATE)
    {
        time_more = 0; //release time for ui task
    }
    else if(btContext.bt_state == BT_STA_CONNECTING)
    {
        time_more = curTick & 100; //random
    }
    else if(btContext.bt_state == BT_STA_BT_FAIL)
    {    
    }
    else if(btContext.bt_state != BT_STA_STOP && btContext.bt_state != BT_STA_LOCK)
    {
        U8 tmp;

        tmp = MtkWakeUpPin();
        
        if(!last_wakeup_pin && tmp)
        {
            Taxeia_Report("wkup1\r\n");
            Bt_SetHostWakeup();
            time_more = 1;
        }

        last_wakeup_pin = tmp;
        if(!btContext.bt_connect_limit)
            bt_lock();

    }

    return time_more;
}

U8 bt_gap_get_scan_mode(void)
{
    return local_info->ScanEnabled;
}

void bt_gap_get_mac_addr(U8 *addr)
{
    Bt__GetMacAddress(addr);
}

void bt_gap_preset_local_name(U8 *str)
{
    if(strlen((char*)str) < 20)
        strcpy((char*)btContext.Local_BdName, (char*)str);
}

void bt_gap_set_local_name(U8 *str)
{
    if(btContext.bt_state >= BT_STA_INITILIZED)
#ifdef EXTEND_INQUIRY_RESP
        Bt__WriteLocalName((U8*)str, (U8)strlen((char*)str)+1, 0);
#else
        Bt__WriteLocalName((U8*)str, (U8)strlen((char*)str)+1);
#endif
}

U8 *bt_gap_get_remote_name(void)
{
    return btContext.Remote_BdName;
}

U8 *bt_gap_get_remote_addr(void)
{
    return btContext.Remote_Bdaddr;
}


U8 bt_gap_is_sco_on(void)
{
    return btContext.sco_on;
}

#if (PROFILE_TYPE == PROFILE_TYPE_HID_HFP || PROFILE_TYPE == PROFILE_TYPE_HID || PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
void bt_hid_ps3_mode(void)
{
    btContext.is_ps3 = 1;
}


#ifdef DYNAMIC_HID_DESCRIPTOR
U8 bt_hid_send_key_mouse_joystick_media(U8 type)
{
 //1: mouse, 2: keyboard, 3: media, 4:gampad, 5:ps3
    U8 data[48], len;

    if(!Bt_AclReady())
        return 1;

    data[0] = 0xA1;
    
    data[1] = type;
    
    if(len = pad_data_get_report(data[1], 48, data+2))
    {
        len += 2;
        Bt__ProfileHIDDISendData(data, len);        
        return 0;
    }

    return 1;
}

#else
U8 bt_hid_send_key_mouse_joystick_media(U8 type)
{
 //1: mouse, 2: keyboard, 3: media, 4:gampad, 5:ps3
    U8 data[48], len;

    if(!Bt_AclReady())
        return 1;

    data[0] = 0xA1;
    
    if(type == 4)//game
    {
        data[1] = 4; //report_id=4;        
    }
    else if(type == 2) //keyboard
    {
        data[1] = 1; //report_id=1;        
    }
    else if(type == 1) //mouse
    {
        data[1] = 2; //report_id = 2;
    }
    else if(type == 3) //mdeia
    {
        data[1] = 3; //report_id = 3;
    }
    else if(type == 5) //PS3
    {
        data[1] = 1;
        if(len = ps3_data_get_report(0x1, 0x1, data+2, 48))
        {    
            len += 2;

            Bt__ProfileHIDDISendData(data, len);
        }
        return 0;
    }

    if(len = pad_data_get_report(data[1], 48, data+2))
    {
        len += 2;
        Bt__ProfileHIDDISendData(data, len);        
        return 0;
    }

    return 1;
}
#endif

/*
void bt_hid_send_key(void)
{
    U8 data[50], len;
    
    if(!Bt_AclReady())
        return;

    if(btContext.hid_mode == BT_HMODE_JOYSTICK)
    {
        data[0] = 0xA1;
        data[1] = 4; //report_id;
        
        if(len = pad_data_get_report(4, 48, data+2))
        {
            len += 2;
            Bt__ProfileHIDDISendData(data, len);
        }
    }
    else if(btContext.hid_mode == BT_HMODE_KEYBOARD || btContext.hid_mode == BT_HMODE_KEYBOARD_MOUSE)
    {
        data[0] = 0xA1;
        data[1] = 1; //report_id;
        
        if(len = pad_data_get_report(1, 48, data+2))
        {
            len += 2;
            Bt__ProfileHIDDISendData(data, len);
        }
    }
    else if(btContext.hid_mode == BT_HMODE_PS3)
    {
        data[0] = 0xa1;
        data[1] = 0x01;
        if(len = ps3_data_get_report(0x1, 0x1, data+2, 48))
        {    
            len += 2;

            Bt__ProfileHIDDISendData(data, len);
        }

    }
}

void bt_hid_send_mouse(void)
{
    U8 data[10], len;
    
    if(!Bt_AclReady())
        return;
    
    if(btContext.hid_mode == BT_HMODE_KEYBOARD_MOUSE)
    {
        data[0] = 0xA1;
        data[1] = 2; //report_id;
        if(len = pad_data_get_report(2, 48, data+2))
        {
            len += 2;
            Bt__ProfileHIDDISendData(data, len);
        }
    }
}
*/

#ifdef DYNAMIC_HID_DESCRIPTOR
void bt_hid_set_hid_descriptor(U8 *desc)
{
    Bt_ProfileHIDAssignDescriptor(desc);
}
#endif


#endif

//HFP status
#if (PROFILE_TYPE == PROFILE_TYPE_HID_HFP || PROFILE_TYPE == PROFILE_TYPE_HFP || PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
void bt_hfp_get_phonebook(void(*pbitemcb)(U16 idx, U8* num, U8 *name))
{
    U16 ret;
    ret = Bt__ProfileHFPGetPhoneBook(1);

    if(!ret)
        pbitemcb(0xffff, 0, 0); //end, no phone book
    else
    {
        btContext.phbook = 1;
        btContext.phbooknum = ret;
        btContext.phbookidx = 0;
        PhoneBookCB = pbitemcb;
    }
}

U8 bt_hfp_get_phonebook_status(U16 *total, U16 *idx)
{
    *total = btContext.phbooknum;
    *idx = btContext.phbookidx;
    return btContext.phbook;
}

void bt_hfp_get_staus(U8 *signal, U8 *battery, U8 *service, U8 *callsetup, U8 *call)
{
    *signal = Bt__ProfileHFPStatus(IND_SIGNAL);
    *battery = Bt__ProfileHFPStatus(IND_BATTARY);
    *service = Bt__ProfileHFPStatus(IND_SERVICE);
    *callsetup = Bt__ProfileHFPStatus(IND_CALLSETUP);
    *call = Bt__ProfileHFPStatus(IND_CALL);

    if(*callsetup == 0)
        btContext.callingnumber[0] = 0;
}

U8 *bt_hfp_get_callingnumber(void)
{
    return btContext.callingnumber;
}

void bt_hfp_connect(void)
{
    if(btContext.bt_state == BT_STA_HID_CONNECTED)
    {
        //no need to change state
        //btContext.bt_state = ???
        Bt__HFPConnect(btContext.aclhandle);
    }
}

void bt_hfp_disconnect(void)
{
    if(btContext.bt_state == BT_STA_HID_HFP_CONNECTED)
    {
        //no need to change state
        //btContext.bt_state = ???
        Bt__HFPDisconnect();
    }
}
#endif

#if (PROFILE_TYPE == PROFILE_TYPE_SPP || PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
void bt_spp_register_callback(void (*cb)(U16 len, U8 *dat))
{
    sppcallback = cb;
}

U8 bt_spp_tx_ava(void)
{
    return SppRfcommAva();
}

void bt_spp_tx(U16 len, U8* dat)
{
    extern U8 Bt__ProfileSPPSendDataDirect(U8 *dat, U16 len);

    Bt__ProfileSPPSendDataDirect(dat, len);
//    Bt__ProfileSPPSendData(dat, len);
}
#endif

