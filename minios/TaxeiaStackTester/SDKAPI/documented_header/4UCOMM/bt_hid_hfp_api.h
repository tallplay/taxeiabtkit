/** @mainpage Taxeia BT Stack Library
  *
*/
/** @defgroup Taxeia BT Stack Functions
 *  API functions for Taxeia BT Stack.
 *  @{
*/

#ifndef __BT_HID_HFP_API_HH__
#define __BT_HID_HFP_API_HH__
#include "bttype.h"

#ifdef __cplusplus
extern "C" {
#endif

//bt state////////////////////////////////////////////////////////////////////////////
/**
 * @def BT_STA_STOP
 * BT stack state is in stop state.
*/
#define BT_STA_STOP 0

/**
 * @def BT_STA_POWERUP
 * BT stack state is in hardware Power Up state.
*/
#define BT_STA_POWERUP 1

/**
 * @def BT_STA_BOOT
 * BT stack state is in hardware Boot state.
*/
#define BT_STA_BOOT 2

/**
 * @def BT_STA_INITILIZED
 * BT stack state is initilized.
*/
#define BT_STA_INITILIZED 3

/**
 * @def BT_STA_PAIRING
 * BT stack state is in pairing state.
*/
#define BT_STA_PAIRING 4

/**
 * @def BT_STA_CONNECTING
 * BT stack state is in connection state.
*/
#define BT_STA_CONNECTING 5

/**
 * @def BT_STA_CONNECTED
 * BT stack state is in connected state.
*/
#define BT_STA_CONNECTED 6       //outgoing link

/**
 * @def BT_STA_HID_CONNECTED
 * BT stack state is in HID profile connected state.
*/
#define BT_STA_HID_CONNECTED 7

/**
 * @def BT_STA_HFP_CONNECTED
 * BT stack state is in HFP profile connected state.
*/
#define BT_STA_HFP_CONNECTED 8

/**
 * @def BT_STA_HID_HFP_CONNECTED
 * BT stack state is in HID&HFP profile connected state.
*/
#define BT_STA_HID_HFP_CONNECTED 9

/**
 * @def BT_STA_DISCONNECTING
 * BT stack state is in disconnecting state.
*/
#define BT_STA_DISCONNECTING 10

/**
 * @def BT_STA_DISCONNECTED
 * BT stack state is in disconnected state.
*/
#define BT_STA_DISCONNECTED 11

/**
 * @def BT_STA_BT_FAIL
 * BT stack state is in hardware error state.
*/
#define BT_STA_BT_FAIL 12

/**
 * @def BT_STA_DISCONNECT_RETRY
 * BT stack state is retrying to disconnect with remote host.
*/
#define BT_STA_DISCONNECT_RETRY 13

/**
 * @def BT_STA_LOCK
 * BT stack state is lock state. Authorization is expired, please reset hardware!
*/
#define BT_STA_LOCK 14

//bt wireless mode///////////////////////////////////////////////////////////////////////////
/**
 * @def BT_WMODE_NO_SCAN
 * BT stack wireless mode is in "No Scan" mode.
*/	
#define BT_WMODE_NO_SCAN 0

/**
 * @def BT_WMODE_PAGING_SCAN
 * BT stack wireless mode is in "Paging Scan" mode.
*/	
#define BT_WMODE_PAGING_SCAN 2

/**
 * @def BT_WMODE_INQUIRY_SCAN
 * BT stack wireless mode is in "Inquiry Scan" mode.
*/	
#define BT_WMODE_INQUIRY_SCAN 3

/**
 * @def BT_WMODE_PAGING
 * BT stack wireless mode is in "Paging" mode.
*/	
#define BT_WMODE_PAGING 4

/**
 * @def BT_WMODE_PAGING_INQUIRY_SCAN
 * BT stack wireless mode is in "Paging & Inquiry Scan" mode.
*/	
#define BT_WMODE_PAGING_INQUIRY_SCAN 5


#define BT_WMODE_ACTIVATION_ENABLED 6

//BT CORE API
/** 
 * @fn void bt_init(void)
 * @brief BT Stack Initialization
 * 
 * Call bt_init to initial BT stack;
 *	
 * @return No return value.
*/
void bt_init(void);

/** 
 * @fn U8 bt_task(U32 curTick)
 * @brief Perform a single execution step of BT stack.
 * 
 * Call bt_task to perform a single task execution of BT stack task. bt_task should be called repeatedly until system exit.
 *	
 * @param [in] curTick Current SystemTick (1ms).
 * @return Return 1 if stack is busy or 0 if stakc is idle.
*/
U8 bt_task(U32 curTick);

/** 
 * @fn void bt_start(U32 curTick, U8 mode, U8 profile_type)
 * @brief Start BT Stack Execution
 * 
 * Call bt_start to start executing of BT stack.
 *	
 * @param [in] curTick Current SystemTick (1ms).
 * @param [in] mode Start BT wireless mode.
 * @param [in] profile_type Start BT profile type. 
 * @return No return value.
*/
void bt_start(U32 curTick, U8 mode, U8 profile_type);

/** 
 * @fn void bt_stop(void)
 * @brief Stop BT Stack Execution
 * 
 * Call bt_stop to stop executing of BT stack.
 *	
 * @return No return value.
*/
void bt_stop(void);

/** 
 * @fn U8 bt_state(void)
 * @brief Get current BT Stack state
 * 
 * Call bt_state to get the current BT stack state.
 *	
 * @return Return the current state of BT stack.
*/
U8 bt_state(void);

//BT GAP API
/** 
 * @fn U8 bt_gap_is_paired_exist(U8 *addr)
 * @brief Check if BT stack has been paired.
 * 
 * Call bt_gap_is_paired_exist to check if BT stack has been paired and return the paired remote BT address.
 *	
 * @param [out] addr BT mac address of the paired remote host.
 * @return Return 1 if BT hasck has been paired or 0 if no remote host has been paired.
*/
U8 bt_gap_is_paired_exist(U8 *addr);

/** 
 * @fn U8 bt_gap_connect(U8 *addr)
 * @brief Connect to Remote Host
 * 
 * Call bt_gap_connect to connect to remote host.
 *	
 * @param [in] addr BT mac address of the remote host.
 * @return Return 0 if function call success or else if function failed.
*/
U8 bt_gap_connect(U8 *addr);

/** 
 * @fn U8 bt_gap_disconnect(void)
 * @brief Disconnect with Connected Remote Host
 * 
 * Call bt_gap_disconnect to disconnect with the connected remote host.
 *	
 * @return Return 0 if function call success or else if function failed.
*/
U8 bt_gap_disconnect(void);

/** 
 * @fn void bt_gap_set_local_name(U8 *str)
 * @brief Set BT Local Name
 * 
 * Call bt_gap_set_local_name to set the BT local name. This function can only be called after bt_start.
 *	
 * @param [in] str BT local name to be set. str must be less than or equal to 20 byte including the null terminator.
 * @return No return value.
*/
void bt_gap_set_local_name(U8 *str);

/** 
 * @fn void bt_gap_preset_local_name(U8 *str)
 * @brief Preset BT Local Name
 * 
 * Call bt_gap_preset_local_name to preset the BT local name. This function can only be called before bt_start.
 *	
 * @param [in] str BT local name to be set. str must be less than or equal to 20 byte including the null terminator.
 * @return No return value.
*/
void bt_gap_preset_local_name(U8 *str);

/** 
 * @fn void bt_gap_get_mac_addr(U8 *addr)
 * @brief Get local BT Address
 * 
 * Call bt_gap_get_mac_addr to get the local BT mac address.
 *	
 * @param [out] addr BT address of local device.
 * @return No return value.
*/
void bt_gap_get_mac_addr(U8 *addr);

/** 
 * @fn U8 bt_gap_get_scan_mode(void)
 * @brief Get Current BT Wireless Mode
 * 
 * Call bt_gap_get_scan_mode to get the current BT stack wireless mode.
 *	
 * @return Return the current BT stack wireless mode.
*/
U8 bt_gap_get_scan_mode(void);

/** 
 * @fn void bt_gap_set_scan_mode(U32 curTick, U8 mode)
 * @brief Set BT State Wireless Mode
 * 
 * Call bt_gap_set_scan_mode to set BT state wireless mode.
 *	
 * @param [in] curTick Current SystemTick (1ms).
 * @param [in] mode BT wireless mode.
 * @return No return value.
*/
void bt_gap_set_scan_mode(U32 curTick, U8 mode);

/** 
 * @fn U8* bt_gap_get_remote_name(void)
 * @brief Get BT Name of the Connected Remote Host
 * 
 * Call bt_gap_get_remote_name to get the BT name of the connected remote host.
 *	
 * @return Return the BT name of the connected remote host.
*/
U8 *bt_gap_get_remote_name(void);

/** 
 * @fn U8* bt_gap_get_remote_addr(void)
 * @brief Get Connected BT Address of Remote Host
 * 
 * Call bt_gap_get_remote_addr to get the BT mac address of the connected remote host.
 *	
 * @return Return the BT mac address of the connected remote host.
*/
U8 *bt_gap_get_remote_addr(void);

/** 
 * @fn U8 bt_gap_is_pincode_req(void)
 * @brief Check if Pin Code Request has been issued.
 * 
 * Call bt_gap_is_pincode_req to check if Pin Code Request has been issued and waiting for reply while BT pairing.
 *	
 * @return Return 1 if Pin Code Request has been issued.
*/
U8 bt_gap_is_pincode_req(void);

/** 
 * @fn U8 bt_gap_pincode_reply(U8 *pin, U8 len)
 * @brief Reply the Pin Code Request
 * 
 * Call bt_gap_pincode_reply to reply the Pin Code Request while BT pairing.
 *	
 * @param [in] pin Pin Code to be replied.
 * @param [in] len Length of Pin Code
 * @return Return 1 if function success or 0 if function failed.
*/
U8 bt_gap_pincode_reply(U8 *pin, U8 len);

/** 
 * @fn U8 bt_gap_is_sco_on(void)
 * @brief Check if SCO Connection Established
 * 
 * Call bt_gap_is_sco_on to check if SOC connection has been established.
 *	
 * @return Return 1 if SCO connection has been established.
*/
U8 bt_gap_is_sco_on(void);

/** 
 * @fn void bt_gap_storage_link_key(U8 *bdaddr, U8 *link_key, U8 type)
 * @brief Store BT Link Key to Local Storage
 * 
 * Call bt_gap_storage_link_key to store BT link key into local storage.
 *	
 * @param [in] bdaddr BT address of remote host
 * @param [in] link_key BT link key
 * @param [in] type Store type
 * @return No return value.
*/
void bt_gap_storage_link_key(U8 *bdaddr, U8 *link_key, U8 type);

U8 bt_gap_is_address_locked(void);

//BT HID API
/** 
 * @fn void bt_hid_ps3_mode();
 * @brief Set BT as PS3 mode
 * 
 * Call bt_hid_ps3_mode to tell act as PS3 mode.
 *	
 * @return No return value.
*/
void bt_hid_ps3_mode();

/** 
 * @fn U8 bt_hid_send_key_mouse_joystick_media(U8 type); //1: mouse, 2: keyboard, 3: media, 4:game, 5 ps3
 * @brief Send report over BT
 * 
 * Call bt_hid_send_key_mouse_joystick_media to send report over BT HID Channel. When type is 1~4, it will call
 * pad_data_get_report to get report data. When type is 5, it will call ps3_data_get_report to get report data.
 *	
 * @param [in] type Specify which type report we want to send. type 1 is mouse, type 2 is keyboard, type 3 is media, type 4 is game pad, and type 5 is PS3.
 * @return Return 0 if succeed, else system busy.
*/
U8 bt_hid_send_key_mouse_joystick_media(U8 type); //1: mouse, 2: keyboard, 3: media, 4:game, 5 ps3

#ifdef __cplusplus
};
#endif

#endif

/** 
  * @}
*/

