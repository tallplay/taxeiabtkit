#ifndef __BT_HID_HFP_API_HH__
#define __BT_HID_HFP_API_HH__
#include "bttype.h"

#ifdef __cplusplus
extern "C" {
#endif

//bt state
#define BT_STA_STOP 0
#define BT_STA_POWERUP 1
#define BT_STA_BOOT 2
#define BT_STA_INITILIZED 3
#define BT_STA_PAIRING 4
#define BT_STA_CONNECTING 5
#define BT_STA_CONNECTED 6       //outgoing link
#define BT_STA_HID_CONNECTED 7
#define BT_STA_HFP_CONNECTED 8
#define BT_STA_HID_HFP_CONNECTED 9
#define BT_STA_DISCONNECTING 10
#define BT_STA_DISCONNECTED 11
#define BT_STA_BT_FAIL 12
#define BT_STA_DISCONNECT_RETRY 13
#define BT_STA_LOCK 14
#define BT_STA_CONNECTED_INCOMING 41       //incoming link

/*testing states begin*/
#define BT_STA_ATE_IDLE 15
#define BT_STA_ATE_DISCONNECTTING 16
#define BT_STA_ATE_CONNECTTED 17
#define BT_STA_DUT_FINDING_ATE 18
#define BT_STA_DUT_FOUND_ATE 19
#define BT_STA_DUT_CONNECTING_ATE 20
#define BT_STA_DUT_CONNECTTED_ATE 21
#define BT_STA_ACTIVATON_REBOOT 29
#define BT_STA_FREQ_TEST 30
/*testing states end*/

#define BT_STA_SPP_CONNECTED 40

//bt wireless mode
#define BT_WMODE_NO_SCAN 0
#define BT_WMODE_PAGING_SCAN 2
#define BT_WMODE_INQUIRY_SCAN 3
#define BT_WMODE_PAGING 4
#define BT_WMODE_PAGING_INQUIRY_SCAN 5
#define BT_WMODE_ACTIVATION_ENABLED 6
#define BT_WMODE_ATE 0x80
#define BT_WMODE_FREQ_TEST 0x40

//BT CORE API
void bt_init(void);
U8 bt_task(U32 curTick);
U8 bt_start(U32 curTick, U8 mode, U8 profile_type);
void bt_stop(void);
U8 bt_state(void);

//BT GAP API
U8 bt_gap_is_paired_exist(U8 *addr);
U8 bt_gap_connect(U8 *addr);
U8 bt_gap_disconnect(void);
void bt_gap_set_local_name(U8 *str);
void bt_gap_preset_local_name(U8 *str);
void bt_gap_get_mac_addr(U8 *addr);
U8 bt_gap_get_scan_mode(void);
void bt_gap_set_scan_mode(U32, U8);
U8 *bt_gap_get_remote_name(void);
U8 *bt_gap_get_remote_addr(void);
U8 bt_gap_is_pincode_req(void);
U8 bt_gap_pincode_reply( U8 *pin, U8 len);
U8 bt_gap_is_sco_on(void);
void bt_gap_storage_link_key(U8 *bdaddr, U8 *link_key, U8 type);
U8 bt_gap_is_address_locked(void);
void bt_gap_writesupervision_timeout(U16 timeout);
void bt_gap_read_link_rssi(void (*cb)(U8 CmdOp, U8 EvtOp, U8 *dat));

//BT HID API
void bt_hid_ps3_mode();
U8 bt_hid_send_key_mouse_joystick_media(U8 type); //1: mouse, 2: keyboard, 3: media, 4:game
void bt_hid_set_hid_descriptor(U8 *desc);

//BT HFP API
void bt_hfp_get_phonebook(void(*pbitemcb)(U16 idx, U8* num, U8 *name));
void bt_hfp_get_staus(U8 *signal, U8 *battery, U8 *service, U8 *callsetup, U8 *call);
U8 bt_hfp_get_phonebook_status(U16 *total, U16 *idx);
U8 *bt_hfp_get_callingnumber(void);
void bt_hfp_disconnect(void);
void bt_hfp_connect(void);

//BT SPP API
void bt_spp_register_callback(void (*cb)(U16 len, U8 *dat));
void bt_spp_tx(U16 len, U8* dat);
U8 bt_spp_tx_ava(void);

//TESTING API
U8 bt_dut_find_ate(U8 test_eq);
U8 bt_dut_connect_ate(void);
U8 bt_ate_disconnect_dut(void);
void bt_dut_register_callback(void (*cb)(U16 len, U8 *dat));
void bt_dut_send_data(U16 len, U8 *dat);
void bt_ate_register_callback(void (*cb)(U16 len, U8 *dat));
void bt_ate_send_data(U16 len, U8 *dat);
void bt_set_ate(U8 test_mode); //from 0x01~0x32
void bt_set_freq_test(U8 freq, U8 type, U8 pattern, U16 packet_length);

//BT CHIP API
U8 bt_set_mcu_sleep_time(U32 ms);
U8 bt_reset_mcu_sleep_time();
void bt_reset_connect_retry();
void bt_set_sniff();
void bt_leave_sniff();


#ifdef __cplusplus
};
#endif

#endif
