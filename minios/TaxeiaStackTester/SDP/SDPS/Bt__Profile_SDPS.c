#include "BtConfig.h"
#include "BtType.h"
#include "Bt__Profile_SDPS.h"
#include "../../GAP/L2CAP/Bt__LLCAP.h"
#include "../../TaxeiaConsoleDebug/TaxeiaConsoledebug.h"
#include <string.h>

#if  (PROFILE_TYPE == PROFILE_TYPE_SPP  || PROFILE_TYPE == PROFILE_TYPE_HID_HFP || PROFILE_TYPE == PROFILE_TYPE_HFP || PROFILE_TYPE == PROFILE_TYPE_DYNAMIC || PROFILE_TYPE == PROFILE_TYPE_HID)
extern void Bt__ProfileHIDDIDGetInquiryData(U8 *num, SdpAttr **attr);
extern void Bt__ProfileHIDDGetInquiryData(U8 *num, SdpAttr **attr, U32 *CoD);
extern void Bt__ProfileHFPGetInquiryData(U8 *num, SdpAttr **attr, U32 *CoD);
extern void Bt__ProfileSPPGetInquiryData(U8 *num, SdpAttr **attr, U32 *CoD);
extern void L2ChannelSendData(U16 aclHandle, U16 cid, U8 len, U8 *dat);
extern U8 L2ChannelPreFillSendData(U16 idx, U8 dat);
extern U8 L2ChannelPreGetSendData(U16 idx);
extern void L2ChannelPreMoveSendData(U16 tgt, U16 src);
extern U16 L2ChannelSendDataCheckAclLen(U16 aclHandle, U16 cid, U16 len, U8 *dat);
extern U16 L2ChannelGetOutMtu(U16 cid);
extern void LLSignalConReq(U16 aclHandle, U16 psm);
extern void L2ChannelSearchTCidHdlByPsm(U16 psm, U16 *cid, U16 *acl);
extern void LLSignalDisConReq(U16 aclHandle, U8 id, U16 tcid, U16 scid);
extern U16 L2ChannelSearchAcidByTcid(U16 cid);

U16 SDPSAttributeRsp(U16 aclHandle, U16 cid, U8 *attrlist, U8 ridx, U8* bytecnt, U8 *cont);

U8 SDPValidateAttributeReq(U16 len, U8* dat, U8 **contbyte);
///////////////////////////////////Public API///////////////////////////////////////////

///////////////////////////////////Private Functions////////////////////////////////////
SdpRec sdp_recs[3];
U8 sdp_recs_num;
U16 SdpCid;
SDPSCallback sdpCB;

//op code
#define SDP_ERROR_RESPONSE              0x01
#define SDP_SERVICE_SEARCH_REQ          0x02
#define SDP_SERVICE_SEARCH_RSP          0x03
#define SDP_SERVICE_ATTRIB_REQ          0x04
#define SDP_SERVICE_ATTRIB_RSP          0x05
#define SDP_SERVICE_SEARCH_ATTRIB_REQ   0x06
#define SDP_SERVICE_SEARCH_ATTRIB_RSP   0x07

//error code
#define SDPE_BAD_VERSION      0x0001 /* Invalid/unsupported SDP version */
#define SDPE_BAD_HANDLE       0x0002 /* Invalid service record handle */
#define SDPE_BAD_SYNTAX       0x0003 /* Invalid request syntax */
#define SDPE_BAD_PDU_SIZE     0x0004 /* Invalid PDU size */
#define SDPE_BAD_CONTINUATION 0x0005 /* Invalid ContinuationState */
#define SDPE_OUT_OF_RESOURCES 0x0006 /* Insufficient resources to satisfy*/

void SDPSSendError(U16 aclHandle, U16 cid, U16 error, U8 *dat)
{
    dat[0] = SDP_ERROR_RESPONSE;
    dat[3] = 0x00;
    dat[4] = 0x03;
    
    dat[7] = 0x00; //cont
    
    Host2BE16(dat+5, error);
    L2ChannelSendData(aclHandle, SdpCid, 8, (U8*)dat);
}

void SDPSSendServiceSearchRsp(U16 aclHandle, U16 cid, U8 total, U8 rec_hdl, U8 *dat)
{
    //reuse buffer
    dat[0] = SDP_SERVICE_SEARCH_RSP;
    dat[3] = 0x00;
    if(total)
    {
        dat[4] = 0x09;        
        dat[5] = 0x00;
        dat[6] = total;
        dat[7] = 0x00;
        dat[8] = total;
        dat[9] = 0x00; //cont if total == 0;
        dat[10] = 0x01;
        dat[11] = 0x00;
        dat[12] = rec_hdl;
        dat[13] = 0x00; //con
    }
    else
    {
        dat[4] = 0x05;        
        dat[5] = 0x00;
        dat[6] = 0x00;
        dat[7] = 0x00;
        dat[8] = 0x00;        
        dat[9] = 0x00; //cont if total == 0;
    }
    
    L2ChannelSendData(aclHandle, SdpCid, dat[4]+5, (U8*)dat);
}

void SDPSSendServiceSearchAttributeRsp(U16 aclHandle, U16 cid, U8 *dat)
{
    //reuse buffer
    dat[0] = SDP_SERVICE_SEARCH_ATTRIB_RSP;
    dat[3] = 0x00;
    dat[4] = 6;
    dat[5] = 0x00;
    dat[6] = 0x03;
    dat[7] = SDTYPE_SEQ | SDSIZE_ADD_16BITS;
    dat[8] = 0; //cnt = 0;
    dat[9] = 0; //cnt = 0;
    dat[10] = 0;
    L2ChannelSendData(aclHandle, SdpCid, 11, (U8*)dat);

}


void SDPCSendSearviceSearchAttributeReq(U16 aclHandle, U16 cid, U16 UUID, U16 attribute)
{
    U8 dat[18];
    
    dat[0] = SDP_SERVICE_SEARCH_ATTRIB_REQ;
    dat[1] = 0x00;
    dat[2] = 0x00;
    dat[3] = 0x00;
    dat[4] = 0x0d;
    //Service Search Pattern
    dat[5] = SDTYPE_SEQ | SDSIZE_ADD_8BITS;
    dat[6] = 0x03;
    dat[7] = SDTYPE_UUID | SDSIZE_2BYTES;
    dat[8] = UUID >> 8;
    dat[9] = UUID & 0xff;
    //max bytecnt
    dat[10] = 0x00;
    dat[11] = 0xff;
    //attribute list
    dat[12] = SDTYPE_SEQ | SDSIZE_ADD_8BITS;
    dat[13] = 0x03;
    dat[14] = SDTYPE_UINT | SDSIZE_2BYTES;
    dat[15] = attribute >> 8;
    dat[16] = attribute & 0xff;
    //continue state
    dat[17] = 0x00; 
    
    L2ChannelSendData(aclHandle, SdpCid, 18, (U8*)dat);
}



////////////////////////////////////////////////////////////////////////////
/*sdp tools*/
U16 SdpParseData(const U8* p, U16* offset)
{
    U16 len = 0;

    if ((p[0] & SDTYPE_MASK) == SDTYPE_NIL) {
        *offset = 0;
        return 1;
    }

    switch (p[0] & SDSIZE_MASK) {
    case SDSIZE_1BYTE:
        *offset = 1;
        len = 1;
        break;

    case SDSIZE_2BYTES:
        *offset = 1;
        len = 2;
        break;

    case SDSIZE_4BYTES:
        *offset = 1;
        len = 4;
        break;

    case SDSIZE_8BYTES:
        *offset = 1;
        len = 8;
        break;

    case SDSIZE_16BYTES:
        *offset = 1;
        len = 16;
        break;

    case SDSIZE_ADD_8BITS:
        *offset = 2;
        len = (U16) p[1];
        break;

    case SDSIZE_ADD_16BITS:
        *offset = 3;
        len = (U16) BE2Host16((const U8*)(p+1));
        break;

    case SDSIZE_ADD_32BITS:
        *offset = 5;
        len = (U16)BE2Host32((const U8*)(p+1));
        break;

    default:
        /* This case shouldn't be possible.  Complain if it somehow occurs */
        ASSERT(0);
        break;
    }
    return len;
}
////////////////////////////////////////////////////////////////////////////
/*Service Search*/

static U8 ValidateServiceSearchPattern(const U8* searchPat, U16 maxLen)
{
    U16   plen;
    U16   len;
    U16   pOffset;

    /* Verify that the pattern is of type Data Element Sequence */
    if ((searchPat[0] & SDTYPE_MASK) != SDTYPE_SEQ) {
        goto err;
    }

    /* Get the size of the sequence */
    plen = SdpParseData(searchPat, &pOffset);

    /* Verify that each element in the sequence is a UUID. Keep
     * a running total of the length according to the UUIDs
     */
    len = 0;
    while (len < plen) {
        /* First check the type */
        if ((searchPat[pOffset+len] & SDTYPE_MASK) != SDTYPE_UUID) {
            goto err;
        }

        /* Determine if the size if valid. Only 2 byte, 4 byte or 16 byte
         * sizes are allowed for UUIDs
         */
        switch (searchPat[pOffset+len] & SDSIZE_MASK) {
        case SDSIZE_2BYTES:
            len += 3;
            break;

        case SDSIZE_4BYTES:
            len += 5; 
            break;

        case SDSIZE_16BYTES:
            len += 17;
            break;

        default:
            goto err;
        }

        /* Make sure we don't read past the end of the buffer */
        if (len > maxLen) {
            goto err;
        }
    }
    /* At this point the length calulated by the attributes should match
     * the length specified by the Data Element Sequence
     */
    if (len != plen) {
        goto err;
    }

    return 0;
err:
    return 1;
}

U16 SDPSValidateServiceSearch(U16 len , U8 *dat)
{
    U16 pdu_len;
    U16 plen;
    U16 pOffset;
    
    pdu_len = BE2Host16(dat+3);
    
    //validate length
    if(len != pdu_len + 5)
        return SDPE_BAD_PDU_SIZE;

    dat += 5; //move to data
    //validate pattern
    if(ValidateServiceSearchPattern(dat, len - 3)) //MaximumServiceRecordCount 2 bytes, Continuation State 1 byte
        return SDPE_BAD_SYNTAX;

    plen = SdpParseData(dat, &pOffset);    //we assume it was data element, because it pass the ValidateServiceSearchPattern
    plen += pOffset;
    //validate MaximumServiceRecordCount != 0
    if(BE2Host16(dat+plen) == 0)
        return SDPE_BAD_SYNTAX;

    //makesure continueLen+1+params == pdu_len == len -5
    plen += 2; //move to continue byte
    if(dat[plen] + plen + 1 != pdu_len)
        return SDPE_BAD_PDU_SIZE;

    return 0; //success
}

void SDPExtendUUID2U32(const U8 *dat, U8 len, U8 *buf)
{
    if(len == 2)
    {
        buf[0] = buf[1] = 0;
        buf[2] = dat[0];
        buf[3] = dat[1];
    }
    else if(len == 4)
    {
        memcpy(buf, dat, 4);
    }
}

U8 SDPSUUIDCompare(const U8 * uuid1, U8 *uuid2)
{
    U8 len1, len2;
    U16 offset1, offset2;
    const U8 *dat1, *dat2;
    U8 tmp[8];

    len1 = (U8)SdpParseData(uuid1, &offset1);
    len2 = (U8)SdpParseData(uuid2, &offset2);
    
    ASSERT(len1 == 2 || len1 == 16 || len1 == 4);
    ASSERT(len2 == 2 || len2 == 16 || len2 == 4);
    dat1 = uuid1 + offset1;
    dat2 = uuid2 + offset2;
    if(len1 == len2)
        return memcmp(dat1, dat2, len1);

    if(len1 < 16 && len2 < 16)
    {
        SDPExtendUUID2U32(dat1, len1, tmp);
        SDPExtendUUID2U32(dat2, len2, tmp+4);
        return memcmp(tmp, tmp+4, 4);
    }
    else if((len1 == 2) && (len2 == 16)) //len2 is 128bits uuid
    {
        SDPExtendUUID2U32(dat1, len1, tmp);
        SDPExtendUUID2U32(dat2, 4, tmp+4);
        return memcmp(tmp, tmp+4, 4);
    }

//    ASSERT(0); //we didn't want to see 128 UUID
    return 1;
}

U8 SDPSSerachAttribByUUID(const U8 *attr, U8 *uuid)
{
    U16 len;
    U16 offset;
    const U8 *end;

    switch(attr[0] & SDTYPE_MASK)
    {
        case SDTYPE_UUID:
            //compare
            if(!(SDPSUUIDCompare(attr, uuid)))
                return 0;
            break;
        case SDTYPE_SEQ:
        case SDTYPE_ALT:
            len = SdpParseData(attr, &offset);
            attr += offset;
            end = attr + len;
            while(attr < end)
            {
                if(!SDPSSerachAttribByUUID(attr, uuid))
                    return 0;
                
                attr += SdpParseData(attr, &offset);
                attr += offset;
            }
            break;
    }

    return 1;
}

U8 SDPSFindUUIDInRecord(U8 idx, U8 *uuid)
{
    U8 i;

    for(i = 0; i < sdp_recs[idx].num_attribs; i++)
    {
        if(!(SDPSSerachAttribByUUID(sdp_recs[idx].attribs[i].val, uuid)))
            return 0;
    }
    
    return 1;
}

U8 SDPSSearchRecordByServicePattern(U8 idx, U8 *dat)
{
    U16 pOffset;
    U16 len;
    U8 *UUID;
    U8 *End;

    //we assume the first data is element sequence 0x3x 0xXX
    len = SdpParseData(dat, &pOffset);
    UUID = dat + pOffset;
    End = UUID + len;
    
    while(UUID < End)
    {
        //search
        if(SDPSFindUUIDInRecord(idx, UUID))
        {
            break;
        }
        
        len = SdpParseData(UUID, &pOffset);
        UUID = UUID + pOffset + len;
    }
    
    //make sure all UUID in pattern were matched
    return !(UUID == End);

}

U8 SDPSSearchRecordsByServicePattern(U8 *dat)
{
    U8 i;
    //find all records
    for(i = 0; i < sdp_recs_num; i++)
    {
        if(!SDPSSearchRecordByServicePattern(i, dat+5))//success
        {           
            return i;
        }
    }
    
    return 0xff;
}

void SDPSHadleServiceSearch(U16 aclHandle, U16 cid, U16 len , U8 *dat)
{
    U16 cand_err;
    U8 i;

    //check error
    cand_err = SDPSValidateServiceSearch(len, dat);
    
    if(!cand_err)
    {       
        i = SDPSSearchRecordsByServicePattern(dat);
        if(i != 0xff)
        {
            SDPSSendServiceSearchRsp(aclHandle, cid, 1, i, dat);
            return;
        }
        
        SDPSSendServiceSearchRsp(aclHandle, cid, 0, 0, dat);
        return;
    }
    
    SDPSSendError(aclHandle, cid, cand_err, dat);
}

U8 SDPValidateServiceSearchAttributeReq(U16 len, U8* dat, U8 **contbyte)
{
    U16 pdu_len;
    U16 plen;
    U16 pOffset;
    U8 *ptr;

    ptr = dat;
    
    pdu_len = BE2Host16(dat+3);
    
    //validate length
    if(len != pdu_len + 5)
        return SDPE_BAD_PDU_SIZE;

    ptr += 5; //move to service search pattern
    
    plen = SdpParseData(ptr, &pOffset);

    ptr += (plen + pOffset); //move to maximum attribute byte count
    if(BE2Host16(ptr) < 7)
        return SDPE_BAD_SYNTAX;

    ptr += 2; //move to attribute list

    plen = SdpParseData(ptr, &pOffset);    //we assume it was data element, because it pass the ValidateServiceSearchPattern
    plen += pOffset;
    ptr += plen; //move to cont byte
    
    if(ptr + 1 + *ptr != pdu_len + 5 + dat)
        return SDPE_BAD_PDU_SIZE;

    *contbyte = ptr;
    return 0; //success

}

void SDPSHadleServiceSearchAttribute(U16 aclHandle, U16 cid, U16 len , U8 *dat)
{
    U8 i;
    U16 cand_err;
    U8 *cont;
    U16 cnt;
    U16 s;
    U16 header_len;
    U16 cont_len;
    
    cand_err = SDPValidateServiceSearchAttributeReq(len, dat, &cont);
    
    if(!cand_err)
    {
        
        i = SDPSSearchRecordsByServicePattern(dat); //only search first service!!!, reduce complications
        if(i != 0xff)
        {
            U16 pOffset;
            U16 pattern_len;
            U8 *End;

            pattern_len = SdpParseData(dat+5, &pOffset);
            End = dat + 5 + pOffset + pattern_len;


            //tallplay try to fix 2013/9/26 begin

            Host2BE16(End, min((BE2Host16(End)- 3) , (L2ChannelGetOutMtu(cid) - 10/*3 cont, 7 sdp header*/ - 3)));

//            Host2BE16(End, (BE2Host16(End) - 3)); //max byte cont減去attribute lists header size(3),
//            //因為我們需要reuse SDPAttributeRsp, 但是回應時需要加上attribute lists header

            //tallplay try to fix 2013/9/26 end
            
            cnt = SDPSAttributeRsp(aclHandle, cid, End+2, i/*rec id*/, End, cont);
            

            header_len = 0;
            cont_len = 1;

            if(BE2Host16(End) + 3 == cnt) //must have continue bytes
                cont_len = 3;

            
            if(*cont == 0)//must have attribute list header, so move data forward 3 bytes
            {
               U16 attrs_len;
    //5=>8
    //n=>n+3
                for(s = cnt+2; s > 0; s--)
                {
                    L2ChannelPreMoveSendData(s+7, s+4);
                }
                
                header_len = 3;
                
                L2ChannelPreFillSendData(7, SDTYPE_SEQ | SDSIZE_ADD_16BITS);

                //reuse attr len to be attrs len
                attrs_len = ((L2ChannelPreGetSendData(11) << 8) + L2ChannelPreGetSendData(12)) + 3;

                L2ChannelPreFillSendData(8, (attrs_len) >> 8);
                L2ChannelPreFillSendData(9, (attrs_len) & 0xff);        
            }
            

            L2ChannelPreFillSendData(0, SDP_SERVICE_SEARCH_ATTRIB_RSP);        
            L2ChannelPreFillSendData(1, dat[1]);
            L2ChannelPreFillSendData(2, dat[2]);
            L2ChannelPreFillSendData(3, (cnt+2+header_len) >> 8);
            L2ChannelPreFillSendData(4, (cnt+2+header_len) & 0xff);        
            L2ChannelPreFillSendData(5, (cnt+ header_len -cont_len) >> 8);
            L2ChannelPreFillSendData(6, (cnt+ header_len -cont_len) & 0xff);

            
            L2ChannelSendDataCheckAclLen(aclHandle, SdpCid, cnt + 7 + header_len, 0);
            
            return;
        }

        SDPSSendServiceSearchAttributeRsp(aclHandle, cid, dat);
        return;
    }
    
    SDPSSendError(aclHandle, cid, cand_err, dat);
}

void SDPMarkAttribute(U8 idx, U16 idstart, U16 idend)
{
    U8 i;
    
    for(i = 0; i < sdp_recs[idx].num_attribs; i++)
    {
        if(sdp_recs[idx].attribs[i].attrid >= idstart && sdp_recs[idx].attribs[i].attrid <= idend)
           sdp_recs[idx].attribs[i].marked = 1;
    }
}

void SDPMarkAttributes(U8 idx, U8 *dat)
{
    U16 tmp;
    U16 slen;
    U16 i;

    //clear marks
    for(i = 0; i < sdp_recs[idx].num_attribs; i++)
        sdp_recs[idx].attribs[i].marked = 0;
        
    //dat+= (5 + 4 + 2); //move to AttributeIDList
    
    slen = SdpParseData(dat, &tmp);
    dat += tmp;
    i = 0;
    while(i < slen)
    {
        ASSERT((dat[i] & SDTYPE_MASK) == SDTYPE_UINT);
        
        switch(dat[i] & SDSIZE_MASK)
        {
            case SDSIZE_2BYTES:
                SDPMarkAttribute(idx, BE2Host16(dat + i + 1), BE2Host16(dat + i + 1));
                i += 3;
                break;
            case SDSIZE_4BYTES:
                SDPMarkAttribute(idx, BE2Host16(dat + i + 1), BE2Host16(dat + i + 3));
                i+=5;
                break;
            default:
                ASSERT(0);
                break;
        }
    }
}

void SDPPreFillAttributeList(U16 *idx, U8 dat, U16 start, U16 max)
{
    if(*idx < start)
    {
        (*idx)++;
        return;
    }
    
    if(*idx - start < max)
    {    
        L2ChannelPreFillSendData(*idx-start+7, dat);
        (*idx)++;
    }
}

U16 SDPSAttributeRsp(U16 aclHandle, U16 cid, U8 *attrlist, U8 ridx, U8* bytecnt, U8 *cont)
{
    U16 maxcnt;
    U16 idx, totalcnt;
    U8 recidx;
    U8 attridx;
    U16 start;
    U16 i;

    recidx = ridx;
    
    SDPMarkAttributes(recidx, attrlist);

    //sdp attribute byte count
    maxcnt = min(BE2Host16(bytecnt) , (L2ChannelGetOutMtu(cid) - 10/*3 cont, 7 sdp header*/));
    
    //calculate all size
    totalcnt = 0;
    
    if(*cont == 0)
    {
        idx = 3;      //from byte 3, because 0, 1, 2 was for attributelist data element sequence header
        start = 0; //from attributes offset byte 0
    }
    else
    {
        idx = 0; //from byte 0
        start = BE2Host16(cont+1); //from attributes offset byte BE2Host16(cont+1)
    }

    attridx = 0;
    
    for(;;)
    {
        if(sdp_recs[recidx].attribs[attridx].marked)
        {
            //put data
            /*attrib id*/
            SDPPreFillAttributeList(&idx, SDTYPE_UINT | SDSIZE_2BYTES, start, maxcnt);
            SDPPreFillAttributeList(&idx, sdp_recs[recidx].attribs[attridx].attrid >> 8, start, maxcnt);
            SDPPreFillAttributeList(&idx, sdp_recs[recidx].attribs[attridx].attrid & 0xff, start, maxcnt);
            /*attrb data*/
            for(i =0; i < sdp_recs[recidx].attribs[attridx].len; i++)
            {
                if(sdp_recs[recidx].attribs[attridx].attrid == ATTRID_SERVICE_RECORD_HANDLE && i == 1)
                {
                    SDPPreFillAttributeList(&idx, recidx, start, maxcnt);
                }
                else
                    SDPPreFillAttributeList(&idx, sdp_recs[recidx].attribs[attridx].val[i], start, maxcnt);
            }

            //calculate size
            totalcnt += (3 + sdp_recs[recidx].attribs[attridx].len);                    
        }
        
        attridx++;

       if(attridx == sdp_recs[recidx].num_attribs)
       {
            if(start == 0)
            {
                //data element sequence
                L2ChannelPreFillSendData(7, SDTYPE_SEQ | SDSIZE_ADD_16BITS);
                L2ChannelPreFillSendData(8, totalcnt >> 8);
                L2ChannelPreFillSendData(9, totalcnt & 0xff);
            }
            
            L2ChannelPreFillSendData(5, (idx-start) >> 8);
            L2ChannelPreFillSendData(6, (idx-start) & 0xff);
            
            if(idx < totalcnt)
            {
                L2ChannelPreFillSendData(idx-start+7, 0x02);//cont byte
                if(start == 0)
                {
                    L2ChannelPreFillSendData(idx-start+8, (idx-3) >> 8);
                    L2ChannelPreFillSendData(idx-start+9, (idx-3) & 0xff);
                }
                else
                {
                    L2ChannelPreFillSendData(idx-start+8, (idx) >> 8);
                    L2ChannelPreFillSendData(idx-start+9, (idx) & 0xff);
                }
                idx += 3; //3 bytes more
            }
            else
            {
                L2ChannelPreFillSendData(idx-start+7, 0x00); //cont byte
                idx++; //1 bytes more
            }
            break;
        }            

    }
            
    return idx-start;
}

void SDPSHandleAttributeReq(U16 aclHandle, U16 cid, U16 len , U8 *dat)
{
    U16 err;
    U8 *cont;    
    U16 cnt;
    
    //check error
    err = SDPValidateAttributeReq(len, dat, &cont);

    if(!err)
    {
        cnt = SDPSAttributeRsp(aclHandle, cid, dat+11, dat[8]/*rec id*/, dat+9, cont);
        
        L2ChannelPreFillSendData(0, SDP_SERVICE_ATTRIB_RSP);        
        L2ChannelPreFillSendData(1, dat[1]);
        L2ChannelPreFillSendData(2, dat[2]);
        L2ChannelPreFillSendData(3, (cnt+2) >> 8);
        L2ChannelPreFillSendData(4, (cnt+2) & 0xff);        
        L2ChannelSendDataCheckAclLen(aclHandle, SdpCid, cnt + 7, 0);
            
        return;
    }
    
    SDPSSendError(aclHandle, cid, err, dat);
}

U8 SDPValidateAttributeReq(U16 len, U8* dat, U8 **contbyte)
{
    U16 pdu_len;
    U16 plen;
    U16 pOffset;
    U32 record_hdl;
    
    pdu_len = BE2Host16(dat+3);
    
    //validate length
    if(len != pdu_len + 5)
        return SDPE_BAD_PDU_SIZE;

    dat += 5; //move to service record handle
    record_hdl = BE2Host32(dat);
    if((record_hdl & 0xffffff00) != 0x00010000 || ((record_hdl & 0xff) >= sdp_recs_num))
        return SDPE_BAD_HANDLE;

    dat += 4; //move to maximum attribute byte count
    if(BE2Host16(dat) < 7)
        return SDPE_BAD_SYNTAX;

    dat += 2; //move to attribute list

    plen = SdpParseData(dat, &pOffset);    //we assume it was data element, because it pass the ValidateServiceSearchPattern
    plen += pOffset;
    //makesure continueLen+1+params == pdu_len == len -5
    if(dat[plen] + plen + 7 != pdu_len)
        return SDPE_BAD_PDU_SIZE;

    *contbyte = dat + plen;
    return 0; //success

}

////////////////////////////////////////////////////////////////////////////


U8 SDPSPsmHandler(U16 aclHandle, U16 cid, U16 len , U8 *dat)
{
    U16 tmpHdl;

    if(aclHandle == 0)
    {
        if(cid == 0)
        {
            L2ChannelSearchTCidHdlByPsm(0x0001, &SdpCid, &tmpHdl);

            if(sdpCB)
            {
                if(SdpCid == 0) //disconnect
                    sdpCB = (SDPSCallback)sdpCB(0, tmpHdl, SdpCid, 0, 0);
                else if(SdpCid) //connect
                    sdpCB = (SDPSCallback)sdpCB(1, tmpHdl, SdpCid, 0, 0);

            }
        }
        return 1;
    }
    
    if (len < 13 && dat[0] != SDP_SERVICE_SEARCH_ATTRIB_RSP) {
        SDPSSendError(aclHandle, cid, SDPE_BAD_PDU_SIZE, dat);
        return 0;
    }

    switch(dat[0])
    {
        case SDP_SERVICE_SEARCH_REQ:
        {

            SDPSHadleServiceSearch(aclHandle, cid, len, dat);            
            return 0;
        }
        case SDP_SERVICE_ATTRIB_REQ:
        {
            SDPSHandleAttributeReq(aclHandle, cid, len, dat);
            return 0;
        }
        case SDP_SERVICE_SEARCH_ATTRIB_REQ:
        {
            SDPSHadleServiceSearchAttribute(aclHandle, cid, len, dat);
            return 0;
        }
        case SDP_ERROR_RESPONSE:
        case SDP_SERVICE_SEARCH_ATTRIB_RSP:
        {
            U16 pOffset;
            U16 pattern_len;

            pattern_len = SdpParseData(dat+7, &pOffset);

            sdpCB = (SDPSCallback)sdpCB(2, aclHandle, cid, pOffset + pattern_len, dat+7);
            return 1; //disconnect launch
        }
        break;        
        default:
            return 1; //fail
    }
    return 1; 
}

void Bt__ProfileSDPCConnect(U16 aclHandle, SDPSCallback connectCB)
{
    LLSignalConReq(aclHandle, 0x0001);
    sdpCB = connectCB;
}

U32 Bt__ProfileSDPSInit(U8 type)
{
    U32 cod = 0;
    sdp_recs_num = 0;
    sdpCB = 0;

#if (PROFILE_TYPE == PROFILE_TYPE_HID)
        Bt__ProfileHIDDGetInquiryData(&sdp_recs[sdp_recs_num].num_attribs, &sdp_recs[sdp_recs_num].attribs, &sdp_recs[sdp_recs_num].CoD);
        sdp_recs[sdp_recs_num].Handle = 0x00010000 + sdp_recs_num;
        cod = sdp_recs[sdp_recs_num].CoD;
        sdp_recs_num++;    
        Bt__ProfileHIDDIDGetInquiryData(&sdp_recs[sdp_recs_num].num_attribs, &sdp_recs[sdp_recs_num].attribs);
        sdp_recs[sdp_recs_num].Handle = 0x00010000 + sdp_recs_num;
        sdp_recs_num++;    
#elif (PROFILE_TYPE == PROFILE_TYPE_HFP)
        Bt__ProfileHFPGetInquiryData(&sdp_recs[sdp_recs_num].num_attribs, &sdp_recs[sdp_recs_num].attribs, &sdp_recs[sdp_recs_num].CoD);
        cod = sdp_recs[sdp_recs_num].CoD;
        sdp_recs[sdp_recs_num].Handle = 0x00010000 + sdp_recs_num;
        sdp_recs_num++;
#elif (PROFILE_TYPE == PROFILE_TYPE_HID_HFP)
        Bt__ProfileHIDDGetInquiryData(&sdp_recs[sdp_recs_num].num_attribs, &sdp_recs[sdp_recs_num].attribs, &sdp_recs[sdp_recs_num].CoD);
        sdp_recs[sdp_recs_num].Handle = 0x00010000 + sdp_recs_num;
        cod = sdp_recs[sdp_recs_num].CoD;
        sdp_recs_num++;    
        Bt__ProfileHIDDIDGetInquiryData(&sdp_recs[sdp_recs_num].num_attribs, &sdp_recs[sdp_recs_num].attribs);
        sdp_recs[sdp_recs_num].Handle = 0x00010000 + sdp_recs_num;
        sdp_recs_num++;    
        Bt__ProfileHFPGetInquiryData(&sdp_recs[sdp_recs_num].num_attribs, &sdp_recs[sdp_recs_num].attribs, &sdp_recs[sdp_recs_num].CoD);
        cod = sdp_recs[sdp_recs_num].CoD;
        sdp_recs[sdp_recs_num].Handle = 0x00010000 + sdp_recs_num;
        sdp_recs_num++;    
#elif (PROFILE_TYPE == PROFILE_TYPE_SPP)        
        Bt__ProfileSPPGetInquiryData(&sdp_recs[sdp_recs_num].num_attribs, &sdp_recs[sdp_recs_num].attribs, &sdp_recs[sdp_recs_num].CoD);
        cod = sdp_recs[sdp_recs_num].CoD;
        sdp_recs[sdp_recs_num].Handle = 0x00010000 + sdp_recs_num;
        sdp_recs_num++;   

#elif (PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
    switch(type)
    {
        case PROFILE_TYPE_HID_HFP:
        case PROFILE_TYPE_HID:
            Bt__ProfileHIDDGetInquiryData(&sdp_recs[sdp_recs_num].num_attribs, &sdp_recs[sdp_recs_num].attribs, &sdp_recs[sdp_recs_num].CoD);
            sdp_recs[sdp_recs_num].Handle = 0x00010000 + sdp_recs_num;
            cod = sdp_recs[sdp_recs_num].CoD;
            sdp_recs_num++;    
            Bt__ProfileHIDDIDGetInquiryData(&sdp_recs[sdp_recs_num].num_attribs, &sdp_recs[sdp_recs_num].attribs);
            sdp_recs[sdp_recs_num].Handle = 0x00010000 + sdp_recs_num;
            sdp_recs_num++;    
            if(type == PROFILE_TYPE_HID_HFP)//reduce code size
                goto profile_type_hfp_init;
            break;
        case PROFILE_TYPE_SPP:
            Bt__ProfileSPPGetInquiryData(&sdp_recs[sdp_recs_num].num_attribs, &sdp_recs[sdp_recs_num].attribs, &sdp_recs[sdp_recs_num].CoD);
            cod = sdp_recs[sdp_recs_num].CoD;
            sdp_recs[sdp_recs_num].Handle = 0x00010000 + sdp_recs_num;
            sdp_recs_num++;    
            break;
        case PROFILE_TYPE_HFP:
profile_type_hfp_init:            
            Bt__ProfileHFPGetInquiryData(&sdp_recs[sdp_recs_num].num_attribs, &sdp_recs[sdp_recs_num].attribs, &sdp_recs[sdp_recs_num].CoD);
            cod = sdp_recs[sdp_recs_num].CoD;
            sdp_recs[sdp_recs_num].Handle = 0x00010000 + sdp_recs_num;
            sdp_recs_num++;    
            break;
    }

#endif

    ASSERT(RegisterPsmHandler(0x0001, SDPSPsmHandler));
    return cod;
}

void Bt__ProfileSDPSDeInit()
{
    sdp_recs_num = 0;
    DeregisterPsmHandler(0x0001); //do not check
}
#endif

