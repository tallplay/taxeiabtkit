#ifndef __BT__PROFILE_SDPS_HH_
#define __BT__PROFILE_SDPS_HH_
#include "../../BTType.h"
#include "../../GAP/LOCAL_DEV/Bt__LocalDevice.h"
///////////////////////////////////Public API///////////////////////////////////////////
typedef void *(*SDPSCallback) (U8 op, U16 aclHandle, U16 cid, U16 len, U8 *dat);

//atrribute
typedef struct
{
    U16              attrid;
    U16              len;    
    const U8        *val;
    U8               marked;
} SdpAttr;

typedef struct
{        
    SdpAttr *attribs;
    U32 CoD;
    U32 Handle;
    U8 num_attribs;
} SdpRec;

#define ATTRID_SERVICE_RECORD_HANDLE               0x0000
#define ATTRID_SERVICE_CLASS_ID_LIST               0x0001
#define ATTRID_SERVICE_RECORD_STATE                0x0002
#define ATTRID_SERVICE_ID                          0x0003
#define ATTRID_PROTOCOL_DESC_LIST                  0x0004
#define ATTRID_BROWSE_GROUP_LIST                   0x0005
#define ATTRID_LANG_BASE_ID_LIST                   0x0006
#define ATTRID_SERVICE_INFO_TIME_TO_LIVE           0x0007
#define ATTRID_SERVICE_AVAILABILITY                0x0008
#define ATTRID_BT_PROFILE_DESC_LIST                0x0009
#define ATTRID_DOC_URL                             0x000a
#define ATTRID_CLIENT_EXEC_URL                     0x000b
#define ATTRID_ICON_URL                            0x000c
#define ATTRID_ADDITIONAL_PROT_DESC_LISTS          0x000d
#define ATTRID_SERVICE_NAME                        0x0000
#define ATTRID_SERVICE_DESCRIPTION                 0x0001
#define ATTRID_PROVIDER_NAME                       0x0002

void Bt__ProfileSDPCConnect(U16 aclHandle, SDPSCallback connectCB);
void SDPCSendSearviceSearchAttributeReq(U16 aclHandle, U16 cid, U16 UUID, U16 attribute);

///////////////////////////////////Private Functions////////////////////////////////////
U32 Bt__ProfileSDPSInit(U8 type);
void Bt__ProfileSDPSDeInit(void);

#endif
