#if !defined(AFX_DLGTITLE_H__57E64AB5_BBA0_437A_8C7A_59D7AD4151E6__INCLUDED_)
#define AFX_DLGTITLE_H__57E64AB5_BBA0_437A_8C7A_59D7AD4151E6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgTitle.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDlgTitle dialog

class CDlgTitle : public CDialog
{
// Construction
public:
	CDlgTitle(CWnd* pParent = NULL);   // standard constructor

	void	vSetTitle(WCHAR *pTitle);

// Dialog Data
	//{{AFX_DATA(CDlgTitle)
	enum { IDD = IDD_DIALOG_TITLE };
	CStatic	m_control_hint;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgTitle)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDlgTitle)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGTITLE_H__57E64AB5_BBA0_437A_8C7A_59D7AD4151E6__INCLUDED_)
