// IVTKeyboardDlg.cpp : implementation file
//

#include "stdafx.h"
#include "IVTKeyboard.h"
#include "IVTKeyboardDlg.h"
#include "DlgAbout.h"
#include "DlgPairing.h"
#include "DlgSetPin.h"

extern "C" {
#include <hidsdi.h>
}

#include <dbt.h>

#define REPLACE_RIGHT_NUMBER
#define REPLACE_RIGHT_ENTER

#define IDT_CHECK_CONNECTION    1
#define IDT_CHECK_MOUSE                 2
#define IDT_CLOSE_TITLE                 3

#define IDM_ABOUT           60000

#define IDM_LAYOUT_ENG      60001
#define IDM_LAYOUT_JIS      60002

#define IDM_STYLE_IPAD      60003
#define IDM_STYLE_MAC       60004
#define IDM_STYLE_WINDOWS   60005
#define IDM_STYLE_ANDROID   60006

#define IDM_BT_DISCOVERABLE 60007
#define IDM_BT_DISCONNECT   60008
#define IDM_BT_DELETE_ALL   60009

#define IDM_BT_CONNECT      60010
#define IDM_BT_DELETE       60030

#define IDM_LEAVE       60050

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char     THIS_FILE[]     = __FILE__;
#endif

BOOL            g_StopMouse;
HHOOK           g_hMouseHook;
HHOOK           g_hKeyboardHook;
BOOL            bIsForeground;  

LRESULT CALLBACK
LowLevelMouseProc(int nCode, WPARAM wParam, LPARAM lParam) {
    if (nCode >= 0) {
        PMSLLHOOKSTRUCT pmll    = (PMSLLHOOKSTRUCT) lParam;
		
        if (g_StopMouse) {
            return -1;
        }
    }
	
    return CallNextHookEx(g_hMouseHook, nCode, wParam, lParam);
}


LRESULT CALLBACK
LowLevelKeyboardProc(int nCode, WPARAM wParam, LPARAM lParam) {
    if (nCode >= 0) {
        PKBDLLHOOKSTRUCT        pkll    = (PKBDLLHOOKSTRUCT) lParam;    
		
        WCHAR                   buf[MAX_PATH];
        wsprintf(buf, TEXT("vkCode=%02X, scanCode=%02X, flags=%02X\n"),
			pkll->vkCode, pkll->scanCode, pkll->flags);
		
        //OutputDebugString(buf);
		
        if (bIsForeground) {
            //OutputDebugString(TEXT("return -1\n"));
            return -1;
        } else {
            //OutputDebugString(TEXT("Not Foregrounbd\n"));
        }
    }
    return CallNextHookEx(g_hKeyboardHook, nCode, wParam, lParam);
}

/////////////////////////////////////////////////////////////////////////////
// CIVTKeyboardDlg dialog

CIVTKeyboardDlg::CIVTKeyboardDlg(CWnd *pParent /*=NULL*/) :
CDialog(CIVTKeyboardDlg::IDD, pParent)
{
    //{{AFX_DATA_INIT(CIVTKeyboardDlg)
    // NOTE: the ClassWizard will add member initialization here
    //}}AFX_DATA_INIT
    // Note that LoadIcon does not require a subsequent DestroyIcon in Win32
    m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	
    m_pLightGrayBkBrush = new CBrush(LIGHT_GRAY);
}

void
CIVTKeyboardDlg::DoDataExchange(CDataExchange *pDX) {
    CDialog::DoDataExchange(pDX);
    //{{AFX_DATA_MAP(CIVTKeyboardDlg)
    //}}AFX_DATA_MAP
	
}

BEGIN_MESSAGE_MAP(CIVTKeyboardDlg, CDialog)
//{{AFX_MSG_MAP(CIVTKeyboardDlg)
ON_WM_CTLCOLOR()
ON_WM_TIMER()
ON_WM_PAINT()
ON_WM_DRAWITEM()
ON_MESSAGE( WM_POWERBROADCAST, OnPowerBroadcast )
ON_MESSAGE(WM_TRAY_NOTIFY, OnTrayNotify)
ON_COMMAND(IDM_ABOUT, OnTrayAbout)
ON_COMMAND(IDM_LAYOUT_ENG, OnTrayLayout)
ON_COMMAND(IDM_LAYOUT_JIS, OnTrayLayout)
ON_COMMAND(IDM_STYLE_WINDOWS, OnTrayStyle)
ON_COMMAND(IDM_STYLE_MAC, OnTrayStyle)
ON_COMMAND(IDM_STYLE_IPAD, OnTrayStyle)
ON_COMMAND(IDM_STYLE_ANDROID, OnTrayStyle)
ON_COMMAND(IDM_BT_DISCOVERABLE, OnTrayBTDiscoverable)
ON_COMMAND(IDM_BT_DISCONNECT, OnTrayBTDisconnect)
ON_COMMAND(IDM_BT_DELETE_ALL, OnTrayBTDeleteAll)
ON_COMMAND(IDM_BT_CONNECT, OnTrayBTConnect)
ON_COMMAND(IDM_BT_CONNECT+1, OnTrayBTConnect)
ON_COMMAND(IDM_BT_CONNECT+2, OnTrayBTConnect)
ON_COMMAND(IDM_BT_CONNECT+3, OnTrayBTConnect)
ON_COMMAND(IDM_BT_CONNECT+4, OnTrayBTConnect)
ON_COMMAND(IDM_BT_CONNECT+5, OnTrayBTConnect)
ON_COMMAND(IDM_BT_CONNECT+6, OnTrayBTConnect)
ON_COMMAND(IDM_BT_CONNECT+7, OnTrayBTConnect)
ON_COMMAND(IDM_BT_CONNECT+8, OnTrayBTConnect)
ON_COMMAND(IDM_BT_CONNECT+9, OnTrayBTConnect)
ON_COMMAND(IDM_BT_CONNECT+10, OnTrayBTConnect)
ON_COMMAND(IDM_BT_CONNECT+11, OnTrayBTConnect)
ON_COMMAND(IDM_BT_CONNECT+12, OnTrayBTConnect)
ON_COMMAND(IDM_BT_CONNECT+13, OnTrayBTConnect)
ON_COMMAND(IDM_BT_CONNECT+14, OnTrayBTConnect)
ON_COMMAND(IDM_BT_CONNECT+15, OnTrayBTConnect)
ON_COMMAND(IDM_BT_CONNECT+16, OnTrayBTConnect)
ON_COMMAND(IDM_BT_CONNECT+17, OnTrayBTConnect)
ON_COMMAND(IDM_BT_CONNECT+18, OnTrayBTConnect)
ON_COMMAND(IDM_BT_CONNECT+19, OnTrayBTConnect)
ON_COMMAND(IDM_BT_DELETE, OnTrayBTDelete)
ON_COMMAND(IDM_BT_DELETE+1, OnTrayBTDelete)
ON_COMMAND(IDM_BT_DELETE+2, OnTrayBTDelete)
ON_COMMAND(IDM_BT_DELETE+3, OnTrayBTDelete)
ON_COMMAND(IDM_BT_DELETE+4, OnTrayBTDelete)
ON_COMMAND(IDM_BT_DELETE+5, OnTrayBTDelete)
ON_COMMAND(IDM_BT_DELETE+6, OnTrayBTDelete)
ON_COMMAND(IDM_BT_DELETE+7, OnTrayBTDelete)
ON_COMMAND(IDM_BT_DELETE+8, OnTrayBTDelete)
ON_COMMAND(IDM_BT_DELETE+9, OnTrayBTDelete)
ON_COMMAND(IDM_BT_DELETE+10, OnTrayBTDelete)
ON_COMMAND(IDM_BT_DELETE+11, OnTrayBTDelete)
ON_COMMAND(IDM_BT_DELETE+12, OnTrayBTDelete)
ON_COMMAND(IDM_BT_DELETE+13, OnTrayBTDelete)
ON_COMMAND(IDM_BT_DELETE+14, OnTrayBTDelete)
ON_COMMAND(IDM_BT_DELETE+15, OnTrayBTDelete)
ON_COMMAND(IDM_BT_DELETE+16, OnTrayBTDelete)
ON_COMMAND(IDM_BT_DELETE+17, OnTrayBTDelete)
ON_COMMAND(IDM_BT_DELETE+18, OnTrayBTDelete)
ON_COMMAND(IDM_BT_DELETE+19, OnTrayBTDelete)
ON_COMMAND(IDM_LEAVE, OnTrayLeave)
ON_BN_CLICKED(IDC_BACK, OnButton)
ON_BN_CLICKED(IDC_TAB, OnButton) 
ON_BN_CLICKED(IDC_ENTER, OnButton)
ON_BN_CLICKED(IDC_SHIFT_LEFT, OnButton)
ON_BN_CLICKED(IDC_CTRL_LEFT, OnButton)
ON_BN_CLICKED(IDC_ALT_LEFT, OnButton)
ON_BN_CLICKED(IDC_CAPS, OnButton)
ON_BN_CLICKED(IDC_ESC, OnButton)
ON_BN_CLICKED(IDC_SPACE, OnButton)
ON_BN_CLICKED(IDC_PAGEUP, OnButton)
ON_BN_CLICKED(IDC_PAGEDOWN, OnButton)
ON_BN_CLICKED(IDC_END, OnButton)
ON_BN_CLICKED(IDC_HOME, OnButton)
ON_BN_CLICKED(IDC_LEFT, OnButton)
ON_BN_CLICKED(IDC_UP, OnButton)
ON_BN_CLICKED(IDC_RIGHT, OnButton)
ON_BN_CLICKED(IDC_DOWN, OnButton)
ON_BN_CLICKED(IDC_INSERT, OnButton)
ON_BN_CLICKED(IDC_DELETE, OnButton)
ON_BN_CLICKED(IDC_0, OnButton)
ON_BN_CLICKED(IDC_1, OnButton)
ON_BN_CLICKED(IDC_2, OnButton)
ON_BN_CLICKED(IDC_3, OnButton)
ON_BN_CLICKED(IDC_4, OnButton)
ON_BN_CLICKED(IDC_5, OnButton)
ON_BN_CLICKED(IDC_6, OnButton)
ON_BN_CLICKED(IDC_7, OnButton)
ON_BN_CLICKED(IDC_8, OnButton)
ON_BN_CLICKED(IDC_9, OnButton)
ON_BN_CLICKED(IDC_A, OnButton)
ON_BN_CLICKED(IDC_B, OnButton)
ON_BN_CLICKED(IDC_C, OnButton)
ON_BN_CLICKED(IDC_D, OnButton)
ON_BN_CLICKED(IDC_E, OnButton)
ON_BN_CLICKED(IDC_F, OnButton)
ON_BN_CLICKED(IDC_G, OnButton)
ON_BN_CLICKED(IDC_H, OnButton)
ON_BN_CLICKED(IDC_I, OnButton)
ON_BN_CLICKED(IDC_J, OnButton)
ON_BN_CLICKED(IDC_K, OnButton)
ON_BN_CLICKED(IDC_L, OnButton)
ON_BN_CLICKED(IDC_M, OnButton)
ON_BN_CLICKED(IDC_N, OnButton)
ON_BN_CLICKED(IDC_O, OnButton)
ON_BN_CLICKED(IDC_P, OnButton)
ON_BN_CLICKED(IDC_Q, OnButton)
ON_BN_CLICKED(IDC_R, OnButton)
ON_BN_CLICKED(IDC_S, OnButton)
ON_BN_CLICKED(IDC_T, OnButton)
ON_BN_CLICKED(IDC_U, OnButton)
ON_BN_CLICKED(IDC_V, OnButton)
ON_BN_CLICKED(IDC_W, OnButton)
ON_BN_CLICKED(IDC_X, OnButton)
ON_BN_CLICKED(IDC_Y, OnButton)
ON_BN_CLICKED(IDC_Z, OnButton)
ON_BN_CLICKED(IDC_WIN_LEFT, OnButton)
ON_BN_CLICKED(IDC_MENU, OnButton)
ON_BN_CLICKED(IDC_PAD0, OnButton)
ON_BN_CLICKED(IDC_PAD1, OnButton)
ON_BN_CLICKED(IDC_PAD2, OnButton)
ON_BN_CLICKED(IDC_PAD3, OnButton)
ON_BN_CLICKED(IDC_PAD4, OnButton)
ON_BN_CLICKED(IDC_PAD5, OnButton)
ON_BN_CLICKED(IDD_IVTKEYBOARD_DIALOG, OnButton)
ON_BN_CLICKED(IDC_PAD6, OnButton)
ON_BN_CLICKED(IDC_PAD7, OnButton)
ON_BN_CLICKED(IDC_PAD8, OnButton)
ON_BN_CLICKED(IDC_PAD9, OnButton)
ON_BN_CLICKED(IDC_MUL , OnButton)
ON_BN_CLICKED(IDC_PLUS, OnButton)
ON_BN_CLICKED(IDC_MINUS, OnButton)
ON_BN_CLICKED(IDC_DOT_RIGHT, OnButton)
ON_BN_CLICKED(IDC_EQUAL_RIGHT, OnButton)
ON_BN_CLICKED(IDC_COMMA_RIGHT, OnButton)
ON_BN_CLICKED(IDC_DIV, OnButton)
ON_BN_CLICKED(IDC_F1, OnButton)
ON_BN_CLICKED(IDC_F2, OnButton)
ON_BN_CLICKED(IDC_F3, OnButton)
ON_BN_CLICKED(IDC_F4, OnButton)
ON_BN_CLICKED(IDC_F5, OnButton)
ON_BN_CLICKED(IDC_F6, OnButton)
ON_BN_CLICKED(IDC_F7, OnButton)
ON_BN_CLICKED(IDC_F8, OnButton)
ON_BN_CLICKED(IDC_F9, OnButton)
ON_BN_CLICKED(IDC_F10, OnButton)
ON_BN_CLICKED(IDC_F11, OnButton)
ON_BN_CLICKED(IDC_F12, OnButton)
ON_BN_CLICKED(IDC_F13, OnButton)
ON_BN_CLICKED(IDC_F14, OnButton)
ON_BN_CLICKED(IDC_F15, OnButton)
ON_BN_CLICKED(IDR_MAINFRAME, OnButton)
ON_BN_CLICKED(IDC_NUMLOCK, OnButton)
ON_BN_CLICKED(IDC_SEMICOLON, OnButton)
ON_BN_CLICKED(IDC_EQUAL, OnButton)
ON_BN_CLICKED(IDC_COMMA, OnButton)
ON_BN_CLICKED(IDC_DASH, OnButton)
ON_BN_CLICKED(IDC_DOT, OnButton)
ON_BN_CLICKED(IDC_SLASH, OnButton)
ON_BN_CLICKED(IDC_SINGLE_QUOTE, OnButton)
ON_BN_CLICKED(IDC_OPEN, OnButton)
ON_BN_CLICKED(IDC_BACKSLASH, OnButton)
ON_BN_CLICKED(IDC_CLOSE, OnButton)
ON_BN_CLICKED(IDC_APOSTROPHE, OnButton)
ON_BN_CLICKED(IDC_ALT_RIGHT, OnButton)
ON_BN_CLICKED(IDC_ENTER_RIGHT, OnButton)
ON_BN_CLICKED(IDC_SHIFT_RIGHT, OnButton)
ON_BN_CLICKED(IDC_CTRL_RIGHT, OnButton)
ON_BN_CLICKED(IDC_EJECT, OnButton)
ON_BN_CLICKED(IDC_MOUSE, OnMouse)
//}}AFX_MSG_MAP
ON_WM_DEVICECHANGE()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CIVTKeyboardDlg message handlers

BOOL
CIVTKeyboardDlg::OnInitDialog() {
    CDialog::OnInitDialog();
	
    SetIcon(m_hIcon, TRUE);                     // Set big icon
    SetIcon(m_hIcon, FALSE);            // Set small icon
	
    m_pTray = NULL;
    m_pTray = new CTrayNot(this, WM_TRAY_NOTIFY, NULL, theApp.m_pIconList);
	
    SetForegroundWindow();
    CenterWindow();    
	
    bIsDlgOpened = FALSE;
    bIsMenuOpened = FALSE;
    theApp.bContinueDlg = FALSE;
    m_IsMediaKeyChanged = FALSE;
	
    g_StopMouse = FALSE;
    bIsForeground = FALSE;
	bPassF4 = FALSE;
	
    g_hMouseHook = SetWindowsHookEx(WH_MOUSE_LL, LowLevelMouseProc,
		GetModuleHandle(NULL), 0);
		/*
        g_hKeyboardHook = SetWindowsHookEx(WH_KEYBOARD_LL, LowLevelKeyboardProc,
		GetModuleHandle(NULL), 0);
    */
    // Register RAW HID INPUT ///////////////////////////////////////////////
    RAWINPUTDEVICE      Rid[2];
	
    // Mouse
    Rid[0].usUsagePage = 0x01; 
    Rid[0].usUsage = 0x02; 
    Rid[0].dwFlags = RIDEV_INPUTSINK;
    Rid[0].hwndTarget = this->GetSafeHwnd();;
	
    // Keyboard
    Rid[1].usUsagePage = 0x01; 
    Rid[1].usUsage = 0x06; 
    Rid[1].dwFlags = RIDEV_INPUTSINK | RIDEV_NOHOTKEYS;  
    Rid[1].hwndTarget = this->GetSafeHwnd();;
	
    if (RegisterRawInputDevices(Rid, 2, sizeof(Rid[0])) == FALSE) {
        return TRUE;
    }
    ////////////////////////////////////////////////////////////////////////
	
    vInitButtonStatus();
    vInitKeyValue();
    vSetKeyboardStyle();
	
    bIsRXConnected = FALSE;
    bIsBTInitialized = FALSE;
	
    SetTimer(IDT_CHECK_CONNECTION, 500, 0);    
    SetTimer(IDT_CHECK_MOUSE, 10, 0);
	
    x_left = 0;     
    y_left = 0;
    z_left = 0;
    button_left = 0x00;
    send_immediately = 0x00;
	
    pTitleDialog = new CDlgTitle;
    pTitleDialog->Create(IDD_DIALOG_TITLE, this); 
    pTitleDialog->ShowWindow(SW_HIDE); 
	
    pHintDialog = new CDialogHint;
    pHintDialog->Create(IDD_DIALOG_HINT, this); 
    pHintDialog->ShowWindow(SW_HIDE); 
	
    vTestDevice();
    vCreateMenu();
	
    return TRUE;  // return TRUE  unless you set the focus to a control
}

void
CIVTKeyboardDlg::vTestDevice() {
    if (bIsDlgOpened) {
        return;
    }
	
    BOOL        r_value;
	
    r_value = bIsBTHarewareExist();
    if (!r_value) {
        theApp.bContinueDlg = FALSE;
        OnLeave();
        return;
    }
	
    BOOL        bIsInitializing;
    BOOL        bIsInitialized;
    BOOL        bIsDiscoverable;
    BOOL        bIsConnecting;
    BOOL        bIsConnected;
    BOOL        bIsPinRequest;
	
    r_value = bBTGetStatus(&bIsInitializing, &bIsInitialized,
		&bIsDiscoverable, &bIsConnecting, &bIsConnected,
		&bIsPinRequest);
	
    if (!(bIsInitializing | bIsInitialized)) {
        OnLeave();
    }
	
    if (!bIsConnected) {
        if (bIsRXConnected) {
            bSetLed(0x00);
			
            pTitleDialog->vSetTitle(TEXT(""));
            pTitleDialog->ShowWindow(SW_HIDE);
			
            this->SetWindowText(TEXT("VirtualHID"));
			
            bIsRXConnected = FALSE;
			theApp.bIsBTConnected = FALSE;
			
            vCreateMenu();
			
            InvalidateRect(NULL, TRUE);
            UpdateWindow();
			
            SetForegroundWindow();
        }
		
        bIsRXConnected = FALSE;
    } else {
        if (!bIsRXConnected) {
            bSetLed(0x01);
			
			theApp.bIsBTConnected = TRUE;
            bIsRXConnected = TRUE;
			
            char        hostname[MAX_PATH];
            WCHAR       hostname_wc[MAX_PATH];
			
            memset(hostname, 0x00, sizeof(hostname));
            memset(hostname_wc, 0x00, sizeof(hostname_wc));
			
            r_value = bBTGetHostName(hostname);
            if (r_value) {
                if (hostname[24] != 0) {
                    hostname[25] = '.';
                    hostname[26] = '.';
                    hostname[27] = '.';
                    hostname[28] = 0x00;
                }
				
                MultiByteToWideChar(CP_UTF8, 0, hostname, 34, hostname_wc,
					MAX_PATH);
				
                pTitleDialog->vSetTitle(hostname_wc);
				
                this->SetWindowText(hostname_wc);
            } else {
                pTitleDialog->vSetTitle(TEXT(""));
                pTitleDialog->ShowWindow(SW_HIDE);
                this->SetWindowText(TEXT("VirtualHID"));
            }
			
            char        address[MAX_PATH];
			memset(address, 0x00, sizeof(address));
			r_value = bBTGetHostAddress(address);
			if(r_value){
				vLoadConfiguration(address);
				vSaveConfiguration(address);
				if (theApp.m_aplang == LANG_JP) {
					if(!theApp.bIsBTConnected){
						theApp.bContinueDlg = TRUE;
						OnLeave();
						return;
					}
				} else {
					vSetKeyboardStyle();
				}
			}
			
			
            vCreateMenu();
			
            InvalidateRect(NULL, TRUE);
            UpdateWindow();
			
            SetForegroundWindow();
			
            SetTimer(IDT_CLOSE_TITLE, 1500, NULL);
        }
		
        bIsRXConnected = TRUE;
    }
	
    if (bIsInitialized) {
        if (!bIsBTInitialized) {
            bIsBTInitialized = TRUE;
            if (bIsMenuOpened) {
                PostMessage(WM_TRAY_NOTIFY, 0, WM_RBUTTONDOWN);
            }
            vCreateMenu();
        }
    }
	
    if ((::GetForegroundWindow() == pHintDialog->GetSafeHwnd()) ||
        (::GetForegroundWindow() == this->GetSafeHwnd())) {
        bIsForeground = TRUE;
    } else {
        if (bIsForeground) {
            bIsForeground = FALSE;
            bSendKey(0x00, 0x00, 0x00, 0x00, 0x00, 0x00);
			
            if (g_StopMouse) {
                g_StopMouse = FALSE;
                pHintDialog->ShowWindow(SW_HIDE);
				
                InvalidateRect(NULL, TRUE);
                UpdateWindow();
            }
        }
    }
}

BOOL
CIVTKeyboardDlg::OnDeviceChange(UINT nEventType, DWORD dwData) {
    return TRUE;
}

void
CIVTKeyboardDlg::OnButton() {
    if (!bIsRXConnected) {
        return;
    }
	
    int         k_value;
	
    k_value = GetCurrentMessage()->wParam;
	
    if (theApp.keyboard_style == STYLE_IPAD) {
        if ((k_value >= IDC_F1) &&
            (k_value <= IDC_F12) ||
            (k_value == IDC_ESC)) {
            m_IsMediaKeyChanged = TRUE;
        }
    } else if (theApp.keyboard_style == STYLE_ANDROID) {
        if ((k_value >= IDC_F1) &&
            (k_value <= IDC_F12) ||
            (k_value == IDC_ESC)) {
            m_IsMediaKeyChanged = TRUE;
        }
    } else if (theApp.keyboard_style == STYLE_MAC) {
        if (k_value == IDC_EJECT) {
            m_IsMediaKeyChanged = TRUE;
        }
    }                           
	
    m_IsButtonPressed[k_value] = TRUE;
    vScanAndSentKey();
	
    Sleep(10);
	
    if (theApp.keyboard_style == STYLE_IPAD) {
        if ((k_value >= IDC_F1) &&
            (k_value <= IDC_F12) ||
            (k_value == IDC_ESC)) {
            m_IsMediaKeyChanged = TRUE;
        }
    } else if (theApp.keyboard_style == STYLE_ANDROID) {
        if ((k_value >= IDC_F1) &&
            (k_value <= IDC_F12) ||
            (k_value == IDC_ESC)) {
            m_IsMediaKeyChanged = TRUE;
        }
    } else if (theApp.keyboard_style == STYLE_MAC) {
        if (k_value == IDC_EJECT) {
            m_IsMediaKeyChanged = TRUE;
        }
    }                           
	
    m_IsButtonPressed[k_value] = FALSE;
    vScanAndSentKey();
	
	
    GetDlgItem(IDC_STATIC_HINT)->SetFocus();
}

void
CIVTKeyboardDlg::OnCancel() {
    if (m_IsButtonPressed[IDC_ESC]) {
        return;
    }
	
    this->ShowWindow(SW_HIDE);
}

void
CIVTKeyboardDlg::OnLeave() {
    // UnRegister RAW HID INPUT ///////////////////////////////////////////////
    RAWINPUTDEVICE      Rid[2];
	
    Rid[0].usUsagePage = 0x01; 
    Rid[0].usUsage = 0x02; 
    Rid[0].dwFlags = RIDEV_REMOVE;
    Rid[0].hwndTarget = NULL;
	
    Rid[1].usUsagePage = 0x01; 
    Rid[1].usUsage = 0x06; 
    Rid[1].dwFlags = RIDEV_REMOVE;
    Rid[1].hwndTarget = NULL;
	
    if (RegisterRawInputDevices(Rid, 2, sizeof(Rid[0])) == FALSE) {
        //OutputDebugString(TEXT("Unregister Fail!\n"));
    }
	
    UnhookWindowsHookEx(g_hMouseHook);
    //UnhookWindowsHookEx(g_hKeyboardHook);
	
    pTitleDialog->DestroyWindow();
    delete pTitleDialog;
	
    pHintDialog->DestroyWindow();
    delete pHintDialog;
	
    if (m_pTray) {
        delete m_pTray;
        m_pTray = NULL;
    }
	
    CDialog::OnCancel();
}

void
CIVTKeyboardDlg::vInitButtonStatus() {
    memset(m_IsMouseButtonPressed, 0x00, sizeof(m_IsMouseButtonPressed));
    memset(m_IsButtonPressed, 0x00, sizeof(m_IsButtonPressed));
    memset(m_IsButtonMultiline, 0x00, sizeof(m_IsButtonMultiline));
	
    if (theApp.m_aplang == LANG_JP) {
        m_IsButtonMultiline[IDC_BACK] = TRUE;
        m_IsButtonMultiline[IDC_A] = TRUE;
        m_IsButtonMultiline[IDC_B] = TRUE;
        m_IsButtonMultiline[IDC_C] = TRUE;
        m_IsButtonMultiline[IDC_D] = TRUE;
        m_IsButtonMultiline[IDC_E] = TRUE;
        m_IsButtonMultiline[IDC_F] = TRUE;
        m_IsButtonMultiline[IDC_G] = TRUE;
        m_IsButtonMultiline[IDC_H] = TRUE;
        m_IsButtonMultiline[IDC_I] = TRUE;
        m_IsButtonMultiline[IDC_J] = TRUE;
        m_IsButtonMultiline[IDC_K] = TRUE;
        m_IsButtonMultiline[IDC_L] = TRUE;
        m_IsButtonMultiline[IDC_M] = TRUE;
        m_IsButtonMultiline[IDC_N] = TRUE;
        m_IsButtonMultiline[IDC_O] = TRUE;
        m_IsButtonMultiline[IDC_P] = TRUE;
        m_IsButtonMultiline[IDC_Q] = TRUE;
        m_IsButtonMultiline[IDC_R] = TRUE;
        m_IsButtonMultiline[IDC_S] = TRUE;
        m_IsButtonMultiline[IDC_T] = TRUE;
        m_IsButtonMultiline[IDC_U] = TRUE;
        m_IsButtonMultiline[IDC_V] = TRUE;
        m_IsButtonMultiline[IDC_W] = TRUE;
        m_IsButtonMultiline[IDC_X] = TRUE;
        m_IsButtonMultiline[IDC_Y] = TRUE;
        m_IsButtonMultiline[IDC_Z] = TRUE;
        m_IsButtonMultiline[IDC_0] = TRUE;
        m_IsButtonMultiline[IDC_1] = TRUE;
        m_IsButtonMultiline[IDC_2] = TRUE;
        m_IsButtonMultiline[IDC_3] = TRUE;
        m_IsButtonMultiline[IDC_4] = TRUE;
        m_IsButtonMultiline[IDC_5] = TRUE;
        m_IsButtonMultiline[IDC_6] = TRUE;
        m_IsButtonMultiline[IDC_7] = TRUE;
        m_IsButtonMultiline[IDC_8] = TRUE;
        m_IsButtonMultiline[IDC_9] = TRUE;
        m_IsButtonMultiline[IDC_YEN_JIS] = TRUE;
        m_IsButtonMultiline[IDC_SEMICOLON] = TRUE;         
        m_IsButtonMultiline[IDC_EQUAL] = TRUE;             
        m_IsButtonMultiline[IDC_COMMA] = TRUE;
        m_IsButtonMultiline[IDC_DASH] = TRUE;              
        m_IsButtonMultiline[IDC_DOT] = TRUE;               
        m_IsButtonMultiline[IDC_SLASH] = TRUE;             
        m_IsButtonMultiline[IDC_SINGLE_QUOTE] = TRUE;      
        m_IsButtonMultiline[IDC_OPEN] = TRUE;              
        m_IsButtonMultiline[IDC_BACKSLASH] = TRUE;         
        m_IsButtonMultiline[IDC_CLOSE] = TRUE;             
        m_IsButtonMultiline[IDC_APOSTROPHE] = TRUE;
        m_IsButtonMultiline[IDC_BACKSLASH_JIS] = TRUE;
        m_IsButtonMultiline[IDC_BACKUP_JIS] = TRUE;
    } else {
        m_IsButtonMultiline[IDC_BACK] = TRUE;
        m_IsButtonMultiline[IDC_0] = TRUE;
        m_IsButtonMultiline[IDC_1] = TRUE;
        m_IsButtonMultiline[IDC_2] = TRUE;
        m_IsButtonMultiline[IDC_3] = TRUE;
        m_IsButtonMultiline[IDC_4] = TRUE;
        m_IsButtonMultiline[IDC_5] = TRUE;
        m_IsButtonMultiline[IDC_6] = TRUE;
        m_IsButtonMultiline[IDC_7] = TRUE;
        m_IsButtonMultiline[IDC_8] = TRUE;
        m_IsButtonMultiline[IDC_9] = TRUE;
        m_IsButtonMultiline[IDC_SEMICOLON] = TRUE;         
        m_IsButtonMultiline[IDC_EQUAL] = TRUE;             
        m_IsButtonMultiline[IDC_COMMA] = TRUE;
        m_IsButtonMultiline[IDC_DASH] = TRUE;              
        m_IsButtonMultiline[IDC_DOT] = TRUE;               
        m_IsButtonMultiline[IDC_SLASH] = TRUE;             
        m_IsButtonMultiline[IDC_SINGLE_QUOTE] = TRUE;      
        m_IsButtonMultiline[IDC_OPEN] = TRUE;              
        m_IsButtonMultiline[IDC_BACKSLASH] = TRUE;         
        m_IsButtonMultiline[IDC_CLOSE] = TRUE;             
        m_IsButtonMultiline[IDC_APOSTROPHE] = TRUE;
    }
}

HBRUSH
CIVTKeyboardDlg::OnCtlColor(CDC *pDC, CWnd *pWnd, UINT nCtlColor) {
    switch (nCtlColor) {
	case CTLCOLOR_BTN:
        return (HBRUSH) GetStockObject(HOLLOW_BRUSH);
		
	case CTLCOLOR_STATIC:
	case CTLCOLOR_DLG:
		{
            pDC->SetBkColor(LIGHT_GRAY);
            return (HBRUSH) (m_pLightGrayBkBrush->GetSafeHandle());
        }
        break;
	default:
        return CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
    }
}

void
CIVTKeyboardDlg::vDrawGradientButton(CDC *pDC, RECT *pRECT, BOOL bConnected,
                                     BOOL bPressed) {
    TRIVERTEX           vertices[2];
    GRADIENT_RECT       gRect;
	
    unsigned short up, down;    
	
    if (bConnected) {
        if (bPressed) {
            up = 0xFFFF;
            down = 180 * 256;
            vertices[0].Red = 0;
            vertices[0].Green = 0;
            vertices[0].Blue = up;
            vertices[1].Red = 0;
            vertices[1].Green = 0;
            vertices[1].Blue = down;
        } else if (g_StopMouse) {
            up = 160 * 256;
            down = 80 * 256; 
            vertices[0].Red = up;
            vertices[0].Green = up;
            vertices[0].Blue = up;
            vertices[1].Red = down;
            vertices[1].Green = down;
            vertices[1].Blue = down;
        } else {
            up = 80 * 256;
            down = 0 * 256;
            vertices[0].Red = up;
            vertices[0].Green = up;
            vertices[0].Blue = up;            
            vertices[1].Red = down;
            vertices[1].Green = down;
            vertices[1].Blue = down;
        }
    } else {
        up = 160 * 256;
        down = 80 * 256; 
        vertices[0].Red = up;
        vertices[0].Green = up;
        vertices[0].Blue = up;
        vertices[1].Red = down;
        vertices[1].Green = down;
        vertices[1].Blue = down;
    }
	
    pDC->BeginPath();
    pDC->RoundRect(pRECT, CPoint(8, 8));
    pDC->EndPath();   
    pDC->SelectClipPath(RGN_COPY);
	
    vertices[0].x = pRECT->left;
    vertices[0].y = pRECT->top;
    vertices[0].Alpha = 0;
	
    vertices[1].x = pRECT->right;
    vertices[1].y = pRECT->bottom;
    vertices[1].Alpha = 0; 
	
    gRect.UpperLeft = 0;
    gRect.LowerRight = 1;
	
    GradientFill(pDC->GetSafeHdc(), vertices, 2, &gRect, 1,
		GRADIENT_FILL_RECT_V);
}

void
CIVTKeyboardDlg::vDrawGradientEnterButton(CDC *pDC, RECT *pRECT,
                                          BOOL bConnected, BOOL bPressed) {
    TRIVERTEX           vertices[2];
    GRADIENT_RECT       gRect;
	
    unsigned short up, down;    
	
    RECT        newrect;
    newrect.top = pRECT->top;
    newrect.left = pRECT->left;
    newrect.right = pRECT->right;
    newrect.bottom = pRECT->bottom / 2 - 1;
	
    if (bConnected) {
        if (bPressed) {
            up = 0xFFFF;
            down = 218 * 256;
            vertices[0].Red = 0;
            vertices[0].Green = 0;
            vertices[0].Blue = up;
            vertices[1].Red = 0;
            vertices[1].Green = 0;
            vertices[1].Blue = down;
        } else if (g_StopMouse) {
            up = 160 * 256;
            down = 120 * 256; 
            vertices[0].Red = up;
            vertices[0].Green = up;
            vertices[0].Blue = up;
            vertices[1].Red = down;
            vertices[1].Green = down;
            vertices[1].Blue = down;
        } else {
            up = 80 * 256;
            down = 40 * 256;
            vertices[0].Red = up;
            vertices[0].Green = up;
            vertices[0].Blue = up;            
            vertices[1].Red = down;
            vertices[1].Green = down;
            vertices[1].Blue = down;
        }
    } else {
        up = 160 * 256;
        down = 120 * 256; 
        vertices[0].Red = up;
        vertices[0].Green = up;
        vertices[0].Blue = up;
        vertices[1].Red = down;
        vertices[1].Green = down;
        vertices[1].Blue = down;
    }
	
    pDC->BeginPath();
    pDC->RoundRect(&newrect, CPoint(8, 8));
    pDC->EndPath();   
    pDC->SelectClipPath(RGN_COPY);
	
    vertices[0].x = newrect.left;
    vertices[0].y = newrect.top;
    vertices[0].Alpha = 0;
	
    vertices[1].x = newrect.right;
    vertices[1].y = newrect.bottom;
    vertices[1].Alpha = 0; 
	
    gRect.UpperLeft = 0;
    gRect.LowerRight = 1;
	
    GradientFill(pDC->GetSafeHdc(), vertices, 2, &gRect, 1,
		GRADIENT_FILL_RECT_V);
	
    newrect.top = pRECT->bottom / 2 - 6;
    newrect.left = pRECT->left + 9;
    newrect.right = pRECT->right;
    newrect.bottom = pRECT->bottom;
	
    if (bConnected) {
        if (bPressed) {
            up = 224 * 256;
            down = 180 * 256;
            vertices[0].Red = 0;
            vertices[0].Green = 0;
            vertices[0].Blue = up;
            vertices[1].Red = 0;
            vertices[1].Green = 0;
            vertices[1].Blue = down;
        } else if (g_StopMouse) {
            up = 125 * 256;
            down = 80 * 256; 
            vertices[0].Red = up;
            vertices[0].Green = up;
            vertices[0].Blue = up;
            vertices[1].Red = down;
            vertices[1].Green = down;
            vertices[1].Blue = down;
        } else {
            up = 45 * 256;
            down = 0 * 256;
            vertices[0].Red = up;
            vertices[0].Green = up;
            vertices[0].Blue = up;            
            vertices[1].Red = down;
            vertices[1].Green = down;
            vertices[1].Blue = down;
        }
    } else {
        up = 125 * 256;
        down = 80 * 256; 
        vertices[0].Red = up;
        vertices[0].Green = up;
        vertices[0].Blue = up;
        vertices[1].Red = down;
        vertices[1].Green = down;
        vertices[1].Blue = down;
    }
	
    pDC->BeginPath();
    pDC->RoundRect(&newrect, CPoint(8, 8));
    pDC->EndPath();   
    pDC->SelectClipPath(RGN_COPY);
	
    vertices[0].x = newrect.left;
    vertices[0].y = newrect.top;
    vertices[0].Alpha = 0;
	
    vertices[1].x = newrect.right;
    vertices[1].y = newrect.bottom;
    vertices[1].Alpha = 0; 
	
    gRect.UpperLeft = 0;
    gRect.LowerRight = 1;
	
    GradientFill(pDC->GetSafeHdc(), vertices, 2, &gRect, 1,
		GRADIENT_FILL_RECT_V);
}

void
CIVTKeyboardDlg::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct) {
    if (nIDCtl <= MAX_KEY_VALUE) {
        CDC     dc;
        RECT    rect;
        RECT    textrect;
        UINT    state;
        CPoint  point;
        CSize   size;
        HICON   h_icon;
        WCHAR   buffer[MAX_PATH];
		
        // Get the Button DC to CDC
        dc.Attach(lpDrawItemStruct ->hDC);   
		
        //Store the Button rect to our local rect.
        rect = lpDrawItemStruct->rcItem;
        state = lpDrawItemStruct->itemState;
		
        //Get the Caption of Button Window 
        memset(buffer, 0x00, MAX_PATH);     
        ::GetWindowText(lpDrawItemStruct->hwndItem, buffer, MAX_PATH); 
		
        //Setting the Text Color                    
        dc.SetTextColor(WHITE);         
        dc.SetBkMode(TRANSPARENT);
		
        if (bIsRXConnected) {
            if ((m_IsButtonPressed[nIDCtl]) || (state & ODS_SELECTED)) {
                if ((theApp.m_aplang == LANG_JP) &&
                    (theApp.keyboard_layout == LAYOUT_JIS) &&
                    (nIDCtl == IDC_ENTER)) {
                    vDrawGradientEnterButton(&dc, &rect, TRUE, TRUE);
                } else {
                    vDrawGradientButton(&dc, &rect, TRUE, TRUE);
                }
            } else {
                if ((theApp.m_aplang == LANG_JP) &&
                    (theApp.keyboard_layout == LAYOUT_JIS) &&
                    (nIDCtl == IDC_ENTER)) {
                    vDrawGradientEnterButton(&dc, &rect, TRUE, FALSE);
                } else {
                    vDrawGradientButton(&dc, &rect, TRUE, FALSE);
                }
            }
        } else {
            if ((theApp.m_aplang == LANG_JP) &&
                (theApp.keyboard_layout == LAYOUT_JIS) &&
                (nIDCtl == IDC_ENTER)) {
                vDrawGradientEnterButton(&dc, &rect, FALSE, FALSE);
            } else {
                vDrawGradientButton(&dc, &rect, FALSE, FALSE);
            }
        }
		
        if (theApp.keyboard_style == STYLE_IPAD) {
            if (((nIDCtl >= IDC_F1) && (nIDCtl <= IDC_F12)) ||
                (nIDCtl == IDC_ESC) ||
                (nIDCtl == IDC_WIN_LEFT) ||
                (nIDCtl == IDC_WIN_RIGHT)) {
                point.x = 3;
                point.y = 3;                
                size.cx = 32;
                size.cy = 32;
				
                switch (nIDCtl) {
				case IDC_ESC:
                    h_icon = AfxGetApp()->LoadIcon(IDI_ESC);
					
                    break;
				case IDC_F1:
                    h_icon = AfxGetApp()->LoadIcon(IDI_SEARCH);
					
                    break;                    
				case IDC_F2:
                    h_icon = AfxGetApp()->LoadIcon(IDI_EARTH);
					
                    break;
				case IDC_F3:
                    h_icon = AfxGetApp()->LoadIcon(IDI_APPLE);
					
                    break;
				case IDC_F4:
                    h_icon = AfxGetApp()->LoadIcon(IDI_SLIDE);
					
                    break;
				case IDC_F5:
                    point.x = 5;
                    h_icon = AfxGetApp()->LoadIcon(IDI_KEYBOARD);
					
                    break;
				case IDC_F6:
                    h_icon = AfxGetApp()->LoadIcon(IDI_BACK);
					
                    break;
				case IDC_F7:
                    h_icon = AfxGetApp()->LoadIcon(IDI_PLAY);
					
                    break;
				case IDC_F8:
                    h_icon = AfxGetApp()->LoadIcon(IDI_NEXT);
					
                    break;
				case IDC_F9:
                    h_icon = AfxGetApp()->LoadIcon(IDI_MUTE);
					
                    break;
				case IDC_F10:
                    h_icon = AfxGetApp()->LoadIcon(IDI_VOLDOWN);
					
                    break;
				case IDC_F11:
                    h_icon = AfxGetApp()->LoadIcon(IDI_VOLUP);
					
                    break;
				case IDC_F12:
                    h_icon = AfxGetApp()->LoadIcon(IDI_LOCK);
					
                    break;
					
				case IDC_WIN_LEFT:
				case IDC_WIN_RIGHT:
                    h_icon = AfxGetApp()->LoadIcon(IDI_COMMAND);
                }
				
                dc.DrawState(point, size, h_icon, DSS_NORMAL, (CBrush *) NULL);
                memset(buffer, 0x00, MAX_PATH);
            }
        } else if (theApp.keyboard_style == STYLE_MAC) {
            if ((nIDCtl == IDC_WIN_LEFT) || (nIDCtl == IDC_WIN_RIGHT)) {
                point.x = 3;
                point.y = 3;                
                size.cx = 32;
                size.cy = 32;
				
                h_icon = AfxGetApp()->LoadIcon(IDI_COMMAND);
				
                dc.DrawState(point, size, h_icon, DSS_NORMAL, (CBrush *) NULL);
                memset(buffer, 0x00, MAX_PATH);
            } else if (nIDCtl == IDC_MOUSE) {
                point.x = 3;
                point.y = 3;                
                size.cx = 32;
                size.cy = 32;
				
				
                h_icon = AfxGetApp()->LoadIcon(IDI_MOUSE);
				
                dc.DrawState(point, size, h_icon, DSS_NORMAL, (CBrush *) NULL);
                memset(buffer, 0x00, MAX_PATH);
            } else if (nIDCtl == IDC_EJECT) {
                point.x = 3;
                point.y = 3;                
                size.cx = 32;
                size.cy = 32;
				
				
                h_icon = AfxGetApp()->LoadIcon(IDI_EJECT);
				
                dc.DrawState(point, size, h_icon, DSS_NORMAL, (CBrush *) NULL);
                memset(buffer, 0x00, MAX_PATH);
            }
        } else if (theApp.keyboard_style == STYLE_WINDOWS) {
            if ((nIDCtl == IDC_WIN_LEFT) || (nIDCtl == IDC_WIN_RIGHT)) {
                point.x = 3;
                point.y = 3;                
                size.cx = 32;
                size.cy = 32;
				
				
                h_icon = AfxGetApp()->LoadIcon(IDI_WIN);
				
                dc.DrawState(point, size, h_icon, DSS_NORMAL, (CBrush *) NULL);
                memset(buffer, 0x00, MAX_PATH);
            } else if (nIDCtl == IDC_MENU) {
                point.x = 3;
                point.y = 3;                
                size.cx = 32;
                size.cy = 32;
				
                h_icon = AfxGetApp()->LoadIcon(IDI_WIN_MENU);
				
                dc.DrawState(point, size, h_icon, DSS_NORMAL, (CBrush *) NULL);
                memset(buffer, 0x00, MAX_PATH);
            } else if (nIDCtl == IDC_MOUSE) {
                point.x = 3;
                point.y = 3;                
                size.cx = 32;
                size.cy = 32;
				
                h_icon = AfxGetApp()->LoadIcon(IDI_MOUSE);
                dc.DrawState(point, size, h_icon, DSS_NORMAL, (CBrush *) NULL);
                memset(buffer, 0x00, MAX_PATH);
            }
        } else if (theApp.keyboard_style == STYLE_ANDROID) {
            if (((nIDCtl >= IDC_F1) && (nIDCtl <= IDC_F12)) ||
                (nIDCtl == IDC_ESC)) {
                point.x = 3;
                point.y = 3;                
                size.cx = 32;
                size.cy = 32;
				
                switch (nIDCtl) {
				case IDC_ESC:
                    h_icon = AfxGetApp()->LoadIcon(IDI_ACBACK);                
                    break;
				case IDC_F1:
                    h_icon = AfxGetApp()->LoadIcon(IDI_SEARCH);
					
                    break;                    
				case IDC_F2:
                    h_icon = AfxGetApp()->LoadIcon(IDI_HOME);
                    break;
				case IDC_F3:
                    h_icon = AfxGetApp()->LoadIcon(IDI_MENU);
                    break;                
				case IDC_F6:
                    h_icon = AfxGetApp()->LoadIcon(IDI_BACK);
                    break;
				case IDC_F7:
                    h_icon = AfxGetApp()->LoadIcon(IDI_PLAY);
                    break;
				case IDC_F8:
                    h_icon = AfxGetApp()->LoadIcon(IDI_NEXT);
                    break;
				case IDC_F9:
                    h_icon = AfxGetApp()->LoadIcon(IDI_MUTE);
                    break;
				case IDC_F10:
                    h_icon = AfxGetApp()->LoadIcon(IDI_VOLDOWN);
                    break;
				case IDC_F11:
                    h_icon = AfxGetApp()->LoadIcon(IDI_VOLUP);
                    break;
				case IDC_F12:
                    h_icon = AfxGetApp()->LoadIcon(IDI_LOCK);
                    break;
                }
				
                dc.DrawState(point, size, h_icon, DSS_NORMAL, (CBrush *) NULL);
                memset(buffer, 0x00, MAX_PATH);
            } else if (nIDCtl == IDC_MOUSE) {
                point.x = 3;
                point.y = 3;                
                size.cx = 32;
                size.cy = 32;
				
                h_icon = AfxGetApp()->LoadIcon(IDI_MOUSE);
                dc.DrawState(point, size, h_icon, DSS_NORMAL, (CBrush *) NULL);
                memset(buffer, 0x00, MAX_PATH);
            }
        }
		
        if (nIDCtl == IDC_UP) {
            point.x = 11;
            point.y = 3;                
            size.cx = 32;
            size.cy = 32;
			
            h_icon = AfxGetApp()->LoadIcon(IDI_UP);
            dc.DrawState(point, size, h_icon, DSS_NORMAL, (CBrush *) NULL);
            memset(buffer, 0x00, MAX_PATH);
        }
		
        if (nIDCtl == IDC_DOWN) {
            point.x = 11;
            point.y = 3;                
            size.cx = 32;
            size.cy = 32;
			
            h_icon = AfxGetApp()->LoadIcon(IDI_DOWN);
            dc.DrawState(point, size, h_icon, DSS_NORMAL, (CBrush *) NULL);
            memset(buffer, 0x00, MAX_PATH);
        }
		
        if (nIDCtl == IDC_RIGHT) {
            point.x = 11;
            point.y = 3;                
            size.cx = 32;
            size.cy = 32;
			
            h_icon = AfxGetApp()->LoadIcon(IDI_RIGHT);        
            dc.DrawState(point, size, h_icon, DSS_NORMAL, (CBrush *) NULL);
            memset(buffer, 0x00, MAX_PATH);
        }
		
        if (nIDCtl == IDC_LEFT) {
            point.x = 11;
            point.y = 3;                
            size.cx = 32;
            size.cy = 32;
			
            h_icon = AfxGetApp()->LoadIcon(IDI_LEFT);
            dc.DrawState(point, size, h_icon, DSS_NORMAL, (CBrush *) NULL);
            memset(buffer, 0x00, MAX_PATH);
        }
		
        //Redraw the  Caption of Button Window 
        if (m_IsButtonMultiline[nIDCtl]) {
            memcpy(&textrect, &rect, sizeof(rect));
			
            if (theApp.m_aplang == LANG_JP) {
                textrect.top += 5;                
                textrect.left += 7;
                dc.DrawText(buffer, &textrect, DT_VCENTER);
            } else {
                textrect.top += 3;                
                dc.DrawText(buffer, &textrect, DT_CENTER | DT_VCENTER);
            }
        } else {
            dc.DrawText(buffer, &rect, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
        }
		
        // Detach the Button DC
        dc.Detach();
    } else {
        CDialog::OnDrawItem(nIDCtl, lpDrawItemStruct);
    }
}


void
CIVTKeyboardDlg::vSetKeyboardStyle() {
    switch (theApp.keyboard_style) {
	case STYLE_MAC :
        GetDlgItem(IDC_MOUSE)->ShowWindow(SW_SHOW);
		
        GetDlgItem(IDC_WIN_LEFT)->SetWindowText(TEXT("Cmd"));
        GetDlgItem(IDC_WIN_RIGHT)->SetWindowText(TEXT("Cmd"));
        GetDlgItem(IDC_ALT_LEFT)->SetWindowText(TEXT("Option"));
        GetDlgItem(IDC_ALT_RIGHT)->SetWindowText(TEXT("Option"));
        GetDlgItem(IDC_MENU)->ShowWindow(SW_HIDE);
		
		//GetDlgItem(IDC_INSERT)->SetWindowText(TEXT("fn"));
		GetDlgItem(IDC_NUMLOCK)->SetWindowText(TEXT("Clear"));
		
        if (GetDlgItem(IDC_EJECT)) {
            GetDlgItem(IDC_EJECT)->ShowWindow(SW_SHOW);
        }
        if (GetDlgItem(IDC_F13)) {
            GetDlgItem(IDC_F13)->ShowWindow(SW_SHOW);
        }
        if (GetDlgItem(IDC_F14)) {
            GetDlgItem(IDC_F14)->ShowWindow(SW_SHOW);
        }
        if (GetDlgItem(IDC_F15)) {
            GetDlgItem(IDC_F15)->ShowWindow(SW_SHOW);
        }
		
        m_KeyValue[IDC_EISU_JIS] = (char) 0x91;
        m_KeyValue[IDC_KANA_JIS] = (char) 0x90;
        m_KeyValue[IDC_BACKUP_JIS] = (char) 0x00;
		
#ifdef REPLACE_RIGHT_ENTER
		m_KeyValue[IDC_ENTER_RIGHT] = 0x58;
#endif
		
        break;
		
	case STYLE_IPAD:
        GetDlgItem(IDC_MOUSE)->ShowWindow(SW_HIDE);
		
        GetDlgItem(IDC_WIN_LEFT)->SetWindowText(TEXT("Cmd"));
        GetDlgItem(IDC_WIN_RIGHT)->SetWindowText(TEXT("Cmd"));
        GetDlgItem(IDC_ALT_LEFT)->SetWindowText(TEXT("Option"));
        GetDlgItem(IDC_ALT_RIGHT)->SetWindowText(TEXT("Option"));
        GetDlgItem(IDC_MENU)->ShowWindow(SW_HIDE);        
		
		//GetDlgItem(IDC_INSERT)->SetWindowText(TEXT("fn"));
		GetDlgItem(IDC_NUMLOCK)->SetWindowText(TEXT("Clear"));
		
        if (GetDlgItem(IDC_EJECT)) {
            GetDlgItem(IDC_EJECT)->ShowWindow(SW_HIDE);
        }
        if (GetDlgItem(IDC_F13)) {
            GetDlgItem(IDC_F13)->ShowWindow(SW_HIDE);
        }
        if (GetDlgItem(IDC_F14)) {
            GetDlgItem(IDC_F14)->ShowWindow(SW_HIDE);
        }
        if (GetDlgItem(IDC_F15)) {
            GetDlgItem(IDC_F15)->ShowWindow(SW_HIDE);
        }
		
        m_KeyValue[IDC_EISU_JIS] = (char) 0x91;
        m_KeyValue[IDC_KANA_JIS] = (char) 0x90;
        m_KeyValue[IDC_BACKUP_JIS] = (char) 0x00;
		
#ifdef REPLACE_RIGHT_ENTER
		m_KeyValue[IDC_ENTER_RIGHT] = 0x28;
#endif
        break;
		
	case STYLE_ANDROID:
        GetDlgItem(IDC_MOUSE)->ShowWindow(SW_SHOW);
		
        GetDlgItem(IDC_WIN_LEFT)->SetWindowText(TEXT("Win"));
        GetDlgItem(IDC_WIN_RIGHT)->SetWindowText(TEXT("Win"));
        GetDlgItem(IDC_ALT_LEFT)->SetWindowText(TEXT("Alt"));
        GetDlgItem(IDC_ALT_RIGHT)->SetWindowText(TEXT("Alt"));
        GetDlgItem(IDC_MENU)->ShowWindow(SW_HIDE);
		
		//GetDlgItem(IDC_INSERT)->SetWindowText(TEXT("Insert"));
		GetDlgItem(IDC_NUMLOCK)->SetWindowText(TEXT("NmLk"));
		
        if (GetDlgItem(IDC_EJECT)) {
            GetDlgItem(IDC_EJECT)->ShowWindow(SW_HIDE);
        }
        if (GetDlgItem(IDC_F13)) {
            GetDlgItem(IDC_F13)->ShowWindow(SW_HIDE);
        }
        if (GetDlgItem(IDC_F14)) {
            GetDlgItem(IDC_F14)->ShowWindow(SW_HIDE);
        }
        if (GetDlgItem(IDC_F15)) {
            GetDlgItem(IDC_F15)->ShowWindow(SW_HIDE);
        }
		
#ifdef REPLACE_RIGHT_ENTER
		m_KeyValue[IDC_ENTER_RIGHT] = 0x58;
#endif
		
        break;
		
	case STYLE_WINDOWS:
	default:
        GetDlgItem(IDC_MOUSE)->ShowWindow(SW_SHOW);
		
        GetDlgItem(IDC_WIN_LEFT)->SetWindowText(TEXT("Win"));
        GetDlgItem(IDC_WIN_RIGHT)->SetWindowText(TEXT("Win"));
        GetDlgItem(IDC_ALT_LEFT)->SetWindowText(TEXT("Alt"));
        GetDlgItem(IDC_ALT_RIGHT)->SetWindowText(TEXT("Alt"));
        GetDlgItem(IDC_MENU)->ShowWindow(SW_SHOW);
		
		//GetDlgItem(IDC_INSERT)->SetWindowText(TEXT("Insert"));
		GetDlgItem(IDC_NUMLOCK)->SetWindowText(TEXT("NmLk"));
		
        if (GetDlgItem(IDC_EJECT)) {
            GetDlgItem(IDC_EJECT)->ShowWindow(SW_HIDE);
        }
        if (GetDlgItem(IDC_F13)) {
            GetDlgItem(IDC_F13)->ShowWindow(SW_HIDE);
        }
        if (GetDlgItem(IDC_F14)) {
            GetDlgItem(IDC_F14)->ShowWindow(SW_HIDE);
        }
        if (GetDlgItem(IDC_F15)) {
            GetDlgItem(IDC_F15)->ShowWindow(SW_HIDE);
        }
		
        m_KeyValue[IDC_EISU_JIS] = (char) 0x8B;
        m_KeyValue[IDC_YEN_JIS] = (char) 0x89;
        m_KeyValue[IDC_KANA_JIS] = (char) 0x8A;
        m_KeyValue[IDC_BACKUP_JIS] = (char) 0x88;
		
#ifdef REPLACE_RIGHT_ENTER
		m_KeyValue[IDC_ENTER_RIGHT] = 0x58;
#endif
		
        break;
    }
	
    InvalidateRect(NULL, FALSE);
    UpdateWindow();
}


void
CIVTKeyboardDlg::OnMouse() {
    if (!bIsRXConnected) {
        return;
    }
	
    if (theApp.keyboard_style != STYLE_IPAD) {
        if (!g_StopMouse) {
            g_StopMouse = TRUE;
			
            pHintDialog->vShowWindow(this);
			
            SetForegroundWindow();
			
            InvalidateRect(NULL, TRUE);
            UpdateWindow();
        }
    }
}

void
CIVTKeyboardDlg::vScanAndSentKey() {
    int         i, j;
    char        metakey;
    char        keys[5];
    char        mediakeys[4];
	
    metakey = 0x00;     
    memset(keys, 0x00, 5);
    memset(mediakeys, 0x00, 4);
	
    // deal with meta key
    if (m_IsButtonPressed[IDC_SHIFT_RIGHT]) {
        metakey |= m_KeyValue[IDC_SHIFT_RIGHT];
    }
	
    if (m_IsButtonPressed[IDC_CTRL_RIGHT]) {
        metakey |= m_KeyValue[IDC_CTRL_RIGHT];
    }
	
    if (m_IsButtonPressed[IDC_ALT_RIGHT]) {
        metakey |= m_KeyValue[IDC_ALT_RIGHT];
    }
	
    if (m_IsButtonPressed[IDC_WIN_RIGHT]) {
        metakey |= m_KeyValue[IDC_WIN_RIGHT];
    }
	
    if (m_IsButtonPressed[IDC_SHIFT_LEFT]) {
        metakey |= m_KeyValue[IDC_SHIFT_LEFT];
    } 
	
    if (m_IsButtonPressed[IDC_CTRL_LEFT]) {
        metakey |= m_KeyValue[IDC_CTRL_LEFT];
    } 
	
    if (m_IsButtonPressed[IDC_ALT_LEFT]) {
        metakey |= m_KeyValue[IDC_ALT_LEFT];
    } 
	
    if (m_IsButtonPressed[IDC_WIN_LEFT]) {
        metakey |= m_KeyValue[IDC_WIN_LEFT];
    }
	
    for (i = 0,j = 0; i < MAX_KEY_VALUE; i++) {
        if ((i == IDC_WIN_LEFT) ||
            (i == IDC_WIN_RIGHT) ||
            (i == IDC_CTRL_LEFT) ||
            (i == IDC_CTRL_RIGHT) ||
            (i == IDC_ALT_LEFT) ||
            (i == IDC_ALT_RIGHT) ||
            (i == IDC_SHIFT_LEFT) ||
            (i == IDC_SHIFT_RIGHT) 
			) {
            continue;
        }
		
        if (m_IsButtonPressed[i]) {
            keys[j++] = m_KeyValue[i];
        }
		
        if (j == 5) {
            break;
        }
    }
	
    BOOL        r_value;
	
    if (theApp.keyboard_style == STYLE_IPAD) {
        if (m_IsMediaKeyChanged) {
            metakey = 0x00;     
            memset(keys, 0x00, 5);
            memset(mediakeys, 0x00, 4);
			
            if (m_IsButtonPressed[IDC_F2]) {
                // input language method change
				
                metakey = m_KeyValue[IDC_WIN_LEFT];
                keys[0] = m_KeyValue[IDC_SPACE];
				
                if (bIsRXConnected) {
                    r_value = bSendKey(metakey, keys[0], keys[1], keys[2],
						keys[3], keys[4]);
					
                    metakey = 0x00;     
                    memset(keys, 0x00, 5);
                    memset(mediakeys, 0x00, 4); 
					
                    Sleep(100);
					
                    r_value = bSendKey(metakey, keys[0], keys[1], keys[2],
						keys[3], keys[4]);
                }
            } else if (m_IsButtonPressed[IDC_F3]) {
                // apple logo
                metakey = 0x00;     
                memset(keys, 0x00, 5);
				
                metakey = m_KeyValue[IDC_ALT_LEFT];
                metakey |= m_KeyValue[IDC_SHIFT_LEFT];
                keys[0] = m_KeyValue[IDC_K];
				
                if (bIsRXConnected) {
                    r_value = bSendKey(metakey, keys[0], keys[1], keys[2],
						keys[3], keys[4]);
					
                    metakey = 0x00;     
                    memset(keys, 0x00, 5);
                    memset(mediakeys, 0x00, 4);
					
                    Sleep(100);
					
                    r_value = bSendKey(metakey, keys[0], keys[1], keys[2],
						keys[3], keys[4]);
                }
            } else {
                i = 0;
                if (m_IsButtonPressed[IDC_ESC]) {
                    // HOME BUTTON                
					
                    mediakeys[0] = 0x40;
                    mediakeys[1] = 0x00;                                        
					
                    i = 1;
                }   
				
                if (m_IsButtonPressed[IDC_F1]) {
                    // SEARCH BUTTON
                    if (i < 2) {
                        mediakeys[i * 2] = 0x21;
                        mediakeys[i * 2 + 1] = 0x02;
                        i++;
                    }
                }
				
                if (m_IsButtonPressed[IDC_F4]) {
                    // PICTURE FRAME
                    if (i < 2) {
                        mediakeys[i * 2] = (char) 0xB1;
                        mediakeys[i * 2 + 1] = 0x01;
                        i++;
                    }
                }
				
                if (m_IsButtonPressed[IDC_F5]) {
                    // Keyboard                
                    if (i < 2) {
                        mediakeys[i * 2] = (char) 0xAE;
                        mediakeys[i * 2 + 1] = 0x01;
                        i++;
                    }
                } 
				
                if (m_IsButtonPressed[IDC_F6]) {
                    // PREVIOUS TRACK
                    if (i < 2) {
                        mediakeys[i * 2] = (char) 0xB6;
                        mediakeys[i * 2 + 1] = 0x00;
                        i++;
                    }
                }
				
                if (m_IsButtonPressed[IDC_F7]) {
                    // PLAY
                    if (i < 2) {
                        mediakeys[i * 2] = (char) 0xCD;
                        mediakeys[i * 2 + 1] = 0x00;
                        i++;
                    }
                }
				
                if (m_IsButtonPressed[IDC_F8]) {
                    // NEXT TRACK
                    if (i < 2) {
                        mediakeys[i * 2] = (char) 0xB5;
                        mediakeys[i * 2 + 1] = 0x00;
                        i++;
                    }
                }
				
                if (m_IsButtonPressed[IDC_F9]) {
                    // MUTE
                    if (i < 2) {
                        mediakeys[i * 2] = (char) 0xE2;
                        mediakeys[i * 2 + 1] = 0x00;
                        i++;
                    }
                }
				
                if (m_IsButtonPressed[IDC_F10]) {
                    // VOLUME DOWN
                    if (i < 2) {
                        mediakeys[i * 2] = (char) 0xEA;
                        mediakeys[i * 2 + 1] = 0x00;                                        
                        i++;
                    }
                }
				
                if (m_IsButtonPressed[IDC_F11]) {
                    // VOLUME UP
                    if (i < 2) {
                        mediakeys[i * 2] = (char) 0xE9;
                        mediakeys[i * 2 + 1] = 0x00;
                        i++;
                    }
                }
				
                if (m_IsButtonPressed[IDC_F12]) {
                    // LOCK UNLOCK
                    if (i < 2) {
                        mediakeys[i * 2] = (char) 0x30;
                        mediakeys[i * 2 + 1] = 0x00;
                        i++;
                    }
                }
				
                if (bIsRXConnected) {
                    r_value = bSendRemote(mediakeys[0], mediakeys[1],
						mediakeys[2], mediakeys[3]);
                }
            }
			
            m_IsMediaKeyChanged = FALSE;
        } else {
            if (bIsRXConnected) {
				r_value = bSendKey(metakey, keys[0], keys[1], keys[2],
					keys[3], keys[4]);
				
            }
        }
    } else if (theApp.keyboard_style == STYLE_ANDROID) {
        if (m_IsMediaKeyChanged) {
            metakey = 0x00;     
            memset(keys, 0x00, 5);
            memset(mediakeys, 0x00, 4);
			
            i = 0;
            if (m_IsButtonPressed[IDC_ESC]) {
                // AC BACK
                mediakeys[0] = 0x24;
                mediakeys[1] = 0x02;
				
                i = 1;
            }   
			
            if (m_IsButtonPressed[IDC_F1]) {
                // AC SEARCH
                if (i < 2) {
                    mediakeys[i * 2] = 0x21;
                    mediakeys[i * 2 + 1] = 0x02;
                    i++;
                }
            }
			
            if (m_IsButtonPressed[IDC_F2]) {
                // AC HOME
                if (i < 2) {
                    mediakeys[i * 2] = 0x23;
                    mediakeys[i * 2 + 1] = 0x02;
                    i++;
                }
            }
			
            if (m_IsButtonPressed[IDC_F3]) {
                // AC MENU
                if (i < 2) {
                    mediakeys[i * 2] = 0x40;
                    mediakeys[i * 2 + 1] = 0x00;
                    i++;
                }
            }
			
            if (m_IsButtonPressed[IDC_F4]) {
                // PICTURE FRAME
                if (i < 2) {
                    mediakeys[i * 2] = (char) 0xB1;
                    mediakeys[i * 2 + 1] = 0x01;
                    i++;
                }
            }
			
            if (m_IsButtonPressed[IDC_F5]) {
                // Keyboard                
                if (i < 2) {
                    mediakeys[i * 2] = (char) 0xAE;
                    mediakeys[i * 2 + 1] = 0x01;
                    i++;
                }
            } 
			
            if (m_IsButtonPressed[IDC_F6]) {
                // PREVIOUS TRACK
                if (i < 2) {
                    mediakeys[i * 2] = (char) 0xB6;
                    mediakeys[i * 2 + 1] = 0x00;
                    i++;
                }
            }
			
            if (m_IsButtonPressed[IDC_F7]) {
                // PLAY
                if (i < 2) {
                    mediakeys[i * 2] = (char) 0xCD;
                    mediakeys[i * 2 + 1] = 0x00;
                    i++;
                }
            }
			
            if (m_IsButtonPressed[IDC_F8]) {
                // NEXT TRACK
                if (i < 2) {
                    mediakeys[i * 2] = (char) 0xB5;
                    mediakeys[i * 2 + 1] = 0x00;
                    i++;
                }
            }
			
            if (m_IsButtonPressed[IDC_F9]) {
                // MUTE
                if (i < 2) {
                    mediakeys[i * 2] = (char) 0xE2;
                    mediakeys[i * 2 + 1] = 0x00;
                    i++;
                }
            }
			
            if (m_IsButtonPressed[IDC_F10]) {
                // VOLUME DOWN
                if (i < 2) {
                    mediakeys[i * 2] = (char) 0xEA;
                    mediakeys[i * 2 + 1] = 0x00;                                        
                    i++;
                }
            }
			
            if (m_IsButtonPressed[IDC_F11]) {
                // VOLUME UP
                if (i < 2) {
                    mediakeys[i * 2] = (char) 0xE9;
                    mediakeys[i * 2 + 1] = 0x00;
                    i++;
                }
            }
			
            if (m_IsButtonPressed[IDC_F12]) {
                // LOCK UNLOCK
                if (i < 2) {
                    mediakeys[i * 2] = (char) 0x30;
                    mediakeys[i * 2 + 1] = 0x00;
                    i++;
                }
            }
			
            if (bIsRXConnected) {
                r_value = bSendRemote(mediakeys[0], mediakeys[1],
					mediakeys[2], mediakeys[3]);
            }
			
            m_IsMediaKeyChanged = FALSE;
        } else {
            if (bIsRXConnected) {                
				r_value = bSendKey(metakey, keys[0], keys[1], keys[2],
					keys[3], keys[4]);                
            }
        }
    } else if (theApp.keyboard_style == STYLE_MAC) {
        if (m_IsMediaKeyChanged) {
            metakey = 0x00;     
            memset(keys, 0x00, 5);
            memset(mediakeys, 0x00, 4);
			
            if (m_IsButtonPressed[IDC_EJECT]) {
                // AC OPEN
                mediakeys[0] = (char) 0xB8;
                mediakeys[1] = 0x00;
            }   
			
            if (bIsRXConnected) {
                r_value = bSendRemote(mediakeys[0], mediakeys[1],
					mediakeys[2], mediakeys[3]);
            }
			
            m_IsMediaKeyChanged = FALSE;
        } else {
            if (bIsRXConnected) {
                r_value = bSendKey(metakey, keys[0], keys[1], keys[2],
					keys[3], keys[4]);
            }
        }
    } else {
        if (bIsRXConnected) {
            r_value = bSendKey(metakey, keys[0], keys[1], keys[2], keys[3],
				keys[4]);
        }
    }
}

void
CIVTKeyboardDlg::vInitKeyValue() {
    memset(m_KeyValue, 0x00, sizeof(m_KeyValue));
	
    m_KeyValue[IDC_BACK] = 0x2a; 
    m_KeyValue[IDC_TAB] = 0x2b;   
    m_KeyValue[IDC_ENTER] = 0x28;
    m_KeyValue[IDC_SHIFT_LEFT] = (0x01) << 1;
    m_KeyValue[IDC_CTRL_LEFT] = (0x01) << 0;
    m_KeyValue[IDC_ALT_LEFT] = (0x01) << 2;
    m_KeyValue[IDC_CAPS] = 0x39;
    m_KeyValue[IDC_ESC] = 0x29;
    m_KeyValue[IDC_SPACE] = 0x2c;
    m_KeyValue[IDC_PAGEUP] = 0x4b;
    m_KeyValue[IDC_PAGEDOWN] = 0x4e;
    m_KeyValue[IDC_END] = 0x4d;
    m_KeyValue[IDC_HOME] = 0x4a;
    m_KeyValue[IDC_LEFT] = 0x50;
    m_KeyValue[IDC_UP] = 0x52;
    m_KeyValue[IDC_RIGHT] = 0x4f;
    m_KeyValue[IDC_DOWN] = 0x51;
    m_KeyValue[IDC_INSERT] = 0x49;
    m_KeyValue[IDC_DELETE] = 0x4c;
    m_KeyValue[IDC_0] = 0x27;
    m_KeyValue[IDC_1] = 0x1e;        
    m_KeyValue[IDC_2] = 0x1f;       
    m_KeyValue[IDC_3] = 0x20;        
    m_KeyValue[IDC_4] = 0x21;           
    m_KeyValue[IDC_5] = 0x22;      
    m_KeyValue[IDC_6] = 0x23; 
    m_KeyValue[IDC_7] = 0x24;
    m_KeyValue[IDC_8] = 0x25;
    m_KeyValue[IDC_9] = 0x26;
    m_KeyValue[IDC_A] = 0x04;
    m_KeyValue[IDC_B] = 0x05;
    m_KeyValue[IDC_C] = 0x06;
    m_KeyValue[IDC_D] = 0x07;
    m_KeyValue[IDC_E] = 0x08;
    m_KeyValue[IDC_F] = 0x09;
    m_KeyValue[IDC_G] = 0x0a;
    m_KeyValue[IDC_H] = 0x0b;
    m_KeyValue[IDC_I] = 0x0c;
    m_KeyValue[IDC_J] = 0x0d;
    m_KeyValue[IDC_K] = 0x0e;
    m_KeyValue[IDC_L] = 0x0f;
    m_KeyValue[IDC_M] = 0x10;
    m_KeyValue[IDC_N] = 0x11;
    m_KeyValue[IDC_O] = 0x12;
    m_KeyValue[IDC_P] = 0x13;
    m_KeyValue[IDC_Q] = 0x14;
    m_KeyValue[IDC_R] = 0x15;
    m_KeyValue[IDC_S] = 0x16;
    m_KeyValue[IDC_T] = 0x17;
    m_KeyValue[IDC_U] = 0x18;
    m_KeyValue[IDC_V] = 0x19;
    m_KeyValue[IDC_W] = 0x1a;
    m_KeyValue[IDC_X] = 0x1b;
    m_KeyValue[IDC_Y] = 0x1c;
    m_KeyValue[IDC_Z] = 0x1d;
    m_KeyValue[IDC_WIN_LEFT] = (0x01) << 3;
    m_KeyValue[IDC_WIN_RIGHT] = (char) ((0x01) << 7);
	
    m_KeyValue[IDC_MENU] = 0x65;
	
#ifdef REPLACE_RIGHT_NUMBER
    m_KeyValue[IDC_PAD0] = 0x27;        
    m_KeyValue[IDC_PAD1] = 0x1e;        
    m_KeyValue[IDC_PAD2] = 0x1f;       
    m_KeyValue[IDC_PAD3] = 0x20;        
    m_KeyValue[IDC_PAD4] = 0x21;           
    m_KeyValue[IDC_PAD5] = 0x22;      
    m_KeyValue[IDC_PAD6] = 0x23; 
    m_KeyValue[IDC_PAD7] = 0x24;
    m_KeyValue[IDC_PAD8] = 0x25;
    m_KeyValue[IDC_PAD9] = 0x26;
    m_KeyValue[IDC_DOT_RIGHT] = 0x37;
#else
    m_KeyValue[IDC_PAD0] = 0x62;
    m_KeyValue[IDC_PAD1] = 0x59;
    m_KeyValue[IDC_PAD2] = 0x5a;
    m_KeyValue[IDC_PAD3] = 0x5b;
    m_KeyValue[IDC_PAD4] = 0x5c;
    m_KeyValue[IDC_PAD5] = 0x5d;
    m_KeyValue[IDC_PAD6] = 0x5e;
    m_KeyValue[IDC_PAD7] = 0x5f;
    m_KeyValue[IDC_PAD8] = 0x60;
    m_KeyValue[IDC_PAD9] = 0x61;
    m_KeyValue[IDC_DOT_RIGHT] = 0x63;
#endif
	
	m_KeyValue[IDC_EQUAL_RIGHT] = 0x67;
	m_KeyValue[IDC_COMMA_RIGHT] = (char)0x85;
	
    m_KeyValue[IDC_MUL] = 0x55;
    m_KeyValue[IDC_PLUS] = 0x57;
    m_KeyValue[IDC_MINUS] = 0x56;    
    m_KeyValue[IDC_DIV] = 0x54;
    m_KeyValue[IDC_F1] = 0x3a;
    m_KeyValue[IDC_F2] = 0x3b;
    m_KeyValue[IDC_F3] = 0x3c;
    m_KeyValue[IDC_F4] = 0x3d;
    m_KeyValue[IDC_F5] = 0x3e;
    m_KeyValue[IDC_F6] = 0x3f;
    m_KeyValue[IDC_F7] = 0x40;
    m_KeyValue[IDC_F8] = 0x41;
    m_KeyValue[IDC_F9] = 0x42;
    m_KeyValue[IDC_F10] = 0x43;
    m_KeyValue[IDC_F11] = 0x44;
    m_KeyValue[IDC_F12] = 0x45;
    m_KeyValue[IDC_F13] = 0x68;
    m_KeyValue[IDC_F14] = 0x69;
    m_KeyValue[IDC_F15] = 0x6A;
    m_KeyValue[IDC_NUMLOCK] = 0x53;
    m_KeyValue[IDC_SEMICOLON] = 0x33;
    m_KeyValue[IDC_EQUAL] = 0x2E;
    m_KeyValue[IDC_COMMA] = 0x36;
    m_KeyValue[IDC_DASH] = 0x2d;
    m_KeyValue[IDC_DOT] = 0x37;
    m_KeyValue[IDC_SLASH] = 0x38;
    m_KeyValue[IDC_SINGLE_QUOTE] = 0x35;
    m_KeyValue[IDC_OPEN] = 0x2f;
    m_KeyValue[IDC_BACKSLASH] = 0x31;
    m_KeyValue[IDC_CLOSE] = 0x30;
    m_KeyValue[IDC_APOSTROPHE] = 0x34;
    m_KeyValue[IDC_ALT_RIGHT] = (0x01) << 6;
    m_KeyValue[IDC_ENTER_RIGHT] = 0x58;    
    m_KeyValue[IDC_SHIFT_RIGHT] = (0x01) << 5;
    m_KeyValue[IDC_CTRL_RIGHT] = (0x01) << 4;
	
    // JIS
    m_KeyValue[IDC_BACKSLASH_JIS] = (char) 0x87;
    m_KeyValue[IDC_EISU_JIS] = (char) 0x8B;
    m_KeyValue[IDC_YEN_JIS] = (char) 0x89;
    m_KeyValue[IDC_KANA_JIS] = (char) 0x8A;
    m_KeyValue[IDC_BACKUP_JIS] = (char) 0x88;
}

LRESULT
CIVTKeyboardDlg::DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam) {
    if (bIsDlgOpened) {
        return CDialog::DefWindowProc(message, wParam, lParam);
    }
	
    if (message == WM_INPUT) {
        unsigned int r_value;
        unsigned int buf_size;
        unsigned char * pBuffer;
        RAWINPUT*pRawInput;
		
        buf_size = 0;
        r_value = GetRawInputData((HRAWINPUT) lParam, (UINT) RID_INPUT,
			(LPVOID) NULL, (PUINT) & buf_size,
			(UINT)sizeof(RAWINPUTHEADER));
		
        if (r_value != 0) {
            return CDialog::DefWindowProc(message, wParam, lParam);
        }
		
        pBuffer = (unsigned char *) malloc(buf_size);
        if (!pBuffer) {
            return CDialog::DefWindowProc(message, wParam, lParam);
        }
		
        GetRawInputData((HRAWINPUT) lParam, (UINT) RID_INPUT,
			(LPVOID) pBuffer, (PUINT) & buf_size,
			(UINT)sizeof(RAWINPUTHEADER));
		
        pRawInput = (RAWINPUT *) pBuffer;
		
        // Mouse Event /////////////////////////////////////////////////////////////////
        if (pRawInput->header.dwType == RIM_TYPEMOUSE) {
            CString     strTemp;
            strTemp.Format(TEXT("usFlags=%02X lLastX=%08X lLastY=%08X usButtonFlags=%04X usButtonData=%04X\n"),
				pRawInput->data.mouse.usFlags,
				pRawInput->data.mouse.lLastX,
				pRawInput->data.mouse.lLastY,
				pRawInput->data.mouse.usButtonFlags,
				pRawInput->data.mouse.usButtonData);
            //OutputDebugString(strTemp);       
			
            char        mouse_button;
            char        x_delta, y_delta, z_delta;                      
			
            send_immediately = FALSE;
			
            if (pRawInput->data.mouse.usButtonFlags & RI_MOUSE_LEFT_BUTTON_DOWN) {
                if (!m_IsMouseButtonPressed[0]) {
                    send_immediately = TRUE;
                }
                m_IsMouseButtonPressed[0] = TRUE;
            }
            if (pRawInput->data.mouse.usButtonFlags & RI_MOUSE_LEFT_BUTTON_UP) {
                if (m_IsMouseButtonPressed[0]) {
                    send_immediately = TRUE;
                }
                m_IsMouseButtonPressed[0] = FALSE;
            }
            if (pRawInput->data.mouse.usButtonFlags & RI_MOUSE_RIGHT_BUTTON_DOWN) {
                if (!m_IsMouseButtonPressed[1]) {
                    send_immediately = TRUE;
                }
                m_IsMouseButtonPressed[1] = TRUE;
            }
            if (pRawInput->data.mouse.usButtonFlags & RI_MOUSE_RIGHT_BUTTON_UP) {
                if (m_IsMouseButtonPressed[1]) {
                    send_immediately = TRUE;
                }
                m_IsMouseButtonPressed[1] = FALSE;
            }
            if (pRawInput->data.mouse.usButtonFlags & RI_MOUSE_MIDDLE_BUTTON_DOWN) {
                if (!m_IsMouseButtonPressed[2]) {
                    send_immediately = TRUE;
                }
                m_IsMouseButtonPressed[2] = TRUE;
            }
            if (pRawInput->data.mouse.usButtonFlags & RI_MOUSE_MIDDLE_BUTTON_UP) {
                if (m_IsMouseButtonPressed[2]) {
                    send_immediately = TRUE;
                }
                m_IsMouseButtonPressed[2] = FALSE;
            }
			
            mouse_button = 0x00;
            if (m_IsMouseButtonPressed[0]) {
                mouse_button |= 0x01;
            }
            if (m_IsMouseButtonPressed[1]) {
                mouse_button |= (0x01) << 1;
            }
            if (m_IsMouseButtonPressed[2]) {
                mouse_button |= (0x01) << 2;
            }
			
            x_delta = (char) pRawInput->data.mouse.lLastX;
            y_delta = (char) pRawInput->data.mouse.lLastY;
			
            z_delta = 0x00;
			
            if (pRawInput->data.mouse.usButtonFlags == RI_MOUSE_WHEEL) {
                if ((short) pRawInput->data.mouse.usButtonData > 0) {
                    z_delta = (char) 0x01;
                } else {
                    z_delta = (char) 0xFF;
                }
            }
			
            if (bIsRXConnected) {
                int     x_temp;
                int     y_temp;
				
                x_temp = x_left + x_delta;
                if (x_temp > 80) {
                    x_temp = 80;
                } else if (x_temp < -80) {
                    x_temp = -80;
                } 
                x_left = x_temp;
				
                y_temp = y_left + y_delta;
                if (y_temp > 80) {
                    y_temp = 80;
                } else if (y_temp < -80) {
                    y_temp = -80;
                }
                y_left = y_temp;                            
				
                z_left = z_delta;
                button_left = mouse_button;
            }
        } 
		
        // Keyboard Event /////////////////////////////////////////////////////////////////
        if (pRawInput->header.dwType == RIM_TYPEKEYBOARD) {
            CString     strTemp;
            strTemp.Format(TEXT("MakeCode=%02X Flags=%02X VKey=%02X Message=%04X\n"),
				pRawInput->data.keyboard.MakeCode,
				pRawInput->data.keyboard.Flags,
				pRawInput->data.keyboard.VKey,
				pRawInput->data.keyboard.Message);
            //OutputDebugString(strTemp);
			
            m_IsMediaKeyChanged = FALSE;
			
            int         k_value;
            BOOL        bPressed;
			
            k_value = 0x00;
            bPressed = FALSE;
			
            if ((pRawInput->data.keyboard.Flags & 0x01) == 0x00) {
                bPressed = TRUE;
            } else {
                bPressed = FALSE;
            }
			
            if (pRawInput->data.keyboard.VKey == IDC_ALT_LEFT) {
                if (pRawInput->data.keyboard.Flags & 0x02) {
                    k_value = IDC_ALT_RIGHT;
                } else {
                    k_value = IDC_ALT_LEFT;
                }
            } else if (pRawInput->data.keyboard.VKey == IDC_CTRL_LEFT) {
                if (pRawInput->data.keyboard.Flags & 0x02) {
                    k_value = IDC_CTRL_RIGHT;
                } else {
                    k_value = IDC_CTRL_LEFT;
                }
            } else if (pRawInput->data.keyboard.VKey == IDC_SHIFT_LEFT) {
                if (pRawInput->data.keyboard.MakeCode == 0x36) {
                    k_value = IDC_SHIFT_RIGHT;
                } else {
                    k_value = IDC_SHIFT_LEFT;
                }
            } else if (pRawInput->data.keyboard.VKey == IDC_ENTER) {
                if (pRawInput->data.keyboard.Flags & 0x02) {
                    k_value = IDC_ENTER_RIGHT;
                } else {
                    k_value = IDC_ENTER;
                }
            } else if (pRawInput->data.keyboard.VKey == 0x1D) {
                k_value = IDC_EISU_JIS;
            } else if (pRawInput->data.keyboard.VKey == 0x1C) {
                k_value = IDC_KANA_JIS;
            } else if (pRawInput->data.keyboard.VKey == 0xC0) {
                if (pRawInput->data.keyboard.MakeCode == 0x29) {
                    // english
                    k_value = IDC_SINGLE_QUOTE;
                } else {
                    // japanese
                    k_value = IDC_OPEN;
                }
            } else if (pRawInput->data.keyboard.VKey == 0xBA) {
                if (pRawInput->data.keyboard.MakeCode == 0x27) {
                    // english
                    k_value = IDC_SEMICOLON;
                } else {
                    // japanese
                    k_value = IDC_APOSTROPHE;
                }
            } else if (pRawInput->data.keyboard.VKey == 0xBB) {
                if (pRawInput->data.keyboard.MakeCode == 0x0D) {
                    // english
                    k_value = IDC_EQUAL;
                } else {
                    // japanese
                    k_value = IDC_SEMICOLON;
                }
            } else if (pRawInput->data.keyboard.VKey == 0xDB) {
                if (pRawInput->data.keyboard.MakeCode == 0x1A) {
                    // english
                    k_value = IDC_OPEN;
                } else {
                    // japanese
                    k_value = IDC_CLOSE;
                }
            } else if (pRawInput->data.keyboard.VKey == 0xDC) {
                if (pRawInput->data.keyboard.MakeCode == 0x2B) {
                    // english
                    k_value = IDC_BACKSLASH;
                } else {
                    // japanese
                    k_value = IDC_YEN_JIS;
                }
            } else if (pRawInput->data.keyboard.VKey == 0xDD) {
                if (pRawInput->data.keyboard.MakeCode == 0x1B) {
                    // english
                    k_value = IDC_CLOSE;
                } else {
                    // japanese
                    k_value = IDC_BACKSLASH;
                }
            } else if (pRawInput->data.keyboard.VKey == 0xDE) {
                if (pRawInput->data.keyboard.MakeCode == 0x28) {
                    // english
                    k_value = IDC_APOSTROPHE;
                } else {
                    // japanese
                    k_value = IDC_EQUAL;
                }
            } else if (pRawInput->data.keyboard.VKey == 0xE2) {
                if (pRawInput->data.keyboard.MakeCode == 0x56) {
                    k_value = IDC_MACRO;
                } else {
                    k_value = IDC_BACKSLASH_JIS;
                }
            } else if (pRawInput->data.keyboard.VKey == 0xF0) {
                if (pRawInput->data.keyboard.Message == 0x0100) {
                    k_value = IDC_CAPS;
					
					if (bIsForeground &&
						(!bIsDlgOpened) &&
						(GetDlgItem(k_value))) {
						
						m_IsButtonPressed[k_value] = TRUE;                               
						vScanAndSentKey();                          
						if (GetDlgItem(k_value)) {
							GetDlgItem(k_value)->InvalidateRect(NULL, FALSE);
							GetDlgItem(k_value)->UpdateWindow();
						}
						
						Sleep(150);
						
						m_IsButtonPressed[k_value] = FALSE;                              
						vScanAndSentKey();                          
						if (GetDlgItem(k_value)) {
							GetDlgItem(k_value)->InvalidateRect(NULL, FALSE);
							GetDlgItem(k_value)->UpdateWindow();
						}
					}
					
                    free(pBuffer);
                    return TRUE;
                }
            } else if (pRawInput->data.keyboard.VKey == 0xF2) {
                if (pRawInput->data.keyboard.Message == 0x0100) {
                    k_value = IDC_BACKUP_JIS;
					
					if (bIsForeground &&
						(!bIsDlgOpened) &&
						(GetDlgItem(k_value))) {
						
						m_IsButtonPressed[k_value] = TRUE;                               
						vScanAndSentKey();                          
						if (GetDlgItem(k_value)) {
							GetDlgItem(k_value)->InvalidateRect(NULL, FALSE);
							GetDlgItem(k_value)->UpdateWindow();
						}
						
						Sleep(150);
						
						m_IsButtonPressed[k_value] = FALSE;                              
						vScanAndSentKey();                          
						if (GetDlgItem(k_value)) {
							GetDlgItem(k_value)->InvalidateRect(NULL, FALSE);
							GetDlgItem(k_value)->UpdateWindow();
						}
					}
					
                    free(pBuffer);
                    return TRUE;
                }
            } else if (pRawInput->data.keyboard.VKey == 0xF3) {
                if (pRawInput->data.keyboard.Message == 0x0100) {
					
					if(!bPassF4){						
						k_value = IDC_SINGLE_QUOTE;
						
						if (bIsForeground &&
							(!bIsDlgOpened) &&
							(GetDlgItem(k_value))) {
							
							m_IsButtonPressed[k_value] = TRUE;                               
							vScanAndSentKey();                          
							if (GetDlgItem(k_value)) {
								GetDlgItem(k_value)->InvalidateRect(NULL, FALSE);
								GetDlgItem(k_value)->UpdateWindow();
							}
							
							Sleep(150);
							
							m_IsButtonPressed[k_value] = FALSE;                              
							vScanAndSentKey();                          
							if (GetDlgItem(k_value)) {
								GetDlgItem(k_value)->InvalidateRect(NULL, FALSE);
								GetDlgItem(k_value)->UpdateWindow();
							}
							
						}
					}
					bPassF4=FALSE;
                    free(pBuffer);
                    return TRUE;
                }
            } else if (pRawInput->data.keyboard.VKey == 0xF4) {
                if (pRawInput->data.keyboard.Message == 0x0100) {

					k_value = IDC_SINGLE_QUOTE;

					if (bIsForeground &&
						(!bIsDlgOpened) &&
						(GetDlgItem(k_value))) {
						
						m_IsButtonPressed[k_value] = TRUE;                               
						vScanAndSentKey();                          
						if (GetDlgItem(k_value)) {
							GetDlgItem(k_value)->InvalidateRect(NULL, FALSE);
							GetDlgItem(k_value)->UpdateWindow();
						}
						
						Sleep(150);
						
						m_IsButtonPressed[k_value] = FALSE;                              
						vScanAndSentKey();                          
						if (GetDlgItem(k_value)) {
							GetDlgItem(k_value)->InvalidateRect(NULL, FALSE);
							GetDlgItem(k_value)->UpdateWindow();
						}
					}
                    free(pBuffer);
                    return TRUE;
                }
            } else if (pRawInput->data.keyboard.VKey == 0xFF) {
                // Maybe JIS Keyboard
                if (pRawInput->data.keyboard.MakeCode == 0x7D) {
                    // YEN
                    k_value = IDC_YEN_JIS;
                } else if (pRawInput->data.keyboard.MakeCode == 0x79) {
                    // KANA
                    k_value = IDC_KANA_JIS;
                } else if (pRawInput->data.keyboard.MakeCode == 0x70) {
                    // �Ը�
                    k_value = IDC_BACKUP_JIS;
                }
            } else {
                k_value = pRawInput->data.keyboard.VKey;
            }
			
            if ((theApp.keyboard_style == STYLE_IPAD) ||
                ((theApp.keyboard_style == STYLE_ANDROID))) {
                if (k_value == IDC_ESC) {
                    m_IsMediaKeyChanged = TRUE;
                }
				
                int     i;
                for (i = 0; i < 12; i++) {
                    if (k_value == IDC_F1 + i) {
                        m_IsMediaKeyChanged = TRUE;
                    }
					
                    if (m_IsMediaKeyChanged) {
                        break;
                    }
                }
            }
			
            BOOL        bToggle = FALSE;
			
            if (k_value < IDC_MOUSE) {
                if (bPressed) {
                    if (!m_IsButtonPressed[k_value]) {
                        bToggle = TRUE;
                    }
                    m_IsButtonPressed[k_value] = TRUE;
                } else {
                    if (m_IsButtonPressed[k_value]) {
                        bToggle = TRUE;
                    }
                    m_IsButtonPressed[k_value] = FALSE;
                }
            }
			
            // if user press shift+esc, release mouse
            if (g_StopMouse &&
                bPressed &&
                bToggle &&
                (k_value == IDC_ESC) &&
                m_IsButtonPressed[IDC_SHIFT_LEFT]) {
                bToggle = FALSE;
                g_StopMouse = FALSE;
				
                pHintDialog->ShowWindow(SW_HIDE);
                SetForegroundWindow();               
				
                InvalidateRect(NULL, TRUE);
                UpdateWindow();
            }
			
            if (bIsForeground &&
                (!bIsDlgOpened) &&
                (k_value < MAX_KEY_VALUE) &&
                (GetDlgItem(k_value))) {
                if (bToggle) {
                    vScanAndSentKey();
                }
				
                GetDlgItem(k_value)->InvalidateRect(NULL, FALSE);
                GetDlgItem(k_value)->UpdateWindow();
				
                GetDlgItem(IDC_STATIC_HINT)->SetFocus();
            }
        }
		
        free(pBuffer);
		
        return TRUE;
    } else {
        return CDialog::DefWindowProc(message, wParam, lParam);
    }
}


HCURSOR
CIVTKeyboardDlg::OnQueryDragIcon() {
    return (HCURSOR) m_hIcon;
}

void
CIVTKeyboardDlg::OnPaint() {
    if (IsIconic()) {
        CPaintDC        dc      (this); // device context for painting
		
        SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);
		
        // Center icon in client rectangle
        int     cxIcon  = GetSystemMetrics(SM_CXICON);
        int     cyIcon  = GetSystemMetrics(SM_CYICON);
        CRect   rect;
        GetClientRect(&rect);
        int     x       = (rect.Width() - cxIcon + 1) / 2;
        int     y       = (rect.Height() - cyIcon + 1) / 2;
		
        // Draw the icon
        dc.DrawIcon(x, y, m_hIcon);
    } else {
        CDialog::OnPaint();
    }
}



void
CIVTKeyboardDlg::OnTimer(UINT nIDEvent) {
    if (nIDEvent == IDT_CHECK_CONNECTION) {
        KillTimer(nIDEvent);   
        vTestDevice();
        SetTimer(IDT_CHECK_CONNECTION, 500, 0);		
    } else if (nIDEvent == IDT_CHECK_MOUSE) {
        if (theApp.keyboard_style != STYLE_IPAD) {
            if (g_StopMouse) {
                if ((bIsRXConnected) && (!bIsDlgOpened)) {
                    if ((x_left != 0) ||
                        (y_left != 0) ||
                        (z_left != 0) ||
                        (button_left != 0) ||
                        send_immediately) {
                        BOOL    r_value;
						
                        if (theApp.keyboard_style != STYLE_IPAD) {
                            r_value = bSendMouse(x_left, y_left, z_left,
								button_left);
                            if (!r_value) {
                                //Sleep(50);
                            }
                        }
						
                        x_left = 0;
                        y_left = 0;
                        z_left = 0;
                        button_left = 0;
                        send_immediately = FALSE;
                    }
                }
            }
        }
		
		//bPassF4				
		BYTE        keyState[256];
		
		GetKeyboardState((LPBYTE) & keyState);
		
		if (keyState[0xF4]) {
			//SetWindowText(TEXT("On"));
			
			if(bIsForeground){
				
				bPassF4 = TRUE;
				
				// Simulate a key press
				keybd_event(0xF3, 0x00, KEYEVENTF_EXTENDEDKEY | 0, 0);
				
				// Simulate a key release
				keybd_event(0xF3, 0x00,
					KEYEVENTF_EXTENDEDKEY | KEYEVENTF_KEYUP, 0);
				
			}
		} 
		
    } else if (nIDEvent == IDT_CLOSE_TITLE) {
        KillTimer(nIDEvent);
        pTitleDialog->vSetTitle(TEXT(""));
        pTitleDialog->ShowWindow(SW_HIDE);
    }
}


LONG
CIVTKeyboardDlg::OnTrayNotify(WPARAM wParam, LPARAM lParam) {
    if (bIsDlgOpened) {
        return TRUE;
    }
	
    switch (lParam) {
	case WM_RBUTTONDOWN:
		{
            bIsMenuOpened = TRUE;
			
            unsigned int mode;
			
            int         i;                       
            BOOL        r_value;
            int         iNumOfRx        = 0;
            char        buf[512];
            char        buf2[512];
            WCHAR       hostname_wc[MAX_PATH];
            char        connected_address[MAX_PATH];
			
            BOOL        bIsBTHardware;
			
            BOOL        bIsInitializing;
            BOOL        bIsInitialized;
            BOOL        bIsDiscoverable;
            BOOL        bIsConnecting;
            BOOL        bIsConnected;
            BOOL        bIsPinRequest;
			
            iConnectedHost = -1;
            bIsBTHardware = bIsBTHarewareExist();                        
			
            if (bIsBTHardware) {
                r_value = bBTGetStatus(&bIsInitializing, &bIsInitialized,
					&bIsDiscoverable, &bIsConnecting,
					&bIsConnected, &bIsPinRequest); 
				
                if (r_value) {
                    memset(buf, 0x00, 512);
                    r_value = bBTGetPaired(0xFF, buf, buf2);
                    if (r_value) {
                        iNumOfRx = buf[0];
                    }
					
                    if (bIsConnected) {
                        bBTGetHostAddress(connected_address);
                    }
                }
            }
			
            CMenu       mainmenu;
            mainmenu.CreatePopupMenu(); 
			
            // About 
            theApp.m_Msg.LoadString(IDS_ABOUT);
            mainmenu.AppendMenu(MF_STRING, IDM_ABOUT, theApp.m_Msg);
			
            if (theApp.m_aplang == LANG_JP) {
                // Keyboard Layout
                CMenu   layout_menu;
                layout_menu.CreateMenu();
				
                if (theApp.keyboard_layout == LAYOUT_ANSI) {
                    mode = MF_CHECKED | MF_STRING;
                } else {
                    mode = MF_STRING;
                }
                theApp.m_Msg.LoadString(IDS_LAYOUT_ENG);
                layout_menu.AppendMenu(mode, IDM_LAYOUT_ENG, theApp.m_Msg);
				
                if (theApp.keyboard_layout == LAYOUT_JIS) {
                    mode = MF_CHECKED | MF_STRING;
                } else {
                    mode = MF_STRING;
                }
                theApp.m_Msg.LoadString(IDS_LAYOUT_JIS);
                layout_menu.AppendMenu(mode, IDM_LAYOUT_JIS, theApp.m_Msg);
				
                theApp.m_Msg.LoadString(IDS_LAYOUT);
                mainmenu.AppendMenu(MF_STRING | MF_POPUP,
					(UINT) layout_menu.m_hMenu, theApp.m_Msg);
            } 
			
            // Keyboard Style
            CMenu       style_menu;
            style_menu.CreateMenu();
			
            if (theApp.keyboard_style == STYLE_IPAD) {
                mode = MF_CHECKED | MF_STRING;
            } else {
                mode = MF_STRING;
            }
            theApp.m_Msg.LoadString(IDS_IPAD);
            style_menu.AppendMenu(mode, IDM_STYLE_IPAD, theApp.m_Msg);
			
            if (theApp.keyboard_style == STYLE_MAC) {
                mode = MF_CHECKED | MF_STRING;
            } else {
                mode = MF_STRING;
            }
            theApp.m_Msg.LoadString(IDS_MAC);
            style_menu.AppendMenu(mode, IDM_STYLE_MAC, theApp.m_Msg);
			
            if (theApp.keyboard_style == STYLE_WINDOWS) {
                mode = MF_CHECKED | MF_STRING;
            } else {
                mode = MF_STRING;
            }
            theApp.m_Msg.LoadString(IDS_WINDOWS);
            style_menu.AppendMenu(mode, IDM_STYLE_WINDOWS, theApp.m_Msg);
			
            if (theApp.keyboard_style == STYLE_ANDROID) {
                mode = MF_CHECKED | MF_STRING;
            } else {
                mode = MF_STRING;
            }
            theApp.m_Msg.LoadString(IDS_ANDROID);
            style_menu.AppendMenu(mode, IDM_STYLE_ANDROID, theApp.m_Msg);
			
            theApp.m_Msg.LoadString(IDS_STYLE);
            mainmenu.AppendMenu(MF_STRING | MF_POPUP,
				(UINT) style_menu.m_hMenu, theApp.m_Msg);
			
			
            if (bIsBTHardware && bIsBTInitialized) {
                // Bluetooth
                CMenu   bt_menu;
                bt_menu.CreateMenu();                       
				
                mode = MF_STRING;
				
                theApp.m_Msg.LoadString(IDS_DISCOVERABLE);
                bt_menu.AppendMenu(mode, IDM_BT_DISCOVERABLE, theApp.m_Msg);            
				
                // Bluetooth->Disconnect
                if (bIsConnected) {
                    memset(buf2, 0x00, sizeof(buf2));
                    r_value = bBTGetHostName(buf2);
                    if (r_value) {
                        if (buf2[24] != 0) {
                            buf2[25] = '.';
                            buf2[26] = '.';
                            buf2[27] = '.';
                            buf2[28] = 0x00;
                        }
						
                        CString temp;
						
                        MultiByteToWideChar(CP_UTF8, 0, buf2, 34, hostname_wc,
							MAX_PATH);
						
                        theApp.m_Msg = hostname_wc;
                        theApp.m_Msg += TEXT(" ");
                        temp.LoadString(IDS_DISCONNECT_THIS);
						
                        theApp.m_Msg += temp;
                    } else {
                        theApp.m_Msg.LoadString(IDS_DISCONNECT_THIS);
                    }
					
                    mode = MF_STRING;
                } else {
                    theApp.m_Msg.LoadString(IDS_DISCONNECT_THIS2);
                    mode = MF_STRING | MF_DISABLED | MF_GRAYED;
                }
				
                bt_menu.AppendMenu(mode, IDM_BT_DISCONNECT, theApp.m_Msg);
				
                bt_menu.AppendMenu(MF_SEPARATOR, 0, TEXT(""));
				
                for (i = 0; i < iNumOfRx; i++) {
                    // Bluetooth->Devices Action
                    CMenu       bt_device_menu;
                    bt_device_menu.CreateMenu();
					
                    memset(buf, 0x00, 512);
                    memset(buf2, 0x00, 512);
                    r_value = bBTGetPaired(i, buf, buf2);
					
                    if (r_value) {
                        if (bIsConnected) {
                            if (memcmp(connected_address, buf, 17) == 0) {
                                iConnectedHost = i;
                            }
                        }
						
                        if (buf2[24] != 0) {
                            buf2[25] = '.';
                            buf2[26] = '.';
                            buf2[27] = '.';
                            buf2[28] = 0x00;
                        }
						
                        MultiByteToWideChar(CP_UTF8, 0, buf2, 34, hostname_wc,
							MAX_PATH);                                                       
						
                        theApp.m_Msg.LoadString(IDS_SELECT);
                        bt_device_menu.AppendMenu(MF_STRING,
							IDM_BT_CONNECT + i,
							theApp.m_Msg);
						
                        theApp.m_Msg.LoadString(IDS_DELETE_THIS);       
                        bt_device_menu.AppendMenu(MF_STRING,
							IDM_BT_DELETE + i,
							theApp.m_Msg);
						
                        bt_menu.AppendMenu(MF_STRING | MF_POPUP,
							(UINT) bt_device_menu.m_hMenu,
							(WCHAR *) hostname_wc);
                    }
                }                                       
				
                bt_menu.AppendMenu(MF_SEPARATOR, 0, TEXT(""));
				
                // Bluetooth->DeleteAllDevice
                if (iNumOfRx > 0) {
                    mode = MF_STRING;
                } else {
                    mode = MF_STRING | MF_DISABLED | MF_GRAYED;
                }
				
                theApp.m_Msg.LoadString(IDS_DELETE_ALL);
                bt_menu.AppendMenu(mode, IDM_BT_DELETE_ALL, theApp.m_Msg);
				
                mainmenu.AppendMenu(MF_STRING | MF_POPUP,
					(UINT) bt_menu.m_hMenu, TEXT("Bluetooth"));
            } else {
                return TRUE;
            }                   
			
            theApp.m_Msg.LoadString(IDS_CLOSE);
            mainmenu.AppendMenu(MF_STRING, IDM_LEAVE, theApp.m_Msg);
			
            POINT       pt ;
            GetCursorPos(&pt);
            SetForegroundWindow();
			
            mainmenu.TrackPopupMenu(TPM_LEFTALIGN | TPM_LEFTBUTTON, pt.x,
				pt.y, this);         
			
            break ;
        }
		
      case WM_LBUTTONDOWN:
		  {
			  this->ShowWindow(SW_SHOW);
			  this->ShowWindow(SW_RESTORE);
			  SetForegroundWindow();
		  }
		  break;
    }
    return (0) ;
}

void
CIVTKeyboardDlg::vCreateMenu() {
    CMenu       *mainmenu;
	
    mainmenu = this->GetMenu();
    if (!mainmenu) {
        return;
    }
	
    unsigned int i;
    unsigned int cnt;
	
    cnt = mainmenu->GetMenuItemCount();
	
    // Delete Old items
    for (i = 0; i < cnt; i++) {
        mainmenu->DeleteMenu(cnt - i - 1, MF_BYPOSITION);
    }
	
    unsigned int mode;
	
    BOOL        r_value;
    unsigned int         iNumOfRx = 0;
    char        buf[512];
    char        buf2[512];
    WCHAR       hostname_wc[MAX_PATH];
    char        connected_address[MAX_PATH];
	
    BOOL        bIsBTHardware;
	
    BOOL        bIsInitializing;
    BOOL        bIsInitialized;
    BOOL        bIsDiscoverable;
    BOOL        bIsConnecting;
    BOOL        bIsConnected;
    BOOL        bIsPinRequest;
	
    iConnectedHost = -1;
    bIsBTHardware = bIsBTHarewareExist();                        
	
    if (bIsBTHardware) {
        r_value = bBTGetStatus(&bIsInitializing, &bIsInitialized,
			&bIsDiscoverable, &bIsConnecting,
			&bIsConnected, &bIsPinRequest); 
		
        if (r_value) {
            memset(buf, 0x00, 512);
            r_value = bBTGetPaired(0xFF, buf, buf2);
            if (r_value) {
                iNumOfRx = buf[0];
            }
			
            if (bIsConnected) {
                bBTGetHostAddress(connected_address);
            }
        }
    }
	
    // Quit
    //theApp.m_Msg.LoadString(IDS_CLOSE);
    //mainmenu->AppendMenu(MF_STRING, IDM_LEAVE, theApp.m_Msg);
	
    if (theApp.m_aplang == LANG_JP) {
        // Keyboard Menu
        CMenu   keyboard_menu;
        keyboard_menu.CreateMenu();
		
        // Keyboard Style
        CMenu   style_menu;
        style_menu.CreateMenu();
		
        if (theApp.keyboard_style == STYLE_IPAD) {
            mode = MF_CHECKED | MF_STRING;
        } else {
            mode = MF_STRING;
        }
        theApp.m_Msg.LoadString(IDS_IPAD);
        style_menu.AppendMenu(mode, IDM_STYLE_IPAD, theApp.m_Msg);
		
        if (theApp.keyboard_style == STYLE_MAC) {
            mode = MF_CHECKED | MF_STRING;
        } else {
            mode = MF_STRING;
        }
        theApp.m_Msg.LoadString(IDS_MAC);
        style_menu.AppendMenu(mode, IDM_STYLE_MAC, theApp.m_Msg);
		
        if (theApp.keyboard_style == STYLE_WINDOWS) {
            mode = MF_CHECKED | MF_STRING;
        } else {
            mode = MF_STRING;
        }
        theApp.m_Msg.LoadString(IDS_WINDOWS);
        style_menu.AppendMenu(mode, IDM_STYLE_WINDOWS, theApp.m_Msg);
		
        if (theApp.keyboard_style == STYLE_ANDROID) {
            mode = MF_CHECKED | MF_STRING;
        } else {
            mode = MF_STRING;
        }
        theApp.m_Msg.LoadString(IDS_ANDROID);
        style_menu.AppendMenu(mode, IDM_STYLE_ANDROID, theApp.m_Msg);
		
        theApp.m_Msg.LoadString(IDS_STYLE);
        keyboard_menu.AppendMenu(MF_STRING | MF_POPUP,
			(UINT) style_menu.m_hMenu, theApp.m_Msg);
		
        // Keyboard Layout
        CMenu   layout_menu;
        layout_menu.CreateMenu();
		
        if (theApp.keyboard_layout == LAYOUT_ANSI) {
            mode = MF_CHECKED | MF_STRING;
        } else {
            mode = MF_STRING;
        }
        theApp.m_Msg.LoadString(IDS_LAYOUT_ENG);
        layout_menu.AppendMenu(mode, IDM_LAYOUT_ENG, theApp.m_Msg);
		
        if (theApp.keyboard_layout == LAYOUT_JIS) {
            mode = MF_CHECKED | MF_STRING;
        } else {
            mode = MF_STRING;
        }
        theApp.m_Msg.LoadString(IDS_LAYOUT_JIS);
        layout_menu.AppendMenu(mode, IDM_LAYOUT_JIS, theApp.m_Msg);
		
        theApp.m_Msg.LoadString(IDS_LAYOUT);
        keyboard_menu.AppendMenu(MF_STRING | MF_POPUP,
			(UINT) layout_menu.m_hMenu, theApp.m_Msg);
		
        theApp.m_Msg.LoadString(IDS_KEYBOARD);
        mainmenu->AppendMenu(MF_STRING | MF_POPUP,
			(UINT) keyboard_menu.m_hMenu, theApp.m_Msg);
    } else {
        // Keyboard Style
        CMenu   style_menu;
        style_menu.CreateMenu();
		
        if (theApp.keyboard_style == STYLE_IPAD) {
            mode = MF_CHECKED | MF_STRING;
        } else {
            mode = MF_STRING;
        }
        theApp.m_Msg.LoadString(IDS_IPAD);
        style_menu.AppendMenu(mode, IDM_STYLE_IPAD, theApp.m_Msg);
		
        if (theApp.keyboard_style == STYLE_MAC) {
            mode = MF_CHECKED | MF_STRING;
        } else {
            mode = MF_STRING;
        }
        theApp.m_Msg.LoadString(IDS_MAC);
        style_menu.AppendMenu(mode, IDM_STYLE_MAC, theApp.m_Msg);
		
        if (theApp.keyboard_style == STYLE_WINDOWS) {
            mode = MF_CHECKED | MF_STRING;
        } else {
            mode = MF_STRING;
        }
        theApp.m_Msg.LoadString(IDS_WINDOWS);
        style_menu.AppendMenu(mode, IDM_STYLE_WINDOWS, theApp.m_Msg);
		
        if (theApp.keyboard_style == STYLE_ANDROID) {
            mode = MF_CHECKED | MF_STRING;
        } else {
            mode = MF_STRING;
        }
        theApp.m_Msg.LoadString(IDS_ANDROID);
        style_menu.AppendMenu(mode, IDM_STYLE_ANDROID, theApp.m_Msg);
		
        theApp.m_Msg.LoadString(IDS_STYLE);
        mainmenu->AppendMenu(MF_STRING | MF_POPUP, (UINT) style_menu.m_hMenu,
			theApp.m_Msg);
    }
	
    if (bIsBTHardware && bIsBTInitialized) {
        // Bluetooth
        CMenu   bt_menu;
        bt_menu.CreateMenu();                       
		
        // Bluetooth->Discoverable
        mode = MF_STRING;
		
        theApp.m_Msg.LoadString(IDS_DISCOVERABLE);
        bt_menu.AppendMenu(mode, IDM_BT_DISCOVERABLE, theApp.m_Msg);            
		
        // Bluetooth->Disconnect
        if (bIsConnected) {
            memset(buf2, 0x00, sizeof(buf2));
            r_value = bBTGetHostName(buf2);
            if (r_value) {
                CString temp;
				
                if (buf2[24] != 0) {
                    buf2[25] = '.';
                    buf2[26] = '.';
                    buf2[27] = '.';
                    buf2[28] = 0x00;
                }
				
                MultiByteToWideChar(CP_UTF8, 0, buf2, 34, hostname_wc,
					MAX_PATH);
				
                theApp.m_Msg = hostname_wc;
                theApp.m_Msg += TEXT(" ");
                temp.LoadString(IDS_DISCONNECT_THIS);
				
                theApp.m_Msg += temp;
            } else {
                theApp.m_Msg.LoadString(IDS_DISCONNECT_THIS);
            }
			
            mode = MF_STRING;
        } else {
            theApp.m_Msg.LoadString(IDS_DISCONNECT_THIS2);
            mode = MF_STRING | MF_DISABLED | MF_GRAYED;
        }
		
        bt_menu.AppendMenu(mode, IDM_BT_DISCONNECT, theApp.m_Msg);
		
        bt_menu.AppendMenu(MF_SEPARATOR, 0, TEXT(""));
		
        for (i = 0; i < iNumOfRx; i++) {
            // Bluetooth->Devices Action
            CMenu       bt_device_menu;
            bt_device_menu.CreateMenu();
			
            memset(buf, 0x00, 512);
            memset(buf2, 0x00, 512);
            r_value = bBTGetPaired(i, buf, buf2);
			
            if (r_value) {
                if (bIsConnected) {
                    if (memcmp(connected_address, buf, 17) == 0) {
                        iConnectedHost = i;
                    }
                }
				
                if (buf2[24] != 0) {
                    buf2[25] = '.';
                    buf2[26] = '.';
                    buf2[27] = '.';
                    buf2[28] = 0x00;
                }
				
                MultiByteToWideChar(CP_UTF8, 0, buf2, 34, hostname_wc,
					MAX_PATH);                                                       
				
                theApp.m_Msg.LoadString(IDS_SELECT);
                bt_device_menu.AppendMenu(MF_STRING, IDM_BT_CONNECT + i,
					theApp.m_Msg);
				
                theApp.m_Msg.LoadString(IDS_DELETE_THIS);       
                bt_device_menu.AppendMenu(MF_STRING, IDM_BT_DELETE + i,
					theApp.m_Msg);
				
                bt_menu.AppendMenu(MF_STRING | MF_POPUP,
					(UINT) bt_device_menu.m_hMenu,
					(WCHAR *) hostname_wc);
            }
        }                                       
		
        bt_menu.AppendMenu(MF_SEPARATOR, 0, TEXT(""));
		
        // Bluetooth->DeleteAllDevice
        if (iNumOfRx > 0) {
            mode = MF_STRING;
        } else {
            mode = MF_STRING | MF_DISABLED | MF_GRAYED;
        }
		
        theApp.m_Msg.LoadString(IDS_DELETE_ALL);
        bt_menu.AppendMenu(mode, IDM_BT_DELETE_ALL, theApp.m_Msg);
		
        mainmenu->AppendMenu(MF_STRING | MF_POPUP, (UINT) bt_menu.m_hMenu,
			TEXT("Bluetooth"));
    }                  
	
    // About 
    theApp.m_Msg.LoadString(IDS_ABOUT);
    mainmenu->AppendMenu(MF_STRING, IDM_ABOUT, theApp.m_Msg);
	
    // RedrawMenuBar
    MENUINFO            MenuInfo        = {
        0
    };
	
    MenuInfo.cbSize = sizeof(MenuInfo);
    MenuInfo.hbrBack = *m_pLightGrayBkBrush;
	
    MenuInfo.fMask = MIM_BACKGROUND;
    MenuInfo.dwStyle = MNS_AUTODISMISS;
	
    SetMenuInfo(mainmenu->m_hMenu, &MenuInfo);
	
    this->DrawMenuBar();
}

void
CIVTKeyboardDlg::OnTrayAbout() {
    bIsDlgOpened = TRUE;
    CDlgAbout           dlg;
    dlg.DoModal();
    bIsDlgOpened = FALSE;
}

void
CIVTKeyboardDlg::OnTrayLayout() {
    if (theApp.m_aplang != LANG_JP) {
        return;
    }
	
    int         i;
    i = GetCurrentMessage()->wParam - IDM_LAYOUT_ENG;
	
    if (i == 0) {
        theApp.keyboard_layout = LAYOUT_ANSI;
    } else {
        theApp.keyboard_layout = LAYOUT_JIS;
    }
	
	if(bIsRXConnected){
		BOOL r_value;
		char        address[MAX_PATH];
		memset(address, 0x00, sizeof(address));
		r_value = bBTGetHostAddress(address);
		if(r_value){
			vSaveConfiguration(address);
		}
	} else {
		vSaveConfiguration();
	}
    
    vCreateMenu();
	
    theApp.bContinueDlg = TRUE;
    OnLeave();
}

void
CIVTKeyboardDlg::OnTrayStyle() {
    theApp.keyboard_style = GetCurrentMessage()->wParam - IDM_STYLE_IPAD;
	
	if(bIsRXConnected){
		BOOL r_value;
		char        address[MAX_PATH];
		memset(address, 0x00, sizeof(address));
		r_value = bBTGetHostAddress(address);
		if(r_value){
			vSaveConfiguration(address);
		}
	} else {
		vSaveConfiguration();
	}
	
    vCreateMenu();
	
    if (theApp.m_aplang == LANG_JP) {
        theApp.bContinueDlg = TRUE;
        OnLeave();
    } else {
        vSetKeyboardStyle();
    }
}

void
CIVTKeyboardDlg::OnTrayBTDiscoverable() {
    bIsDlgOpened = TRUE;
	
    this->ShowWindow(SW_SHOW);
    this->ShowWindow(SW_RESTORE);
	
    ::SetForegroundWindow(this->GetSafeHwnd());
	
    BOOL        r_value;
	
    pTitleDialog->vSetTitle(TEXT(""));
    this->SetWindowText(TEXT("VirtualHID"));
	
    if ((theApp.m_aplang == LANG_JP) && (theApp.keyboard_style == STYLE_IPAD)) {
        bDidRegisterSdp(2);
    } else {
        bDidDeregisterSdp();
    }
	
    CDlgPairing         pairdlg;
	
    pairdlg.iBTAction = BT_ACTION_PAIR;             
	
    r_value = pairdlg.DoModal();
	
    bIsRXConnected = FALSE;
	
    if (r_value == IDOK) {
    } else if (r_value == IDD_DIALOG_SETPIN) {
        CDlgSetPin      setpindlg;
        r_value = setpindlg.DoModal();
        if (r_value != IDOK) {
            pTitleDialog->vSetTitle(TEXT(""));
            pTitleDialog->ShowWindow(SW_HIDE);
			
            this->SetWindowText(TEXT("VirtualHID"));
			
            bIsRXConnected = FALSE;
			
            vCreateMenu();
			
            InvalidateRect(NULL, TRUE);
            UpdateWindow();
			
            SetForegroundWindow();
        }
    } else {
        bSetLed(0);
        pTitleDialog->vSetTitle(TEXT(""));
        pTitleDialog->ShowWindow(SW_HIDE);
		
        this->SetWindowText(TEXT("VirtualHID"));
		
        bIsRXConnected = FALSE;
		
        vCreateMenu();
		
        InvalidateRect(NULL, TRUE);
        UpdateWindow();
		
        SetForegroundWindow();
		
		theApp.m_Msg.LoadString(IDS_PAIR_FAIL);
		MessageBox(theApp.m_Msg);	
    }
    bBTPair(FALSE);
	
    bIsDlgOpened = FALSE;
}

void
CIVTKeyboardDlg::OnTrayBTDisconnect() {
    bIsDlgOpened = TRUE;
	
    CDlgPairing         pairdlg;
    pairdlg.iBTAction = BT_ACTION_DISCONNECT;
	
    pairdlg.DoModal();  
    bBTDisconnect();
	
    bIsDlgOpened = FALSE;
}

void
CIVTKeyboardDlg::OnTrayBTConnect() {
    bIsDlgOpened = TRUE;
	
    this->ShowWindow(SW_SHOW);
    this->ShowWindow(SW_RESTORE);
	
    ::SetForegroundWindow(this->GetSafeHwnd());
	
    int         target_device;
    target_device = GetCurrentMessage()->wParam - IDM_BT_CONNECT;
	
    if ((!bIsRXConnected) || (iConnectedHost != target_device)) {
		
        bIsRXConnected = FALSE;
        this->SetWindowText(TEXT("VirtualHID"));
        pTitleDialog->vSetTitle(TEXT(""));
		
        BOOL            r_value;
        CDlgPairing     pairdlg;
		
        pairdlg.iBTAction = BT_ACTION_CONNECT;
        pairdlg.iBTIndex = target_device;
		
        r_value = pairdlg.DoModal();
        //if (r_value != IDOK) {            
        //}
		
		BOOL        bIsInitializing;
		BOOL        bIsInitialized;
		BOOL        bIsDiscoverable;
		BOOL        bIsConnecting;
		BOOL        bIsConnected;
		BOOL        bIsPinRequest;
		
		r_value = bBTGetStatus(&bIsInitializing, &bIsInitialized,
			&bIsDiscoverable, &bIsConnecting, &bIsConnected,
			&bIsPinRequest);
		
		if(!bIsConnected){
			bSetLed(0);
			
            pTitleDialog->vSetTitle(TEXT(""));
            pTitleDialog->ShowWindow(SW_HIDE);
			
            this->SetWindowText(TEXT("VirtualHID"));
			
            bIsRXConnected = FALSE;
			
            vCreateMenu();
			
            InvalidateRect(NULL, TRUE);
            UpdateWindow();
			
            SetForegroundWindow();
			
			theApp.m_Msg.LoadString(IDS_CONNECT_FAIL);
			MessageBox(theApp.m_Msg);		
		}
		
    }
	
    bIsDlgOpened = FALSE;
}

void
CIVTKeyboardDlg::OnTrayBTDelete() {
    bIsDlgOpened = TRUE;
	
    int         i;
    i = GetCurrentMessage()->wParam - IDM_BT_DELETE;
	
    BOOL        r_value;
    char        buf[512];
    char        buf2[512];
    WCHAR       hostname_wc[MAX_PATH];
    CString     temp;
	
    memset(buf, 0x00, 512);
    memset(buf2, 0x00, 512);
    memset(hostname_wc, 0x00, sizeof(hostname_wc));

    r_value = bBTGetPaired(i, buf, buf2);
    if (r_value) {
        MultiByteToWideChar(CP_UTF8, 0, buf2, 34, hostname_wc, MAX_PATH);
        temp = hostname_wc;
        temp += TEXT(" ");
    } else {
        temp = TEXT("");
    }

    theApp.m_Msg.LoadString(IDS_UNPAIR_WARNING);

    temp += theApp.m_Msg;

    if (MessageBox(temp, APNAME_STR, MB_YESNO | MB_ICONEXCLAMATION) == IDYES) {
        CDlgPairing     pairdlg;

        pairdlg.iBTAction = BT_ACTION_UNPAIR;
        pairdlg.DoModal();

        bBTUnpairSingle(i);

        bSetLed(0);
        pTitleDialog->vSetTitle(TEXT(""));
        pTitleDialog->ShowWindow(SW_HIDE);

        this->SetWindowText(TEXT("VirtualHID"));

        bIsRXConnected = FALSE;

        vCreateMenu();

        InvalidateRect(NULL, TRUE);
        UpdateWindow();
    } 

    bIsDlgOpened = FALSE;
}

void
CIVTKeyboardDlg::OnTrayBTDeleteAll() {
    bIsDlgOpened = TRUE;

    theApp.m_Msg.LoadString(IDS_CLEAR_WARNING);
    if (MessageBox(theApp.m_Msg, APNAME_STR, MB_YESNO | MB_ICONEXCLAMATION) ==
        IDYES) {
        CDlgPairing     pairdlg;

        pairdlg.iBTAction = BT_ACTION_UNPAIR;
        pairdlg.DoModal();

        bBTUnpairAll();

        bSetLed(0);
        pTitleDialog->vSetTitle(TEXT(""));
        pTitleDialog->ShowWindow(SW_HIDE);

        this->SetWindowText(TEXT("VirtualHID"));

        bIsRXConnected = FALSE;

        vCreateMenu();

        InvalidateRect(NULL, TRUE);
        UpdateWindow();
    }

    bIsDlgOpened = FALSE;
}

void
CIVTKeyboardDlg::OnTrayLeave() {
    theApp.bContinueDlg = FALSE;

    if (bIsRXConnected) {
        CDlgPairing     pairdlg;
        pairdlg.iBTAction = BT_ACTION_DISCONNECT;

        pairdlg.DoModal();      
        bBTDisconnect();
    }

    OnLeave();
}

LRESULT
CIVTKeyboardDlg::OnPowerBroadcast(WPARAM wParam, LPARAM lParam) {
    if (wParam == PBT_APMSUSPEND) {
        theApp.bContinueDlg = FALSE;

        if (bIsRXConnected) {
            CDlgPairing         pairdlg;
            pairdlg.iBTAction = BT_ACTION_DISCONNECT;

            pairdlg.DoModal();  
            bBTDisconnect();
        }

        OnLeave();
    }

    return TRUE;
}

int
CIVTKeyboardDlg::DoModal() {
    CDialogTemplate     dlt;
    int                 nResult;

    if (theApp.m_aplang == LANG_JP) {
        if (theApp.keyboard_layout == LAYOUT_ANSI) {
            if (!dlt.Load(MAKEINTRESOURCE(IDD_IVTKEYBOARD_DIALOG_ANSI))) {
                return -1;
            }
        } else {
            // load dialog template 
            if (theApp.keyboard_style >= STYLE_WINDOWS) {
                if (!dlt.Load(MAKEINTRESOURCE(IDD_IVTKEYBOARD_DIALOG))) {
                    return -1;
                }
            } else {
                if (!dlt.Load(MAKEINTRESOURCE(IDD_IVTKEYBOARD_DIALOG_MAC))) {
                    return -1;
                }
            }
        }
    } else {
        if (!dlt.Load(MAKEINTRESOURCE(IDD_IVTKEYBOARD_DIALOG))) {
            return -1;
        }
    }

    if (theApp.m_ScreenWidth < 1024) {
        dlt.SetFont(TEXT("Microsoft Sans Serif"), 6);
    } else {
        dlt.SetFont(TEXT("Microsoft Sans Serif"), 8);
    }

    // get pointer to the modified dialog template
    LPSTR       pdata   = (LPSTR) GlobalLock(dlt.m_hTemplate);

    // let MFC know that you are using your own template
    m_lpszTemplateName = NULL;
    m_hDialogTemplate = NULL;
    InitModalIndirect(pdata);

    // display dialog box
    nResult = CDialog::DoModal();

    // unlock memory object
    GlobalUnlock(dlt.m_hTemplate);

    return nResult;
}


BOOL
CIVTKeyboardDlg::PreTranslateMessage(MSG *pMsg) {
    // TODO: Add your specialized code here and/or call the base class
    return FALSE;
    return CDialog::PreTranslateMessage(pMsg);
}


LRESULT
CIVTKeyboardDlg::OnTaskBarCreated(WPARAM wParam, LPARAM lParam) {
    if (m_pTray) {
        delete m_pTray;
        m_pTray = NULL;
    }

    m_pTray = new CTrayNot(this, WM_TRAY_NOTIFY, NULL, theApp.m_pIconList);
    m_pTray->SetTip();    

    return true;
}
