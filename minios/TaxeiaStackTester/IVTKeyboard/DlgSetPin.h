#if !defined(AFX_DLGSETPIN_H__A4E4387E_39F5_4B54_97BF_A3A8A9598294__INCLUDED_)
#define AFX_DLGSETPIN_H__A4E4387E_39F5_4B54_97BF_A3A8A9598294__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgSetPin.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDlgSetPin dialog

class CDlgSetPin : public CDialog
{
// Construction
public:
	CDlgSetPin(CWnd* pParent = NULL);   // standard constructor
	CBrush      *m_pWhiteBkBrush;
// Dialog Data
	//{{AFX_DATA(CDlgSetPin)
	enum { IDD = IDD_DIALOG_SETPIN };
	CEdit	m_control_pin;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgSetPin)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDlgSetPin)
	afx_msg void OnTimer(UINT nIDEvent);
	virtual void OnOK();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGSETPIN_H__A4E4387E_39F5_4B54_97BF_A3A8A9598294__INCLUDED_)
