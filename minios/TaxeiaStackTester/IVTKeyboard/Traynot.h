// CTrayNot window

class CTrayNot : public CObject {
    // Construction
  public:
                        CTrayNot(CWnd *pWnd, UINT uCallbackMessage,
                                 LPCTSTR szTip, HICON *pList) ;

    // Attributes
						BOOL                m_bEnabled ; 
  public: 
    NOTIFYICONDATA      m_tnd ;
    HICON               *m_pIconList ;

  public:    
    void                SetTip();

    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CTrayNot)
    //}}AFX_VIRTUAL

    // Implementation
  public:
    virtual             ~CTrayNot();
};

/////////////////////////////////////////////////////////////////////////////
