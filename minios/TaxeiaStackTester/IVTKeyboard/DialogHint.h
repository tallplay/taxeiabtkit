#if !defined(AFX_DIALOGHINT_H__7E392A8F_5E1E_46DB_B43E_C1804CB0F112__INCLUDED_)
#define AFX_DIALOGHINT_H__7E392A8F_5E1E_46DB_B43E_C1804CB0F112__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DialogHint.h : header file
//
#include "ColorEdit.h"

/////////////////////////////////////////////////////////////////////////////
// CDialogHint dialog

class CDialogHint : public CDialog
{
// Construction
public:
	CDialogHint(CWnd* pParent = NULL);   // standard constructor
	CBrush              *m_pBkBrush;
// Dialog Data
	//{{AFX_DATA(CDialogHint)
	enum { IDD = IDD_DIALOG_HINT };
	CColorEdit	m_control_hint2;
	CColorEdit	m_control_hint1;
	//}}AFX_DATA

	void vShowWindow(CWnd *parent_hwnd);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDialogHint)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDialogHint)
	virtual BOOL OnInitDialog();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnNcPaint();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DIALOGHINT_H__7E392A8F_5E1E_46DB_B43E_C1804CB0F112__INCLUDED_)
