// IVTKeyboard.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "IVTKeyboard.h"
#include "IVTKeyboardDlg.h"

#include "DlgAbout.h"

#define SECURITY_WIN32
#include <Security.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char     THIS_FILE[]     = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CIVTKeyboardApp

BEGIN_MESSAGE_MAP(CIVTKeyboardApp, CSingleInstanceApp)
        //{{AFX_MSG_MAP(CIVTKeyboardApp)
                // NOTE - the ClassWizard will add and remove mapping macros here.
                //    DO NOT EDIT what you see in these blocks of generated code!
        //}}AFX_MSG
        ON_COMMAND(ID_HELP, CSingleInstanceApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CIVTKeyboardApp construction
CIVTKeyboardApp::CIVTKeyboardApp() :
    CSingleInstanceApp(TEXT("{DA042E56-944F-40bf-8A47-F017AB6D5C75}"))
{
    // TODO: add construction code here,
    // Place all significant initialization in InitInstance
}
/////////////////////////////////////////////////////////////////////////////
// The one and only CIVTKeyboardApp object

CIVTKeyboardApp theApp;
CIVTKeyboardDlg *pMainDlg;

/////////////////////////////////////////////////////////////////////////////
// CIVTKeyboardApp initialization

BOOL
CIVTKeyboardApp::InitInstance() {
    // Standard initialization
    // If you are not using these features and wish to reduce the size
    //  of your final executable, you should remove from the following
    //  the specific initialization routines you do not need.

#ifdef _AFXDLL
    Enable3dControls();                 // Call this when using MFC in a shared DLL
#else
    Enable3dControlsStatic();   // Call this when linking to MFC statically
#endif

    if (!AmITheFirst()) {
        /*
                HWND    hwnd    = FindWindow(NULL, TEXT("VirtualHID")); 
                //HWND    hwnd    = ::FindWindow(TEXT("CIVTKeyboardDlg"), NULL); 
                if (hwnd) {
                        PostMessage(hwnd, WM_TRAY_NOTIFY, NULL, WM_LBUTTONDOWN);
                } 
                */
        return FALSE;
    }

    /*
    ///////////////////////////////////////////////////////////////////////// 
    ///////////////////////////////////////////////////////////////////////// 
    WNDCLASS wc;
    // Get the info for this class.
            // #32770 is the default class name for dialogs boxes.
    ::GetClassInfo(AfxGetInstanceHandle(), TEXT("#32770"), &wc);
    // Change the name of the class.
    wc.lpszClassName = TEXT("VirtualHID");
    // Register this class so that MFC can use it.
    AfxRegisterClass(&wc);      
    ///////////////////////////////////////////////////////////////////////// 
    ///////////////////////////////////////////////////////////////////////// 
    */

    m_pIconList[0] = LoadIcon(MAKEINTRESOURCE(IDR_MAINFRAME));

    keyboard_layout = LAYOUT_ANSI;
    keyboard_style = STYLE_IPAD;    
    pMainDlg = NULL;

    // Check OS Language
    LCID        lcid    = GetThreadLocale();
    switch (lcid & 0x0000FFFF) {
      case 0x0804:
        m_aplang = LANG_CHS;
        break;
      case 0x0411:
        m_aplang = LANG_JP;     
        break;
      case 0x0404:
        m_aplang = LANG_CHT;
        break;       
      default:
        // switch language to English
        m_aplang = LANG_ENG;
    }

    RECT        rectWnd; 
    ::GetWindowRect(GetDesktopWindow(), &rectWnd);
    m_ScreenWidth = rectWnd.right;
    m_ScreenHeight = rectWnd.bottom;    

    vSetApLang();

    vLoadConfiguration();

    // Init BT Stack
    BOOL        r_value;  
    int         n_device;

    vInit();    

    while (1) {
        // Check if device exist
        r_value = bOpenBTHardware();
        if (r_value) {
            break;
        } 

        theApp.m_Msg.LoadString(IDS_DEVICE_NOT_FOUND);

        if (AfxMessageBox(theApp.m_Msg, MB_OKCANCEL) == IDCANCEL) {
            return FALSE;
        }
    }

    n_device = iOpenDevice();
    bSetLed(0x00);

    theApp.bContinueDlg = FALSE;
	theApp.bIsBTConnected = FALSE;

    do {
        CIVTKeyboardDlg dlg;
        pMainDlg = &dlg;
        //m_pMainWnd = &dlg;
        r_value = dlg.DoModal();
    } while (theApp.bContinueDlg);

    // Since the dialog has been closed, return FALSE so that we exit the
    //  application, rather than start the application's message pump.
    return FALSE;
}

void
CIVTKeyboardApp::OnSecondInstance(UINT, LPARAM) {
    if (pMainDlg) {
        PostMessage(pMainDlg->GetSafeHwnd(), WM_TRAY_NOTIFY, NULL,
                    WM_LBUTTONDOWN);
    }
}

void
CIVTKeyboardApp::vSetApLang() {
    //m_aplang = LANG_ENG;
    //m_aplang = LANG_CHT;
    //m_aplang = LANG_JP;
    //m_aplang = LANG_CHS;

    switch (m_aplang) {
      case LANG_JP:
        if (!IsValidLocale(MAKELCID(MAKELANGID(LANG_JAPANESE, SUBLANG_DEFAULT),
                                    SORT_DEFAULT),
                           LCID_INSTALLED)) {
            m_aplang = LANG_ENG;                        
            break;
        }

        // switch language to Japanese
        ::SetThreadLocale(MAKELCID(MAKELANGID(LANG_JAPANESE, SUBLANG_DEFAULT),
                                   SORT_DEFAULT));   
        break;

      case LANG_CHT:
        if (!IsValidLocale(MAKELCID(MAKELANGID(LANG_CHINESE,
                                               SUBLANG_CHINESE_TRADITIONAL),
                                    SORT_DEFAULT),
                           LCID_INSTALLED)) {
            m_aplang = LANG_ENG;
            break;
        }

        // switch language to Traditional Chinese
        ::SetThreadLocale(MAKELCID(MAKELANGID(LANG_CHINESE,
                                              SUBLANG_CHINESE_TRADITIONAL),
                                   SORT_DEFAULT));
        break;

      case LANG_CHS:
        if (!IsValidLocale(MAKELCID(MAKELANGID(LANG_CHINESE,
                                               SUBLANG_CHINESE_SIMPLIFIED),
                                    SORT_DEFAULT),
                           LCID_INSTALLED)) {
            m_aplang = LANG_ENG;
            break;
        }

        // switch language to Simplified Chinese
        ::SetThreadLocale(MAKELCID(MAKELANGID(LANG_CHINESE,
                                              SUBLANG_CHINESE_SIMPLIFIED),
                                   SORT_DEFAULT));   

        break;

      default:
        m_aplang = LANG_ENG;
        break;
    }

    if (m_aplang == LANG_ENG) {
        // switch language to English           
        ::SetThreadLocale(MAKELCID(MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US),
                                   SORT_DEFAULT));
    }
}

void
CIVTKeyboardApp::WinHelp(DWORD dwData, UINT nCmd) {
    // TODO: Add your specialized code here and/or call the base class  
    //CSingleInstanceApp::WinHelp(dwData, nCmd);
}

void
vLoadConfiguration() {
    HKEY        hMiniKey;
    DWORD       Disposition;
    DWORD       err;
    UCHAR       szValue[MAX_PATH];
    DWORD       cbValue;
    DWORD       dwType; 

    WCHAR       keyname[MAX_PATH];

    wsprintf(keyname, TEXT("Software\\VirtualHID"));

    err = RegCreateKeyEx(HKEY_CURRENT_USER, keyname, 0, TEXT(""),
                         REG_OPTION_NON_VOLATILE, KEY_WRITE | KEY_READ, NULL,
                         &hMiniKey, &Disposition);  

    if (ERROR_SUCCESS == err) {
        cbValue = sizeof(DWORD);
        err = RegQueryValueEx(hMiniKey, TEXT("KeyboardStyle"), NULL, &dwType,
                              szValue, &cbValue);
        if (ERROR_SUCCESS == err) {
            theApp.keyboard_style = *((DWORD *) szValue);
        } else {
            theApp.keyboard_style = STYLE_IPAD;
        }

        cbValue = sizeof(DWORD);
        err = RegQueryValueEx(hMiniKey, TEXT("KeyboardLayout"), NULL, &dwType,
                              szValue, &cbValue);
        if (ERROR_SUCCESS == err) {
            theApp.keyboard_layout = *((DWORD *) szValue);
        } else {
            if (theApp.m_aplang == LANG_JP) {
                theApp.keyboard_layout = LAYOUT_JIS;
            } else {
                theApp.keyboard_layout = LAYOUT_ANSI;
            }
        }
    }

    RegCloseKey(hMiniKey);
}

void
vSaveConfiguration() {
    HKEY        hMiniKey;
    DWORD       Disposition;
    DWORD       err;
    UCHAR       szValue[MAX_PATH];
    DWORD       cbValue;
    DWORD       dwType; 

    WCHAR       keyname[MAX_PATH];

    wsprintf(keyname, TEXT("Software\\VirtualHID"));

    err = RegCreateKeyEx(HKEY_CURRENT_USER, keyname, 0, TEXT(""),
                         REG_OPTION_NON_VOLATILE, KEY_WRITE | KEY_READ, NULL,
                         &hMiniKey, &Disposition);  

    if (ERROR_SUCCESS == err) {
        dwType = REG_DWORD;
        cbValue = sizeof(DWORD);
        *((DWORD *) &szValue) = theApp.keyboard_style;

        RegSetValueEx(hMiniKey, TEXT("KeyboardStyle"), 0, dwType, szValue,
                      cbValue);

        dwType = REG_DWORD;
        cbValue = sizeof(DWORD);
        *((DWORD *) &szValue) = theApp.keyboard_layout;

        RegSetValueEx(hMiniKey, TEXT("KeyboardLayout"), 0, dwType, szValue,
                      cbValue);
    }

    RegCloseKey(hMiniKey);
}


void
vLoadConfiguration(char *pAddress) {
	USES_CONVERSION;

    HKEY        hMiniKey;
    DWORD       Disposition;
    DWORD       err;
    UCHAR       szValue[MAX_PATH];
    DWORD       cbValue;
    DWORD       dwType; 

    WCHAR       keyname[MAX_PATH];
	memset(keyname, 0x00, sizeof(keyname));

    wsprintf(keyname,
             TEXT("Software\\VirtualHID\\Bluetooth\\RM:%s"),A2W(pAddress));

    err = RegCreateKeyEx(HKEY_CURRENT_USER, keyname, 0, TEXT(""),
                         REG_OPTION_NON_VOLATILE, KEY_WRITE | KEY_READ, NULL,
                         &hMiniKey, &Disposition);  

    if (ERROR_SUCCESS == err) {
        cbValue = sizeof(DWORD);
        err = RegQueryValueEx(hMiniKey, TEXT("KeyboardStyle"), NULL, &dwType,
                              szValue, &cbValue);
        if (ERROR_SUCCESS == err) {
            theApp.keyboard_style = *((DWORD *) szValue);
        }

        cbValue = sizeof(DWORD);
        err = RegQueryValueEx(hMiniKey, TEXT("KeyboardLayout"), NULL, &dwType,
                              szValue, &cbValue);
        if (ERROR_SUCCESS == err) {
            theApp.keyboard_layout = *((DWORD *) szValue);
        } 
    }

    RegCloseKey(hMiniKey);
}

void
vSaveConfiguration(char *pAddress) {
	USES_CONVERSION;

    HKEY        hMiniKey;
    DWORD       Disposition;
    DWORD       err;
    UCHAR       szValue[MAX_PATH];
    DWORD       cbValue;
    DWORD       dwType; 

    WCHAR       keyname[MAX_PATH];

	wsprintf(keyname,
		TEXT("Software\\VirtualHID\\Bluetooth\\RM:%s"),A2W(pAddress));

    err = RegCreateKeyEx(HKEY_CURRENT_USER, keyname, 0, TEXT(""),
                         REG_OPTION_NON_VOLATILE, KEY_WRITE | KEY_READ, NULL,
                         &hMiniKey, &Disposition);  

    if (ERROR_SUCCESS == err) {
        dwType = REG_DWORD;
        cbValue = sizeof(DWORD);
        *((DWORD *) &szValue) = theApp.keyboard_style;

        RegSetValueEx(hMiniKey, TEXT("KeyboardStyle"), 0, dwType, szValue,
                      cbValue);

        dwType = REG_DWORD;
        cbValue = sizeof(DWORD);
        *((DWORD *) &szValue) = theApp.keyboard_layout;

        RegSetValueEx(hMiniKey, TEXT("KeyboardLayout"), 0, dwType, szValue,
                      cbValue);
    }

    RegCloseKey(hMiniKey);
}

