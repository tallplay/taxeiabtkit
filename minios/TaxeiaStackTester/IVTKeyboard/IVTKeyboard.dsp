# Microsoft Developer Studio Project File - Name="IVTKeyboard" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=IVTKeyboard - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "IVTKeyboard.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "IVTKeyboard.mak" CFG="IVTKeyboard - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "IVTKeyboard - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "IVTKeyboard - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "IVTKeyboard - Win32 Release"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MT /W3 /GX /O2 /I "..\\" /I "..\DemoApp\\" /D POW=5 /D POW_INIT=5 /D PROFILE_TYPE=0 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "UNICODE" /D "_UNICODE" /Fr /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x404 /d "NDEBUG"
# ADD RSC /l 0x404 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 Release\BTStackLibrary.lib Secur32.lib setupapi.lib Winmm.lib hid.lib Msimg32.lib /nologo /entry:"wWinMainCRTStartup" /subsystem:windows /machine:I386 /nodefaultlib:"nafxcw.lib" /out:"Release/VirtualHID.exe"
# SUBTRACT LINK32 /incremental:yes /map /debug /nodefaultlib

!ELSEIF  "$(CFG)" == "IVTKeyboard - Win32 Debug"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /I "..\\" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "UNICODE" /D "_UNICODE" /D PROFILE_TYPE=0 /D POW=5 /D POW_INIT=5 /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x404 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 Debug\BTStackLibrary.lib Secur32.lib setupapi.lib Winmm.lib Msimg32.lib /nologo /entry:"wWinMainCRTStartup" /subsystem:windows /debug /machine:I386 /nodefaultlib:"nafxcwd.lib" /out:"Debug/VirtualHID.exe" /pdbtype:sept

!ENDIF 

# Begin Target

# Name "IVTKeyboard - Win32 Release"
# Name "IVTKeyboard - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\ColorEdit.cpp
# End Source File
# Begin Source File

SOURCE=.\DialogHint.cpp
# End Source File
# Begin Source File

SOURCE=.\DlgAbout.cpp
# End Source File
# Begin Source File

SOURCE=.\DlgPairing.cpp
# End Source File
# Begin Source File

SOURCE=.\DlgSetPin.cpp
# End Source File
# Begin Source File

SOURCE=.\DlgTitle.cpp
# End Source File
# Begin Source File

SOURCE=.\IVTKeyboard.cpp
# End Source File
# Begin Source File

SOURCE=.\IVTKeyboard.rc
# End Source File
# Begin Source File

SOURCE=.\IVTKeyboardDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\IVTUSB.CPP
# End Source File
# Begin Source File

SOURCE=".\rijndael-alg.cpp"
# End Source File
# Begin Source File

SOURCE=".\rijndael-api.cpp"
# End Source File
# Begin Source File

SOURCE=.\SingleInstanceApp.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\Traynot.cpp
# End Source File
# Begin Source File

SOURCE=.\TXBT.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\Color.h
# End Source File
# Begin Source File

SOURCE=.\ColorEdit.h
# End Source File
# Begin Source File

SOURCE=.\DialogHint.h
# End Source File
# Begin Source File

SOURCE=.\DlgAbout.h
# End Source File
# Begin Source File

SOURCE=.\DlgPairing.h
# End Source File
# Begin Source File

SOURCE=.\DlgSetPin.h
# End Source File
# Begin Source File

SOURCE=.\DlgTitle.h
# End Source File
# Begin Source File

SOURCE=.\IVTKeyboard.h
# End Source File
# Begin Source File

SOURCE=.\IVTKeyboardDlg.h
# End Source File
# Begin Source File

SOURCE=.\IVTUSB.H
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=.\resrc1.h
# End Source File
# Begin Source File

SOURCE=.\SingleInstanceApp.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=.\Traynot.h
# End Source File
# Begin Source File

SOURCE=.\TXBT.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\ICON\aback.ico
# End Source File
# Begin Source File

SOURCE=.\res\ICON\aback_over.ico
# End Source File
# Begin Source File

SOURCE=.\res\ICON\apple.ico
# End Source File
# Begin Source File

SOURCE=.\res\ICON\apple_over.ico
# End Source File
# Begin Source File

SOURCE=.\res\ICON\back.ico
# End Source File
# Begin Source File

SOURCE=.\res\ICON\back_over.ico
# End Source File
# Begin Source File

SOURCE=.\res\ICON\command.ico
# End Source File
# Begin Source File

SOURCE=.\res\ICON\command_over.ico
# End Source File
# Begin Source File

SOURCE=.\res\ICON\down.ico
# End Source File
# Begin Source File

SOURCE=.\res\ICON\down_over.ico
# End Source File
# Begin Source File

SOURCE=.\res\ICON\earth.ico
# End Source File
# Begin Source File

SOURCE=.\res\ICON\earth_over.ico
# End Source File
# Begin Source File

SOURCE=.\res\ICON\eject.ico
# End Source File
# Begin Source File

SOURCE=.\res\ICON\esc.ico
# End Source File
# Begin Source File

SOURCE=.\res\ICON\esc_over.ico
# End Source File
# Begin Source File

SOURCE=.\res\ICON\home.ico
# End Source File
# Begin Source File

SOURCE=.\res\ICON\home_over.ico
# End Source File
# Begin Source File

SOURCE=.\res\icon1.ico
# End Source File
# Begin Source File

SOURCE=.\res\ICON\icon5.ico
# End Source File
# Begin Source File

SOURCE=.\res\IVTKeyboard.ico
# End Source File
# Begin Source File

SOURCE=.\res\IVTKeyboard.rc2
# End Source File
# Begin Source File

SOURCE=.\res\ICON\keyboard.ico
# End Source File
# Begin Source File

SOURCE=.\res\ICON\keyboard_over.ico
# End Source File
# Begin Source File

SOURCE=.\res\ICON\left.ico
# End Source File
# Begin Source File

SOURCE=.\res\ICON\left_over.ico
# End Source File
# Begin Source File

SOURCE=.\res\ICON\lightdown.ico
# End Source File
# Begin Source File

SOURCE=.\res\ICON\lightdown_over.ico
# End Source File
# Begin Source File

SOURCE=.\res\ICON\lightup.ico
# End Source File
# Begin Source File

SOURCE=.\res\ICON\lightup_over.ico
# End Source File
# Begin Source File

SOURCE=.\res\ICON\lock.ico
# End Source File
# Begin Source File

SOURCE=.\res\ICON\lock_over.ico
# End Source File
# Begin Source File

SOURCE=.\res\logo.bmp
# End Source File
# Begin Source File

SOURCE=.\res\ICON\menu.ico
# End Source File
# Begin Source File

SOURCE=.\res\ICON\menu_over.ico
# End Source File
# Begin Source File

SOURCE=.\res\ICON\mouse.ico
# End Source File
# Begin Source File

SOURCE=.\res\ICON\mouse_over.ico
# End Source File
# Begin Source File

SOURCE=.\res\ICON\mute.ico
# End Source File
# Begin Source File

SOURCE=.\res\ICON\mute_over.ico
# End Source File
# Begin Source File

SOURCE=.\res\ICON\next.ico
# End Source File
# Begin Source File

SOURCE=.\res\ICON\next_over.ico
# End Source File
# Begin Source File

SOURCE=.\res\ICON\play.ico
# End Source File
# Begin Source File

SOURCE=.\res\ICON\play_over.ico
# End Source File
# Begin Source File

SOURCE=.\res\ICON\right.ico
# End Source File
# Begin Source File

SOURCE=.\res\ICON\right_over.ico
# End Source File
# Begin Source File

SOURCE=.\res\ICON\search.ico
# End Source File
# Begin Source File

SOURCE=.\res\ICON\search_over.ico
# End Source File
# Begin Source File

SOURCE=.\res\ICON\slide.ico
# End Source File
# Begin Source File

SOURCE=.\res\ICON\slide_over.ico
# End Source File
# Begin Source File

SOURCE=.\res\ICON\up.ico
# End Source File
# Begin Source File

SOURCE=.\res\ICON\up_over.ico
# End Source File
# Begin Source File

SOURCE=.\res\ICON\voldown.ico
# End Source File
# Begin Source File

SOURCE=.\res\ICON\voldown_over.ico
# End Source File
# Begin Source File

SOURCE=.\res\ICON\volup.ico
# End Source File
# Begin Source File

SOURCE=.\res\ICON\volup_over.ico
# End Source File
# Begin Source File

SOURCE=.\res\ICON\win.ico
# End Source File
# Begin Source File

SOURCE=.\res\ICON\win_menu.ico
# End Source File
# Begin Source File

SOURCE=.\res\ICON\win_meun_over.ico
# End Source File
# Begin Source File

SOURCE=.\res\ICON\win_over.ico
# End Source File
# End Group
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# End Target
# End Project
