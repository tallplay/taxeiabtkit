// DlgMain.cpp : implementation file
//
#include "stdafx.h"
#include "IVTKeyboard.h"
#include "DlgMain.h"
#include "IVTKeyboardDlg.h"
#include "DlgUninstall.h"
#include "rijndael-api.h"
#include <dbt.h>
#include "IVTUSB.h"

#ifdef _DEBUG
#define NO_THREAD_DESKTOP
#endif

#define m_nHotKeyID 100

#define APRELEASETIME   0x4f50813b 
#define TRIALDAYS               100

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char     THIS_FILE[]     = __FILE__;
#endif

typedef struct _THREAD_DATA {
    HDESK       hDesk;
    WCHAR       szDesktopName[20];
} THREAD_DATA;

BOOL StartThread(LPTHREAD_START_ROUTINE ThreadFunc, THREAD_DATA *td);
int  WINAPI Thread_Desktop(LPTHREAD_START_ROUTINE ThreadFunc, THREAD_DATA *td);
DWORD   WINAPI ThreadFunc(void *pData);

#define TIMER_EXPOSE 0

#define IDM_SET_TRIGGER      60000
#define IDM_SET_AUTORUN      60001
#define IDM_SET_HOTKEY       60002

#define IDM_BG_DEFAULT       60003
#define IDM_BG_CURRENT       60004
#define IDM_BG_CUSTOM            60005

/////////////////////////////////////////////////////////////////////////////
// CDlgMain dialog

CDlgMain::CDlgMain(CWnd *pParent /*=NULL*/) :
    CDialog(CDlgMain::IDD, pParent)
{
    //{{AFX_DATA_INIT(CDlgMain)
        // NOTE: the ClassWizard will add member initialization here
    //}}AFX_DATA_INIT
    m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void
CDlgMain::DoDataExchange(CDataExchange *pDX) {
    CDialog::DoDataExchange(pDX);
    //{{AFX_DATA_MAP(CDlgMain)
        // NOTE: the ClassWizard will add DDX and DDV calls here
    //}}AFX_DATA_MAP

}

BEGIN_MESSAGE_MAP(CDlgMain, CDialog)
//{{AFX_MSG_MAP(CDlgMain)
ON_WM_PAINT()
ON_MESSAGE(WM_TRAY_NOTIFY, OnTrayNotify)
ON_WM_WINDOWPOSCHANGING()
ON_WM_TIMER()
ON_COMMAND(IDM_SET_TRIGGER, OnSetTrigger)
ON_COMMAND(IDM_SET_HOTKEY, OnSetHotKey)
ON_COMMAND(IDM_SET_AUTORUN, OnSetAutorun)
ON_COMMAND(IDM_BG_DEFAULT, OnSetBGDefault)
ON_COMMAND(IDM_BG_CURRENT, OnSetBGCurrent)
ON_COMMAND(IDM_BG_CUSTOM, OnSetBGCustom)
ON_MESSAGE( WM_POWERBROADCAST, OnPowerBroadcast )
//}}AFX_MSG_MAP
ON_MESSAGE(WM_HOTKEY,OnHotKey)
ON_WM_DEVICECHANGE()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgMain message handlers

void
CDlgMain::OnPaint() {
    if (IsIconic()) {
        CPaintDC        dc      (this); // device context for painting

        SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

        // Center icon in client rectangle
        int     cxIcon  = GetSystemMetrics(SM_CXICON);
        int     cyIcon  = GetSystemMetrics(SM_CYICON);
        CRect   rect;
        GetClientRect(&rect);
        int     x       = (rect.Width() - cxIcon + 1) / 2;
        int     y       = (rect.Height() - cyIcon + 1) / 2;

        // Draw the icon
        dc.DrawIcon(x, y, m_hIcon);
    } else {
        CDialog::OnPaint();
    }
}

BOOL
CDlgMain::OnInitDialog() {
    CDialog::OnInitDialog();

    // TODO: Add extra initialization here
    SetIcon(m_hIcon, TRUE);                     // Set big icon
    SetIcon(m_hIcon, FALSE);            // Set small icon

    m_pTray = NULL;
    m_pTray = new CTrayNot(this, WM_TRAY_NOTIFY, NULL, theApp.m_pIconList);

    BOOL        r_value;  

    vInit();    

    // Check if device exist
    r_value = bOpenBTHardware();
    if (r_value) {
        BYTE    hardware_address[512];     

        memset(hardware_address, 0x00, 512);
        r_value = bGetBTHardwareAddress(hardware_address);
        if (!r_value) {
            bCloseBTHardware(); 
            bIsDlgOpened = FALSE;
            return TRUE;
        }

#if 0

        BYTE    zero_data[512];
        BYTE    init_data[512];
        BYTE    encrypt_data[512];

        r_value = bGetEncryptData(encrypt_data);
        if (!r_value) {
            bCloseBTHardware(); 
            bIsDlgOpened = FALSE;
            return TRUE;
        }

        keyInstance     keyInst; 
        rijndaelInit(&keyInst, 128, hardware_address);
        rijndael_ecb_decrypt(&keyInst, encrypt_data, 512, 0, init_data);

        memset(zero_data, 0x00, 512);
        if (memcmp(zero_data, init_data, 512)) {
            theApp.m_DemoVersion = TRUE;
        }                       

#endif

        bCloseBTHardware();

        if (r_value) {
            // Open BT Stack
            int         n_device;
            n_device = iOpenDevice();

            bSetLed(0x00);

            vStartSniffer(AfxGetInstanceHandle());
        }
    } else {
		EndDialog(0);
		return TRUE;
	}

    /*
       if (theApp.m_DemoVersion) {
           // Check AP Release Time;
           time_t  currenttime     = time(NULL);
           if ((currenttime - APRELEASETIME) > (TRIALDAYS * 24 * 60 * 60)) {
               AfxMessageBox(TEXT("DemoVersion has been expired!!"));        
               EndDialog(0);
               bIsDlgOpened = FALSE;
               return TRUE;
           }
       }
    */

    bIsDlgOpened = FALSE;
    theApp.bIsTrigger = TRUE;
    theApp.bIsHotKey = TRUE;

    vLoadConfiguration();

    theApp.bIsAutorun = TRUE;

    CString     cmdline = ::GetCommandLine();
    TCHAR       *ptrtemp;    
    ptrtemp = NULL;
    ptrtemp = _tcsstr(cmdline, TEXT("-noautorun"));

    if (!ptrtemp) {
        OnSetAutorun();
        SendMessage(WM_TRAY_NOTIFY, 0, WM_LBUTTONDBLCLK);
        //PostMessage(WM_TRAY_NOTIFY, 0, WM_LBUTTONDBLCLK);
    }

    //SendMessage(WM_TRAY_NOTIFY, 0, WM_LBUTTONDBLCLK);

    SetTimer(TIMER_EXPOSE, 100, 0); 
    RegisterHotKey(this->GetSafeHwnd(), m_nHotKeyID + 0, MOD_ALT, 'K');

    return TRUE;  // return TRUE unless you set the focus to a control
    // EXCEPTION: OCX Property Pages should return FALSE
}

HCURSOR
CDlgMain::OnQueryDragIcon() {
    return (HCURSOR) m_hIcon;
}

void
CDlgMain::OnSetTrigger() {
    if (theApp.bIsTrigger) {
        theApp.bIsTrigger = FALSE;
    } else {
        theApp.bIsTrigger = TRUE;
    }

    vSaveConfiguration();
}

void
CDlgMain::OnSetHotKey() {
    if (theApp.bIsHotKey) {
        theApp.bIsHotKey = FALSE;
    } else {
        theApp.bIsHotKey = TRUE;
    }

    vSaveConfiguration();
}

void
CDlgMain::vLoadConfiguration() {
                USES_CONVERSION;

    HKEY        hMiniKey;
    DWORD       Disposition;
    DWORD       err;
    UCHAR       szValue[MAX_PATH];
    DWORD       cbValue;
    DWORD       dwType; 

    WCHAR       keyname[MAX_PATH];

    wsprintf(keyname, TEXT("Software\\WKM2"));

    err = RegCreateKeyEx(HKEY_CURRENT_USER, keyname, 0, TEXT(""),
                         REG_OPTION_NON_VOLATILE, KEY_WRITE | KEY_READ, NULL,
                         &hMiniKey, &Disposition);  

    if (ERROR_SUCCESS == err) {
        cbValue = sizeof(DWORD);
        err = RegQueryValueEx(hMiniKey, TEXT("KeyboardStyle"), NULL, &dwType,
                              szValue, &cbValue);
        if (ERROR_SUCCESS == err) {
            theApp.keyboard_style = *((DWORD *) szValue);
        } else {
            theApp.keyboard_style = STYLE_IPAD;
        }

        cbValue = sizeof(DWORD);
        err = RegQueryValueEx(hMiniKey, TEXT("bIsTrigger"), NULL, &dwType,
                              szValue, &cbValue);
        if (ERROR_SUCCESS == err) {
            theApp.bIsTrigger = *((DWORD *) szValue);
        } else {
            theApp.bIsTrigger = 1;
        }

        cbValue = sizeof(DWORD);
        err = RegQueryValueEx(hMiniKey, TEXT("bIsHotKey"), NULL, &dwType,
                              szValue, &cbValue);
        if (ERROR_SUCCESS == err) {
            theApp.bIsHotKey = *((DWORD *) szValue);
        } else {
            theApp.bIsHotKey = 1;
        }

        cbValue = sizeof(DWORD);
        err = RegQueryValueEx(hMiniKey, TEXT("iBackgroundMode"), NULL,
                              &dwType, szValue, &cbValue);
        if (ERROR_SUCCESS == err) {
            theApp.iBackgroundMode = *((DWORD *) szValue);
        } else {
            theApp.iBackgroundMode = 0;
        }

        cbValue = MAX_PATH;
        err = RegQueryValueEx(hMiniKey, TEXT("m_background_file"), NULL,
                              &dwType, szValue, &cbValue);
        if (ERROR_SUCCESS == err) {
            wcscpy(theApp.m_background_file, (WCHAR *) szValue);
        } else {
            memset(theApp.m_background_file, 0x00,
                   sizeof(theApp.m_background_file));
        }
    }

    RegCloseKey(hMiniKey);
}

void
CDlgMain::vSaveConfiguration() {
                USES_CONVERSION;

    HKEY        hMiniKey;
    DWORD       Disposition;
    DWORD       err;
    UCHAR       szValue[MAX_PATH];
    DWORD       cbValue;
    DWORD       dwType; 

    WCHAR       keyname[MAX_PATH];

    wsprintf(keyname, TEXT("Software\\WKM2"));

    err = RegCreateKeyEx(HKEY_CURRENT_USER, keyname, 0, TEXT(""),
                         REG_OPTION_NON_VOLATILE, KEY_WRITE | KEY_READ, NULL,
                         &hMiniKey, &Disposition);  

    if (ERROR_SUCCESS == err) {
        dwType = REG_DWORD;
        cbValue = sizeof(DWORD);
        *((DWORD *) &szValue) = theApp.keyboard_style;

        RegSetValueEx(hMiniKey, TEXT("KeyboardStyle"), 0, dwType, szValue,
                      cbValue);

        dwType = REG_DWORD;
        cbValue = sizeof(DWORD);
        *((DWORD *) &szValue) = theApp.bIsTrigger;

        RegSetValueEx(hMiniKey, TEXT("bIsTrigger"), 0, dwType, szValue,
                      cbValue);

        dwType = REG_DWORD;
        cbValue = sizeof(DWORD);
        *((DWORD *) &szValue) = theApp.bIsHotKey;

        RegSetValueEx(hMiniKey, TEXT("bIsHotKey"), 0, dwType, szValue, cbValue);


        dwType = REG_DWORD;
        cbValue = sizeof(DWORD);
        *((DWORD *) &szValue) = theApp.iBackgroundMode;

        RegSetValueEx(hMiniKey, TEXT("iBackgroundMode"), 0, dwType, szValue,
                      cbValue);

        dwType = REG_SZ;
        cbValue = (wcslen(theApp.m_background_file) + 1) * 2;
        wcscpy((WCHAR *) szValue, theApp.m_background_file);

        RegSetValueEx(hMiniKey, TEXT("m_background_file"), 0, dwType, szValue,
                      cbValue);
    }

    RegCloseKey(hMiniKey);
}

void
CDlgMain::OnSetBGDefault() {
    theApp.iBackgroundMode = 0;
    vSaveConfiguration();
}

void
CDlgMain::OnSetBGCurrent() {
    theApp.iBackgroundMode = 1;
    vSaveConfiguration();
}

void
CDlgMain::OnSetBGCustom() {
    static WCHAR BASED_CODE     szFilter[]      = TEXT("BMP Files (*.bmp)|*.bmp||");
    CFileDialog                 *mydlg          = new CFileDialog(TRUE, NULL,
                                                                  NULL,
                                                                  OFN_HIDEREADONLY |
                                                                  OFN_OVERWRITEPROMPT,
                                                                  szFilter,
                                                                  NULL);

    if (mydlg->DoModal() != IDOK) {
        return;
    }   

    theApp.iBackgroundMode = 2;
    wcscpy(theApp.m_background_file, mydlg->m_ofn.lpstrFile);
    vSaveConfiguration();

    delete mydlg;
}


void
CDlgMain::OnSetAutorun() {
    HKEY        hMiniKey;
    DWORD       err;
    DWORD       Disposition;

    err = RegCreateKeyEx(HKEY_CURRENT_USER,
                         TEXT("Software\\Microsoft\\Windows\\CurrentVersion\\Run"),
                         0, TEXT(""), REG_OPTION_NON_VOLATILE,
                         KEY_WRITE |
                         KEY_READ |
                         KEY_WOW64_64KEY, NULL,
                         &hMiniKey, &Disposition);

    if (ERROR_SUCCESS != err) {
        return;
    }

    if (theApp.bIsAutorun) {
        err = RegDeleteValue(hMiniKey, TEXT("WKM2"));
        theApp.bIsAutorun = FALSE;
    } else {
        WCHAR   tgtfile[MAX_PATH];

        wsprintf(tgtfile, TEXT("\"%s\\WKM2.exe\" -noautorun"),
                 theApp.localdatapath);

        RegSetValueEx(hMiniKey, TEXT("WKM2"), 0, REG_SZ,
                      (unsigned char *) tgtfile, _tcslen(tgtfile) * 2);
    }


    RegCloseKey(hMiniKey);
    return;
}


void
CDlgMain::vCheckAutorun() {
    HKEY        hMiniKey;
    DWORD       err;
    DWORD       Disposition;
    UCHAR       szValue[MAX_PATH];
    DWORD       cbValue = MAX_PATH;
    DWORD       dwType;

    theApp.bIsAutorun = FALSE;

    err = RegCreateKeyEx(HKEY_CURRENT_USER,
                         TEXT("Software\\Microsoft\\Windows\\CurrentVersion\\Run"),
                         0, TEXT(""), REG_OPTION_NON_VOLATILE,
                         KEY_WRITE |
                         KEY_READ |
                         KEY_WOW64_64KEY, NULL,
                         &hMiniKey, &Disposition);

    if (ERROR_SUCCESS != err) {
        return;
    }

    err = RegQueryValueEx(hMiniKey, TEXT("WKM2"), NULL, &dwType, szValue,
                          &cbValue);

    if (ERROR_SUCCESS != err) {
        RegCloseKey(hMiniKey);
        return;
    }

    RegCloseKey(hMiniKey);

    theApp.bIsAutorun = TRUE;
    return;
}


LONG
CDlgMain::OnTrayNotify(WPARAM wParam, LPARAM lParam) {
    if (bIsDlgOpened) {
        return TRUE;
    }

    switch (lParam) {
      case WM_RBUTTONDOWN:
         {
            vCheckAutorun();

            CMenu       mainmenu;
            mainmenu.CreatePopupMenu(); 
#if 0
            theApp.m_Msg.LoadString(IDS_AUTORUN);
            if (theApp.bIsAutorun) {
                mainmenu.AppendMenu(MF_STRING | MF_CHECKED, IDM_SET_AUTORUN,
                                    theApp.m_Msg);
            } else {
                mainmenu.AppendMenu(MF_STRING, IDM_SET_AUTORUN, theApp.m_Msg);
            }
#endif
            if (theApp.bIsTrigger) {
                theApp.m_Msg.LoadString(IDS_TRIGGER_ENABLE);
                mainmenu.AppendMenu(MF_STRING | MF_CHECKED, IDM_SET_TRIGGER,
                                    theApp.m_Msg);
            } else {
                theApp.m_Msg.LoadString(IDS_TRIGGER_DISABLE);
                mainmenu.AppendMenu(MF_STRING, IDM_SET_TRIGGER, theApp.m_Msg);
            }

            if (theApp.bIsHotKey) {
                theApp.m_Msg.LoadString(IDS_HOTKEY_ENABLE);
                mainmenu.AppendMenu(MF_STRING | MF_CHECKED, IDM_SET_HOTKEY,
                                    theApp.m_Msg);
            } else {
                theApp.m_Msg.LoadString(IDS_HOTKEY_DISABLE);
                mainmenu.AppendMenu(MF_STRING, IDM_SET_HOTKEY, theApp.m_Msg);
            }

            UINT        mode;

            CMenu       bg_menu;
            bg_menu.CreateMenu();

            if (theApp.iBackgroundMode == 0) {
                mode = MF_STRING | MF_CHECKED;
            } else {
                mode = MF_STRING;
            }
            theApp.m_Msg.LoadString(IDS_BG_DEFAULT);
            bg_menu.AppendMenu(mode, IDM_BG_DEFAULT, theApp.m_Msg);

            if (theApp.iBackgroundMode == 1) {
                mode = MF_STRING | MF_CHECKED;
            } else {
                mode = MF_STRING;
            }            
            theApp.m_Msg.LoadString(IDS_BG_CURRENT);
            bg_menu.AppendMenu(mode, IDM_BG_CURRENT, theApp.m_Msg);

            if (theApp.iBackgroundMode == 2) {
                mode = MF_STRING | MF_CHECKED;
            } else {
                mode = MF_STRING;
            }            
            theApp.m_Msg.LoadString(IDS_BG_CUSTOM);
            bg_menu.AppendMenu(mode, IDM_BG_CUSTOM, theApp.m_Msg);

            theApp.m_Msg.LoadString(IDS_BACKGROUND);
            mainmenu.AppendMenu(MF_STRING | MF_POPUP, (UINT) bg_menu.m_hMenu,
                                theApp.m_Msg);

            theApp.m_Msg.LoadString(IDS_CLOSE);

            mainmenu.AppendMenu(MF_STRING, IDCANCEL, theApp.m_Msg);

            POINT       pt ;
            GetCursorPos(&pt);
            SetForegroundWindow();
            mainmenu.TrackPopupMenu(TPM_LEFTALIGN | TPM_LEFTBUTTON, pt.x,
                                    pt.y, this);         

            break ;
        }
      case WM_LBUTTONDBLCLK:
         {
            bIsDlgOpened = TRUE;

            BOOL        r_value;

            r_value = bIsBTHarewareExist();
            if (r_value) {
#ifdef NO_THREAD_DESKTOP
                CIVTKeyboardDlg dlg;
                dlg.DoModal();
#else

                theApp.bContinueDlg = FALSE;

                THREAD_DATA     td;
                wcscpy(td.szDesktopName, TEXT("IVTKEYBOARD_DESKTOP"));
                Thread_Desktop(ThreadFunc, (THREAD_DATA *) &td);

                while (theApp.bContinueDlg == TRUE) {
                    Thread_Desktop(ThreadFunc, (THREAD_DATA *) &td);
                }

#endif                          

                if (!bIsBTHarewareExist()) {
                    vStopSniffer();
                    bCloseDevice();

                    OnCancel();
                }
            } else {
                // Check if device exist
                r_value = bOpenBTHardware();
                if (r_value) {
                    BYTE        hardware_address[512];          

                    r_value = bGetBTHardwareAddress(hardware_address);
                    bCloseBTHardware();

                    if (!r_value) {
                        bIsDlgOpened = FALSE;
                        return 0;
                    }

                    // Open BT Stack
                    int         n_device;
                    n_device = iOpenDevice();                                   

                    if (n_device > 0) {
                        vStartSniffer(AfxGetInstanceHandle());

#ifdef NO_THREAD_DESKTOP                                                

                        CIVTKeyboardDlg dlg;
                        dlg.DoModal();
#else
                        theApp.bContinueDlg = FALSE;

                        THREAD_DATA     td;
                        wcscpy(td.szDesktopName, TEXT("IVTKEYBOARD_DESKTOP"));
                        while (theApp.bContinueDlg == TRUE) {
                            Thread_Desktop(ThreadFunc, (THREAD_DATA *) &td);
                        }
#endif

                        if (!bIsBTHarewareExist()) {
                            bCloseDevice();

                            OnCancel();
                        }
                    }
                }
            }

            vSaveConfiguration();
            bIsDlgOpened = FALSE;
        }
        break;
    }
    return (0) ;
}

void
CDlgMain::OnTimer(UINT nIDEvent) {
    if (nIDEvent == TIMER_EXPOSE) {
        if (!theApp.bIsTrigger) {
            return;
        }

        POINT   pt ;

        GetCursorPos(&pt);

        if (pt.x == (theApp.m_ScreenWidth - 1)) {
            if (!bIsDlgOpened) {
                KillTimer(TIMER_EXPOSE);
                PostMessage(WM_TRAY_NOTIFY, 0, WM_LBUTTONDBLCLK);
                SetTimer(TIMER_EXPOSE, 100, 0);
            }
        }
    }
}

void
CDlgMain::OnCancel() {
    // TODO: Add extra cleanup here
#if 0
    CDlgUninstall       dlg;

    if(dlg.DoModal()!=IDOK){
                return;
        }

    if (dlg.m_uninstall) {
                OnSetAutorun();
        theApp.m_Msg.LoadString(IDS_UNINSTALLED);
        MessageBox(theApp.m_Msg, APNAME, MB_OK | MB_ICONINFORMATION);
    }
#else
        theApp.bIsAutorun = TRUE;
        OnSetAutorun();
#endif


    if (m_pTray) {
        delete m_pTray;
        m_pTray = NULL;
    }

#if 1

    vStopSniffer();

    if (bIsBTHarewareExist()) {
        bSetLed(0);
		bBTDisconnect();		
    }

    bCloseDevice();

#endif  

    UnregisterHotKey(this->GetSafeHwnd(), m_nHotKeyID + 0);

    CDialog::OnCancel();
}


LRESULT
CDlgMain::OnHotKey(WPARAM wParam, LPARAM lParam) {
    if (theApp.bIsHotKey) {
        long    key     = lParam;
        SendMessage(WM_TRAY_NOTIFY, 0, WM_LBUTTONDBLCLK);
    }

    return 0;
}

BOOL
CDlgMain::PreTranslateMessage(MSG *pMsg) {
    /*
       if ((pMsg->message == WM_HOTKEY) && (pMsg->wParam == m_nHotKeyID)) {
           OutputDebugString(TEXT("Alt+K\r\n"));
        SendMessage(WM_TRAY_NOTIFY, 0, WM_LBUTTONDBLCLK);
           return TRUE;
       }
    */

    return CDialog::PreTranslateMessage(pMsg);
}


BOOL
CDlgMain::OnDeviceChange(UINT nEventType, DWORD dwData) {
    if (nEventType == DBT_DEVICEREMOVECOMPLETE) {
        if (!bIsBTHarewareExist()) {
            bCloseDevice();
            OutputDebugString(TEXT(""));
			OnCancel();
        }
    } 
    return true;
}

LRESULT
CDlgMain::OnTaskBarCreated(WPARAM wParam, LPARAM lParam) {
    if (m_pTray) {
        delete m_pTray;
        m_pTray = NULL;
    }

    m_pTray = new CTrayNot(this, WM_TRAY_NOTIFY, NULL, theApp.m_pIconList);
    m_pTray->SetTip();    

    return true;
}

void
CDlgMain::OnWindowPosChanging(WINDOWPOS FAR *lpwndpos) {
    lpwndpos->flags &= ~SWP_SHOWWINDOW;
    CDialog::OnWindowPosChanging(lpwndpos);
}


LRESULT
CDlgMain::OnPowerBroadcast(WPARAM wParam, LPARAM lParam) {
    if (wParam == PBT_APMSUSPEND) {
        bCloseDevice();
    }

    return TRUE;
}


int WINAPI
Thread_Desktop(LPTHREAD_START_ROUTINE ThreadFunc, THREAD_DATA *td) {
    // Capture Background Now
    HBITMAP     hBitmapTemp;
    HANDLE      dib2Write;
    RECT        rectWnd; 
    ::GetWindowRect(GetDesktopWindow(), &rectWnd);    
    hBitmapTemp = CopyScreenToBitmap(&rectWnd);

    dib2Write = DDBToDIB(hBitmapTemp, BI_RGB, NULL);
    WriteDIB(theApp.tempfile, dib2Write);       
    DeleteObject(hBitmapTemp);    

    if (theApp.iBackgroundMode == 1) {
        ::SystemParametersInfo(SPI_SETDESKWALLPAPER, MAX_PATH,
                               theApp.tempfile, 0);
    } else if (theApp.iBackgroundMode == 2) {
        ::SystemParametersInfo(SPI_SETDESKWALLPAPER, MAX_PATH,
                               theApp.m_background_file, 0);
    }

    HDESK       hOriginalThread;
    HDESK       hOriginalInput;
    HDESK       hNewDesktop;

    // Save original ...
    hOriginalThread = GetThreadDesktop(GetCurrentThreadId());
    hOriginalInput = OpenInputDesktop(0, FALSE, DESKTOP_SWITCHDESKTOP);

    // Create a new Desktop and switch to it
    hNewDesktop = CreateDesktop(td->szDesktopName, NULL, NULL, 0, GENERIC_ALL,
                                NULL);
    SetThreadDesktop(hNewDesktop);
    SwitchDesktop(hNewDesktop);

    // Execute thread in new desktop
    td->hDesk = hNewDesktop;
    StartThread(ThreadFunc, td);

    // Restore original ...
    SwitchDesktop(hOriginalInput);
    SetThreadDesktop(hOriginalThread);

    ::SystemParametersInfo(SPI_SETDESKWALLPAPER, MAX_PATH, theApp.oldfile, 0);

    // Close the Desktop
    //CloseDesktop(hNewDesktop);  
    return 0;
}

BOOL
StartThread(LPTHREAD_START_ROUTINE ThreadFunc, THREAD_DATA *td) {
    ULONG       tid;
    HANDLE      hThread;

    // Create and start thread
    hThread = CreateThread(NULL, 0, ThreadFunc, td, 0, &tid);
    if (!hThread) {
        return FALSE;
    }

    // Wait until thread terminates
    WaitForSingleObject(hThread, INFINITE);

    CloseHandle(hThread);

    return TRUE;
}

DWORD WINAPI
ThreadFunc(void *pData) {
    SetThreadDesktop(((THREAD_DATA *) pData)->hDesk);

    theApp.vSetApLang();

    CIVTKeyboardDlg     dlg;
    theApp.m_pMainWnd = &dlg;
    dlg.DoModal();
    return 0;
}

