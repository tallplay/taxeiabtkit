#ifndef __RIJNDAEL_API__
#define __RIJNDAEL_API__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include "rijndael-alg.h"

    /*  Generic Defines  */
#define     DIR_ENCRYPT           0 /*  Are we encrpyting?  */
#define     DIR_DECRYPT           1 /*  Are we decrpyting?  */
#define     MODE_ECB              1 /*  Are we ciphering in ECB mode?   */
#define     MODE_CBC              2 /*  Are we ciphering in CBC mode?   */
#define     MODE_CFB1             3 /*  Are we ciphering in 1-bit CFB mode? */
#define     MODE_CFB8             4 /*  Are we ciphering in 8-bit CFB mode? */
#define     MODE_OFB              5 /*  Ciphering in OFB mode  */
#define     MODE_CTR              6 /*  Ciphering in CTR mode  */
#define     MODE_CBC_MAC          7 /*  Ciphering in CBC-MAC mode  */
#define     TRUE                  1
#define     FALSE                 0
#define     BUFFER_SIZE     500*128 /*  64k bytes */
#define     BITSPERBLOCK        128 /* Default number of bits in a cipher block */

    /*  Error Codes  */
#define     BAD_MODE             -1 /*  mode is invalid, e.g., unknown value */
#define     BAD_KEY              -2 /*  Key is not of correct format */
#define     BAD_KEY_INSTANCE     -3 /*  KeyInstance is not valid */
#define     BAD_KEY_DIR          -4 /*  Params struct passed to cipherInit invalid */
#define     BAD_CIPHER_STATE     -5 /*  Cipher in wrong state (e.g., not initialized) */
#define     BAD_BLOCK_LENGTH     -6
#define     BAD_ASCII_STRING     -7
#define     BAD_DATA             -8 /*  Data contents are invalid, e.g., invalid padding */
#define     BAD_OTHER            -9 /*  Unknown error */


    /*  Typedefs  */

    typedef unsigned char       BYTE;



    /*  Function prototypes  */

    BYTE *ascii2binary(int inputLen, char *input);

    int rijndaelInit(keyInstance *keyInst, int keyLen, u8 *key);

    int rijndael_block_encrypt(int mode, BYTE *iv, keyInstance *key,
                               BYTE *input, int inputLen, BYTE *outBuffer);

    int rijndael_pad_encrypt(int mode, BYTE *iv, keyInstance *key,
                             BYTE *input, int inputLen, BYTE *outBuffer);

    int rijndael_block_decrypt(int mode, BYTE *iv, keyInstance *key,
                               BYTE *input, int inputLen, BYTE *outBuffer);

    int rijndael_pad_decrypt(int mode, BYTE *iv, keyInstance *key,
                             BYTE *input, int inputLen, BYTE *outBuffer);

    int rijndael_file_encrypt(int mode, unsigned char *iv, unsigned char *key,
                              int keyLength, const char *inputFileName,
                              const char *outputFileName);

    int rijndael_file_decrypt(int mode, unsigned char *iv, unsigned char *key,
                              int keyLength, const char *inputFileName,
                              const char *outputFileName);

    int rijndael_ecb_encrypt(keyInstance *keyInst, unsigned char *input,
                             int inputLen, int padding,
                             unsigned char *outBuffer);

    int rijndael_ecb_decrypt(keyInstance *keyInst, unsigned char *input,
                             int inputLen, int padding,
                             unsigned char *outBuffer);

    int rijndael_cbc_encrypt(keyInstance *keyInst, unsigned char *iv,
                             unsigned char *input, int inputLen, int padding,
                             unsigned char *outBuffer);

    int rijndael_cbc_decrypt(keyInstance *keyInst, unsigned char *iv,
                             unsigned char *input, int inputLen, int padding,
                             unsigned char *outBuffer);

    int rijndael_cfb1_encrypt(keyInstance *keyInst, unsigned char *iv,
                              unsigned char *input, int inputLen,
                              unsigned char *outBuffer);

    int rijndael_cfb1_decrypt(keyInstance *keyInst, unsigned char *iv,
                              unsigned char *input, int inputLen,
                              unsigned char *outBuffer);

    int rijndael_cfb8_encrypt(keyInstance *keyInst, unsigned char *iv,
                              unsigned char *input, int inputLen,
                              unsigned char *outBuffer);

    int rijndael_cfb8_decrypt(keyInstance *keyInst, unsigned char *iv,
                              unsigned char *input, int inputLen,
                              unsigned char *outBuffer);

    int rijndael_ofb_encrypt(keyInstance *keyInst, unsigned char *iv,
                             unsigned char *input, int inputLen, int padding,
                             unsigned char *outBuffer);

    int rijndael_ofb_decrypt(keyInstance *keyInst, unsigned char *iv,
                             unsigned char *input, int inputLen, int padding,
                             unsigned char *outBuffer);

    int rijndael_ctr_encrypt(keyInstance *keyInst, unsigned char *iv,
                             unsigned char *input, int inputLen, int padding,
                             unsigned char *outBuffer);

    int rijndael_ctr_decrypt(keyInstance *keyInst, unsigned char *iv,
                             unsigned char *input, int inputLen, int padding,
                             unsigned char *outBuffer);

    int rijndael_cbc_mac_encrypt(keyInstance *keyInst, unsigned char *iv,
                                 unsigned char *input, int inputLen,
                                 int padding, unsigned char *outBuffer);

    int rijndael_cbc_mac_decrypt(keyInstance *keyInst, unsigned char *iv,
                                 unsigned char *input, int inputLen,
                                 int padding, unsigned char *outBuffer);

#ifdef INTERMEDIATE_VALUE_KAT
    int cipherUpdateRounds(int dir, keyInstance *key, BYTE *input,
                           int inputLen, BYTE *outBuffer, int Rounds);
#endif /* INTERMEDIATE_VALUE_KAT */

#ifdef __cplusplus
}
#endif

#endif /* __RIJNDAEL_API__ */
