#ifndef __RIJNDAEL_ALG__
#define __RIJNDAEL_ALG__

#define MAXKC   (256/32)
#define MAXKB   (256/8)
#define BLOCK_SIZE (128/8)
#define MAXNR   14
#define MAX_KEY_SIZE         64 /* # of ASCII char's needed to represent a key */
#define GETU32(pt) (((u32)(pt)[0] << 24) ^ ((u32)(pt)[1] << 16) ^ ((u32)(pt)[2] <<  8) ^ ((u32)(pt)[3]))
#define PUTU32(ct, st) { (ct)[0] = (u8)((st) >> 24); (ct)[1] = (u8)((st) >> 16); (ct)[2] = (u8)((st) >>  8); (ct)[3] = (u8)(st); }


typedef unsigned char   u8;     
typedef unsigned short  u16;    
typedef unsigned int    u32;
typedef struct {
    int         keyLen;                          //key 的長度 (bits)
    u8          key[MAX_KEY_SIZE + 1];             //確保足夠的空間存放 key
    int         Nr;                              //回合數目
    u32         ek[4 * (MAXNR + 1)];              //加密用的 round keys
    u32         dk[4 * (MAXNR + 1)];              //解密用的 round keys
} keyInstance;

int rijndaelKeySetupEnc(keyInstance *keyInst);
int rijndaelKeySetup(keyInstance *keyInst);
void rijndaelEncrypt(keyInstance *keyInst, const u8 pt[16], u8 ct[16]);
void rijndaelDecrypt(keyInstance *keyInst, const u8 ct[16], u8 pt[16]);

#ifdef INTERMEDIATE_VALUE_KAT
void rijndaelEncryptRound(keyInstance *keyInst, u8 block[16], int rounds);
void rijndaelDecryptRound(keyInstance *keyInst, u8 block[16], int rounds);
#endif /* INTERMEDIATE_VALUE_KAT */

#endif /*__RIJNDAEL_ALG__*/
