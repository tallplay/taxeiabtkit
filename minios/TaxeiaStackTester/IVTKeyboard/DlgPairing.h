#if !defined(AFX_DLGPAIRING_H__EDD4EDF7_E807_457C_B300_3657AFF7B596__INCLUDED_)
#define AFX_DLGPAIRING_H__EDD4EDF7_E807_457C_B300_3657AFF7B596__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgPairing.h : header file
//

enum {
	BT_ACTION_DISCONNECT,
	BT_ACTION_CONNECT,
	BT_ACTION_PAIR,
	BT_ACTION_UNPAIR	
};

/////////////////////////////////////////////////////////////////////////////
// CDlgPairing dialog

class CDlgPairing : public CDialog
{
// Construction
public:
	CDlgPairing(CWnd* pParent = NULL);   // standard constructor
	CBrush              *m_pWhiteBkBrush;	

	int					iBTAction;
	int					iBTIndex;

// Dialog Data
	//{{AFX_DATA(CDlgPairing)
	enum { IDD = IDD_DIALOG_PAIRING };
	CStatic	m_control_status;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgPairing)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDlgPairing)
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnTimer(UINT nIDEvent);
	virtual BOOL OnInitDialog();
	virtual void OnCancel();
	afx_msg void OnPaint();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGPAIRING_H__EDD4EDF7_E807_457C_B300_3657AFF7B596__INCLUDED_)
