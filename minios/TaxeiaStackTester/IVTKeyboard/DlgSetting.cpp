// DlgSetting.cpp : implementation file
//

#include "stdafx.h"
#include "IVTKeyboard.h"
#include "DlgSetting.h"
#include "Color.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDlgSetting dialog


CDlgSetting::CDlgSetting(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgSetting::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgSetting)
	m_keyboard_style = theApp.keyboard_style;
	//}}AFX_DATA_INIT
	m_pWhiteBkBrush = new CBrush(WHITE);
}


void CDlgSetting::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgSetting)
	DDX_Radio(pDX, IDC_RADIO1, m_keyboard_style);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgSetting, CDialog)
	//{{AFX_MSG_MAP(CDlgSetting)
	ON_WM_CTLCOLOR()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgSetting message handlers

HBRUSH CDlgSetting::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) 
{
	switch (nCtlColor) {
      case CTLCOLOR_STATIC:      
	  case CTLCOLOR_BTN:
        pDC->SetBkColor(WHITE);
        return (HBRUSH) (m_pWhiteBkBrush->GetSafeHandle());

      default:
        return CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
    }
}
