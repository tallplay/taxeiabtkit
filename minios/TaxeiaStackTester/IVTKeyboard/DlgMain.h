#if !defined(AFX_DLGMAIN_H__EC5B0949_0CF3_4D5D_B259_E6CFEBCA4D3D__INCLUDED_)
#define AFX_DLGMAIN_H__EC5B0949_0CF3_4D5D_B259_E6CFEBCA4D3D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgMain.h : header file
//
#include "traynot.h"

/////////////////////////////////////////////////////////////////////////////
// CDlgMain dialog

class CDlgMain : public CDialog {
    // Construction
  public:
                        CDlgMain(CWnd *pParent = NULL);   // standard constructor
    CTrayNot            *m_pTray;
    BOOL                bIsDlgOpened;
    	
	void				vCheckAutorun();

	void				vLoadConfiguration();
	void				vSaveConfiguration();
	
    // Dialog Data
    //{{AFX_DATA(CDlgMain)
    enum { IDD = IDD_MAIN_DIALOG };
        // NOTE: the ClassWizard will add data members here
    //}}AFX_DATA


    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CDlgMain)
    protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreTranslateMessage(MSG* pMsg);
    //}}AFX_VIRTUAL

    // Implementation
  protected:
    HICON               m_hIcon;
    // Generated message map functions
    //{{AFX_MSG(CDlgMain)
    afx_msg void OnPaint();
    virtual BOOL OnInitDialog();
    afx_msg LONG OnTrayNotify ( WPARAM wParam, LPARAM lParam ) ;
    afx_msg void OnWindowPosChanging(WINDOWPOS FAR* lpwndpos);
    virtual void OnCancel();
    afx_msg void OnTimer(UINT nIDEvent);
    afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnSetTrigger();
	afx_msg void OnSetHotKey();
	afx_msg void OnSetAutorun();
	afx_msg void OnSetBGDefault();
	afx_msg void OnSetBGCurrent();
	afx_msg void OnSetBGCustom();
	afx_msg	LRESULT OnPowerBroadcast(WPARAM wParam, LPARAM lParam);
    //}}AFX_MSG
	LRESULT				OnHotKey(WPARAM wParam, LPARAM lParam);
    afx_msg LRESULT     OnTaskBarCreated(WPARAM wp, LPARAM lp);
    afx_msg BOOL        OnDeviceChange(UINT nEventType, DWORD dwData);                	
                        DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGMAIN_H__EC5B0949_0CF3_4D5D_B259_E6CFEBCA4D3D__INCLUDED_)
