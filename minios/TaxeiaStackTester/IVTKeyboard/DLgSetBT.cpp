// DlgSetBT.cpp : implementation file
//

#include "stdafx.h"
#include "IVTKeyboard.h"
#include "DlgSetBT.h"


#include "Color.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char     THIS_FILE[]     = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDlgSetBT dialog


CDlgSetBT::CDlgSetBT(CWnd *pParent /*=NULL*/) :
    CDialog(CDlgSetBT::IDD, pParent)
{
    //{{AFX_DATA_INIT(CDlgSetBT)
    m_irx = 0;
    //}}AFX_DATA_INIT
    m_pWhiteBkBrush = new CBrush(WHITE);
}


void
CDlgSetBT::DoDataExchange(CDataExchange *pDX) {
    CDialog::DoDataExchange(pDX);
    //{{AFX_DATA_MAP(CDlgSetBT)
        // NOTE: the ClassWizard will add DDX and DDV calls here
    DDX_Control(pDX, IDC_RADIO1, m_control_rx1);
    DDX_Control(pDX, IDC_RADIO2, m_control_rx2);
    DDX_Control(pDX, IDC_RADIO3, m_control_rx3);
    DDX_Control(pDX, IDC_RADIO4, m_control_rx4);
    DDX_Control(pDX, IDC_RADIO5, m_control_rx5);
    DDX_Control(pDX, IDC_RADIO6, m_control_rx6);
    DDX_Control(pDX, IDC_RADIO7, m_control_rx7);
    DDX_Control(pDX, IDC_RADIO8, m_control_rx8);
    DDX_Control(pDX, IDC_RADIO9, m_control_rx9);
    DDX_Control(pDX, IDC_RADIO10, m_control_rx10);
    DDX_Control(pDX, IDC_RADIO11, m_control_rx11);
    DDX_Control(pDX, IDC_RADIO12, m_control_rx12);
    DDX_Control(pDX, IDC_RADIO13, m_control_rx13);
    DDX_Control(pDX, IDC_RADIO14, m_control_rx14);
    DDX_Control(pDX, IDC_RADIO15, m_control_rx15);
    DDX_Control(pDX, IDC_RADIO16, m_control_rx16);
    DDX_Control(pDX, IDC_RADIO17, m_control_rx17);
    DDX_Control(pDX, IDC_RADIO18, m_control_rx18);
    DDX_Control(pDX, IDC_RADIO19, m_control_rx19);
    DDX_Control(pDX, IDC_RADIO20, m_control_rx20);
    DDX_Radio(pDX, IDC_RADIO1, m_irx);
    //}}AFX_DATA_MAP

}

BEGIN_MESSAGE_MAP(CDlgSetBT, CDialog)
        //{{AFX_MSG_MAP(CDlgSetBT)
        ON_BN_CLICKED(IDC_DISCOVERABLE, OnDiscoverable)        
        ON_BN_CLICKED(IDC_UNPAIR, OnUnpair)
	ON_BN_CLICKED(IDU_UNPAIRTHIS, OnUnpairthis)
	ON_WM_CTLCOLOR()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgSetBT message handlers

BOOL
CDlgSetBT::bCheckConnected() {
    BOOL        r_value;
    BOOL        bIsInitializing;
    BOOL        bIsInitialized;
    BOOL        bIsDiscoverable;
    BOOL        bIsConnecting;
    BOOL        bIsConnected;
    BOOL        bIsPinRequest;

    r_value = bBTGetStatus(&bIsInitializing, &bIsInitialized,
                           &bIsDiscoverable, &bIsConnecting, &bIsConnected,
                           &bIsPinRequest);  

    if (bIsConnected) {
        return TRUE;
    } else {
        return FALSE;
    }
}

void
CDlgSetBT::OnDiscoverable() {
    if (bCheckConnected()) {
        theApp.m_Msg.LoadString(IDS_PAIR_WARNING);
        if (MessageBox(theApp.m_Msg, APNAME_STR, MB_YESNO | MB_ICONEXCLAMATION) !=
            IDYES) {
            return;
        }
    }

    EndDialog(IDC_DISCOVERABLE);
    return;
}

BOOL
CDlgSetBT::OnInitDialog() {
    CDialog::OnInitDialog();

    // TODO: Add extra initialization here
    m_control_rx[0] = &m_control_rx1;
    m_control_rx[1] = &m_control_rx2;
    m_control_rx[2] = &m_control_rx3;
    m_control_rx[3] = &m_control_rx4;
    m_control_rx[4] = &m_control_rx5;
    m_control_rx[5] = &m_control_rx6;
    m_control_rx[6] = &m_control_rx7;
    m_control_rx[7] = &m_control_rx8;
    m_control_rx[8] = &m_control_rx9;
    m_control_rx[9] = &m_control_rx10;
    m_control_rx[10] = &m_control_rx11;
    m_control_rx[11] = &m_control_rx12;
    m_control_rx[12] = &m_control_rx13;
    m_control_rx[13] = &m_control_rx14;
    m_control_rx[14] = &m_control_rx15;
    m_control_rx[15] = &m_control_rx16;
    m_control_rx[16] = &m_control_rx17;
    m_control_rx[17] = &m_control_rx18;
    m_control_rx[18] = &m_control_rx19;
    m_control_rx[19] = &m_control_rx20;

    memset(m_bt_address, 0x00, sizeof(m_bt_address));

    m_connected = -1;

    vLoadRXName();

    return TRUE;  // return TRUE unless you set the focus to a control
    // EXCEPTION: OCX Property Pages should return FALSE
}

void
CDlgSetBT::vLoadRXName() {
                USES_CONVERSION;

    int         i;
    int         iNumOfRx;
    BOOL        r_value;
    char        buf[512];
    char        buf2[512];
    WCHAR       hostname_wc[MAX_PATH];

    memset(buf, 0x00, 512);
    r_value = bBTGetPaired(0xFF, buf, buf2);
    if (!r_value) {
        EndDialog(IDCANCEL);
        return;
    }

    BOOL        bIsBTConnected;
    BOOL        bFound;
    char        connected_address[MAX_PATH];

    bIsBTConnected = bCheckConnected();
    if (bIsBTConnected) {
        bBTGetHostAddress(connected_address);
    }   

    iNumOfRx = buf[0];

    bFound = FALSE;

    for (i = 0; i < iNumOfRx; i++) {
        memset(buf, 0x00, 512);
        r_value = bBTGetPaired(i, buf, buf2);
        if (!r_value) {
            EndDialog(IDCANCEL);
            return;
        }

        MultiByteToWideChar(CP_UTF8, 0, buf2, 34, hostname_wc, MAX_PATH);               

        m_control_rx[i]->SetWindowText((WCHAR *) hostname_wc);

        memcpy(m_bt_address[i], buf, 17); 

        if (bIsBTConnected) {
            if (memcmp(connected_address, m_bt_address[i], 17) == 0) {
                m_control_rx[i]->SetCheck(1);
                bFound = TRUE;
                m_connected = i;
            } else {
                m_control_rx[i]->SetCheck(0);
            }
        }
			
		m_control_rx[i]->EnableWindow(TRUE);
	
    }

    if (!bFound) {
        m_control_rx[0]->SetCheck(1);
    }

    if (iNumOfRx > 0) {
        RECT    main_rect;
        RECT    rx_rect;
        ::GetWindowRect(this->GetSafeHwnd(), &main_rect);
        ::GetWindowRect(m_control_rx[iNumOfRx - 1]->GetSafeHwnd(), &rx_rect);

        main_rect.bottom = rx_rect.bottom + 6;

        MoveWindow(&main_rect, TRUE);
    } else {
        GetDlgItem(IDOK)->EnableWindow(FALSE);
        GetDlgItem(IDC_UNPAIR)->EnableWindow(FALSE);

        RECT    main_rect;
        RECT    rx_rect;
        ::GetWindowRect(this->GetSafeHwnd(), &main_rect);
        ::GetWindowRect(GetDlgItem(IDC_UNPAIR)->GetSafeHwnd(), &rx_rect);

        main_rect.bottom = rx_rect.bottom + 10;

        MoveWindow(&main_rect, TRUE);
    }
}

void
CDlgSetBT::OnOK() {
    UpdateData();

    if (m_connected == m_irx) {
        EndDialog(IDCANCEL);
        return;
    }

    EndDialog(IDOK);
    return;
}

HBRUSH
CDlgSetBT::OnCtlColor(CDC *pDC, CWnd *pWnd, UINT nCtlColor) {
    switch (nCtlColor) {
      case CTLCOLOR_DLG:
      case CTLCOLOR_STATIC:
        //case CTLCOLOR_BTN:
        pDC->SetBkColor(WHITE);
        return (HBRUSH) (m_pWhiteBkBrush->GetSafeHandle());

      default:
        return CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
    }
}

void CDlgSetBT::OnUnpairthis() 
{
	theApp.m_Msg.LoadString(IDS_UNPAIR_WARNING);
    if (MessageBox(theApp.m_Msg, APNAME_STR, MB_YESNO | MB_ICONEXCLAMATION) !=
        IDYES) {
        return;
    }
	UpdateData();
    EndDialog(IDU_UNPAIRTHIS);
    return;
}

void
CDlgSetBT::OnUnpair() {
    theApp.m_Msg.LoadString(IDS_CLEAR_WARNING);
    if (MessageBox(theApp.m_Msg, APNAME_STR, MB_YESNO | MB_ICONEXCLAMATION) !=
        IDYES) {
        return;
    }
    EndDialog(IDC_UNPAIR);
    return;
}
