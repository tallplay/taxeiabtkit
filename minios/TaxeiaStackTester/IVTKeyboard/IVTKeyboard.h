// IVTKeyboard.h : main header file for the IVTKEYBOARD application
//

#if !defined(AFX_IVTKEYBOARD_H__75735D48_133A_41F2_885C_E634464A3702__INCLUDED_)
#define AFX_IVTKEYBOARD_H__75735D48_133A_41F2_885C_E634464A3702__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"           // main symbols

#include "SingleInstanceApp.h"
#include "Color.h"
#include "IVTUSB.h"

#define APNAME_STR TEXT("VirtualHID")

enum {
	STYLE_IPAD,
	STYLE_MAC,
    STYLE_WINDOWS,     
    STYLE_ANDROID
};

enum {
    LAYOUT_ANSI,
    LAYOUT_JIS,
};

enum {
    LANG_ENG,
    LANG_CHT,
    LANG_CHS,
    LANG_JP
};

void vLoadConfiguration();
void vSaveConfiguration();
void vLoadConfiguration(char *pAddress);
void vSaveConfiguration(char *pAddress);


/////////////////////////////////////////////////////////////////////////////
// CIVTKeyboardApp:
// See IVTKeyboard.cpp for the implementation of this class
//

class CIVTKeyboardApp : public CSingleInstanceApp {
  public:
                                CIVTKeyboardApp();

    HICON                       m_pIconList[1];

    int                         keyboard_style;
    int                         keyboard_layout;

    CString                     m_Msg;
    int                         m_aplang;
    void                        vSetApLang();
    int                         m_ScreenWidth;
    int                         m_ScreenHeight;

    BOOL                        bContinueDlg;
	BOOL						bIsBTConnected;

    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CIVTKeyboardApp)
    public:
    virtual BOOL InitInstance();
    virtual void WinHelp(DWORD dwData, UINT nCmd = HELP_CONTEXT);
    //}}AFX_VIRTUAL

    // Implementation
	virtual void        OnSecondInstance(UINT, LPARAM);

    //{{AFX_MSG(CIVTKeyboardApp)
        // NOTE - the ClassWizard will add and remove member functions here.
        //    DO NOT EDIT what you see in these blocks of generated code !
    //}}AFX_MSG
                                DECLARE_MESSAGE_MAP()
};

extern CIVTKeyboardApp  theApp;
/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IVTKEYBOARD_H__75735D48_133A_41F2_885C_E634464A3702__INCLUDED_)
