#if !defined(AFX_DLGUNINSTALL_H__C297315F_310C_46CA_9E1D_B5C6D6B117A5__INCLUDED_)
#define AFX_DLGUNINSTALL_H__C297315F_310C_46CA_9E1D_B5C6D6B117A5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgUninstall.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDlgUninstall dialog

class CDlgUninstall : public CDialog
{
// Construction
public:
	CDlgUninstall(CWnd* pParent = NULL);   // standard constructor
	CBrush              *m_pWhiteBkBrush;
// Dialog Data
	//{{AFX_DATA(CDlgUninstall)
	enum { IDD = IDD_DIALOG_UNINSTALL };
	BOOL	m_uninstall;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgUninstall)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDlgUninstall)
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGUNINSTALL_H__C297315F_310C_46CA_9E1D_B5C6D6B117A5__INCLUDED_)
