// Color.h
// Colorref's

#define RED     RGB(255,  0,  0)
#define GREEN   RGB(  0,255,  0)
#define BLUE    RGB(  0,  0,255)
#define BLACK   RGB(  0,  0,  0)
#define WHITE   RGB(255,255,255)
#define LIGHT_GRAY  RGB(245,245,245)

#define GRAY    RGB(212,208,200)
#define YELLOW  RGB(255,255,0)

#define GRAY_BACKGROUND		RGB(128, 128, 128)

#define BRIGHT_BACKGROUND	RGB(160, 160, 160)
//#define DARK_BACKGROUND		RGB(216, 216, 216)
#define DARK_BACKGROUND		RGB(204, 204, 204)

#define DARK_TEXT			RGB(172,168,153)