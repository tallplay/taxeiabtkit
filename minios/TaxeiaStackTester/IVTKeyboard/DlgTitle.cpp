// DlgTitle.cpp : implementation file
//

#include "stdafx.h"
#include "ivtkeyboard.h"
#include "DlgTitle.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char     THIS_FILE[]     = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDlgTitle dialog


CDlgTitle::CDlgTitle(CWnd *pParent /*=NULL*/) :
    CDialog(CDlgTitle::IDD, pParent)
{
    //{{AFX_DATA_INIT(CDlgTitle)
        // NOTE: the ClassWizard will add member initialization here
    //}}AFX_DATA_INIT

}


void
CDlgTitle::DoDataExchange(CDataExchange *pDX) {
    CDialog::DoDataExchange(pDX);
    //{{AFX_DATA_MAP(CDlgTitle)
	DDX_Control(pDX, IDC_STATIC_HINT, m_control_hint);
	//}}AFX_DATA_MAP

}


BEGIN_MESSAGE_MAP(CDlgTitle, CDialog)
        //{{AFX_MSG_MAP(CDlgTitle)
        ON_WM_PAINT()
        ON_WM_CTLCOLOR()
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgTitle message handlers

BOOL
CDlgTitle::OnInitDialog() {
    CDialog::OnInitDialog();

    RECT        main_rect;
	RECT		new_rect;

    ::GetWindowRect(this->GetSafeHwnd(), &main_rect);

	new_rect.left = 0;
	new_rect.right = theApp.m_ScreenWidth;	
	new_rect.top = theApp.m_ScreenHeight - 50 - (main_rect.bottom - main_rect.top);
    new_rect.bottom = theApp.m_ScreenHeight - 50 ;

    this->MoveWindow(&new_rect, TRUE);

	ScreenToClient(&new_rect);

	m_control_hint.MoveWindow(&new_rect, TRUE);
	m_control_hint.SetWindowText(TEXT(""));

    return TRUE;  // return TRUE unless you set the focus to a control
    // EXCEPTION: OCX Property Pages should return FALSE
}

void
CDlgTitle::OnPaint() {
    CPaintDC            dc      (this); // device context for painting

    // TODO: Add your message handler code here

    // Do not call CDialog::OnPaint() for painting messages
}

HBRUSH
CDlgTitle::OnCtlColor(CDC *pDC, CWnd *pWnd, UINT nCtlColor) {
    HBRUSH      hbr     = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

    // TODO: Change any attributes of the DC here
    if (nCtlColor == CTLCOLOR_STATIC) {
        pDC->SetTextColor(GREEN);
        pDC->SetBkMode(TRANSPARENT);

        return (HBRUSH) ::GetStockObject(HOLLOW_BRUSH);
        return hbr;
    } else {
        return (HBRUSH) ::GetStockObject(NULL_BRUSH);
    }
}

void	
CDlgTitle::vSetTitle(WCHAR *pTitle){

	ShowWindow(SW_HIDE);

	m_control_hint.SetWindowText(pTitle);	

	InvalidateRect(NULL, FALSE);
    UpdateWindow();

	if(wcslen(pTitle)){
		ShowWindow(SW_SHOW);
	}
/*
	WCHAR buf[MAX_PATH];
	wsprintf(buf, TEXT("vSetTitle:%s\n"), pTitle);
	OutputDebugString(buf);
*/
	//SetTimer(0, 2000, NULL);
}

void CDlgTitle::OnTimer(UINT nIDEvent) 
{
	KillTimer(nIDEvent);

	m_control_hint.SetWindowText(TEXT(""));

	InvalidateRect(NULL, FALSE);
    UpdateWindow();

	ShowWindow(SW_HIDE);
}
