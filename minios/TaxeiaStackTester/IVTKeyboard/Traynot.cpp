#include "stdafx.h"
#include "TrayNot.h"
#include "IVTKeyboard.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char     THIS_FILE[]     = __FILE__;
#endif

//#define AP_NAME "FpApp"
/////////////////////////////////////////////////////////////////////////////
// CTrayNot

CTrayNot::CTrayNot(CWnd *pWnd,
                   UINT uCallbackMessage,
                   LPCTSTR szTip,
                   HICON *pList) {
    // this is only for Windows 95 (or higher)
    m_bEnabled = (GetVersion() & 0xff) >= 4 ;
    if (!m_bEnabled) {
        return ;
    }

    // load up the NOTIFYICONDATA structure
    m_tnd.cbSize = sizeof(NOTIFYICONDATA) ;
    //m_tnd.cbSize = 88;
    m_tnd.hWnd = pWnd->GetSafeHwnd() ;
    m_tnd.uID = 0 ;
    m_tnd.uFlags = NIF_MESSAGE | NIF_ICON | NIF_TIP ;   
    m_tnd.uCallbackMessage = uCallbackMessage;  

    _stprintf(m_tnd.szTip, APNAME_STR);

    m_pIconList = pList ;
    m_tnd.hIcon = m_pIconList[0] ;

    Shell_NotifyIcon(NIM_ADD, &m_tnd);
}

CTrayNot::~CTrayNot() {
    if (m_bEnabled) {
        Shell_NotifyIcon(NIM_DELETE, &m_tnd);
    }
}

void
CTrayNot::SetTip() {
    //strcpy ( m_tnd.szTip, ptip);
    _stprintf(m_tnd.szTip, _T("%s"), APNAME_STR);
    Shell_NotifyIcon(NIM_MODIFY, &m_tnd);
}
