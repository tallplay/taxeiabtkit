// DialogHint.cpp : implementation file
//

#include "stdafx.h"
#include "ivtkeyboard.h"
#include "DialogHint.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char     THIS_FILE[]     = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDialogHint dialog


CDialogHint::CDialogHint(CWnd *pParent /*=NULL*/) :
    CDialog(CDialogHint::IDD, pParent)
{
    //{{AFX_DATA_INIT(CDialogHint)
        // NOTE: the ClassWizard will add member initialization here
    //}}AFX_DATA_INIT
    m_pBkBrush = new CBrush(BRIGHT_BACKGROUND);
}


void
CDialogHint::DoDataExchange(CDataExchange *pDX) {
    CDialog::DoDataExchange(pDX);
    //{{AFX_DATA_MAP(CDialogHint)
	DDX_Control(pDX, IDC_STATIC_HINT2, m_control_hint2);
	DDX_Control(pDX, IDC_STATIC_HINT1, m_control_hint1);
	//}}AFX_DATA_MAP

}


BEGIN_MESSAGE_MAP(CDialogHint, CDialog)
        //{{AFX_MSG_MAP(CDialogHint)
        ON_WM_CTLCOLOR()
        ON_WM_NCPAINT()
        //}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDialogHint message handlers

BOOL
CDialogHint::OnInitDialog() {
    CDialog::OnInitDialog();

    // TODO: Add extra initialization here
    CenterWindow();

	theApp.m_Msg.LoadString(IDS_STOP_MOUSE);
	m_control_hint1.SetWindowText(theApp.m_Msg);
	m_control_hint1.SetBigFont();

	theApp.m_Msg.LoadString(IDS_STOP_MOUSE2);
	m_control_hint2.SetWindowText(theApp.m_Msg);
	m_control_hint2.SetSmallFont();

    return TRUE;  // return TRUE unless you set the focus to a control
    // EXCEPTION: OCX Property Pages should return FALSE
}

void
CDialogHint::vShowWindow(CWnd *parent_hwnd) {
    //CenterWindow(parent_hwnd);
    
	// Mouse cursot to centor of software
	RECT        main_rect;
	RECT		parent_rect;
	RECT		new_rect;

	::GetWindowRect(this->GetSafeHwnd(), &main_rect);
	::GetWindowRect(parent_hwnd->GetSafeHwnd(), &parent_rect);

	int parent_width = parent_rect.right - parent_rect.left;
	int parent_height = parent_rect.bottom - parent_rect.top;
	int main_width = main_rect.right - main_rect.left;
	int main_height = main_rect.bottom - main_rect.top;

	new_rect.left = parent_rect.left + ((parent_width-main_width)/2);
	new_rect.right = parent_rect.right - ((parent_width-main_width)/2);

	if(parent_rect.bottom + main_height > theApp.m_ScreenHeight){
		new_rect.bottom = parent_rect.top;
		new_rect.top = new_rect.bottom - main_height;
	} else {
		new_rect.top = parent_rect.bottom;
		new_rect.bottom = new_rect.top + main_height;
	}


	MoveWindow(&new_rect, TRUE);
	
	int         x       = (new_rect.right + new_rect.left) / 2;
	int         y       = (new_rect.bottom + new_rect.top) / 2 + 23;
	
	SetCursorPos(x, y);

	ShowWindow(SW_SHOW);
}

HBRUSH
CDialogHint::OnCtlColor(CDC *pDC, CWnd *pWnd, UINT nCtlColor) {
    switch (nCtlColor) {
      case CTLCOLOR_DLG:       
      case CTLCOLOR_STATIC:
        pDC->SetBkColor(BRIGHT_BACKGROUND);
        return (HBRUSH) (m_pBkBrush->GetSafeHandle());
        break;
      default:
        return CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
    }
}

void
CDialogHint::OnNcPaint() {
}
