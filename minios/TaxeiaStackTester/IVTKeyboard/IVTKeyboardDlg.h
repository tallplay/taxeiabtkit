// IVTKeyboardDlg.h : header file
//

#if !defined(AFX_IVTKEYBOARDDLG_H__EFA57DF3_86D2_48E0_9910_35EEB2D52404__INCLUDED_)
#define AFX_IVTKEYBOARDDLG_H__EFA57DF3_86D2_48E0_9910_35EEB2D52404__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "traynot.h"
#include "ColorEdit.h"
#include "DlgTitle.h"
#include "DialogHint.h"

/////////////////////////////////////////////////////////////////////////////
// CIVTKeyboardDlg dialog

class CIVTKeyboardDlg : public CDialog {
    // Construction
  public:
                        CIVTKeyboardDlg(CWnd *pParent = NULL);  // standard constructor

    CTrayNot            *m_pTray;
	CBrush              *m_pLightGrayBkBrush;

    CDlgTitle           *pTitleDialog;
	CDialogHint			*pHintDialog;

    BOOL                m_IsButtonPressed[MAX_KEY_VALUE];
    BOOL                m_IsMouseButtonPressed[3];//0: left, 1: right, 2: middle
    BOOL                m_IsButtonMultiline[MAX_KEY_VALUE];
    char                m_KeyValue[MAX_KEY_VALUE];

    BOOL                m_IsMediaKeyChanged;

    BOOL                bIsBTInitialized;;
    BOOL                bIsRXConnected;
    int                 iConnectedHost;

	void				vDrawGradientButton(CDC *pDC, RECT *pRECT, BOOL bConnected, BOOL bPressed);
	void				vDrawGradientEnterButton(CDC *pDC, RECT *pRECT, BOOL bConnected, BOOL bPressed);	

    void                vInitButtonStatus();
    void                vInitKeyValue();
    void                vScanAndSentKey();

    void                vSetKeyboardStyle();

    void                vTestDevice();

    BOOL                bIsDlgOpened;    
	BOOL				bIsMenuOpened;

    int                 x_left;
    int                 y_left;
    int                 z_left; 
    char                button_left;
    BOOL                send_immediately;

    void                OnLeave();

	void				vCreateMenu();

	BOOL				bPassF4;

    // Dialog Data
    //{{AFX_DATA(CIVTKeyboardDlg)
    enum { IDD = IDD_IVTKEYBOARD_DIALOG };    
    //}}AFX_DATA

    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CIVTKeyboardDlg)
    public:
    virtual int DoModal();
    virtual BOOL PreTranslateMessage(MSG* pMsg);
    protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    virtual LRESULT DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam);
    //}}AFX_VIRTUAL

  private:

    // Implementation
  protected:    
    HICON               m_hIcon;
    // Generated message map functions
    //{{AFX_MSG(CIVTKeyboardDlg)
    virtual BOOL OnInitDialog();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
    virtual void OnCancel();
    afx_msg void OnPaint();         
    afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
    afx_msg void OnButton();
    afx_msg void OnMouse();
    afx_msg void OnTimer(UINT nIDEvent);
    afx_msg LRESULT OnPowerBroadcast(WPARAM wParam, LPARAM lParam);
    afx_msg HCURSOR OnQueryDragIcon();
    afx_msg LONG OnTrayNotify ( WPARAM wParam, LPARAM lParam ) ;
    afx_msg void OnTrayAbout(); 
    afx_msg void OnTrayLayout();
    afx_msg void OnTrayStyle();
    afx_msg void OnTrayBTDiscoverable();
    afx_msg void OnTrayBTDisconnect();
    afx_msg void OnTrayBTConnect();
    afx_msg void OnTrayBTDelete();      
    afx_msg void OnTrayBTDeleteAll();
	afx_msg void OnTrayLeave();
    //}}AFX_MSG
    afx_msg LRESULT     OnTaskBarCreated(WPARAM wp, LPARAM lp);
    afx_msg BOOL        OnDeviceChange(UINT nEventType, DWORD dwData); 
                        DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IVTKEYBOARDDLG_H__EFA57DF3_86D2_48E0_9910_35EEB2D52404__INCLUDED_)
