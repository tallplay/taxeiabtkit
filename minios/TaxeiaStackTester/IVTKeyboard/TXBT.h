void WINAPI vTXBT_Init();
BOOL WINAPI bTXBT_OS_init();
void WINAPI vTXBT_OS_Deinit();

BOOL WINAPI bTXBT_BTGetStatus(BOOL *pInitializing, BOOL *pInitialized,
                              BOOL *pDiscoverable, BOOL *pConnecting,
                              BOOL *pConnected, BOOL *pPinRequest);
void WINAPI vTXBT_HIDAPP_SetDiscoverable(BOOL on) ;
BOOL WINAPI bTXBT_BTGetHostName(char *pName);
BOOL WINAPI bTXBT_BTGetHostAddress(char *pAddress);
BOOL WINAPI bTXBT_BTGetPaired(int index, char *pData, char *pData2);
BOOL WINAPI bTXBT_BTUnpairAll();
BOOL WINAPI bTXBT_BTUnpairSingle(int index);
BOOL WINAPI bTXBT_BTConnect(int index);
BOOL WINAPI bTXBT_BTDisconnect();
BOOL WINAPI bTXBT_BTSetPin(unsigned char *pPin, int iPinLen);

BOOL WINAPI bTXBT_DidRegisterSdp(BYTE ansi_iso_jis); //0, 1, 2
BOOL WINAPI bTXBT_DidDeregisterSdp();

BOOL WINAPI bTXBT_SendKey(char metakey, char key1, char key2, char key3, char key4, char key5);
BOOL WINAPI bTXBT_SendMouse(char x, char y, char z, char button);
BOOL WINAPI bTXBT_SendRemote(char byte2, char byte3, char byte4, char byte5);

BOOL WINAPI bTXBT_IsBTHarewareExist();
BOOL WINAPI bTXBT_SetLed(BYTE LedStatus);

BOOL WINAPI bTXBT_MassUart_Open();
BOOL WINAPI bTXBT_MassUart_Close();
BOOL WINAPI bTXBT_MassUart_Execute(int bulkinout, int *transferlength,
                                   char *cdb, char *databuf);