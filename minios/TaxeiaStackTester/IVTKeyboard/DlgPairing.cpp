// DlgPairing.cpp : implementation file
//

#include "stdafx.h"
#include "IVTKeyboard.h"
#include "DlgPairing.h"
#include "Color.h"

#define IDT_CHECK_PAIRED           1000
#define IDT_CHECK_DISCONNECTED 1001
#define IDT_TIMEOUT                        1002

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char     THIS_FILE[]     = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDlgPairing dialog


CDlgPairing::CDlgPairing(CWnd *pParent /*=NULL*/) :
    CDialog(CDlgPairing::IDD, pParent)
{
    //{{AFX_DATA_INIT(CDlgPairing)
        // NOTE: the ClassWizard will add member initialization here
    //}}AFX_DATA_INIT
    m_pWhiteBkBrush = new CBrush(WHITE);
}


void
CDlgPairing::DoDataExchange(CDataExchange *pDX) {
    CDialog::DoDataExchange(pDX);
    //{{AFX_DATA_MAP(CDlgPairing)
    DDX_Control(pDX, IDC_STATIC_STATUS, m_control_status);
    //}}AFX_DATA_MAP

}

BEGIN_MESSAGE_MAP(CDlgPairing, CDialog)
        //{{AFX_MSG_MAP(CDlgPairing)
        ON_WM_CTLCOLOR()
        ON_WM_TIMER()
        ON_WM_PAINT()
        //}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgPairing message handlers

HBRUSH
CDlgPairing::OnCtlColor(CDC *pDC, CWnd *pWnd, UINT nCtlColor) {
    switch (nCtlColor) {
      case CTLCOLOR_DLG:
      case CTLCOLOR_STATIC:
        //case CTLCOLOR_BTN:
        pDC->SetBkColor(WHITE);
        return (HBRUSH) (m_pWhiteBkBrush->GetSafeHandle());

      default:
        return CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
    }
}

void
CDlgPairing::OnTimer(UINT nIDEvent) {
    if (nIDEvent == IDT_CHECK_PAIRED) {
        BOOL    r_value;

        BOOL    bIsInitializing;
        BOOL    bIsInitialized;
        BOOL    bIsDiscoverable;
        BOOL    bIsConnecting;
        BOOL    bIsConnected;
        BOOL    bIsPinRequest;

        bSetLed(0x02);

        r_value = bBTGetStatus(&bIsInitializing, &bIsInitialized,
                               &bIsDiscoverable, &bIsConnecting,
                               &bIsConnected, &bIsPinRequest);

        if (!r_value) {
            KillTimer(IDT_CHECK_PAIRED);
            EndDialog(IDCANCEL);

            bSetLed(0x00);
            return;
        }       

        if (bIsConnected) {
            KillTimer(IDT_CHECK_PAIRED);
            EndDialog(IDOK);

            bSetLed(0x01);
            return;
        }

        if (!bIsConnecting) {
            KillTimer(IDT_CHECK_PAIRED);
            EndDialog(IDOK);

            bSetLed(0x00);
            return;
        }

        if (bIsPinRequest) {
            KillTimer(IDT_CHECK_PAIRED);
            EndDialog(IDD_DIALOG_SETPIN);

            bSetLed(0x00);
            return;
        }
    }

    if (nIDEvent == IDT_CHECK_DISCONNECTED) {
        BOOL    r_value;

        BOOL    bIsInitializing;
        BOOL    bIsInitialized;
        BOOL    bIsDiscoverable;
        BOOL    bIsConnecting;
        BOOL    bIsConnected;
        BOOL    bIsPinRequest;

        r_value = bBTGetStatus(&bIsInitializing, &bIsInitialized,
                               &bIsDiscoverable, &bIsConnecting,
                               &bIsConnected, &bIsPinRequest);


        if (!r_value) {
            KillTimer(IDT_CHECK_PAIRED);
            EndDialog(IDCANCEL);
            return;
        }

        if (!bIsConnected) {
            KillTimer(IDT_CHECK_DISCONNECTED);
            bSetLed(0);

            if (iBTAction == BT_ACTION_CONNECT) {
                theApp.m_Msg.LoadString(IDS_CONNECT);
                m_control_status.SetWindowText(theApp.m_Msg);

                //MessageBox(TEXT("Go Connect"));

                //Sleep(5000);

                if (!bBTConnect(iBTIndex)) {
                    //MessageBox(TEXT("can't detect disconnect"));
                    EndDialog(IDCANCEL);
                    return;
                }
            } else if (iBTAction == BT_ACTION_PAIR) {
                bBTPair(TRUE);
            } else {
                EndDialog(IDOK);
                return;
            }

            SetTimer(IDT_CHECK_PAIRED, 500, 0);
            return;
        }
    }

    if (nIDEvent == IDT_TIMEOUT) {
        KillTimer(IDT_TIMEOUT);
        EndDialog(IDCANCEL);
    }
}

BOOL
CDlgPairing::OnInitDialog() {
    CDialog::OnInitDialog();

    CenterWindow();

    // TODO: Add extra initialization here
    SetTimer(IDT_TIMEOUT, 60*1000, 0);

	if(theApp.m_aplang == LANG_JP){
		if (iBTAction != BT_ACTION_PAIR) {
			m_control_status.ModifyStyle(0,SS_CENTERIMAGE|SS_CENTER);	
		}
	}

    if (iBTAction == BT_ACTION_CONNECT) {
        theApp.m_Msg.LoadString(IDS_DISCONNECT);
        m_control_status.SetWindowText(theApp.m_Msg);
    } else if (iBTAction == BT_ACTION_PAIR) {
        theApp.m_Msg.LoadString(IDS_PAIRING);
        m_control_status.SetWindowText(theApp.m_Msg);
    } else if (iBTAction == BT_ACTION_UNPAIR) {
        theApp.m_Msg.LoadString(IDS_UNPAIRING);
        m_control_status.SetWindowText(theApp.m_Msg);
    } else if (iBTAction == BT_ACTION_DISCONNECT) {
        theApp.m_Msg.LoadString(IDS_DISCONNECT);
        m_control_status.SetWindowText(theApp.m_Msg);
    } else {
        EndDialog(IDCANCEL);
        return TRUE;
    }

    bBTDisconnect();

    SetTimer(IDT_CHECK_DISCONNECTED, 1000, 0);  

    //CenterWindow();

    return TRUE;  // return TRUE unless you set the focus to a control
    // EXCEPTION: OCX Property Pages should return FALSE
}

void
CDlgPairing::OnCancel() {
    KillTimer(IDT_CHECK_PAIRED);
    KillTimer(IDT_CHECK_DISCONNECTED);
    KillTimer(IDT_TIMEOUT);
    EndDialog(IDCANCEL);
    delete m_pWhiteBkBrush;
}
void
CDlgPairing::OnPaint() {
    CPaintDC            dc      (this); // device context for painting 

	/*
    CRect               rect; 
    GetClientRect(rect); 
    dc.SelectObject(GetParent()->GetFont()); 
    dc.SetBkMode(TRANSPARENT); 
    CString     strOutput; 
    GetWindowText(strOutput); 
    CRect       rectText        (rect); 
    rectText.bottom = 0; 
    dc.DrawText(strOutput, strOutput.GetLength(), rectText,
                DT_WORDBREAK | DT_CALCRECT); 
    rectText.OffsetRect(0, (rect.Height() - rectText.Height()) / 2); 
    dc.DrawText(strOutput, strOutput.GetLength(), rectText, DT_WORDBREAK);
	*/
}
