// TXBT.cpp : Defines the entry point for the DLL application.
//

#include "stdafx.h"
#include "TXBT.h"
#define SECURITY_WIN32
#include <Security.h>


#include <stdio.h>

#include "sdkapi\bt_hid_hfp_api.h"

#if defined(__cplusplus)
extern "C" {
#endif

#include "HAL\MassUart.h"
#include "Windows-Porting\LocalStorage.h"

    BYTE RunBtThread(BYTE execute);
    void htoa(BYTE h, char *pData);
    void AssignLinkKeyFileName(char *name);
    void AssignDeviceFileName(char *name);
    DWORD AssignBDAddr(void);
    void AssignBDName(char *name);
    void AssignBDMacName(char *name);
    void MemoAttachName2BtDevice(U8 *bdaddr, U8 *name);
    U8 MemoFindBtDevice(U8 idx, U8 *bdaddr, U8 *name);
    U8 MemoAddBtDevice(U8 *bdaddr);
    U8 MemoDeleteAllBTDevice();
	U8 MemoDeleteBTDevice(U8 idx);

	U8 pad_data_get_report(U8 report_id, U8 len, U8 *dat);
	void Bt__ProfileHIDDSetVidPid(U16 vid, U16 pid);

	U8 ps3_data_set_report(U8 type, U8 report, const U8 *buf, int len);
	U8 ps3_data_get_report(U8 type, U8 report, U8 *buf, int maxlen);

#if defined(__cplusplus)
}
#endif 

#ifndef U8
#define U8 BYTE
#endif

#define BD_ADDR_SIZE                    6
#define MAX_DEVICE                              20
#define MAX_DEVICE_NAME_SIZE    26

typedef struct _TXBT_Status {
    BYTE        bOSInitializing;
    BYTE        bOSInitialized;    
    BYTE        bBTDiscoverable;
    BYTE        bBTConnecting;
    BYTE        bBTConnected;
    BYTE        bBTPinRequest;
} TXBT_Status;

typedef struct _BD_ADDR {
    U8          addr[BD_ADDR_SIZE];
} BD_ADDR;

typedef struct _BtAddrNamePair {
    BD_ADDR             bdAddr;
    char                name[MAX_DEVICE_NAME_SIZE];
}BtAddrNamePair;


BtAddrNamePair  gBANP[MAX_DEVICE];
BYTE            gNumberOfDevices;
TXBT_Status     gTXBT_Status;

BYTE            gTXBT_Last_BT_State;
BYTE            gTXBT_BT_Busy;

BYTE			gJoyStickBuffer[27];
BYTE			gKeyboardBuffer[8];
BYTE			gMouseBuffer[4];
BYTE			gMediaBuffer[4];
BYTE			gSendMouse;

BOOL            bIsBTHardwareAlive;
BOOL            bIsTestingBTHardware;
BOOL			bTestBTHardware();

BOOL			bIsSetLed;
BYTE			gLedStatus;
BOOL			bSetLedStatus();


void WINAPI
vTXBT_Init() {
    memset(&gTXBT_Status, 0x00, sizeof(gTXBT_Status));
    memset(gBANP, 0x00, sizeof(gBANP));
    gNumberOfDevices = 0x00;
    gTXBT_BT_Busy = 0;
    gTXBT_Last_BT_State = 0;
    bIsTestingBTHardware = 0;
    bIsBTHardwareAlive = 0;	
	bIsSetLed = 0;
	gLedStatus = 0;

	memset(gJoyStickBuffer, 0x00, sizeof(gJoyStickBuffer));
	memset(gKeyboardBuffer, 0x00, sizeof(gKeyboardBuffer));
	memset(gMouseBuffer, 0x00, sizeof(gMouseBuffer));
	memset(gMediaBuffer, 0x00, sizeof(gMediaBuffer));

	gSendMouse = 0;
}

BOOL WINAPI
bTXBT_OS_init() {
    memset(&gTXBT_Status, 0x00, sizeof(gTXBT_Status));
    memset(gBANP, 0x00, sizeof(gBANP));    
    gNumberOfDevices = 0x00;
    gTXBT_BT_Busy = 0;
    gTXBT_Last_BT_State = 0;
    bIsTestingBTHardware = 0;
    bIsBTHardwareAlive = 0;	
	bIsSetLed = 0;
	gLedStatus = 0;

	memset(gJoyStickBuffer, 0x00, sizeof(gJoyStickBuffer));
	memset(gKeyboardBuffer, 0x00, sizeof(gKeyboardBuffer));
	memset(gMouseBuffer, 0x00, sizeof(gMouseBuffer));
	memset(gMediaBuffer, 0x00, sizeof(gMediaBuffer));

	gSendMouse = 0;

    gTXBT_Status.bOSInitializing = TRUE;

	bt_init();

    RunBtThread(0); // Start Thread

    return TRUE;
}

void WINAPI
vTXBT_OS_Deinit() {
    RunBtThread(1); // Stop Thread

    memset(&gTXBT_Status, 0x00, sizeof(gTXBT_Status));  
    memset(gBANP, 0x00, sizeof(gBANP));
    gNumberOfDevices = 0x00;
    gTXBT_BT_Busy = 0;
    gTXBT_Last_BT_State = 0;
    bIsTestingBTHardware = 0;
    bIsBTHardwareAlive = 0;	
	bIsSetLed = 0;
	gLedStatus = 0;

	memset(gJoyStickBuffer, 0x00, sizeof(gJoyStickBuffer));
	memset(gKeyboardBuffer, 0x00, sizeof(gKeyboardBuffer));
	memset(gMouseBuffer, 0x00, sizeof(gMouseBuffer));
	memset(gMediaBuffer, 0x00, sizeof(gMediaBuffer));

	gSendMouse = 0;
}

BOOL WINAPI
bTXBT_BTGetStatus(BOOL *pInitializing, BOOL *pInitialized,
                  BOOL *pDiscoverable, BOOL *pConnecting, BOOL *pConnected,
                  BOOL *pPinRequest) {
    memset(&gTXBT_Status, 0x00, sizeof(gTXBT_Status));  

    BYTE        bt_current_state;
    bt_current_state = bt_state();

    switch (bt_current_state) {
      case BT_STA_STOP:
        break;

      case BT_STA_POWERUP:
      case BT_STA_BOOT:
        gTXBT_Status.bOSInitializing = TRUE;
        break;

      case BT_STA_INITILIZED:
        gTXBT_Status.bOSInitialized = TRUE;
        break;

      case BT_STA_PAIRING:
        gTXBT_Status.bOSInitialized = TRUE;
		gTXBT_Status.bBTConnecting = TRUE;
        gTXBT_Status.bBTDiscoverable = TRUE;
        break;

      case BT_STA_CONNECTING:
        gTXBT_Status.bOSInitialized = TRUE;
        gTXBT_Status.bBTConnecting = TRUE;
        break;

      case BT_STA_CONNECTED:
        gTXBT_Status.bOSInitialized = TRUE;
        gTXBT_Status.bBTConnecting = TRUE;
        break;

      case BT_STA_HID_CONNECTED:
      case BT_STA_HFP_CONNECTED:
	  case BT_STA_DISCONNECTING:
        gTXBT_Status.bOSInitialized = TRUE;
        gTXBT_Status.bBTConnected = TRUE;
        break;

      case BT_STA_DISCONNECTED:
	  case BT_STA_DISCONNECT_RETRY:
		gTXBT_Status.bOSInitialized = TRUE;                
        break;

	  case BT_STA_BT_FAIL:
		  //gTXBT_Status.bOSInitialized = TRUE;                
		  //OutputDebugString(TEXT("BT Fail!!"));
		  break;
	  case BT_STA_LOCK:
		  gTXBT_Status.bOSInitialized = TRUE;
		  //OutputDebugString(TEXT("Authorization expired, BT Lock, please reset HW!!"));
		  break;			 	
    };  

	if(gTXBT_Last_BT_State <= BT_STA_BOOT){
		if(bt_current_state > BT_STA_BOOT){
			unsigned char name[256];
			
			//set our name
			AssignBDName((char*)name);
			bt_gap_set_local_name(name);
			//bt_gap_preset_local_name(name);

			U8      idx;
			U8      bdaddr[6];
			U8      bdname[26];
			
			idx = 0;
			while (idx < MAX_DEVICE) {
				memset(bdaddr, 0x00, sizeof(bdaddr));
				memset(bdname, 0x00, sizeof(bdname));
				
				if (MemoFindBtDevice(idx, bdaddr, bdname)) {
					memcpy(&(gBANP[gNumberOfDevices].bdAddr), bdaddr,
						sizeof(bdaddr));
					memcpy(gBANP[gNumberOfDevices].name, bdname, sizeof(bdname));
					gNumberOfDevices++;
				} else {
					break;
				}
				
				idx++;
			}
		}	
	} 

	if(bt_gap_is_pincode_req()){
		gTXBT_Status.bBTConnecting = TRUE;
		gTXBT_Status.bBTPinRequest = TRUE;
		//OutputDebugString(TEXT("PinRequest!!!!!!!!!!!!!!!!!!\n"));
	} else {
		gTXBT_Status.bBTPinRequest = FALSE;
	}

    *pInitializing = gTXBT_Status.bOSInitializing;
    *pInitialized = gTXBT_Status.bOSInitialized;
    *pDiscoverable = gTXBT_Status.bBTDiscoverable;
    *pConnecting = gTXBT_Status.bBTConnecting;
    *pConnected = gTXBT_Status.bBTConnected;
    *pPinRequest = gTXBT_Status.bBTPinRequest;

	gTXBT_Last_BT_State = bt_current_state;

    return TRUE;
}

void WINAPI
vTXBT_HIDAPP_SetDiscoverable(BOOL on) {
    gTXBT_Status.bBTPinRequest = 0x00;

    if (on) {
        bt_gap_set_scan_mode(GetTickCount(), BT_WMODE_INQUIRY_SCAN);
    } else {
        //bt_gap_set_scan_mode(GetTickCount(), BT_WMODE_NO_SCAN);
    }    

    if (on) {
        gTXBT_Status.bBTConnecting = TRUE;
        gTXBT_Status.bBTDiscoverable = TRUE;
    } else {
        gTXBT_Status.bBTConnecting = FALSE;
        gTXBT_Status.bBTDiscoverable = FALSE;
    }
}

BOOL WINAPI
bTXBT_BTGetHostName(char *pName) {
    if (gTXBT_Status.bBTConnected) {
        memcpy(pName, bt_gap_get_remote_name(), MAX_DEVICE_NAME_SIZE);

        U8      idx;
        idx = MemoAddBtDevice(bt_gap_get_remote_addr());
        MemoAttachName2BtDevice(bt_gap_get_remote_addr(), bt_gap_get_remote_name());

        return TRUE;
    } else {
        return FALSE;
    }
}

BOOL WINAPI 
bTXBT_BTSetPin(unsigned char *pPin, int iPinLen){
	bt_gap_pincode_reply(pPin, iPinLen);
	return TRUE;
}

BOOL WINAPI
bTXBT_BTGetHostAddress(char *pAddress) {
    U8          addr[6];

    memcpy(addr, bt_gap_get_remote_addr(), BD_ADDR_SIZE);   

    htoa(addr[5], pAddress);
    pAddress[2] = ':';

    htoa(addr[4], pAddress + 3);
    pAddress[5] = ':';

    htoa(addr[3], pAddress + 6);
    pAddress[8] = ':';

    htoa(addr[2], pAddress + 9);
    pAddress[11] = ':';

    htoa(addr[1], pAddress + 12);
    pAddress[14] = ':';

    htoa(addr[0], pAddress + 15);

    return TRUE;
}


BOOL WINAPI
bTXBT_BTGetPaired(int index, char *pData, char *pData2) {
    if (index == 0xFF) {
        U8      idx;
        U8      bdaddr[6];
        U8      bdname[26];

        idx = 0;
        gNumberOfDevices = 0;
        memset(&gBANP, 0x00, sizeof(gBANP));

        while (idx < MAX_DEVICE) {
            memset(bdaddr, 0x00, sizeof(bdaddr));
            memset(bdname, 0x00, sizeof(bdname));

            if (MemoFindBtDevice(idx, bdaddr, bdname)) {
                memcpy(&(gBANP[gNumberOfDevices].bdAddr), bdaddr,
                       sizeof(bdaddr));
                memcpy(gBANP[gNumberOfDevices].name, bdname, sizeof(bdname));

                gNumberOfDevices++;
            } else {
                break;
            }

            idx++;
        }

        pData[0] = gNumberOfDevices;
        return TRUE;
    } else if (index < gNumberOfDevices) {
        htoa(gBANP[index].bdAddr.addr[5], pData);
        pData[2] = ':';

        htoa(gBANP[index].bdAddr.addr[4], pData + 3);
        pData[5] = ':';

        htoa(gBANP[index].bdAddr.addr[3], pData + 6);
        pData[8] = ':';

        htoa(gBANP[index].bdAddr.addr[2], pData + 9);
        pData[11] = ':';

        htoa(gBANP[index].bdAddr.addr[1], pData + 12);
        pData[14] = ':';

        htoa(gBANP[index].bdAddr.addr[0], pData + 15);

        memcpy(pData2, gBANP[index].name, MAX_DEVICE_NAME_SIZE);

        return TRUE;
    } else {
        return FALSE;
    }
}

BOOL WINAPI
bTXBT_BTUnpairAll() {
    // Delete Link Keys
    StorageClear();

    return MemoDeleteAllBTDevice();
}

BOOL WINAPI 
bTXBT_BTUnpairSingle(int index){

    if (index > gNumberOfDevices) {
        return FALSE;
    }

	return MemoDeleteBTDevice(index);
}

BOOL WINAPI
bTXBT_BTConnect(int index) {
    if (index > gNumberOfDevices) {
        return FALSE;
    }

    U8          addr[6];
    U8          name[MAX_DEVICE_NAME_SIZE]; //tmp

	bt_gap_connecttime_reset();

    if (MemoFindBtDevice(index, addr, name)) {
        bt_gap_connect(addr);

        gTXBT_Status.bBTConnecting = TRUE;
    }

    return TRUE;
}

BOOL WINAPI
bTXBT_BTDisconnect() {
    if (gTXBT_Status.bBTConnected) {

		bt_gap_disconnect();

		while(bt_state()!=BT_STA_DISCONNECTED){
			Sleep(1);
		}

        gTXBT_Status.bBTConnected = 0;

        return TRUE;
    }

    return FALSE;
}
 
BOOL WINAPI
bTXBT_SendKey(char metakey, char key1, char key2, char key3, char key4,
              char key5) {

	if(!gTXBT_Status.bBTConnected){
		return FALSE;
	}

	gKeyboardBuffer[0] = metakey;
	gKeyboardBuffer[2] = key1;
	gKeyboardBuffer[3] = key2;
	gKeyboardBuffer[4] = key3;
	gKeyboardBuffer[5] = key4;
	gKeyboardBuffer[6] = key5;

	unsigned int retry=200;
	while(retry--){
		//1: mouse, 2: keyboard, 3: media, 4:game
		if(bt_hid_send_key_mouse_joystick_media(2)==0){
			break;
		}
		Sleep(1);
	}

    return TRUE;
}

BOOL WINAPI
bTXBT_SendMouse(char x, char y, char z, char button) {
	if(!gTXBT_Status.bBTConnected){
		return FALSE;
	}

	gMouseBuffer[0] = button;
	gMouseBuffer[1] = x;
	gMouseBuffer[2] = y;
	gMouseBuffer[3] = z;

    //1: mouse, 2: keyboard, 3: media, 4:game
	//bt_hid_send_key_mouse_joystick_media(1);
	gSendMouse = 1;

    return TRUE;
}

BOOL WINAPI
bTXBT_SendRemote(char byte2, char byte3, char byte4, char byte5) {
	if(!gTXBT_Status.bBTConnected){
		return FALSE;
	}

    gMediaBuffer[0] = byte2;
	gMediaBuffer[1] = byte3;
	gMediaBuffer[2] = byte4;
	gMediaBuffer[3] = byte5;

	unsigned int retry=200;
	while(retry--){
		//1: mouse, 2: keyboard, 3: media, 4:game
		if(bt_hid_send_key_mouse_joystick_media(3)==0){
			break;
		}
		Sleep(1);
	}

    return TRUE;
}

BOOL WINAPI
bTXBT_MassUart_Open() {
    int         r_value;

    r_value = MassUart_Open();
    if (r_value == RETCODE_SUCCESS) {
        return TRUE;
    } else {
        return FALSE;
    }
}

BOOL WINAPI
bTXBT_MassUart_Close() {
    int         r_value;

    r_value = MassUart_Close();
    if (r_value == RETCODE_SUCCESS) {
        return TRUE;
    } else {
        return FALSE;
    }
}


BOOL WINAPI
bTXBT_MassUart_Execute(int bulkinout, int *transferlength, char *cdb,
                       char *databuf) {
    int         r_value;

    r_value = MassUart_Execute(bulkinout, transferlength, cdb, databuf);
    if (r_value) {
        return TRUE;
    } else {
        return FALSE;
    }
}

BOOL WINAPI
bTXBT_IsBTHarewareExist() {
	bIsTestingBTHardware = 1;
    return RunBtThread(2);
}

BOOL			
bTestBTHardware(){

	BOOL		r_value;
	int         transferlength;
	char        cdb[16];
	char        buf[512];
	
	transferlength = 0;
	
	memset(cdb, 0x00, sizeof(cdb));
	
	r_value = bTXBT_MassUart_Execute(0x00, &transferlength, cdb,
		(char *) buf);
	if(!r_value){
		r_value = bTXBT_MassUart_Open();
	}
	bIsTestingBTHardware = 0;
	return r_value;
}


//0: Off, 1: On, 2: Toggle
BOOL WINAPI 
bTXBT_SetLed(BYTE LedStatus){
	bIsSetLed = TRUE;
	gLedStatus = LedStatus;
	return TRUE;
}

BOOL			
bSetLedStatus(){
	int         transferlength;
    char        cdb[16];
    char        buf[512];

    transferlength = 0x00;

    memset(cdb, 0x00, sizeof(cdb));
	memset(buf, 0x00, sizeof(buf));

    cdb[0] = (char)0xF4;
	cdb[2] = gLedStatus;

    return bTXBT_MassUart_Execute(0x00, &transferlength, cdb, buf);
}

//0, 1, 2
BOOL WINAPI
bTXBT_DidRegisterSdp(BYTE ansi_iso_jis) {
    if (ansi_iso_jis == 1) {
        Bt__ProfileHIDDSetVidPid(0x05AC, 0x023A);
    } else if (ansi_iso_jis == 2) {
        Bt__ProfileHIDDSetVidPid(0x05AC, 0x023B);
    } else {
        Bt__ProfileHIDDSetVidPid(0x05AC, 0x0239);
    }
    return TRUE;
}

BOOL WINAPI
bTXBT_DidDeregisterSdp() {
    Bt__ProfileHIDDSetVidPid(0x19C6, 0xFFFF);
    return TRUE;
}

// Misc Functions ////////////////////////////////////////////
void
htoa(BYTE h, char *pData) {
    char        buf[MAX_PATH];

    sprintf(buf, "%02X", h);

    pData[0] = (char) buf[0];
    pData[1] = (char) buf[1];
}

//0:start 1:stop else: thread self
BYTE
RunBtThread(BYTE execute) {
    static HANDLE       OSThread        = (HANDLE) - 1;

    if (execute == 0) {
        //Start Thread

        if (OSThread != (HANDLE) - 1) {
            //already runing
            return 2;
        }

        DWORD   dwThreadID;

        bt_start(GetTickCount(), BT_WMODE_PAGING_SCAN, PROFILE_TYPE);

        OSThread = CreateThread((LPSECURITY_ATTRIBUTES) NULL, 0,
                                (LPTHREAD_START_ROUTINE) RunBtThread,
                                (LPVOID) 3, 0, &dwThreadID);
	
        return 0;
    } else if (execute == 1) {
        //Stop Thread
        if (OSThread != (HANDLE) - 1) {
            bt_stop();				
            TerminateThread(OSThread, 0);
            CloseHandle(OSThread);
            OSThread = (HANDLE) - 1;
            return 0;
        }
        return 1;
    } else if (execute == 2) {
        //test if device unplug
        //return BTCoreHwHealth();
		//return bTXBT_MassUart_Open();
        if (gTXBT_Status.bOSInitialized) {
            while (bIsTestingBTHardware){
				Sleep(1);
			}
            return bIsBTHardwareAlive;
        } else {
            bIsTestingBTHardware = 0;
            bIsBTHardwareAlive = bTestBTHardware();
            return bIsBTHardwareAlive;
        }

    }

    /////Thread Body/////////////////////
    while (1) {

		//1: mouse, 2: keyboard, 3: media, 4:game
		if(gSendMouse){
			bt_hid_send_key_mouse_joystick_media(1);			
		}

        gTXBT_BT_Busy = bt_task(GetTickCount());

		if(!gTXBT_BT_Busy){
			Sleep(1);
		}		

		if(bIsTestingBTHardware){
			bIsTestingBTHardware = 0;
			bIsBTHardwareAlive = bTestBTHardware();
		}

		if(bIsSetLed){
			bSetLedStatus();
			bIsSetLed = 0;
		}
	
		//bt_hid_send_key_mouse_joystick_media(1);
    }
    return 0;
}

void
AssignLinkKeyFileName(char *name) {
    sprintf(name, "LinkKeys");
}

void
AssignDeviceFileName(char *name) {
    sprintf(name, "DeviceNames");
}

void
AssignBDMacName(char *name) {
    DWORD       id;

    id = AssignBDAddr();

    sprintf(name, "KMS:%02X:%02X:%02X:%02X:%02X:%02X", 0x00, 0x0C, 0xE7,
            (id & 0xFF0000) >> 16, (id & 0xFF00) >> 8, (id & 0xFF));
}

DWORD
AssignBDAddr(void) {
    return (MassUart_GetUniqueID() + 1);
}

void
AssignBDName(char *name) {
    //BD name
    //sprintf((char *) name, "VirtualHID");

	EXTENDED_NAME_FORMAT        mynameformat;
	mynameformat = NameSamCompatible ;

    char       buf[MAX_PATH];
    unsigned long l = MAX_PATH;
    char       *pchar1, *pchar2;

    GetUserNameExA(mynameformat, buf, &l);
    pchar1 = strtok(buf, "\\");//DOMAIN
    pchar2 = strtok(NULL, "");//USERNAME

	sprintf((char *) name, "VirtualHID_%s", pchar1);
	
}

//////////////////////////////////////////////////////////////////////////////////////////////
// Deal with Paired Device Names.
#define         DEVICE_NAMES_SIZE 640
unsigned char   gDeviceNames[DEVICE_NAMES_SIZE];

char            BDMacName[MAX_PATH];
char            DeviceNamesName[MAX_PATH];

//simplify the paired device enumeration begin
void
MemoAttachName2BtDevice(U8 *bdaddr, U8 *name) {
                USES_CONVERSION;

    HKEY        hMiniKey;
    DWORD       Disposition;
    DWORD       err;
    UCHAR       szValue[DEVICE_NAMES_SIZE];
    DWORD       cbValue         = DEVICE_NAMES_SIZE;
    DWORD       dwType; 

    char        keyname[MAX_PATH];
    unsigned char ZeroAddr[6];

    memset(gDeviceNames, 0x00, sizeof(gDeviceNames));
    memset(BDMacName, 0x00, sizeof(BDMacName));
    memset(DeviceNamesName, 0x00, sizeof(DeviceNamesName));
    memset(ZeroAddr, 0x00, sizeof(ZeroAddr));

    AssignBDMacName(BDMacName);
    AssignDeviceFileName(DeviceNamesName);

    sprintf(keyname, "Software\\VirtualHID\\Bluetooth\\%s", BDMacName);

    err = RegCreateKeyEx(HKEY_CURRENT_USER, A2W(keyname), 0, TEXT(""),
                         REG_OPTION_NON_VOLATILE, KEY_WRITE | KEY_READ, NULL,
                         &hMiniKey, &Disposition);  

    if (ERROR_SUCCESS == err) {
        err = RegQueryValueEx(hMiniKey, A2W(DeviceNamesName), NULL, &dwType,
                              szValue, &cbValue);
        if (ERROR_SUCCESS == err) {
            memcpy(gDeviceNames, szValue, cbValue);
        }
    }

    int         i;      
    for (i = 0; i < MAX_DEVICE; i++) {
        if (memcmp(gDeviceNames + i * 32, bdaddr, 6) == 0) {
            // Address found, update its name.

            memcpy(gDeviceNames + i * 32 + 6, name, 26);
            break;
        }

        if (memcmp(gDeviceNames + i * 32, ZeroAddr, 6) == 0) {
            // New device comes, add it to database.

            memcpy(gDeviceNames + i * 32, bdaddr, 6);
            memcpy(gDeviceNames + i * 32 + 6, name, 26);
            break;
        }
    }

    // Write Back
    memcpy(szValue, gDeviceNames, DEVICE_NAMES_SIZE);

    dwType = REG_BINARY;
    cbValue = DEVICE_NAMES_SIZE;

    err = RegSetValueEx(hMiniKey, A2W(DeviceNamesName), 0, dwType, szValue,
                        cbValue);               

    RegCloseKey(hMiniKey);
}

U8
MemoFindBtDevice(U8 idx, U8 *bdaddr, U8 *name) {
                USES_CONVERSION;

    HKEY        hMiniKey;
    DWORD       Disposition;
    DWORD       err;
    UCHAR       szValue[DEVICE_NAMES_SIZE];
    DWORD       cbValue         = DEVICE_NAMES_SIZE;
    DWORD       dwType; 

    char        keyname[MAX_PATH];
    unsigned char ZeroAddr[6];

    memset(gDeviceNames, 0x00, sizeof(gDeviceNames));
    memset(BDMacName, 0x00, sizeof(BDMacName));
    memset(DeviceNamesName, 0x00, sizeof(DeviceNamesName));
    memset(ZeroAddr, 0x00, sizeof(ZeroAddr));

    AssignBDMacName(BDMacName);
    AssignDeviceFileName(DeviceNamesName);

    sprintf(keyname, "Software\\VirtualHID\\Bluetooth\\%s", BDMacName);

    err = RegCreateKeyEx(HKEY_CURRENT_USER, A2W(keyname), 0, TEXT(""),
                         REG_OPTION_NON_VOLATILE, KEY_WRITE | KEY_READ, NULL,
                         &hMiniKey, &Disposition);  

    if (ERROR_SUCCESS == err) {
        err = RegQueryValueEx(hMiniKey, A2W(DeviceNamesName), NULL, &dwType,
                              szValue, &cbValue);
        if (ERROR_SUCCESS == err) {
            memcpy(gDeviceNames, szValue, cbValue);
        }        

        RegCloseKey(hMiniKey);
    }

    if (memcmp(gDeviceNames + idx * 32, ZeroAddr, 6) != 0) {
        memcpy(bdaddr, gDeviceNames + idx * 32, 6);

        if (memcmp(gDeviceNames + idx * 32 + 6, ZeroAddr, 6) != 0) {
            memcpy(name, gDeviceNames + idx * 32 + 6, 26);
        } else {
            strcpy((char *) name, "Remote Machine");
        }

        return 1;
    } else {
        // No such index.
        return 0;
    }
}

U8
MemoAddBtDevice(U8 *bdaddr) {
                USES_CONVERSION;

    int         idx;
    HKEY        hMiniKey;
    DWORD       Disposition;
    DWORD       err;
    UCHAR       szValue[DEVICE_NAMES_SIZE];
    DWORD       cbValue         = DEVICE_NAMES_SIZE;
    DWORD       dwType; 

    char        keyname[MAX_PATH];
    unsigned char ZeroAddr[6];

    memset(gDeviceNames, 0x00, sizeof(gDeviceNames));
    memset(BDMacName, 0x00, sizeof(BDMacName));
    memset(DeviceNamesName, 0x00, sizeof(DeviceNamesName));
    memset(ZeroAddr, 0x00, sizeof(ZeroAddr));

    AssignBDMacName(BDMacName);
    AssignDeviceFileName(DeviceNamesName);

    sprintf(keyname, "Software\\VirtualHID\\Bluetooth\\%s", BDMacName);

    err = RegCreateKeyEx(HKEY_CURRENT_USER, A2W(keyname), 0, TEXT(""),
                         REG_OPTION_NON_VOLATILE, KEY_WRITE | KEY_READ, NULL,
                         &hMiniKey, &Disposition);  

    if (ERROR_SUCCESS == err) {
        err = RegQueryValueEx(hMiniKey, A2W(DeviceNamesName), NULL, &dwType,
                              szValue, &cbValue);
        if (ERROR_SUCCESS == err) {
            memcpy(gDeviceNames, szValue, cbValue);
        }
    }

    int         i;      
    for (i = 0; i < MAX_DEVICE; i++) {
        if (memcmp(gDeviceNames + i * 32, bdaddr, 6) == 0) {
            // Address found.
            idx = i;
            break;
        }

        if (memcmp(gDeviceNames + i * 32, ZeroAddr, 6) == 0) {
            // New space found. add it to database

            memcpy(gDeviceNames + i * 32, bdaddr, 6);   

            idx = i;
            break;
        }
    }

    if (i == MAX_DEVICE) {
        idx = 0xFF;
    }

    // Write Back
    memcpy(szValue, gDeviceNames, DEVICE_NAMES_SIZE);

    dwType = REG_BINARY;
    cbValue = DEVICE_NAMES_SIZE;

    err = RegSetValueEx(hMiniKey, A2W(DeviceNamesName), 0, dwType, szValue,
                        cbValue);               

    RegCloseKey(hMiniKey);

    return idx;
}

U8 
MemoDeleteBTDevice(U8 idx){
                USES_CONVERSION;

    HKEY        hMiniKey;
    DWORD       Disposition;
    DWORD       err;
    UCHAR       szValue[DEVICE_NAMES_SIZE];
    DWORD       cbValue         = DEVICE_NAMES_SIZE;
    DWORD       dwType; 

	unsigned char NewDeviceNames[DEVICE_NAMES_SIZE];

    char        keyname[MAX_PATH];
    unsigned char ZeroAddr[6];

    memset(gDeviceNames, 0x00, sizeof(gDeviceNames));
	memset(NewDeviceNames, 0x00, sizeof(NewDeviceNames));
    memset(BDMacName, 0x00, sizeof(BDMacName));
    memset(DeviceNamesName, 0x00, sizeof(DeviceNamesName));
    memset(ZeroAddr, 0x00, sizeof(ZeroAddr));

    AssignBDMacName(BDMacName);
    AssignDeviceFileName(DeviceNamesName);

    sprintf(keyname, "Software\\VirtualHID\\Bluetooth\\%s", BDMacName);

    err = RegCreateKeyEx(HKEY_CURRENT_USER, A2W(keyname), 0, TEXT(""),
                         REG_OPTION_NON_VOLATILE, KEY_WRITE | KEY_READ, NULL,
                         &hMiniKey, &Disposition);  

    if (ERROR_SUCCESS == err) {
        err = RegQueryValueEx(hMiniKey, A2W(DeviceNamesName), NULL, &dwType,
                              szValue, &cbValue);
        if (ERROR_SUCCESS == err) {
            memcpy(gDeviceNames, szValue, cbValue);
        } else {
			return 0;
		}
    } else {
		return 0;
	}

	int i;
	for(i=0;i<idx;i++){
		memcpy(NewDeviceNames+i*32, gDeviceNames+i*32, 32);
	}

	for(i=idx;i<19;i++){
		memcpy(NewDeviceNames+i*32, gDeviceNames+(i+1)*32, 32);
	}

    // Write Back
	memcpy(gDeviceNames, NewDeviceNames, DEVICE_NAMES_SIZE);
    memcpy(szValue, gDeviceNames, DEVICE_NAMES_SIZE);

    dwType = REG_BINARY;
    cbValue = DEVICE_NAMES_SIZE;

    err = RegSetValueEx(hMiniKey, A2W(DeviceNamesName), 0, dwType, szValue,
                        cbValue);               

    RegCloseKey(hMiniKey);

	return 1;
}

U8
MemoDeleteAllBTDevice() {
                USES_CONVERSION;

    HKEY        hMiniKey;
    DWORD       Disposition;
    DWORD       err;
    UCHAR       szValue[DEVICE_NAMES_SIZE];
    DWORD       cbValue         = DEVICE_NAMES_SIZE;
    DWORD       dwType; 

    char        keyname[MAX_PATH];

    memset(gDeviceNames, 0x00, sizeof(gDeviceNames));
    memset(&gBANP, 0x00, sizeof(gBANP));
    gNumberOfDevices = 0;


    sprintf(keyname, "Software\\VirtualHID\\Bluetooth\\%s", BDMacName);

    err = RegCreateKeyEx(HKEY_CURRENT_USER, A2W(keyname), 0, TEXT(""),
                         REG_OPTION_NON_VOLATILE, KEY_WRITE | KEY_READ, NULL,
                         &hMiniKey, &Disposition);  

    if (ERROR_SUCCESS == err) {
        // Write Back
        memcpy(szValue, gDeviceNames, DEVICE_NAMES_SIZE);

        dwType = REG_BINARY;
        cbValue = DEVICE_NAMES_SIZE;

        err = RegSetValueEx(hMiniKey, A2W(DeviceNamesName), 0, dwType,
                            szValue, cbValue);
    }

    RegCloseKey(hMiniKey);

    return TRUE;
}

U8 pad_data_get_report(U8 report_id, U8 len, U8 *dat)
{
	int i;

    if(report_id == 4) //game pad
    {
		for(i=0;i<sizeof(gJoyStickBuffer);i++){
			dat[i]=gJoyStickBuffer[i];
		}
/*
        dat[0] = 0;
		dat[1] = 0;
        dat[2] = 0x08; //hat
        dat[3] = 0x80; //lx
        dat[4] = 0x80; //ly
        dat[5] = 0x80; //rx
        dat[6] = 0x80; //ry
        dat[7] = 0; //right
        dat[8] = 0; //left
        dat[9] = 0; //up
        dat[10] = 0; //down
        dat[11] = 0; //tri
        dat[12] = 0; //cir
        dat[13] = 0; //cro
        dat[14] = 0; //squ
        dat[15] = 0; //l1
        dat[16] = 0; //l2
        dat[17] = 0; //r1
        dat[18] = 0; //r2
        dat[19] = 0; //acclx_l
        dat[20] = 0; //acclx_h
        dat[21] = 0; //acclz_l
        dat[22] = 0; //acclz_h
        dat[23] = 0; //accly_l
        dat[24] = 0; //accly_h
        dat[25] = 0; //acclw_l
        dat[26] = 0; //acclw_h
*/
        return 27;
    }
    else if(report_id == 1) //keyboard
    {
		for(i=0;i<sizeof(gKeyboardBuffer);i++){
			dat[i]=gKeyboardBuffer[i];
		}
/*
        dat[0] = 0;  //modifier
        dat[1] = 0;  //reseved
        dat[2] = 0;  //keycode 0
        dat[3] = 0; //'P' //keycode 1
        dat[4] = 0;   //keycode 2
        dat[5] = 0;   //keycode 3
        dat[6] = 0;   //keycode 4
        dat[7] = 0;   //keycode 5
*/
        return 8;
    }
    else if(report_id == 2) //mouse
    {
		for(i=0;i<sizeof(gMouseBuffer);i++){
			dat[i]=gMouseBuffer[i];
		}
/*
        dat[0] = 0; //buttons
        dat[1] = mouse_data[0]; //x
        dat[2] = mouse_data[1]; //y
        dat[3] = 0; //z
*/
		gSendMouse = 0;
        return 4;
    }
    else if(report_id == 3) //media
    {
		for(i=0;i<sizeof(gMediaBuffer);i++){
			dat[i]=gMediaBuffer[i];
		}

        return 4;
    }

    return 0;
}

U8 ps3_data_set_report(U8 type, U8 report, const U8 *buf, int len){
	return 0;
}

U8 ps3_data_get_report(U8 type, U8 report, U8 *buf, int maxlen){
	return 0;
}
