// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__D8499F76_1FF7_4D83_9AF0_5860BF383989__INCLUDED_)
#define AFX_STDAFX_H__D8499F76_1FF7_4D83_9AF0_5860BF383989__INCLUDED_

#define _WIN32_WINNT 0x0501
#define _WIN32_IE 0x0501

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

// defined for system tray
#define WM_TRAY_NOTIFY WM_APP+1000
#include <afxpriv.h>
//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__D8499F76_1FF7_4D83_9AF0_5860BF383989__INCLUDED_)
