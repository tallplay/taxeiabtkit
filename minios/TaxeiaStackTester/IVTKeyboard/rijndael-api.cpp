#include "stdafx.h"
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "rijndael-alg.h"
#include "rijndael-api.h"

int
rijndaelInit(keyInstance *keyInst, int keyLen, u8 *key) {
    if (keyInst == NULL)
        return BAD_KEY_INSTANCE;

    if ((keyLen == 128) || (keyLen == 192) || (keyLen == 256))
        keyInst->keyLen = keyLen;
    else
        return BAD_KEY;

    if (key != NULL)
        memcpy(keyInst->key, key, keyLen / 8);
    else
        return BAD_KEY;

    return rijndaelKeySetup(keyInst);
}

BYTE*
ascii2binary(int inputLen, char *input) {
    BYTE        *output;

    if (inputLen % 2)
        return FALSE;

    output = (BYTE *) malloc((inputLen / 2) * sizeof(char));

    if (input != NULL) {
        int     i;
        for (i = 0; i < inputLen / 2; i++) {
            int         t, j;

            t = input[2 * i];
            if ((t >= '0') && (t <= '9'))
                j = (t - '0') << 4;
            else if ((t >= 'a') && (t <= 'f'))
                j = (t - 'a' + 10) << 4;
            else if ((t >= 'A') && (t <= 'F'))
                j = (t - 'A' + 10) << 4;
            else
                return FALSE;

            t = input[2 * i + 1];
            if ((t >= '0') && (t <= '9'))
                j ^= (t - '0');
            else if ((t >= 'a') && (t <= 'f'))
                j ^= (t - 'a' + 10);
            else if ((t >= 'A') && (t <= 'F'))
                j ^= (t - 'A' + 10);
            else
                return FALSE;

            output[i] = (u8) j;
        }
    } else
        memset(output, 0, (inputLen / 2));
    return output;
}

/**
 * Encrypt data blocks
 *
 * @param   mode            ECB, CBC, CFB1, CFB8, OFB, CTR, CBC_MAC    
 * @param   iv              initial vector ( 128/8 bytes (block size)) 
 * @param   keyInst         keyInstance include keyLen, key[], Nr, ek[], dk[]
 * @param   input           data to be encrypted (character stream)
 * @param   inputLen        input length in bytes
 * @param   outBuffer       encrypted output data
 *
 * @return      length in bytes of the encrypted output buffer.
 */
int
rijndael_block_encrypt(int mode, BYTE *iv, keyInstance *keyInst, BYTE *input,
                       int inputLen, BYTE *outBuffer) {
    int         i, k = 0, t, numBlocks;
    u8          block[16];
    BYTE        *iv_old;

    iv_old = iv;

    if (keyInst == NULL)
        return BAD_KEY_INSTANCE;
    if (outBuffer == NULL)
        return BAD_CIPHER_STATE;
    if (input == NULL || inputLen <= 0) {
        return 0; /* nothing to do */
    }

    if (!(mode == MODE_CFB1 || mode == MODE_CFB8) && (inputLen % 16))
        return BAD_BLOCK_LENGTH;

    numBlocks = inputLen / 16;

    switch (mode) {
      case MODE_ECB:
        for (i = numBlocks; i > 0; i--) {
            rijndaelEncrypt(keyInst, input, outBuffer);
            input += 16;
            outBuffer += 16;
        }
        break;

      case MODE_CBC:
        for (i = numBlocks; i > 0; i--) {
            ((u32 *) block)[0] = ((u32 *) input)[0] ^ ((u32 *) iv)[0];
            ((u32 *) block)[1] = ((u32 *) input)[1] ^ ((u32 *) iv)[1];
            ((u32 *) block)[2] = ((u32 *) input)[2] ^ ((u32 *) iv)[2];
            ((u32 *) block)[3] = ((u32 *) input)[3] ^ ((u32 *) iv)[3];
            rijndaelEncrypt(keyInst, block, outBuffer);
            iv = outBuffer;
            input += 16;
            outBuffer += 16;
        }
        memcpy(iv_old, iv, BLOCK_SIZE);
        break;

      case MODE_CFB1:
        memcpy(outBuffer, input, inputLen);
        for (i = 0; i < (inputLen << 3); i++) {
            rijndaelEncrypt(keyInst, iv, block);
            outBuffer[i >> 3] ^= (block[0] & 0x80U) >> (i & 7);
            for (t = 0; t < 15; t++)
                iv[t] = (iv[t] << 1) | (iv[t + 1] >> 7);
            iv[15] = (iv[15] << 1) |
                     ((outBuffer[i >> 3] >> (7 - (k & 7))) & 1);
        }
        return inputLen;

      case MODE_CFB8:
        memcpy(outBuffer, input, inputLen);
        for (i = 0; i < inputLen; i++) {
            rijndaelEncrypt(keyInst, iv, block);
            outBuffer[i] ^= block[0];
            for (k = 0; k < 15; k++)
                iv[k] = iv[k + 1];
            iv[15] = outBuffer[i];
        }
        return inputLen;

      case MODE_OFB:
        for (i = numBlocks; i > 0; i--) {
            rijndaelEncrypt(keyInst, iv, outBuffer);
            memcpy(block, outBuffer, 16);
            ((u32 *) outBuffer)[0] ^= ((u32 *) input)[0];
            ((u32 *) outBuffer)[1] ^= ((u32 *) input)[1];
            ((u32 *) outBuffer)[2] ^= ((u32 *) input)[2];
            ((u32 *) outBuffer)[3] ^= ((u32 *) input)[3];
            iv = block;
            input += 16;
            outBuffer += 16;
        }
        memcpy(iv_old, iv, BLOCK_SIZE);
        break;

      case MODE_CTR:
        for (i = numBlocks; i > 0; i--) {
            rijndaelEncrypt(keyInst, iv, outBuffer);
            ((u32 *) outBuffer)[0] ^= ((u32 *) input)[0];
            ((u32 *) outBuffer)[1] ^= ((u32 *) input)[1];
            ((u32 *) outBuffer)[2] ^= ((u32 *) input)[2];
            ((u32 *) outBuffer)[3] ^= ((u32 *) input)[3];
            /* need to do something and 
                                    let iv(counter) increase 1 */
            /*
                                if(!(((u32*)iv)[3] ^ 0xffffffU)) {
                                        if(!(((u32*)iv)[2] ^ 0xffffffU)) {
                                                if(!(((u32*)iv)[1] ^ 0xffffffU)) {
                                                        (((u32*)iv)[0])++ ;
                                                }
                                                (((u32*)iv)[1])++ ;
                                        }
                                        (((u32*)iv)[2])++ ;
                                }
                                (((u32*)iv)[3])++ ;
            */
            for (t = BLOCK_SIZE - 1; (t >= 0) && (!(iv[t] ^ 0xffU)); t--)
                iv[t] ++;

            input += 16;
            outBuffer += 16;
        }
        break;

      case MODE_CBC_MAC:
        for (i = numBlocks; i > 0; i--) {
            ((u32 *) block)[0] = ((u32 *) input)[0] ^ ((u32 *) iv)[0];
            ((u32 *) block)[1] = ((u32 *) input)[1] ^ ((u32 *) iv)[1];
            ((u32 *) block)[2] = ((u32 *) input)[2] ^ ((u32 *) iv)[2];
            ((u32 *) block)[3] = ((u32 *) input)[3] ^ ((u32 *) iv)[3];
            rijndaelEncrypt(keyInst, block, outBuffer);
            iv = outBuffer;
            input += 16;
        }
        memcpy(iv_old, iv, BLOCK_SIZE);
        break;

      default:
        return BAD_MODE;
    }

    return 16 * numBlocks;
}

/**
 * Encrypt data partitioned in bytes, using RFC 2040-like padding.
 *
 * @param   mode            ECB, CBC, OFB, CTR, CBC_MAC    
 * @param   iv              initial vector ( 128/8 bytes (block size)) 
 * @param   keyInst         keyInstance include keyLen, key[], Nr, ek[], dk[]
 * @param   input           data to be encrypted (character stream)
 * @param   inputLen        input length in bytes 
 * @param   outBuffer       encrypted output data
 *
 * @return      length in bytes of the encrypted output buffer.
 */
int
rijndael_pad_encrypt(int mode, BYTE *iv, keyInstance *keyInst, BYTE *input,
                     int inputLen, BYTE *outBuffer) {
    int         i, t, numBlocks, padLen;
    u8          block[16];
    //  BYTE *iv_old;

    //  iv_old = iv;

    if (keyInst == NULL)
        return BAD_KEY_INSTANCE;
    if (outBuffer == NULL)
        return BAD_CIPHER_STATE;
    if (input == NULL || inputLen <= 0) {
        return 0; /* nothing to do */
    }

    numBlocks = inputLen / 16;

    switch (mode) {
      case MODE_ECB:
        for (i = numBlocks; i > 0; i--) {
            rijndaelEncrypt(keyInst, input, outBuffer);
            input += 16;
            outBuffer += 16;
        }
        padLen = 16 - (inputLen - 16 * numBlocks);
        if (padLen == 0)
            padLen = 16;
        assert(padLen > 0 && padLen <= 16);
        memcpy(block, input, 16 - padLen);
        memset(block + 16 - padLen, padLen, padLen);
        rijndaelEncrypt(keyInst, block, outBuffer);
        break;

      case MODE_CBC:
        for (i = numBlocks; i > 0; i--) {
            ((u32 *) block)[0] = ((u32 *) input)[0] ^ ((u32 *) iv)[0];
            ((u32 *) block)[1] = ((u32 *) input)[1] ^ ((u32 *) iv)[1];
            ((u32 *) block)[2] = ((u32 *) input)[2] ^ ((u32 *) iv)[2];
            ((u32 *) block)[3] = ((u32 *) input)[3] ^ ((u32 *) iv)[3];
            rijndaelEncrypt(keyInst, block, outBuffer);
            iv = outBuffer;
            input += 16;
            outBuffer += 16;
        }
        padLen = 16 - (inputLen - 16 * numBlocks);
        if (padLen == 0)
            padLen = 16;
        assert(padLen > 0 && padLen <= 16);
        for (i = 0; i < 16 - padLen; i++)
            block[i] = input[i] ^ iv[i];
        for (i = 16 - padLen; i < 16; i++)
            block[i] = (BYTE) padLen ^ iv[i];
        rijndaelEncrypt(keyInst, block, outBuffer);
        /*
                        iv = outBuffer;
                        memcpy( iv_old, iv, BLOCK_SIZE );
        */
        break;

      case MODE_OFB:
        for (i = numBlocks; i > 0; i--) {
            rijndaelEncrypt(keyInst, iv, outBuffer);
            memcpy(block, outBuffer, 16);
            ((u32 *) outBuffer)[0] ^= ((u32 *) input)[0];
            ((u32 *) outBuffer)[1] ^= ((u32 *) input)[1];
            ((u32 *) outBuffer)[2] ^= ((u32 *) input)[2];
            ((u32 *) outBuffer)[3] ^= ((u32 *) input)[3];
            iv = block;
            input += 16;
            outBuffer += 16;
        }
        padLen = 16 - (inputLen - 16 * numBlocks);
        if (padLen == 0)
            padLen = 16;
        assert(padLen > 0 && padLen <= 16);
        rijndaelEncrypt(keyInst, iv, outBuffer);
        /*
                        iv = outBuffer;
                        memcpy( iv_old, iv, BLOCK_SIZE );
        */
        for (i = 0; i < 16 - padLen; i++)
            outBuffer[i] ^= input[i];
        for (i = 16 - padLen; i < 16; i++)
            outBuffer[i] ^= (BYTE) padLen;
        break;

      case MODE_CTR:
        for (i = numBlocks; i > 0; i--) {
            rijndaelEncrypt(keyInst, iv, outBuffer);
            ((u32 *) outBuffer)[0] ^= ((u32 *) input)[0];
            ((u32 *) outBuffer)[1] ^= ((u32 *) input)[1];
            ((u32 *) outBuffer)[2] ^= ((u32 *) input)[2];
            ((u32 *) outBuffer)[3] ^= ((u32 *) input)[3];

            /* let block(counter) increase 1 */
            for (t = BLOCK_SIZE - 1; (t >= 0) && (!(iv[t] ^ 0xffU)); t--)
                iv[t] ++;

            input += 16;
            outBuffer += 16;
        }
        padLen = 16 - (inputLen - 16 * numBlocks);
        if (padLen == 0)
            padLen = 16;
        assert(padLen > 0 && padLen <= 16);
        rijndaelEncrypt(keyInst, iv, outBuffer);
        for (i = 0; i < 16 - padLen; i++)
            outBuffer[i] ^= input[i];
        for (i = 16 - padLen; i < 16; i++)
            outBuffer[i] ^= (BYTE) padLen;

        break;

      case MODE_CBC_MAC:
        for (i = numBlocks; i > 0; i--) {
            ((u32 *) block)[0] = ((u32 *) input)[0] ^ ((u32 *) iv)[0];
            ((u32 *) block)[1] = ((u32 *) input)[1] ^ ((u32 *) iv)[1];
            ((u32 *) block)[2] = ((u32 *) input)[2] ^ ((u32 *) iv)[2];
            ((u32 *) block)[3] = ((u32 *) input)[3] ^ ((u32 *) iv)[3];
            rijndaelEncrypt(keyInst, block, outBuffer);
            iv = outBuffer;
            input += 16;
        }
        padLen = 16 - (inputLen - 16 * numBlocks);
        if (padLen == 0)
            padLen = 16;
        assert(padLen > 0 && padLen <= 16);
        for (i = 0; i < 16 - padLen; i++)
            block[i] = input[i] ^ iv[i];
        for (i = 16 - padLen; i < 16; i++)
            block[i] = (BYTE) padLen ^ iv[i];
        rijndaelEncrypt(keyInst, block, outBuffer);
        /*
                        iv = outBuffer;
                        memcpy( iv_old, iv, BLOCK_SIZE );
        */
        break;
      default:
        return BAD_MODE;
    }

    return 16 * (numBlocks + 1);
}

/**
 * Decrypt data blocks
 *
 * @param   mode            ECB, CBC, CFB1, CFB8, OFB, CTR
 * @param   iv              initial vector ( 128/8 bytes (block size))
 * @param   keyInst         keyInstance include keyLen, key[], Nr, ek[], dk[]
 * @param   input           data to be encrypted (character stream)
 * @param   inputLen        input length in bytes
 * @param   outBuffer       decrypted output data
 *
 * @return      length in bytes of the decrypted output buffer.
 */

int
rijndael_block_decrypt(int mode, BYTE *iv, keyInstance *keyInst, BYTE *input,
                       int inputLen, BYTE *outBuffer) {
    int         i, k = 0, t, numBlocks;
    u8          block[16];
    BYTE        *iv_old;

    iv_old = iv;

    if (keyInst == NULL)
        return BAD_KEY_INSTANCE;
    if (outBuffer == NULL)
        return BAD_CIPHER_STATE;
    if (input == NULL || inputLen <= 0) {
        return 0; /* nothing to do */
    }
    if (!(mode == MODE_CFB1 || mode == MODE_CFB8) && (inputLen % 16))
        return BAD_BLOCK_LENGTH;
    numBlocks = inputLen / 16;

    switch (mode) {
      case MODE_ECB:
        for (i = numBlocks; i > 0; i--) {
            rijndaelDecrypt(keyInst, input, outBuffer);
            input += 16;
            outBuffer += 16;
        }
        break;

      case MODE_CBC:
        for (i = numBlocks; i > 0; i--) {
            rijndaelDecrypt(keyInst, input, block);
            ((u32 *) block)[0] ^= ((u32 *) iv)[0];
            ((u32 *) block)[1] ^= ((u32 *) iv)[1];
            ((u32 *) block)[2] ^= ((u32 *) iv)[2];
            ((u32 *) block)[3] ^= ((u32 *) iv)[3];
            iv = input;
            memcpy(outBuffer, block, 16);
            input += 16;
            outBuffer += 16;
        }
        memcpy(iv_old, iv, BLOCK_SIZE);
        break;

      case MODE_CFB1:
        memcpy(outBuffer, input, inputLen);
        for (i = 0; i < (inputLen << 3); i++) {
            rijndaelEncrypt(keyInst, iv, block);
            outBuffer[i >> 3] ^= (block[0] & 0x80U) >> (i & 7);
            for (t = 0; t < 15; t++)
                iv[t] = (iv[t] << 1) | (iv[t + 1] >> 7);
            iv[15] = (iv[15] << 1) | ((input[i >> 3] >> (7 - (k & 7))) & 1);
        }
        return inputLen;

      case MODE_CFB8:
        memcpy(outBuffer, input, inputLen);
        for (i = 0; i < inputLen; i++) {
            rijndaelEncrypt(keyInst, iv, block);
            outBuffer[i] ^= block[0];
            for (k = 0; k < 15; k++)
                iv[k] = iv[k + 1];
            iv[15] = input[i];
        }
        return inputLen;

      case MODE_OFB:
        memcpy(block, iv, BLOCK_SIZE);
        for (i = numBlocks; i > 0; i--) {
            rijndaelEncrypt(keyInst, block, outBuffer);
            memcpy(block, outBuffer, 16);
            ((u32 *) outBuffer)[0] ^= ((u32 *) input)[0];
            ((u32 *) outBuffer)[1] ^= ((u32 *) input)[1];
            ((u32 *) outBuffer)[2] ^= ((u32 *) input)[2];
            ((u32 *) outBuffer)[3] ^= ((u32 *) input)[3];
            input += 16;
            outBuffer += 16;
        }
        memcpy(iv_old, block, BLOCK_SIZE);
        break;

      case MODE_CTR:
        for (i = numBlocks; i > 0; i--) {
            rijndaelEncrypt(keyInst, iv, outBuffer);
            ((u32 *) outBuffer)[0] ^= ((u32 *) input)[0];
            ((u32 *) outBuffer)[1] ^= ((u32 *) input)[1];
            ((u32 *) outBuffer)[2] ^= ((u32 *) input)[2];
            ((u32 *) outBuffer)[3] ^= ((u32 *) input)[3];
            /* need to do something and 
                                    let block(counter) increase 1 */
            for (t = BLOCK_SIZE - 1; (t >= 0) && (!(iv[t] ^ 0xffU)); t--)
                iv[t] ++;

            input += 16;
            outBuffer += 16;
        }
        break;

      default:
        return BAD_MODE;
    }

    return 16 * numBlocks;
}

/**
 * Decrypt data partitioned in bytes, using RFC 2040-like padding.
 *
 * @param   mode            ECB, CBC, OFB, CTR
 * @param   iv              initial vector ( 128/8 bytes (block size)) 
 * @param   keyInst         keyInstance include keyLen, key[], Nr, ek[], dk[]
 * @param   input           data to be decrypted (character stream)
 * @param   inputLen        input length in bytes 
 * @param   outBuffer       encrypted output data
 *
 * @return      length in bytes of the decrypted output buffer.
 */
int
rijndael_pad_decrypt(int mode, BYTE *iv, keyInstance *keyInst, BYTE *input,
                     int inputLen, BYTE *outBuffer) {
    int         i, t, numBlocks, padLen;
    u8          block[16], block2[16];
    //  BYTE *iv_old;


    //  iv_old = iv;
    if (keyInst == NULL)
        return BAD_KEY_INSTANCE;
    if (outBuffer == NULL)
        return BAD_CIPHER_STATE;
    if (input == NULL || inputLen <= 0) {
        return 0; /* nothing to do */
    }

    if (inputLen % 16)
        return BAD_BLOCK_LENGTH;

    numBlocks = inputLen / 16;

    switch (mode) {
      case MODE_ECB:
        /* all blocks but last */
        for (i = numBlocks - 1; i > 0; i--) {
            rijndaelDecrypt(keyInst, input, outBuffer);
            input += 16;
            outBuffer += 16;
        }
        /* last block */
        rijndaelDecrypt(keyInst, input, block);
        padLen = block[15];
        if (padLen > 16)
            return BAD_DATA;
        for (i = 16 - padLen; i < 16; i++) {
            if (block[i] != padLen)
                return BAD_DATA;
        }
        if (padLen < 16)
            memcpy(outBuffer, block, 16 - padLen);
        break;

      case MODE_CBC:
        /* all blocks but last */
        for (i = numBlocks - 1; i > 0; i--) {
            rijndaelDecrypt(keyInst, input, block);
            ((u32 *) block)[0] ^= ((u32 *) iv)[0];
            ((u32 *) block)[1] ^= ((u32 *) iv)[1];
            ((u32 *) block)[2] ^= ((u32 *) iv)[2];
            ((u32 *) block)[3] ^= ((u32 *) iv)[3];
            memcpy(iv, input, 16);
            memcpy(outBuffer, block, 16);
            input += 16;
            outBuffer += 16;
        }
        /* last block */
        rijndaelDecrypt(keyInst, input, block);
        ((u32 *) block)[0] ^= ((u32 *) iv)[0];
        ((u32 *) block)[1] ^= ((u32 *) iv)[1];
        ((u32 *) block)[2] ^= ((u32 *) iv)[2];
        ((u32 *) block)[3] ^= ((u32 *) iv)[3];
        padLen = block[15];
        if (padLen <= 0 || padLen > 16)
            return BAD_DATA;
        for (i = 16 - padLen; i < 16; i++) {
            if (block[i] != padLen)
                return BAD_DATA;
        }
        memcpy(outBuffer, block, 16 - padLen);
        break;

      case MODE_OFB:
        memcpy(block, iv, 16);
        for (i = numBlocks - 1; i > 0; i--) {
            rijndaelEncrypt(keyInst, block, outBuffer);
            memcpy(block, outBuffer, 16);
            ((u32 *) outBuffer)[0] ^= ((u32 *) input)[0];
            ((u32 *) outBuffer)[1] ^= ((u32 *) input)[1];
            ((u32 *) outBuffer)[2] ^= ((u32 *) input)[2];
            ((u32 *) outBuffer)[3] ^= ((u32 *) input)[3];
            input += 16;
            outBuffer += 16;
        }
        rijndaelEncrypt(keyInst, block, block2);
        ((u32 *) block2)[0] ^= ((u32 *) input)[0];
        ((u32 *) block2)[1] ^= ((u32 *) input)[1];
        ((u32 *) block2)[2] ^= ((u32 *) input)[2];
        ((u32 *) block2)[3] ^= ((u32 *) input)[3];

        padLen = block2[15];
        if (padLen <= 0 || padLen > 16)
            return BAD_DATA;
        for (i = 16 - padLen; i < 15; i++) {
            if (block2[i] != padLen)
                return BAD_DATA;
        }
        memcpy(outBuffer, block2, 16 - padLen);
        break;

      case MODE_CTR:
        for (i = numBlocks - 1; i > 0; i--) {
            rijndaelEncrypt(keyInst, iv, outBuffer);
            ((u32 *) outBuffer)[0] ^= ((u32 *) input)[0];
            ((u32 *) outBuffer)[1] ^= ((u32 *) input)[1];
            ((u32 *) outBuffer)[2] ^= ((u32 *) input)[2];
            ((u32 *) outBuffer)[3] ^= ((u32 *) input)[3];
            /* need to do something and 
                                    let block(counter) increase 1 */
            for (t = BLOCK_SIZE - 1; (t >= 0) && (!(iv[t] ^ 0xffU)); t--)
                iv[t] ++;

            input += 16;
            outBuffer += 16;
        }
        rijndaelEncrypt(keyInst, iv, block);
        ((u32 *) block)[0] ^= ((u32 *) input)[0];
        ((u32 *) block)[1] ^= ((u32 *) input)[1];
        ((u32 *) block)[2] ^= ((u32 *) input)[2];
        ((u32 *) block)[3] ^= ((u32 *) input)[3];

        padLen = block[15];
        if (padLen <= 0 || padLen > 16)
            return BAD_DATA;
        for (i = 16 - padLen; i < 15; i++) {
            if (block[i] != padLen)
                return BAD_DATA;
        }
        memcpy(outBuffer, block, 16 - padLen);
        break;

      default:
        return BAD_MODE;
    }

    return 16 * numBlocks - padLen;
}


int
rijndael_ecb_encrypt(keyInstance *keyInst, unsigned char *input, int inputLen,
                     int padding, unsigned char *outBuffer) {
    if (padding)
        return rijndael_pad_encrypt(MODE_ECB, NULL, keyInst, input, inputLen,
                                    outBuffer);
    else
        return rijndael_block_encrypt(MODE_ECB, NULL, keyInst, input,
                                      inputLen, outBuffer);
}

int
rijndael_ecb_decrypt(keyInstance *keyInst, unsigned char *input, int inputLen,
                     int padding, unsigned char *outBuffer) {
    if (padding)
        return rijndael_pad_decrypt(MODE_ECB, NULL, keyInst, input, inputLen,
                                    outBuffer);
    else
        return rijndael_block_decrypt(MODE_ECB, NULL, keyInst, input,
                                      inputLen, outBuffer);
}

int
rijndael_cbc_encrypt(keyInstance *keyInst, unsigned char *iv,
                     unsigned char *input, int inputLen, int padding,
                     unsigned char *outBuffer) {
    if (padding)
        return rijndael_pad_encrypt(MODE_CBC, iv, keyInst, input, inputLen,
                                    outBuffer);
    else
        return rijndael_block_encrypt(MODE_CBC, iv, keyInst, input, inputLen,
                                      outBuffer);
}

int
rijndael_cbc_decrypt(keyInstance *keyInst, unsigned char *iv,
                     unsigned char *input, int inputLen, int padding,
                     unsigned char *outBuffer) {
    if (padding)
        return rijndael_pad_decrypt(MODE_CBC, iv, keyInst, input, inputLen,
                                    outBuffer);
    else
        return rijndael_block_decrypt(MODE_CBC, iv, keyInst, input, inputLen,
                                      outBuffer);
}

int
rijndael_cfb1_encrypt(keyInstance *keyInst, unsigned char *iv,
                      unsigned char *input, int inputLen,
                      unsigned char *outBuffer) {
    return rijndael_block_encrypt(MODE_CFB1, iv, keyInst, input, inputLen,
                                  outBuffer);
}

int
rijndael_cfb1_decrypt(keyInstance *keyInst, unsigned char *iv,
                      unsigned char *input, int inputLen,
                      unsigned char *outBuffer) {
    return rijndael_block_decrypt(MODE_CFB1, iv, keyInst, input, inputLen,
                                  outBuffer);
}

int
rijndael_cfb8_encrypt(keyInstance *keyInst, unsigned char *iv,
                      unsigned char *input, int inputLen,
                      unsigned char *outBuffer) {
    return rijndael_block_encrypt(MODE_CFB8, iv, keyInst, input, inputLen,
                                  outBuffer);
}

int
rijndael_cfb8_decrypt(keyInstance *keyInst, unsigned char *iv,
                      unsigned char *input, int inputLen,
                      unsigned char *outBuffer) {
    return rijndael_block_decrypt(MODE_CFB8, iv, keyInst, input, inputLen,
                                  outBuffer);
}

int
rijndael_ofb_encrypt(keyInstance *keyInst, unsigned char *iv,
                     unsigned char *input, int inputLen, int padding,
                     unsigned char *outBuffer) {
    if (padding)
        return rijndael_pad_encrypt(MODE_OFB, iv, keyInst, input, inputLen,
                                    outBuffer);
    else
        return rijndael_block_encrypt(MODE_OFB, iv, keyInst, input, inputLen,
                                      outBuffer);
}

int
rijndael_ofb_decrypt(keyInstance *keyInst, unsigned char *iv,
                     unsigned char *input, int inputLen, int padding,
                     unsigned char *outBuffer) {
    if (padding)
        return rijndael_pad_decrypt(MODE_OFB, iv, keyInst, input, inputLen,
                                    outBuffer);
    else
        return rijndael_block_decrypt(MODE_OFB, iv, keyInst, input, inputLen,
                                      outBuffer);
}

int
rijndael_ctr_encrypt(keyInstance *keyInst, unsigned char *iv,
                     unsigned char *input, int inputLen, int padding,
                     unsigned char *outBuffer) {
    if (padding)
        return rijndael_pad_encrypt(MODE_CTR, iv, keyInst, input, inputLen,
                                    outBuffer);
    else
        return rijndael_block_encrypt(MODE_CTR, iv, keyInst, input, inputLen,
                                      outBuffer);
}

int
rijndael_ctr_decrypt(keyInstance *keyInst, unsigned char *iv,
                     unsigned char *input, int inputLen, int padding,
                     unsigned char *outBuffer) {
    if (padding)
        return rijndael_pad_decrypt(MODE_CTR, iv, keyInst, input, inputLen,
                                    outBuffer);
    else
        return rijndael_block_decrypt(MODE_CTR, iv, keyInst, input, inputLen,
                                      outBuffer);
}

int
rijndael_cbc_mac_encrypt(keyInstance *keyInst, unsigned char *iv,
                         unsigned char *input, int inputLen, int padding,
                         unsigned char *outBuffer) {
    if (padding)
        return rijndael_pad_encrypt(MODE_CBC_MAC, iv, keyInst, input,
                                    inputLen, outBuffer);
    else
        return rijndael_block_encrypt(MODE_CBC_MAC, iv, keyInst, input,
                                      inputLen, outBuffer);
}
int
rijndael_cbc_mac_decrypt(keyInstance *keyInst, unsigned char *iv,
                         unsigned char *input, int inputLen, int padding,
                         unsigned char *outBuffer) {
    if (padding)
        return rijndael_pad_encrypt(MODE_CBC_MAC, iv, keyInst, input,
                                    inputLen, outBuffer);
    else
        return rijndael_block_encrypt(MODE_CBC_MAC, iv, keyInst, input,
                                      inputLen, outBuffer);
}

int
rijndael_file_encrypt(int mode, unsigned char *iv, unsigned char *key,
                      int keyLength, const char *inputFileName,
                      const char *outputFileName) {
    int                 r, i, s = 0;
    keyInstance         keyInst;
    FILE                *input, *output;
    BYTE                inputBuffer[BUFFER_SIZE],
                        outputBuffer[BUFFER_SIZE + BLOCK_SIZE];

    if ((iv == NULL) && (mode != MODE_ECB)) {
        fprintf(stderr, "Initial vector can't be NULL\n");
        exit(-1);
    }
    r = rijndaelInit(&keyInst, keyLength, key);
    if (r <= 0) {
        fprintf(stderr, "rijndael initial error %d\n", r);
        exit(-1);
    }

    if ((input = fopen(inputFileName, "rb")) == NULL) {
        fprintf(stderr, "Can't open file: %s to read\n", inputFileName);
        exit(-1);
    }

    if ((output = fopen(outputFileName, "wb")) == NULL) {
        fprintf(stderr, "Can't open file: %s to write\n", outputFileName);
        exit(-1);
    }

    while ((r = fread((void *) inputBuffer, 1, BUFFER_SIZE, input)) > 0) {
        if (!feof(input))
            i = rijndael_block_encrypt(mode, iv, &keyInst, inputBuffer, r,
                                       outputBuffer);
        else
            i = rijndael_pad_encrypt(mode, iv, &keyInst, inputBuffer, r,
                                     outputBuffer); 
        fwrite((void *) outputBuffer, 1, i, output);
        s += i;
    }

    fflush(output);
    fclose(input);
    fclose(output);
    return s;
}
int
rijndael_file_decrypt(int mode, unsigned char *iv, unsigned char *key,
                      int keyLength, const char *inputFileName,
                      const char *outputFileName) {
    int                 r, i, s = 0;
    keyInstance         keyInst;
    FILE                *input, *output;
    BYTE                inputBuffer[BUFFER_SIZE], outputBuffer[BUFFER_SIZE];

    if ((iv == NULL) && (mode != MODE_ECB)) {
        fprintf(stderr, "Initial vector can't be NULL\n");
        exit(-1);
    }
    r = rijndaelInit(&keyInst, keyLength, key);
    if (r <= 0) {
        fprintf(stderr, "rijndael initial error %d\n", r);
        exit(-1);
    }

    if ((input = fopen(inputFileName, "rb")) == NULL) {
        fprintf(stderr, "Can't open file: %s to read\n", inputFileName);
        exit(-1);
    }

    if ((output = fopen(outputFileName, "wb")) == NULL) {
        fprintf(stderr, "Can't open file: %s to write\n", outputFileName);
        exit(-1);
    }

    while ((r = fread((void *) inputBuffer, 1, BUFFER_SIZE, input)) > 0) {
        if (!feof(input))
            i = rijndael_block_decrypt(mode, iv, &keyInst, inputBuffer, r,
                                       outputBuffer);
        else
            i = rijndael_pad_decrypt(mode, iv, &keyInst, inputBuffer, r,
                                     outputBuffer); 
        fwrite((void *) outputBuffer, 1, i, output);
        s += i;
    }

    fflush(output);
    fclose(input);
    fclose(output);
    return s;
}

#ifdef INTERMEDIATE_VALUE_KAT
/**
 *      cipherUpdateRounds:
 *
 *      Encrypts/Decrypts exactly one full block a specified number of rounds.
 *      Only used in the Intermediate Value Known Answer Test.  
 *
 *      Returns:
 *              TRUE - on success
 *              BAD_CIPHER_STATE - cipher in bad state (e.g., not initialized)
 */
int
cipherUpdateRounds(int dir, keyInstance *keyInst, BYTE *input, int inputLen,
                   BYTE *outBuffer, int rounds) {
    u8          block[16];

    if (keyInst == NULL)
        return BAD_KEY_INSTANCE;

    memcpy(block, input, 16);

    switch (dir) {
      case DIR_ENCRYPT:
        rijndaelEncryptRound(keyInst, block, rounds);
        break;

      case DIR_DECRYPT:
        rijndaelDecryptRound(keyInst, block, rounds);
        break;

      default:
        return BAD_KEY_DIR;
    } 

    memcpy(outBuffer, block, 16);

    return TRUE;
}
#endif /* INTERMEDIATE_VALUE_KAT */
