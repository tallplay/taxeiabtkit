# Microsoft Developer Studio Project File - Name="BTStackLibrary" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Static Library" 0x0104

CFG=BTStackLibrary - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "BTStackLibrary.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "BTStackLibrary.mak" CFG="BTStackLibrary - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "BTStackLibrary - Win32 Release" (based on "Win32 (x86) Static Library")
!MESSAGE "BTStackLibrary - Win32 Debug" (based on "Win32 (x86) Static Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "BTStackLibrary - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_MBCS" /D "_LIB" /YX /FD /c
# ADD CPP /nologo /W3 /GX /O2 /I "..\\" /D PROFILE_TYPE=0 /D "WIN32" /D "NDEBUG" /D "_MBCS" /D "_LIB" /D POW=5 /D POW_INIT=5 /FR /YX /FD /c
# ADD BASE RSC /l 0x404 /d "NDEBUG"
# ADD RSC /l 0x404 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ELSEIF  "$(CFG)" == "BTStackLibrary - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_MBCS" /D "_LIB" /YX /FD /GZ /c
# ADD CPP /nologo /W3 /Gm /GX /ZI /Od /I "..\\" /D "WIN32" /D "_DEBUG" /D "_MBCS" /D "_LIB" /D PROFILE_TYPE=0 /D POW=5 /D POW_INIT=5 /D FAST_SCAN=0x00120140 /FR /YX /FD /GZ /c
# ADD BASE RSC /l 0x404 /d "_DEBUG"
# ADD RSC /l 0x404 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ENDIF 

# Begin Target

# Name "BTStackLibrary - Win32 Release"
# Name "BTStackLibrary - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Group "CORE"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\CORE\BTCore.c
# End Source File
# Begin Source File

SOURCE=..\CORE\DataQueue.c
# End Source File
# End Group
# Begin Group "DEBUG"

# PROP Default_Filter ""
# Begin Group "CONSOLE_DEBUG"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\TaxeiaConsoleDebug\TaxeiaConsoleDebug.c
# End Source File
# End Group
# Begin Group "UI_DEBUG"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\TaxeiaHCILog\TaxeiaHCILog.c
# End Source File
# Begin Source File

SOURCE=..\TaxeiaHCILog\TaxeiaHCILog_Modeling.c
# End Source File
# End Group
# End Group
# Begin Group "GAP"

# PROP Default_Filter ""
# Begin Group "HCI"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\GAP\HCI\BTCmdEvt.c
# End Source File
# End Group
# Begin Group "L2CAP"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\GAP\L2CAP\Bt__LLCAP.c
# End Source File
# Begin Source File

SOURCE=..\GAP\L2CAP\Bt__LLCAP_Channel.c
# End Source File
# End Group
# Begin Group "LL"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\GAP\LL\Bt__RemoteDevice.c
# End Source File
# End Group
# Begin Group "LOCAL_DEV"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\GAP\LOCAL_DEV\Bt__LocalDevice.c
# End Source File
# End Group
# Begin Group "SECURITY"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\GAP\SECURITY\Bt__SecurityManager.c
# End Source File
# End Group
# End Group
# Begin Group "HAL"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\HAL\bulkusb.cpp
# End Source File
# Begin Source File

SOURCE=..\HAL\MassUart.cpp
# End Source File
# End Group
# Begin Group "HFP"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\HFP\Bt__Profile_HFP.c
# End Source File
# End Group
# Begin Group "HIDPD"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\HIDPD\Bt__Profile_HIDD.c
# End Source File
# End Group
# Begin Group "RFCOMM"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\RFCOMM\rfcomm.c
# End Source File
# End Group
# Begin Group "SDP"

# PROP Default_Filter ""
# Begin Group "SDPS"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\SDP\SDPS\Bt__Profile_SDPS.c
# End Source File
# End Group
# End Group
# Begin Group "Windows-Porting"

# PROP Default_Filter ""
# Begin Source File

SOURCE="..\Windows-Porting\LocalOutput.c"
# End Source File
# Begin Source File

SOURCE="..\Windows-Porting\localoutput.h"
# End Source File
# Begin Source File

SOURCE="..\Windows-Porting\LocalStorage.c"
# End Source File
# Begin Source File

SOURCE="..\Windows-Porting\LocalStorage.h"
# End Source File
# End Group
# Begin Group "API"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\SDKAPI\bt_hid_hfp_api.c
# End Source File
# End Group
# Begin Group "CRYPTO"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\CRYPTO\aes.c
# End Source File
# Begin Source File

SOURCE=..\CRYPTO\Crypto.c
# End Source File
# End Group
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=..\DemoApp\gapcontrol.h
# End Source File
# Begin Source File

SOURCE=..\DemoApp\hfpcontrol.h
# End Source File
# Begin Source File

SOURCE=..\DemoApp\HIDControl.h
# End Source File
# End Group
# End Target
# End Project
