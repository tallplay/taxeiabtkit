#if !defined(AFX_CDlgSetBT_H__F87C2DA6_08B4_418B_A1EA_7807F57E4BD7__INCLUDED_)
#define AFX_CDlgSetBT_H__F87C2DA6_08B4_418B_A1EA_7807F57E4BD7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CDlgSetBT.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDlgSetBT dialog

class CDlgSetBT : public CDialog
{
// Construction
public:
	CDlgSetBT(CWnd* pParent = NULL);   // standard constructor
	CBrush      *m_pWhiteBkBrush;
    void        vLoadRXName();
    
	CButton     *m_control_rx[20];	
	char		m_bt_address[20][18];

	int			m_connected;
	BOOL		bCheckConnected();
	
// Dialog Data
	//{{AFX_DATA(CDlgSetBT)
	enum { IDD = IDD_DIALOG_SETBT };
		// NOTE: the ClassWizard will add data members here
    CButton     m_control_rx1;
    CButton     m_control_rx2;
    CButton     m_control_rx3;
    CButton     m_control_rx4;
    CButton     m_control_rx5;
    CButton     m_control_rx6;
    CButton     m_control_rx7;
    CButton     m_control_rx8;
    CButton     m_control_rx9;
    CButton     m_control_rx10;
    CButton     m_control_rx11;
    CButton     m_control_rx12;
    CButton     m_control_rx13;
    CButton     m_control_rx14;
    CButton     m_control_rx15;
    CButton     m_control_rx16;
    CButton     m_control_rx17;
    CButton     m_control_rx18;
    CButton     m_control_rx19;
    CButton     m_control_rx20;
    int         m_irx;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgSetBT)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDlgSetBT)
	afx_msg void OnDiscoverable();
	virtual BOOL OnInitDialog();	
	afx_msg void OnUnpair();
	virtual void OnOK();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnUnpairthis();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CDlgSetBT_H__F87C2DA6_08B4_418B_A1EA_7807F57E4BD7__INCLUDED_)
