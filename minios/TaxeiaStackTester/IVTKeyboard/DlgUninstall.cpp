// DlgUninstall.cpp : implementation file
//

#include "stdafx.h"
#include "IVTKeyboard.h"
#include "DlgUninstall.h"
#include "Color.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDlgUninstall dialog


CDlgUninstall::CDlgUninstall(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgUninstall::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgUninstall)
	m_uninstall = FALSE;
	//}}AFX_DATA_INIT
	m_pWhiteBkBrush = new CBrush(WHITE);
}


void CDlgUninstall::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgUninstall)
	DDX_Check(pDX, IDC_CHECK1, m_uninstall);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgUninstall, CDialog)
	//{{AFX_MSG_MAP(CDlgUninstall)
	ON_WM_CTLCOLOR()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgUninstall message handlers

HBRUSH CDlgUninstall::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) 
{
    switch (nCtlColor) {
      case CTLCOLOR_STATIC:
      case CTLCOLOR_BTN:
	   case CTLCOLOR_DLG:
        pDC->SetBkColor(WHITE);
        return (HBRUSH) (m_pWhiteBkBrush->GetSafeHandle());

      default:
        return CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
    }
}
