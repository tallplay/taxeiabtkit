// DlgSetPin.cpp : implementation file
//

#include "stdafx.h"
#include "ivtkeyboard.h"
#include "DlgSetPin.h"
#include "Color.h"

#define IDT_CHECK_PAIRED 0
#define IDT_TIMEOUT              1

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char     THIS_FILE[]     = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDlgSetPin dialog


CDlgSetPin::CDlgSetPin(CWnd *pParent /*=NULL*/) :
    CDialog(CDlgSetPin::IDD, pParent)
{
    //{{AFX_DATA_INIT(CDlgSetPin)
        // NOTE: the ClassWizard will add member initialization here
    //}}AFX_DATA_INIT
    m_pWhiteBkBrush = new CBrush(WHITE);
}       

void
CDlgSetPin::DoDataExchange(CDataExchange *pDX) {
    CDialog::DoDataExchange(pDX);
    //{{AFX_DATA_MAP(CDlgSetPin)
    DDX_Control(pDX, IDC_EDIT_PIN, m_control_pin);
    //}}AFX_DATA_MAP

}


BEGIN_MESSAGE_MAP(CDlgSetPin, CDialog)
        //{{AFX_MSG_MAP(CDlgSetPin)
        ON_WM_TIMER()
        ON_WM_CTLCOLOR()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgSetPin message handlers

void
CDlgSetPin::OnTimer(UINT nIDEvent) {
    if (nIDEvent == IDT_CHECK_PAIRED) {
        BOOL    r_value;

        BOOL    bIsInitializing;
        BOOL    bIsInitialized;
        BOOL    bIsDiscoverable;
        BOOL    bIsConnecting;
        BOOL    bIsConnected;
        BOOL    bIsPinRequest;

        r_value = bBTGetStatus(&bIsInitializing, &bIsInitialized,
                               &bIsDiscoverable, &bIsConnecting,
                               &bIsConnected, &bIsPinRequest);       

        if (bIsConnected) {
            KillTimer(IDT_CHECK_PAIRED);
            EndDialog(IDOK);
            return;
        }
    }

    if (nIDEvent == IDT_TIMEOUT) {
        KillTimer(IDT_TIMEOUT);
        EndDialog(IDCANCEL);
    }
}

void
CDlgSetPin::OnOK() {
    USES_CONVERSION;

    UpdateData();
    WCHAR       wbuf[MAX_PATH];
    m_control_pin.GetWindowText(wbuf, MAX_PATH);
    bBTSendPin(W2A(wbuf));
#if 1
	CDialog::OnOK();
	return;
#else
	GetDlgItem(IDOK)->EnableWindow(FALSE);

    SetTimer(IDT_CHECK_PAIRED, 1000, 0);
    SetTimer(IDT_TIMEOUT, 60000, 0);
#endif

}

HBRUSH
CDlgSetPin::OnCtlColor(CDC *pDC, CWnd *pWnd, UINT nCtlColor) {
    switch (nCtlColor) {
      case CTLCOLOR_DLG:
      case CTLCOLOR_STATIC:
        //case CTLCOLOR_BTN:
        pDC->SetBkColor(WHITE);
        return (HBRUSH) (m_pWhiteBkBrush->GetSafeHandle());

      default:
        return CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
    }
}

BOOL CDlgSetPin::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
    RECT        main_rect;
	RECT		new_rect;

    GetWindowRect(&main_rect);

	new_rect.left = (theApp.m_ScreenWidth/2) - (main_rect.right-main_rect.left)/2;
	new_rect.right = (theApp.m_ScreenWidth/2) + (main_rect.right-main_rect.left)/2;
	new_rect.top = 50;
	new_rect.bottom = 50 + (main_rect.bottom - main_rect.top);

    this->MoveWindow(&new_rect, TRUE);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
