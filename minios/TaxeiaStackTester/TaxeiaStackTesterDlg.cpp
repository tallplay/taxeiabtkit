// TaxeiaStackTesterDlg.cpp : implementation file
//

#include "stdafx.h"
#include "TaxeiaStackTester.h"
#include "TaxeiaStackTesterDlg.h"
#include "PinDialog.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CTaxeiaStackTesterDlg dialog



CTaxeiaStackTesterDlg::CTaxeiaStackTesterDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTaxeiaStackTesterDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CTaxeiaStackTesterDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LED1, m_led[0]);	
	DDX_Control(pDX, IDC_LED2, m_led[1]);	
	DDX_Control(pDX, IDC_LED3, m_led[2]);	
	DDX_Control(pDX, IDC_LED4, m_led[3]);	
} 

BEGIN_MESSAGE_MAP(CTaxeiaStackTesterDlg, CDialog)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_WM_TIMER()
END_MESSAGE_MAP()

void AssignLinkKeyFileName(char *name)
{
    strcpy(name, "StackTester");
}


DWORD AssignBDAddr(void)
{
    return MassUart_GetUniqueID() + 5;
}

// CTaxeiaStackTesterDlg message handlers
static CTaxeiaStackTesterDlg *mycwnd; 

/////////////////////////////////////////////////////////////////////////////////////////////
 #define LED_CH1 1
 #define LED_CH2 2
 #define LED_CH3 4
 #define LED_CH4 8
 #define LED_ALL (1+2+4+8)
 
 #define OFF            (0)
 #define ON             (1)
 #define BLINK          (2)
 #define TOGGLE         (3)

void LED_Init(void)
{
   //all off
   mycwnd->m_led[0].SetDisabledBgColor(RGB_GRAY);
   mycwnd->m_led[1].SetDisabledBgColor(RGB_GRAY);
   mycwnd->m_led[2].SetDisabledBgColor(RGB_GRAY);
   mycwnd->m_led[3].SetDisabledBgColor(RGB_GRAY);
}

void LED_Set(U16 IdCh, U8 LEDStatus, U32 interval)
{
    U8 x;
    for(x = 0; x < 4; x++)
	{
	    if(IdCh & (1 << x))
		{
		    mycwnd->KillTimer(0x01d + (x << 8));
			
		    if(LEDStatus == ON)
	    	{
	    	    //off
	    	    mycwnd->m_led[x].SetDisabledBgColor(RGB_RED);
		    }
			else if(LEDStatus == OFF)
	    	{
	    	    //on
				mycwnd->m_led[x].SetDisabledBgColor(RGB_GRAY);		
		    }
			else if(LEDStatus == TOGGLE)
			{
			    //toggle
			    if( mycwnd->m_led[x].GetDisabledBgColor() == RGB_GRAY)
		    	    mycwnd->m_led[x].SetDisabledBgColor(RGB_RED);
				else
					mycwnd->m_led[x].SetDisabledBgColor(RGB_GRAY);
			}
			else if(LEDStatus == BLINK)
			{
			    mycwnd->SetTimer(0x01d + (x << 8), interval, 0);
			}

			mycwnd->m_led[x].Invalidate();			
		}
	}
}


/*TBD*/
U8 battery_charging_enabled = 0;
U8 battery_level = 3;
U8 battery_charging = 0;

void battery_charging_enable()
{
    /*TBD*/
    battery_charging_enabled = 1;
}

void battery_charging_disable()
{
    /*TBD*/
    battery_charging_enabled = 0;
    battery_charging = 0;
}

U8 battery_state_is_change()
{
    /*TBD*/
    static U32 count = 1;    
    
    if(!(count++ & 0x7fffff))
    {
        if(battery_charging_enabled)
        {
            battery_level = 2;
            battery_charging = 1;
        }
        return 1;
    }
    
    return 0;
}

#define UI_TASK 0
#define BT_TASK 1
#define IDLE_TASK 2
#define TICK_NUM_PER_SECOND 1000

U8 cur_task_id = UI_TASK;
U8 cur_task_request_more = 0;
U32 msTicks = 0;

U8 task_switch(void)
{
    static U32 lastUiTick = 0;
    if(cur_task_request_more)
    {
        cur_task_request_more = 0;
    }
    else //normal schedule
    {    
        if(cur_task_id == UI_TASK)
        {
           if(!( msTicks % (TICK_NUM_PER_SECOND)))//1s
                cur_task_id = IDLE_TASK;
           else
                cur_task_id = BT_TASK;
        }
        else if(cur_task_id == IDLE_TASK)
            cur_task_id = BT_TASK;
        else if(cur_task_id == BT_TASK) //give bt most time
        {
            if(msTicks > lastUiTick)
            {
                cur_task_id = UI_TASK;
                lastUiTick = msTicks;
            }
        }
    }

    return cur_task_id;
}

///////////Key grap//////////////
U8 is_tiny_changed = 0;
static U8 button_data[17] = {0, 0x80, 0x80, 0x80, 0x80, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

void tiny_set_data(U8 idx, U8 dat)
{
    if(idx)
	    button_data[idx] = dat;
	else
		button_data[0] |= dat;
	
	is_tiny_changed = 1;
}

void tiny_clear_data(U8 idx, U8 dat)
{
    if(idx)
		button_data[idx] = 0;
	else
		button_data[0] &= ~dat;
	is_tiny_changed = 1;
}

U8 tiny_get(U8 *dat)
{
    if(is_tiny_changed)
	{
	    is_tiny_changed = 0;
		memcpy(dat, button_data, 17);
		for(int i = 0; i < 17; i++)
			_CrtDbgReport(0, NULL, 0, NULL, "%02x ", button_data[i]);
		_CrtDbgReport(0, NULL, 0, NULL, "\r\n");
		return 0;
	}
    return 1;
}

void pin_code_input(void)
{
    CPinDialog cpin;
	U8 pin[100];
	cpin.DoModal();
	
	cpin.GetPinCode(pin);
    bt_gap_pincode_reply(pin + 1, pin[0]);
}

///////Pointer grap//////////////////
static U8 is_pointer_changed = 0;
static U8 pointer_data[2] = {0, 0};
void pointer_set_data(U8 idx, U8 dat)
{
    if(idx)
	    pointer_data[idx] = dat;
	else
		pointer_data[0] |= dat;
	
	is_pointer_changed = 1;
}

void pointer_clear_data(U8 idx, U8 dat)
{
	pointer_data[idx] = 0;
	is_pointer_changed = 1;
}

U8 pointer_get(U8 *dat)
{
    if(is_pointer_changed)
	{
	    is_pointer_changed = 0;
		memcpy(dat, pointer_data, 2);
		for(int i = 0; i < 2; i++)
			_CrtDbgReport(0, NULL, 0, NULL, "%02x ", pointer_data[i]);
		_CrtDbgReport(0, NULL, 0, NULL, "\r\n");
		return 0;
	}
    return 1;
}

void set_local_name()
{
    unsigned char addr[6];
    char name[40];
    bt_gap_get_mac_addr(addr);
    sprintf(name, "\xE8\xBF\x85\xE7\x8C\x9B\xE6\xB8\xAC\xE8\xA9\xA6-%02X%02X%02X\0", addr[0], addr[1], addr[2]);
    bt_gap_preset_local_name((U8*)name);//���r����
}
#if 1
#define PRINTF(...) _CrtDbgReport(0, NULL, 0, NULL, ##__VA_ARGS__);
void pad_usb_send()
{
}

U8 ui_task(void)
{
    U8 new_bt_state;
    U8 new_usb_connected;
    U8 battery_changed;
    U8 dat[17];
    U8 time_more = 0;
    static U8 tmp;

    static U8 cur_bt_state = 0xff;
    static U8 led_byte = 0x00;
    static U8 usb_connected = 0;
    static U8 is_in_hot_swap = 0;

    /*button*/
    if(!tiny_get(dat))
        pad_data_key_set(dat);

    /*pointer*/
    if(!pointer_get(dat))
        pad_data_pointer_set(dat);

    /*battery*/
    battery_changed = battery_state_is_change();
    //set battery into pad-data
    if(battery_changed)
        pad_data_set_battery(battery_level , battery_charging);

    /*led*/
    //get led info from pad-data and set led
    if(led_byte != pad_data_led_byte() || battery_changed)
    {
        led_byte = pad_data_led_byte();
        PRINTF( "leds : [");
        PRINTF( "%c", led_byte & 0x80 ? '!' : '.');
        PRINTF( "%c", led_byte & (1 << 4) ? '^' : '_');
        PRINTF( "%c", led_byte & (1 << 3) ? '^' : '_');
        PRINTF( "%c", led_byte & (1 << 2) ? '^' : '_');
        PRINTF( "%c]\r\n", led_byte & (1 << 1) ? '^' : '_');

        if(led_byte & 0x80) //usb connecting
            LED_Set(LED_ALL, BLINK, 1000);
        else if(led_byte & 0x40) //bt connecting
            LED_Set(LED_ALL, BLINK, 333);
        else if(led_byte & 0x20) //hot swap
        {
            LED_Set(LED_ALL, OFF, 0);
            LED_Set( (led_byte & (1 << 1) ? LED_CH1 : 0) |
                     (led_byte & (1 << 2) ? LED_CH2 : 0) |
                     (led_byte & (1 << 3) ? LED_CH3 : 0) |
                     (led_byte & (1 << 4) ? LED_CH4 : 0)
                     , BLINK, 100);
        }
        else
        {
            LED_Set(LED_CH1, led_byte & (1 << 1) ? (battery_charging ? BLINK : ON) : OFF, 1000);
            LED_Set(LED_CH2, led_byte & (1 << 2) ? (battery_charging ? BLINK : ON) : OFF, 1000);
            LED_Set(LED_CH3, led_byte & (1 << 3) ? (battery_charging ? BLINK : ON) : OFF, 1000);
            LED_Set(LED_CH4, led_byte & (1 << 4) ? (battery_charging ? BLINK : ON) : OFF, 1000);
        }
    }

////PS button state distinguish begin///////////////////
        static U32 ps_pressed_tick = 0;
        static U32 ps_release_tick = 0;
        static U32 ps_hold_last_time = 0;
        static U8 ps_click = 0;
        static U32 ps_pressed = 0;

        if(!ps_pressed && pad_data_is_ps_pressed()) //release-> pressed
            ps_pressed_tick = msTicks << 2;
        else if(ps_pressed && !pad_data_is_ps_pressed()) //pressed -> released
            ps_release_tick = msTicks << 2;

        ps_click = 0;

        if(ps_pressed_tick)
        {
            if(ps_release_tick)
            {
                ps_click = 1;

                ps_release_tick = ps_pressed_tick = 0;
                ps_hold_last_time = 0;
            }
            else
                ps_hold_last_time = (msTicks << 2) - ps_pressed_tick;
        }
        else //no press
        {
            ps_release_tick = ps_pressed_tick = 0;
            ps_hold_last_time = 0;
        }
////PS button state distinguish end///////////////////

////SELECT button state distinguish begin///////////////////
        static U32 select_pressed_tick = 0;
        static U32 select_release_tick = 0;
        static U32 select_hold_last_time = 0;
        static U8 select_click = 0;
        static U32 select_pressed = 0;

        if(!select_pressed && pad_data_is_select_pressed()) //release-> pressed
            select_pressed_tick = msTicks << 2;
        else if(select_pressed && !pad_data_is_select_pressed()) //pressed -> released
            select_release_tick = msTicks << 2;

        select_click = 0;

        if(select_pressed_tick)
        {
            if(select_release_tick)
            {
                select_click = 1;

                select_release_tick = select_pressed_tick = 0;
                select_hold_last_time = 0;
            }
            else
                select_hold_last_time = (msTicks << 2) - select_pressed_tick;
        }
        else //no press
        {
            select_release_tick = select_pressed_tick = 0;
            select_hold_last_time = 0;
        }
////SELECT button state distinguish end//////////////////

////START button state distinguish begin///////////////////
        static U32 start_pressed_tick = 0;
        static U32 start_release_tick = 0;
        static U32 start_hold_last_time = 0;
        static U8 start_click = 0;
        static U32 start_pressed = 0;

        if(!start_pressed && pad_data_is_start_pressed()) //release-> pressed
            start_pressed_tick = msTicks << 2;
        else if(start_pressed && !pad_data_is_start_pressed()) //pressed -> released
            start_release_tick = msTicks << 2;

        start_click = 0;

        if(start_pressed_tick)
        {
            if(start_release_tick)
            {
                start_click = 1;

                start_release_tick = start_pressed_tick = 0;
                start_hold_last_time = 0;
            }
            else
                start_hold_last_time = (msTicks << 2) - start_pressed_tick;
        }
        else //no press
        {
            start_release_tick = start_pressed_tick = 0;
            start_hold_last_time = 0;
        }
////START button state distinguish end//////////////////
/*Hot swap*/
     static U8 cur_mode;
    if(is_in_hot_swap)
    {
        if(select_click)
        {
            pad_data_state_notify(LEAVE_HOT_SWAP_MODE);
            is_in_hot_swap = 0;
            pad_set_mode(cur_mode);
        }

        if(is_in_hot_swap == 1) //need to show mode
        {
            if(start_click)
            {
                pad_data_state_notify(MODE1_SHOW + cur_mode);
                is_in_hot_swap = 2;
            }
        }
        else if(is_in_hot_swap == 2) //need to change and show mode
        {
            if(start_click)
            {
                cur_mode += 1;
                if(cur_mode > PAD_HMODE_KEYBOARD_MOUSE)
                    cur_mode = PAD_HMODE_JOYSTICK;
                pad_data_state_notify(MODE1_SHOW + cur_mode);
            }
        }
    }
    else if(select_hold_last_time > 5000)
    {
        pad_data_state_notify(ENTER_HOT_SWAP_MODE);
        is_in_hot_swap = 1;
        cur_mode = pad_get_mode();
    }

    /*usb*//*BT*/
#if (TARGET_MODE != TARGET_MODE_NO_USB)
    new_usb_connected = pad_usb_connected();
#else
    new_usb_connected = 0;
#endif

    new_bt_state = bt_state();

    if(new_usb_connected)
    {
        pad_usb_send();

        //BT
        if(new_bt_state == BT_STA_STOP)
        {   //don't runing bt stack
            time_more = 1;
        }
        else
        {
            if(new_bt_state == BT_STA_CONNECTING || new_bt_state == BT_STA_DISCONNECTED)
            {
                bt_stop();
            }
            else if(new_bt_state == BT_STA_HID_CONNECTED)
            {
                bt_gap_disconnect();
            }
        }

    }
    else
    {
        if(new_bt_state == BT_STA_STOP)
        {
            if(ps_hold_last_time > 1000) //hold PS > 1sec, go to pairing
            {
                ps_pressed_tick = 0;
                bt_start((msTicks << 2), BT_WMODE_INQUIRY_SCAN, 0);
                tmp = 1;
            }
            else if(ps_click) //press PS and release to go to connecting
            {
                bt_start((msTicks << 2), BT_WMODE_PAGING, 0);
                tmp = 0;
            }
        }
        else if(new_bt_state == BT_STA_PAIRING)
        {
            if(ps_click) //press PS to stop pairing
            {
                bt_stop();             //stop pairing by stopping bt                
            }
			else if(bt_gap_is_pincode_req())
			{
			    pin_code_input();
			}
			
        }
		else if(new_bt_state == BT_STA_CONNECTED)
		{
			if(bt_gap_is_pincode_req())
			{
			    pin_code_input();
			}
		}
        else if(new_bt_state == BT_STA_DISCONNECTED)
        {
            bt_stop();
        }
        else if(new_bt_state == BT_STA_HID_CONNECTED)
        {
            if(ps_hold_last_time > 2000) //hold PS > 2sec, disconnect BT
            {
                ps_pressed_tick = 0;
                bt_gap_disconnect();
            }
			else if(pad_data_is_report_enabled())
			{
	            if(pad_data_key_change())
	                bt_hid_send_key_mouse_joystick_media(2);
	            else if(pad_data_pointer_change())
	                bt_hid_send_key_mouse_joystick_media(1);
			}
			else if(pad_data_is_report_enabled2disabled())
			{
			    bt_gap_disconnect();
			}


        }            
    }

    ps_pressed = pad_data_is_ps_pressed();
    select_pressed = pad_data_is_select_pressed();
    start_pressed = pad_data_is_start_pressed();
    
    
    /*state change*/
    if(new_bt_state == BT_STA_BT_FAIL)
    {
        PRINTF( "bt fail~~\r\n");

        while(1);
    }
    else
    {

        //USB
        if(new_usb_connected != usb_connected)
        {
            usb_connected = new_usb_connected;

            if(usb_connected)
            {
                pad_data_state_notify(USB_CONNECTING);
                battery_charging_enable();
            }
            else
            {
                pad_data_state_notify(USB_DISCONNECTED);
                battery_charging_disable();
            }
        }

        if(cur_bt_state != new_bt_state)
        {
            cur_bt_state = new_bt_state;
            PRINTF( "<<<<BT_STATE>>> %d\r\n", cur_bt_state);

            switch(cur_bt_state)
            {
                case BT_STA_STOP:
                   pad_data_state_notify(BT_DISCONNECTED);
                   break;
                case BT_STA_POWERUP:
                    if(tmp)
                        pad_data_state_notify(BT_PAIRING);
                    else
                        pad_data_state_notify(BT_CONNECTING);                    
                   break;
                case BT_STA_BOOT:
                    break;
                case BT_STA_INITILIZED:
                    break;
                case BT_STA_PAIRING:
                    pad_data_state_notify(BT_PAIRING);
                    break;
                case BT_STA_CONNECTING:
                    pad_data_state_notify(BT_CONNECTING);                    
                    break;
                case BT_STA_HID_CONNECTED:
                    pad_data_state_notify(BT_CONNECTED);
                    break;
                case BT_STA_DISCONNECTING:
                    break;
                case BT_STA_DISCONNECTED:
                    pad_data_state_notify(BT_DISCONNECTED);                    
                    break;
                case BT_STA_BT_FAIL:
                    break;
                case BT_STA_DISCONNECT_RETRY:
                    break;
                case BT_STA_LOCK:
					break;
                   
            }
        }

    }

    return time_more;
}

#else
U8 ui_task(void)
{
    U8 new_bt_state;
    U8 new_usb_connected;
    U8 battery_changed;
    U8 dat[17];
    U8 time_more = 0;
    
    static U8 cur_bt_state = 0xff;
    static U8 led_byte = 0x00;    
    static U8 usb_connected = 0;
	static U8 is_in_hot_swap = 0;

/*button*/
    if(!tiny_get(dat))
        pad_data_key_set(dat);

/*pointer*/
    if(!pointer_get(dat))
		pad_data_pointer_set(dat);

/*battery*/
    battery_changed = battery_state_is_change();
    //set battery into ps3-data
    if(battery_changed)
        pad_data_set_battery(battery_level , battery_charging);

/*led*/
    //get led info from ps3-data and set led
    if(led_byte != pad_data_led_byte() || battery_changed)
    {
        led_byte = pad_data_led_byte();
        _CrtDbgReport(0, NULL, 0, NULL, "leds : [");
        _CrtDbgReport(0, NULL, 0, NULL, "%c", led_byte & 0x80 ? '!' : '.');
        _CrtDbgReport(0, NULL, 0, NULL, "%c", led_byte & (1 << 4) ? '^' : '_');
        _CrtDbgReport(0, NULL, 0, NULL, "%c", led_byte & (1 << 3) ? '^' : '_');
        _CrtDbgReport(0, NULL, 0, NULL, "%c", led_byte & (1 << 2) ? '^' : '_');
        _CrtDbgReport(0, NULL, 0, NULL, "%c]\r\n", led_byte & (1 << 1) ? '^' : '_'); 

        if(led_byte & 0x80) //usb connecting
            LED_Set(LED_ALL, BLINK, 1000);
        else if(led_byte & 0x40) //bt connecting
            LED_Set(LED_ALL, BLINK, 333);
		else if(led_byte & 0x20) //hot swap
		{
            LED_Set(LED_ALL, OFF, 0);
		    LED_Set( (led_byte & (1 << 1) ? LED_CH1 : 0) |
				    (led_byte & (1 << 2) ? LED_CH2 : 0) |
				    (led_byte & (1 << 3) ? LED_CH3 : 0) |
				    (led_byte & (1 << 4) ? LED_CH4 : 0)
		    , BLINK, 100);
	    }
        else
        {
            LED_Set(LED_CH1, led_byte & (1 << 1) ? (battery_charging ? BLINK : ON) : OFF, 1000);
            LED_Set(LED_CH2, led_byte & (1 << 2) ? (battery_charging ? BLINK : ON) : OFF, 1000);
            LED_Set(LED_CH3, led_byte & (1 << 3) ? (battery_charging ? BLINK : ON) : OFF, 1000);
            LED_Set(LED_CH4, led_byte & (1 << 4) ? (battery_charging ? BLINK : ON) : OFF, 1000);
        }
    }


/*usb*/    
#ifdef USB_OK
#if (TARGET_MODE != TARGET_MODE_NO_USB)
        new_usb_connected = pad_usb_main();
#else
        new_usb_connected = 0;
#endif

    //connect state change
    if(new_usb_connected != usb_connected)
    {
        usb_connected = new_usb_connected;
        if(usb_connected) //->just become connected
        {
            pad_data_state_notify(USB_CONNECTING);
            battery_charging_enable();
        }
        else //->just become disconnected
        {
            pad_data_state_notify(USB_DISCONNECTED);
            battery_charging_disable();            
        }
    }
#endif
/*BT*/
    new_bt_state = bt_state();
    if(usb_connected) //usb connected
    {        
        if(new_bt_state == BT_STA_STOP)
        {//don't runing bt stack
           time_more = 1;
        }
        else
        {
            if(new_bt_state == BT_STA_CONNECTING || new_bt_state == BT_STA_DISCONNECTED)
            {
                bt_stop();
            }
            else if(new_bt_state == BT_STA_HID_CONNECTED || new_bt_state == BT_STA_HFP_CONNECTED)
            {
#ifdef NEW_SDK
				bt_gap_disconnect();
#else
                 bt_disconnect();
#endif
            }
        }



    }
    else //usb not connected
    {

////PS button state distinguish begin///////////////////	
        static U32 ps_pressed_tick = 0;
		static U32 ps_release_tick = 0;
		static U32 ps_hold_last_time = 0;
		static U8 ps_click = 0;
        static U32 ps_pressed = 0;

		if(!ps_pressed && pad_data_is_ps_pressed()) //release-> pressed
			ps_pressed_tick = msTicks;
		else if(ps_pressed && !pad_data_is_ps_pressed()) //pressed -> released
			ps_release_tick = msTicks;

        ps_click = 0;
		
		if(ps_pressed_tick)
		{
			if(ps_release_tick)
			{
    			ps_click = 1;
				
				ps_release_tick = ps_pressed_tick = 0;
				ps_hold_last_time = 0;
		    }
			else
			    ps_hold_last_time = msTicks - ps_pressed_tick;
		}
		else //no press
		{
			ps_release_tick = ps_pressed_tick = 0;
			ps_hold_last_time = 0;
		}
////PS button state distinguish end///////////////////	

////SELECT button state distinguish begin///////////////////	
        static U32 select_pressed_tick = 0;
		static U32 select_release_tick = 0;
		static U32 select_hold_last_time = 0;
		static U8 select_click = 0;
        static U32 select_pressed = 0;

		if(!select_pressed && pad_data_is_select_pressed()) //release-> pressed
			select_pressed_tick = msTicks;
		else if(select_pressed && !pad_data_is_select_pressed()) //pressed -> released
			select_release_tick = msTicks;

        select_click = 0;
		
		if(select_pressed_tick)
		{
			if(select_release_tick)
			{
    			select_click = 1;
				
				select_release_tick = select_pressed_tick = 0;
				select_hold_last_time = 0;
		    }
			else
			    select_hold_last_time = msTicks - select_pressed_tick;
		}
		else //no press
		{
			select_release_tick = select_pressed_tick = 0;
			select_hold_last_time = 0;
		}
////SELECT button state distinguish end//////////////////

////START button state distinguish begin///////////////////	
        static U32 start_pressed_tick = 0;
		static U32 start_release_tick = 0;
		static U32 start_hold_last_time = 0;
		static U8 start_click = 0;
        static U32 start_pressed = 0;

		if(!start_pressed && pad_data_is_start_pressed()) //release-> pressed
			start_pressed_tick = msTicks;
		else if(start_pressed && !pad_data_is_start_pressed()) //pressed -> released
			start_release_tick = msTicks;

        start_click = 0;
		
		if(start_pressed_tick)
		{
			if(start_release_tick)
			{
    			start_click = 1;
				
				start_release_tick = start_pressed_tick = 0;
				start_hold_last_time = 0;
		    }
			else
			    start_hold_last_time = msTicks - start_pressed_tick;
		}
		else //no press
		{
			start_release_tick = start_pressed_tick = 0;
			start_hold_last_time = 0;
		}
////START button state distinguish end//////////////////
        if(is_in_hot_swap)
        {
		    if(select_click)
	    	{
			    pad_data_state_notify(LEAVE_HOT_SWAP_MODE);
    			is_in_hot_swap = 0;
		    }

			if(is_in_hot_swap == 1) //need to show mode
			{
				if(start_click)
    			{
#ifdef NEW_SDK
		 		    pad_data_state_notify(MODE1_SHOW + bt_hid_get_mode());
#else
		 		    pad_data_state_notify(MODE1_SHOW + bt_get_mode());
#endif
				    is_in_hot_swap = 2;
	    		}
			}
			else if(is_in_hot_swap == 2) //need to change and show mode
			{
				if(start_click)
    			{
#ifdef NEW_SDK
    			    bt_hid_set_mode(bt_hid_get_mode()+ 1);
		 		    pad_data_state_notify(MODE1_SHOW + bt_hid_get_mode());
#else
    			    bt_set_mode(bt_get_mode()+ 1);
		 		    pad_data_state_notify(MODE1_SHOW + bt_get_mode());
#endif					
	    		}
        	}
        }
		else if(select_hold_last_time > 5000)
    	{
    	    pad_data_state_notify(ENTER_HOT_SWAP_MODE);
			is_in_hot_swap = 1;
        }
        else if(new_bt_state == BT_STA_STOP)
        {						
            if(ps_hold_last_time > 1000) //hold PS > 1sec, go to pairing
            {
                set_local_name();
                ps_pressed_tick = 0;
                pad_data_state_notify(BT_PAIRING);                
                bt_start(GetTickCount(), BT_WMODE_INQUIRY_SCAN, PROFILE_TYPE);
            }
			else if(ps_click) //press PS and release to go to connecting
			{
			    set_local_name();
                pad_data_state_notify(BT_CONNECTING);                
                bt_start(GetTickCount(), BT_WMODE_PAGING, PROFILE_TYPE);
		    }
        }
		else if(new_bt_state == BT_STA_PAIRING)
		{
		    if(ps_click) //press PS to stop pairing
	    	{
	            bt_stop();             //stop pairing by stopping bt
	            pad_data_state_notify(BT_DISCONNECTED);                
	    	}
		}
        else if(new_bt_state == BT_STA_DISCONNECTED)
        {            
            bt_stop();            
            pad_data_state_notify(BT_DISCONNECTED);                
        }
        else if(new_bt_state == BT_STA_HID_CONNECTED || new_bt_state == BT_STA_HFP_CONNECTED)
        {
            if(cur_bt_state != new_bt_state)
				pad_data_state_notify(BT_CONNECTED);
			
            if(ps_hold_last_time > 2000) //hold PS > 2sec, disconnect BT
        	{
                ps_pressed_tick = 0;
#ifdef NEW_SDK
				bt_gap_disconnect();
#else
				bt_disconnect();
#endif
            }
			else if(pad_data_key_change())
#ifdef NEW_SDK
                bt_hid_send_key();
#else
                bt_send_key();
#endif
			else if(pad_data_pointer_change())
#ifdef NEW_SDK
                bt_hid_send_mouse();
#else
				bt_send_mouse();
#endif
			
        }
		
    	ps_pressed = pad_data_is_ps_pressed();
    	select_pressed = pad_data_is_select_pressed();
    	start_pressed = pad_data_is_start_pressed();
    }


/*state printing*/
    if(new_bt_state == BT_STA_BT_FAIL)
    {
        _CrtDbgReport(0, NULL, 0, NULL, "bt fail~~\r\n");

        while(1);
    }
    else
    {

        if(cur_bt_state != new_bt_state)
        {
            cur_bt_state = new_bt_state;
            _CrtDbgReport(0, NULL, 0, NULL, "<<<<BT_STATE>>> %d\r\n", cur_bt_state);
        }        
        
    }   

    return time_more;
}
#endif

static DWORD FAR PASCAL osThread(LPSTR lpData);

static DWORD FAR PASCAL osThread(LPSTR lpData)
{
	while(1) 
	{
        msTicks = GetTickCount() >> 2;

	    switch(task_switch())
    	{
    	    case BT_TASK:
			    cur_task_request_more = bt_task(msTicks <<2);
			break;
			case UI_TASK:
				cur_task_request_more = ui_task();
				break;
		    case IDLE_TASK:
				break;
	    }

	}
	ASSERT(0);
	return 0;
}
/////////////////////////////////////////////////////////////////////////////////////////////
void CTaxeiaStackTesterDlg::OnOK()
{
	CDialog::OnOK();

//	StopHciLog();
}

BT_LOCAL_INFO *bt_info;
BOOL CTaxeiaStackTesterDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
    mycwnd = this;

   // TODO: Add extra initialization here
    RAWINPUTDEVICE      Rid[1];

    // Keyboard
    Rid[0].usUsagePage = 0x01; //generic desktop
    Rid[0].usUsage = 0x06; //keyboard 
    Rid[0].dwFlags = RIDEV_INPUTSINK;//RIDEV_INPUTSINK;//RIDEV_NOLEGACY; 
    Rid[0].hwndTarget = m_hWnd;

    if (RegisterRawInputDevices(Rid, 1, sizeof(Rid[0])) == FALSE) {
        //registration failed. Call GetLastError for the cause of the error
        EndDialog(0);
        return TRUE;
    }
	

    pad_data_init(battery_level, battery_charging);
	bt_init();
	LED_Init();
    //create main loop thread
    HANDLE OSThread;
	DWORD dwThreadID;

    OSThread = CreateThread((LPSECURITY_ATTRIBUTES)NULL,
                              0,
                              (LPTHREAD_START_ROUTINE)osThread,
                              (LPVOID)NULL,
                              0, &dwThreadID);
    CloseHandle(OSThread);

    SetTimer(1118, 1000, 0); //for status update

   Bt__GetLocalInfo(&bt_info);
   
#ifdef TAXEIA_UI_LOG
    StartHciLog(AfxGetInstanceHandle(), SW_SHOW);
#endif


	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CTaxeiaStackTesterDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CTaxeiaStackTesterDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CTaxeiaStackTesterDlg::SetDlgText(U16 staticid, const char *format, ...)
{
    char buffer[512];  /* Output buffer */
    va_list     args;
    
    va_start(args, format);
    _vsnprintf(buffer, 512, format, args);
    va_end(args);

	this->GetDlgItem(staticid)->SetWindowTextA(buffer);
}



void BtLocalDeviceStatusStr(char *buf, U16 len)
{    

    U8 i;
    char versionstr[][20] = {"BT Spec 1.0b"
                            ,"BT Spec 1.1"
                            ,"BT Spec 1.2"
                            ,"BT Spec 2.0 + EDR"
                            ,"BT Spec 2.1 + EDR"
                            ,"BT Spec 3.0 + HS"
                            ,"BT Spec 4.0"
                            ,"Unknown"};

    if(len >= 290)
    {    
        sprintf(buf, "HCI version\t: %s(0x%02x)\r\n", versionstr[bt_info->HCIVersion], bt_info->HCIVersion);
        sprintf(buf, "%sHCI revision\t: 0x%x\r\n", buf, bt_info->HCIRevision);
        sprintf(buf, "%sLMP/PAL version\t: %s(0x%02x)\r\n", buf, versionstr[bt_info->LMPALVersion], bt_info->LMPALVersion);
        sprintf(buf, "%sManufactor name\t: 0x%x\r\n", buf, bt_info->ManufactorName);
        sprintf(buf, "%sLMP/PAL revision\t: 0x%x\r\n", buf, bt_info->LMPALRevision);
        
        sprintf(buf, "%sBDADDR\t\t: ", buf);
        for(i = 0; i < 5; i++)
            sprintf(buf, "%s%02x:", buf, bt_info->BT_ADDR[5-i]);
        sprintf(buf, "%s%02x", buf, bt_info->BT_ADDR[0]);
        sprintf(buf, "%s\r\n", buf);

        sprintf(buf, "%sACL max (len,num)\t: (%d,%d)\r\n", buf, bt_info->ACLPktLen, bt_info->ACLPktNum);
        sprintf(buf, "%sSCO max (len,num)\t: (%d,%d)\r\n", buf, bt_info->SCOPktLen, bt_info->SCOPktNum);

        sprintf(buf, "%sPageTimeout\t: %d seconds\r\n", buf, (bt_info->PageTimeout * 625)/1000);
     
        sprintf(buf, "%sAuth Enabled\t: %d\r\n", buf, bt_info->AuthEnabled);
        sprintf(buf, "%sScan Enabled\t: %d\r\n", buf, bt_info->ScanEnabled);

        sprintf(buf, "%sClassOfDevice\t: 0x%06x\r\n", buf, bt_info->COD);
        sprintf(buf, "%sFeatures\t\t: %02x%02x%02x%02x%02x%02x%02x%02x%02x\r\n", buf, bt_info->feature[0],bt_info->feature[1],
            bt_info->feature[2],bt_info->feature[3],bt_info->feature[4],bt_info->feature[5],bt_info->feature[6],bt_info->feature[7],bt_info->feature[8]);
    }
}
static U16 keyboardhit = 0;
void CTaxeiaStackTesterDlg::OnTimer(UINT_PTR nIDEvent)
{
    
 	// TODO: Add your message handler code here and/or call default
 	if((nIDEvent & 0xff) == 0x1d) //led
	{
	    U8 x = (nIDEvent >> 8) & 0x3;
	    if( mycwnd->m_led[x].GetDisabledBgColor() == RGB_GRAY)
    	    mycwnd->m_led[x].SetDisabledBgColor(RGB_RED);
		else
			mycwnd->m_led[x].SetDisabledBgColor(RGB_GRAY);
		mycwnd->m_led[x].Invalidate();
	}
	
 	if(nIDEvent == 650108)
 	{
	    KillTimer(650108);
 	}
	else if(nIDEvent == 1118) //for status update
	{
		//update stack status
		BYTE buff[1024];
	    char stackstr[][20] = {"Stop", "Initializing", "Initialized", "Busy", "RxBusy"};

		sprintf((char*)buff, "Stack\t: %s\r\n", stackstr[BTCoreState()]);

        char statestr[][30] = {
            "BT_STA_STOP",
            "BT_STA_POWERUP",
            "BT_STA_BOOT",
            "BT_STA_INITILIZED",
            "BT_STA_PAIRING",
            "BT_STA_CONNECTING",
            "BT_STA_CONNECTED",
            "BT_STA_HID_CONNECTED",
            "BT_STA_HFP_CONNECTED",
            "BT_STA_HID_HFP_CONNECTED",
            "BT_STA_DISCONNECTING",
            "BT_STA_DISCONNECTED",
            "BT_FAIL",
            "BT_STA_DISCONNECT_RETRY",
            "BT_STA_LOCK"
        	};

		sprintf((char*)buff, "%sBT\t: %s\r\n", buff, statestr[bt_state()]);

		sprintf((char*)buff, "%sHID mode: %d\r\n", buff, pad_get_mode());
		
		SetDlgText(IDC_EDIT_STACK_STATUS, (const char *)buff);

		BtLocalDeviceStatusStr((char*)buff, 1024);
		
		SetDlgText(IDC_EDIT_LOCAL_STATUS, (const char *)buff);		
	}
	else if(nIDEvent == IDC_BUTTON_CMD_SEND)
	{
	}
	else if(nIDEvent == IDC_BUTTON_CMD_SEND+1)
	{
	}
	else
	{
	}

	CDialog::OnTimer(nIDEvent);
}



BOOL CTaxeiaStackTesterDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: Add your specialized code here and/or call the base class
    if( pMsg->message == WM_KEYUP)
	{
		return SendMessage(WM_KEYUP, pMsg->wParam, pMsg->lParam);
	}
    if( pMsg->message == WM_KEYDOWN)
	{
		return SendMessage(WM_KEYDOWN, pMsg->wParam, pMsg->lParam);
	}
	return CDialog::PreTranslateMessage(pMsg);
}

LRESULT CTaxeiaStackTesterDlg::DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
#if (PROFILE_TYPE == PROFILE_TYPE_PS3)

    static U8 lx = 128 , ly = 128;
    CString     strTemp;

	
    if (message == WM_INPUT) {
        unsigned int r_value;
        unsigned int buf_size;
        unsigned char * pBuffer;
        RAWINPUT*pRawInput;

		if(!(hidinfo.aclconnect && hidinfo.hidcconnect && hidinfo.hidiconnect))
			return TRUE;

        buf_size = 0;
        r_value = GetRawInputData((HRAWINPUT) lParam, (UINT) RID_INPUT,
                                  (LPVOID) NULL, (PUINT) & buf_size,
                                  (UINT)sizeof(RAWINPUTHEADER));

        if (r_value != 0) {
            return CDialog::DefWindowProc(message, wParam, lParam);
        }

        pBuffer = (unsigned char *) malloc(buf_size);
        if (!pBuffer) {
            return CDialog::DefWindowProc(message, wParam, lParam);
        }

        GetRawInputData((HRAWINPUT) lParam, (UINT) RID_INPUT,
                        (LPVOID) pBuffer, (PUINT) & buf_size,
                        (UINT)sizeof(RAWINPUTHEADER));

        pRawInput = (RAWINPUT *) pBuffer;

        if (pRawInput->header.dwType == RIM_TYPEMOUSE) {
			U8 x;
			U8 y;
			U8 btns;

			x = pRawInput->data.mouse.lLastX;
			y = pRawInput->data.mouse.lLastY;
			btns = pRawInput->data.mouse.usButtonFlags;

//            strTemp.Format("usFlags=%02X lLastX=%08X lLastY=%08X usButtonFlags=%04X usButtonData=%04X\n",
  //                         pRawInput->data.mouse.usFlags,
    //                       pRawInput->data.mouse.lLastX,
      //                     pRawInput->data.mouse.lLastY,
        //                   pRawInput->data.mouse.usButtonFlags,
          //                 pRawInput->data.mouse.usButtonData);

            static U8 enableCnt = 0;
            if(btns == 1) //click down
                enableCnt = !enableCnt;
			
            if(enableCnt) //click down
        	{
        	  
                /* X!!!!*/
				if(x && x < 128)//positive
				{
				    if(lx + x > 255)
						lx = 255;
					else
				    {
				        if(lx < 128 + 75)
				        {
				            if(lx + x > 128+74)
								lx = 128 + 75;
							else
							    lx += x;
				        }
						else
							lx += x/4;
				    }
				}
				else if(x == 0)
				{
				}
				else
				{ 
				    x = (x ^0xff) + 1; //abolute value

                    if(lx - x < 0)
						lx = 0;
					else
					{
					    if(lx > 128 - 75)
					    {
					        if(lx - x < 128 - 74)
								lx = 128 - 75;
							else
								lx -= x;
					    }
						else
							lx -= x/4;
				    }
				}

                 /* Y!!!!*/
				if(y && y < 128)//positive
				{
				    if(ly + y > 255)
						ly = 255;
					else
				    {
				        if(ly < 128 + 75)
				        {
				            if(ly + y > 128+74)
								ly = 128 + 75;
							else
							    ly += y;
				        }
						else
							ly += y/4;
				    }
				}
				else if(y == 0)
				{
				}
				else
				{ 
				    y = (y ^ 0xff) + 1; //abolute value

                    if(ly - y < 0)
						ly = 0;
					else
					{
					    if(ly > 128 - 75)
					    {
					        if(ly - y < 128 - 74)
								ly = 128 - 75;
							else
								ly -= y;
					    }
						else
							ly -= y/4;
				    }
				}
                //if(!enableCnt)
	   		     //   SetTimer(787778, 50, 0);

			    strTemp.Format("%x\n", lx);
				OutputDebugStringA(strTemp);


				Bt__PS3AssignJoyStick(sa_lstick_x, lx);
				Bt__PS3AssignJoyStick(sa_lstick_y, ly);
				
				if(BTCoreState() != BUSY && Bt_AclAvable())
				{
    				Bt__PS3SendKeycode();
			    }				
			}
			else
			{
			    lx = 128; ly = 128;
	    		Bt__PS3AssignJoyStick(sa_lstick_x, lx);
    			Bt__PS3AssignJoyStick(sa_lstick_y, ly);
                  //  KillTimer(787778);
			}

        }
		
		free(pBuffer);

        return TRUE;
    }
	else if(message == WM_TIMER)
	{
		if(wParam == 787778)
		{
		/*
     		lx = ly = 128;
		    strTemp.Format("%x\n", lx);
    		OutputDebugStringA(strTemp);
	        KillTimer(787778);
			Bt__PS3AssignJoyStick(sa_lstick_x, lx);
			Bt__PS3AssignJoyStick(sa_lstick_y, ly);
			
			if(BTCoreState() == BUSY || !Bt_AclAvable())
				return TRUE;
			
			Bt__PS3SendKeycode();
*/
		    return TRUE;
		}
	}
#else
 if (message == WM_INPUT) {
        unsigned int r_value;
        unsigned int buf_size;
        unsigned char * pBuffer;
        RAWINPUT*pRawInput;


        buf_size = 0;
        r_value = GetRawInputData((HRAWINPUT) lParam, (UINT) RID_INPUT,
                                  (LPVOID) NULL, (PUINT) & buf_size,
                                   (UINT)sizeof(RAWINPUTHEADER));

        if (r_value != 0) {
            return CDialog::DefWindowProc(message, wParam, lParam);
        }

        pBuffer = (unsigned char *) malloc(buf_size);
        if (!pBuffer) {
            return CDialog::DefWindowProc(message, wParam, lParam);
        }

        GetRawInputData((HRAWINPUT) lParam, (UINT) RID_INPUT,
                        (LPVOID) pBuffer, (PUINT) & buf_size,
                        (UINT)sizeof(RAWINPUTHEADER));

        pRawInput = (RAWINPUT *) pBuffer;

        if (pRawInput->header.dwType == RIM_TYPEKEYBOARD) 
		{	
			UINT nChar = pRawInput->data.keyboard.VKey;
			
		    if(!pRawInput->data.keyboard.Flags) //key down
	    	{
				if(nChar == 'U')
				{
			        tiny_set_data(7, 0xff);
				}
				else if(nChar == 'D')
				{
			        tiny_set_data(8, 0xff);
				}
				else if(nChar == 'L')
				{
			        tiny_set_data(6, 0xff);
				}
				else if(nChar == 'R')
				{
			        tiny_set_data(5, 0xff);
				}

				if(nChar == VK_NUMPAD8)
				{
				    tiny_set_data(9, 0xff); //tri
				}

				if(nChar == VK_NUMPAD6)
				{
				    tiny_set_data(10, 0xff); //cir
				}

				if(nChar == VK_NUMPAD4)
				{
				    tiny_set_data(12, 0xff); //squ
				}

				if(nChar == VK_NUMPAD2)
				{
				    tiny_set_data(11, 0xff); //cro
				}

				if(nChar == 'P')
				    tiny_set_data(0, 0x10); //ps

				if(nChar == 'S')
					tiny_set_data(0, 0x01); //sel

				if(nChar == 'T')
					tiny_set_data(0, 0x02); //start

			    if(nChar == VK_OEM_6) //']'
		    	{
		    	    pointer_set_data(0, 1); //x = 1;
		    	}

				if(nChar == VK_OEM_4) //'['
				{
				    pointer_set_data(0, -1); //x = -1;
				}

			    if(nChar == VK_OEM_PLUS) //'+'
		    	{
		    	    pointer_set_data(1, -1); //y = -1;
		    	}

				if(nChar == VK_OEM_7) //'''
				{
				    pointer_set_data(1, 1); //y = 1;
				}

	    	}
		    else //key up
	    	{
				if(nChar == 'U')
				{
			        tiny_clear_data(7, 0xff);
				}
				else if(nChar == 'D')
				{
			        tiny_clear_data(8, 0xff);
				}
				else if(nChar == 'L')
				{
			        tiny_clear_data(6, 0xff);
				}
				else if(nChar == 'R')
				{
			        tiny_clear_data(5, 0xff);
				}

				if(nChar == VK_NUMPAD8)
				{
				    tiny_clear_data(9, 0xff); //tri
				}
				if(nChar == VK_NUMPAD6)
				{
				    tiny_clear_data(10, 0xff); //cir
				}
				if(nChar == VK_NUMPAD4)
				{
				    tiny_clear_data(12, 0xff); //squ
				}
				if(nChar == VK_NUMPAD2)
				{
				    tiny_clear_data(11, 0xff); //cro
				}

				if(nChar == 'P')
				    tiny_clear_data(0, 0x10); //ps

				if(nChar == 'S')
					tiny_clear_data(0, 0x01); //sel

				if(nChar == 'T')
					tiny_clear_data(0, 0x02); //start

			    if(nChar == VK_OEM_6) //']'
		    	{
		    	    pointer_clear_data(0, 1); //x = 1;
		    	}

				if(nChar == VK_OEM_4) //'['
				{
				    pointer_clear_data(0, -1); //x = -1;
				}

			    if(nChar == VK_OEM_PLUS) //'+'
		    	{
		    	    pointer_clear_data(1, -1); //y = -1;
		    	}

				if(nChar == VK_OEM_7) //'''
				{
				    pointer_clear_data(1, 1); //y = 1;
				}
					
	    	}
    	}
			
		
		free(pBuffer);

        return TRUE;
    }
#endif
    return CDialog::DefWindowProc(message, wParam, lParam);
}

#if (PROFILE_TYPE == 1)
void bt_hid_send_key(void)
{
}

void bt_hid_send_mouse(void)
{
}

void bt_hid_set_mode(U8 mode)
{
}

U8 bt_hid_get_mode(void)
{
    return 0;
}

#endif

