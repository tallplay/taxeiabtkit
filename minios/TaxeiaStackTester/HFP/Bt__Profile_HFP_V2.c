#include "BtConfig.h"
#include "Bt__Profile_HFP_V2.h"
#include "../TaxeiaConsoleDebug/TaxeiaConsoledebug.h"
#include "../SDP/SDPS/Bt__Profile_SDPS_V2.h"
#include "../CORE/BTCore_V2.h"
#include "../rfcomm/rfcomm_V2.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#if  (PROFILE_TYPE == PROFILE_TYPE_HID_HFP  || PROFILE_TYPE == PROFILE_TYPE_HFP || PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
///////////////////////////////////Public API///////////////////////////////////////////

///////////////////////////////////Private Functions////////////////////////////////////
/* Handsfree Profile */
#define PROF_HFP                    0x111E
#define SERV_GEN_AUDIO              0x1203

#define PROT_L2CAP                  0x0100
#define PROT_RFC                    0x0003

static const U8 HfpRecordHandle [] = {
    SDP_UINT_SIZE32(0x00010000)
};

static const U8 HfpRecordState [] = {
    SDP_UINT_SIZE32(0x20111118)
};

static const U8 HfpServClassIdVal[] = {
    SDP_DATA_ELEMENT_SEQ_SIZE8(6),           /* Data element sequence, 3 bytes */
    SDP_UUID_SIZE16(PROF_HFP), /* Uuid16 HFP */
    SDP_UUID_SIZE16(SERV_GEN_AUDIO) /*uuid16 generic audio*/
};

static const U8 HfpProtocolDescListVal[] = {
    SDP_DATA_ELEMENT_SEQ_SIZE8(12),  /* Data element sequence, 12 bytes */
    SDP_DATA_ELEMENT_SEQ_SIZE8(3),   /* Data element sequence for L2CAP, 3 bytes */     
    SDP_UUID_SIZE16(PROT_L2CAP),  /* Uuid16 L2CAP */
    SDP_DATA_ELEMENT_SEQ_SIZE8(5),   /* Data element sequence for RFCOMM, 5 bytes */
    SDP_UUID_SIZE16(PROT_RFC),  /* Uuid16 rfcomm */
    SDP_UINT_SIZE8(03)
};

        
static const U8 HfpLangBaseVal[] = {
    SDP_DATA_ELEMENT_SEQ_SIZE8(9),   /* Data element sequence, 9 bytes */
    SDP_UINT_SIZE16(0x656E),      /* Uint16 "en" (English) */
    SDP_UINT_SIZE16(0x006A),      /* Uint16 UTF-8 encoding */
    SDP_UINT_SIZE16(0x0100)       /* Uint16 primary language base ID */
};

static const U8 HfpProfileDescListVal[] = {
    SDP_DATA_ELEMENT_SEQ_SIZE8(8),           /* Data element sequence, 8 bytes */
    SDP_DATA_ELEMENT_SEQ_SIZE8(6),           /* Data element sequence for ProfileDescriptor, 6 bytes */
    SDP_UUID_SIZE16(PROF_HFP),          /* Uuid16 Hid */
    SDP_UINT_SIZE16(0x0106)               /* Uint16 version number */
};

static const U8 Hfp_ServiceName[] = {
    SDP_TEXT_SIZE8(19),          /* Null terminated text string */
    'B', 'T', ' ', 'T', 'a', 'x', 'e', 'i', 'a', ' ', 'H','F', 
    'P', ' ', 'T', 'E', 'S', 'T' , '\0'
};


static const U8 HfpSupportedFeatures[] = {
    SDP_UINT_SIZE16(0x001C)     /* volcontrol, voice reg, CLL presentation*/
};


//sdp structure begin
SdpAttr hfp_attrs[] = {
    {ATTRID_SERVICE_RECORD_HANDLE, sizeof(HfpRecordHandle), HfpRecordHandle, 0},
        
    /* Service Class ID List attribute */
    {ATTRID_SERVICE_CLASS_ID_LIST, sizeof(HfpServClassIdVal), HfpServClassIdVal, 0},

//    {ATTRID_SERVICE_RECORD_STATE, sizeof(HfpRecordState), HfpRecordState, 0},

    /* Protocol Descriptor List attribute */
    {ATTRID_PROTOCOL_DESC_LIST, sizeof(HfpProtocolDescListVal), HfpProtocolDescListVal, 0},

    /* Language base Id List attribute */
//    {ATTRID_LANG_BASE_ID_LIST, sizeof(HfpLangBaseVal), HfpLangBaseVal, 0},

    /* Bluetooth Profile Descriptor List attribute */
    {ATTRID_BT_PROFILE_DESC_LIST, sizeof(HfpProfileDescListVal), HfpProfileDescListVal, 0},

    /* HID Service Name in English */
 //   {(ATTRID_SERVICE_NAME + 0x0100), sizeof(Hfp_ServiceName), Hfp_ServiceName, 0},

    { ATTRID_HFP_SUPPORTED_FEATURES, sizeof(HfpSupportedFeatures), HfpSupportedFeatures, 0}    
};
//sdp structure end

typedef enum
{
    HFP_IDLE,
    HFP_RFCOMM_CONNECTED,
    HFP_BRSF_SENT,
    HFP_BRSF_RECV,
    HFP_BRSF_OK_RECV,
    HFP_CINDTEST_SENT,
    HFP_CINDTEST_RECV,
    HFP_CINDTEST_OK_RECV,
    HFP_CIND_SENT,
    HFP_CIND_RECV,
    HFP_CIND_OK_RECV,
    HFP_CMER_SENT,
    HFP_CMER_OK_RECV,
    HFP_CONNECTED,
}HFP_STATE;

HFP_STATE HfsState = HFP_IDLE;

#define AT_REQ_HANDUP 1
#define AT_REQ_CSCS 2
#define AT_REQ_CPBS_ME 3
#define AT_REQ_CPBRTEST 4
#define AT_REQ_CPBR 5
#define AT_REQ_IPHONE_VENDOR_SET 6
#define AT_REQ_IPHONE_BATTERY_INFO 7
#define HFP_CONNECT 8
#define AT_REQ_VENDOR 9

U8 HfsRequest = 0;
U16 HfsRequestParm;
U16 HfsRequestParm1;

FinishNotifyCallback hfpcb = 0;
    
struct rfcomm_dlc *rfcomm_d = 0;

//signal, batt, service, callsetup, call, roam, callheld
U8 hfp_ind_idx[7] = {0, 0, 0, 0, 0, 0, 0};
U8 hfp_ind_val[7] = {0, 0, 0, 0, 0, 0, 0};

int pblow = 0;
int pbhigh = 0;
U8 *cbbuf = 0;
    
void HfpIndicationParse(U8 *buf)
{
    U16 len;
    U8 idx = 0;

    len = (U16)strlen((char const*)buf);

    while(len)
    {
        if(*buf == '(')
        {
            if(buf[1] == '"')
            {
                buf += 2;
                len -= 2;
                if(!strncmp("service", (char const*)buf, 7))
                {
                    hfp_ind_idx[idx++] = IND_SERVICE;
                    len-= 7;
                    buf+= 7;
                }
                if(!strncmp("callsetup", (char const*)buf, 9))
                {
                    hfp_ind_idx[idx++] = IND_CALLSETUP;
                    len-= 9;
                    buf+= 9;
                }
                if(!strncmp("battchg", (char const*)buf, 7))
                {
                    hfp_ind_idx[idx++] = IND_BATTARY;
                    len-= 7;
                    buf+= 7;
                }
                if(!strncmp("signal", (char const*)buf, 6))
                {
                    hfp_ind_idx[idx++] = IND_SIGNAL;
                    len-= 6;
                    buf+= 6;
                }
                if(!strncmp("roam", (char const*)buf, 4))
                {
                    hfp_ind_idx[idx++] = IND_ROAM;
                    len-= 4;
                    buf+= 4;
                }
                if(!strncmp("callheld", (char const*)buf, 8))
                {
                    hfp_ind_idx[idx++] = IND_CALLHELD;
                    len-= 8;
                    buf+= 8;
                }
                if(!strncmp("call", (char const*)buf, 4))
                {
                    hfp_ind_idx[idx++] = IND_CALL;
                    len-= 4;
                    buf+= 4;
                }
                
            }
            else
            {
                len -= 2;
                buf += 2;
            }
        }
        else
        {
            len--;        
            buf++;
        }
    }
}

U8 *HfpBuffer = 0;
U16 HfpBufferStart = 0;
U16 HfpBufferSize = 0;
U16 HfpBufferEnd = 0;

#define HFP_BUF_SIZE 512

static void HfpBufferAlloc(void)
{
    if(!HfpBufferSize)
    {
        HfpBuffer = (U8*)malloc(HFP_BUF_SIZE);
        HfpBufferSize = HFP_BUF_SIZE;
        HfpBufferStart = HfpBufferEnd = 0;

        Taxeia_Report("Hfp buffer = %x\r\n", HfpBuffer);
    }
}

static void HfpBufferFree(void)
{
    Taxeia_Report("Hfp buffer free %d %d\r\n", HfpBufferStart, HfpBufferEnd);
    free(HfpBuffer);
    HfpBufferSize = 0;
}

static void HfpBufferCopy(U8 *dat, U8 len)
{
   //check free size
    ASSERT(HFP_BUF_SIZE - HfpBufferEnd >= len);
   
    if(HFP_BUF_SIZE - HfpBufferEnd < len)
    {
        HfpBufferFree();
        return;
    }

    memcpy(&HfpBuffer[HfpBufferEnd], dat, len);
    HfpBufferEnd += len;
    HfpBuffer[HfpBufferEnd] = 0;//mark end
    Taxeia_Report("HfpBufferEnd become = %d\n", HfpBufferEnd);
}

static void HfpBufferGarbageCollect(void)
{
    ASSERT(HfpBufferStart <= HfpBufferEnd);
    
    if(HfpBufferStart == HfpBufferEnd && HfpBufferSize)
    {
        HfpBufferFree();
    }
}

U8 *HfpSendBuf = 0;

static U8* HfpSendBufAlloc(U8 size)
{
    if(!HfpSendBuf)
        HfpSendBuf = malloc(size);
    return HfpSendBuf;
}

void HfpSendBufFree()
{
    free(HfpSendBuf);
    HfpSendBuf = 0;
}

void HfpSendbufCollection()
{
    if(rfcomm_d)
    if(!rfcomm_d->tx_len && HfpSendBuf)
    {
        HfpSendBufFree();
    }
}


static U8* HfpBufferFindAT(void)
{
    char *start;
    char *end;

    HfpBufferGarbageCollect();
    if(!HfpBufferSize)
        return 0;
    
    start = strstr((char const*)&HfpBuffer[HfpBufferStart], "\r\n");

    if(!start) //error
    {
        Taxeia_Report("Hfp packet can't find start!!!\r\n");
        HfpBufferFree();
        return 0;
    }

    end = strstr(start+2, "\r\n");

        
    if(start && end)
    {
        HfpBufferStart += (end - start) + 2;
        *end = 0;
        Taxeia_Report("HfpBufferStart become %d\r\n", HfpBufferStart);

        return (U8*)start+2;
    }
    else
    {
        Taxeia_Report("Hfp packet can't find end!!!\r\n");
    }
        

    return 0;
}

void HfpRrcommDataRdy(struct rfcomm_dlc *d, U16 len, U8 *dat)
{
    U8 *at_str;
    
    HfpBufferAlloc();

    HfpBufferCopy(dat, len);
    
    while(at_str = HfpBufferFindAT())
    {        

        Taxeia_Report("HFP <= %s\r\n", at_str);

        switch(HfsState)
        {
            case HFP_BRSF_SENT:
                if(!strncmp((char const*)at_str, "+BRSF", 5))
                    HfsState = HFP_BRSF_RECV;
                break;
            case HFP_BRSF_RECV:
                if(!strncmp((char const*)at_str, "OK", 2))
                    HfsState = HFP_BRSF_OK_RECV;
                break;
            case HFP_CINDTEST_SENT:
            {
                if(!strncmp((char const*)at_str, "+CIND", 5))
                {
                    HfpIndicationParse(at_str);
                    HfsState = HFP_CINDTEST_RECV;
                }
            }
                break;
            case HFP_CINDTEST_RECV:
                if(!strncmp((char const*)at_str, "OK", 2))
                    HfsState = HFP_CINDTEST_OK_RECV;
                break;
            case HFP_CIND_SENT:
                if(!strncmp((char const*)at_str, "+CIND", 5))
                {
                    U8 i;
                    for(i = 0; i < 7; i++)
                    {
                        hfp_ind_val[i] = at_str[7+2*i] - '0';
                    }
                    HfsState = HFP_CIND_RECV;
                }
                break;
            case HFP_CIND_RECV:
                if(!strncmp((char const*)at_str, "OK", 2))
                    HfsState = HFP_CIND_OK_RECV;
                break;
            case HFP_CMER_SENT:
                if(!strncmp((char const*)at_str, "OK", 2))
                    HfsState = HFP_CMER_OK_RECV;
                break;
            case HFP_CONNECTED:
                if(!strncmp((char const*)at_str, "+CIEV", 5))
                {
                    hfp_ind_val[(at_str[7] - '0') - 1] = at_str[9] - '0';
                }
                else if(!strncmp((char const*)at_str, "RING", 4))
                {
                }
                else if(!strncmp((char const*)at_str, "+CLIP", 5))
                {
                    char *start, *end;
                    start = strchr((char const*)at_str, '"');

                    if(!start) return;
                    
                    end = strchr(start + 1, '"');

                    *end = 0;
                    
                    if(hfpcb)
                    {

                        cbbuf = (U8*)start+1;
                        hfpcb(HFPCB_PBNUMBER);
                    }
                }
                else if(!strncmp((char const*)at_str, "+CPBR: (", 8)) //range
                {
                    Taxeia_Report("bufstart = %d, bufend = %d\r\n", HfpBufferStart, HfpBufferEnd);

                    sscanf((char const*)at_str+7, "(%d-%d)", (int*)&pblow, (int*)&pbhigh);

                    Taxeia_Report("bufstart = %d, bufend = %d\r\n", HfpBufferStart, HfpBufferEnd);
                }
                else if(!strncmp((char const*)at_str, "+CPBR:", 5))
                {
                    if(hfpcb)
                    {
                        cbbuf = at_str + 7;
                        hfpcb(HFPCB_PBITEM);
                    }
                }
                break;
        }
    }

}

void HfpRfcommStsChg(struct rfcomm_dlc *d, int err)
{
    if(d->state == BT_CONNECTED) //just connect
    {
        HfsState = HFP_RFCOMM_CONNECTED;
        rfcomm_d = d;
    }
    else if(d->state == BT_CLOSED)
    {
        HfsState = HFP_IDLE;
        rfcomm_d = 0;
    }
}

U8 HfpRfcommSend(U8 *dat, U16 len)
{
    if(!rfcomm_d->tx_len)
    {
        Taxeia_Report("HFP => %s\n", dat);

        rfcomm_d->tx_dat = dat;
        rfcomm_d->tx_len = (U8)len;
        return 1;
    }
    Taxeia_Report("HFP => %s send fail!!\r\n", dat);
  
    return 0;
}

void Bt__ProfileHFPScheduler(void)
{
    switch(HfsState)
    {
        case HFP_IDLE:
            if(HfsRequest == HFP_CONNECT)
            {
                if(Bt_AclReady())
                {
                    Bt__RfCommConnect(HfsRequestParm1, (U8)HfsRequestParm, HfpRrcommDataRdy, HfpRfcommStsChg);
                    HfsRequest = 0;
                }
            }
            break;
        case HFP_RFCOMM_CONNECTED:
            if(HfpRfcommSend((U8*)"AT+BRSF=28\x0D" /*0x1C*/,11))
            {
                HfsState = HFP_BRSF_SENT;
            }
            break;
        case HFP_BRSF_OK_RECV:
            if(HfpRfcommSend((U8*)"AT+CIND=?\x0D", 10))
                HfsState = HFP_CINDTEST_SENT;
            break;
        case HFP_CINDTEST_OK_RECV:
            if(HfpRfcommSend((U8*)"AT+CIND?\x0D", 9))
                HfsState = HFP_CIND_SENT;
            break;
        case HFP_CIND_OK_RECV:
            if(HfpRfcommSend((U8*)"AT+CMER=3,0,0,1\x0D", 16))
                HfsState = HFP_CMER_SENT;
            break;
        case HFP_CMER_OK_RECV:
            if(HfpRfcommSend((U8*)"AT+CLIP=1\x0D", 10))
            {
                HfsState = HFP_CONNECTED;
                HfsRequest = AT_REQ_CSCS; //set to UTF-8, and get phonebook info related
            }
            break;
        case HFP_CONNECTED:
            if(HfsRequest == AT_REQ_HANDUP)
            {
                if(HfpRfcommSend((U8*)"AT+CHUP\x0D", 8))
                    HfsRequest = 0;
            }
            else if(HfsRequest == AT_REQ_CSCS)
            {
                if(HfpRfcommSend("AT+CSCS=\"UTF-8\"\x0D", 16))
                    HfsRequest = AT_REQ_CPBS_ME;
            }            
            else if(HfsRequest == AT_REQ_CPBS_ME)
            {
                if(HfpRfcommSend((U8*)"AT+CPBS=ME\x0D", 11))
                    HfsRequest = AT_REQ_CPBRTEST;
            }
            else if(HfsRequest == AT_REQ_CPBRTEST)
            {
                if(HfpRfcommSend("AT+CPBR=?\x0D", 10))
                    HfsRequest = 0;//AT_REQ_IPHONE_VENDOR_SET;
            }
            else if(HfsRequest == AT_REQ_IPHONE_VENDOR_SET)
            {
                if(HfpRfcommSend("AT+XAPL=3693-0300-0100,1\x0D", 25))
                    HfsRequest = AT_REQ_IPHONE_BATTERY_INFO;
            }
            else if(HfsRequest == AT_REQ_IPHONE_BATTERY_INFO)
            {
                if(HfpRfcommSend("AT+IPHONEACCEV=1,1,3\x0D", 21))
                    HfsRequest = 0;
            }
            else if(HfsRequest == AT_REQ_CPBR)
            {
                char *strbuf = HfpSendBufAlloc(20);
                sprintf(strbuf, "AT+CPBR=%d\x0D", HfsRequestParm); 
                if(HfpRfcommSend((U8*)strbuf, (U16)strlen(strbuf)))
                    HfsRequest = 0;
                else
                    HfpSendBufFree();
            }
            else if(HfsRequest == AT_REQ_VENDOR)
            {
                /*
                if(HfpRfcommSend((U8*)BarrowBuffer()+100, (U16)HfsRequestParm))
                    HfsRequest = 0;
                    */
            }
            break;
    }

    HfpSendbufCollection();
    HfpBufferGarbageCollect();
}

void Bt__ProfileHFPGetInquiryData(U8 *num, SdpAttr **attr, U32 *CoD)
{
    *num = sizeof(hfp_attrs) / sizeof(SdpAttr);
    *attr = hfp_attrs;
    *CoD = 0x200408;
}

void Bt__ProfileHFPInit()
{
    HfsState = HFP_IDLE;
    rfcomm_d = 0;
    Bt_RfCommChannelRegister(3, HfpRrcommDataRdy, HfpRfcommStsChg);
}

void Bt__ProfileHFPDeInit()
{
    HfsState = HFP_IDLE;
    rfcomm_d = 0;
    hfpcb = 0;
}

///////////////////////////////////Public API///////////////////////////////////////////
U8 *Bt__ProfileHFPGetCallBackBuf()
{
    return cbbuf;
}

U16 Bt__ProfileHFPGetPhoneBook(U16 idx)
{
    if(idx > pbhigh)
        return 0;
    HfsRequest = AT_REQ_CPBR;
    HfsRequestParm = idx;
    return pbhigh;
}

void Bt__ProfileHFPHandUp(void)
{
    HfsRequest = AT_REQ_HANDUP;
}

void Bt__ProfileSendVendor(char *str)
{
    /*
    U8 *buf;
    buf = (U8*)BarrowBuffer()+100;
    HfsRequest = AT_REQ_VENDOR;
    strcpy((char*)buf, str);
    HfsRequestParm = (U16)strlen((char const*)buf);
    buf[HfsRequestParm++] = 0x0D;
    */
}

U8 Bt__ProfileHFPStatus(U8 ind)
{
    U8 i;
    for(i = 0; i < 7 ;i++)
    {
        if(hfp_ind_idx[i] == ind)
            return hfp_ind_val[i];
    }
    return 0;
}

//0: not connect 1: connect 2: connecting
U8 Bt__ProfileHFPConnectStatus(void)
{
    switch(HfsState)
    {
        case HFP_IDLE:
            return 0;
        case HFP_CONNECTED:
            return 1;
        default:
            return 2;
    }
    return 2;
}

void Bt__HFPDisconnect(void)
{
    Bt__RfCommDisconnect(rfcomm_d);
}


void * sdpc_callback(U8 op, U16 aclHandle, U16 cid, U16 len, U8 *dat)
{
    if(op == 1)//connect
        SDPCSendSearviceSearchAttributeReq(aclHandle, cid, /*0x1101*//*spp*/0x111F/*agw*/, 0x0004);
    else if(op == 0) //disconnect
    {
        if(HfsRequestParm) //channel number exist
            HfsRequest = HFP_CONNECT;
        return 0;
    }
    else if(op == 2) //data
    {
        HfsRequestParm = dat[len - 1]; //channel number
        HfsRequestParm1 = aclHandle;
    }
        

    return (void*)sdpc_callback;
}

void Bt__HFPConnect(U16 aclHandle)
{
    Bt__ProfileSDPCConnect(aclHandle, sdpc_callback);
}


void Bt__ProfileHFPCB(FinishNotifyCallback fc)
{
    hfpcb = fc;
    Bt__RfCommCB(fc);
}

#endif


