#ifndef __BT_CONFIG_HH_
#define __BT_CONFIG_HH_

//ouput log
#ifdef NO_CONSOLE_LOG
#else
    #ifndef CONSOLE_LOG_LEVEL
        #define TAXEIA_DEBUG
        #define TAXEIA_RAW_DEBUG
        #define TAXEIA_L2_DEBUG
    #else
        #if (CONSOLE_LOG_LEVEL > 2)
            #define TAXEIA_DEBUG
            #define TAXEIA_L2_DEBUG
            #define TAXEIA_RAW_DEBUG
        #elif (CONSOLE_LOG_LEVEL > 1)
            #define TAXEIA_DEBUG
            #define TAXEIA_L2_DEBUG
        #elif (CONSOLE_LOG_LEVEL > 0)
            #define TAXEIA_DEBUG
        #else
        #endif
    #endif
#endif

#define PROFILE_TYPE_HID 0
#define PROFILE_TYPE_HFP 1
#define PROFILE_TYPE_DYNAMIC 2
#define PROFILE_TYPE_NOTHING 3
#define PROFILE_TYPE_PS3 4
#define PROFILE_TYPE_HID_HFP 5
#define PROFILE_TYPE_SPP 6
#define PROFILE_TYPE_TEST 0xff

//!!!!!!!!!!!!config!!!!!!!!!!!!!!!!!!!!!!!!!!!//

#define NEW_BT_LOOP
#define NEW_BT_LOOP2
//chip
    #define MTK_6622 0x18
    #define STLC2500C 0x19
    #define ISSC_77 0x20

#ifdef ISSC_BT
    #define BTCHIP ISSC_77
#else
    #define BTCHIP MTK_6622
#endif
//    #define MTK_6622_CHANGE_BAUD 1000000

//l2cap
    //L2CAP SDU maximun size, try to use the same IN/OUT MTU
    #ifndef LL_MTU
    #define LL_MTU 672
    #endif 
    
    #define ACL_MAX_PACKET_LEN (LL_MTU+9)
    #define ACL_MAX_PACKET_NUM 1
    #define PSM_SUPPORT_NUM 5

//pairing
    //#define OOB_ENABLE

//db size
#ifndef SM_SIZE
#define SM_SIZE (8192-32)
#endif

#ifndef PROFILE_TYPE
    #error "Need to define PROFILE_TYPE"    
#endif

//profile included
#if (PROFILE_TYPE == PROFILE_TYPE_HID)
    #define SDP_HID_DEVICE_SUBCLASS   ((U8)(0x00000040))
#elif (PROFILE_TYPE == PROFILE_TYPE_DYNAMIC || PROFILE_TYPE == PROFILE_TYPE_NOTHING || PROFILE_TYPE == PROFILE_TYPE_HID_HFP)
    #define SDP_HID_DEVICE_SUBCLASS   ((U8)(0x00000040))
#elif (PROFILE_TYPE == PROFILE_TYPE_SPP || PROFILE_TYPE == PROFILE_TYPE_HFP || PROFILE_TYPE == PROFILE_TYPE_SPP || PROFILE_TYPE == PROFILE_TYPE_TEST)
#else
    #error "Invalid PROFILE_TYPE"    
#endif


/*!!!!!!!!!!!!!!Customize HID sdp descirptor!!!!!!!!!!!!!!!!!*/
#ifdef UCOMM_HID_JOYSTICK

#define CUSTOMIZE_SDP_HID_DESCRIPTOR

#define VID 0x19C6
#define PID 0xffff
#define REV 0x0213
#define SDP_HID_DEVICE_SUBCLASS   ((U8)(0x00000040))
#define HID_SERVICENAME \
    'B', 'T', ' ', 'U', 'C', 'O', 'M', 'M', ' ', 'H', 'I','D', \
    ' ', 'G', 'A', 'M', 'E', ' ' , '\0'\

#define HID_PROVIDERNAME \
    'T', 'a', 'x', 'e', 'i', 'a', ' ', ' ', '\0' \


#ifdef DYNAMIC_HID_DESCRIPTOR
#else
#define SDP_HID_DESCRIPTOR_LEN (96+137+2)  
#define SDP_HID_DESCRIPTOR \
0x05, 0x01,           /* Usage Page (Generic Desktop)      */ \
0x09, 0x02,           /* Usage (Mouse)                     */ \
0xA1, 0x01,           /* Collection (Application)         **/ \
0x85, 0x02,           /* REPORT_ID (2)                     */ \
0x09, 0x01,           /* Usage (Pointer)                   */ \
0xA1, 0x00,           /* Collection (Physical)            **/ \
0x05, 0x09,           /* Usage Page (Button)               */ \
0x19, 0x01,           /* Usage Minimum (1)                 */ \
0x29, 0x08,           /* Usage Maximum (8)                 */ \
0x15, 0x00,           /* Logical Minimum (0)               */ \
0x25, 0x01,           /* Logical Maximum (1)               */ \
0x75, 0x01,           /* Report Size (1)                   */ \
0x95, 0x08,           /* Report Count (8)                  */ \
0x81, 0x02,           /* Input (Data, Variable, Absolute)  */ \
0x05, 0x01,           /* Usage Page (Generic Desktop)      */ \
0x09, 0x30,           /* Usage X                           */ \
0x09, 0x31,           /* Usage Y                           */ \
0x09, 0x38,           /* Usage Wheel                       */ \
0x15, 0x81,           /* Logical Minimum (-127)            */ \
0x25, 0x7F,           /* Logical Maximum (127)             */ \
0x75, 0x08,           /* Report Size (8)                   */ \
0x95, 0x03,           /* Report Count (3)                  */ \
0x81, 0x06,           /* Input (Data, Variable, Absolute)  */ \
0xC0, 0xC0,           /* End Collection                   **/ \
0x05, 0x01,           /* Usage Page (Generic Desktop)      */ \
0x09, 0x06,           /* Usage (Keyboard)                  */ \
0xA1, 0x01,           /* Collection (Application)          */ \
0x85, 0x01,           /* REPORT_ID (1)                     */ \
0x75, 0x01,           /* Report Size (1)                   */ \
0x95, 0x08,           /* Report Count (8)                  */ \
0x05, 0x07,           /* Usage Page (Key codes)            */ \
0x19, 0xE0,           /* Usage Minimum (224)               */ \
0x29, 0xE7,           /* Usage Maximum (231)               */ \
0x15, 0x00,           /* Logical Minimum (0)               */ \
0x25, 0x01,           /* Logical Maximum (1)               */ \
0x81, 0x02,           /* Input (Data, Variable, Absolute)  */ \
0x95, 0x01,           /* Report Count (1)                  */ \
0x75, 0x08,           /* Report Size (8)                   */ \
0x81, 0x03,           /* Input (xxxxxx)                    */ \
0x95, 0x06,           /* Report Count (6)                  */ \
0x75, 0x08,           /* Report Size (8)                   */ \
0x15, 0x00,           /* Logical Minimum (0)               */ \
0x26, 0xFF, 0x00,     /* Logical Maximum (255)             */ \
0x05, 0x07,           /* Usage Page (Key codes)            */ \
0x19, 0x00,           /* Usage Minimum (0)                 */ \
0x29, 0xFF,           /* Usage Maximum (255)               */ \
0x81, 0x00,           /* Input (Data, Array)               */ \
0xC0,                 /* END_COLLECTION                    */ \
/*game pad 137+2 bytes*/\
0x05, 0x01,           /*Usage Page(Generic Desktop)*/\
0x09, 0x05,           /*Usage(Game Pad)*/\
0xa1, 0x01,           /*Collection(application)*/\
0x85, 0x04,           /*Report ID(4)*/\
0x15, 0x00,           /*Logical Minimum(0)*/\
0x25, 0x01,           /*Logical Maximum(1)*/\
0x35, 0x00,           /*Physical Minimum(0)*/\
0x45, 0x01,           /*Physical Maximum(1)*/\
0x75, 0x01,           /*Report Size(1)*/\
0x95, 0x0d,           /*Report Count(13)*/\
0x05, 0x09,           /*Usage Page(Button)*/\
0x19, 0x01,           /*Usage Minimum(1)*/\
0x29, 0x0d,           /*Usage Maximum(13)*/\
0x81, 0x02,           /*Input(Absolute, Variable, Data)*/\
0x95, 0x03,           /*Report Count(3)*/\
0x81, 0x01,           /*Input(Absolute, Array, Const)*/\
0x05, 0x01,           /*Usage Page(Generic Desktop)*/\
0x25, 0x07,           /*Logical Maximum(7)*/\
0x46, 0x3b, 0x01,     /*Physical Maximum(315)*/\
0x75, 0x04,           /*Report Size(4)*/\
0x95, 0x01,           /*Report Count(1)*/\
0x65, 0x14,           /*Unit(degrees)*/\
0x09, 0x39,           /*Usage(Hat switch)*/\
0x81, 0x42,           /*Input(Null, Absolute, Variable, Data)*/\
0x65, 0x00,           /*Unit(0)*/\
0x95, 0x01,           /*Report Count(1)*/\
0x81, 0x01,           /*Input(Absolute, Array, Const)*/\
0x26, 0xff, 0x00,     /*Logical Maximun(255)*/\
0x46, 0xff, 0x00,     /*Physical Maximum(255)*/\
0x09, 0x30,           /*Usage(X)*/\
0x09, 0x31,           /*Usage(Y)*/\
0x09, 0x32,           /*Usage(Z)*/\
0x09, 0x35,           /*Usage(Rz)*/\
0x75, 0x08,           /*Report Size(8)*/\
0x95, 0x04,           /*Report Count(4)*/\
0x81, 0x02,           /*Input(Absolute, Variable, Data)*/\
0x06, 0x00, 0xff,     /*Usage(vendor define)*/\
0x09, 0x20,           /*Usage(0x20)*/\
0x09, 0x21,           /*Usage(0x21)*/\
0x09, 0x22,           /*Usage(0x22)*/\
0x09, 0x23,           /*Usage(0x23)*/\
0x09, 0x24,           /*Usage(0x24)*/\
0x09, 0x25,           /*Usage(0x25)*/\
0x09, 0x26,           /*Usage(0x26)*/\
0x09, 0x27,           /*Usage(0x27)*/\
0x09, 0x28,           /*Usage(0x28)*/\
0x09, 0x29,           /*Usage(0x29)*/\
0x09, 0x2a,           /*Usage(0x2a)*/\
0x09, 0x2b,           /*Usage(0x2b)*/\
0x95, 0x0c,           /*Report Count(12)*/\
0x81, 0x02,           /*Input(Absolute, Variable, Data)*/\
0x0a, 0x21, 0x26,     /*Usage(0x2126)*/\
0x95, 0x08,           /*Report Count(8)*/\
0xb1, 0x02,           /*Feature(Absolute, Variable, Data)*/\
0x0a, 0x21, 0x26,     /*Usage(0x2126)*/\
0x91, 0x02,           /*Output(Absolute, Variable, Data)*/\
0x26, 0xff, 0x03,     /*Logical Maximun(0x3ff)*/\
0x46, 0xff, 0x03,     /*Physical Maximun(0x3ff)*/\
0x09, 0x2c,           /*Usage(0x2c)*/\
0x09, 0x2d,           /*Usage(0x2d)*/\
0x09, 0x2e,           /*Usage(0x2e)*/\
0x09, 0x2f,           /*Usage(0x2f)*/\
0x75, 0x10,           /*Report Size(16)*/\
0x95, 0x04,           /*Report Count(4)*/\
0x81, 0x02,           /*Input(Absolute, Variable, Data)*/\
0xc0                  /*End Collections*/\

#endif //not DYNAMIC

#endif //UCOMM


#endif
