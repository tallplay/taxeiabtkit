#include "BtConfig.h"
#include "TaxeiaHCILogrc.h"
#include "TaxeiaHciLog.h"
#include "TaxeiaHciLog_Modeling.h"
#include "../GAP/HCI/BtCmdEvt.h"
#include <string.h>
#include "../TaxeiaConsoleDebug/TaxeiaConsoledebug.h"
#include <stdio.h>


#ifdef TAXEIA_UI_LOG
//internal variables
static BYTE Isshown = 0;
static HWND hwnd = 0;
static HINSTANCE hinstance = 0;

static U8 showHCI = 1;
static U8 showLL2CAP = 1;
static U8 showDATA = 1;
static U8 autoFlush = 0;
static U16 showRFComm = 0;
static U16 showRFComm2 = 0;
typedef struct 
{
    U8 type;
    U8 *ptr;
    U16 len;
}ST_ADD_LOG ;
ST_ADD_LOG st_addlog;

static U8 shit = 0;

static _w64 int CALLBACK MsgProcedure(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
#define LINE_WIDTH 16

HWND StartHciLog(HINSTANCE Instance, BYTE show)
{

    hinstance = Instance;
    if (hwnd == NULL) {
        hwnd = CreateDialogA(Instance, MAKEINTRESOURCEA(HCILOG_MAIN), NULL, MsgProcedure);
    } else {
        SendDlgItemMessage(hwnd, HCILOG_LISTBOX, LB_RESETCONTENT, 0, 0);
    }

    if (hwnd == NULL) {
        return 0;
    }

    SetLineDisplayOption(showHCI, showLL2CAP, showDATA, autoFlush, showRFComm, showRFComm2);
    OpenLog();
    
    ShowWindow(hwnd, show);
    Isshown = 1;
    return hwnd;
}

void StopHciLog(void)
{
    if (hwnd != NULL)
        ShowWindow(hwnd, SW_HIDE);

    CloseLog();
    Isshown = 0;
}

HFONT CreateBoldFont(HFONT hFont) {
    LOGFONT lf;
    GetObject ( hFont, sizeof(LOGFONT), &lf );
    lf.lfWeight = FW_BOLD;
    return CreateFontIndirect(&lf);
}

HFONT CreateDefaultFont(HFONT hFont) {
    LOGFONT lf;
    GetObject ( hFont, sizeof(LOGFONT), &lf );
    lf.lfWeight = FW_DONTCARE;
    return CreateFontIndirect(&lf);
}

HFONT           defaultFont;

typedef struct 
{
    U16 cid1;
    U16 cid2;
    U8 *header;
}CidRequest;

static BOOL CALLBACK EnterCidProc(HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
{

    char                tmp[50];
    static HWND         saveDlg;
    U8 len;
    
    static CidRequest *cidr = 0;

    switch (uMsg) {
    
    case WM_INITDIALOG:
        cidr = (CidRequest*)lParam;

        sprintf(tmp, "%s", (char*)cidr->header);
        SendMessageA(hDlg, WM_SETTEXT, 0, (LPARAM)tmp);
        SendMessageA(hDlg, EM_SETSEL, 0, (LPARAM)-1);
        SendMessageA(hDlg, EM_SETLIMITTEXT, 16/*MAX_PIN_LEN*/, 0L);
        saveDlg = hDlg;
        break;

    case WM_COMMAND:
        switch (LOWORD(wParam)) {
        case IDCANCEL:
            EndDialog(hDlg, FALSE);
            return TRUE;

        case IDOK:
            cidr->cid1 = 0;
            cidr->cid2 = 0;
            /* Retrieve the Pin Code */
            len = (U8)SendDlgItemMessageA(hDlg, IDC_CID, EM_GETLINE,
                                                      0, (LPARAM)tmp);
            if (len == 0) {
                /* The user didn't enter a pin. */
                EndDialog(hDlg, FALSE);
                return TRUE;
            }

            /* Trusted */
            len = sscanf(tmp, "%04x%04x", &cidr->cid1, &cidr->cid2);
            if(!cidr->cid2) cidr->cid2 = cidr->cid1;
            EndDialog(hDlg, TRUE);
            return TRUE;

        }
        break;
    }
    return FALSE;
}
int CALLBACK MsgProcedure(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    static HWND     listWnd;
    static LONG     wOffset, hOffset, tmHeight;
    LONG            width, height;
    RECT            w_rect, l_rect;
    BOOL            checked;
    LOGFONT         lf;

    switch (uMsg) {

        case WM_SETFONT:
        {
            HDC hdc = GetDC(GetDlgItem(hWnd, HCILOG_LISTBOX));
            TEXTMETRIC tm;
            HGDIOBJ oldObj;

            /* Retrieve the font height for later. */
            defaultFont = SelectObject(hdc, (void *)wParam);
            GetTextMetrics(hdc, &tm);
            SelectObject(hdc, (void *)defaultFont);
            ReleaseDC(hWnd, hdc);

            tmHeight = tm.tmHeight;
        }
        break;

        case WM_SHOWWINDOW:
            if (wParam == FALSE)
                break;

        case WM_SETFOCUS:
            listWnd = GetDlgItem(hWnd, HCILOG_LISTBOX);
            GetWindowRect(hWnd, &w_rect);
            GetWindowRect(listWnd, &l_rect);
            wOffset = (w_rect.right - l_rect.right);
            hOffset = (w_rect.bottom - l_rect.bottom);

            /* Initialize display options in menu */
#if 1
            if (showHCI)
                CheckMenuItem(GetMenu(hWnd), ID_TRACEOPTION_SHOWCMDEVT, MF_BYCOMMAND|MF_CHECKED);
            if (showLL2CAP)
                CheckMenuItem(GetMenu(hWnd), ID_TRACEOPTION_SHOWLLCAP, MF_BYCOMMAND|MF_CHECKED);
            if (showDATA)
                CheckMenuItem(GetMenu(hWnd), ID_TRACEOPTION_SHOWRAW, MF_BYCOMMAND|MF_CHECKED);
            if (autoFlush)
                CheckMenuItem(GetMenu(hWnd), ID_TARCEOPTION_AUTOFLUSH, MF_BYCOMMAND|MF_CHECKED);
            if (showRFComm)
                CheckMenuItem(GetMenu(hWnd), ID_TRACEOPTION_RFCOMM, MF_BYCOMMAND|MF_CHECKED);
                
            SetLineDisplayOption(showHCI, showLL2CAP, showDATA, autoFlush, showRFComm, showRFComm2);    

#endif
            break;

        case WM_SIZING:
        case WM_EXITSIZEMOVE:       /* Msg for Win95 */
            GetClientRect(hWnd, &w_rect);
            width = (w_rect.right - w_rect.left - wOffset);
            height = (w_rect.bottom - w_rect.top - hOffset);
            MoveWindow(listWnd, 5, 6, width, height, TRUE);     /* Origin (x,y) = 5,6 */
            break;

        case WM_MEASUREITEM:
        {
            LPMEASUREITEMSTRUCT lpmis = (LPMEASUREITEMSTRUCT)lParam;

            lpmis->itemHeight = tmHeight;
        }
        return FALSE;

        case WM_DRAWITEM:
        {
            LPDRAWITEMSTRUCT    lpdis = (LPDRAWITEMSTRUCT)lParam;
            int                 y;
            DWORD               bkColor, textColor, read;

            DWORD               hash = 0, color = 0x808080;
            U8     output[240];
            U32 len;
            /* If there are no list box items, skip this message. */
            if (lpdis->itemID == -1) {
                return TRUE;
            }

            /* Draw the text for the list box item. */
            if (lpdis->itemAction & (ODA_DRAWENTIRE|ODA_SELECT)) {
                GetLineDisplayData(lpdis->itemID, output, &len, &color);

                y = (lpdis->rcItem.bottom + lpdis->rcItem.top - tmHeight) / 2;

                if (lpdis->itemState & ODS_SELECTED) {
                    /* Set the selected background and text colors. */
                    bkColor = SetBkColor(lpdis->hDC, GetSysColor(COLOR_HIGHLIGHT));
                    textColor = SetTextColor(lpdis->hDC, GetSysColor(COLOR_HIGHLIGHTTEXT));

                } else {
                    SetTextColor(lpdis->hDC, color);
                }
/*
                defaultFont = SelectObject(lpdis->hDC, defaultFont);

                if(color != 0x808080)
                {
                    defaultFont = CreateBoldFont(defaultFont);
                    defaultFont = SelectObject(lpdis->hDC, defaultFont);
                }
                else
                {
                    defaultFont = CreateDefaultFont(defaultFont);
                    defaultFont = SelectObject(lpdis->hDC, defaultFont);
                }*/

                ExtTextOutA(lpdis->hDC, 0, y, (ETO_CLIPPED|ETO_OPAQUE), &lpdis->rcItem, (char*)output, len, 0);

                if (lpdis->itemState & ODS_SELECTED) {
                    /* Restore normal background color. */
                    bkColor = SetBkColor(lpdis->hDC, bkColor);
                }
            }
            return TRUE;
        }
        break;

        case WM_INITDIALOG:
            break;

        case WM_COMMAND:
            switch (LOWORD(wParam)) {
                case ID_ADD_LOG:
                {
                    U8 more_line;
                    DWORD nItem;

                    shit = 1;
                    more_line = AppendLogEntry(st_addlog.type, st_addlog.ptr, st_addlog.len);

                    
                    if (hwnd)
                    {
                        while(more_line--)
                        {
                            nItem = (DWORD)SendMessageA(GetDlgItem(hwnd, HCILOG_LISTBOX), LB_ADDSTRING, 0, (LPARAM)0);
                            SendMessageA(GetDlgItem(hwnd, HCILOG_LISTBOX), LB_SETCARETINDEX, nItem, 0);
                        }
                    }        
                    shit = 0;

                    if(IsLineFull() && autoFlush)
                    {
                        SendMessage(hwnd, WM_COMMAND, ID_FILE_CLEAROUTPUT, 0);
                    }

               }
                    break;
                case ID_FILE_SAVETEXT_AS:
                {

                    U32 i;
                    OPENFILENAMEA   saveFile;
                    char           savefile[_MAX_PATH];
                    U32 written;

                    SYSTEMTIME CurrTime;
                    HANDLE fptr;
                    GetLocalTime(&CurrTime);


                    sprintf(savefile, "HciLog-%04d%02d%02d-%02d%02d.txt", CurrTime.wYear, CurrTime.wMonth, CurrTime.wDay,
                        CurrTime.wHour, CurrTime.wMinute);

                    /* Post dialog to get filename to save results in */
                    memset( &saveFile, 0, sizeof(saveFile));
                    saveFile.lStructSize = sizeof(saveFile);
                    saveFile.hwndOwner = hWnd;
                    saveFile.lpstrFile = savefile;
                    saveFile.lpstrFilter = "(*.txt)\0*.TXT\0All Files(*.*)\0*.*\0";
                    saveFile.nMaxFile = _MAX_PATH;
                    saveFile.Flags = OFN_OVERWRITEPROMPT;
                    saveFile.lpstrTitle = "Save Results";

                    if (GetSaveFileNameA(&saveFile) == 0)
                        break;

                    /* Concatenates ".txt" to the file name if it has no extension */
                    if (strchr(savefile, '.') == 0)
                        strcat(savefile, ".txt");

                    fptr = CreateFileA(savefile, GENERIC_READ|GENERIC_WRITE,
                                  FILE_SHARE_READ, 0, CREATE_ALWAYS,
                                  FILE_ATTRIBUTE_NORMAL|FILE_FLAG_RANDOM_ACCESS, 0);

                    for(i = 0; i < GetLineNumber(); i++)
                    {
                        DWORD color;
                        U32 len;
                        U8 output[512];
                        GetLineDisplayData(i, output, &len, &color);
                        output[len++] = '\r';
                        output[len++] = '\n';
                        WriteFile(fptr, output, len, &written, 0);
                        if(written != len)
                            break;

                    }
                    CloseHandle(fptr);
                }
                break;
                case ID_FILE_SAVERAW_AS:
                {

                    U32 i;
                    OPENFILENAMEA   saveFile;
                    char           savefile[_MAX_PATH];
                    U32 written;

                    SYSTEMTIME CurrTime;
                    HANDLE fptr;
                    GetLocalTime(&CurrTime);


                    sprintf(savefile, "HciLog-%04d%02d%02d-%02d%02d.raw", CurrTime.wYear, CurrTime.wMonth, CurrTime.wDay,
                        CurrTime.wHour, CurrTime.wMinute);

                    /* Post dialog to get filename to save results in */
                    memset( &saveFile, 0, sizeof(saveFile));
                    saveFile.lStructSize = sizeof(saveFile);
                    saveFile.hwndOwner = hWnd;
                    saveFile.lpstrFile = savefile;
                    saveFile.lpstrFilter = "(*.raw)\0*.RAW\0All Files(*.*)\0*.*\0";
                    saveFile.nMaxFile = _MAX_PATH;
                    saveFile.Flags = OFN_OVERWRITEPROMPT;
                    saveFile.lpstrTitle = "Save Results";

                    if (GetSaveFileNameA(&saveFile) == 0)
                        break;

                    /* Concatenates ".txt" to the file name if it has no extension */
                    if (strchr(savefile, '.') == 0)
                        strcat(savefile, ".raw");

                    if (CopyLog(savefile) == 0)
                        MessageBoxA(hWnd, "File copy failed!", "Error", (MB_OK|MB_ICONEXCLAMATION));
                }
                break;

                case ID_FILE_OPEN_RAW:
                {

                    U32 i;
                    OPENFILENAMEA   saveFile;
                    char           savefile[_MAX_PATH];
                    U32 written;
                    DWORD moreline, nItem;

                    HANDLE fptr;
                    sprintf(savefile, "");
                    /* Post dialog to get filename to save results in */
                    memset( &saveFile, 0, sizeof(saveFile));
                    saveFile.lStructSize = sizeof(saveFile);
                    saveFile.hwndOwner = hWnd;
                    saveFile.lpstrFile = savefile;
                    saveFile.lpstrFilter = "(*.raw)\0*.RAW\0All Files(*.*)\0*.*\0";
                    saveFile.nMaxFile = _MAX_PATH;
                    saveFile.Flags = 0;
                    saveFile.lpstrTitle = "Open Raw";

                    if (GetSaveFileNameA(&saveFile) == 0)
                        break;

                    /* Concatenates ".txt" to the file name if it has no extension */
                    if (strchr(savefile, '.') == 0)
                        strcat(savefile, ".raw");

                    moreline = 0;
                    if ((i = ImportLog(savefile, &moreline)) != 0)
                    {
                        if(i == 1)
                            MessageBoxA(hWnd, "Invalid File", "Error", (MB_OK|MB_ICONEXCLAMATION));
                        else if(i == 2)
                            MessageBoxA(hWnd, "Wrong tag", "Error", (MB_OK|MB_ICONEXCLAMATION));
                        else if(i == 3)
                            MessageBoxA(hWnd, "Wrong size", "Error", (MB_OK|MB_ICONEXCLAMATION));
                    }
                    else
                    {
                        SendDlgItemMessage(hwnd, HCILOG_LISTBOX, LB_RESETCONTENT, 0, 0);                    
                        while(moreline--)
                        {
                            nItem = SendMessageA(GetDlgItem(hwnd, HCILOG_LISTBOX), LB_ADDSTRING, 0, (LPARAM)0);
                        }     
                        SendMessageA(GetDlgItem(hwnd, HCILOG_LISTBOX), LB_SETCARETINDEX, nItem, 0);
                    }
                }                    
                    break;
                case ID_FILE_CLEAROUTPUT:
                    SendDlgItemMessage(hwnd, HCILOG_LISTBOX, LB_RESETCONTENT, 0, 0);
                    CloseLog();
                    OpenLog();
                    break;
                case ID_TRACEOPTION_RFCOMM:
                case ID_TRACEOPTION_SHOWCMDEVT:
                case ID_TRACEOPTION_SHOWLLCAP:
                case ID_TRACEOPTION_SHOWRAW:
                case ID_TARCEOPTION_AUTOFLUSH:
                {
                    DWORD moreline, nItem;
                    CidRequest CidR;
                    
                    if (GetMenuState(GetMenu(hWnd), LOWORD(wParam), MF_BYCOMMAND) & MFS_CHECKED) {
                        // Toggle to unchecked
                        CheckMenuItem(GetMenu(hWnd), LOWORD(wParam), MF_BYCOMMAND|MF_UNCHECKED);
                        checked = FALSE;
                    } else {
                        // Toggle to checked
                        CheckMenuItem(GetMenu(hWnd), LOWORD(wParam), MF_BYCOMMAND|MF_CHECKED);
                        checked = TRUE;
                    }
                    
                    showHCI = (GetMenuState(GetMenu(hWnd), ID_TRACEOPTION_SHOWCMDEVT, MF_BYCOMMAND) & MFS_CHECKED);
                    showLL2CAP = (GetMenuState(GetMenu(hWnd), ID_TRACEOPTION_SHOWLLCAP, MF_BYCOMMAND) & MFS_CHECKED);
                    showDATA = (GetMenuState(GetMenu(hWnd), ID_TRACEOPTION_SHOWRAW, MF_BYCOMMAND) & MFS_CHECKED);
                    autoFlush = (GetMenuState(GetMenu(hWnd), ID_TARCEOPTION_AUTOFLUSH, MF_BYCOMMAND) & MFS_CHECKED);     
                    showRFComm = (GetMenuState(GetMenu(hWnd), ID_TRACEOPTION_RFCOMM, MF_BYCOMMAND) & MFS_CHECKED);

                    if(showRFComm)
                    {
                        CidR.header = "Input Rfcomm CID";
                        DialogBoxParamA(hinstance, MAKEINTRESOURCE(CID_INPUTDLG), IsWindowVisible(hWnd) ? hWnd : GetParent(hWnd),
                            EnterCidProc, (LPARAM)&CidR);
                        showRFComm = CidR.cid1;
                        showRFComm2 = CidR.cid2;
                    }
                    
                    SetLineDisplayOption(showHCI, showLL2CAP, showDATA, autoFlush, showRFComm, showRFComm2);

                    ReParseLog(-1, &moreline);
                        
//                    moreline = SendMessage(GetDlgItem(hwnd, HCILOG_LISTBOX), LB_GETCOUNT, 0, 0);
                    SendDlgItemMessage(hwnd, HCILOG_LISTBOX, LB_RESETCONTENT, 0, 0);                    
                    while(moreline--)
                    {
                        nItem = SendMessage(GetDlgItem(hwnd, HCILOG_LISTBOX), LB_ADDSTRING, 0, (LPARAM)0);
                    }     
                    SendMessage(GetDlgItem(hwnd, HCILOG_LISTBOX), LB_SETCARETINDEX, nItem, 0);
                    
                }
                    break;
            }
    }
    return 0L;
}

void TaxeiaHCILog(U8 type, U8 *ptr, U16 len)
{
    if(hwnd && Isshown)
    {
        ASSERT(shit == 0);
        st_addlog.type = type;
        st_addlog.ptr = ptr;
        st_addlog.len = len;
        
        SendMessage(hwnd, WM_COMMAND, ID_ADD_LOG, 0);
    }
}
#endif
