#ifndef __TAXEIAHCILOG__H__
#define __TAXEIAHCILOG__H__
#include "bttype.h"
#include "btconfig.h"
#ifdef TAXEIA_UI_LOG

#include "windows.h"

#define ID_SNIFFER_SAVEAS              2118+0
#define ID_ADD_LOG                     2118+2

#define LINE_LENGTH 80

HWND StartHciLog(HINSTANCE Instance, BYTE show);
void StopHciLog(void);
void TaxeiaHCILog(U8 type/*tx or rx*/, U8 *ptr, U16 len);

#else
#define StartHciLog(x, y)
#define StopHciLog()
#define TaxeiaHCILog(x, y, z)
#endif

#endif
