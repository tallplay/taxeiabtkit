//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by TaxeiaHCILog.rc
//
#define IDD_DIALOG1                     101
#define HCILOG_MAIN                     101
#define HCILOG_MENU                     102
#define CID_INPUTDLG                    103
#define IDC_LIST1                       1001
#define HCILOG_LISTBOX                  1001
#define IDC_HOTKEY1                     1003
#define IDC_CID                         1004
#define IDC_BUTTON1                     1005
#define ID_FILE_SAVETEXT_AS             40001
#define ID_FILE_CLEAROUTPUT             40002
#define ID_TRACEOPTION_SHOWCMDEVT       40003
#define ID_TRACEOPTION_SHOWLLCAP        40004
#define ID_TRACEOPTION_SHOWRAW          40005
#define ID_FILE_SAVERAW_AS              40006
#define ID_FILE_OPEN_RAW                40007
#define ID_TARCEOPTION_AUTOFLUSH        40008
#define ID_TRACEOPTION_RFCOMM           40009

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        104
#define _APS_NEXT_COMMAND_VALUE         40006
#define _APS_NEXT_CONTROL_VALUE         1006
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
