#include "btconfig.h"
#include "bttype.h"
#include "TaxeiaHCILog_Modeling.h"
#include <windows.h>
#include "..\GAP\HCI\BtCmdEvt.h"
#include <stdio.h>
#include "../TaxeiaConsoleDebug/TaxeiaConsoledebug.h"


#ifdef TAXEIA_UI_LOG

static HANDLE content_file = INVALID_HANDLE_VALUE;
static char content_filename[_MAX_PATH];
static DWORD content_file_parse_offset = 0;

typedef enum
{
    RAW_DATA,
    HCI_CMD_EVT,
    RXDAT,
    TXDAT,
    RXLL,
    TXLL,
    SPACE,
    
} DISPLAY_TYPE;

static U16 line_number = 0;
static BYTE line_display_type[1024]; //0: raw data 1: hci cmd or evt 2: rx data 3: tx dat 4: rx l2cap 5: tx l2cap
static WORD line_length[1024];
static DWORD line_offset[1024];
static WORD line_offset_offset[1024];
static DWORD line_time[1024];
static DWORD start_local = 0;

static U8 showHCI = 1;
static U8 showLLCAP = 1;
static U8 showRAW = 1;
static U8 autoFlush = 0;
static U16 showRFComm1 = 0;
static U16 showRFComm2 = 0;
    
U16 txcnt = 0;
U16 txlength = 0;
U8 txlog_state = 0; //0: type 1:header 2: data
U8 txlog[1024];
DWORD tx_memo_offset;
U16 tx_memo_offset_offset;

U16 rxcnt = 0;
U16 rxlength = 0;
U8 rxlog_state = 0; //0: type 1:header 2: data
U8 rxlog[1024];
DWORD rx_memo_offset;
U16 rx_memo_offset_offset;

static BYTE HciCommandParse(char *buf, U16 cmd, U16 idx, U8 dat);
static BYTE HciEventParse(char * buf, U8 Evt, U16 idx, U8 dat);
static BYTE HciDataParse(char * buf, U16 idx, U8 dat);
static BYTE HciL2Parse(char * buf, U16 cid, U16 idx, U8 dat);

//ctl
#define RFCOMM_SABM 0x2f
#define RFCOMM_DISC 0x43
#define RFCOMM_UA   0x63
#define RFCOMM_DM   0x0f
#define RFCOMM_UIH  0xef

//type
#define RFCOMM_TEST 0x08
#define RFCOMM_FCON 0x28
#define RFCOMM_FCOFF 0x18
#define RFCOMM_MSC 0x38
#define RFCOMM_RPN 0x24
#define RFCOMM_RLS 0x14
#define RFCOMM_PN 0x20
#define RFCOMM_NSC 0x04

static const char *RFControlStr(U8 ctl)
{
    switch(ctl & 0xEF)
    {
        case RFCOMM_SABM:
            return "SABM";
        case RFCOMM_DISC:
            return "DISC";
        case RFCOMM_UA:
            return "UA";
        case RFCOMM_DM:
            return "DM";
        case RFCOMM_UIH:
            return "UIH";
        default:
            return "UNKNOWN";
    }
}

static const char *RFDLCI0TypeStr(U8 type)
{
    switch(type)
    {
        case RFCOMM_TEST:
            return "test";
        case RFCOMM_FCON:
            return "fcon";
        case RFCOMM_FCOFF:
            return "fcoff";
        case RFCOMM_MSC:
            return "msc";
        case RFCOMM_RPN:
            return "rpn";
        case RFCOMM_RLS:
            return "rls";
        case RFCOMM_PN:
            return "pn";
        case RFCOMM_NSC:
            return "nsc";
        default:
            return "unknown";
    }
}

static const char *L2CAPConnStatusStr(U16 Status)
{
    switch (Status) {
        case L2CONN_ACCEPTED:
            return "Accepted";

        case L2CONN_PENDING:
            return "Pending";

        case L2CONN_REJECT_PSM_NOT_SUPPORTED:
            return "Rejected Psm Not Supported";

        case L2CONN_REJECT_SECURITY_BLOCK:
            return "Rejected Security Block";

        case L2CONN_REJECT_NO_RESOURCES:
            return "Rejected No Resources";
    }
    return "UNKNOWN";
}

static const char *HciStatusStr(U8 Status)
{
    const char *hcStatus[] = {
        "Success",
        "Unknown HCI Cmd",
        "No Connection",
        "Hardware Failure",
        "Page Timeout",
        "Auth Failure",
        "Key Missing",
        "Memory Full",
        "Conn Timeout",
        "Max Num Conns",
        "Max SCO Conns",
        "Max ACL Conns",
        "Cmd Disallowed",
        "Host Rej No Resources",
        "Host Rej Security",
        "Host Rej Personal Dev",
        "Host Timeout",
        "Unsupp Feature/Parm Val",
        "Invalid HCI Parm Val",
        "Conn Term User Req",
        "Conn Term Low Resources",
        "Conn Term Power Off",
        "Conn Term Local Host",
        "Repeated Attempts",
        "Pairing Disallowed",
        "Unknown LMP PDU",
        "Unsupp Remote Feature",
        "SCO Offset Rejected",
        "SCO Interval Rejected",
        "SCO Air Mode Rejected",
        "Invalid LMP Parm",
        "Unspecified Error",
        "Unsupp LMP Parm",
        "Role Change Disallowed",
        "Lmp Response Timedout",
        "Lmp Err Transaction Coll",
        "Lmp PDU Disallowed",
        "Encryption Mode Unacceptable",
        "Unit Key Used",
        "QoS Not Supported",
        "Instant Passed",
        "Pairing w/Unit Key Unsupp", //0x29
        "Different Transaction Collision", //0x2A
        "QoS Unacceptable Parameter", //0X2C
        "QoS Rejected", //0X2D
        "Channel Assessment  Not Supported", //0X2E
        "Insufficient Security", //0X2F
        "Parameter out of Mandatory Range", //0X30
        "unknown error", //0X31
        "Role Switch Pending", //0X32
        "unknown error", //0X33
        "Reserved Slot Violation", //0X34
        "Role Switch Failed", //0X35
        "Extended Inquiry Response Too Large", //0x36
        "Simple Pairing Not Supported By Host", //0X37
        "Host Busy�VPairing", //0X38
        "Connection Rejected Due To No Suitable Channel Found", //0X39
        "Controller Busy", //0X3A
        "Unacceptable Connection Interval", //0X3B
        "Directed Advertising Timeout", //0X3C
        "Connection Terminated Due To MIC Failure", //0X3D
        "Connection Failed To Be Established", //0X3E
        "MAC Connection Failed", //0x3F
    };
#if 0
    if (Status > HC_STATUS_PAIRING_W_UNIT_KEY_UNSUPP) {
        /* Handle out of sequence BtErrorCode values */
        switch (Status) {
            case BEC_STORE_LINK_KEY_ERR:
                return "Store Link Key Error";
            case BEC_REQUEST_CANCELLED:
                return "Request cancelled";
            case BEC_NOT_FOUND:
                return "Not Found";
            default:
                return "UNKNOWN";
        }
    }
#endif

    return hcStatus[Status];
}

static const char *HciEventStr(U8 Event)
{
    switch (Event) {
        case HCE_INQUIRY_COMPLETE:
            return "INQUIRY_COMPLETE";
        case HCE_INQUIRY_RESULT:
            return "INQUIRY_RESULT";
        case HCE_CONNECT_COMPLETE:
            return "CONNECT_COMPLETE";
        case HCE_CONNECT_REQUEST:
            return "CONNECT_REQUEST";
        case HCE_DISCONNECT_COMPLETE:
            return "DISCONNECT_COMPLETE";
        case HCE_AUTH_COMPLETE:
            return "AUTH_COMPLETE";
        case HCE_REMOTE_NAME_REQ_COMPLETE:
            return "REMOTE_NAME_REQ_COMPLETE";
        case HCE_ENCRYPT_CHNG:
            return "ENCRYPT_CHNG";
        case HCE_CHNG_CONN_LINK_KEY_COMPLETE:
            return "CHNG_CONN_LINK_KEY_COMPLETE";
        case HCE_MASTER_LINK_KEY_COMPLETE:
            return "MASTER_LINK_KEY_COMPLETE";
        case HCE_READ_REMOTE_FEATURES_COMPLETE:
            return "READ_REMOTE_FEATURES_COMPLETE";
        case HCE_READ_REMOTE_VERSION_COMPLETE:
            return "READ_REMOTE_VERSION_COMPLETE";
        case HCE_QOS_SETUP_COMPLETE:
            return "QOS_SETUP_COMPLETE";
        case HCE_COMMAND_COMPLETE:
            return "COMMAND_COMPLETE";
        case HCE_COMMAND_STATUS:
            return "COMMAND_STATUS";
        case HCE_HARDWARE_ERROR:
            return "HARDWARE_ERROR";
        case HCE_FLUSH_OCCURRED:
            return "FLUSH_OCCURRED";
        case HCE_ROLE_CHANGE:
            return "ROLE_CHANGE";
        case HCE_NUM_COMPLETED_PACKETS:
            return "NUM_COMPLETED_PACKETS";
        case HCE_MODE_CHNG:
            return "MODE_CHNG";
        case HCE_RETURN_LINK_KEYS:
            return "RETURN_LINK_KEYS";
        case HCE_PIN_CODE_REQ:
            return "PIN_CODE_REQ";
        case HCE_LINK_KEY_REQ:
            return "LINK_KEY_REQ";
        case HCE_LINK_KEY_NOTIFY:
            return "LINK_KEY_NOTIFY";
        case HCE_LOOPBACK_COMMAND:
            return "LOOPBACK_COMMAND";
        case HCE_DATA_BUFFER_OVERFLOW:
            return "DATA_BUFFER_OVERFLOW";
        case HCE_MAX_SLOTS_CHNG:
            return "MAX_SLOTS_CHNG";
        case HCE_READ_CLOCK_OFFSET_COMPLETE:
            return "READ_CLOCK_OFFSET_COMPLETE";
        case HCE_CONN_PACKET_TYPE_CHNG:
            return "CONN_PACKET_TYPE_CHNG";
        case HCE_QOS_VIOLATION:
            return "QOS_VIOLATION";
        case HCE_PAGE_SCAN_MODE_CHANGE:
            return "PAGE_SCAN_MODE_CHANGE";
        case HCE_PAGE_SCAN_REPETITION_MODE:
            return "PAGE_SCAN_REPETITION_MODE";
        case HCE_VENDOR_SPECIFIC:
            return "VENDOR SPECIFIC";
        case HCE_BLUETOOTH_LOGO:
            return "LOGO TESTING";
        case HCE_FLOW_SPECIFICATION_COMPLETE:
            return "FLOW_SPECIFICATION_COMPLETE";
        case HCE_INQUIRY_RESULT_WITH_RSSI:
            return "INQUIRY_RESULT_WITH_RSSI";
        case HCE_READ_REMOTE_EXT_FEAT_COMPLETE:
            return "READ_REMOTE_EXT_FEAT_COMPLETE";
        case HCE_FIXED_ADDRESS:
            return "FIXED_ADDRESS";
        case HCE_ALIAS_ADDRESS:
            return "ALIAS_ADDRESS";
        case HCE_GENERATE_ALIAS_REQ:
            return "GENERATE_ALIAS_REQ";
        case HCE_ACTIVE_ADDRESS:
            return "ACTIVE_ADDRESS";
        case HCE_ALLOW_PRIVATE_PAIRING:
            return "ALLOW_PRIVATE_PAIRING";
        case HCE_ALIAS_ADDRESS_REQ:
            return "ALIAS_ADDRESS_REQ";
        case HCE_ALIAS_NOT_RECOGNIZED:
            return "ALIAS_NOT_RECOGNIZED";
        case HCE_FIXED_ADDRESS_ATTEMPT:
            return "FIXED_ADDRESS_ATTEMPT";
        case HCE_SYNC_CONNECT_COMPLETE:
            return "SYNC_CONNECT_COMPLETE";
        case HCE_SYNC_CONN_CHANGED:
            return "SYNC_CONN_CHANGED";
        case HCE_IOCAP_REQUEST:
            return "IOCAP_REQUEST";
        case HCE_SSP_COMPLETE:
            return "SSP_COMPLETE";
        case HCE_IOCAP_RESPONSE:
            return "IOCAP_RESPONSE";
        case HCE_USER_COMFIRM_REQ:
            return "USER_COMFIRM_REQ";
        case HCE_USER_PASSKEY_REQ:
            return "USER_PASSKEY_REQ";
        case HCE_REMOTE_OOB_DATA_REQ:
            return "REMOTE_OOB_DATA_REQ";
        case HCE_LINK_SUPER_TOUT_CHG:
            return "LINK_SUPERVISION_TIMEOUT_CHANGE";
    }
    return "UNKNOWN";
}

static const char *L2CAPSignalOpcodeStr(U8 Opcode)
{
    switch (Opcode) {
        case LLC_COMMAND_REJ:
            return "Command Reject";
        case LLC_CONN_REQ:
            return "Connect Request";
        case LLC_CONN_RSP:
            return "Connect Response";
        case LLC_CONFIG_REQ:
            return "Config Request";
        case LLC_CONFIG_RSP:
            return "Config Response";
        case LLC_DISC_REQ:
            return "Disconnect Request";
        case LLC_DISC_RSP:
            return "Disconnect Response";
        case LLC_ECHO_REQ:
            return "Echo Request";
        case LLC_ECHO_RSP:
            return "Echo Response";
        case LLC_INFO_REQ:
            return "Info Request";
        case LLC_INFO_RSP:
            return "Info Response";
        case LLC_TEST_REQ:
            return "Test Channel";

    }
    return "UNKNOWN Signal Opcode";
}

static const char *HciCommandStr(U16 cmd)
{
    char unknowbuf[50];
    switch (cmd) {
        case HCC_INQUIRY:
            return "INQUIRY";
        case HCC_INQUIRY_CANCEL:
            return "INQUIRY_CANCEL";
        case HCC_START_PERIODIC_INQ_MODE:
            return "START_PERIODIC_INQ_MODE";
        case HCC_EXIT_PERIODIC_INQ_MODE:
            return "EXIT_PERIODIC_INQ_MODE";
        case HCC_CREATE_CONNECTION:
            return "CREATE_CONNECTION";
        case HCC_DISCONNECT:
            return "DISCONNECT";
        case HCC_ADD_SCO_CONNECTION:
            return "ADD_SCO_CONNECTION";
        case HCC_ACCEPT_CON_REQ:
            return "ACCEPT_CON_REQ";
        case HCC_REJECT_CON_REQ:
            return "REJECT_CON_REQ";
        case HCC_LINK_KEY_REQ_REPL:
            return "LINK_KEY_REQ_REPL";
        case HCC_LINK_KEY_REQ_NEG_REPL:
            return "LINK_KEY_REQ_NEG_REPL";
        case HCC_PIN_CODE_REQ_REPL:
            return "PIN_CODE_REQ_REPL";
        case HCC_PIN_CODE_REQ_NEG_REPL:
            return "PIN_CODE_REQ_NEG_REPL";
        case HCC_CHNG_CONN_PACKET_TYPE:
            return "CHNG_CONN_PACKET_TYPE";
        case HCC_AUTH_REQ:
            return "AUTH_REQ";
        case HCC_SET_CONN_ENCRYPT:
            return "SET_CONN_ENCRYPT";
        case HCC_CHNG_CONN_LINK_KEY:
            return "CHNG_CONN_LINK_KEY";
        case HCC_MASTER_LINK_KEY:
            return "MASTER_LINK_KEY";
        case HCC_REM_NAME_REQ:
            return "REM_NAME_REQ";
        case HCC_READ_REMOTE_FEATURES:
            return "READ_REMOTE_FEATURES";
        case HCC_READ_REMOTE_VERSION:
            return "READ_REMOTE_VERSION";
        case HCC_IOCAP_REQ_REPLY:
            return "HCC_IOCAP_REQ_REPLY";
        case HCC_USER_CONFIRM_REQ_REPLY:    
            return "HCC_USER_CONFIRM_REQ_REPLY";    
        case HCC_USER_CONFIRM_REQ_NEG_REPLY:
            return "HCC_USER_CONFIRM_REQ_NEG_REPLY";
        case HCC_USER_PASSKEY_REQ_REPLY:
            return "HCC_USER_PASSKEY_REQ_REPLY";    
        case HCC_USER_PASSKEY_REQ_NEG_REPLY:
            return "HCC_USER_PASSKEY_REQ_NEG_REPLY";
        case HCC_REM_OOB_DATA_REQ_REPLY:   
            return "HCC_REM_OOB_DATA_REQ_REPLY";    
        case HCC_REM_OOB_DATA_REQ_NEG_REPLY:
            return "HCC_REM_OOB_DATA_REQ_NEG_REPLY";
        case HCC_IOCAP_REQ_NEG_REPLY: 
            return "HCC_IOCAP_REQ_NEG_REPLY";                   
        case HCC_READ_CLOCK_OFFSET:
            return "READ_CLOCK_OFFSET";
        case HCC_HOLD_MODE:
            return "HOLD_MODE";
        case HCC_SNIFF_MODE:
            return "SNIFF_MODE";
        case HCC_EXIT_SNIFF_MODE:
            return "EXIT_SNIFF_MODE";
        case HCC_PARK_MODE:
            return "PARK_MODE";
        case HCC_EXIT_PARK_MODE:
            return "EXIT_PARK_MODE";
        case HCC_QOS_SETUP:
            return "QOS_SETUP";
        case HCC_ROLE_DISCOVERY:
            return "ROLE_DISCOVERY";
        case HCC_SWITCH_ROLE:
            return "SWITCH_ROLE";
        case HCC_READ_LINK_POLICY:
            return "READ_LINK_POLICY";
        case HCC_WRITE_LINK_POLICY:
            return "WRITE_LINK_POLICY";
        case HCC_SET_EVENT_MASK:
            return "SET_EVENT_MASK";
        case HCC_RESET:
            return "RESET";
        case HCC_EVENT_FILTER:
            return "EVENT_FILTER";
        case HCC_FLUSH:
            return "FLUSH";
        case HCC_READ_PIN_TYPE:
            return "READ_PIN_TYPE";
        case HCC_WRITE_PIN_TYPE:
            return "WRITE_PIN_TYPE";
        case HCC_CREATE_NEW_UNIT_KEY:
            return "CREATE_NEW_UNIT_KEY";
        case HCC_READ_STORED_LINK_KEY:
            return "READ_STORED_LINK_KEY";
        case HCC_WRITE_STORED_LINK_KEY:
            return "WRITE_STORED_LINK_KEY";
        case HCC_DEL_STORED_LINK_KEY:
            return "DEL_STORED_LINK_KEY";
        case HCC_CHNG_LOCAL_NAME:
            return "CHNG_LOCAL_NAME";
        case HCC_READ_LOCAL_NAME:
            return "READ_LOCAL_NAME";
        case HCC_READ_CONN_ACCEPT_TIMEOUT:
            return "READ_CONN_ACCEPT_TIMEOUT";
        case HCC_WRITE_CONN_ACCEPT_TIMEOUT:
            return "WRITE_CONN_ACCEPT_TIMEOUT";
        case HCC_READ_PAGE_TIMEOUT:
            return "READ_PAGE_TIMEOUT";
        case HCC_WRITE_PAGE_TIMEOUT:
            return "WRITE_PAGE_TIMEOUT";
        case HCC_READ_SCAN_ENABLE:
            return "READ_SCAN_ENABLE";
        case HCC_WRITE_SCAN_ENABLE:
            return "WRITE_SCAN_ENABLE";
        case HCC_READ_PAGE_SCAN_ACTIVITY:
            return "READ_PAGE_SCAN_ACTIVITY";
        case HCC_WRITE_PAGE_SCAN_ACTIVITY:
            return "WRITE_PAGE_SCAN_ACTIVITY";
        case HCC_READ_INQ_SCAN_ACTIVITY:
            return "READ_INQ_SCAN_ACTIVITY";
        case HCC_WRITE_INQ_SCAN_ACTIVITY:
            return "WRITE_INQ_SCAN_ACTIVITY";
        case HCC_READ_AUTH_ENABLE:
            return "READ_AUTH_ENABLE";
        case HCC_WRITE_AUTH_ENABLE:
            return "WRITE_AUTH_ENABLE";
        case HCC_READ_ENCRYPT_MODE:
            return "READ_ENCRYPT_MODE";
        case HCC_WRITE_ENCRYPT_MODE:
            return "WRITE_ENCRYPT_MODE";
        case HCC_READ_CLASS_OF_DEVICE:
            return "READ_CLASS_OF_DEVICE";
        case HCC_WRITE_CLASS_OF_DEVICE:
            return "WRITE_CLASS_OF_DEVICE";
        case HCC_READ_VOICE_SETTING:
            return "READ_VOICE_SETTING";
        case HCC_WRITE_VOICE_SETTING:
            return "WRITE_VOICE_SETTING";
        case HCC_READ_AUTO_FLUSH_TIMEOUT:
            return "READ_AUTO_FLUSH_TIMEOUT";
        case HCC_WRITE_AUTO_FLUSH_TIMEOUT:
            return "WRITE_AUTO_FLUSH_TIMEOUT";
        case HCC_READ_NUM_BC_RETRANSMIT:
            return "READ_NUM_BC_RETRANSMIT";
        case HCC_WRITE_NUM_BC_RETRANSMIT:
            return "WRITE_NUM_BC_RETRANSMIT";
        case HCC_READ_HOLD_MODE_ACTIVITY:
            return "READ_HOLD_MODE_ACTIVITY";
        case HCC_WRITE_HOLD_MODE_ACTIVITY:
            return "WRITE_HOLD_MODE_ACTIVITY";
        case HCC_READ_XMIT_POWER_LEVEL:
            return "READ_XMIT_POWER_LEVEL";
        case HCC_READ_SCO_FC_ENABLE:
            return "READ_SCO_FC_ENABLE";
        case HCC_WRITE_SCO_FC_ENABLE:
            return "WRITE_SCO_FC_ENABLE";
        case HCC_SET_CTRLR_TO_HOST_FLOW_CTRL:
            return "SET_CTRLR_TO_HOST_FLOW_CTRL";
        case HCC_HOST_BUFFER_SIZE:
            return "HOST_BUFFER_SIZE";
        case HCC_HOST_NUM_COMPLETED_PACKETS:
            return "HOST_NUM_COMPLETED_PACKETS";
        case HCC_READ_LINK_SUPERV_TIMEOUT:
            return "READ_LINK_SUPERV_TIMEOUT";
        case HCC_WRITE_LINK_SUPERV_TIMEOUT:
            return "WRITE_LINK_SUPERV_TIMEOUT";
        case HCC_READ_NUM_IAC:
            return "READ_NUM_IAC";
        case HCC_READ_CURRENT_IAC_LAP:
            return "READ_CURRENT_IAC_LAP";
        case HCC_WRITE_CURRENT_IAC_LAP:
            return "WRITE_CURRENT_IAC_LAP";
        case HCC_READ_PAGE_SCAN_PERIOD_MODE:
            return "READ_PAGE_SCAN_PERIOD_MODE";
        case HCC_WRITE_PAGE_SCAN_PERIOD_MODE:
            return "WRITE_PAGE_SCAN_PERIOD_MODE";
        case HCC_READ_PAGE_SCAN_MODE:
            return "READ_PAGE_SCAN_MODE";
        case HCC_WRITE_PAGE_SCAN_MODE:
            return "WRITE_PAGE_SCAN_MODE";
        case HCC_READ_LOCAL_VERSION:
            return "READ_LOCAL_VERSION";
        case HCC_READ_LOCAL_FEATURES:
            return "READ_LOCAL_FEATURES";
        case HCC_READ_BUFFER_SIZE:
            return "READ_BUFFER_SIZE";
        case HCC_READ_COUNTRY_CODE:
            return "READ_COUNTRY_CODE";
        case HCC_READ_BD_ADDR:
            return "READ_BD_ADDR";
        case HCC_READ_FAILED_CONTACT_COUNT:
            return "READ_FAILED_CONTACT_COUNT";
        case HCC_RESET_FAILED_CONTACT_COUNT:
            return "RESET_FAILED_CONTACT_COUNT";
        case HCC_GET_LINK_QUALITY:
            return "GET_LINK_QUALITY";
        case HCC_READ_RSSI:
            return "READ_RSSI";
        case HCC_READ_LOOPBACK_MODE:
            return "READ_LOOPBACK_MODE";
        case HCC_WRITE_LOOPBACK_MODE:
            return "WRITE_LOOPBACK_MODE";
        case HCC_ENABLE_DUT:
            return "ENABLE_DUT";
        case HCC_CREATE_CONNECTION_CANCEL:
            return "CREATE_CONNECTION_CANCEL";
        case HCC_REM_NAME_REQ_CANCEL:
            return "REM_NAME_REQ_CANCEL";
        case HCC_READ_REMOTE_EXT_FEATURES:
            return "READ_REMOTE_EXT_FEATURES";
        case HCC_READ_LMP_HANDLE:
            return "READ_LMP_HANDLE";
        case HCC_EXCHANGE_FIXED_INFO:
            return "EXCHANGE_FIXED_INFO";
        case HCC_EXCHANGE_ALIAS_INFO:
            return "EXCHANGE_ALIAS_INFO";
        case HCC_PRIVATE_PAIRING_REQ_REPL:
            return "PRIVATE_PAIRING_REQ_REPL";
        case HCC_PRIVATE_PAIRING_REQ_NEG_REPL:
            return "PRIVATE_PAIRING_REQ_NEG_REPL";
        case HCC_GENERATED_ALIAS:
            return "GENERATED_ALIAS";
        case HCC_ALIAS_ADDRESS_REQ_REPL:
            return "ALIAS_ADDRESS_REQ_REPL";
        case HCC_ALIAS_ADDRESS_REQ_NEG_REPL:
            return "ALIAS_ADDRESS_REQ_NEG_REPL";
        case HCC_SETUP_SYNC_CONNECTION:
            return "SETUP_SYNC_CONNECTION";
        case HCC_ACCEPT_SYNC_CON_REQ:
            return "ACCEPT_SYNC_CON_REQ";
        case HCC_REJECT_SYNC_CON_REQ:
            return "REJECT_SYNC_CON_REQ";
        case HCC_READ_DEFAULT_LINK_POLICY:
            return "READ_DEFAULT_LINK_POLICY";
        case HCC_WRITE_DEFAULT_LINK_POLICY:
            return "WRITE_DEFAULT_LINK_POLICY";
        case HCC_FLOW_SPECIFICATION:
            return "FLOW_SPECIFICATION";
        case HCC_SET_AFH_HOST_CHAN_CLASS:
            return "SET_AFH_HOST_CHAN_CLASS";
        case HCC_READ_INQ_SCAN_TYPE:
            return "READ_INQ_SCAN_TYPE";
        case HCC_WRITE_INQ_SCAN_TYPE:
            return "WRITE_INQ_SCAN_TYPE";
        case HCC_READ_INQ_MODE:
            return "READ_INQ_MODE";
        case HCC_WRITE_INQ_MODE:
            return "WRITE_INQ_MODE";
        case HCC_READ_PAGE_SCAN_TYPE:
            return "READ_PAGE_SCAN_TYPE";
        case HCC_WRITE_PAGE_SCAN_TYPE:
            return "WRITE_PAGE_SCAN_TYPE";
        case HCC_READ_AFH_CHAN_ASSESS_MODE:
            return "READ_AFH_CHAN_ASSESS_MODE";
        case HCC_WRITE_AFH_CHAN_ASSESS_MODE:
            return "WRITE_AFH_CHAN_ASSESS_MODE";
        case HCC_READ_SIMPLE_PAIRING_MODE:
            return "HCC_READ_SIMPLE_PAIRING_MODE";
        case HCC_WRITE_SIMPLE_PAIRING_MODE:
            return "HCC_WRITE_SIMPLE_PAIRING_MODE";
        case HCC_READ_LOCAL_COMMANDS:
            return "READ_LOCAL_COMMANDS";
        case HCC_READ_LOCAL_EXT_FEATURES:
            return "READ_LOCAL_EXT_FEATURES";
        case HCC_READ_AFH_CHANNEL_MAP:
            return "READ_AFH_CHANNEL_MAP";
        case HCC_READ_CLOCK:
            return "READ_CLOCK";
        case HCC_MTK6622_SET_PCM:
            return "HCC_MTK6622_SET_PCM";
        case HCC_MTK6622_SET_BDADDR:
            return "MT6622_SET_BDADDR";
        case HCC_MTK6622_SET_RADIO:
            return "MT6622_SET_RADIO";
        case HCC_MTK6622_FW_PATCH_MODE:
            return "MT6622_FW_PATCH_MODE";
        case HCC_MTK6622_FW_PATCH:
            return "MT6622_FW_PATCH";
    }


    if ((cmd >> 8) & 0xFC) {
        sprintf(unknowbuf, "%s=%04x", "Vendor Specific", cmd);
        return unknowbuf;
    }

    sprintf(unknowbuf, "%s=%04x", "UNKNOWN Command", cmd);

    return unknowbuf;
}
static const char *GetTimeStr(const char *Msg, DWORD time)
{
    static char   buffer[20];

    sprintf(buffer, "%s %02ld.%03lds ", Msg, (time)/1000, (time)%1000);

    return buffer;
}

static BYTE HciDataParse(char * buf, U16 idx, U8 dat)
{
    return 0; 
}

static BYTE HciL2Parse(char * buf, U16 cid,  U16 idx, U8 dat)
{
    if(cid == showRFComm1 || cid == showRFComm2)
    {
        static U16 len = 0;
        static U8 elen = 0;
        static U8 ctl = 0;
        static U8 dlci = 0;
        static U8 credit = 0;
        if(idx == 0)
        {
            elen = 0;
            dlci = (dat >> 2)& 0x3f;
            return sprintf(buf, " [RFCOMM Adr=0x%02X(EA%1X CR%1X DLCI%02X)", dat, dat & 1, (dat >> 1) & 1, dlci);
        }
        else if(idx == 1)
        {
            credit = (dat == 0xFF) ? 1 : 0;

            ctl = dat & 0xEF;
            
            return sprintf(buf, " Ctl=0x%02X(%s PF%1X)", dat, RFControlStr(dat), (dat >> 4) & 0x1);            
        }
        else if(idx == 2)
        {
            len = dat >> 1;

            if(!(dat & 1))
            {
                elen = 1;
                return 0;
            }
            else
                return sprintf(buf, " Len=%02d", len);
        }
        else if(elen == 1 && idx == 3)
        {
            len |= (dat << 7);
            return sprintf(buf, " Len=%04d", len);
        }
        else if((credit && idx == 3) || (elen == 1 && credit && idx == 4))
        {
            return sprintf(buf, " Credit=0x%02X", dat);
        }
        else if(idx == 3 + len + credit + elen)
        {
            return sprintf(buf, " FCS=0x%02X]", dat);
        }
        else //Information
        {
            if(dlci == 0)
            {
                static U8 dlen = 0;
                static U8 type = 0;
                if(idx == (3 + credit + elen) + 0)
                {
                    type = dat >> 2;
                    return sprintf(buf, " <Type=0x%02X(%c:%s)", type, dat & 2 ? 'C':'R', RFDLCI0TypeStr(type));
                }
                else if(idx == (3 + credit + elen) + 1)
                {
                    dlen = dat >> 1;
                    return sprintf(buf, " Len=%02d", dlen);
                }
                else 
                {
                    if(idx == (3 + credit + elen) + dlen + 1)
                        return sprintf(buf, " 0x%02X>", dat);
                    else
                        return sprintf(buf, " 0x%02X", dat);
                }
            }
            else //channel
            {
            }
        }
    }
    else
    if(cid == 0x0001) //EDR signaling channel
    {
        static U8 op;
        if(idx == 0)
        {
            op = dat;
        }
        else
        {
            if(op == LLC_CONN_REQ)
            {
                static U8 tmp;
                if(idx == 1) //identifier
                    return sprintf(buf, "[id:0x%x] ", dat);
                else if(idx == 4 || idx == 6) //psm low byte, source cid low byte
                    tmp = dat;
                else if(idx == 5) //psm high byte
                    return sprintf(buf, "[psm:0x%04x] ", (dat << 8) + tmp);
                else if(idx == 7) //source cid high byte
                    return sprintf(buf, "[scid:0x%04x] ", (dat << 8) + tmp);
            }
            else if(op == LLC_CONN_RSP)
            {
                static U8 tmp;
                if(idx == 1) //identifier
                    return sprintf(buf, "[id:0x%x] ", dat);
                else if(idx == 4 || idx == 6 || idx == 8 || idx == 10) //dcid low byte, scid low byte, result low byte, status low byte
                    tmp = dat;
                else if(idx == 5) //dcid high byte
                    return sprintf(buf, "[dcid:0x%04x] ", (dat << 8) + tmp);
                else if(idx == 7) //scid high byte
                    return sprintf(buf, "[scid:0x%04x] ", (dat << 8) + tmp);
                else if(idx == 9) //result high byte
                    return sprintf(buf, "[%s]", L2CAPConnStatusStr((dat << 8) + tmp));
                else if(idx == 10)
                    return sprintf(buf, "[reasion:0x%x]", (dat << 8) + tmp);
            }
            else if(op == LLC_CONFIG_REQ)
            {
                static U8 tmp;
                if(idx == 1) //identifier
                    return sprintf(buf, "[id:0x%x] ", dat);
                else if(idx == 4 || idx == 6) //dcid low byte, flag low byte
                    tmp = dat;
                else if(idx == 5) //dcid high byte
                    return sprintf(buf, "[dcid:0x%04x] ", (dat << 8) + tmp);
                else if(idx == 7) //flag high byte
                    return sprintf(buf, "[flag:0x%04x] ", (dat << 8) + tmp);
                else if(idx == 8)
                {
                    return sprintf(buf, "config: %02x", dat);
                }
                else if(idx > 8)
                {
                    return sprintf(buf, "%02x ", dat);
                }
            }
            else if(op == LLC_CONFIG_RSP)
            {
                static U8 tmp;
                if(idx == 1) //identifier
                    return sprintf(buf, "[id:0x%x] ", dat);
                else if(idx == 4 || idx == 6 || idx == 8) //scid low byte, flag low byte, result low byte
                    tmp = dat;
                else if(idx == 5) //scid high byte
                    return sprintf(buf, "[scid:0x%04x] ", (dat << 8) + tmp);
                else if(idx == 7) //flag high byte
                    return sprintf(buf, "[flag:0x%04x] ", (dat << 8) + tmp);
                else if(idx == 9) //result high byte
                    return sprintf(buf, "[result:0x%04x] ", (dat << 8) + tmp);
                else if(idx == 10)
                {                    
                    return sprintf(buf, "config: %02x", dat);
                }
                else if(idx > 10)
                {
                    return sprintf(buf, "%02x ", dat);
                }
            }
            else if(op == LLC_INFO_REQ)
            {
                static U8 tmp;
                if(idx == 1) //identifier
                    return sprintf(buf, "[id:0x%x] ", dat);
                else if(idx == 4) //infoType low byte
                    tmp = dat;
                else if(idx == 5) //infoType high byte
                {
                    return sprintf(buf, "[infoType:0x%x]", tmp + (dat << 8));
                }
            }
            else if(op == LLC_INFO_RSP)
            {
                static U8 tmp;
                static U8 result;
                if(idx == 1) //identifier
                    return sprintf(buf, "[id:0x%x] ", dat);
                else if(idx == 4 || idx == 6) //infoType low byte, result low byte
                    tmp = dat;
                else if(idx == 5) //infoType high byte
                    return sprintf(buf, "[infoType:0x%x] ", tmp + (dat << 8));
                else if(idx == 7) //result high byte                
                {
                    result = (tmp + (dat << 8));
                    return sprintf(buf, "[result:%s]", result == 0x01 ? "NotSupport" : result == 0x00 ? "Success" : "Unknown");
                }
                else if(idx == 8)
                {
                    return sprintf(buf, "Data: ");
                }
                else if(idx > 8)
                {
                    return sprintf(buf, "%02x ", dat);
                }
            }
        }
    }

    return 0;
}

static BYTE HciEventParse(char * buf, U8 Evt, U16 idx, U8 dat)
{

    switch (Evt) {
#if 0
        case HCE_INQUIRY_COMPLETE:
            sprintf(eLine[1], "         Status:%s",
                    pHC_Status(p[0]));
            p++;
            break;

        case HCE_INQUIRY_RESULT:
        case HCE_INQUIRY_RESULT_WITH_RSSI:
            if (p[0] == 1) {
                sprintf(eLine[1], "         BD_ADDR: %s ClockOffset:%d",
                        bdaddr_ntoa((BD_ADDR *)(p+1), addrString),
                        LEtoHost16(p+13));
                if (Buffer[0] == HCE_INQUIRY_RESULT_WITH_RSSI) {
                    sprintf(eLine[1], "%s Rssi:%d",eLine[1],(S8)(p[14]));
                }
                sprintf(eLine[2], "         ");
                pHC_ClassOfDevice((U8 *)p + 9 + (Buffer[0]==HCE_INQUIRY_RESULT?1:0),
                                  eLine[2] + strlen(eLine[2]));
                p += 15;
            }
            break;
#endif
        case HCE_SSP_COMPLETE:
        {
            static U8 tmp[6];
            if(idx == 0)                
            {
                return sprintf(buf, "<Simple Pairing: %s> ", HciStatusStr(dat));
            }
            else if(idx < 6) //BD ADDR
            {
                tmp[idx-1] = dat;
            }
            else if(idx == 6) //BD ADDR
            {
                return sprintf(buf, "<%02x:%02x:%02x:%02x:%02x:%02x> ", dat, tmp[4], tmp[3], tmp[2], tmp[1], tmp[0]);
            }
            break;
        }
        case HCE_COMMAND_COMPLETE:
        {
            static U8 tmp;
            if(idx == 0) //Num_HCI_Command_Packets
            {
                return sprintf(buf, "<CmdPtNm:%2d> ", dat);
            }
            else if(idx == 1) //Command_Opcode low
                tmp = dat;
            else if(idx == 2) //Command_Opcode high
            {
                return sprintf(buf, "<%s:", HciCommandStr((dat << 8) + tmp));
            }
            else if(idx == 3)
            {
                return sprintf(buf, "%s>", HciStatusStr(dat));
            }
        }
        break;
        case HCE_CONNECT_REQUEST:
        {
            static U8 tmp[6];
            if(idx < 5) //BD ADDR
            {
                tmp[idx] = dat;
            }
            else if(idx == 5) //BD ADDR
            {
                return sprintf(buf, "<%02x:%02x:%02x:%02x:%02x:%02x> ", dat, tmp[4], tmp[3], tmp[2], tmp[1], tmp[0]);
            }
            else if(idx < 8)//COD
            {
                tmp[idx-6] = dat;
            }
            else if(idx == 8) //COD
            {
                return sprintf(buf, "<COD:%02x%02x%02x> ", dat, tmp[1], tmp[0]);
            }
            else if(idx == 9) //line type
            {
                return sprintf(buf, "<type:%s>", dat == 0 ? "SCO" : dat == 1 ? "ACL" : dat == 2 ? "eSCO" : "unknown");
            }
            break;
        }
        case HCE_LINK_SUPER_TOUT_CHG:
        {
            static U8 tmp;
            if(idx == 0 || idx == 2)
                tmp = dat;
            else if(idx == 1)
                return sprintf(buf, "<Hndl:%03x>", tmp + ((dat & 0xf) << 8));
            else if(idx == 3)
                return sprintf(buf, "<Tout:%d s>", (tmp + (dat << 8) * 625) / 1000 /1000);
         
            break;
        }
        case HCE_IOCAP_RESPONSE:
        {
            static U8 tmp[6];
            if(idx < 5)
            {
                tmp[idx] = dat;
            }
            else if(idx == 5)
            {
                return sprintf(buf, "<%02x:%02x:%02x:%02x:%02x:%02x> ", dat, tmp[4], tmp[3], tmp[2], tmp[1], tmp[0]);
            }
            else if(idx == 6) //io capability
            {
                if(dat == 0)
                    return sprintf(buf, "<DisplayOnly>");
                else if(dat == 1)
                    return sprintf(buf, "<DisplayYesNo>");
                else if(dat == 2)
                    return sprintf(buf, "<KeyboardOnly>");
                else if(dat == 3)
                    return sprintf(buf, "<NoInputNoOutput>");
            }
            else if(idx == 7) //oob data present
            {
               if(dat == 1)
                   return sprintf(buf, "<OOB present>");
               else if(dat == 0)
                   return sprintf(buf, "<OOB not present>");
            }
            else if(idx == 8)
            {
                if(dat & 0x2) //dedicated bonding
                {
                    return sprintf(buf, "<Dedicated Bonding - %s", dat & 1 ? "IO cap>" : "Num comp>");
                }
                else if(dat & 0x4) //general bonding
                {
                    return sprintf(buf, "<General Bonding - %s", dat & 1 ? "IO cap>" : "Num comp>");
                }
                else if(dat < 2) //no bonding
                {
                    return sprintf(buf, "<No Bonding - %s", dat & 1 ? "IO cap>" : "Num comp>");
                }
            }
                
                
            break;
        }

        case HCE_DISCONNECT_COMPLETE:
        {
            static U8 tmp;
            if(idx == 1)
                tmp = dat;
            else if(idx == 2)
                return sprintf(buf, "<Hndl:%03x>", tmp + ((dat & 0xf) << 8));
            else if(idx == 3)
                return sprintf(buf, "<Reason:%s>", HciStatusStr(dat));
            break;
        }
#if 0
        case HCE_AUTH_COMPLETE:
            sprintf(eLine[1], "         Status:%s ConnHndl:%d",
                    pHC_Status(p[0]),
                    (LEtoHost16(p+1) & 0x0FFF));
            p += 3;
            break;

        case HCE_REMOTE_NAME_REQ_COMPLETE:
            sprintf(eLine[1], "         Status:%s BD_ADDR: %s",
                    pHC_Status(p[0]),
                    bdaddr_ntoa((BD_ADDR *)(p+1), addrString));
            p += 7;
            break;

        case HCE_ENCRYPT_CHNG:
            sprintf(eLine[1], "         Status:%s ConnHndl:%d",
                    pHC_Status(p[0]),
                    (LEtoHost16(p+1) & 0x0FFF));
            p += 3;
            break;

        case HCE_CHNG_CONN_LINK_KEY_COMPLETE:
            sprintf(eLine[1], "         Status:%s ConnHndl:%d",
                    pHC_Status(p[0]),
                    (LEtoHost16(p+1) & 0x0FFF));
            p += 3;
            p++;
            break;

        case HCE_MASTER_LINK_KEY_COMPLETE:
            sprintf(eLine[1], "         Status:%s ConnHndl:%d",
                    pHC_Status(p[0]),
                    (LEtoHost16(p+1) & 0x0FFF));
            p += 3;
            break;

        case HCE_READ_REMOTE_FEATURES_COMPLETE:
            sprintf(eLine[1], "         Status:%s ConnHndl:%d",
                    pHC_Status(p[0]),
                    (LEtoHost16(p+1) & 0x0FFF));
            p += 3;
            break;

        case HCE_READ_REMOTE_VERSION_COMPLETE:
            sprintf(eLine[1], "         Status:%s ConnHndl:%d",
                    pHC_Status(p[0]),
                    (LEtoHost16(p+1) & 0x0FFF));
            p += 3;
            break;

        case HCE_QOS_SETUP_COMPLETE:
            sprintf(eLine[1], "         Status:%s ConnHndl:%d",
                    pHC_Status(p[0]),
                    (LEtoHost16(p+1) & 0x0FFF));
            p += 3;
            break;
#endif
        case HCE_CONNECT_COMPLETE:
        {
            static U8 tmp[6];
            if(idx == 0)
            {
                return sprintf(buf, "<%s> ", HciStatusStr(dat));
            }
            else if(idx == 1) //connect handle low
            {
                tmp[0] = dat;
            }
            else if(idx == 2) //connect handle high
            {
                return sprintf(buf, "<Hndl:%03x> ", (dat << 8) + tmp[0]);
            }
            else if(idx < 8) //BD ADDR
            {
                tmp[idx-3] = dat;
            }
            else if(idx == 8) //BD ADDR
            {
                return sprintf(buf, "<%02x:%02x:%02x:%02x:%02x:%02x> ", dat, tmp[4], tmp[3], tmp[2], tmp[1], tmp[0]);
            }
            else if(idx == 9) //line type
            {
                return sprintf(buf, "<type:%s> ", dat == 0 ? "SCO" : dat == 1 ? "ACL" : dat == 2 ? "eSCO" : "unknown");
            }
            else if(idx == 10) //encryption
            {
                return sprintf(buf, "<enc:%s>", dat == 0 ? "disabled" : "enabled");
            }
            break;
        }
        case HCE_COMMAND_STATUS:
        {
            static U8 tmp;
            if(idx == 0)
            {
                return sprintf(buf, "<%s> ", HciStatusStr(dat));
            }
            if(idx == 1) //Num_HCI_Command_Packets
            {
                return sprintf(buf, "<CmdPtNm:%2d> ", dat);
            }
            else if(idx == 2) //Command_Opcode low
                tmp = dat;
            else if(idx == 3) //Command_Opcode high
            {
                return sprintf(buf, "<%s>", HciCommandStr((dat << 8) + tmp));
            }
        }
        break;
#if 0
        case HCE_HARDWARE_ERROR:
            break;

        case HCE_FLUSH_OCCURRED:
            sprintf(eLine[1], "         ConnHndl:%d",
                    (LEtoHost16(p) & 0x0FFF));
            p +=2;
            break;

        case HCE_ROLE_CHANGE:
            sprintf(eLine[1], "         Status:%s BD_ADDR:%s",
                    pHC_Status(p[0]),
                    bdaddr_ntoa((BD_ADDR *)(p+1), addrString),
                    p[7] ? "Slave" : "Master");
            p += 8;
            break;

        case HCE_NUM_COMPLETED_PACKETS:
            numHndls = p[0];
            for (i=0, j=1; i<numHndls; i++, j++) {
                sprintf(eLine[j], "         NumHndls:%d ConnHndl:%d NumPckts:%d",
                        numHndls,
                        (LEtoHost16(p+1+2*i) & 0x0FFF),
                        (LEtoHost16(p+1+2*numHndls+2*i)));
                if ( j == 8) {
                    for (j=0; j<9; j++) {
                        SnifferMsg(eLine[j]);
                    }
                    j = 0;
                }
            }
            p += 1 + numHndls*4;
            break;

        case HCE_MODE_CHNG:
            sprintf(eLine[1], "         Status:%s ConnHndl:%d",
                    pHC_Status(p[0]),
                    (LEtoHost16(p+1) & 0x0FFF));
            p += 3;
            break;

        case HCE_RETURN_LINK_KEYS:
            break;

        case HCE_PIN_CODE_REQ:
            sprintf(eLine[1], "         BD_ADDR:%s",
                    bdaddr_ntoa((BD_ADDR *)p, addrString));
            p += 6;
            break;

        case HCE_LINK_KEY_REQ:
            sprintf(eLine[1], "         BD_ADDR:%s",
                    bdaddr_ntoa((BD_ADDR *)p, addrString));
            p += 6;
            break;

        case HCE_LINK_KEY_NOTIFY:
            sprintf(eLine[1], "         BD_ADDR:%s",
                    bdaddr_ntoa((BD_ADDR *)p, addrString));
            p += 6;
            break;

        case HCE_LOOPBACK_COMMAND:
            break;

        case HCE_DATA_BUFFER_OVERFLOW:
            break;

        case HCE_MAX_SLOTS_CHNG:
            sprintf(eLine[1], "         ConnHndl:%d MaxSlots:%d",
                    (LEtoHost16(p) & 0x0FFF),
                    p[2]);
            p += 3;
            break;

        case HCE_READ_CLOCK_OFFSET_COMPLETE:
            sprintf(eLine[1], "         Status:%s ConnHndl:%d",
                    pHC_Status(p[0]),
                    (LEtoHost16(p+1) & 0x0FFF));
            p += 3;
            break;

        case HCE_CONN_PACKET_TYPE_CHNG:
            sprintf(eLine[1], "         Status:%s ConnHndl:%d",
                    pHC_Status(p[0]),
                    (LEtoHost16(p+1) & 0x0FFF));
            p += 3;
            break;

        case HCE_QOS_VIOLATION:
            sprintf(eLine[1], "         ConnHndl:%d",
                    (LEtoHost16(p) & 0x0FFF));
            p += 2;
            break;

        case HCE_PAGE_SCAN_MODE_CHANGE:
            sprintf(eLine[1], "         BD_ADDR:%s",
                    bdaddr_ntoa((BD_ADDR *)p, addrString));
            p += 6;
            break;

        case HCE_PAGE_SCAN_REPETITION_MODE:
            sprintf(eLine[1], "         BD_ADDR:%s",
                    bdaddr_ntoa((BD_ADDR *)p, addrString));
            p += 6;
            break;

        case HCE_SYNC_CONNECT_COMPLETE:
            sprintf(eLine[1], "         Status:%s ConnHndl:%d BD_ADDR:%s",
                    pHC_Status(p[0]),
                    (LEtoHost16(p+1) & 0x0FFF),
                    bdaddr_ntoa((BD_ADDR *)p+3, addrString));
            p += 1 + 2 + 6;
            sprintf(eLine[2], "         LinkType:%s SlotsBetween:%d SlotsRetrans: %d",
                    pHC_LinkType(p[0]),
                    p[1], p[2]);
            p += 1 + 1 + 1;
            sprintf(eLine[3], "         RxPayload:%d TxPayload:%d AirMode:%s",
                    LEtoHost16(p),
                    LEtoHost16(p+2),
                    pHC_AirMode(p[4]));
            p += 2 + 2 + 1;
            break;

        case HCE_SYNC_CONN_CHANGED:
            sprintf(eLine[1], "         Status:%s ConnHndl:%d",
                    pHC_Status(p[0]),
                    (LEtoHost16(p+1) & 0x0FFF));
            p += 1 + 2;
            sprintf(eLine[2], "         SlotsBetween: %d SlotsRetrans: %d",
                    p[1], p[2]);
            p += 1 + 1;
            sprintf(eLine[3], "         RxPayload:%d TxPayload:%d",
                    LEtoHost16(p),
                    LEtoHost16(p+2));
            p += 2 + 2;
            break;

        case HCE_VENDOR_SPECIFIC:
            break;

        case HCE_BLUETOOTH_LOGO:
            break;
#endif
        default:
            break;
    }
    return 0;
}

static BYTE HciCommandParse(char * buf, U16 cmd, U16 idx, U8 dat)
{
    switch (cmd) {
#if 0
        case HCC_CREATE_CONNECTION:
            sprintf(cLine[1], "         BD_ADDR:%s Type:%s",
                    bdaddr_ntoa((BD_ADDR *)p, addrString),
                    pPacketType(LEtoHost16(p+6), typeString));
            sprintf(cLine[2], "         PSRMode:%s PSMode:%s ClkOffset:%d RSwitch:%s",
                    pPsrMode(p[8]),
                    pPsMode(p[9]),
                    (LEtoHost16(p+10) & 0xEFFF),
                    p[12] ? "Yes" : "No");
            p += 13;
            break;

        case HCC_DISCONNECT:
            sprintf(cLine[1], "         ConnHndl:%d Status:%s",
                    (LEtoHost16(p) & 0x0FFF),
                    pHC_Status(p[2]));
            p += 3;
            break;
#endif
        case HCC_ACCEPT_CON_REQ:
        {
            static U8 tmp[5];
            if(idx < 5) //BD ADDR
            {
                tmp[idx] = dat;
            }
            else if(idx == 5) //BD ADDR
            {
                return sprintf(buf, "<%02x:%02x:%02x:%02x:%02x:%02x> ", dat, tmp[4], tmp[3], tmp[2], tmp[1], tmp[0]);
            }
            else if(idx == 6) //role
            {
                return sprintf(buf, "<%s>", dat == 0 ? "Master" : "Slave");
            }
            break;
        }
        case HCC_IOCAP_REQ_REPLY:
        {
            static U8 tmp[6];
            if(idx < 5)
            {
                tmp[idx] = dat;
            }
            else if(idx == 5)
            {
                return sprintf(buf, "<%02x:%02x:%02x:%02x:%02x:%02x> ", dat, tmp[4], tmp[3], tmp[2], tmp[1], tmp[0]);
            }
            else if(idx == 6) //io capability
            {
                if(dat == 0)
                    return sprintf(buf, "<DisplayOnly>");
                else if(dat == 1)
                    return sprintf(buf, "<DisplayYesNo>");
                else if(dat == 2)
                    return sprintf(buf, "<KeyboardOnly>");
                else if(dat == 3)
                    return sprintf(buf, "<NoInputNoOutput>");
            }
            else if(idx == 7) //oob data present
            {
               if(dat == 1)
                   return sprintf(buf, "<OOB present>");
               else if(dat == 0)
                   return sprintf(buf, "<OOB not present>");
            }
            else if(idx == 8)
            {
                if(dat & 0x2) //dedicated bonding
                {
                    return sprintf(buf, "<Dedicated Bonding - %s", dat & 1 ? "IO cap>" : "Num comp>");
                }
                else if(dat & 0x4) //general bonding
                {
                    return sprintf(buf, "<General Bonding - %s", dat & 1 ? "IO cap>" : "Num comp>");
                }
                else if(dat < 2) //no bonding
                {
                    return sprintf(buf, "<No Bonding - %s", dat & 1 ? "IO cap>" : "Num comp>");
                }
            }
                
            break;
        }
#if 0
        case HCC_REJECT_CON_REQ:
            sprintf(cLine[1], "         BD_ADDR:%s Reason:%s",
                    bdaddr_ntoa((BD_ADDR *)p, addrString),
                    pHC_Status(p[6]));
            p += 7;
            break;

        case HCC_LINK_KEY_REQ_REPL:
            sprintf(cLine[1], "         BD_ADDR:%s",
                    bdaddr_ntoa((BD_ADDR *)p, addrString));
            p += 6;
            break;

        case HCC_LINK_KEY_REQ_NEG_REPL:
            sprintf(cLine[1], "         BD_ADDR:%s",
                    bdaddr_ntoa((BD_ADDR *)p, addrString));
            p += 6;
            break;

        case HCC_PIN_CODE_REQ_REPL:
            sprintf(cLine[1], "         BD_ADDR:%s",
                    bdaddr_ntoa((BD_ADDR *)p, addrString));
            p += 6;
            break;

        case HCC_PIN_CODE_REQ_NEG_REPL:
            sprintf(cLine[1], "         BD_ADDR:%s",
                    bdaddr_ntoa((BD_ADDR *)p, addrString));
            p += 6;
            break;

        case HCC_SWITCH_ROLE:
            sprintf(cLine[1], "         BD_ADDR:%s",
                    bdaddr_ntoa((BD_ADDR *)p, addrString));
            p += 6;
            break;

        case HCC_WRITE_SCAN_ENABLE:
            sprintf(cLine[1], "         Scan_Enable:%s",
                    pScanEnable(p[0]));
            p++;
            break;

        case HCC_SETUP_SYNC_CONNECTION:
            sprintf(cLine[1], "         ConnHndl:%d TxBandwidth:%ld RxBandwidth:%ld",
                    (LEtoHost16(p) & 0x0FFF),
                    LEtoHost32(p+2),
                    LEtoHost32(p+6));
            p += 2 + 4 + 4;
            break;

        case HCC_ACCEPT_SYNC_CON_REQ:
            sprintf(cLine[1], "         BD_ADDR:%s",
                    bdaddr_ntoa((BD_ADDR *)p, addrString));
            p += 6;
            break;

        case HCC_REJECT_SYNC_CON_REQ:
            sprintf(cLine[1], "         BD_ADDR:%s Reason:%s",
                    bdaddr_ntoa((BD_ADDR *)p, addrString),
                    pHC_Status(p[6]));
            p += 7;
            break;

        case HCC_WRITE_LINK_POLICY:
            sprintf(cLine[1], "         ConnHndl:%d Enabled Modes:%s%s%s%s%s",
                    (LEtoHost16(p) & 0x0FFFF),
                    (LEtoHost16(p+2) == 0 ? "None" : ""),
                    (LEtoHost16(p+2) & 0x01 ? "Role-switch " : ""),
                    (LEtoHost16(p+2) & 0x02 ? "Hold " : ""),
                    (LEtoHost16(p+2) & 0x04 ? "Sniff " : ""),
                    (LEtoHost16(p+2) & 0x08 ? "Park " : ""));
            p += 4;
            break;
#else
        case HCC_HOST_BUFFER_SIZE:
        {
            static U8 tmp;
            if(idx == 0 || idx == 3 || idx == 5) //acl packet length low, acl total num low, sco total num low
            {
                tmp = dat;
            }
            else if(idx == 1 || idx == 4 || idx == 6) //acl packet length high, acl total num high, sco total num high
            {
                return sprintf(buf, " 0x%02x%02x", dat, tmp);
            }
            else if(idx == 2) //sco packet length
            {
                return sprintf(buf, " 0x%02x", dat);
            }
            break;
        }
#endif
        default:
            break;
    }
    return 0;
}

U8 AddOneLine(U8 type, U16 length, U32 offset, U16 offset_offset, U32 time)
{
    if(line_number == 1023)
    {
        Taxeia_Report("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!UI log FULL!!!!!!!!!!!!!!!!!!!!!\n");
        return 0;
    }
    line_display_type[line_number] = type;
    line_length[line_number] = length;
    line_offset[line_number] = offset;
    line_offset_offset[line_number] = offset_offset;
    if(line_time[line_number] == 0)
        line_time[line_number++] = time;
    return 1;
}

U8 IsLineFull()
{
    return line_number == 1023;
}

static U8 TaxeiaUpdateRxLineBuffer()
{
    U8 rlen;
    U8 more_line;
    U8 i;

    more_line = 0;

    for(i = 0; i < 2; i++)
    {
        ASSERT(rxlog[0] == 4 || rxlog[0] == 2);
        
        if(i == 0)
        {
            if(showHCI && rxlog[0] == 4)
            {
                if(AddOneLine(SPACE, 0, 0, 0, 0))
                    more_line++;
                if(AddOneLine(HCI_CMD_EVT, rxlength, rx_memo_offset, rx_memo_offset_offset, timeGetTime() - start_local))
                    more_line++;
            }
            else
                continue;
        }
        else if(i == 1)
        {
            if(rxlog[0] == 2)
            {
                if(AddOneLine(SPACE, 0, 0, 0, 0))
                    more_line++;
                if(AddOneLine(showLLCAP ? RXLL : RXDAT, rxlength, rx_memo_offset, rx_memo_offset_offset, timeGetTime() - start_local))
                    more_line++;
            }
            else
                continue;
        }        
    }

    if(showRAW)
    {
        //show raw data
        while(rxlength)
        {
            rlen = rxlength > 16 ? 16: rxlength;

            if(AddOneLine(RAW_DATA, rlen, rx_memo_offset, rx_memo_offset_offset, timeGetTime() - start_local))
                more_line ++;

            rxlength -= rlen;
            rx_memo_offset_offset += rlen;
        }
    }

    rxlength = 0;
    rxlog_state = 0;
    rxcnt = 0;
    return more_line;
}

static U8 TaxeiaUpdateTxLineBuffer()
{
    U8 tlen;
    U8 more_line;
    U8 i;

    more_line = 0;

    for(i = 0; i < 2; i++)
    {
        if(i == 0)
        {
            if(showHCI && txlog[0] == 1)
            {
                if(AddOneLine(SPACE, 0, 0, 0, 0))
                    more_line++;          
                if(AddOneLine(HCI_CMD_EVT, txlength, tx_memo_offset, tx_memo_offset_offset, timeGetTime() - start_local))
                    more_line ++;
            }
            else
                continue;

        }
        else if(i == 1)
        {
            if(txlog[0] == 2)
            {
                if(AddOneLine(SPACE, 0, 0, 0, 0))
                    more_line++;
                if(AddOneLine(showLLCAP ? TXLL : TXDAT, txlength, tx_memo_offset, tx_memo_offset_offset, timeGetTime() - start_local))
                    more_line ++;
            }
            else
                continue;
        }
        
    }

    if(showRAW)
    {
        //show raw data
        while(txlength)
        {
            tlen = txlength > 16 ? 16: txlength;

            if(AddOneLine(RAW_DATA, tlen, tx_memo_offset, tx_memo_offset_offset, timeGetTime() - start_local))
                more_line ++;

            txlength -= tlen;
            tx_memo_offset_offset += tlen;            
        }
    }
    
    txlength = 0;
    txlog_state = 0;
    txcnt = 0;
    return more_line;
}


void SetLineDisplayOption(U8 HCI, U8 LL2CAP, U8 dat, U8 flush, U16 rfcommcid, U16 rfcommcid2)
{
    showHCI = HCI;
    showLLCAP = LL2CAP;
    showRAW = dat;
    autoFlush = flush;
    showRFComm1 = rfcommcid;
    showRFComm2 = rfcommcid2;
}

U16 GetLineNumber()
{
    return line_number;
}

void ReadDataFromContentFile(DWORD offset, DWORD offset_offset, U16 len, U8 *buf)
{
    U32 read;
    U8 type;
    U16 rlen;
    U8 type_save;
    
    
    if (SetFilePointer(content_file, offset, 0, FILE_BEGIN) == 0xFFFFFFFF) return;

    ReadFile(content_file, &type_save, 1, &read, 0);
    ReadFile(content_file, &rlen, 2, &read, 0);

    while(offset_offset)
    {
        if(rlen > offset_offset)
        {
            SetFilePointer(content_file, offset_offset, 0, FILE_CURRENT);
            rlen -= (U16)offset_offset;
            offset_offset = 0;
        }
        else //< or ==, wee need to move to next portino
        {
            SetFilePointer(content_file, rlen, 0, FILE_CURRENT);
            offset_offset -= rlen;

            do
            {//skip different type direction
                ReadFile(content_file, &type, 1, &read, 0);
                ReadFile(content_file, &rlen, 2, &read, 0);
                if(type != type_save)
                {
                    SetFilePointer(content_file, rlen, 0, FILE_CURRENT);
                }
            }while(type != type_save);
        }
    }
    
    if(rlen > len) rlen = len;

    ReadFile(content_file, buf, rlen, &read, 0);
    
    buf += rlen;
    len -= rlen;

    while(len) 
    {
        ReadFile(content_file, &type, 1, &read, 0);
        ReadFile(content_file, &rlen, 2, &read, 0);
        if(type == type_save)
        {
            if(rlen > len) rlen = len;
            ReadFile(content_file, buf, rlen, &read, 0);
            buf += rlen;
            len -= rlen;
        }
        else
            SetFilePointer(content_file, rlen, 0, FILE_CURRENT);
        
    }
}

void GetLineDisplayData(U32 lineidx, U8 *output, U32 *len, U32 *color)
{
    U16 actual_length;
    BYTE rawdata[1024];
    U32 read;
    const U8 *p;
    U8       *o;
    U32       po;

    if(line_display_type[lineidx] == SPACE) //space
    {
        o = output;
        o+= sprintf(output, "");
    }
    else if(line_display_type[lineidx] == RAW_DATA) //raw data
    {
        actual_length = line_length[lineidx];
       
        ReadDataFromContentFile(line_offset[lineidx], line_offset_offset[lineidx], actual_length, rawdata);
        
        {
            p = rawdata;
            po = 0;
            o = output;

            /* Dumps the packet in basic ASCII dump format */
            po = 0;
            while (po < 16) {
                if (p+po < rawdata+actual_length)
                    o += sprintf(o, "%02x ", p[po]);
                else
                    o += sprintf(o, "   ");
                if (++po == 16/2) o += sprintf(o, "  ");
            }
            o += sprintf(o, "    ");

            po = 0;
            while (po < 16) {
                if (p+po < rawdata+actual_length)
                    o += sprintf(o, "%1c", ((p[po] >= ' ' && p[po] <= '~') ? p[po] : '.'));
                else break;
                po++;
            }
        }
    }
    else //hci cmd/evt/tdata/rdata/tl2/rl2
    {
        DWORD start;
#define PARSE_IDLE 0
#define PARSE_CMD 1
#define PARSE_RDATA 2
#define PARSE_TDATA 3
#define PARSE_EVT 4
#define PARSE_RL2CAP 5
#define PARSE_TL2CAP 6

#define PARSE_CMD_BODY 7
#define PARSE_EVT_BODY 8
#define PARSE_RDATA_BODY 9
#define PARSE_TDATA_BODY 10
#define PARSE_RL2CAP_BODY 11
#define PARSE_TL2CAP_BODY 12

        BYTE state = PARSE_IDLE;
        WORD residue ;
        WORD idx;
        WORD cmd;
        WORD handle;
        BYTE evt;
        WORD cid;
        BYTE l2op;

        start = line_offset[lineidx];


        if(GetFileSize(content_file, NULL) < start) return ;

      //  if (SetFilePointer(content_file, start, 0, FILE_BEGIN) == 0xFFFFFFFF) return ;
      //  if (ReadFile(content_file, rawdata, line_length[lineidx], &read, 0) == FALSE) return ;

        ReadDataFromContentFile(line_offset[lineidx], line_offset_offset[lineidx], line_length[lineidx], rawdata);

        po = 0;
        o = output;
        residue = line_length[lineidx];
        //ASSERT(residue == read);

        do
        {

            switch(state)
            {
                case PARSE_IDLE:
                {
                    ASSERT(po == 0);
                    if(rawdata[0] == 1)
                    {
                        o += sprintf(o, GetTimeStr("HCI-CMD :", line_time[lineidx]));
                        state = PARSE_CMD;
                        *color = 0x000000;       // Black

                    }
                    else if(rawdata[0] == 2) //data
                    {                        
                        if(line_display_type[lineidx] == RXDAT)
                        {
                            o += sprintf(o, GetTimeStr("ACL-RX :", line_time[lineidx]));
                            state = PARSE_RDATA; //rx data
                            *color = 0x000080; //light red
                        }
                        else if(line_display_type[lineidx] == RXLL)
                        {
                            o += sprintf(o, GetTimeStr("L2-RX :", line_time[lineidx]));
                            state = PARSE_RL2CAP; //rx data
                            *color = 0x0000ff; //light red
                        }
                        else if(line_display_type[lineidx] == TXDAT)
                        {
                            o += sprintf(o, GetTimeStr("ACL-TX :", line_time[lineidx]));
                            state = PARSE_TDATA; //rx data
                            *color = 0x008000; //light red
                        }
                        else
                        {
                            ASSERT(line_display_type[lineidx] == TXLL);
                            o += sprintf(o, GetTimeStr("L2-TX :", line_time[lineidx]));
                            state = PARSE_TL2CAP; //rx data
                            *color = 0x00ff00; //light red
                        }
                    }
                    else if(rawdata[0] == 4)
                    {
                        o += sprintf(o, GetTimeStr("HCI-EVT :", line_time[lineidx]));
                        state = PARSE_EVT;
                        *color = 0x800000;       // Blue
                    }
                    po++;
                    read --;
                    break;
                }
                case PARSE_CMD:
                {
                    cmd = (rawdata[po+1] << 8) + rawdata[po];
                    o += sprintf(o, "[%s] ", HciCommandStr(cmd));
                    residue = rawdata[po+2];
                    po+=3;
                    read -=3;
                    state = PARSE_CMD_BODY;
                    idx = 0;
                    break;
                }
                case PARSE_EVT:
                {
                    evt = rawdata[po];
                    residue = rawdata[po+1];
                    o += sprintf(o, "[%s] ", HciEventStr(evt));

                    po+=2;
                    read -=2;
                    state = PARSE_EVT_BODY;
                    idx = 0;
                    break;
                }
                case PARSE_RDATA:
                case PARSE_TDATA:
                {
                    ASSERT(po == 1);

                    handle = (rawdata[1] + ((rawdata[2] & 0xf) << 8));
                    residue = rawdata[3] + (rawdata[4] << 8);
                    o += sprintf(o, "[Hdl:0x%03x] PB:%1x BC:%1x Len:%d ", handle, (rawdata[2] >> 4), (rawdata[2] >> 6),
                                 residue);
                    po += 4;
                    read -= 4;
                    state = PARSE_RDATA_BODY;
                    idx = 0;
                    break;
                }
                case PARSE_RL2CAP:
                case PARSE_TL2CAP:
                {
                    ASSERT(po == 1);
                    if((rawdata[2] >> 4) == 0x1) //continue packet, no l2cap cid
                    {
                        handle = (rawdata[1] + ((rawdata[2] & 0xf) << 8));
                        residue = rawdata[3] + (rawdata[4] << 8);
                        o += sprintf(o, "[Continue Data: Hdl:0x%03x] ", handle);

                        cid = 0x00;
                        po += 4;
                        read -= 4;
                    }
                    else
                    {
                        residue = (rawdata[3] + (rawdata[4] << 8)) - 4; //acl length - 4
                        cid = rawdata[7] + (rawdata[8] << 8);
                        l2op = rawdata[9];

                        if(cid == 0x0001) //EDR signaling
                            o += sprintf(o, "[signaling cid:0x%04x] [%s] ", cid, L2CAPSignalOpcodeStr(l2op));
                        else
                            o += sprintf(o, "[allocated cid:0x%04x] Len:%d", cid, (rawdata[5] + (rawdata[6] << 8)));

                        po+= 8;
                        read -= 8;
                    }
                    state = PARSE_RL2CAP_BODY;
                    idx = 0;
                    break;
                }
                case PARSE_CMD_BODY:
                {

                    o += HciCommandParse(o, cmd, idx++, rawdata[po++]);
                    read--;
                    residue --;
                    break;
                }
                case PARSE_EVT_BODY:
                {
                    o += HciEventParse(o, evt, idx++, rawdata[po++]);
                    read--;
                    residue --;
                    break;
                }
                case PARSE_RDATA_BODY:
                case PARSE_TDATA_BODY:
                {
                    o += HciDataParse(o, idx++, rawdata[po++]);
                    read --;
                    residue --;
                    break;
                }
                case PARSE_RL2CAP_BODY:
                case PARSE_TL2CAP_BODY:
                {
                    o += HciL2Parse(o, cid, idx++, rawdata[po++]);
                    read --;
                    residue --;
                }
                break;
            }
/*
            if(read == 0)
            {
                ASSERT(residue == read);
            }
            */
        } while(residue);
        state = PARSE_IDLE;
    }

    *len = o - output;
}

U8 updatelinebuffer()
{
#define STATE_TYPE 0
#define STATE_HEADER 1
#define STATE_BODY 2

    U8 temp[1024];
    U8 type;
    U16 len;
    U8 *ptr;
    DWORD read;
    U8 more_line;
    U32 save_offset;

    more_line = 0;

    SetFilePointer(content_file, content_file_parse_offset, 0, FILE_BEGIN);

    save_offset = content_file_parse_offset;
    
    //type
    ASSERT(ReadFile(content_file, &type, 1, &read, 0));

    //len
    ASSERT(ReadFile(content_file, &len, 2, &read, 0));
    ASSERT(read == 2);
    
    content_file_parse_offset+= 3/*1+2*/;
    
    //content
    ASSERT(ReadFile(content_file, temp, len, &read, 0));
    ASSERT(read == len);
    content_file_parse_offset += read;
    
    ptr = temp;

    while(read)
    {

        if(type == 0x55) //tx
        {
            {
                //fill into txlog buffer
                switch(txlog_state)
                {
                    case STATE_TYPE: //type
                        ASSERT(txcnt == 0);

                        ASSERT(*ptr == 1 || *ptr == 2);//cmd or data

                        txlog[txcnt++] = *ptr++;
                        txlog_state = STATE_HEADER; //header
                        tx_memo_offset = save_offset;
                        tx_memo_offset_offset = (len - (U16)read);
                        read --;
                        break;
                    case STATE_HEADER:
                        txlog[txcnt++] = *ptr++;
                        read--;
                        if(txlog[0] == 1) //cmd
                        {
                            if(txcnt == 4) //type*1+op*2+length*1
                            {
                                txlog_state = 2;
                                txlength = txlog[3] + 4;

                                if(txlength == txcnt) //no data
                                {
                                    more_line += TaxeiaUpdateTxLineBuffer();
                                }
                            }
                        }
                        else if(txlog[0] == 2) //data
                        {
                            if(txcnt == 5) //type*1+op*2+length*1
                            {
                                txlog_state = 2;
                                txlength = txlog[3] + (txlog[4] << 8)  + 5;
                                if(txlength == txcnt) //no data
                                {
                                    more_line += TaxeiaUpdateTxLineBuffer();
                                }
                            }
                        }
                        break;
                    case STATE_BODY:
                        txlog[txcnt++] = *ptr++;
                        read--;
                        if(txlength == txcnt)
                        {
                            more_line += TaxeiaUpdateTxLineBuffer();
                        }
                        break;
                }
            }
        }
        else if(type == 0xAA) //rx
        {
            {
                //fill into txlog buffer
                switch(rxlog_state)
                {
                    case STATE_TYPE: //type
                        ASSERT(rxcnt == 0);

                        ASSERT(*ptr == 4 || *ptr == 2);//event or data

                        rxlog[rxcnt++] = *ptr++;
                        rxlog_state = STATE_HEADER; //header
                        rx_memo_offset = save_offset;
                        rx_memo_offset_offset = (len - (U16)read);                        
                        read --;
                        break;
                    case STATE_HEADER:
                        rxlog[rxcnt++] = *ptr++;
                        read--;
                        if(rxlog[0] == 4) //event
                        {
                            if(rxcnt == 3) //type*1+event*1+length*1
                            {
                                rxlog_state = 2;
                                rxlength = rxlog[2] + 3;

                                if(rxlength == rxcnt) //no data
                                {
                                    more_line += TaxeiaUpdateRxLineBuffer();
                                }
                            }
                        }
                        else if(rxlog[0] == 2) //data
                        {
                            if(rxcnt == 5) //type*1+op*2+length*1
                            {
                                rxlog_state = 2;
                                rxlength = rxlog[3] + (rxlog[4] << 8)  + 5;
                                if(rxlength == rxcnt) //no data
                                {
                                    more_line += TaxeiaUpdateRxLineBuffer();
                                }
                            }
                        }
                        break;
                    case STATE_BODY:
                        rxlog[rxcnt++] = *ptr++;
                        read--;
                        if(rxlength == rxcnt) //no data
                        {
                            more_line += TaxeiaUpdateRxLineBuffer();
                        }
                        break;
                }
            }
        }
        else
            ASSERT(type == 0xAA || type == 0x55);
    }

    return more_line;
}


void OpenLog()
{
    U32 di;
    char *name;


    while (content_file == INVALID_HANDLE_VALUE) {
        name = _tempnam(0, "TxiLog");
        strcpy(content_filename, name);
        strcat(content_filename, ".tmp");
        free(name);

        /* This Win32 call is similar to fopen() but with more control. */
        content_file = CreateFileA(content_filename, GENERIC_READ|GENERIC_WRITE,
                                  FILE_SHARE_READ, 0, CREATE_ALWAYS,
                                  FILE_ATTRIBUTE_NORMAL|FILE_FLAG_RANDOM_ACCESS, 0);
    }

    for(di = 0; di < 1024; di++)
    {
        line_length[di] = 0;
        line_offset[di] = 0;
        line_display_type[di] = 0;
        line_number = 0;
        line_time[di] = 0;
    }

    content_file_parse_offset = 0;
    start_local = timeGetTime();
    
}

U8 AppendLogEntry(U8 type, U8 *ptr, U16 len)
{
    DWORD    written;

    if(len == 0) return 0;
    SetFilePointer(content_file, 0, 0, FILE_END);

    WriteFile(content_file, &type, 1, &written, 0);
    WriteFile(content_file, &len, 2, &written, 0);
    WriteFile(content_file, ptr, len, &written, 0);
    ASSERT(len == written);

    return updatelinebuffer();
}

void CloseLog()
{
    U16 di;
    if(content_file != INVALID_HANDLE_VALUE)
    {
        CloseHandle(content_file);
    }
  
    content_file = INVALID_HANDLE_VALUE;

    for(di = 0; di < 1024; di++)
    {
        line_length[di] = 0;
        line_offset[di] = 0;
        line_display_type[di] = 0;
        line_number = 0;
        line_time[di] = 0;
    }

    content_file_parse_offset = 0;
    start_local = timeGetTime();    
}

DWORD CopyLog(char *file)
{
    HANDLE fptr;
    DWORD tmp;
    DWORD written;
    DWORD size;
    
    BYTE buf[256];
    BYTE tag[4] = {0x20, 0x12, 0x11, 0x18};

    fptr = CreateFileA(file, GENERIC_WRITE,
                          FILE_SHARE_READ, 0, CREATE_ALWAYS,
                          FILE_ATTRIBUTE_NORMAL|FILE_FLAG_RANDOM_ACCESS, 0);
    //write tag
    WriteFile(fptr, tag, 4, &written, 0);

    //write content file
    size = GetFileSize(content_file, &tmp);
    WriteFile(fptr, &size, 4, &written, 0);
    SetFilePointer(content_file, 0, 0, FILE_BEGIN);
    while(ReadFile(content_file, buf, 256, &size, 0))
    {
        if(size == 0)
            break;
        WriteFile(fptr, buf, size, &written, 0);
    }
    

    
    CloseHandle(fptr);
    return 1;
}

void ReParseLog(DWORD parse_end, DWORD *more_line)
{
    DWORD di;
    for(di = 0; di < 1024; di++)
    {
        line_length[di] = 0;
        line_offset[di] = 0;
        line_display_type[di] = 0;
        line_number = 0;
        line_time[di] = 0;
    }

    if(parse_end == -1)
        parse_end = content_file_parse_offset; //current end
    content_file_parse_offset = 0;
    SetFilePointer(content_file, 0, 0, FILE_BEGIN);    
    *more_line = 0;
    while(content_file_parse_offset != parse_end)
    {
        *more_line += updatelinebuffer();
    }

}

DWORD ImportLog(char *file, DWORD *more_line)
{
    HANDLE fptr;
    DWORD read, tmp, fsize;
    DWORD csize;
    BYTE buf[256];
    DWORD written;
    
    fptr = CreateFileA(file, GENERIC_READ,
                           0, 0, OPEN_EXISTING,
                          FILE_ATTRIBUTE_NORMAL|FILE_FLAG_RANDOM_ACCESS, 0);

    fsize = GetFileSize(fptr, &tmp);
    
    if(fptr == INVALID_HANDLE_VALUE) return 1;

    //check tag
    ReadFile(fptr, buf, 4, &read, 0);
    if(buf[0] != 0x20 || buf[1] != 0x12 || buf[2] != 0x11 || buf[3] != 0x18)
    {
        CloseHandle(fptr);
        return 2;
    }

    ReadFile(fptr, &csize, 4, &read, 0);

    if(fsize != (csize + 8))
    {
        CloseHandle(fptr);
        return 3;
    }
    
    //creat log files
    CloseLog();
    OpenLog();

    //copy onto content file
    while(csize)
    {
        tmp = csize > 256 ? 256 : csize;
        
        ReadFile(fptr, buf, tmp, &read, 0);
        WriteFile(content_file, buf, read, &written, 0);

        csize -= tmp;
    }
    CloseHandle(fptr);

    ReParseLog(fsize - 8, more_line);
    
    return 0;
}

#endif


