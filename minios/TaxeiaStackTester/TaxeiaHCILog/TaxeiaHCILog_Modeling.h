#ifndef __TAXEIAHCILOG_MODEL_HH_
#define __TAXEIAHCILOG_MODEL_HH_
void GetLineDisplayData(U32 lineidx, U8 *output, U32 *len, U32 *color);

void OpenLog();
U8 AppendLogEntry(U8 type, U8 *ptr, U16 len);
U8 IsLineFull();

void CloseLog();
U32 CopyLog(char *file);
U32 ImportLog(char *file, U32 *more_line); 
void ReParseLog(U32 parse_end, U32 *more_line);
    
//set style
void SetLineDisplayOption(U8 HCI, U8 LL2CAP, U8 dat, U8 flush, U16 rfcommcid, U16 rfcommcid2);

//UI get data
U16 GetLineNumber();


#endif
