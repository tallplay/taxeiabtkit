//#include "common.h"
#include "aes.h"
#include <string.h>

/* --- Calculated parameters follow, do not change --- */

//! AES state block size in number of bytes.
#define BLOCK_SIZE              16

//! S-Box lookup table.
static const U8 sBox[] =
{
    0x63, 0x7c, 0x77, 0x7b, 0xf2, 0x6b, 0x6f, 0xc5,
    0x30, 0x01, 0x67, 0x2b, 0xfe, 0xd7, 0xab, 0x76,
    0xca, 0x82, 0xc9, 0x7d, 0xfa, 0x59, 0x47, 0xf0,
    0xad, 0xd4, 0xa2, 0xaf, 0x9c, 0xa4, 0x72, 0xc0,
    0xb7, 0xfd, 0x93, 0x26, 0x36, 0x3f, 0xf7, 0xcc,
    0x34, 0xa5, 0xe5, 0xf1, 0x71, 0xd8, 0x31, 0x15,
    0x04, 0xc7, 0x23, 0xc3, 0x18, 0x96, 0x05, 0x9a,
    0x07, 0x12, 0x80, 0xe2, 0xeb, 0x27, 0xb2, 0x75,
    0x09, 0x83, 0x2c, 0x1a, 0x1b, 0x6e, 0x5a, 0xa0,
    0x52, 0x3b, 0xd6, 0xb3, 0x29, 0xe3, 0x2f, 0x84,
    0x53, 0xd1, 0x00, 0xed, 0x20, 0xfc, 0xb1, 0x5b,
    0x6a, 0xcb, 0xbe, 0x39, 0x4a, 0x4c, 0x58, 0xcf,
    0xd0, 0xef, 0xaa, 0xfb, 0x43, 0x4d, 0x33, 0x85,
    0x45, 0xf9, 0x02, 0x7f, 0x50, 0x3c, 0x9f, 0xa8,
    0x51, 0xa3, 0x40, 0x8f, 0x92, 0x9d, 0x38, 0xf5,
    0xbc, 0xb6, 0xda, 0x21, 0x10, 0xff, 0xf3, 0xd2,
    0xcd, 0x0c, 0x13, 0xec, 0x5f, 0x97, 0x44, 0x17,
    0xc4, 0xa7, 0x7e, 0x3d, 0x64, 0x5d, 0x19, 0x73,
    0x60, 0x81, 0x4f, 0xdc, 0x22, 0x2a, 0x90, 0x88,
    0x46, 0xee, 0xb8, 0x14, 0xde, 0x5e, 0x0b, 0xdb,
    0xe0, 0x32, 0x3a, 0x0a, 0x49, 0x06, 0x24, 0x5c,
    0xc2, 0xd3, 0xac, 0x62, 0x91, 0x95, 0xe4, 0x79,
    0xe7, 0xc8, 0x37, 0x6d, 0x8d, 0xd5, 0x4e, 0xa9,
    0x6c, 0x56, 0xf4, 0xea, 0x65, 0x7a, 0xae, 0x08,
    0xba, 0x78, 0x25, 0x2e, 0x1c, 0xa6, 0xb4, 0xc6,
    0xe8, 0xdd, 0x74, 0x1f, 0x4b, 0xbd, 0x8b, 0x8a,
    0x70, 0x3e, 0xb5, 0x66, 0x48, 0x03, 0xf6, 0x0e,
    0x61, 0x35, 0x57, 0xb9, 0x86, 0xc1, 0x1d, 0x9e,
    0xe1, 0xf8, 0x98, 0x11, 0x69, 0xd9, 0x8e, 0x94,
    0x9b, 0x1e, 0x87, 0xe9, 0xce, 0x55, 0x28, 0xdf,
    0x8c, 0xa1, 0x89, 0x0d, 0xbf, 0xe6, 0x42, 0x68,
    0x41, 0x99, 0x2d, 0x0f, 0xb0, 0x54, 0xbb, 0x16
};

static const U8 sBoxInv[] = {
    0x52, 0x09, 0x6a, 0xd5, 0x30, 0x36, 0xa5, 0x38,
    0xbf, 0x40, 0xa3, 0x9e, 0x81, 0xf3, 0xd7, 0xfb,
    0x7c, 0xe3, 0x39, 0x82, 0x9b, 0x2f, 0xff, 0x87,
    0x34, 0x8e, 0x43, 0x44, 0xc4, 0xde, 0xe9, 0xcb,
    0x54, 0x7b, 0x94, 0x32, 0xa6, 0xc2, 0x23, 0x3d,
    0xee, 0x4c, 0x95, 0x0b, 0x42, 0xfa, 0xc3, 0x4e,
    0x08, 0x2e, 0xa1, 0x66, 0x28, 0xd9, 0x24, 0xb2,
    0x76, 0x5b, 0xa2, 0x49, 0x6d, 0x8b, 0xd1, 0x25,
    0x72, 0xf8, 0xf6, 0x64, 0x86, 0x68, 0x98, 0x16,
    0xd4, 0xa4, 0x5c, 0xcc, 0x5d, 0x65, 0xb6, 0x92,
    0x6c, 0x70, 0x48, 0x50, 0xfd, 0xed, 0xb9, 0xda,
    0x5e, 0x15, 0x46, 0x57, 0xa7, 0x8d, 0x9d, 0x84,
    0x90, 0xd8, 0xab, 0x00, 0x8c, 0xbc, 0xd3, 0x0a,
    0xf7, 0xe4, 0x58, 0x05, 0xb8, 0xb3, 0x45, 0x06,
    0xd0, 0x2c, 0x1e, 0x8f, 0xca, 0x3f, 0x0f, 0x02,
    0xc1, 0xaf, 0xbd, 0x03, 0x01, 0x13, 0x8a, 0x6b,
    0x3a, 0x91, 0x11, 0x41, 0x4f, 0x67, 0xdc, 0xea,
    0x97, 0xf2, 0xcf, 0xce, 0xf0, 0xb4, 0xe6, 0x73,
    0x96, 0xac, 0x74, 0x22, 0xe7, 0xad, 0x35, 0x85,
    0xe2, 0xf9, 0x37, 0xe8, 0x1c, 0x75, 0xdf, 0x6e,
    0x47, 0xf1, 0x1a, 0x71, 0x1d, 0x29, 0xc5, 0x89,
    0x6f, 0xb7, 0x62, 0x0e, 0xaa, 0x18, 0xbe, 0x1b,
    0xfc, 0x56, 0x3e, 0x4b, 0xc6, 0xd2, 0x79, 0x20,
    0x9a, 0xdb, 0xc0, 0xfe, 0x78, 0xcd, 0x5a, 0xf4,
    0x1f, 0xdd, 0xa8, 0x33, 0x88, 0x07, 0xc7, 0x31,
    0xb1, 0x12, 0x10, 0x59, 0x27, 0x80, 0xec, 0x5f,
    0x60, 0x51, 0x7f, 0xa9, 0x19, 0xb5, 0x4a, 0x0d,
    0x2d, 0xe5, 0x7a, 0x9f, 0x93, 0xc9, 0x9c, 0xef,
    0xa0, 0xe0, 0x3b, 0x4d, 0xae, 0x2a, 0xf5, 0xb0,
    0xc8, 0xeb, 0xbb, 0x3c, 0x83, 0x53, 0x99, 0x61,
    0x17, 0x2b, 0x04, 0x7e, 0xba, 0x77, 0xd6, 0x26,
    0xe1, 0x69, 0x14, 0x63, 0x55, 0x21, 0x0c, 0x7d
};



//! Lower 8 bits of AES polynomial (x^8+x^4+x^3+x+1), ie. (x^4+x^3+x+1).
#define BPOLY                       0x1b

//! schedule key address in EEPROM

/**
 * Cycle a 4-byte array left once.
 *
 * @param Row pointer of 4-byte array which want to cycle left once
 */
static void CycleLeft(U8 *Row)
{
    U8 Temp;

    Temp = Row[0];
    Row[0] = Row[1];
    Row[1] = Row[2];
    Row[2] = Row[3];
    Row[3] = Temp;
} // end of CycleLeft


/**
 * Perform an AES column mix operation on the 4 bytes in 'column' buffer.
 *
 * @param Column column buffer pointer which want to calculated
 */
static void MixColumn(U8 *Column)
{
    U8 Result0, Result1, Result2, Result3;
    U8 Column0, Column1, Column2, Column3;

    // This generates more effective code, at least
    // with the IAR C compiler.
    Column0 = Column[0];
    Column1 = Column[1];
    Column2 = Column[2];
    Column3 = Column[3];

    // Partial sums (modular addition using XOR).
    Result0 = Column1 ^ Column2 ^ Column3;
    Result1 = Column0 ^ Column2 ^ Column3;
    Result2 = Column0 ^ Column1 ^ Column3;
    Result3 = Column0 ^ Column1 ^ Column2;

    // Multiply column bytes by 2 modulo BPOLY.
    if(Column0 & 0x80)
    {
        Column0 = (Column0 << 1) ^ BPOLY;
    }
    else
    {
        Column0 = (Column0 << 1);
    }

    if(Column1 & 0x80)
    {
        Column1 = (Column1 << 1) ^ BPOLY;
    }
    else
    {
        Column1 = (Column1 << 1);
    }

    if(Column2 & 0x80)
    {
        Column2 = (Column2 << 1) ^ BPOLY;
    }
    else
    {
        Column2 = (Column2 << 1);
    }

    if(Column3 & 0x80)
    {
        Column3 = (Column3 << 1) ^ BPOLY;
    }
    else
    {
        Column3 = (Column3 << 1);
    }

    // Final sums stored into original column bytes.
    Column[0] = Result0 ^ Column0 ^ Column1;
    Column[1] = Result1 ^ Column1 ^ Column2;
    Column[2] = Result2 ^ Column2 ^ Column3;
    Column[3] = Result3 ^ Column0 ^ Column3;
} // end of MixColumn



//! Perform an AES inverse column mix operation on the 4 bytes in 'column' buffer.
static void invMixColumn( U8 * column )
{
    U8 result0, result1, result2, result3;
    U8 column0, column1, column2, column3;

    // This generates more effective code, at least
    // with the IAR C compiler.
    column0 = column[0];
    column1 = column[1];
    column2 = column[2];
    column3 = column[3];

    // Partial sums (modular addition using XOR).
    result0 = column1 ^ column2 ^ column3;
    result1 = column0 ^ column2 ^ column3;
    result2 = column0 ^ column1 ^ column3;
    result3 = column0 ^ column1 ^ column2;

    // Multiply column bytes by 2 modulo BPOLY.
    if( column0 & 0x80 ) {
        column0 = (column0 << 1) ^ BPOLY;
    } else {
        column0 = (column0 << 1);
    }
    if( column1 & 0x80 ) {
        column1 = (column1 << 1) ^ BPOLY;
    } else {
        column1 = (column1 << 1);
    }
    if( column2 & 0x80 ) {
        column2 = (column2 << 1) ^ BPOLY;
    } else {
        column2 = (column2 << 1);
    }
    if( column3 & 0x80 ) {
        column3 = (column3 << 1) ^ BPOLY;
    } else {
        column3 = (column3 << 1);
    }

    // More partial sums.
    result0 ^= column0 ^ column1;
    result1 ^= column1 ^ column2;
    result2 ^= column2 ^ column3;
    result3 ^= column0 ^ column3;

    // Multiply by 2.
    if( column0 & 0x80 ) {
        column0 = (column0 << 1) ^ BPOLY;
    } else {
        column0 = (column0 << 1);
    }
    if( column1 & 0x80 ) {
        column1 = (column1 << 1) ^ BPOLY;
    } else {
        column1 = (column1 << 1);
    }
    if( column2 & 0x80 ) {
        column2 = (column2 << 1) ^ BPOLY;
    } else {
        column2 = (column2 << 1);
    }
    if( column3 & 0x80 ) {
        column3 = (column3 << 1) ^ BPOLY;
    } else {
        column3 = (column3 << 1);
    }

    // More partial sums.
    result0 ^= column0 ^ column2;
    result1 ^= column1 ^ column3;
    result2 ^= column0 ^ column2;
    result3 ^= column1 ^ column3;

    // Multiply by 2.
    if( column0 & 0x80 ) {
        column0 = (column0 << 1) ^ BPOLY;
    } else {
        column0 = (column0 << 1);
    }
    if( column1 & 0x80 ) {
        column1 = (column1 << 1) ^ BPOLY;
    } else {
        column1 = (column1 << 1);
    }
    if( column2 & 0x80 ) {
        column2 = (column2 << 1) ^ BPOLY;
    } else {
        column2 = (column2 << 1);
    }
    if( column3 & 0x80 ) {
        column3 = (column3 << 1) ^ BPOLY;
    } else {
        column3 = (column3 << 1);
    }

    // Final partial sum.
    column0 ^= column1 ^ column2 ^ column3;

    // Final sums stored indto original column bytes.
    column[0] = result0 ^ column0;
    column[1] = result1 ^ column0;
    column[2] = result2 ^ column0;
    column[3] = result3 ^ column0;
}


/**
 * Perform AES column mixing for the whole AES block/state.
 *
 * @param State block/state pointer which want to calculated
 */
static void MixColumns(U8 *State)
{
    MixColumn(State + 0*4);
    MixColumn(State + 1*4);
    MixColumn(State + 2*4);
    MixColumn(State + 3*4);
} // end of MixColumns




//! Perform AES inverse column mixing for the whole AES block/state.
static void invMixColumns( U8 * state )
{
    invMixColumn( state + 0*4 );
    invMixColumn( state + 1*4 );
    invMixColumn( state + 2*4 );
    invMixColumn( state + 3*4 );
}


/**
 * Substitute 'count' bytes in the 'bytes' buffer in SRAM using the S-Box.
 *
 * @param Bytes pointer of SRAM which want to be subsituted
 * @param Count # of bytes want to be subsituted
 */
static void SubBytes(U8 *Bytes,
                     U8 Count)
{
    U8 i, Data;

    for(i = 0; i < Count; i++)
    {
        Data = *(Bytes + i);
#ifdef __GNUC__                          // AVRGCC does not support point to PGM space
        *(Bytes + i) = pgm_read_byte_near(&(sBox[Data]));    // Substitute every byte in state.
#else
        *(Bytes + i) = sBox[Data];    // Substitute every byte in state.
#endif
    }
} // end of SubBytes

static void invSubBytes(U8 *Bytes,
                     U8 Count)
{
    U8 i, Data;

    for(i = 0; i < Count; i++)
    {
        Data = *(Bytes + i);
#ifdef __GNUC__                          // AVRGCC does not support point to PGM space
        *(Bytes + i) = pgm_read_byte_near(&(sBoxInv[Data]));    // Substitute every byte in state.
#else
        *(Bytes + i) = sBoxInv[Data];    // Substitute every byte in state.
#endif
    }
} // end of SubBytes


/**
 * Perform AES shift rows operation for the whole AES block/state.
 *
 * @param State block/state pointer which want to calculated
 */
static void ShiftRows(U8 *State)
{
    U8 Temp;

    // Note: State is arranged column by column.

    // Cycle second row left one time.
    Temp = State[ 1 + 0*4 ];
    State[ 1 + 0*4 ] = State[ 1 + 1*4 ];
    State[ 1 + 1*4 ] = State[ 1 + 2*4 ];
    State[ 1 + 2*4 ] = State[ 1 + 3*4 ];
    State[ 1 + 3*4 ] = Temp;

    // Cycle third row left two times.
    Temp = State[ 2 + 0*4 ];
    State[ 2 + 0*4 ] = State[ 2 + 2*4 ];
    State[ 2 + 2*4 ] = Temp;

    Temp = State[ 2 + 1*4 ];
    State[ 2 + 1*4 ] = State[ 2 + 3*4 ];
    State[ 2 + 3*4 ] = Temp;

    // Cycle fourth row left three times, ie. right once.
    Temp = State[ 3 + 3*4 ];
    State[ 3 + 3*4 ] = State[ 3 + 2*4 ];
    State[ 3 + 2*4 ] = State[ 3 + 1*4 ];
    State[ 3 + 1*4 ] = State[ 3 + 0*4 ];
    State[ 3 + 0*4 ] = Temp;
} // end of ShiftRows

static void invShiftRows(U8 *State)
{
    U8 Temp;

    // Note: State is arranged column by column.

    // Cycle second row right one time.
    Temp = State[ 1 + 3*4 ];
    State[ 1 + 3*4 ] = State[ 1 + 2*4 ];
    State[ 1 + 2*4 ] = State[ 1 + 1*4 ];
    State[ 1 + 1*4 ] = State[ 1 + 0*4 ];
    State[ 1 + 0*4 ] = Temp;

    // Cycle third row right two times.
    Temp = State[ 2 + 2*4 ];
    State[ 2 + 2*4 ] = State[ 2 + 0*4 ];
    State[ 2 + 0*4 ] = Temp;

    Temp = State[ 2 + 3*4 ];
    State[ 2 + 3*4 ] = State[ 2 + 1*4 ];
    State[ 2 + 1*4 ] = Temp;

    // Cycle fourth row three times, ie. left once.
    Temp = State[ 3 + 0*4 ];
    State[ 3 + 0*4 ] = State[ 3 + 1*4 ];
    State[ 3 + 1*4 ] = State[ 3 + 2*4 ];
    State[ 3 + 2*4 ] = State[ 3 + 3*4 ];
    State[ 3 + 3*4 ] = Temp;
} // end of ShiftRows

/**
 * XOR 'count' bytes from 'constant' buffer into 'bytes' buffer in SRAM.
 *
 * @param Bytes    SRAM pointer of operand
 * @param Constant SRAM pointer of operator
 * @param Count    # of byte(s) want to be calculated
 */
static void XORConstant(U8 *Bytes,
                        U8 *Constant,
                        U8 Count)
{
    U8 i, Data;

    // Copy to temporary variables for optimization.
    for(i = 0; i < Count; i++)
    {
        Data = *(Bytes + i) ^ *(Constant + i);
        *(Bytes + i) = Data;
    }
} // end of XORConstant

// Precalculate AES key schedule from given cipher key.

static void XORFEESubstitute(U8 *Bytes,
                                               U8 *fakeEE,
                                               U8 Count)
{
    U8 Value, Data, i;
    // Copy to temporary variables for optimization.

    for(i = 0; i < Count; i++)
    {
        Data = *(Bytes + i) ^ *(fakeEE + i);
#ifdef __GNUC__                          // AVRGCC does not support point to PGM space
        Value = pgm_read_byte_near(&(sBox[Data]));    // Substitute every byte in state.
#else
        Value = sBox[Data];    // Substitute every byte in state.
#endif
        *(Bytes + i) = Value;
    }
}

static void XORFEE(U8 *Bytes, U8 *fakeEE, U8 Count)
{
    U8 Value, i;
    // Copy to temporary variables for optimization.

    for(i = 0; i < Count; i++)
    {
        Value = *(Bytes + i) ^ *(fakeEE + i);
        *(Bytes + i) = Value;
    }
}


void AES_GetDcryptRoundKey(U8 keysize, U8 *key, U8 ridx, U8 *roundkey)
{
    U8  SchedulePos; // Current position inside schedule.
    U8  Temp[4]; // Temporary word when expanding the key.
    U8  RoundConstant[4] = {0x01, 0x00, 0x00, 0x00};
    U16 Addr = 0;
    U8  schedule_size;
    U8  EEPROM[32];
    U8  round  = keysize / 16;
    
    if(keysize == 16)
        schedule_size = 11 * 16;
    else
        schedule_size = 15 * 16;

    memcpy(EEPROM, key, keysize);
    // Copy entire key to start of schedule.    
    Addr = 0;
    
    // Copy last 4 bytes of key to temp word.
    memcpy(Temp, (EEPROM+keysize) - 4, 4 );

    // Expand key into schedule buffer 1 first.
    SchedulePos = keysize;
    while(SchedulePos < schedule_size)
    {
        if((SchedulePos % BLOCK_SIZE) == 0)
        {
           //begin of "round"th round
           if(ridx < round)
           {
            if(keysize == 32)
                memcpy(roundkey, EEPROM + (BLOCK_SIZE * (ridx & 1)), BLOCK_SIZE);
            else
                memcpy(roundkey, EEPROM, BLOCK_SIZE);
            return;
           }
           
           round++;
        }
        
        // Multiple of key size?
        if((SchedulePos % keysize) == 0)
        {
            CycleLeft(Temp); // Cycle left one byte.
            SubBytes(Temp, 4); // Substitute each byte.
            XORConstant(Temp, RoundConstant, 4); // Add to temp.

            // Modular doubling of round constant's first byte.
            if(RoundConstant[0] & 0x80)
            {
                RoundConstant[0] = RoundConstant[0] << 1;
                RoundConstant[0] = RoundConstant[0] ^ BPOLY;
            }
            else
            {
                RoundConstant[0] = RoundConstant[0] << 1;
            }
            
            //^-^
            Addr = 0;
        }
		else if((SchedulePos % keysize) == BLOCK_SIZE ) 
		{ //for AES256, AES128 never get into it
			SubBytes( Temp, 4 ); // Substitute each byte.
		}
        
        // Add with data KEY_SIZE backwards in schedule.
        //^-^ XORConstantFromEEPROM(Temp, Addr - KEY_SIZE, 4);        
        XORFEE(Temp, EEPROM + Addr, 4);
        //^-^ memcpyToEEPROM(Addr, Temp, 4);
        memcpy(EEPROM + Addr, Temp, 4);
        
        Addr += 4;
        SchedulePos += 4;
    }

    memcpy(roundkey, EEPROM, BLOCK_SIZE);
}

void AES_DeCipherOnTheFly(U8 keysize, U8 *Key, U8 *Block)
{
    U8 round;
    U8 roundkey[16];

    if(keysize == 32)
        round = 14;
    else
        round = 10;
 
    //first round
    AES_GetDcryptRoundKey(keysize, Key, round--, roundkey);
    XORFEE(Block, roundkey, BLOCK_SIZE);
    invShiftRows(Block);
    invSubBytes(Block, BLOCK_SIZE);

    //middle round
    for(; round > 0; round--)
    {
       AES_GetDcryptRoundKey(keysize, Key, round, roundkey);
       XORFEE(Block, roundkey, BLOCK_SIZE);
       invMixColumns(Block);
       invShiftRows(Block);
       invSubBytes(Block, BLOCK_SIZE);
    }

    //last round
    AES_GetDcryptRoundKey(keysize, Key, 0, roundkey);
    XORFEE(Block, roundkey, BLOCK_SIZE);
}

void AES_CipherOnTheFly(U8 keysize, U8 *Key, U8 *Block)
{
    U8  SchedulePos; // Current position inside schedule.
    U8  Temp[4]; // Temporary word when expanding the key.
    U8  RoundConstant[4] = {0x01, 0x00, 0x00, 0x00};
    U16 Addr = 0;
    U8  EEPROM[32]; //AES256 is the biggist
    U8  schedule_size;
    if(keysize == 16)
        schedule_size = 11 * 16;
    else
        schedule_size = 15 * 16;
        
    // Copy entire key to start of schedule.

    //^-^ memcpyToEEPROM(Addr, Key, KEY_SIZE);
    memcpy(EEPROM, Key, keysize);
    
    //^-^ Addr += KEY_SIZE;
    Addr = 0;
    
    // Copy last 4 bytes of key to temp word.
    memcpy(Temp, (EEPROM+keysize) - 4, 4 );

    // Expand key into schedule buffer 1 first.
    SchedulePos = keysize;
    while(SchedulePos < schedule_size)
    {
        // Multiple of key size?
        if((SchedulePos % keysize) == 0)
        {            
            CycleLeft(Temp); // Cycle left one byte.
            SubBytes(Temp, 4); // Substitute each byte.
            XORConstant(Temp, RoundConstant, 4); // Add to temp.

            // Modular doubling of round constant's first byte.
            if(RoundConstant[0] & 0x80)
            {
                RoundConstant[0] = RoundConstant[0] << 1;
                RoundConstant[0] = RoundConstant[0] ^ BPOLY;
            }
            else
            {
                RoundConstant[0] = RoundConstant[0] << 1;
            }
            
            //^-^
            Addr = 0;
        }
		else if((SchedulePos % keysize) == BLOCK_SIZE ) 
		{ //for AES256, AES128 never get into it
			SubBytes( Temp, 4 ); // Substitute each byte.
		}
        

        //do cipher begin***********************************
        if((SchedulePos % BLOCK_SIZE) == 0)
        {
            if(SchedulePos == schedule_size - BLOCK_SIZE)
            {
                XORFEESubstitute(Block, EEPROM + Addr, BLOCK_SIZE);
                ShiftRows(Block);
            }
            else
            {
                XORFEESubstitute(Block, EEPROM + Addr, BLOCK_SIZE);
                ShiftRows(Block);
                MixColumns(Block);
            }
        }
        //do cipher end*************************************

        // Add with data KEY_SIZE backwards in schedule.
        //^-^ XORConstantFromEEPROM(Temp, Addr - KEY_SIZE, 4);        
        XORFEE(Temp, EEPROM + Addr, 4);
        //^-^ memcpyToEEPROM(Addr, Temp, 4);
        memcpy(EEPROM + Addr, Temp, 4);
        
        Addr += 4;
        SchedulePos += 4;
    }

    if(keysize == 32)  //one more, because we behind two in the begging, tallplay
	{
        MixColumns(Block);
        XORFEESubstitute(Block, EEPROM + BLOCK_SIZE, BLOCK_SIZE);
        ShiftRows(Block);
    }

    //last cipher
    XORFEE(Block, EEPROM + 0, BLOCK_SIZE);    
}

