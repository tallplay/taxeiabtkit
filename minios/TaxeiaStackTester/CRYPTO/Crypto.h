#ifndef __CRYPTO__
#define __CRYPTO__
#include "aes.h"

//only allow padded data now
void    Crypto_AES256Enc(U8 *key, U8 *pin, int pinlen, U8 *cout);
void    Crypto_AES256Dec(U8 *key, U8 *cin, int cinlen, U8 *pout);

void    Crypto_AES128Enc(U8 *key, U8 *pin, int pinlen, U8 *cout);
void    Crypto_AES128Dec(U8 *key, U8 *cin, int cinlen, U8 *pout);
void    Crypto_3DESEnc(U8 *key, U8 *pin, int pinlen, U8 *cout);
void    Crypto_3DESDec(U8 *key, U8 *cin, int cinlen, U8 *pout);


//because we don't have more SRAM in macro to save S array, we need to generate output directly.
void    Crypto_RC4Output(U8 *key, int keylen, U8 *data, int datalen);

#endif
