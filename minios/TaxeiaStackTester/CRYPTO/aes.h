/**
 * @file aes.h
 *
 * ----------------------------------------------------------------------------\n
 * Copyright 2007 Presto Technology Inc. as an unpublished work.\n
 * All Rights Reserved.\n
 * \n
 * The information contained herein is confidential property of Presto\n
 * Technology Inc. The use, copying, transfer or disclosure of such\n
 * information is prohibited except by express written agreement with\n
 * Presto Technology Inc.\n
 * ----------------------------------------------------------------------------\n
 *
 * @brief AES algorithm function prototype
 *
 * ----------------------------------------------------------------------------\n
 * Description :\n
 *     AES algorithm function prototype,\n
 *     only AES encrypt implemented.\n
 * ----------------------------------------------------------------------------\n
 * $RCSfile: $
 * $Date: $
 * $Revision: $
 * $Author: $
 * ----------------------------------------------------------------------------\n
 * Revision History :\n
 * \n
 * $Log$\n
 */

#ifndef _AES_H_
#define _AES_H_

#include "BtType.h"
/**
 * Precalculate AES key schedule from given cipher key.\n
 * Once key changed, this function must be execute again.
 */

void AES_CipherOnTheFly(U8 keysize, U8 *Key, U8 *Block);
void AES_DeCipherOnTheFly(U8 keysize, U8 *Key, U8 *Block);


#endif
