#include "Crypto.h"
#include "aes.h"
#include <string.h>

//RC 4 

void swap(U8 *s, unsigned int i, unsigned int j) {
    U8 temp = s[i];
    s[i] = s[j];
    s[j] = temp;
}


#define SBOXLEN 16
void rc4_quick_output(U8 *key, int key_length, U8 *out, int outlen) {
    U8 tmp1, tmp2;

#if (SBOXLEN == 256)
    int i, j;
#else
    U8 i, j; 
#endif
    U8 S[SBOXLEN];

    for (i = 0; i < SBOXLEN; i++)
        S[i] = i;
 
    for (i = j = 0; i < SBOXLEN; i++) {

        tmp1 = (i % (key_length*2));
        tmp2 = tmp1 >> 1;

        if( tmp1 & 1)
        {
            j = (j + (key[tmp2] >> 4) + S[i]) % SBOXLEN;
        }
        else
            j = (j + (key[tmp2] & 0xf) + S[i]) % SBOXLEN;

        swap(S, i, j);
}
 
    i = j = 0;

    while(outlen --)
    { 

        i = (i + 1) % SBOXLEN;
        j = (j + S[i]) % SBOXLEN;
        swap(S, i, j);
        *out = (S[(S[i] + S[j]) % SBOXLEN]);

        i = (i + 1) % SBOXLEN;
        j = (j + S[i]) % SBOXLEN;
        swap(S, i, j);
       *out ^= (S[(S[i] + S[j])  % SBOXLEN] << 4);

        i = (i + 1) % SBOXLEN;
        j = (j + S[i]) % SBOXLEN;
        swap(S, i, j);
        *out ^= (S[(S[i] + S[j])  % SBOXLEN]);

        i = (i + 1) % SBOXLEN;
        j = (j + S[i]) % SBOXLEN;
        swap(S, i, j);
        *out ^= (S[(S[i] + S[j])  % SBOXLEN] << 4);

        out++;
    }
 
    return;
}


void Crypto_AES256Enc(U8 *key, U8 *pin, int pinlen, U8 *cout)
{
    if(cout != 0 && cout != pin)
        memcpy(cout, pin, pinlen);
    else
        cout = pin;
    
    while(pinlen >= 16)
    {
        AES_CipherOnTheFly(32, key, cout);
        pinlen -= 16;
        pin += 16;
        cout+= 16;
    }
}

void Crypto_AES256Dec(U8 *key, U8 *cin, int cinlen, U8 *pout)
{
    if(pout != 0 && pout != cin)
        memcpy(pout, cin, cinlen);
    else
        pout = cin;

    while(cinlen >= 16)
    {
        AES_DeCipherOnTheFly(32,  key, pout);
        cinlen -= 16;
        cin += 16;
        pout += 16;
    }
}

void Crypto_AES128Enc(U8 *key, U8 *pin, int pinlen, U8 *cout)
{
    if(cout != 0 && cout != pin)
        memcpy(cout, pin, pinlen);
    else
        cout = pin;

    while(pinlen >= 16)
    {
        AES_CipherOnTheFly(16, key, cout);
        pinlen -= 16;
        pin += 16;
        cout += 16;
    }
}

void Crypto_AES128Dec(U8 *key, U8 *cin, int cinlen, U8 *pout)
{
    if(pout != 0 && pout != cin)
        memcpy(pout, cin, cinlen);
    else
        pout = cin;
    
    while(cinlen >= 16)
    {
        AES_DeCipherOnTheFly(16, key, pout);
        cinlen -= 16;
        cin += 16;
        pout += 16;
    }
 }

/*
void Crypto_3DESEnc(U8 *key, U8 *pin, int pinlen, U8 *cout)
{
    if(cout != 0 && cout != pin)
        memcpy(cout, pin, pinlen);
    else
        cout = pin;

    while(pinlen >= 8)
    {
        tdes_enc(cout, pin, key);
        pinlen -= 8;
        pin += 8;
        cout += 8;
    }
}

void Crypto_3DESDec(U8 *key, U8 *cin, int cinlen, U8 *pout)
{
    if(pout != 0 && pout != cin)
        memcpy(pout, cin, cinlen);
    else
        pout = cin;
    
    while(cinlen >= 8)
    {
        tdes_dec(pout, cin, key);
        cinlen -= 8;
        cin += 8;
        pout += 8;
    }
 }
*/
void Crypto_RC4Output(U8 *key, int keylen, U8 *dat, int datalen)
{
    rc4_quick_output(key, keylen, dat, datalen);
}

