#include "HIDControl.h"
#include "GAPControl.h"

#if (PROFILE_TYPE == PROFILE_TYPE_HID || PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
struct
{
    U16 hidcconnect : 1;
    U16 hidiconnect : 1;
	U16 outgoing : 1;
	U16 reserved : 13;
}hidinfo;

//parm[0]: 1: connect sucess 0: disconnect or connect fail
//parm[1]: 1: hidc 0:hidi
U8 * UpdateHidInfo(U16 parm)
{
    switch(parm & 0x3)
	{
	    case 0: //hidi disconnect
			hidinfo.hidiconnect = 0;
			if(hidinfo.outgoing)
			{
			    Bt__HIDDCDisconnect();
			}				
			break;
	    case 1: //hidi connect
			hidinfo.hidiconnect = 1;
		    Bt__WriteScanEanbled(0); //no scan
			if(hidinfo.outgoing)
			{
			    hidinfo.outgoing = 0;
			}

			break;
		case 2: //hidc disconnect
	        hidinfo.hidcconnect = 0;
			if(hidinfo.outgoing)
			{
			    Bt__DisconnRst(GAPGetAclHandle(), 0x13, 0);
			}
			break;
		case 3: //hidc connect
		{
	        hidinfo.hidcconnect = 1;
		    if(hidinfo.outgoing)
	    	{
	    	    Bt__HIDDIConnect(GAPGetAclHandle()); //create intr l2cap link
	    	}
	    }
			break;
	}

	return 0;
}

U8 * HIDACLConnectCB(void)
{
	if(GAPIsAclConnected())
	{
		if(hidinfo.outgoing)
		    Bt__WriteAuthReq(GAPGetAclHandle());
	}
	else
	{
	    if(hidinfo.outgoing)
			hidinfo.outgoing = 0;
	    Bt__WriteScanEanbled(2); //paging scan		
	}
	return 0;
}

//0: not connect 1: connect 2: connecting
U8 HIDConnectStatus(void)
{
    if(hidinfo.hidcconnect && hidinfo.hidiconnect)
		return 1;
    else if(!hidinfo.hidcconnect && !hidinfo.hidiconnect)
		return 0;
    return 2;
}

U8 HIDIsConnectDisconnectOnGoing()
{
    return hidinfo.outgoing;
}

void HIDCommonInit(void)
{
	hidinfo.hidiconnect = 0;
	hidinfo.hidcconnect = 0;
	hidinfo.outgoing = 0;
	Bt__ProfileHIDDCB(UpdateHidInfo);
}

void HIDConnectDisconnect(U8 *bdaddr)
{

    if(!GAPIsAclConnected() && !hidinfo.hidcconnect && !hidinfo.hidiconnect)
    {
    	hidinfo.outgoing = 1;
		//Bt__SetBtWakeup();
		Bt__ConnRst(bdaddr, 0);
    }
	else if(GAPIsAclConnected() && hidinfo.hidcconnect && hidinfo.hidiconnect)
	{
	    hidinfo.outgoing = 1;
		Bt__HIDDIDisconnect();
	}

}

void HIDSendMouse(U8 btn, U8 x, U8 y, U8 z)
{
	if(hidinfo.hidiconnect &&  Bt_AclReady())
		Bt__ProfileHIDDSendMouse(btn, x, y, z);

}

void HIDSendMedia(U8 *media)
{
	if(hidinfo.hidiconnect &&  Bt_AclReady())
	    Bt__ProfileHIDDSendMedia(media);
}

void HIDSendKeyCode(U8 modifier, U8 *keycode, U8 len)
{
	if(hidinfo.hidiconnect &&  Bt_AclReady())
        Bt__ProfileHIDDSendKeyCode(modifier, keycode, len);
}
#endif

