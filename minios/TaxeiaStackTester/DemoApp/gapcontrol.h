#ifndef __GAPCONTROL_HH_
#define __GAPCONTROL_HH_
#include "BtType.h"

extern "C"
{
    #include "GAP\LOCAL_DEV\Bt__LocalDevice.h"
    #include "GAP\LL\Bt__RemoteDevice.h"
    #include "CORE\BTCore.h"
}

typedef void (*GAPCALLBACK)(U16 parm1, void *parm2);

void GAPInit(GAPCALLBACK cb, U8 profile_type);
U8 *GAPGetRemoteBdAddr();
U8 *GAPGetRemoteName();
U8 GAPIsAclConnected();
U16 GAPGetAclHandle();
void GAPGetBtStatus(U8 type, U32 *val);
#endif
