#include "GAPControl.h"
#include "HIDControl.h"
#include "HFPControl.h"
#include <string.h>
#include <windows.h>

struct
{
	U16 aclconnect : 1;
	U16 reserved : 15;
	U16 aclhandle;
	U8 AclBdaddr[6];
    U8 AclBdName[26];
}aclinfo;

BT_LOCAL_INFO *bt_info;
U8 profile_type;

void (*GAPCB)(U16 parm1, void * parm2);

U8* GAPACLConnectCB(U16 parm)
{
    OutputDebugString(TEXT("ACL connected callback"));
	return 0;
}

static U8 * ACLConnectCB(U16 parm)
{
	aclinfo.aclconnect = parm ? 1: 0;
	aclinfo.aclhandle = parm;

	#if (PROFILE_TYPE == PROFILE_TYPE_HID)
    return HIDACLConnectCB();
	#elif (PROFILE_TYPE == PROFILE_TYPE_HFP)
    return HFPACLConnectCB();
    #elif (PROFILE_TYPE == PROFILE_TYPE_NOTHING)
	return GAPACLConnectCB(parm);
	#elif (PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
	if(profile_type == PROFILE_TYPE_HID)
        return HIDACLConnectCB();
	else if(profile_type == PROFILE_TYPE_HFP)
		return HFPACLConnectCB();
	else
		return 0;
	//else if(profile_type == PROFILE_TYPE_HFP)
      //  return HfpACLConnectCB(parm);
	#endif
}

static U8 * HidLocalDeviceInfo(U16 parm)
{
	#if (PROFILE_TYPE == PROFILE_TYPE_HID || PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
	if(HIDIsConnectDisconnectOnGoing())
	{
		if(parm == LOCALCB_AUTH_SUCCESS)
		{
		   Bt__SetConnectionEncryption(aclinfo.aclhandle, 1);
		}
		else if(parm == LOCALCB_ENCRYPTION_CHANGE)
		{
			Bt__HIDDCConnect(aclinfo.aclhandle);
		}
		else if(parm == LOCALCB_AUTH_FAIL)
		{
		    Bt__DisconnRst(aclinfo.aclhandle, 0x05, 0); //auth fail(reason);
		}
		else if(parm == LOCALCB_BT_WAKEUP)
		{
	        //no mtk sleep function now!!
    	    //Bt__SetBtSleep(0); //no sleep mode
		}
		else if(parm == LOCALCB_SET_SLEEP)
		{
	        //no mtk sleep function now!!
		   //U8 tmp[6];
    	   //Bt__EnumPairedDevice(tmp);
	       // Bt__ConnRst(tmp, 0);
		}
    }
	#endif
    return 0;
}


static U8 * HfpLocalDeviceInfo(U16 parm)
{
	#if (PROFILE_TYPE == PROFILE_TYPE_HFP || PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
	if(HFPIsConnectDisconnectOnGoing())
	{
		if(parm == LOCALCB_AUTH_SUCCESS)
		{
		   Bt__SetConnectionEncryption(aclinfo.aclhandle, 1);
		}
		else if(parm == LOCALCB_ENCRYPTION_CHANGE)
		{
			Bt__HFPConnect(aclinfo.aclhandle);
		}
		else if(parm == LOCALCB_AUTH_FAIL)
		{
		    Bt__DisconnRst(aclinfo.aclhandle, 0x05, 0); //auth fail(reason);
		}
		else if(parm == LOCALCB_BT_WAKEUP)
		{
	        //no mtk sleep function now!!
    	    //Bt__SetBtSleep(0); //no sleep mode
		}
		else if(parm == LOCALCB_SET_SLEEP)
		{
	        //no mtk sleep function now!!
		   //U8 tmp[6];
    	   //Bt__EnumPairedDevice(tmp);
	       // Bt__ConnRst(tmp, 0);
		}
    }
	#endif
    return 0;
}

static U8 * LocalDeviceInfo(U16 parm)
{
    char name[20];
    if(parm == LOCALCB_INITIALIZED)//initilized
	{
		GAPCB(0, (void*)bt_info->ScanEnabled);
//	    Bt__WriteScanEanbled(2); //paging scan

		//BD name
		GAPCB(2, name);
	    Bt__WriteLocalName((U8*)name, (U8)strlen((char*)name));

	    return 0;
	}
	else if(parm == LOCALCB_SCAN_CHANGE)
	{
	    GAPCB(1, (void*)bt_info->ScanEnabled);
	}
	else if(parm == LOCALCB_PIN_CODE_REQ)
	{
	    U8 pin[10];
    	GAPCB(3, pin);
		return pin;
	}

	#if (PROFILE_TYPE == PROFILE_TYPE_HID)
	    return HidLocalDeviceInfo(parm);
	#elif (PROFILE_TYPE == PROFILE_TYPE_HFP)
	    return HfpLocalDeviceInfo(parm);
	#elif (PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
	    if(profile_type == PROFILE_TYPE_HID)
    	{
    	    return HidLocalDeviceInfo(parm);
    	}
		else if(profile_type == PROFILE_TYPE_HFP)
		{
		    return HfpLocalDeviceInfo(parm);
		}
    #endif

	return 0;
}

void GAPGetBtStatus(U8 type, U32 *val)
{
    
    if(type == 0)
	{
	    switch(BTCoreState())
    	{
    	    case INITIALIZING:
				*val = 1;
				break;
		    case INITIALIZED:
				*val = 2;
				break;
		    default:
				*val = 2;
				break;
    	}
    }
}

U16 GAPGetAclHandle()
{
    return aclinfo.aclhandle;
}

U8 GAPIsAclConnected()
{
    return aclinfo.aclconnect;
}

U8 *GAPGetRemoteName()
{
    return aclinfo.AclBdName;
}

U8 *GAPGetRemoteBdAddr()
{
    return aclinfo.AclBdaddr;
}

//0 : initilized 1: scan change
void GAPInit(GAPCALLBACK cb, U8 ptype)
{
	#if (PROFILE_TYPE == PROFILE_TYPE_HID)
		HIDCommonInit();
	#elif (PROFILE_TYPE == PROFILE_TYPE_HFP)
	    HFPCommonInit();
	#elif (PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
	    profile_type = ptype;
	    if(profile_type == PROFILE_TYPE_HID_HFP) //two profiles
    	{
			BtCoreProfileInit(profile_type);
			HIDCommonInit();
			HFPCommonInit();
	    }
		else
		{
			BtCoreProfileInit(profile_type);
		    if(profile_type == PROFILE_TYPE_HID)
				HIDCommonInit();
			else if(profile_type == PROFILE_TYPE_HFP)
				HFPCommonInit();
	    }
	#endif

    GAPCB = cb;
	aclinfo.aclconnect = 0;
	aclinfo.aclhandle = 0;

    Bt__GetLocalInfo(&bt_info);
	Bt__LocalDeviceCB(LocalDeviceInfo);
	Bt__RemoteDeviceCB(ACLConnectCB);
	Bt__RemoteBdaddrReg(aclinfo.AclBdaddr);
    Bt__RemoteBdNameReg(aclinfo.AclBdName);
}

	

