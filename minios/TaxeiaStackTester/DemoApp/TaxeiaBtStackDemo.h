// TaxeiaBtStackDemo.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
    #error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"    // main symbols
#include "HFSMessageBox.h"

extern "C" {
#include "CORE\BTCore.h"
#include "TaxeiaHciLog\TaxeiaHCILog.h"
#include "GAP\SECURITY\Bt__SecurityManager.h"
#include "hfp\Bt__Profile_HFP.h"
#include "TaxeiaConsoleDebug\TaxeiaConsoledebug.h"
unsigned int MassUart_GetUniqueID(void);
DWORD AssignBDAddr(void);
void AssignLinkKeyFileName(char *name);
}

#define WM_UPDATE_DATA WM_USER+12345
#define WM_HFSMBOX_CLOSED WM_USER+12346 
#define WM_HFSMBOX_ACTION WM_USER+12347 

// CTaxeiaBtStackDemoApp:
// See TaxeiaBtStackDemo.cpp for the implementation of this class
//

class CTaxeiaBtStackDemoApp : public CWinApp
{
public:
	CTaxeiaBtStackDemoApp();

// Overrides
	public:
	virtual BOOL InitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
};

extern CTaxeiaBtStackDemoApp theApp;
