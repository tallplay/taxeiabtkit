//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by TaxeiaBtStackDemo.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_TAXEIABTSTACKDEMO_DIALOG    102
#define IDR_MAINFRAME                   128
#define IDD_DIALOG_HFP_MESSAGEBOX       129
#define IDD_PIN_DIALOG                  130
#define IDC_RADIO1                      1000
#define IDC_RADIO_PHID                  1000
#define IDC_RADIO2                      1001
#define IDC_RADIO_PHFP                  1001
#define IDC_EDIT_DEV_STATUS             1002
#define IDC_RADIO_PBOTH                 1002
#define IDC_EDIT_DEV_STATUS2            1003
#define IDC_EDIT_BTSTK_STATUS           1003
#define IDC_RADIO_NO_SCAN               1004
#define IDC_RADIO_PAGE_SCAN             1005
#define IDC_RADIO_INQ_PAGE_SCAN         1006
#define IDC_RADIO_INQ_SCAN              1007
#define IDC_STATIC_SCAN_MODE            1008
#define IDC_EDIT_ACL_STAUS              1009
#define IDC_STATIC_ACL_STATUS           1010
#define IDC_EDIT_HID_STATUS             1011
#define IDC_STATIC_HID_STATUS           1012
#define IDC_STATIC_PAIR_SELECT          1013
#define IDC_RADIO_PAIR_DEVICE0          1014
#define IDC_RADIO_PAIR_DEVICE1          1015
#define IDC_RADIO_PAIR_DEVICE2          1016
#define IDC_RADIO_PAIR_DEVICE3          1017
#define IDC_BUTTON_HID_CONNECT          1018
#define IDC_STATIC_GAP                  1019
#define IDC_STATIC_HID_HFP_SPP          1020
#define IDC_STATIC_DEVICE               1021
#define IDC_STATIC_BDNAME               1022
#define IDC_STATIC_PSEL                 1023
#define IDC_STATIC_HFP_IND_STATUS       1024
#define IDC_BUTTON_HFP_VENDOR_AT        1025
#define IDC_BUTTON_HFP_GETPB            1026
#define IDC_STATIC_HFP_STATUS           1027
#define IDC_EDIT_HFP_STATUS             1028
#define IDC_BUTTON_HFP_CONNECT          1029
#define IDC_EDIT_SPP_STATUS             1030
#define IDC_COMBO_HFP_PB                1031
#define IDC_STATIC_TITLE                1032
#define IDC_STATIC_SPP_STATUS           1032
#define IDC_BUTTON_ACTION               1033
#define IDC_BUTTON_HFP_CONNECT2         1033
#define IDC_BUTTON_SPP_CONNECT          1033
#define IDC_EDIT_VENDOR_AT              1034
#define IDC_EDIT_PIN                    1035
#define ID_STATUS_TIMER                 2011

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1036
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
