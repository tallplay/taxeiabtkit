// PinDialog.cpp : implementation file
//

#include "stdafx.h"
#include "TaxeiaBtStackDemo.h"
#include "PinDialog.h"


// CPinDialog dialog

IMPLEMENT_DYNAMIC(CPinDialog, CDialog)

CPinDialog::CPinDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CPinDialog::IDD, pParent)
{

}

CPinDialog::~CPinDialog()
{
}

void CPinDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_PIN, m_edit_pin);
}


BEGIN_MESSAGE_MAP(CPinDialog, CDialog)
	ON_EN_UPDATE(IDC_EDIT_PIN, &CPinDialog::OnEnUpdateEditPin)
END_MESSAGE_MAP()


// CPinDialog message handlers

void CPinDialog::OnEnUpdateEditPin()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function to send the EM_SETEVENTMASK message to the control
	// with the ENM_UPDATE flag ORed into the lParam mask.

	// TODO:  Add your control notification handler code here
	m_edit_pin.GetWindowText((LPTSTR)pin+1, 10);
	pin[0] = m_edit_pin.GetWindowTextLength();

}

unsigned char CPinDialog::GetPinCode(unsigned char *pin)
{
	for(int i = 0; i < this->pin[0]+1; i++)
		pin[i] = this->pin[i];	
	return 0;
}
