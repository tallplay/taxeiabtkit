#pragma once


// CHFSMessageBox dialog

class CHFSMessageBox : public CDialog
{
	DECLARE_DYNAMIC(CHFSMessageBox)

public:
	CWnd *mParent;
	CHFSMessageBox(CWnd* pParent = NULL);   // standard constructor
	virtual ~CHFSMessageBox();

    void SetTitleActionText(CString title, CString action);

// Dialog Data
	enum { IDD = IDD_DIALOG_HFP_MESSAGEBOX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
	virtual void PostNcDestroy();
	virtual void OnCancel();
public:
	afx_msg void OnBnClickedButtonAction();
};
