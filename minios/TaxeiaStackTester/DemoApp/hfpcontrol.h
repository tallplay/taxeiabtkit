#ifndef __HFPCONTROL_HH_
#define __HFPCONTROL_HH_
#include "BtType.h"
extern "C"
{
    #include "HFP\Bt__Profile_HFP.h"
    #include "GAP\LOCAL_DEV\Bt__LocalDevice.h"
    #include "GAP\LL\Bt__RemoteDevice.h"
}
U8 * HFPACLConnectCB(void);
void HFPCommonInit(void);
void HFPConnectDisconnect(U8*);
U8 HFPConnectStatus(void);
U8 HFPIsConnectDisconnectOnGoing();
void HFPGetPhoneBook(void(*pbitemcb)(U16 idx, U8* num, U8 *name));
U8 HFPGetPhoneBookStatus(U16 *total, U16 *idx);
void HFPHFPStatus(U8 *signal, U8 *battery, U8 *service, U8 *callsetup, U8 *call);
U8 *HFSCallingNumber();

#endif

