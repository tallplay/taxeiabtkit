// TaxeiaBtStackDemoDlg.h : header file
//

#pragma once
#include "afxwin.h"


// CTaxeiaBtStackDemoDlg dialog
class CTaxeiaBtStackDemoDlg : public CDialog
{
// Construction
public:
    CTaxeiaBtStackDemoDlg(CWnd* pParent = NULL);	// standard constructor

    void UpdateStatus(void);
    void UpdateHFPProfileStatus(U8);
    void UpdateSPPProfileStatus(U8);    
    void UpdateHIDProfileStatus(U8);
    void UpdateProfileStatus(U8);
    void UpdateGAPStatus(U8);
    
    void ShowControls(int);
    void ShowProfileControls(int);
    void ShowGAPControls(int);
    void ShowDeviceControls(int);

// Dialog Data
    enum { IDD = IDD_TAXEIABTSTACKDEMO_DIALOG };

    protected:
    virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
    HICON m_hIcon;

    // Generated message map functions
    virtual BOOL OnInitDialog();
    afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
    afx_msg void OnPaint();
    afx_msg HCURSOR OnQueryDragIcon();
    afx_msg BOOL OnDeviceChange(UINT nEventType, DWORD_PTR dwData);
DECLARE_MESSAGE_MAP()
public:
    CHFSMessageBox *hfsMBox;
    afx_msg void OnTimer(UINT_PTR nIDEvent);
    int ScanSelect;
    int ScanMode;
    int PairSelect;
    int profile_select;
    int profile_type;
    afx_msg void OnBnClickedRadioNoScan();
    afx_msg void OnBnClickedRadioPageScan();
    afx_msg void OnBnClickedRadioInqPageScan();
    afx_msg LRESULT UpdateDat(WPARAM wParam, LPARAM lParam);
    afx_msg LRESULT HFSMBoxEnd(WPARAM wParam, LPARAM lParam);
    afx_msg LRESULT HFSMBoxAction(WPARAM wParam, LPARAM lParam);
    

    afx_msg void OnMouseMove(UINT nFlags, CPoint point);
    afx_msg void OnBnClickedButtonHidConnect();
    afx_msg void OnBnClickedRadioPhid();
    afx_msg void OnBnClickedButtonHfpHandup();
    afx_msg void OnBnClickedButtonHfpConnect();
    afx_msg void OnBnClickedButtonHfpGetpb();
    afx_msg void OnCbnSelchangeComboHfpPb();
    CComboBox m_HfpPbCombo;
protected:
	virtual void OnCancel();
public:
	afx_msg void OnBnClickedButtonSppConnect();
	afx_msg void OnBnClickedRadioInqScan();
};
