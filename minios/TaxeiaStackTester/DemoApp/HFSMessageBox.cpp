// HFSMessageBox.cpp : implementation file
//

#include "stdafx.h"
#include "TaxeiaBtStackDemo.h"
#include "HFSMessageBox.h"


// CHFSMessageBox dialog

IMPLEMENT_DYNAMIC(CHFSMessageBox, CDialog)

CHFSMessageBox::CHFSMessageBox(CWnd* pParent /*=NULL*/)
	: CDialog(CHFSMessageBox::IDD, pParent)
{
    mParent = pParent;
}

CHFSMessageBox::~CHFSMessageBox()
{
}

void CHFSMessageBox::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

void CHFSMessageBox::SetTitleActionText(CString title, CString action)
{
	this->GetDlgItem(IDC_STATIC_TITLE)->SetWindowText(title);
	this->GetDlgItem(IDC_BUTTON_ACTION)->SetWindowText(action);
	UpdateData(FALSE);
}


BEGIN_MESSAGE_MAP(CHFSMessageBox, CDialog)
	ON_BN_CLICKED(IDC_BUTTON_ACTION, &CHFSMessageBox::OnBnClickedButtonAction)
END_MESSAGE_MAP()


// CHFSMessageBox message handlers

void CHFSMessageBox::PostNcDestroy()
{
	// TODO: Add your specialized code here and/or call the base class
	CDialog::PostNcDestroy();
	mParent->PostMessage(WM_HFSMBOX_CLOSED, 0, 0);
	delete this;
}

void CHFSMessageBox::OnCancel()
{
	// TODO: Add your specialized code here and/or call the base class
	DestroyWindow();
}

void CHFSMessageBox::OnBnClickedButtonAction()
{
	// TODO: Add your control notification handler code here
	mParent->PostMessage(WM_HFSMBOX_ACTION, 0, 0);
}
