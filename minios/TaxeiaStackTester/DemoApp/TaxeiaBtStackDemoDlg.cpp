
// TaxeiaBtStackDemoDlg.cpp : implementation file
//

#include "stdafx.h"

#include "TaxeiaBtStackDemo.h"
#include "TaxeiaBtStackDemoDlg.h"
#include <dbt.h>
#include "PinDialog.h"
#include "sdkapi\bt_hid_hfp_api.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

extern "C"
{
    U8 pad_data_get_report(U8 report_id, U8 len, U8 *dat);
    U8 ps3_data_get_report(U8 type, U8 report, U8 *buf, int maxlen);
    U8 ps3_data_set_report(U8 type, U8 report, const U8 *buf, int len);
}

// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
    CAboutDlg();

// Dialog Data
    enum { IDD = IDD_ABOUTBOX };

protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
    DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CTaxeiaBtStackDemoDlg dialog




CTaxeiaBtStackDemoDlg::CTaxeiaBtStackDemoDlg(CWnd* pParent /*=NULL*/)
    : CDialog(CTaxeiaBtStackDemoDlg::IDD, pParent)
    , ScanSelect(0)
    , ScanMode(0)
    , PairSelect(0)
    , profile_select(86)
#if (PROFILE_TYPE == PROFILE_TYPE_HFP)
    , profile_type(PROFILE_TYPE_HFP)
#elif (PROFILE_TYPE == PROFILE_TYPE_HID)
    , profile_type(PROFILE_TYPE_HID)
#elif (PROFILE_TYPE == PROFILE_TYPE_HID_HFP)
    , profile_type(PROFILE_TYPE_HID_HFP)
#elif (PROFILE_TYPE == PROFILE_TYPE_SPP)
    , profile_type(PROFILE_TYPE_SPP)
#else
    , profile_type(0xff)
#endif
    ,hfsMBox(NULL)
{
    m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CTaxeiaBtStackDemoDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    DDX_Radio(pDX, IDC_RADIO_NO_SCAN, ScanMode);
    DDX_Radio(pDX, IDC_RADIO_PAIR_DEVICE0, PairSelect);
    DDX_Radio(pDX, IDC_RADIO_PHID, profile_select);
    DDX_Control(pDX, IDC_COMBO_HFP_PB, m_HfpPbCombo);
}

BEGIN_MESSAGE_MAP(CTaxeiaBtStackDemoDlg, CDialog)
    ON_WM_SYSCOMMAND()
    ON_WM_PAINT()
    ON_WM_QUERYDRAGICON()
    ON_WM_DEVICECHANGE()
    //}}AFX_MSG_MAP
    ON_WM_TIMER()
    ON_BN_CLICKED(IDC_RADIO_NO_SCAN, &CTaxeiaBtStackDemoDlg::OnBnClickedRadioNoScan)
    ON_BN_CLICKED(IDC_RADIO_PAGE_SCAN, &CTaxeiaBtStackDemoDlg::OnBnClickedRadioPageScan)
    ON_BN_CLICKED(IDC_RADIO_INQ_PAGE_SCAN, &CTaxeiaBtStackDemoDlg::OnBnClickedRadioInqPageScan)
    ON_MESSAGE(WM_UPDATE_DATA, &CTaxeiaBtStackDemoDlg::UpdateDat)
    ON_WM_MOUSEMOVE()
    ON_BN_CLICKED(IDC_BUTTON_HID_CONNECT, &CTaxeiaBtStackDemoDlg::OnBnClickedButtonHidConnect)
    ON_BN_CLICKED(IDC_RADIO_PHID, &CTaxeiaBtStackDemoDlg::OnBnClickedRadioPhid)
    ON_BN_CLICKED(IDC_RADIO_PHFP, &CTaxeiaBtStackDemoDlg::OnBnClickedRadioPhid)
    ON_BN_CLICKED(IDC_RADIO_PBOTH, &CTaxeiaBtStackDemoDlg::OnBnClickedRadioPhid)
    ON_BN_CLICKED(IDC_BUTTON_HFP_VENDOR_AT, &CTaxeiaBtStackDemoDlg::OnBnClickedButtonHfpHandup)
    ON_BN_CLICKED(IDC_BUTTON_HFP_CONNECT, &CTaxeiaBtStackDemoDlg::OnBnClickedButtonHfpConnect)
    ON_BN_CLICKED(IDC_BUTTON_HFP_GETPB, &CTaxeiaBtStackDemoDlg::OnBnClickedButtonHfpGetpb)
    ON_CBN_SELCHANGE(IDC_COMBO_HFP_PB, &CTaxeiaBtStackDemoDlg::OnCbnSelchangeComboHfpPb)
    ON_MESSAGE(WM_HFSMBOX_CLOSED, &CTaxeiaBtStackDemoDlg::HFSMBoxEnd)
    ON_MESSAGE(WM_HFSMBOX_ACTION, &CTaxeiaBtStackDemoDlg::HFSMBoxAction)
	ON_BN_CLICKED(IDC_BUTTON_SPP_CONNECT, &CTaxeiaBtStackDemoDlg::OnBnClickedButtonSppConnect)
	ON_BN_CLICKED(IDC_RADIO_INQ_SCAN, &CTaxeiaBtStackDemoDlg::OnBnClickedRadioInqScan)
END_MESSAGE_MAP()

CTaxeiaBtStackDemoDlg *TheDlg = NULL;


//////////////TOOLs///////////////////////////////
void AssignLinkKeyFileName(char *name)
{
#if (PROFILE_TYPE == PROFILE_TYPE_HID)
    strcpy(name, "HIDLink");
#elif (PROFILE_TYPE == PROFILE_TYPE_HFP)
    strcpy(name, "HFPLink");
#elif (PROFILE_TYPE == PROFILE_TYPE_HID_HFP)
    strcpy(name, "HIDHFPLink");
#elif (PROFILE_TYPE == PROFILE_TYPE_SPP)
    strcpy(name, "SPPLink");
#elif (PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
    if(TheDlg->profile_type == PROFILE_TYPE_HID) //HID
        strcpy(name, "HIDLink");
    else if(TheDlg->profile_type == PROFILE_TYPE_HFP) //HFP
        strcpy(name, "HFPLink");
    else if(TheDlg->profile_type == PROFILE_TYPE_HID_HFP) //HID_HFP
        strcpy(name, "HIDHFPLink");
    else
        strcpy(name, "XXXLink");
#else
#error "Unsupported PROFILE SETTING"
#endif
}

void AssignDeviceFileName(char *name)
{
#if (PROFILE_TYPE == PROFILE_TYPE_HID)
    sprintf(name, "HIDDevs-%8x", AssignBDAddr());
#elif (PROFILE_TYPE == PROFILE_TYPE_HFP)
    sprintf(name, "HFPDevs-%8x", AssignBDAddr());
#elif (PROFILE_TYPE == PROFILE_TYPE_HID_HFP)
    sprintf(name, "HIDHFPDevs-%8x", AssignBDAddr());
#elif (PROFILE_TYPE == PROFILE_TYPE_SPP)
    sprintf(name, "SPPDevs-%8x", AssignBDAddr());
#elif (PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
    if(TheDlg->profile_type == PROFILE_TYPE_HID) //HID
        sprintf(name, "HIDDevs-%8x", AssignBDAddr());
    else if(TheDlg->profile_type == PROFILE_TYPE_HFP) //HFP
        sprintf(name, "HFPDevs-%8x", AssignBDAddr());
    else if(TheDlg->profile_type == PROFILE_TYPE_HID_HFP) //HFP
        sprintf(name, "HIDHFPDevs-%8x", AssignBDAddr());
    else
        strcpy(name, "XXXDevs");
#else
#error "Unsupported PROFILE SETTING"
#endif
}

DWORD AssignBDAddr(void)
{
#if (PROFILE_TYPE == PROFILE_TYPE_HID)
    return MassUart_GetUniqueID() + 1;
#elif (PROFILE_TYPE == PROFILE_TYPE_HFP)
    return MassUart_GetUniqueID() + 2;
#elif (PROFILE_TYPE == PROFILE_TYPE_HID_HFP)
    return MassUart_GetUniqueID() + 3;
#elif (PROFILE_TYPE == PROFILE_TYPE_SPP)
    return MassUart_GetUniqueID() + 5;
#elif (PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
    if(TheDlg->profile_type == PROFILE_TYPE_HID) //HID
        return MassUart_GetUniqueID() + 1;
    else if(TheDlg->profile_type == PROFILE_TYPE_HFP) //HFP
        return MassUart_GetUniqueID() + 2;
    else if(TheDlg->profile_type == PROFILE_TYPE_HID_HFP) //HFP
        return MassUart_GetUniqueID() + 3; //HFP_HID
    else
        return MassUart_GetUniqueID() + 4;
#else
#error "Unsupported PROFILE SETTING"
#endif
}

void AssignBDName(char *name)
{
//BD name
#if (PROFILE_TYPE == PROFILE_TYPE_HID)
    #ifdef UCOMM_HID_JOYSTICK
    sprintf((char*)name, "BT_JOYSTICK", AssignBDAddr());
	#else
    sprintf((char*)name, "HID_%08X_f", AssignBDAddr());
	#endif
#elif (PROFILE_TYPE == PROFILE_TYPE_HFP)
    sprintf((char*)name, "HFP_%08X_f", AssignBDAddr());
#elif (PROFILE_TYPE == PROFILE_TYPE_HID_HFP)
    sprintf((char*)name, "HFP_HID_%08X_f", AssignBDAddr());
#elif (PROFILE_TYPE == PROFILE_TYPE_SPP)
    sprintf((char*)name, "SPP_%08X_f", AssignBDAddr());
sprintf((char*)name, "YuKey!");
#elif (PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
    if(TheDlg->profile_type == PROFILE_TYPE_HID) //HID
        sprintf((char*)name, "HID_%08X", AssignBDAddr());
    else if(TheDlg->profile_type == PROFILE_TYPE_HFP) //HFP
        sprintf((char*)name, "HFP_%08X", AssignBDAddr());
    else if(TheDlg->profile_type == PROFILE_TYPE_HID_HFP) //HID_HFP
        sprintf((char*)name, "HFP_HID_%08X", AssignBDAddr());
#else
#error "Unsupported PROFILE SETTING"
#endif
}

//simplify the paired device enumeration begin
void MemoAttachName2BtDevice(U8 *bdaddr, U8 *name)
{
    U32 read, written;
    U32 addr;
    U8 dat[6];
    char tmpff[32*20];
    char filename[128];

    AssignDeviceFileName(filename);
    strcat(filename, ".tmp");

    memset(tmpff, 0xff, 32*20);
    HANDLE fp = INVALID_HANDLE_VALUE;

    while (fp == INVALID_HANDLE_VALUE) {
        fp = CreateFileA(filename, GENERIC_READ|GENERIC_WRITE,
                         0, 0, OPEN_EXISTING,
                         FILE_ATTRIBUTE_NORMAL|FILE_FLAG_RANDOM_ACCESS, 0);
        if(fp == INVALID_HANDLE_VALUE)
        {
            fp = CreateFileA(filename, GENERIC_READ|GENERIC_WRITE,
                             FILE_SHARE_READ, 0, CREATE_ALWAYS,
                             FILE_ATTRIBUTE_NORMAL|FILE_FLAG_RANDOM_ACCESS, 0);
            WriteFile(fp, tmpff, 32*20, &written, 0);
            CloseHandle(fp);
            fp = INVALID_HANDLE_VALUE;
        }
    }

    addr = 0;

    while(addr < 32 *20)
    {
        SetFilePointer(fp, addr, 0, FILE_BEGIN);
        ReadFile(fp, dat, 6, &read, 0);
        if(read == 6)
        {
            if(!memcmp(dat, bdaddr, 6)) //already exist
            {
                //not exist in current file, write it
                WriteFile(fp, name, strlen((char*)name) + 1, &written, 0);
                CloseHandle(fp);
                return;
            }
            else if(!memcmp(dat, tmpff, 6)) //reach end
            {
                CloseHandle(fp);
                return;
            }
        }

        addr += 32; //6 bytes addr, 26 bytes name

    }


    CloseHandle(fp);

}

U8 MemoFindBtDevice(U8 idx, U8 *bdaddr, U8 *name)
{
    HANDLE fp;
    U32 read;
    U8 tmpff[6];
    memset(tmpff, 0xff, 6);
    char filename[128];

    AssignDeviceFileName(filename);
    strcat(filename, ".tmp");

    fp = CreateFileA(filename, GENERIC_READ,
                     0, 0, OPEN_EXISTING,
                     FILE_ATTRIBUTE_NORMAL|FILE_FLAG_RANDOM_ACCESS, 0);

    if(fp != INVALID_HANDLE_VALUE)
    {
        SetFilePointer(fp, 32 *idx, 0, FILE_BEGIN);
        ReadFile(fp, bdaddr, 6, &read, 0);
        if(read == 6)
        {
            if(memcmp(tmpff, bdaddr, 6)) //not ff
            {
                ReadFile(fp, name, 26, &read, 0);
                if(!memcmp(tmpff, name, 6))
                {
                    strcpy((char*)name, "Anonymous");
                }
                CloseHandle(fp);
                return 1;
            }
        }
        CloseHandle(fp);
    }

    return 0;
}

U8 MemoAddBtDevice(U8 *bdaddr)
{
    U32 read, written;
    U32 addr;
    U8 dat[6];
    U8 tmpff[32*20];
    char filename[128];

    AssignDeviceFileName(filename);
    strcat(filename, ".tmp");

    memset(tmpff, 0xff, 32*20);
    HANDLE fp = INVALID_HANDLE_VALUE;

    while (fp == INVALID_HANDLE_VALUE) {
        fp = CreateFileA(filename, GENERIC_READ|GENERIC_WRITE,
                         0, 0, OPEN_EXISTING,
                         FILE_ATTRIBUTE_NORMAL|FILE_FLAG_RANDOM_ACCESS, 0);
        if(fp == INVALID_HANDLE_VALUE)
        {
            fp = CreateFileA(filename, GENERIC_READ|GENERIC_WRITE,
                             FILE_SHARE_READ, 0, CREATE_ALWAYS,
                             FILE_ATTRIBUTE_NORMAL|FILE_FLAG_RANDOM_ACCESS, 0);
            WriteFile(fp, tmpff, 32*20, &written, 0);
            CloseHandle(fp);
            fp = INVALID_HANDLE_VALUE;
        }
    }

    addr = 0;

    while(addr < 32 *20)
    {
        SetFilePointer(fp, addr, 0, FILE_BEGIN);
        ReadFile(fp, dat, 6, &read, 0);
        if(read == 6)
        {
            if(!memcmp(dat, bdaddr, 6)) //already exist
            {
                CloseHandle(fp);
                return (U8) (addr / 32); //idx
            }
            else if(!memcmp(dat, tmpff, 6)) //reach end
            {
                SetFilePointer(fp, addr, 0, FILE_BEGIN);
                //not exist in current file, write it
                WriteFile(fp, bdaddr, 6, &written, 0);
                CloseHandle(fp);
                return (U8) (addr / 32); //idx
            }
        }

        addr += 32; //6 bytes addr, 26 bytes name

    }


    CloseHandle(fp);
    return 0xff;

}
//simplify the paired device enumeration end
U8 mouse_data[2];


//0:start 1:stop else: thread self
U8 RunBtThread(BYTE execute)
{
    static HANDLE OSThread = (HANDLE)-1;

    if(execute == 0) //Start Thread
    {
        if(OSThread != (HANDLE) -1) //already runing
            return 2;

        DWORD dwThreadID;

        U8 name[20];

        //set our name
        bt_start(GetTickCount(), BT_WMODE_PAGING_SCAN, TheDlg->profile_type);
        AssignBDName((char*)name);
        bt_gap_preset_local_name(name);

		//        bt_start(GetTickCount(), BT_WMODE_FREQ_TEST, 30);

        OSThread = CreateThread((LPSECURITY_ATTRIBUTES)NULL,
                                0,
                                (LPTHREAD_START_ROUTINE)RunBtThread,
                                (LPVOID)3,
                                0, &dwThreadID);

        return 0;
    }
    else if(execute == 1) //Stop Thread
    {
        if(OSThread != (HANDLE)-1)
        {
            bt_stop();
            TerminateThread(OSThread, 0);
            CloseHandle(OSThread);
            OSThread = (HANDLE)-1;
            return 0;
        }
        return 1;
    }
    else if(execute == 2) //test if device unplug
    {
        return BTCoreHwHealth();
    }

    /////Thread Body/////////////////////
    while(1)
    {
        if(!bt_task(GetTickCount()))
        {
            Sleep(1);
        }

		
#if (PROFILE_TYPE != PROFILE_TYPE_HFP && PROFILE_TYPE != PROFILE_TYPE_SPP)
#ifdef KEYBOARD_MOUSE_ONLY
#else
        if(mouse_data[0] || mouse_data[1])
    	{
#ifdef DYNAMIC_HID_DESCRIPTOR
            bt_hid_send_key_mouse_joystick_media(2);
#else
            bt_hid_send_key_mouse_joystick_media(1);
#endif
    	}
#endif
#endif

    }
    return 0;
}

void CTaxeiaBtStackDemoDlg::ShowDeviceControls(int show)
{
    U8 bdaddr[6], idx, bdname[26];
    this->GetDlgItem(IDC_STATIC_DEVICE)->ShowWindow(show);

    //show paired devices
    this->GetDlgItem(IDC_STATIC_PAIR_SELECT)->ShowWindow(show);

    if(show == SW_SHOW)
    {
        idx = 0;
        while(idx < 4)
        {
            if(MemoFindBtDevice(idx, bdaddr, bdname))
            {
                CButton *crdo;
                WCHAR addrstr[30];
                wsprintf(addrstr, TEXT("%S"), bdname);

                crdo = (CButton*)this->GetDlgItem(IDC_RADIO_PAIR_DEVICE0+idx);
                crdo->SetWindowText(addrstr);
                this->GetDlgItem(IDC_RADIO_PAIR_DEVICE0+idx)->ShowWindow(SW_SHOW);
            }
            idx ++;
        }
    }
    else
    {
        idx = 0;
        while(idx < 4)
        {
            this->GetDlgItem(IDC_RADIO_PAIR_DEVICE0+idx)->ShowWindow(SW_HIDE);
            idx++;
        }
    }
}

void CTaxeiaBtStackDemoDlg::ShowGAPControls(int show)
{
    USES_CONVERSION;

    char name[20];

    this->GetDlgItem(IDC_STATIC_BDNAME)->ShowWindow(show);
    this->GetDlgItem(IDC_STATIC_GAP)->ShowWindow(show);
    this->GetDlgItem(IDC_STATIC_ACL_STATUS)->ShowWindow(show);
    this->GetDlgItem(IDC_EDIT_ACL_STAUS)->ShowWindow(show);
    this->GetDlgItem(IDC_STATIC_SCAN_MODE)->ShowWindow(show);
    this->GetDlgItem(IDC_RADIO_NO_SCAN)->ShowWindow(show);
    this->GetDlgItem(IDC_RADIO_PAGE_SCAN)->ShowWindow(show);
    this->GetDlgItem(IDC_RADIO_INQ_PAGE_SCAN)->ShowWindow(show);
    this->GetDlgItem(IDC_RADIO_INQ_SCAN)->ShowWindow(show);

    if(show == SW_SHOW)
    {
        AssignBDName(name);
        this->GetDlgItem(IDC_STATIC_BDNAME)->SetWindowText(A2W(name));
    }
}

void CTaxeiaBtStackDemoDlg::ShowProfileControls(int show)
{
#if (PROFILE_TYPE == PROFILE_TYPE_HID)
    this->GetDlgItem(IDC_STATIC_HID_HFP_SPP)->SetWindowText(TEXT("HID"));
    this->GetDlgItem(IDC_STATIC_HID_HFP_SPP)->ShowWindow(show);
    this->GetDlgItem(IDC_STATIC_HID_STATUS)->ShowWindow(show);
    this->GetDlgItem(IDC_EDIT_HID_STATUS)->ShowWindow(show);
    this->GetDlgItem(IDC_BUTTON_HID_CONNECT)->ShowWindow(show);
#elif (PROFILE_TYPE == PROFILE_TYPE_HFP)
    this->GetDlgItem(IDC_STATIC_HID_HFP_SPP)->SetWindowText(TEXT("HFP"));
    this->GetDlgItem(IDC_STATIC_HID_HFP_SPP)->ShowWindow(show);
    this->GetDlgItem(IDC_STATIC_HFP_STATUS)->ShowWindow(show);
    this->GetDlgItem(IDC_EDIT_HFP_STATUS)->ShowWindow(show);
    this->GetDlgItem(IDC_BUTTON_HFP_CONNECT)->ShowWindow(show);
    this->GetDlgItem(IDC_STATIC_HFP_IND_STATUS)->ShowWindow(show);
    this->GetDlgItem(IDC_BUTTON_HFP_VENDOR_AT)->ShowWindow(show);
    this->GetDlgItem(IDC_BUTTON_HFP_GETPB)->ShowWindow(show);
    this->GetDlgItem(IDC_COMBO_HFP_PB)->ShowWindow(show);
    this->GetDlgItem(IDC_EDIT_VENDOR_AT)->ShowWindow(show);
#elif (PROFILE_TYPE == PROFILE_TYPE_SPP)
    this->GetDlgItem(IDC_STATIC_HID_HFP_SPP)->SetWindowText(TEXT("SPP"));
    this->GetDlgItem(IDC_STATIC_HID_HFP_SPP)->ShowWindow(show);
    this->GetDlgItem(IDC_STATIC_SPP_STATUS)->ShowWindow(show);
    this->GetDlgItem(IDC_EDIT_SPP_STATUS)->ShowWindow(show);
    this->GetDlgItem(IDC_BUTTON_SPP_CONNECT)->ShowWindow(show);
#elif (PROFILE_TYPE == PROFILE_TYPE_HID_HFP)
    this->GetDlgItem(IDC_STATIC_HID_HFP_SPP)->SetWindowText(TEXT("HID_HFP"));
    this->GetDlgItem(IDC_STATIC_HID_HFP_SPP)->ShowWindow(show);
    this->GetDlgItem(IDC_STATIC_HID_STATUS)->ShowWindow(show);
    this->GetDlgItem(IDC_EDIT_HID_STATUS)->ShowWindow(show);
    this->GetDlgItem(IDC_BUTTON_HID_CONNECT)->ShowWindow(show);

    this->GetDlgItem(IDC_STATIC_HFP_STATUS)->ShowWindow(show);
    this->GetDlgItem(IDC_EDIT_HFP_STATUS)->ShowWindow(show);
    this->GetDlgItem(IDC_BUTTON_HFP_CONNECT)->ShowWindow(show);
    this->GetDlgItem(IDC_STATIC_HFP_IND_STATUS)->ShowWindow(show);
    this->GetDlgItem(IDC_BUTTON_HFP_VENDOR_AT)->ShowWindow(show);
    this->GetDlgItem(IDC_BUTTON_HFP_GETPB)->ShowWindow(show);
    this->GetDlgItem(IDC_COMBO_HFP_PB)->ShowWindow(show);
    this->GetDlgItem(IDC_EDIT_VENDOR_AT)->ShowWindow(show);
#elif (PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
    if(profile_type == PROFILE_TYPE_HID || show == SW_HIDE) //HID
    {
        this->GetDlgItem(IDC_STATIC_HID_HFP_SPP)->SetWindowText(TEXT("HID"));
        this->GetDlgItem(IDC_STATIC_HID_HFP_SPP)->ShowWindow(show);
        this->GetDlgItem(IDC_STATIC_HID_STATUS)->ShowWindow(show);
        this->GetDlgItem(IDC_EDIT_HID_STATUS)->ShowWindow(show);
        this->GetDlgItem(IDC_BUTTON_HID_CONNECT)->ShowWindow(show);
    }

    if(profile_type == PROFILE_TYPE_HFP || show == SW_HIDE) //HFP
    {
        this->GetDlgItem(IDC_STATIC_HID_HFP_SPP)->SetWindowText(TEXT("HFP"));
        this->GetDlgItem(IDC_STATIC_HID_HFP_SPP)->ShowWindow(show);
        this->GetDlgItem(IDC_STATIC_HFP_STATUS)->ShowWindow(show);
        this->GetDlgItem(IDC_EDIT_HFP_STATUS)->ShowWindow(show);
        this->GetDlgItem(IDC_BUTTON_HFP_CONNECT)->ShowWindow(show);
        this->GetDlgItem(IDC_STATIC_HFP_IND_STATUS)->ShowWindow(show);
        this->GetDlgItem(IDC_BUTTON_HFP_VENDOR_AT)->ShowWindow(show);
        this->GetDlgItem(IDC_BUTTON_HFP_GETPB)->ShowWindow(show);
        this->GetDlgItem(IDC_COMBO_HFP_PB)->ShowWindow(show);
        this->GetDlgItem(IDC_EDIT_VENDOR_AT)->ShowWindow(show);
    }

    if(profile_type == PROFILE_TYPE_HID_HFP && show == SW_SHOW)
    {
        this->GetDlgItem(IDC_STATIC_HID_HFP_SPP)->SetWindowText(TEXT("HID_HFP"));
        this->GetDlgItem(IDC_STATIC_HID_HFP_SPP)->ShowWindow(show);
        this->GetDlgItem(IDC_STATIC_HID_STATUS)->ShowWindow(show);
        this->GetDlgItem(IDC_EDIT_HID_STATUS)->ShowWindow(show);
        this->GetDlgItem(IDC_BUTTON_HID_CONNECT)->ShowWindow(show);

        this->GetDlgItem(IDC_STATIC_HFP_STATUS)->ShowWindow(show);
        this->GetDlgItem(IDC_EDIT_HFP_STATUS)->ShowWindow(show);
        this->GetDlgItem(IDC_BUTTON_HFP_CONNECT)->ShowWindow(show);
        this->GetDlgItem(IDC_STATIC_HFP_IND_STATUS)->ShowWindow(show);
        this->GetDlgItem(IDC_BUTTON_HFP_VENDOR_AT)->ShowWindow(show);
        this->GetDlgItem(IDC_BUTTON_HFP_GETPB)->ShowWindow(show);
        this->GetDlgItem(IDC_COMBO_HFP_PB)->ShowWindow(show);
        this->GetDlgItem(IDC_EDIT_VENDOR_AT)->ShowWindow(show);
    }

#else
#error "Unsupported PROFILE SETTING"    
#endif
}

void CTaxeiaBtStackDemoDlg::ShowControls(int show)
{
    ShowProfileControls(show);
    ShowGAPControls(show);
    ShowDeviceControls(show);
}

void CTaxeiaBtStackDemoDlg::UpdateHIDProfileStatus(U8 stksts)
{
#if (PROFILE_TYPE == PROFILE_TYPE_HID || PROFILE_TYPE == PROFILE_TYPE_HID_HFP || PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
    CEdit *sts;

    static U8 lasthidiconnect = 0;

    sts = (CEdit*)this->GetDlgItem(IDC_EDIT_HID_STATUS);
    if(stksts == BT_STA_HID_CONNECTED || stksts == BT_STA_HID_HFP_CONNECTED)
    {
        sts->SetWindowText(TEXT("Connected"));
        if(!lasthidiconnect) //new connection, add memo device
        {
            U8 idx = 0;
            idx = MemoAddBtDevice(bt_gap_get_remote_addr());
            MemoAttachName2BtDevice(bt_gap_get_remote_addr(), bt_gap_get_remote_name());
            ShowDeviceControls(SW_SHOW); //update paired list names
            PairSelect = idx;
            UpdateData(FALSE);
        }
    }
    else
    {
        if(stksts == BT_STA_CONNECTED || stksts == BT_STA_CONNECTING)
            sts->SetWindowText(TEXT("Connecting"));
        else if(stksts == BT_STA_DISCONNECT_RETRY || stksts == BT_STA_DISCONNECTING)
            sts->SetWindowText(TEXT("DisConnecting"));
        else
            sts->SetWindowText(TEXT("Disconnected"));
    }

    lasthidiconnect = (stksts == BT_STA_HID_CONNECTED || stksts == BT_STA_HID_HFP_CONNECTED); //to avoid update link key to many times

    if(stksts == BT_STA_HID_CONNECTED || stksts == BT_STA_HID_HFP_CONNECTED)
    {
        this->GetDlgItem(IDC_BUTTON_HID_CONNECT)->EnableWindow(TRUE);
        this->GetDlgItem(IDC_BUTTON_HID_CONNECT)->SetWindowText(TEXT("Disconnect"));
    }
    else if(stksts == BT_STA_DISCONNECTED)
    {
        this->GetDlgItem(IDC_BUTTON_HID_CONNECT)->EnableWindow(TRUE);
        this->GetDlgItem(IDC_BUTTON_HID_CONNECT)->SetWindowText(TEXT("Connect"));
    }
    else //intermedia state
        this->GetDlgItem(IDC_BUTTON_HID_CONNECT)->EnableWindow(FALSE);

#endif

}

void CTaxeiaBtStackDemoDlg::UpdateProfileStatus(U8 stksts)
{

#if (PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
    if(profile_type == PROFILE_TYPE_HID || profile_type == PROFILE_TYPE_HID_HFP)
#endif
        UpdateHIDProfileStatus(stksts);
#if (PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
    if(profile_type == PROFILE_TYPE_HFP || profile_type == PROFILE_TYPE_HID_HFP)
#endif
        UpdateHFPProfileStatus(stksts);
#if (PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
    if(profile_type == PROFILE_TYPE_SPP)
#endif
        UpdateSPPProfileStatus(stksts);

}

void CTaxeiaBtStackDemoDlg::UpdateGAPStatus(U8 stksts)
{

    CEdit *sts;
    //acl status
    sts = (CEdit*)this->GetDlgItem(IDC_EDIT_ACL_STAUS);
    if(stksts == BT_STA_CONNECTED || stksts == BT_STA_SPP_CONNECTED || stksts == BT_STA_HID_HFP_CONNECTED || stksts == BT_STA_HID_CONNECTED || stksts == BT_STA_HFP_CONNECTED)
    {
        sts->SetWindowText(TEXT("Connected"));
    }
    else
    {
        sts->SetWindowText(TEXT("Not connected"));
    }

}

#if (PROFILE_TYPE == PROFILE_TYPE_HFP || PROFILE_TYPE == PROFILE_TYPE_HID_HFP || PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)

U8 *PbNumber[1000];
U16 PbNumberCnt = 0;

U8 UTF82UCS2(U8 *utf8, U8 *ucs2, U8 len)
{

    U8 *src, *tgt;
    src = utf8;
    tgt = ucs2;

    while(len)
    {
        if((*src & 0xF0) == 0xE0) //0x800 ~ 0x10000
        {
            if(len > 2)
            {
                *tgt++ = ((src[1] & 0x3) << 6) | (src[2] & 0x3f);
                *tgt++ = ((src[0] & 0xf) << 4) | ((src[1] >> 2) & 0xf);
                src += 3;
                len -= 3;
            }
            else
                len = 0;
        }
        else if((*src & 0xE0) == 0xC0) //0x80 ~ 0x800
        {
            if(len > 1)
            {
                *tgt++ = ((src[0] & 0x3) << 6) | (src[1] & 0x3f);
                *tgt++ = ((src[0] >> 2) & 0x7);
                src += 2;
                len -=2;
            }
            else
                len = 0;
        }
        else if(*src < 0x80)
        {
            *tgt++ = src[0] & 0x7f;
            *tgt++ = 0x00;
            src += 1;
            len -= 1;
        }
        else
        {
            break;
        }
    }

    return (U8)(tgt - ucs2);

}

void PBItemCB(U16 idx, U8 *number, U8 *name)
{

    char wname[100];
    U8 len;
    int itemidx;

    if(idx == 0xffff)
    {
        return;
    }


    len = UTF82UCS2(name, (U8*)wname, strlen((char*)name));
    wname[len] = 0;
    wname[len+1] = 0;
    itemidx = TheDlg->m_HfpPbCombo.AddString((LPCTSTR)wname);

    PbNumber[PbNumberCnt] = (U8*)malloc(strlen((char*)number) + 1);
    strcpy((char*)PbNumber[PbNumberCnt], (char*)number);
    TheDlg->m_HfpPbCombo.SetItemData(itemidx, (DWORD_PTR)PbNumber[PbNumberCnt++]);

}

U8 SearchNamebyNumber(U8 *number, CString &name)
{
    U16 i, j;
    for(i = 0; i < PbNumberCnt; i++)
    {
        if(!strcmp((char*)PbNumber[i], (char*)number))
        {
            for(j = 0; j < TheDlg->m_HfpPbCombo.GetCount() ; j++)
            {
                if(TheDlg->m_HfpPbCombo.GetItemData(j) == (DWORD_PTR)PbNumber[i])
                {
                    //return name
                    TheDlg->m_HfpPbCombo.GetLBText(j, name);

                    return 1;
                }
            }

            return 0;

        }
    }

    return 0;
}
#endif

#if (PROFILE_TYPE == PROFILE_TYPE_SPP || PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
U8 spp_txbuf[512];

U8 device_info[] ={
0x4c, 0x4b, 0x00, 0x98, 0x4f, 0xf1, 0x01, 0x02, 0x30, 0x30, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x89, 0x79,
0x5f, 0x08, 0xff, 0x6c, 0x61, 0x6b, 0x61, 0x6c, 0x61, 0x5f, 0x74, 0x61, 0x69, 0x73, 0x6f, 0x6c,
0x5f, 0x30, 0x30, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x41, 0x75,
0x67, 0x20, 0x31, 0x34, 0x20, 0x32, 0x30, 0x31, 0x34, 0x20, 0x31, 0x2e, 0x36, 0x31, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x00, 0x02, 0x00, 0x08, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x00, 0x08, 0x00, 0x00, 0x2e, 0x71, 0x71, 0x2e,
0x05, 0xe2, 0x03, 0xf5};
 
void spp_dataincomming(U16 len, U8 * dat)
{

    if(!memcmp(dat, "\x4c\x4b\x00", 3))
	{
	    if(dat[5] == 0xf1 && dat[6] == 0x01)
    	    bt_spp_tx(sizeof(device_info), device_info);
	}
	else
	{
	    if(len > 512) len = 512;
	    memcpy(spp_txbuf, dat, len);
	    bt_spp_tx(len, spp_txbuf);  
	}
}

#endif

void CTaxeiaBtStackDemoDlg::UpdateHFPProfileStatus(U8 stksts)
{
    USES_CONVERSION;

    char indsts[120];

    char callsetupstr[50];

    U16 total, pidx;
    U8 sts_signal;
    U8 sts_batt;
    U8 sts_service;
    U8 sts_callsetup;
    U8 sts_call;


#if (PROFILE_TYPE == PROFILE_TYPE_HFP || PROFILE_TYPE == PROFILE_TYPE_HID_HFP || PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
    CEdit *sts;

    static U8 lasthfpiconnect = 0;

    sts = (CEdit*)this->GetDlgItem(IDC_EDIT_HFP_STATUS);
    if(stksts == BT_STA_HFP_CONNECTED || stksts == BT_STA_HID_HFP_CONNECTED)
    {
        sts->SetWindowText(TEXT("Connected"));
        if(!lasthfpiconnect) //new connection, add memo device
        {
            U8 idx = 0;
            idx = MemoAddBtDevice(bt_gap_get_remote_addr());
            MemoAttachName2BtDevice(bt_gap_get_remote_addr(), bt_gap_get_remote_name());
            ShowDeviceControls(SW_SHOW); //update paired list names
            PairSelect = idx;
            UpdateData(FALSE);
        }
    }
    else
    {
        if(stksts == BT_STA_CONNECTED || stksts == BT_STA_CONNECTING)
            sts->SetWindowText(TEXT("Connecting"));
        else if(stksts == BT_STA_DISCONNECT_RETRY || stksts == BT_STA_DISCONNECTING)
            sts->SetWindowText(TEXT("Disconnecting"));
        else
        {
            sts->SetWindowText(TEXT("Disconnected"));
        }
    }

    lasthfpiconnect = (stksts == BT_STA_HFP_CONNECTED || stksts == BT_STA_HID_HFP_CONNECTED); //to avoid update to many times

    if(stksts == BT_STA_HFP_CONNECTED || stksts == BT_STA_HID_HFP_CONNECTED)
    {
        this->GetDlgItem(IDC_BUTTON_HFP_CONNECT)->EnableWindow(TRUE);
        this->GetDlgItem(IDC_BUTTON_HFP_CONNECT)->SetWindowText(TEXT("Disconnect"));
        this->GetDlgItem(IDC_BUTTON_HFP_VENDOR_AT)->EnableWindow(TRUE);
    }
    else if(stksts == BT_STA_DISCONNECTED || stksts == BT_STA_HID_CONNECTED)
    {
        this->GetDlgItem(IDC_BUTTON_HFP_CONNECT)->EnableWindow(TRUE);
        this->GetDlgItem(IDC_BUTTON_HFP_CONNECT)->SetWindowText(TEXT("Connect"));
        this->GetDlgItem(IDC_BUTTON_HFP_VENDOR_AT)->EnableWindow(FALSE);
    }
    else //intermedia state
    {
        this->GetDlgItem(IDC_BUTTON_HFP_CONNECT)->EnableWindow(FALSE);
        this->GetDlgItem(IDC_BUTTON_HFP_VENDOR_AT)->EnableWindow(FALSE);
    }

#if 1
    bt_hfp_get_staus(&sts_signal, &sts_batt, &sts_service, &sts_callsetup, &sts_call);

    sprintf(indsts, "battery:\t%1x", sts_batt);
    sprintf(indsts, "%s\nservice:\t\t%s", indsts, sts_service ? "on" : "off");
    sprintf(indsts, "%s\nsignal:\t\t%1x", indsts, sts_signal);
    sprintf(indsts, "%s\ncall:\t\t%s", indsts, sts_call ? "online" : "idle");


    if(sts_callsetup == 1) //incomming call
    {
        CString name;
        static U8 instridx = 0;
        strcpy(callsetupstr, "===");
        callsetupstr[instridx++] = '<';
        if(instridx == 3) instridx = 0;

        name = "Unknown";

        if(bt_hfp_get_callingnumber()[0])
            if(!SearchNamebyNumber(bt_hfp_get_callingnumber(), name))
                name = bt_hfp_get_callingnumber();

        if(!hfsMBox)
        {
            hfsMBox = new CHFSMessageBox(this);
            hfsMBox->Create(CHFSMessageBox::IDD);
            hfsMBox->SetWindowText(TEXT("Incomming Call"));
            hfsMBox->SetTitleActionText(name, TEXT("Hand Up"));
            hfsMBox->ShowWindow(SW_SHOW);
        }
        else
        {
            hfsMBox->SetTitleActionText(name, TEXT("Hand Up"));
        }

    }
    else if(sts_callsetup == 2)
    {
        static U8 outidx = 0;
        strcpy(callsetupstr, "===");
        callsetupstr[outidx++] = '>'	;
        if(outidx == 3) outidx = 0;
    }
    else if(sts_callsetup == 3)
    {
        strcpy(callsetupstr, ">>>");
    }
    else
    {
        strcpy(callsetupstr, "idle");
        if(hfsMBox)
        {
            hfsMBox->DestroyWindow();
        }
    }

    sprintf(indsts, "%s\ncallsetup:\t%s", indsts, callsetupstr);

    if(bt_hfp_get_phonebook_status(&total, &pidx))
    {
        sprintf(indsts, "%s\n%d/%d", indsts, pidx, total);
        this->GetDlgItem(IDC_BUTTON_HFP_GETPB)->EnableWindow(FALSE);
    }
    else
        this->GetDlgItem(IDC_BUTTON_HFP_GETPB)->EnableWindow(TRUE);

    this->GetDlgItem(IDC_STATIC_HFP_IND_STATUS)->SetWindowText(A2W(indsts));

#endif
#endif

}

void CTaxeiaBtStackDemoDlg::UpdateSPPProfileStatus(U8 stksts)
{
    USES_CONVERSION;

#if (PROFILE_TYPE == PROFILE_TYPE_SPP || PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
    CEdit *sts;

    static U8 lastsppconnect = 0;

    sts = (CEdit*)this->GetDlgItem(IDC_EDIT_SPP_STATUS);
    if(stksts == BT_STA_SPP_CONNECTED)
    {
        sts->SetWindowText(TEXT("Connected"));
        if(!lastsppconnect) 
        {
			//new connection, add memo device
            U8 idx = 0;
            idx = MemoAddBtDevice(bt_gap_get_remote_addr());
            MemoAttachName2BtDevice(bt_gap_get_remote_addr(), bt_gap_get_remote_name());
            ShowDeviceControls(SW_SHOW); //update paired list names
            PairSelect = idx;
            UpdateData(FALSE);

			//Register spp data callback
			bt_spp_register_callback(spp_dataincomming);

			
        }
    }
    else
    {
        if(stksts == BT_STA_CONNECTED || stksts == BT_STA_CONNECTING)
            sts->SetWindowText(TEXT("Connecting"));
        else if(stksts == BT_STA_DISCONNECT_RETRY || stksts == BT_STA_DISCONNECTING)
            sts->SetWindowText(TEXT("Disconnecting"));
        else
        {
            sts->SetWindowText(TEXT("Disconnected"));
        }
    }

    lastsppconnect = (stksts == BT_STA_SPP_CONNECTED); //to avoid update to many times

    if(stksts == BT_STA_SPP_CONNECTED)
    {
        this->GetDlgItem(IDC_BUTTON_SPP_CONNECT)->EnableWindow(TRUE);
        this->GetDlgItem(IDC_BUTTON_SPP_CONNECT)->SetWindowText(TEXT("Disconnect"));
    }
    else if(stksts == BT_STA_DISCONNECTED)
    {
        this->GetDlgItem(IDC_BUTTON_SPP_CONNECT)->EnableWindow(TRUE);
        this->GetDlgItem(IDC_BUTTON_SPP_CONNECT)->SetWindowText(TEXT("Connect"));
    }
    else //intermedia state
    {
        this->GetDlgItem(IDC_BUTTON_SPP_CONNECT)->EnableWindow(FALSE);
    }

#endif

}

// CTaxeiaBtStackDemoDlg message handlers
void CTaxeiaBtStackDemoDlg::UpdateStatus(void)
{

    U8 stkst;
    static U8 last_stkst = 0xffffffff;
    CEdit *sts;

    sts = (CEdit*)this->GetDlgItem(IDC_EDIT_BTSTK_STATUS);

    stkst = bt_state();

    switch(stkst)
    {
        case BT_STA_STOP:
            sts->SetWindowText(TEXT("Please Plug Taxeia BT dongle ..."));
            break;
        case BT_STA_POWERUP:
            break;
        case BT_STA_BOOT:
            sts->SetWindowText(TEXT("Initializing"));
            break;
        case BT_STA_INITILIZED:
        case BT_STA_PAIRING:
        case BT_STA_CONNECTING:
        case BT_STA_CONNECTED:
        case BT_STA_HID_CONNECTED:
        case BT_STA_HFP_CONNECTED:
        case BT_STA_SPP_CONNECTED:
        case BT_STA_DISCONNECTING:
        case BT_STA_DISCONNECTED:
        case BT_STA_DISCONNECT_RETRY:
            sts->SetWindowText(TEXT("Initialized"));

            if(last_stkst == BT_STA_BOOT || last_stkst == BT_STA_POWERUP)
            {
                //show control
                ShowControls(SW_SHOW);
                //update scan mode
                ScanMode = bt_gap_get_scan_mode();
                SendMessage(WM_UPDATE_DATA, 0, 0);
            }
            else if(bt_gap_is_pincode_req())
            {
                CPinDialog cpin;
                U8 pin[100];
                cpin.DoModal();

                cpin.GetPinCode(pin);
                bt_gap_pincode_reply(pin + 1, pin[0]);
            }
            //periodically check scan mode
            else if(ScanMode != bt_gap_get_scan_mode())
            {
                ScanMode = bt_gap_get_scan_mode();
                SendMessage(WM_UPDATE_DATA, 0, 0);
            }

            break;
        case BT_STA_BT_FAIL:
            sts->SetWindowText(TEXT("BT Fail!!"));
            break;
        case BT_STA_LOCK:
            sts->SetWindowText(TEXT("Authorization expired, BT Lock, please reset HW!!"));
            break;
    }



    UpdateGAPStatus(stkst);
    UpdateProfileStatus(stkst);
    last_stkst = stkst;
}

#ifdef DYNAMIC_HID_DESCRIPTOR

#define HID_DESC_LEN 235
static U8 hid_desc[HID_DESC_LEN+11] = {
    
    0x36, ((HID_DESC_LEN + 8) >> 8), (HID_DESC_LEN+8) & 0xff,
    0x36, ((HID_DESC_LEN + 5) >> 8), (HID_DESC_LEN+5) & 0xff,
    0x08, 0x22, //uint8, hid descriptor type
    0x26, (HID_DESC_LEN >> 8), HID_DESC_LEN & 0xff,
   
    //235 bytes, hid descriptor
    0x05, 0x01,           /* Usage Page (Generic Desktop)      */ 
    0x09, 0x02,           /* Usage (Mouse)                     */ 
    0xA1, 0x01,           /* Collection (Application)         **/ 
    0x85, 0x02,           /* REPORT_ID (2)                     */ 
    0x09, 0x01,           /* Usage (Pointer)                   */ 
    0xA1, 0x00,           /* Collection (Physical)            **/ 
    0x05, 0x09,           /* Usage Page (Button)               */ 
    0x19, 0x01,           /* Usage Minimum (1)                 */ 
    0x29, 0x08,           /* Usage Maximum (8)                 */ 
    0x15, 0x00,           /* Logical Minimum (0)               */ 
    0x25, 0x01,           /* Logical Maximum (1)               */ 
    0x75, 0x01,           /* Report Size (1)                   */ 
    0x95, 0x08,           /* Report Count (8)                  */ 
    0x81, 0x02,           /* Input (Data, Variable, Absolute)  */ 
    0x05, 0x01,           /* Usage Page (Generic Desktop)      */ 
    0x09, 0x30,           /* Usage X                           */ 
    0x09, 0x31,           /* Usage Y                           */ 
    0x09, 0x38,           /* Usage Wheel                       */ 
    0x15, 0x81,           /* Logical Minimum (-127)            */ 
    0x25, 0x7F,           /* Logical Maximum (127)             */ 
    0x75, 0x08,           /* Report Size (8)                   */ 
    0x95, 0x03,           /* Report Count (3)                  */ 
    0x81, 0x06,           /* Input (Data, Variable, Absolute)  */ 
    0xC0, 0xC0,           /* End Collection                   **/ 
    0x05, 0x01,           /* Usage Page (Generic Desktop)      */ 
    0x09, 0x06,           /* Usage (Keyboard)                  */ 
    0xA1, 0x01,           /* Collection (Application)          */ 
    0x85, 0x01,           /* REPORT_ID (1)                     */ 
    0x75, 0x01,           /* Report Size (1)                   */ 
    0x95, 0x08,           /* Report Count (8)                  */ 
    0x05, 0x07,           /* Usage Page (Key codes)            */ 
    0x19, 0xE0,           /* Usage Minimum (224)               */ 
    0x29, 0xE7,           /* Usage Maximum (231)               */ 
    0x15, 0x00,           /* Logical Minimum (0)               */ 
    0x25, 0x01,           /* Logical Maximum (1)               */ 
    0x81, 0x02,           /* Input (Data, Variable, Absolute)  */ 
    0x95, 0x01,           /* Report Count (1)                  */ 
    0x75, 0x08,           /* Report Size (8)                   */ 
    0x81, 0x03,           /* Input (xxxxxx)                    */ 
    0x95, 0x06,           /* Report Count (6)                  */ 
    0x75, 0x08,           /* Report Size (8)                   */ 
    0x15, 0x00,           /* Logical Minimum (0)               */ 
    0x26, 0xFF, 0x00,     /* Logical Maximum (255)             */ 
    0x05, 0x07,           /* Usage Page (Key codes)            */ 
    0x19, 0x00,           /* Usage Minimum (0)                 */ 
    0x29, 0xFF,           /* Usage Maximum (255)               */ 
    0x81, 0x00,           /* Input (Data, Array)               */ 
    0xC0,                 /* END_COLLECTION                    */ 
    /*game pad 137+2 bytes*/
    0x05, 0x01,           /*Usage Page(Generic Desktop)*/
    0x09, 0x05,           /*Usage(Game Pad)*/
    0xa1, 0x01,           /*Collection(application)*/
    0x85, 0x04,           /*Report ID(4)*/
    0x15, 0x00,           /*Logical Minimum(0)*/
    0x25, 0x01,           /*Logical Maximum(1)*/
    0x35, 0x00,           /*Physical Minimum(0)*/
    0x45, 0x01,           /*Physical Maximum(1)*/
    0x75, 0x01,           /*Report Size(1)*/
    0x95, 0x0d,           /*Report Count(13)*/
    0x05, 0x09,           /*Usage Page(Button)*/
    0x19, 0x01,           /*Usage Minimum(1)*/
    0x29, 0x0d,           /*Usage Maximum(13)*/
    0x81, 0x02,           /*Input(Absolute, Variable, Data)*/
    0x95, 0x03,           /*Report Count(3)*/
    0x81, 0x01,           /*Input(Absolute, Array, Const)*/
    0x05, 0x01,           /*Usage Page(Generic Desktop)*/
    0x25, 0x07,           /*Logical Maximum(7)*/
    0x46, 0x3b, 0x01,     /*Physical Maximum(315)*/
    0x75, 0x04,           /*Report Size(4)*/
    0x95, 0x01,           /*Report Count(1)*/
    0x65, 0x14,           /*Unit(degrees)*/
    0x09, 0x39,           /*Usage(Hat switch)*/
    0x81, 0x42,           /*Input(Null, Absolute, Variable, Data)*/
    0x65, 0x00,           /*Unit(0)*/
    0x95, 0x01,           /*Report Count(1)*/
    0x81, 0x01,           /*Input(Absolute, Array, Const)*/
    0x26, 0xff, 0x00,     /*Logical Maximun(255)*/
    0x46, 0xff, 0x00,     /*Physical Maximum(255)*/
    0x09, 0x30,           /*Usage(X)*/
    0x09, 0x31,           /*Usage(Y)*/
    0x09, 0x32,           /*Usage(Z)*/
    0x09, 0x35,           /*Usage(Rz)*/
    0x75, 0x08,           /*Report Size(8)*/
    0x95, 0x04,           /*Report Count(4)*/
    0x81, 0x02,           /*Input(Absolute, Variable, Data)*/
    0x06, 0x00, 0xff,     /*Usage(vendor define)*/
    0x09, 0x20,           /*Usage(0x20)*/
    0x09, 0x21,           /*Usage(0x21)*/
    0x09, 0x22,           /*Usage(0x22)*/
    0x09, 0x23,           /*Usage(0x23)*/
    0x09, 0x24,           /*Usage(0x24)*/
    0x09, 0x25,           /*Usage(0x25)*/
    0x09, 0x26,           /*Usage(0x26)*/
    0x09, 0x27,           /*Usage(0x27)*/
    0x09, 0x28,           /*Usage(0x28)*/
    0x09, 0x29,           /*Usage(0x29)*/
    0x09, 0x2a,           /*Usage(0x2a)*/
    0x09, 0x2b,           /*Usage(0x2b)*/
    0x95, 0x0c,           /*Report Count(12)*/
    0x81, 0x02,           /*Input(Absolute, Variable, Data)*/
    0x0a, 0x21, 0x26,     /*Usage(0x2126)*/
    0x95, 0x08,           /*Report Count(8)*/
    0xb1, 0x02,           /*Feature(Absolute, Variable, Data)*/
    0x0a, 0x21, 0x26,     /*Usage(0x2126)*/
    0x91, 0x02,           /*Output(Absolute, Variable, Data)*/
    0x26, 0xff, 0x03,     /*Logical Maximun(0x3ff)*/
    0x46, 0xff, 0x03,     /*Physical Maximun(0x3ff)*/
    0x09, 0x2c,           /*Usage(0x2c)*/
    0x09, 0x2d,           /*Usage(0x2d)*/
    0x09, 0x2e,           /*Usage(0x2e)*/
    0x09, 0x2f,           /*Usage(0x2f)*/
    0x75, 0x10,           /*Report Size(16)*/
    0x95, 0x04,           /*Report Count(4)*/
    0x81, 0x02,           /*Input(Absolute, Variable, Data)*/
    0xc0                  /*End Collections*/

};
//tplchange end
#endif

BOOL CTaxeiaBtStackDemoDlg::OnInitDialog()
{
    CDialog::OnInitDialog();

    // Add "About..." menu item to system menu.

    // IDM_ABOUTBOX must be in the system command range.
    ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
    ASSERT(IDM_ABOUTBOX < 0xF000);

    CMenu* pSysMenu = GetSystemMenu(FALSE);
    if (pSysMenu != NULL)
    {
        CString strAboutMenu;
        strAboutMenu.LoadString(IDS_ABOUTBOX);
        if (!strAboutMenu.IsEmpty())
        {
            pSysMenu->AppendMenu(MF_SEPARATOR);
            pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
        }
    }

    // Set the icon for this dialog.  The framework does this automatically
    //  when the application's main window is not a dialog
    SetIcon(m_hIcon, TRUE);			// Set big icon
    SetIcon(m_hIcon, FALSE);		// Set small icon

    // TODO: Add extra initialization here
    TheDlg = this;
    bt_init();
#ifdef DYNAMIC_HID_DESCRIPTOR
    bt_hid_set_hid_descriptor(hid_desc);  //tplchange
#endif	

#if (PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
    this->SetWindowText(TEXT("Dynamic Profiles Demo"));
    this->GetDlgItem(IDC_STATIC_PSEL)->ShowWindow(SW_SHOW);
    this->GetDlgItem(IDC_RADIO_PHID)->ShowWindow(SW_SHOW);
    this->GetDlgItem(IDC_RADIO_PHFP)->ShowWindow(SW_SHOW);
    this->GetDlgItem(IDC_RADIO_PBOTH)->ShowWindow(SW_SHOW);
#elif (PROFILE_TYPE == PROFILE_TYPE_HID)
    this->SetWindowText(TEXT("HID Profile Demo"));
#elif (PROFILE_TYPE == PROFILE_TYPE_HFP)
    this->SetWindowText(TEXT("HFP Profile Demo"));
#elif (PROFILE_TYPE == PROFILE_TYPE_SPP)
    this->SetWindowText(TEXT("SPP Profile Demo"));
#elif (PROFILE_TYPE == PROFILE_TYPE_HID_HFP)
    this->SetWindowText(TEXT("HID_HFP Profile Demo"));
#endif


    //status timer
    SetTimer(ID_STATUS_TIMER, 1000, NULL);

#ifdef TAXEIA_UI_LOG
    StartHciLog(AfxGetInstanceHandle(), SW_SHOW);
#endif

    RunBtThread(0);
    return TRUE;  // return TRUE  unless you set the focus to a control
}

void CTaxeiaBtStackDemoDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
    if ((nID & 0xFFF0) == IDM_ABOUTBOX)
    {
        CAboutDlg dlgAbout;
        dlgAbout.DoModal();
    }
    else
    {
        CDialog::OnSysCommand(nID, lParam);
    }
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CTaxeiaBtStackDemoDlg::OnPaint()
{
    if (IsIconic())
    {
        CPaintDC dc(this); // device context for painting

        SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

        // Center icon in client rectangle
        int cxIcon = GetSystemMetrics(SM_CXICON);
        int cyIcon = GetSystemMetrics(SM_CYICON);
        CRect rect;
        GetClientRect(&rect);
        int x = (rect.Width() - cxIcon + 1) / 2;
        int y = (rect.Height() - cyIcon + 1) / 2;

        // Draw the icon
        dc.DrawIcon(x, y, m_hIcon);
    }
    else
    {
        CDialog::OnPaint();
    }
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CTaxeiaBtStackDemoDlg::OnQueryDragIcon()
{
    return static_cast<HCURSOR>(m_hIcon);
}

BOOL CTaxeiaBtStackDemoDlg::OnDeviceChange(UINT nEventType, DWORD_PTR dwData)
{
    PDEV_BROADCAST_HDR pHdr = (PDEV_BROADCAST_HDR)dwData;


    if (nEventType == DBT_DEVICEARRIVAL) {
        //SetTimer(IDT_ADD_DEVICE, 100, NULL);
        OutputDebugStringA("DBT_DEVICEARRIVAL\n");

        if(pHdr->dbch_devicetype == DBT_DEVTYP_DEVICEINTERFACE) {
            OutputDebugStringA("DBT_DEVTYP_DEVICEINTERFACE\n");
        } else if(pHdr->dbch_devicetype == DBT_DEVTYP_VOLUME) {
            OutputDebugStringA("DBT_DEVTYP_VOLUME\n");
        }

        if(pHdr->dbch_devicetype == DBT_DEVTYP_VOLUME) {
            if(RunBtThread(2)) RunBtThread(0);
        }

    } else  if (nEventType == DBT_DEVICEREMOVECOMPLETE) {
        OutputDebugStringA("DBT_DEVICEREMOVECOMPLETE\n");

        if(pHdr->dbch_devicetype == DBT_DEVTYP_DEVICEINTERFACE) {
            OutputDebugStringA("DBT_DEVTYP_DEVICEINTERFACE\n");
        }
        else if(pHdr->dbch_devicetype == DBT_DEVTYP_VOLUME)
        {
            OutputDebugStringA("DBT_DEVTYP_VOLUME\n");
            if(!RunBtThread(2)) //if our hw unplug, it could be someone else
            {
                RunBtThread(1); //try stop BT
                ShowControls(SW_HIDE);
            }
        }

    } else {
        OutputDebugStringA("OnDeviceChange\n");
    }

    return true;
    return TRUE;
}

void CTaxeiaBtStackDemoDlg::OnTimer(UINT_PTR nIDEvent)
{
    // TODO: Add your message handler code here and/or call default

    CDialog::OnTimer(nIDEvent);

    if(nIDEvent == ID_STATUS_TIMER)
    {
        UpdateStatus();
    }
}

void CTaxeiaBtStackDemoDlg::OnBnClickedRadioNoScan()
{
    // TODO: Add your control notification handler code here
    bt_gap_set_scan_mode(GetTickCount(), BT_WMODE_NO_SCAN);
}

void CTaxeiaBtStackDemoDlg::OnBnClickedRadioPageScan()
{
    // TODO: Add your control notification handler code here
    bt_gap_set_scan_mode(GetTickCount(), BT_WMODE_PAGING_SCAN);
}

void CTaxeiaBtStackDemoDlg::OnBnClickedRadioInqPageScan()
{
    // TODO: Add your control notification handler code here
    bt_gap_set_scan_mode(GetTickCount(), BT_WMODE_INQUIRY_SCAN);
}

LRESULT CTaxeiaBtStackDemoDlg::UpdateDat(WPARAM wParam, LPARAM lParam)
{
    UpdateData(FALSE);
    return 0;
}

LRESULT CTaxeiaBtStackDemoDlg::HFSMBoxEnd(WPARAM wParam, LPARAM lParam)
{
    hfsMBox = NULL;
    return 0;
}

LRESULT CTaxeiaBtStackDemoDlg::HFSMBoxAction(WPARAM wParam, LPARAM lParam)
{
#if (PROFILE_TYPE == PROFILE_TYPE_HFP || PROFILE_TYPE == PROFILE_TYPE_HID_HFP || PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
    Bt__ProfileHFPHandUp();
    hfsMBox->DestroyWindow();
#endif

    return 0;
}


void CTaxeiaBtStackDemoDlg::OnMouseMove(UINT nFlags, CPoint point)
{
    // TODO: Add your message handler code here and/or call default
    static CPoint last_point;
    CDialog::OnMouseMove(nFlags, point);

    char mousestr[100];

#if (PROFILE_TYPE == PROFILE_TYPE_HID || PROFILE_TYPE == PROFILE_TYPE_HID_HFP || PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
#if (PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
    if(profile_type == PROFILE_TYPE_HID || profile_type == PROFILE_TYPE_HID_HFP) //HID
    {
#endif
        mouse_data[0] = point.x-last_point.x;
        mouse_data[1] = point.y-last_point.y;
#if 0
        bt_hid_send_mouse();
#endif
#if (PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
    }
#endif
#endif
    last_point = point;

}

void CTaxeiaBtStackDemoDlg::OnBnClickedButtonHidConnect()
{
    U8 addr[6];
    U8 name[26]; //tmp

    // TODO: Add your control notification handler code here
#if (PROFILE_TYPE == PROFILE_TYPE_HID || PROFILE_TYPE == PROFILE_TYPE_HID_HFP || PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
#if (PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
    if(profile_type == PROFILE_TYPE_HID || profile_type == PROFILE_TYPE_HID_HFP) //HID
    {
#endif
        UpdateData(TRUE);
        if(MemoFindBtDevice(PairSelect, addr, name))
        {
            if(this->GetDlgItem(IDC_BUTTON_HID_CONNECT)->GetWindowTextLength() == 7) //Connect
            {
				static BYTE shit = 0;
				if(shit == 0)
				{
				    bt_set_mcu_sleep_time(180*1000);
				    shit = 1;
				}
/*				else if(shit == 1)
				{
					bt_reset_mcu_sleep_time();
					shit = 2;

				}*/
                bt_gap_connect(addr);
            }
            else //disconnect
            {
                bt_gap_disconnect();
            }
        }
#if (PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
    }
#endif
#endif
}

void CTaxeiaBtStackDemoDlg::OnBnClickedRadioPhid()
{
    // TODO: Add your control notification handler code here
    UpdateData(TRUE); //read data from controls
    if(profile_select ==  0)
        profile_type = PROFILE_TYPE_HID;
    else if(profile_select == 1)
        profile_type = PROFILE_TYPE_HFP;
    else
        profile_type = PROFILE_TYPE_HID_HFP;

    UpdateData(TRUE);

    if(RunBtThread(2))
    {
        RunBtThread(1); //try stop BT
        ShowControls(SW_HIDE);
        RunBtThread(0); //try start BT
    }
}

void CTaxeiaBtStackDemoDlg::OnBnClickedButtonHfpHandup()
{
    // TODO: Add your control notification handler code here
#if (PROFILE_TYPE == PROFILE_TYPE_HFP || PROFILE_TYPE == PROFILE_TYPE_HID_HFP || PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
    USES_CONVERSION;
    CEdit *edit;
    wchar_t buf[50];
    edit = (CEdit*)this->GetDlgItem(IDC_EDIT_VENDOR_AT);
    edit->GetWindowText(buf, 50);
    Bt__ProfileSendVendor(W2A(buf));
#endif

}


void CTaxeiaBtStackDemoDlg::OnBnClickedButtonHfpConnect()
{
    U8 addr[6];
    U8 name[26]; //tmp

    // TODO: Add your control notification handler code here
#if (PROFILE_TYPE == PROFILE_TYPE_HFP || PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
#if (PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
    if(profile_type == PROFILE_TYPE_HFP) //HFP
    {
#endif
        UpdateData(TRUE);
        if(MemoFindBtDevice(PairSelect, addr, name))
        {
            if(this->GetDlgItem(IDC_BUTTON_HFP_CONNECT)->GetWindowTextLength() == 7) //Connect
            {
                bt_gap_connect(addr);
            }
            else
                bt_gap_disconnect();
        }

#if (PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
    }
    else if(profile_type == PROFILE_TYPE_HID_HFP)
    {
        if(this->GetDlgItem(IDC_BUTTON_HFP_CONNECT)->GetWindowTextLength() == 7)
            bt_hfp_connect();
        else
            bt_hfp_disconnect();
    }
#endif
#endif

}


void CTaxeiaBtStackDemoDlg::OnBnClickedButtonHfpGetpb()
{
    // TODO: Add your control notification handler code here
#if (PROFILE_TYPE == PROFILE_TYPE_HFP || PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
    U16 i;
    m_HfpPbCombo.ResetContent();
    for(i = 0; i < PbNumberCnt; i++)
    {
        free(PbNumber[i]);
    }
    PbNumberCnt = 0;

    bt_hfp_get_phonebook(PBItemCB);
#endif
}

void CTaxeiaBtStackDemoDlg::OnCbnSelchangeComboHfpPb()
{
    // TODO: Add your control notification handler code here
    USES_CONVERSION;
    int idx;
    U8 *number;
    CString name;

    idx = m_HfpPbCombo.GetCurSel();
    number = (U8*) m_HfpPbCombo.GetItemData(idx);
    m_HfpPbCombo.GetLBText(idx, name);
    MessageBox(A2W((char*)number), name);
}

void CTaxeiaBtStackDemoDlg::OnCancel()
{
    // TODO: Add your specialized code here and/or call the base class
    U16 i;
    CDialog::OnCancel();

#if (PROFILE_TYPE == PROFILE_TYPE_HFP || PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
    m_HfpPbCombo.ResetContent();
    for(i = 0; i < PbNumberCnt; i++)
        free(PbNumber[i]);

    if(hfsMBox)
    {
        hfsMBox->DestroyWindow();
    }
#endif
}

U8 ps3_data_get_report(U8 type, U8 report,
                       U8 *buf, int maxlen)
{
    ASSERT(0);
    return 0;
}

U8 ps3_data_set_report(U8 type, U8 report, const U8 *buf, int len)
{
    ASSERT(0);
    return 0;
}

U8 pad_data_get_report(U8 report_id, U8 len, U8 *dat)
{
    if(report_id == 4) //game pad
    {
        dat[0] = 0;
        dat[1] = 0;
        dat[2] = 0x08; //hat
        dat[3] = 0x80; //lx
        dat[4] = 0x80; //ly
        dat[5] = 0x80; //rx
        dat[6] = 0x80; //ry
        dat[7] = 0; //right
        dat[8] = 0; //left
        dat[9] = 0; //up
        dat[10] = 0; //down
        dat[11] = 0; //tri
        dat[12] = 0; //cir
        dat[13] = 0; //cro
        dat[14] = 0; //squ
        dat[15] = 0; //l1
        dat[16] = 0; //l2
        dat[17] = 0; //r1
        dat[18] = 0; //r2
        dat[19] = 0; //acclx_l
        dat[20] = 0; //acclx_h
        dat[21] = 0; //acclz_l
        dat[22] = 0; //acclz_h
        dat[23] = 0; //accly_l
        dat[24] = 0; //accly_h
        dat[25] = 0; //acclw_l
        dat[26] = 0; //acclw_h
        return 27;
    }
    else if(report_id == 1) //keyboard
    {
        dat[0] = 0;  //modifier
        dat[1] = 0;  //reseved
        dat[2] = 0;  //keycode 0
        dat[3] = 0; //'P' //keycode 1
        dat[4] = 0;   //keycode 2
        dat[5] = 0;   //keycode 3
        dat[6] = 0;   //keycode 4
        dat[7] = 0;   //keycode 5
        return 8;
    }
    else if(report_id == 2) //mouse
    {
        dat[0] = 0; //buttons
        dat[1] = mouse_data[0]; //x
        dat[2] = mouse_data[1]; //y
        dat[3] = 0; //z

        mouse_data[0] = 0;
        mouse_data[1] = 0;
        return 4;
    }

    return 0;
}

void CTaxeiaBtStackDemoDlg::OnBnClickedButtonSppConnect()
{
	// TODO: Add your control notification handler code here
    U8 addr[6];
    U8 name[26]; //tmp

    // TODO: Add your control notification handler code here
#if (PROFILE_TYPE == PROFILE_TYPE_SPP || PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
#if (PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
    if(profile_type == PROFILE_TYPE_SPP) //SPP
    {
#endif
        bt_gap_connect((U8*)"\x56\x34\x12\x78\xBC\x9A");
//          bt_gap_connect((U8*)"\x01\x00\x00\x00\x04\x23");
        return;
        UpdateData(TRUE);
        if(MemoFindBtDevice(PairSelect, addr, name))
        {
            if(this->GetDlgItem(IDC_BUTTON_SPP_CONNECT)->GetWindowTextLength() == 7) //Connect
            {
                bt_gap_connect(addr);
            }
            else
                bt_gap_disconnect();
        }

#if (PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
    }
#endif
#endif


}

extern "C" {
	void Bt__EnterDUT(void);
};

void CTaxeiaBtStackDemoDlg::OnBnClickedRadioInqScan()
{
	//static U8 dat[1] = {0x00};
	
	// TODO: Add your control notification handler code here
   // Bt__MT6622_SET_TRIM(dat[0]);
	//dat[0]++;
	
    Bt__EnterDUT();

}
