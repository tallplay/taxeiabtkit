#ifndef __HIDCONTROL_HH_
#define __HIDCONTROL_HH_
#include "BtType.h"
extern "C"
{
    #include "hidpd\Bt__Profile_HIDD.h"
    #include "GAP\LOCAL_DEV\Bt__LocalDevice.h"
    #include "GAP\LL\Bt__RemoteDevice.h"
}
U8 * UpdateHidInfo(U16 parm);
U8 * HIDACLConnectCB(void);
void HIDCommonInit(void);
void HIDConnectDisconnect(U8*);
U8 HIDConnectStatus(void);
U8 HIDIsConnectDisconnectOnGoing();
void HIDSendMouse(U8 btn, U8 x, U8 y, U8 z);
void HIDSendKeyCode(U8 modifier, U8 *keycode, U8 len);
void HIDSendMedia(U8 *media);

#endif
