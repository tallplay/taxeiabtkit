#include "HFPControl.h"
#include "GAPControl.h"
#include <stdio.h>
#include <string.h>

#if (PROFILE_TYPE == PROFILE_TYPE_HFP || PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
struct
{
	U16 outgoing : 1;
	U16 phbook : 1;
	U16 reserved : 14;
	U16 phbooknum;
	U16 phbookidx;
}hfpinfo;

U8 callingnumber[20];

U8 * HFPACLConnectCB(void)
{
	if(GAPIsAclConnected())
	{
		if(hfpinfo.outgoing)
		    Bt__WriteAuthReq(GAPGetAclHandle());
	}
	else
	{
	    if(hfpinfo.outgoing)
			hfpinfo.outgoing = 0;
	    Bt__WriteScanEanbled(2); //paging scan		
	}
	return 0;
}

//0: not connect 1: connect 2: connecting
U8 HFPConnectStatus(void)
{
    return Bt__ProfileHFPConnectStatus();
}

void HFPHFPStatus(U8 *signal, U8 *battery, U8 *service, U8 *callsetup, U8 *call)
{
    *signal = Bt__ProfileHFPStatus(IND_SIGNAL);
    *battery = Bt__ProfileHFPStatus(IND_BATTARY);
    *service = Bt__ProfileHFPStatus(IND_SERVICE);
    *callsetup = Bt__ProfileHFPStatus(IND_CALLSETUP);
    *call = Bt__ProfileHFPStatus(IND_CALL);

	if(*callsetup == 0)
		callingnumber[0] = 0;
}

U8 *HFSCallingNumber()
{
    return callingnumber;
}

U8 HFPIsConnectDisconnectOnGoing()
{
    return hfpinfo.outgoing;
}

static void(*PhoneBookCB)(U16 idx, U8* num, U8 *name);

static U8 *HFPCB (U16 parm)
{
    if(parm == HFPCB_PBITEM)
	{
	    U8 *ptr;
	    int idx;
		U8 *num;
		U8 *name;
		ptr = Bt__ProfileHFPGetCallBackBuf();

		idx = 0x1;
	    sscanf((char*)ptr, "%d,", &idx);

        /* mark number */
		while(*ptr++ != '"');//find begin "
		num = ptr;
		while(*++ptr != '"');//find end "
		*ptr = 0; //mark end

		/* mark name */
		while(*ptr++ != '"');//find begin "
		name = ptr;
		if(name[0] == '"') //null
		{
		    name[0] = 0;
		}
		else
	    {
			while(*++ptr != '"');//find end "
			*ptr = 0; //mark end
		}

		if(PhoneBookCB)
		{
			hfpinfo.phbookidx = idx;
			PhoneBookCB((U16)idx, num, name);
			if(!Bt__ProfileHFPGetPhoneBook((U16)idx+1))
			{
				hfpinfo.phbook = 0;
				PhoneBookCB(0xffff, 0, 0); //end, no phone book
			}
		}
		
	}

	if(parm == HFPCB_PBNUMBER)
	{
	    strcpy((char*)callingnumber, (char*)Bt__ProfileHFPGetCallBackBuf());
	}

	if(parm == 2) //l2cap disconnected
	{
	    if(hfpinfo.outgoing)
    	{
    	    Bt__DisconnRst(GAPGetAclHandle(), 0x13, 0);
    	}
	}
	
	return 0;
}

void HFPCommonInit(void)
{
	hfpinfo.outgoing = 0;
	hfpinfo.phbook = 0;
	Bt__ProfileHFPCB(HFPCB);
	PhoneBookCB = 0;
}

U8 HFPGetPhoneBookStatus(U16 *total, U16 *idx)
{
    *total = hfpinfo.phbooknum;
	*idx = hfpinfo.phbookidx;
    return hfpinfo.phbook;
}

void HFPGetPhoneBook(void(*pbitemcb)(U16 idx, U8* num, U8 *name))
{
    U16 ret;
	ret = Bt__ProfileHFPGetPhoneBook(1);
    if(!ret)
		pbitemcb(0xffff, 0, 0); //end, no phone book
	else
	{
		hfpinfo.phbook = 1;
		hfpinfo.phbooknum = ret;
		hfpinfo.phbookidx = 0;
		PhoneBookCB = pbitemcb;
	}
}

void HFPConnectDisconnect(U8 *bdaddr)
{

    if(!GAPIsAclConnected() && (Bt__ProfileHFPConnectStatus() == 0))
    {
    	hfpinfo.outgoing = 1;
	//	//Bt__SetBtWakeup();
		Bt__ConnRst(bdaddr, 0);
    }
	else if(GAPIsAclConnected() && (Bt__ProfileHFPConnectStatus() == 1))
	{
	    hfpinfo.outgoing = 1;
		Bt__HFPDisconnect();
	}

}

#endif


