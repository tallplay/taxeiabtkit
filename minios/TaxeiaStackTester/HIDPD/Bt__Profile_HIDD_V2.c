#include "BtConfig.h"
#include "Bt__Profile_HIDD_V2.h"
#include "../GAP/L2CAP/Bt__LLCAP_V2.h"
#include "../TaxeiaConsoleDebug/TaxeiaConsoledebug.h"
#include "../SDP/SDPS/Bt__Profile_SDPS.h"
#include "../CORE/BTCore_V2.h"
#include "pad_data/ps3-data.h"
#include <stdlib.h>

#if  (PROFILE_TYPE == PROFILE_TYPE_HID_HFP || PROFILE_TYPE == PROFILE_TYPE_HID || PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
///////////////////////////////////Public API///////////////////////////////////////////

///////////////////////////////////Private Functions////////////////////////////////////
/* Human Interface Device Profile */
#define PROF_HID 0x1124
#define PROF_DID 0x1200

#define PROT_L2CAP                   0x0100
#define PROT_HIDP                    0x0011
#define PROT_SDP                     0x0001

/* Value for the HID Control Channel */
#define BT_PSM_HID_CTRL         0x0011
/* Value for the HID Interrupt Channel */
#define BT_PSM_HID_INTR         0x0013
#define BT_PSM_SDP              0x0001

static const U8 HidRecordHandle [] = {
    SDP_UINT_SIZE32(0x00010000)
};

static const U8 HidRecordState [] = {
    SDP_UINT_SIZE32(0x20121118)
};

static const U8 HidServClassIdVal[] = {
    SDP_DATA_ELEMENT_SEQ_SIZE8(3),           /* Data element sequence, 3 bytes */
    SDP_UUID_SIZE16(PROF_HID), /* Uuid16 HID */
};


/* Attribute value of Hid Protocol Descriptor List. */
static const U8 HidProtocolDescListVal[] = {
    SDP_DATA_ELEMENT_SEQ_SIZE8(13),  /* Data element sequence, 12 bytes */
    SDP_DATA_ELEMENT_SEQ_SIZE8(6),   /* Data element sequence for L2CAP, 3 bytes */     
    SDP_UUID_SIZE16(PROT_L2CAP),  /* Uuid16 L2CAP */
    SDP_UINT_SIZE16(BT_PSM_HID_CTRL),  /* Uuid16 PSM */    
    SDP_DATA_ELEMENT_SEQ_SIZE8(3),   /* Data element sequence for RFCOMM, 5 bytes */
    SDP_UUID_SIZE16(PROT_HIDP),  /* Uuid16 HID protocol */};

/* Attribute value of Hid Language Base ID List */
static const U8 HidLangBaseVal[] = {
    SDP_DATA_ELEMENT_SEQ_SIZE8(9),   /* Data element sequence, 9 bytes */
    SDP_UINT_SIZE16(0x656E),      /* Uint16 "en" (English) */
    SDP_UINT_SIZE16(0x006A),      /* Uint16 UTF-8 encoding */
    SDP_UINT_SIZE16(0x0100)       /* Uint16 primary language base ID */
};

/* Attribute value of Hid Bluetooth Profile Descriptor List */
static const U8 HidProfileDescListVal[] = {
    SDP_DATA_ELEMENT_SEQ_SIZE8(8),           /* Data element sequence, 8 bytes */
    SDP_DATA_ELEMENT_SEQ_SIZE8(6),           /* Data element sequence for ProfileDescriptor, 6 bytes */
    SDP_UUID_SIZE16(PROF_HID),          /* Uuid16 Hid */
    SDP_UINT_SIZE16(0x0100)               /* Uint16 version number */
};

/* Attribute value of Hid Protocol Descriptor List. */
static const U8 HidAdditionalProtocolDescListVal[] = {
    SDP_DATA_ELEMENT_SEQ_SIZE8(15),  /* Data element sequence, 15 bytes */
    SDP_DATA_ELEMENT_SEQ_SIZE8(13),  /* Data element sequence, 15 bytes */
    SDP_DATA_ELEMENT_SEQ_SIZE8(6),   /* Data element sequence for L2CAP, 3 bytes */     
    SDP_UUID_SIZE16(PROT_L2CAP),  /* Uuid16 L2CAP */
    SDP_UINT_SIZE16(BT_PSM_HID_INTR),  /* Uuid16 PSM */    
    SDP_DATA_ELEMENT_SEQ_SIZE8(3),   /* Data element sequence for RFCOMM, 5 bytes */
    SDP_UUID_SIZE16(PROT_HIDP),  /* Uuid16 HID protocol */};


/*---------------------------------------------------------------------------
 * * OPTIONAL *  ServiceName
 *
 * This is the English string.  Other languguages can be defined.
 */
#ifndef CUSTOMIZE_SDP_HID_DESCRIPTOR
#define HID_SERVICENAME \
    'B', 'T', ' ', 'T', 'a', 'x', 'e', 'i', 'a', ' ', 'H','I', \
    'D', ' ', 'K', 'M', 'S', 'H' , '\0'\
    
#endif
static const U8 Hid_ServiceName[] = {
    SDP_TEXT_SIZE8(19),          /* Null terminated text string */
    HID_SERVICENAME
};

/*---------------------------------------------------------------------------
 * * OPTIONAL *  ServiceDescription
 *
 * This is the English string.  Other languguages can be defined.
 */
static const U8 Hid_ServiceDescription[] = {
    SDP_TEXT_SIZE8(17),          /* Null terminated text string */
    'S', 'h', 'a', 'r', 'e', 'K', 'e', 'y', 'b', 'o', 'a', 
    'r', 'd', 'M', 'o', 'u', '\0'
};

/*---------------------------------------------------------------------------
 * * OPTIONAL *  ProviderName
 *
 * This is the English string.  Other languguages can be defined.
 */
#ifndef CUSTOMIZE_SDP_HID_DESCRIPTOR
#define HID_PROVIDERNAME \
    'T', 'a', 'x', 'e', 'i', 'a', ' ', ' ', '\0' \

#endif
    
static const U8 Hid_ProviderName[] = {
    SDP_TEXT_SIZE8(9),          /* Null terminated text string */
    HID_PROVIDERNAME
};

/*---------------------------------------------------------------------------
 * Device Release Number.
 */
#define SDP_HID_DEVICE_RELEASE 0x0100
static const U8 Hid_DeviceRelease[] = {
    SDP_UINT_SIZE16(SDP_HID_DEVICE_RELEASE)
};

/*---------------------------------------------------------------------------
 * Parser Version.
 */
#define SDP_HID_PARSER_VERSION 0x0111
static const U8 Hid_ParserVersion[] = {
    SDP_UINT_SIZE16(SDP_HID_PARSER_VERSION)
};

/*---------------------------------------------------------------------------
 * Device Subclass.
 */
static const U8 Hid_DeviceSubclass[] = {
    SDP_UINT_SIZE8(SDP_HID_DEVICE_SUBCLASS)
};

/*---------------------------------------------------------------------------
 * Country Code.
 */
#define SDP_HID_COUNTRY_CODE 0x21
static const U8 Hid_Countrycode[] = {
    SDP_UINT_SIZE8(SDP_HID_COUNTRY_CODE)
};

/*---------------------------------------------------------------------------
 * Virtual Cable.
 */
#define SDP_HID_VIRTUAL_CABLE TRUE
static const U8 Hid_VirtualCable[] = {
    SDP_BOOL_SIZE8(SDP_HID_VIRTUAL_CABLE)
};

/*---------------------------------------------------------------------------
 * Initiate Reconnect.
 */
#define SDP_HID_RECONNECT_INITIATE TRUE
static const U8 Hid_ReconnectInitiate[] = {
    SDP_BOOL_SIZE8(SDP_HID_RECONNECT_INITIATE)
};

/*---------------------------------------------------------------------------
 * Descriptor List.
 */

#define SDP_HID_DESCRIPTOR_TYPE 0x22

#ifndef CUSTOMIZE_SDP_HID_DESCRIPTOR
#define VID 0x19c6
#define PID 0xffff
#define REV 0x0213

#define SDP_HID_DESCRIPTOR_LEN (139+25)  

#define SDP_HID_DESCRIPTOR \
0x05, 0x01,           /* Usage Page (Generic Desktop)      */ \
0x09, 0x02,           /* Usage (Mouse)                     */ \
0xA1, 0x01,           /* Collection (Application)         **/ \
0x85, 0x02,           /* REPORT_ID (2)                     */ \
0x09, 0x01,           /* Usage (Pointer)                   */ \
0xA1, 0x00,           /* Collection (Physical)            **/ \
0x05, 0x09,           /* Usage Page (Button)               */ \
0x19, 0x01,           /* Usage Minimum (1)                 */ \
0x29, 0x08,           /* Usage Maximum (8)                 */ \
0x15, 0x00,           /* Logical Minimum (0)               */ \
0x25, 0x01,           /* Logical Maximum (1)               */ \
0x75, 0x01,           /* Report Size (1)                   */ \
0x95, 0x08,           /* Report Count (8)                  */ \
0x81, 0x02,           /* Input (Data, Variable, Absolute)  */ \
0x05, 0x01,           /* Usage Page (Generic Desktop)      */ \
0x09, 0x30,           /* Usage X                           */ \
0x09, 0x31,           /* Usage Y                           */ \
0x09, 0x38,           /* Usage Wheel                       */ \
0x15, 0x81,           /* Logical Minimum (-127)            */ \
0x25, 0x7F,           /* Logical Maximum (127)             */ \
0x75, 0x08,           /* Report Size (8)                   */ \
0x95, 0x03,           /* Report Count (3)                  */ \
0x81, 0x06,           /* Input (Data, Variable, Absolute)  */ \
0xC0, 0xC0,           /* End Collection                   **/ \
                      /* End Collection                   **/ \
0x05, 0x01,           /* Usage Page (Generic Desktop)      */ \
0x09, 0x06,           /* Usage (Keyboard)                  */ \
0xA1, 0x01,           /* Collection (Application)          */ \
0x85, 0x01,           /* REPORT_ID (1)                     */ \
0x75, 0x01,           /* Report Size (1)                   */ \
0x95, 0x08,           /* Report Count (8)                  */ \
0x05, 0x07,           /* Usage Page (Key codes)            */ \
0x19, 0xE0,           /* Usage Minimum (224)               */ \
0x29, 0xE7,           /* Usage Maximum (231)               */ \
0x15, 0x00,           /* Logical Minimum (0)               */ \
0x25, 0x01,           /* Logical Maximum (1)               */ \
0x81, 0x02,           /* Input (Data, Variable, Absolute)  */ \
0x95, 0x01,           /* Report Count (1)                  */ \
0x75, 0x08,           /* Report Size (8)                   */ \
0x81, 0x03,           /* Input (xxxxxx)                    */ \
0x95, 0x05,           /* Report Count (5)                  */ \
0x75, 0x01,           /* Report Size (1)                   */ \
0x05, 0x08,           /* Usage Page (LED)                  */ \
0x19, 0x01,           /* Usage Minimum (1)                 */ \
0x29, 0x05,           /* Usage Maximum (5)                 */ \
0x91, 0x02,           /* Output (Data, Variable, Absolute) */ \
0x95, 0x01,           /* Report Count (1)                  */ \
0x75, 0x03,           /* Report Size (3)                   */ \
0x91, 0x03,           /* Output (Constant)                 */ \
0x95, 0x06,           /* Report Count (6)                  */ \
0x75, 0x08,           /* Report Size (8)                   */ \
0x15, 0x00,           /* Logical Minimum (0)               */ \
0x26, 0xFF, 0x00,     /* Logical Minimum (255)             */ \
0x05, 0x07,           /* Usage Page (Key codes)            */ \
0x19, 0x00,           /* Usage Minimum (0)                 */ \
0x29, 0xFF,           /* Usage Maximum (255)               */ \
0x81, 0x00,           /* Input (Data, Array)               */ \
0xC0,                 /* END_COLLECTION                    */ \
0x05, 0x0C,           /* USAGE_PAGE (Consumer Devices)     */ \
0x09, 0x01,           /* USAGE (Consumer Control)          */ \
0xA1, 0x01,           /* COLLECTION (Application)          */ \
0x85, 0x03,           /* REPORT_ID (3)                     */ \
0x15, 0x01,           /* LOGICAL_MINIMUM (1)               */ \
0x26, 0x9C, 0x02,     /* LOGICAL_MAXIMUM (652)             */ \
0x75, 0x10,           /* REPORT_SIZE (16)                  */ \
0x95, 0x02,           /* REPORT_COUNT (2)                  */ \
0x19, 0x01,           /* USAGE_MINIMUM (Consumer Control)  */ \
0x2A, 0x9C, 0x02,     /* USAGE_MAXIMUM (Police Alarm)      */ \
0x81, 0x60,           /* INPUT (Data,Ary,Abs,NPrf,Null)    */ \
0xC0,                 /* END_COLLECTION                    */ \
/* Gamepad 25bytes */ \
0x05, 0x01,        /* USAGE_PAGE (Generic Desktop)*/ \
0x09, 0x05,        /* Usage(Game Pad) */ \
0xa1, 0x01,        /* Collection (Application)   */ \
0x85, 0x04,        /* REPORT_ID (4)      */ \
0x05, 0x09,        /* Usage page (Button) */ \
0x19, 0x01,        /* Usage Minimum(1) */ \
0x29, 0x10,        /* Usage Maximum(16) */ \
0x15, 0x00,        /* Logical Minimum(1) */ \
0x25, 0x01,        /* Logical Maximum(16)  */ \
0x95, 0x10,        /* Report Count (16) */ \
0x75, 0x01,        /* Report Size (1) */ \
0x81, 0x02,        /* Input (Variable) */ \
0xc0               /*END_COLLECTION */

#endif

static const U8 Hid_DescriptorList[] = {
    SDP_DATA_ELEMENT_SEQ_SIZE8(SDP_HID_DESCRIPTOR_LEN + 6), /* Data element sequence */

    /* Each element of the list is a HID descriptor which is a
     * data element sequence.
     */
    SDP_DATA_ELEMENT_SEQ_SIZE8(SDP_HID_DESCRIPTOR_LEN + 4),
    SDP_UINT_SIZE8(SDP_HID_DESCRIPTOR_TYPE),  /* Report Descriptor Type */

    /* Length of the HID descriptor */
    SDP_TEXT_SIZE8(SDP_HID_DESCRIPTOR_LEN),

    /* The actual descriptor is defined in hid.h or in overide.h */
    SDP_HID_DESCRIPTOR

    /* Addition descriptors may be added, but the header must be added
     * along with the actual descriptor data.  Also, all lengths in the 
     * headers above must be adjusted.
     */
};

static const U8 Hid_LangIdBaseList[] = {
    SDP_DATA_ELEMENT_SEQ_SIZE8(8),   /* Data element sequence */

    /* Each element of the list is a data element sequence describing a HID
     * language ID base.  Only one element is included.
     */
    SDP_DATA_ELEMENT_SEQ_SIZE8(6),   /* Data element sequence. */
    SDP_UINT_SIZE16(0x0409),      /* Language = English (United States) */
    SDP_UINT_SIZE16(0x0100)       /* Bluetooth String Offset */

    /* Additional languages may be defined.  Each language is defined by
     * a data element sequence with 2 elements.
     *
     * The first element defines the language and is encoded according to the
     * the "Universal Serial Bus Language Identifiers (LANGIDs)" specification.
     *
     * The second element defines the Bluetooth base attribute ID as is defined
     * in the Language Base Attribute ID List (see HidLangBaseAttrIdList above).
     *
     * Headers must be added along with the actual element (HID Language ID
     * base) data.  Also,  the length in the first header must be adjusted.
     */
};

static const U8 Hid_SdpDisable[] = {
    SDP_BOOL_SIZE8(FALSE)
};

/*---------------------------------------------------------------------------
 * * OPTIONAL *  Hid_BatteryPower
 */
#define SDP_HID_BATTERY_POWER FALSE
static const U8 Hid_BatteryPower[] = {
    SDP_BOOL_SIZE8(SDP_HID_BATTERY_POWER)
};

/*---------------------------------------------------------------------------
 * * OPTIONAL *  Hid_RemoteWake
 */
#define SDP_HID_REMOTE_WAKE TRUE
static const U8 Hid_RemoteWake[] = {
    SDP_BOOL_SIZE8(SDP_HID_REMOTE_WAKE)
};

static const U8 Hid_ProfileVersion[] = {
    SDP_UINT_SIZE16(0x0100)
};

/*---------------------------------------------------------------------------
 * * OPTIONAL *  Hid_SupervisionTimeout
 */
#define SDP_HID_SUPERVISION_TIMEOUT 0x7D00

static const U8 Hid_SupervisionTimeout[] = {
    SDP_UINT_SIZE16(SDP_HID_SUPERVISION_TIMEOUT)
};

/*---------------------------------------------------------------------------
 * * OPTIONAL *  Hid_NormallyConnectable
 */
#define SDP_HID_NORMALLY_CONNECTABLE TRUE
static const U8 Hid_NormallyConnectable[] = {
    SDP_BOOL_SIZE8(SDP_HID_NORMALLY_CONNECTABLE)
};

#define SDP_HID_BOOT_DEVICE TRUE

static const U8 Hid_BootDevice[] = {
    SDP_BOOL_SIZE8(SDP_HID_BOOT_DEVICE)
};

//sdp structure begin
SdpAttr hidd_attrs[] = {
    {ATTRID_SERVICE_RECORD_HANDLE, sizeof(HidRecordHandle), HidRecordHandle, 0},
        
    /* Service Class ID List attribute */
    {ATTRID_SERVICE_CLASS_ID_LIST, sizeof(HidServClassIdVal), HidServClassIdVal, 0},

    {ATTRID_SERVICE_RECORD_STATE, sizeof(HidRecordState), HidRecordState, 0},

    /* Protocol Descriptor List attribute */
    {ATTRID_PROTOCOL_DESC_LIST, sizeof(HidProtocolDescListVal), HidProtocolDescListVal, 0},
    /* Language base Id List attribute */
    {ATTRID_LANG_BASE_ID_LIST, sizeof(HidLangBaseVal), HidLangBaseVal, 0},
    /* Bluetooth Profile Descriptor List attribute */
    {ATTRID_BT_PROFILE_DESC_LIST, sizeof(HidProfileDescListVal), HidProfileDescListVal, 0},
    /* Bluetooth Profile Descriptor List attribute */
    {ATTRID_ADDITIONAL_PROT_DESC_LISTS, sizeof(HidAdditionalProtocolDescListVal), HidAdditionalProtocolDescListVal, 0},

    /* HID Service Name in English */
    {(ATTRID_SERVICE_NAME + 0x0100), sizeof(Hid_ServiceName), Hid_ServiceName, 0},
    /* HID Service Description in English */
    {(ATTRID_SERVICE_DESCRIPTION+ 0x0100), sizeof(Hid_ServiceDescription), Hid_ServiceDescription, 0},
    /* HID Provider Name in English*/
    {(ATTRID_PROVIDER_NAME + 0x0100), sizeof(Hid_ProviderName), Hid_ProviderName, 0},

    /* Mandatory HID attributes */

    /* Device release number */
    {ATTRID_HID_DEVICE_RELEASE, sizeof(Hid_DeviceRelease), Hid_DeviceRelease, 0},
    /* HID parser version */
    {ATTRID_HID_PARSER_VERSION, sizeof(Hid_ParserVersion), Hid_ParserVersion, 0},
    /* Device subclass */
    {ATTRID_HID_DEVICE_SUBCLASS, sizeof(Hid_DeviceSubclass), Hid_DeviceSubclass, 0},
    /* Country Code */
    {ATTRID_HID_COUNTRY_CODE, sizeof(Hid_Countrycode), Hid_Countrycode, 0},
    /* Virtual Cable */
    {ATTRID_HID_VIRTUAL_CABLE, sizeof(Hid_VirtualCable), Hid_VirtualCable, 0},
    /* Device initiates reconnect */
    {ATTRID_HID_RECONNECT_INIT, sizeof(Hid_ReconnectInitiate), Hid_ReconnectInitiate, 0},
    /* HID descriptor list */
    {ATTRID_HID_DESCRIPTOR_LIST, sizeof(Hid_DescriptorList), Hid_DescriptorList, 0},
    /* Language ID Base List */
    {ATTRID_HID_LANG_ID_BASE_LIST, sizeof(Hid_LangIdBaseList), Hid_LangIdBaseList, 0},

    /* Optional HID attibutes */

    /* SDP Disable/Enable */
    {ATTRID_HID_SDP_DISABLE, sizeof(Hid_SdpDisable), Hid_SdpDisable, 0},
    /* Battery powered */
    {ATTRID_HID_BATTERY_POWER, sizeof(Hid_BatteryPower), Hid_BatteryPower, 0},
    /* Device support of remote wakeup */
    {ATTRID_HID_REMOTE_WAKE, sizeof(Hid_RemoteWake), Hid_RemoteWake, 0},

    /* Mandatory HID attribute */

    /* HID profile version*/
    {ATTRID_HID_PROFILE_VERSION, sizeof(Hid_ProfileVersion), Hid_ProfileVersion, 0},

    /* Optional HID attributes */

    /* Recommended supervision timeout */
    {ATTRID_HID_SUPERV_TIMEOUT, sizeof(Hid_SupervisionTimeout), Hid_SupervisionTimeout, 0},
    /* Device connectability */
    {ATTRID_HID_NORM_CONNECTABLE, sizeof(Hid_NormallyConnectable), Hid_NormallyConnectable, 0},

    /* Support for boot protocol */
    {ATTRID_HID_BOOT_DEVICE, sizeof(Hid_BootDevice), Hid_BootDevice, 0},

};


static const U8 HididRecordHandle [] = {
    SDP_UINT_SIZE32(0x00010000)
};

static const U8 HididServClassIdVal[] = {
    SDP_DATA_ELEMENT_SEQ_SIZE8(3),           /* Data element sequence, 3 bytes */
    SDP_UUID_SIZE16(PROF_DID), /* Uuid16 DIS */
};

static const U8 HididProtocolDescListVal[] = {
    SDP_DATA_ELEMENT_SEQ_SIZE8(13),  /* Data element sequence, 12 bytes */
    SDP_DATA_ELEMENT_SEQ_SIZE8(6),   /* Data element sequence for L2CAP, 3 bytes */     
    SDP_UUID_SIZE16(PROT_L2CAP),  /* Uuid16 L2CAP */
    SDP_UINT_SIZE16(BT_PSM_SDP),  /* Uuid16 PSM */    
    SDP_DATA_ELEMENT_SEQ_SIZE8(3),   /* Data element sequence for RFCOMM, 5 bytes */
    SDP_UUID_SIZE16(PROT_SDP),  /* Uuid16 SDP protocol */
};

static const U8 HididProfileDescListVal[] = {
    SDP_DATA_ELEMENT_SEQ_SIZE8(8),           /* Data element sequence, 8 bytes */
    SDP_DATA_ELEMENT_SEQ_SIZE8(6),           /* Data element sequence for ProfileDescriptor, 6 bytes */
    SDP_UUID_SIZE16(PROF_DID),          /* Uuid16 did */
    SDP_UINT_SIZE16(0x0103)               /* Uint16 version number */
};

static const U8 HididSpecIdVal[] = {
    SDP_UINT_SIZE16(0x0103)
};


U8 HididVendorIdVal[] = {
    SDP_UINT_SIZE16(VID)
};

U8 HididProductIdVal[] = {
    SDP_UINT_SIZE16(PID)
};

static const U8 HididVersionVal[] = {
    SDP_UINT_SIZE16(REV)//2.1.3
};

static const U8 HididPrimaryRecordVal[] = {
    SDP_BOOL_SIZE8(TRUE)
};

static const U8 HididVendorIdSourceVal[] = {
    SDP_UINT_SIZE16(0x02)//usb
};

SdpAttr hidid_attrs[] = 
{
    {ATTRID_SERVICE_RECORD_HANDLE, sizeof(HididRecordHandle), HididRecordHandle, 0},
    {ATTRID_SERVICE_CLASS_ID_LIST, sizeof(HididServClassIdVal), HididServClassIdVal, 0},
        
    {ATTRID_PROTOCOL_DESC_LIST, sizeof(HididProtocolDescListVal), HididProtocolDescListVal, 0}, 
    {ATTRID_BT_PROFILE_DESC_LIST, sizeof(HididProfileDescListVal), HididProfileDescListVal, 0},    
    {ATTRID_DID_SPEC_ID, sizeof(HididSpecIdVal), HididSpecIdVal, 0},
    {ATTRID_DID_VENDOR_ID, sizeof(HididVendorIdVal), HididVendorIdVal, 0},
    {ATTRID_DID_PRODUCT_ID, sizeof(HididProductIdVal), HididProductIdVal, 0},
    {ATTRID_DID_VERSION, sizeof(HididVersionVal), HididVersionVal, 0},
    {ATTRID_DID_PRIMARY_RECORD, sizeof(HididPrimaryRecordVal), HididPrimaryRecordVal, 0},
    {ATTRID_DID_VENDOR_ID_SOURCE, sizeof(HididVendorIdSourceVal), HididVendorIdSourceVal, 0},

};
//sdp structure end
#endif

#if  (PROFILE_TYPE == PROFILE_TYPE_HID_HFP || PROFILE_TYPE == PROFILE_TYPE_HID || PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)

extern void L2ChannelSearchTCidHdlByPsm(U16 psm, U16 *cid, U16 *acl);
extern void L2ChannelSendData(U16 aclHandle, U16 cid, U16 len, U8 *dat);

typedef struct
{
    U16 aclHdl;
    U16 ccid; //target control cid
    U16 icid; //target interrupt cid
    U8  profile_type; //HID or PS3
//parm[0]: 1: connect sucess 0: disconnect or connect fail
//parm[1]: 1: hidc 0:hidi
    FinishNotifyCallback hidfc;
    BT_LOCAL_INFO *bt_info;
}HIDD_ST;

HIDD_ST hidst;

U8 HIDControlPsmHandler(U16 aclHandle, U16 cid, U16 len , U8 *dat)
{
    U16 tmpHdl;
    U16 tmpCid;
    U8 type, report;
    U16 maxsize;

    if(aclHandle == 0) //update channel status
    {
        L2ChannelSearchTCidHdlByPsm(0x0011, &tmpCid, &tmpHdl);

        if(hidst.ccid) //currently connected
        {
            if(!tmpHdl || !tmpCid)
            {
                hidst.ccid = 0; //disconnected update
                hidst.aclHdl = 0;        
                if(hidst.hidfc)
                    hidst.hidfc(0+2); //disconnect inform                
            }
        }
        else //currently disconnected
        {
            if(tmpHdl && tmpCid)
            {
                hidst.ccid = tmpCid; //connected update
                hidst.aclHdl = tmpHdl;      
                if(hidst.hidfc)
                    hidst.hidfc(1+2); //connect inform

            }
        }

        return 0;
    }
    
    switch(dat[0] >> 4)
    {
        case MSG_TYPE_HID_CTL:
            if((dat[0] & 0xf) == 3) //goto suspend
            {
            }
            else if((dat[0] & 0xf) == 4) //exit suspend
            {
            }
            else if((dat[0] & 0xf) == 5) //unplug
            {
            }
            else
                ASSERT(0);
            break;
        case MSG_TYPE_GET_REPORT:
        {
            if (hidst.profile_type == PROFILE_TYPE_PS3)
            {
                if (dat[0] & 0x08) 
                {
                    if (len < 4) 
                    {
                        //printf("GET_REPORT short\n");
                        break;
                    }
                    maxsize = (dat[3] << 8) | dat[2];
                    if (maxsize < 64) 
                    {
                        //printf("GET_REPORT short buf (%d)\n", maxsize);
                        break;
                    }
                }
                
                type = dat[0] & 0x03;

                if (type == 0) 
                {
                    break;
                }

                report = dat[1];

                len = ps3_data_get_report(type, report, dat+2, 1000);

                /* Fill common portion */
                dat[0] = 0xa0 | type;
                dat[1] = report;
                len += 2;
                Bt__ProfileHIDDCSendData(dat, len);
            }
            else
                ASSERT(0);
      }
            break;
        case MSG_TYPE_SET_REPORT:
        case MSG_TYPE_DATA:   
            if (hidst.profile_type == PROFILE_TYPE_PS3)
            {
                type = dat[0] & 3;
                report = dat[1];

                if(type == 0)
                    break;
                
                ps3_data_set_report(type, report, dat + 2, len - 2);                
                dat[0] = (MSG_TYPE_HANDSHAKE << 4) | 0x0;
                Bt__ProfileHIDDCSendData(dat, 1);
            }
            else
                ASSERT(0);
            break;
        case MSG_TYPE_GET_PROTOCOL:
            ASSERT(0);
            break;
        case MSG_TYPE_SET_PROTOCOL:
        {            
            if((dat[0] & 0xf) == 1) //report protocol mode
            {
                dat[0] = (MSG_TYPE_HANDSHAKE << 4) | 0; //success
                L2ChannelSendData(aclHandle, hidst.ccid, 1, dat);
            }
            else
            {
                dat[0] = (MSG_TYPE_HANDSHAKE << 4) | 4; //invalid parameter
                L2ChannelSendData(aclHandle, hidst.ccid, 1, dat);
            }
            break;
        }
        case MSG_TYPE_SET_IDLE:
            if(dat[1] != 0)
                dat[0] = (MSG_TYPE_HANDSHAKE << 4) | 4; //invalid parameter
            else
                dat[0] = (MSG_TYPE_HANDSHAKE << 4) | 0; //success
            
            L2ChannelSendData(aclHandle, hidst.ccid, 1, dat);
            break;
            
    }
    return 0;
}


U8 HIDIntPsmHandler(U16 aclHandle, U16 cid, U16 len , U8 *dat)
{
    U16 tmpCid;
    U16 tmpHdl;
    U8 type, report;
    if(aclHandle == 0) //update channel status
    {
        if(cid == 0)
        {
            L2ChannelSearchTCidHdlByPsm(0x0013, &tmpCid, &tmpHdl);

            if(hidst.icid) //currently connected
            {
                if(!tmpHdl || !tmpCid)
                {
                    hidst.icid = 0; //disconnected update
                    //hidst.aclHdl = 0;        
                    if(hidst.hidfc)
                        hidst.hidfc(0); //disconnect inform                
                }

            }
            else //currently disconnected
            {
                if(tmpHdl && tmpCid)
                {
                    hidst.icid = tmpCid; //connected update
                    hidst.aclHdl = tmpHdl;      
                    if(hidst.hidfc)
                        hidst.hidfc(1); //connect inform

                }
            }
        }
        
        return 0;
    }

    switch(dat[0] >> 4)
    {
        case MSG_TYPE_DATA:
            if (hidst.profile_type == PROFILE_TYPE_PS3)
            {
                type = dat[0] & 3;
                report = dat[1];

                if(type == 0)
                    break;
                
                ps3_data_set_report(type, report, dat + 2, len - 2); 
            }
            break;
        default:
            ASSERT(0);
    }
    return 0;
}

void Bt__ProfileHIDDIDGetInquiryData(U8 *num, SdpAttr **attr)
{
    *num = sizeof(hidid_attrs) / sizeof(SdpAttr);
    *attr = hidid_attrs;
}

void Bt__ProfileHIDDGetInquiryData(U8 *num, SdpAttr **attr, U32 *CoD)
{
    *num = sizeof(hidd_attrs) / sizeof(SdpAttr);
    *attr = hidd_attrs;
    *CoD = 0x002500 | SDP_HID_DEVICE_SUBCLASS;
}

void Bt__ProfileHIDDInit(U8 type)
{
    hidst.aclHdl = 0;
    hidst.ccid = 0;
    hidst.icid = 0;
    hidst.hidfc = 0;
    hidst.profile_type = type;

    ASSERT(RegisterPsmHandler(0x0011, HIDControlPsmHandler));
    ASSERT(RegisterPsmHandler(0x0013, HIDIntPsmHandler));
    Bt__GetLocalInfo(&hidst.bt_info);
}

void Bt__ProfileHIDDDeInit()
{
    DeregisterPsmHandler(0x0011); //do not check
    DeregisterPsmHandler(0x0013); //do not check
}


///////////////////////////////////Public API///////////////////////////////////////////
extern void LLSignalConReq(U16 aclHandle, U16 psm);
extern void LLSignalDisConReq(U16 aclHandle, U8 id, U16 tcid, U16 scid);
extern U16 L2ChannelSearchAcidByTcid(U16 cid);

void Bt__HIDDCDisconnect()
{
    LLSignalDisConReq(hidst.aclHdl, 0, hidst.ccid, L2ChannelSearchAcidByTcid(hidst.ccid));
}

void Bt__HIDDIDisconnect()
{
    LLSignalDisConReq(hidst.aclHdl, 0, hidst.icid, L2ChannelSearchAcidByTcid(hidst.icid));
}

void Bt__HIDDCConnect(U16 aclHandle)
{
    LLSignalConReq(aclHandle, 0x0011);
}

void Bt__HIDDIConnect(U16 aclHandle)
{
    LLSignalConReq(aclHandle, 0x0013);
}

void Bt__ProfileHIDDCB(FinishNotifyCallback fc)
{
   hidst.hidfc = fc;
}

void Bt__ProfileHIDDSendKeyCode(U8 modifier, U8 *keycode, U8 len)
{
    U8 i;
    U8 dat[10];

    if(hidst.aclHdl == 0)
        return;
    
    dat[0] = 0xA1;
    dat[1] = 1; //report id = 1
    dat[2] = modifier; //modifier
    dat[3] = 0; //reserved
    
    for(i = 0; i < len && i < 6 ; i++)
    {
        dat[4+i] = keycode[i];
    }

    for(i = i + 4;i < 10; i++)
    {
        dat[i] = 0;    
    }
    
    L2ChannelSendData(hidst.aclHdl, hidst.icid, 10, 0);
}

void Bt__ProfileHIDDSendMouse(U8 btn, U8 x, U8 y, U8 z)
{

    U8 dat[6];
    if(hidst.aclHdl == 0)
        return;
    
    dat[0] = 0xA1;
    dat[1] = 2; //report id = 2
    dat[2] = btn; //modifier
    dat[3] = x; //reserved
    dat[4] = y; //reserved
    dat[5] = z; //reserved
        
    L2ChannelSendData(hidst.aclHdl, hidst.icid, 6, 0);
}

void Bt__ProfileHIDDSendMedia(U8 *media)
{

    U8 dat[6];
    if(hidst.aclHdl == 0)
        return;
    
    dat[0] = 0xA1;
    dat[1] = 3; //report id = 2
    dat[2] = media[0]; //modifier
    dat[3] = media[1]; //reserved
    dat[4] = 0; //reserved
    dat[5] = 0; //reserved
        
    L2ChannelSendData(hidst.aclHdl, hidst.icid, 6, 0);
}

void Bt__ProfileHIDDSendGame(U8 *btn, U8 len)
{
    U8 i;
    
    U8 *dat;

    dat = malloc(2+len);

    ASSERT(dat);
    
    if(hidst.aclHdl == 0)
        return;
    
    dat[0] = 0xA1;
    dat[1] = 4; //report id = 4

    for(i = 0; i < len; i++)
        dat[2] = btn[i];
        
    L2ChannelSendData(hidst.aclHdl, hidst.icid, len+2, 0);

    free(dat);
}

void Bt__ProfileHIDDSetVidPid(U16 vid, U16 pid)
{
    HididProductIdVal[1] = pid >> 8;
    HididProductIdVal[2] = pid & 0xff;
    HididVendorIdVal[1] = vid >> 8;
    HididVendorIdVal[2] = vid & 0xff;
}

void Bt__ProfileHIDDISendData(U8 *dat, U8 len)
{
    if(hidst.aclHdl == 0)
        return;
    
    L2ChannelSendData(hidst.aclHdl, hidst.icid, len, dat);
}

void Bt__ProfileHIDSendPS3Key(void)
{
    U8 dat[50];
    int len;

    if(!Bt_AclReady())
        return;

    len = ps3_data_get_report(0x1, 0x1, dat+2, 48);

    if(len < 0) return;
    
    dat[0] = 0xa1;
    dat[1] = 0x01;
    len += 2;

    Bt__ProfileHIDDISendData(dat, len);
}

void Bt__ProfileHIDDCSendData(U8 *dat, U8 len)
{
    if(hidst.aclHdl == 0)
        return;
    
    L2ChannelSendData(hidst.aclHdl, hidst.ccid, len, dat);
}

#endif


