#ifndef __BT_PROFILE_HIDD_HH_
#define __BT_PROFILE_HIDD_HH_
#include "../BTType.h"
#include "../GAP/LOCAL_DEV/Bt__LocalDevice.h"

///////////////////////////////////Public API///////////////////////////////////////////

//void Bt__PS3SendKeycode(void);
//void Bt__PS3AssignJoyStick(U8 joy, U8 jval);
//void Bt__PS3AssignDigiKey(U8 digi, U8 val);

void Bt__ProfileHIDSendPS3Key(void);
void Bt__ProfileHIDDISendData(U8 *dat, U8 len);
void Bt__ProfileHIDDCSendData(U8 *dat, U8 len);
void Bt__ProfileHIDDSendGame(U8 *btn, U8 len);
void Bt__ProfileHIDDSetVidPid(U16 vid, U16 pid);
void Bt__ProfileHIDDSendMedia(U8 *media);
void Bt__ProfileHIDDSendMouse(U8 btn, U8 x, U8 y, U8 z);
void Bt__ProfileHIDDSendKeyCode(U8 modifier, U8 *keycode, U8 len);
void Bt__ProfileHIDDCB(FinishNotifyCallback fc);
void Bt__HIDDCConnect(U16 aclHandle);
void Bt__HIDDIConnect(U16 aclHandle);
void Bt__HIDDIDisconnect(void);
void Bt__HIDDCDisconnect(void);

///////////////////////////////////Private Functions////////////////////////////////////
#define ATTRID_HID_DEVICE_RELEASE        0x0200
#define ATTRID_HID_PARSER_VERSION        0x0201
#define ATTRID_HID_DEVICE_SUBCLASS       0x0202
#define ATTRID_HID_COUNTRY_CODE          0x0203
#define ATTRID_HID_VIRTUAL_CABLE         0x0204
#define ATTRID_HID_RECONNECT_INIT        0x0205
#define ATTRID_HID_DESCRIPTOR_LIST       0x0206
#define ATTRID_HID_LANG_ID_BASE_LIST     0x0207
#define ATTRID_HID_SDP_DISABLE           0x0208
#define ATTRID_HID_BATTERY_POWER         0x0209
#define ATTRID_HID_REMOTE_WAKE           0x020A
#define ATTRID_HID_PROFILE_VERSION       0x020B
#define ATTRID_HID_SUPERV_TIMEOUT        0x020C
#define ATTRID_HID_NORM_CONNECTABLE      0x020D
#define ATTRID_HID_BOOT_DEVICE           0x020E
#define ATTRID_DID_SPEC_ID               0x0200
#define ATTRID_DID_VENDOR_ID             0x0201
#define ATTRID_DID_PRODUCT_ID            0x0202
#define ATTRID_DID_VERSION               0x0203
#define ATTRID_DID_PRIMARY_RECORD        0x0204
#define ATTRID_DID_VENDOR_ID_SOURCE      0x0205


#define MSG_TYPE_HANDSHAKE 0
#define MSG_TYPE_HID_CTL 1
#define MSG_TYPE_GET_REPORT 4
#define MSG_TYPE_SET_REPORT 5
#define MSG_TYPE_GET_PROTOCOL 6
#define MSG_TYPE_SET_PROTOCOL 7
#define MSG_TYPE_SET_IDLE 9
#define MSG_TYPE_DATA 0xA

void Bt__ProfileHIDDInit(U8 type);
void Bt__ProfileHIDDDeInit(void);

#endif

