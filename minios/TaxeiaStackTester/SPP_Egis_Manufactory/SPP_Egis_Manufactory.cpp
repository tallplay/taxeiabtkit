// SPP_Egis_Manufactory.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "SPP_Egis_Manufactory.h"
#include "SPP_Egis_ManufactoryDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CSPP_Egis_ManufactoryApp

BEGIN_MESSAGE_MAP(CSPP_Egis_ManufactoryApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CSPP_Egis_ManufactoryApp construction

CSPP_Egis_ManufactoryApp::CSPP_Egis_ManufactoryApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}


// The one and only CSPP_Egis_ManufactoryApp object

CSPP_Egis_ManufactoryApp theApp;


// CSPP_Egis_ManufactoryApp initialization

BOOL CSPP_Egis_ManufactoryApp::InitInstance()
{
	CWinApp::InitInstance();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	// of your final executable, you should remove from the following
	// the specific initialization routines you do not need
	// Change the registry key under which our settings are stored
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization
	SetRegistryKey(_T("Local AppWizard-Generated Applications"));

    memset(runtimepath, 0x00, sizeof(runtimepath));             
    GetModuleFileName(NULL, runtimepath, MAX_PATH);    
    for (int i = (int)(strlen(runtimepath) - 1); i >= 0; i--) {
        if (runtimepath[i] == '\\') {
            runtimepath[i + 1] = '\0';
            break;
        }
    }
    SetCurrentDirectory(runtimepath);


	CSPP_Egis_ManufactoryDlg dlg;
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}

int
ishex(char c) {
    if (isdigit(c)) {
        return 1;
    }
    if ((c > 'a' - 1) && (c < 'g')) {
        return 1;
    }
    if ((c > 'A' - 1) && (c < 'G')) {
        return 1;
    }
    return 0;
}

int
h2toi(char *hex) {
	int r_value=0;

	if(!ishex(hex[0])){
		return 0;
	}

	if(isdigit(hex[0])){
		r_value += hex[0] - 48;
	} else {
		if ((hex[0] > 'a' - 1) && (hex[0] < 'g')) {
			r_value += hex[0] - 'a' + 10;
		} else {
			r_value += hex[0] - 'A' + 10;
		}	
	}

	r_value *= 16;

	if(!ishex(hex[1])){
		return 0;
	}

	if(isdigit(hex[1])){
		r_value += hex[1] - 48;
	} else {
		if ((hex[1] > 'a' - 1) && (hex[1] < 'g')) {
			r_value += hex[1] - 'a' + 10;
		} else {
			r_value += hex[1] - 'A' + 10;
		}	
	}

    return r_value;
}