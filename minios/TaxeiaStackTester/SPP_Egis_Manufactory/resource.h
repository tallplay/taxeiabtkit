//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by SPP_Egis_Manufactory.rc
//
#define IDCONNECT                       3
#define IDCSET_COD_NAME                 4
#define IDCACTIVATING                   5
#define IDCASSIGNADDR                   5
#define IDCSPPData                      6
#define IDCSPPSEND                      6
#define IDD_SPP_EGIS_MANUFACTORY_DIALOG 102
#define IDR_MAINFRAME                   128
#define IDD_DIALOG_SPPSEND              131
#define IDD_DIALOG_SETTING              132
#define IDC_STATIC_BT_INTERNAL_INFO     1000
#define IDC_STATIC_STATUS               1001
#define IDC_LIST_FOUND                  1003
#define IDC_STATIC_YESNO                1004
#define IDC_STATIC_COD                  1005
#define IDC_STATIC_FW                   1006
#define IDC_STATIC_NAME                 1007
#define IDC_EDIT_RECEIVED               1008
#define IDC_EDIT_COD                    1009
#define IDC_EDIT_NAME                   1010
#define IDC_EDIT_NAME2                  1011
#define IDC_EDIT_MESSAGE                1012
#define IDC_GETVERSION                  1013
#define IDC_EDIT_RSSI                   1014
#define IDC_BUTTON_SETTING              1015
#define IDC_STATIC_RSSI_FILTER          1016
#define IDC_STATIC_NAME_FILTER          1017
#define IDC_STATIC_cod_to_set           1018
#define IDC_STATIC_NAME_TO_SET          1019
#define IDC_STATIC_KEY_CNT              1020
#define IDC_STATIC_ADDR                 1021

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        133
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1022
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
