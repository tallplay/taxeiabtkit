#ifndef __BT_MANUFACTORY__

typedef unsigned long  U32;
typedef unsigned short U16;
typedef unsigned char  U8;
typedef signed char  S8;

#define BT_CONN_STS_INITING 0
#define BT_CONN_STS_IDLE 1
#define BT_CONN_STS_CONNECTING 2
#define BT_CONN_STS_CONNECTED 3
#define BT_CONN_STS_DISCONNECTING 4
#define BT_CONN_STS_HWFAIL 5

#define BT_INQUIRY_END_NOT_FOUND 0
#define BT_INQUIRY_END_FOUND 1
#define BT_INQUIRY_NOT_END_FOUND_NOT_QUALIFY 2

void BT_ATE_INIT(void (*spp_cb)(U16 len, U8 *dat), void (*bt_state_changed)(U8 sts));
void BT_ATE_CONNECT(U8 BDADDR[6]);
void BT_ATE_DISCONNECT(void);
void BT_ATE_GET_FW_STATUS(void (*ready_cb)(U8 is_addr_assigned, U32 cod, U16 fw_version, U8 *name));
void BT_ATE_SET_COD_AND_NAME(U32 cod, U8 *name, void(*ready_cb)(void));
void BT_ATE_SET_BT_ADDR(void(*ready_cb)(U8 bd_addr[6], U8 status));
void BT_ATE_GET_KEY_STATUS(U8 *status, int *count);

void BT_ATE_INQUIRY(U8 *name_filter, S8 rssi_filter, void(*inquiry_cb)(U8 found_state, U32 cod, S8 rssi, U8 bd_addr[6]));
void BT_ATE_SPP_SEND(U16 len, U8 *dat);
void BT_ATE_READ_RSSI(void(*rssi_callback)(S8));

#endif
