// SPP_Egis_Manufactory.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols

#include "BT_MANUFACTORY.h"
// CSPP_Egis_ManufactoryApp:
// See SPP_Egis_Manufactory.cpp for the implementation of this class
//

class CSPP_Egis_ManufactoryApp : public CWinApp
{
public:
	CSPP_Egis_ManufactoryApp();

// Overrides
	public:
	virtual BOOL InitInstance();

	char runtimepath[MAX_PATH];

// Implementation

	DECLARE_MESSAGE_MAP()
};


int h2toi(char *hex) ;

extern CSPP_Egis_ManufactoryApp theApp;
