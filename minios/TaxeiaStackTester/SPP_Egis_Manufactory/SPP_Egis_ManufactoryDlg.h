// SPP_Egis_ManufactoryDlg.h : header file
//

#pragma once
#include "afxwin.h"
#include "afxcmn.h"
#include "ColorEdit.h"

// CSPP_Egis_ManufactoryDlg dialog
class CSPP_Egis_ManufactoryDlg : public CDialog
{
// Construction
public:
	CSPP_Egis_ManufactoryDlg(CWnd* pParent = NULL);	// standard constructor

	void ResetConnectedDevice();
	void SetConnectedDevice(U8 is_addr_assigned, U32 cod, U16 fw_version, U8 *name);
	void AddSppReceived(U16 len, U8 *dat);

	void ReadConfig();
	void WriteConfig();

	void SetNewAddr(U8 *addr);

// Dialog Data
	enum { IDD = IDD_SPP_EGIS_MANUFACTORY_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
    void UpdateControl();
	afx_msg void OnBnClickedConnect();
	afx_msg void OnBnClickedCodName();
	afx_msg void OnBnClickedAssignAddr();
	afx_msg void OnBnClickedCsppdata();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnBnClickedCancel();
	afx_msg void OnNMDblclkListFound(NMHDR *pNMHDR, LRESULT *pResult);	
	CStatic m_control_status;
	CButton m_button_discover;
	CButton m_button_connect;
	CButton m_button_setcodname;
	CButton m_button_assignaddr;
	CListCtrl m_control_found_list;	
	CStatic m_control_is_addr_assigned;
	CStatic m_control_cod;
	CStatic m_control_fw;
	CStatic m_control_name;
	CEdit m_control_received;
	CButton m_button_sppsend;
	afx_msg void OnBnClickedGetversion();
	afx_msg void OnLvnItemchangingListFound(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedButtonSetting();
	CStatic m_control_rssi_filter;
	CStatic m_control_name_filter;
	CStatic m_control_cod_to_set;
	CStatic m_control_name_to_set;
	CStatic m_control_key_cnt;
	CColorEdit m_control_addr;
};
