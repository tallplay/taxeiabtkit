// SPP_Egis_ManufactoryDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SPP_Egis_Manufactory.h"
#include "SPP_Egis_ManufactoryDlg.h"

#include "DlgSetting.h"
#include "DlgSppSend.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define TIMER_GET_FW	1000

#define APP_STATE_INITIALIZATING 0          //<== BT_CONN_STS_INITING
#define APP_STATE_HW_FAIL 1                 //<== BT_CONN_STS_HWFAIL

#define APP_STATE_IDLE 2                    //<==BT_CONN_STS_IDLE
#define APP_STATE_IDLE_CONNECTING_FAIL 3    //<==BT_CONN_STS_IDLE
#define APP_STATE_FINDING 4                 //<==BT_CONN_STS_IDLE
#define APP_STATE_FOUND 5                   //<==BT_CONN_STS_IDLE

#define APP_STATE_CONNECTING 6              //<==BT_CONN_STS_CONNECTING
#define APP_STATE_CONNECTED 7               //<==BT_CONN_STS_CONNECTED
#define APP_STATE_DISCONNECTING 8           //<==BT_CONN_STS_DISCONNECTING

U8 g_inquiry_name_filter[30];
S8 g_rssi_filter;
U32 g_cod_to_set;
U8 g_name_to_set[30];

U8 n_device_found;
U8 remote_addr[6];
U32 remote_cod;
S8 remote_rssi;
U16 remote_clk_offset;

U8 app_state = APP_STATE_INITIALIZATING;

CSPP_Egis_ManufactoryDlg *TheDlg = NULL;

extern "C" {
	extern HWND StartHciLog(HINSTANCE , BYTE);
};

void remote_internal_ready(U8 is_addr_assigned, U32 cod, U16 fw_version, U8 *name)
{
	TheDlg->SetConnectedDevice(is_addr_assigned, cod, fw_version, name);
}

void set_cod_name_ready(void)
{
    //update status again
    BT_ATE_GET_FW_STATUS(&remote_internal_ready);
}

void set_addr_ready(U8 bt_addr[6], U8 status)
{
    if(!status)
		TheDlg->SetNewAddr(bt_addr);

    //update status again
    BT_ATE_GET_FW_STATUS(&remote_internal_ready);
}

void found_cb(U8 is_found, U32 cod, S8 rssi, U8 bd_addr[6])
{
    if(is_found==BT_INQUIRY_END_FOUND) {
		memcpy(remote_addr, bd_addr, 6);
		remote_cod = cod;
		remote_rssi = rssi;
		app_state = APP_STATE_FOUND;
		TheDlg->UpdateControl();
	} else if (is_found == BT_INQUIRY_NOT_END_FOUND_NOT_QUALIFY){
		memcpy(remote_addr, bd_addr, 6);
		remote_cod = cod;
		remote_rssi = rssi;
		app_state = APP_STATE_FOUND;
		TheDlg->UpdateControl();
	} else {
		app_state = APP_STATE_IDLE;
		TheDlg->UpdateControl();
	}
}

void spp_cb(U16 len, U8 *dat)
{
	TheDlg->AddSppReceived(len, dat);
}

void bt_cb(U8 sts)
{
    switch(sts)
	{
	    case BT_CONN_STS_INITING: //initilizaing
	        app_state = APP_STATE_INITIALIZATING;
			break;
	    case BT_CONN_STS_IDLE: //idle
	    {
			if(app_state == APP_STATE_CONNECTING)
				app_state = APP_STATE_IDLE_CONNECTING_FAIL;
			else
			{
		        app_state = APP_STATE_IDLE;
		    }
			break;
    	}
	    case BT_CONN_STS_CONNECTING: //connecting
	        app_state = APP_STATE_CONNECTING;
			break;

	    case BT_CONN_STS_CONNECTED: //connected
	        app_state = APP_STATE_CONNECTED;
			TheDlg->SetTimer(TIMER_GET_FW, 500, 0);
			
			break;

		case BT_CONN_STS_DISCONNECTING: //disconnecting
	        app_state = APP_STATE_DISCONNECTING;
		    break;

	    case BT_CONN_STS_HWFAIL: //bt hardware fail
	        app_state = APP_STATE_HW_FAIL;
			break;	    
	}

	TheDlg->UpdateControl();
}

// CSPP_Egis_ManufactoryDlg dialog
CSPP_Egis_ManufactoryDlg::CSPP_Egis_ManufactoryDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSPP_Egis_ManufactoryDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CSPP_Egis_ManufactoryDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATIC_STATUS, m_control_status);
	DDX_Control(pDX, IDOK, m_button_discover);
	DDX_Control(pDX, IDCONNECT, m_button_connect);
	DDX_Control(pDX, IDCSET_COD_NAME, m_button_setcodname);
	DDX_Control(pDX, IDCASSIGNADDR, m_button_assignaddr);
	DDX_Control(pDX, IDC_LIST_FOUND, m_control_found_list);
	DDX_Control(pDX, IDC_STATIC_YESNO, m_control_is_addr_assigned);
	DDX_Control(pDX, IDC_STATIC_COD, m_control_cod);
	DDX_Control(pDX, IDC_STATIC_FW, m_control_fw);
	DDX_Control(pDX, IDC_STATIC_NAME, m_control_name);
	DDX_Control(pDX, IDC_EDIT_RECEIVED, m_control_received);
	DDX_Control(pDX, IDCSPPSEND, m_button_sppsend);
	DDX_Control(pDX, IDC_STATIC_RSSI_FILTER, m_control_rssi_filter);
	DDX_Control(pDX, IDC_STATIC_NAME_FILTER, m_control_name_filter);
	DDX_Control(pDX, IDC_STATIC_cod_to_set, m_control_cod_to_set);
	DDX_Control(pDX, IDC_STATIC_NAME_TO_SET, m_control_name_to_set);
	DDX_Control(pDX, IDC_STATIC_KEY_CNT, m_control_key_cnt);
	DDX_Control(pDX, IDC_STATIC_ADDR, m_control_addr);
}

BEGIN_MESSAGE_MAP(CSPP_Egis_ManufactoryDlg, CDialog)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDOK, &CSPP_Egis_ManufactoryDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCONNECT, &CSPP_Egis_ManufactoryDlg::OnBnClickedConnect)
	ON_BN_CLICKED(IDCSET_COD_NAME, &CSPP_Egis_ManufactoryDlg::OnBnClickedCodName)
	ON_BN_CLICKED(IDCASSIGNADDR, &CSPP_Egis_ManufactoryDlg::OnBnClickedAssignAddr)
	ON_BN_CLICKED(IDCSPPData, &CSPP_Egis_ManufactoryDlg::OnBnClickedCsppdata)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDCANCEL, &CSPP_Egis_ManufactoryDlg::OnBnClickedCancel)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_FOUND, &CSPP_Egis_ManufactoryDlg::OnNMDblclkListFound)
	ON_BN_CLICKED(IDC_GETVERSION, &CSPP_Egis_ManufactoryDlg::OnBnClickedGetversion)
	ON_NOTIFY(LVN_ITEMCHANGING, IDC_LIST_FOUND, &CSPP_Egis_ManufactoryDlg::OnLvnItemchangingListFound)
	ON_BN_CLICKED(IDC_BUTTON_SETTING, &CSPP_Egis_ManufactoryDlg::OnBnClickedButtonSetting)
END_MESSAGE_MAP()


// CSPP_Egis_ManufactoryDlg message handlers

BOOL CSPP_Egis_ManufactoryDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	TheDlg = this;

	BT_ATE_INIT(spp_cb, bt_cb);
	
			
    m_control_found_list.SetExtendedStyle(m_control_found_list.GetExtendedStyle() |
                                    LVS_EX_GRIDLINES |
                                    LVS_EX_FULLROWSELECT);

    m_control_found_list.InsertColumn(0, "BDADDR", LVCFMT_LEFT,150);      
	m_control_found_list.InsertColumn(1, "COD", LVCFMT_LEFT,70);
	m_control_found_list.InsertColumn(2, "RSSI", LVCFMT_LEFT,70);
	m_control_found_list.InsertColumn(3, "NAME", LVCFMT_LEFT,150);

    m_control_found_list.SetColumnWidth(3, LVSCW_AUTOSIZE_USEHEADER);

	ResetConnectedDevice();

	ReadConfig();
	WriteConfig();

	//StartHciLog(AfxGetInstanceHandle(), SW_SHOW);
	//StartHciLog(AfxGetInstanceHandle(), SW_HIDE);


	m_control_addr.SetWindowText("");
	m_control_addr.SetTextColor(GREEN);
	m_control_addr.SetBigFont();

	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CSPP_Egis_ManufactoryDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CSPP_Egis_ManufactoryDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CSPP_Egis_ManufactoryDlg::SetNewAddr(U8 *addr){
	char str[MAX_PATH];
	// Address	
	sprintf(str, "%02X:%02X:%02X:%02X:%02X:%02X", 
		addr[5], addr[4], addr[3], 
		addr[2], addr[1], addr[0]);

	m_control_addr.SetWindowText(str);
}

void CSPP_Egis_ManufactoryDlg::UpdateControl(void)
{
		char str[MAX_PATH];

	    switch(app_state)
		{
		    case APP_STATE_INITIALIZATING:
				m_control_status.SetWindowTextA("Initializating Stack...");
				break;

		    case APP_STATE_HW_FAIL:
				m_control_status.SetWindowTextA("BT Initialization FAIL!!");
				break;

		    case APP_STATE_IDLE:
			    m_button_discover.EnableWindow(TRUE);
			    m_button_connect.EnableWindow(FALSE);
			    m_button_setcodname.EnableWindow(FALSE);
			    m_button_sppsend.EnableWindow(FALSE);								
				m_button_assignaddr.EnableWindow(FALSE);
			    m_button_discover.SetWindowTextA("Discover");
			    m_button_connect.SetWindowTextA("Connect");

				sprintf(str, "Press to Discover button to discover %s", g_inquiry_name_filter);
				m_control_status.SetWindowText(str);

				m_control_found_list.DeleteAllItems(); 
				n_device_found = 0;
				ResetConnectedDevice();
/*Update Key status*/
			    U8 key_status;
				int key_cnt;
				
				BT_ATE_GET_KEY_STATUS(&key_status, &key_cnt);


				char buf[MAX_PATH];

			    if(!key_status)
					sprintf(buf, "%d", key_cnt);
				else
					sprintf(buf, "%d", 0);

				m_control_key_cnt.SetWindowText(buf);	
				
				break;

		    case APP_STATE_IDLE_CONNECTING_FAIL:
			    m_button_discover.EnableWindow(TRUE);
			    m_button_connect.EnableWindow(FALSE);
			    m_button_setcodname.EnableWindow(FALSE);
			    m_button_sppsend.EnableWindow(FALSE);				
				m_button_assignaddr.EnableWindow(FALSE);
				m_button_discover.SetWindowTextA("Discover");
			    m_button_connect.SetWindowTextA("Connect");

				sprintf(str, "Connceting fail, press to discover %s", g_inquiry_name_filter);
				m_control_status.SetWindowText(str);

				m_control_found_list.DeleteAllItems(); 
				n_device_found = 0;
				ResetConnectedDevice();		

				break;

		    case APP_STATE_FINDING:
			    m_button_discover.EnableWindow(FALSE);
			    m_button_connect.EnableWindow(FALSE);
			    m_button_setcodname.EnableWindow(FALSE);
			    m_button_sppsend.EnableWindow(FALSE);		
				m_button_assignaddr.EnableWindow(FALSE);
				m_control_status.SetWindowTextA("Discovering...");
				break;

		    case APP_STATE_FOUND:
			{								
				// Address	
				sprintf(str, "%02X:%02X:%02X:%02X:%02X:%02X", 
					remote_addr[5], remote_addr[4], remote_addr[3], 
					remote_addr[2], remote_addr[1], remote_addr[0]);

				m_control_found_list.InsertItem(n_device_found, str);

				// COD
				sprintf(str, "0x%06X", remote_cod);
				m_control_found_list.SetItemText(n_device_found, 1,str);

				// RSSI
				sprintf(str, "%d dbm", remote_rssi);
				m_control_found_list.SetItemText(n_device_found, 2,str);

				// Name
				if(remote_rssi > g_rssi_filter){
					sprintf(str, "%s", g_inquiry_name_filter);
					m_control_found_list.SetItemText(n_device_found, 3,str);
					m_control_found_list.SetItemData(n_device_found, 1);
				} else {
					m_control_found_list.SetItemText(n_device_found, 3,"<RSSI Invalid>");
					m_control_found_list.SetItemData(n_device_found, 0);
				}

				//m_control_found_list.SetItemState(n_device_found, LVIS_SELECTED, LVIS_SELECTED);

				n_device_found++;

			    m_button_discover.EnableWindow(TRUE);
			    m_button_connect.EnableWindow(TRUE);
			    m_button_setcodname.EnableWindow(FALSE);
			    m_button_sppsend.EnableWindow(FALSE);		
				m_button_assignaddr.EnableWindow(FALSE);
				m_button_discover.SetWindowTextA("Discover");
			    m_button_connect.SetWindowTextA("Connect");

				sprintf(str, "%d device found.", n_device_found);
				m_control_status.SetWindowTextA(str);

				break;
	    	}

		    case APP_STATE_CONNECTING:
			    m_button_discover.EnableWindow(FALSE);
			    m_button_connect.EnableWindow(FALSE);
			    m_button_setcodname.EnableWindow(FALSE);
			    m_button_sppsend.EnableWindow(FALSE);		
				m_button_assignaddr.EnableWindow(FALSE);
			    m_button_discover.SetWindowTextA("Discover");
			    m_button_connect.SetWindowTextA("Connect");
				m_control_status.SetWindowTextA("Connecting...");
				break;

		    case APP_STATE_CONNECTED:				
			    m_button_discover.EnableWindow(FALSE);
			    m_button_connect.EnableWindow(TRUE);
			    m_button_setcodname.EnableWindow(TRUE);
			    m_button_sppsend.EnableWindow(TRUE);				
			    m_button_discover.SetWindowTextA("Discover");
			    m_button_connect.SetWindowTextA("Disconnect");
				m_control_status.SetWindowTextA("Connected");
				break;

		    case APP_STATE_DISCONNECTING:
			    m_button_discover.EnableWindow(FALSE);
			    m_button_connect.EnableWindow(FALSE);
			    m_button_setcodname.EnableWindow(FALSE);
			    m_button_sppsend.EnableWindow(FALSE);		
				m_button_assignaddr.EnableWindow(FALSE);
			    m_button_discover.SetWindowTextA("Discover");
			    m_button_connect.SetWindowTextA("Connect");
				m_control_status.SetWindowTextA("DisConnecting...");
				break;
		}
		
}


void CSPP_Egis_ManufactoryDlg::OnBnClickedOk()
{       
    if(app_state == APP_STATE_IDLE || app_state == APP_STATE_IDLE_CONNECTING_FAIL || app_state == APP_STATE_FOUND)//idle
	{
	    BT_ATE_INQUIRY((U8*)g_inquiry_name_filter, g_rssi_filter ,found_cb);
	    
		app_state = APP_STATE_FINDING; //finding

		m_control_found_list.DeleteAllItems(); 
		n_device_found = 0;
		ResetConnectedDevice();

		UpdateControl();
	}
}

void CSPP_Egis_ManufactoryDlg::OnBnClickedConnect()
{
	if(app_state == APP_STATE_FOUND) //found
	{
		POSITION pos;
		int selected_item;
		U8  selectedt_addr[6];
		char buf[MAX_PATH];
		
		pos = m_control_found_list.GetFirstSelectedItemPosition(); 
		if (pos == NULL) {
			MessageBox("Please select device to connect."); 
			return;
		}

		selected_item = m_control_found_list.GetNextSelectedItem(pos); 

		memset(buf, 0x00, sizeof(buf));
		m_control_found_list.GetItemText(selected_item, 0, buf, MAX_PATH);

		memset(selectedt_addr, 0x00, sizeof(selectedt_addr));
		selectedt_addr[5]=h2toi(buf);
		selectedt_addr[4]=h2toi(buf+3);
		selectedt_addr[3]=h2toi(buf+6);
		selectedt_addr[2]=h2toi(buf+9);
		selectedt_addr[1]=h2toi(buf+12);
		selectedt_addr[0]=h2toi(buf+15);
		
	    BT_ATE_CONNECT(selectedt_addr);

		app_state = APP_STATE_CONNECTING;
		UpdateControl();
	}
	else if(app_state == APP_STATE_CONNECTED) //connected
	{
	    BT_ATE_DISCONNECT();
		app_state = APP_STATE_DISCONNECTING;

		m_control_found_list.DeleteAllItems(); 
		n_device_found = 0;
		ResetConnectedDevice();
		UpdateControl();
	}
}

void 
CSPP_Egis_ManufactoryDlg::SetConnectedDevice(U8 is_addr_assigned, U32 cod, U16 fw_version, U8 *name){
    char str[MAX_PATH];

	sprintf(str, "0x%06X", cod);
	m_control_cod.SetWindowText(str);

	sprintf(str, "0x%04X", fw_version);
	m_control_fw.SetWindowText(str);
	
	m_control_name.SetWindowText((char*)name);

    U8 key_status;
	int key_cnt;
	
	BT_ATE_GET_KEY_STATUS(&key_status, &key_cnt);

	if(is_addr_assigned){
		m_control_is_addr_assigned.SetWindowText("YES");
		m_button_assignaddr.EnableWindow(FALSE);
	} else {
		m_control_is_addr_assigned.SetWindowText("NO");

		if(key_cnt == 0){
			m_button_assignaddr.EnableWindow(FALSE);
		} else {
			m_button_assignaddr.EnableWindow(TRUE);
		}

	}
			m_button_assignaddr.EnableWindow(TRUE); //talplay test

}

void 
CSPP_Egis_ManufactoryDlg::ResetConnectedDevice(){
	m_control_addr.SetWindowText("");
	m_control_is_addr_assigned.SetWindowText("");
	m_control_cod.SetWindowText("");
	m_control_fw.SetWindowText("");
	m_control_name.SetWindowText("");
	m_control_received.SetWindowText("");
}

void CSPP_Egis_ManufactoryDlg::OnBnClickedCodName()
{
	if(app_state == APP_STATE_CONNECTED) //connected
	{
		BT_ATE_SET_COD_AND_NAME(g_cod_to_set, g_name_to_set, set_cod_name_ready);
	}

}

void CSPP_Egis_ManufactoryDlg::OnBnClickedAssignAddr()
{

	// TODO: Add your control notification handler code here	
	if(app_state == APP_STATE_CONNECTED) //connected
	{
		BT_ATE_SET_BT_ADDR(&set_addr_ready);		
    }
}

void CSPP_Egis_ManufactoryDlg::OnBnClickedCsppdata()
{
	U16 message_len;
	U8 message_to_send[65536];
	
	CDlgSppSend dlg;

	if(dlg.DoModal()==IDOK){
		memset(message_to_send, 0x00, sizeof(message_to_send));
		strcpy((char*)message_to_send, dlg.m_message.operator LPCTSTR());

		message_len = (U16)strlen((char*)message_to_send);

		if(app_state == APP_STATE_CONNECTED) //connected
		{
			BT_ATE_SPP_SEND(message_len, message_to_send);
		}	
	}
}

void CSPP_Egis_ManufactoryDlg::OnTimer(UINT_PTR nIDEvent)
{	
	if(nIDEvent == TIMER_GET_FW)
	{
		BT_ATE_GET_FW_STATUS(&remote_internal_ready);
		KillTimer(TIMER_GET_FW);
	}
}

void CSPP_Egis_ManufactoryDlg::OnBnClickedCancel()
{
	if(app_state == APP_STATE_CONNECTED) //connected
	{
	    BT_ATE_DISCONNECT();
		app_state = APP_STATE_DISCONNECTING;
		UpdateControl();
	}

	OnCancel();
}

void CSPP_Egis_ManufactoryDlg::OnNMDblclkListFound(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: Add your control notification handler code here
	*pResult = 0;
	if(app_state == APP_STATE_FOUND) {
		OnBnClickedConnect();
	}
}


void 
CSPP_Egis_ManufactoryDlg::AddSppReceived(U16 len, U8 *dat){
	int  i;
	char buf[65536];
	char buf2[MAX_PATH];


	sprintf(buf, "spp received! len=%d.\n", len);
	OutputDebugString(buf);

	memset(buf, 0x00, sizeof(buf));	

	CString str;
	m_control_received.GetWindowText(str);

	for(i=0;i<len;i++){
		sprintf(buf2, "%02X ", dat[i]);
		strcat(buf, buf2);
	}
		
	str += buf;
	m_control_received.SetWindowText(str);
}


BYTE CheckSum(const BYTE *pData, int nLen)
{
	BYTE dwSum   = 0;
	int i = 1;

	dwSum = 0;

	for( i=0;i<nLen;i++)
	{
		dwSum = dwSum ^ pData[i];
	}

	return dwSum;

}

void CSPP_Egis_ManufactoryDlg::OnBnClickedGetversion()
{
	U16 message_len;
	U8 message_to_send[65536];	

	message_len = 5;

	memset(message_to_send, 0x00, sizeof(message_to_send));
	message_to_send[0] = 0x79;//y
	message_to_send[1] = 0x75;//u
	message_to_send[2] = 0x3a;//:
	message_to_send[3] = 0x08;
	message_to_send[4] = CheckSum( message_to_send+3, message_len-1 ); 

	message_len = (U16)strlen((char*)message_to_send);
	
	if(app_state == APP_STATE_CONNECTED) //connected
	{
		BT_ATE_SPP_SEND(message_len, message_to_send);
	}	

}

void CSPP_Egis_ManufactoryDlg::OnLvnItemchangingListFound(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: Add your control notification handler code here
	if(m_control_found_list.GetItemData(pNMLV->iItem) == 1){	
		*pResult = 0;
	} else {
		if ((pNMLV->uChanged & LVIF_STATE) && (pNMLV->uNewState & LVNI_SELECTED)) {
			// yes - never allow a selected item
			*pResult = 1;
		} else {
			*pResult = 0;
		}
	}	
}

void CSPP_Egis_ManufactoryDlg::OnBnClickedButtonSetting()
{
	char str[MAX_PATH];
	char cod_str[MAX_PATH];
	char rssi_str[MAX_PATH];

	sprintf(cod_str, "%06X", g_cod_to_set);
	sprintf(rssi_str, "%d", g_rssi_filter);

	CDlgSetting dlg;
	dlg.Preset((char*)g_inquiry_name_filter, rssi_str, cod_str,(char*) g_name_to_set);

	if(dlg.DoModal()==IDOK){
		strcpy((char*)g_inquiry_name_filter, dlg.m_name.operator LPCTSTR());
		strcpy((char*)rssi_str, dlg.m_rssi.operator LPCTSTR());
		strcpy((char*)cod_str, dlg.m_cod.operator LPCTSTR());
		strcpy((char*)g_name_to_set, dlg.m_name2.operator LPCTSTR());

		g_rssi_filter = atoi(rssi_str);

		g_cod_to_set = h2toi((char*)cod_str);
		g_cod_to_set <<= 8;

		g_cod_to_set += h2toi((char*)(cod_str+2));
		g_cod_to_set <<= 8;

		g_cod_to_set += h2toi((char*)(cod_str+4));

		m_control_name_filter.SetWindowText((char*)g_inquiry_name_filter);

		sprintf(str, "%d", g_rssi_filter);
		m_control_rssi_filter.SetWindowText(str);

		sprintf(str, "%06X", g_cod_to_set);
		m_control_cod_to_set.SetWindowText(str);

		m_control_name_to_set.SetWindowText((char*)g_name_to_set);

		WriteConfig();

	}
	
}

void 
CSPP_Egis_ManufactoryDlg::ReadConfig(){
	char ini_file[MAX_PATH];
	char str[MAX_PATH];

	sprintf(ini_file, "%s\\setting.ini", theApp.runtimepath);
	
	GetPrivateProfileString("SETTING", "NAME_FILTER", "YuKey!", str, MAX_PATH, ini_file);
	memset(g_inquiry_name_filter, 0x00, sizeof(g_inquiry_name_filter));
	strcpy((char*)g_inquiry_name_filter, str);

	GetPrivateProfileString("SETTING", "RSSI_FILTER", "-40", str, MAX_PATH, ini_file);
	g_rssi_filter = atoi(str);

	GetPrivateProfileString("SETTING", "COD_TO_SET", "002000", str, MAX_PATH, ini_file);
	g_cod_to_set = h2toi((char*)str);
	g_cod_to_set <<= 8;

	g_cod_to_set += h2toi((char*)(str+2));
	g_cod_to_set <<= 8;

	g_cod_to_set += h2toi((char*)(str+4));

	GetPrivateProfileString("SETTING", "NAME_TO_SET", "YuKey!", str, MAX_PATH, ini_file);
	memset(g_name_to_set, 0x00, sizeof(g_name_to_set));
	strcpy((char*)g_name_to_set, str);	

	m_control_name_filter.SetWindowText((char*)g_inquiry_name_filter);

	sprintf(str, "%d", g_rssi_filter);
	m_control_rssi_filter.SetWindowText(str);
	
	sprintf(str, "%06X", g_cod_to_set);
	m_control_cod_to_set.SetWindowText(str);

	m_control_name_to_set.SetWindowText((char*)g_name_to_set);
}

void 
CSPP_Egis_ManufactoryDlg::WriteConfig(){
	char ini_file[MAX_PATH];
	char str[MAX_PATH];

	sprintf(ini_file, "%s\\setting.ini", theApp.runtimepath);

	WritePrivateProfileString("SETTING","NAME_FILTER", (char*)g_inquiry_name_filter, ini_file);

	sprintf(str, "%d", g_rssi_filter);
	WritePrivateProfileString("SETTING","RSSI_FILTER", str, ini_file);

	sprintf(str, "%06X", g_cod_to_set);
	WritePrivateProfileString("SETTING","COD_TO_SET", str, ini_file);
	
	WritePrivateProfileString("SETTING","NAME_TO_SET", (char*)g_name_to_set, ini_file);
}
