#pragma once
#include "afxwin.h"


// CDlgSetting dialog

class CDlgSetting : public CDialog
{
	DECLARE_DYNAMIC(CDlgSetting)

public:
	CDlgSetting(CWnd* pParent = NULL);   // standard constructor
	virtual ~CDlgSetting();

	void Preset(char *filter_name, char *filter_rssi, char *cod_to_set, char *name_to_set);

// Dialog Data
	enum { IDD = IDD_DIALOG_SETTING };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CEdit m_control_name;
	CString m_name;
	CEdit m_control_rssi;
	CString m_rssi;
	CEdit m_control_cod;
	CString m_cod;
	CEdit m_control_name2;
	CString m_name2;
	virtual BOOL OnInitDialog();
};
