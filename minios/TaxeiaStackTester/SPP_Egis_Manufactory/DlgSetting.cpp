// DlgSetting.cpp : implementation file
//

#include "stdafx.h"
#include "SPP_Egis_Manufactory.h"
#include "DlgSetting.h"


// CDlgSetting dialog

IMPLEMENT_DYNAMIC(CDlgSetting, CDialog)

CDlgSetting::CDlgSetting(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgSetting::IDD, pParent)
	, m_name(_T(""))
	, m_rssi(_T(""))
	, m_cod(_T(""))
	, m_name2(_T(""))
{

}

CDlgSetting::~CDlgSetting()
{
}

void CDlgSetting::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_NAME, m_control_name);
	DDX_Text(pDX, IDC_EDIT_NAME, m_name);
	DDX_Control(pDX, IDC_EDIT_RSSI, m_control_rssi);
	DDX_Text(pDX, IDC_EDIT_RSSI, m_rssi);
	DDX_Control(pDX, IDC_EDIT_COD, m_control_cod);
	DDX_Text(pDX, IDC_EDIT_COD, m_cod);
	DDX_Control(pDX, IDC_EDIT_NAME2, m_control_name2);
	DDX_Text(pDX, IDC_EDIT_NAME2, m_name2);
}


BEGIN_MESSAGE_MAP(CDlgSetting, CDialog)
END_MESSAGE_MAP()

void 
CDlgSetting::Preset(char *filter_name, char *filter_rssi, char *cod_to_set, char *name_to_set){
	m_name = filter_name;
	m_rssi = filter_rssi;
	m_cod = cod_to_set;
	m_name2  = name_to_set;

}

// CDlgSetting message handlers

BOOL CDlgSetting::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  Add extra initialization here

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}
