// DlgSppSend.cpp : implementation file
//

#include "stdafx.h"
#include "SPP_Egis_Manufactory.h"
#include "DlgSppSend.h"


// CDlgSppSend dialog

IMPLEMENT_DYNAMIC(CDlgSppSend, CDialog)

CDlgSppSend::CDlgSppSend(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgSppSend::IDD, pParent)
	, m_message(_T(""))
{

}

CDlgSppSend::~CDlgSppSend()
{
}

void CDlgSppSend::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_MESSAGE, m_control_message);
	DDX_Text(pDX, IDC_EDIT_MESSAGE, m_message);
}


BEGIN_MESSAGE_MAP(CDlgSppSend, CDialog)
	ON_BN_CLICKED(IDOK, &CDlgSppSend::OnBnClickedOk)
END_MESSAGE_MAP()


// CDlgSppSend message handlers

void CDlgSppSend::OnBnClickedOk()
{
	if(m_control_message.GetWindowTextLength()==0){
		MessageBox("Please type message to send");
		return;
	}
	OnOK();
}
