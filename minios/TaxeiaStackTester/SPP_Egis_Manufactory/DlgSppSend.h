#pragma once
#include "afxwin.h"


// CDlgSppSend dialog

class CDlgSppSend : public CDialog
{
	DECLARE_DYNAMIC(CDlgSppSend)

public:
	CDlgSppSend(CWnd* pParent = NULL);   // standard constructor
	virtual ~CDlgSppSend();

// Dialog Data
	enum { IDD = IDD_DIALOG_SPPSEND };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CEdit m_control_message;
	CString m_message;
	afx_msg void OnBnClickedOk();
};
