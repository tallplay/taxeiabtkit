#ifndef __BT_PROFILE_SPP_HH_
#define __BT_PROFILE_SPP_HH_
#include "../BTType.h"
#include "../GAP/LOCAL_DEV/Bt__LocalDevice_V2.h"

///////////////////////////////////Public API///////////////////////////////////////////

void Bt__ProfileSPPCB(void (*)(U8, U16, U8 *) );
void Bt__SPPConnect(U16 aclHandle);
void Bt__SPPDisconnect(void);
U8 Bt__ProfileSPPConnectStatus(void);
U8 Bt__ProfileSPPSendData(U8 *dat, U16 len);
U8 Bt__ProfileSPPSendDataDirect(U8 *dat, U16 len);
U8 *Bt__ProfileSPPGetCallBackBuf(void);
void Bt__ProfileSPPScheduler(void);
#define IND_SIGNAL 0
#define IND_BATTARY 1
#define IND_SERVICE 2
#define IND_CALLSETUP 3
#define IND_CALL 4
#define IND_ROAM 5
#define IND_CALLHELD 6

#define SPPCB_PBITEM 0x3118
#define SPPCB_PBNUMBER 0x3119

///////////////////////////////////Private Functions////////////////////////////////////

#define MSG_TYPE_HANDSHAKE 0
#define MSG_TYPE_HID_CTL 1
#define MSG_TYPE_GET_REPORT 4
#define MSG_TYPE_SET_REPORT 5
#define MSG_TYPE_GET_PROTOCOL 6
#define MSG_TYPE_SET_PROTOCOL 7
#define MSG_TYPE_SET_IDLE 9
#define MSG_TYPE_DATA 0xA

void Bt__ProfileSPPInit(void);
void Bt__ProfileSPPDeInit(void);

#define INVALID_RFCOMM_MUX 0xFF
#define INVALID_DLC_ID     0xFF
#define MAX_CMD_RSP_SIZE   11

/* Control Channel Commands */
#define PN     0x81
#define TEST   0x21
#define FCON   0xA1
#define FCOFF  0x61
#define MSC    0xE1
#define NSC    0x11
#define RPN    0x91
#define RLS    0x51

/* Credit based flow control tokens */
#define CREDIT_FLOW_REQ 0xF0
#define CREDIT_FLOW_RSP 0xE0

/* RFCOMM Control Fields and events */
#define SABM    0x3F
#define UA_F    0x73
#define DM_F    0x1F
#define DM      0x0F
#define DISC    0x53
#define UIH     0xEF
#define UIH_F   0xFF

/* Address bits */
#define EA_BIT 0x01
#define CR_BIT 0x02
#define D_BIT  0x04


#endif

