#include "BtConfig.h"
#include "Bt__Profile_SPP.h"
#include "../TaxeiaConsoleDebug/TaxeiaConsoledebug.h"
#include "../SDP/SDPS/Bt__Profile_SDPS.h"
#include "../CORE/BTCore.h"
#include "../rfcomm/rfcomm.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#if  (PROFILE_TYPE == PROFILE_TYPE_SPP || PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
///////////////////////////////////Public API///////////////////////////////////////////

///////////////////////////////////Private Functions////////////////////////////////////
/* Handsfree Profile */
#define PROF_SPP                    0x1101

#define PROT_L2CAP                  0x0100
#define PROT_RFC                    0x0003

static const U8 SppRecordHandle [] = {
    SDP_UINT_SIZE32(0x00010000)
};

static const U8 SppRecordState [] = {
    SDP_UINT_SIZE32(0x20111118)
};

static const U8 SppServClassIdVal[] = {
    SDP_DATA_ELEMENT_SEQ_SIZE8(3),           /* Data element sequence, 3 bytes */
    SDP_UUID_SIZE16(PROF_SPP), /* Uuid16 SPP */
};

static const U8 SppProtocolDescListVal[] = {
    SDP_DATA_ELEMENT_SEQ_SIZE8(12),  /* Data element sequence, 12 bytes */
    SDP_DATA_ELEMENT_SEQ_SIZE8(3),   /* Data element sequence for L2CAP, 3 bytes */     
    SDP_UUID_SIZE16(PROT_L2CAP),  /* Uuid16 L2CAP */
    SDP_DATA_ELEMENT_SEQ_SIZE8(5),   /* Data element sequence for RFCOMM, 5 bytes */
    SDP_UUID_SIZE16(PROT_RFC),  /* Uuid16 rfcomm */
    SDP_UINT_SIZE8(03)
};

        
static const U8 SppLangBaseVal[] = {
    SDP_DATA_ELEMENT_SEQ_SIZE8(9),   /* Data element sequence, 9 bytes */
    SDP_UINT_SIZE16(0x656E),      /* Uint16 "en" (English) */
    SDP_UINT_SIZE16(0x006A),      /* Uint16 UTF-8 encoding */
    SDP_UINT_SIZE16(0x0100)       /* Uint16 primary language base ID */
};


static const U8 Spp_ServiceName[] = {
    SDP_TEXT_SIZE8(19),          /* Null terminated text string */
    'B', 'T', ' ', 'T', 'a', 'x', 'e', 'i', 'a', ' ', 'S','P', 
    'P', ' ', 'K', 'i', 't', ' ' , '\0'
};


//sdp structure begin
SdpAttr spp_attrs[] = {
    {ATTRID_SERVICE_RECORD_HANDLE, sizeof(SppRecordHandle), SppRecordHandle, 0},
        
    /* Service Class ID List attribute */
    {ATTRID_SERVICE_CLASS_ID_LIST, sizeof(SppServClassIdVal), SppServClassIdVal, 0},

    /* Protocol Descriptor List attribute */
    {ATTRID_PROTOCOL_DESC_LIST, sizeof(SppProtocolDescListVal), SppProtocolDescListVal, 0},

    {ATTRID_LANG_BASE_ID_LIST, sizeof(SppLangBaseVal), SppLangBaseVal, 0},

    {(ATTRID_SERVICE_NAME + 0x0100), sizeof(Spp_ServiceName), Spp_ServiceName, 0},

};
//sdp structure end

typedef enum
{
    SPP_IDLE,
    SPP_RFCOMM_CONNECTED,
    SPP_CONNECTED,
    SPP_SUICIDE,
}SPP_STATE;

SPP_STATE SppState = SPP_IDLE;

#define SPP_CONNECT 8
#define AT_REQ_VENDOR 9

U8 SppRequest = 0;
U16 SppRequestParm;

void (*sppcb)(U8, U16, U8 *)  = 0;

static U8 *SPPRFCOMMCB (U16 parm)
{
    if(parm == 2) //l2cap disconnected
    {
        sppcb(2, 0, 0);
    }
    
    return 0;
}
    
static struct rfcomm_dlc *rfcomm_d = 0;

static U8 *cbbuf = 0;

extern U8* BarrowBuffer();
    
void SppRrcommDataRdy(struct rfcomm_dlc *d, U16 len, U8 *dat)
{
    sppcb(0, len, dat); //data ready
    //Taxeia_Report("SPP <= %s\r\n", dat);
}

void SppRfcommStsChg(struct rfcomm_dlc *d, int err)
{
    if(d->state == BT_CONNECTED) //just connect
    {
        SppState = SPP_RFCOMM_CONNECTED;
        rfcomm_d = d;
    }
    else if(d->state == BT_CLOSED)
    {
        SppState = SPP_IDLE;
    }
}

U8 SppRfcommSend(U8 *dat, U16 len)
{
    if(!rfcomm_d->tx_len)
    {
        //Taxeia_Report("SPP => %s\n", dat);

        rfcomm_d->tx_dat = dat;
        rfcomm_d->tx_len = (U8)len;
        return 1;
    }
    Taxeia_Report("SPP => %s send fail!!\r\n", dat);
  
    return 0;
}

/*
void SppRfcommTest(U8 *a, U8 *b, U8 *c)
{
    *a = rfcomm_d->tx_len;
    *b = rfcomm_d->tx_credits;
    *c = Bt_AclReady();
}*/

U8 SppRfcommAva(void)
{
    //Taxeia_Report("%d", !rfcomm_d->tx_len && rfcomm_d->tx_credits && Bt_AclReady());
    return (!rfcomm_d->tx_len && rfcomm_d->tx_credits && Bt_AclReady());
}

void Bt__ProfileSPPScheduler(void)
{
    switch(SppState)
    {
        case SPP_IDLE:
            if(SppRequest == SPP_CONNECT)
            {
                if(Bt_AclReady())
                {
                    Bt__RfCommConnect(*(U16*)BarrowBuffer(), (U8)SppRequestParm, SppRrcommDataRdy, SppRfcommStsChg);
                    SppRequest = 0;
                }
            }
            break;
        case SPP_RFCOMM_CONNECTED:
                SppState = SPP_CONNECTED;
            break;
    }
}

void Bt__ProfileSPPGetInquiryData(U8 *num, SdpAttr **attr, U32 *CoD)
{
    *num = sizeof(spp_attrs) / sizeof(SdpAttr);
    *attr = spp_attrs;
    *CoD = 0x2000;
}

void Bt__ProfileSPPInit()
{
    SppState = SPP_IDLE;
    rfcomm_d = 0;
    Bt_RfCommChannelRegister(3, SppRrcommDataRdy, SppRfcommStsChg);
}

void Bt__ProfileSPPDeInit()
{
    SppState = SPP_IDLE;
    rfcomm_d = 0;
    sppcb = 0;
}

///////////////////////////////////Public API///////////////////////////////////////////
U8 *Bt__ProfileSPPGetCallBackBuf()
{
    return cbbuf;
}


U8 Bt__ProfileSPPSendDataDirect(U8 *dat, U16 len)
{
    return SppRfcommSend(dat, len);
}

U8 Bt__ProfileSPPSendData(U8 *dat, U16 len)
{
    return SppRfcommSend(dat, len);
}

//0: not connect 1: connect 2: connecting
U8 Bt__ProfileSPPConnectStatus(void)
{
    switch(SppState)
    {
        case SPP_IDLE:
            return 0;
        case SPP_CONNECTED:
            return 1;
        default:
            return 2;
    }
    return 2;
}

void Bt__SPPDisconnect(void)
{
    Bt__RfCommDisconnect(rfcomm_d);
    SppState = SPP_SUICIDE;
}


void * spp_sdpc_callback(U8 op, U16 aclHandle, U16 cid, U16 len, U8 *dat)
{
    if(op == 1)//l2cap connect
        SDPCSendSearviceSearchAttributeReq(aclHandle, cid, 0x1101, 0x0004);
    else if(op == 0) //l2cap disconnect
    {
        if(SppRequestParm) //channel number exist
            SppRequest = SPP_CONNECT;
        return 0;
    }
    else if(op == 2) //data
    {
        SppRequestParm = dat[len - 1]; //channel number
        *(U16 *)BarrowBuffer() = aclHandle;
    }
        

    return (void*)spp_sdpc_callback;
}

void Bt__SPPConnect(U16 aclHandle)
{
    Bt__ProfileSDPCConnect(aclHandle, spp_sdpc_callback);
}


void Bt__ProfileSPPCB(void (*fc)(U8, U16, U8 *))
{
    sppcb = fc;
    Bt__RfCommCB(SPPRFCOMMCB);
}

#endif


