#ifndef __KEYPRO_WRITER_HH_
#define __KEYPRO_WRITER_HH_

#define TESTRESULT_VALID_KEY 0
#define TESTRESULT_GETUID_FAIL 1
#define TESTRESULT_AUTH_FAIL 2
#define TESTRESULT_ENC_TEST_FAIL 3
#define TESTRESULT_DEC_TEST_FAIL 4
#define TESTRESULT_INVALID_KEY 5

int OpenKey();
int FindKey();
int ValideateKey();
int TestKeyTimes();
int MinusKeyTimes();
//4 bytes pid 8 byete kid
void GetKeyProductIDs(unsigned char *pid, unsigned char *kid);


//validate key example
#if 0
    int num, ret;

	num = FindKey();
  
	if(num == 0)
	{
		//"Fail!! No Key Found!!";
		return;
	}

	if(OpenKey())
	{
		//"Fail!! Open Key Fail!!";
		return;
	}

	ret = ValideateKey();
	switch(ret)
	{
    	case TESTRESULT_VALID_KEY:
		{
			//"Success!!";
			break;
		}
	    case TESTRESULT_GETUID_FAIL:
		   //"Fail!! Get UID Fail!!";
			return;
        case TESTRESULT_AUTH_FAIL:
		    //"Fail!! Authenticate Fail!!";
			return;
        case TESTRESULT_ENC_TEST_FAIL:
    		//"Fail!! Encrypt Fail!!";
			return;
        case TESTRESULT_DEC_TEST_FAIL:
    		//"Fail!! Descrypt Fail!!";
			return;
        case TESTRESULT_INVALID_KEY:
        	//"InValid Key Pro!!";
			return;
	}

	return;
#endif

//test key residue
#if 0
return TestKeyTimes();
#endif

//minuse key residue by 1
#if 0
MinusKeyTimes();
#endif

#endif