
#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions

#include "KeyProWriter.h"

extern "C" {
#include "bttype.h"
#include "btconfig.h"

#include "HAL\LocalOutput.h"
#include "sdkapi\bt_hid_hfp_api.h"
/*private API begin*/
U8 BTCoreHwHealth(void);
void bt_ate_send_data(U16 len, U8 *dat);
void bt_ate_register_callback(void (*cb)(U16 len, U8 *dat));

typedef void (*InquiryCallback) (U8, U8 *);
void Bt__InquiryCancel(void);
void Bt__InquiryNormal(U8 times, InquiryCallback ic);
void Bt__InquiryMode(U8 mode);

typedef void (*CompleteDataHandler)(U8 CmdOp, U8 EvtOp, U8 *dat);
void Bt__RemoteNameReq(U8 *parm, CompleteDataHandler cb);

U8 Bt_AclReady(void);
void Bt__GenerateData(U8 *dat, U8 *cout);

/*private API end*/

U32 AssignBDAddr(void);
void AssignLinkKeyFileName(char *name);
void BT_ATE_GET_FW_STATUS(void (*ready_cb)(U8 is_addr_assigned, U32 cod, U16 fw_version, U8 *name));

}

/////////////////////////////////KEY////////////////////////////////////////////////////////////////////////
#define KEYPRO_KEY			    "MTKSPP01"

#define SECURITY_KEY_STATUS_VALID 0
#define SECURITY_KEY_STATUS_NOT_FOUND 1
#define SECURITY_KEY_STATUS_INVALID 2

U8 key_sta = SECURITY_KEY_STATUS_NOT_FOUND;
int key_cnt = 0;

void key_init(void)
{
	BOOL		r_value;
	int			n_device;

	key_sta = SECURITY_KEY_STATUS_NOT_FOUND;
	n_device = FindKey();
	if(n_device == 0){
//		MessageBoxA("MassProduction Key Not Found!!");
		//EndDialog(0);
		return;
	}

	r_value = OpenKey();
	if(r_value){
//		MessageBoxA("MassProduction Key Not Found!!");
		//EndDialog(0);
		return;
	}

	r_value = ValideateKey();

	key_sta = SECURITY_KEY_STATUS_INVALID;

	switch (r_value)
	{
		case TESTRESULT_VALID_KEY:
			key_sta = SECURITY_KEY_STATUS_VALID;
			break;
		
	    case TESTRESULT_GETUID_FAIL:
		   //"Fail!! Get UID Fail!!";			
        case TESTRESULT_AUTH_FAIL:
		    //"Fail!! Authenticate Fail!!";
        case TESTRESULT_ENC_TEST_FAIL:
    		//"Fail!! Encrypt Fail!!";
        case TESTRESULT_DEC_TEST_FAIL:
    		//"Fail!! Descrypt Fail!!";
        case TESTRESULT_INVALID_KEY:
        	//"InValid Key Pro!!";
		default:
//			MessageBoxA("MassProduction Key Invalid(-1)!!");
			//EndDialog(0);
			return;
	}

	unsigned char pid[9];
	unsigned char kid[9];

	memset(pid, 0x00, sizeof(pid));
	memset(kid, 0x00, sizeof(kid));

	GetKeyProductIDs(pid, kid);

	if(memcmp(pid, KEYPRO_KEY, 8)){
//		MessageBoxA("MassProduction Key Invalid(-2)!!");
		key_sta = SECURITY_KEY_STATUS_INVALID;
		//EndDialog(0);
		return;	
	} 
	

    key_cnt = TestKeyTimes();
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
char bt_state_str[41][30] =
{
    "BT_STA_STOP",
    "BT_STA_POWERUP",
    "BT_STA_BOOT",
    "BT_STA_INITILIZED",
    "BT_STA_PAIRING",
    "BT_STA_CONNECTING",
    "BT_STA_CONNECTED",
    "BT_STA_HID_CONNECTED",
    "BT_STA_HFP_CONNECTED",
    "BT_STA_HID_HFP_CONNECTED",
    "BT_STA_DISCONNECTING",
    "BT_STA_DISCONNECTED",
    "BT_STA_BT_FAIL",
    "BT_STA_DISCONNECT_RETRY",
    "BT_STA_LOCK",
    "UNKNOW!!",
    "UNKNOW!!",
    "UNKNOW!!",
    "UNKNOW!!",
    "UNKNOW!!",
    "UNKNOW!!",
    "UNKNOW!!",
    "UNKNOW!!",
    "UNKNOW!!",
    "UNKNOW!!",
    "UNKNOW!!",
    "UNKNOW!!",
    "UNKNOW!!",
    "UNKNOW!!",
    "UNKNOW!!",
    "UNKNOW!!",
    "UNKNOW!!",
    "UNKNOW!!",
    "UNKNOW!!",
    "UNKNOW!!",
    "UNKNOW!!",
    "UNKNOW!!",
    "UNKNOW!!",
    "UNKNOW!!",
    "UNKNOW!!",
    "BT_STA_SPP_CONNECTED"
};

U8 BtThread(U8 execute);

void AssignLinkKeyFileName(char *name)
{
    strcpy(name, "SPP_Egis_Link");
}

U32 AssignBDAddr(void)
{
	srand(GetTickCount());
		
    return rand();
}


#define BT_CONN_STS_INITING 0
#define BT_CONN_STS_IDLE 1
#define BT_CONN_STS_CONNECTING 2
#define BT_CONN_STS_CONNECTED 3
#define BT_CONN_STS_DISCONNECTING 4
#define BT_CONN_STS_HWFAIL 5

#define BT_INQUIRY_END_NOT_FOUND 0
#define BT_INQUIRY_END_FOUND 1
#define BT_INQUIRY_NOT_END_FOUND_NOT_QUALIFY 2

U8 fwsta_addrassigned;
//U32 *fwsta_cod;
//U16 *fwsta_version;
U8 *fwsta_name;
void (*fwsta_ready)(U8 is_addr_assigned, U32 cod, U16 fw_version, U8 *name);
void (*cod_name_cb)(void);
void (*activated_cb)(U8 bd_addr[6], U8 key_status);
U8 inquiry_name_filter[30];
S8 rssi_filter;
void(*inquiry_callback)(U8 is_found, U32 cod, S8 rssi, U8 bd_addr[6]);
void (*spp_data_callback)(U16 len, U8 *dat);
void (*bt_connect_state_callback)(U8 sts);

U8 inquiry_name[50];
U8 inquiry_addr[6];
U32 inquiry_cod;
S8 inquiry_rssi;
U16 inquiry_clk_offset;
U8 inquiry_repetition;
U8 remote_name_requesting = 0;

U8 thread_inside = 0;

extern "C" {
	extern HWND StartHciLog(HINSTANCE , BYTE);
};

U8 BtThread(U8 execute)
{
	static HANDLE OSThread = (HANDLE)-1;

    if(execute == 0) //Start Thread
	{
		if(OSThread != (HANDLE) -1) //already runing
			return 2;
		
		DWORD dwThreadID;

		bt_start(GetTickCount(), BT_WMODE_NO_SCAN, PROFILE_TYPE_SPP);
		
		OSThread = CreateThread((LPSECURITY_ATTRIBUTES)NULL,
		                          0,
		                          (LPTHREAD_START_ROUTINE)BtThread,
		                          (LPVOID)3,
		                          0, &dwThreadID);

		return 0;
	}
	else if(execute == 1) //Stop Thread
	{
		if(OSThread != (HANDLE)-1)
		{
			bt_stop();
		    TerminateThread(OSThread, 0);
			CloseHandle(OSThread);
		    OSThread = (HANDLE)-1;			
			return 0;
		}
	    return 1;
	}
	else if(execute == 2) //test if device unplug
	{
	    return BTCoreHwHealth();
	}

    /////Thread Body/////////////////////
	while(1)  
	{		
	   static U8 bt_cur_state = 0xff;

		if(!bt_task(GetTickCount()))
		{
			Sleep(4);
	    }

		if(bt_cur_state != bt_state())
		{
		    //additional setting
			if(bt_cur_state < BT_STA_INITILIZED && bt_state() >=BT_STA_INITILIZED)
				Bt__InquiryMode(1); //Inquiry with RSSI

		    bt_cur_state = bt_state();
			
			LocalOutput("<<<%s>>>\n", bt_state_str[bt_cur_state]);

			if(bt_cur_state < BT_STA_INITILIZED)
			{

				bt_connect_state_callback(0);
			}
			else if(bt_cur_state == BT_STA_BT_FAIL)
				bt_connect_state_callback(5);
			else if(bt_cur_state == BT_STA_CONNECTING || bt_cur_state == BT_STA_CONNECTED)
				bt_connect_state_callback(2);
			else if(bt_cur_state == BT_STA_SPP_CONNECTED)
			{
				while(bt_task(GetTickCount())); //wait msc process
                bt_gap_writesupervision_timeout(0x640); //1s
           		while(bt_task(GetTickCount()));
				BT_ATE_GET_FW_STATUS(0);
           		while(bt_task(GetTickCount()));

				bt_connect_state_callback(3);
		    }
			else if(bt_cur_state == BT_STA_DISCONNECTING)
				bt_connect_state_callback(4);
			else 
			{
				bt_connect_state_callback(1);
			}
		}			
    }
	return 0;
}
void ate_data_cb(U16 len, U8 *dat)
{
    if(dat[0] == 0x00) //bt internal status
	{	
	    fwsta_addrassigned = !dat[1];
		if(fwsta_ready)
			fwsta_ready(!dat[1], dat[2] + (dat[3] << 8) + (dat[4] <<16), dat[5] + (dat[6] << 8), dat+15);
		LocalOutput("fwsta_addrassigned = %d\n", fwsta_addrassigned);
	}
	else if(dat[0] == 0x88) //set ok
	{
	    cod_name_cb();
	}
	else if(dat[0] == 0x87) //activating ok
	{
	    dat[16] = inquiry_addr[3];
	    dat[17] = inquiry_addr[4];
	    dat[18] = inquiry_addr[5];
	    activated_cb(dat+13, key_sta);
	}
}

void spp_data_cb(U16 len, U8 *dat)
{
    spp_data_callback(len, dat);
}

void Bt__remote_nameCB(U8 CmdOp, U8 EvtOp, U8 *dat)
{
    if(EvtOp == 0x07/*HCE_REMOTE_NAME_REQ_COMPLETE*/ && dat[0] == 0x00/*HC_STATUS_SUCCESS*/)
    {
        U32 len;
        len = (size_t)strlen((char*)dat+7);
        len = len > 49 ? 49 : len;        
        memcpy(inquiry_name, dat+7, (U8)len);
        inquiry_name[len] = 0;

		LocalOutput("%s found\n", inquiry_name);

		if(!memcmp(inquiry_name, inquiry_name_filter, 6))
		{
		    inquiry_callback(BT_INQUIRY_END_FOUND, inquiry_cod, inquiry_rssi, dat+1);
		}
		else
		{
			inquiry_callback(BT_INQUIRY_END_NOT_FOUND, 0, 0, 0); //not found
		}
    }
    else if(EvtOp == 0x07/*HCE_REMOTE_NAME_REQ_COMPLETE*/ || (EvtOp == 0x0F/*HCE_COMMAND_STATUS*/ && dat[0] != 0x00/*HC_STATUS_SUCCESS*/))
    {
		inquiry_callback(BT_INQUIRY_END_NOT_FOUND, 0, 0, 0); //not found
    }
	remote_name_requesting = 2;
}

void SPPInquiryCB(U8 ret, U8 * dat)
{
	U8 local_inquiry_addr[6];
	U32 local_inquiry_cod;
	S8 local_inquiry_rssi;
	U16 local_inquiry_clk_offset;
	U8 local_inquiry_repetition;

    if(ret == 1) //command fail!!
	{
	    if(dat[0] == 0xC)
    	{
		    LocalOutput("Stop Inquiry \n");
			Bt__InquiryCancel();
//			remote_name_requesting = 0;
    	}

		inquiry_callback(BT_INQUIRY_END_NOT_FOUND, 0, 0, 0); //not found
	}
    else if(ret == 0) //timeout or maximun result
    {
		inquiry_callback(BT_INQUIRY_END_NOT_FOUND, 0, 0, 0); //not found

	    LocalOutput("Inquiry Complete!!!\n");
		remote_name_requesting = 0;
    }
    else if(ret == 2) //without RSSI
    {
        char str[100];
		U8 parm[15];

    
        memcpy(local_inquiry_addr, dat+1, 6);

        local_inquiry_cod = (dat[12] << 16) + (dat[11] << 8) + dat[10];
		local_inquiry_rssi = 0;
		local_inquiry_clk_offset = dat[13] + (dat[14] << 8);
		
        if(remote_name_requesting == 0)
    	{

			if(local_inquiry_addr[3] == 0xe7 && local_inquiry_addr[4] == 0x0c && local_inquiry_addr[5] == 0x00)//MTK
			{
                LocalOutput("%2x:%2x:%2x:%2x:%2x:%2x rssi = %d\n", local_inquiry_addr[5], local_inquiry_addr[4], local_inquiry_addr[3], local_inquiry_addr[2], local_inquiry_addr[1], local_inquiry_addr[0], local_inquiry_rssi);
			    if(local_inquiry_rssi > rssi_filter)
		    	{

					inquiry_cod = local_inquiry_cod;
					inquiry_rssi = local_inquiry_rssi;
					inquiry_clk_offset = local_inquiry_clk_offset;
					
			        /*finding name*/
					memcpy(inquiry_addr, local_inquiry_addr, 6);
					memcpy(parm, inquiry_addr, 6);
					parm[6] = dat[7]; //paging scan repetition
					parm[7] = 0;
					parm[8] = dat[13];
					parm[9] = dat[14] | 0x80; //valid clockoffset

		            Bt__InquiryCancel();
					Bt__RemoteNameReq(parm, Bt__remote_nameCB);
					remote_name_requesting = 1;
			     }
				 else
	 			    inquiry_callback(BT_INQUIRY_NOT_END_FOUND_NOT_QUALIFY, local_inquiry_cod, local_inquiry_rssi, local_inquiry_addr);	

			}
    	}
	}
    else if(ret == 3) //result with RSSI
    {
        char str[100];
		U8 parm[15];

   
        memcpy(local_inquiry_addr, dat+1, 6);

        local_inquiry_cod = (dat[11] << 16) + (dat[10] << 8) + dat[9];
		local_inquiry_rssi = dat[14];
		local_inquiry_clk_offset = dat[12] + (dat[13] << 8);
		local_inquiry_repetition = dat[7];

        if(remote_name_requesting == 0)
    	{
			if(local_inquiry_addr[3] == 0xe7 && local_inquiry_addr[4] == 0x0c && local_inquiry_addr[5] == 0x00)//MTK
			{
                LocalOutput("%02x:%02x:%2x:%02x:%02x:%02x rssi = %d\n", local_inquiry_addr[5], local_inquiry_addr[4], local_inquiry_addr[3], local_inquiry_addr[2], local_inquiry_addr[1], local_inquiry_addr[0], local_inquiry_rssi);
			    if(local_inquiry_rssi > rssi_filter)
		    	{
				    LocalOutput("Request name!\n");
			        /*finding name*/
					inquiry_cod = local_inquiry_cod;
					inquiry_rssi = local_inquiry_rssi;
					inquiry_clk_offset = local_inquiry_clk_offset;
					inquiry_repetition = local_inquiry_repetition;

					memcpy(inquiry_addr, local_inquiry_addr, 6);
					memcpy(parm, inquiry_addr, 6);
					parm[6] = inquiry_repetition; //paging scan repetition
					parm[7] = 0;
					parm[8] = dat[12];
					parm[9] = dat[13] | 0x80; //valid clockoffset

		            Bt__InquiryCancel();
					Bt__RemoteNameReq(parm, Bt__remote_nameCB);
	                remote_name_requesting = 1;				
			    }
				else
				{
				    LocalOutput("Report not qualify!\n");
	 			    inquiry_callback(BT_INQUIRY_NOT_END_FOUND_NOT_QUALIFY, local_inquiry_cod, local_inquiry_rssi, local_inquiry_addr);						
			    }
				 
			}
		}
    }
}


void BT_ATE_INIT(void (*spp_cb)(U16 len, U8 *dat), void (*bt_state_changed)(U8 sts))
{
	bt_init();
	bt_ate_register_callback(&ate_data_cb);
    bt_spp_register_callback(&spp_data_cb);

	if(BtThread(2))
	{	
	    BtThread(1); 
	    BtThread(0); 
    }

    spp_data_callback = spp_cb;
	bt_connect_state_callback = bt_state_changed;
	key_init();
}

void BT_ATE_CONNECT(U8 BDADDR[6])
{
	fwsta_addrassigned = 0;
	bt_gap_connect(BDADDR);
}

void BT_ATE_DISCONNECT(void)
{
	bt_gap_disconnect();
}

void BT_ATE_GET_FW_STATUS(void (*ready_cb)(U8 is_addr_assigned, U32 cod, U16 fw_version, U8 *name))
{
	//wait for bt ready
	while(!Bt_AclReady())
	{
		MSG msg;
		PeekMessage(&msg, 0, 0, 0, PM_NOREMOVE);
		Sleep(1);
	}

	bt_ate_send_data(1, (U8*)"\x00"); //get bt internal status
	fwsta_ready = ready_cb;
}

void BT_ATE_SET_COD_AND_NAME(U32 cod, U8 *name, void(*ready_cb)(void))
{
	U8 buf[34];

	U8 len;

	len = strlen((char*)name);

	buf[0] = 0x88; //set cod and name
	buf[1] = cod & 0xff;
	buf[2] = (cod >> 8) & 0xff;
	buf[3] = (cod >> 16) & 0xff;
	buf[4] = len;
	memcpy(buf+5, name, len);
	bt_ate_send_data(5+len, buf);
	
	cod_name_cb = ready_cb;
}

void BT_ATE_GET_KEY_STATUS(U8 *status, int *count)
{
    *status = key_sta;
	*count = key_cnt;
    return;
}

void BT_ATE_SET_BT_ADDR(void(*ready_cb)(U8 bd_addr[6], U8 status))
{
    U8 dat[33];

	key_cnt = TestKeyTimes();

	if(key_sta == SECURITY_KEY_STATUS_VALID && key_cnt)
	{
	    dat[0] = 0x87; //activating
	    dat[14] = 0;
	    dat[15] = 0;
	    dat[16] = 0;

		Bt__GenerateData(dat+1, dat+1);
	    bt_ate_send_data(33, dat);

	    activated_cb = ready_cb;

		if(!fwsta_addrassigned)
		{
        	MinusKeyTimes();
	    	key_cnt = TestKeyTimes();
		}
	}
	else
	{
		memset(dat, 0, 6);
		ready_cb(dat, key_sta);
	}
}

void BT_ATE_INQUIRY(U8 *name_filter, S8 rssifilter, void(*inquiry_cb)(U8 is_found, U32 cod, S8 rssi, U8 bd_addr[6]))
{
    strcpy((char*)inquiry_name_filter, (char*)name_filter);
	inquiry_callback = inquiry_cb;
	rssi_filter = rssifilter;
	Bt__InquiryNormal(0xff, SPPInquiryCB);
	remote_name_requesting = 0;
}

void(*rssiget_cb)(S8);
void rssi_cb(U8 CmdOp, U8 EvtOp, U8 *dat)
{
    rssiget_cb((S8)dat[3]);
}

void BT_ATE_READ_RSSI(void(*rssi_callback)(S8))
{
    bt_gap_read_link_rssi(rssi_cb);
	rssiget_cb = rssi_callback;
}


void BT_ATE_SPP_SEND(U16 len, U8 *dat)
{
    while(!bt_spp_tx_ava())
	{
		MSG msg;
		PeekMessage(&msg, 0, 0, 0, PM_NOREMOVE);
		Sleep(1);
	}

    bt_spp_tx(len, dat);
}

