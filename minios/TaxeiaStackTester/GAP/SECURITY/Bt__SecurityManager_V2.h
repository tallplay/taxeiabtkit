#ifndef __BT__SECURITY_MANAGER__HH_
#define __BT__SECURITY_MANAGER__HH_

#include "Btconfig.h"

///////////////////////////////////Public API///////////////////////////////////////////
U8 Bt__EnumPairedDevice(U8 *bdaddr);
void Bt__UserConformReply(void);
U8 Bt__GetPairedDevice(U8 *bdaddr, U8 idx);

#endif
