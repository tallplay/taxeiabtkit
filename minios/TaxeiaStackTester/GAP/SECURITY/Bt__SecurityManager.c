#include "../HCI/BTCmdEvt.h"
#include "../../TaxeiaConsoleDebug/TaxeiaConsoledebug.h"
#include "Bt__SecurityManager.h"
#include "../../HAL/LocalStorage.h"
#include <string.h>
#include "../LOCAL_DEV/Bt__LocalDevice.h"

///////////////////////////////////Private Functions/////////////////////////////////////
//0: not exist(return empty slot) 1: exist(return use slot)
//3: full (return nothing)

U8 Bt__SecuritySearchDevice(U8 *bdaddr, U16 *addr)
{
    U16 adr;
    U8 tmp[6], tmpff[6];

    memset(tmpff, 0xff, 6);
    
    adr = 0;
    *addr = 0xffff;
    while(adr < SM_SIZE)
    {
        StorageGetBytes(adr, 6, tmp);
        if(!memcmp(tmpff, tmp, 6))//all ff
        {
            if(*addr != 0xffff) //exist
            {
                return 1; //exist
            }

            *addr = adr;
            return 0; //not exist
        }
        else if(!memcmp(tmp, bdaddr, 6))//not all ff, same as search
        {
            *addr = adr;
        }
        
        adr += 32;
    }

    if(*addr != 0xffff) //exist
    {
        return 1; //exist
    }

    return 3; //full

}

//1: exist 0: no
#if 0
U8 Bt__EnumPairedDevice(U8 *bdaddr)
{
    U16 adr;
    U8 tmp[6], tmpff[6];

    adr = 0;
    memset(tmpff, 0xff, 6);
    memset(bdaddr, 0xff, 6);

    while(adr < SM_SIZE)
    {
        StorageGetBytes(adr, 6, tmp);
        if(!memcmp(tmpff, tmp, 6))//all ff
        {
            if(memcmp(bdaddr, tmpff, 6))
                return 1; //exist
            return 0; //not exist
        }
        else
            memcpy(bdaddr, tmp, 6); //exist
        
        adr += 32;
    }

    if(memcmp(bdaddr, tmpff, 6))
        return 1; //exist

    return 0;    
}
#else
U8 Bt__EnumPairedDevice(U8 *bdaddr)
{
    return Bt__GetPairedDevice(bdaddr, 0);
}

U8 Bt__GetPairedDevice(U8 *bdaddr, U8 idx)
{
    U16 adr;
    U8 cnt;
    U8 tmpff[6];

    adr = SM_SIZE;
    memset(tmpff, 0xff, 6);
    memset(bdaddr, 0xff, 6);
    cnt = 0;

    do
    {
        adr -= 32;
        StorageGetBytes(adr, 6, bdaddr);
        if(memcmp(tmpff, bdaddr, 6))//notff
        {
            if(cnt == idx)
                return 1;
            else
                cnt++;
        }
        
    }
    while(adr != 0);

    return 0;    
}

#endif

void Bt__StoreLinkKey(U8 *bdaddr, U8 *link_key, U8 type)
{
   
    U16 addr;
    U8 tmpff[6];
    
    memset(tmpff, 0xff, 6);

    if(Bt__SecuritySearchDevice(tmpff, &addr) != 3) //not full
    {
         ASSERT(addr < SM_SIZE);                
    }
    else
    {
        U8 tmp[32];
        StorageGetBytes(SM_SIZE, 32, tmp);            
        StorageClear();
        addr = 0;
        StorageSetBytes(SM_SIZE, 32, tmp);//recover BT_LOCK's bt address
    }

    StorageSetBytes(addr, 6, bdaddr);
    StorageSetBytes(addr + 6, 16, link_key);
    tmpff[0] = type;
    StorageSetBytes(addr + 6 + 16, 2, tmpff);

}

U8 Bt__RetrieveLinkKey(U8 *bdaddr, U8 *link_key, U8 *type)
{
    U16 addr;
    U8 tmp[2];

    if(Bt__SecuritySearchDevice(bdaddr, &addr) == 1)//exist
    {
        StorageGetBytes(addr + 6, 16, link_key);
        StorageGetBytes(addr + 6 + 16, 2, tmp);
        *type = tmp[0];
        return 0;
    }

    return 1;
}

static U8 Request_BD_ADDR[6];

void Bt__UserConformReply()
{
    SendCmd(HCCI_USER_CONFIRM_REQ_REPLY, Request_BD_ADDR, 0);
}

void Bt__SecurityEvtCB(U8 EvtOp, U8 *dat)
{
    switch(EvtOp)
    {
        case HCE_IOCAP_REQUEST:
        {
            /*
            dat[6] = 0x18;
            SendCmd(HCCI_IOCAP_REQ_NEG_REPLY, dat, 0);             
            */
            dat[6] = 0x03; //no input no output
           // dat[6] = 0x01; //display yes no
            dat[7] = 0x00; //oob from remote

//            dat[8] = 0x02; //no MITM dedicated
            //dat[8] = 0x03; //MITM dedicated

//            dat[8] = 0x04; //no MITM generic
            //dat[8] = 0x05; //MITM generic

            dat[8] = 0x00; //no MITM no-bonding
            //dat[8] = 0x01; //MITM no-bonding

            SendCmd(HCCI_IOCAP_REQ_REPLY, dat, 0); 

            break;
        }
        case HCE_USER_COMFIRM_REQ:
        {
            /*
            SendCmd(HCCI_USER_CONFIRM_REQ_NEG_REPLY, dat, 0);
            */
            memcpy(Request_BD_ADDR, dat, 6);
            Bt__UserConformReply();

            break;
        }
        case HCE_REMOTE_OOB_DATA_REQ:
        {
            //dat[6] ~dat[21]; //C from remote OOB transfer
            //dat[22] ~dat[37]; //R from remote OOB transfer
            SendCmd(HCCI_REM_OOB_DATA_REQ_REPLY, dat, 0);
            break;
        }
        case HCE_LINK_KEY_NOTIFY:
        {
            Bt__StoreLinkKey(dat, dat+6, dat[22]);
            break;
        }
        case HCE_LINK_KEY_REQ:
        {
            if(!Bt__RetrieveLinkKey(dat, dat + 6, dat + 22))                 
                SendCmd(HCCI_LINK_KEY_REQ_REPL, dat, 0);
            else
                SendCmd(HCCI_LINK_KEY_REQ_NEG_REPL, dat, 0);
            break;
        }
    }
}



