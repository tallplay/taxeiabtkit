#include "BTCmdEvt_loader.h"
#include "../../CORE/BTCore.h"
#include "../../TaxeiaConsoleDebug/TaxeiaConsoledebug.h"
#include "../../CORE/DataQueue.h"
#include <string.h>
#include "../../TaxeiaHCILog/TaxeiaHCILog.h"

//command arguments
#define CMD_PARM_MAX 38
//main arguments
static CMDIDX Cmdidx;
static CompleteDataHandler CmdHandler = 0;
//saved arguments, used to store the command which can't be executed immediately, it will be executed later
static CMDIDX CmdidxSave = HCCI_TAXEIA_END;
static CompleteDataHandler CmdHandlerSave = 0;

static U8 CmdParm[CMD_PARM_MAX+3];
static U8 CmdParmSave[CMD_PARM_MAX+3];

U8 EvtBuffer[66];
U8 RxDataBuffer[ACL_MAX_PACKET_LEN];
U8 RxDataHeader[4];

static U8 TxDataPacket[ACL_MAX_PACKET_LEN];
static U16 TxDataLen;
static EvtHandler evtHandler = 0;

const HCICommandFormat hci_cmds[HCCI_TAXEIA_END+1]=
{
    {HCC_INQUIRY                      ,0x05, HCE_COMMAND_STATUS, CmdParm/*, HCE_INQUIRY_RESULT, HCE_INQUIRY_COMPLETE*/}
   ,{HCC_DISCONNECT                   ,0x03, HCE_DISCONNECT_COMPLETE, CmdParm/*, 0xff, HCE_DISCONNECT_COMPLETE*/}
   ,{HCC_ACCEPT_CON_REQ               ,0x07, HCE_COMMAND_STATUS/*HCE_CONNECT_COMPLETE*/, CmdParm}

/* Group: Link policy commands */          
    
/* Group: Host controller and baseband commands */
   ,{HCC_RESET                        ,0x00, HCE_COMMAND_COMPLETE, 0}
   ,{HCC_HOST_BUFFER_SIZE             ,0x07, HCE_COMMAND_COMPLETE, CmdParm}
   ,{HCC_READ_BUFFER_SIZE             ,0x00, HCE_COMMAND_COMPLETE, 0}
   
/* Group: Informational parameters */
   ,{HCC_READ_BD_ADDR                 ,0x00, HCE_COMMAND_COMPLETE, 0}

/* Group: Status parameters */

   
/* Group: Testing commands */

/* STLC2500C vendor commands */
/* mtk vendor commands */
   ,{HCC_MTK6622_SET_BDADDR           ,6, HCE_COMMAND_COMPLETE, CmdParm}
   ,{HCC_MTK6622_SET_RADIO            ,6, HCE_COMMAND_COMPLETE, CmdParm}
   ,{HCC_MTK6622_SET_BAUD             ,6, HCE_COMMAND_COMPLETE, CmdParm}
   ,{0, 0, 0, 0}
};


///////////////////////////////////Public API///////////////////////////////////////////
extern void Bt__EvtCB(U8 EvtOp, U8 *dat);

BOOL SendCmd(CMDIDX idx, U8 *dat, CompleteDataHandler cdh)
{
    return SendCmdWithLen(idx, dat, hci_cmds[idx].parmlen, cdh);
}

BOOL SendCmdWithLen(CMDIDX idx, U8 *dat, U8 len, CompleteDataHandler cdh)
{
    BOOL sts;
    sts = CmdRequest();
    
    if(sts == TRUE)
    {
        CmdHandler = cdh;
        
        if(dat) memcpy(CmdParm+3, dat, len);
        
        Cmdidx = idx;
    }
    else
    {
        if(CmdidxSave == HCCI_TAXEIA_END) //no one save
        {
            Taxeia_Report("Save Cmd %x\r\n", hci_cmds[idx].op);
            CmdHandlerSave = cdh;
            if(dat) memcpy(CmdParmSave+3, dat, len > CMD_PARM_MAX ? CMD_PARM_MAX : len);            
            CmdidxSave = idx;
            sts = TRUE;
        }
        else
        {
            Taxeia_Report("Save method full!!!!!\r\n");
        }
    }
    return sts;
}

void CheckSavedCmd(void)
{    
    if(CmdidxSave != HCCI_TAXEIA_END)
    {
        if(CmdRequest() == TRUE)
        {
            Taxeia_Report("ReSend Cmd %x!!!!!\r\n", hci_cmds[CmdidxSave].op);
            CmdHandler = CmdHandlerSave;
            Cmdidx = CmdidxSave;
            if(hci_cmds[Cmdidx].parmlen) memcpy(CmdParm+3, CmdParmSave+3, hci_cmds[Cmdidx].parmlen > CMD_PARM_MAX ? CMD_PARM_MAX : hci_cmds[Cmdidx].parmlen);            

            CmdidxSave = HCCI_TAXEIA_END;
        }
    }
}

U8 GetTxDataBuffer(U16 idx)
{
    return TxDataPacket[idx];
}

void FillTxDataBuffer(U16 idx, U8 dat)
{
    TxDataPacket[idx] = dat;
}

void MoveTxDataBuffer(U16 tgtidx, U16 srcidx)
{
    TxDataPacket[tgtidx] = TxDataPacket[srcidx];
}

//only allow l2cap use, because we do acl num control
BOOL SendData(U16 len)
{
    BOOL sts;
    sts = DataRequest();
    
    if(sts == TRUE)
    {
        TxDataLen = len;
    }
    return sts;
}


///////////////////////////////////Private Functions/////////////////////////////////////
U16 DataSendCallback(U8 **ptr)
{
    *ptr = TxDataPacket;
    return TxDataLen;
}

//return length
U16 CmdSendCallback(U8 **ptr, U16 *zero_padding_len)
{    
    U16 total_len;

    total_len = hci_cmds[Cmdidx].parmlen + 3;

    if(total_len > CMD_PARM_MAX + 3)
    {
        *zero_padding_len = total_len - (CMD_PARM_MAX + 3);
        total_len = (CMD_PARM_MAX + 3);
    }
    else
        *zero_padding_len = 0;

    if(hci_cmds[Cmdidx].parm)
    {
        *ptr = hci_cmds[Cmdidx].parm;
    }
    else
    {
        *ptr = CmdParm;
    }

    (*ptr)[0] =  hci_cmds[Cmdidx].op & 0xff;
    (*ptr)[1] = (hci_cmds[Cmdidx].op >> 8) & 0xff;
    (*ptr)[2] = hci_cmds[Cmdidx].parmlen;
    return total_len;
    
}

void EvtCallBack(U8 *evtbuf)
{
    U8 EvtOp;
    U16 CmdOp;

    EvtOp = evtbuf[0];
    
    if(EvtOp == HCE_COMMAND_COMPLETE)
    {
        CmdOp = (evtbuf[3] | (evtbuf[4] << 8));
    }

    if(EvtOp == HCE_COMMAND_STATUS && evtbuf[4] == 0 && evtbuf[5] == 0)
    {//nop 
        return;
    }
            
    if(hci_cmds[Cmdidx].event_end == EvtOp)
    {
        //ending event handler
        if(CmdHandler)
        {
            if(EvtOp == HCE_COMMAND_COMPLETE)
                CmdHandler(Cmdidx, EvtOp, evtbuf + 5);
            else
                CmdHandler(Cmdidx, EvtOp, evtbuf + 2);
        }
        
        Cmdidx = HCCI_TAXEIA_END;
        CmdResponsed();                    
    }
    else if(EvtOp == HCE_COMMAND_STATUS) //we need to consider if command can't execute
    {
        if((evtbuf[2] != HC_STATUS_SUCCESS)) //not success, end the command 
        {
            Cmdidx = HCCI_TAXEIA_END;
            CmdResponsed();
        }

        //mid event handler
        if(CmdHandler)
            CmdHandler(Cmdidx, HCE_COMMAND_STATUS, evtbuf + 2);                    
    }

    //global event handler
        Bt__EvtCB(EvtOp, evtbuf + 2);                       
}

extern void Bt__DataHandlerCB(U16 handle, U16 len, U8 *dat);

void RxDataCallBack(U8 *dat)
{
    U16 residue = 0;
    U16 handle = 0;
    U8 PB_bit = 0;
    
    handle = dat[0] | ((dat[1] & 0xf) << 8);
    PB_bit = (dat[1] >> 4) & 0x3;
    residue = dat[2] | (dat[3] << 8);

    Bt__DataHandlerCB(handle, residue, dat + 4);        
}

