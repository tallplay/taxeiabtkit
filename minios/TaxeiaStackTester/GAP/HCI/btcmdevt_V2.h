#ifndef __BTCMDEVT_H_
#define __BTCMDEVT_H_
#include "BTType.h"
#include "BtConfig.h"
typedef U8 (*CompleteDataHandler)(U16 CmdOp, U8 EvtOp, U8 *dat);
typedef void (*EvtHandler)(U8 EvtOp, U8 *dat);
typedef void (*AclRxDataHandler)(U16 handle, U16 len, U8 *dat);
    
void FillTxDataBuffer(U16 idx, U8 dat);
BOOL SendCmd(U16 cmdop, U8 *dat, CompleteDataHandler handler, U8 len);
BOOL SendCmdWithLen(U16 cmdop, U8 *dat, U8 len, CompleteDataHandler cdh);
void EvtCmdCompleteCallBack(U8 idx, U8 dat);
void EvtCmdStatusCallBack(U8 idx, U8 dat);
void BtCmdEvtInit();

void Evt_CallBack(U8 *evtbuf);
void Rx_DataCallBack(U8 *datbuf);

void RegisterEvtHandler(EvtHandler evh);
void RegisterAclDataHandler(AclRxDataHandler adh);

//temporarily here


/* Group: Link control commands */
#define HCC_INQUIRY                      0x0401
#define HCC_INQUIRY_CANCEL               0x0402
#define HCC_START_PERIODIC_INQ_MODE      0x0403
#define HCC_EXIT_PERIODIC_INQ_MODE       0x0404
#define HCC_CREATE_CONNECTION            0x0405
#define HCC_DISCONNECT                   0x0406
#define HCC_ADD_SCO_CONNECTION           0x0407 /* Not in 1.2 */
#define HCC_CREATE_CONNECTION_CANCEL     0x0408 /* 1.2 */
#define HCC_ACCEPT_CON_REQ               0x0409
#define HCC_REJECT_CON_REQ               0x040A
#define HCC_LINK_KEY_REQ_REPL            0x040B
#define HCC_LINK_KEY_REQ_NEG_REPL        0x040C
#define HCC_PIN_CODE_REQ_REPL            0x040D
#define HCC_PIN_CODE_REQ_NEG_REPL        0x040E
#define HCC_CHNG_CONN_PACKET_TYPE        0x040F
#define HCC_AUTH_REQ                     0x0411
#define HCC_SET_CONN_ENCRYPT             0x0413
#define HCC_CHNG_CONN_LINK_KEY           0x0415
#define HCC_MASTER_LINK_KEY              0x0417
#define HCC_REM_NAME_REQ                 0x0419
#define HCC_REM_NAME_REQ_CANCEL          0x041A /* 1.2 */
#define HCC_READ_REMOTE_FEATURES         0x041B
#define HCC_READ_REMOTE_EXT_FEATURES     0x041C /* 1.2 */
#define HCC_READ_REMOTE_VERSION          0x041D
#define HCC_READ_CLOCK_OFFSET            0x041F
#define HCC_READ_LMP_HANDLE              0x0420 // 1.2
#define HCC_EXCHANGE_FIXED_INFO          0x0421 
#define HCC_EXCHANGE_ALIAS_INFO          0x0422 
#define HCC_PRIVATE_PAIRING_REQ_REPL     0x0423 
#define HCC_PRIVATE_PAIRING_REQ_NEG_REPL 0x0424 
#define HCC_GENERATED_ALIAS              0x0425 
#define HCC_ALIAS_ADDRESS_REQ_REPL       0x0426 
#define HCC_ALIAS_ADDRESS_REQ_NEG_REPL   0x0427 
#define HCC_SETUP_SYNC_CONNECTION        0x0428 
#define HCC_ACCEPT_SYNC_CON_REQ          0x0429 
#define HCC_REJECT_SYNC_CON_REQ          0x042A 
#define HCC_IOCAP_REQ_REPLY              0x042B
#define HCC_USER_CONFIRM_REQ_REPLY       0x042C
#define HCC_USER_CONFIRM_REQ_NEG_REPLY   0x042D
#define HCC_USER_PASSKEY_REQ_REPLY       0x042E
#define HCC_USER_PASSKEY_REQ_NEG_REPLY   0x042F
#define HCC_REM_OOB_DATA_REQ_REPLY       0x0430
#define HCC_REM_OOB_DATA_REQ_NEG_REPLY   0x0433
#define HCC_IOCAP_REQ_NEG_REPLY          0x0434
                                       
/* Group: Link policy commands */             
#define HCC_HOLD_MODE                    0x0801
#define HCC_SNIFF_MODE                   0x0803
#define HCC_EXIT_SNIFF_MODE              0x0804
#define HCC_PARK_MODE                    0x0805
#define HCC_EXIT_PARK_MODE               0x0806
#define HCC_QOS_SETUP                    0x0807
#define HCC_ROLE_DISCOVERY               0x0809
#define HCC_SWITCH_ROLE                  0x080B
#define HCC_READ_LINK_POLICY             0x080C
#define HCC_WRITE_LINK_POLICY            0x080D
#define HCC_READ_DEFAULT_LINK_POLICY     0x080E /* 1.2 */
#define HCC_WRITE_DEFAULT_LINK_POLICY    0x080F /* 1.2 */
#define HCC_FLOW_SPECIFICATION           0x0810 /* 1.2 */
#define HCC_SNIFF_SUBRATING              0x0811 /* 2.1 */

/* Group: Host controller and baseband commands */
#define HCC_SET_EVENT_MASK               0x0C01
#define HCC_RESET                        0x0C03
#define HCC_EVENT_FILTER                 0x0C05
#define HCC_FLUSH                        0x0C08
#define HCC_READ_PIN_TYPE                0x0C09
#define HCC_WRITE_PIN_TYPE               0x0C0A
#define HCC_CREATE_NEW_UNIT_KEY          0x0C0B
#define HCC_READ_STORED_LINK_KEY         0x0C0D
#define HCC_WRITE_STORED_LINK_KEY        0x0C11
#define HCC_DEL_STORED_LINK_KEY          0x0C12
#define HCC_CHNG_LOCAL_NAME              0x0C13
#define HCC_READ_LOCAL_NAME              0x0C14
#define HCC_READ_CONN_ACCEPT_TIMEOUT     0x0C15
#define HCC_WRITE_CONN_ACCEPT_TIMEOUT    0x0C16
#define HCC_READ_PAGE_TIMEOUT            0x0C17
#define HCC_WRITE_PAGE_TIMEOUT           0x0C18
#define HCC_READ_SCAN_ENABLE             0x0C19
#define HCC_WRITE_SCAN_ENABLE            0x0C1A
#define HCC_READ_PAGE_SCAN_ACTIVITY      0x0C1B
#define HCC_WRITE_PAGE_SCAN_ACTIVITY     0x0C1C
#define HCC_READ_INQ_SCAN_ACTIVITY       0x0C1D
#define HCC_WRITE_INQ_SCAN_ACTIVITY      0x0C1E
#define HCC_READ_AUTH_ENABLE             0x0C1F
#define HCC_WRITE_AUTH_ENABLE            0x0C20
#define HCC_READ_ENCRYPT_MODE            0x0C21
#define HCC_WRITE_ENCRYPT_MODE           0x0C22
#define HCC_READ_CLASS_OF_DEVICE         0x0C23
#define HCC_WRITE_CLASS_OF_DEVICE        0x0C24
#define HCC_READ_VOICE_SETTING           0x0C25
#define HCC_WRITE_VOICE_SETTING          0x0C26
#define HCC_READ_AUTO_FLUSH_TIMEOUT      0x0C27
#define HCC_WRITE_AUTO_FLUSH_TIMEOUT     0x0C28
#define HCC_READ_NUM_BC_RETRANSMIT       0x0C29
#define HCC_WRITE_NUM_BC_RETRANSMIT      0x0C2A
#define HCC_READ_HOLD_MODE_ACTIVITY      0x0C2B
#define HCC_WRITE_HOLD_MODE_ACTIVITY     0x0C2C
#define HCC_READ_XMIT_POWER_LEVEL        0x0C2D
#define HCC_READ_SCO_FC_ENABLE           0x0C2E
#define HCC_WRITE_SCO_FC_ENABLE          0x0C2F
#define HCC_SET_CTRLR_TO_HOST_FLOW_CTRL  0x0C31
#define HCC_HOST_BUFFER_SIZE             0x0C33
#define HCC_HOST_NUM_COMPLETED_PACKETS   0x0C35
#define HCC_READ_LINK_SUPERV_TIMEOUT     0x0C36
#define HCC_WRITE_LINK_SUPERV_TIMEOUT    0x0C37
#define HCC_READ_NUM_IAC                 0x0C38
#define HCC_READ_CURRENT_IAC_LAP         0x0C39
#define HCC_WRITE_CURRENT_IAC_LAP        0x0C3A
#define HCC_READ_PAGE_SCAN_PERIOD_MODE   0x0C3B
#define HCC_WRITE_PAGE_SCAN_PERIOD_MODE  0x0C3C
#define HCC_READ_PAGE_SCAN_MODE          0x0C3D /* Not in 1.2 */
#define HCC_WRITE_PAGE_SCAN_MODE         0x0C3E /* Not in 1.2 */
#define HCC_SET_AFH_HOST_CHAN_CLASS      0x0C3F /* 1.2 */
#define HCC_READ_INQ_SCAN_TYPE           0x0C42 /* 1.2 */
#define HCC_WRITE_INQ_SCAN_TYPE          0x0C43 /* 1.2 */
#define HCC_READ_INQ_MODE                0x0C44 /* 1.2 */
#define HCC_WRITE_INQ_MODE               0x0C45 /* 1.2 */
#define HCC_READ_PAGE_SCAN_TYPE          0x0C46 /* 1.2 */
#define HCC_WRITE_PAGE_SCAN_TYPE         0x0C47 /* 1.2 */
#define HCC_READ_AFH_CHAN_ASSESS_MODE    0x0C48 /* 1.2 */
#define HCC_WRITE_AFH_CHAN_ASSESS_MODE   0x0C49 /* 1.2 */
#define HCC_WRITE_EXTENDED_INQURIY_RSP   0x0C52
#define HCC_READ_SIMPLE_PAIRING_MODE     0x0C55 /* 2.1 */
#define HCC_WRITE_SIMPLE_PAIRING_MODE    0x0C56 /* 2.1 */
#define HCC_READ_LOCAL_OOB_DATA          0x0C57

/* Group: Informational parameters */
#define HCC_READ_LOCAL_VERSION           0x1001
#define HCC_READ_LOCAL_COMMANDS          0x1002 /* 1.2 */
#define HCC_READ_LOCAL_FEATURES          0x1003
#define HCC_READ_LOCAL_EXT_FEATURES      0x1004 /* 1.2 */
#define HCC_READ_BUFFER_SIZE             0x1005
#define HCC_READ_COUNTRY_CODE            0x1007 /* Not in 1.2 */
#define HCC_READ_BD_ADDR                 0x1009
#define HCC_READ_DATA_BLOCK_SIZE         0x100A /* 3.0 */

/* Group: Status parameters */
#define HCC_READ_FAILED_CONTACT_COUNT    0x1401
#define HCC_RESET_FAILED_CONTACT_COUNT   0x1402
#define HCC_GET_LINK_QUALITY             0x1403
#define HCC_READ_RSSI                    0x1405
#define HCC_READ_AFH_CHANNEL_MAP         0x1406 /* 1.2 */
#define HCC_READ_CLOCK                   0x1407 /* 1.2 */

/* Group: Testing commands */
#define HCC_READ_LOOPBACK_MODE           0x1801
#define HCC_WRITE_LOOPBACK_MODE          0x1802
#define HCC_ENABLE_DUT                   0x1803

/* STLC2500C vendor commands */
#define HCC_ST_STATIC_TXRX               0xFCF8
#define HCC_ST_TXRX_TEST                 0xFCF4
#define HCC_ERI_ADDRESS                  0xFC22

/* mtk vendor commands */
#define HCC_MTK6622_SET_BDADDR           0xFC1A
#define HCC_MTK6622_SET_RADIO            0xFC79
#define HCC_MTK6622_SET_SLEEP            0xFC7A
#define HCC_MTK6622_WAKEUP               0xFCC0
#define HCC_MTK6622_HOST_WAKEUP          0xFCC1
#define HCC_MTK6622_SET_BAUD             0xFC77
#define HCC_MTK6622_SET_PCM              0xFC72
#define HCC_MTK6622_TEST_CONTROL         0xFC0D
#define HCC_MTK6622_FW_PATCH             0xFCC4
#define HCC_MTK6622_FW_PATCH_MODE        0xFCCC
#define HCC_MTK6622_SET_XO_TRIM          0xFC7F
/* st vendor commands*/
#define HCC_ST_SET_BDADDR                0xFC06

/* End of HciCommandType */

#define HCE_INQUIRY_COMPLETE               0x01
#define HCE_INQUIRY_RESULT                 0x02
#define HCE_CONNECT_COMPLETE               0x03
#define HCE_CONNECT_REQUEST                0x04
#define HCE_DISCONNECT_COMPLETE            0x05
#define HCE_AUTH_COMPLETE                  0x06
#define HCE_REMOTE_NAME_REQ_COMPLETE       0x07
#define HCE_ENCRYPT_CHNG                   0x08
#define HCE_CHNG_CONN_LINK_KEY_COMPLETE    0x09
#define HCE_MASTER_LINK_KEY_COMPLETE       0x0A
#define HCE_READ_REMOTE_FEATURES_COMPLETE  0x0B
#define HCE_READ_REMOTE_VERSION_COMPLETE   0x0C
#define HCE_QOS_SETUP_COMPLETE             0x0D
#define HCE_COMMAND_COMPLETE               0x0E
#define HCE_COMMAND_STATUS                 0x0F
#define HCE_HARDWARE_ERROR                 0x10
#define HCE_FLUSH_OCCURRED                 0x11
#define HCE_ROLE_CHANGE                    0x12
#define HCE_NUM_COMPLETED_PACKETS          0x13
#define HCE_MODE_CHNG                      0x14
#define HCE_RETURN_LINK_KEYS               0x15
#define HCE_PIN_CODE_REQ                   0x16
#define HCE_LINK_KEY_REQ                   0x17
#define HCE_LINK_KEY_NOTIFY                0x18
#define HCE_LOOPBACK_COMMAND               0x19
#define HCE_DATA_BUFFER_OVERFLOW           0x1A
#define HCE_MAX_SLOTS_CHNG                 0x1B
#define HCE_READ_CLOCK_OFFSET_COMPLETE     0x1C
#define HCE_CONN_PACKET_TYPE_CHNG          0x1D
#define HCE_QOS_VIOLATION                  0x1E
#define HCE_PAGE_SCAN_MODE_CHANGE          0x1F /* Not in 1.2 */
#define HCE_PAGE_SCAN_REPETITION_MODE      0x20
#define HCE_FLOW_SPECIFICATION_COMPLETE    0x21
#define HCE_INQUIRY_RESULT_WITH_RSSI       0x22
#define HCE_READ_REMOTE_EXT_FEAT_COMPLETE  0x23
#define HCE_FIXED_ADDRESS                  0x24
#define HCE_ALIAS_ADDRESS                  0x25
#define HCE_GENERATE_ALIAS_REQ             0x26
#define HCE_ACTIVE_ADDRESS                 0x27
#define HCE_ALLOW_PRIVATE_PAIRING          0x28
#define HCE_ALIAS_ADDRESS_REQ              0x29
#define HCE_ALIAS_NOT_RECOGNIZED           0x2A
#define HCE_FIXED_ADDRESS_ATTEMPT          0x2B
#define HCE_SYNC_CONNECT_COMPLETE          0x2C
#define HCE_SYNC_CONN_CHANGED              0x2D
#define HCE_IOCAP_REQUEST                  0x31
#define HCE_IOCAP_RESPONSE                 0x32
#define HCE_USER_COMFIRM_REQ               0x33
#define HCE_USER_PASSKEY_REQ               0x34
#define HCE_REMOTE_OOB_DATA_REQ            0x35
#define HCE_SSP_COMPLETE                   0x36
#define HCE_LINK_SUPER_TOUT_CHG            0x38
#define HCE_BLUETOOTH_LOGO                 0xFE
#define HCE_VENDOR_SPECIFIC                0xFF

#define HC_STATUS_SUCCESS                 0x00
#define HC_STATUS_UNKNOWN_HCI_CMD         0x01
#define HC_STATUS_NO_CONNECTION           0x02
#define HC_STATUS_HARDWARE_FAILURE        0x03
#define HC_STATUS_PAGE_TIMEOUT            0x04
#define HC_STATUS_AUTH_FAILURE            0x05
#define HC_STATUS_KEY_MISSING             0x06
#define HC_STATUS_MEMORY_FULL             0x07
#define HC_STATUS_CONN_TIMEOUT            0x08
#define HC_STATUS_MAX_NUM_CONNS           0x09
#define HC_STATUS_MAX_SCO_CONNS           0x0A
#define HC_STATUS_ACL_ALREADY_EXISTS      0x0B
#define HC_STATUS_CMD_DISALLOWED          0x0C
#define HC_STATUS_HOST_REJ_NO_RESOURCES   0x0D
#define HC_STATUS_HOST_REJ_SECURITY       0x0E
#define HC_STATUS_HOST_REJ_PERSONAL_DEV   0x0F
#define HC_STATUS_HOST_TIMEOUT            0x10
#define HC_STATUS_UNSUPP_FEATUR_PARM_VAL  0x11
#define HC_STATUS_INVAL_HCI_PARM_VAL      0x12
#define HC_STATUS_CONN_TERM_USER_REQ      0x13
#define HC_STATUS_CONN_TERM_LOW_RESOURCES 0x14
#define HC_STATUS_CONN_TERM_POWER_OFF     0x15
#define HC_STATUS_CONN_TERM_LOCAL_HOST    0x16
#define HC_STATUS_REPEATED_ATTEMPTS       0x17
#define HC_STATUS_PAIRING_DISALLOWED      0x18
#define HC_STATUS_UNKNOWN_LMP_PDU         0x19
#define HC_STATUS_UNSUPP_REMOTE_FEATURE   0x1A
#define HC_STATUS_SCO_OFFSET_REJECTED     0x1B
#define HC_STATUS_SCO_INTERVAL_REJECTED   0x1C
#define HC_STATUS_SCO_AIR_MODE_REJECTED   0x1D
#define HC_STATUS_INVALID_LMP_PARM        0x1E
#define HC_STATUS_UNSPECIFIED_ERROR       0x1F
#define HC_STATUS_UNSUPP_LMP_PARM         0x20
#define HC_STATUS_ROLE_CHANGE_DISALLOWED  0x21
#define HC_STATUS_LMP_RESPONSE_TIMEDOUT   0x22
#define HC_STATUS_LMP_ERR_TRANSACT_COLL   0x23
#define HC_STATUS_LMP_PDU_DISALLOWED      0x24
#define HC_STATUS_ENCRYPTN_MODE_UNACCEPT  0x25
#define HC_STATUS_UNIT_KEY_USED           0x26
#define HC_STATUS_QOS_NOT_SUPPORTED       0x27
#define HC_STATUS_INSTANT_PASSED          0x28
#define HC_STATUS_PAIRING_W_UNIT_KEY_UNSUPP         0x29
#define HC_STATUS_DIFFERENT_TRANSACTION_COLLISION   0x2A
#define HC_STATUS_INSUFF_RESOURCES_FOR_SCATTER_MODE 0x2B
#define HC_STATUS_QOS_UNACCEPTABLE_PARAMETER        0x2C
#define HC_STATUS_QOS_REJECTED                      0x2D
#define HC_STATUS_CHANNEL_CLASSIF_NOT_SUPPORTED     0x2E
#define HC_STATUS_INSUFFICIENT_SECURITY             0x2F
#define HC_STATUS_PARAMETER_OUT_OF_MANDATORY_RANGE  0x30
#define HC_STATUS_SCATTER_MODE_NO_LONGER_REQUIRED   0x31
#define HC_STATUS_ROLE_SWITCH_PENDING               0x32
#define HC_STATUS_SCATTER_MODE_PARM_CHNG_PENDING    0x33
#define HC_STATUS_RESERVED_SLOT_VIOLATION           0x34
#define HC_STATUS_SWITCH_FAILED                     0x35

#define LLC_COMMAND_REJ     0x01
#define LLC_CONN_REQ        0x02
#define LLC_CONN_RSP        0x03
#define LLC_CONFIG_REQ      0x04
#define LLC_CONFIG_RSP      0x05
#define LLC_DISC_REQ        0x06
#define LLC_DISC_RSP        0x07
#define LLC_ECHO_REQ        0x08
#define LLC_ECHO_RSP        0x09
#define LLC_INFO_REQ        0x0A
#define LLC_INFO_RSP        0x0B
#define LLC_TEST_REQ        0x18

//internall used
#define LLC_OPEN_CHANNEL    0x82
#define LLC_CONFIG_CHANNEL  0x83

/* The connection request is accepted. */
#define L2CONN_ACCEPTED                     0x0000

/* The connection request is pending. This might be necessary if some
 * user action is required to approve the connection.
 *
 * After the pending indication is delivered to the remote device, the local
 * protocol will receive an L2EVENT_COMPLETE event. After this event occurs
 * the protocol may re-issue the L2CAP_ConnectRsp call with one of the
 * other connection status values.
 */
#define L2CONN_PENDING                      0x0001

/* Rejects the connection request because no such protocol service exists.
 * Generally, this rejection status should only be originated by L2CAP,
 * not by protocol services.
 *
 * The L2EVENT_DISCONNECTED event will be generated after the rejection
 * has been sent.
 */
#define L2CONN_REJECT_PSM_NOT_SUPPORTED     0x0002

/* Rejects the connection request due to a security failure. The
 * L2EVENT_DISCONNECTED event will be generated after the rejection has
 * been sent.
 */
#define L2CONN_REJECT_SECURITY_BLOCK        0x0003

/* Rejects the connection request due to a resource shortage. The
 * L2EVENT_DISCONNECTED event will be generated after the rejection has
 * been sent.
 */
#define L2CONN_REJECT_NO_RESOURCES          0x0004

/* End of L2capConnStatus */

#endif
