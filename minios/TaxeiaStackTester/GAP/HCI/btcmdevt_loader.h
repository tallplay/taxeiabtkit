#ifndef __BTCMDEVT_H_
#define __BTCMDEVT_H_
#include "BTType.h"
#include "BtConfig.h"
typedef void (*CompleteDataHandler)(U8 CmdOp, U8 EvtOp, U8 *dat);
typedef void (*EvtHandler)(U8 EvtOp, U8 *dat);
typedef void (*AclRxDataHandler)(U16 handle, U16 len, U8 *dat);
    
typedef struct {
   U16 op;
   U8 parmlen;
   U8 event_end; //event which indicate command was stop execute. 0xFF means no end event
   U8 *parm;
}HCICommandFormat;

typedef enum
{
    HCCI_INQUIRY = 0,
    HCCI_DISCONNECT,
    HCCI_ACCEPT_CON_REQ,
    HCCI_RESET,

    HCCI_HOST_BUFFER_SIZE,
    HCCI_READ_BUFFER_SIZE,
    HCCI_READ_BD_ADDR,
    HCCI_MTK6622_SET_BDADDR,
    HCCI_MTK6622_SET_RADIO,
    HCCI_MTK6622_SET_BAUD,
    HCCI_TAXEIA_END,
}CMDIDX;

U8 GetTxDataBuffer(U16 idx);
void FillTxDataBuffer(U16 idx, U8 dat);
void MoveTxDataBuffer(U16, U16);
BOOL SendCmd(CMDIDX idx, U8 *dat, CompleteDataHandler handler);
BOOL SendCmdWithLen(CMDIDX idx, U8 *dat, U8 len, CompleteDataHandler cdh);
void EvtCmdCompleteCallBack(U8 idx, U8 dat);
void EvtCmdStatusCallBack(U8 idx, U8 dat);
#ifdef NEW_BT_LOOP2
U16 CmdSendCallback(U8 **ptr, U16 *zero_padding_len);
U16 DataSendCallback(U8 **ptr);
#else
BOOL CmdSendCallback(void);
BOOL DataSendCallback(void);
#endif

#ifdef NEW_BT_LOOP
void EvtCallBack(U8 *evtbuf);
void RxDataCallBack(U8 *datbuf);
#else
BOOL EvtCallBack(void);
BOOL RxDataCallBack(void);
#endif

void RegisterEvtHandler(EvtHandler evh);
void RegisterAclDataHandler(AclRxDataHandler adh);

//temporarily here


/* Group: Link control commands */
#define HCC_INQUIRY                      0x0401
#define HCC_DISCONNECT                   0x0406
#define HCC_ACCEPT_CON_REQ               0x0409                                       
/* Group: Link policy commands */             

/* Group: Host controller and baseband commands */
#define HCC_RESET                        0x0C03
#define HCC_HOST_BUFFER_SIZE             0x0C33

/* Group: Informational parameters */
#define HCC_READ_BUFFER_SIZE             0x1005
#define HCC_READ_BD_ADDR                 0x1009

/* Group: Status parameters */

/* Group: Testing commands */

/* STLC2500C vendor commands */

/* mtk vendor commands */
#define HCC_MTK6622_SET_BDADDR           0xFC1A
#define HCC_MTK6622_SET_RADIO            0xFC79
#define HCC_MTK6622_SET_BAUD             0xFC77

/* End of HciCommandType */

#define HCE_INQUIRY_COMPLETE               0x01
#define HCE_INQUIRY_RESULT                 0x02
#define HCE_CONNECT_COMPLETE               0x03
#define HCE_CONNECT_REQUEST                0x04
#define HCE_DISCONNECT_COMPLETE            0x05
#define HCE_AUTH_COMPLETE                  0x06
#define HCE_REMOTE_NAME_REQ_COMPLETE       0x07
#define HCE_ENCRYPT_CHNG                   0x08
#define HCE_CHNG_CONN_LINK_KEY_COMPLETE    0x09
#define HCE_MASTER_LINK_KEY_COMPLETE       0x0A
#define HCE_READ_REMOTE_FEATURES_COMPLETE  0x0B
#define HCE_READ_REMOTE_VERSION_COMPLETE   0x0C
#define HCE_QOS_SETUP_COMPLETE             0x0D
#define HCE_COMMAND_COMPLETE               0x0E
#define HCE_COMMAND_STATUS                 0x0F
#define HCE_HARDWARE_ERROR                 0x10
#define HCE_FLUSH_OCCURRED                 0x11
#define HCE_ROLE_CHANGE                    0x12
#define HCE_NUM_COMPLETED_PACKETS          0x13
#define HCE_MODE_CHNG                      0x14
#define HCE_RETURN_LINK_KEYS               0x15
#define HCE_PIN_CODE_REQ                   0x16
#define HCE_LINK_KEY_REQ                   0x17
#define HCE_LINK_KEY_NOTIFY                0x18
#define HCE_LOOPBACK_COMMAND               0x19
#define HCE_DATA_BUFFER_OVERFLOW           0x1A
#define HCE_MAX_SLOTS_CHNG                 0x1B
#define HCE_READ_CLOCK_OFFSET_COMPLETE     0x1C
#define HCE_CONN_PACKET_TYPE_CHNG          0x1D
#define HCE_QOS_VIOLATION                  0x1E
#define HCE_PAGE_SCAN_MODE_CHANGE          0x1F /* Not in 1.2 */
#define HCE_PAGE_SCAN_REPETITION_MODE      0x20
#define HCE_FLOW_SPECIFICATION_COMPLETE    0x21
#define HCE_INQUIRY_RESULT_WITH_RSSI       0x22
#define HCE_READ_REMOTE_EXT_FEAT_COMPLETE  0x23
#define HCE_FIXED_ADDRESS                  0x24
#define HCE_ALIAS_ADDRESS                  0x25
#define HCE_GENERATE_ALIAS_REQ             0x26
#define HCE_ACTIVE_ADDRESS                 0x27
#define HCE_ALLOW_PRIVATE_PAIRING          0x28
#define HCE_ALIAS_ADDRESS_REQ              0x29
#define HCE_ALIAS_NOT_RECOGNIZED           0x2A
#define HCE_FIXED_ADDRESS_ATTEMPT          0x2B
#define HCE_SYNC_CONNECT_COMPLETE          0x2C
#define HCE_SYNC_CONN_CHANGED              0x2D
#define HCE_IOCAP_REQUEST                  0x31
#define HCE_IOCAP_RESPONSE                 0x32
#define HCE_USER_COMFIRM_REQ               0x33
#define HCE_USER_PASSKEY_REQ               0x34
#define HCE_REMOTE_OOB_DATA_REQ            0x35
#define HCE_SSP_COMPLETE                   0x36
#define HCE_LINK_SUPER_TOUT_CHG            0x38
#define HCE_BLUETOOTH_LOGO                 0xFE
#define HCE_VENDOR_SPECIFIC                0xFF

#define HC_STATUS_SUCCESS                 0x00
#define HC_STATUS_UNKNOWN_HCI_CMD         0x01
#define HC_STATUS_NO_CONNECTION           0x02
#define HC_STATUS_HARDWARE_FAILURE        0x03
#define HC_STATUS_PAGE_TIMEOUT            0x04
#define HC_STATUS_AUTH_FAILURE            0x05
#define HC_STATUS_KEY_MISSING             0x06
#define HC_STATUS_MEMORY_FULL             0x07
#define HC_STATUS_CONN_TIMEOUT            0x08
#define HC_STATUS_MAX_NUM_CONNS           0x09
#define HC_STATUS_MAX_SCO_CONNS           0x0A
#define HC_STATUS_ACL_ALREADY_EXISTS      0x0B
#define HC_STATUS_CMD_DISALLOWED          0x0C
#define HC_STATUS_HOST_REJ_NO_RESOURCES   0x0D
#define HC_STATUS_HOST_REJ_SECURITY       0x0E
#define HC_STATUS_HOST_REJ_PERSONAL_DEV   0x0F
#define HC_STATUS_HOST_TIMEOUT            0x10
#define HC_STATUS_UNSUPP_FEATUR_PARM_VAL  0x11
#define HC_STATUS_INVAL_HCI_PARM_VAL      0x12
#define HC_STATUS_CONN_TERM_USER_REQ      0x13
#define HC_STATUS_CONN_TERM_LOW_RESOURCES 0x14
#define HC_STATUS_CONN_TERM_POWER_OFF     0x15
#define HC_STATUS_CONN_TERM_LOCAL_HOST    0x16
#define HC_STATUS_REPEATED_ATTEMPTS       0x17
#define HC_STATUS_PAIRING_DISALLOWED      0x18
#define HC_STATUS_UNKNOWN_LMP_PDU         0x19
#define HC_STATUS_UNSUPP_REMOTE_FEATURE   0x1A
#define HC_STATUS_SCO_OFFSET_REJECTED     0x1B
#define HC_STATUS_SCO_INTERVAL_REJECTED   0x1C
#define HC_STATUS_SCO_AIR_MODE_REJECTED   0x1D
#define HC_STATUS_INVALID_LMP_PARM        0x1E
#define HC_STATUS_UNSPECIFIED_ERROR       0x1F
#define HC_STATUS_UNSUPP_LMP_PARM         0x20
#define HC_STATUS_ROLE_CHANGE_DISALLOWED  0x21
#define HC_STATUS_LMP_RESPONSE_TIMEDOUT   0x22
#define HC_STATUS_LMP_ERR_TRANSACT_COLL   0x23
#define HC_STATUS_LMP_PDU_DISALLOWED      0x24
#define HC_STATUS_ENCRYPTN_MODE_UNACCEPT  0x25
#define HC_STATUS_UNIT_KEY_USED           0x26
#define HC_STATUS_QOS_NOT_SUPPORTED       0x27
#define HC_STATUS_INSTANT_PASSED          0x28
#define HC_STATUS_PAIRING_W_UNIT_KEY_UNSUPP         0x29
#define HC_STATUS_DIFFERENT_TRANSACTION_COLLISION   0x2A
#define HC_STATUS_INSUFF_RESOURCES_FOR_SCATTER_MODE 0x2B
#define HC_STATUS_QOS_UNACCEPTABLE_PARAMETER        0x2C
#define HC_STATUS_QOS_REJECTED                      0x2D
#define HC_STATUS_CHANNEL_CLASSIF_NOT_SUPPORTED     0x2E
#define HC_STATUS_INSUFFICIENT_SECURITY             0x2F
#define HC_STATUS_PARAMETER_OUT_OF_MANDATORY_RANGE  0x30
#define HC_STATUS_SCATTER_MODE_NO_LONGER_REQUIRED   0x31
#define HC_STATUS_ROLE_SWITCH_PENDING               0x32
#define HC_STATUS_SCATTER_MODE_PARM_CHNG_PENDING    0x33
#define HC_STATUS_RESERVED_SLOT_VIOLATION           0x34
#define HC_STATUS_SWITCH_FAILED                     0x35

#define LLC_COMMAND_REJ     0x01
#define LLC_CONN_REQ        0x02
#define LLC_CONN_RSP        0x03
#define LLC_CONFIG_REQ      0x04
#define LLC_CONFIG_RSP      0x05
#define LLC_DISC_REQ        0x06
#define LLC_DISC_RSP        0x07
#define LLC_ECHO_REQ        0x08
#define LLC_ECHO_RSP        0x09
#define LLC_INFO_REQ        0x0A
#define LLC_INFO_RSP        0x0B
#define LLC_TEST_REQ        0x18

//internall used
#define LLC_OPEN_CHANNEL    0x82
#define LLC_CONFIG_CHANNEL  0x83

/* The connection request is accepted. */
#define L2CONN_ACCEPTED                     0x0000

/* The connection request is pending. This might be necessary if some
 * user action is required to approve the connection.
 *
 * After the pending indication is delivered to the remote device, the local
 * protocol will receive an L2EVENT_COMPLETE event. After this event occurs
 * the protocol may re-issue the L2CAP_ConnectRsp call with one of the
 * other connection status values.
 */
#define L2CONN_PENDING                      0x0001

/* Rejects the connection request because no such protocol service exists.
 * Generally, this rejection status should only be originated by L2CAP,
 * not by protocol services.
 *
 * The L2EVENT_DISCONNECTED event will be generated after the rejection
 * has been sent.
 */
#define L2CONN_REJECT_PSM_NOT_SUPPORTED     0x0002

/* Rejects the connection request due to a security failure. The
 * L2EVENT_DISCONNECTED event will be generated after the rejection has
 * been sent.
 */
#define L2CONN_REJECT_SECURITY_BLOCK        0x0003

/* Rejects the connection request due to a resource shortage. The
 * L2EVENT_DISCONNECTED event will be generated after the rejection has
 * been sent.
 */
#define L2CONN_REJECT_NO_RESOURCES          0x0004

/* End of L2capConnStatus */

#endif
