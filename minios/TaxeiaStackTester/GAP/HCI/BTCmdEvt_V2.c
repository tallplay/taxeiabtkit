#include "BTCmdEvt_v2.h"
#include "../../CORE/BTCore_V2.h"
#include "../../TaxeiaConsoleDebug/TaxeiaConsoledebug.h"
#include "../../CORE/DataQueue.h"
#include <string.h>
#include "../../TaxeiaHCILog/TaxeiaHCILog.h"
#include <stdio.h>

#define atomic_inc(num) (++num)                                                                         
/********************************************************************************************************/
//Queue for complete callback begin
/********************************************************************************************************/
typedef struct                                                                                          //
{                                                                                                       //
    U16 cmdop;                                                                                          //
    CompleteDataHandler handler;                                                                        //
}CompleteQueueEntry;                                                                                    //
                                                                                                        //
#define CMPQMASK (4-1)                                                                                  //                                                                                                        //
U8 Cmp_QueueQRidx, Cmp_QueueQWidx;                                                                        //
                                                                                                        //
CompleteQueueEntry Cmp_Queue[CMPQMASK+1];                                                                //
                                                                                                        //
#define CMPQ_BUF_INIT   {Cmp_QueueQRidx = Cmp_QueueQWidx = 0;}                                            //
#define CMPQ_BUF_EMPTY()         ((CMPQMASK & Cmp_QueueQRidx) == (CMPQMASK & Cmp_QueueQWidx))             //
#define CMPQ_BUF_FULL()          ((CMPQMASK & Cmp_QueueQRidx) == (CMPQMASK & (Cmp_QueueQWidx + 1)))       //
#define CMPQ_BUF_COUNT()         (CMPQMASK & (Cmp_QueueQWidx - Cmp_QueueQRidx))                           //
                                                                                                        //
void CMPQ_BUF_WRITE(U16 OP, CompleteDataHandler CB)                                                     //   
{                                                                                                       //
    CompleteQueueEntry *tmp = &Cmp_Queue[CMPQMASK & atomic_inc(Cmp_QueueQWidx)];                          //
    tmp->cmdop = OP;                                                                                    //
    tmp->handler = CB;                                                                                  //
}                                                                                                       //
                                                                                                        //
CompleteDataHandler CMPQ_BUF_READ(U16 OP)                                                               //
{                                                                                                       //
    CompleteQueueEntry *tmp;                                                                            //
    U8 search;                                                                                          //
                                                                                                        //
    search = CMPQMASK;                                                                                  //
                                                                                                        //
    if(CMPQ_BUF_EMPTY()) return 0;                                                                      //
                                                                                                        //
    while(!CMPQ_BUF_EMPTY() && search)                                                                  //
    {                                                                                                   //
        tmp = &(Cmp_Queue[CMPQMASK & atomic_inc(Cmp_QueueQRidx)]);                                        //
                                                                                                        //
        if(tmp->cmdop == OP)                                                                            //
            return tmp->handler;                                                                        //
        else                                                                                            //
        {                                                                                               //
            CMPQ_BUF_WRITE(tmp->cmdop, tmp->handler);                                                   //
            search--;                                                                                   //
        }                                                                                               //
    }                                                                                                   //
                                                                                                        //
    return 0;                                                                                           //
}                                                                                                       //
/********************************************************************************************************/

/********************************************************************************************************/
//command wait queue                                                                                    //
/********************************************************************************************************/
typedef struct                                                                                          //
{                                                                                                       //
    U8 *buf; //4+parameter, 0x01+OPL+OPH+LEN+...                                                        //
}CmdQueueEntry;                                                                                         //
                                                                                                        //
#define CMDQMASK (4-1)                                                                                  //
                                                                                                        //
U8 CmdQRidx, CmdQWidx;                                                                                  //
                                                                                                        //
CmdQueueEntry Cmd_Wait[CMDQMASK+1];                                                                      //
                                                                                                        //
#define atomic_inc(num) (++num)                                                                         //
                                                                                                        //
#define CMDQ_BUF_INIT   {CmdQRidx = CmdQWidx = 0;}                                                      //
                                                                                                        //
void CMDQ_BUF_WRITE(U16 OP, U8 len, U8 *buf, CompleteDataHandler CB)                                    //   
{                                                                                                       //
    CmdQueueEntry *tmp = &Cmd_Wait[CMDQMASK & atomic_inc(CmdQWidx)];                                     //
    tmp->buf = (U8*)malloc(len+4);                                                                           //
                                                                                                        //
    ASSERT(tmp->buf);                                                                                   //
                                                                                                        //
    tmp->buf[0] = 0x01;                                                                                 //
    tmp->buf[1] = OP & 0xff;                                                                            //
    tmp->buf[2] = (OP >> 8) & 0xff;                                                                     //
    tmp->buf[3] = len;                                                                                  //
                                                                                                        //
    if(len) memcpy(tmp->buf+4, buf, len);                                                               //
                                                                                                        //
    CMPQ_BUF_WRITE(OP, CB);                                                                             //
}                                                                                                       //
                                                                                                        //
#define CMDQ_BUF_READ()          (Cmd_Wait[CMDQMASK & atomic_inc(CmdQRidx)])                             //
#define CMDQ_BUF_EMPTY()         ((CMDQMASK & CmdQRidx) == (CMDQMASK & CmdQWidx))                       //
#define CMDQ_BUF_FULL()          ((CMDQMASK & CmdQRidx) == (CMDQMASK & (CmdQWidx + 1)))                 //
#define CMDQ_BUF_COUNT()         (CMDQMASK & (CmdQWidx - CmdQRidx))                                     //
                                                                                                        //
                                                                                                        //
U8 BtCmdReq(U16 OP, U8 len, U8 *buf, CompleteDataHandler CB)                                            //
{                                                                                                       //
    if(!CMDQ_BUF_FULL())                                                                                //
    {                                                                                                   //
        CMDQ_BUF_WRITE(OP, len, buf, CB);                                                               //
        return 0;                                                                                       //
    }                                                                                                   //
                                                                                                        //
    ASSERT(0);                                                                                          //
    return 1; //full                                                                                    //
}                                                                                                       //
                                                                                                        //
                                                                                                        //
U8 BtCmdWaitFetch(void (*SendC)(U8 *))                                                                  //
{                                                                                                       //
    if(!CMDQ_BUF_EMPTY())                                                                               //
    {                                                                                                   //
        U8 *buf = CMDQ_BUF_READ().buf;                                                                  //
        SendC(buf);                                                                                     //
        free(buf);                                                                                      //
        return 0;                                                                                       //
    }                                                                                                   //
                                                                                                        //
    return 1; //empty                                                                                   //
}                                                                                                       //
/********************************************************************************************************/


//command arguments
#define CMD_PARM_MAX 38

U8 Evt_Buffer[66];
U8 Rx_DataBuffer[ACL_MAX_PACKET_LEN];
U8 Rx_DataHeader[4];

static EvtHandler evtHandler = 0;
static AclRxDataHandler AclRDataHandler = 0;

///////////////////////////////////Public API///////////////////////////////////////////
void RegisterEvtHandler(EvtHandler evh)
{
    evtHandler = evh;
}

void RegisterAclDataHandler(AclRxDataHandler adh)
{
    AclRDataHandler = adh;
}

BOOL SendCmd(U16 op, U8 *dat, CompleteDataHandler cdh, U8 len)
{
    return SendCmdWithLen(op, dat, len, cdh);
}

BOOL SendCmdWithLen(U16 op, U8 *dat, U8 len, CompleteDataHandler cdh)
{
    return !BtCmdReq(op, len, dat, cdh);        
}

///////////////////////////////////Private Functions/////////////////////////////////////
void Evt_CallBack(U8 *evtbuf)
{
    U8 EvtOp;
    U16 CmdOp;

    EVT_SHOW(evtbuf[0], evtbuf[1], evtbuf+2);

    EvtOp = evtbuf[0];
    
    if(EvtOp == HCE_COMMAND_COMPLETE)
    {
        CmdOp = (evtbuf[3] | (evtbuf[4] << 8));
        if(CmdOp == HCC_MTK6622_HOST_WAKEUP)
        {
            Taxeia_Report("skip host wakeup complete event\r\n");
            return;
        }
        else if(CmdOp == HCC_MTK6622_WAKEUP)
        {
            Taxeia_Report("skip bt wakeup complete event\r\n");
            return;
        }
    }

    if(EvtOp == HCE_COMMAND_STATUS && evtbuf[4] == 0 && evtbuf[5] == 0)
    {//nop 
        return;
    }
            
    if(EvtOp == HCE_COMMAND_COMPLETE)
    {

        //ending event handler
        CompleteDataHandler cdh;

        CmdOp = (evtbuf[3] | (evtbuf[4] << 8));
        
        cdh = CMPQ_BUF_READ(CmdOp);
        if(cdh)
            cdh(CmdOp, HCE_COMMAND_COMPLETE, evtbuf + 5);
        
    }
    else if(EvtOp == HCE_COMMAND_STATUS) //we need to consider if command can't execute
    {
        CompleteDataHandler cdh;
        if((evtbuf[2] != HC_STATUS_SUCCESS)) //not success, end the command 
        {
        }
        
        CmdOp = (evtbuf[4] | (evtbuf[5] << 8));
        cdh = CMPQ_BUF_READ(CmdOp);
        
        if(cdh)
        {
            if(cdh(CmdOp, HCE_COMMAND_STATUS, evtbuf + 2) == 1) //if commplete complete callback register?
                CMPQ_BUF_WRITE(CmdOp, cdh);
        }
    }

    //global event handler
    if(evtHandler)
        evtHandler(EvtOp, evtbuf + 2);                       
}

void Rx_DataCallBack(U8 *dat)
{
    U16 residue = 0;
    U16 handle = 0;
    U8 PB_bit = 0;
    
    handle = dat[0] | ((dat[1] & 0xf) << 8);
    PB_bit = (dat[1] >> 4) & 0x3;
    residue = dat[2] | (dat[3] << 8);

    if(AclRDataHandler)
    {
        AclRDataHandler(handle, residue, dat + 4);
    }
        
}

void BtCmdEvtInit()                                                                                     //
{                                                                                                       //
    CMPQ_BUF_INIT;                                                                                      //
    CMDQ_BUF_INIT;                                                                                      //
}                                                                                                       //

