#include "BTCmdEvt.h"
#include "../../CORE/BTCore.h"
#include "../../TaxeiaConsoleDebug/TaxeiaConsoledebug.h"
#include "../../CORE/DataQueue.h"
#include <string.h>
#include "../../TaxeiaHCILog/TaxeiaHCILog.h"

//command arguments
#define CMD_PARM_MAX 38
//main arguments
static CMDIDX Cmdidx;
static CompleteDataHandler CmdHandler = 0;
//saved arguments, used to store the command which can't be executed immediately, it will be executed later
static CMDIDX CmdidxSave = HCCI_TAXEIA_END;
static CompleteDataHandler CmdHandlerSave = 0;

#ifdef NEW_BT_LOOP2
static U8 CmdParm[CMD_PARM_MAX+3];
static U8 CmdParmSave[CMD_PARM_MAX+3];
#else
static U8 CmdParm[CMD_PARM_MAX];
static U8 CmdParmSave[CMD_PARM_MAX];
#endif

#ifdef NEW_BT_LOOP
U8 EvtBuffer[66];
U8 RxDataBuffer[ACL_MAX_PACKET_LEN];
U8 RxDataHeader[4];
#else
static U8 EvtReturnParam[64];
static U16 CmdCompleteOp;
static U8 RxDataPacket[ACL_MAX_PACKET_LEN];
#endif
static U8 TxDataPacket[ACL_MAX_PACKET_LEN];
static U16 TxDataLen;
static EvtHandler evtHandler = 0;
static AclRxDataHandler AclRDataHandler = 0;

const HCICommandFormat hci_cmds[HCCI_TAXEIA_END+1]=
{
    {HCC_INQUIRY                      ,0x05, HCE_COMMAND_STATUS, CmdParm/*, HCE_INQUIRY_RESULT, HCE_INQUIRY_COMPLETE*/}
   ,{HCC_INQUIRY_CANCEL               ,0x00, HCE_COMMAND_COMPLETE, 0}
   ,{HCC_START_PERIODIC_INQ_MODE      ,0x08, HCE_COMMAND_COMPLETE, 0/*, HCE_INQUIRY_RESULT, HCE_INQUIRY_COMPLETE*/}
   ,{HCC_EXIT_PERIODIC_INQ_MODE       ,0x00, HCE_COMMAND_COMPLETE, 0}
   ,{HCC_CREATE_CONNECTION            ,0x0d, HCE_CONNECT_COMPLETE, CmdParm/*, 0xff, HCE_CONNECT_COMPLETE*/}
   ,{HCC_DISCONNECT                   ,0x03, HCE_DISCONNECT_COMPLETE, CmdParm/*, 0xff, HCE_DISCONNECT_COMPLETE*/}
   ,{HCC_CREATE_CONNECTION_CANCEL     ,0x06, HCE_COMMAND_COMPLETE, 0} /* 1.2 */
   ,{HCC_ACCEPT_CON_REQ               ,0x07, HCE_COMMAND_STATUS/*HCE_CONNECT_COMPLETE*/, CmdParm}
   ,{HCC_REJECT_CON_REQ               ,0x07, HCE_CONNECT_COMPLETE, 0/*, 0xff, HCE_CONNECT_COMPLETE*/}
   ,{HCC_LINK_KEY_REQ_REPL            ,0x16, HCE_COMMAND_COMPLETE, CmdParm}
   ,{HCC_LINK_KEY_REQ_NEG_REPL        ,0x06, HCE_COMMAND_COMPLETE, CmdParm}
   ,{HCC_PIN_CODE_REQ_REPL            ,0x17, HCE_COMMAND_COMPLETE, CmdParm}
   ,{HCC_PIN_CODE_REQ_NEG_REPL        ,0x06, HCE_COMMAND_COMPLETE, 0}
   ,{HCC_CHNG_CONN_PACKET_TYPE        ,0x04, HCE_CONN_PACKET_TYPE_CHNG, 0/*, 0xff, HCE_CONN_PACKET_TYPE_CHNG*/}
   ,{HCC_AUTH_REQ                     ,0x02, HCE_COMMAND_STATUS/*HCE_AUTH_COMPLETE*/, CmdParm}
   ,{HCC_SET_CONN_ENCRYPT             ,0x03, HCE_ENCRYPT_CHNG, CmdParm/*, 0xff, HCE_ENCRYPT_CHNG*/}
   ,{HCC_CHNG_CONN_LINK_KEY           ,0x02, HCE_CHNG_CONN_LINK_KEY_COMPLETE, 0/*, 0xff, HCE_CHNG_CONN_LINK_KEY_COMPLETE*/}
   ,{HCC_MASTER_LINK_KEY              ,0x01, HCE_MASTER_LINK_KEY_COMPLETE, 0/*, 0xff, HCE_MASTER_LINK_KEY_COMPLETE*/}
   ,{HCC_REM_NAME_REQ                 ,0x0a, HCE_REMOTE_NAME_REQ_COMPLETE, CmdParm/*, 0xff, HCE_REMOTE_NAME_REQ_COMPLETE*/}
   ,{HCC_REM_NAME_REQ_CANCEL          ,0x06, HCE_COMMAND_COMPLETE, 0} /* 1.2 */
   ,{HCC_READ_REMOTE_FEATURES         ,0x02, HCE_READ_REMOTE_FEATURES_COMPLETE, 0/*, 0xff, HCE_READ_REMOTE_FEATURES_COMPLETE*/}
   ,{HCC_READ_REMOTE_EXT_FEATURES     ,0x03, HCE_READ_REMOTE_EXT_FEAT_COMPLETE, 0/*, 0xff, HCE_READ_REMOTE_EXT_FEAT_COMPLETE*/} /* 1.2 */
   ,{HCC_READ_REMOTE_VERSION          ,0x02, HCE_READ_REMOTE_VERSION_COMPLETE, 0/*, 0xff, HCE_READ_REMOTE_VERSION_COMPLETE*/}
   ,{HCC_READ_CLOCK_OFFSET            ,0x02, HCE_READ_CLOCK_OFFSET_COMPLETE, 0/*, 0xff, HCE_READ_CLOCK_OFFSET_COMPLETE*/}
   ,{HCC_READ_LMP_HANDLE              ,0x02, HCE_COMMAND_COMPLETE, 0} /* 1.2 */

   ,{HCC_SETUP_SYNC_CONNECTION        ,0x11, HCE_SYNC_CONNECT_COMPLETE, CmdParm} /* 1.2 */
   ,{HCC_ACCEPT_SYNC_CON_REQ          ,0x15, HCE_SYNC_CONNECT_COMPLETE, CmdParm/*, 0xff, HCE_SYNC_CONNECT_COMPLETE*/} /* 1.2 */
   ,{HCC_REJECT_SYNC_CON_REQ          ,0x07, HCE_SYNC_CONNECT_COMPLETE, CmdParm/*, 0xff, HCE_SYNC_CONNECT_COMPLETE*/} /* 1.2 */
   ,{HCC_IOCAP_REQ_REPLY              ,0x09, HCE_COMMAND_COMPLETE, CmdParm} 
   ,{HCC_USER_CONFIRM_REQ_REPLY       ,0x06, HCE_COMMAND_COMPLETE, CmdParm}
   ,{HCC_USER_CONFIRM_REQ_NEG_REPLY   ,0x06, HCE_COMMAND_COMPLETE, CmdParm}
   ,{HCC_USER_PASSKEY_REQ_REPLY       ,0x0a, HCE_COMMAND_COMPLETE, 0}   
   ,{HCC_USER_PASSKEY_REQ_NEG_REPLY   ,0x06, HCE_COMMAND_COMPLETE, 0}   
   ,{HCC_REM_OOB_DATA_REQ_REPLY       ,0x26, HCE_COMMAND_COMPLETE, CmdParm}      
   ,{HCC_REM_OOB_DATA_REQ_NEG_REPLY   ,0x06, HCE_COMMAND_COMPLETE, 0}  
   ,{HCC_IOCAP_REQ_NEG_REPLY          ,0x07, HCE_COMMAND_COMPLETE, CmdParm}   



   //need to add 0x0435 to 0x043C commands, 3.0
                                       
/* Group: Link policy commands */          
   ,{HCC_HOLD_MODE                    ,0x06, HCE_MODE_CHNG, 0/*, 0xff, HCE_MODE_CHNG*/}
   ,{HCC_SNIFF_MODE                   ,0x0a, HCE_MODE_CHNG, CmdParm/*, 0xff, HCE_MODE_CHNG*/}
   ,{HCC_EXIT_SNIFF_MODE              ,0x02, HCE_MODE_CHNG, CmdParm/*, 0xff, HCE_MODE_CHNG*/}
   ,{HCC_PARK_MODE                    ,0x06, HCE_MODE_CHNG, 0/*, 0xff, HCE_MODE_CHNG*/}
   ,{HCC_EXIT_PARK_MODE               ,0x02, HCE_MODE_CHNG, 0/*, 0xff, HCE_MODE_CHNG*/}
   ,{HCC_QOS_SETUP                    ,0x14, HCE_QOS_SETUP_COMPLETE, 0/*, 0xff, HCE_QOS_SETUP_COMPLETE*/}
   ,{HCC_ROLE_DISCOVERY               ,0x02, HCE_COMMAND_COMPLETE, CmdParm}
   ,{HCC_SWITCH_ROLE                  ,0x07, HCE_ROLE_CHANGE, CmdParm/*, 0xff, HCE_ROLE_CHANGE*/}
   ,{HCC_READ_LINK_POLICY             ,0x02, HCE_COMMAND_COMPLETE, 0}
   ,{HCC_WRITE_LINK_POLICY            ,0x04, HCE_COMMAND_COMPLETE, CmdParm}
   ,{HCC_READ_DEFAULT_LINK_POLICY     ,0x00, HCE_COMMAND_COMPLETE, 0} /* 1.2 */
   ,{HCC_WRITE_DEFAULT_LINK_POLICY    ,0x02, HCE_COMMAND_COMPLETE, CmdParm} /* 1.2 */
   ,{HCC_FLOW_SPECIFICATION           ,0x15, HCE_FLOW_SPECIFICATION_COMPLETE, 0/*, 0xff, HCE_FLOW_SPECIFICATION_COMPLETE*/} /* 1.2 */
   ,{HCC_SNIFF_SUBRATING              ,0x08, HCE_COMMAND_COMPLETE, CmdParm} /* 2.1 */
    
/* Group: Host controller and baseband commands */
   ,{HCC_SET_EVENT_MASK               ,0x08, HCE_COMMAND_COMPLETE, 0}
   ,{HCC_RESET                        ,0x00, HCE_COMMAND_COMPLETE, 0}
   ,{HCC_EVENT_FILTER                 ,0x03/*or else*/, HCE_COMMAND_COMPLETE, 0}
   ,{HCC_FLUSH                        ,0x02, HCE_COMMAND_COMPLETE, 0}
   ,{HCC_READ_PIN_TYPE                ,0x00, HCE_COMMAND_COMPLETE, 0}
   ,{HCC_WRITE_PIN_TYPE               ,0x01, HCE_COMMAND_COMPLETE, 0}
   ,{HCC_CREATE_NEW_UNIT_KEY          ,0x00, HCE_COMMAND_COMPLETE, 0}
   ,{HCC_READ_STORED_LINK_KEY         ,0x07, HCE_COMMAND_COMPLETE, CmdParm}
   ,{HCC_WRITE_STORED_LINK_KEY        ,0x17/*or+22n*/, HCE_COMMAND_COMPLETE, CmdParm}
   ,{HCC_DEL_STORED_LINK_KEY          ,0x07, HCE_COMMAND_COMPLETE, 0}
   ,{HCC_CHNG_LOCAL_NAME              ,248,  HCE_COMMAND_COMPLETE, CmdParm}
   ,{HCC_READ_LOCAL_NAME              ,0x00, HCE_COMMAND_COMPLETE, 0}
   ,{HCC_READ_CONN_ACCEPT_TIMEOUT     ,0x00, HCE_COMMAND_COMPLETE, 0}
   ,{HCC_WRITE_CONN_ACCEPT_TIMEOUT    ,0x02, HCE_COMMAND_COMPLETE, 0}
   ,{HCC_READ_PAGE_TIMEOUT            ,0x00, HCE_COMMAND_COMPLETE, 0}
   ,{HCC_WRITE_PAGE_TIMEOUT           ,0x02, HCE_COMMAND_COMPLETE, 0}
   ,{HCC_READ_SCAN_ENABLE             ,0x00, HCE_COMMAND_COMPLETE, 0}
   ,{HCC_WRITE_SCAN_ENABLE            ,0x01, HCE_COMMAND_COMPLETE, CmdParm}
   ,{HCC_READ_PAGE_SCAN_ACTIVITY      ,0x00, HCE_COMMAND_COMPLETE, 0}
   ,{HCC_WRITE_PAGE_SCAN_ACTIVITY     ,0x04, HCE_COMMAND_COMPLETE, 0}
   ,{HCC_READ_INQ_SCAN_ACTIVITY       ,0x00, HCE_COMMAND_COMPLETE, 0}
   ,{HCC_WRITE_INQ_SCAN_ACTIVITY      ,0x04, HCE_COMMAND_COMPLETE, CmdParm}
   ,{HCC_READ_AUTH_ENABLE             ,0x00, HCE_COMMAND_COMPLETE, 0}
   ,{HCC_WRITE_AUTH_ENABLE            ,0x01, HCE_COMMAND_COMPLETE, CmdParm}
   ,{HCC_READ_CLASS_OF_DEVICE         ,0x00, HCE_COMMAND_COMPLETE, 0}
   ,{HCC_WRITE_CLASS_OF_DEVICE        ,0x03, HCE_COMMAND_COMPLETE, CmdParm}
   ,{HCC_READ_VOICE_SETTING           ,0x00, HCE_COMMAND_COMPLETE, 0}
   ,{HCC_WRITE_VOICE_SETTING          ,0x02, HCE_COMMAND_COMPLETE, 0}
   ,{HCC_READ_AUTO_FLUSH_TIMEOUT      ,0x02, HCE_COMMAND_COMPLETE, 0}
   ,{HCC_WRITE_AUTO_FLUSH_TIMEOUT     ,0x04, HCE_COMMAND_COMPLETE, 0}
   ,{HCC_READ_NUM_BC_RETRANSMIT       ,0x00, HCE_COMMAND_COMPLETE, 0}
   ,{HCC_WRITE_NUM_BC_RETRANSMIT      ,0x01, HCE_COMMAND_COMPLETE, 0}
   ,{HCC_READ_HOLD_MODE_ACTIVITY      ,0x00, HCE_COMMAND_COMPLETE, 0}
   ,{HCC_WRITE_HOLD_MODE_ACTIVITY     ,0x01, HCE_COMMAND_COMPLETE, 0}
   ,{HCC_READ_XMIT_POWER_LEVEL        ,0x03, HCE_COMMAND_COMPLETE, 0}
   ,{HCC_READ_SCO_FC_ENABLE           ,0x00, HCE_COMMAND_COMPLETE, 0}
   ,{HCC_WRITE_SCO_FC_ENABLE          ,0x01, HCE_COMMAND_COMPLETE, 0}
   ,{HCC_SET_CTRLR_TO_HOST_FLOW_CTRL  ,0x01, HCE_COMMAND_COMPLETE, 0}
   ,{HCC_HOST_BUFFER_SIZE             ,0x07, HCE_COMMAND_COMPLETE, CmdParm}
   ,{HCC_HOST_NUM_COMPLETED_PACKETS   ,0x05/*or+4n*/, 0xff}
   ,{HCC_READ_LINK_SUPERV_TIMEOUT     ,0x02, HCE_COMMAND_COMPLETE, 0}
   ,{HCC_WRITE_LINK_SUPERV_TIMEOUT    ,0x04, HCE_COMMAND_COMPLETE, CmdParm}
   ,{HCC_READ_NUM_IAC                 ,0x00, HCE_COMMAND_COMPLETE, CmdParm}
   ,{HCC_READ_CURRENT_IAC_LAP         ,0x00, HCE_COMMAND_COMPLETE, 0}
   ,{HCC_WRITE_CURRENT_IAC_LAP        ,0x04/*or+3n*/, HCE_COMMAND_COMPLETE, CmdParm}
   ,{HCC_SET_AFH_HOST_CHAN_CLASS      ,0x0a, HCE_COMMAND_COMPLETE, 0} /* 1.2 */
   ,{HCC_READ_INQ_SCAN_TYPE           ,0x00, HCE_COMMAND_COMPLETE, 0} /* 1.2 */
   ,{HCC_WRITE_INQ_SCAN_TYPE          ,0x01, HCE_COMMAND_COMPLETE, CmdParm} /* 1.2 */
   ,{HCC_READ_INQ_MODE                ,0x00, HCE_COMMAND_COMPLETE, 0} /* 1.2 */
   ,{HCC_WRITE_INQ_MODE               ,0x01, HCE_COMMAND_COMPLETE, CmdParm} /* 1.2 */
   ,{HCC_READ_PAGE_SCAN_TYPE          ,0x00, HCE_COMMAND_COMPLETE, 0} /* 1.2 */
   ,{HCC_WRITE_PAGE_SCAN_TYPE         ,0x01, HCE_COMMAND_COMPLETE, CmdParm} /* 1.2 */
   ,{HCC_READ_AFH_CHAN_ASSESS_MODE    ,0x00, HCE_COMMAND_COMPLETE, 0} /* 1.2 */
   ,{HCC_WRITE_AFH_CHAN_ASSESS_MODE   ,0x01, HCE_COMMAND_COMPLETE, 0} /* 1.2 */
   ,{HCC_READ_SIMPLE_PAIRING_MODE     ,0x00, HCE_COMMAND_COMPLETE, 0}
   ,{HCC_WRITE_SIMPLE_PAIRING_MODE    ,0x01, HCE_COMMAND_COMPLETE, CmdParm}
   ,{HCC_READ_LOCAL_OOB_DATA          ,0x00, HCE_COMMAND_COMPLETE, CmdParm}   
   /*need to add 0x0C51 to 0x0C59 commands*/
   
/* Group: Informational parameters */
   ,{HCC_READ_LOCAL_VERSION           ,0x00, HCE_COMMAND_COMPLETE, 0}
   ,{HCC_READ_LOCAL_COMMANDS          ,0x00, HCE_COMMAND_COMPLETE, 0} /* 1.2 */
   ,{HCC_READ_LOCAL_FEATURES          ,0x00, HCE_COMMAND_COMPLETE, 0}
   ,{HCC_READ_LOCAL_EXT_FEATURES      ,0x01, HCE_COMMAND_COMPLETE, CmdParm} /* 1.2 */
   ,{HCC_READ_BUFFER_SIZE             ,0x00, HCE_COMMAND_COMPLETE, 0}
   ,{HCC_READ_BD_ADDR                 ,0x00, HCE_COMMAND_COMPLETE, 0}
   ,{HCC_READ_DATA_BLOCK_SIZE         ,0x00, HCE_COMMAND_COMPLETE, 0}

/* Group: Status parameters */
   ,{HCC_READ_FAILED_CONTACT_COUNT    ,0x02, HCE_COMMAND_COMPLETE, 0}
   ,{HCC_RESET_FAILED_CONTACT_COUNT   ,0x02, HCE_COMMAND_COMPLETE, 0}
   ,{HCC_GET_LINK_QUALITY             ,0x02, HCE_COMMAND_COMPLETE, 0}
   ,{HCC_READ_RSSI                    ,0x02, HCE_COMMAND_COMPLETE, CmdParm}
   ,{HCC_READ_AFH_CHANNEL_MAP         ,0x02, HCE_COMMAND_COMPLETE, 0} /* 1.2 */
   ,{HCC_READ_CLOCK                   ,0x03, HCE_COMMAND_COMPLETE, 0} /* 1.2 */

   /*need to add 0x1408 to 0x140B commands*, 3.0+HS*/
   
/* Group: Testing commands */
   ,{HCC_READ_LOOPBACK_MODE           ,0x00, HCE_COMMAND_COMPLETE, 0}
   ,{HCC_WRITE_LOOPBACK_MODE          ,0x01, HCE_COMMAND_COMPLETE, 0}
   ,{HCC_ENABLE_DUT                   ,0x00, HCE_COMMAND_COMPLETE, CmdParm}

/* STLC2500C vendor commands */
   ,{HCC_ST_STATIC_TXRX               ,5, HCE_COMMAND_COMPLETE, 0}
   ,{HCC_ST_TXRX_TEST                 ,28,HCE_COMMAND_COMPLETE, 0}
   ,{HCC_ERI_ADDRESS                  ,8, HCE_COMMAND_COMPLETE, CmdParm}
/* mtk vendor commands */
   ,{HCC_MTK6622_SET_BDADDR           ,6, HCE_COMMAND_COMPLETE, CmdParm}
   ,{HCC_MTK6622_SET_RADIO            ,6, HCE_COMMAND_COMPLETE, CmdParm}
   ,{HCC_MTK6622_SET_SLEEP            ,7, HCE_COMMAND_COMPLETE, CmdParm}
   ,{HCC_MTK6622_WAKEUP               ,0, HCE_COMMAND_COMPLETE, CmdParm}
   ,{HCC_MTK6622_HOST_WAKEUP          ,0, HCE_COMMAND_COMPLETE, CmdParm}
   ,{HCC_MTK6622_SET_BAUD             ,6, HCE_COMMAND_COMPLETE, CmdParm}
   ,{HCC_MTK6622_SET_PCM              ,1, HCE_COMMAND_COMPLETE, CmdParm}
   ,{HCC_MTK6622_TEST_CONTROL        ,23, HCE_COMMAND_COMPLETE, CmdParm} 
   ,{HCC_MTK6622_FW_PATCH_MODE       ,1, HCE_COMMAND_COMPLETE, CmdParm}
   ,{HCC_MTK6622_FW_PATCH            ,37, HCE_COMMAND_COMPLETE, CmdParm}
   ,{HCC_MTK6622_SET_XO_TRIM         ,1,  HCE_COMMAND_COMPLETE, CmdParm}
   ,{0, 0, 0, 0}
};


///////////////////////////////////Public API///////////////////////////////////////////
void RegisterEvtHandler(EvtHandler evh)
{
    evtHandler = evh;
}

void RegisterAclDataHandler(AclRxDataHandler adh)
{
    AclRDataHandler = adh;
}

BOOL SendCmd(CMDIDX idx, U8 *dat, CompleteDataHandler cdh)
{
    return SendCmdWithLen(idx, dat, hci_cmds[idx].parmlen, cdh);
}

BOOL SendCmdWithLen(CMDIDX idx, U8 *dat, U8 len, CompleteDataHandler cdh)
{
    BOOL sts;
    sts = CmdRequest();
#ifdef NEW_BT_LOOP2
    
    if(sts == TRUE)
    {
        CmdHandler = cdh;
        
        if(dat) memcpy(CmdParm+3, dat, len);
        
        Cmdidx = idx;
    }
    else
    {
        if(CmdidxSave == HCCI_TAXEIA_END) //no one save
        {
            Taxeia_Report("Save Cmd %x\r\n", hci_cmds[idx].op);
            CmdHandlerSave = cdh;
            if(dat) memcpy(CmdParmSave+3, dat, len > CMD_PARM_MAX ? CMD_PARM_MAX : len);            
            CmdidxSave = idx;
            sts = TRUE;
        }
        else
        {
            Taxeia_Report("Save method full!!!!!\r\n");
        }
    }
#else
d
    if(sts == TRUE)
    {
        CmdHandler = cdh;
        
        if(dat) memcpy(CmdParm, dat, len);
        
        Cmdidx = idx;
    }
    else
    {
        if(CmdidxSave == HCCI_TAXEIA_END) //no one save
        {
            Taxeia_Report("Save Cmd %x\r\n", hci_cmds[idx].op);
            CmdHandlerSave = cdh;
            if(dat) memcpy(CmdParmSave, dat, len);            
            CmdidxSave = idx;
        }
        else
        {
            Taxeia_Report("Save method full!!!!!\r\n");
        }
    }
#endif
    return sts;
}

void CheckSavedCmd(void)
{    
#ifdef NEW_BT_LOOP2
    if(CmdidxSave != HCCI_TAXEIA_END)
    {
        if(CmdRequest() == TRUE)
        {
            Taxeia_Report("ReSend Cmd %x!!!!!\r\n", hci_cmds[CmdidxSave].op);
            CmdHandler = CmdHandlerSave;
            Cmdidx = CmdidxSave;
            if(hci_cmds[Cmdidx].parmlen) memcpy(CmdParm+3, CmdParmSave+3, hci_cmds[Cmdidx].parmlen > CMD_PARM_MAX ? CMD_PARM_MAX : hci_cmds[Cmdidx].parmlen);            

            CmdidxSave = HCCI_TAXEIA_END;
        }
    }
#else
    if(CmdidxSave != HCCI_TAXEIA_END)
    {
        if(CmdRequest() == TRUE)
        {
            Taxeia_Report("ReSend Cmd %x!!!!!\r\n", hci_cmds[CmdidxSave].op);
            CmdHandler = CmdHandlerSave;
            Cmdidx = CmdidxSave;
            if(hci_cmds[Cmdidx].parmlen) memcpy(CmdParm, CmdParmSave, hci_cmds[Cmdidx].parmlen);            

            CmdidxSave = HCCI_TAXEIA_END;
        }
    }
#endif
}

U8 GetTxDataBuffer(U16 idx)
{
    return TxDataPacket[idx];
}

void FillTxDataBuffer(U16 idx, U8 dat)
{
    TxDataPacket[idx] = dat;
}

void MoveTxDataBuffer(U16 tgtidx, U16 srcidx)
{
    TxDataPacket[tgtidx] = TxDataPacket[srcidx];
}

//only allow l2cap use, because we do acl num control
BOOL SendData(U16 len)
{
    BOOL sts;
    sts = DataRequest();
    
    if(sts == TRUE)
    {
        TxDataLen = len;
    }
    return sts;
}


///////////////////////////////////Private Functions/////////////////////////////////////
#ifndef NEW_BT_LOOP

void EvtCmdCompleteCallBack(U8 idx, U8 dat)
{
    if(idx == 1) //Command_Opcode
    {
        ASSERT(dat == (hci_cmds[Cmdidx].op & 0xff) || (dat == (HCC_MTK6622_HOST_WAKEUP & 0xff))
            || (dat == (HCC_MTK6622_WAKEUP & 0xff)));
        CmdCompleteOp = dat;
    }
    else if(idx == 2) //Command_Opcode
    {
        ASSERT(dat == ((hci_cmds[Cmdidx].op >> 8) & 0xff) || (dat == (HCC_MTK6622_HOST_WAKEUP >> 8))
            || (dat == (HCC_MTK6622_WAKEUP >> 8)));
        CmdCompleteOp = CmdCompleteOp | (dat << 8);
    }
    else if(idx >= 3) //Return Parameters
    {
        if(idx < 64+3)
            EvtReturnParam[idx-3] = dat;
    }
    else if(idx == 0)//number of command packet allowed
    {
    }    
}

void EvtCmdStatusCallBack(U8 idx, U8 dat)
{
    if(idx == 2) //op low
	{
        ASSERT(dat == 0 || dat == (hci_cmds[Cmdidx].op & 0xff) || dat == (HCC_SET_CONN_ENCRYPT & 0xff)
        || dat == (HCC_REJECT_SYNC_CON_REQ & 0xff));
	}
    else if(idx == 3) //op high
    {
        ASSERT(dat == 0 || dat == ((hci_cmds[Cmdidx].op >> 8) & 0xff) || dat == ((HCC_SET_CONN_ENCRYPT >> 8) & 0xff)
        || dat == ((HCC_REJECT_SYNC_CON_REQ >> 8) & 0xff));

        if(dat == ((hci_cmds[Cmdidx].op >> 8) & 0xff)) //for except NOP command status
        {
         
        }
    }
    else if(idx == 1) //number of command packet allowed
    {
    }
    else if(idx == 0) //status
    {
    }
    EvtReturnParam[idx] = dat;
}
#endif
void EvtConReqCallBack(U8 idx, U8 dat)
{
}

#ifndef NEW_BT_LOOP2

BOOL DataSendCallback()
{
    static U16 idx = 0;
    U8 *ptr;
    U8 len;
#ifdef TAXEIA_UI_LOG
    U8 len_pre;
    U8 *ptr_pre;
#endif

    if(idx < TxDataLen)
    {
        TxQueuePrePush(&ptr, &len);
        len = len + idx > TxDataLen ? TxDataLen - idx : len;
        TxQueuePostPush(len);

#ifdef TAXEIA_UI_LOG
        ptr_pre = ptr; //for log
        len_pre = len; //for log
#endif
        //push data
        while(len--)
        {
            *ptr++ = TxDataPacket[idx++];
        }
    }

    if(idx == TxDataLen) //push finished!!
    {
        idx = 0;
        TxDataLen = 0;
        return TRUE;
    }
                
    return FALSE;
}

BOOL CmdSendCallback()
{
    static U8 idx = 0;
    U8 *ptr;
    U8 len;
#ifdef TAXEIA_UI_LOG
    U8 len_pre;
    U8 *ptr_pre;
#endif

    if(idx < hci_cmds[Cmdidx].parmlen + 3)
    {
        TxQueuePrePush(&ptr, &len);
        len = len + idx > hci_cmds[Cmdidx].parmlen + 3 ? hci_cmds[Cmdidx].parmlen + 3 - idx : len;
        TxQueuePostPush(len);

#ifdef TAXEIA_UI_LOG
        ptr_pre = ptr; //for log
        len_pre = len; //for log
#endif
        while(len--)
        {
            if(idx > 2) //push data
            {
               if(!hci_cmds[Cmdidx].parm || (idx - 3 > (CMD_PARM_MAX - 1)))
                    *ptr++ = 0x00;
               else
                    *ptr++ = hci_cmds[Cmdidx].parm[idx-3];
            }
            else if(idx == 0) //push op code low
                *ptr++ = hci_cmds[Cmdidx].op & 0xff;
            else if(idx == 1) //push op code high
                *ptr++ = (hci_cmds[Cmdidx].op >> 8) & 0xff;
            else if(idx == 2) //push parameter length
                *ptr++ = hci_cmds[Cmdidx].parmlen;
            idx++;
        }
    }

    if(idx == hci_cmds[Cmdidx].parmlen + 3) //push finished!!
    {
        idx = 0;
        return TRUE;
    }
                
    return FALSE;
}
#else
U16 DataSendCallback(U8 **ptr)
{
    *ptr = TxDataPacket;
    return TxDataLen;
}

//return length
U16 CmdSendCallback(U8 **ptr, U16 *zero_padding_len)
{    
    U16 total_len;

    total_len = hci_cmds[Cmdidx].parmlen + 3;

    if(total_len > CMD_PARM_MAX + 3)
    {
        *zero_padding_len = total_len - (CMD_PARM_MAX + 3);
        total_len = (CMD_PARM_MAX + 3);
    }
    else
        *zero_padding_len = 0;

    if(hci_cmds[Cmdidx].parm)
    {
        *ptr = hci_cmds[Cmdidx].parm;
    }
    else
    {
        *ptr = CmdParm;
    }

    (*ptr)[0] =  hci_cmds[Cmdidx].op & 0xff;
    (*ptr)[1] = (hci_cmds[Cmdidx].op >> 8) & 0xff;
    (*ptr)[2] = hci_cmds[Cmdidx].parmlen;
    return total_len;
    
}
#endif

#ifndef NEW_BT_LOOP

BOOL EvtCallBack()
{
    static U8 idx = 0;
    static U8 lenassigned = 0;
    static U8 residue = 0;
    static U8 EvtOp;
    U8 *ptr;
    U8 rlen;
    while(idx < 2) //get EvtCode and ParmLen
    {
        if(!RxQueueIsEmpty())
        {
            if(idx == 0)
            {
                RxQueuePop(&EvtOp);
                idx++;
            }
            else if(idx == 1)
            {
                RxQueuePop(&residue);
                lenassigned = 1;
                idx++;
            }
        }
        else
            break;
    }
    
    if(residue)
    {
        RxQueuePrePop(&ptr, &rlen);
        rlen = rlen > residue ? residue : rlen;
        RxQueuePostPop(rlen);

        residue -= rlen;
        while(rlen)
        {//parameter region
            switch(EvtOp)
            {
                case HCE_COMMAND_COMPLETE:
                    EvtCmdCompleteCallBack(idx - 2, *ptr);
                    break;
                case HCE_COMMAND_STATUS:
                    EvtCmdStatusCallBack(idx - 2, *ptr);
                    break;
                default:
                    EvtReturnParam[idx - 2] = *ptr;
                    break;
            }

            if(residue == 0 && rlen == 1) //last byte
            {
                U8 tmp;

                if(EvtOp == HCE_COMMAND_COMPLETE && CmdCompleteOp == HCC_MTK6622_HOST_WAKEUP)
                {
                    Taxeia_Report("skip host wakeup complete event\r\n");
                }
                else if(EvtOp == HCE_COMMAND_COMPLETE && CmdCompleteOp == HCC_MTK6622_WAKEUP)
                {
                    Taxeia_Report("skip bt wakeup complete event\r\n");
                }
                else if(EvtOp == HCE_COMMAND_STATUS && EvtReturnParam[2] == 0 && EvtReturnParam[3] == 0)
                {//nop 
                }
                else
                {
                    
                    tmp = Cmdidx;
                    if(hci_cmds[Cmdidx].event_end == EvtOp)
                    {

                        //ending event handler
                        if(CmdHandler)
                            CmdHandler(tmp, EvtOp, EvtReturnParam);
                        
                        Cmdidx = HCCI_TAXEIA_END;
                        CmdResponsed();                    
                    }
                    else if(EvtOp == HCE_COMMAND_STATUS) //we need to consider if command can't execute
                    {
                        if((EvtReturnParam[0] != HC_STATUS_SUCCESS)) //not success, end the command 
                        {
                            Cmdidx = HCCI_TAXEIA_END;
                            CmdResponsed();
                        }

                        //mid event handler
                        if(CmdHandler)
                            CmdHandler(tmp, EvtOp, EvtReturnParam);                    
                    }

                    //global event handler
                    if(evtHandler)
                        evtHandler(EvtOp, EvtReturnParam);                       
                }
            }
            
            rlen--;
            idx++;
            ptr++;
        }
    }

    if((residue == 0) && lenassigned)
    {
        idx = 0;
        residue = 0;
        lenassigned = 0;
        return TRUE;
    }
        
    return FALSE;
}

BOOL RxDataCallBack(void)
{
    static U16 idx = 0;
    static U8 lenassigned = 0;
    static U16 residue = 0;
    static U16 handle = 0;
    static U8 PB_bit = 0;
    static U16 offset = 0;
    
    U8 *ptr;
    U8 rlen;
    static U8 tmp;
    while(idx < 4) //get handle and Total data len
    {
        if(!RxQueueIsEmpty())
        {
            if(idx == 0) //handle low byte
            {
                RxQueuePop(&tmp);
            }
            else if(idx == 1) //handle high byte
            {
                handle = tmp;
                RxQueuePop(&tmp);
                handle |= ((tmp & 0xf) << 8);
                PB_bit = (tmp >> 4) & 0x3;
                if(PB_bit != 0x01) //continue
                {
                    offset = 0;
                }
            }
            else if(idx == 2) //length low byte
            {
                RxQueuePop(&tmp);
            }
            else if(idx == 3) //length high byte
            {
                residue = tmp;
                RxQueuePop(&tmp);
                residue += (tmp << 8);
                lenassigned = 1;
            }

            idx++;

        }
        else
            break;
    }

    if(residue)
    {
        RxQueuePrePop(&ptr, &rlen);
        rlen = rlen > residue ? residue : rlen;
        RxQueuePostPop(rlen);
        residue -= rlen;
        while(rlen)
        {         
            RxDataPacket[offset + idx-4] = *ptr;
            rlen--;
            idx++;
            ptr++;
        }
    }

    if((residue == 0) && lenassigned)
    {
        if(AclRDataHandler)
        {
            AclRDataHandler(handle, offset + idx - 4, RxDataPacket);
            offset = offset + idx - 4;  //this is for continue acl packet
        }
        handle = 0;
        idx = 0;
        residue = 0;
        lenassigned = 0;
        return TRUE;
    }
        
    return FALSE;
}
#else
void EvtCallBack(U8 *evtbuf)
{
    U8 EvtOp;
    U16 CmdOp;

    EvtOp = evtbuf[0];
    
    if(EvtOp == HCE_COMMAND_COMPLETE)
    {
        CmdOp = (evtbuf[3] | (evtbuf[4] << 8));
        if(CmdOp == HCC_MTK6622_HOST_WAKEUP)
        {
            Taxeia_Report("skip host wakeup complete event\r\n");
            return;
        }
        else if(CmdOp == HCC_MTK6622_WAKEUP)
        {
            Taxeia_Report("skip bt wakeup complete event\r\n");
            return;
        }
    }

    if(EvtOp == HCE_COMMAND_STATUS && evtbuf[4] == 0 && evtbuf[5] == 0)
    {//nop 
        return;
    }
            
    if(hci_cmds[Cmdidx].event_end == EvtOp)
    {
        //ending event handler
        if(CmdHandler)
        {
            if(EvtOp == HCE_COMMAND_COMPLETE)
                CmdHandler(Cmdidx, EvtOp, evtbuf + 5);
            else
                CmdHandler(Cmdidx, EvtOp, evtbuf + 2);
        }
        
        Cmdidx = HCCI_TAXEIA_END;
        CmdResponsed();                    
    }
    else if(EvtOp == HCE_COMMAND_STATUS) //we need to consider if command can't execute
    {
        if((evtbuf[2] != HC_STATUS_SUCCESS)) //not success, end the command 
        {
            Cmdidx = HCCI_TAXEIA_END;
            CmdResponsed();
        }

        //mid event handler
        if(CmdHandler)
            CmdHandler(Cmdidx, HCE_COMMAND_STATUS, evtbuf + 2);                    
    }

    //global event handler
    if(evtHandler)
        evtHandler(EvtOp, evtbuf + 2);                       
}

void RxDataCallBack(U8 *dat)
{
    U16 residue = 0;
    U16 handle = 0;
    U8 PB_bit = 0;
    
    handle = dat[0] | ((dat[1] & 0xf) << 8);
    PB_bit = (dat[1] >> 4) & 0x3;
    residue = dat[2] | (dat[3] << 8);

    if(AclRDataHandler)
    {
        AclRDataHandler(handle, residue, dat + 4);
    }
        
}
#endif



//its dangerous!!
U8* BarrowBuffer()
{
    return TxDataPacket+512;
}
