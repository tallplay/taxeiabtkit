#ifndef __BT_LOCAL_DEVICE_HH
#define __BT_LOCAL_DEVICE_HH
#include "Btconfig.h"

///////////////////////////////////Public API///////////////////////////////////////////
typedef U8 *(*FinishNotifyCallback) (U16);
typedef struct
{

    //version
    U8 HCIVersion;
    U8 LMPALVersion;
    U16 HCIRevision;
    U16 ManufactorName;
    U16 LMPALRevision;

    //address
    U8 BT_ADDR[6];

    //variant
    U8 UsedAclNum;

    //packet length
    U16 ACLPktLen;
    U16 ACLPktNum;
    U16 SCOPktNum;
    U8 SCOPktLen;

    //page timeout
    U16 PageTimeout;

    //class of device
    U32 COD;

    //auth enabled
    U8 AuthEnabled;

    //scan enabled
    U8 ScanEnabled;

    U16 isInited : 1;
    U16 isPs3 : 1;
    U16 isAddressLocked : 1;
    U16 Reserved : 13;
    //supported features
    U8 feature[9];

    //trim value
    U8 trim;
#ifdef OOB_ENABLE
    U8 C[16];
    U8 R[16];
#endif
}BT_LOCAL_INFO;

U8 Bt__WriteScanEanbled(U8 mode);
U8 Bt__WriteAuthReq(U16 aclHandle);
U8 Bt__WriteCod(U32 COD);
U8 Bt_AclAvable(void);
U8 Bt_AclReady(void);
U16 Bt__GetAclMaxLength(void);
U8 Bt__WriteSSPMode(U8 mode);
#ifdef EXTEND_INQUIRY_RESP
U8 Bt__WriteLocalName(U8 *dat, U8 len, U8 (*cdh)(U16 CmdOp, U8 EvtOp, U8 *dat));
#else
U8 Bt__WriteLocalName(U8 *dat, U8 len);
#endif
U8 Bt_WriteExtInquiryRsp(U8 *name, U8 len);
U8 Bt__SetBtSleep(U8 enable);
U8 Bt__SetHostSleep(U8 enable);
U8 Bt__SetBtWakeup(void);
U8 Bt__PinCodeReply(U8 *bdaddr, U8 *pin, U8 len);
U8 Bt_SetHostWakeup(void);
U8 Bt__SetConnectionEncryption(U16 aclHandle, U8 enable);
U8 Bt__Test(void);
U8 Bt__RoleDiscovery(U16 aclHandle);

void Bt__SniffRst(U16 aclHandle, U16 attemp, U16 timeout);
void Bt__FreqTest(U8 tx_freq, U8 rx_freq, U8 type, U8 pattern, U16 packet_length);
void Bt__SetIAC(U8 IAC);
void Bt__SniffExit(U16 aclHandle);
void Bt__SniffSubRatingRst(U16 aclHandle);
void Bt__EnterDUT(void);


void Bt__GetLocalInfo(BT_LOCAL_INFO **bt_inf);
void Bt__PreSetCod(U32 cod);
void Bt__LocalDeviceCB(FinishNotifyCallback fc); //call back for initialized

U8 Bt__GetMacAddress(U8 *addr); //is default address
U8 Bt__GetTrim(void);
void Bt__SetPower(U8 power);

#define LOCALCB_INITIALIZED 0x1118
#define LOCALCB_AUTH_SUCCESS 0x1119
#define LOCALCB_ENCRYPTION_CHANGE 0x111A
#define LOCALCB_AUTH_FAIL 0x111B
#define LOCALCB_SCAN_CHANGE 0x111C
#define LOCALCB_BT_WAKEUP 0x111D
#define LOCALCB_ENTER_SNIFF 0x111E
#define LOCALCB_SET_SLEEP 0x111F
#define LOCALCB_SET_HOST_WAKEUP 0x1120
#define LOCALCB_PIN_CODE_REQ 0x1121
///////////////////////////////////Private Functions/////////////////////////////////////
void Bt__LocalDeviceInit(U8);
void Bt__LocalDeviceDeInit(void);
U8 Bt__IsLocalDeviceInited(void);

#endif
