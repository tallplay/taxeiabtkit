#include "../HCI/BTCmdEvt_loader.h"
#include "../../TaxeiaConsoleDebug/TaxeiaConsoledebug.h"
#include "Bt__LocalDevice.h"
#include "../../HAL/LocalStorage.h"
#include "../../Core/BtCore.h"
#include <stdio.h>
#include <string.h>
#include "Crypto/Crypto.h"

///////////////////////////////////////////////////////////////////////////////////////
static BT_LOCAL_INFO bt_info;

///////////////////////////////////////////////////////////////////////////////////////
FinishNotifyCallback FinishNotifyCB = 0;
///////////////////////////////////////////////////////////////////////////////////////

static void Bt__GenericCB(U8 CmdOp, U8 EvtOp, U8 *dat);
static void Bt__LocalDeviceInitCB(U8 CmdOp, U8 EvtOp, U8 *dat);
static void Bt__InitResetSetAddressCB(U8 CmdOp, U8 EvtOp, U8 *dat);
extern void MassUart_ChangeBaud(unsigned int baud); //bad style

///////////////////////////////////Public API///////////////////////////////////////////
    
void Bt__GetLocalInfo(BT_LOCAL_INFO **bt_inf)
{
    *bt_inf = &bt_info;
}

void Bt__PreSetCod(U32 cod)
{
    bt_info.COD = cod;
}

void Bt__LocalDeviceCB(FinishNotifyCallback fc) //call back for initialized
{
    FinishNotifyCB = fc;
}

///////////////////////////////////Private Functions/////////////////////////////////////
static void Bt__InitResetCB(U8 CmdOp, U8 EvtOp, U8 *dat)
{
    switch(CmdOp)       
    {
        case HCCI_RESET:
        {
            ASSERT(EvtOp == HCE_COMMAND_COMPLETE);

            if(*dat == HC_STATUS_SUCCESS) //success
            {
                SendCmd(HCCI_READ_BD_ADDR, 0 ,Bt__LocalDeviceInitCB);
            }
        }
    }
}

static void Bt__InitResetSetAddressCB(U8 CmdOp, U8 EvtOp, U8 *dat)
{
    switch(CmdOp)       
    {
        #ifdef MTK_6622_CHANGE_BAUD
        case HCCI_MTK6622_SET_BAUD:
        {
            if(EvtOp == HCE_COMMAND_STATUS)
            {
                MassUart_ChangeBaud(MTK_6622_CHANGE_BAUD);
                if(dat[0] != 0)
                    SendCmd(HCCI_RESET, 0, Bt__InitResetSetAddressCB);

            }
            else
            {
                ASSERT(EvtOp == HCE_COMMAND_COMPLETE);
                SendCmd(HCCI_RESET, 0, Bt__InitResetSetAddressCB);
            }
            break;
        }
        #endif
        case HCCI_RESET:
        {
            ASSERT(EvtOp == HCE_COMMAND_COMPLETE);

            if(*dat == HC_STATUS_SUCCESS) //success
            {
                SendCmd(HCCI_MTK6622_SET_BDADDR, bt_info.BT_ADDR ,Bt__InitResetSetAddressCB);
            }
            break;
        }
        case HCCI_MTK6622_SET_BDADDR:
        {
            ASSERT(EvtOp == HCE_COMMAND_COMPLETE);
            if(dat[0] == HC_STATUS_SUCCESS)
            {
#ifndef POW       
      #define POW 7
#endif            
#ifndef POW_INIT            
      #define POW_INIT 5
#endif
                dat[0] = POW_INIT;
                dat[1] = 0x80;
                dat[2] = 0x00;
                dat[3] = 0x06;
                dat[4] = 0x03;
                dat[5] = POW;
                SendCmd(HCCI_MTK6622_SET_RADIO, dat ,Bt__InitResetSetAddressCB);
            }
            else                 
                ASSERT(0);
            break;
        }
        case HCCI_MTK6622_SET_RADIO:
        {
            ASSERT(EvtOp == HCE_COMMAND_COMPLETE);
            if(dat[0] == HC_STATUS_SUCCESS)
            {
                SendCmd(HCCI_RESET, 0 ,Bt__InitResetCB);
            }
            else                 
                ASSERT(0);
            break;
        }
    }
}


static void Bt__LocalDeviceInitCB(U8 CmdOp, U8 EvtOp, U8 *dat)
{
    switch(CmdOp)       
    {
        case HCCI_READ_BD_ADDR:
        {
            ASSERT(EvtOp == HCE_COMMAND_COMPLETE);
            if(dat[0] == HC_STATUS_SUCCESS)
            {
                 U8 i;

                 for(i = 0; i < 6; i++)
                    bt_info.BT_ADDR[i] = dat[1+i];

                 SendCmd(HCCI_READ_BUFFER_SIZE, 0, Bt__LocalDeviceInitCB);

            }
            else
                ASSERT(0);
        }
        break;
        case HCCI_READ_BUFFER_SIZE:
        {
            ASSERT(EvtOp == HCE_COMMAND_COMPLETE);
            if(dat[0] == HC_STATUS_SUCCESS)
            {
                bt_info.ACLPktLen = dat[1] + (dat[2] << 8);
                bt_info.SCOPktLen = dat[3];
                bt_info.ACLPktNum = dat[4] + (dat[5] << 8);
                bt_info.SCOPktNum = dat[6];

                dat[2] =(ACL_MAX_PACKET_LEN >> 8);
                dat[1] = ACL_MAX_PACKET_LEN & 0xff;

                dat[5] = (ACL_MAX_PACKET_NUM >> 8);
                dat[4] = ACL_MAX_PACKET_NUM & 0xff;

                SendCmd(HCCI_HOST_BUFFER_SIZE, dat + 1, Bt__LocalDeviceInitCB);
            }
            else
                ASSERT(0);
        }
        break;
        case HCCI_HOST_BUFFER_SIZE:
        {
            ASSERT(EvtOp == HCE_COMMAND_COMPLETE);
            if(dat[0] == HC_STATUS_SUCCESS)
            {
                bt_info.isInited = 1;
                if(FinishNotifyCB)
                {
                    FinishNotifyCB(LOCALCB_INITIALIZED); //initialized, you need to use Bt__LocalDeviceCB
                }
            }
            else
                ASSERT(0);
        }
        break;       
    }
}


/*Event handler*/

//to Bt__RemoteDevice
extern void Bt__ConnIncomingRstCB(U8 *dat);
extern void Bt__ConnCmpCB(U8 *dat, U8 *aclnum);
extern void Bt__FlushOccurred(U8 *dat, U8 *aclnum);
extern void Bt__DisConnCmpCB(U8 *dat, U8 *aclnum);
extern void Bt__DataCmpHandlerCB(U8 *dat);

U8 Bt__AclDataSend(void)
{
    if(bt_info.UsedAclNum < bt_info.ACLPktNum)
    {
        bt_info.UsedAclNum++;
        return 0;//success
    }

    return 1;
}

U8 Bt_AclReady(void)
{
    if(BTCoreState() == BUSY || !Bt_AclAvable())
        return 0;
    return 1;
}

U8 Bt_AclAvable(void)
{
//    Taxeia_Report("acl# %d %d\r\n", bt_info.UsedAclNum, bt_info.ACLPktNum);
    return ((bt_info.UsedAclNum + 1) < bt_info.ACLPktNum);
}

extern void Bt__EvtCB(U8 EvtOp, U8 *dat)
{
    U8 i;
    switch(EvtOp)
    {
        case HCE_CONNECT_REQUEST:
            Bt__ConnIncomingRstCB(dat);
            break;
        case HCE_CONNECT_COMPLETE:
            Bt__ConnCmpCB(dat, &bt_info.UsedAclNum);            
            break;
        case HCE_DISCONNECT_COMPLETE:
            Bt__DisConnCmpCB(dat, &bt_info.UsedAclNum);   
            break;
        case HCE_NUM_COMPLETED_PACKETS:
            if(bt_info.UsedAclNum) //aclnum will be reset on connect or disconnect, our design choose
                bt_info.UsedAclNum -= 1;

            ASSERT(bt_info.UsedAclNum <= bt_info.ACLPktNum);
            break;
        case HCE_FLUSH_OCCURRED:
            bt_info.UsedAclNum = 0;
            break;
    }
}

U8 Bt__GetMacAddress(U8 *addr)
{
    addr[0] = 0x66;
    addr[1] = 0x55;
    addr[2] = 0x44;
    addr[3] = 0x33;
    addr[4] = 0x22;
    addr[5] = 0x11;
    
    return 0;
}
    
void Bt__LocalDeviceInit(U8 profile_type)
{
    #ifdef MTK_6622_CHANGE_BAUD
    U8 dat[6];
    #endif
    
    bt_info.isInited = 0;
    bt_info.UsedAclNum = 0;

    Bt__GetMacAddress(bt_info.BT_ADDR);

    #ifdef MTK_6622_CHANGE_BAUD

    dat[0] = 0x00;
    dat[1] = 0x00; //8n1
    
//    *(U32*)(dat + 2) = MTK_6622_CHANGE_BAUD;
    dat[2] = MTK_6622_CHANGE_BAUD & 0xff;
    dat[3] = (MTK_6622_CHANGE_BAUD >> 8) & 0xff;
    dat[4] = (MTK_6622_CHANGE_BAUD >> 16) & 0xff;
    dat[5] = (MTK_6622_CHANGE_BAUD >> 24) & 0xff;
    SendCmd(HCCI_MTK6622_SET_BAUD, dat, Bt__InitResetSetAddressCB); //always try to set address, because mtk is volatile bdaddr
    
    #else

    SendCmd(HCCI_RESET, 0, Bt__InitResetSetAddressCB); //always try to set address, because mtk is volatile bdaddr
    #endif
}

U8 Bt__IsLocalDeviceInited(void)
{
    return (U8)bt_info.isInited;
}

static void Bt__GenericCB(U8 CmdOp, U8 EvtOp, U8 *dat)
{
}



