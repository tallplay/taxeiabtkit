#ifndef __BT_LLCAP_H__
#define __BT_LLCAP_H__
#include "BtType.h"

///////////////////////////////////Public API///////////////////////////////////////////
typedef U8 (*PsmHandler)(U16 aclHandle, U16 cid, U16 len , U8 *dat);
U8 RegisterPsmHandler(U16 psm, PsmHandler ph);
U8 DeregisterPsmHandler(U16 psm);

///////////////////////////////////Private Functions////////////////////////////////////
void Bt__LLCAPInit(void);
void Bt__LLCAPDeInit(void);

#ifdef TAXEIA_L2_DEBUG
//#define Taxeia_L2_Report(...)  do { Taxeia_Report(##__VA_ARGS__); } while(0)
#define Taxeia_L2_Report Taxeia_Report
#else
#define Taxeia_L2_Report(str, ...)
#endif

#endif
