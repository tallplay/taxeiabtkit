#ifndef __BT_LLCAP_CHANNEL_HH_
#define __BT_LLCAP_CHANNEL_HH_

typedef enum
{
    LLCAP_STATE_CLOSED, //channel not connected
    LLCAP_STATE_WAIT_CONNECT, //a connection request has been received, but only a connection response with indication ¡§pending¡¨ can be sent.
    LLCAP_STATE_WAIT_CONNECT_RSP, //a connectionrequest has been sent, pending a positive connect response

    /////////////////////////config state///////////////////////////////////////////////////////
    LLCAP_SUBSTATE_WAIT_CONFIG,
    LLCAP_SUBSTATE_WAIT_SEND_CONFIG,
    LLCAP_SUBSTATE_WAIT_CONFIG_REQ_RSP,
    LLCAP_SUBSTATE_WAIT_CONFIG_RSP, 
    LLCAP_SUBSTATE_WAIT_CONFIG_REQ,    
    ///Extended Flow Specification related
    LLCAP_SUBSTATE_WAIT_IND_FINAL_RSP,    
    LLCAP_SUBSTATE_WAIT_FINAL_RSP,    
    LLCAP_SUBSTATE_WAIT_CONTROL_IND,    
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    LLCAP_STATE_OPEN, //user data transfer state
    LLCAP_STATE_WAIT_DISCONNECT, //a disconnect request has been sent, pending a dis-connect response.
    
    ////AMP(HS) controller
    LLCAP_STATE_WAIT_CREATE, //a channel creation request has been received, but only a response with indication ¡§pending¡¨ can be sent. Thisstate is similar to WAIT_CONNECT_RSP.
    LLCAP_STATE_WAIT_CREATE_RSP, //a channel creation request has been sent, pending a channel creation response. This state is similar to WAIT_CONNECT_RSP.
    LLCAP_STATE_WAIT_MOVE, //a request to move the current channel to another Controller has been received, but only a response with indication ¡§pending¡¨ can be sent.
    LLCAP_STATE_WAIT_MOVE_RSP, //a request to move a channel to another Controller has been sent, pending a move response
    LLCAP_STATE_WAIT_MOVE_CONFIRM, //a response to the move channel request has been sent, waiting for a confirmation of the move operation by the initiator side
    LLCAP_STATE_WAIT_CONFIRM_RSP, //a move channel confirm has been sent, waiting for a move channel confirm response
}LLCAP_STATE;

typedef enum
{
    L2_ALLOC_SUCCESS,
    L2_ALLOC_PSM_NOT_EXIST,
    L2_ALLOC_NO_RESOURCE
}L2_ALLOC_STATUS;

#endif
