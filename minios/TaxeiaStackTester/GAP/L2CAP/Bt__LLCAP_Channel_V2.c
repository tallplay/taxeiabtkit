#include "BtConfig.h"
#include "Bt__LLCAP_V2.h"
#include "Bt__LLCAP_Channel_V2.h"
#include "../LOCAL_DEV/Bt__LocalDevice_V2.h"
#include "../LL/Bt__RemoteDevice_V2.h"
#include "../HCI/BtCmdEvt_V2.h"
#include "../../TaxeiaConsoleDebug/TaxeiaConsoledebug.h"
#include "../../TaxeiaHciLog/TaxeiaHciLog.h"

#ifdef TAXEIA_L2_DEBUG
       char *state_str[] = 
        {
            "LLCAP_STATE_CLOSED",
            "LLCAP_STATE_WAIT_CONNECT",
            "LLCAP_STATE_WAIT_CONNECT_RSP",
            "LLCAP_SUBSTATE_WAIT_CONFIG",
            "LLCAP_SUBSTATE_WAIT_SEND_CONFIG",
            "LLCAP_SUBSTATE_WAIT_CONFIG_REQ_RSP",
            "LLCAP_SUBSTATE_WAIT_CONFIG_RSP", 
            "LLCAP_SUBSTATE_WAIT_CONFIG_REQ",    
            "LLCAP_SUBSTATE_WAIT_IND_FINAL_RSP",    
            "LLCAP_SUBSTATE_WAIT_FINAL_RSP",    
            "LLCAP_SUBSTATE_WAIT_CONTROL_IND",    
            "LLCAP_STATE_OPEN",
            "LLCAP_STATE_WAIT_DISCONNECT",
            "LLCAP_STATE_WAIT_CREATE",
            "LLCAP_STATE_WAIT_CREATE_RSP",
            "LLCAP_STATE_WAIT_MOVE",
            "LLCAP_STATE_WAIT_MOVE_RSP",
            "LLCAP_STATE_WAIT_MOVE_CONFIRM",
            "LLCAP_STATE_WAIT_CONFIRM_RSP"
        };
const char *L2CAPSignalOpcodeStr(U8 Opcode);
#endif

typedef struct
{
    U16 AllowPsm;
    U16 AllocatedCID;
    U16 TargetCID;
    PsmHandler PsmHandlerCB;
    U16 AclHandle;
    U16 outMtu;
    LLCAP_STATE oldstate;
    LLCAP_STATE newstate;
    U16 ContinueDataIdx;
    U16 ContinueDataLen;
} LLCAP_CHANEL;


static LLCAP_CHANEL l2cap_channels[PSM_SUPPORT_NUM];
static void L2ChannelStateAssign(U8 op, U8 idx, LLCAP_STATE lls);
extern void LLSignalCnfReq(U16 aclHandle, U8 id, U16 dcid, U8 len, U8 *cnf);
U8 L2ChannelSearchIdxByCid(U16 cid);
U8 GetLLDataBuffer(U16 idx);
void SendLLDataBuffer(U16 acl_handle, U16 cid, U8 *dat, U16 len);
U8 LLDataAvaliable(void);
U8 Bt__AclDataSend(void);
U16 L2ChannelSearchAcidByTcid(U16 cid);

///////////////////////////////////Public API///////////////////////////////////////////
U8 RegisterPsmHandler(U16 psm, PsmHandler ph)
{
    U8 i;
    for(i = 0; i < PSM_SUPPORT_NUM; i++)
    {
        if(l2cap_channels[i].AllowPsm == 0)
            break;
    }

    if(i < PSM_SUPPORT_NUM)
    {
        l2cap_channels[i].AllowPsm = psm;
        l2cap_channels[i].PsmHandlerCB = ph;
        return 1;
    }

    return 0;
}

U8 DeregisterPsmHandler(U16 psm)
{
    U8 i;
    for(i = 0; i < PSM_SUPPORT_NUM; i++)
    {
        if(l2cap_channels[i].AllowPsm == psm)
            break;
    }

    if(i < PSM_SUPPORT_NUM)
    { 
        l2cap_channels[i].AllowPsm = 0;
        l2cap_channels[i].PsmHandlerCB = 0;
        return 1;
    }

    return 0;
}

///////////////////////////////////Private Functions////////////////////////////////////
void L2ChannelSendData(U16 aclHandle, U16 cid, U16 len, U8 *dat)
{

    BT_LOCAL_INFO *bt_info;

    Bt__GetLocalInfo(&bt_info);

    ASSERT(bt_info->ACLPktLen >= len+9);

    SendLLDataBuffer(aclHandle, cid, dat, len);
}

#if 0
U16 L2ChannelSendDataCheckAclLen(U16 aclHandle, U16 cid, U16 len, U8 *dat)
{
    U16 i;
    U8 idx;
    U16 reallen;
    BT_LOCAL_INFO *bt_info;

    Bt__GetLocalInfo(&bt_info);
    
    reallen = 0;
    if((idx = L2ChannelSearchIdxByCid(L2ChannelSearchAcidByTcid(cid))) != PSM_SUPPORT_NUM)
    {
        if(bt_info->ACLPktLen >= len)
        {
            reallen = len;
        }
        else
        {
            reallen = bt_info->ACLPktLen;
            l2cap_channels[idx].ContinueDataIdx = reallen;
            l2cap_channels[idx].ContinueDataLen = len - reallen;
        }

        if(dat) //data maybe preloaded
        {
            /*copy all data into buffer for memo*/
            /*if continue acl packet need, will be move from back to front position*/
            for(i = 0; i < len; i++)
            {
                FillLLDataBuffer(i, dat[i]);
            }
        }

        SendLLDataBuffer(aclHandle, cid, len, reallen);
    }
    
    return reallen;
}
#endif

#if 0
//we have issue that more than 2 continue acl data packet
U16 L2ChannelSendDataContinueCheckMtu(U8 idx)
{
    U16 reallen;
    U16 i;

    /*will be move from back to front position*/
    for(i = 0; i < l2cap_channels[idx].ContinueDataLen; i++)
    {
        MoveTxDataBuffer(i + 4, i + 8 + l2cap_channels[idx].ContinueDataIdx);
    }

    reallen = 0;
    
    if(l2cap_channels[idx].outMtu >= l2cap_channels[idx].ContinueDataLen)
    {
        reallen = l2cap_channels[idx].ContinueDataLen;
        l2cap_channels[idx].ContinueDataIdx = 0;
        l2cap_channels[idx].ContinueDataLen = 0;
    }
    else
    {
        reallen = l2cap_channels[idx].outMtu;
        l2cap_channels[idx].ContinueDataIdx = reallen;
        l2cap_channels[idx].ContinueDataLen -= reallen;
    }

    //acl handle
    FillTxDataBuffer(0, l2cap_channels[idx].AclHandle & 0xff);
    FillTxDataBuffer(1, ((l2cap_channels[idx].AclHandle & 0xf) >> 8) | 0x10);

    //acllen
    FillTxDataBuffer(2, (reallen) & 0xff);
    FillTxDataBuffer(3, (reallen) >> 8);

    ASSERT(!Bt__AclDataSend());

    ASSERT(SendData(reallen+4));
  
    return reallen;
}
#endif

U8 L2ChannelDataHandler(U16 aclHandle, U16 cid, U8 *dat, U16 len)
{
    U8 i;
    for(i = 0; i < PSM_SUPPORT_NUM; i++)
    {
        if(l2cap_channels[i].AllocatedCID == cid)
        {
            if(!l2cap_channels[i].PsmHandlerCB(aclHandle, cid, len, dat))
                return i;
        }

    }
    return PSM_SUPPORT_NUM;
}

U8 L2ChannelSearchIdxByPsm(U16 psm)
{
    U8 i;
    for(i = 0; i < PSM_SUPPORT_NUM; i++)
    {
        if(l2cap_channels[i].AllowPsm == psm)
            return i;
    }
    return PSM_SUPPORT_NUM;
}

void L2ChannelSearchTCidHdlByPsm(U16 psm, U16 *tcid, U16 *acl)
{
    U8 i;
    if((i = L2ChannelSearchIdxByPsm(psm)) != PSM_SUPPORT_NUM)
    {
        *tcid = l2cap_channels[i].TargetCID;
        *acl = l2cap_channels[i].AclHandle;
        return ;
    }

    *tcid = 0;
    *acl = 0;
}


U8 L2ChannelSearchIdxByCid(U16 cid)
{
    U8 i;
    for(i = 0; i < PSM_SUPPORT_NUM; i++)
    {
        if(l2cap_channels[i].AllocatedCID == cid)
            return i;
    }
    return PSM_SUPPORT_NUM;
}

U16 L2ChannelSearchTcidByCid(U16 cid)
{
    U8 i;
    for(i = 0; i < PSM_SUPPORT_NUM; i++)
    {
        if(l2cap_channels[i].AllocatedCID == cid)
            return l2cap_channels[i].TargetCID;
    }
    
    return 0;
}

U16 L2ChannelSearchAcidByTcid(U16 cid)
{
    U8 i;
    for(i = 0; i < PSM_SUPPORT_NUM; i++)
    {
        if(l2cap_channels[i].TargetCID == cid)
            return l2cap_channels[i].AllocatedCID;
    }
    
    return 0;
}

U16 L2ChannelSearchAcidByPsm(U16 psm)
{
    U8 i;
    if((i = L2ChannelSearchIdxByPsm(psm)) != PSM_SUPPORT_NUM)
    {
        return l2cap_channels[i].AllocatedCID;
    }

    return 0;
}

void L2ChannelDeallocateCid(U16 cid)
{
    U8 i;
    for(i = 0; i < PSM_SUPPORT_NUM; i++)
    {
        if(l2cap_channels[i].AllocatedCID == cid)
        {
            //printf("free psm = %d\n", l2cap_channels[i].AllowPsm);
            l2cap_channels[i].AllocatedCID = 0;
            l2cap_channels[i].TargetCID = 0;
            l2cap_channels[i].AclHandle = 0;
            L2ChannelStateAssign(0, i, LLCAP_STATE_CLOSED);
            l2cap_channels[i].PsmHandlerCB(0, 0, 0, 0); //inform to update channel status
            break;
        }
    }
}

void L2ChannelDeallocateCids(U16 tcid, U16 cid)
{
    U8 i;
    for(i = 0; i < PSM_SUPPORT_NUM; i++)
    {
        if(l2cap_channels[i].AllocatedCID == cid && l2cap_channels[i].TargetCID == tcid)
        {
         //   printf("free psm = %d\n", l2cap_channels[i].AllowPsm);
            l2cap_channels[i].AllocatedCID = 0;
            l2cap_channels[i].TargetCID = 0;
            l2cap_channels[i].AclHandle = 0;
            L2ChannelStateAssign(0, i, LLCAP_STATE_CLOSED);
            l2cap_channels[i].PsmHandlerCB(0, 0, 0, 0); //inform to update channel status
            break;
        }
    }
}

void L2ChannelDeallocateAclHdl(U16 hdl)
{
    U8 i;
    for(i = 0; i < PSM_SUPPORT_NUM; i++)
    {
        if(l2cap_channels[i].AclHandle == hdl)
        {
           // printf("free psm = %d\n", l2cap_channels[i].AllowPsm);
            l2cap_channels[i].AllocatedCID = 0;
            l2cap_channels[i].TargetCID = 0;
            l2cap_channels[i].AclHandle = 0;
            L2ChannelStateAssign(0, i, LLCAP_STATE_CLOSED);
            l2cap_channels[i].PsmHandlerCB(0, 0, 0, 0); //inform to update channel status
        }
    }
}

void L2ChannelSendConfig(U8 i)
{
    U8 config[4];
    
    config[0] = 0x01; //mtu
    config[1] = 0x02; //length
    config[2] = LL_MTU & 0xff;
    config[3] = LL_MTU >> 8;
    
    LLSignalCnfReq(l2cap_channels[i].AclHandle, 0x88, l2cap_channels[i].TargetCID, 4, config);
}

void L2ChannelPostAllocate(U16 tcid, U16 scid, U16 aclHandle)
{
    U8 cididx;

    cididx = L2ChannelSearchIdxByCid(scid);
    if(cididx != PSM_SUPPORT_NUM) //scan registered psm
    {
        l2cap_channels[cididx].TargetCID = tcid;
    }
}

U16 LLSearchFreeCid(void)
{
    U16 scid = 0x0040;
    while(1)
    {
        if(L2ChannelSearchIdxByCid(scid) == PSM_SUPPORT_NUM)
        {
            return scid;
        }
        
        scid ++;
        if(!scid)
            scid = 0x0040;
    }
}

L2_ALLOC_STATUS L2ChannelAllocate(U16 psm, U16 target_cid, U16 my_cid, U16 aclHandle)
{
    U8 psmidx;

    //printf("alloc psm = %d\n", psm);
    psmidx = L2ChannelSearchIdxByPsm(psm);
    if(psmidx != PSM_SUPPORT_NUM) //scan registered psm
    {
        if(l2cap_channels[psmidx].newstate == LLCAP_STATE_CLOSED && l2cap_channels[psmidx].oldstate == LLCAP_STATE_CLOSED)
        {
            if(my_cid == 0x00)
            {
                my_cid = LLSearchFreeCid();
            }
            
            l2cap_channels[psmidx].AllocatedCID = my_cid;
            l2cap_channels[psmidx].TargetCID = target_cid;
            l2cap_channels[psmidx].AclHandle = aclHandle;
            return L2_ALLOC_SUCCESS;
        }
        else  //no resource
        {
            return L2_ALLOC_NO_RESOURCE;
        }
    }
    else //psm not support
    {
        return L2_ALLOC_PSM_NOT_EXIST;
    }
}

//op 0: back to close state 1: candidate state 2: confirm candidate
void L2ChannelStateAssign(U8 op, U8 idx, LLCAP_STATE lls)
{
    if(op == 0)
    {
        Taxeia_L2_Report("[L2CH%1d:0x%x] (%s => %s)\n", idx, l2cap_channels[idx].AllocatedCID, state_str[l2cap_channels[idx].oldstate], state_str[LLCAP_STATE_CLOSED]);
        l2cap_channels[idx].newstate = LLCAP_STATE_CLOSED;
        l2cap_channels[idx].oldstate = LLCAP_STATE_CLOSED;        
    }
    else if(op == 1) //candidate
    {
        l2cap_channels[idx].newstate = lls;

        Taxeia_L2_Report("[L2CH%1d:0x%x] (%s >>> %s)\n", idx, l2cap_channels[idx].AllocatedCID, state_str[l2cap_channels[idx].oldstate], state_str[l2cap_channels[idx].newstate]);
    }
    else if(op == 2)
    {
        Taxeia_L2_Report("[L2CH%1d:0x%x] (%s => %s)\n", idx, l2cap_channels[idx].AllocatedCID, state_str[l2cap_channels[idx].oldstate], state_str[l2cap_channels[idx].newstate]);
        l2cap_channels[idx].oldstate = l2cap_channels[idx].newstate;
    }
}

U16 L2ChannelGetOutMtu(U16 cid)
{
    U8 i;
    if((i = L2ChannelSearchIdxByCid(cid)) != PSM_SUPPORT_NUM)
    {
        return l2cap_channels[i].outMtu;
    }
	ASSERT(i != PSM_SUPPORT_NUM);
	return 0;
}

void L2ChannelStateConfigOutMtu(U16 cid, U16 mtu)
{
    U8 i;
    if((i = L2ChannelSearchIdxByCid(cid)) != PSM_SUPPORT_NUM)
    {
        l2cap_channels[i].outMtu = mtu;
    }
}

void L2ChannelStateTransitReq(U16 cid, U8 event)
{
    U8 i;

    if((i = L2ChannelSearchIdxByCid(cid)) != PSM_SUPPORT_NUM)
    {
        if(l2cap_channels[i].oldstate != l2cap_channels[i].newstate) //wait signal acl complete
        {
            switch(l2cap_channels[i].newstate)
            {
                case LLCAP_SUBSTATE_WAIT_CONFIG_RSP:
                    if(event == LLC_CONFIG_RSP) //very fast config_rsp
                    {
                        Taxeia_L2_Report("**************L2 not gracefully!!\r\n");
                        
                        //candidate
                        L2ChannelStateAssign(1, i, LLCAP_STATE_OPEN);
                        //confirm
                        L2ChannelStateAssign(2, i, LLCAP_STATE_OPEN);
                        
                        l2cap_channels[i].PsmHandlerCB(0, 0, 0, 0); //inform to update channel status
                        return;
                    }
                case LLCAP_SUBSTATE_WAIT_CONFIG:
                    if(event == LLC_CONFIG_REQ) //always return succes now
                    {
                        Taxeia_L2_Report("**************L2 not gracefully!!\r\n");
                        L2ChannelStateAssign(1, i, LLCAP_SUBSTATE_WAIT_SEND_CONFIG);
                        return;
                    }
                    break;
            }

            Taxeia_L2_Report("**************no**\r\n");

            return;
        }
        
        switch(l2cap_channels[i].oldstate)
        {
            case LLCAP_STATE_CLOSED:
                if(event == LLC_CONN_REQ) //incoming
                {
                    L2ChannelStateAssign(1, i, LLCAP_SUBSTATE_WAIT_CONFIG);
                }
                else if(event == LLC_OPEN_CHANNEL) //outgoing
                {
                    L2ChannelStateAssign(1, i, LLCAP_STATE_WAIT_CONNECT_RSP);
                }
                break;
            case LLCAP_STATE_WAIT_CONNECT:
                break;
            case LLCAP_STATE_WAIT_CONNECT_RSP:
                if(event == LLC_CONN_RSP)
                {
                    //candidate
                    L2ChannelStateAssign(1, i, LLCAP_SUBSTATE_WAIT_CONFIG);

                    //if avaliable then send config directly, else wait for current data ava(info request, etc...)
                    if(LLDataAvaliable())
                    {
                        //confirm
                        L2ChannelStateAssign(2, i, LLCAP_SUBSTATE_WAIT_CONFIG);

                        //we lauch config first, because window 7 will wait forever
                        L2ChannelSendConfig(i);
                        L2ChannelStateTransitReq(l2cap_channels[i].AllocatedCID, LLC_CONFIG_CHANNEL);
                    }
                    
                }
                break;
                //config state
            case LLCAP_SUBSTATE_WAIT_CONFIG:
                if(event == LLC_CONFIG_REQ) //always return succes now
                {
                    L2ChannelStateAssign(1, i, LLCAP_SUBSTATE_WAIT_SEND_CONFIG);
                }
                else if(event == LLC_CONFIG_CHANNEL)
                {
                    L2ChannelStateAssign(1, i, LLCAP_SUBSTATE_WAIT_CONFIG_REQ_RSP);
                    L2ChannelStateAssign(2, i, LLCAP_SUBSTATE_WAIT_CONFIG_REQ_RSP); //to prevent fast config req
                }
                break;
            case LLCAP_SUBSTATE_WAIT_SEND_CONFIG:
                if(event == LLC_CONFIG_CHANNEL)
                {
                    L2ChannelStateAssign(1, i, LLCAP_SUBSTATE_WAIT_CONFIG_RSP);
                }
                break;
            case LLCAP_SUBSTATE_WAIT_CONFIG_REQ_RSP:
                if(event == LLC_CONFIG_RSP)
                {
                    L2ChannelStateAssign(1, i, LLCAP_SUBSTATE_WAIT_CONFIG_REQ);
                    L2ChannelStateAssign(2, i, LLCAP_SUBSTATE_WAIT_CONFIG_REQ);
                }
                else if(event == LLC_CONFIG_REQ)
                {
                    L2ChannelStateAssign(1, i, LLCAP_SUBSTATE_WAIT_CONFIG_RSP);
                    L2ChannelStateAssign(2, i, LLCAP_SUBSTATE_WAIT_CONFIG_RSP); //to prevent fast config rsp
                }
                break;
            case LLCAP_SUBSTATE_WAIT_CONFIG_RSP:
                if(event == LLC_CONFIG_RSP)
                {
                    //candidate
                    L2ChannelStateAssign(1, i, LLCAP_STATE_OPEN);
                    //confirm
                    L2ChannelStateAssign(2, i, LLCAP_STATE_OPEN);
                    
                    l2cap_channels[i].PsmHandlerCB(0, 0, 0, 0); //inform to update channel status
                    break;
                }
            case LLCAP_SUBSTATE_WAIT_CONFIG_REQ:
                if(event == LLC_CONFIG_REQ) //always return succes now
                {
                    L2ChannelStateAssign(1, i, LLCAP_STATE_OPEN);
                }
                break;

                ///Extended Flow Specification related/////////
            case LLCAP_SUBSTATE_WAIT_IND_FINAL_RSP:      //
                break;                                   //
            case LLCAP_SUBSTATE_WAIT_FINAL_RSP:          //
                break;                                   //
            case LLCAP_SUBSTATE_WAIT_CONTROL_IND:        //
                break;                                   //
                ///////////////////////////////////////////////

            case LLCAP_STATE_OPEN:
                break;
            case LLCAP_STATE_WAIT_DISCONNECT:
                break;

                ////AMP(HS) controller////////////////////////
            case LLCAP_STATE_WAIT_CREATE:               //
                break;                                  //
            case LLCAP_STATE_WAIT_CREATE_RSP:           //
                break;                                  //
            case LLCAP_STATE_WAIT_MOVE:                 //
                break;                                  //
            case LLCAP_STATE_WAIT_MOVE_RSP:             //
                break;                                  //
            case LLCAP_STATE_WAIT_MOVE_CONFIRM:         //
                break;                                  //
            case LLCAP_STATE_WAIT_CONFIRM_RSP:          //
                break;                                  //
                //////////////////////////////////////////////
        }
    }
}

void L2ChannelTxComplete(U16 aclHandle, U8 is_timeout, U8 *dat)
{
    U8 i;
    for(i = 0; i < PSM_SUPPORT_NUM; i++)
    {
        if(l2cap_channels[i].AclHandle == aclHandle)
        {
            Taxeia_L2_Report("[L2CH%1d:0x%x] == [aclhdl:0x%x]\n", i, l2cap_channels[i].AllocatedCID, aclHandle);
            switch(l2cap_channels[i].newstate)
            {
                case LLCAP_STATE_CLOSED:
                    break;
                case LLCAP_STATE_WAIT_CONNECT:
                    break;
                case LLCAP_STATE_WAIT_CONNECT_RSP:
                    if(!is_timeout && l2cap_channels[i].oldstate != l2cap_channels[i].newstate)
                        L2ChannelStateAssign(2, i, LLCAP_STATE_WAIT_CONNECT_RSP);
                    break;                
                //config state
                case LLCAP_SUBSTATE_WAIT_CONFIG:
                {
                    if(!is_timeout && l2cap_channels[i].oldstate != l2cap_channels[i].newstate)
                    {
                        L2ChannelStateAssign(2, i, LLCAP_SUBSTATE_WAIT_CONFIG);
                        if(!l2cap_channels[i].outMtu) //we lunched connection
                        {
                            L2ChannelSendConfig(i);
                            L2ChannelStateTransitReq(l2cap_channels[i].AllocatedCID, LLC_CONFIG_CHANNEL);
                        }
                    }
                    break;
                }
                case LLCAP_SUBSTATE_WAIT_SEND_CONFIG:
                {
                    if(!is_timeout && l2cap_channels[i].oldstate != l2cap_channels[i].newstate)
                    {
                        L2ChannelStateAssign(2, i, LLCAP_SUBSTATE_WAIT_SEND_CONFIG);
                        L2ChannelSendConfig(i);
                        L2ChannelStateTransitReq(l2cap_channels[i].AllocatedCID, LLC_CONFIG_CHANNEL);
                    }
                    else
                    {
                        ASSERT(0);
                    }
                    break;
                }
                case LLCAP_SUBSTATE_WAIT_CONFIG_REQ_RSP:
                    if(!is_timeout && l2cap_channels[i].oldstate != l2cap_channels[i].newstate)
                    {
                        L2ChannelStateAssign(2, i, LLCAP_SUBSTATE_WAIT_CONFIG_REQ_RSP);
                    }
                    break;
                case LLCAP_SUBSTATE_WAIT_CONFIG_RSP:
                    if(!is_timeout && l2cap_channels[i].oldstate != l2cap_channels[i].newstate)
                        L2ChannelStateAssign(2, i, LLCAP_SUBSTATE_WAIT_CONFIG_RSP);
                    break;
                case LLCAP_SUBSTATE_WAIT_CONFIG_REQ:
                    break;

                    ///Extended Flow Specification related/////////
                case LLCAP_SUBSTATE_WAIT_IND_FINAL_RSP:      //
                    break;                                   //
                case LLCAP_SUBSTATE_WAIT_FINAL_RSP:          //
                    break;                                   //
                case LLCAP_SUBSTATE_WAIT_CONTROL_IND:        //
                    break;                                   //
                    ///////////////////////////////////////////////

                case LLCAP_STATE_OPEN:
                {
                    if(!is_timeout && l2cap_channels[i].oldstate != l2cap_channels[i].newstate)
                    {
                        L2ChannelStateAssign(2, i, LLCAP_STATE_OPEN);
                        l2cap_channels[i].PsmHandlerCB(0, 0, 0, 0); //inform to update channel status
                        return; //need to return, because xxx
                    }
                    //we handle continue here, new and old state must be opened
                    else if(l2cap_channels[i].ContinueDataIdx != 0)
                    {
                        ASSERT(0);
                        //L2ChannelSendDataContinueCheckMtu(i);
                    }
                    else  //normal tx complete inform
                    {
                         l2cap_channels[i].PsmHandlerCB(0, 1, 0, 0); //inform to update channel status
                    }
                    break;
                }
                case LLCAP_STATE_WAIT_DISCONNECT:
                    break;

                    ////AMP(HS) controller////////////////////////
                case LLCAP_STATE_WAIT_CREATE:               //
                    break;                                  //
                case LLCAP_STATE_WAIT_CREATE_RSP:           //
                    break;                                  //
                case LLCAP_STATE_WAIT_MOVE:                 //
                    break;                                  //
                case LLCAP_STATE_WAIT_MOVE_RSP:             //
                    break;                                  //
                case LLCAP_STATE_WAIT_MOVE_CONFIRM:         //
                    break;                                  //
                case LLCAP_STATE_WAIT_CONFIRM_RSP:          //
                    break;                                  //
                    //////////////////////////////////////////////
            }
        }
    }

}

void L2ChannelDeregisterAll()
{
    U8 i;

    for(i = 0; i < PSM_SUPPORT_NUM; i++)
    {
        l2cap_channels[i].AllowPsm = 0;
        l2cap_channels[i].AllocatedCID = 0;
        l2cap_channels[i].TargetCID = 0;
        l2cap_channels[i].PsmHandlerCB = 0;
        l2cap_channels[i].AclHandle = 0;
        l2cap_channels[i].outMtu = 0;
        l2cap_channels[i].ContinueDataIdx = 0;
        l2cap_channels[i].ContinueDataLen = 0;
        L2ChannelStateAssign(0, i, LLCAP_STATE_CLOSED);
    }
}

