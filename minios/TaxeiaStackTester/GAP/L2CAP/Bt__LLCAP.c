#include "../HCI/BTCmdEvt.h"
#include "../../TaxeiaConsoleDebug/TaxeiaConsoledebug.h"
#include "../LOCAL_DEV/Bt__LocalDevice.h"
#include "../LL/Bt__RemoteDevice.h"
#include "Bt__LLCAP.h"
#include "BtConfig.h"
#include "Bt__LLCAP_Channel.h"
#include "../../TaxeiaHciLog/TaxeiaHciLog.h"

///////////////////////////////////Public API///////////////////////////////////////////

///////////////////////////////////Private Functions////////////////////////////////////
U8 L2ChannelSearchIdxByPsm(U16 psm);
U8 L2ChannelSearchIdxByCid(U16 cid);
void L2ChannelDeallocateCid(U16 cid);
void L2ChannelDeallocateCids(U16 tcid, U16 cid);
L2_ALLOC_STATUS L2ChannelAllocate(U16 psm, U16 target_cid, U16 my_cid, U16 aclHandle);
void L2ChannelStateTransitReq(U16 cid, U8 event);
void L2ChannelTxComplete(U16 aclHandle, U8 is_timeout, U8 *dat);
void L2ChannelDeregisterAll(void);
U8 L2ChannelGetStateTransiting(LLCAP_STATE *state, U16 *cid);
U8 L2ChannelDataHandler(U16 aclHandle, U16 cid, U8 *dat, U16 len);
void L2ChannelStateConfigOutMtu(U16 cid, U16 mtu);
void L2ChannelDeallocateAclHdl(U16 hdl);
void RegisterLLDataHandler(LLDataHandler lldh);
void FillLLDataBuffer(U16 idx, U8 dat); 
void MoveLLDataBuffer(U16 tgt, U16 src);
void SendLLDataBuffer(U16 acl_handle, U16 cid, U16 lllen, U16 sendlen);
U8 LLDataAvaliable(void);
U16 L2ChannelSearchTcidByCid(U16 cid);
U16 L2ChannelSearchAcidByTcid(U16 cid);
U16 L2ChannelSearchAcidByPsm(U16 psm);
void L2ChannelPostAllocate(U16 tcid, U16 scid, U16 aclHandle);
extern void Bt__TestCallBack(U16 len, U8 *dat);

U8 LLSignalIndentifierGenerate()
{
    static U8 id = 1;
    if(id == 0) id++;
    return id++;
}

#if (PROFILE_TYPE == PROFILE_TYPE_NOTHING)
#else

void LLSignalCmdRej(U16 aclHandle, U8 id, U16 resean, U8 len, U8 *dat)
{
    U8 i;
    //code
    FillLLDataBuffer(0, LLC_COMMAND_REJ); //connect response
    //id
    FillLLDataBuffer(1, id); //identifier

    //length
    FillLLDataBuffer(2, 2+len);
    FillLLDataBuffer(3, 0);

    //resean
    FillLLDataBuffer(4, resean & 0xff);
    FillLLDataBuffer(5, resean >> 8);
    for(i = 0; i < len; i++)
        FillLLDataBuffer(i+6, dat[i]);

    SendLLDataBuffer(aclHandle, 0x0001, 6+len, 6+len);

    Taxeia_L2_Report("{=>%s}\n", L2CAPSignalOpcodeStr(LLC_COMMAND_REJ));
}


void LLSignalConReq(U16 aclHandle, U16 psm)
{
    L2_ALLOC_STATUS l2sts;
    U16 scid;
    l2sts = L2ChannelAllocate(psm, 0/*further assign*/, 0/*assign inside*/, aclHandle);
    if(l2sts != L2_ALLOC_SUCCESS)
        return;

    scid = L2ChannelSearchAcidByPsm(psm);
    //code
    FillLLDataBuffer(0, LLC_CONN_REQ);
    //id
    FillLLDataBuffer(1, LLSignalIndentifierGenerate());
    //length
    FillLLDataBuffer(2, 4);
    FillLLDataBuffer(3, 0);
    //psm
    FillLLDataBuffer(4, psm & 0xff);
    FillLLDataBuffer(5, psm >> 8);

    //scid
    FillLLDataBuffer(6, scid & 0xff);
    FillLLDataBuffer(7, scid >> 8);

    SendLLDataBuffer(aclHandle, 0x0001, 8, 8);

    Taxeia_L2_Report("{=>%s}\n", L2CAPSignalOpcodeStr(LLC_CONN_REQ));

    L2ChannelStateTransitReq(scid, LLC_OPEN_CHANNEL);

    scid ++;
    if(!scid)
        scid = 0x0040;

}

void LLSignalConRsp(U16 aclHandle, U8 id, U16 dcid, U16 scid, U16 result, U16 sts)
{
    //code
    FillLLDataBuffer(0, LLC_CONN_RSP); //connect response
    //id
    FillLLDataBuffer(1, id); //identifier

    //length
    FillLLDataBuffer(2, 8);
    FillLLDataBuffer(3, 0);

    //dest cid
    FillLLDataBuffer(4, dcid & 0xff);
    FillLLDataBuffer(5, dcid >> 8);

    //source cid
    FillLLDataBuffer(6, scid & 0xff);
    FillLLDataBuffer(7, scid >> 8);

    //result
    FillLLDataBuffer(8, result & 0xff);
    FillLLDataBuffer(9, result >> 8);

    //status
    FillLLDataBuffer(10, sts & 0xff);
    FillLLDataBuffer(11, sts >> 8);

    SendLLDataBuffer(aclHandle, 0x0001, 12, 12);

    Taxeia_L2_Report("{=>%s}\n", L2CAPSignalOpcodeStr(LLC_CONN_RSP));
}

void LLSignalDisConReq(U16 aclHandle, U8 id, U16 tcid, U16 scid)
{
    //code
    FillLLDataBuffer(0, LLC_DISC_REQ); //disconnect response

    //id
    FillLLDataBuffer(1, LLSignalIndentifierGenerate()); //identifier

    //length
    FillLLDataBuffer(2, 4);
    FillLLDataBuffer(3, 0);

    //dest cid
    FillLLDataBuffer(4, tcid & 0xff);
    FillLLDataBuffer(5, tcid >> 8);

    //source cid
    FillLLDataBuffer(6, scid & 0xff);
    FillLLDataBuffer(7, scid >> 8);

    SendLLDataBuffer(aclHandle, 0x0001, 8, 8);

    Taxeia_L2_Report("{=>%s}\n", L2CAPSignalOpcodeStr(LLC_DISC_REQ));
}

void LLSignalDisConRsp(U16 aclHandle, U8 id, U16 dcid, U16 scid)
{
    //code
    FillLLDataBuffer(0, LLC_DISC_RSP); //disconnect response

    //id
    FillLLDataBuffer(1, id); //identifier

    //length
    FillLLDataBuffer(2, 4);
    FillLLDataBuffer(3, 0);

    //dest cid
    FillLLDataBuffer(4, dcid & 0xff);
    FillLLDataBuffer(5, dcid >> 8);

    //source cid
    FillLLDataBuffer(6, scid & 0xff);
    FillLLDataBuffer(7, scid >> 8);

    SendLLDataBuffer(aclHandle, 0x0001, 8, 8);


    Taxeia_L2_Report("{=>%s}\n", L2CAPSignalOpcodeStr(LLC_DISC_RSP));

}

void LLSignalCnfRsp(U16 aclHandle, U8 id, U16 scid, U16 result, U8 len, U8 *cnf)
{
    U8 i;

    //code
    FillLLDataBuffer(0, LLC_CONFIG_RSP); //disconnect response

    //id
    FillLLDataBuffer(1, id); //identifier

    //length
    FillLLDataBuffer(2, 6+len);
    FillLLDataBuffer(3, 0);

    //src cid
    FillLLDataBuffer(4, scid & 0xff);
    FillLLDataBuffer(5, scid >> 8);

    //flags
    FillLLDataBuffer(6, 0);
    FillLLDataBuffer(7, 0);

    //result
    FillLLDataBuffer(8, result & 0xff);
    FillLLDataBuffer(9, result >> 8);

    for(i = 0; i < len; i++)
        FillLLDataBuffer(i+10, cnf[i]);

    SendLLDataBuffer(aclHandle, 0x0001, 10+len, 10+len);

    Taxeia_L2_Report("{=>%s}\n", L2CAPSignalOpcodeStr(LLC_CONFIG_RSP));
}

void LLSignalCnfReq(U16 aclHandle, U8 id, U16 dcid, U8 len, U8 *cnf)
{
    U8 i;

    //code
    FillLLDataBuffer(0, LLC_CONFIG_REQ); //disconnect response

    //id
    FillLLDataBuffer(1, LLSignalIndentifierGenerate()); //identifier

    //length
    FillLLDataBuffer(2, 4+len);
    FillLLDataBuffer(3, 0);

    //dest cid
    FillLLDataBuffer(4, dcid & 0xff);
    FillLLDataBuffer(5, dcid >> 8);

    //flags
    FillLLDataBuffer(6, 0);
    FillLLDataBuffer(7, 0);

    for(i = 0; i < len; i++)
        FillLLDataBuffer(i+8, cnf[i]);

    SendLLDataBuffer(aclHandle, 0x0001, 8+len, 8+len);

    Taxeia_L2_Report("{=>%s}\n", L2CAPSignalOpcodeStr(LLC_CONFIG_REQ));
}

void LLSignalInfoRsp(U16 aclHandle, U8 id, U16 infotype)
{
    //code
    FillLLDataBuffer(0, LLC_INFO_RSP); //disconnect response

    //id
    FillLLDataBuffer(1, id); //identifier

    //length
        FillLLDataBuffer(2, 4);
    FillLLDataBuffer(3, 0);

    //infotype
    FillLLDataBuffer(4, infotype & 0xff);
    FillLLDataBuffer(5, infotype >> 8);

    //result
    FillLLDataBuffer(6, 0x01); //Not support
    FillLLDataBuffer(7, 0x00);

    SendLLDataBuffer(aclHandle, 0x0001, 8, 8);

    Taxeia_L2_Report("{=>%s}\n", L2CAPSignalOpcodeStr(LLC_INFO_RSP));

}
#endif

void LLSignalTestReq(U16 aclHandle, U8 len, U8 *data)
{
    U8 i;

    //code
    FillLLDataBuffer(0, LLC_TEST_REQ); //disconnect response

    //id
    FillLLDataBuffer(1, LLSignalIndentifierGenerate()); //identifier

    //length
    FillLLDataBuffer(2, len);
    FillLLDataBuffer(3, 0);

    for(i = 0; i < len; i++)
        FillLLDataBuffer(i+4, data[i]);

    SendLLDataBuffer(aclHandle, 0x0001, 4+len, 4+len);

    Taxeia_L2_Report("{=>%s}\n", L2CAPSignalOpcodeStr(LLC_TEST_REQ));
}

#ifdef Taxeia_L2_Report
    extern const char *L2CAPSignalOpcodeStr(U8 Opcode);
#endif

#if (PROFILE_TYPE == PROFILE_TYPE_NOTHING)
static void Bt__EDRSignalingChannelHandler(U16 aclHandle, U16 cid, U8 *dat)
{
    U8 id, op;
    U16 length;
    U16 tmp;
    U16 dat45;
    U16 dat67;

    op = dat[0];
    id = dat[1];
    length = ((dat[3] << 8) + dat[2]);
    dat45 = ((dat[5] << 8) + dat[4]);
    dat67 = ((dat[7] << 8) + dat[6]);
    tmp = 0;
    
    Taxeia_L2_Report("{<=%s}\n", L2CAPSignalOpcodeStr(op));
    if(op == LLC_TEST_REQ)
                Bt__TestCallBack(length, dat+4);
    
}

#else
static void Bt__EDRSignalingChannelHandler(U16 aclHandle, U16 cid, U8 *dat)
{
    U8 id, op;
    U16 length;
    U16 tmp;
    U16 dat45;
    U16 dat67;

    op = dat[0];
    id = dat[1];
    length = ((dat[3] << 8) + dat[2]);
    dat45 = ((dat[5] << 8) + dat[4]);
    dat67 = ((dat[7] << 8) + dat[6]);
    tmp = 0;
    
    Taxeia_L2_Report("{<=%s}\n", L2CAPSignalOpcodeStr(op));

    //check length
    switch(op) //l2cap signaling op
    {
        case LLC_COMMAND_REJ:
            break;
        case LLC_CONN_REQ:
            if(length == 4)
                goto check_pass;
            break;
        case LLC_CONN_RSP:
            if(length == 8)
                goto check_pass;
            break;
        case LLC_CONFIG_REQ:
        {
            //check if the cid was allocated
            if(L2ChannelSearchIdxByCid(dat45) == PSM_SUPPORT_NUM)
                tmp = 2; //invalid cid
            else if(dat[6] == 0 && dat[7] == 0) //only allow flags = 0
                goto check_pass;
            break;
        }
        case LLC_CONFIG_RSP:
            //check if the cid was allocated
            if(L2ChannelSearchIdxByCid(dat45) == PSM_SUPPORT_NUM)
                tmp = 2; //invalid cid
            else  //no check length
                goto check_pass;
            break;
        case LLC_DISC_REQ:
        {
            //check if the cid was allocated
            if(L2ChannelSearchIdxByCid(dat45) == PSM_SUPPORT_NUM)
                tmp = 2; //invalid cid
            else if(length == 4)
                goto check_pass;
            break;
        }
        case LLC_DISC_RSP:
            goto check_pass;
            break;
        case LLC_ECHO_REQ:
            break;
        case LLC_ECHO_RSP:
            break;
        case LLC_INFO_REQ:
            if(length == 2)
                goto check_pass;
            break;
        case LLC_INFO_RSP:
            break;
        case LLC_TEST_REQ:
            goto check_pass;
            break;
        default:
            break;
    }

    LLSignalCmdRej(aclHandle, id, tmp/*command not understand*/, 0, 0);
    return;

check_pass:

    switch(op) //l2cap signaling op
    {
        case LLC_COMMAND_REJ:
            break;
        case LLC_CONN_REQ:
        {
//          LSB  octet 0    octet 1     octet 2  octet 3   MSB
//             |---------|-----------|--------------------|
//             |Code=0x02| Identifier|      Length        |
//             |---------|-----------|--------------------|
//             |        PSM          |       SCID         |
//             |---------------------|--------------------|

            L2_ALLOC_STATUS l2sts;

            l2sts = L2ChannelAllocate(dat45, dat67, 0/*allocated insigned*/, aclHandle);

            if(l2sts == L2_ALLOC_SUCCESS) //scan registered psm
            {
                U16 mycid;
                mycid = L2ChannelSearchAcidByPsm(dat45);
                LLSignalConRsp(aclHandle, id, mycid, dat67, L2CONN_ACCEPTED, 0);
                L2ChannelStateTransitReq(mycid, LLC_CONN_REQ);
                L2ChannelStateConfigOutMtu(mycid, LL_MTU); //default mtu
            }
            else if(l2sts == L2_ALLOC_NO_RESOURCE)//no resource
            {
                LLSignalConRsp(aclHandle, id, 0, 0, L2CONN_REJECT_NO_RESOURCES, 0);
            }
            else if(l2sts == L2_ALLOC_PSM_NOT_EXIST)
            {
                LLSignalConRsp(aclHandle, id, 0, 0, L2CONN_REJECT_PSM_NOT_SUPPORTED, 0);
            }
            break;
        }
        case LLC_CONN_RSP:
        {
//          LSB  octet 0    octet 1     octet 2  octet 3   MSB
//             |---------|-----------|--------------------|
//             |Code=0x03| Identifier|      Length        |
//             |---------|-----------|--------------------|
//             |       DCID          |       SCID         |
//             |---------------------|--------------------|
//             |       Result        |      Status        |
//             |---------------------|--------------------|
            if((dat[8] + (dat[9] << 8)) == 0 || (dat[8] + (dat[9] << 8)) == 1) //connect successfully or pending
            {
                if((dat[8] + (dat[9] << 8)) == 1) //pending, nothing, 2014/04/29
                {
                    Taxeia_L2_Report("connect pending..\n");
                }
                else
                {
                    L2ChannelPostAllocate(dat45, dat67, aclHandle);//always success, supposely
                    L2ChannelStateTransitReq(dat67, LLC_CONN_RSP);                
                }
            }
            else
            {
                //disconnect request
                //LLSignalDisConReq(aclHandle, 0, L2ChannelSearchTcidByCid(cid), cid);
                //L2ChannelDeallocateCid(cid);
                Bt__DisconnRst(aclHandle, 0x13, 0);
            }
            break;
        }
        case LLC_CONFIG_REQ:
        {
//          LSB  octet 0    octet 1     octet 2  octet 3   MSB
//             |---------|-----------|--------------------|
//             |Code=0x04| Identifier|      Length        |
//             |---------|-----------|--------------------|
//             |       DCID          |       Flags        |
//             |---------------------|--------------------|
//             |          Configuration options           |
//             |---------------------|--------------------|

            //parse options
            if(((dat[8] & 0xef) == 0x01)/*MTU*/ && dat[9] == 2 /*length = 2*/)
            {
                    U16 mtu;
                    mtu = (dat[11] << 8) + dat[10];
                    if(mtu >= 48)
                    {
                        if(mtu > LL_MTU)
                        {
                            mtu = LL_MTU;
                            dat[10] = LL_MTU & 0xff;
                            dat[11] = LL_MTU >> 8;
                            
                            //send positive response, and send non-negotiate option
                            LLSignalCnfRsp(aclHandle, id, L2ChannelSearchTcidByCid(dat45), 0x0000/*success*/, 4, dat + 8);
                        }
                        else
                            LLSignalCnfRsp(aclHandle, id, L2ChannelSearchTcidByCid(dat45), 0x0000/*success*/, 0, 0);
                        
                        L2ChannelStateTransitReq(dat45, LLC_CONFIG_REQ);
                        L2ChannelStateConfigOutMtu(dat45, mtu);
                        break;
                    }
                    
            }
            else if(((dat[8] & 0xef) == 0x03)/*QOS*/ && dat[9] == 22 /*length = 22*/)
            {
                    if(1) //no judgement
                    {
                        LLSignalCnfRsp(aclHandle, id, L2ChannelSearchTcidByCid(dat45), 0x0000/*success*/, 24, dat+8);
                        L2ChannelStateTransitReq(dat45, LLC_CONFIG_REQ);
                        break;
                    }
            }
            else if((length == 4)/*no config*/)
            {
                LLSignalCnfRsp(aclHandle, id, L2ChannelSearchTcidByCid(dat45), 0x0000/*success*/, 0, 0);
                L2ChannelStateTransitReq(dat45, LLC_CONFIG_REQ);
                break;
            }

            //fail
            LLSignalCnfRsp(aclHandle, id, L2ChannelSearchTcidByCid(dat45), 0x0001/*unaccept parameter*/, 0, 0);

        }
        break;
        case LLC_CONFIG_RSP:
        {
//          LSB  octet 0    octet 1     octet 2  octet 3   MSB
//             |---------|-----------|--------------------|
//             |Code=0x05| Identifier|      Length        |
//             |---------|-----------|--------------------|
//             |       SCID          |       Flags        |
//             |---------------------|--------------------|
//             |       Result        |      Config        |
//             |---------------------|--------------------|
            if(dat[8] == 0 && dat[9] == 0) //success
                  L2ChannelStateTransitReq(dat45, LLC_CONFIG_RSP);
            break;
        }
        case LLC_DISC_REQ:
        {
//          LSB  octet 0    octet 1     octet 2  octet 3   MSB
//             |---------|-----------|--------------------|
//             |Code=0x06| Identifier|      Length        |
//             |---------|-----------|--------------------|
//             |       DCID          |       SCID         |
//             |---------------------|--------------------|
            LLSignalDisConRsp(aclHandle, id, dat45, dat67);

            L2ChannelDeallocateCid(dat45);

        }
        break;
        case LLC_DISC_RSP:
            L2ChannelDeallocateCids(dat45, dat67); //need to compare allocat and target cid, because there is a windows.
            break;
        case LLC_ECHO_REQ:
            break;
        case LLC_ECHO_RSP:
            break;
        case LLC_INFO_REQ:
        {
//          LSB  octet 0    octet 1     octet 2  octet 3   MSB
//             |---------|-----------|--------------------|
//             |Code=0x0A| Identifier|      Length        |
//             |---------|-----------|--------------------|
//             |       InfoType      |
//             |---------------------|
            LLSignalInfoRsp(aclHandle, id, dat45);
            
            break;
        }
        case LLC_INFO_RSP:
            break;
		case LLC_TEST_REQ:
            Bt__TestCallBack(length, dat+4);
            break;
        default:
            LLSignalCmdRej(aclHandle, dat[1]/*id*/, 0/*command not understood*/, 0, 0);
            break;

    }
}
#endif

#if (PROFILE_TYPE == PROFILE_TYPE_NOTHING)
static void Bt__LLDataCB(U16 aclHandle, U16 len, U16 cid, U8 *dat)
{
    if(cid == 0x0001) //EDR signaling channel
    {
        Bt__EDRSignalingChannelHandler(aclHandle, cid, dat);
    }
    
}
#else
static void Bt__LLDataCB(U16 aclHandle, U16 len, U16 cid, U8 *dat)
{
    if(cid == 0x0000) //null identifier, taxeia internal used
    {
        if(dat == (U8*)0xffff) //tx data complete
        {

            Taxeia_L2_Report("{<=AclDataComplete} [aclhdl:0x%x]\n", aclHandle);

            L2ChannelTxComplete(aclHandle, 0, dat);
        }
        else if (dat == (U8*)0xfffe)
        {
            L2ChannelDeallocateAclHdl(aclHandle);
        }
    }
    else if(cid == 0x0001) //EDR signaling channel
    {
        Bt__EDRSignalingChannelHandler(aclHandle, cid, dat);
    }
    else
    {
        if(L2ChannelDataHandler(aclHandle, cid, dat, len) == PSM_SUPPORT_NUM) //channel not found
        {
            //disconnect request
            LLSignalDisConReq(aclHandle, 0, L2ChannelSearchTcidByCid(cid), cid);
            L2ChannelDeallocateCid(cid);
            
        }
    }
    
}
#endif

void Bt__LLCAPInit()
{
    RegisterLLDataHandler(Bt__LLDataCB);
#if (PROFILE_TYPE == PROFILE_TYPE_NOTHING)
#else
    L2ChannelDeregisterAll();
#endif
}

void Bt__LLCAPDeInit()
{
    RegisterLLDataHandler(0);
#if (PROFILE_TYPE == PROFILE_TYPE_NOTHING)
#else
    L2ChannelDeregisterAll();
#endif
}


