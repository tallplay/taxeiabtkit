#include "../HCI/BTCmdEvt.h"
#include "../../TaxeiaConsoleDebug/TaxeiaConsoledebug.h"
#include "../LOCAL_DEV/Bt__LocalDevice.h"
#include "Bt__RemoteDevice.h"
#include <string.h>

///////////////////////////////////////////////////////////////////////////////////////
typedef enum
{
    ACL_NORESPONSE,
    ACL_ACCEPT,
    ACL_REJECT,
    ACL_ACCEPT_IF,
    ACL_CONNECTED,
    ACL_CONNECTING,
    ACL_DISCONNECTING
}ACL_SETTING;

static U8 ConnectionStatus = ACL_ACCEPT; //0: no response, 1: accept, 2: reject, 3: accept if xxx, 4:already connected
static U16 AclHandle = 0;
static U8 *AclBdaddr = 0;
static U8 *AclBdName = 0;
static U16 ScoHandle = 0;
LLDataHandler llDataHandler = 0;

static FinishNotifyCallback FinishNotifyCB = 0;
static TestDataHandler TestDataCB = 0;
static InquiryCallback InquiryCB = 0;
extern BOOL SendData(U16 len);
extern U8 Bt__AclDataSend(void);
///////////////////////////////////////////////////////////////////////////////////////
//only for command status
void Bt__ConnRstStatusCB(U8 CmdOp, U8 EvtOp, U8 *dat)
{
    if(EvtOp == HCE_COMMAND_STATUS)
    {
        if(CmdOp == HCCI_CREATE_CONNECTION)
        {
            if(dat == 0x00)
                ConnectionStatus = ACL_CONNECTING;
        }
        else if(HCCI_DISCONNECT == CmdOp)
        {
            if(dat == 0x00)
                ConnectionStatus = ACL_DISCONNECTING;
        }
    }
}

void Bt__InquriyCB(U8 CmdOp, U8 EvtOp, U8 *dat)
{
    if(EvtOp == HCE_COMMAND_STATUS)
    {
        if(CmdOp == HCCI_INQUIRY && dat[0]!= HC_STATUS_SUCCESS)
        {
            if(InquiryCB)
                InquiryCB(1, dat); //fail
        }
    }
    else if(EvtOp == HCE_INQUIRY_COMPLETE)
    {
            if(InquiryCB)
                InquiryCB(0, 0); //compelete
    }
    else if(EvtOp == HCE_INQUIRY_RESULT)
    {
            if(InquiryCB)
                InquiryCB(2, dat); //result
    }
    else if(EvtOp == HCE_INQUIRY_RESULT_WITH_RSSI)
    {
            if(InquiryCB)
                InquiryCB(3, dat); //result
    }
}

//private
void Bt__ConnRstClkOffset(U8 *bdaddr, U16 clkoffset, U8 repetition_mode)
{
    U8 parm[13];
    if(ConnectionStatus == ACL_ACCEPT) //only allow in this situation, else cold handling
    {
        memcpy(parm, bdaddr, 6);
        //packet type
        parm[6] = 0x18;
        parm[7] = 0xcc;
        //repetition
        parm[8] = repetition_mode; //R0
        //reserved
        parm[9] = 0x00;
        //clock_offset
        parm[10] = clkoffset & 0xff;
        parm[11] = clkoffset >> 8;
        
        //allow role switch
        #ifdef DENY_ROLE_CHANGE
        parm[12] = 0x00; //deny
        #else
        parm[12] = 0x01; //allow
        #endif        
        SendCmd(HCCI_CREATE_CONNECTION, parm, Bt__ConnRstStatusCB);
    }
}

//private
void Bt__SetupSCOConnect(void)
{
    U8 parm[17];
    if(!ScoHandle)
    {
        parm[0] = AclHandle & 0xff;
        parm[1] = AclHandle >> 8;
        parm[2] = 0x40;
        parm[3] = 0x1F;
        parm[4] = 0x00;
        parm[5] = 0x00;
        parm[6] = 0x40;
        parm[7] = 0x1F;
        parm[8] = 0x00;
        parm[9] = 0x00;
        parm[10] = 0xff;
        parm[11] = 0xff;

        parm[12] = 0x01; //1's, u-law
        parm[13] = 0x01; //u-law

        parm[14] = 0xff;
        
        parm[15] = 0x3f;
        parm[16] = 0x00;
        SendCmd(HCCI_SETUP_SYNC_CONNECTION, parm, 0);
    }
}

///////////////////////////////////Public API///////////////////////////////////////////
void Bt__ConnRst(U8 *bdaddr, FinishNotifyCallback fc)
{
    Bt__ConnRstClkOffset(bdaddr, 0, 0);
}


void Bt__DisconnRst(U16 aclHandle, U8 reason, FinishNotifyCallback fc)
{
    U8 parm[3];
    if(ConnectionStatus == ACL_CONNECTED) //only allow in this situation, else cold handling
    {
        parm[0] = aclHandle & 0xff;
        parm[1] = aclHandle >> 8;
        parm[2] = reason;
        SendCmd(HCCI_DISCONNECT, parm, Bt__ConnRstStatusCB);
        //FinishNotifyCB = fc;
    }
}

void Bt__RemoteDeviceTestCB(TestDataHandler tdh)
{
    TestDataCB = tdh;
}

void Bt__RemoteDeviceTest(U16 len, U8 *dat)
{
    extern void LLSignalTestReq(U16 aclHandle, U8 len, U8 *data);
    LLSignalTestReq(AclHandle, len, dat);
}

void Bt__RemoteDeviceCB(FinishNotifyCallback fc) //call back for initialized
{
    FinishNotifyCB = fc;
}

//max 26 bytes
void Bt__RemoteBdNameReg(U8 *name)
{
#ifdef NO_REMOTE_NAME
#else
   AclBdName = name;
#endif
}

void Bt__RemoteBdaddrReg(U8 *bdaddr)
{
    AclBdaddr = bdaddr;
}

//0 0x9E8B33 General/Unlimited Inquiry Access Code (GIAC)
//1 0x9E8B00 Limited Dedicated Inquiry Access Code (LIAC)
//2 0x9E8B01-0x9E8B32 RESERVED FOR FUTURE
//63 0x9E8B34-0x9E8B3F USE

void Bt__Inquiry(U8 IAC, InquiryCallback ic)
{
    U8 parm[5];

    InquiryCB = ic;

    if(!IAC)
        parm[0] = 0x33;
    else
        parm[0] = IAC;

    parm[1] = 0x8B;
    parm[2] = 0x9E;
    
    parm[3] = 0x14;
    parm[4] = 0x01;
    SendCmd(HCCI_INQUIRY, parm, Bt__InquriyCB);
}

void Bt__InquiryNormal(U8 times, InquiryCallback ic)
{
    U8 parm[5];

    InquiryCB = ic;

    parm[0] = 0x33;

    parm[1] = 0x8B;
    parm[2] = 0x9E;
    
    parm[3] = 0x14;
    parm[4] = times;
    SendCmd(HCCI_INQUIRY, parm, Bt__InquriyCB);
}

void Bt__InquiryCancel(void)
{
    SendCmd(HCCI_INQUIRY_CANCEL, 0, 0);
}

void Bt__InquiryMode(U8 mode)
{
    SendCmd(HCCI_WRITE_INQ_MODE, &mode, 0);
}


void Bt__RemoteNameReq(U8 *parm, CompleteDataHandler cb)
{
    SendCmd(HCCI_REM_NAME_REQ, parm, cb);
}

void Bt__ReadRSSI(U16 aclHandle, CompleteDataHandler cb)
{
    U8 parm[2];

    parm[0] = aclHandle & 0xff;
    parm[1] = aclHandle >> 8;

    SendCmd(HCCI_READ_RSSI, parm, cb);
}
///////////////////////////////////Private Functions////////////////////////////////////

void RegisterLLDataHandler(LLDataHandler lldh)
{
    llDataHandler = lldh;
}

U8 LLDataAvaliable()
{
    extern U8 TxDataPresent;
    return !TxDataPresent;
}

void SendLLDataBuffer(U16 acl_handle, U16 cid, U16 lllen, U16 sendlen)
{    
    //acl handle
    FillTxDataBuffer(0, acl_handle & 0xff);
    FillTxDataBuffer(1, ((acl_handle & 0xf) >> 8) | 0x20/*pb = 10*/);

    //acllen
    FillTxDataBuffer(2, (sendlen+4) & 0xff);
    FillTxDataBuffer(3, (sendlen+4) >> 8);
    
    //lllen
    FillTxDataBuffer(4, lllen & 0xff);
    FillTxDataBuffer(5, lllen >> 8);

    //cid
    FillTxDataBuffer(6, cid & 0xff);
    FillTxDataBuffer(7, cid >> 8);

    ASSERT(!Bt__AclDataSend());

    #ifdef EGIS_NO_ASSERT
    
    SendData(sendlen+8);
    #else    
    ASSERT(SendData(sendlen+8));
    #endif
}

U8 GetLLDataBuffer(U16 idx)
{
    return GetTxDataBuffer(idx + 8);
}

void FillLLDataBuffer(U16 idx, U8 dat)
{
    FillTxDataBuffer(idx+8, dat);
}

void MoveLLDataBuffer(U16 tgt, U16 src)
{
    MoveTxDataBuffer(tgt+8, src+8);
}


void Bt__ConnIncomingRstCB(U8 *dat)
{
    
    switch(ConnectionStatus)
    {
        case ACL_NORESPONSE:
            break;
        case ACL_ACCEPT: //accept
        {
            U8 parm[7];
            if(dat[9] == 0x01) //if ACL, then accept
            {
                if(AclBdaddr)
                    memcpy(AclBdaddr, dat, 6);

                memcpy(parm, dat, 6);
                #ifdef ROLE_CHANGE_ON_ACCEPT
                
                parm[6] = 0x00; //master
                #else
                parm[6] = 0x01; //slave
                #endif
                SendCmd(HCCI_ACCEPT_CON_REQ, parm, 0);
            }
            break;
        }
        case ACL_REJECT:
            break;
        case ACL_ACCEPT_IF:
            break;
        case ACL_CONNECTED:
            if(dat[9] == 0x02 || dat[9] == 0x00) //eSCO or SCO
            {
#if 1
                U8 parm[21];
                memcpy(parm, dat, 6);
                memset(parm+6, 0xFF, 15);
                parm[6] = 0x40;
                parm[7] = 0x1F;
                parm[8] = 0x00;
                parm[9] = 0x00;
                parm[10] = 0x40;
                parm[11] = 0x1F;
                parm[12] = 0x00;
                parm[13] = 0x00;

                parm[16] = 0x01; //1's, u-law
                parm[17] = 0x01; //u-law
                //0011000000
                SendCmd(HCCI_ACCEPT_SYNC_CON_REQ, parm, 0);
#else
                U8 parm[7];
                memcpy(parm, dat, 6);
                parm[6] = 0x0D; //limited of resource
                SendCmd(HCCI_REJECT_SYNC_CON_REQ, parm, 0);
#endif
                return ;
            }
        break;
        default:
            break;
    }
}

void Bt__NameReqCB(U8 CmdOp, U8 EvtOp, U8 *dat)
{
    if(EvtOp == HCE_REMOTE_NAME_REQ_COMPLETE && dat[0] == HC_STATUS_SUCCESS)
    {
        U32 len;
        len = (size_t)strlen((char*)dat+7);
        len = len > 25 ? 25 : len;        
        memcpy(AclBdName, dat+7, (U8)len);
        AclBdName[len] = 0;
    }
    else if(EvtOp == HCE_REMOTE_NAME_REQ_COMPLETE || (EvtOp == HCE_COMMAND_STATUS && dat[0] != HC_STATUS_SUCCESS))
    {
        strcpy((char*)AclBdName, "Unknown Name");
    }
}

void Bt__FlushOccurred(U8 *dat, U8 *aclnum)
{
    Taxeia_Report("!!!!!!! Flush occurred !!!!!!\r\n");
    
    if(AclHandle == ((dat[1] & 0xf) << 8) + dat[0])
    {
        Taxeia_Report("!!!!!!! AclHandle match !!!!!!\r\n");
        Taxeia_Report("!!!!!!! AclHandle match !!!!!!\r\n");
        Taxeia_Report("!!!!!!! AclHandle match !!!!!!\r\n");
        Taxeia_Report("!!!!!!! AclHandle match !!!!!!\r\n");
        Taxeia_Report("!!!!!!! AclHandle match !!!!!!\r\n");
        *aclnum = 0;
    }
}

void Bt__SCoConnCmpCB(U8 *dat)
{
    if(dat[0] == HC_STATUS_SUCCESS) //immediately disconnected, we don't have pcm hw
    {
        ScoHandle = dat[1] + (dat[2] << 8);
        Taxeia_Report("SCO connected air_mode = %x\r\n", dat[16]);

        if(FinishNotifyCB)
            FinishNotifyCB(ScoHandle | 0x80);

    }
}

void Bt__ConnPacketChgCB(U8 *dat, U8 *aclnum)
{
    BT_LOCAL_INFO *btinfo;
    
    Bt__GetLocalInfo(&btinfo);
    
    if(dat[0] == HC_STATUS_SUCCESS) //status
    {
        if(ConnectionStatus == ACL_ACCEPT)
        {
            Taxeia_Report("!!!early connected\n");
            ConnectionStatus = ACL_CONNECTED;
            AclHandle = ((dat[2] & 0xf) << 8) + dat[1];

            if(FinishNotifyCB)
                FinishNotifyCB(AclHandle);
         }

        *aclnum = 0; //reset aclnum, because we only allow one acl
    }
}

void Bt__ConnCmpCB(U8 *dat, U8 *aclnum)
{
    BT_LOCAL_INFO *btinfo;
    
    Bt__GetLocalInfo(&btinfo);
    
    if(dat[0] == HC_STATUS_SUCCESS) //status
    {
        U8 parm[10]; //for name request
        AclHandle = ((dat[2] & 0xf) << 8) + dat[1];
        if(AclBdaddr)
        {
           memcpy(AclBdaddr, dat+3, 6);
#ifndef DUT_MODE
            if(memcmp(btinfo->BT_ADDR, AclBdaddr, 6))
            {            
                if(AclBdName)
                {
                    AclBdName[0] = 0;
                    memcpy(parm, dat+3, 6);
                    memset(parm+ 6, 0, 4);
                    SendCmd(HCCI_REM_NAME_REQ, parm, Bt__NameReqCB);
                }
            }
#endif            
        }

        if(ConnectionStatus == ACL_ACCEPT)
        {
                ConnectionStatus = ACL_CONNECTED;

            if(FinishNotifyCB)
                FinishNotifyCB(AclHandle);
        }

    }
    else
    {
        if(ConnectionStatus != ACL_CONNECTED)
        {
            ConnectionStatus = ACL_ACCEPT;
            FinishNotifyCB(0);
        }
    }
    
    *aclnum = 0; //reset aclnum, because we only allow one acl
}

void Bt__DisConnCmpCB(U8 *dat, U8 *aclnum)
{
    if(dat[0] == HC_STATUS_SUCCESS)
    {
        if((((dat[2] & 0xf) << 8) + dat[1]) == AclHandle)
        {
            ConnectionStatus = ACL_ACCEPT; //allow connected
            if(llDataHandler)
                llDataHandler(AclHandle, 0, 0x0000, (U8*)0xfffe); //inform acl disconnect
            AclHandle = 0;    
            if(FinishNotifyCB)
            {
                FinishNotifyCB(0);
                //FinishNotifyCB = 0;
            }

            *aclnum = 0; //reset aclnum, because we only allow one acl
        }
        else
        {
            ScoHandle = 0;
            Taxeia_Report("maybe SCO disconnect\r\n");
            if(FinishNotifyCB)
                FinishNotifyCB(0x80);
        }
        
    }
}

void Bt__WriteLinkSupervisionTimeout(U16 AclHandle, U16 timeout)
{
    U8 dat[4];
    dat[0] = AclHandle & 0xff;
    dat[1] = AclHandle >> 8;
    dat[2] = timeout & 0xff;
    dat[3] = timeout >> 8;
    SendCmd(HCCI_WRITE_LINK_SUPERV_TIMEOUT, dat, 0);
}

static void Bt__DataHandlerCB(U16 handle, U16 len, U8 *dat)
{
    if(AclHandle == handle) 
    {
        if(len > 4) //l2cap packet should > 4
        {
            if(len == (((dat[1] << 8) + dat[0]))+4) // Infomation payload + 4 == total length
            {
                if(llDataHandler)
                    llDataHandler(handle, len-4, ((dat[3] << 8) + dat[2])/*cid*/, &dat[4]/*information payload*/);
            }
        }
    }
}

void Bt__TestCallBack(U16 len, U8 *dat)
{
    if(TestDataCB)
        TestDataCB(len, dat);
}

void Bt__DataCmpHandlerCB(U8 *dat)
{
    U8 cnt;
    U16 acl_handle;
    
    cnt = *dat++; //number of handle
    acl_handle = (((dat[1] & 0xf) << 8) + dat[0]); //connection handle
    
    while(cnt --)
    {
        if(llDataHandler)
            llDataHandler(acl_handle, 0x0000, 0x0000, (U8*)0xffff);
    }
}

void Bt__RemoteDeviceInit()
{
    RegisterAclDataHandler(Bt__DataHandlerCB);
}

void Bt__RemoteDeviceDeInit()
{
    ConnectionStatus = ACL_ACCEPT;
    RegisterAclDataHandler(0);
}

