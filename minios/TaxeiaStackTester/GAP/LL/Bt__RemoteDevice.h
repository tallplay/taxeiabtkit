#ifndef __BT_REMOTE_HH_
#define __BT_REMOTE_HH_
#include "../HCI/BtCmdEvt.h"

///////////////////////////////////Public API///////////////////////////////////////////
typedef void (*LLDataHandler)(U16 aclHandle, U16 len, U16 cid, U8 *dat);
typedef void (*TestDataHandler)(U16 len, U8 *dat);
typedef void (*InquiryCallback) (U8, U8 *);
void Bt__WriteLinkSupervisionTimeout(U16 AclHandle, U16 timeout);
void Bt__ConnRst(U8 *bdaddr, FinishNotifyCallback fc);
void Bt__DisconnRst(U16 aclHandle, U8 reason, FinishNotifyCallback fc);
void Bt__RemoteDeviceCB(FinishNotifyCallback fc); //call back for initialized
void Bt__RemoteBdaddrReg(U8 *bdaddr);
void Bt__Inquiry(U8 IAC, InquiryCallback);
void Bt__InquiryMode(U8 mode);
void Bt__ReadRSSI(U16 aclHandle, CompleteDataHandler);

//testing command
void Bt__RemoteDeviceTestCB(TestDataHandler tdh);
void Bt__RemoteDeviceTest(U16 len, U8 *dat);

//max 26 bytes
void Bt__RemoteBdNameReg(U8 *name);

///////////////////////////////////Private Functions////////////////////////////////////
void Bt__RemoteDeviceInit(void);
void Bt__RemoteDeviceDeInit(void);

#endif
