#include "../HCI/BTCmdEvt_loader.h"
#include "../../TaxeiaConsoleDebug/TaxeiaConsoledebug.h"
#include "../LOCAL_DEV/Bt__LocalDevice.h"
#include "Bt__RemoteDevice.h"
#include <string.h>

///////////////////////////////////////////////////////////////////////////////////////
static U16 AclHandle = 0;
static U8 *AclBdaddr = 0;
static U8 *AclBdName = 0;
static U16 ScoHandle = 0;

static FinishNotifyCallback FinishNotifyCB = 0;
static TestDataHandler TestDataCB = 0;
static InquiryCallback InquiryCB = 0;
extern BOOL SendData(U16 len);
extern U8 Bt__AclDataSend(void);
///////////////////////////////////////////////////////////////////////////////////////
//only for command status
void Bt__ConnRstStatusCB(U8 CmdOp, U8 EvtOp, U8 *dat)
{
}

///////////////////////////////////Public API///////////////////////////////////////////

void Bt__DisconnRst(U16 aclHandle, U8 reason, FinishNotifyCallback fc)
{
    U8 parm[3];
    parm[0] = aclHandle & 0xff;
    parm[1] = aclHandle >> 8;
    parm[2] = reason;
    SendCmd(HCCI_DISCONNECT, parm, Bt__ConnRstStatusCB);
}

void Bt__RemoteDeviceTestCB(TestDataHandler tdh)
{
    TestDataCB = tdh;
}

void Bt__RemoteDeviceTest(U16 len, U8 *dat)
{
    extern void LLSignalTestReq(U16 aclHandle, U8 len, U8 *data);
    LLSignalTestReq(AclHandle, len, dat);
}

void Bt__RemoteDeviceCB(FinishNotifyCallback fc) //call back for initialized
{
    FinishNotifyCB = fc;
}


///////////////////////////////////Private Functions////////////////////////////////////
void Bt__TestCallBack(U16 len, U8 *dat);

static void L2DataHandler(U16 aclHandle, U16 len, U16 cid, U8 *dat)
{
    if(cid == 0x0001) //EDR signaling channel
    {
        U8 id, op;
        U16 length;

        op = dat[0];

        length = ((dat[3] << 8) + dat[2]);

        if(op == LLC_TEST_REQ)
            Bt__TestCallBack(length, dat+4);
    }
    
}


U8 LLDataAvaliable()
{
    extern U8 TxDataPresent;
    return !TxDataPresent;
}

void SendLLDataBuffer(U16 acl_handle, U16 cid, U16 lllen, U16 sendlen)
{    
    //acl handle
    FillTxDataBuffer(0, acl_handle & 0xff);
    FillTxDataBuffer(1, ((acl_handle & 0xf) >> 8) | 0x20/*pb = 10*/);

    //acllen
    FillTxDataBuffer(2, (sendlen+4) & 0xff);
    FillTxDataBuffer(3, (sendlen+4) >> 8);
    
    //lllen
    FillTxDataBuffer(4, lllen & 0xff);
    FillTxDataBuffer(5, lllen >> 8);

    //cid
    FillTxDataBuffer(6, cid & 0xff);
    FillTxDataBuffer(7, cid >> 8);

    ASSERT(!Bt__AclDataSend());

    #ifdef EGIS_NO_ASSERT
    
    SendData(sendlen+8);
    #else    
    ASSERT(SendData(sendlen+8));
    #endif
}



void Bt__ConnIncomingRstCB(U8 *dat)
{    
    U8 parm[7];
    if(dat[9] == 0x01) //if ACL, then accept
    {
        if(AclBdaddr)
            memcpy(AclBdaddr, dat, 6);

        memcpy(parm, dat, 6);
        parm[6] = 0x01; //slave
        SendCmd(HCCI_ACCEPT_CON_REQ, parm, 0);
    }
}

void Bt__ConnCmpCB(U8 *dat, U8 *aclnum)
{   
    if(dat[0] == HC_STATUS_SUCCESS) //status
    {
        U8 parm[10]; //for name request
        AclHandle = ((dat[2] & 0xf) << 8) + dat[1];
        if(AclBdaddr)
        {
           memcpy(AclBdaddr, dat+3, 6);
        }


        if(FinishNotifyCB)
            FinishNotifyCB(AclHandle);

    }
    else
    {
        FinishNotifyCB(0);
    }
    
    *aclnum = 0; //reset aclnum, because we only allow one acl
}

void Bt__DisConnCmpCB(U8 *dat, U8 *aclnum)
{
    if(dat[0] == HC_STATUS_SUCCESS)
    {
        AclHandle = 0;    
        if(FinishNotifyCB)
        {
            FinishNotifyCB(0);
            //FinishNotifyCB = 0;
        }

        *aclnum = 0; //reset aclnum, because we only allow one acl
    }
}

extern void Bt__DataHandlerCB(U16 handle, U16 len, U8 *dat)
{
    if(AclHandle == handle) 
    {
        if(len > 4) //l2cap packet should > 4
        {
            if(len == (((dat[1] << 8) + dat[0]))+4) // Infomation payload + 4 == total length
            {
                L2DataHandler(handle, len-4, ((dat[3] << 8) + dat[2])/*cid*/, &dat[4]/*information payload*/);
            }
        }
    }
}

void Bt__TestCallBack(U16 len, U8 *dat)
{
    if(TestDataCB)
        TestDataCB(len, dat);
}

