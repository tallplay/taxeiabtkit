#ifndef __TYPES_H
#define __TYPES_H

typedef int BOOL;

#ifndef __COMMON_INC__
#ifdef WIN32
typedef unsigned long  U32;
#else
typedef unsigned int  U32;
#endif
typedef unsigned short U16;
typedef unsigned char  U8;

typedef int  S32;
typedef short S16;
typedef signed char  S8;
#endif

#ifndef TRUE
#define TRUE  (1==1)
#endif

#ifndef FALSE
#define FALSE (0==1)
#endif

#define SDTYPE_NIL  0x00
#define SDTYPE_UINT 0x08
#define SDTYPE_SINT 0x10
#define SDTYPE_UUID 0x18
#define SDTYPE_TEXT 0x20
#define SDTYPE_BOOL 0x28
#define SDTYPE_SEQ  0x30
#define SDTYPE_ALT  0x38
#define SDTYPE_URL  0x40
#define SDTYPE_MASK 0xf8
#define SDSIZE_1BYTE      0x00
#define SDSIZE_2BYTES     0x01
#define SDSIZE_4BYTES     0x02
#define SDSIZE_8BYTES     0x03
#define SDSIZE_16BYTES    0x04
#define SDSIZE_ADD_8BITS  0x05
#define SDSIZE_ADD_16BITS 0x06
#define SDSIZE_ADD_32BITS 0x07
#define SDSIZE_MASK       0x07

#define SDP_DATA_ELEMENT_SEQ_SIZE8(size) \
            SDTYPE_SEQ + SDSIZE_ADD_8BITS, \
            size

#define SDP_UUID_SIZE16(uuid) \
            SDTYPE_UUID + SDSIZE_2BYTES, \
            (U8)(((uuid) & 0xff00) >> 8),\
            (U8)((uuid) & 0x00ff)

#define SDP_TEXT_SIZE8(size) \
            SDTYPE_TEXT + SDSIZE_ADD_8BITS,\
            (U8)(size)

#define SDP_UINT_SIZE8(uint) \
            SDTYPE_UINT + SDSIZE_1BYTE, \
            (U8)(uint)

#define SDP_UINT_SIZE16(uint) \
            SDTYPE_UINT + SDSIZE_2BYTES,        \
            (U8)(((uint) & 0xff00) >> 8), \
            (U8)((uint) & 0x00ff)

#define SDP_UINT_SIZE32(uint) \
            SDTYPE_UINT + SDSIZE_4BYTES,           \
            (U8)(((uint) & 0xff000000) >> 24),    \
            (U8)(((uint) & 0x00ff0000) >> 16),    \
            (U8)(((uint) & 0x0000ff00) >> 8),     \
            (U8)((uint) & 0x000000ff)

#define SDP_BOOL_SIZE8(value) \
            SDTYPE_BOOL + SDSIZE_1BYTE,         \
            (U8)(value)


#define LE2Host16(ptr)  (U16)( ((U16) *((U8*)(ptr)+1) << 8) | \
                                ((U16) *((U8*)(ptr))) )

#define LE2Host32(ptr)  (U32)( ((U32) *((U8*)(ptr)+3) << 24) | \
                                ((U32) *((U8*)(ptr)+2) << 16) | \
                                ((U32) *((U8*)(ptr)+1) << 8)  | \
                                ((U32) *((U8*)(ptr))) )

#define BE2Host16(ptr)  (U16)( ((U16) *((U8*)(ptr)) << 8) | \
                                ((U16) *((U8*)(ptr)+1)) )

#define BE2Host32(ptr)  (U32)( ((U32) *((U8*)(ptr)) << 24)   | \
                                ((U32) *((U8*)(ptr)+1) << 16) | \
                                ((U32) *((U8*)(ptr)+2) << 8)  | \
                                ((U32) *((U8*)(ptr)+3)) )

#define Host2LE16(buff,num) ( ((buff)[1] = (U8) ((num)>>8)),    \
                              ((buff)[0] = (U8) (num)) )

#define Host2LE32(buff,num) ( ((buff)[3] = (U8) ((num)>>24)),  \
                              ((buff)[2] = (U8) ((num)>>16)),  \
                              ((buff)[1] = (U8) ((num)>>8)),   \
                              ((buff)[0] = (U8) (num)) )

#define Host2BE16(buff,num) ( ((buff)[0] = (U8) ((num)>>8)),    \
                              ((buff)[1] = (U8) (num)) )

#define Host2BE32(buff,num) ( ((buff)[0] = (U8) ((num)>>24)),  \
                              ((buff)[1] = (U8) ((num)>>16)),  \
                              ((buff)[2] = (U8) ((num)>>8)),   \
                              ((buff)[3] = (U8) (num)) )


#ifndef max
#define max(a,b)            (((a) > (b)) ? (a) : (b))
#endif /* max */

#ifndef min
#define min(a,b)            (((a) < (b)) ? (a) : (b))
#endif /* min */


#endif

