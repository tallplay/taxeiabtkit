#include "stdafx.h"
#include "BtPhoneSimulator.h"
#include "BtPhoneSimulatorDlg.h"

#include "GAPControl.h"
#include "HFPControl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


//#define NO_BT

CPresentorSimulatorDlg *btpdlg;
static U8 PanelXStart;
static U16 PanelYStart;
static U8 PanelXEnd;
static U16 PanelYEnd;
static U8 PanelXCurr;
static U16 PanelYCurr;


// CPresentorSimulatorDlg dialog


CPresentorSimulatorDlg::CPresentorSimulatorDlg(CWnd* pParent /*=NULL*/)
    : CDialog(CPresentorSimulatorDlg::IDD, pParent)
{
    m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
    m_PanelDc = 0;
}

void CPresentorSimulatorDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CPresentorSimulatorDlg, CDialog)
    ON_WM_PAINT()
    ON_WM_QUERYDRAGICON()
    //}}AFX_MSG_MAP
    ON_BN_CLICKED(IDC_BUTTON_TEST, &CPresentorSimulatorDlg::OnBnClickedButtonTest)
	ON_WM_KEYUP()
END_MESSAGE_MAP()


// CPresentorSimulatorDlg message handlers

void PanelUpdate()
{
    StretchDIBits(btpdlg->m_PanelDc->m_hDC, 0, 0, btpdlg->m_PanelDcW, btpdlg->m_PanelDcH, 0, 0, btpdlg->m_PanelDcW, btpdlg->m_PanelDcH, btpdlg->m_PanelBuf, (BITMAPINFO*)&btpdlg->m_PanelBmInfo, DIB_RGB_COLORS, SRCCOPY);
}

void PanelStartReadData()
{
    PanelXCurr = PanelXStart;
    PanelYCurr = PanelYStart;
}

void PanelStartWriteData()
{
    PanelXCurr = PanelXStart;
    PanelYCurr = PanelYStart;
}

void PanelEndWRData()
{
    PanelUpdate();
}

void PanelSetCursorStartEnd(U8 xpos, U16 ypos, U8 xpos_end, U16 ypos_end)
{
    PanelXStart = xpos;
    PanelYStart = ypos;
    PanelXEnd = xpos_end;
    PanelYEnd = ypos_end;
}

void PanelGetCursorStartEnd(U8 *xpos, U16 *ypos, U8 *xpos_end, U16 *ypos_end)
{
    *xpos = PanelXStart;
    *ypos = PanelYStart;
    *xpos_end = PanelXEnd;
    *ypos_end = PanelYEnd;
}

void WritePixelData(U8 dat1, U8 dat2)
{
    btpdlg->m_PanelBuf[PanelYCurr * btpdlg->m_PanelDcW + PanelXCurr] = (dat1 << 8) + dat2;
    PanelXCurr++;

    if(PanelXCurr > PanelXEnd)
    {
        PanelYCurr++;
        PanelXCurr = PanelXStart;
    }

    if(PanelYCurr > PanelYEnd)
    {
        PanelYCurr = PanelYStart;
    }

}

void ReadPixelData(U8 *dat1, U8 *dat2)
{
    U16 dat;

    dat = btpdlg->m_PanelBuf[PanelYCurr * btpdlg->m_PanelDcW + PanelXCurr];

    *dat1 = dat >> 8;
    *dat2 = dat & 0xff;

    PanelXCurr++;

    if(PanelXCurr > PanelXEnd)
    {
        PanelYCurr++;
        PanelXCurr = PanelXStart;
    }

    if(PanelYCurr > PanelYEnd)
    {
        PanelYCurr = PanelYStart;
    }
}

static FILE *snor_fp;

void snor_read_open1(U32 pos)
{

	snor_fp = fopen("..\\..\\..\\..\\..\\..\\..\\..\\Dropbox\\WorkDoc\\子機\\Snor\\FlashWrite\\xp_range.txt.bin", "rb");
//    snor_fp = fopen("..\\..\\..\\..\\..\\..\\..\\..\\Dropbox\\WorkDoc\\子機\\Snor\\FlashWrite\\bg0718.bin", "rb");
    fseek(snor_fp, pos, SEEK_SET);
}

void SNOR_desel(void)
{
    fclose(snor_fp);
}

U8 snor_read_one_byte(void)
{
    U8 dat;
    fread(&dat, 1, 1, snor_fp);
    return dat;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////
void AssignLinkKeyFileName(char *name)
{
#if (PROFILE_TYPE == PROFILE_TYPE_HID)
    strcpy(name, "HIDLink");
#elif (PROFILE_TYPE == PROFILE_TYPE_HFP)
    strcpy(name, "HFPLink");
#elif (PROFILE_TYPE == PROFILE_TYPE_DYNAMIC)
    if(TheDlg->profile_select == 0) //HID
	    strcpy(name, "HIDLink");
	else if(TheDlg->profile_select == 1) //HFP
	    strcpy(name, "HFPLink");
	else
        strcpy(name, "XXXLink");
#else
    #error "Unsupported PROFILE SETTING"
#endif    
}

U8 input_key = KEY_NONE;

CPresentorSimulatorDlg *TheDlg = NULL;

DWORD AssignBDAddr(void)
{
    return MassUart_GetUniqueID() ^ 0x18;
}

void AssignBDName(char *name)
{
    sprintf((char*)name, "BTP_%08X", AssignBDAddr());
}

U8 scan_mode = 0xff;
void GapCB(U16 parm1, void* parm2)
{
    switch(parm1)
	{
	    case 0: //initilized
            //update scan mode
			scan_mode = (U8) parm2;
		    break;
	    case 1: //scan change
	        scan_mode = (U8) parm2;
	        break;
	    case 2: //get name
		    AssignBDName((char*)parm2);
	        break;
	}
	
}


void BtUICommunicate(U8 type, U32 *val)
{
    if(type == BTCOMM_GET_STACK_STATUS) //get stack status
		GAPGetBtStatus(type, val);
	else if(type == BTCOMM_GET_CONNECT_STATUS) //get connection status
    {
		*val = HFPConnectStatus();
    }
	else if(type == BTCOMM_GET_SCAN_MODE)
	{
	    *val = scan_mode;
	}
	else if(type == BTCOMM_SET_SCAN_MODE) //set scan mode
	{
		Bt__WriteScanEanbled((U8)val);
	}
	else if(type == BTCOMM_SET_PAGING)
	{
	    U8 addr[6];
	    if(Bt__EnumPairedDevice(addr))
			HFPConnectDisconnect(addr);
	}
}

//0:start 1:stop else: thread self
U8 RunSimulatorThread(BYTE execute)
{
	static HANDLE OSThread = (HANDLE)-1;

    if(execute == 0) //Start Thread
	{
		if(OSThread != (HANDLE) -1) //already runing
			return 2;
		
		DWORD dwThreadID;

#ifndef NO_BT
        if(!BTCoreStart(0)) //success
#endif
    	{
			UIInit(BtUICommunicate);
			OSThread = CreateThread((LPSECURITY_ATTRIBUTES)NULL,
			                          0,
			                          (LPTHREAD_START_ROUTINE)RunSimulatorThread,
			                          (LPVOID)3,
			                          0, &dwThreadID);
#ifndef NO_BT
			GAPInit(GapCB, PROFILE_TYPE_HFP);
#endif

			return 0;
         }
		return 1;
	}
	else if(execute == 1) //Stop Thread
	{
		if(OSThread != (HANDLE)-1)
		{
#ifndef NO_BT
			BTCoreStop();
#endif
		    TerminateThread(OSThread, 0);
			CloseHandle(OSThread);
		    OSThread = (HANDLE)-1;			
			return 0;
		}
	    return 1;
	}
	else if(execute == 2) //test if device unplug
	{
#ifndef NO_BT
	    return BTCoreHwHealth();
#endif
	}

    /////Thread Body/////////////////////

	while(1) 
    {
    /* BT part */
#ifndef NO_BT
        BTCoreLoop();
        switch(BTCoreState())
    	{
		    case STOP:
				Sleep(1000); //1 sec
				OutputDebugStringA("stopped\r\n");
				break;
		    case INITIALIZING:
				//Sleep(0);
				break;
		    case INITIALIZED:
				Sleep(1);
				break;
		    case BUSY:
				//Sleep(0);
				break;
		    case RXBUSY:
				//Sleep(0);
				break;
    	}
#endif
    /* UI part */
	UICoreLoop(input_key);
	input_key = KEY_NONE;
    Sleep(1);
    }
	return 0;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
BOOL CPresentorSimulatorDlg::OnInitDialog()
{
    CDialog::OnInitDialog();

    // Set the icon for this dialog.  The framework does this automatically
    //  when the application's main window is not a dialog
    SetIcon(m_hIcon, TRUE);			// Set big icon
    SetIcon(m_hIcon, FALSE);		// Set small icon

    // TODO: Add extra initialization here
    CRect rect;

    m_PanelDc = this->GetDlgItem(IDC_STATIC_PANEL)->GetDC();
    btpdlg = this;

    this->GetDlgItem(IDC_STATIC_PANEL)->GetClientRect(&rect);

    m_PanelDcH = rect.Height();
    m_PanelDcW = rect.Width();

    ZeroMemory(&m_PanelBmInfo,sizeof(BITMAPINFO2));
    m_PanelBmInfo.bmiHeader.biBitCount = 16;      //每?像素多少位，也可直接?24(RGB)或者32(RGBA)
    m_PanelBmInfo.bmiHeader.biCompression = BI_BITFIELDS;
    m_PanelBmInfo.bmiHeader.biHeight = -m_PanelDcH;
    m_PanelBmInfo.bmiHeader.biPlanes = 1;
    m_PanelBmInfo.bmiHeader.biSizeImage = 0;
    m_PanelBmInfo.bmiHeader.biClrUsed = 0;
    m_PanelBmInfo.bmiHeader.biSize=sizeof(BITMAPINFOHEADER);
    m_PanelBmInfo.bmiHeader.biWidth=m_PanelDcW;
    m_PanelBmInfo.bmiColors[0] = 0xF800;
    m_PanelBmInfo.bmiColors[1] = 0x07e0;
    m_PanelBmInfo.bmiColors[2] = 0x001f;

    /*
    	for(int i = 0; i < 65536; i++)
    	{
    	    m_PanelBmInfo.bmiColors[i].rgbBlue = i%32;
            m_PanelBmInfo.bmiColors[i].rgbGreen = (i >> 5) %64;
            m_PanelBmInfo.bmiColors[i].rgbRed = (i >> 11) % 32;
    		m_PanelBmInfo.bmiColors[i].rgbReserved = 0;
    	}
    	*/
    m_PanelBuf = new WORD[m_PanelDcH * m_PanelDcW];


    for(int i = 0; i < 320; i++)
        for(int j = 0; j < 240; j++)
            m_PanelBuf[i * 240 + j] = 0x0000; //black

#ifdef TAXEIA_UI_LOG
    StartHciLog(AfxGetInstanceHandle(), SW_SHOW);
#endif

    return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CPresentorSimulatorDlg::OnPaint()
{
    if (IsIconic())
    {
        CPaintDC dc(this); // device context for painting

        SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

        // Center icon in client rectangle
        int cxIcon = GetSystemMetrics(SM_CXICON);
        int cyIcon = GetSystemMetrics(SM_CYICON);
        CRect rect;
        GetClientRect(&rect);
        int x = (rect.Width() - cxIcon + 1) / 2;
        int y = (rect.Height() - cyIcon + 1) / 2;

        // Draw the icon
        dc.DrawIcon(x, y, m_hIcon);
    }
    else
    {
        CDialog::OnPaint();
    }

}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CPresentorSimulatorDlg::OnQueryDragIcon()
{
    return static_cast<HCURSOR>(m_hIcon);
}


void CPresentorSimulatorDlg::OnBnClickedButtonTest()
{
    // TODO: Add your control notification handler code here
#if 0
    CRect rect;
    char recstr[100];
    this->GetDlgItem(IDC_STATIC_PANEL)->GetWindowRect(&rect);

    sprintf(recstr, "w=%d h=%d\n", rect.Width(), rect.Height());
    OutputDebugStringA(recstr);

    this->GetDlgItem(IDC_STATIC_PANEL)->GetClientRect(&rect);
    sprintf(recstr, "w=%d h=%d\n", rect.Width(), rect.Height());
    OutputDebugStringA(recstr);

 
    PanelDrawFillBgColor(0, 0, 240, 20, 320, 0, 0);

    PanelDrawRect(30, 30, 40, 40);
    PanelDrawRect(35, 35, 30, 30);
    PanelDrawRect(40, 40, 20, 20);
    PanelDrawRect(45, 45, 10, 10);


    //PanelDrawCharOnBg(15, 15, 0x6211, 0, 20);
    PanelDrawImg(0, 0, 240, 320, 0, 0);
#endif
    RunSimulatorThread(0);

}

void CPresentorSimulatorDlg::OnCancel()
{
    // TODO: Add your specialized code here and/or call the base class
    CDialog::OnCancel();
	RunSimulatorThread(1);
    delete m_PanelBuf;
}


void CPresentorSimulatorDlg::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	// TODO: Add your message handler code here and/or call default

	if(nChar == VK_UP)
    	input_key = KEY_UP;
	else if(nChar == VK_DOWN)
    	input_key = KEY_DOWN;
	else if(nChar == VK_LEFT)
		input_key = KEY_LEFT;
	else if(nChar == VK_RIGHT)
		input_key = KEY_RIGHT;
	else if(nChar == VK_OEM_COMMA)
		input_key = KEY_LEFT2;
	else if(nChar == VK_OEM_2)
		input_key = KEY_RIGHT2;
	else if(nChar == VK_RETURN)
		input_key = KEY_OK;
	

	CDialog::OnKeyUp(nChar, nRepCnt, nFlags);
}

BOOL CPresentorSimulatorDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: Add your specialized code here and/or call the base class
    if( pMsg->message == WM_KEYUP)
	{
		return SendMessage(WM_KEYUP, pMsg->wParam, pMsg->lParam);
	}
	return CDialog::PreTranslateMessage(pMsg);
}

