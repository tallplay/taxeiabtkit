// PresentorSimulator.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols

extern "C" {
#include "CORE\BTCore.h"
#include "TaxeiaHciLog\TaxeiaHCILog.h"
#include "GAP\SECURITY\Bt__SecurityManager.h"
#include "hfp\Bt__Profile_HFP.h"
unsigned int MassUart_GetUniqueID(void);
DWORD AssignBDAddr(void);
void AssignLinkKeyFileName(char *name);

#include "Bttype.h"
#include "ili9341.h"
#include "UICore.h"
void PanelStartWriteData(void);
void PanelEndWRData(void);
void WritePixelData(U8 dat1, U8 dat2);
void PanelStartReadData(void);
void PanelSetCursorStartEnd(U8 xpos, U16 ypos, U8 xpos_end, U16 ypos_end);
void ReadPixelData(U8 *dat1, U8 *dat2);
void snor_read_open1(U32 pos);
void SNOR_desel(void);
U8 snor_read_one_byte(void);
}

// CPresentorSimulatorApp:
// See PresentorSimulator.cpp for the implementation of this class
//

class CPresentorSimulatorApp : public CWinApp
{
public:
	CPresentorSimulatorApp();

// Overrides
	public:
	virtual BOOL InitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
};

extern CPresentorSimulatorApp theApp;
