// PresentorSimulatorDlg.h : header file
//

#pragma once


typedef struct bitmap__info__
{
	   BITMAPINFOHEADER bmiHeader;
	   DWORD bmiColors[3];
}BITMAPINFO2;

// CPresentorSimulatorDlg dialog
class CPresentorSimulatorDlg : public CDialog
{
// Construction
public:
	CPresentorSimulatorDlg(CWnd* pParent = NULL);	// standard constructor
	CDC *m_PanelDc;
	int m_PanelDcW;
	int m_PanelDcH;
	unsigned short *m_PanelBuf;
	BITMAPINFO2 m_PanelBmInfo;

// Dialog Data
	enum { IDD = IDD_PRESENTORSIMULATOR_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;


	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButtonTest();
protected:
	virtual void OnCancel();
public:
    afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
};
