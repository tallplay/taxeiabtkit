#if !defined(AFX_BULKUSB_H)
#define AFX_BULKUSB_H

#include <devioctl.h>
#include <ntdddisk.h>
#include <ntddscsi.h>
#include <setupapi.h>
#include <initguid.h>

typedef struct _SCSI_PASS_THROUGH_WITH_BUFFERS {
    SCSI_PASS_THROUGH           spt;
    ULONG                       Filler;      // realign buffers to double word boundary
    UCHAR                       ucSenseBuf[32];
    UCHAR                       ucDataBuf[512];
} SCSI_PASS_THROUGH_WITH_BUFFERS, *PSCSI_PASS_THROUGH_WITH_BUFFERS;

typedef struct _SCSI_PASS_THROUGH_DIRECT_WITH_BUFFER {
    SCSI_PASS_THROUGH_DIRECT            sptd;
    ULONG                               Filler;      // realign buffer to double word boundary
    UCHAR                               ucSenseBuf[32];
} SCSI_PASS_THROUGH_DIRECT_WITH_BUFFER, *PSCSI_PASS_THROUGH_DIRECT_WITH_BUFFER;

typedef struct _CBWCB {
    UCHAR       cmddata[12];
} CBWCB;

//
// Command Descriptor Block constants.
//

#define CDB6GENERIC_LENGTH                   6
#define CDB10GENERIC_LENGTH                  10
#define CDB12GENERIC_LENGTH                                      12
#define CDB16GENERIC_LENGTH                                      16     

#define SETBITON                             1
#define SETBITOFF                            0

#define ATAPI_VENDOR_INFO	"LTTSS500"
#define ATAPI_PRODUCT_ID	"HM120"

class CBulkUSB {
  public:
                                CBulkUSB();
                                ~CBulkUSB();

    int                         open_device(char driveletter, int retry);
    int                         open_device(char driveletter);
    int                         open_device_by_name(char *unique_name);

    int                         close_device();
    int                         ReadCapacity(char *readdata);
    int                         TestUnitReady();
    int                         RequestSense();
    int                         FindNewDevice();
    int                         ResetDevice();
    int                         IsWriteable();  
    int                         CheckVerify();
    int                         Removal();
    int                         Eject();
    int                         Load();
    int                         GetNumber(PSTORAGE_DEVICE_NUMBER pdevicenum);
	int							GetCapacity();

	int							Execute(int bulkinout, int transferlength, char *cdb, char *databuf);
	int							Execute(int bulkinout, int *transferlength, char *cdb, char *databuf);

    int                         Inquiry(char *readdata);
    int                         Read10(const int lba, int transferlength,
                                       char *readdata);
    int                         Read12(int lba, int transferlength,
                                       char *readdata);
    int                         Write10(int lba, int transferlength,
                                        char *writedata);
    int                         Write12(int lba, int transferlength,
                                        char *writedata);
	int							Media2ND();
	int							Media2ND2();

    UINT                        last_lba;
    UINT                        block_length;

    // Sense Key
    char                        sensekey;
    // Additional sense key
    char                        ASC;
    // Additional sense key qualifier
    char                        ASCQ;

  private:
    HANDLE                      deviceHandle;
};

HANDLE OpenOneDevice(IN       HDEVINFO                    HardwareDeviceInfo,
                     IN       PSP_INTERFACE_DEVICE_DATA   DeviceInfoData,
                     IN           char *devName);
BOOL ExecCBWCB(HANDLE        handle, UCHAR *pCBWCB, UCHAR    DataIn,
               PVOID   DataBuffer, ULONG       DataBufferLen);

BOOL ExecCBWCB_RLEN(HANDLE        handle, UCHAR *pCBWCB, UCHAR    DataIn,
               PVOID   DataBuffer, ULONG       *DataBufferLen);

BOOL ExecCBWCB6(HANDLE        handle, UCHAR *pCBWCB, UCHAR    DataIn,
               PVOID   DataBuffer, ULONG       DataBufferLen);

BOOL ExecCBWCB10(HANDLE        handle, UCHAR *pCBWCB, UCHAR    DataIn,
               PVOID   DataBuffer, ULONG       DataBufferLen);



#endif
