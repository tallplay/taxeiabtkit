#ifndef __LOCAL_STORAGE_HH__
#define __LOCAL_STORAGE_HH__
#include "BtType.h"

void StorageInit(void);
void StorageDeInit(void);
void StorageGetBytes(U16 addr, U8 len, U8 *dat);
void StorageSetBytes(U16 addr, U8 len, U8 *dat);
void StorageClear(void);

#ifdef STORAGE_GET_ID_6_BYTES
void StorageGetID6Bytes(U8 *); //it was used for generate BT ADDR (BD ADDR in spec)
#else
U32 StorageGetID(void); //it was used for generate BT ADDR (BD ADDR in spec)
#endif
void StorageSetID(U32 id);
U32 StorageGetRandom(void);

#endif
