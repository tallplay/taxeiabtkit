#include "LocalOutput.h"
#include <windows.h>
#include <crtdbg.h>
#include <stdio.h>


void LocalOutput(const char *format, ...)
{
    char buffer[128];  /* Output buffer */

    va_list     args;

    va_start(args, format);
#ifdef WIN32
    _vsnprintf(buffer, 128, format, args);
#else
    vsnprintf(buffer, 128, format, args);
#endif
    va_end(args);

#ifdef _DEBUG
    _CrtDbgReport(0, NULL, 0, NULL, "%s", buffer);
#else
    OutputDebugStringA(buffer);
#endif

}

void LocalBreak(const char * file, int line, const char *exp, ...)
{
#ifdef _DEBUG
    if (_CrtDbgReport(2, file, line, 0, exp) == 1)
#endif
    {
        /* Halt the system */
        __asm { int 3 };
    }
}

