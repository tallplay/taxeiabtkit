//#include "stdafx.h"
#pragma once

#ifndef _SECURE_ATL
#define _SECURE_ATL 1
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers
#endif

// Modify the following defines if you have to target a platform prior to the ones specified below.
// Refer to MSDN for the latest info on corresponding values for different platforms.
#ifndef WINVER				// Allow use of features specific to Windows XP or later.
#define WINVER 0x0501		// Change this to the appropriate value to target other versions of Windows.
#endif

#ifndef _WIN32_WINNT		// Allow use of features specific to Windows XP or later.                   
#define _WIN32_WINNT 0x0501	// Change this to the appropriate value to target other versions of Windows.
#endif						

#ifndef _WIN32_WINDOWS		// Allow use of features specific to Windows 98 or later.
#define _WIN32_WINDOWS 0x0410 // Change this to the appropriate value to target Windows Me or later.
#endif

#ifndef _WIN32_IE			// Allow use of features specific to IE 6.0 or later.
#define _WIN32_IE 0x0600	// Change this to the appropriate value to target other versions of IE.
#endif

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// some CString constructors will be explicit

// turns off MFC's hiding of some common and often safely ignored warning messages
#define _AFX_ALL_WARNINGS

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions

#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

#include "bulkusb.h"



#include <usbdi.h>
#include <usbioctl.h>
#include <usb100.h>

static int ExecCBWRetry = 0;

CBulkUSB::CBulkUSB() {
    deviceHandle = INVALID_HANDLE_VALUE;
    last_lba = 0;
    block_length = 0;
}

CBulkUSB::~CBulkUSB() {
    /*
    if(deviceHandle)
        CloseHandle(deviceHandle);
        */
}

int
CBulkUSB::open_device(char driveletter) {
    int         r_value;
    char        buf[200];

    sprintf(buf, "\\\\.\\%c:", driveletter);
    deviceHandle = CreateFileA(buf, GENERIC_WRITE | GENERIC_READ,
                              FILE_SHARE_READ | FILE_SHARE_WRITE, NULL,
                              OPEN_EXISTING, 0, NULL);
    if (deviceHandle == INVALID_HANDLE_VALUE) {
        r_value = GetLastError();
        return 0;
    } else
        return 1;
}

int
CBulkUSB::open_device(char driveletter, int retry) {
    int         r_value;
    char        buf[200];

	ExecCBWRetry = retry;
    sprintf(buf, "\\\\.\\%c:", driveletter);
    deviceHandle = CreateFileA(buf, GENERIC_WRITE | GENERIC_READ,
                              FILE_SHARE_READ | FILE_SHARE_WRITE, NULL,
                              OPEN_EXISTING, 0, NULL);
    if (deviceHandle == INVALID_HANDLE_VALUE) {
        r_value = GetLastError();
        return 0;
    } else
        return 1;
}


HANDLE
OpenOneDevice(IN       HDEVINFO                    HardwareDeviceInfo,
              IN       PSP_INTERFACE_DEVICE_DATA   DeviceInfoData,
              IN           char *devName) {
    PSP_INTERFACE_DEVICE_DETAIL_DATA_A            functionClassDeviceData = NULL;
    ULONG                                       predictedLength         = 0;
    ULONG                                       requiredLength          = 0;
    HANDLE                                      hOut                    = INVALID_HANDLE_VALUE;


    //
    // allocate a function class device data structure to receive the
    // goods about this particular device.
    //
    SetupDiGetInterfaceDeviceDetail(HardwareDeviceInfo, DeviceInfoData, NULL, // probing so no output buffer yet
                                    0, // probing so output buffer length of zero
                                    & requiredLength, NULL); // not interested in the specific dev-node


    predictedLength = requiredLength;
    // sizeof (SP_FNCLASS_DEVICE_DATA) + 512;

    functionClassDeviceData = (struct _SP_DEVICE_INTERFACE_DETAIL_DATA_A *)
                              malloc(predictedLength);
    functionClassDeviceData->cbSize = sizeof(SP_INTERFACE_DEVICE_DETAIL_DATA);

    //
    // Retrieve the information from Plug and Play.
    //
    if (!SetupDiGetInterfaceDeviceDetailA(HardwareDeviceInfo, DeviceInfoData,
                                         functionClassDeviceData,
                                         predictedLength, &requiredLength,
                                         NULL)) {
        free(functionClassDeviceData);
        return INVALID_HANDLE_VALUE;
    }

    strcpy(devName, functionClassDeviceData->DevicePath) ;
    //AfxMessageBox(devName);
    //printf("Attempting to open %s\n", devName);

    hOut = CreateFileA(functionClassDeviceData->DevicePath,
                      GENERIC_READ |
                      GENERIC_WRITE,
                      FILE_SHARE_READ |
                      FILE_SHARE_WRITE, NULL, // no SECURITY_ATTRIBUTES structure
                      OPEN_EXISTING, // No special create flags
                      0, // No special attributes
                      NULL); // No template file

    if (INVALID_HANDLE_VALUE == hOut)
        printf("FAILED to open %s\n", devName);
    free(functionClassDeviceData);
    return hOut;
}

int
CBulkUSB::open_device_by_name(char *unique_name) {
    HDEVINFO                            hardwareDeviceInfo;
    SP_INTERFACE_DEVICE_DATA            deviceInfoData;
    ULONG                               i;
    SP_DEVINFO_DATA                     DeviceInfo;
    int                                 r_value;
    char                                device_name[512];
    char                                unique_name_temp[512];
    int                                 error_code;


    hardwareDeviceInfo = SetupDiGetClassDevs((LPGUID) & DiskClassGuid, NULL, // Define no enumerator (global)
                                             NULL, // Define no
                                             (DIGCF_PRESENT |                // Only Devices present
                                              DIGCF_INTERFACEDEVICE)); // Function class devices.

    deviceInfoData.cbSize = sizeof(SP_INTERFACE_DEVICE_DATA);   
    DeviceInfo.cbSize = sizeof(SP_DEVINFO_DATA);

    for (i = 0; i < 16; i++) {
        r_value = SetupDiEnumDeviceInfo(hardwareDeviceInfo, i, &DeviceInfo);
        if (r_value == 0)
            error_code = GetLastError();
        r_value = SetupDiGetDeviceRegistryProperty(hardwareDeviceInfo,
                                                   &DeviceInfo, SPDRP_DRIVER,
                                                   NULL,
                                                   (unsigned char *)
                                                   unique_name_temp,
                                                   150, NULL);   
        if (r_value == 0)
            error_code = GetLastError();

        if (strcmp(unique_name_temp, unique_name) == 0) {
            //SetupDiRemoveDevice(hardwareDeviceInfo,&DeviceInfo);

            if (SetupDiEnumDeviceInterfaces(hardwareDeviceInfo, 0, // We don't care about specific PDOs
                                            (LPGUID) & DiskClassGuid, i,
                                            &deviceInfoData)) {
                deviceHandle = OpenOneDevice(hardwareDeviceInfo,
                                             &deviceInfoData, device_name);

                if (deviceHandle == INVALID_HANDLE_VALUE) {
                    //return 0;
                    r_value = 0 ;
                    break;
                } else {
                    r_value = 1;
                    break;
                }
            } else {
                if (ERROR_NO_MORE_ITEMS == GetLastError()) {
                    //return 0;
                    r_value = 0 ;
                    break;
                }
            }
        }
    }

    SetupDiDestroyDeviceInfoList(hardwareDeviceInfo);
    return r_value;
}

int
CBulkUSB::close_device() {
    if (deviceHandle != INVALID_HANDLE_VALUE) {
        CloseHandle(deviceHandle);
        deviceHandle = INVALID_HANDLE_VALUE;	
        return 1;
    }
    return 1;
}




int
CBulkUSB::ReadCapacity(char *readdata) {
    int         r_value;
    CBWCB       mycbwcb;

    // Setup the 12 bytes of Security Access ARA CBWCB
    memset(&mycbwcb, 0x00, sizeof(CBWCB));
    mycbwcb.cmddata[0] = 0x25;
    r_value = ExecCBWCB(deviceHandle, (UCHAR *) &mycbwcb, SCSI_IOCTL_DATA_IN,
                        readdata, 8);
    if (r_value) {
        last_lba = ((unsigned char) readdata[0] << 24);
        last_lba += ((unsigned char) readdata[1] << 16);
        last_lba += ((unsigned char) readdata[2] << 8);         
        last_lba += ((unsigned char) readdata[3]);

        //last_lba = *((int *)readdata);
        block_length = (unsigned char) readdata[4] << 24;
        block_length += (unsigned char) readdata[5] << 16;
        block_length += (unsigned char) readdata[6] << 8;
        block_length += (unsigned char) readdata[7];
    }
    return r_value;
}

int
CBulkUSB::RequestSense() {
    int         r_value;
    CBWCB       mycbwcb;
    char        readdata[20];

    // Setup the 12 bytes of Request Sense CBWCB
    memset(&mycbwcb, 0x00, sizeof(CBWCB));
    mycbwcb.cmddata[0] = 0x03;
    mycbwcb.cmddata[4] = 0x12;

    r_value = ExecCBWCB(deviceHandle, (UCHAR *) &mycbwcb, SCSI_IOCTL_DATA_IN,
                        readdata, 18);
    if (r_value) {
        sensekey = readdata[2] & 15; 
        ASC = readdata[12];
        ASCQ = readdata[13];
    }
    return r_value;
}

int
CBulkUSB::Execute(int bulkinout, int *transferlength, char *cdb, char *databuf){

    int         r_value;
    
    CBWCB       mycbwcb;

    // Setup the 12 bytes of Request Sense CBWCB
    memset(&mycbwcb, 0x00, sizeof(CBWCB));
	memcpy(mycbwcb.cmddata, cdb, 12);

	if(bulkinout==0)
		r_value = ExecCBWCB_RLEN(deviceHandle, (UCHAR *) &mycbwcb, SCSI_IOCTL_DATA_IN,
			                databuf, (ULONG*)transferlength);
	else
		r_value = ExecCBWCB_RLEN(deviceHandle, (UCHAR *) &mycbwcb, SCSI_IOCTL_DATA_OUT,
			                databuf, (ULONG*)transferlength);
    return r_value;

}

int
CBulkUSB::Execute(int bulkinout, int transferlength, char *cdb, char *databuf){

    int         r_value;
    
    CBWCB       mycbwcb;

    // Setup the 12 bytes of Request Sense CBWCB
    memset(&mycbwcb, 0x00, sizeof(CBWCB));
	memcpy(mycbwcb.cmddata, cdb, 12);

	if(bulkinout==0)
		r_value = ExecCBWCB(deviceHandle, (UCHAR *) &mycbwcb, SCSI_IOCTL_DATA_IN,
			                databuf, transferlength);
	else
		r_value = ExecCBWCB(deviceHandle, (UCHAR *) &mycbwcb, SCSI_IOCTL_DATA_OUT,
			                databuf, transferlength);
    return r_value;

}

int
CBulkUSB::Read10(const int lba, int transferlength, char *readdata) {
    int         r_value;
    int         nblocks;
    CBWCB       mycbwcb;
    //char      readdata[20];
    nblocks = transferlength / 512;

    // Setup the 12 bytes of Request Sense CBWCB
    memset(&mycbwcb, 0x00, sizeof(CBWCB));
    mycbwcb.cmddata[0] = 0x28;
    mycbwcb.cmddata[2] = (lba & (0xFF000000)) >> 24;
    mycbwcb.cmddata[3] = (lba & (0xFF0000)) >> 16;
    mycbwcb.cmddata[4] = (lba & (0xFF00)) >> 8;
    mycbwcb.cmddata[5] = (lba & (0xFF));

    mycbwcb.cmddata[7] = (nblocks & (0xFF00)) >> 8;
    mycbwcb.cmddata[8] = (nblocks & (0xFF));


    r_value = ExecCBWCB(deviceHandle, (UCHAR *) &mycbwcb, SCSI_IOCTL_DATA_IN,
                        readdata, transferlength);
    return r_value;
}

int
CBulkUSB::Inquiry(char *readdata) {
    int         r_value;

    CBWCB       mycbwcb;

    // Setup the 12 bytes of Request Sense CBWCB
    memset(&mycbwcb, 0x00, sizeof(CBWCB));
    mycbwcb.cmddata[0] = 0x12;
    mycbwcb.cmddata[4] = 42;

    r_value = ExecCBWCB10(deviceHandle, (UCHAR *) &mycbwcb, SCSI_IOCTL_DATA_IN,
                        readdata, 42);
    return r_value;
}

int
CBulkUSB::Read12(const int lba, int transferlength, char *readdata) {
    int         r_value;
    int         nblocks;
    CBWCB       mycbwcb;
    //char      readdata[20];
    nblocks = transferlength / 512;

    // Setup the 12 bytes of Request Sense CBWCB
    memset(&mycbwcb, 0x00, sizeof(CBWCB));
    mycbwcb.cmddata[0] = 0xA8;
    mycbwcb.cmddata[2] = (lba & (0xFF000000)) >> 24;
    mycbwcb.cmddata[3] = (lba & (0xFF0000)) >> 16;
    mycbwcb.cmddata[4] = (lba & (0xFF00)) >> 8;
    mycbwcb.cmddata[5] = (lba & (0xFF));

    mycbwcb.cmddata[6] = (nblocks & (0xFF000000)) >> 24;
    mycbwcb.cmddata[7] = (nblocks & (0xFF0000)) >> 16;
    mycbwcb.cmddata[8] = (nblocks & (0xFF00)) >> 8;
    mycbwcb.cmddata[9] = (nblocks & (0xFF));

    r_value = ExecCBWCB(deviceHandle, (UCHAR *) &mycbwcb, SCSI_IOCTL_DATA_IN,
                        readdata, transferlength);
    return r_value;
}

int
CBulkUSB::Write10(const int lba, int transferlength, char *writedata) {
    int         r_value;
    int         nblocks;
    CBWCB       mycbwcb;
    //char      readdata[20];
    nblocks = transferlength / 512;

    // Setup the 12 bytes of Request Sense CBWCB
    memset(&mycbwcb, 0x00, sizeof(CBWCB));
    mycbwcb.cmddata[0] = 0x2A;
    mycbwcb.cmddata[2] = (lba & (0xFF000000)) >> 24;
    mycbwcb.cmddata[3] = (lba & (0xFF0000)) >> 16;
    mycbwcb.cmddata[4] = (lba & (0xFF00)) >> 8;
    mycbwcb.cmddata[5] = (lba & (0xFF));

    mycbwcb.cmddata[7] = (nblocks & (0xFF00)) >> 8;
    mycbwcb.cmddata[8] = (nblocks & (0xFF));


    r_value = ExecCBWCB(deviceHandle, (UCHAR *) &mycbwcb, SCSI_IOCTL_DATA_OUT,
                        writedata, transferlength);
    return r_value;
}


int
CBulkUSB::Write12(const int lba, int transferlength, char *writedata) {
    int         r_value;
    int         nblocks;
    CBWCB       mycbwcb;
    //char      readdata[20];
    nblocks = transferlength / 512;

    // Setup the 12 bytes of Request Sense CBWCB
    memset(&mycbwcb, 0x00, sizeof(CBWCB));
    mycbwcb.cmddata[0] = 0xAA;
    mycbwcb.cmddata[2] = (lba & (0xFF000000)) >> 24;
    mycbwcb.cmddata[3] = (lba & (0xFF0000)) >> 16;
    mycbwcb.cmddata[4] = (lba & (0xFF00)) >> 8;
    mycbwcb.cmddata[5] = (lba & (0xFF));

    mycbwcb.cmddata[6] = (nblocks & (0xFF000000)) >> 24;
    mycbwcb.cmddata[7] = (nblocks & (0xFF0000)) >> 16;
    mycbwcb.cmddata[8] = (nblocks & (0xFF00)) >> 8;
    mycbwcb.cmddata[9] = (nblocks & (0xFF));


    r_value = ExecCBWCB(deviceHandle, (UCHAR *) &mycbwcb, SCSI_IOCTL_DATA_OUT,
                        writedata, transferlength);
    return r_value;
}


int
CBulkUSB::TestUnitReady() {
    int         r_value;
    char        buf[32];

    CBWCB       mycbwcb;

    // Setup the 12 bytes of Security User Login CBWCB
    memset(&mycbwcb, 0x00, sizeof(CBWCB));
    mycbwcb.cmddata[0] = 0x00;
    r_value = ExecCBWCB(deviceHandle, (UCHAR *) &mycbwcb, SCSI_IOCTL_DATA_OUT,
                        buf, 0);

    return r_value;
}

int
CBulkUSB::Media2ND() {
    int         r_value;
    char        buf[32];

    CBWCB       mycbwcb;

    // Setup the 12 bytes of Security User Login CBWCB
    memset(&mycbwcb, 0x00, sizeof(CBWCB));
    mycbwcb.cmddata[0] = 0xCF;
	mycbwcb.cmddata[1] = 0x30;
	mycbwcb.cmddata[2] = 0xAA;
    r_value = ExecCBWCB6(deviceHandle, (UCHAR *) &mycbwcb, SCSI_IOCTL_DATA_OUT,
                        buf, 0);

    return r_value;
}

int
CBulkUSB::Media2ND2() {
    int         r_value;
    char        buf[32];

    CBWCB       mycbwcb;

    // Setup the 12 bytes of Security User Login CBWCB
    memset(&mycbwcb, 0x00, sizeof(CBWCB));
    mycbwcb.cmddata[0] = 0xCF;
	mycbwcb.cmddata[1] = 0x20;
	mycbwcb.cmddata[2] = 0x00;
	mycbwcb.cmddata[3] = 0x00;
	mycbwcb.cmddata[4] = 0x06;
	mycbwcb.cmddata[5] = 0xFC;
	mycbwcb.cmddata[6] = 0x08;
	mycbwcb.cmddata[7] = 0x70;
	mycbwcb.cmddata[8] = 0x41;
    r_value = ExecCBWCB10(deviceHandle, (UCHAR *) &mycbwcb, SCSI_IOCTL_DATA_OUT,
                        buf, 0);

    return r_value;
}



int
CBulkUSB::FindNewDevice() {
    BOOL        status  = FALSE;
    DWORD       BytesReturned;
    char        buf[64];
    int         error;

    status = DeviceIoControl(deviceHandle, IOCTL_DISK_FIND_NEW_DEVICES, buf,
                             64, buf, 64, &BytesReturned, FALSE);
    if (status == 0)
        error = GetLastError();
    return status;
}

int
CBulkUSB::CheckVerify() {
    BOOL        status  = FALSE;
    DWORD       BytesReturned;
    char        buf[64];
    int         error;

    status = DeviceIoControl(deviceHandle, IOCTL_STORAGE_CHECK_VERIFY, buf,
                             64, buf, 64, &BytesReturned, FALSE);
    if (status == 0)
        error = GetLastError();
    return status;
}



int
CBulkUSB::ResetDevice() {
    BOOL        status  = FALSE;
    DWORD       BytesReturned;
    char        buf[64];
    int         error;

    status = DeviceIoControl(deviceHandle, IOCTL_DISK_EJECT_MEDIA,
                                                                                                                                                                                                                                                                                                                                                           //IOCTL_STORAGE_RESET_BUS,
                             //IOCTL_STORAGE_RESET_DEVICE,
                             //IOCTL_SCSI_RESCAN_BUS,
                                                         //IOCTL_DISK_FIND_NEW_DEVICES,
                                                         //IOCTL_DISK_UPDATE_PROPERTIES,
                             buf, 64, buf, 64, &BytesReturned, FALSE);
    status = DeviceIoControl(deviceHandle, IOCTL_DISK_LOAD_MEDIA,
                                                                                                                                                                                                                                                                                                                                                           //IOCTL_STORAGE_RESET_BUS,
                             //IOCTL_STORAGE_RESET_DEVICE,
                             //IOCTL_SCSI_RESCAN_BUS,
                                                         //IOCTL_DISK_FIND_NEW_DEVICES,
                                                         //IOCTL_DISK_UPDATE_PROPERTIES,
                             buf, 64, buf, 64, &BytesReturned, FALSE);

    if (status == 0)
        error = GetLastError();
    return status;
}

int
CBulkUSB::Removal() {
    BOOL        status  = FALSE;
    DWORD       BytesReturned;
    char        buf[64];
    int         error;
    status = DeviceIoControl(deviceHandle, IOCTL_STORAGE_MEDIA_REMOVAL,
                                                                                                                                                                                                                                                                                                                                                           //IOCTL_DISK_MEDIA_REMOVAL,
                             buf, 64, buf, 64, &BytesReturned, FALSE);

    if (status == 0)
        error = GetLastError();
    return status;
}

int
CBulkUSB::Eject() {
    BOOL        status  = FALSE;
    DWORD       BytesReturned;
    char        buf[64];
    int         error;
    status = DeviceIoControl(deviceHandle, IOCTL_DISK_EJECT_MEDIA, buf, 64,
                             buf, 64, &BytesReturned, FALSE);
    if (status == 0)
        error = GetLastError();
    return status;
}

int
CBulkUSB::Load() {
    BOOL        status  = FALSE;
    DWORD       BytesReturned;
    char        buf[64];
    int         error;
    status = DeviceIoControl(deviceHandle, IOCTL_STORAGE_LOAD_MEDIA, buf, 64,
                             buf, 64, &BytesReturned, FALSE);
    if (status == 0)
        error = GetLastError();
    return status;
}


int
CBulkUSB::IsWriteable() {
    BOOL        status  = FALSE;
    DWORD       BytesReturned;
    //char      buf[64];
    int         error;

    status = DeviceIoControl(deviceHandle, IOCTL_DISK_IS_WRITABLE, NULL, 0,
                             NULL, 0, &BytesReturned, FALSE);
    if (status == 0)
        error = GetLastError();

    return status;
}

int
CBulkUSB::GetNumber(PSTORAGE_DEVICE_NUMBER pdevicenum) {
    BOOL        status  = FALSE;
    DWORD       BytesReturned;
    //char      buf[64];
    int         error;
    //pdevicenum = (PSTORAGE_DEVICE_NUMBER)malloc(sizeof(STORAGE_DEVICE_NUMBER));

    status = DeviceIoControl(deviceHandle, IOCTL_STORAGE_GET_DEVICE_NUMBER,
                             NULL, 0, pdevicenum,
                             sizeof(STORAGE_DEVICE_NUMBER), &BytesReturned,
                             FALSE);
    if (status == 0)
        error = GetLastError();

    return status;
}

int
CBulkUSB::GetCapacity() {

    BOOL        status  = FALSE;
    DWORD       BytesReturned;
    
    int         error;

	IO_SCSI_CAPABILITIES	ISC;

    status = DeviceIoControl(deviceHandle, IOCTL_SCSI_GET_CAPABILITIES,
                             &ISC, sizeof(ISC), &ISC,
                             sizeof(ISC), &BytesReturned,
                             FALSE);
    if (status == 0)
        error = GetLastError();

    return status;
}

BOOL
ExecCBWCB6(HANDLE        handle, UCHAR *pCBWCB, UCHAR    DataIn,
          PVOID   DataBuffer, ULONG       DataBufferLen) {
    SCSI_PASS_THROUGH_DIRECT_WITH_BUFFER        sptdwb;
    BOOL                                        status          = FALSE;
    BOOL                                        ReturnValue     = FALSE;
    ULONG                                       sptdwb_size     = 0;
    DWORD                                       BytesReturned; 

    ZeroMemory(&sptdwb, sizeof(SCSI_PASS_THROUGH_DIRECT_WITH_BUFFER));
    sptdwb.sptd.Length = sizeof(SCSI_PASS_THROUGH_DIRECT);
    //sptdwb.sptd.PathId = 0;
    //sptdwb.sptd.TargetId = 1;
    //sptdwb.sptd.Lun = 0;
    sptdwb.sptd.CdbLength = CDB6GENERIC_LENGTH;
    sptdwb.sptd.DataIn = DataIn;                                        // SCSI_IOCTL_DATA_IN or SCSI_IOCTL_DATA_OUT
    sptdwb.sptd.SenseInfoLength = 8;
    sptdwb.sptd.DataTransferLength = DataBufferLen;
    sptdwb.sptd.TimeOutValue = 150;
    sptdwb.sptd.DataBuffer = DataBuffer;
    sptdwb.sptd.SenseInfoOffset = offsetof(SCSI_PASS_THROUGH_DIRECT_WITH_BUFFER,
                                           ucSenseBuf);
    // SCSI-2 command descriptor block 

    memcpy(sptdwb.sptd.Cdb, pCBWCB, sizeof(CBWCB));
    sptdwb_size = sizeof(SCSI_PASS_THROUGH_DIRECT_WITH_BUFFER);
    status = DeviceIoControl(handle, IOCTL_SCSI_PASS_THROUGH_DIRECT, &sptdwb,
                             sptdwb_size, &sptdwb, sptdwb_size,
                             &BytesReturned, FALSE);
    if ((sptdwb.sptd.ScsiStatus == 0) && (status != 0))
        ReturnValue = TRUE;

    return ReturnValue;
}

BOOL
ExecCBWCB10(HANDLE        handle, UCHAR *pCBWCB, UCHAR    DataIn,
          PVOID   DataBuffer, ULONG       DataBufferLen) {
    SCSI_PASS_THROUGH_DIRECT_WITH_BUFFER        sptdwb;
    BOOL                                        status          = FALSE;
    BOOL                                        ReturnValue     = FALSE;
    ULONG                                       sptdwb_size     = 0;
    DWORD                                       BytesReturned; 

    ZeroMemory(&sptdwb, sizeof(SCSI_PASS_THROUGH_DIRECT_WITH_BUFFER));
    sptdwb.sptd.Length = sizeof(SCSI_PASS_THROUGH_DIRECT);
    //sptdwb.sptd.PathId = 0;
    //sptdwb.sptd.TargetId = 1;
    //sptdwb.sptd.Lun = 0;
    sptdwb.sptd.CdbLength = CDB10GENERIC_LENGTH;
    sptdwb.sptd.DataIn = DataIn;                                        // SCSI_IOCTL_DATA_IN or SCSI_IOCTL_DATA_OUT
    sptdwb.sptd.SenseInfoLength = 8;
    sptdwb.sptd.DataTransferLength = DataBufferLen;
    sptdwb.sptd.TimeOutValue = 150;
    sptdwb.sptd.DataBuffer = DataBuffer;
    sptdwb.sptd.SenseInfoOffset = offsetof(SCSI_PASS_THROUGH_DIRECT_WITH_BUFFER,
                                           ucSenseBuf);
    // SCSI-2 command descriptor block 

    memcpy(sptdwb.sptd.Cdb, pCBWCB, sizeof(CBWCB));
    sptdwb_size = sizeof(SCSI_PASS_THROUGH_DIRECT_WITH_BUFFER);
    status = DeviceIoControl(handle, IOCTL_SCSI_PASS_THROUGH_DIRECT, &sptdwb,
                             sptdwb_size, &sptdwb, sptdwb_size,
                             &BytesReturned, FALSE);
    if ((sptdwb.sptd.ScsiStatus == 0) && (status != 0))
        ReturnValue = TRUE;

    return ReturnValue;
}




BOOL
ExecCBWCB_RLEN(HANDLE        handle, UCHAR *pCBWCB, UCHAR    DataIn,
          PVOID   DataBuffer, ULONG       *DataBufferLen) {
    SCSI_PASS_THROUGH_DIRECT_WITH_BUFFER        sptdwb;
    BOOL                                        status          = FALSE;
    BOOL                                        ReturnValue     = FALSE;
    ULONG                                       sptdwb_size     = 0;
    DWORD                                       BytesReturned; 

    ZeroMemory(&sptdwb, sizeof(SCSI_PASS_THROUGH_DIRECT_WITH_BUFFER));
    sptdwb.sptd.Length = sizeof(SCSI_PASS_THROUGH_DIRECT);

	sptdwb.sptd.PathId = 0;
    sptdwb.sptd.TargetId = 1; 
    sptdwb.sptd.Lun = 0;

	sptdwb.sptd.CdbLength = CDB12GENERIC_LENGTH;
    sptdwb.sptd.DataIn = DataIn;                                        // SCSI_IOCTL_DATA_IN or SCSI_IOCTL_DATA_OUT
    sptdwb.sptd.SenseInfoLength = 8;
	sptdwb.sptd.DataTransferLength = (*DataBufferLen == 64) ? 65 : *DataBufferLen;
    sptdwb.sptd.TimeOutValue = 2; //1 s
    sptdwb.sptd.DataBuffer = DataBuffer;
    sptdwb.sptd.SenseInfoOffset = offsetof(SCSI_PASS_THROUGH_DIRECT_WITH_BUFFER,
                                           ucSenseBuf);
    // SCSI-2 command descriptor block 

    memcpy(sptdwb.sptd.Cdb, pCBWCB, sizeof(CBWCB));
    sptdwb_size = sizeof(SCSI_PASS_THROUGH_DIRECT_WITH_BUFFER);
    status = DeviceIoControl(handle, IOCTL_SCSI_PASS_THROUGH_DIRECT, &sptdwb,
                             sptdwb_size, &sptdwb, sptdwb_size,
                             &BytesReturned, FALSE);
    if ((sptdwb.sptd.ScsiStatus == 0) && (status != 0))
    {
        ReturnValue = TRUE;
    }

	if(sptdwb.sptd.DataTransferLength == 65)
		*DataBufferLen = 0;
	else
        *DataBufferLen = sptdwb.sptd.DataTransferLength;

    return ReturnValue;
}


BOOL
ExecCBWCB(HANDLE        handle, UCHAR *pCBWCB, UCHAR    DataIn,
          PVOID   DataBuffer, ULONG       DataBufferLen) {
    SCSI_PASS_THROUGH_DIRECT_WITH_BUFFER        sptdwb;
    BOOL                                        status          = FALSE;
    BOOL                                        ReturnValue     = FALSE;
    ULONG                                       sptdwb_size     = 0;
    DWORD                                       BytesReturned; 
    int retry = ExecCBWRetry;
    ZeroMemory(&sptdwb, sizeof(SCSI_PASS_THROUGH_DIRECT_WITH_BUFFER));
    sptdwb.sptd.Length = sizeof(SCSI_PASS_THROUGH_DIRECT);
    //sptdwb.sptd.PathId = 0;
    //sptdwb.sptd.TargetId = 1;
    //sptdwb.sptd.Lun = 0;
    sptdwb.sptd.CdbLength = CDB12GENERIC_LENGTH;
    sptdwb.sptd.DataIn = DataIn;                                        // SCSI_IOCTL_DATA_IN or SCSI_IOCTL_DATA_OUT
    sptdwb.sptd.SenseInfoLength = 8;
    sptdwb.sptd.DataTransferLength = DataBufferLen;
    sptdwb.sptd.TimeOutValue = 2;
    sptdwb.sptd.DataBuffer = DataBuffer;
    sptdwb.sptd.SenseInfoOffset = offsetof(SCSI_PASS_THROUGH_DIRECT_WITH_BUFFER,
                                           ucSenseBuf);
    // SCSI-2 command descriptor block 

    memcpy(sptdwb.sptd.Cdb, pCBWCB, sizeof(CBWCB));
    sptdwb_size = sizeof(SCSI_PASS_THROUGH_DIRECT_WITH_BUFFER);
	do
	{
    status = DeviceIoControl(handle, IOCTL_SCSI_PASS_THROUGH_DIRECT, &sptdwb,
                             sptdwb_size, &sptdwb, sptdwb_size,
                             &BytesReturned, FALSE);
        if ((sptdwb.sptd.ScsiStatus == 0) && (status != 0))
	    {
            ReturnValue = TRUE;
			break;
	    }
	}while(retry--);

    return ReturnValue;
}

BOOL
ExecCBW(HANDLE  handle, UCHAR *pCBW, UCHAR      DataIn, PVOID   DataBuffer,
        ULONG       DataBufferLen) {
    SCSI_PASS_THROUGH_DIRECT_WITH_BUFFER        sptdwb;
    BOOL                                        status          = FALSE;
    BOOL                                        ReturnValue     = FALSE;
    ULONG                                       sptdwb_size     = 0;
    DWORD                                       BytesReturned; 

    ZeroMemory(&sptdwb, sizeof(SCSI_PASS_THROUGH_DIRECT_WITH_BUFFER));
    sptdwb.sptd.Length = sizeof(SCSI_PASS_THROUGH_DIRECT);
    sptdwb.sptd.PathId = 0;
    sptdwb.sptd.TargetId = 1;
    sptdwb.sptd.Lun = 0;
    sptdwb.sptd.CdbLength = CDB16GENERIC_LENGTH;
    sptdwb.sptd.DataIn = DataIn;                                        // SCSI_IOCTL_DATA_IN or SCSI_IOCTL_DATA_OUT
    sptdwb.sptd.SenseInfoLength = 8;
    sptdwb.sptd.DataTransferLength = DataBufferLen;
    sptdwb.sptd.TimeOutValue = 150;
    sptdwb.sptd.DataBuffer = DataBuffer;
    sptdwb.sptd.SenseInfoOffset = offsetof(SCSI_PASS_THROUGH_DIRECT_WITH_BUFFER,
                                           ucSenseBuf);
    // SCSI-2 command descriptor block 

    memcpy(sptdwb.sptd.Cdb, pCBW, 16);
    sptdwb_size = sizeof(SCSI_PASS_THROUGH_DIRECT_WITH_BUFFER);
    status = DeviceIoControl(handle, IOCTL_SCSI_PASS_THROUGH_DIRECT, &sptdwb,
                             sptdwb_size, &sptdwb, sptdwb_size,
                             &BytesReturned, FALSE);
    if ((sptdwb.sptd.ScsiStatus == 0) && (status != 0))
        ReturnValue = TRUE;

    return ReturnValue;
}

