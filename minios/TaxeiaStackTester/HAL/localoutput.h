#ifndef __LOCAL_OUTPUT_HH_
#define __LOCAL_OUTPUT_HH_

void LocalOutput(const char *format, ...);
void LocalBreak(const char * file,  int line, const char *exp, ...);

#endif
