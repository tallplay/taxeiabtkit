#include "LocalStorage.h"
#include <windows.h>
#include "..\TaxeiaConsoleDebug\TaxeiaConsoleDebug.h"
#if 1 //file

static HANDLE storage_file = INVALID_HANDLE_VALUE;
static char storage_filename[250] = {0};
extern DWORD AssignBDAddr(void);
extern void AssignLinkKeyFileName(char *name);
extern BYTE AssigneTrim(void);

void StorageInit(void)
{
//    char *name;
//    U8 dat;
//    U32 read;
    if(storage_file != INVALID_HANDLE_VALUE)
    {
        CloseHandle(storage_file);
        storage_file = INVALID_HANDLE_VALUE;
    }
        
    while (storage_file == INVALID_HANDLE_VALUE) {

            //name = _tempnam(0, "TxiStorage");
            AssignLinkKeyFileName(storage_filename);
            //strcpy(storage_filename, name);
            strcat(storage_filename, ".tmp");
            //free(name);
        /* This Win32 call is similar to fopen() but with more control. */
        storage_file = CreateFileA(storage_filename, GENERIC_READ|GENERIC_WRITE,
                              0, 0, OPEN_EXISTING,
                              FILE_ATTRIBUTE_NORMAL|FILE_FLAG_RANDOM_ACCESS, 0);

        if(storage_file == INVALID_HANDLE_VALUE)
        {
            U8 tmp[8192];
            U32 written;
            storage_file = CreateFileA(storage_filename, GENERIC_READ|GENERIC_WRITE,
                                  FILE_SHARE_READ, 0, CREATE_ALWAYS,
                                  FILE_ATTRIBUTE_NORMAL|FILE_FLAG_RANDOM_ACCESS, 0);
            
            ASSERT(storage_file != INVALID_HANDLE_VALUE);

            memset(tmp, 0xff, 8192);
            WriteFile(storage_file, tmp, 8192, &written, 0);
            CloseHandle(storage_file);
            storage_file = INVALID_HANDLE_VALUE;
        }
    }
    
}

void StorageClear(void)
{
    U8 tmp[8192];
    U32 written;

    if(INVALID_SET_FILE_POINTER != SetFilePointer(storage_file, 0, 0, FILE_BEGIN))
    {
        memset(tmp, 0xff, 8192);
        WriteFile(storage_file, tmp, 8192, &written, 0);
    }

}
    
void StorageDeInit(void)
{
    if(storage_file != INVALID_HANDLE_VALUE)
    {
        ASSERT(CloseHandle(storage_file));
        storage_file = INVALID_HANDLE_VALUE;
    }
}


void StorageGetBytes(U16 addr, U8 len, U8 *dat)
{
    U32 read;
    if(INVALID_SET_FILE_POINTER != SetFilePointer(storage_file, addr, 0, FILE_BEGIN))
    {
        ReadFile(storage_file, dat, len, &read, 0);
    }
    else
    {
        memset(dat, 0xff, len);
    }
}


void StorageSetBytes(U16 addr, U8 len, U8 *dat)
{
    U32 written;
    SetFilePointer(storage_file, addr, 0, FILE_BEGIN);
    WriteFile(storage_file, dat, len, &written, 0);
}


#ifdef STORAGE_GET_ID_6_BYTES
void StorageGetID6Bytes(U8 *addr)
{
    U32 u32addr = AssignBDAddr();
    memset(addr, 0, 6);
    memcpy(&addr[0], &u32addr, 4);
}
#else
DWORD StorageGetID(void)
{
    return AssignBDAddr();
}
#endif

void StorageSetID(DWORD id)
{
    return;
}

DWORD StorageGetRandom()
{
    return 0x86868686;
}

#ifdef XO_TRIM

U8 StorageGetTrim(void)
{
    return AssigneTrim();
}
#endif
#else


#endif
