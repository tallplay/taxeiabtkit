#ifndef __SECUREKEY_TPL_
#define __SECUREKEY_TPL_

typedef unsigned char U8;
typedef unsigned short U16;

//return value 
enum
{
    RETCODE_SUCCESS = 0,
    RETCODE_DEVICE_FAIL = 1,
    RETCODE_COMMAND_INVALID = 2,
    RETCODE_AUTHFAIL = 3,
    RETCODE_READFAIL = 4,
    RETCODE_WRITEFAIL = 5
};

int MassUart_Close(void);
int MassUart_Open(void);
int MassUart_Recv(U8 *buf, U8 *len);
int MassUart_Send(U8 *buf, U16 *len);
int MassUart_Execute(int bulkinout, int *transferlength, char *cdb, char *databuf);
int MassUart_RecvLen(U8 *buf, U16 *len);
int MassUart_TxCnt(U16 *cnt);
int MassUart_Test(void);
unsigned int MassUart_GetUniqueID(void);
void MtkPower(U8 on);

#endif
