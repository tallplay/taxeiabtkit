/****************************************************************************
 *
 * File:
 *     $Workfile:HCI3WDat.c$ for iAnywhere Blue SDK, Version 2.2
 *     $Revision:14$
 *
 * Description: This file is part of an HCI 3-Wire implementation.  It 
 * handles the conversion of data to and from the framed format.  
 *
 * Created:     October 31, 2006
 *
 * Copyright 2006-2007 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions,  
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "sys/HCI3WDat.h"

/****************************************************************************
 *
 * Prototypes
 *
 ****************************************************************************/

static BOOL h3wDatDeHCI(Hci3WDatStruct *cP, 
                        HciBuffStruct   hB,
                        FrameHdrStruct *hdrNewP,
                        Hci3WErrStruct *errObjP);

static BOOL h3wDatDeHCIBytes(Hci3WDatStruct *cP, 
                             HciBuffStruct   hB, 
                             FrameHdrStruct *hdrInfoPtr,
                             Hci3WErrStruct *errObjP);

static BOOL h3wDatEnHCI(Hci3WDatStruct  *cP, 
                        HciBuffStruct    hB, 
                        PACKET_TYPE_ENUM packetType,
                        BOOL             sendReliably, 
                        BOOL             includeCRC,
                        Hci3WErrStruct  *errObjP);

/****************************************************************************
 *
 * Public Functions
 *
 ****************************************************************************/



/*---------------------------------------------------------------------------
 * h3wDatInit()
 *     Initializes the HCI 3-wire data frame module.  This function must be 
 *     invoked before any of the other HCI 3-wire data frame functions will 
 *     work correctly.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire data frame module context structure.  
 *     crcObjP - Pointer to an HCI 3-wire cyclic redundancy check module 
 *         context structure.  
 *     evtObjP - Pointer to an HCI 3-wire event handling module context 
 *         structure.  
 *     hdrObjP - Pointer to an HCI 3-wire frame header module context 
 *         structure.  
 *     memObjP - Pointer to an HCI 3-wire memory module context structure.  
 *     slpObjP - Pointer to an HCI 3-wire slip module context structure.  
 *     txqObjP - Pointer to an HCI 3-wire transmit queue module context 
 *         structure.  
 *     utlObjP - Pointer to an HCI 3-wire utilities module context structure.  
 */
void h3wDatInit(Hci3WDatStruct *cP,
                Hci3WCrcStruct *crcObjP,
                Hci3WEvtStruct *evtObjP, 
                Hci3WHdrStruct *hdrObjP, 
                Hci3WMemStruct *memObjP, 
                Hci3WSlpStruct *slpObjP, 
                Hci3WTxQStruct *txqObjP, 
                Hci3WUtlStruct *utlObjP)
{
    cP->slipState = SS_UNINIT;
    cP->privCRCObjP = crcObjP;
    cP->privEvtObjP = evtObjP;
    cP->privHdrObjP = hdrObjP;
    cP->privMemObjP = memObjP;
    cP->privSlpObjP = slpObjP;
    cP->privTXQObjP = txqObjP;
    cP->privUtlObjP = utlObjP;
}



/*---------------------------------------------------------------------------
 * h3wDatSlipLinking()
 *     When an HCI 3-wire link operation commences, invoking this function 
 *     sets the data layer state data to the appropriate values.  This 
 *     functions signals the lower slip layer that linking has commenced.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire data frame module context structure.  
 *     slipStateP - This parameter must point to a location where a slip 
 *         state enumeration can be written.  
 */
void h3wDatSlipLinking(Hci3WDatStruct *cP, SLIP_STATE_ENUM *slipStateP)
{
    UART_STATE_ENUM currentUARTState;

    Assert(SS_PARTIAL_INIT == cP->slipState);

    /* Signal the lower slip layer that linking has commenced.  */ 
    h3wSlpUARTInitialized(cP->privSlpObjP, &currentUARTState);

    Assert(US_WORKING == currentUARTState);
    cP->slipState = SS_LINKING;
    *slipStateP = cP->slipState;
}



/*---------------------------------------------------------------------------
 * h3wDatSlipSetSpeed()
 *     This function redefines the speed at which the Slip Layer is to 
 *     operate.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire data frame module context structure.  
 *     bps - This is the new bits per second rate that is to be used.
 *     errObjP - A pointer to an error context structure.  
 *
 * Returns:
 *     This function returns TRUE if it was successfully able to set the Slip 
 *     Layer data transfer speed, or FALSE otherwise.  
 */
BOOL h3wDatSlipSetSpeed(Hci3WDatStruct *cP, U32 bps, Hci3WErrStruct *errObjP) 
{
    return(h3wSlpUARTSetSpeed(cP->privSlpObjP, bps, errObjP));
}



/*---------------------------------------------------------------------------
 * h3wDatSlipResetLink()
 *     When an active HCI 3-wire session is reset, invoking this function 
 *     sets the data layer state data to the appropriate values.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire data frame module context structure.  
 *     slipStateP - This parameter must point to a location where a slip 
 *         state enumeration can be written.  
 */
void h3wDatSlipResetLink(Hci3WDatStruct *cP, SLIP_STATE_ENUM *slipStateP)
{
    Assert(SS_ACTIVE == cP->slipState);
    cP->slipState = SS_LINKING;
    *slipStateP = cP->slipState;
}



/*---------------------------------------------------------------------------
 * h3wDatSlipActivated()
 *     When an HCI 3-wire session finishes linking successfully, invoking 
 *     this function sets the data layer state data to the appropriate values.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire data frame module context structure.  
 *     slipStateP - This parameter must point to a location where a slip 
 *         state enumeration can be written.  
 */
void h3wDatSlipActivated(Hci3WDatStruct *cP, SLIP_STATE_ENUM *slipStateP)
{
    Assert(SS_LINKING == cP->slipState);
    cP->slipState = SS_ACTIVE;
    *slipStateP = cP->slipState;
}



/*---------------------------------------------------------------------------
 * h3wDatSlipInit()
 *     This function signals the lower slip layer that it should begin 
 *     initializing the UART, and sets the data layer state data to the 
 *     appropriate values.
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire data frame module context structure.  
 *     slipStateP - This parameter must point to a location where a slip 
 *         state enumeration can be written.  
 */
void h3wDatSlipInit(Hci3WDatStruct *cP, SLIP_STATE_ENUM *slipStateP)
{
    UART_STATE_ENUM currentUARTState;

    Assert(SS_UNINIT == cP->slipState);

    /* Signal the lower slip layer that it 
     * should begin initializing the UART 
     */ 
    h3wSlpUARTInit(cP->privSlpObjP, &currentUARTState);

    Assert(  (US_UNINIT       == currentUARTState) 
          || (US_PARTIAL_INIT == currentUARTState)
          || (US_WORKING      == currentUARTState));
    switch(currentUARTState) {
    case US_PARTIAL_INIT:
        cP->slipState = SS_PARTIAL_INIT;
        break;

    case US_WORKING:
        cP->slipState = SS_LINKING;
        break;

    case US_UNINIT:
    default:
        cP->slipState = SS_UNINIT;
    }
    *slipStateP = cP->slipState;
}



/*---------------------------------------------------------------------------
 * h3wDatSend()
 *     This function checks for conditions that prevent transmissions, gets 
 *     enough memory to frame the given HCI 3-wire message, creates an HCI 
 *     3-wire framed version of the message, invokes the lower layer slip 
 *     transmission routine, notes that this latest transmission has 
 *     acknowledged all of the outstanding received messages, adds the newly 
 *     transmitted message to the retransmit queue (if it was reliable), then 
 *     frees up the memory it just allocated.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire data frame module context structure.  
 *     handleP - a pointer to a location where a data frame handle can be 
 *         written.  
 *     isLowPowerMsg - This parameter must be TRUE if the message is a low 
 *         power message, or FALSE otherwise.  
 *     dataBuffP - This parameter must point to an HCI 3-wire message.  
 *     packetType - This parameter must contain an enumeration corresponding 
 *         to the HCI 3-wire packet type.  
 *     sendReliably - This parameter must be TRUE if the HCI 3-wire message 
 *         is relaiable, or FALSE otherwise.  
 *     includeCRC - This parameter must be TRUE if the HCI 3-wire message is 
 *         to have a CRC value appended to the end of the payload, or FALSE 
 *         otherwise.  
 *     errObjP - A pointer to an error context structure.  
 *
 * Returns:
 *     This function returns TRUE if it was successfully able to transmit the 
 *     message, or FALSE otherwise.  
 */
BOOL h3wDatSend(Hci3WDatStruct  *cP, 
                U8              *handleP,
                BOOL             isLowPowerMsg,
                BuffUseStruct   *dataBuffP,
                PACKET_TYPE_ENUM packetType,
                BOOL             sendReliably, 
                BOOL             includeCRC,
                Hci3WErrStruct  *errObjP)
{
    BOOL          h3wBuffAllocated;
    BuffUseStruct hbs;
    HciBuffStruct hB;

    *handleP = '\0';
    hB.dP = dataBuffP;
    hB.hP = &hbs;
    h3wBuffAllocated = FALSE;
    Assert((SS_LINKING == cP->slipState) || (SS_ACTIVE == cP->slipState));

    /* Check for conditions that prevent transmissions.  */ 
    switch(packetType) {
    case PT_CMD:
    case PT_ACL:
    case PT_EVENT:
        if (TRUE != sendReliably) {
            h3wErrSet(errObjP, ES_DAT_SEND, ER_BAD_PKT_RELIABILITY);
        }
        break;

    case PT_SYNCH:
        if (  TRUE 
           == h3wUtlGetFlowControl(cP->privUtlObjP,
                                   FL_HCI_SYNCHRONOUS, 
                                   FT_CURRENT_STATE)) {
            if (TRUE != sendReliably) {
                h3wErrSet(errObjP, 
                          ES_DAT_SEND, 
                          ER_BAD_PKT_RELIABILITY);
            }
        } else {
            if (TRUE == sendReliably) {
                h3wErrSet(errObjP, 
                          ES_DAT_SEND, 
                          ER_BAD_PKT_RELIABILITY);
            }
        }
        break;

    default:
        break;
    }
    if (TRUE == h3wErrHasNone(errObjP)) {
        switch(packetType) {
        case PT_CMD:
            if (HR_HOST != h3wUtlGetRole(cP->privUtlObjP)) {
                h3wErrSet(errObjP, ES_DAT_SEND, ER_BAD_PKT_ROLE);
            }
            break;

        case PT_EVENT:
            if (HR_CONTROLLER != h3wUtlGetRole(cP->privUtlObjP)) {
                h3wErrSet(errObjP, ES_DAT_SEND, ER_BAD_PKT_ROLE);
            }

        default:
            break;
        }
    }
    if (TRUE == h3wErrHasNone(errObjP)) {
        if (  TRUE 
           == h3wHdrAtSlidingLimit(cP->privHdrObjP, 
                                   h3wTXQNumNodesInQ(cP->privTXQObjP))) {
            h3wErrSet(errObjP, ES_DAT_SEND, ER_SLIDING_WINDOW_FULL);
        }
    }

    if (TRUE == h3wErrHasNone(errObjP)) {

        /* Get enough memory to frame the given HCI 3-wire message.  */ 
        if (TRUE == includeCRC) {
            hB.hP->maxSize = (U16)(hB.dP->maxSize + 6);
        } else {
            hB.hP->maxSize = (U16)(hB.dP->maxSize + 4);
        }
        if (  TRUE 
           == h3wMemGet( cP->privMemObjP, 
                        &hB.hP->buf, 
                         hB.hP->maxSize, 
                         errObjP)) {
            h3wBuffAllocated = TRUE;
        }

    }
    if (TRUE == h3wErrHasNone(errObjP)) {

        /* Create an HCI 3-wire framed version of the given message.  */ 
        hB.hP->used = 0;
        h3wDatEnHCI(cP, hB, packetType, sendReliably, includeCRC, errObjP);

    }
    if (TRUE == h3wErrHasNone(errObjP)) {

        /* Invoke the lower layer slip transmission routine.  */ 
        h3wSlpSend(cP->privSlpObjP, 
                   isLowPowerMsg, 
                   hB.hP->buf, 
                   hB.hP->used, 
                   errObjP);

    }
    if (TRUE == h3wErrHasNone(errObjP)) {

        /* Note that this latest transmission has acknowledged 
         * all of the outstanding received messages.  
         */ 
        h3wHdrClearNumRXNeedingACK(cP->privHdrObjP);

        /* If this was a reliable message, 
         * add it to the retransmit queue.  
         */ 
        if (TRUE == sendReliably) {
            h3wTXQPush(cP->privTXQObjP, 
                       handleP, 
                       hB.hP->buf, 
                       hB.hP->used, 
                       errObjP);
        }

    }

    /* Free up the memory it just allocated.  */ 
    if (TRUE == h3wBuffAllocated) {
        h3wMemFree(cP->privMemObjP, hB.hP->buf, errObjP);
    }

    return(h3wErrHasNone(errObjP));
}



/*---------------------------------------------------------------------------
 * h3wDatPureAck()
 *     This function transmits a pure acknowledgement message, as long as the 
 *     out of frame software flow control is not choked off.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire data frame module context structure.  
 *     errObjP - A pointer to an error context structure.  
 *
 * Returns:
 *     If MemCopy actually had a return I'd list it here. Otherwise this 
 *     section would not exist. Return values and descriptions would be
 *     listed much like the "Parameters" section.
 */
BOOL h3wDatPureAck(Hci3WDatStruct *cP, Hci3WErrStruct *errObjP)
{
    BuffUseStruct ackMsg;
    U8            dummyBuff;
    U8            handle;

    /* Don't try to send the message if the out of frame 
     * software flow control is choked off, because 
     * such an attempt will only result in an error.  
     */ 
    if (  TRUE 
       == h3wUtlGetFlowControl(cP->privUtlObjP,
                               FL_OUT_OF_FRAME_SOFTWARE, 
                               FT_CURRENT_STATE)) {

        ackMsg.buf     = &dummyBuff;
        ackMsg.maxSize = 1;
        ackMsg.used    = 0;
        h3wDatSend( cP, 
                   &handle, 
                    FALSE, 
                   &ackMsg, 
                    PT_ACK, 
                    FALSE, 
                    FALSE, 
                    errObjP);
    }
    return(h3wErrHasNone(errObjP));
}



/*---------------------------------------------------------------------------
 * h3wDatReceive()
 *     This function checks for conditions that prevent data reception, gets 
 *     enough memory to hold an HCI 3-wire framed message, invokes the lower 
 *     layer slip receive routine, creates an unframed version of the HCI 
 *     3-wire framed message, and then frees up the memory it just allocated.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire data frame module context structure.  
 *     dataBuffP - This parameter must point to a location where a buffer use 
 *         structure can be written.  
 *     hdrNewP - This parameter must point to a location where a frame header 
 *         structure can be written.  
 *     errObjP - A pointer to an error context structure.  
 *
 * Returns:
 *     If MemCopy actually had a return I'd list it here. Otherwise this 
 *     section would not exist. Return values and descriptions would be
 *     listed much like the "Parameters" section.
 */
BOOL h3wDatReceive(Hci3WDatStruct *cP, 
                   BuffUseStruct  *dataBuffP,
                   FrameHdrStruct *hdrNewP,
                   Hci3WErrStruct *errObjP)
{
    BOOL          h3wBuffAllocated;
    BuffUseStruct hbs;
    HciBuffStruct hB;

    hB.hP                 = &hbs;
    hB.dP                 = dataBuffP;
    hB.dP->used           = 0;
    hdrNewP->payldLen     = 0;
    hdrNewP->hdrChecksum  = 0;
    hdrNewP->seqNum       = SE_0;
    hdrNewP->ackNum       = SE_0;
    hdrNewP->packetType   = PT_ACK;
    hdrNewP->includesCRC  = FALSE;
    hdrNewP->sentReliably = FALSE;
    h3wBuffAllocated      = FALSE;

    /* Check for conditions that prevent data reception.  */ 
    if ((SS_LINKING != cP->slipState) && (SS_ACTIVE != cP->slipState)) {
        h3wErrSet(errObjP, ES_DAT_RECEIVE, ER_NOT_INIT);
    }
    if (TRUE == h3wErrHasNone(errObjP)) {
        if (0 >= hB.dP->maxSize) {
             h3wErrSet(errObjP, ES_DAT_RECEIVE, ER_PAYLOAD_TOO_SMALL);
        }
    }

    if (TRUE == h3wErrHasNone(errObjP)) {

        /* Get enough memory to hold an HCI 3-wire framed message.  */ 
        if (TRUE == h3wMemGet( cP->privMemObjP,
                              &hB.hP->buf, 
                               hB.dP->maxSize + 6, 
                               errObjP)) {
            h3wBuffAllocated = TRUE;
        }

    }
    if (TRUE == h3wErrHasNone(errObjP)) {

        /* Invoke the lower layer slip receive routine.  */ 
        hB.hP->maxSize = (U16)(hB.dP->maxSize + 6);
        hB.hP->used = 0;
        h3wSlpReceive(cP->privSlpObjP, hB.hP, errObjP);

    }
    if (TRUE == h3wErrHasNone(errObjP)) {

        /* Create an unframed version of the HCI 3-wire framed message.  */ 
        if (0 < hB.hP->used) {
            h3wDatDeHCI(cP, hB, hdrNewP, errObjP);
        }

    }

    /* Free up the memory it just allocated.  */ 
    if (TRUE == h3wBuffAllocated) {
        h3wMemFree(cP->privMemObjP, hB.hP->buf, errObjP);
    }

    /* Don't signal that more receive data is pending 
     * here, as this will be done by the slip decoder when 
     * it puts message fragments into the receive queue.
     */ 

    return(h3wErrHasNone(errObjP));
}



/*---------------------------------------------------------------------------
 * h3wDatSlipTerminated()
 *     When an HCI 3-wire link is terminated, invoking this function sets the 
 *     data layer state data to the appropriate values.  This functions 
 *     signals the lower slip layer that link has terminated.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire data frame module context structure.  
 *     slipStateP - This parameter must point to a location where a slip 
 *         state enumeration can be written.  
 */
void h3wDatSlipTerminated(Hci3WDatStruct *cP, SLIP_STATE_ENUM *slipStateP)
{
    UART_STATE_ENUM currentUARTState;

    Assert(SS_PARTIAL_FREE == cP->slipState);

    /* Signal the lower slip layer that the link has been terminated.  */ 
    h3wSlpUARTTerminated(cP->privSlpObjP, &currentUARTState);

    Assert(US_UNINIT == currentUARTState);
    cP->slipState = SS_UNINIT;
    *slipStateP = cP->slipState;
}



/*---------------------------------------------------------------------------
 * h3wDatSlipFree()
 *     This function signals the lower slip layer that it should begin 
 *     freeing the UART, sets the data layer state data to the appropriate 
 *     values, and frees up the transmit queue and the frame header objects.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire data frame module context structure.  
 *     slipStateP - This parameter must point to a location where a slip 
 *         state enumeration can be written.  
 *     errObjP - A pointer to an error context structure.  
 */
BOOL h3wDatSlipFree(Hci3WDatStruct  *cP, 
                    SLIP_STATE_ENUM *slipStateP,
                    Hci3WErrStruct  *errObjP)
{
    UART_STATE_ENUM currentUARTState;

    Assert((SS_LINKING == cP->slipState) || (SS_ACTIVE == cP->slipState));

    /* Signal the lower slip layer that it should begin freeing the UART.  */ 
    h3wSlpUARTFree(cP->privSlpObjP, &currentUARTState);

    Assert(  (US_WORKING      == currentUARTState) 
          || (US_PARTIAL_FREE == currentUARTState) 
          || (US_UNINIT       == currentUARTState));
    switch(currentUARTState) {
    case US_WORKING:
        Assert((SS_LINKING == cP->slipState) || (SS_ACTIVE == cP->slipState));
        break;

    case US_PARTIAL_FREE:
        cP->slipState = SS_PARTIAL_FREE;
        break;

    case US_UNINIT:
    default:
        cP->slipState = SS_UNINIT;
    }
    *slipStateP = cP->slipState;

    /* Free up the transmit queue and the frame header objects.  */ 
    h3wTXQFree(cP->privTXQObjP, errObjP);
    h3wHdrFree(cP->privHdrObjP);

    return(h3wErrHasNone(errObjP));
}



/*---------------------------------------------------------------------------
 * h3wDatTerm()
 *     This function destroys a given slip module, cleaning up any allocated 
 *     resources.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire slip module context structure.  
 */
void h3wDatTerm(Hci3WDatStruct *cP)
{

    /* Don't try to call h3wDatSlipFree() here if 
     * slipInitialized is TRUE, because the call would need to 
     * go in a loop until the status was successful or timed out, 
     * resulting in an unacceptable delay for shut down code.  
     */ 

    cP->privCRCObjP = (Hci3WCrcStruct *)0;
    cP->privHdrObjP = (Hci3WHdrStruct *)0;
    cP->privEvtObjP = (Hci3WEvtStruct *)0;
    cP->privMemObjP = (Hci3WMemStruct *)0;
    cP->privSlpObjP = (Hci3WSlpStruct *)0;
    cP->privTXQObjP = (Hci3WTxQStruct *)0;
    cP->privUtlObjP = (Hci3WUtlStruct *)0;

}



/****************************************************************************
 *
 * Private Functions
 *
 ****************************************************************************/



/*---------------------------------------------------------------------------
 * h3wDatEnHCI()
 *     This function creates a framed version of the given HCI 3-wire message.  
 *     It loops through each byte of the HCI 3-wire data frame, as it is 
 *     created, adding the frame header, copying the payload bytes, and 
 *     appending the cyclic redundancy check.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire slip module context structure.  
 *     hB - This is an HCI buffer structure containing information about the 
 *         source message and containing a place where the information about 
 *         the destination framed message can be written.  
 *     packetType - This parameter must contain an enumeration corresponding 
 *         to the HCI 3-wire packet type.  
 *     sendReliably - This parameter must be TRUE if the packet is to be sent 
 *         reliably, or FALSE otherwise.  
 *     includeCRC - This parameter must be TRUE if a cyclic redundancy check 
 *         should be appended to the end of the payload, or FALSE otherwise.  
 *     errObjP - A pointer to an error context structure.  
 *
 * Returns:
 *     This function returns TRUE if it successfully created a framed version 
 *     of the given HCI 3-wire message, or FALSE otherwise.  
 */
static BOOL h3wDatEnHCI(Hci3WDatStruct  *cP, 
                        HciBuffStruct    hB, 
                        PACKET_TYPE_ENUM packetType,
                        BOOL             sendReliably, 
                        BOOL             includeCRC,
                        Hci3WErrStruct  *errObjP)
{
    BOOL h3wEncoding;
    U8   crcNdx;
    U8   nextByte;
    U8   useSizeIncr;
    U16  crc16CCIT;
    U16  datNdx;

    hB.hP->used = 0;
    if (h3wUtlGetMaxPayloadBytes(cP->privUtlObjP) < hB.dP->used) {
        h3wErrSet(errObjP, ES_DAT_ENHCI, ER_PAYLOAD_TOO_LARGE);
    }
    if (TRUE == h3wErrHasNone(errObjP)) {
        datNdx      = 0;
        crcNdx      = 0;
        crc16CCIT   = 0;
        useSizeIncr = 4;

        /* Loop through each byte of the HCI 
         * 3-wire data frame, as it is created.  
         */ 
        do {
            h3wEncoding = FALSE;
            Assert(hB.hP->maxSize >= (hB.hP->used + useSizeIncr));
            if (0 == hB.hP->used) {

                /* Add the frame header.  */ 
                h3wHdrSet(cP->privHdrObjP,
                          hB.hP->buf + hB.hP->used,
                          sendReliably, 
                          includeCRC,
                          packetType, 
                          hB.dP->used,
                          errObjP);

            } else {
                if (datNdx < hB.dP->used) {

                    /* Copy the payload bytes.  */ 
                    nextByte = *(hB.dP->buf + datNdx);
                    datNdx += 1;

                } else {

                    /* Append the CRC.  */ 
                    if (0 == crcNdx) {
                        crc16CCIT 
                            = h3wCRCCalc(cP->privCRCObjP,
                                         hB.hP->buf, 
                                         hB.hP->used);
                        nextByte = (U8)((crc16CCIT & 0xFF00) >> 8);
                    } else {
                        nextByte = (U8)(crc16CCIT & 0xFF);
                    }
                    crcNdx += 1;

                }
                *(hB.hP->buf + hB.hP->used) = nextByte;
            }

            /* Determine whether another loop iteration is needed.  */ 
            if (TRUE == h3wErrHasNone(errObjP)) {
                hB.hP->used = (U16)(hB.hP->used + useSizeIncr);
                useSizeIncr = 1;
                if (datNdx < hB.dP->used) {
                    h3wEncoding = TRUE;
                } else {
                    if (TRUE == includeCRC) {
                        if (2 > crcNdx) {
                            h3wEncoding = TRUE;
                        }
                    }
                }
            }

        } while (h3wEncoding);

    }
    return(h3wErrHasNone(errObjP));
}



/*---------------------------------------------------------------------------
 * h3wDatDeHCIBytes()
 *     This function creates an unframed version of an HCI 3-wire framed 
 *     message.  It verifies the payload size and the cyclic redundancy check 
 *     (if present).
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire slip module context structure.  
 *     hB - This is an HCI buffer structure containing information about the 
 *         source framed message and containing a place where the information 
 *         about the destination unframed message can be written.  
 *     hdrInfoPtr - This parameter must point to a frame header structure 
 *         containing information extracted from the header bytes in the 
 *         framed message.  
 *     errObjP - A pointer to an error context structure.  
 *
 * Returns:
 *     This function returns TRUE if it was successfully able to create an 
 *     unframed version of the given HCI 3-wire message.  
 */
static BOOL h3wDatDeHCIBytes(Hci3WDatStruct *cP, 
                             HciBuffStruct   hB, 
                             FrameHdrStruct *hdrInfoPtr,
                             Hci3WErrStruct *errObjP)
{
    BOOL h3wDecoding;
    U8   csOffset;
    U16  crc16CCIT;
    U16  ndx;
    U16  receivedCRC;

    if (TRUE == hdrInfoPtr->includesCRC) {
        csOffset = 2;
    } else {
        csOffset = 0;
    }
    ndx = 4;

    /* Loop through each byte in the unframed message, as it is 
     * created.  Verify the frame was correct after the last byte.  
     */ 
    do {
        h3wDecoding = FALSE;
        if (ndx + csOffset < hB.hP->used) {

            /* Copy the payload bytes.  */ 
            Assert(hB.dP->maxSize > hB.dP->used);
            *(hB.dP->buf + hB.dP->used) = *(hB.hP->buf + ndx);
            hB.dP->used += 1;
            ndx += 1;
            h3wDecoding = TRUE;

        } else {
            if (ndx + csOffset == hB.hP->used) {

                /* Verify the payload size.  */ 
                if (hdrInfoPtr->payldLen == hB.dP->used) {

                    /* Verify the cyclic redundancy check.  */ 
                    if (TRUE == hdrInfoPtr->includesCRC) {
                        crc16CCIT 
                            = h3wCRCCalc(cP->privCRCObjP, 
                                         hB.hP->buf, 
                                         ndx);
                        receivedCRC 
                            = (U16)( ((*(hB.hP->buf + ndx    )) << 8)
                                   | ( *(hB.hP->buf + ndx + 1)      ));
                        if (receivedCRC != crc16CCIT) {
                            h3wErrSet(errObjP,
                                      ES_DAT_DEHCIBYTES, 
                                      ER_CRC_ERROR);
                        }
                    }

                } else {
                    h3wErrSet(errObjP,
                              ES_DAT_DEHCIBYTES, 
                              ER_PAYLOAD_SIZE_MISMATCH);
                }
            } else {

                /* This will happen when a message is received that 
                 * should have a CRC and zero payload bytes, and the 
                 * last CRC byte is not included in the received bytes.
                 */ 
                h3wErrSet(errObjP, 
                          ES_DAT_DEHCIBYTES, 
                          ER_TRUNCATED_HCI_MSG);

            }
        }
    } while (h3wDecoding);

    return(h3wErrHasNone(errObjP));
}



/*---------------------------------------------------------------------------
 * h3wDatDeHCI()
 *     This function verifies the correctness of the frame data in an HCI 
 *     3-wire framed message, and then creates an unframed version of an HCI 
 *     3-wire framed message.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire slip module context structure.  
 *     hB - This is an HCI buffer structure containing information about the 
 *         source framed message and containing a place where the information 
 *         about the destination unframed message can be written.  
 *     hdrNewP - This parameter must point to a frame header structure 
 *         containing information extracted from the header bytes in the 
 *         framed message.  
 *     errObjP - A pointer to an error context structure.  
 *
 * Returns:
 *     This function returns TRUE if it was successfully able to create an 
 *     unframed version of a given HCI 3-wire framed message, or FALSE 
 *     otherwise.  
 */
static BOOL h3wDatDeHCI(Hci3WDatStruct *cP, 
                        HciBuffStruct   hB, 
                        FrameHdrStruct *hdrNewP,
                        Hci3WErrStruct *errObjP)
{
    BOOL isNewActiveRX;

    hB.dP->used           = 0;
    hdrNewP->hdrChecksum  = 0;
    hdrNewP->payldLen     = 0;
    hdrNewP->ackNum       = SE_0;
    hdrNewP->seqNum       = SE_0;
    hdrNewP->includesCRC  = FALSE;
    hdrNewP->sentReliably = FALSE;
    hdrNewP->packetType   = PT_ACK;

    /* Verify that the message is minimally 
     * long enough to contain a frame.  
     */ 
    if (4 > hB.hP->used) {
        h3wErrSet(errObjP, ES_DAT_DEHCI, ER_TRUNCATED_HCI_MSG);
    }

    if (TRUE == h3wErrHasNone(errObjP)) {

        /* Extract the header information from the framed header bytes.  */ 
        if (SS_ACTIVE == cP->slipState) {
            isNewActiveRX = TRUE;
        } else {
            isNewActiveRX = FALSE;
        }
        h3wHdrGet(cP->privHdrObjP, 
                  hB.hP->buf, 
                  hdrNewP, 
                  isNewActiveRX, 
                  errObjP);

    }
    if (TRUE == h3wErrHasNone(errObjP)) {

        /* Verify that the reliability is appropriate for 
         * the packet type and flow control being used.  
         */ 
        switch(hdrNewP->packetType) {
        case PT_CMD:
        case PT_ACL:
        case PT_EVENT:
            if (FALSE == hdrNewP->sentReliably) {
                h3wErrSet(errObjP, ES_DAT_DEHCI, ER_BAD_PKT_RELIABILITY);
            }
            break;

        case PT_SYNCH:
            if (  TRUE 
               == h3wUtlGetFlowControl(cP->privUtlObjP,
                                       FL_HCI_SYNCHRONOUS, 
                                       FT_CURRENT_STATE)) {
                if (FALSE == hdrNewP->sentReliably) {
                    h3wErrSet(errObjP, 
                              ES_DAT_DEHCI, 
                              ER_BAD_PKT_RELIABILITY);
                }
            } else {
                if (TRUE == hdrNewP->sentReliably) {
                    h3wErrSet(errObjP, 
                              ES_DAT_DEHCI, 
                              ER_BAD_PKT_RELIABILITY);
                }
            }
            break;

        default:
            break;
        }

    }
    if (TRUE == h3wErrHasNone(errObjP)) {

        /* Verify that the packet type is 
         * appropriate for the role being used.  
         */ 
        switch(hdrNewP->packetType) {
        case PT_CMD:
            if (HR_HOST == h3wUtlGetRole(cP->privUtlObjP)) {
                h3wErrSet(errObjP, ES_DAT_DEHCI, ER_BAD_PKT_ROLE);
            }
            break;

        case PT_EVENT:
            if (HR_CONTROLLER == h3wUtlGetRole(cP->privUtlObjP)) {
                h3wErrSet(errObjP, ES_DAT_DEHCI, ER_BAD_PKT_ROLE);
            }
            break;

        default:
            break;
        }

    }
    if (TRUE == h3wErrHasNone(errObjP)) {

        /* Create an unframed version of the 
         * given HCI 3-wire framed message.  
         */ 
        h3wDatDeHCIBytes(cP, hB, hdrNewP, errObjP);

    }
    return(h3wErrHasNone(errObjP));
}
