/****************************************************************************
 *
 * File:
 *     $Workfile:HCI3WSlp.c$ for iAnywhere Blue SDK, Version 2.2
 *     $Revision:14$
 *
 * Description: This file is part of an HCI 3-Wire implementation.  It 
 * handles the conversion of data between the framed format and the enslipped 
 * format, and interacts with the UART port.  
 *
 * Created:     October 31, 2006
 *
 * Copyright 2006-2007 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions,  
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "osapi.h"
#include "sys/HCI3WSlp.h"

/****************************************************************************
 *
 * RAM Data
 *
 ****************************************************************************/

Hci3WSlpStruct *hci3wSlpP = (Hci3WSlpStruct *)0;

/****************************************************************************
 *
 * Prototypes
 *
 ****************************************************************************/

static BOOL h3wSlpDeSlip(Hci3WSlpStruct *cP, 
                         SlipBuffStruct  dB, 
                         Hci3WErrStruct *errObjP);

static BOOL h3wSlpDeSlipChar(Hci3WSlpStruct *cP, 
                             SlipBuffStruct  dB, 
                             U16             ndx,
                             BOOL           *slipDecodingP,
                             Hci3WErrStruct *errObjP);

static BOOL h3wSlpEnSlip(Hci3WSlpStruct *cP, 
                         SlipBuffStruct  dB, 
                         Hci3WErrStruct *errObjP);

static BOOL h3wSlpPrivUARTInitialized(Hci3WSlpStruct *cP, 
                                      Hci3WErrStruct *errObjP);

static BOOL h3wSlpUARTSetSpeedE(Hci3WSlpStruct *cP, 
                                U32             bps, 
                                BOOL            allowPartial,
                                BOOL           *uartSpeedChangedP, 
                                Hci3WErrStruct *errObjP);

static void h3wSlpHandleUARTEvent(Hci3WSlpStruct *cP, 
                                  UartEvent       eventFromUART);

static void h3wSlpPrivUARTTerminated(Hci3WSlpStruct *cP);

static void h3wSlpSetDeslipState(Hci3WSlpStruct *cP, 
                                 U8              inputChar);

static void h3wSlpSetUARTState(Hci3WSlpStruct *cP, 
                               UART_STATE_ENUM newState);

/****************************************************************************
 *
 * Public Functions
 *
 ****************************************************************************/



/*---------------------------------------------------------------------------
 * h3wSlpUARTEventCallback()
 *     This function is invoked by the UART code when an event occurs that may 
 *     be of interest.  Unfortunately, we need some context information.  
 *     Since the UART software does not currently support this notion, we are 
 *     forced to work around it by using context data we store in a RAM 
 *     location.   Note that this workaround will only work in systems using a 
 *     single UART port.  If multiple UART ports are in use, then the UART 
 *     code will have to be modified to provide context information.  Once 
 *     this routine has the context data, it invokes the UART event handling 
 *     routine.  
 * 
 * Parameters:
 *     event - An enumeration corresponding to the event that occurred.  
 */
void h3wSlpUARTEventCallback(UartEvent event) 
{
    if ((Hci3WSlpStruct *)0 != hci3wSlpP) {
        h3wSlpHandleUARTEvent(hci3wSlpP, event);
    }
}



/*---------------------------------------------------------------------------
 * h3wSlpInit()
 *     Initializes the HCI 3-wire slip module.  This function must be invoked 
 *     before any of the other HCI 3-wire slip functions will work correctly.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire slip module context structure.  
 *     evtObjP - Pointer to an HCI 3-wire event handling module context 
 *         structure.  
 *     hdrObjP - Pointer to an HCI 3-wire frame header module context 
 *         structure.  
 *     memObjP - Pointer to an HCI 3-wire memory module context structure.  
 *     utlObjP - Pointer to an HCI 3-wire utilities module context structure.  
 */
void h3wSlpInit(Hci3WSlpStruct *cP,
                Hci3WEvtStruct *evtObjP, 
                Hci3WHdrStruct *hdrObjP, 
                Hci3WMemStruct *memObjP,
                Hci3WUtlStruct *utlObjP)
{
    cP->privEvtObjP         = evtObjP;
    cP->privHdrObjP         = hdrObjP;
    cP->privMemObjP         = memObjP;
    cP->privUtlObjP         = utlObjP;
    h3wRXQInit(&cP->slpRXQObj, 
                cP->privMemObjP, 
                cP->privUtlObjP);
    cP->uart.state          = US_UNINIT;
    cP->uart.allowDualEvent = FALSE;
    cP->deslipState         = SD_UNKNOWN;

    /* It makes no sense to have multiple slip objects for 
     * a single UART, so make sure this is the only one.  
     */ 
    Assert((Hci3WSlpStruct *)0 == hci3wSlpP);

    hci3wSlpP               = cP;
}



/*---------------------------------------------------------------------------
 * h3wSlpUARTInit()
 *     This function invokes the UART software initialization routine.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire slip module context structure.  
 *     uartStateP - This parameter must point to a location where the 
 *         resulting UART state can be written.  
 */
void h3wSlpUARTInit(Hci3WSlpStruct *cP, UART_STATE_ENUM *uartStateP)
{
    BtStatus       uartStatus;
    Hci3WErrStruct errObj;

    Assert(US_UNINIT == cP->uart.state);
    cP->uart.allowDualEvent = TRUE;
    uartStatus = UART_Init(h3wSlpUARTEventCallback);
    switch(uartStatus) {
    case BT_STATUS_SUCCESS:
        h3wErrInit(&errObj);
        h3wSlpPrivUARTInitialized(cP, &errObj);
        h3wErrReportUnsolicited(&errObj, TRUE, " UART Start Success");
        h3wErrTerm(&errObj);
        break;

    case BT_STATUS_PENDING:
        h3wSlpSetUARTState(cP, US_PARTIAL_INIT);
        break;

    case BT_STATUS_FAILED:
    default:
        cP->uart.allowDualEvent = FALSE;
        h3wErrInit(&errObj);
        h3wErrSet(&errObj, ES_SLP_UARTINIT, ER_UART_FAILURE);
        h3wErrReportUnsolicited(&errObj, FALSE, " UART Start");
        h3wErrTerm(&errObj);
    }
    *uartStateP = cP->uart.state;
}



/*---------------------------------------------------------------------------
 * h3wSlpUARTInitialized()
 *     This function invokes the routine that indicates the UART has been 
 *     successfully initialized.  It reports any resulting errors.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire slip module context structure.  
 *     uartStateP - This parameter must point to a location where the 
 *         resulting UART state can be written.  
 */
void h3wSlpUARTInitialized(Hci3WSlpStruct *cP, UART_STATE_ENUM *uartStateP)
{
    Hci3WErrStruct errObj;

    h3wErrInit(&errObj);
    if (TRUE == h3wSlpPrivUARTInitialized(cP, &errObj)) {
        *uartStateP = cP->uart.state;
    }
    h3wErrReportUnsolicited(&errObj, TRUE, " UART Initialized");
    h3wErrTerm(&errObj);
}



/*---------------------------------------------------------------------------
 * h3wSlpUARTFree()
 *     This function invokes the UART software shutdown routine.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire slip module context structure.  
 *     uartStateP - This parameter must point to a location where the 
 *         resulting UART state can be written.  
 */
void h3wSlpUARTFree(Hci3WSlpStruct *cP, UART_STATE_ENUM *uartStateP) 
{
    BtStatus       uartStatus;
    Hci3WErrStruct errObj;

    Assert(US_WORKING == cP->uart.state);
    cP->uart.allowDualEvent = TRUE;
    uartStatus = UART_Shutdown();
    switch(uartStatus) {
    case BT_STATUS_SUCCESS:
        h3wSlpPrivUARTTerminated(cP);
        break;

    case BT_STATUS_PENDING:
        h3wSlpSetUARTState(cP, US_PARTIAL_FREE);
        break;

    case BT_STATUS_FAILED:
    default:
        cP->uart.allowDualEvent = FALSE;
        h3wErrInit(&errObj);
        h3wErrSet(&errObj, ES_SLP_UARTFREE, ER_UART_FAILURE);
        h3wErrReportUnsolicited(&errObj, FALSE, " UART Shut Down Start");
        h3wErrTerm(&errObj);
    }
    *uartStateP = cP->uart.state;
}



/*---------------------------------------------------------------------------
 * h3wSlpUARTTerminated()
 *     This function invokes the routine that indicates the UART has been 
 *     successfully shutdown.  It reports any resulting errors.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire slip module context structure.  
 *     uartStateP - This parameter must point to a location where the 
 *         resulting UART state can be written.  
 */
void h3wSlpUARTTerminated(Hci3WSlpStruct *cP, UART_STATE_ENUM *uartStateP)
{
    h3wSlpPrivUARTTerminated(cP);
    *uartStateP = cP->uart.state;
}



/*---------------------------------------------------------------------------
 * h3wSlpResend()
 *     This function verifies that the semaphore is already held, sends the 
 *     given data, and clears the number of messages needing acknowledgements.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire slip module context structure.  
 *     h3wBuff - This parameter must point to a buffer to be transmitted.  
 *     h3wSize - This parameter must contain the size of the given buffer.  
 *     errObjP - A pointer to an error context structure.
 *
 * Returns:
 *     This function returns TRUE if it was successfully able to retransmit 
 *     the given message, or FALSE otherwise.  
 */
BOOL h3wSlpResend(Hci3WSlpStruct *cP, 
                  U8             *h3wBuff, 
                  U16             h3wSize, 
                  Hci3WErrStruct *errObjP)
{
    BOOL semaphore;

    /* The semaphore must already be held by the code that 
     * is looping through the messages to be retransmitted.  The 
     * semaphore prevents the code from processing transmission complete 
     * events before the messages being resent can be transmitted.
     */ 
    semaphore = h3wEvtGetSemaphore(cP->privEvtObjP);
    Assert(FALSE == semaphore);

    if (TRUE == h3wSlpSend(cP, FALSE, h3wBuff, h3wSize, errObjP)) {
        h3wHdrClearNumRXNeedingACK(cP->privHdrObjP);
    }

    /* Any transmission events that arrived during the time the 
     * semaphore is held will be handled by the code that is 
     * looping through the messages to be retransmitted.  So 
     * the event process routine should not be called here.  
     */ 

    return(h3wErrHasNone(errObjP));
}



/*---------------------------------------------------------------------------
 * h3wSlpSend()
 *     This function checks for conditionss that prevent transmissions, gets 
 *     enough memory to slip encode the given framed HCI 3-wire message, 
 *     creates a slip encoded version the message, invokes the UART write 
 *     routine, and then frees up the memory it just allocated.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire slip module context structure.  
 *     isLowPowerMsg - This parameter must be TRUE if the message is a low 
 *         power message, or FALSE otherwise.  
 *     h3wBuff - This parameter must point to an HCI 3-wire framed message.  
 *     h3wSize - This parameter must contain the size of the given buffer.  
 *     errObjP - A pointer to an error context structure.
 *
 * Returns:
 *     This function returns TRUE if it was successfully able to transmit the 
 *     message, or FALSE otherwise.  
 */
BOOL h3wSlpSend(Hci3WSlpStruct *cP, 
                BOOL            isLowPowerMsg,
                U8             *h3wBuff, 
                U16             h3wSize, 
                Hci3WErrStruct *errObjP)
{
    BOOL           slipBuffAllocated;
    BuffUseStruct  hbs;
    BuffUseStruct  sbs;
    U16            numSent;
    SlipBuffStruct sB;

    Assert(US_WORKING == cP->uart.state);
    sB.hP = &hbs;
    sB.sP = &sbs;
    slipBuffAllocated = FALSE;

    /* Check for conditions that prevent transmissions.  */ 
    if (  TRUE 
       != h3wUtlGetFlowControl(cP->privUtlObjP,
                               FL_OUT_OF_FRAME_SOFTWARE, 
                               FT_CURRENT_STATE)) {
        h3wErrSet(errObjP, ES_SLP_SEND, ER_XOFF);
    }
    if (TRUE != isLowPowerMsg) {
        if (TRUE != h3wUtlGetOtherAssumedAwake(cP->privUtlObjP)) {
            h3wErrSet(errObjP, ES_SLP_SEND, ER_NOT_AWAKE);
        }
    }

    if (TRUE == h3wErrHasNone(errObjP)) {

        /* Get enough memory to slip encode the 
         * given framed HCI 3-wire message.  
         */ 
        sB.hP->buf = h3wBuff;
        sB.hP->maxSize = h3wSize;
        sB.hP->used = h3wSize;
        if (  TRUE 
           == h3wMemGet( cP->privMemObjP, 
                        &sB.sP->buf, 
                         2 + sB.hP->used * 2, 
                         errObjP)) {
            slipBuffAllocated = TRUE;
        }

    }
    if (TRUE == h3wErrHasNone(errObjP)) {

        /* Create a slip encoded version the message.  */ 
        sB.sP->maxSize = (U16)(2 + sB.hP->used * 2);
        sB.sP->used = 0;
        h3wSlpEnSlip(cP, sB, errObjP);

    }
    if (TRUE == h3wErrHasNone(errObjP)) {

        /* Invoke the UART write routine.  */ 
        numSent = UART_Write((const U8 *)sB.sP->buf, (U16)sB.sP->used);
        if (numSent < sB.sP->used) {
            h3wErrSet(errObjP, ES_SLP_SEND, ER_SENT_TOO_FEW);
        } else {
            if (numSent > sB.sP->used) {
                h3wErrSet(errObjP, ES_SLP_SEND, ER_SENT_TOO_MANY);
            }
        }

    }

    /* Free up the memory it just allocated.  */ 
    if (TRUE == slipBuffAllocated) {
        h3wMemFree(cP->privMemObjP, sB.sP->buf, errObjP);
    }

    return(h3wErrHasNone(errObjP));
}



/*---------------------------------------------------------------------------
 * h3wSlpReceive()
 *     This function allocates memory for the received data, reads any left 
 *     over fragments from before, reads any newly arrived UART data, creates 
 *     a deslipped version of the slip encoded message, and then frees up the 
 *     newly allocated memory.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire slip module context structure.  
 *     buP - This parameter must point to a location where a buffer use 
 *         structure can be written.  
 *     errObjP - A pointer to an error context structure.
 *
 * Returns:
 *     This function returns TRUE if it was successfully able to receive a 
 *     message, or FALSE otherwise.  
 */
BOOL h3wSlpReceive(Hci3WSlpStruct *cP, 
                   BuffUseStruct  *buP, 
                   Hci3WErrStruct *errObjP)
{
    BOOL           slipBuffAllocated;
    BuffUseStruct  hbs;
    BuffUseStruct  sbs;
    SlipBuffStruct sB;
    U16            uartUsed;

    sB.hP = &hbs;
    sB.sP = &sbs;
    slipBuffAllocated = FALSE;
    buP->used = 0;
    Assert(US_WORKING == cP->uart.state);

    /* Allocate memory for the received data.  */ 
    if (  TRUE 
       == h3wMemGet( cP->privMemObjP, 
                    &sB.sP->buf, 
                     2 + 2 * buP->maxSize, 
                     errObjP)) {
        slipBuffAllocated = TRUE;
    }

    if (TRUE == h3wErrHasNone(errObjP)) {

        /* Read any left over fragments from before. */ 
        sB.sP->maxSize = (U16)(2 + 2 * buP->maxSize);
        sB.sP->used = 0;
        sB.hP->buf = buP->buf;
        sB.hP->maxSize = buP->maxSize;
        sB.hP->used = 0;
        h3wRXQGetAll(&cP->slpRXQObj, sB.sP, errObjP);

    }
    if (TRUE == h3wErrHasNone(errObjP)) {
        if (sB.sP->used < sB.sP->maxSize) {

            /* Read any newly arrived UART data. */ 
            uartUsed = UART_Read((U8 *)(sB.sP->buf + sB.sP->used), 
                                 (U16)(sB.sP->maxSize - sB.sP->used));
            sB.sP->used = (U16)(sB.sP->used + uartUsed);

        }

        /* Create a deslipped version of the 
         * slip encoded HCI 3-wire message.  
         */ 
        h3wSlpDeSlip(cP, sB, errObjP);

    }
    if (TRUE == h3wErrHasNone(errObjP)) {
        buP->used = sB.hP->used;
    }

    /* Free up the newly allocated memory.  */ 
    if (TRUE == slipBuffAllocated) {
        h3wMemFree(cP->privMemObjP, sB.sP->buf, errObjP);
    }

    return(h3wErrHasNone(errObjP));
}



/*---------------------------------------------------------------------------
 * h3wSlpUARTSetSpeed()
 *     This function redefines the speed at which the UART is to operate.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire slip module context structure.  
 *     bps - The new bits per second rate at which the UART is to operate.  
 *     errObjP - A pointer to an error context structure.
 *
 * Returns:
 *     This function returns TRUE if it was successfully able to set the UART 
 *     data transfer speed, or FALSE otherwise.  
 */
BOOL h3wSlpUARTSetSpeed(Hci3WSlpStruct *cP, U32 bps, Hci3WErrStruct *errObjP) 
{
    BOOL uartSpeedChanged;

    return(h3wSlpUARTSetSpeedE(cP, bps, FALSE, &uartSpeedChanged, errObjP));
}



/*---------------------------------------------------------------------------
 * h3wSlpTerm()
 *     This function destroys a given slip module, cleaning up any allocated 
 *     resources.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire slip module context structure.  
 */
void h3wSlpTerm(Hci3WSlpStruct *cP)
{
    hci3wSlpP = (Hci3WSlpStruct *)0;
    h3wRXQTerm(&cP->slpRXQObj);
}



/****************************************************************************
 *
 * Private Functions
 *
 ****************************************************************************/



/*---------------------------------------------------------------------------
 * h3wSlpHandleUARTEvent()
 *     This function processes events received from the UART.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire slip module context structure.  
 *     eventFromUART - An enumeration corresponding to the event that 
 *         occurred.  
 */
static void h3wSlpHandleUARTEvent(Hci3WSlpStruct *cP, UartEvent eventFromUART)
{
    EventStruct event;

    switch(cP->uart.state) {
    case US_PARTIAL_INIT:
        if (UE_INIT_COMPLETE == eventFromUART) {
            event.type = ET_UART_INIT;
            event.data = 0;
            h3wEvtQueue(cP->privEvtObjP, event);
        }
        break;

    case US_WORKING:
        switch(eventFromUART) {
        case UE_DATA_TO_READ:

            /* If the callback routine takes a long time (such as while 
             * reading the received data) then another UE_DATA_TO_READ 
             * event may occur.  If so, we don't want to start handling 
             * the second event before the first event handling is 
             * complete.  The req() function chains multiple event 
             * handling sequentially so that they don't clobber each other.
             */ 
            event.type = ET_RX_UART;
            event.data = 0;
            h3wEvtQueue(cP->privEvtObjP, event);

            break;

        case UE_WRITE_COMPLETE:
            event.type = ET_TX_WRITE_COMPLETE;
            event.data = 0;
            h3wEvtQueue(cP->privEvtObjP, event);
            break;

        case UE_SHUTDOWN_COMPLETE:
            if (TRUE == cP->uart.allowDualEvent) {
                cP->uart.allowDualEvent = FALSE;
                event.type = ET_UART_FREE;
                event.data = 0;
                h3wEvtQueue(cP->privEvtObjP, event);
            }
            break;

        default:
            break;
        }
        break;

    case US_PARTIAL_FREE:
        if (UE_SHUTDOWN_COMPLETE == eventFromUART) {
            event.type = ET_UART_FREE;
            event.data = 0;
            h3wEvtQueue(cP->privEvtObjP, event);
        }
        break;

    case US_UNINIT:
    default:
        if (TRUE == cP->uart.allowDualEvent) {
            if (UE_INIT_COMPLETE == eventFromUART) {
                cP->uart.allowDualEvent = FALSE;
                event.type = ET_UART_INIT;
                event.data = 0;
                h3wEvtQueue(cP->privEvtObjP, event);
            }
        }
        break;
    }
}



/*---------------------------------------------------------------------------
 * h3wSlpSetUARTState()
 *     This function sets the UART state enumeration value.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire slip module context structure.  
 *     newState - The new UART state.
 */
static void h3wSlpSetUARTState(Hci3WSlpStruct *cP, UART_STATE_ENUM newState)
{
    switch(newState) {
    case US_PARTIAL_INIT:
        Assert(US_UNINIT == cP->uart.state);
        cP->uart.state = US_PARTIAL_INIT;
        break;

    case US_WORKING:
        Assert(  (US_UNINIT       == cP->uart.state) 
              || (US_PARTIAL_INIT == cP->uart.state));
        cP->uart.state = US_WORKING;
        break;

    case US_PARTIAL_FREE:
        Assert(US_WORKING == cP->uart.state);
        cP->uart.state = US_PARTIAL_FREE;
        break;

    case US_UNINIT:
    default:
        Assert(  (US_WORKING      == cP->uart.state) 
              || (US_PARTIAL_FREE == cP->uart.state));
        cP->uart.state = US_UNINIT;
    }
    cP->uart.allowDualEvent = FALSE;
}



/*---------------------------------------------------------------------------
 * h3wSlpPrivUARTInitialized()
 *     This function should be called when it is known that the UART is 
 *     initialized.  It gets the configured bits per second transmission rate, 
 *     encodes it in the UART software format, and then invokes the UART 
 *     routine that sets the speed.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire slip module context structure.  
 *     errObjP - A pointer to an error context structure.
 *
 * Returns:
 *     This function returns TRUE if it was successfully able to set the UART 
 *     data transfer speed, or FALSE otherwise.  
 */
static BOOL h3wSlpPrivUARTInitialized(Hci3WSlpStruct *cP, 
                                      Hci3WErrStruct *errObjP)
{
    BOOL uartSpeedChanged;
    U32  bps;

    bps = h3wUtlGetUARTBPS(cP->privUtlObjP);
    if (  TRUE 
       == h3wSlpUARTSetSpeedE(cP, bps, TRUE, &uartSpeedChanged, errObjP)) {
        if (TRUE == uartSpeedChanged) {
            h3wSlpSetUARTState(cP, US_WORKING);
            Report(("HCI3W:  UART Initialized\n"));
        } else {
            h3wErrSet(errObjP, ES_SLP_UARTINITIALIZED, ER_UART_SPEED);
        }
    }
    return(h3wErrHasNone(errObjP));
}



/*---------------------------------------------------------------------------
 * h3wSlpUARTSetSpeedE()
 *     This function redefines the speed at which the UART is to operate.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire slip module context structure.  
 *     bps - The new bits per second rate at which the UART is to operate.  
 *     allowPartial - This parameter should be set to TRUE if UART_SetSpeed 
 *         can be invoked when the UART is only partially initialized, or 
 *         FALSE otherwise.  
 *     uartSpeedChangedP - This parameter must point to a location where a 
 *         BOOL can be written.  This location will be set to TRUE if 
 *         UART_SetSpeed() is invoked, or FALSE otherwise.  Note that there 
 *         are cases where UART_SetSpeed would not be invoked, such as when 
 *         the UART has not been initialized.  
 *     errObjP - A pointer to an error context structure.
 *
 * Returns:
 *     This function returns TRUE if it was successfully able to set the UART 
 *     data transfer speed, or FALSE otherwise.  
 */
static BOOL h3wSlpUARTSetSpeedE(Hci3WSlpStruct *cP, 
                                U32             bps, 
                                BOOL            allowPartial,
                                BOOL           *uartSpeedChangedP, 
                                Hci3WErrStruct *errObjP) 
{
    U16 uartSpeedEncoded;

    *uartSpeedChangedP = FALSE;
    if (  TRUE 
       == h3wUtlBPStoUARTEncoding( cP->privUtlObjP, 
                                  &uartSpeedEncoded, 
                                   bps, 
                                   errObjP)) {
        h3wUtlSetUARTBPSSpeed(cP->privUtlObjP, bps);
        switch(cP->uart.state) {
        case US_PARTIAL_INIT:
            if (TRUE == allowPartial) {
                *uartSpeedChangedP = TRUE;
            }
            break;

        case US_WORKING:
            *uartSpeedChangedP = TRUE;
            break;

        default:
            break;
        }
        if (TRUE == *uartSpeedChangedP) {
            UART_SetSpeed(uartSpeedEncoded);
        }
    }
    return(h3wErrHasNone(errObjP));
}



/*---------------------------------------------------------------------------
 * h3wSlpPrivUARTTerminated()
 *     This function should be invoked after the UART is known to be 
 *     terminated.  It sets the UART state variable, then frees up the 
 *     receive queue.  It reports any errors it encounters.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire slip module context structure.  
 */
static void h3wSlpPrivUARTTerminated(Hci3WSlpStruct *cP)
{
    Hci3WErrStruct errObj;

    h3wSlpSetUARTState(cP, US_UNINIT);
    h3wErrInit(&errObj);
    h3wRXQFree(&cP->slpRXQObj, &errObj);
    h3wErrReportUnsolicited(&errObj, TRUE, " Receive Queue Free");
    h3wErrTerm(&errObj);
}



/*---------------------------------------------------------------------------
 * h3wSlpEnSlip()
 *     This function creates a slip encoded version of a given HCI 3-wire 
 *     framed message.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire slip module context structure.  
 *     dB - A slip buffer structure containing information about the source 
 *         framed message, and where information about the destination slip 
 *         encoded message can be written.  
 *     errObjP - A pointer to an error context structure.
 *
 * Returns:
 *     This function returns TRUE if it was successfully able to slip encode 
 *     the framed HCI 3-wire message, or FALSE otherwise.  
 */
static BOOL h3wSlpEnSlip(Hci3WSlpStruct *cP, 
                         SlipBuffStruct  dB, 
                         Hci3WErrStruct *errObjP)
{
    BOOL slipEncoding;
    BOOL slipEscapeSequence;
    U8   slipChar;
    U16  ndx;

    ndx = 0;
    dB.sP->used = 0;
    slipChar = 0;
    Assert(dB.sP->used < dB.sP->maxSize);
    *(dB.sP->buf + dB.sP->used) = (U8)'\xC0';
    dB.sP->used += 1;
    do {
        slipEncoding = FALSE;
        if (ndx < dB.hP->used) {
            slipEscapeSequence = FALSE;
            switch(*(dB.hP->buf + ndx)) {
            case (U8)'\x11':
                if (  TRUE 
                   == h3wUtlGetFlowControl(cP->privUtlObjP,
                                           FL_OUT_OF_FRAME_SOFTWARE, 
                                           FT_CHOKE_ABILITY)) {
                    slipEscapeSequence = TRUE;
                    slipChar = (U8)'\xDE';
                }
                break;

            case (U8)'\x13':
                if (  TRUE 
                   == h3wUtlGetFlowControl(cP->privUtlObjP,
                                           FL_OUT_OF_FRAME_SOFTWARE, 
                                           FT_CHOKE_ABILITY)) {
                    slipEscapeSequence = TRUE;
                    slipChar = (U8)'\xDF';
                }
                break;

            case (U8)'\xC0':
                slipEscapeSequence = TRUE;
                slipChar = (U8)'\xDC';
                break;

            case (U8)'\xDB':
                slipEscapeSequence = TRUE;
                slipChar = (U8)'\xDD';
                break;

            default:
                break;
            }
            Assert(dB.sP->used < dB.sP->maxSize);
            if (TRUE == slipEscapeSequence) {
                *(dB.sP->buf + dB.sP->used) = (U8)'\xDB';
                dB.sP->used += 1;
                Assert(dB.sP->used < dB.sP->maxSize);
                *(dB.sP->buf + dB.sP->used) = slipChar;
            } else {
                *(dB.sP->buf + dB.sP->used) = *(dB.hP->buf + ndx);
            }
            slipEncoding = TRUE;
            ndx += 1;
        } else {
            Assert(dB.sP->used < dB.sP->maxSize);
            *(dB.sP->buf + dB.sP->used) = (U8)'\xC0';
        }
        dB.sP->used += 1;
    } while (slipEncoding);
    return(h3wErrHasNone(errObjP));
}



/*---------------------------------------------------------------------------
 * h3wSlpSetDeslipState()
 *     This function provides a state machine that tracks the deslip process 
 *     on a character by character basis.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire slip module context structure.  
 *     inputChar - The next character that is being deslipped.  
 */
static void h3wSlpSetDeslipState(Hci3WSlpStruct *cP, U8 inputChar)
{
    switch(cP->deslipState) {
    case SD_DELIMITER:
        switch(inputChar) {
        case (U8)'\xC0':

            /* If two delimeters are received one after another, 
             * the second may indicate the end of a zero length 
             * packet instead of the start of a new packet.  So 
             * do not assume we are at the start of a packet yet.
             */ 

            break;

        case (U8)'\xDB':
            cP->deslipState = SD_ESCAPE_SEQUENCE1;
            break;

        default:
            cP->deslipState = SD_PACKET_DATA;
        }
        break;

    case SD_START_PACKET:
        switch(inputChar) {
        case (U8)'\xC0':
            cP->deslipState = SD_END_PACKET;
            break;

        case (U8)'\xDB':
            cP->deslipState = SD_ESCAPE_SEQUENCE1;
            break;

        default:
            cP->deslipState = SD_PACKET_DATA;
        }
        break;

    case SD_PACKET_DATA:
        switch(inputChar) {
        case (U8)'\xC0':
            cP->deslipState = SD_END_PACKET;
            break;

        case (U8)'\xDB':
            cP->deslipState = SD_ESCAPE_SEQUENCE1;
            break;

        default:
            break;
        }
        break;

    case SD_ESCAPE_SEQUENCE1:
        switch(inputChar) {
        case (U8)'\xDC':
        case (U8)'\xDD':
            cP->deslipState = SD_ESCAPE_SEQUENCE2;
            break;

        case (U8)'\xDE':
        case (U8)'\xDF':
            if (  TRUE 
               == h3wUtlGetFlowControl(cP->privUtlObjP,
                                       FL_OUT_OF_FRAME_SOFTWARE, 
                                       FT_CHOKE_ABILITY)) {
                cP->deslipState = SD_ESCAPE_SEQUENCE2;
            } else {
                cP->deslipState = SD_GARBLED_SEQUENCE;
            }
            break;

        default:
            cP->deslipState = SD_GARBLED_SEQUENCE;
        }
        break;

    case SD_ESCAPE_SEQUENCE2:
        switch(inputChar) {
        case (U8)'\xC0':
            cP->deslipState = SD_END_PACKET;
            break;

        case (U8)'\xDB':
            cP->deslipState = SD_ESCAPE_SEQUENCE1;
            break;

        default:
            cP->deslipState = SD_PACKET_DATA;
        }
        break;

    case SD_END_PACKET:
        if ((U8)'\xC0' == inputChar) {
            cP->deslipState = SD_START_PACKET;
        } else {
            cP->deslipState = SD_UNKNOWN;
        }
        break;

    case SD_GARBLED_SEQUENCE:
        switch(inputChar) {
        case (U8)'\xC0':
            cP->deslipState = SD_END_PACKET;
            break;

        case (U8)'\xDB':
            cP->deslipState = SD_ESCAPE_SEQUENCE1;
            break;

        default:
            cP->deslipState = SD_PACKET_DATA;
        }
        break;

    case SD_UNKNOWN:
    default:
        if ((U8)'\xC0' == inputChar) {
            cP->deslipState = SD_DELIMITER;
        }
    }
}



/*---------------------------------------------------------------------------
 * h3wSlpDeSlipChar()
 *     This function performs a deslip operation on a single character.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire slip module context structure.  
 *     dB - A slip buffer structure containing information about the slip 
 *         encoded source buffer, and where information about the framed 
 *         destination buffer can be written.  
 *     ndx - The location of the character currently being deslipped in the 
 *         source buffer.  
 *     slipDecodingP - This parameter must point to a location where TRUE can 
 *         be written if slip decoding is needed on subsequent characters, or 
 *         FALSE otherwise.  
 *     errObjP - A pointer to an error context structure.
 *
 * Returns:
 *     This function returns TRUE if it was successfully able to deslip the 
 *     given character, or FALSE otherwise.  
 */
static BOOL h3wSlpDeSlipChar(Hci3WSlpStruct *cP, 
                             SlipBuffStruct  dB, 
                             U16             ndx,
                             BOOL           *slipDecodingP,
                             Hci3WErrStruct *errObjP)
{
    BOOL               isOOFChar;
    DECODING_SLIP_ENUM decodingSlip;
    EventStruct        event;
    U8                 decodedChar;

    decodedChar = 0;
    isOOFChar = FALSE;
    switch(*(dB.sP->buf + ndx)) {
    case (U8)'\x11':
        if (  TRUE 
           == h3wUtlGetFlowControl(cP->privUtlObjP,
                                   FL_OUT_OF_FRAME_SOFTWARE, 
                                   FT_CHOKE_ABILITY)) {
            h3wUtlSetFlowControl(cP->privUtlObjP,
                                 FL_OUT_OF_FRAME_SOFTWARE,
                                 FT_CURRENT_STATE, 
                                 TRUE);
            isOOFChar = TRUE;
        }
        break;

    case (U8)'\x13':
        if (  TRUE 
           == h3wUtlGetFlowControl(cP->privUtlObjP,
                                   FL_OUT_OF_FRAME_SOFTWARE, 
                                   FT_CHOKE_ABILITY)) {
            h3wUtlSetFlowControl(cP->privUtlObjP,
                                 FL_OUT_OF_FRAME_SOFTWARE,
                                 FT_CURRENT_STATE, 
                                 FALSE);
            isOOFChar = TRUE;
        }
        break;

    default:
        break;
    }
    if (TRUE == isOOFChar) {
        *slipDecodingP = TRUE;
    } else {
        h3wSlpSetDeslipState(cP, *(dB.sP->buf + ndx));
        decodingSlip = DS_CONTINUE_TO_NEXT;
        switch(cP->deslipState) {
        case SD_PACKET_DATA:
            decodedChar = *(dB.sP->buf + ndx);
            decodingSlip = DS_CHAR_WAS_DECODED;
            break;

        case SD_ESCAPE_SEQUENCE2:
            switch(*(dB.sP->buf + ndx)) {
            case (U8)'\xDD':
                decodedChar = (U8)'\xDB';
                break;

            case (U8)'\xDE':
                decodedChar = (U8)'\x11';
                break;

            case (U8)'\xDF':
                decodedChar = (U8)'\x13';
                break;

            case (U8)'\xDC':
            default:
                decodedChar = (U8)'\xC0';
            }
            decodingSlip = DS_CHAR_WAS_DECODED;
            break;

        case SD_END_PACKET:
            decodingSlip = DS_END_OF_PACKET;
            break;

        default:
            break;
        }
        switch(decodingSlip) {
        case DS_CHAR_WAS_DECODED:
            if (dB.hP->maxSize > dB.hP->used) {
                *(dB.hP->buf + dB.hP->used) = decodedChar;
                dB.hP->used += 1;
                *slipDecodingP = TRUE;
            } else {

                /* Since we are not going to finish 
                 * parsing the rest of the input data, 
                 * the slip decoder state becomes unknown.
                 */ 
                cP->deslipState = SD_UNKNOWN;

                h3wErrSet(errObjP, ES_SLP_DESLIP, ER_BUF_TOO_SMALL);
            }
            break;

        case DS_CONTINUE_TO_NEXT:
            *slipDecodingP = TRUE;
            break;

        case DS_END_OF_PACKET:
        default:
            if ((ndx + 1) < dB.sP->used) {

                /* Put the remaining fragment into the receive queue.  */ 
                if (  TRUE 
                   == h3wRXQPut(&cP->slpRXQObj, 
                                 dB.sP->buf + ndx + 1, 
                                 (U16)(dB.sP->used - (ndx + 1)),
                                 errObjP)) {

                    /* Signal that more receive data is pending. */ 
                    event.type = ET_RX_UART;
                    event.data = 0;
                    h3wEvtQueue(cP->privEvtObjP, event);

                }
            }
        }
    }
    return(h3wErrHasNone(errObjP));
}



/*---------------------------------------------------------------------------
 * h3wSlpDeSlip()
 *     This function creates a deslipped version of an HCI 3-wire slip encoded 
 *     message.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire slip module context structure.  
 *     dB - A slip buffer structure containing information about the slip 
 *         encoded source buffer, and where information about the framed 
 *         destination buffer can be written.  
 *     errObjP - A pointer to an error context structure.
 *
 * Returns:
 *     This function returns TRUE if it was successfully able to deslip the 
 *     given buffer, or FALSE otherwise.  
 */
static BOOL h3wSlpDeSlip(Hci3WSlpStruct *cP, 
                         SlipBuffStruct  dB, 
                         Hci3WErrStruct *errObjP)
{
    BOOL              slipDecoding;
    DESLIP_STATE_ENUM oldDeslipState;
    U16               ndx;

    oldDeslipState = cP->deslipState;
    ndx = 0;
    dB.hP->used = 0;
    do {
        slipDecoding = FALSE;
        if (dB.sP->used > ndx) {
            if (  TRUE 
               == h3wSlpDeSlipChar(cP, dB, ndx, &slipDecoding, errObjP)) {
                if (TRUE == slipDecoding) {
                    ndx += 1;
                }
            }
        } else {
            if (SD_END_PACKET != cP->deslipState) {
                dB.hP->used = 0;
                cP->deslipState = oldDeslipState;

                /* An incomplete message has been received.  Put the 
                 * incomplete fragment into the receive queue so that it 
                 * can be processed when the rest of the message arrives. 
                 */ 
                h3wRXQPut(&cP->slpRXQObj, 
                           dB.sP->buf, 
                           dB.sP->used, 
                           errObjP);

                /* Don't signal that more receive data is pending 
                 * here, as a complete message has yet to be recieved.
                 */ 

            }
        }
    } while (TRUE == slipDecoding);
    return(h3wErrHasNone(errObjP));
}

