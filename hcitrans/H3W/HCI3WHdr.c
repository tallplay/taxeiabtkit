/****************************************************************************
 *
 * File:
 *     $Workfile:HCI3WHdr.c$ for iAnywhere Blue SDK, Version 2.2
 *     $Revision:11$
 *
 * Description: This file is part of an HCI 3-Wire implementation.  It 
 * handles the frame header information.  
 *
 * Created:     October 31, 2006
 *
 * Copyright 2006-2007 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions,  
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "sys/HCI3WHdr.h"

/****************************************************************************
 *
 * Prototypes
 *
 ****************************************************************************/

static BOOL h3wHdrSetHdrByte1(Hci3WHdrStruct *cP, 
                              U8             *destP, 
                              BOOL            sendReliably, 
                              BOOL            tincludeCRC,
                              Hci3WErrStruct *errObjP);

static void h3wHdrAssignBits(Hci3WHdrStruct *cP,
                             U8             *destP, 
                             U8              startBit, 
                             U8              endBit, 
                             U8              value);

static void h3wHdrHandleTimerEvent(Hci3WHdrStruct *cP);

static void h3wHdrSetHdrByte2(Hci3WHdrStruct  *cP, 
                              U8              *destP, 
                              PACKET_TYPE_ENUM packetType, 
                              U16              payldLen);

static void h3wHdrSetHdrByte3(U8 *destP, U16 payldLen);

static void h3wHdrSetHdrByte4(U8 *destP, U8 sumMod256);

/****************************************************************************
 *
 * Public Functions
 *
 ****************************************************************************/



/*---------------------------------------------------------------------------
 * h3wHdrTimerEventCallback()
 *     This function is invoked when the frame header timer fires.  If it is 
 *     given a valid context, it invokes the frame header timer event code.  
 * 
 * Parameters:
 *     evmTimerP - The event manager structure corresponding to the timer that 
 *         just fired.  
 */
void h3wHdrTimerEventCallback(struct _EvmTimer *evmTimerP)
{
    if ((void *)0 != evmTimerP->context) {
        h3wHdrHandleTimerEvent((Hci3WHdrStruct *)evmTimerP->context);
    }
}



/*---------------------------------------------------------------------------
 * h3wHdrInit()
 *     Initializes the HCI 3-wire frame header module.  This function must 
 *     be invoked before any of the other HCI 3-wire frame header functions 
 *     will work correctly.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire frame header module context structure.  
 *     evtObjP - Pointer to an HCI 3-wire event handling module context 
 *         structure.  
 *     utlObjP - Pointer to an HCI 3-wire utilities module context structure.  
 */
void h3wHdrInit(Hci3WHdrStruct *cP, 
                Hci3WEvtStruct *evtObjP, 
                Hci3WUtlStruct *utlObjP)
{
    cP->privEvtObjP       = evtObjP;
    cP->privUtlObjP       = utlObjP;
    h3wHdrResetSeqAck(cP);
    cP->numRXNeedingACK   = 0;
    InitializeListEntry(&cP->timer.node);
    cP->timer.context     = (void *)cP;
    cP->timer.func        = h3wHdrTimerEventCallback;
    cP->timer.time        = (TimeT)0;
    cP->timer.startTime   = (TimeT)0;

    cP->maxValue[1] = (U8)'\x01';
    cP->maxValue[2] = (U8)'\x03';
    cP->maxValue[3] = (U8)'\x07';
    cP->maxValue[4] = (U8)'\x0F';
    cP->maxValue[5] = (U8)'\x1F';
    cP->maxValue[6] = (U8)'\x3F';
    cP->maxValue[7] = (U8)'\x7F';

    /* 1111 1110 */ 
    /* 1111 1100 */ 
    /* 1111 1000 */ 
    /* 1111 0000 */ 
    /* 1110 0000 */ 
    /* 1100 0000 */ 
    /* 1000 0000 */ 
    cP->andMask[0][0] = (U8)'\xFE';
    cP->andMask[0][1] = (U8)'\xFC';
    cP->andMask[0][2] = (U8)'\xF8';
    cP->andMask[0][3] = (U8)'\xF0';
    cP->andMask[0][4] = (U8)'\xE0';
    cP->andMask[0][5] = (U8)'\xC0';
    cP->andMask[0][6] = (U8)'\x80';

    /* 1111 1101 */ 
    /* 1111 1001 */ 
    /* 1111 0001 */ 
    /* 1110 0001 */ 
    /* 1100 0001 */ 
    /* 1000 0001 */ 
    /* 0000 0001 */ 
    cP->andMask[1][1] = (U8)'\xFD';
    cP->andMask[1][2] = (U8)'\xF9';
    cP->andMask[1][3] = (U8)'\xF1';
    cP->andMask[1][4] = (U8)'\xE1';
    cP->andMask[1][5] = (U8)'\xC1';
    cP->andMask[1][6] = (U8)'\x81';
    cP->andMask[1][7] = (U8)'\x01';

    /* 1111 1011 */ 
    /* 1111 0011 */ 
    /* 1110 0011 */ 
    /* 1100 0011 */ 
    /* 1000 0011 */ 
    /* 0000 0011 */ 
    cP->andMask[2][2] = (U8)'\xFB';
    cP->andMask[2][3] = (U8)'\xF3';
    cP->andMask[2][4] = (U8)'\xE3';
    cP->andMask[2][5] = (U8)'\xC3';
    cP->andMask[2][6] = (U8)'\x83';
    cP->andMask[2][7] = (U8)'\x03';

    /* 1111 0111 */ 
    /* 1110 0111 */ 
    /* 1100 0111 */ 
    /* 1000 0111 */ 
    /* 0000 0111 */ 
    cP->andMask[3][3] = (U8)'\xF7';
    cP->andMask[3][4] = (U8)'\xE7';
    cP->andMask[3][5] = (U8)'\xC7';
    cP->andMask[3][6] = (U8)'\x87';
    cP->andMask[3][7] = (U8)'\x07';

    /* 1110 1111 */ 
    /* 1100 1111 */ 
    /* 1000 1111 */ 
    /* 0000 1111 */ 
    cP->andMask[4][4] = (U8)'\xEF';
    cP->andMask[4][5] = (U8)'\xCF';
    cP->andMask[4][6] = (U8)'\x8F';
    cP->andMask[4][7] = (U8)'\x0F';

    /* 1101 1111 */ 
    /* 1001 1111 */ 
    /* 0001 1111 */ 
    cP->andMask[5][5] = (U8)'\xDF';
    cP->andMask[5][6] = (U8)'\x9F';
    cP->andMask[5][7] = (U8)'\x1F';

    /* 1011 1111 */ 
    /* 0011 1111 */ 
    cP->andMask[6][6] = (U8)'\xBF';
    cP->andMask[6][7] = (U8)'\x3F';

    /* 0111 1111 */ 
    cP->andMask[7][7] = (U8)'\x7F';

}



/*---------------------------------------------------------------------------
 * h3wHdrGet()
 *     This function extracts header information from a given buffer.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire frame header module context structure.  
 *     sourcePtr - This parameter must point to a buffer containing an HCI 
 *         3-wire framed message.  
 *     hdrInfoP - This parameter must point to a location where a frame 
 *         header structure can be written.  
 *     isNewActiveRX - This parameter must be set to TRUE if the given buffer 
 *         was newly received, or FALSE otherwise.  
 *     errObjP - A pointer to an error context structure.
 *
 * Returns:
 *     This function returns TRUE if it was successfully able to extract the 
 *     header data from the given buffer, or FALSE otherwise.  
 */
BOOL h3wHdrGet(Hci3WHdrStruct *cP,
               U8             *sourcePtr, 
               FrameHdrStruct *hdrInfoP,
               BOOL            isNewActiveRX,
               Hci3WErrStruct *errObjP)
{
    U8 ndx;
    U8 sumMod256;

    /* Determine the packet type.  */ 
    switch(*(sourcePtr + 1) & 0x0F) {
    case 0:
        hdrInfoP->packetType = PT_ACK;
        break;

    case 1:
        hdrInfoP->packetType = PT_CMD;
        break;

    case 2:
        hdrInfoP->packetType = PT_ACL;
        break;

    case 3:
        hdrInfoP->packetType = PT_SYNCH;
        break;

    case 4:
        hdrInfoP->packetType = PT_EVENT;
        break;

    case 14:
        hdrInfoP->packetType = PT_VENDOR;
        break;

    case 15:
        hdrInfoP->packetType = PT_LINK;
        break;

    default:
        h3wErrSet(errObjP, ES_HDR_GETHDR, ER_UNKNOWN_PKT_TYPE);
    }

    /* Determine the sequence number.  */ 
    if (TRUE == h3wErrHasNone(errObjP)) {
        h3wUtlSeqU8ToE(&hdrInfoP->seqNum, 
                        (U8)(*sourcePtr & 0x07), 
                        errObjP);
    }

    /* Determine the acknowledgement number.  */ 
    if (TRUE == h3wErrHasNone(errObjP)) {
        h3wUtlSeqU8ToE(&hdrInfoP->ackNum, 
                        (U8)((*sourcePtr & 0x38) >> 3), 
                        errObjP);
    }

    /* Determine whether a cyclic redundancy value 
     * was appended to the end of the payload.  
     */
    if (TRUE == h3wErrHasNone(errObjP)) {
        if (*sourcePtr & 0x40) {
            if (TRUE == h3wUtlGetMayUseCRC(cP->privUtlObjP)) {
                hdrInfoP->includesCRC = TRUE;
            } else {
                h3wErrSet(errObjP, ES_HDR_GETHDR, ER_LINK_PROHIBITS_CRCS);
            }
        } else {
            hdrInfoP->includesCRC = FALSE;
        }
    }

    /* Determine whether this is a reliable message.  */ 
    if (TRUE == h3wErrHasNone(errObjP)) {
        if (*sourcePtr & 0x80) {
            hdrInfoP->sentReliably = TRUE;
            if (TRUE == isNewActiveRX) {

                /* Determine whether the message is not out of sequence.  */ 
                if (cP->rxSequenceNum == hdrInfoP->seqNum) {
                    cP->rxSequenceNum 
                        = h3wUtlGetNextSeq(cP->rxSequenceNum);
                } else {
                    h3wErrSet(errObjP, ES_HDR_GETHDR, ER_OUT_OF_SEQUENCE);
                }

            }
        } else {
            hdrInfoP->sentReliably = FALSE;
        }
    }

    if (TRUE == h3wErrHasNone(errObjP)) {

        /* Determine the payload length.  */ 
        hdrInfoP->payldLen 
            = (U16)( ((*(sourcePtr + 1) & 0xF0) >> 4) 
                   |  (*(sourcePtr + 2)         << 4));
        if ( h3wUtlGetMaxPayloadBytes(cP->privUtlObjP) 
           < hdrInfoP->payldLen) {

            /* This can only happen if the default payload 
             * size is overridden to be less than 4095.  
             */ 
            h3wErrSet(errObjP, ES_HDR_GETHDR, ER_PAYLOAD_TOO_LARGE);

        }

    }
    if (TRUE == h3wErrHasNone(errObjP)) {

        /* Determine the header checksum and verify that it is correct.  */ 
        hdrInfoP->hdrChecksum = *(sourcePtr + 3);
        sumMod256 = (U8)(*sourcePtr);
        for (ndx = 1; ndx < 4; ndx += 1) {
            sumMod256 = (U8)((sumMod256 + *(sourcePtr + ndx)) % 256);
        }
        if ((U8)'\xFF' != sumMod256) {
            h3wErrSet(errObjP, ES_HDR_GETHDR, ER_BAD_HDR_CS);
        }

    }
    return(h3wErrHasNone(errObjP));
}



/*---------------------------------------------------------------------------
 * h3wHdrUpdateACK()
 *     This function updates the acknowledgement number in a given header 
 *     byte, and recalculates the header checksum.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire frame header module context structure.  
 *     hdrOldP - This parameter must point to a frame header structure 
 *         containing the extracted header information for the given 
 *         destination.  
 *     destP - This parameter must point to a buffer containing an HCI 3-wire 
 *         framed message that can be updated.  
 */
void h3wHdrUpdateACK(Hci3WHdrStruct *cP, 
                     FrameHdrStruct *hdrOldP, 
                     U8             *destP)
{
    U8 sumMod256;

    if (hdrOldP->ackNum != cP->rxSequenceNum) {
        h3wHdrAssignBits(cP, 
                         destP, 
                         3, 
                         5, 
                         h3wUtlSeqEToU8(cP->rxSequenceNum));

        /* Since the header has been changed, the 
         * header checksum must be recalculated. 
         */ 
        sumMod256 =                    *destP;
        sumMod256 = (U8)((sumMod256 + *(destP + 1)) % 256);
        sumMod256 = (U8)((sumMod256 + *(destP + 2)) % 256);
        *(destP + 3) = (U8)(0x00FF - sumMod256);

    }
}



/*---------------------------------------------------------------------------
 * h3wHdrSet()
 *     This function sets the header of an HCI 3-wire framed message buffer 
 *     from the given parameters.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire frame header module context structure.  
 *     destP - This parameter must point to an HCI 3-wire framed message 
 *         buffer where the header can be written.  
 *     sendReliably - This parameter must be set to TRUE if the given buffer 
 *         is for a reliable message, or FALSE otherwise.  
 *     includeCRC - This parameter must be set to TRUE if a CRC will be 
 *         appended to the end of the given HCI 3-wire framed message, or 
 *         FALSE otherwise.  
 *     packetType - This parameter must be set to an enumeration 
 *         corresponding to the packet type.  
 *     payldLen - This parameter must be set to the size of the payload in 
 *         the given HCI 3-wire framed buffer.  
 *     errObjP - A pointer to an error context structure.
 *
 * Returns:
 *     This function returns TRUE if it was successfully able to set the 
 *     header bytes, or FALSE otherwise.  
 */
BOOL h3wHdrSet(Hci3WHdrStruct  *cP, 
               U8              *destP, 
               BOOL             sendReliably, 
               BOOL             includeCRC,
               PACKET_TYPE_ENUM packetType, 
               U16              payldLen,
               Hci3WErrStruct  *errObjP)
{
    U8 sumMod256;

    sumMod256 = 0;
    if (  TRUE 
       == h3wHdrSetHdrByte1(cP, destP, sendReliably, includeCRC, errObjP)) {
        sumMod256 = *destP;
        destP += 1;
        h3wHdrSetHdrByte2(cP, destP, packetType, payldLen);
        sumMod256 = (U8)((sumMod256 + *destP) % 256);
        destP += 1;
        h3wHdrSetHdrByte3(destP, payldLen);
        sumMod256 = (U8)((sumMod256 + *destP) % 256);
        destP += 1;
        h3wHdrSetHdrByte4(destP, sumMod256);
    }
    return(h3wErrHasNone(errObjP));
}



/*---------------------------------------------------------------------------
 * h3wHdrInSlidingWin()
 *     This function determines if a header structure contains a sequence 
 *     number that is within the sliding window.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire frame header module context structure.  
 *     hdrOldP - This parameter must point to a frame header structure 
 *         containing the sequence number that is to be tested.  
 *     ackNum - This is the acknowledgement number that is used to determine 
 *         the position of the sliding window.  
 *
 * Returns:
 *     This function returns TRUE if the sequence number in the given header 
 *     is within the sliding window positioned by the given acknowledgement 
 *     number.  Otherwise, it returns FALSE.  
 */
BOOL h3wHdrInSlidingWin(Hci3WHdrStruct *cP, 
                        FrameHdrStruct *hdrOldP, 
                        SEQ_ENUM        ackNum)
{
    BOOL retVal;
    U8   hdrSeqNum;
    U8   wrapOffset;

    retVal = FALSE;
    wrapOffset = 0;
    hdrSeqNum = h3wUtlSeqEToU8(hdrOldP->seqNum);
    if (hdrSeqNum >= h3wUtlSeqEToU8(ackNum)) {
        wrapOffset = 8;
    }
    if (  (ackNum + wrapOffset) 
       <= ( hdrSeqNum + h3wUtlGetSlidingWindowSize(cP->privUtlObjP))) {
        retVal = TRUE;
    }
    return(retVal);
}



/*---------------------------------------------------------------------------
 * h3wHdrPureACKWindowFull()
 *     This function determines whether the sliding window is full indicating 
 *     the need for a pure acknowledgement message.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire frame header module context structure.  
 *
 * Returns:
 *     This function returns TRUE if the sliding window is full, or FALSE 
 *     otherwise.  
 */
BOOL h3wHdrPureACKWindowFull(Hci3WHdrStruct *cP)
{
    BOOL retVal;

    retVal = FALSE;

    /* The code cannot assert that the number of received messages needing an 
     * acknowledgement is less than or equal to the sliding window size, 
     * because there are situations where acknowledgements (whether pure or 
     * not) cannot be sent out.  For example, suppose an XOFF is received.  
     * Then, until an XON is received, no acknowledgements can be sent out.  
    */ 

    if (cP->numRXNeedingACK >= h3wUtlGetSlidingWindowSize(cP->privUtlObjP)) {
        retVal = TRUE;
    }
    return(retVal);
}



/*---------------------------------------------------------------------------
 * h3wHdrAllAcknowledged()
 *     This function indicates whether there are messages needing 
 *     acknowledgement or not.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire frame header module context structure.  
 *
 * Returns:
 *     This function returns TRUE if there are no messages needing 
 *     acknowledgement, or FALSE otherwise.  
 */
BOOL h3wHdrAllAcknowledged(Hci3WHdrStruct *cP)
{
    BOOL retVal;

    retVal = TRUE;

    /* The code cannot assert that the number of received messages needing an 
     * acknowledgement is less than or equal to the sliding window size, 
     * because there are situations where acknowledgements (whether pure or 
     * not) cannot be sent out.  For example, suppose an XOFF is received.  
     * Then, until an XON is received, no acknowledgements can be sent out.  
    */ 

    if (0 < cP->numRXNeedingACK) {
        retVal = FALSE;
    }
    return(retVal);
}



/*---------------------------------------------------------------------------
 * h3wHdrClearNumRXNeedingACK()
 *     This function clears the number of messages needing acknowledgement.  
 *     It should be called anytime a transmission occurs, since transmission 
 *     messages always acknowledge all outstanding messages.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire frame header module context structure.  
 */
void h3wHdrClearNumRXNeedingACK(Hci3WHdrStruct *cP)
{

    /* The code cannot assert that the number of received messages needing an 
     * acknowledgement is less than or equal to the sliding window size, 
     * because there are situations where acknowledgements (whether pure or 
     * not) cannot be sent out.  For example, suppose an XOFF is received.  
     * Then, until an XON is received, no acknowledgements can be sent out.  
    */ 

    if (0 < cP->numRXNeedingACK) {
        h3wHdrFree(cP);
        cP->numRXNeedingACK = 0;
    }
}



/*---------------------------------------------------------------------------
 * h3wHdrIncrNumRXNeedingACK()
 *     This function increments the number of received messages needing 
 *     acknowledgement, and initiates the pure acknowledgement timer when 
 *     appropriate.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire frame header module context structure.  
 */
void h3wHdrIncrNumRXNeedingACK(Hci3WHdrStruct *cP)
{
    cP->numRXNeedingACK += 1;

    /* The code cannot assert that the number of received messages needing an 
     * acknowledgement is less than or equal to the sliding window size, 
     * because there are situations where acknowledgements (whether pure or 
     * not) cannot be sent out.  For example, suppose an XOFF is received.  
     * Then, until an XON is received, no acknowledgements can be sent out.  
    */ 

    if (1 == cP->numRXNeedingACK) {
        EVM_StartTimer(&cP->timer, 2 * h3wUtlGetTmax(cP->privUtlObjP));
    }
}



/*---------------------------------------------------------------------------
 * h3wHdrAtSlidingLimit()
 *     This function determines whether the sliding window limit has been 
 *     reached.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire frame header module context structure.  
 *     numNodesInTXQ - This parameter must be set to the number of nodes the 
 *         the transmit queue.  
 *
 * Returns:
 *     This function returns TRUE if the sliding window limit has been 
 *     reached, or FALSE otherwise.  
 */
BOOL h3wHdrAtSlidingLimit(Hci3WHdrStruct *cP, U8 numNodesInTXQ)
{
    BOOL retVal;
    U8   slidingWindowSize;

    retVal = FALSE;
    slidingWindowSize = h3wUtlGetSlidingWindowSize(cP->privUtlObjP);
    Assert(numNodesInTXQ <= slidingWindowSize);
    if (numNodesInTXQ == slidingWindowSize) {
        retVal = TRUE;
    }
    return(retVal);
}



/*---------------------------------------------------------------------------
 * h3wHdrFree()
 *     This function frees up any resources allocated by the frame header 
 *     module.  It cancels the pure acknowledgement timer.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire frame header module context structure.  
 */
void h3wHdrFree(Hci3WHdrStruct *cP)
{
    EVM_CancelTimer(&cP->timer);
}



/*---------------------------------------------------------------------------
 * h3wHdrResetSeqAck()
 *     This function resets the sequence and acknowledgement numbers to the 
 *     state they should be in right after the link establishment.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire frame header module context structure.  
 */
void h3wHdrResetSeqAck(Hci3WHdrStruct *cP)
{
    cP->rxSequenceNum = SE_0;
    cP->txSequenceNum = SE_0;
    h3wUtlSetLastRXAck(cP->privUtlObjP, FALSE, SE_0);
}



/*---------------------------------------------------------------------------
 * h3wHdrTerm()
 *     This function destroys a given framed header module, cleaning up any 
 *     allocated resources.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire frame header module context structure.  
 */
void h3wHdrTerm(Hci3WHdrStruct *cP)
{
    h3wHdrFree(cP);
    cP->timer.context = (void *)0;
}



/****************************************************************************
 *
 * Private Functions
 *
 ****************************************************************************/



/*---------------------------------------------------------------------------
 * h3wHdrHandleTimerEvent()
 *     This function schedules a pure acknowledgement event.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire frame header module context structure.  
 */
static void h3wHdrHandleTimerEvent(Hci3WHdrStruct *cP) 
{
    EventStruct event;

    event.type = ET_TX_PURE_ACK;
    event.data = 0;
    h3wEvtQueue(cP->privEvtObjP, event);
}



/*---------------------------------------------------------------------------
 * h3wHdrAssignBits()
 *     This function assigns the bits to a bit field within a byte.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire frame header module context structure.  
 *     destP - This parameter must point at a byte where the bitfield is to 
 *         be written.  
 *     startBit - This is the position of the first bit in the bitfield, 
 *         where zero is the least significant bit, and eight is the most 
 *         significant bit.  
 *     endBit - This is the position of the last bit in the bitfield, where 
 *         zero is the least significant bit, and eight is the most 
 *         significant bit.  
 *     value - This is the value that is to be stored within the bitfield.  
 */
static void h3wHdrAssignBits(Hci3WHdrStruct *cP,
                             U8             *destP, 
                             U8              startBit, 
                             U8              endBit, 
                             U8              value)
{
    U8 bitsInField;

    Assert(8 > endBit);
    Assert(0 <= startBit);
    bitsInField = (U8)(1 + endBit - startBit);
    Assert(0 < bitsInField);
    Assert(8 > bitsInField);
    Assert(value <= cP->maxValue[bitsInField]);
    *destP = (U8)( (*destP & cP->andMask[startBit][endBit])
                 | (value << startBit));
}



/*---------------------------------------------------------------------------
 * h3wHdrSetHdrByte1()
 *     This function sets the first header byte in an HCI 3-wire framed 
 *     message.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire frame header module context structure.  
 *     destP - This parameter must point at the first header byte in an HCI 
 *         3-wire framed message.  
 *     sendReliably - This parameter must be TRUE if the HCI 3-wire framed 
 *         message is relaiable, or FALSE otherwise.  
 *     includeCRC - This parameter must be TRUE if the HCI 3-wire framed 
 *         message is to have a CRC value appended to the end of the payload, 
 *         or FALSE otherwise.  
 *     errObjP - A pointer to an error context structure.  
 *
 * Returns:
 *     This function returns TRUE if it was successfully able to set the 
 *     first header byte, or FALSE otherwise.  
 */
static BOOL h3wHdrSetHdrByte1(Hci3WHdrStruct *cP, 
                              U8             *destP, 
                              BOOL            sendReliably, 
                              BOOL            includeCRC,
                              Hci3WErrStruct *errObjP)
{
    BOOL settingHdrByte1;
    U8   endBit;
    U8   fldNdx;
    U8   startBit;
    U8   value;

    fldNdx = 0;
    value = 0;
    do {
        settingHdrByte1 = FALSE;
        if (4 > fldNdx) {
            switch(fldNdx) {
            case 0:
                startBit = 0;
                endBit = 2;
                if (TRUE == sendReliably) {
                    value = h3wUtlSeqEToU8(cP->txSequenceNum);
                    cP->txSequenceNum = h3wUtlGetNextSeq(cP->txSequenceNum);
                } else {
                    value = 0;
                }
                break;

            case 1:
                startBit = 3;
                endBit = 5;
                value = h3wUtlSeqEToU8(cP->rxSequenceNum);
                break;

            case 2:
                startBit = 6;
                endBit = 6;
                if (TRUE == includeCRC) {
                    if (TRUE == h3wUtlGetMayUseCRC(cP->privUtlObjP)) {
                        value = 1;
                    } else {
                        h3wErrSet(errObjP, 
                                  ES_HDR_SETHDR, 
                                  ER_LINK_PROHIBITS_CRCS);
                    }
                } else {
                    value = 0;
                }
                break;

            case 3:
            default:
                startBit = 7;
                endBit = 7;
                if (TRUE == sendReliably) {
                    value = 1;
                } else {
                    value = 0;
                }
            }
            if (TRUE == h3wErrHasNone(errObjP)) {
                h3wHdrAssignBits(cP, destP, startBit, endBit, value);
                settingHdrByte1 = TRUE;
                fldNdx += 1;
            }
        }
    } while (settingHdrByte1);
    return(h3wErrHasNone(errObjP));
}



/*---------------------------------------------------------------------------
 * h3wHdrSetHdrByte2()
 *     This function sets the second header byte in an HCI 3-wire framed 
 *     message.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire frame header module context structure.  
 *     destP - This parameter must point at the second header byte in an HCI 
 *         3-wire framed message.  
 *     packetType - This parameter must be set to an enumeration 
 *         corresponding to the packet type.  
 *     payldLen - This parameter must be set to the payload length of the HCI 
 *         3-wire framed buffer.  
 */
static void h3wHdrSetHdrByte2(Hci3WHdrStruct  *cP, 
                              U8              *destP, 
                              PACKET_TYPE_ENUM packetType, 
                              U16              payldLen)
{
    BOOL settingHdrByte2;
    U8   endBit;
    U8   fldNdx;
    U8   startBit;
    U8   value;

    fldNdx = 0;
    do {
        settingHdrByte2 = FALSE;
        if (2 > fldNdx) {
            switch(fldNdx) {
            case 0:
                startBit = 0;
                endBit = 3;
                switch(packetType){
                case PT_ACK:
                    value = 0;
                    break;

                case PT_CMD:
                    value = 1;
                    break;

                case PT_ACL:
                    value = 2;
                    break;

                case PT_SYNCH:
                    value = 3;
                    break;

                case PT_EVENT:
                    value = 4;
                    break;

                case PT_LINK:
                    value = 15;
                    break;

                case PT_VENDOR:
                default:
                    value = 14;
                }
                break;

            case 1:
            default:
                startBit = 4;
                endBit = 7;
                value = (U8)(payldLen & 0x0F);
            }
            h3wHdrAssignBits(cP, destP, startBit, endBit, value);
            settingHdrByte2 = TRUE;
            fldNdx += 1;
        }
    } while (settingHdrByte2);
}



/*---------------------------------------------------------------------------
 * h3wHdrSetHdrByte3()
 *     This function sets the third header byte in an HCI 3-wire framed 
 *     message.  
 * 
 * Parameters:
 *     destP - This parameter must point at the third header byte in an HCI 
 *         3-wire framed message.  
 *     payldLen - This parameter must be set to the payload length of the HCI 
 *         3-wire framed buffer.  
 */
static void h3wHdrSetHdrByte3(U8 *destP, U16 payldLen)
{
    *destP = (U8)((payldLen & 0x0FF0) >> 4);
}



/*---------------------------------------------------------------------------
 * h3wHdrSetHdrByte4()
 *     This function sets the fourth header byte in an HCI 3-wire framed 
 *     message.  
 * 
 * Parameters:
 *     destP - This parameter must point at the fourth header byte in an HCI 
 *         3-wire framed message.  
 *     sumMod256 - This parameter must be set to sum of the previous header 
 *         bytes (modulo 256).  
 */
static void h3wHdrSetHdrByte4(U8 *destP, U8 sumMod256)
{
    *destP = (U8)((U8)'\xFF' - sumMod256);
}


