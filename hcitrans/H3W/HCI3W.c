/****************************************************************************
 *
 * File:
 *     $Workfile:HCI3W.c$ for iAnywhere Blue SDK, Version 2.2
 *     $Revision:18$
 *
 * Description: This file is part of an HCI 3-Wire implementation.  It 
 * provides the coordination and control of the lower layers.  
 *
 * Created:     October 31, 2006
 *
 * Copyright 2006-2007 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions,  
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "sys/hci3w.h"

/****************************************************************************
 *
 * Prototypes
 *
 ****************************************************************************/

static BOOL         h3wGetConfigE(Hci3WStruct         *cP,
                                  ConfigOptionsStruct *configOptionsP, 
                                  Hci3WErrStruct      *errObjP);

static BOOL         h3wHandleActiveRX(Hci3WStruct    *cP, 
                                      Hci3WErrStruct *errObjP);

static BOOL         h3wHandleInitRX(Hci3WStruct    *cP, 
                                    Hci3WErrStruct *errObjP);

static BOOL         h3wHandleLowPowerRX(Hci3WStruct *cP);

static BOOL         h3wProcessRX(Hci3WStruct    *cP, 
                                 FrameHdrStruct *hdrNewP, 
                                 Hci3WErrStruct *errObjP);

static BOOL         h3wReceiveE(Hci3WStruct      *cP, 
                                BOOL              checkRXQ, 
                                PACKET_TYPE_ENUM *packetTypeP,
                                Hci3WErrStruct   *errObjP);

static BOOL         h3wSendE(Hci3WStruct     *cP,
                             U8              *handleP,
                             BOOL             isLowPowerMsg,
                             BuffUseStruct   *dataBuffP,
                             PACKET_TYPE_ENUM packetType,
                             BOOL             sendReliably, 
                             BOOL             includeCRC,
                             Hci3WErrStruct  *errObjP);

static TRIGGER_ENUM h3wHandleActiveTrigger(Hci3WStruct *cP);

static void         h3wHandleAwake(Hci3WStruct *cP);

static void         h3wHandleConfigField(Hci3WStruct *cP, U8 configField);

static void         h3wHandleEvent(Hci3WStruct *cP, EventStruct event);

static void         h3wHandleTimerEvent(Hci3WStruct *cP);

static void         h3wHandleUninitRX(Hci3WStruct *cP);

static void         h3wInitiateLink(Hci3WStruct *cP);

static void         h3wLinkActive(Hci3WStruct *cP);

static void         h3wLinkInit(Hci3WStruct *cP);

static void         h3wLinkRX(Hci3WStruct *cP, char *operationStr);

static void         h3wReportLinkMsgRX(LINK_MESSEGES_ENUM msg);

static void         h3wReset(Hci3WStruct *cP);

static void         h3wScheduleLinkTimer(Hci3WStruct *cP);

static void         h3wSendLnk(Hci3WStruct *cP);

static void         h3wSendNow(Hci3WStruct *cP);

static void         h3wSendConfigRespActive(Hci3WStruct *cP);

static void         h3wSendConfigRespInit(Hci3WStruct *cP, 
                                          U8           confField);

static void         h3wSendSyncResp(Hci3WStruct *cP);

static void         h3wSendWakeup(Hci3WStruct *cP);

static void         h3wSendWoken(Hci3WStruct *cP);

/****************************************************************************
 *
 * Public Functions
 *
 ****************************************************************************/



/*---------------------------------------------------------------------------
 * h3wTimerLnkEventCallback()
 *     This function is invoked when the coordination and control timer fires.  
 *     If it is given a valid context, it invokes the coordination and 
 *     control timer event code.  
 * 
 * Parameters:
 *     evmTimerP - The event manager structure corresponding to the timer that 
 *         just fired.  
 */
void h3wTimerLnkEventCallback(struct _EvmTimer *evmTimerP)
{
    if ((void *)0 != evmTimerP->context) {
        h3wHandleTimerEvent((Hci3WStruct *)(evmTimerP->context));
    }
}



/*---------------------------------------------------------------------------
 * h3wEventCallback()
 *     This function is called by the event module when an event needs to be 
 *     handled.  If it has a valid context, it invokes the event handling 
 *     routine.  
 * 
 * Parameters:
 *     context - This parameter must be set to the address of an HCI 3-wire 
 *         coordination and control context structure.  
 *     event - This is an event structure containing information about the 
 *         event to be handled.  
 */
void h3wEventCallback(void *context, EventStruct event) 
{
    if ((void *)0 != context) {
        h3wHandleEvent((Hci3WStruct *)context, event);
    }
}



/*---------------------------------------------------------------------------
 * h3wInit()
 *     Initializes the HCI 3-wire coordination and control module.  This 
 *     function must be invoked before any of the other HCI 3-wire 
 *     coordination and control functions will work correctly.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire coordination and control module context 
 *         structure.  
 *     memPoolP - This parameter must point to a buffer that can be used as a 
 *         memory pool.  
 *     memPoolSize - This parameter must contain the size of the given memory 
 *         pool.  
 *     hciSynchFlowControl - This parameter must be TRUE if the upper HCI 
 *         layer is performing HCI synchronous flow control, or FALSE 
 *         otherwise.  
 *     role - This parameter must contain an enumeration corresponding to 
 *         whether the role is the host, a controller that delays until it 
 *         receives a message, or a controller that immediately begins 
 *         sending messages.  
 *     allowed - This parameter must contain a configuration options 
 *         structure indicating what features are allowed as long as the 
 *         other device also supports them.  
 *     afterLinkAllowOOF - This parameter must be TRUE if out of frame 
 *         software flow control is both desired and allowed by this device.  
 *     callBack - This parameter must be set to the address of a function 
 *         that can be called to signal the upper layers of events of 
 *         interest.  
*/
void h3wInit(Hci3WStruct        *cP, 
             U8                 *memPoolP,
             U32                 memPoolSize,
             BOOL                hciSynchFlowControl,
             ROLE_ENUM           role,
             ConfigOptionsStruct allowed,
             BOOL                afterLinkAllowOOF,
             void              (*callBack)(H3W_EVENT_ENUM, U8))
{
    BOOL            parityEven;
    EcbStruct       ecb;
    HCI3W_ROLE_ENUM hci3wRole;
    U8              ndx;
    U8              parityBase;
    U8              pNdx;

    cP->busy                  = 1;
    cP->privAfterLinkAllowOOF = afterLinkAllowOOF;
    cP->delayUntilSYNCRX      = FALSE;
    cP->synchReceived         = FALSE;
    switch(role) {
    case RE_CNTRLR_DELAYED:
        cP->delayUntilSYNCRX = TRUE;
        /* Fall through, no break */ 

    case RE_CNTRLR_AT_ONCE:
        hci3wRole = HR_CONTROLLER;
        break;

    case RE_HOST:
    default:
        hci3wRole = HR_HOST;
    }
    cP->dataState              = DS_UNINIT;
    cP->activeTriggered        = FALSE;
    cP->configFieldTransferred = FALSE;
    cP->configResponseReceived = FALSE;
    cP->linkMsgNeeded          = FALSE;
    cP->waitingForWoken        = FALSE;
    InitializeListEntry(&cP->timer.node);
    cP->timer.context          = (void *)cP;
    cP->timer.func             = h3wTimerLnkEventCallback;
    cP->timer.time             = (TimeT)0;
    cP->timer.startTime        = (TimeT)0;

    /* Calculate the second byte of the 
     * link messages based on their parity.  
     */ 
    for (ndx = 0; ndx < NUM_LINK_MESSAGES; ndx += 1) {
        cP->linkMsgs[ndx].octets[0] = (U8)(ndx + 1);
        parityBase = (U8)(0x007E - ndx);
        parityEven = TRUE;
        for (pNdx = 1; 0x060 > pNdx; pNdx *= 2) {
            if (parityBase & pNdx) {
                if (TRUE == parityEven) {
                    parityEven = FALSE;
                } else {
                    parityEven = TRUE;
                }
            }
        }
        if (TRUE != parityEven) {
            parityBase += 0x0080;
        }
        cP->linkMsgs[ndx].octets[1] = parityBase;
    }

    cP->rawRX.buf              = (U8             *)0;
    cP->rawRX.maxSize          =                   0;
    cP->rawRX.used             =                   0;
    cP->msgRX.buf              = (U8             *)0;
    cP->msgRX.maxSize          =                   0;
    cP->msgRX.used             =                   0;
    cP->hdrInfoP               = (FrameHdrStruct *)0;
    cP->msgTX.buf              = &cP->bufTX[0];
    cP->msgTX.maxSize          = LINK_BUFF_SIZE;
    cP->msgTX.used             =                   0;

    /* Initialize the lower level modules.  */ 
    h3wUtlInit(&cP->utlObj, 
                allowed.crc, 
                hciSynchFlowControl, 
                hci3wRole, 
                allowed.slidingWindow);
    h3wCRCInit(&cP->crcObj);
    ecb.callBack = h3wEventCallback;
    ecb.context  = cP;
    h3wEvtInit(&cP->evtObj, ecb);
    h3wHdrInit(&cP->hdrObj, &cP->evtObj, &cP->utlObj);
    h3wMemInit(&cP->memObj, memPoolP, memPoolSize);
    h3wRXQInit(&cP->datRXQObj, &cP->memObj, &cP->utlObj);
    h3wSlpInit(&cP->slpObj,
               &cP->evtObj,
               &cP->hdrObj, 
               &cP->memObj, 
               &cP->utlObj);
    h3wTXQInit(&cP->txqObj,
               &cP->crcObj, 
               &cP->evtObj, 
               &cP->hdrObj, 
               &cP->memObj, 
               &cP->slpObj, 
               &cP->utlObj);
    h3wDatInit(&cP->datObj,
               &cP->crcObj, 
               &cP->evtObj, 
               &cP->hdrObj, 
               &cP->memObj, 
               &cP->slpObj, 
               &cP->txqObj, 
               &cP->utlObj);

    cP->h3CallBack             = callBack;

    /* Initialize the synchronization message.  */ 
    cP->bufSync[0]             = cP->linkMsgs[LM_SYNC].octets[0];
    cP->bufSync[1]             = cP->linkMsgs[LM_SYNC].octets[1];
    cP->msgSync.buf            = &cP->bufSync[0];
    cP->msgSync.maxSize        = 8;
    cP->msgSync.used           = 2;

    /* Initialize the configuration message.  */ 
    cP->bufConfig[0]           = cP->linkMsgs[LM_CONF].octets[0];
    cP->bufConfig[1]           = cP->linkMsgs[LM_CONF].octets[1];
    if (HR_HOST == hci3wRole) {
        cP->bufConfig[2] = h3wUtlGetSlidingWindowSize(&cP->utlObj);
        if (TRUE == h3wUtlGetMayUseCRC(&cP->utlObj)) {
            cP->bufConfig[2]  |= 0x0010;
        }
        if (TRUE == cP->privAfterLinkAllowOOF) {
            cP->bufConfig[2]  |= 0x08;
        }
        cP->msgConfig.used     = 3;
    } else {
        cP->msgConfig.used     = 2;
    }
    cP->msgConfig.buf          = &cP->bufConfig[0];
    cP->msgConfig.maxSize      = 8;

    cP->busy                   = 0;
}



/*---------------------------------------------------------------------------
 * h3wReceive()
 *     This function receives HCI 3-wire data messages from the lower levels.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire coordination and control module context 
 *         structure.  
 *     buff - This parameter must point to a buffer where the received data 
 *         can be written.  
 *     usedP - This parameter must point to a location where the size of the 
 *         received data can be written.  
 *     maxSize - This is the maximum size of the given buffer.  
 *     packetTypeP - This parameter must point to a location where an 
 *         enumeration corresponding to the HCI 3-wire packet type can be 
 *         written.  
 *
 * Returns:
 *     This function returns zero if it successfully received a message.  
 *     Otherwise, it returns an error number.  
 */
U32 h3wReceive(Hci3WStruct      *cP,
               U8               *buff,
               U16              *usedP,
               U16               maxSize,
               PACKET_TYPE_ENUM *packetTypeP)
{
    BOOL           clearBusy;
    BOOL           semaphore;
    EventStruct    event;
    Hci3WErrStruct errObj;
    U16            ndx;
    U32            retVal;

    /* Verify that this function is not called in the context of 
     * another HCI 3-wire coordination and control module function.  
     */ 
    cP->busy += 1;
    h3wErrInit(&errObj);
    if (1 < cP->busy) {
        cP->busy = 1;
        clearBusy = FALSE;
        h3wErrSet(&errObj, ES_H3W_RECEIVE, ER_BUSY);
    } else {

        clearBusy = TRUE;
    }
    *usedP    = 0;
    semaphore = FALSE;
    if (TRUE == h3wErrHasNone(&errObj)) {

        /* clearBusy was set to TRUE if this code is reached.  */ 

        semaphore = h3wEvtGetSemaphore(&cP->evtObj);

        /* Signal the lower layers to receive an HCI 3-wire message.  */ 
        if (TRUE == h3wReceiveE(cP, TRUE, packetTypeP, &errObj)) {

            if (maxSize < cP->msgRX.used) {
                h3wErrSet(&errObj, ES_H3W_RECEIVE, ER_BUF_TOO_SMALL);
            } else {

                /* Copy the received data into the destination buffer.  */ 
                *usedP = cP->msgRX.used;
                for (ndx = 0; ndx < *usedP; ndx += 1) {
                    *(buff + ndx) = *(cP->msgRX.buf + ndx);
                }

            }
        }
    }

    /* If data was placed in the coordination and control module's 
     * receive queue because it was not a link nor a low power message, 
     * then signal the upper layers that more received data is available.  
     */ 
    if (TRUE != h3wRXQIsEmpty(&cP->datRXQObj)) {
        event.type = ET_RX_APP;
        event.data = 0;
        h3wEvtQueue(&cP->evtObj, event);
    }

    /* If this routine owns the event semaphore, it must 
     * call the event process routine.  The event process routine 
     * processes events that arrived during the time that 
     * the semaphore was held, and then releases the semaphore.
     */ 
    if (TRUE == semaphore) {
        h3wEvtProcess(&cP->evtObj);
    }

    retVal = h3wErrGet(&errObj);
    h3wErrTerm(&errObj);
    if (TRUE == clearBusy) {
        cP->busy = 0;
    }
    return(retVal);
}



/*---------------------------------------------------------------------------
 * h3wSendUnreliably()
 *     This function sends an unrealiable message.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire coordination and control module context 
 *         structure.  
 *     buff - This parameter must point to a buffer containing the message 
 *         that is to be sent.  
 *     size - This parameter must contain the size of the given buffer.  
 *     packetType - This parameter must be set to an enumeration 
 *         corresponding to the HCI 3-wire packet type.  
 *     includeCRC - This parameter must be TRUE if a cyclic redundancy check 
 *         is to be appended to the end of the message when it is framed.  
 *
 * Returns:
 *     This function returns zero if it successfully (yet unreliably) sent a 
 *     message.  Otherwise, it returns an error number.  
 */
U32 h3wSendUnreliably(Hci3WStruct     *cP,
                      U8              *buff,
                      U16              size,
                      PACKET_TYPE_ENUM packetType,
                      BOOL             includeCRC)
{
    BOOL            clearBusy;
    BuffUseStruct   dataTX;
    Hci3WErrStruct  errObj;
    U32             retVal;
    U8              handle;

    /* Verify that this function is not called in the context of 
     * another HCI 3-wire coordination and control module function.  
     */ 
    cP->busy += 1;
    h3wErrInit(&errObj);
    if (1 < cP->busy) {
        cP->busy = 1;
        clearBusy = FALSE;
        h3wErrSet(&errObj, ES_H3W_SENDUNREL, ER_BUSY);
    } else {

        clearBusy = TRUE;
        if (DS_LINK_ACTIVE == cP->dataState) {
            dataTX.buf = buff;
            dataTX.maxSize = size;
            dataTX.used = size;
            h3wSendE( cP,
                     &handle,
                      FALSE, 
                     &dataTX, 
                      packetType, 
                      FALSE, 
                      includeCRC, 
                     &errObj);
        } else {
            h3wErrSet(&errObj, ES_H3W_SENDUNREL, ER_NOT_ACTIVE);
        }
    }
    retVal = h3wErrGet(&errObj);
    h3wErrTerm(&errObj);
    if (TRUE == clearBusy) {
        cP->busy = 0;
    }
    return(retVal);
}



/*---------------------------------------------------------------------------
 * h3wSendReliably()
 *     This function sends a realiable message.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire coordination and control module context 
 *         structure.  
 *     handleP - This parameter must point to a location where an HCI 3-wire 
 *         reliable transmission handle can be written.  
 *     buff - This parameter must point to a buffer containing the message 
 *         that is to be sent.  
 *     size - This parameter must contain the size of the given buffer.  
 *     packetType - This parameter must be set to an enumeration 
 *         corresponding to the HCI 3-wire packet type.  
 *     includeCRC - This parameter must be TRUE if a cyclic redundancy check 
 *         is to be appended to the end of the message when it is framed.  
 *
 * Returns:
 *     This function returns zero if it successfully sent a message.  
 *     Otherwise, it returns an error number.  (Note that because the message 
 *     is being sent reliably, the sending is not truly complete until it is 
 *     acknowledged.)  
 */
U32 h3wSendReliably(Hci3WStruct     *cP,
                    U8              *handleP,
                    U8              *buff,
                    U16              size,
                    PACKET_TYPE_ENUM packetType,
                    BOOL             includeCRC)
{
    BOOL            clearBusy;
    BuffUseStruct   dataTX;
    Hci3WErrStruct  errObj;
    U32             retVal;

    /* Verify that this function is not called in the context of 
     * another HCI 3-wire coordination and control module function.  
     */ 
    cP->busy += 1;
    h3wErrInit(&errObj);
    if (1 < cP->busy) {
        cP->busy = 1;
        clearBusy = FALSE;
        h3wErrSet(&errObj, ES_H3W_SENDREL, ER_BUSY);
    } else {

        clearBusy = TRUE;
        if (DS_LINK_ACTIVE == cP->dataState) {
            dataTX.buf = buff;
            dataTX.maxSize = size;
            dataTX.used = size;
            h3wSendE( cP,
                      handleP,
                      FALSE, 
                     &dataTX, 
                      packetType, 
                      TRUE, 
                      includeCRC, 
                     &errObj);
        } else {
            h3wErrSet(&errObj, ES_H3W_SENDREL, ER_NOT_ACTIVE);
        }
    }
    retVal = h3wErrGet(&errObj);
    h3wErrTerm(&errObj);
    if (TRUE == clearBusy) {
        cP->busy = 0;
    }
    return(retVal);
}



/*---------------------------------------------------------------------------
 * h3wSetMaxPayloadBytes()
 *     This is an optional function that can be called at initialization time 
 *     to reduce the maximum payload size in messages.  Note that while this 
 *     will reduce memory requirements, it will also increase the overhead of 
 *     high volume message streams.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire coordination and control module context 
 *         structure.  
 *     maxPayloadBytes - The new value for the maximum number of payload 
 *         bytes.  Note that this value should range from about 10 on up to 
 *         no more than 4095.  
 *
 * Returns:
 *     This function returns zero if it successfully redefined the maximum 
 *     payload size.  Otherwise, it returns an error number.  
 */
U32 h3wSetMaxPayloadBytes(Hci3WStruct *cP, U16 maxPayloadBytes)
{
    BOOL           clearBusy;
    Hci3WErrStruct errObj;
    U32            retVal;

    /* Verify that this function is not called in the context of 
     * another HCI 3-wire coordination and control module function.  
     */ 
    cP->busy += 1;
    h3wErrInit(&errObj);
    if (1 < cP->busy) {
        cP->busy = 1;
        clearBusy = FALSE;
        h3wErrSet(&errObj, ES_H3W_SETPLD, ER_BUSY);
    } else {

        clearBusy = TRUE;
        h3wUtlSetMaxPayloadBytes(&cP->utlObj, maxPayloadBytes, &errObj);
    }
    retVal = h3wErrGet(&errObj);
    h3wErrTerm(&errObj);
    if (TRUE == clearBusy) {
        cP->busy = 0;
    }
    return(retVal);
}



/*---------------------------------------------------------------------------
 * h3wDataInit()
 *     If the initializing process has not previously been started, this 
 *     function signals the lower data frame layer that it should begin 
 *     initializing the slip layer at a given transfer rate.  In any event, 
 *     this function sets the coordination and control layer state data to 
 *     the appropriate values.  This function obtains the configuration 
 *     options put in use if the state indicates that the HCI 3-wire linking 
 *     process has completed successfully and has gone active.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire coordination and control module context 
 *         structure.  
 *     dataInitP - This parameter must point to a location where a data frame 
 *         initialization enumeration can be written.  
 *     configuredP - This parameter must point to a location where a 
 *         configuration options structure can be written.  
 *     uartBPSSpeed - This parameter must be set the transport speed at 
 *         which the HCI 3-wire code is to operate.  
 *
 * Returns:
 *     This function returns zero if it successfully checked (and initiated 
 *     if needed) the initialization state.  Otherwise, it returns an error 
 *     number.  
 */
U32 h3wDataInit(Hci3WStruct         *cP,
                DATA_INIT_ENUM      *dataInitP, 
                ConfigOptionsStruct *configuredP,
                U32                  uartBPSSpeed)
{
    BOOL            clearBusy;
    BOOL            semaphore;
    U32             retVal;
    SLIP_STATE_ENUM currentSlipState;
    Hci3WErrStruct  errObj;

    /* Verify that this function is not called in the context of 
     * another HCI 3-wire coordination and control module function.  
     */ 
    cP->busy += 1;
    semaphore = FALSE;
    h3wErrInit(&errObj);
    if (1 < cP->busy) {
        cP->busy = 1;
        clearBusy = FALSE;
        h3wErrSet(&errObj, ES_H3W_DATAINIT, ER_BUSY);
    } else {

        clearBusy = TRUE;
        semaphore = h3wEvtGetSemaphore(&cP->evtObj);
        switch(cP->dataState) {
        case DS_UNINIT:
            if (TRUE == semaphore) {

                /* The initialization process has not yet been 
                 * started, so signal the lower data frame 
                 * layer that that it should begin initializing 
                 * the slip layer at a given transfer rate.  
                 */ 
                h3wUtlSetUARTBPSSpeed(&cP->utlObj, uartBPSSpeed);
                h3wDatSlipInit(&cP->datObj, &currentSlipState);
                Assert(  (SS_UNINIT       == currentSlipState) 
                      || (SS_PARTIAL_INIT == currentSlipState) 
                      || (SS_LINKING      == currentSlipState));
                switch(currentSlipState) {
                case SS_PARTIAL_INIT:
                    *dataInitP = DI_DATA_PARTIAL;
                    cP->dataState = DS_PARTIAL_INIT;
                    break;

                case SS_LINKING:
                    *dataInitP = DI_LINK_UNINIT;
                     h3wInitiateLink(cP);
                    break;

                case SS_UNINIT:
                default:
                    *dataInitP = DI_DATA_NONE;
                    cP->dataState = DS_UNINIT;
                }

            } else {
                if (DS_UNINIT == cP->dataState) {
                    *dataInitP = DI_DATA_NONE;
                } else {
                    *dataInitP = DI_DATA_PARTIAL;
                }
            }
            break;

        case DS_PARTIAL_INIT:
            *dataInitP = DI_DATA_PARTIAL;
            break;

        case DS_LINK_UNINIT:
            *dataInitP = DI_LINK_UNINIT;
            break;

        case DS_LINK_INIT:
            *dataInitP = DI_LINK_INIT;
            break;

        case DS_LINK_ACTIVE:

            /* This is not an error case, because the link state could 
             * have gone active between the last invocation of this 
             * function, and this invocation of this function.  The 
             * caller would not be able to know that this had occurred.  
             */ 

            *dataInitP = DI_LINK_ACTIVE;
            h3wGetConfigE(cP, configuredP, &errObj);
            break;

        case DS_PARTIAL_FREE:
            h3wErrSet(&errObj, ES_H3W_INIT, ER_ALREADY_INIT);
            break;

        default:
            break;
        }

        /* If this routine owns the event semaphore, it must 
         * call the event process routine.  The event process routine 
         * processes events that arrived during the time that 
         * the semaphore was held, and then releases the semaphore.
         */ 
        if (TRUE == semaphore) {
            h3wEvtProcess(&cP->evtObj);
        }

    }
    retVal = h3wErrGet(&errObj);
    h3wErrTerm(&errObj);
    if (TRUE == clearBusy) {
        cP->busy = 0;
    }
    return(retVal);
}



/*---------------------------------------------------------------------------
 * h3wGetConfigOptions()
 *     This function obtains the configuration options that are in use.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire coordination and control module context 
 *         structure.  
 *     configOptionsP - This parameter must point to a location where a 
 *         configuration options structure can be written.  
 *
 * Returns:
 *     This function returns zero if it successfully obtained the 
 *     configuration options in use.  Otherwise, it returns an error number.  
 */
U32 h3wGetConfigOptions(Hci3WStruct         *cP, 
                        ConfigOptionsStruct *configOptionsP)
{
    BOOL           clearBusy;
    Hci3WErrStruct errObj;
    U32            retVal;

    /* Verify that this function is not called in the context of 
     * another HCI 3-wire coordination and control module function.  
     */ 
    cP->busy += 1;
    h3wErrInit(&errObj);
    if (1 < cP->busy) {
        cP->busy = 1;
        clearBusy = FALSE;
        h3wErrSet(&errObj, ES_H3W_GETCONFIG, ER_BUSY);
    } else {

        clearBusy = TRUE;
        h3wGetConfigE(cP, configOptionsP, &errObj);
    }
    retVal = h3wErrGet(&errObj);
    h3wErrTerm(&errObj);
    if (TRUE == clearBusy) {
        cP->busy = 0;
    }
    return(retVal);
}



/*---------------------------------------------------------------------------
 * h3wSleep()
 *     This is an optional function that informs the other device that this 
 *     software is entering its sleep mode state.  It should not be used unless 
 *     the device making the call has a low power sleep mode, and unless no 
 *     HCI 3-wire communication is needed for the time being.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire coordination and control module context 
 *         structure.  
 *
 * Returns:
 *     This function returns zero if it successfully sent the other device a 
 *     SLEEP message.  Otherwise, it returns an error number.  Any further 
 *     action such as entering a low power mode is up to the upper layers to 
 *     carry out.  
 */
U32 h3wSleep(Hci3WStruct *cP)
{
    BOOL           clearBusy;
    Hci3WErrStruct errObj;
    U8             handle;
    U32            retVal;

    /* Verify that this function is not called in the context of 
     * another HCI 3-wire coordination and control module function.  
     */ 
    cP->busy += 1;
    h3wErrInit(&errObj);
    if (1 < cP->busy) {
        cP->busy = 1;
        clearBusy = FALSE;
        h3wErrSet(&errObj, ES_H3W_SLEEP, ER_BUSY);
    } else {

        clearBusy = TRUE;
        if (DS_LINK_ACTIVE == cP->dataState) {
            if (TRUE == h3wUtlGetOtherAssumedAwake(&cP->utlObj)) {
                cP->msgTX.buf[0] = cP->linkMsgs[LM_SLEEP].octets[0];
                cP->msgTX.buf[1] = cP->linkMsgs[LM_SLEEP].octets[1];
                cP->msgTX.used = 2;
                if (h3wSendE( cP, 
                             &handle, 
                              TRUE, 
                             &cP->msgTX, 
                              PT_LINK, 
                              FALSE, 
                              FALSE, 
                             &errObj)) {
                    h3wUtlSetOtherAwake(&cP->utlObj, FALSE);
                }
            } else {
                h3wErrSet(&errObj, ES_H3W_SLEEP, ER_NOT_AWAKE);
            }
        } else {
            h3wErrSet(&errObj, ES_H3W_SLEEP, ER_NOT_ACTIVE);
        }
    }
    retVal = h3wErrGet(&errObj);
    h3wErrTerm(&errObj);
    if (TRUE == clearBusy) {
        cP->busy = 0;
    }
    return(retVal);
}



/*---------------------------------------------------------------------------
 * h3wWakeup()
 *     This function sends a wakeup message to the other device.  This 
 *     function is not needed if the other device does not have a SLEEP mode.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire coordination and control module context 
 *         structure.  
 *     lowPowerStateP - This parameter must point to a location where an 
 *         enumeration corresponding to the assumed low power state can be 
 *         written.
 *
 * Returns:
 *     This function returns zero if it successfully sent the other device a 
 *     WAKEUP message.  Otherwise, it returns an error number.  
 */
U32 h3wWakeup(Hci3WStruct *cP, LOW_POWER_ENUM *lowPowerStateP)
{
    BOOL           clearBusy;
    EventStruct    event;
    Hci3WErrStruct errObj;
    U32            retVal;

    /* Verify that this function is not called in the context of 
     * another HCI 3-wire coordination and control module function.  
     */ 
    cP->busy += 1;
    h3wErrInit(&errObj);
    if (1 < cP->busy) {
        cP->busy = 1;
        clearBusy = FALSE;
        h3wErrSet(&errObj, ES_H3W_WAKEUP, ER_BUSY);
    } else {

        clearBusy = TRUE;
        switch(cP->dataState) {
        case DS_LINK_UNINIT:
        case DS_LINK_INIT:
        case DS_LINK_ACTIVE:
            event.type = ET_LOW_POWER_TX_WAKEUP;
            event.data = 0;
            h3wEvtQueue(&cP->evtObj, event);
            break;

        default:
            h3wErrSet(&errObj, ES_H3W_WAKEUP, ER_NOT_ACTIVE);
        }
    }
    retVal = h3wErrGet(&errObj);
    h3wErrTerm(&errObj);
    if (TRUE == h3wUtlGetOtherAssumedAwake(&cP->utlObj)) {
        *lowPowerStateP = LP_AWAKE;
    } else {
        *lowPowerStateP = LP_ASLEEP;
    }
    if (TRUE == clearBusy) {
        cP->busy = 0;
    }
    return(retVal);
}



/*---------------------------------------------------------------------------
 * h3wDataFree()
 *     If the freeing process has not previously been started, this function 
 *     signals the lower data frame layer that it should begin freeing the 
 *     slip layer.  In any event, this function sets the coordination and 
 *     control layer state data to the appropriate values.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire coordination and control module context 
 *         structure.  
 *     dataFreeP - This parameter must point to a location where an 
 *         enumeration corresponding to the data free state can be written.  
 *
 * Returns:
 *     This function returns zero if it successfully checked (and began 
 *     freeing if needed) the free state.  Otherwise, it returns an error 
 *     number.  
 */
U32 h3wDataFree(Hci3WStruct *cP, DATA_FREE_ENUM *dataFreeP) 
{
    BOOL            clearBusy;
    BOOL            semaphore;
    Hci3WErrStruct  errObj;
    U32             retVal;
    SLIP_STATE_ENUM currentSlipState;

    /* Verify that this function is not called in the context of 
     * another HCI 3-wire coordination and control module function.  
     */ 
    cP->busy += 1;
    semaphore = FALSE;
    h3wErrInit(&errObj);
    if (1 < cP->busy) {
        cP->busy = 1;
        clearBusy = FALSE;
        h3wErrSet(&errObj, ES_H3W_DATAFREE, ER_BUSY);
    } else {

        clearBusy = TRUE;
        semaphore = h3wEvtGetSemaphore(&cP->evtObj);
        switch(cP->dataState) {
        case DS_UNINIT:

            /* This is not an error case, because the link state could 
             * have become uninitialized between the last invocation of 
             * this function, and this invocation of this function.  The 
             * caller would not be able to know that this had occurred.  
             */ 

            *dataFreeP = DF_FREE;
            break;

        case DS_PARTIAL_INIT:
        case DS_LINK_UNINIT:
        case DS_LINK_INIT:
        case DS_LINK_ACTIVE:
            if (TRUE == semaphore) {
                h3wDatSlipFree(&cP->datObj, &currentSlipState, &errObj);
                Assert(  (SS_PARTIAL_INIT == currentSlipState) 
                      || (SS_LINKING      == currentSlipState) 
                      || (SS_ACTIVE       == currentSlipState) 
                      || (SS_PARTIAL_FREE == currentSlipState) 
                      || (SS_UNINIT       == currentSlipState));
                switch(currentSlipState) {
                case SS_PARTIAL_INIT:
                case SS_LINKING:
                    Assert(  (DS_LINK_UNINIT == cP->dataState) 
                          || (DS_LINK_INIT   == cP->dataState) 
                          || (DS_LINK_ACTIVE == cP->dataState));
                    *dataFreeP = DF_WORKING;
                    break;

                case SS_ACTIVE:
                    Assert(DS_LINK_ACTIVE == cP->dataState);
                    *dataFreeP = DF_WORKING;
                    break;

                case SS_PARTIAL_FREE:
                    cP->dataState = DS_PARTIAL_FREE;
                    *dataFreeP = DF_PARTIAL_FREE;
                    cP->activeTriggered = FALSE;
                    break;

                case SS_UNINIT:
                default:
                    cP->dataState = DS_UNINIT;
                    *dataFreeP = DF_FREE;
                    cP->activeTriggered = FALSE;
                }
            } else {
                if (DS_PARTIAL_FREE == cP->dataState) {
                    *dataFreeP = DF_PARTIAL_FREE;
                } else {
                    *dataFreeP = DF_WORKING;
                }
            }
            break;

        case DS_PARTIAL_FREE:
            *dataFreeP = DF_PARTIAL_FREE;
            break;

        default:
            break;
        }

        /* If this routine owns the event semaphore, it must 
         * call the event process routine.  The event process routine 
         * processes events that arrived during the time that 
         * the semaphore was held, and then releases the semaphore.
         */ 
        if (TRUE == semaphore) {
            h3wEvtProcess(&cP->evtObj);
        }

    }
    retVal = h3wErrGet(&errObj);
    h3wErrTerm(&errObj);
    if (TRUE == clearBusy) {
        cP->busy = 0;
    }
    return(retVal);
}



/*---------------------------------------------------------------------------
 * h3wGetMaxPayloadBytes()
 *     This function obtains the maximum number of payload bytes allowed.  
 *     (Note that this value will be 4095 unless the code was instructed to 
 *     reduce this default value.)
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire coordination and control module context 
 *         structure.  
 *     maxPayloadBytesP - This parameter must point to a location where the 
 *         maximum number of payload bytes can be written.  
 *
 * Returns:
 *     This function returns zero if it successfully obtained the maximum 
 *     number of payload bytes value.  Otherwise, it returns an error number.  
 */
U32 h3wGetMaxPayloadBytes(Hci3WStruct *cP, U16 *maxPayloadBytesP)
{
    BOOL           clearBusy;
    Hci3WErrStruct errObj;
    U32            retVal;

    /* Verify that this function is not called in the context of 
     * another HCI 3-wire coordination and control module function.  
     */ 
    cP->busy += 1;
    h3wErrInit(&errObj);
    if (1 < cP->busy) {
        cP->busy = 1;
        clearBusy = FALSE;
        h3wErrSet(&errObj, ES_H3W_GETPLD, ER_BUSY);
    } else {

        clearBusy = TRUE;
        *maxPayloadBytesP = h3wUtlGetMaxPayloadBytes(&cP->utlObj);
    }
    retVal = h3wErrGet(&errObj);
    h3wErrTerm(&errObj);
    if (TRUE == clearBusy) {
        cP->busy = 0;
    }
    return(retVal);
}



/*---------------------------------------------------------------------------
 * h3wUARTEncodingToBPS()
 *     Converts a given encoding expected by the lower UART layer software 
 *     into an bits per second transfer rate.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire coordination and control module context 
 *         structure.  
 *     bpsP - This parameter must point to a location where the bits per 
 *         second rate can be written.  
 *     uartSpeedEncoding - This parameter must containt the UART speed 
 *         encoded as expected by the lower UART software.  
 *
 * Returns:
 *     This function returns zero if it successfully converted the given 
 *     encoding expected by the lower UART layer software into an bits per 
 *     second transfer rate.  Otherwise, it returns an error number.  
 */
U32 h3wUARTEncodingToBPS(Hci3WStruct *cP, U32 *bpsP, U16 uartSpeedEncoding)
{
    BOOL           clearBusy;
    Hci3WErrStruct errObj;
    U32            retVal;

    /* Verify that this function is not called in the context of 
     * another HCI 3-wire coordination and control module function.  
     */ 
    cP->busy += 1;
    h3wErrInit(&errObj);
    *bpsP = 0;
    if (1 < cP->busy) {
        cP->busy = 1;
        clearBusy = FALSE;
        h3wErrSet(&errObj, ES_H3W_UARTTOBPS, ER_BUSY);
    } else {

        clearBusy = TRUE;
        *bpsP = h3wUtlUARTEncodingToBPS(&cP->utlObj, uartSpeedEncoding);
    }
    retVal = h3wErrGet(&errObj);
    h3wErrTerm(&errObj);
    if (TRUE == clearBusy) {
        cP->busy = 0;
    }
    return(retVal);
}



/*---------------------------------------------------------------------------
 * h3wSetUARTClockMultiplier()
 *     Sets the value of the UART clock multiplier.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire coordination and control module context 
 *         structure.  
 *     uartClockMultiplier - An enumeration corresponding to the amount the 
 *         UART speeds are overclocked.  
 *
 * Returns:
 *     This function returns zero if it successfully set the value of the 
 *     UART clock multiplier.  Otherwise, it returns an error number.  
 */
U32 h3wSetUARTClockMultiplier(Hci3WStruct   *cP, 
                              OVERCLOCK_ENUM uartClockMultiplier)
{
    BOOL           clearBusy;
    Hci3WErrStruct errObj;
    U32            retVal;

    /* Verify that this function is not called in the context of 
     * another HCI 3-wire coordination and control module function.  
     */ 
    cP->busy += 1;
    h3wErrInit(&errObj);
    if (1 < cP->busy) {
        cP->busy = 1;
        clearBusy = FALSE;
        h3wErrSet(&errObj, ES_H3W_CLOCKMULT, ER_BUSY);
    } else {

        clearBusy = TRUE;
        h3wUtlSetUARTMultiplier(&cP->utlObj, uartClockMultiplier);
    }
    retVal = h3wErrGet(&errObj);
    h3wErrTerm(&errObj);
    if (TRUE == clearBusy) {
        cP->busy = 0;
    }
    return(retVal);
}



/*---------------------------------------------------------------------------
 * h3wDataSetSpeed()
 *     This function redefines the speed at which the Data Layer is to 
 *     operate.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire coordination and control module context 
 *         structure.  
 *     bps - This is the new bits per second rate that is to be used.
 *
 * Returns:
 *     This function returns TRUE if it was successfully able to set the Data 
 *     Layer data transfer speed, or FALSE otherwise.  
 */
U32 h3wDataSetSpeed(Hci3WStruct *cP, U32 bps) 
{
    BOOL           clearBusy;
    Hci3WErrStruct errObj;
    U32            retVal;

    /* Verify that this function is not called in the context of 
     * another HCI 3-wire coordination and control module function.  
     */ 
    cP->busy += 1;
    h3wErrInit(&errObj);
    if (1 < cP->busy) {
        cP->busy = 1;
        clearBusy = FALSE;
        h3wErrSet(&errObj, ES_H3W_SETSPEED, ER_BUSY);
    } else {

        clearBusy = TRUE;
        h3wDatSlipSetSpeed(&cP->datObj, bps, &errObj);
    }
    retVal = h3wErrGet(&errObj);
    h3wErrTerm(&errObj);
    if (TRUE == clearBusy) {
        cP->busy = 0;
    }
    return(retVal);
}



/*---------------------------------------------------------------------------
 * h3wTerm()
 *     This function destroys a given coordination and control module, 
 *     cleaning up any allocated resources.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire coordination and control module context 
 *         structure.  
 */
void h3wTerm(Hci3WStruct *cP)
{
    Hci3WErrStruct errObj;

    cP->busy = 1;
    cP->h3CallBack = (void (*)(H3W_EVENT_ENUM, U8))0;
    cP->timer.context = (void *)0;
    if (0 < cP->rawRX.maxSize) {
        h3wErrInit(&errObj);
        h3wMemFree(&cP->memObj, cP->rawRX.buf, &errObj);
        cP->rawRX.buf     = (U8             *)0;
        cP->rawRX.used    =                   0;
        cP->rawRX.maxSize =                   0;
        cP->hdrInfoP      = (FrameHdrStruct *)0;
        cP->msgRX.buf     = (U8             *)0;
        cP->msgRX.used    =                   0;
        cP->msgRX.maxSize =                   0;
        h3wErrReportUnsolicited(&errObj, TRUE, "n RX buffer free");
        h3wErrTerm(&errObj);
    }

    /* Due to interobject dependencies, these objects should be 
     * deleted in the reverse order from when they were constructed.  
     */ 
    h3wDatTerm(&cP->datObj);
    h3wTXQTerm(&cP->txqObj);
    h3wSlpTerm(&cP->slpObj);
    h3wRXQTerm(&cP->datRXQObj);
    h3wMemTerm(&cP->memObj);
    h3wHdrTerm(&cP->hdrObj);
    h3wEvtTerm(&cP->evtObj);
    h3wCRCTerm(&cP->crcObj);
    h3wUtlTerm(&cP->utlObj);

}



/****************************************************************************
 *
 * Private Functions
 *
 ****************************************************************************/



/*---------------------------------------------------------------------------
 * h3wHandleTimerEvent()
 *     This function schedules a link message send or a wakeup message send 
 *     event, depending on the state.  It reschedules the timer when 
 *     necessary.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire coordination and control module context 
 *         structure.  
 */
static void h3wHandleTimerEvent(Hci3WStruct *cP) 
{
    BOOL        rescheduleTimer;
    EventStruct event;
    U32         delay;

    delay = 0;
    rescheduleTimer = FALSE;
    if (TRUE == h3wUtlGetOtherAssumedAwake(&cP->utlObj)) {
        if (TRUE == cP->linkMsgNeeded) {
            Assert(TRUE != cP->waitingForWoken);
            if (  TRUE
               == h3wUtlGetFlowControl(&cP->utlObj,
                                        FL_OUT_OF_FRAME_SOFTWARE,
                                        FT_CURRENT_STATE)) {

                /* Schedule a link message send event.  */ 
                event.type = ET_LINK_TX_LINK;
                event.data = 0;
                h3wEvtQueue(&cP->evtObj, event);

            }

            /* Determine if we need to keep sending link messages.  */ 
            switch(cP->dataState) {
            case DS_LINK_UNINIT:
            case DS_LINK_INIT:
                rescheduleTimer = TRUE;
                delay           = 250;
                break;

            default:
                cP->linkMsgNeeded = FALSE;
            }

        }
    } else {
        if (TRUE == cP->linkMsgNeeded) {
            cP->waitingForWoken = TRUE;
        }
        if (TRUE == cP->waitingForWoken) {

            /* Schedule a wakup message send event.  */ 
            event.type = ET_LOW_POWER_TX_WAKEUP;
            event.data = 0;
            h3wEvtQueue(&cP->evtObj, event);

            rescheduleTimer = TRUE;
            delay           = h3wUtlGetTimeOneChar(&cP->utlObj);
        }
    }
    if (TRUE == rescheduleTimer) {
        EVM_StartTimer(&cP->timer, delay);
    }
}



/*---------------------------------------------------------------------------
 * h3wScheduleLinkTimer()
 *     This functions starts the link timer and indicates that link messages 
 *     need to be transmitted.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire coordination and control module context 
 *         structure.  
 */
static void h3wScheduleLinkTimer(Hci3WStruct *cP)
{
    if (TRUE != cP->linkMsgNeeded) {
        cP->linkMsgNeeded = TRUE;
    }
    EVM_StartTimer(&cP->timer, 250);
}



/*---------------------------------------------------------------------------
 * h3wHandleEvent()
 *     This function handles events that need to be processed.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire coordination and control module context 
 *         structure.  
 *     event - This contains an event structure with information about the 
 *         event that needs to be processed.  
 */
static void h3wHandleEvent(Hci3WStruct *cP, EventStruct event)
{
    AckStruct       ack;
    Hci3WErrStruct  errObj;
    SLIP_STATE_ENUM currentSlipState;

    if ((void(*)(H3W_EVENT_ENUM, U8))0 != cP->h3CallBack) {
        switch(cP->dataState) {
        case DS_PARTIAL_INIT:
            if (ET_UART_INIT == event.type) {
                h3wDatSlipLinking(&cP->datObj, &currentSlipState);
                Assert(SS_LINKING == currentSlipState);
                h3wInitiateLink(cP);
            }
            break;

        case DS_LINK_UNINIT:
            switch(event.type) {
            case ET_LINK_TX_LINK:
                h3wSendLnk(cP);
                break;

            case ET_LINK_TX_SYNC_RESP:
                h3wSendSyncResp(cP);
                break;

            case ET_LINK_INIT:
                h3wLinkInit(cP);
                break;

            case ET_LINK_TX_NOW:
                h3wSendNow(cP);
                break;

            case ET_RX_UART:
                h3wLinkRX(cP, " receive (link uninit)");
                break;

            default:
                break;
            }
            break;

        case DS_LINK_INIT:
            switch(event.type) {
            case ET_LINK_TX_LINK:
                h3wSendLnk(cP);
                break;

            case ET_LINK_TX_SYNC_RESP:
                h3wSendSyncResp(cP);
                break;

            case ET_LINK_INIT_TX_CONF_RESP:
                h3wSendConfigRespInit(cP, event.data);
                break;

            case ET_LINK_ACTIVE:
                h3wLinkActive(cP);
                break;

            case ET_RX_UART:
                h3wLinkRX(cP, " receive (link init)");
                break;

            default:
                break;
            }
            break;

        case DS_LINK_ACTIVE:
            switch(event.type) {
            case ET_LINK_ACTIVATED:
                cP->h3CallBack(H3_ACTIVATED, '\0');
                break;

            case ET_LINK_RESET:
                h3wReset(cP);
                break;

            case ET_LINK_ACTIVE_TX_CONF_RESP:
                h3wSendConfigRespActive(cP);
                break;

            case ET_TX_RESEND_RELIABLE:
                h3wErrInit(&errObj);

                /* The h3wUtlGetLastRXAck() function may not return a valid 
                 * number for one of two reasons.  Either no messages have 
                 * ever been received, or the link just went active so that 
                 * the sequence and acknowledgement numbers were reset.  
                 */ 

                h3wUtlGetLastRXAck(&cP->utlObj, &ack);
                h3wTXQResendReliable(&cP->txqObj, 
                                      ack, 
                                      cP->h3CallBack, 
                                     &errObj);
                h3wErrReportUnsolicited(&errObj, TRUE, " reliable resend");
                h3wErrTerm(&errObj);
                break;

            case ET_TX_PURE_ACK:
                h3wErrInit(&errObj);

                /* The call to pureAck will fail under 
                 * certain circumstances such as when 
                 * an XOFF has been previously received.  
                 */ 
                h3wDatPureAck(&cP->datObj, &errObj);

                h3wErrReportUnsolicited(&errObj, 
                                         TRUE, 
                                         " pure acknowledgement");
                h3wErrTerm(&errObj);
                break;

            case ET_TX_WRITE_COMPLETE:
                cP->h3CallBack(H3_TRANSMIT_COMPLETE, '\0');
                break;

            case ET_RX_UART:
                h3wLinkRX(cP, " receive (link active)");
                break;

            case ET_RX_APP:
                cP->h3CallBack(H3_DATA_RECEIVED, '\0');
                break;

            case ET_LOW_POWER_TX_WOKEN:
                h3wSendWoken(cP);
                break;

            case ET_LOW_POWER_TX_WAKEUP:
                h3wSendWakeup(cP);
                break;

            case ET_LOW_POWER_SLEEP:
                cP->h3CallBack(H3_LOW_POWER_SLEEP, '\0');
                break;

            case ET_LOW_POWER_WOKEN:
                cP->h3CallBack(H3_LOW_POWER_WOKEN, '\0');
                break;

            default:
                break;
            }
            break;

        case DS_PARTIAL_FREE:
            if (ET_UART_FREE == event.type) {
                h3wDatSlipTerminated(&cP->datObj, &currentSlipState);
                Assert(SS_UNINIT == currentSlipState);
                cP->dataState = DS_UNINIT;
                cP->activeTriggered = FALSE;

                /* Inform the invoking layer that the 
                 * link has been successfully terminated.  
                 */ 
                event.type = ET_LINK_TERMINATED;
                event.data = 0;
                h3wEvtQueue(&cP->evtObj, event);

            }
            break;

        case DS_UNINIT:
        default:
            if (ET_LINK_TERMINATED == event.type) {
                cP->h3CallBack(H3_TERMINATED, '\0');
            }
        }
    }
}



/*---------------------------------------------------------------------------
 * h3wReset()
 *     This function resets the HCI 3-wire link.  It should be called when a 
 *     SYNC message is unexpectedly received from the other device, because 
 *     this implies that the other device just reset.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire coordination and control module context 
 *         structure.  
 */
static void h3wReset(Hci3WStruct *cP)
{
    Hci3WErrStruct  errObj;
    SLIP_STATE_ENUM currentSlipState;

    Assert(DS_LINK_ACTIVE == cP->dataState);
    cP->timer.context = (void *)0;
    cP->configFieldTransferred = FALSE;
    cP->configResponseReceived = FALSE;
    h3wErrInit(&errObj);
    h3wTXQFree(&cP->txqObj, &errObj);
    h3wErrReportUnsolicited(&errObj, TRUE, " Link Reset");
    h3wErrTerm(&errObj);
    h3wHdrClearNumRXNeedingACK(&cP->hdrObj);
    cP->timer.context = (void *)cP;
    h3wDatSlipResetLink(&cP->datObj, &currentSlipState);
    Assert(SS_LINKING == currentSlipState);
    h3wInitiateLink(cP);
    cP->h3CallBack(H3_LINK_LOST, '\0');
}



/*---------------------------------------------------------------------------
 * h3wSendNow()
 *     This function stops any pending link action and schedules a send link 
 *     message event.  It schedules (or reschedules) the link timer.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire coordination and control module context 
 *         structure.  
 */
static void h3wSendNow(Hci3WStruct *cP)
{
    EventStruct event;

    cP->linkMsgNeeded = FALSE;
    event.type = ET_LINK_TX_LINK;
    event.data = 0;
    h3wEvtQueue(&cP->evtObj, event);
    h3wScheduleLinkTimer(cP);
}



/*---------------------------------------------------------------------------
 * h3wSendSyncResp()
 *     This function transmits a SYNC_RESP message.  It should be invoked 
 *     when a SYNC message is received.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire coordination and control module context 
 *         structure.  
 */
static void h3wSendSyncResp(Hci3WStruct *cP)
{
    Hci3WErrStruct errObj;
    U8             handle;

    cP->synchReceived = TRUE;
    cP->linkMsgNeeded = FALSE;
    cP->msgTX.buf[0] = cP->linkMsgs[LM_SYNC_RESPONSE].octets[0];
    cP->msgTX.buf[1] = cP->linkMsgs[LM_SYNC_RESPONSE].octets[1];
    cP->msgTX.used = 2;
    h3wErrInit(&errObj);
    h3wSendE( cP,
             &handle,
              FALSE, 
             &cP->msgTX, 
              PT_LINK, 
              FALSE, 
              FALSE, 
             &errObj);
    h3wErrReportUnsolicited(&errObj, FALSE, " SYNC RESPONSE send");
    h3wErrTerm(&errObj);
    h3wScheduleLinkTimer(cP);
}



/*---------------------------------------------------------------------------
 * h3wLinkInit()
 *     This function should be called after a SYNC_RESP message is received.  
 *     It sets the link state to "initialized", reports the event, and 
 *     commences the sending of CONFIG messages.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire coordination and control module context 
 *         structure.  
 */
static void h3wLinkInit(Hci3WStruct *cP)
{
    Assert(DS_LINK_UNINIT == cP->dataState);
    cP->dataState = DS_LINK_INIT;
    Report(("HCI3W:  HCI 3-Wire Link Set to the Initialized State.\n"));

    /* Begin sending CONFIG messages */ 
    h3wSendNow(cP);

}



/*---------------------------------------------------------------------------
 * h3wReportLinkMsgRX()
 *     This function reports the reception of a link message.  
 * 
 * Parameters:
 *     msg - This parameter must contain an enumeration corresponding to the 
 *         link message just received.  
 */
static void h3wReportLinkMsgRX(LINK_MESSEGES_ENUM msg)
{
    char *msgStr;

    switch(msg) {
    case LM_SYNC_RESPONSE:
        msgStr = "SYNC RESPONSE";
        break;

    case LM_CONF:
        msgStr = "CONFIG";
        break;

    case LM_CONF_RESPONSE:
        msgStr = "CONFIG RESPONSE";
        break;

    case LM_WAKEUP:
        msgStr = "WAKEUP";
        break;

    case LM_WOKEN:
        msgStr = "WOKEN";
        break;

    case LM_SLEEP:
        msgStr = "SLEEP";
        break;

    case LM_SYNC:
    default:
        msgStr = "SYNC";
    }
    Report(("HCI3W:  A %s message has been received.\n", msgStr));
}



/*---------------------------------------------------------------------------
 * h3wHandleAwake()
 *     This function should be invoked when it is determined that the other 
 *     device is awake.  It changes the state variables accordingly, and 
 *     schedules the woken event.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire coordination and control module context 
 *         structure.  
 */
static void h3wHandleAwake(Hci3WStruct *cP) 
{
    EventStruct event;

    if (TRUE != h3wUtlGetOtherAssumedAwake(&cP->utlObj)) {
        cP->waitingForWoken = FALSE;
        h3wUtlSetOtherAwake(&cP->utlObj, TRUE);
        event.type = ET_LOW_POWER_WOKEN;
        event.data = 0;
        h3wEvtQueue(&cP->evtObj, event);
    }
}



/*---------------------------------------------------------------------------
 * h3wHandleUninitRX()
 *     This function is invoked to handle messages received while in the 
 *     uninitialized link state.  If a SYNC message is received, it sets up 
 *     a SYNC message send event.  If a SYNC_RESP message is received, it 
 *     schedules a link initialized events.  For any other message received, 
 *     it schedules a SYNC message send event.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire coordination and control module context 
 *         structure.  
 */
static void h3wHandleUninitRX(Hci3WStruct *cP)
{
    BOOL        sendSync;
    EventStruct event;

    Assert((U8 *)0 != cP->msgRX.buf);
    sendSync = TRUE;
    if (2 == cP->msgRX.used) {
        if (  cP->linkMsgs[LM_SYNC].octets[0] 
           == cP->msgRX.buf[0]) {
            if (  cP->linkMsgs[LM_SYNC].octets[1] 
               == cP->msgRX.buf[1]) {
                h3wReportLinkMsgRX(LM_SYNC);
                h3wHandleAwake(cP);

                /* Send a SYNC RESPONSE message. */ 
                sendSync = FALSE;
                event.type = ET_LINK_TX_SYNC_RESP;
                event.data = 0;
                h3wEvtQueue(&cP->evtObj, event);

            }
        } else {
            if (  cP->linkMsgs[LM_SYNC_RESPONSE].octets[0] 
               == cP->msgRX.buf[0]) {
                if (  cP->linkMsgs[LM_SYNC_RESPONSE].octets[1] 
                   == cP->msgRX.buf[1]) {
                    h3wReportLinkMsgRX(LM_SYNC_RESPONSE);
                    h3wHandleAwake(cP);

                    /* Go to the link initialized state. */ 
                    sendSync = FALSE;
                    event.type = ET_LINK_INIT;
                    event.data = 0;
                    h3wEvtQueue(&cP->evtObj, event);

                }
            }
        }
    }
    if (TRUE == sendSync) {

        /* Respond to all other messages with a SYNC message. */ 
        if (  (TRUE  == cP->delayUntilSYNCRX) 
           && (FALSE == cP->synchReceived)) {

            /* The code must not have yet started the link timer, 
             * because the invoker asked it to act as a controller, and 
             * to wait for the first SYNC message to be received before 
             * transmitting link messages at quarter second intervals.  
             */ 
            Assert(TRUE != cP->linkMsgNeeded);

            event.type = ET_LINK_TX_LINK;
            event.data = 0;
            h3wEvtQueue(&cP->evtObj, event);
        } else {
            event.type = ET_LINK_TX_NOW;
            event.data = 0;
            h3wEvtQueue(&cP->evtObj, event);
        }

    }
}



/*---------------------------------------------------------------------------
 * h3wHandleConfigField()
 *     This function is called to handle the reception of a CONFIG field.  It 
 *     changes the values proposed to those actually being used.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire coordination and control module context 
 *         structure.  
 *     configField - This must contain the configuration field byte that was 
 *         just received.  
 */
static void h3wHandleConfigField(Hci3WStruct *cP, U8 configField)
{
    SLIDING_WIN_ENUM slidingWindow;

    /* Handle the cyclic redundancy check option.  */ 
    if (TRUE == h3wUtlGetMayUseCRC(&cP->utlObj)) {
        if (0 == (0x10 & configField)) {
            h3wUtlSetMayUseCRC(&cP->utlObj, FALSE);
        }
    }

    /* Handle the sliding window option.  */ 
    if ((0x07 & configField) < h3wUtlGetSlidingWindowSize(&cP->utlObj)) {
        switch(0x07 & configField) {
        case 2:
            slidingWindow = SW_LOWER;
            break;

        case 3:
            slidingWindow = SW_LOW;
            break;

        case 4:
            slidingWindow = SW_MID;
            break;

        case 5:
            slidingWindow = SW_HIGH;
            break;

        case 6:
            slidingWindow = SW_HIGHER;
            break;

        /* There is no need for a case 7:, because the 
         * configField contains a value that is smaller than 
         * the current sliding window, so it cannot be 7.  
         */ 

        case 1:
        default:
            slidingWindow = SW_LOWEST;
        }
        h3wUtlSetSlidingWindow(&cP->utlObj, slidingWindow);
    }

    /* Handle the out of frame software flow control option.  */ 
    if (TRUE == cP->privAfterLinkAllowOOF) {
        if ((0x08 & configField)) {
            h3wUtlSetFlowControl(&cP->utlObj,
                                  FL_OUT_OF_FRAME_SOFTWARE, 
                                  FT_CHOKE_ABILITY, 
                                  TRUE);
        }
    }

    cP->configFieldTransferred = TRUE;
}



/*---------------------------------------------------------------------------
 * h3wHandleActiveTrigger()
 *     This function should be invoked when the link state goes active.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire coordination and control module context 
 *         structure.  
 *
 * Returns:
 *     This function returns an enumeration corresponding to whether the 
 *     activation has not triggered, just triggered, or has already been 
 *     triggered.  
 */
static TRIGGER_ENUM h3wHandleActiveTrigger(Hci3WStruct *cP)
{
    EventStruct  event;
    TRIGGER_ENUM retVal;

    retVal = TE_NOT_TRIGGERED;
    if (TRUE == cP->activeTriggered) {
        retVal = TE_PREV_TRIGGERED;
    } else {
        if (TRUE == cP->configFieldTransferred) {
            if (TRUE == cP->configResponseReceived) {

                /* There are three events that could trigger changing 
                 * the link state from initialized to active:  
                 *
                 *    1.   The reception of a CONFIG RESPONSE message 
                 *         by the host with a configuration field.  
                 *    2.   The reception of a CONFIG RESPONSE message by 
                 *         the controller with no configuration field.  
                 *    3.   The transmission of a CONFIG RESPONSE message 
                 *         by the controller with a configuration field.  
                 *
                 * In order to avoid a technical violation of the HCI 
                 * 3-wire specification, the code should only consider 
                 * the first two events.  However, the controller must 
                 * not transition to the active state until both event 
                 * #2 and event #3 have occurred, to work around an 
                 * oversight in the HCI 3-wire specification.  See the 
                 * Bluetooth Errata 1838 for additional details.  
                 */ 

                retVal = TE_JUST_TRIGGERED;
                cP->activeTriggered = TRUE;
                event.type = ET_LINK_ACTIVE;
                event.data = 0;
                h3wEvtQueue(&cP->evtObj, event);

            }
        }
    }
    return(retVal);
}



/*---------------------------------------------------------------------------
 * h3wSendConfigRespInit()
 *     This function is called when a CONFIG_RESP message needs to be sent 
 *     during the link initialized state.  It handles the configuration field 
 *     if one was just recieved.  It transmits the CONFIG_RESP message.  If 
 *     the active event was not just triggered, then it schedules the link 
 *     timer to run.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire coordination and control module context 
 *         structure.  
 *     confField - The configuration field just received if the code is 
 *         running as a controller.  Otherwise, this parameter is meaningless.  
 */
static void h3wSendConfigRespInit(Hci3WStruct *cP, 
                                  U8           confField)
{
    Hci3WErrStruct errObj;
    U8             handle;

    /* Handle the configuration field if one was just recieved.  */ 
    if (HR_CONTROLLER == h3wUtlGetRole(&cP->utlObj)) {
        h3wHandleConfigField(cP, confField);
    }

    /* Send a CONFIG RESPONSE message.  */ 
    cP->linkMsgNeeded = FALSE;
    cP->msgTX.buf[0] = cP->linkMsgs[LM_CONF_RESPONSE].octets[0];
    cP->msgTX.buf[1] = cP->linkMsgs[LM_CONF_RESPONSE].octets[1];
    if (HR_HOST == h3wUtlGetRole(&cP->utlObj)) {
        cP->msgTX.used = 2;
    } else {
        cP->msgTX.buf[2] = h3wUtlGetSlidingWindowSize(&cP->utlObj);
        if (  TRUE 
           == h3wUtlGetFlowControl(&cP->utlObj,
                                    FL_OUT_OF_FRAME_SOFTWARE, 
                                    FT_CHOKE_ABILITY)) {
            cP->msgTX.buf[2] |= 0x08;
        }
        if (TRUE == h3wUtlGetMayUseCRC(&cP->utlObj)) {
            cP->msgTX.buf[2] |= 0x0010;
        }
        cP->msgTX.used = 3;
    }
    h3wErrInit(&errObj);
    h3wSendE( cP, 
             &handle,
              FALSE, 
             &cP->msgTX, 
              PT_LINK, 
              FALSE, 
              FALSE, 
             &errObj);
    h3wErrReportUnsolicited(&errObj, FALSE, " CONFIG RESPONSE send");
    if (TRUE == h3wErrHasNone(&errObj)) {

        /* Note whether the configuration field has been transferred.  */ 
        if (HR_CONTROLLER == h3wUtlGetRole(&cP->utlObj)) {
            cP->configFieldTransferred = TRUE;
        }

    }
    h3wErrTerm(&errObj);

    /* If the active event was not just triggered, 
     * then schedule the link timer to run.  
     */ 
    if (TE_NOT_TRIGGERED == h3wHandleActiveTrigger(cP)) {
        h3wScheduleLinkTimer(cP);
    }

}



/*---------------------------------------------------------------------------
 * h3wLinkActive()
 *     This function is invoked when the HCI 3-wire link goes active.  It 
 *     updates the state variables.  It signals the lower data frame layer 
 *     that the link has gone active.  It resets the sequence and 
 *     acknowledgement numbers.  It schedules an event to inform the upper 
 *     layers that the initialization sequence has completed successfully.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire coordination and control module context 
 *         structure.  
 */
static void h3wLinkActive(Hci3WStruct *cP)
{
    EventStruct     event;
    SLIP_STATE_ENUM currentSlipState;

    Assert(DS_LINK_INIT == cP->dataState);
    cP->linkMsgNeeded = FALSE;
    cP->dataState = DS_LINK_ACTIVE;

    /* Signal the lower data frame layer that the link has gone active.  */ 
    h3wDatSlipActivated(&cP->datObj, &currentSlipState);
    Assert(SS_ACTIVE == currentSlipState);

    h3wHdrResetSeqAck(&cP->hdrObj);

    /* Inform the invoking layer that the initialization 
     * link sequence has successfully completed.  
     */ 
    event.type = ET_LINK_ACTIVATED;
    event.data = 0;
    h3wEvtQueue(&cP->evtObj, event);

}



/*---------------------------------------------------------------------------
 * h3wHandleInitRX()
 *     This function is invoked to handle messages received while in the 
 *     initialized link state.  It responds to SYNC messages with a SYNC_RESP 
 *     message.  It responds to CONF messages with a CONF_RESP message.  For 
 *     CONF or CONF_RESP messages, it checks to see if the link has gone 
 *     active.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire coordination and control module context 
 *         structure.  
 *     errObjP - A pointer to an error context structure.  
 *
 * Returns:
 *     This function returns TRUE if it successfully handled the received 
 *     message, or FALSE otherwise.  
 */
static BOOL h3wHandleInitRX(Hci3WStruct *cP, Hci3WErrStruct *errObjP)
{
    BOOL        rxConfig;
    BOOL        rxConfigResp;
    EventStruct event;

    Assert((U8 *)0 != cP->msgRX.buf);

    /* Check for a SYNC message.  */ 
    if (  cP->linkMsgs[LM_SYNC].octets[0] 
       == cP->msgRX.buf[0]) {
        if (  cP->linkMsgs[LM_SYNC].octets[1] 
           == cP->msgRX.buf[1]) {
            if (2 == cP->msgRX.used) {
                h3wReportLinkMsgRX(LM_SYNC);
                h3wHandleAwake(cP);

                /* Send a SYNC RESPONSE message. */ 
                event.type = ET_LINK_TX_SYNC_RESP;
                event.data = 0;
                h3wEvtQueue(&cP->evtObj, event);

            }
        }

    } else {

        /* Check for a CONF message.  */ 
        if (  cP->linkMsgs[LM_CONF].octets[0] 
           == cP->msgRX.buf[0]) {
            if (  cP->linkMsgs[LM_CONF].octets[1] 
               == cP->msgRX.buf[1]) {
                rxConfig = FALSE;
                event.data = 0;
                if (HR_HOST == h3wUtlGetRole(&cP->utlObj)) {
                    if (2 == cP->msgRX.used) {
                        rxConfig = TRUE;
                    }
                } else {
                    if (3 <= cP->msgRX.used) {
                        if (0x07 & cP->msgRX.buf[2]) {

                            /* The sliding window 
                             * field is not zeroed out.  
                             */ 

                            event.data = cP->msgRX.buf[2];
                            rxConfig = TRUE;
                        }
                    }
                }
                if (TRUE == rxConfig) {
                    h3wReportLinkMsgRX(LM_CONF);
                    h3wHandleAwake(cP);

                    /* Send a CONFIG RESPONSE message. */ 
                    event.type = ET_LINK_INIT_TX_CONF_RESP;
                    h3wEvtQueue(&cP->evtObj, event);

                }
            }
        } else {

            /* Check for a CONF_RESP message.  */ 
            if (  cP->linkMsgs[LM_CONF_RESPONSE].octets[0] 
               == cP->msgRX.buf[0]) {
                if (  cP->linkMsgs[LM_CONF_RESPONSE].octets[1] 
                   == cP->msgRX.buf[1]) {
                    rxConfigResp = FALSE;
                    if (HR_HOST == h3wUtlGetRole(&cP->utlObj)) {
                        if (3 <= cP->msgRX.used) {
                            h3wHandleConfigField(cP, cP->msgRX.buf[2]);
                            rxConfigResp = TRUE;
                        }
                    } else {
                        if (2 == cP->msgRX.used) {
                            rxConfigResp = TRUE;
                        }
                    }
                    if (TRUE == rxConfigResp) {
                        h3wReportLinkMsgRX(LM_CONF_RESPONSE);
                        h3wHandleAwake(cP);
                        cP->configResponseReceived = TRUE;
                        h3wHandleActiveTrigger(cP);
                    }
                }
            }

        }
    }
    return(h3wErrHasNone(errObjP));
}



/*---------------------------------------------------------------------------
 * h3wSendConfigRespActive()
 *     This function is invoked when a CONFIG_RESP message needs to be sent 
 *     while in the link active state.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire coordination and control module context 
 *         structure.  
 */
static void h3wSendConfigRespActive(Hci3WStruct *cP)
{
    Hci3WErrStruct errObj;
    U8             handle;

    Assert(TRUE == cP->configFieldTransferred);
    cP->msgTX.buf[0] = cP->linkMsgs[LM_CONF_RESPONSE].octets[0];
    cP->msgTX.buf[1] = cP->linkMsgs[LM_CONF_RESPONSE].octets[1];
    if (HR_HOST == h3wUtlGetRole(&cP->utlObj)) {
        cP->msgTX.used = 2;
    } else {
        cP->msgTX.buf[2] = h3wUtlGetSlidingWindowSize(&cP->utlObj);
        if (  TRUE 
           == h3wUtlGetFlowControl(&cP->utlObj,
                                    FL_OUT_OF_FRAME_SOFTWARE, 
                                    FT_CHOKE_ABILITY)) {
            cP->msgTX.buf[2] |= 0x08;
        }
        if (TRUE == h3wUtlGetMayUseCRC(&cP->utlObj)) {
            cP->msgTX.buf[2] |= 0x0010;
        }
        cP->msgTX.used = 3;
    }
    h3wErrInit(&errObj);
    h3wSendE( cP, 
             &handle,
              FALSE, 
             &cP->msgTX, 
              PT_LINK, 
              FALSE, 
              FALSE, 
             &errObj);
    h3wErrReportUnsolicited(&errObj, FALSE, " CONFIG RESPONSE send");
    h3wErrTerm(&errObj);
}



/*---------------------------------------------------------------------------
 * h3wHandleLowPowerRX()
 *     This function is called to handle the reception of low power 
 *     messages.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire coordination and control module context 
 *         structure.  
 *
 * Returns:
 *     If the received message was a low power message, this function returns 
 *     TRUE.  Otherwise, it returns FALSE.  
 */
static BOOL h3wHandleLowPowerRX(Hci3WStruct *cP)
{
    BOOL        retVal;
    EventStruct event;

    retVal = FALSE;
    if (2 == cP->msgRX.used) {
        if (  cP->linkMsgs[LM_WAKEUP].octets[0] 
           == cP->msgRX.buf[0]) {
            if (  cP->linkMsgs[LM_WAKEUP].octets[1] 
               == cP->msgRX.buf[1]) {
                h3wReportLinkMsgRX(LM_WAKEUP);
                h3wHandleAwake(cP);
                retVal = TRUE;
            }
        } else {
            if (  cP->linkMsgs[LM_WOKEN].octets[0] 
               == cP->msgRX.buf[0]) {
                if (  cP->linkMsgs[LM_WOKEN].octets[1] 
                   == cP->msgRX.buf[1]) {
                    h3wReportLinkMsgRX(LM_WOKEN);
                    h3wHandleAwake(cP);
                    retVal = TRUE;
                }
            } else {
                if (  cP->linkMsgs[LM_SLEEP].octets[0] 
                   == cP->msgRX.buf[0]) {
                    if (  cP->linkMsgs[LM_SLEEP].octets[1] 
                       == cP->msgRX.buf[1]) {
                        h3wReportLinkMsgRX(LM_SLEEP);
                        if (  TRUE 
                           == h3wUtlGetOtherAssumedAwake(&cP->utlObj)) {
                            h3wUtlSetOtherAwake(&cP->utlObj, FALSE);
                            event.type = ET_LOW_POWER_SLEEP;
                            event.data = 0;
                            h3wEvtQueue(&cP->evtObj, event);
                        }
                        retVal = TRUE;
                    }
                }
            }
        }
    }
    return(retVal);
}



/*---------------------------------------------------------------------------
 * h3wHandleActiveRX()
 *     This function is called the handle the reception of a message while in 
 *     the link active state.  If a SYNC message is received, this function 
 *     schedules an event to perform a reset.  If a CONF message was received, 
 *     this message schedules an event to send a CONF_RESP message.  If a 
 *     CONF_RESP message was received, it discards it.  Otherwise, it puts 
 *     the message on a receive queue and schedules an event to signal the 
 *     upper layers that received data is available.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire coordination and control module context 
 *         structure.  
 *     errObjP - A pointer to an error context structure.  
 *
 * Returns:
 *     This function returns TRUE if no error was encountered while 
 *     processing the received data, or FALSE otherwise.  
 */
static BOOL h3wHandleActiveRX(Hci3WStruct    *cP, 
                              Hci3WErrStruct *errObjP)
{
    BOOL        eventWasHandled;
    BOOL        rxConfig;
    BOOL        rxConfigResp;
    EventStruct event;

    Assert((U8 *)0 != cP->msgRX.buf);
    Assert((U8 *)0 != cP->rawRX.buf);
    eventWasHandled = FALSE;
    if (  cP->linkMsgs[LM_SYNC].octets[0] 
       == cP->msgRX.buf[0]) {
        if (  cP->linkMsgs[LM_SYNC].octets[1] 
           == cP->msgRX.buf[1]) {
            if (2 == cP->msgRX.used) {
                h3wReportLinkMsgRX(LM_SYNC);
                h3wHandleAwake(cP);

                /* The connected device must have reset.  In 
                 * that case, this code should also be reset.  
                 */ 

                event.type = ET_LINK_RESET;
                event.data = 0;
                h3wEvtQueue(&cP->evtObj, event);
                eventWasHandled = TRUE;

            }
        }
    } else {
        if (  cP->linkMsgs[LM_CONF].octets[0] 
           == cP->msgRX.buf[0]) {
            if (  cP->linkMsgs[LM_CONF].octets[1] 
               == cP->msgRX.buf[1]) {
                rxConfig = FALSE;
                if (HR_HOST == h3wUtlGetRole(&cP->utlObj)) {
                    if (2 == cP->msgRX.used) {
                        rxConfig = TRUE;
                    }
                } else {
                    if (3 <= cP->msgRX.used) {
                        if (0x07 & cP->msgRX.buf[2]) {

                            /* The sliding window 
                             * field is not zeroed out.  
                             */ 

                            rxConfig = TRUE;
                        }
                    }
                }
                if (TRUE == rxConfig) {
                    h3wReportLinkMsgRX(LM_CONF);
                    h3wHandleAwake(cP);

                    /* Send a CONFIG RESPONSE message. */ 
                    event.type = ET_LINK_ACTIVE_TX_CONF_RESP;
                    event.data = 0;
                    h3wEvtQueue(&cP->evtObj, event);
                    eventWasHandled = TRUE;

                }
            }
        } else {
            if (  cP->linkMsgs[LM_CONF_RESPONSE].octets[0] 
               == cP->msgRX.buf[0]) {
                if (  cP->linkMsgs[LM_CONF_RESPONSE].octets[1] 
                   == cP->msgRX.buf[1]) {
                    rxConfigResp = FALSE;
                    if (HR_HOST == h3wUtlGetRole(&cP->utlObj)) {
                        if (3 <= cP->msgRX.used) {
                            rxConfigResp = TRUE;
                        }
                    } else {
                        if (2 == cP->msgRX.used) {
                            rxConfigResp = TRUE;
                        }
                    }
                    if (TRUE == rxConfigResp) {
                        h3wReportLinkMsgRX(LM_CONF_RESPONSE);
                        h3wHandleAwake(cP);

                        /* Discard Config Responses in the active state. */ 

                        eventWasHandled = TRUE;

                    }
                }
            } else {
                eventWasHandled = h3wHandleLowPowerRX(cP);
            }
        }
    }
    if (TRUE != eventWasHandled) {
        if (TRUE == h3wUtlGetOtherAssumedAwake(&cP->utlObj)) {
            h3wRXQPut(&cP->datRXQObj,
                      &cP->rawRX.buf[0], 
                       cP->rawRX.used, 
                       errObjP);
            event.type = ET_RX_APP;
            event.data = 0;
            h3wEvtQueue(&cP->evtObj, event);
        } else {
            h3wErrSet(errObjP, ES_H3W_HANDLEACTIVERX, ER_NOT_AWAKE);
        }
    }
    return(h3wErrHasNone(errObjP));
}



/*---------------------------------------------------------------------------
 * h3wLinkRX()
 *     This function receives data and routes it to one of three link 
 *     handlers:  an uninitialized handler, an initialized handler, or an 
 *     active handler.  If the raw receive buffer has not been initializted, 
 *     it allocates memory for it.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire coordination and control module context 
 *         structure.  
 *     operationStr - A character string describing the receive operation in 
 *         progress.  
 */
static void h3wLinkRX(Hci3WStruct *cP, char *operationStr)
{
    Hci3WErrStruct   errObj;
    PACKET_TYPE_ENUM packetType;
    U16              maxPayloadBytes;
    U16              rawSize;

    h3wErrInit(&errObj);

    /* If the raw receive buffer has not been 
     * initializted, allocate memory for it.  
     */ 
    if (0 >= cP->rawRX.maxSize) {
        maxPayloadBytes = h3wUtlGetMaxPayloadBytes(&cP->utlObj);
        rawSize = (U16)(sizeof(FrameHdrStruct) + maxPayloadBytes);
        if (  TRUE 
           == h3wMemGet(&cP->memObj, &cP->rawRX.buf, rawSize, &errObj)) {
            cP->rawRX.maxSize = rawSize;
            cP->rawRX.used    = 0;
            cP->hdrInfoP      = (FrameHdrStruct *)(cP->rawRX.buf);
            cP->msgRX.buf     = cP->rawRX.buf + sizeof(FrameHdrStruct);
            cP->msgRX.maxSize = maxPayloadBytes;
            cP->msgRX.used    = 0;
        }
    }

    if (TRUE == h3wErrHasNone(&errObj)) {

        /* Receive the data.  */ 
        if (TRUE == h3wReceiveE(cP, FALSE, &packetType, &errObj)) {

            if (0 < cP->msgRX.used) {

                /* Route the received data to one of three link 
                 * handlers:  the uninitialized handler, the 
                 * initialized handler, or the active handler.  
                 */ 
                switch(cP->dataState) {
                case DS_LINK_INIT:
                    h3wHandleInitRX(cP, &errObj);
                    break;

                case DS_LINK_ACTIVE:
                    h3wHandleActiveRX(cP, &errObj);
                    break;

                case DS_LINK_UNINIT:
                default:
                    h3wHandleUninitRX(cP);
                }

            }
        }
    }
    h3wErrReportUnsolicited(&errObj, TRUE, operationStr);
    h3wErrTerm(&errObj);
}



/*---------------------------------------------------------------------------
 * h3wSendLnk()
 *     This function sends out a link message appropriate for the HCI 3-wire 
 *     link state.  If uninitialized, it sends a SYNC message.  If 
 *     initialized, it sends a CONFIG message.  If active, it sends nothing.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire coordination and control module context 
 *         structure.  
 */
static void h3wSendLnk(Hci3WStruct *cP)
{
    Hci3WErrStruct errObj;
    U8             handle;

    switch(cP->dataState) {
    case DS_LINK_UNINIT:
        h3wErrInit(&errObj);
        h3wSendE( cP,
                 &handle,
                  FALSE, 
                 &cP->msgSync, 
                  PT_LINK, 
                  FALSE, 
                  FALSE, 
                 &errObj);
        h3wErrReportUnsolicited(&errObj, FALSE, " send SYNC");
        h3wErrTerm(&errObj);
        break;

    case DS_LINK_INIT:
        h3wErrInit(&errObj);
        h3wSendE( cP,
                 &handle,
                  FALSE, 
                 &cP->msgConfig, 
                  PT_LINK, 
                  FALSE, 
                  FALSE, 
                 &errObj);

        /* Don't set configFieldTransferred here, because we only care 
         * about the configuration field in the CONFIG RESPONSE message.  
         */ 

        h3wErrReportUnsolicited(&errObj, FALSE, " send CONFIG");
        h3wErrTerm(&errObj);
        break;

    default:
        break;
    }
}



/*---------------------------------------------------------------------------
 * h3wReceiveE()
 *     This function receives and processes data.  If the check receive queue 
 *     parameter is asserted, and if the receive queue has data from previous 
 *     receptions, that is used as the source of the data.  Otherwise, data 
 *     is obtained from the data frame receive routine.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire coordination and control module context 
 *         structure.  
 *     checkRXQ - This parameter must be set to TRUE if the function should 
 *         check in the receive queue for data, or FALSE otherwise.  
 *     packetTypeP - This parameter must point to a location where an 
 *         enumeration corresponding to the HCI 3-wire packet type can be 
 *         written.  
 *     errObjP - A pointer to an error context structure.  
 *
 * Returns:
 *     This function returns TRUE if it was successfully able to obtain and 
 *     process some receive data, or FALSE otherwise.  
 */
static BOOL h3wReceiveE(Hci3WStruct      *cP, 
                        BOOL              checkRXQ, 
                        PACKET_TYPE_ENUM *packetTypeP,
                        Hci3WErrStruct   *errObjP)
{
    switch(cP->dataState) {
    case DS_LINK_UNINIT:
    case DS_LINK_INIT:
    case DS_LINK_ACTIVE:
        Assert((U8             *)0 != cP->rawRX.buf);
        Assert((FrameHdrStruct *)0 != cP->hdrInfoP );
        Assert((U8             *)0 != cP->msgRX.buf);
        if (TRUE == checkRXQ) {
            if (TRUE == h3wRXQIsEmpty(&cP->datRXQObj)) {
                checkRXQ = FALSE;
            }
        }
        if (TRUE == checkRXQ) {

            /* See if there is any receive data in the 
             * receive queue that can be processed.  
             */ 
            if (TRUE == h3wRXQGet(&cP->datRXQObj, &cP->rawRX, errObjP)) {
                Assert(sizeof(FrameHdrStruct) < cP->rawRX.used);
                cP->msgRX.used 
                    = (U16)( cP->rawRX.used - sizeof(FrameHdrStruct));
                *packetTypeP = cP->hdrInfoP->packetType;
            }

        } else {

            /* Invoke the data frame receive routine 
             * to obtain some newly received data.  
             */ 
            if (TRUE == h3wDatReceive(&cP->datObj,
                                      &cP->msgRX, 
                                       cP->hdrInfoP, 
                                       errObjP)) {
                cP->rawRX.used 
                    = (U16)(cP->msgRX.used + sizeof(FrameHdrStruct));
                *packetTypeP = cP->hdrInfoP->packetType;
                h3wUtlSetLastRXAck(&cP->utlObj, TRUE, cP->hdrInfoP->ackNum);
            }

        }
        if (TRUE == h3wErrHasNone(errObjP)) {

            /* Don't ignore zero length messages.  They should 
             * be processed just like any other message, because 
             * zero length messages (such as pure ACKs) have 
             * information in their headers that should be handled.  
             */ 

            h3wProcessRX(cP, cP->hdrInfoP, errObjP);
        }
        break;

    default:
        h3wErrSet(errObjP, ES_H3W_RECEIVE_E, ER_NOT_INIT);
    }
    return(h3wErrHasNone(errObjP));
}



/*---------------------------------------------------------------------------
 * h3wProcessRX()
 *     For reliable messages, this function increments the number of reliably 
 *     received messages that have not yet been acknowledged.  It schedules 
 *     an event to resend messages that are being reliably transmitted.  If 
 *     the received data needs acknowledgement, and if the sliding window is 
 *     full, it schedules an event to generate a pure acknowledgement message.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire coordination and control module context 
 *         structure.  
 *     hdrNewP - This parameter must point to a frame header structure filled 
 *         in with information about the data frame header.  
 *     errObjP - A pointer to an error context structure.  
 *
 * Returns:
 *     If MemCopy actually had a return I'd list it here. Otherwise this 
 *     section would not exist. Return values and descriptions would be
 *     listed much like the "Parameters" section.
 */
static BOOL h3wProcessRX(Hci3WStruct    *cP, 
                         FrameHdrStruct *hdrNewP, 
                         Hci3WErrStruct *errObjP)
{
    EventStruct event;

    /* If this is a reliable message, increment the number of reliably 
     * received messages that have not yet been acknowledged.  
     */ 
    if (TRUE == hdrNewP->sentReliably) {
        h3wHdrIncrNumRXNeedingACK(&cP->hdrObj);
    }

    /* Schedule an event to resend messages 
     * that are being reliably transmitted.  
     */ 
    event.type = ET_TX_RESEND_RELIABLE;
    event.data = 0;
    h3wEvtQueue(&cP->evtObj, event);


    /* If the received data needs acknowledgement, and 
     * if the sliding window is full, schedule an event 
     * to generate a pure acknowledgement message.  
     */ 
    if (TRUE == hdrNewP->sentReliably) {
        if (TRUE == h3wHdrPureACKWindowFull(&cP->hdrObj)) {
            event.type = ET_TX_PURE_ACK;
            event.data = 0;
            h3wEvtQueue(&cP->evtObj, event);
        }
    }

    return(h3wErrHasNone(errObjP));
}



/*---------------------------------------------------------------------------
 * h3wSendE()
 *     This function sends a message to the HCI 3-wire data frame layer, and 
 *     then processes any events that arrived during the send.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire coordination and control module context 
 *         structure.  
 *     handleP - This parameter must point to a location where an HCI 3-wire 
 *         reliable transmission handle can be written.  
 *     isLowPowerMsg - This parameter must be set to TRUE if it is a low 
 *         power message, or FALSE otherwise.  
 *     dataBuffP - This parameter must point to a data buffer structure 
 *         containing the data to be transmitted.  
 *     packetType - This parameter must contain an enumeration corresponding 
 *         to the HCI 3-wire packet type.  
 *     sendReliably - This parameter must be TRUE if the data is to be 
 *         transmitted reliably, or FALSE otherwise.  
 *     includeCRC - This parameter must be set to TRUE if a cyclic redundancy 
 *         check is to be appended to the end of the payload, or FALSE 
 *         otherwise.  
 *     errObjP - A pointer to an error context structure.  
 *
 * Returns:
 *     This function returns TRUE if it was successfully able to send the 
 *     data, or FALSE otherwise.  
 */
static BOOL h3wSendE(Hci3WStruct     *cP,
                     U8              *handleP,
                     BOOL             isLowPowerMsg,
                     BuffUseStruct   *dataBuffP,
                     PACKET_TYPE_ENUM packetType,
                     BOOL             sendReliably, 
                     BOOL             includeCRC,
                     Hci3WErrStruct  *errObjP)
{
    BOOL semaphore;

    *handleP = '\0';
    Assert(  (DS_LINK_UNINIT == cP->dataState) 
          || (DS_LINK_INIT   == cP->dataState) 
          || (DS_LINK_ACTIVE == cP->dataState));
    semaphore = h3wEvtGetSemaphore(&cP->evtObj);
    h3wDatSend(&cP->datObj, 
                handleP,
                isLowPowerMsg,
                dataBuffP, 
                packetType, 
                sendReliably, 
                includeCRC, 
                errObjP);

    /* If this routine owns the semaphore, it must call 
     * the event process routine.  The event process routine 
     * processes events that arrived during the time that 
     * the semaphore was held, and then releases the semaphore.
     */ 
    if (TRUE == semaphore) {
        h3wEvtProcess(&cP->evtObj);
    }

    return(h3wErrHasNone(errObjP));
}



/*---------------------------------------------------------------------------
 * h3wInitiateLink()
 *     This function should be called after the UART is initialized to 
 *     initiate the HCI 3-wire link process.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire coordination and control module context 
 *         structure.  
 */
static void h3wInitiateLink(Hci3WStruct *cP)
{
    Assert(  (DS_UNINIT       == cP->dataState) 
          || (DS_PARTIAL_INIT == cP->dataState) 
          || (DS_LINK_ACTIVE  == cP->dataState));
    cP->dataState = DS_LINK_UNINIT;
    Report(("HCI3W:  HCI 3-Wire Link Set to the Uninitialized State.\n"));
    if (TRUE != cP->delayUntilSYNCRX) {
        h3wSendNow(cP);
    }
}



/*---------------------------------------------------------------------------
 * h3wGetConfigE()
 *     This function obtains the configuration information that was 
 *     negotiated during the HCI 3-wire link process.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire coordination and control module context 
 *         structure.  
 *     configOptionsP - This parameter must point to a location where a 
 *         configuration options structure can be written.  
 *     errObjP - A pointer to an error context structure.  
 *
 * Returns:
 *     If MemCopy actually had a return I'd list it here. Otherwise this 
 *     section would not exist. Return values and descriptions would be
 *     listed much like the "Parameters" section.
 */
static BOOL h3wGetConfigE(Hci3WStruct         *cP,
                          ConfigOptionsStruct *configOptionsP,
                          Hci3WErrStruct      *errObjP)
{
    if (TRUE == cP->activeTriggered) {
        configOptionsP->crc = h3wUtlGetMayUseCRC(&cP->utlObj);
        switch(h3wUtlGetSlidingWindowSize(&cP->utlObj)) {
        case 1:
            configOptionsP->slidingWindow = SW_LOWEST;
            break;

        case 2:
            configOptionsP->slidingWindow = SW_LOWER;
            break;

        case 4:
            configOptionsP->slidingWindow = SW_MID;
            break;

        case 5:
            configOptionsP->slidingWindow = SW_HIGH;
            break;

        case 6:
            configOptionsP->slidingWindow = SW_HIGHER;
            break;

        case 7:
            configOptionsP->slidingWindow = SW_HIGHEST;
            break;

        case 3:
        default:
            configOptionsP->slidingWindow = SW_LOW;
        }
    } else {
        h3wErrSet(errObjP, ES_H3W_GETCONFIGE, ER_NOT_INIT);
    }
    return(h3wErrHasNone(errObjP));
}



/*---------------------------------------------------------------------------
 * h3wSendWoken()
 *     This function transmits a WOKEN message.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire coordination and control module context 
 *         structure.  
 */
static void h3wSendWoken(Hci3WStruct *cP)
{
    Hci3WErrStruct errObj;
    U8             handle;

    cP->msgTX.buf[0] = cP->linkMsgs[LM_WOKEN].octets[0];
    cP->msgTX.buf[1] = cP->linkMsgs[LM_WOKEN].octets[1];
    cP->msgTX.used = 2;
    h3wErrInit(&errObj);
    h3wSendE( cP, 
             &handle,
              TRUE, 
             &cP->msgTX, 
              PT_LINK, 
              FALSE, 
              FALSE, 
             &errObj);
    h3wErrReportUnsolicited(&errObj, FALSE, " WOKEN send");
    h3wErrTerm(&errObj);
}



/*---------------------------------------------------------------------------
 * h3wSendWakeup()
 *     This function transmits a WAKEUP message, and starts the timer that 
 *     periodically continues sending WAKEUP messages until a WOKEN message 
 *     is received.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire coordination and control module context 
 *         structure.  
 */
static void h3wSendWakeup(Hci3WStruct *cP)
{
    Hci3WErrStruct errObj;
    U8             handle;

    cP->waitingForWoken = TRUE;
    cP->msgTX.buf[0] = cP->linkMsgs[LM_WAKEUP].octets[0];
    cP->msgTX.buf[1] = cP->linkMsgs[LM_WAKEUP].octets[1];
    cP->msgTX.used = 2;
    h3wErrInit(&errObj);
    h3wSendE( cP, 
             &handle, 
              TRUE, 
             &cP->msgTX, 
              PT_LINK, 
              FALSE, 
              FALSE, 
             &errObj);
    h3wErrReportUnsolicited(&errObj, FALSE, " WAKEUP send");
    h3wErrTerm(&errObj);
    EVM_StartTimer(&cP->timer, h3wUtlGetTimeOneChar(&cP->utlObj));
}
