/****************************************************************************
 *
 * File:
 *     $Workfile:HCI3WTXQ.c$ for iAnywhere Blue SDK, Version 2.2
 *     $Revision:13$
 *
 * Description: This file is part of an HCI 3-Wire implementation.  It 
 * contains code dealing with messages stored and retrieved from a transmit 
 * queue, including reliable message retransmissions.  
 *
 * Created:     October 31, 2006
 *
 * Copyright 2006-2007 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions,  
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "sys/HCI3WTXQ.h"

/****************************************************************************
 *
 * Prototypes
 *
 ****************************************************************************/

static BOOL h3wTXQCreateNewNode(Hci3WTxQStruct   *cP,
                                TxQNodeHdrStruct *txqNodeP,
                                U8               *h3wBuff, 
                                U16               size,
                                Hci3WErrStruct   *errObjP);

static BOOL h3wTXQFreeNode(Hci3WTxQStruct   *cP,
                           TxQNodeHdrStruct *currNodeHdrP, 
                           Hci3WErrStruct   *errObjP);

static void h3wTXQCopyNodeHdr(TxQNodeHdrStruct *dstNodeHdrP, 
                              TxQNodeHdrStruct *srcNodeHdrP);

static void h3wTXQEmptyNodeHdr(TxQNodeHdrStruct *nodeHdrP);

static void h3wTXQHandleTimerEvent(Hci3WTxQStruct *cP);

static void h3wTXQResend(Hci3WTxQStruct *cP, 
                         FrameHdrStruct *hdrOldp, 
                         U8             *h3wBuff);

/****************************************************************************
 *
 * Public Functions
 *
 ****************************************************************************/



/*---------------------------------------------------------------------------
 * h3wTXQTimerEventCallback()
 *     This function is invoked when the transmit queue timer fires.  If it 
 *     is given a valid context, it invokes the transmit queue timer event 
 *     code.  
 * 
 * Parameters:
 *     evmTimerP - The event manager structure corresponding to the timer that 
 *         just fired.  
 */
void h3wTXQTimerEventCallback(struct _EvmTimer *evmTimerP)
{
    if ((void *)0 != evmTimerP->context) {
        h3wTXQHandleTimerEvent((Hci3WTxQStruct *)evmTimerP->context);
    }
}



/*---------------------------------------------------------------------------
 * h3wTXQInit()
 *     Initializes the HCI 3-wire transmit queue module.  This function must 
 *     be invoked before any of the other HCI 3-wire transmit queue functions 
 *     will work correctly.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire transmit queue module context structure.  
 *     crcObjP - Pointer to an HCI 3-wire cyclic redundancy check module 
 *         context structure.  
 *     evtObjP - Pointer to an HCI 3-wire event handling module context 
 *         structure.  
 *     hdrObjP - Pointer to an HCI 3-wire frame header module context 
 *         structure.  
 *     memObjP - Pointer to an HCI 3-wire memory module context structure.  
 *     slpObjP - Pointer to an HCI 3-wire slip module context structure.  
 *     utlObjP - Pointer to an HCI 3-wire utilities module context structure.  
 */
void h3wTXQInit(Hci3WTxQStruct *cP,
                Hci3WCrcStruct *crcObjP,
                Hci3WEvtStruct *evtObjP, 
                Hci3WHdrStruct *hdrObjP, 
                Hci3WMemStruct *memObjP, 
                Hci3WSlpStruct *slpObjP,
                Hci3WUtlStruct *utlObjP)
{
    cP->privCRCObjP     = crcObjP;
    cP->privEvtObjP     = evtObjP;
    cP->privHdrObjP     = hdrObjP;
    cP->privMemObjP     = memObjP;
    cP->privSlpObjP     = slpObjP;
    cP->privUtlObjP     = utlObjP;
    h3wTXQEmptyNodeHdr(&cP->root);
    InitializeListEntry(&cP->timer.node);
    cP->timer.context   = (void *)cP;
    cP->timer.func      = h3wTXQTimerEventCallback;
    cP->timer.time      = (TimeT)0;
    cP->timer.startTime = (TimeT)0;
    cP->handleCntr      = 1;
}



/*---------------------------------------------------------------------------
 * h3wTXQPush()
 *     Pushes an message onto the transmit queue.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire transmit queue module context structure.  
 *     handleP - a pointer to a location where a transmit queue handle can be 
 *         written.  
 *     h3wBuff - a pointer to the message buffer that is to be pushed on the 
 *         queue.  
 *     size - the size of the given message buffer.  
 *     errObjP - A pointer to an error context structure.
 *
 * Returns:
 *     This function returns TRUE if it was successfully able to push the 
 *     transmit message onto the transmit queue, or FALSE otherwise.  
 */
BOOL h3wTXQPush(Hci3WTxQStruct *cP,
                U8             *handleP, 
                U8             *h3wBuff, 
                U16             size, 
                Hci3WErrStruct *errObjP)
{
    BOOL             findingEndOfList;
    TxQNodeHdrStruct currNext;
    U8              *currBuff;

    if (TRUE == cP->root.ptrIsValid) {
        currBuff = cP->root.ptr;
        do {
            findingEndOfList = FALSE;

            /* Copy the next information from the current 
             * buffer into the currNext structure.  
             */ 
            cP->bufPtrs.src = currBuff;
            cP->bufPtrs.dst = (U8 *)(&currNext);
            h3wUtlCopyBytes(&cP->bufPtrs, sizeof(TxQNodeHdrStruct));

            if (TRUE == currNext.ptrIsValid) {

                /* We are not at the end of the linked 
                 * list yet.  Go to the next node.  
                 */ 
                currBuff = currNext.ptr;
                findingEndOfList = TRUE;

            } else {
    
                /* The end of the linked list has 
                 * been found.  Create a new node.
                 */ 
                if (  TRUE 
                   == h3wTXQCreateNewNode( cP,
                                          &currNext, 
                                           h3wBuff, 
                                           size, 
                                           errObjP)) {

                    *handleP = currNext.handle;

                    /* Now point the node that used to be the last 
                     * node in the linked list to the new node.
                     */ 
                    cP->bufPtrs.src = (U8 *)(&currNext);
                    cP->bufPtrs.dst = currBuff;
                    h3wUtlCopyBytes(&cP->bufPtrs, 
                                     sizeof(TxQNodeHdrStruct));

                }

            }
        } while (TRUE == findingEndOfList);
    } else {

        /* Create a new node */ 
        if (  TRUE 
           == h3wTXQCreateNewNode(cP, &cP->root, h3wBuff, size, errObjP)) {
            *handleP = cP->root.handle;
            EVM_StartTimer(&cP->timer, 3 * h3wUtlGetTmax(cP->privUtlObjP));
        }

    }
    return(h3wErrHasNone(errObjP));
}



/*---------------------------------------------------------------------------
 * h3wTXQResendReliable()
 *     This function loops through the transmit queue, freeing messages that 
 *     have been acknowledged, and retransmitting messages that have not.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire transmit queue module context structure.  
 *     ack - An acknowledgement structure that must contain the most recently 
 *         received acknowledgement information.  
 *     h3CallBack - This must point at the callback routine that is to be 
 *         invoked anytime a message has been successfully acknowledged and 
 *         removed from the transmit queue.  
 *     errObjP - A pointer to an error context structure.
 *
 * Returns:
 *     This function returns TRUE if it was successfully able to resend or 
 *     free the nodes on the transmit queue, or FALSE otherwise.  
 */
BOOL h3wTXQResendReliable(Hci3WTxQStruct *cP, 
                          AckStruct       ack, 
                          void          (*h3CallBack)(H3W_EVENT_ENUM, U8),
                          Hci3WErrStruct *errObjP)
{
    BOOL             doRetransmit;
    BOOL             freeingAckedMsgs;
    FrameHdrStruct   hdrOld;
    TxQNodeHdrStruct currNodeHdr;
    TxQNodeHdrStruct nextNodeHdr;
    TxQNodeHdrStruct prevNodeHdr;
    U8               doneHandle;
    U8   *h3wBuff;

    if (TRUE == cP->root.ptrIsValid) {
        h3wTXQEmptyNodeHdr(&prevNodeHdr);
        h3wTXQCopyNodeHdr(&currNodeHdr, &cP->root);
        do {
            freeingAckedMsgs = FALSE;

            /* Get the next node header information */ 
            cP->bufPtrs.src = currNodeHdr.ptr;
            cP->bufPtrs.dst = (U8 *)(&nextNodeHdr);
            h3wUtlCopyBytes(&cP->bufPtrs, sizeof(TxQNodeHdrStruct));

            h3wBuff = currNodeHdr.ptr + sizeof(TxQNodeHdrStruct);
            h3wHdrGet(cP->privHdrObjP, h3wBuff, &hdrOld, FALSE, errObjP);

            /* Since only valid headers will be placed in the reliable 
             * TX queue, the headers must never contain an error. 
             */ 
            Assert(h3wErrHasNone(errObjP));

            doRetransmit = TRUE;

            /* We must always be freeing the first node in the list. */ 
            if (TRUE != prevNodeHdr.ptrIsValid) {
                if (TRUE == ack.valid) {
                    if (  TRUE 
                       == h3wHdrInSlidingWin( cP->privHdrObjP, 
                                             &hdrOld, 
                                              ack.ackNum)) {
                        doneHandle = currNodeHdr.handle;
                        h3wTXQFreeNode(cP, &currNodeHdr, errObjP);

                        /* The upper invoking HCI layer will assert if you 
                         * send it received event data, and then indicate 
                         * that the command corresponding to the event has 
                         * been successfully sent and its buffer can be 
                         * released.  Instead, the upper invoking HCI 
                         * layer expects to first be informed that a 
                         * command has been successfully sent and its 
                         * buffer can be released, and only then be signaled 
                         * that the corresponding event data has been 
                         * received.  To comply with this expectation, the 
                         * resendReliable() function signals the upper 
                         * invoking layer that data has been successfully 
                         * sent immediately upon detecting that condition, 
                         * rather than setting things up so that this 
                         * event will be signaled at a later time.  
                         */ 
                        h3CallBack(H3_RELIABLY_SENT, doneHandle);

                        doRetransmit = FALSE;
                    }
                }
            }

            if (TRUE == doRetransmit) {

                /* This reliable message has been NAKed, so it needs 
                 * to be retransmitted if Tmax * 3 time has elapsed. 
                 */
                if (  MS_TO_TICKS(3 * h3wUtlGetTmax(cP->privUtlObjP)) 
                   <= h3wUtlElapsed(cP->privUtlObjP, 
                                    currNodeHdr.enqueuedAtTicks)) {
                    h3wTXQResend(cP, &hdrOld, h3wBuff);
                }

                /* Copy the current node header into 
                 * the previous node header in preparation 
                 * for the next loop iteration.
                 */ 
                h3wTXQCopyNodeHdr(&prevNodeHdr, &currNodeHdr);

            }
            if (TRUE == h3wErrHasNone(errObjP)) {
                h3wTXQCopyNodeHdr(&currNodeHdr, &nextNodeHdr);
                if (TRUE == currNodeHdr.ptrIsValid) {
                    freeingAckedMsgs = TRUE;
                }
            }
        } while (TRUE == freeingAckedMsgs);
        if (TRUE != cP->root.ptrIsValid) {
            EVM_CancelTimer(&cP->timer);
        }
    }
    return(h3wErrHasNone(errObjP));
}



/*---------------------------------------------------------------------------
 * h3wTXQFree()
 *     Frees up the resources associated with the transmit queue.  Any 
 *     messages on the queue are dropped.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire transmit queue module context structure.  
 *     errObjP - A pointer to an error context structure.
 */
void h3wTXQFree(Hci3WTxQStruct *cP, Hci3WErrStruct *errObjP)
{
    BOOL             freeingAllocatedMemory;
    TxQNodeHdrStruct currNodeHdr;
    TxQNodeHdrStruct nextNodeHdr;
    TxQNodeHdrStruct prevNodeHdr;

    if (TRUE == cP->root.ptrIsValid) {
        EVM_CancelTimer(&cP->timer); 
        h3wTXQEmptyNodeHdr(&prevNodeHdr);
        h3wTXQCopyNodeHdr(&currNodeHdr, &cP->root);
        do {
            freeingAllocatedMemory = FALSE;

            /* Get the next node header information */ 
            cP->bufPtrs.src = currNodeHdr.ptr;
            cP->bufPtrs.dst = (U8 *)(&nextNodeHdr);
            h3wUtlCopyBytes(&cP->bufPtrs, sizeof(TxQNodeHdrStruct));

            /* We must always be freeing the first node in the list. */ 
            Assert(TRUE != prevNodeHdr.ptrIsValid);

            if (TRUE == h3wTXQFreeNode(cP, &currNodeHdr, errObjP)) {
                h3wTXQCopyNodeHdr(&currNodeHdr, &nextNodeHdr);
                if (TRUE == currNodeHdr.ptrIsValid) {
                    freeingAllocatedMemory = TRUE;
                }
            }
        } while (TRUE == freeingAllocatedMemory);
    }
}



/*---------------------------------------------------------------------------
 * h3wTXQNumNodesInQ()
 *     Determines how many messages are currently in the transmit queue.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire transmit queue module context structure.  
 *
 * Returns:
 *     This function returns the number of messages in the transmit queue.  
 */
U8 h3wTXQNumNodesInQ(Hci3WTxQStruct *cP)
{
    BOOL             findingEndOfList;
    TxQNodeHdrStruct currNext;
    U8              *currBuff;
    U8               retVal;

    retVal = 0;
    if (TRUE == cP->root.ptrIsValid) {
        retVal += 1;
        currBuff = cP->root.ptr;
        do {
            findingEndOfList = FALSE;

            /* Copy the next information from the current 
             * buffer into the currNext structure.  
             */ 
            cP->bufPtrs.src = currBuff;
            cP->bufPtrs.dst = (U8 *)(&currNext);
            h3wUtlCopyBytes(&cP->bufPtrs, sizeof(TxQNodeHdrStruct));

            if (TRUE == currNext.ptrIsValid) {

                /* We are not at the end of the linked 
                 * list yet.  Go to the next node.  
                 */ 
                currBuff = currNext.ptr;
                findingEndOfList = TRUE;

                retVal += 1;
            }
        } while (TRUE == findingEndOfList);
    }
    return(retVal);
}



/*---------------------------------------------------------------------------
 * h3wTXQTerm()
 *     This function destroys a given transmit queue module, cleaning up any 
 *     allocated resources.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire transmit queue module context structure.  
 */
void h3wTXQTerm(Hci3WTxQStruct *cP)
{
    cP->timer.context = (void *)0;
}



/****************************************************************************
 *
 * Private Functions
 *
 ****************************************************************************/



/*---------------------------------------------------------------------------
 * h3wTXQHandleTimerEvent()
 *     Handles the timer event for the transmit queue when it fires.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire transmit queue module context structure.  
 */
static void h3wTXQHandleTimerEvent(Hci3WTxQStruct *cP) 
{
    EventStruct event;

    event.type = ET_TX_RESEND_RELIABLE;
    event.data = 0;
    h3wEvtQueue(cP->privEvtObjP, event);

    /* Reschedule the timer if there are still 
     * reliable messages in the retransmit queue.  
     */ 
    if (TRUE == cP->root.ptrIsValid) {
        EVM_StartTimer(&cP->timer, 3 * h3wUtlGetTmax(cP->privUtlObjP));
    }

}



/*---------------------------------------------------------------------------
 * h3wTXQEmptyNodeHdr()
 *     Sets a transmit queue header to an initial empty state.  
 * 
 * Parameters:
 *     nodeHdrP - This parameter must point to a transmit queue node header 
 *         structure.  
 */
static void h3wTXQEmptyNodeHdr(TxQNodeHdrStruct *nodeHdrP)
{
    nodeHdrP->ptrIsValid      = FALSE;
    nodeHdrP->enqueuedAtTicks = 0;
    nodeHdrP->handle          = 0;
    nodeHdrP->ptr             = (U8 *)0;
}



/*---------------------------------------------------------------------------
 * h3wTXQCopyNodeHdr()
 *     This function copies one transmit queue node header structure into 
 *     another.  
 * 
 * Parameters:
 *     dstNodeHdrP - This must point to the destination transmit queue node 
 *         header structure.  
 *     srcNodeHdrP - This must point to the source transmit queue node header 
 *         structure.  
 */
static void h3wTXQCopyNodeHdr(TxQNodeHdrStruct *dstNodeHdrP, 
                              TxQNodeHdrStruct *srcNodeHdrP)
{
    dstNodeHdrP->ptrIsValid      = srcNodeHdrP->ptrIsValid;
    dstNodeHdrP->enqueuedAtTicks = srcNodeHdrP->enqueuedAtTicks;
    dstNodeHdrP->ptr             = srcNodeHdrP->ptr;
    dstNodeHdrP->handle          = srcNodeHdrP->handle;
}



/*---------------------------------------------------------------------------
 * h3wTXQCreateNewNode()
 *     Creates a new node that can be added into the transmit queue.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire transmit queue module context structure.  
 *     txqNodeP - This parameter must point to a location where a new transmit 
 *         queue node header structure can be written.  
 *     h3wBuff - This must point to a message that is to be associated with 
 *         the new transmit node.  
 *     size - This must contain the size of the given message buffer.  
 *     errObjP - A pointer to an error context structure.
 *
 * Returns:
 *     This function returns TRUE if it is successfully able to create a new 
 *     transmit queue node, or FALSE otherwise.  
 */
static BOOL h3wTXQCreateNewNode(Hci3WTxQStruct   *cP,
                                TxQNodeHdrStruct *txqNodeP,
                                U8               *h3wBuff, 
                                U16               size,
                                Hci3WErrStruct   *errObjP)
{
    TxQNodeHdrStruct newNext;

    h3wTXQEmptyNodeHdr(txqNodeP);
    if (  TRUE 
       == h3wMemGet( cP->privMemObjP, 
                    &txqNodeP->ptr, 
                     size + sizeof(TxQNodeHdrStruct),
                     errObjP)) {

        /* Set up the new node so that it 
         * does not point to a next node. 
         */ 
        h3wTXQEmptyNodeHdr(&newNext);
        cP->bufPtrs.src = (U8 *)(&newNext);
        cP->bufPtrs.dst = txqNodeP->ptr;
        h3wUtlCopyBytes(&cP->bufPtrs, sizeof(TxQNodeHdrStruct));

        /* Copy the passed in buffer data into the new node. */ 
        cP->bufPtrs.src = h3wBuff;
        h3wUtlCopyBytes(&cP->bufPtrs, size);

        /* Set the handle */ 
        txqNodeP->handle = cP->handleCntr;
        if (0x0ff > cP->handleCntr) {
            cP->handleCntr += 1;
        } else {
            cP->handleCntr = 1;
        }

        /* Indicate that a valid node has been obtained. */ 
        txqNodeP->ptrIsValid = TRUE;

        txqNodeP->enqueuedAtTicks = OS_GetSystemTime();
    }
    return(h3wErrHasNone(errObjP));
}



/*---------------------------------------------------------------------------
 * h3wTXQFreeNode()
 *     Frees up a transmit queue node.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire transmit queue module context structure.  
 *     currNodeHdrP - This parameter must point at a valid transmit queue node 
 *         that is to be freed up.  
 *     errObjP - A pointer to an error context structure.
 *
 * Returns:
 *     This function returns TRUE if it is successfully able to free up the 
 *     given transmit queue node, or FALSE otherwise.  
 */
static BOOL h3wTXQFreeNode(Hci3WTxQStruct   *cP,
                           TxQNodeHdrStruct *currNodeHdrP, 
                           Hci3WErrStruct   *errObjP)
{
    TxQNodeHdrStruct nextNodeHdr;

    /* Get the next node */ 
    cP->bufPtrs.src = currNodeHdrP->ptr;
    cP->bufPtrs.dst = (U8 *)(&nextNodeHdr);
    h3wUtlCopyBytes(&cP->bufPtrs, sizeof(TxQNodeHdrStruct));

    /* Copy the next node into the root */ 
    h3wTXQCopyNodeHdr(&cP->root, &nextNodeHdr);

    if (TRUE == h3wMemFree(cP->privMemObjP, currNodeHdrP->ptr, errObjP)) {
        h3wTXQEmptyNodeHdr(currNodeHdrP);
    }
    return(h3wErrHasNone(errObjP));
}



/*---------------------------------------------------------------------------
 * h3wTXQResend()
 *     This function updates a messages acknowledgement number and CRC value, 
 *     and then resends it.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire transmit queue module context structure.  
 *     hdrOldP - This parameter must point to a frame header structure filled 
 *         in with data corresponding to the message to be retransmitted.  
 *         The acknowledgement number in this structure will be updated.  
 *     h3wBuff - This parameter must point to a buffer containing the HCI 
 *         3-wire framed message that is to be retransmitted.  The CRC in this 
 *         buffer (if present) will be updated.
 */
static void h3wTXQResend(Hci3WTxQStruct *cP, 
                         FrameHdrStruct *hdrOldp, 
                         U8             *h3wBuff)
{
    Hci3WErrStruct errObj;
    U16            crc16CCIT;
    U16            h3wSize;

    /* Update the ACK number */ 
    h3wHdrUpdateACK(cP->privHdrObjP, hdrOldp, h3wBuff);
    h3wSize = (U16)(4 + hdrOldp->payldLen);
    if (TRUE == hdrOldp->includesCRC) {

        /* Recalculate the CRC */ 
        crc16CCIT = h3wCRCCalc(cP->privCRCObjP, h3wBuff, (U16)h3wSize);
        *(h3wBuff + h3wSize) = (U8)((crc16CCIT & 0xFF00) >> 8);
        h3wSize += 1;
        *(h3wBuff + h3wSize) = (U8)(crc16CCIT & 0xFF);
        h3wSize += 1;
    }

    if (  TRUE
       == h3wUtlGetFlowControl(cP->privUtlObjP,
                               FL_OUT_OF_FRAME_SOFTWARE,
                               FT_CURRENT_STATE)) {

        /* Retransmit the message */ 
        h3wErrInit(&errObj);
        h3wSlpResend(cP->privSlpObjP, h3wBuff, h3wSize, &errObj);
        h3wErrReportUnsolicited(&errObj, TRUE, " Reliable Resend");
        h3wErrTerm(&errObj);

    }
}

