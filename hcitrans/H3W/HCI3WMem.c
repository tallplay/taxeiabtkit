/****************************************************************************
 *
 * File:
 *     $Workfile:HCI3WMem.c$ for iAnywhere Blue SDK, Version 2.2
 *     $Revision:8$
 *
 * Description: This file is part of an HCI 3-Wire implementation.  It 
 * coordinates the use of the memory pool.  
 *
 * Created:     October 31, 2006
 *
 * Copyright 2006-2007 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions,  
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "osapi.h"
#include "sys/HCI3WMem.h"

/****************************************************************************
 *
 * Prototypes
 *
 ****************************************************************************/

static void h3wMemGetHdr(Hci3WMemStruct *cP, 
                         BuffHdrStruct  *bhS, 
                         U32             srcNdx);

static void h3wMemSetHdr(Hci3WMemStruct *cP,
                         BuffHdrStruct  *bhS, 
                         U32             dstNdx);

/****************************************************************************
 *
 * Public Functions
 *
 ****************************************************************************/



/*---------------------------------------------------------------------------
 * h3wMemInit()
 *     Initializes the HCI 3-wire memory module.  This function must be 
 *     invoked before any of the other HCI 3-wire memory functions will work 
 *     correctly.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire memory module context structure.  
 *     memPoolP - This parameter must point to a buffer that will be used as 
 *         the memory pool.
 *     memPoolSize - This parameter must contain the size of the given memory 
 *         pool.  
 */
void h3wMemInit(Hci3WMemStruct *cP, 
                U8             *memPoolP, 
                U32             memPoolSize)
{
    BuffHdrStruct unusedMem;

    cP->initialized = FALSE;
    cP->privMemPoolP = memPoolP;
    if (0 < memPoolSize) {
        cP->privMemPoolSize = memPoolSize;
    } else {
        cP->privMemPoolSize = 0;
    }
    if (sizeof(BuffHdrStruct) <= cP->privMemPoolSize) {
        unusedMem.status     = BS_FREE;
        unusedMem.next.valid = FALSE;
        h3wMemSetHdr(cP, &unusedMem, 0);
        cP->initialized = TRUE;
    }
}



/*---------------------------------------------------------------------------
 * h3wMemGet()
 *     Allocates memory from the memory pool.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire memory module context structure.  
 *     buffP - This parameter must point to a location where the address of a 
 *         buffer can be written.  
 *     size - The size of the buffer to be allocated.  
 *     errObjP - A pointer to an error context structure.
 *
 * Returns:
 *     This function returns TRUE if it was successfully able to allocate a 
 *     buffer of the desired size, or FALSE otherwise.  
 */
BOOL h3wMemGet(Hci3WMemStruct *cP, 
               U8            **buffP, 
               U32             size,
               Hci3WErrStruct *errObjP)
{
    BOOL          bestInitialized;
    BOOL          gettingBuf;
    BOOL          splitChunk;
    BuffHdrStruct bhs;
    NdxStruct     best;
    NdxStruct     bestNext;
    U32           bestBuffSize;
    U32           buffSize;
    U32           ndx;
    U32           splitNdx;

    bestBuffSize = cP->privMemPoolSize;
    best.valid     = FALSE;
    best.ndx       = 0;
    bestNext.valid = FALSE;
    bestNext.ndx   = 0;
    if (TRUE != cP->initialized) {
        h3wErrSet(errObjP, ES_MEM_GETBUF, ER_INSUFFICIENT_MEMORY);
    }
    if (TRUE == h3wErrHasNone(errObjP)) {

        /* Find the chunk of free memory that 
         * most closely matches the desired size.  
         */ 
        bestInitialized = FALSE;
        ndx = 0;
        do {
            gettingBuf = FALSE;
            h3wMemGetHdr(cP, &bhs, ndx);
            if (BS_FREE == bhs.status) {
                if (TRUE != bhs.next.valid) {
                    buffSize = cP->privMemPoolSize 
                             - (ndx + sizeof(BuffHdrStruct));
                } else {
                    buffSize = bhs.next.ndx 
                             - (ndx + sizeof(BuffHdrStruct));
                }
                if (buffSize >= size) {
                    if (TRUE == bestInitialized) {
                        if (buffSize < bestBuffSize) {
                            bestBuffSize   = buffSize;
                            best.valid     = TRUE;
                            best.ndx       = ndx;
                            bestNext.valid = bhs.next.valid;
                            bestNext.ndx   = bhs.next.ndx;
                        }
                    } else {
                        bestBuffSize    = buffSize;
                        best.valid      = TRUE;
                        best.ndx        = ndx;
                        bestNext.valid  = bhs.next.valid;
                        bestNext.ndx    = bhs.next.ndx;
                        bestInitialized = TRUE;
                    }
                }
            }
            if (TRUE == bhs.next.valid) {
                ndx = bhs.next.ndx;
                gettingBuf = TRUE;
            }
        } while (TRUE == gettingBuf);
        if (TRUE != bestInitialized) {
            h3wErrSet(errObjP, ES_MEM_GETBUF, ER_INSUFFICIENT_MEMORY);
        }

    }
    if (TRUE == h3wErrHasNone(errObjP)) {
        splitNdx = best.ndx + sizeof(BuffHdrStruct) + size;
        bhs.status = BS_IN_USE;
        if (TRUE != bestNext.valid) {
            if ((U32)sizeof(BuffHdrStruct) + size >= bestBuffSize) {
                splitChunk = FALSE;
            } else {
                splitChunk = TRUE;
            }
        } else {
            if ((U32)sizeof(BuffHdrStruct) + splitNdx > bestNext.ndx) {
                splitChunk = FALSE;
            } else {
                splitChunk = TRUE;
            }
        }
        if (TRUE == splitChunk) {
            bhs.next.valid = TRUE;
            bhs.next.ndx   = splitNdx;
            h3wMemSetHdr(cP, &bhs, best.ndx);

            /* Split the previous free chunk of memory
             * into the used and the free portion. 
             */ 
            bhs.status     = BS_FREE;
            bhs.next.valid = bestNext.valid;
            bhs.next.ndx   = bestNext.ndx;
            h3wMemSetHdr(cP, &bhs, splitNdx);

        } else {

            /* This buffer is too small to split up into a used 
             * and a free portion, so just take the whole thing.
             */ 
            bhs.next.valid = bestNext.valid;
            bhs.next.ndx   = bestNext.ndx;
            h3wMemSetHdr(cP, &bhs, best.ndx);

        }
        *buffP = cP->privMemPoolP + best.ndx + sizeof(BuffHdrStruct);
    }
    return(h3wErrHasNone(errObjP));
}



/*---------------------------------------------------------------------------
 * h3wMemFree()
 *     This function frees up a previously allocated memory buffer.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire memory module context structure.  
 *     buff - This parameter must point to a buffer that was previously 
 *         allocated by this module.  
 *     errObjP - A pointer to an error context structure.
 *
 * Returns:
 *     This function returns TRUE if it was successfully able to deallocate 
 *     the given memory buffer, or FALSE otherwise.  
 */
BOOL h3wMemFree(Hci3WMemStruct *cP,
                U8             *buff, 
                Hci3WErrStruct *errObjP)
{
    BOOL          findingBuf;
    BuffHdrStruct currBHS;
    BuffHdrStruct nextBHS;
    BuffHdrStruct prevBHS;
    NdxStruct     curr;
    NdxStruct     next;
    NdxStruct     prev;

    Assert(TRUE == cP->initialized);
    prev.valid         = FALSE;
    prev.ndx           = 0;
    curr.valid         = TRUE;
    curr.ndx           = 0;
    prevBHS.next.valid = FALSE;
    prevBHS.status     = BS_IN_USE;

    /* Search for an buffer with the same address as the given buffer.  */ 
    do {
        findingBuf = FALSE;
        if (cP->privMemPoolP + curr.ndx + sizeof(BuffHdrStruct) >= buff) {
            Assert(  cP->privMemPoolP + curr.ndx + sizeof(BuffHdrStruct) 
                  == buff);
            h3wMemGetHdr(cP, &currBHS, curr.ndx);
            currBHS.status = BS_FREE;
            h3wMemSetHdr(cP, &currBHS, curr.ndx);
            if (TRUE == prev.valid) {
                if (BS_FREE == prevBHS.status) {

                    /* Merge the previous and the current records */ 
                    prevBHS.next.valid = currBHS.next.valid;
                    prevBHS.next.ndx   = currBHS.next.ndx;
                    h3wMemSetHdr(cP, &prevBHS, prev.ndx);
                    currBHS.next.valid = prevBHS.next.valid;
                    currBHS.next.ndx   = prevBHS.next.ndx;
                    currBHS.status     = prevBHS.status;
                    curr.valid         = prev.valid;
                    curr.ndx           = prev.ndx;
                }
            }
            if (TRUE == currBHS.next.valid) {
                next.valid = currBHS.next.valid;
                next.ndx   = currBHS.next.ndx;
                h3wMemGetHdr(cP, &nextBHS, next.ndx);
                if (BS_FREE == nextBHS.status) {

                    /* Merge the current and next records */ 
                    currBHS.next.valid = nextBHS.next.valid;
                    currBHS.next.ndx   = nextBHS.next.ndx;
                    h3wMemSetHdr(cP, &currBHS, curr.ndx);

                }
            }
        } else {

            /* The buffer address does not match, so continue searching.  */ 
            h3wMemGetHdr(cP, &currBHS, curr.ndx);
            Assert(TRUE == currBHS.next.valid);
            prev.valid         = curr.valid;
            prev.ndx           = curr.ndx;
            prevBHS.next.valid = currBHS.next.valid;
            prevBHS.next.ndx   = currBHS.next.ndx;
            prevBHS.status     = currBHS.status;
            curr.valid         = currBHS.next.valid;
            curr.ndx           = currBHS.next.ndx;
            findingBuf         = TRUE;

        }
    } while (TRUE == findingBuf);

    return(h3wErrHasNone(errObjP));
}



/*---------------------------------------------------------------------------
 * h3wMemTerm()
 *     This function destroys a given memory module, cleaning up any 
 *     allocated resources.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire memory module context structure.  
 */
void h3wMemTerm(Hci3WMemStruct *cP)
{
    cP->initialized = FALSE;
}



/****************************************************************************
 *
 * Private Functions
 *
 ****************************************************************************/



/*---------------------------------------------------------------------------
 * h3wMemSetHdr()
 *     This function sets the header block of data at the indicated memory 
 *     pool location.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire memory module context structure.  
 *     bhS - This parameter must point to a buffer header structure that is 
 *         to be copied into the memory pool location.  
 *     dstNdx - This is the index into the memory pool where the header data 
 *         is to be written.  
 */
static void h3wMemSetHdr(Hci3WMemStruct *cP, 
                         BuffHdrStruct  *bhS, 
                         U32             dstNdx) 
{
    U8 *dstP;
    U8  ndx;
    U8 *srcP;

    srcP = (U8 *)bhS;
    dstP = cP->privMemPoolP + dstNdx;
    for (ndx = 0; ndx < sizeof(BuffHdrStruct); ndx += 1) {
        *(dstP + ndx) = *(srcP + ndx);
    }
}



/*---------------------------------------------------------------------------
 * h3wMemGetHdr()
 *     This function copies the header block of data from the indicated 
 *     memory pool location into a given buffer header structure.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire memory module context structure.  
 *     bhS - This parameter must point to a location where a buffer header 
 *         structure can be written.  
 *     srcNdx - This is the index into the memory pool where the header data 
 *         is to be read from.  
 */
static void h3wMemGetHdr(Hci3WMemStruct *cP, 
                         BuffHdrStruct  *bhS, 
                         U32             srcNdx) 
{
    U8 *dstP;
    U8  ndx;
    U8 *srcP;

    srcP = cP->privMemPoolP + srcNdx;
    dstP = (U8 *)bhS;
    for (ndx = 0; ndx < sizeof(BuffHdrStruct); ndx += 1) {
        *(dstP + ndx) = *(srcP + ndx);
    }
}
