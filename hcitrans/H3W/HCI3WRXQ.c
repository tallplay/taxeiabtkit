/****************************************************************************
 *
 * File:
 *     $Workfile:HCI3WRXQ.c$ for iAnywhere Blue SDK, Version 2.2
 *     $Revision:10$
 *
 * Description: This file is part of an HCI 3-Wire implementation.  It 
 * contains code dealing with messages stored and retrieved from a receive 
 * queue.  
 *
 * Created:     October 31, 2006
 *
 * Copyright 2006-2007 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions,  
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "sys/HCI3WRXQ.h"

/****************************************************************************
 *
 * Prototypes
 *
 ****************************************************************************/

static BOOL h3wRXQPopRoot(Hci3WRxQStruct *cP, 
                          BuffUseStruct  *dstP, 
                          Hci3WErrStruct *errObjP);

static void h3wRXQCopyBuffBytes(BuffUseStruct *dstP, 
                                BuffUseStruct *srcP, 
                                U16           *copiedP);

/****************************************************************************
 *
 * Public Functions
 *
 ****************************************************************************/



/*---------------------------------------------------------------------------
 * h3wRXQInit()
 *     Initializes the HCI 3-wire receive queue module.  This function must 
 *     be invoked before any of the other HCI 3-wire receive queue functions 
 *     will work correctly.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire receive queue module context structure.  
 *     memObjP - Pointer to an HCI 3-wire memory module context structure.  
 *     utlObjP - Pointer to an HCI 3-wire utilities module context structure.  
 */
void h3wRXQInit(Hci3WRxQStruct *cP, 
                Hci3WMemStruct *memObjP, 
                Hci3WUtlStruct *utlObjP)
{
    cP->privMemObjP = memObjP;
    cP->privUtlObjP = utlObjP;
    cP->rootValid = FALSE;
    cP->rootPtr = (U8 *)0;
}



/*---------------------------------------------------------------------------
 * h3wRXQGet()
 *     This function returns the node pointed at by the root of the receive 
 *     queue.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire receive queue module context structure.  
 *     dstP - A pointer to a buffer use structure, where information about 
 *         the newly popped node can be written.  
 *     errObjP - A pointer to an error context structure.
 *
 * Returns:
 *     This routine returns TRUE if the receive queue was emptry or if it was 
 *     successfully able to get a node from the receive queue.  Otherwise, it 
 *     returns FALSE.  
 */
BOOL h3wRXQGet(Hci3WRxQStruct *cP, 
               BuffUseStruct  *dstP, 
               Hci3WErrStruct *errObjP)
{
    dstP->used = 0;
    if (dstP->used < dstP->maxSize) {
        if (TRUE == cP->rootValid) {
            h3wRXQPopRoot(cP, dstP, errObjP);
        }
    }
    return(h3wErrHasNone(errObjP));
}



/*---------------------------------------------------------------------------
 * h3wRXQGetAll()
 *     This function returns the all of the data from the receive queue.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire receive queue module context structure.  
 *     dstP - A pointer to a buffer use structure, where information about 
 *         the receive queue can be written.  
 *     errObjP - A pointer to an error context structure.
 *
 * Returns:
 *     This routine returns TRUE if the receive queue was emptry or if it was 
 *     successfully able to get all the nodes from the receive queue.  
 *     Otherwise, it returns FALSE.  
 */
BOOL h3wRXQGetAll(Hci3WRxQStruct *cP, 
                  BuffUseStruct  *dstP, 
                  Hci3WErrStruct *errObjP)
{
    BOOL gettingBytes;

    dstP->used = 0;
    do {
        gettingBytes = FALSE;
        if (dstP->used < dstP->maxSize) {
            if (TRUE == cP->rootValid) {
                if (TRUE == h3wRXQPopRoot(cP, dstP, errObjP)) {
                    gettingBytes = TRUE;
                }
            }
        }
    } while (TRUE == gettingBytes);
    return(h3wErrHasNone(errObjP));
}



/*---------------------------------------------------------------------------
 * h3wRXQFree()
 *     This function frees up all the nodes in the receive queue.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire receive queue module context structure.  
 *     errObjP - A pointer to an error context structure.
 *
 * Returns:
 *     This function returns TRUE if it was successfully able to free up all 
 *     of the nodes in the receive queue, or FALSE otherwise.  
 */
void h3wRXQFree(Hci3WRxQStruct *cP, Hci3WErrStruct *errObjP)
{
    BOOL             freeingAllocatedMemory;
    BuffUseStruct    src;
    RxQNodeHdrStruct currNode;

    do {
        freeingAllocatedMemory = FALSE;
        if (TRUE == cP->rootValid) {

            /* Get information about the first buffer in the list. */ 
            cP->bufPtrs.src = cP->rootPtr;
            cP->bufPtrs.dst = (U8 *)(&currNode);
            h3wUtlCopyBytes(&cP->bufPtrs, sizeof(RxQNodeHdrStruct));
            src.buf = cP->rootPtr;
            src.maxSize = (U16)(currNode.size + sizeof(RxQNodeHdrStruct));
            src.used = src.maxSize;

            /* Change the root to the second buffer in the list. */ 
            cP->rootValid = currNode.hasNext;
            cP->rootPtr = currNode.nextPtr;

            /* Free the first buffer */ 
            if (TRUE == h3wMemFree(cP->privMemObjP, src.buf, errObjP)) {
                freeingAllocatedMemory = TRUE;
            }

        }
    } while (TRUE == freeingAllocatedMemory);
}



/*---------------------------------------------------------------------------
 * h3wRXQIsEmpty()
 *     Indicates whether the receive queue is empty.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire receive queue module context structure.  
 *
 * Returns:
 *     This function returns TRUE if the receive queue is empty, or FALSE 
 *     otherwise.  
 */
BOOL h3wRXQIsEmpty(Hci3WRxQStruct *cP)
{
    BOOL retVal;

    retVal = TRUE;
    if (TRUE == cP->rootValid) {
        retVal = FALSE;
    }
    return(retVal);
}



/*---------------------------------------------------------------------------
 * h3wRXQPut()
 *     This function allocates memory for the new node, pushes the given data 
 *     onto the tail end of the receive queue, and copies the given data into 
 *     the new node.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire receive queue module context structure.  
 *     buf - This parameter must point to a buffer that is to be pushed onto 
 *         the recieve queue.
 *     size - This parameter must contain the size of the given buffer.  
 *     errObjP - A pointer to an error context structure.
 *
 * Returns:
 *     This function returns TRUE if it was successfully able to push the 
 *     given buffer onto the receive queue, or FALSE otherwise.  
 */
BOOL h3wRXQPut(Hci3WRxQStruct *cP,
               U8             *buf, 
               U16             size, 
               Hci3WErrStruct *errObjP)
{
    BOOL             findingTail;
    RxQNodeHdrStruct currNode;
    RxQNodeHdrStruct newNode;
    U8              *currNodeP;
    U8              *newBuff;

    if (0 < size) {

        /* Attempt to allocate enough memory to store 
         * the node about to be pushed onto the queue.  
         */ 
        if (  TRUE 
           == h3wMemGet( cP->privMemObjP, 
                        &newBuff, 
                         sizeof(RxQNodeHdrStruct) + size, 
                         errObjP)) {

            if (TRUE == cP->rootValid) {

                /* Find the tail node in the receive queue.  */ 
                currNodeP = cP->rootPtr;
                do {
                    findingTail = FALSE;
                    cP->bufPtrs.src = currNodeP;
                    cP->bufPtrs.dst = (U8 *)(&currNode);
                    h3wUtlCopyBytes(&cP->bufPtrs, 
                                     sizeof(RxQNodeHdrStruct));
                    if (TRUE == currNode.hasNext) {
                        currNodeP = currNode.nextPtr;
                        findingTail = TRUE;
                    }
                } while (TRUE == findingTail);

                /* Rewrite the header in the tail node 
                 * to point to the newly allocated node.  
                 */ 
                currNode.hasNext = TRUE;
                currNode.nextPtr = newBuff;
                cP->bufPtrs.src = (U8 *)(&currNode);
                cP->bufPtrs.dst = currNodeP;
                h3wUtlCopyBytes(&cP->bufPtrs, sizeof(RxQNodeHdrStruct));
            } else {
                cP->rootValid = TRUE;
                cP->rootPtr = newBuff;
            }

            /* Write the header data into the newly allocated node.  */ 
            newNode.hasNext = FALSE;
            newNode.nextPtr = (U8 *)0;
            newNode.size = size;
            cP->bufPtrs.src = (U8 *)(&newNode);
            cP->bufPtrs.dst = newBuff;
            h3wUtlCopyBytes(&cP->bufPtrs, sizeof(RxQNodeHdrStruct));

            /* Write the data buffer into the newly allocated node.  */ 
            cP->bufPtrs.src = buf;
            h3wUtlCopyBytes(&cP->bufPtrs, size);
        }
    }
    return(h3wErrHasNone(errObjP));
}



/*---------------------------------------------------------------------------
 * h3wRXQTerm()
 *     This function destroys a given receive queue module, cleaning up any 
 *     allocated resources.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire receive queue module context structure.  
 */
void h3wRXQTerm(Hci3WRxQStruct *cP)
{
}



/****************************************************************************
 *
 * Private Functions
 *
 ****************************************************************************/



/*---------------------------------------------------------------------------
 * h3wRXQCopyBuffBytes()
 *     This function copies data from a source buffer into a destination 
 *     buffer.  
 * 
 * Parameters:
 *     dstP - This parameter must point to a buffer use structure where the 
 *         source data can be copied.
 *     srcP - This parameter must point to the source buffer use structure.  
 *     copiedP - This parameter must point to a location where the number of 
 *         bytes copied can be recorded.  
 */
static void h3wRXQCopyBuffBytes(BuffUseStruct *dstP,
                                BuffUseStruct *srcP,
                                U16           *copiedP)
{
    BOOL copyingBytes;

    do {
        copyingBytes = FALSE;
        if (dstP->used < dstP->maxSize) {
            if (*copiedP < srcP->maxSize) {
                *(dstP->buf + dstP->used) = *(srcP->buf + *copiedP);
                dstP->used  += 1;
                *copiedP    += 1;
                copyingBytes = TRUE;
            }
        }
    } while (copyingBytes);
}



/*---------------------------------------------------------------------------
 * h3wRXQPopRoot()
 *     This function pops the head node off of the receive queue and copies it 
 *     into the given destination buffer.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire receive queue module context structure.  
 *     dstP - This parameter must point at a buffer use structure where 
 *         containing information about the destination buffer.  
 *     errObjP - A pointer to an error context structure.
 *
 * Returns:
 *     This function returns TRUE if it was successfully able to pop the 
 *     first buffer off of the receive queue and copy it into the destination 
 *     buffer.  Otherwise, it returns FALSE.  
 */
static BOOL h3wRXQPopRoot(Hci3WRxQStruct *cP, 
                          BuffUseStruct  *dstP, 
                          Hci3WErrStruct *errObjP)
{
    BuffUseStruct    src;
    RxQNodeHdrStruct currNode;
    U16              copied;

    /* Get information about the first buffer in the list. */ 
    cP->bufPtrs.src = cP->rootPtr;
    cP->bufPtrs.dst = (U8 *)(&currNode);
    h3wUtlCopyBytes(&cP->bufPtrs, sizeof(RxQNodeHdrStruct));
    src.buf = cP->rootPtr;
    src.maxSize = (U16)(currNode.size + sizeof(RxQNodeHdrStruct));
    src.used = src.maxSize;

    /* Change the root to the second buffer in the list. */ 
    cP->rootValid = currNode.hasNext;
    cP->rootPtr = currNode.nextPtr;

    /* Copy bytes from the first buffer
     * into the final destination buffer 
     */ 
    copied = sizeof(RxQNodeHdrStruct);
    h3wRXQCopyBuffBytes(dstP, &src, &copied);

    Assert(copied == src.maxSize);

    /* Free the first buffer */ 
    h3wMemFree(cP->privMemObjP, src.buf, errObjP);

    return(h3wErrHasNone(errObjP));
}

