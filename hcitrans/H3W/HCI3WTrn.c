/****************************************************************************
 *
 * File:
 *     $Workfile:HCI3WTrn.c$ for iAnywhere Blue SDK, Version 2.2
 *     $Revision:19$
 *
 * Description: This file is part of an HCI 3-Wire implementation.  It 
 * provides the transport layer interface between the upper HCI layer and the 
 * lower HCI 3-wire layer.  
 *
 * Created:     October 31, 2006
 *
 * Copyright 2006-2007 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions,  
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "btconfig.h"
#include "sys/hci3wtrn.h"
#include "parseopts.h"

/****************************************************************************
 *
 *  RAM Data
 *
 ****************************************************************************/

#if (XA_CONTEXT_PTR == XA_ENABLED)

/* If context pointers are used create a pointer called h3wxprt 
 */
HCI3W_Context h3wxprtTemp;
HCI3W_Context *h3wxprt = &h3wxprtTemp;

#elif (XA_CONTEXT_PTR == XA_DISABLED)

/* If context pointers are NOT used create a static object.
 */
HCI3W_Context h3wxprt;

#endif /* XA_CONTEXT_PTR */ 

/****************************************************************************
 *
 * Prototypes
 *
 ****************************************************************************/

static H3WMGR_ERROR_ENUM h3wTrnManager(H3WMGR_REQ_ENUM h3wmgrRequest, 
                                       DATA_UNION      data);

static void              H3W_SendData(HciPacket *hciPacket);

static void              H3W_BufferAvailable(HciBufFlags buffType);

static void              H3W_SetSpeed(U16 speed);

/****************************************************************************
 *
 * Public Functions
 *
 ****************************************************************************/



/*---------------------------------------------------------------------------
 * h3wTrnHandleRetryTX()
 *     This is the function that is invoked when the retry timer fires.  If 
 *     there is data in the pending transmit queue, it signals that the HCI 
 *     3-wire transport manager should attempt a transmit.  
 * 
 * Parameters:
 *     evmTimerP - The event manager structure corresponding to the timer that 
 *         just fired.  
 */
void h3wTrnHandleRetryTX(struct _EvmTimer *evmTimerP)
{
    DATA_UNION data;

    if (!IsListEmpty(&HCI3W(txQPending))) {
        data.hciPacket = (HciPacket *)0;
        h3wTrnManager(HR_TX, data);
    }
}



/*---------------------------------------------------------------------------
 * h3wTrnHandleSignalHCI()
 *     This is the function that is invoked when the signal HCI timer fires.  
 *     It signals the upper HCI layer that a message has been successfully 
 *     transmitted.  This function is used to work around a weakness in the HCI 
 *     logic.  
 * 
 * Parameters:
 *     evmTimerP - The event manager structure corresponding to the timer that 
 *         just fired.  
 */
void h3wTrnHandleSignalHCI(struct _EvmTimer *evmTimerP)
{
    TranEvent tranEvent;

    /* The HCI layer should really not process the events signaled by 
     * HCI3W(callback) in the same thread in which it is invoked.  
     * Instead, it should only set a flag when the HCI3W(callback) is 
     * invoked.  Later, the HCI manager thread should do the actual 
     * processing.  Otherwise, you can end up with deadlocks due to 
     * multiple threads trying to lock both the hardware and the stack at 
     * the same time.  Because this is currently not the case, we have to 
     * do a workaround.  Putting the invocation of the HCI3W(callback) in 
     * a timer causes the invocation to happen in another thread.
     */ 

    tranEvent = (TranEvent)((U32)evmTimerP->context);
    HCI3W(callback)(tranEvent, BT_STATUS_SUCCESS);
}



/*---------------------------------------------------------------------------
 * h3wTrnEventCallback()
 *     This function is invoked when the lower layer HCI 3-wire code determines 
 *     that an event has occurred that may be of interest to this upper layer.  
 * 
 * Parameters:
 *     hci3wEvent - This parameter contains an enumeration corresponding to 
 *         the event that just occurred.  
 *     callbackData - This contains data that corresponds to the event (if 
 *         any).  
 */
void h3wTrnEventCallback(H3W_EVENT_ENUM hci3wEvent, 
                         U8             callbackData)
{
    BOOL            doingPending;
    DATA_UNION      data;
    H3WMGR_REQ_ENUM pendingReq;
    U8              wakeNdx;

    data.hciPacket = (HciPacket *)0;
    switch(hci3wEvent) {
    case H3_ACTIVATED:
        h3wTrnManager(HR_INITIALIZE_DONE, data);
        break;

    case H3_LINK_LOST:
        h3wTrnManager(HR_LINK_UNEXPECTED, data);
        break;

    case H3_DATA_RECEIVED:
        h3wTrnManager(HR_RX, data);
        break;

    case H3_TRANSMIT_COMPLETE:
        break;

    case H3_LOW_POWER_WOKEN:
        HCI3W(cntrlrAsleep) = FALSE;
        do {
            doingPending = FALSE;
            if (0 < HCI3W(pendingReq)) {
                pendingReq = HCI3W(wake)[0].req;
                data.hciPacket = HCI3W(wake)[0].data.hciPacket;
                HCI3W(pendingReq) -= 1;
                for (wakeNdx  = 0; 
                     wakeNdx <  HCI3W(pendingReq); 
                     wakeNdx += 1) {
                    HCI3W(wake)[wakeNdx].req = HCI3W(wake)[wakeNdx + 1].req;
                    HCI3W(wake)[wakeNdx].data.hciPacket 
                        = HCI3W(wake)[wakeNdx + 1].data.hciPacket;
                }
                h3wTrnManager(pendingReq, data);
                doingPending = TRUE;
            }
        } while (TRUE == doingPending);
        break;

    case H3_RELIABLY_SENT:
        data.hci3wHandle = callbackData;
        h3wTrnManager(HR_TX_FREE, data);
        break;

    case H3_LOW_POWER_SLEEP:
        HCI3W(cntrlrAsleep) = TRUE;
        break;

    case H3_TERMINATED:
        HCI3W(h3wMgrState) = HS_INITIALIZED;
        h3wTrnManager(HR_TERMINATE_DONE, data);
        break;

    default:
        Assert(0);
    }
}



/*---------------------------------------------------------------------------
 * H3W_Init()
 *     This function is invoked by the upper HCI layer whenever the transport 
 *     layer needs to initialize the HCI 3-Wire transport and UART hardware 
 *     drivers.  
 * 
 * Parameters:
 *     tranCallback - This is the routine that should be invoked to signal the 
 *     upper HCI layers when a successful initialization or shutdown event has 
 *     occurred.  
 *
 * Returns:
 *     This function returns a Bluetooth Status enumeration indicating things 
 *     such as whether the operation succeeded, whether the operation is 
 *     currently pending, or whether the operation encountered some type of 
 *     error.  
 */
BtStatus H3W_Init(TranCallback tranCallback)
{
    BtStatus   retVal;
    DATA_UNION data;
    U16        ndx;

    data.hciPacket = (HciPacket *)0;
    Assert((0 < HCI3W_MAX_PAYLOAD   ) && (0x1000 > HCI3W_MAX_PAYLOAD   ));
    Assert((0 < HCI3W_SLIDING_WINDOW) && (8      > HCI3W_SLIDING_WINDOW));

    /* Clear the transport's context memory */ 

#if (XA_CONTEXT_PTR == XA_ENABLED)

    OS_MemSet((U8 *)h3wxprt, 0, sizeof(HCI3W_Context));

#elif (XA_CONTEXT_PTR == XA_DISABLED)

    OS_MemSet((U8 *)&h3wxprt, 0, sizeof(HCI3W_Context));

#endif /* XA_CONTEXT_PTR */ 

#if (HCI_SCO_FLOW_CONTROL == XA_ENABLED)

    HCI3W(hciSynchronousFlowControl) = TRUE;

#else /* HCI_SCO_FLOW_CONTROL */ 

    HCI3W(hciSynchronousFlowControl) = FALSE;

#endif  /* HCI_SCO_FLOW_CONTROL */ 

    HCI3W(h3wMgrState) = HS_UNINITIALIZED;
    HCI3W(aqrNdx) = 0;
    HCI3W(aqwNdx) = 0;
    for (ndx = 0; ndx < HCI3W_TXQ_MAX; ndx += 1) {
        if (0 < ndx) {
            HCI3W(txQ)[ndx].pNdx = (U8)(ndx - 1);
        } else {
            HCI3W(txQ)[ndx].pNdx = 0xFF;
        }
        if (HCI3W_TXQ_MAX > ndx + 1) {
            HCI3W(txQ)[ndx].cNdx = (U8)(ndx + 1);
        } else {
            HCI3W(txQ)[ndx].cNdx = 0xFF;
        }
    }
    HCI3W(txRootFree)             =  0;
    HCI3W(txRootToBeAcknowledged) = 0xFF;
    HCI3W(cntrlrAsleep)           = FALSE;
    HCI3W(pendingReq)             = 0;
    InitializeListHead(&(HCI3W(txQPending)));
    HCI3W(slidingWindowUsed)      = 0;

    /* Set up the timer that will be used to retry transmissions.  */ 
    InitializeListEntry(&HCI3W(retryTX).node);
    HCI3W(retryTX).context        = (void *)0;
    HCI3W(retryTX).func           = h3wTrnHandleRetryTX;
    HCI3W(retryTX).time           = (TimeT)0;
    HCI3W(retryTX).startTime      = (TimeT)0;

    /* Set up the timer that will be used to signal the HCI layer.  */ 
    InitializeListEntry(&HCI3W(signalHCI).node);
    HCI3W(signalHCI).context      = (void *)TRAN_INIT_STATUS;
    HCI3W(signalHCI).func         = h3wTrnHandleSignalHCI;
    HCI3W(signalHCI).time         = (TimeT)0;
    HCI3W(signalHCI).startTime    = (TimeT)0;

    switch(h3wTrnManager(HR_INITIALIZE, data)) {
    case HE_FINISHED_SUCCESSFULLY:
        retVal = BT_STATUS_SUCCESS;
        break;

    case HE_PENDING:
        retVal = BT_STATUS_PENDING;
        break;

    case HE_BUSY:
        retVal = BT_STATUS_BUSY;
        break;

    default:
        retVal = BT_STATUS_FAILED;
    }
    HCI3W(callback) = tranCallback;
    HCI3W(tranEntry).sendData  = H3W_SendData;
    HCI3W(tranEntry).buffAvail = H3W_BufferAvailable;
    HCI3W(tranEntry).setSpeed  = H3W_SetSpeed;
    HCI_RegisterTransport(&HCI3W(tranEntry));
    return(retVal);
}



/*---------------------------------------------------------------------------
 * H3W_Shutdown()
 *     This function is invoked by the upper HCI layer whenever the transport 
 *     layer needs to be shut down.
 * 
 * Returns:
 *     This function returns a Bluetooth Status enumeration indicating things 
 *     such as whether the operation succeeded, whether the operation is 
 *     currently pending, or whether the operation encountered some type of 
 *     error.  
 */
BtStatus H3W_Shutdown(void)
{
    BtStatus   retVal;
    DATA_UNION data;

    data.hciPacket = (HciPacket *)0;
    switch(h3wTrnManager(HR_TERMINATE, data)) {
    case HE_FINISHED_SUCCESSFULLY:
        retVal = BT_STATUS_SUCCESS;
        break;

    case HE_PENDING:
        retVal = BT_STATUS_PENDING;
        break;

    case HE_BUSY:
        retVal = BT_STATUS_BUSY;
        break;

    default:
        retVal = BT_STATUS_FAILED;
    }
    return retVal;
}



/****************************************************************************
 *
 * Private Functions
 *
 ****************************************************************************/



/*---------------------------------------------------------------------------
 * H3W_SendData()
 *     This function is invoked by the upper HCI layer whenever the transport 
 *     layer needs to transmit some data.  
 * 
 * Parameters:
 *     hciPacket - This parameter must point to an HCI packet structure 
 *         containing the data to be transmitted.  
 */
static void H3W_SendData(HciPacket *hciPacket)
{
    BOOL       wasEmpty;
    DATA_UNION data;

    if (IsListEmpty(&HCI3W(txQPending))) {
        wasEmpty = TRUE;
    } else {
        wasEmpty = FALSE;
    }
    InsertTailList(&HCI3W(txQPending), (ListEntry *)&hciPacket->node);
    data.hciPacket = (HciPacket *)0;
    h3wTrnManager(HR_TX, data);
    if (TRUE == wasEmpty) {
        if (!IsListEmpty(&HCI3W(txQPending))) {
            EVM_StartTimer(&HCI3W(retryTX), 250);
        }
    }
}



/*---------------------------------------------------------------------------
 * H3W_BufferAvailable()
 *     This function is invoked by the upper HCI layer whenever it finishes 
 *     using a receive buffer that can now be used by the HCI 3-wire transport 
 *     layer.  
 * 
 * Parameters:
 *     buffType - This parameter contains the HCI buffer flags indicating the 
 *     type of buffer that is now available.  
 */
static void H3W_BufferAvailable(HciBufFlags buffType)
{
    DATA_UNION data;

    data.hciPacket = (HciPacket *)0;
    if (TRUE == HCI3W(rxAwaitingHCIBuffer)) {
        switch(buffType) {
        case HCI_BUFTYPE_EVENT:
            if (PT_EVENT == HCI3W(rxPacketType)) {
                h3wTrnManager(HR_HCI_RX_BUFF_AVAIL, data);
            }
            break;

        case HCI_BUFTYPE_ACL_DATA:
            if (PT_ACL == HCI3W(rxPacketType)) {
                h3wTrnManager(HR_HCI_RX_BUFF_AVAIL, data);
            }
            break;

        case HCI_BUFTYPE_SCO_DATA:
            if (PT_SYNCH == HCI3W(rxPacketType)) {
                h3wTrnManager(HR_HCI_RX_BUFF_AVAIL, data);
            }
            break;

        default:
            break;
        }
    }
}



/*---------------------------------------------------------------------------
 * H3W_SetSpeed()
 *     This function is invoked by the upper HCI layer whenever the transport 
 *     layer needs to set the UART BAUD rate.  
 * 
 * Parameters:
 *     speed - An encoding of a UART speed as understood by the UART software.  
 */
static void H3W_SetSpeed(U16 speed)
{
    U32 bps;
    U32 h3Ret;

    h3Ret = h3wUARTEncodingToBPS(&HCI3W(hci3wObj), &bps, speed);
    if (0 == h3Ret) {
        h3Ret = h3wDataSetSpeed(&HCI3W(hci3wObj), bps);
    }
    if (0 != h3Ret) {
        Report(("HCI3W:  Unable to set the speed to %d.\n", speed));
    }
}



/*---------------------------------------------------------------------------
 * h3wTrnReqAction()
 *     This function pushes an request onto the action queue.  
 * 
 * Parameters:
 *     action - An enumeration corresponding to the requested action.  
 *     data - A union containing the requested action data (if any).  
 *
 * Returns:
 *     This function returns an index to the node just written to on the 
 *     action queue.  
 */
static U8 h3wTrnReqAction(H3WMGR_REQ_ENUM action, DATA_UNION data)
{
    U8 retVal;

    retVal = HCI3W(aqwNdx);
    HCI3W(actionQ)[HCI3W(aqwNdx)].req = action;
    HCI3W(actionQ)[HCI3W(aqwNdx)].data.hciPacket = data.hciPacket;
    if (ACTION_Q_MAX > (HCI3W(aqwNdx) + 1)) {
        HCI3W(aqwNdx) += 1;
    } else {
        HCI3W(aqwNdx)  = 0;
    }
    Assert(HCI3W(aqrNdx) != HCI3W(aqwNdx));
    return(retVal);
}



/*---------------------------------------------------------------------------
 * h3wTrnMngInit()
 *     This function invokes the HCI 3-wire initialization routine.  If 
 *     needed, It also calls the HCI 3-wire routine that limits Tmax.  
 * 
 * Parameters:
 *     intSlidingWindow - This parameter must contain the desired HCI 3-wire 
 *         sliding window size.  
 *
 * Returns:
 *     This function returns an HCI 3-wire status enumeration value indicating 
 *     things such as whether the operation succeeded, whether the operation 
 *     is currently pending, or whether the operation encountered some type of 
 *     error.  
 */
static H3WMGR_ERROR_ENUM h3wTrnMngInit(U8 intSlidingWindow)
{
    BOOL                allowOOF;
    ConfigOptionsStruct allowed;
    H3WMGR_ERROR_ENUM   retVal;

    if (HS_UNINITIALIZED == HCI3W(h3wMgrState)) {
        if (2 > intSlidingWindow) {
            allowed.slidingWindow = SW_LOWEST;
        } else {
            if (6 < intSlidingWindow) {
                allowed.slidingWindow = SW_HIGHEST;
            } else {
                switch(intSlidingWindow) {
                case 2:
                    allowed.slidingWindow = SW_LOWER;
                    break;

                case 4:
                    allowed.slidingWindow = SW_MID;
                    break;

                case 5:
                    allowed.slidingWindow = SW_HIGH;
                    break;

                case 6:
                    allowed.slidingWindow = SW_HIGHER;
                    break;

                case 3:
                default:
                    allowed.slidingWindow = SW_LOW;
                }
            }
        }

#if (HCI3W_CRC == XA_ENABLED)

        allowed.crc = TRUE;

#else /* HCI3W_CRC */ 

        allowed.crc = FALSE;

#endif /* HCI3W_CRC */ 

#if (HCI3W_OOF == XA_ENABLED)

        allowOOF = TRUE;

#else /* HCI3W_OOF */ 

        allowOOF = FALSE;

#endif /* HCI3W_OOF */ 

    DPrintf("%x",                  &HCI3W(aqrNdx));
    DPrintf("%x",                  &HCI3W(aqwNdx));
    DPrintf("%x",                  &HCI3W(h3wmgrBusy));
    DPrintf("%x",                  &HCI3W(hci3wRXBuff));
    DPrintf("%x",                  &HCI3W(hci3wTXBuff));
    DPrintf("%x",                  &HCI3W(memPool));

        h3wInit(&HCI3W(hci3wObj), 
                &HCI3W(memPool[0]),
                 HCI3W_MEM_POOL_SIZE,
                 HCI3W(hciSynchronousFlowControl),
                 RE_HOST,
                 allowed,
                 allowOOF, 
                 h3wTrnEventCallback);
        retVal = HE_FINISHED_SUCCESSFULLY;
        HCI3W(h3wMgrState) = HS_MAX_PAYLOAD;
    } else {
        retVal = HE_ALREADY_INITIALIZED;
    }
    return(retVal);
}



/*---------------------------------------------------------------------------
 * h3wTrnMngInitDone()
 *     This function obtains the configuration options in use by the lower 
 *     level HCI 3-wire transport layer.  Then it invokes the callback that 
 *     signals the upper HCI layer to let it know that the HCI 3-wire 
 *     transport layer has been successfully initialized.
 * 
 * Returns:
 *     This function returns an HCI 3-wire status enumeration value indicating 
 *     things such as whether the operation succeeded, whether the operation 
 *     is currently pending, or whether the operation encountered some type of 
 *     error.  
 */
static H3WMGR_ERROR_ENUM h3wTrnMngInitDone(void)
{
    H3WMGR_ERROR_ENUM retVal;
    U32               h3Ret;

    if (HS_LINKING == HCI3W(h3wMgrState)) {
        HCI3W(h3wMgrState) = HS_ACTIVE;
        h3Ret = h3wGetConfigOptions(&HCI3W(hci3wObj), &HCI3W(configured));
        if (0 == h3Ret) {

            /* To avoid a deadlock due to multiple threads trying to lock 
             * the stack and the hardware at the same time, don't invoke 
             * the HCI callback directly.  Instead, let a timer invoke the 
             * callback, since the timer will run in a separate thread.
             */ 
            HCI3W(signalHCI).context = (void *)TRAN_INIT_STATUS;
            EVM_StartTimer(&HCI3W(signalHCI), 0);
            retVal = HE_FINISHED_SUCCESSFULLY;

        } else {
            retVal = HE_GET_CONFIG_FAILURE;
        }
    } else {
        retVal = HE_NOT_LINKING;
    }
    return(retVal);
}



/*---------------------------------------------------------------------------
 * h3wTrnMngPld()
 *     This function invokes the lower level HCI 3-wire transport routine 
 *     that sets the maximum allowable payload size (before framing and slip 
 *     encoding).  
 * 
 * Returns:
 *     This function returns an HCI 3-wire status enumeration value indicating 
 *     things such as whether the operation succeeded, whether the operation 
 *     is currently pending, or whether the operation encountered some type of 
 *     error.  
 */
static H3WMGR_ERROR_ENUM h3wTrnMngPld(void)
{
    H3WMGR_ERROR_ENUM retVal;
    U32               h3Ret;

    if (HS_MAX_PAYLOAD == HCI3W(h3wMgrState)) {
        h3Ret = h3wSetMaxPayloadBytes(&HCI3W(hci3wObj), 
                                       HCI3W_MAX_PAYLOAD);
        if (0 == h3Ret) {
            HCI3W(h3wMgrState) = HS_UART_MULT;
            retVal = HE_FINISHED_SUCCESSFULLY;
        } else {
            HCI3W(h3wMgrState) = HS_UNINITIALIZED;
            retVal = HE_PAYLOAD_MAX_FAILURE;
        }
    } else {
        retVal = HE_PAYLOAD_DEFINED;
    }
    return(retVal);
}



/*---------------------------------------------------------------------------
 * h3wTrnMngUARTMult()
 *     This function invokes the lower level HCI 3-wire transport routine 
 *     that sets the uart clock multiplier.  
 * 
 * Returns:
 *     This function returns an HCI 3-wire status enumeration value indicating 
 *     things such as whether the operation succeeded, whether the operation 
 *     is currently pending, or whether the operation encountered some type of 
 *     error.  
 */
static H3WMGR_ERROR_ENUM h3wTrnMngUARTMult(U8 uartClockMultiplier)
{
    H3WMGR_ERROR_ENUM retVal;
    OVERCLOCK_ENUM    overClock;
    U32               h3Ret;

    if (HS_UART_MULT == HCI3W(h3wMgrState)) {
        switch(uartClockMultiplier) {
        case 2:
            overClock = OR_2;
            break;

        case 4:
            overClock = OR_4;
            break;

        case 8:
            overClock = OR_8;
            break;

        case 1:
        default:
            overClock = OR_1;
        }
        h3Ret = h3wSetUARTClockMultiplier(&HCI3W(hci3wObj), overClock);
        if (0 == h3Ret) {
            HCI3W(h3wMgrState) = HS_INITIALIZED;
            retVal = HE_FINISHED_SUCCESSFULLY;
        } else {
            HCI3W(h3wMgrState) = HS_UNINITIALIZED;
            retVal = HE_UART_MULT_FAILURE;
        }
    } else {
        retVal = HE_UART_MULT_DEFINED;
    }
    return(retVal);
}



/*---------------------------------------------------------------------------
 * h3wTrnMngLnk()
 *     This function invokes the lower level HCI 3-wire transport routine 
 *     that commences the linkup process.  
 * 
 * Returns:
 *     This function returns an HCI 3-wire status enumeration value indicating 
 *     things such as whether the operation succeeded, whether the operation 
 *     is currently pending, or whether the operation encountered some type of 
 *     error.  
 */
static H3WMGR_ERROR_ENUM h3wTrnMngLnk(void)
{
    DATA_INIT_ENUM    h3wDataInitState;
    H3WMGR_ERROR_ENUM retVal;
    U16               uartSpeedEncoded;
    U32               bps;
    U32               h3Ret;

    if (HS_INITIALIZED == HCI3W(h3wMgrState)) {
        HCI3W(h3wMgrState) = HS_LINKING;
    }
    if (HS_LINKING == HCI3W(h3wMgrState)) {
        uartSpeedEncoded = getSpeedOption();
        if (0 == uartSpeedEncoded) {
            uartSpeedEncoded = 115;
        }
        h3Ret 
            = h3wUARTEncodingToBPS(&HCI3W(hci3wObj), &bps, uartSpeedEncoded);
        if (0 == h3Ret) {
            h3Ret 
                = h3wDataInit(&HCI3W(hci3wObj), 
                              &h3wDataInitState, 
                              &HCI3W(configured),
                               bps);
            if (0 == h3Ret) {
                if (DI_LINK_ACTIVE == h3wDataInitState) {
                    HCI3W(h3wMgrState) = HS_ACTIVE;
                    retVal = HE_FINISHED_SUCCESSFULLY;
                } else {
                    retVal = HE_PENDING;
                }
            } else {
                HCI3W(h3wMgrState) = HS_UNINITIALIZED;
                retVal = HE_LINK_FAILURE;
            }
        } else {
            HCI3W(h3wMgrState) = HS_UNINITIALIZED;
            retVal = HE_BPS_FAILURE;
        }
    } else {
        retVal = HE_ALREADY_LINKED;
    }
    return(retVal);
}



/*---------------------------------------------------------------------------
 * h3wTrnMngWake()
 *     This function invokes the lower level HCI 3-wire transport routine 
 *     that commences the wakeup process.  If the device is still asleep, 
 *     this routine pushes the given pending request information (if provided) 
 *     onto the wakeup queue.  
 * 
 * Parameters:
 *     eventIsPending - This parameter must be TRUE if there is a new pending 
 *        request waiting for the wakup event.  
 *     pendingReq - If there is a pending request, this parameter contains an 
 *         enumeration corresponding to the request.  Otherwise, this 
 *         parameter is meaningless.  
 *     pendingData - If there is a pending request, this parameter contains 
 *         the request data (if any).  Otherwise, this parameter is meaningless.  
 *
 * Returns:
 *     This function returns an HCI 3-wire status enumeration value indicating 
 *     things such as whether the operation succeeded, whether the operation 
 *     is currently pending, or whether the operation encountered some type of 
 *     error.  
 */
static H3WMGR_ERROR_ENUM h3wTrnMngWake(BOOL            eventIsPending, 
                                       H3WMGR_REQ_ENUM pendingReq, 
                                       DATA_UNION      pendingData)
{
    H3WMGR_ERROR_ENUM retVal;
    LOW_POWER_ENUM    lowPowerState;
    U32               h3Ret;

    if (HS_ACTIVE == HCI3W(h3wMgrState)) {
        HCI3W(h3wMgrState) = HS_WAKING;
    }
    if (HS_WAKING == HCI3W(h3wMgrState)) {
        h3Ret = h3wWakeup(&HCI3W(hci3wObj), &lowPowerState);
        if (0 == h3Ret) {
            if (LP_AWAKE == lowPowerState) {
                HCI3W(h3wMgrState) = HS_ACTIVE;
                HCI3W(cntrlrAsleep) = FALSE;
                retVal = HE_FINISHED_SUCCESSFULLY;
            } else {
                if (TRUE == eventIsPending) {
                    Assert(MAX_WAKE_PENDING > HCI3W(pendingReq));
                    HCI3W(wake)[HCI3W(pendingReq)].req = pendingReq;
                    HCI3W(wake)[HCI3W(pendingReq)].data.hciPacket 
                        = pendingData.hciPacket;
                    HCI3W(pendingReq) += 1;
                }
                retVal = HE_PENDING;
            }
        } else {
            retVal = HE_WAKE_FAILURE;
            HCI3W(h3wMgrState) = HS_ACTIVE;
        }
    } else {
        retVal = HE_NOT_WAKING;
    }
    return(retVal);
}



/*---------------------------------------------------------------------------
 * h3wTrnMngRX()
 *     This function invokes the lower level HCI 3-wire transport routine 
 *     that receive data.  
 * 
 * Returns:
 *     This function returns an HCI 3-wire status enumeration value indicating 
 *     things such as whether the operation succeeded, whether the operation 
 *     is currently pending, or whether the operation encountered some type of 
 *     error.  
 */
static H3WMGR_ERROR_ENUM h3wTrnMngRX(void)
{
    H3WMGR_ERROR_ENUM retVal;
    U32               h3Ret;

    if (HS_ACTIVE == HCI3W(h3wMgrState)) {
        h3Ret = h3wReceive(&HCI3W(hci3wObj),
                           &HCI3W(hci3wRXBuff)[0],
                           &HCI3W(rxBuffUsed),
                            HCI3W_MAX_PAYLOAD,
                           &HCI3W(rxPacketType));
        if (0 == h3Ret) {
            if (0 < HCI3W(rxBuffUsed)) {
                HCI3W(h3wMgrState) = HS_NEED_HCI_RX_BUFF;
            }
            retVal = HE_FINISHED_SUCCESSFULLY;
        } else {
            retVal = HE_RX_FAILURE;
        }
    } else {
        retVal = HE_NOT_ACTIVE;
    }
    return(retVal);
}



/*---------------------------------------------------------------------------
 * h3wTrnMngGetRXBuff()
 *     This function invokes the upper HCI layer routines that obtain a buffer 
 *     where recieved data can be written.  
 * 
 * Returns:
 *     This function returns an HCI 3-wire status enumeration value indicating 
 *     things such as whether the operation succeeded, whether the operation 
 *     is currently pending, or whether the operation encountered some type of 
 *     error.  
 */
static H3WMGR_ERROR_ENUM h3wTrnMngGetRXBuff(void)
{
    BtStatus          status;
    H3WMGR_ERROR_ENUM retVal;
    U16               connHndl;

    if (HS_NEED_HCI_RX_BUFF == HCI3W(h3wMgrState)) {
        switch (HCI3W(rxPacketType)) {
        case PT_EVENT:

            /* Try to get a buffer from the HCI 
             * layer to store an event message.  
             */ 
            status = HCI_GetEventBuffer(&HCI3W(rxBuff), HCI3W(rxBuffUsed));

            switch(status) {
            case BT_STATUS_SUCCESS:
                HCI3W(h3wMgrState) = HS_GIVE_RX_TO_HCI;
                retVal = HE_FINISHED_SUCCESSFULLY;
                break;

            case BT_STATUS_NO_RESOURCES:
                HCI3W(rxAwaitingHCIBuffer) = TRUE;
                retVal = HE_PENDING;
                break;

            default:
                retVal = HE_HCIGETBUF;
            }
            break;

        case PT_ACL:
            if (2 < HCI3W(rxBuffUsed)) {

                /* Extract the connection handle from the 
                 * first two bytes of the received data.  
                 */ 
                connHndl = (U16)((HCI3W(hci3wRXBuff)[1] << 8) 
                                | HCI3W(hci3wRXBuff)[0]);

                /* Try to get a buffer from the HCI 
                 * layer to store an ACL message.  
                 */ 
                status 
                    = HCI_GetAclBuffer(&HCI3W(rxBuff), 
                                        connHndl, 
                                        HCI3W(rxBuffUsed));

                switch(status) {
                case BT_STATUS_SUCCESS:
                    HCI3W(h3wMgrState) = HS_GIVE_RX_TO_HCI;
                    retVal = HE_FINISHED_SUCCESSFULLY;
                    break;

                case BT_STATUS_NO_RESOURCES:
                    HCI3W(rxAwaitingHCIBuffer) = TRUE;
                    retVal = HE_PENDING;
                    break;

                default:
                    retVal = HE_HCIGETBUF;
                }
            } else {
                retVal = HE_RX_TRUNCATED_ACL;
            }
            break;

        case PT_SYNCH:
            if (2 < HCI3W(rxBuffUsed)) {

                /* Extract the connection handle from the 
                 * first two bytes of the received data.  
                 */ 
                connHndl = (U16)((HCI3W(hci3wRXBuff)[1] << 8) 
                                | HCI3W(hci3wRXBuff)[0]);

                /* Try to get a buffer from the HCI 
                 * layer to store an SCO message.  
                 */ 
                status 
                    = HCI_GetScoBuffer(&HCI3W(rxBuff), 
                                        connHndl, 
                                        HCI3W(rxBuffUsed));

                switch(status) {
                case BT_STATUS_SUCCESS:
                    HCI3W(h3wMgrState) = HS_GIVE_RX_TO_HCI;
                    retVal = HE_FINISHED_SUCCESSFULLY;
                    break;

                case BT_STATUS_NO_RESOURCES:
                    HCI3W(rxAwaitingHCIBuffer) = TRUE;
                    retVal = HE_PENDING;
                    break;

                default:
                    retVal = HE_HCIGETBUF;
                }
            } else {
                retVal = HE_RX_TRUNCATED_SYNCH;
            }
            break;

        default:
            retVal = HE_RX_BAD_PACKET_TYPE;

        }
        if (HE_FINISHED_SUCCESSFULLY != retVal) {
            HCI3W(h3wMgrState) = HS_ACTIVE;
        }
    } else {
        retVal = HE_NO_BUFFER_NEEDED;
    }
    return(retVal);
}



/*---------------------------------------------------------------------------
 * h3wTrnMngGiveRXBuff()
 *     This function invokes the upper HCI layer routine that give the upper 
 *     layer ownership of a received data buffer.
 * 
 * Returns:
 *     This function returns an HCI 3-wire status enumeration value indicating 
 *     things such as whether the operation succeeded, whether the operation 
 *     is currently pending, or whether the operation encountered some type of 
 *     error.  
 */
static H3WMGR_ERROR_ENUM h3wTrnMngGiveRXBuff(void)
{
    H3WMGR_ERROR_ENUM retVal;
    U16               ndx;

    if (HS_GIVE_RX_TO_HCI == HCI3W(h3wMgrState)) {

        /* Copy the received data into the 
         * buffer obtained from the HCI layer.  
         */ 
        for (ndx = 0; HCI3W(rxBuffUsed) > ndx; ndx += 1) {
            *(HCI3W(rxBuff)->buffer + ndx) = HCI3W(hci3wRXBuff)[ndx];
        }

        /* Give control of the receive 
         * buffer back to the HCI layer. 
         */ 
        HCI_RxBufferInd(HCI3W(rxBuff), BT_STATUS_SUCCESS);

        HCI3W(h3wMgrState) = HS_ACTIVE;
        retVal = HE_FINISHED_SUCCESSFULLY;
    } else {
        retVal = HE_NOT_RETURNING_BUFF;
    }
    return(retVal);
}



/*---------------------------------------------------------------------------
 * h3wTrnMngTXHdrInfo()
 *     This function extracts the header information from a given HCI 
 *     transmission packet.  
 * 
 * Parameters:
 *     sendReliablyP - This parameter must point to a location that can be set 
 *         to TRUE if the data in the HCI transmission packet must be sent 
 *         reliably, or FALSE otherwise.  
 *     buffTypeP - This parameter must point to a location that can be set to 
 *         the HCI buffer flags.  
 *     packetTypeP - This parameter must point to a location that can be set 
 *         to an enumeration corresponding to the packet type.  
 *     buffP - This parameter must point to a location that can be set to the 
 *         address of a transmission buffer.  
 *     buffLenP - This parameter must point to a location that can be set to 
 *         the length of the transmission buffer.  
 *     txPacket - This parameter must contain the address of the HCI 
 *         transmission packet from which the header data is to be extracted.  
 */
static void h3wTrnMngTXHdrInfo(BOOL             *sendReliablyP,
                               HciBufFlags      *buffTypeP,
                               PACKET_TYPE_ENUM *packetTypeP,
                               U8              **buffP, 
                               U16              *buffLenP,
                               HciPacket        *txPacket)
{
    *buffP         = &txPacket->header[0];
    *buffLenP      =  3;
    *sendReliablyP =  TRUE;
    *buffTypeP     =  (HciBufFlags)( txPacket->flags 
                                   & ( HCI_BUFTYPE_COMMAND
                                     | HCI_BUFTYPE_EVENT
                                     | HCI_BUFTYPE_ACL_DATA
                                     | HCI_BUFTYPE_SCO_DATA));
    *packetTypeP   = PT_CMD;
    switch(*buffTypeP) {
    case HCI_BUFTYPE_COMMAND:
        break;

    case HCI_BUFTYPE_ACL_DATA:
        *packetTypeP = PT_ACL;
        *buffLenP = 4;
        break;

    case HCI_BUFTYPE_SCO_DATA:
        *packetTypeP = PT_SYNCH;
        if (TRUE != HCI3W(hciSynchronousFlowControl)) {

            /* The HCI 3-wire specification requires that 
             * synchronous data always be sent unreliably unless 
             * the hci synchronous flow control is enabled.  
             */ 
            *sendReliablyP = FALSE;

        }
        break;

    case HCI_BUFTYPE_EVENT:
    default:
        Assert(0);
    }
}



/*---------------------------------------------------------------------------
 * h3wTrnMngTXAssemble()
 *     This function assembles a complete HCI 3-wire message out of the header 
 *     and fragment data kept in the given HCI transmission packet.  
 * 
 * Parameters:
 *     buffP - This parameter must point to a location that can be set to the 
 *         address of the header or fragment transmission buffers.  
 *     buffLenP - This parameter must point to a location that can be set to 
 *         the length of the header or fragment transmission buffers.  
 *     txPacket - This parameter must contain the address of the HCI 
 *         transmission packet from which the header and fragment buffer data 
 *         is to be extracted.  
 *
 * Returns:
 *     This function returns TRUE if there was enough room in the buffer to 
 *     assemble the header and fragment data, or FALSE otherwise.  
 */
static BOOL h3wTrnMngTXAssemble(U8       **buffP, 
                                U16       *buffLenP,
                                HciPacket *txPacket)
{
    BOOL assemblingHeaderAndFragments;
    BOOL copyingPiece;
    BOOL retVal;
    U8   frgNdx;
    U16  srcNdx;

    Assert(MAX_HCI_FRAGMENTS >= txPacket->fCount);
    retVal            = TRUE;
    HCI3W(txBuffUsed) = 0;
    frgNdx            = 0;
    do {
        assemblingHeaderAndFragments = FALSE;
        srcNdx = 0;
        do {
            copyingPiece = FALSE;
            if (srcNdx < *buffLenP) {
                if (HCI3W_MAX_PAYLOAD > HCI3W(txBuffUsed)) {
                    HCI3W(hci3wTXBuff)[HCI3W(txBuffUsed)] 
                        = *(*buffP + srcNdx);
                    HCI3W(txBuffUsed) += 1;
                    srcNdx            += 1;
                    copyingPiece = TRUE;
                } else {
                    retVal = FALSE;
                }
            }
        } while (TRUE == copyingPiece);
        if (TRUE == retVal) {
            if (frgNdx < txPacket->fCount) {
                *buffLenP = txPacket->f[frgNdx].len;
                *buffP    = (U8 *)txPacket->f[frgNdx].buffer;
                frgNdx   += 1;
                assemblingHeaderAndFragments = TRUE;
            }
        }
    } while (TRUE == assemblingHeaderAndFragments);
    return(retVal);
}



/*---------------------------------------------------------------------------
 * h3wTrnMngTXPushTBA()
 *     This routine pushes a transmission message onto the To-Be-Acknowledged 
 *     queue.  
 * 
 * Parameters:
 *     aNdxP - This parameter must point to a location where the index to the 
 *         last To-Be_Acknowledged node can be written.  
 *     fNdxP - This parameter must point to a location where the index to the 
 *         free node that was just moved to the To-Be-Acknowledged queue can 
 *         be written.  
 *     pNdxP - This parameter must point to a location where the index to the 
 *         parent of the free node that was just moved to the 
 *         To-Be-Acknowledged queue can be written.  
 *     fPtrP - This parameter must point to a location where the address of 
 *         the node justed moved to the To-Be-Acknowledged queue can be 
 *         written.  
 *     txPacket - This parameter must contain the address of the HCI 
 *         transmission packet that needs acknowledgement.  
 */
static void h3wTrnMngTXPushTBA(U8            *aNdxP, 
                               U8            *fNdxP, 
                               U8            *pNdxP, 
                               TxNodeStruct **fPtrP,
                               HciPacket     *txPacket)
{
    BOOL searchingFree;
    BOOL searchingTBA;

    /* Find the last free node */ 
    *pNdxP = 0xFF;
    *fNdxP = HCI3W(txRootFree);
    Assert(HCI3W_TXQ_MAX > *fNdxP);
    do {
        searchingFree = FALSE;
        if (HCI3W_TXQ_MAX > HCI3W(txQ)[*fNdxP].cNdx) {
            *pNdxP        = *fNdxP;
            *fNdxP        = HCI3W(txQ)[*pNdxP].cNdx;
            searchingFree = TRUE;
        }
    } while (TRUE == searchingFree);
    *fPtrP = &HCI3W(txQ)[*fNdxP];

    /* Remove the last free node from the free list */ 
    if (HCI3W_TXQ_MAX > *pNdxP) {
        HCI3W(txQ)[*pNdxP].cNdx = 0xFF;
    } else {
        HCI3W(txRootFree) = 0xFF;
    }

    /* Find the last node in the To Be Acknowledged list */ 
    *aNdxP = HCI3W(txRootToBeAcknowledged);
    if ((HCI3W_TXQ_MAX > *aNdxP)) {
        do {
            searchingTBA = FALSE;
            if (HCI3W_TXQ_MAX > HCI3W(txQ)[*aNdxP].cNdx) {
                *aNdxP       = HCI3W(txQ)[*aNdxP].cNdx;
                searchingTBA = TRUE;
            } else {

                /* The last node in the To Be Acknowledged list has 
                 * been found.  Insert the node just removed from the 
                 * free list at the end of the To Be Acknowledged list.  
                 */ 
                HCI3W(txQ)[*aNdxP].cNdx = *fNdxP;
                (*fPtrP)->pNdx          = *aNdxP;

            }
        } while (TRUE == searchingTBA);
    } else {

        /* The To Be Acknowledged list is empty, so 
         * make the node just removed from the free list 
         * be the root of the To Be Acknowledged list.  
         */ 
        (*fPtrP)->pNdx = 0xFF;
        HCI3W(txRootToBeAcknowledged) = *fNdxP;

    }

    (*fPtrP)->hciPacket = txPacket;
}



/*---------------------------------------------------------------------------
 * h3wTrnMngTXDone()
 *     This function invokes the upper HCI layer routine that indicates that a 
 *     message has been successfully sent.  It then signals that the HCI 
 *     3-wire transport manager should attempt a transmit if the transmit 
 *     pending queue is not empty.  
 * 
 * Parameters:
 *     reliable - This parameter must be set to TRUE if the message was 
 *         transmitted reliably, or FALSE otherwise.  
 *     txPacket - This parameter must point to the HCI transmission packet 
 *         that was just successfully transmitted, and which the upper HCI 
 *         layer may now reuse.  
 */
static void h3wTrnMngTXDone(BOOL reliable, HciPacket *txPacket)
{
    DATA_UNION data;

    data.hciPacket = (HciPacket *)0;
    HCI_PacketSent(txPacket);
    if (TRUE == reliable) {
        HCI3W(slidingWindowUsed) -= 1;
    }
    if (!IsListEmpty(&HCI3W(txQPending))) {
        h3wTrnManager(HR_TX, data);
    }
}


/*---------------------------------------------------------------------------
 * h3wTrnMngTX()
 *     This function gets a pointer to the first node in the pending 
 *     transmission queue, extracts the header information from the HCI 
 *     transmission packet, assembles the header and fragments into a single 
 *     message, puts reliable messages into the To-Be-Acknowledged queue, and 
 *     then invokes the lower level HCI 3-wire transport routines that 
 *     transmit data.  
 * 
 * Returns:
 *     This function returns an HCI 3-wire status enumeration value indicating 
 *     things such as whether the operation succeeded, whether the operation 
 *     is currently pending, or whether the operation encountered some type of 
 *     error.  
 */
static H3WMGR_ERROR_ENUM h3wTrnMngTX(void)
{
    BOOL              sendReliably;
    DATA_UNION        data;
    H3WMGR_ERROR_ENUM retVal;
    HciBufFlags       buffType;
    HciPacket        *txPacket;
    PACKET_TYPE_ENUM  packetType;
    TxNodeStruct     *fPtr;
    U8                aNdx;
    U8               *buff;
    U8                fNdx;
    U8                pNdx;
    U16               buffLen;
    U32               h3Ret;

    data.hciPacket = (HciPacket *)0;
    Assert(HS_ACTIVE == HCI3W(h3wMgrState));
    retVal = HE_PENDING;
    if (HCI3W_SLIDING_WINDOW > HCI3W(slidingWindowUsed)) {

        /* Get a pointer to the first node 
         * in the pending transmission queue.  
         */ 
        Assert(!IsListEmpty(&HCI3W(txQPending)));
        txPacket = (HciPacket *)HCI3W(txQPending).Flink;

        retVal = HE_FINISHED_SUCCESSFULLY;

        /* Extract the HCI header information.  */ 
        h3wTrnMngTXHdrInfo(&sendReliably, 
                           &buffType, 
                           &packetType, 
                           &buff, 
                           &buffLen, 
                            txPacket);

        /* If you can't assemble the header and fragments into 
         * a single message, then abort the transmission.  
         */ 
        if (TRUE == h3wTrnMngTXAssemble(&buff, &buffLen, txPacket)) {

            if (TRUE == sendReliably) {

                /* Put the message in the To 
                 * Be Acknowledged queue.  
                 */ 
                h3wTrnMngTXPushTBA(&aNdx, 
                                   &fNdx, 
                                   &pNdx, 
                                   &fPtr, 
                                    txPacket);

                HCI3W(slidingWindowUsed) += 1;
                h3Ret = h3wSendReliably(&HCI3W(hci3wObj),
                                        &fPtr->hci3wHandle,
                                         HCI3W(hci3wTXBuff),
                                         HCI3W(txBuffUsed),
                                         packetType,
                                         HCI3W(configured).crc);
                if (0 == h3Ret) {
                    RemoveHeadList(&HCI3W(txQPending));
                } else {
                    HCI3W(slidingWindowUsed) -= 1;

                    /* We were unable to send the data, so remove 
                     * the node from the to be acknowledged list.
                     */ 
                    if (HCI3W_TXQ_MAX > aNdx) {
                        HCI3W(txQ)[aNdx].cNdx = 0xFF;
                    } else {
                        HCI3W(txRootToBeAcknowledged) = 0xFF;
                    }
                    if (HCI3W_TXQ_MAX > pNdx) {
                        HCI3W(txQ)[pNdx].cNdx = fNdx;
                        fPtr->pNdx = pNdx;
                    } else {
                        HCI3W(txRootFree) = fNdx;
                        fPtr->pNdx = 0xFF;
                    }

                }
            } else {
                h3Ret = h3wSendUnreliably(&HCI3W(hci3wObj),
                                           HCI3W(hci3wTXBuff),
                                           HCI3W(txBuffUsed),
                                           packetType,
                                           HCI3W(configured).crc);
                RemoveHeadList(&HCI3W(txQPending));

                /* Let the HCI layer know that the 
                 * unreliable HCI package was sent.  
                 */ 
                h3wTrnMngTXDone(FALSE, txPacket);

            }
            if (0 < h3Ret) {
                if (ER_SLIDING_WINDOW_FULL == (h3Ret & 0x0FF)) {
                    data.hciPacket = (HciPacket *)0;
                    h3wTrnReqAction(HR_WAKE, data);
                }
                retVal = HE_TX_FAILURE;
            }
        } else {
            retVal = HE_TX_PAYLOAD_TOO_LARGE;
            if (TRUE != sendReliably) {

                /* Let the HCI layer know that the code is 
                 * through with the unreliable HCI package.  
                 */ 
                h3wTrnMngTXDone(FALSE, txPacket);

            }
        }
    }
    return(retVal);
}



/*---------------------------------------------------------------------------
 * h3wTrnMngTXFreeTBANode()
 *     This function removes a newly acknowledged message from the 
 *     To-Be-Acknowledged queue.  
 * 
 * Parameters:
 *     pNdx - The index to the parent of the node that was just acknowledged, 
 *         if any.  
 *     aNdx - The index to the node that was just acknowledged.  
 */
static void h3wTrnMngTXFreeTBANode(U8 pNdx, U8 aNdx)
{
    BOOL searchingFree;
    U8   cNdx;
    U8   fNdx;

    cNdx = HCI3W(txQ)[aNdx].cNdx;

    /* Check if the free list is empty */ 
    fNdx = HCI3W(txRootFree);
    if (HCI3W_TXQ_MAX > fNdx) {

        /* Find the last free node */ 
        do {
            searchingFree = FALSE;
            if (HCI3W_TXQ_MAX > HCI3W(txQ)[fNdx].cNdx) {
                fNdx = HCI3W(txQ)[fNdx].cNdx;
                searchingFree = TRUE;
            }
        } while (TRUE == searchingFree);

        /* Link the newly acknowledged node 
         * to the end of the free list. 
         */ 
        HCI3W(txQ)[fNdx].cNdx = aNdx;
        HCI3W(txQ)[aNdx].pNdx = fNdx;

    } else {

        /* Make the newly acknowledged node be the 
         * root of the previously empty free list.  
         */ 
        HCI3W(txRootFree) = aNdx;
        HCI3W(txQ)[aNdx].pNdx = 0xFF;

    }

    /* Since the newly acknowledged node is 
     * now at the very end of the free list, 
     * it must have no associated child node.  
     */ 
    HCI3W(txQ)[aNdx].cNdx = 0xFF;

    /* Unlink the To Be Acknowledged links */ 
    if (HCI3W_TXQ_MAX > pNdx) {
        if (HCI3W_TXQ_MAX > cNdx) {
            HCI3W(txQ)[pNdx].cNdx = cNdx;
            HCI3W(txQ)[cNdx].cNdx = pNdx;
        } else {
            HCI3W(txQ)[pNdx].cNdx = 0xFF;
        }
    } else {
        if (HCI3W_TXQ_MAX > cNdx) {
            HCI3W(txRootToBeAcknowledged) = cNdx;
            HCI3W(txQ)[cNdx].pNdx = 0xFF;
        } else {
            HCI3W(txRootToBeAcknowledged) = 0xFF;
        }
    }

    /* Let the HCI layer know that the reliable 
     * HCI package was successfully sent.  
     */ 
    h3wTrnMngTXDone(TRUE, HCI3W(txQ)[aNdx].hciPacket);

}



/*---------------------------------------------------------------------------
 * h3wTrnMngTXFree()
 *     This function locates the node in the To-Be-Acknowledged queue 
 *     corresponding to the given HCI 3-wire handle, and moves it to the free 
 *     queue.  
 * 
 * Parameters:
 *     hci3wHandle - The HCI 3-wire handle corresponding to the message that 
 *         was just successfully acknowledged.  
 *
 * Returns:
 *     This function returns an HCI 3-wire status enumeration value indicating 
 *     things such as whether the operation succeeded, whether the operation 
 *     is currently pending, or whether the operation encountered some type of 
 *     error.  
 */
static H3WMGR_ERROR_ENUM h3wTrnMngTXFree(U8 hci3wHandle)
{
    BOOL              searchingTBA;
    H3WMGR_ERROR_ENUM retVal;
    TxNodeStruct     *aPtr;
    U8                aNdx;
    U8                cnt;
    U8                pNdx;

    retVal = HE_FINISHED_SUCCESSFULLY;
    if (HS_ACTIVE == HCI3W(h3wMgrState)) {
        aNdx = HCI3W(txRootToBeAcknowledged);
        pNdx = 0xFF;
        cnt  = 0;
        do {
            searchingTBA = FALSE;
            if (HCI3W_TXQ_MAX > aNdx) {
                aPtr = &HCI3W(txQ)[aNdx];
                if (hci3wHandle == aPtr->hci3wHandle) {
                    h3wTrnMngTXFreeTBANode(pNdx, aNdx);
                } else {

                    /* This To Be Acknowledged list node does not 
                     * have a matching handle, so set up the loop to 
                     * test the child node on the next iteration.  
                     */ 
                    pNdx = aNdx;
                    aNdx = HCI3W(txQ)[pNdx].cNdx;
                    searchingTBA = TRUE;

                }
            } else {
                retVal = HE_TX_ACK_NOT_FOUND;
            }
            Assert(HCI3W_TXQ_MAX > cnt);
            cnt += 1;
        } while (TRUE == searchingTBA);
    } else {
        retVal = HE_NOT_ACTIVE;
    }
    return(retVal);
}



/*---------------------------------------------------------------------------
 * h3wTrnMngEnd()
 *     This function invokes the routine that frees the HCI 3-wire layer.  
 * 
 * Returns:
 *     This function returns an HCI 3-wire status enumeration value indicating 
 *     things such as whether the operation succeeded, whether the operation 
 *     is currently pending, or whether the operation encountered some type of 
 *     error.  
 */
static H3WMGR_ERROR_ENUM h3wTrnMngEnd(void)
{
    DATA_FREE_ENUM    h3wDataFreeState;
    H3WMGR_ERROR_ENUM retVal;
    U32               h3Ret;

    if (HS_ACTIVE == HCI3W(h3wMgrState)) {
        HCI3W(h3wMgrState) = HS_SHUTTING_DOWN;
    }
    if (HS_SHUTTING_DOWN == HCI3W(h3wMgrState)) {
        h3Ret = h3wDataFree(&HCI3W(hci3wObj), &h3wDataFreeState);
        if (0 == h3Ret) {
            if (DF_FREE == h3wDataFreeState) {
                HCI3W(h3wMgrState) = HS_INITIALIZED;
                retVal = HE_FINISHED_SUCCESSFULLY;
            } else {
                retVal = HE_PENDING;
            }
        } else {
            HCI3W(h3wMgrState) = HS_ACTIVE;
            retVal = HE_SHUTDOWN_FAILURE;
        }
    } else {
        retVal = HE_NOT_SHUTTING_DOWN;
    }
    return(retVal);
}



/*---------------------------------------------------------------------------
 * h3wTrnMngKill()
 *     This function invokes the routine that destroys the HCI 3-wire layer.  
 * 
 * Returns:
 *     This function returns an HCI 3-wire status enumeration value indicating 
 *     things such as whether the operation succeeded, whether the operation 
 *     is currently pending, or whether the operation encountered some type of 
 *     error.  
 */
static H3WMGR_ERROR_ENUM h3wTrnMngKill(void)
{
    H3WMGR_ERROR_ENUM retVal;

    if (HS_INITIALIZED == HCI3W(h3wMgrState)) {
        h3wTerm(&HCI3W(hci3wObj));
        HCI3W(h3wMgrState) = HS_UNINITIALIZED;
        retVal = HE_FINISHED_SUCCESSFULLY;
    } else {
        retVal = HE_NOT_SHUTDOWN;
    }
    return(retVal);
}



/*---------------------------------------------------------------------------
 * h3wTrnManager()
 *     This function pushes the given request onto the action queue.  Then, 
 *     if the busy flag is clear, it begins looping on the action queue until 
 *     it is empty, popping the oldest requests from the action queue and 
 *     processing the request.  
 * 
 * Parameters:
 *     h3wmgrRequest - The new request that is to be pushed on the action 
 *         queue.  
 *     data - The data corresponding to the new request, if any.  
 *
 * Returns:
 *     This function returns an HCI 3-wire status enumeration value indicating 
 *     things such as whether the operation succeeded, whether the operation 
 *     is currently pending, or whether the operation encountered some type of 
 *     error.  
 */
static H3WMGR_ERROR_ENUM h3wTrnManager(H3WMGR_REQ_ENUM h3wmgrRequest, 
                                       DATA_UNION      data)
{
    ActionStruct     *actionP;
    BOOL              clearBusy;
    BOOL              doShutdown;
    BOOL              doWakeup;
    H3WMGR_ERROR_ENUM currRetVal;
    H3WMGR_ERROR_ENUM retVal;
    U8                hci3wHandle;
    U8                retNdx;

    HCI3W(h3wmgrBusy) += 1;
    if (1 < HCI3W(h3wmgrBusy)) {
        HCI3W(h3wmgrBusy) = 1;
        clearBusy = FALSE;
        retVal    = HE_BUSY;
    } else {
        clearBusy = TRUE;
        retVal = HE_FINISHED_SUCCESSFULLY;
    }
    retNdx = h3wTrnReqAction(h3wmgrRequest, data);
    if (TRUE == clearBusy) {
        currRetVal = HE_FINISHED_SUCCESSFULLY;
        while (HCI3W(aqwNdx) != HCI3W(aqrNdx)) {
            actionP = &HCI3W(actionQ)[HCI3W(aqrNdx)];
            switch(actionP->req) {
            case HR_INITIALIZE_DONE:
                currRetVal = h3wTrnMngInitDone();
                break;

            case HR_LINK_UNEXPECTED:
                HCI3W(h3wMgrState) = HS_INITIALIZED;
                currRetVal = h3wTrnMngLnk();
                break;

            case HR_WAKE:
                currRetVal = h3wTrnMngWake(FALSE, HR_WAKE, actionP->data);
                break;

            case HR_RX:
                currRetVal = HE_FINISHED_SUCCESSFULLY;
                if (TRUE == HCI3W(cntrlrAsleep)) {
                    currRetVal = h3wTrnMngWake(TRUE, HR_RX, actionP->data);
                }
                if (HE_FINISHED_SUCCESSFULLY == currRetVal) {
                    if (HS_ACTIVE == HCI3W(h3wMgrState)) {
                        currRetVal = h3wTrnMngRX();
                        if (HE_FINISHED_SUCCESSFULLY == currRetVal) {
                            if (HS_NEED_HCI_RX_BUFF == HCI3W(h3wMgrState)) {
                                currRetVal = h3wTrnMngGetRXBuff();
                                if (HE_FINISHED_SUCCESSFULLY == currRetVal) {
                                    if (  HS_GIVE_RX_TO_HCI 
                                       == HCI3W(h3wMgrState)) {
                                        currRetVal = h3wTrnMngGiveRXBuff();
                                    }
                                }
                            }
                        }
                    }
                }
                break;

            case HR_HCI_RX_BUFF_AVAIL:
                HCI3W(rxAwaitingHCIBuffer) = FALSE;
                HCI3W(h3wMgrState) = HS_NEED_HCI_RX_BUFF;
                currRetVal = h3wTrnMngGetRXBuff();
                if (HE_FINISHED_SUCCESSFULLY == currRetVal) {
                    if (HS_GIVE_RX_TO_HCI == HCI3W(h3wMgrState)) {
                        currRetVal = h3wTrnMngGiveRXBuff();
                    }
                }
                break;

            case HR_TX:
                currRetVal = HE_FINISHED_SUCCESSFULLY;
                if (TRUE == HCI3W(cntrlrAsleep)) {
                    currRetVal = h3wTrnMngWake(TRUE, HR_TX, actionP->data);
                }
                if (HE_FINISHED_SUCCESSFULLY == currRetVal) {
                    if (HS_ACTIVE == HCI3W(h3wMgrState)) {
                        currRetVal = h3wTrnMngTX();
                    }
                }
                break;

            case HR_TX_FREE:
                hci3wHandle = actionP->data.hci3wHandle;
                currRetVal = h3wTrnMngTXFree(hci3wHandle);
                break;

            case HR_TERMINATE:
                doWakeup   = FALSE;
                doShutdown = FALSE;
                switch(HCI3W(h3wMgrState)) {
                case HS_WAKING:
                    doWakeup = TRUE;
                    break;

                case HS_SHUTTING_DOWN:
                    doShutdown = TRUE;
                    break;

                default:
                    if (TRUE == HCI3W(cntrlrAsleep)) {
                        doWakeup = TRUE;
                    } else {
                        doShutdown = TRUE;
                    }
                }
                if (TRUE == doWakeup) {
                    currRetVal 
                        = h3wTrnMngWake(TRUE, HR_TERMINATE, actionP->data);
                    if (HE_FINISHED_SUCCESSFULLY == currRetVal) {
                        if (HS_ACTIVE == HCI3W(h3wMgrState)) {
                            doShutdown = TRUE;
                        }
                    }
                }
                if (TRUE == doShutdown) {
                    currRetVal = h3wTrnMngEnd();
                    if (HE_FINISHED_SUCCESSFULLY == currRetVal) {
                        if (HS_INITIALIZED == HCI3W(h3wMgrState)) {
                            currRetVal = h3wTrnMngKill();
                        }
                    }
                }
                break;

            case HR_TERMINATE_DONE:
                if (HS_INITIALIZED == HCI3W(h3wMgrState)) {
                    currRetVal = h3wTrnMngKill();
                    if (HE_FINISHED_SUCCESSFULLY == currRetVal) {

                        /* To avoid a deadlock due to multiple threads trying 
                         * to lock the stack and the hardware at the same 
                         * time, don't invoke the HCI callback directly.  
                         * Instead, let a timer invoke the callback, since 
                         * the timer will run in a separate thread.   
                         */ 
                        HCI3W(signalHCI).context 
                            = (void *)TRAN_SHUTDOWN_STATUS;
                        EVM_StartTimer(&HCI3W(signalHCI), 0);

                    }
                }
                break;

            case HR_INITIALIZE:
            default:
                currRetVal = HE_FINISHED_SUCCESSFULLY;
                if (HS_LINKING != HCI3W(h3wMgrState)) {
                    currRetVal = h3wTrnMngInit(HCI3W_SLIDING_WINDOW);
                    if (HE_FINISHED_SUCCESSFULLY == currRetVal) {
                        currRetVal = h3wTrnMngPld();
                    }
                    if (HE_FINISHED_SUCCESSFULLY == currRetVal) {
                        currRetVal = h3wTrnMngUARTMult(HCI3W_UART_MULTIPLIER);
                    }
                }
                if (HE_FINISHED_SUCCESSFULLY == currRetVal) {
                    currRetVal = h3wTrnMngLnk();
                }
            }
            if (retNdx == HCI3W(aqrNdx)) {
                retVal = currRetVal;
            }
            if (ACTION_Q_MAX > (HCI3W(aqrNdx) + 1)) {
                HCI3W(aqrNdx) += 1;
            } else {
                HCI3W(aqrNdx)  = 0;
            }
        }
        HCI3W(h3wmgrBusy) = 0;
    }
    return(retVal);
}
