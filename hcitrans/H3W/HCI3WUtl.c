/****************************************************************************
 *
 * File:
 *     $Workfile:HCI3WUtl.c$ for iAnywhere Blue SDK, Version 2.2
 *     $Revision:18$
 *
 * Description: This file is part of an HCI 3-Wire implementation.  It 
 * contains utility function used by multiple HCI 3-wire files, including the 
 * shared data code, the CRC code, the event handling code, and the error 
 * handling code.  
 *
 * Created:     October 31, 2006
 *
 * Copyright 2006-2007 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions,  
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "sys/HCI3WUtl.h"

/****************************************************************************
 *
 * Prototypes
 *
 ****************************************************************************/

/****************************************************************************
 *
 * Public Functions
 *
 ****************************************************************************/

/* ####################################################################### */ 
/* ###### The following functions are part of the utility module.  ####### */ 
/* ####################################################################### */ 



/*---------------------------------------------------------------------------
 * h3wUtlInit()
 *     Initializes the HCI 3-wire utilities module.  This function must be 
 *     invoked before any of the other HCI 3-wire utility functions will 
 *     work correctly.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire utilities module context structure.  
 *     allowCRC - This parameter indicates whether the HCI 3-wire layer 
 *         wants to support cyclic redundancy check calculations.  Whether 
 *         CRCs actually end up being used depends on whether the other side 
 *         also supports them.
 *     hciSynchFC - This parameter must be set to TRUE if HCI Flow Control is 
 *         in use.  
 *     role - Indicates whether the code is being run by an HCI 3-Wire Host 
 *         or by an HCI 3-Wire Controller.
 */
void h3wUtlInit(Hci3WUtlStruct  *cP,
                BOOL             allowCRC,
                BOOL             hciSynchFC,
                HCI3W_ROLE_ENUM  role,
                SLIDING_WIN_ENUM slidingWindow)
{
    TimeT zeroTicks;
    TimeT maxTicks;

    zeroTicks                   = 0;
    maxTicks                    = ~zeroTicks;
    cP->halfWrap                = (maxTicks / 2) + 1;
    cP->afterIni                = FALSE;
    cP->privMaxPayloadBytes     = 4095;
    cP->privUARTBPS             = 115200;
    cP->privMayUseCRC           = allowCRC;
    h3wUtlSetSlidingWindow(cP, slidingWindow);

    /* Keep track of whether the HCI Synchronous Flow Control is enabled. */ 
    cP->flowControl[FL_HCI_SYNCHRONOUS][FT_CHOKE_ABILITY] = TRUE;
    cP->flowControl[FL_HCI_SYNCHRONOUS][FT_CURRENT_STATE] = hciSynchFC;

    /* Disallow the out of frame software flow control until a 
     * link configuration field is received that enables it.  
     */ 
    cP->flowControl[FL_OUT_OF_FRAME_SOFTWARE][FT_CHOKE_ABILITY] = FALSE;
    cP->flowControl[FL_OUT_OF_FRAME_SOFTWARE][FT_CURRENT_STATE] = TRUE;

    cP->privRole                = role;
    cP->privOtherAssumedAwake   = TRUE;
    h3wUtlSetLastRXAck(cP, FALSE, SE_0);
    cP->privUARTClockMultiplier = 1;
}



/*---------------------------------------------------------------------------
 * h3wUtlCopyBytes()
 *     Given a buffer pointers structure, this function copies bytes from the 
 *     source to the destination.
 * 
 * Parameters:
 *     bpP - Pointer to buffer pointers structure containing the source and 
 *         destination addresses.
 *     size - The number of bytes to copy
 */
void h3wUtlCopyBytes(BuffPtrsStruct *bpP, U32 size)
{
    U32 ndx;

    for (ndx = 0; ndx < size; ndx += 1) {
        *(bpP->dst) = *(bpP->src);
        bpP->dst = bpP->dst + 1;
        bpP->src = bpP->src + 1;
    }
}



/*---------------------------------------------------------------------------
 * h3wUtlElapsed()
 *     Determines the number of clock ticks that have elapsed since a given 
 *     starting time.  This function correctly handles clock wraps.
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire utilities module context structure.  
 *     startTicks - The starting time from which the elapsed time is to be 
 *         determined.
 *
 * Returns:
 *     The number of clock ticks that have elapsed since the given starting 
 *     time..
 */
TimeT h3wUtlElapsed(Hci3WUtlStruct *cP, TimeT startTicks)
{
    BOOL  doOffset;
    TimeT currTicks;
    TimeT retVal;

    doOffset = FALSE;
    if (cP->halfWrap < startTicks) {
        startTicks -= cP->halfWrap;
        doOffset = TRUE;
    }
    currTicks = OS_GetSystemTime();
    if (cP->halfWrap < currTicks) {
        currTicks -= cP->halfWrap;
        if (TRUE == doOffset) {
            doOffset = FALSE;
        } else {
            doOffset = TRUE;
        }
    }
    if (startTicks > currTicks) {
        currTicks += cP->halfWrap;
        if (TRUE == doOffset) {
            doOffset = FALSE;
        } else {
            doOffset = TRUE;
        }
    }
    retVal = currTicks - startTicks;
    if (TRUE == doOffset) {
        Assert(retVal < cP->halfWrap);
        retVal += cP->halfWrap;
    }
    return(retVal);
}



/*---------------------------------------------------------------------------
 * h3wUtlSetMaxPayloadBytes()
 *     Sets the maximum number of payload bytes that can be transmitted in an 
 *     HCI 3-wire message before framing and slip encoding.
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire utilities module context structure.  
 *     maxPayloadBytes - The new value to be assigned to the maximum number of 
 *         payload bytes.
 *     errObjP - A pointer to an error context structure.
 *
 * Returns:
 *     This function returns TRUE if the maximum number of payload bytes were 
 *     successfully redefined, or FALSE otherwise.
 */
BOOL h3wUtlSetMaxPayloadBytes(Hci3WUtlStruct *cP, 
                              U16             maxPayloadBytes,
                              Hci3WErrStruct *errObjP)
{
    if (FALSE == cP->afterIni) {
        if (0 < maxPayloadBytes) {
            if (4096 > maxPayloadBytes) {
                cP->privMaxPayloadBytes = maxPayloadBytes;
            } else {
                h3wErrSet(errObjP, ES_UTL_SETPLD, ER_PAYLOAD_TOO_LARGE);
            }
        } else {
            h3wErrSet(errObjP, ES_UTL_SETPLD, ER_PAYLOAD_TOO_SMALL);
        }
    } else {
        h3wErrSet(errObjP, ES_UTL_SETPLD, ER_PAYLOAD_INI);
    }
    return(h3wErrHasNone(errObjP));
}



/*---------------------------------------------------------------------------
 * h3wUtlGetMaxPayloadBytes()
 *     Provides the maximum number of payload bytes that can be used in an HCI 
 *     3-wire message before framing and slip encoding.
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire utilities module context structure.  
 *
 * Returns:
 *     This function returns the maximum number of payload bytes that can be 
 *     used in an HCI 3-wire message before framing and slip encoding.
 */
U16 h3wUtlGetMaxPayloadBytes(Hci3WUtlStruct *cP)
{
    cP->afterIni = TRUE;
    return(cP->privMaxPayloadBytes);
}



/*---------------------------------------------------------------------------
 * h3wUtlGetTmax()
 *     Determines the value of Tmax.
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire utilities module context structure.  
 *
 * Returns:
 *     This function returns the value of Tmax in milliseconds, which is 
 *     calculated as per the HCI 3-wire specification from the maximum 
 *     payload size and the data transfer rate.  
 */
U32 h3wUtlGetTmax(Hci3WUtlStruct *cP)
{
    return( ((((U32)h3wUtlGetMaxPayloadBytes(cP) + 6) * 2) 
          * UART_BITS_PER_DATA_OCTET) 
          / (cP->privUARTBPS / 1000));
}



/*---------------------------------------------------------------------------
 * h3wUtlGetTimeOneChar()
 *     Determines the time required to transmit a byte of data.
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire utilities module context structure.  
 *
 * Returns:
 *     This function returns the value of Tmax in milliseconds, which is 
 *     normally calculated from the data transfer rate.  However, this 
 *     function never returns a value less than 1.
 */
U32 h3wUtlGetTimeOneChar(Hci3WUtlStruct *cP)
{
    U32 retVal;

    retVal = UART_BITS_PER_DATA_OCTET / (cP->privUARTBPS / 1000);
    if (0 >= retVal) {
        retVal = 1;
    }
    return(retVal);
}



/*---------------------------------------------------------------------------
 * h3wUtlGetUARTBPS()
 *     Determines the bits per second transfer rate.
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire utilities module context structure.  
 *
 * Returns:
 *     This function returns the number of bits that can be transferred per 
 *     second.  Note that this does not correspond to the amount of data that 
 *     can be transferred, becuase transfers include the UART start and stop 
 *     bits, and also include the overhead for HCI 3-wire framing and slip 
 *     encoding.  
 */
U32 h3wUtlGetUARTBPS(Hci3WUtlStruct *cP)
{
    return(cP->privUARTBPS);
}



/*---------------------------------------------------------------------------
 * h3wUtlSetUARTBPSSpeed()
 *     Defines the number of bits the UART can transfer in one second.  Note 
 *     that this does not correspond to the amount of data that can be 
 *     transferred, becuase transfers include the UART start and stop bits, 
 *     and also include the overhead for HCI 3-wire framing and slip encoding.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire utilities module context structure.  
 *     uartBPSSpeed - Number of bytes to copy
 */
void h3wUtlSetUARTBPSSpeed(Hci3WUtlStruct *cP, U32 uartBPSSpeed)
{
    cP->privUARTBPS = uartBPSSpeed;
}



/*---------------------------------------------------------------------------
 * h3wUtlSetUARTMultiplier()
 *     Sets the value of the UART clock multiplier.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire utilities module context structure.  
 *     uartClockMultiplier - An enumeration corresponding to the amount the 
 *         UART speeds are overclocked.  
 */
void h3wUtlSetUARTMultiplier(Hci3WUtlStruct *cP, 
                             OVERCLOCK_ENUM  uartClockMultiplier)
{
    switch(uartClockMultiplier) {
    case OR_2:
        cP->privUARTClockMultiplier = 2;
        break;

    case OR_4:
        cP->privUARTClockMultiplier = 4;
        break;

    case OR_8:
        cP->privUARTClockMultiplier = 8;
        break;

    case OR_1:
    default:
        cP->privUARTClockMultiplier = 1;
    }
}



/*---------------------------------------------------------------------------
 * h3wUtlBPStoUARTEncoding()
 *     Converts a given bits per second transfer rate into an encoding 
 *     expected by the lower UART layer software.
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire utilities module context structure.  
 *     uartSpeedEncodingP - A pointer to a location where the UART speed 
 *         encoding can be written.
 *     bps - The bits per second transfer rate.
 *     errObjP - A pointer to an error context structure.
 *
 * Returns:
 *     This function returns TRUE if the give bits per second rate were 
 *     successfully converted into a UART speed encoding, or FALSE otherwise.  
 */
BOOL h3wUtlBPStoUARTEncoding(Hci3WUtlStruct *cP, 
                             U16            *uartSpeedEncodingP,
                             U32             bps,
                             Hci3WErrStruct *errObjP)
{
    U32 unclockedBPS;

    unclockedBPS = bps / cP->privUARTClockMultiplier;

    /* Note that for six standard values, the encoding is in kilobits per 
     * second.  Otherwise, the encoding is in hundreds of bits per second.  
     * The reason for this seemingly bizarre inconsistency is so that 
     * command line speed parameters that are sent with a decimal place 
     * can be interpreted so as to not lose the fractional portion of the 
     * parameter, while still allowing standard values to be expressed 
     * in kilobits per second.  As a result of these encoding rules, the 
     * following bit per second values cannot be represented:  900-999, 
     * 3800-3899, 5700-5799, 11500-11599, 12800-12899, and 25600-25699.
     */ 

    switch(unclockedBPS) {
    case   9600:
    case  38400:
    case  57600:
    case 115200:
    case 128000:
    case 256000:
        *uartSpeedEncodingP = (U16)(unclockedBPS / 1000);
        break;

    default:
        *uartSpeedEncodingP = (U16)(unclockedBPS / 100);
        switch(*uartSpeedEncodingP) {
        case   9:
        case  38:
        case  57:
        case 115:
        case 128:
        case 256:
            h3wErrSet(errObjP, ES_UTL_BPSENCODE, ER_BPS_UNREPRESENTABLE);
            *uartSpeedEncodingP = 0;
            break;

        default:
            break;
        }
    }
    return(h3wErrHasNone(errObjP));
}



/*---------------------------------------------------------------------------
 * h3wUtlUARTEncodingToBPS()
 *     Converts a given encoding expected by the lower UART layer software 
 *     into an bits per second transfer rate.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire utilities module context structure.  
 *     uartSpeedEncoding - The UART speed encoding that is to be converted 
 *         into a BPS rate.  
 *
 * Returns:
 *     This function returns the bits per second transfer rate derived from 
 *     the given encoding expected by the lower UART layer software.  
 */
U32 h3wUtlUARTEncodingToBPS(Hci3WUtlStruct *cP, U16 uartSpeedEncoding)
{
    U32 retVal;

    switch(uartSpeedEncoding) {
    case   9:
        retVal = 9600;
        break;

    case  38:
        retVal = 38400;
        break;

    case  57:
        retVal = 57600;
        break;

    case 115:
        retVal = 115200;
        break;

    case 128:
        retVal = 128000;
        break;

    case 256:
        retVal = 256000;
        break;

    default:
        retVal = (U32)uartSpeedEncoding * 100;
    }
    retVal *= cP->privUARTClockMultiplier;
    return(retVal);
}



/*---------------------------------------------------------------------------
 * h3wUtlSetMayUseCRC()
 *     Sets the permission to use cyclic redundancy checks in HCI 3-wire 
 *     messages.
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire utilities module context structure.  
 *     mayUseCRC - The new permission value to be assigned.
 */
void h3wUtlSetMayUseCRC(Hci3WUtlStruct *cP, BOOL mayUseCRC)
{
    cP->privMayUseCRC = mayUseCRC;
}



/*---------------------------------------------------------------------------
 * h3wUtlGetMayUseCRC()
 *     Determines whether cyclic redundancy checks can be used in HCI 3-wire 
 *     messages.
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire utilities module context structure.  
 *
 * Returns:
 *     This function returns TRUE if cyclic redundancy checks can be used in 
 *     HCI 3-wire messages, or FALSE otherwise.
 */
BOOL h3wUtlGetMayUseCRC(Hci3WUtlStruct *cP)
{
    return(cP->privMayUseCRC);
}



/*---------------------------------------------------------------------------
 * h3wUtlSetSlidingWindow()
 *     Defines the HCI 3-wire sliding window size.
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire utilities module context structure.  
 *     slidingWindow - An enumeration value corresponding to the new sliding 
 *         window size.
 */
void h3wUtlSetSlidingWindow(Hci3WUtlStruct  *cP, 
                            SLIDING_WIN_ENUM slidingWindow) 
{
    switch(slidingWindow) {
    case SW_LOWER:
        cP->privSlidingWindowSize = 2;
        break;

    case SW_LOW:
        cP->privSlidingWindowSize = 3;
        break;

    case SW_MID:
        cP->privSlidingWindowSize = 4;
        break;

    case SW_HIGH:
        cP->privSlidingWindowSize = 5;
        break;

    case SW_HIGHER:
        cP->privSlidingWindowSize = 6;
        break;

    case SW_HIGHEST:
        cP->privSlidingWindowSize = 7;
        break;

    case SW_LOWEST:
    default:
        cP->privSlidingWindowSize = 1;
    }
}



/*---------------------------------------------------------------------------
 * h3wUtlGetSlidingWindowSize()
 *     Determines the HCI 3-wire sliding window size.
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire utilities module context structure.  
 *
 * Returns:
 *     This function returns the HCI 3-wire sliding window size.  
 */
U8 h3wUtlGetSlidingWindowSize(Hci3WUtlStruct *cP) 
{
    return(cP->privSlidingWindowSize);
}



/*---------------------------------------------------------------------------
 * h3wUtlSetFlowControl()
 *     Sets the flow control value for the given layer and type.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire utilities module context structure.  
 *     layer - An enumeration corresponding either to the HCI flow control or 
 *         to the Out-Of-Frame Software flow control.
 *     type - An enumeration corresponding either to the choke ability or to 
 *         the current state of the flow control.  
 */
void h3wUtlSetFlowControl(Hci3WUtlStruct *cP,
                          FLOW_LAYER_ENUM layer, 
                          FLOW_TYPE_ENUM  type, 
                          BOOL            value)
{
    if (FT_CHOKE_ABILITY == type) {
        if (FALSE == value) {

            /* If flow control chokes are disallowed for this 
             * layer, then current state currently must be flowing.  
             */ 
            Assert(TRUE == cP->flowControl[layer][FT_CURRENT_STATE]);

        }
        cP->flowControl[layer][FT_CHOKE_ABILITY] = value;
    } else { 

        /* FT_CURRENT_STATE == type */ 

        Assert(TRUE == cP->flowControl[layer][FT_CHOKE_ABILITY]);
        if (FALSE == cP->flowControl[layer][FT_CURRENT_STATE]) {
            if (TRUE == value) {
                cP->flowControl[layer][FT_CURRENT_STATE] = TRUE;
                if (FL_OUT_OF_FRAME_SOFTWARE == layer) {
                    Report(("HCI3W:  XON\n"));
                }
            }
        } else {
            if (FALSE == value) {
                cP->flowControl[layer][FT_CURRENT_STATE] = FALSE;
                if (FL_OUT_OF_FRAME_SOFTWARE == layer) {
                    Report(("HCI3W:  XOFF\n"));
                }
            }
        }
    }
}



/*---------------------------------------------------------------------------
 * h3wUtlGetFlowControl()
 *     Determines a flow control value for a given layer and type.
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire utilities module context structure.  
 *     layer - An enumeration corresponding either to the HCI flow control or 
 *         to the Out-Of-Frame Software flow control.
 *     type - An enumeration corresponding either to the choke ability or to 
 *         the current state of the flow control.  
 *
 * Returns:
 *     This function returns TRUE if the value for the given flow control 
 *     layer and type is enabled, or FALSE otherwise.  
 */
BOOL h3wUtlGetFlowControl(Hci3WUtlStruct *cP,
                          FLOW_LAYER_ENUM layer, 
                          FLOW_TYPE_ENUM  type)
{
    if (FT_CURRENT_STATE == type) {
        if (FALSE == cP->flowControl[layer][FT_CHOKE_ABILITY]) {

            /* If flow control chokes are disallowed for this 
             * layer, then current state currently must be flowing.  
             */ 
            Assert(TRUE == cP->flowControl[layer][FT_CURRENT_STATE]);

        }
    }
    return(cP->flowControl[layer][type]);
}



/*---------------------------------------------------------------------------
 * h3wUtlGetRole()
 *     Determines the HCI 3-wire role.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire utilities module context structure.  
 *
 * Returns:
 *     This function returns an enumeration corresponding either to the HCI 
 *     3-wire Host or to the HCI 3-wire Controller, depending on the current 
 *     role.  
 */
HCI3W_ROLE_ENUM h3wUtlGetRole(Hci3WUtlStruct *cP) 
{
    return(cP->privRole);
}



/*---------------------------------------------------------------------------
 * h3wUtlGetOtherAssumedAwake()
 *     This function determines whether that the HCI 3-wire device being 
 *     communicated with is assumed to be awake.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire utilities module context structure.  
 *
 * Returns:
 *     This function returns TRUE if the HCI 3-wire device being communicated 
 *     with is assumed to be awake, or FALSE otherwise.  
 */
BOOL h3wUtlGetOtherAssumedAwake(Hci3WUtlStruct *cP)
{
    return(cP->privOtherAssumedAwake);
}



/*---------------------------------------------------------------------------
 * h3wUtlSetOtherAwake()
 *     Indicates whether the HCI 3-wire device being communicated with is to 
 *     be assumed to be awake or not.
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire utilities module context structure.  
 *     otherAssumedAwake - This parameter must be set to TRUE if the HCI 
 *         3-wire device being communicated with is to be assumed to be awake, 
 *         or FALSE otherwise.
 */
void h3wUtlSetOtherAwake(Hci3WUtlStruct *cP, BOOL otherAssumedAwake)
{
    cP->privOtherAssumedAwake = otherAssumedAwake;
}



/*---------------------------------------------------------------------------
 * h3wUtlSetLastRXAck()
 *     Defines the last acknowledgement number to have been received.
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire utilities module context structure.  
 *     valid - This parameter must be TRUE if an acknowledgement number has 
 *         been received since the last time the HCI 3-wire link was 
 *         established, or FALSE otherwise.  
 *     ackNum - If an acknowledgement number has been received since the last 
 *         time the HCI 3-wire link was established, this parameter must 
 *         contain the last received acknowledgement number.  Otherwise, this 
 *         parameter has no meaning.  
 */
void h3wUtlSetLastRXAck(Hci3WUtlStruct *cP, BOOL valid, SEQ_ENUM ackNum)
{
    cP->lastRX.valid  = valid;
    cP->lastRX.ackNum = ackNum;
}



/*---------------------------------------------------------------------------
 * h3wUtlGetLastRXAck()
 *     Determines the last acknowledgement number received since the last time 
 *     the HCI 3-wire link was established.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire utilities module context structure.  
 *     ackP - This parameter must point to an acknowledgement structure where 
 *         the current acknowledgement data can be written.  
 */
void h3wUtlGetLastRXAck(Hci3WUtlStruct *cP, AckStruct *ackP)
{
    ackP->valid  = cP->lastRX.valid;
    ackP->ackNum = cP->lastRX.ackNum;
}



/*---------------------------------------------------------------------------
 * h3wUtlSeqU8ToE()
 *     Converts a numeric HCI 3-wire sequence number to its corresponding 
 *     enumeration value.
 * 
 * Parameters:
 *     asEnumP - This parameter must point to a sequence enumeration location 
 *         where the converted value can be written.  
 *     asU8 - This parameter must contain a numeric version of an HCI 3-wire 
 *         sequence number.  
 *     errObjP - A pointer to an error context structure.
 *
 * Returns:
 *     This function returns TRUE if it was successfully able to convert the 
 *     given numeric HCI 3-wire sequence number into an enumeration value, or 
 *     FALSE otherwise.  
 */
BOOL h3wUtlSeqU8ToE(SEQ_ENUM       *asEnumP, 
                    U8              asU8, 
                    Hci3WErrStruct *errObjP)
{
    switch(asU8) {
    case 0:
        *asEnumP = SE_0;
        break;

    case 1:
        *asEnumP = SE_1;
        break;

    case 2:
        *asEnumP = SE_2;
        break;

    case 3:
        *asEnumP = SE_3;
        break;

    case 4:
        *asEnumP = SE_4;
        break;

    case 5:
        *asEnumP = SE_5;
        break;

    case 6:
        *asEnumP = SE_6;
        break;

    case 7:
        *asEnumP = SE_7;
        break;

    default:
        h3wErrSet(errObjP, ES_UTL_SEQITOE, ER_OUT_OF_RANGE);
    }
    return(h3wErrHasNone(errObjP));
}


/*---------------------------------------------------------------------------
 * h3wUtlSeqEToU8()
 *     Converts a enumeration corresponding to an HCI 3-wire sequence number 
 *     to a numeric value.  
 * 
 * Parameters:
 *     seqNum - An enumeration corresponding to an HCI 3-wire sequence number.  
 *
 * Returns:
 *     This function returns a numeric HCI 3-wire sequence number 
 *     corresponding to the given enumeration value.  
 */
U8 h3wUtlSeqEToU8(SEQ_ENUM seqNum)
{
    U8 retVal;

    switch(seqNum) {
    case SE_1:
        retVal = 1;
        break;

    case SE_2:
        retVal = 2;
        break;

    case SE_3:
        retVal = 3;
        break;

    case SE_4:
        retVal = 4;
        break;

    case SE_5:
        retVal = 5;
        break;

    case SE_6:
        retVal = 6;
        break;

    case SE_7:
        retVal = 7;
        break;

    case SE_0:
    default:
        retVal = 0;
    }
    return(retVal);
}



/*---------------------------------------------------------------------------
 * h3wUtlGetNextSeq()
 *     Given a current sequence number enumeration, this function determines 
 *     the next sequence number enumeration value.
 * 
 * Parameters:
 *     seqNum - An enumeration corresponding to an HCI 3-wire sequence number.  
 *
 * Returns:
 *     This function returns the next sequence number enumeration value for 
 *     the given sequence number.  
 */
SEQ_ENUM h3wUtlGetNextSeq(SEQ_ENUM seqNum)
{
    SEQ_ENUM retVal;

    switch(seqNum) {
    case SE_0:
        retVal = SE_1;
        break;

    case SE_1:
        retVal = SE_2;
        break;

    case SE_2:
        retVal = SE_3;
        break;

    case SE_3:
        retVal = SE_4;
        break;

    case SE_4:
        retVal = SE_5;
        break;

    case SE_5:
        retVal = SE_6;
        break;

    case SE_6:
        retVal = SE_7;
        break;

    case SE_7:
    default:
        retVal = SE_0;
    }
    return(retVal);
}



/*---------------------------------------------------------------------------
 * h3wUtlTerm()
 *     This function destroys a given utilities module, cleaning up any 
 *     allocated resources.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire utilities module context structure.  
 */
void h3wUtlTerm(Hci3WUtlStruct *cP)
{
}



/* ####################################################################### */ 
/* ################ The following functions are part of  ################# */ 
/* ################ the cyclic redundancy check module.  ################# */ 
/* ####################################################################### */ 



/*---------------------------------------------------------------------------
 * h3wCRCInit()
 *     Initializes the HCI 3-wire cyclic redundancy check module.  This 
 *     function must be invoked before any of the other HCI 3-wire cyclic 
 *     redundancy check functions will work correctly.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire cyclic redundancy check module context 
 *         structure.  
 */
void h3wCRCInit(Hci3WCrcStruct *cP)
{
    cP->crcTable[0x00] = 0x0000; cP->crcTable[0x01] = 0x1189;
    cP->crcTable[0x02] = 0x2312; cP->crcTable[0x03] = 0x329B;
    cP->crcTable[0x04] = 0x4624; cP->crcTable[0x05] = 0x57AD;
    cP->crcTable[0x06] = 0x6536; cP->crcTable[0x07] = 0x74BF;
    cP->crcTable[0x08] = 0x8C48; cP->crcTable[0x09] = 0x9DC1;
    cP->crcTable[0x0A] = 0xAF5A; cP->crcTable[0x0B] = 0xBED3;
    cP->crcTable[0x0C] = 0xCA6C; cP->crcTable[0x0D] = 0xDBE5;
    cP->crcTable[0x0E] = 0xE97E; cP->crcTable[0x0F] = 0xF8F7;
    cP->crcTable[0x10] = 0x1081; cP->crcTable[0x11] = 0x0108;
    cP->crcTable[0x12] = 0x3393; cP->crcTable[0x13] = 0x221A;
    cP->crcTable[0x14] = 0x56A5; cP->crcTable[0x15] = 0x472C;
    cP->crcTable[0x16] = 0x75B7; cP->crcTable[0x17] = 0x643E;
    cP->crcTable[0x18] = 0x9CC9; cP->crcTable[0x19] = 0x8D40;
    cP->crcTable[0x1A] = 0xBFDB; cP->crcTable[0x1B] = 0xAE52;
    cP->crcTable[0x1C] = 0xDAED; cP->crcTable[0x1D] = 0xCB64;
    cP->crcTable[0x1E] = 0xF9FF; cP->crcTable[0x1F] = 0xE876;
    cP->crcTable[0x20] = 0x2102; cP->crcTable[0x21] = 0x308B;
    cP->crcTable[0x22] = 0x0210; cP->crcTable[0x23] = 0x1399;
    cP->crcTable[0x24] = 0x6726; cP->crcTable[0x25] = 0x76AF;
    cP->crcTable[0x26] = 0x4434; cP->crcTable[0x27] = 0x55BD;
    cP->crcTable[0x28] = 0xAD4A; cP->crcTable[0x29] = 0xBCC3;
    cP->crcTable[0x2A] = 0x8E58; cP->crcTable[0x2B] = 0x9FD1;
    cP->crcTable[0x2C] = 0xEB6E; cP->crcTable[0x2D] = 0xFAE7;
    cP->crcTable[0x2E] = 0xC87C; cP->crcTable[0x2F] = 0xD9F5;
    cP->crcTable[0x30] = 0x3183; cP->crcTable[0x31] = 0x200A;
    cP->crcTable[0x32] = 0x1291; cP->crcTable[0x33] = 0x0318;
    cP->crcTable[0x34] = 0x77A7; cP->crcTable[0x35] = 0x662E;
    cP->crcTable[0x36] = 0x54B5; cP->crcTable[0x37] = 0x453C;
    cP->crcTable[0x38] = 0xBDCB; cP->crcTable[0x39] = 0xAC42;
    cP->crcTable[0x3A] = 0x9ED9; cP->crcTable[0x3B] = 0x8F50;
    cP->crcTable[0x3C] = 0xFBEF; cP->crcTable[0x3D] = 0xEA66;
    cP->crcTable[0x3E] = 0xD8FD; cP->crcTable[0x3F] = 0xC974;
    cP->crcTable[0x40] = 0x4204; cP->crcTable[0x41] = 0x538D;
    cP->crcTable[0x42] = 0x6116; cP->crcTable[0x43] = 0x709F;
    cP->crcTable[0x44] = 0x0420; cP->crcTable[0x45] = 0x15A9;
    cP->crcTable[0x46] = 0x2732; cP->crcTable[0x47] = 0x36BB;
    cP->crcTable[0x48] = 0xCE4C; cP->crcTable[0x49] = 0xDFC5;
    cP->crcTable[0x4A] = 0xED5E; cP->crcTable[0x4B] = 0xFCD7;
    cP->crcTable[0x4C] = 0x8868; cP->crcTable[0x4D] = 0x99E1;
    cP->crcTable[0x4E] = 0xAB7A; cP->crcTable[0x4F] = 0xBAF3;
    cP->crcTable[0x50] = 0x5285; cP->crcTable[0x51] = 0x430C;
    cP->crcTable[0x52] = 0x7197; cP->crcTable[0x53] = 0x601E;
    cP->crcTable[0x54] = 0x14A1; cP->crcTable[0x55] = 0x0528;
    cP->crcTable[0x56] = 0x37B3; cP->crcTable[0x57] = 0x263A;
    cP->crcTable[0x58] = 0xDECD; cP->crcTable[0x59] = 0xCF44;
    cP->crcTable[0x5A] = 0xFDDF; cP->crcTable[0x5B] = 0xEC56;
    cP->crcTable[0x5C] = 0x98E9; cP->crcTable[0x5D] = 0x8960;
    cP->crcTable[0x5E] = 0xBBFB; cP->crcTable[0x5F] = 0xAA72;
    cP->crcTable[0x60] = 0x6306; cP->crcTable[0x61] = 0x728F;
    cP->crcTable[0x62] = 0x4014; cP->crcTable[0x63] = 0x519D;
    cP->crcTable[0x64] = 0x2522; cP->crcTable[0x65] = 0x34AB;
    cP->crcTable[0x66] = 0x0630; cP->crcTable[0x67] = 0x17B9;
    cP->crcTable[0x68] = 0xEF4E; cP->crcTable[0x69] = 0xFEC7;
    cP->crcTable[0x6A] = 0xCC5C; cP->crcTable[0x6B] = 0xDDD5;
    cP->crcTable[0x6C] = 0xA96A; cP->crcTable[0x6D] = 0xB8E3;
    cP->crcTable[0x6E] = 0x8A78; cP->crcTable[0x6F] = 0x9BF1;
    cP->crcTable[0x70] = 0x7387; cP->crcTable[0x71] = 0x620E;
    cP->crcTable[0x72] = 0x5095; cP->crcTable[0x73] = 0x411C;
    cP->crcTable[0x74] = 0x35A3; cP->crcTable[0x75] = 0x242A;
    cP->crcTable[0x76] = 0x16B1; cP->crcTable[0x77] = 0x0738;
    cP->crcTable[0x78] = 0xFFCF; cP->crcTable[0x79] = 0xEE46;
    cP->crcTable[0x7A] = 0xDCDD; cP->crcTable[0x7B] = 0xCD54;
    cP->crcTable[0x7C] = 0xB9EB; cP->crcTable[0x7D] = 0xA862;
    cP->crcTable[0x7E] = 0x9AF9; cP->crcTable[0x7F] = 0x8B70;
    cP->crcTable[0x80] = 0x8408; cP->crcTable[0x81] = 0x9581;
    cP->crcTable[0x82] = 0xA71A; cP->crcTable[0x83] = 0xB693;
    cP->crcTable[0x84] = 0xC22C; cP->crcTable[0x85] = 0xD3A5;
    cP->crcTable[0x86] = 0xE13E; cP->crcTable[0x87] = 0xF0B7;
    cP->crcTable[0x88] = 0x0840; cP->crcTable[0x89] = 0x19C9;
    cP->crcTable[0x8A] = 0x2B52; cP->crcTable[0x8B] = 0x3ADB;
    cP->crcTable[0x8C] = 0x4E64; cP->crcTable[0x8D] = 0x5FED;
    cP->crcTable[0x8E] = 0x6D76; cP->crcTable[0x8F] = 0x7CFF;
    cP->crcTable[0x90] = 0x9489; cP->crcTable[0x91] = 0x8500;
    cP->crcTable[0x92] = 0xB79B; cP->crcTable[0x93] = 0xA612;
    cP->crcTable[0x94] = 0xD2AD; cP->crcTable[0x95] = 0xC324;
    cP->crcTable[0x96] = 0xF1BF; cP->crcTable[0x97] = 0xE036;
    cP->crcTable[0x98] = 0x18C1; cP->crcTable[0x99] = 0x0948;
    cP->crcTable[0x9A] = 0x3BD3; cP->crcTable[0x9B] = 0x2A5A;
    cP->crcTable[0x9C] = 0x5EE5; cP->crcTable[0x9D] = 0x4F6C;
    cP->crcTable[0x9E] = 0x7DF7; cP->crcTable[0x9F] = 0x6C7E;
    cP->crcTable[0xA0] = 0xA50A; cP->crcTable[0xA1] = 0xB483;
    cP->crcTable[0xA2] = 0x8618; cP->crcTable[0xA3] = 0x9791;
    cP->crcTable[0xA4] = 0xE32E; cP->crcTable[0xA5] = 0xF2A7;
    cP->crcTable[0xA6] = 0xC03C; cP->crcTable[0xA7] = 0xD1B5;
    cP->crcTable[0xA8] = 0x2942; cP->crcTable[0xA9] = 0x38CB;
    cP->crcTable[0xAA] = 0x0A50; cP->crcTable[0xAB] = 0x1BD9;
    cP->crcTable[0xAC] = 0x6F66; cP->crcTable[0xAD] = 0x7EEF;
    cP->crcTable[0xAE] = 0x4C74; cP->crcTable[0xAF] = 0x5DFD;
    cP->crcTable[0xB0] = 0xB58B; cP->crcTable[0xB1] = 0xA402;
    cP->crcTable[0xB2] = 0x9699; cP->crcTable[0xB3] = 0x8710;
    cP->crcTable[0xB4] = 0xF3AF; cP->crcTable[0xB5] = 0xE226;
    cP->crcTable[0xB6] = 0xD0BD; cP->crcTable[0xB7] = 0xC134;
    cP->crcTable[0xB8] = 0x39C3; cP->crcTable[0xB9] = 0x284A;
    cP->crcTable[0xBA] = 0x1AD1; cP->crcTable[0xBB] = 0x0B58;
    cP->crcTable[0xBC] = 0x7FE7; cP->crcTable[0xBD] = 0x6E6E;
    cP->crcTable[0xBE] = 0x5CF5; cP->crcTable[0xBF] = 0x4D7C;
    cP->crcTable[0xC0] = 0xC60C; cP->crcTable[0xC1] = 0xD785;
    cP->crcTable[0xC2] = 0xE51E; cP->crcTable[0xC3] = 0xF497;
    cP->crcTable[0xC4] = 0x8028; cP->crcTable[0xC5] = 0x91A1;
    cP->crcTable[0xC6] = 0xA33A; cP->crcTable[0xC7] = 0xB2B3;
    cP->crcTable[0xC8] = 0x4A44; cP->crcTable[0xC9] = 0x5BCD;
    cP->crcTable[0xCA] = 0x6956; cP->crcTable[0xCB] = 0x78DF;
    cP->crcTable[0xCC] = 0x0C60; cP->crcTable[0xCD] = 0x1DE9;
    cP->crcTable[0xCE] = 0x2F72; cP->crcTable[0xCF] = 0x3EFB;
    cP->crcTable[0xD0] = 0xD68D; cP->crcTable[0xD1] = 0xC704;
    cP->crcTable[0xD2] = 0xF59F; cP->crcTable[0xD3] = 0xE416;
    cP->crcTable[0xD4] = 0x90A9; cP->crcTable[0xD5] = 0x8120;
    cP->crcTable[0xD6] = 0xB3BB; cP->crcTable[0xD7] = 0xA232;
    cP->crcTable[0xD8] = 0x5AC5; cP->crcTable[0xD9] = 0x4B4C;
    cP->crcTable[0xDA] = 0x79D7; cP->crcTable[0xDB] = 0x685E;
    cP->crcTable[0xDC] = 0x1CE1; cP->crcTable[0xDD] = 0x0D68;
    cP->crcTable[0xDE] = 0x3FF3; cP->crcTable[0xDF] = 0x2E7A;
    cP->crcTable[0xE0] = 0xE70E; cP->crcTable[0xE1] = 0xF687;
    cP->crcTable[0xE2] = 0xC41C; cP->crcTable[0xE3] = 0xD595;
    cP->crcTable[0xE4] = 0xA12A; cP->crcTable[0xE5] = 0xB0A3;
    cP->crcTable[0xE6] = 0x8238; cP->crcTable[0xE7] = 0x93B1;
    cP->crcTable[0xE8] = 0x6B46; cP->crcTable[0xE9] = 0x7ACF;
    cP->crcTable[0xEA] = 0x4854; cP->crcTable[0xEB] = 0x59DD;
    cP->crcTable[0xEC] = 0x2D62; cP->crcTable[0xED] = 0x3CEB;
    cP->crcTable[0xEE] = 0x0E70; cP->crcTable[0xEF] = 0x1FF9;
    cP->crcTable[0xF0] = 0xF78F; cP->crcTable[0xF1] = 0xE606;
    cP->crcTable[0xF2] = 0xD49D; cP->crcTable[0xF3] = 0xC514;
    cP->crcTable[0xF4] = 0xB1AB; cP->crcTable[0xF5] = 0xA022;
    cP->crcTable[0xF6] = 0x92B9; cP->crcTable[0xF7] = 0x8330;
    cP->crcTable[0xF8] = 0x7BC7; cP->crcTable[0xF9] = 0x6A4E;
    cP->crcTable[0xFA] = 0x58D5; cP->crcTable[0xFB] = 0x495C;
    cP->crcTable[0xFC] = 0x3DE3; cP->crcTable[0xFD] = 0x2C6A;
    cP->crcTable[0xFE] = 0x1EF1; cP->crcTable[0xFF] = 0x0F78;
}



/*---------------------------------------------------------------------------
 * h3wCRCCalc()
 *     Calculates the HCI 3-wire cyclic redundancy value for a given data 
 *     buffer.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire cyclic redundancy check module context 
 *         structure.  
 *     dataP - Must point to a valid buffer containing an HCI 3-wire framed 
 *         message.  
 *     numOctets - Must contain the size of the given HCI 3-wire framed 
 *         buffer.  
 *
 * Returns:
 *     This function returns the cyclic redundancy value corresponding to the 
 *     given buffer.  Note that there are many different "flavors" of CRC 
 *     algorithms.  The value returned by this function corresponds to the 
 *     16-bit CCITT-CRC algorithm specified in the HCI 3-wire specification.  
 */
U16 h3wCRCCalc(Hci3WCrcStruct *cP, U8 *dataP, U16 numOctets) 
{
    U16 crc;
    U16 hiMask;
    U16 loMask;
    U16 ndx;
    U16 retVal;

    crc = 0xFFFF;
    for (ndx = 0; ndx < numOctets; ndx += 1) {
        crc = (U16)( (crc >> 8) 
                   ^ cP->crcTable[(crc & 0xFF) ^ *(dataP + ndx)]);
    }

    /* Reverse the order of the bits in the CRC.  */ 
    retVal = 0;
    hiMask = 0x8000;
    loMask = 0x0001;
    for (ndx = 0; ndx < 16; ndx += 1) {
        if (hiMask & crc) {
            retVal |= loMask;
        }
        hiMask >>= 1;
        loMask <<= 1;
    }

    return(retVal);
}



/*---------------------------------------------------------------------------
 * h3wCRCTerm()
 *     This function destroys a given cyclic redundancy check module, cleaning 
 *     up any allocated resources.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire cyclic redundancy check module context 
 *         structure.  
 */
void h3wCRCTerm(Hci3WCrcStruct *cP)
{
}



/* ####################################################################### */ 
/* ### The following functions are part of the event handling module.  ### */ 
/* ####################################################################### */ 



/*---------------------------------------------------------------------------
 * h3wEvtInit()
 *     Initializes the HCI 3-wire event handling module.  This function must 
 *     be invoked before any of the other HCI 3-wire event handling functions 
 *     will work correctly.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire event handling module context structure.  
 *     ecb - This parameter must contain a valid event callback structure.  
 */
void h3wEvtInit(Hci3WEvtStruct *cP, EcbStruct ecb)
{
    U16 ndx;

    cP->semaphoreCnt = 0;
    cP->rdNdx = 0;
    cP->wrNdx = 0;
    for (ndx = 0; EVENTQ_SIZE > ndx; ndx += 1) {
        cP->eventQ[ndx].type = (EVENT_TYPE_ENUM)0;
        cP->eventQ[ndx].data = 0;
    }
    for (ndx = 0; NUM_EVENTS > ndx; ndx += 1) {
        switch(ndx) {
        case ET_TX_PURE_ACK:
        case ET_LOW_POWER_TX_WAKEUP:
            cP->qState[ndx].queueMultiple = FALSE;
            break;

        default:
            cP->qState[ndx].queueMultiple = TRUE;
        }
        cP->qState[ndx].currentlyQueued = FALSE;
    }
    cP->privECB.callBack = ecb.callBack;
    cP->privECB.context  = ecb.context;
}



/*---------------------------------------------------------------------------
 * h3wEvtGetSemaphore()
 *     This function attempts to obtain the event semaphore used to protect 
 *     the event handling queue.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire event handling module context structure.  
 *
 * Returns:
 *     This function returns TRUE if it was successfully able to obtain 
 *     ownership of the event queue semaphore, or FALSE otherwise.  
 */
BOOL h3wEvtGetSemaphore(Hci3WEvtStruct *cP)
{
    BOOL retVal;

    retVal = FALSE;
    cP->semaphoreCnt = (U8)(cP->semaphoreCnt + 1);
    if (1 == cP->semaphoreCnt) {
        retVal = TRUE;
    } else {
        cP->semaphoreCnt = 1;
    }
    return(retVal);
}



/*---------------------------------------------------------------------------
 * h3wEvtProcess()
 *     This function processes the events on the event handling queue.  This 
 *     function must not be called unless the event handling semaphore is 
 *     owned by the invoking code.  After the pending events are processed, 
 *     this function releases the semaphore.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire event handling module context structure.  
 */
void h3wEvtProcess(Hci3WEvtStruct *cP) 
{
    BOOL            eventFoundOnQueue;
    BOOL            handlePending;
    BOOL            searchingQ;
    EVENT_TYPE_ENUM justProcessedEvent;
    U8              qNdx;

    Assert(0 < cP->semaphoreCnt);
    do {
        handlePending = FALSE;
        if (cP->rdNdx != cP->wrNdx) {
            cP->privECB.callBack(cP->privECB.context, 
                                 cP->eventQ[cP->rdNdx]);
            justProcessedEvent = cP->eventQ[cP->rdNdx].type;
            if (255 > cP->rdNdx) {
                cP->rdNdx += 1;
            } else {
                cP->rdNdx = 0;
            }

            /* Determine whether or not there is another instance 
             * of the event just processed still on the queue.  
             */ 
            qNdx = cP->rdNdx;
            eventFoundOnQueue = FALSE;
            do {
                searchingQ = FALSE;
                if (qNdx != cP->wrNdx) {
                    if (cP->eventQ[qNdx].type == justProcessedEvent) {
                        eventFoundOnQueue = TRUE;
                    } else {
                        if (255 > qNdx) {
                            qNdx += 1;
                        } else {
                            qNdx = 0;
                        }
                        searchingQ = TRUE;
                    }
                }
            } while (TRUE == searchingQ);
            if (TRUE != eventFoundOnQueue) {
                cP->qState[justProcessedEvent].currentlyQueued = FALSE;
            }

            handlePending = TRUE;
        }
    } while (TRUE == handlePending);
    cP->semaphoreCnt = 0;
}



/*---------------------------------------------------------------------------
 * h3wEvtQueue()
 *     This function pushes an event on the event queue.  If it is able to 
 *     obtain the semaphore protecting the queue, it then invokes the function 
 *     that processes the queued up events.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire event handling module context structure.  
 *     event - This parameter must contain the event structure that is to be 
 *         queued up.  
 */
void h3wEvtQueue(Hci3WEvtStruct *cP, EventStruct event)
{
    BOOL enqueue;
    BOOL hasSemaphore;

    hasSemaphore = h3wEvtGetSemaphore(cP);
    enqueue = TRUE;
    if (TRUE != cP->qState[event.type].queueMultiple) {
        if (TRUE == cP->qState[event.type].currentlyQueued) {
            enqueue = FALSE;
        }
    }
    if (TRUE == enqueue) {
        cP->eventQ[cP->wrNdx].type = event.type;
        cP->eventQ[cP->wrNdx].data = event.data;
        if (255 > cP->wrNdx) {
            cP->wrNdx += 1;
        } else {
            cP->wrNdx = 0;
        }
        Assert(cP->wrNdx != cP->rdNdx);
        cP->qState[event.type].currentlyQueued = TRUE;
    }
    if (TRUE == hasSemaphore) {
        h3wEvtProcess(cP);
    }
}



/*---------------------------------------------------------------------------
 * h3wEvtTerm()
 *     This function destroys a given event handling module, cleaning up any 
 *     allocated resources.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire event handling module context structure.  
 */
void h3wEvtTerm(Hci3WEvtStruct *cP)
{
}



/* ####################################################################### */ 
/* ### The following functions are part of the error handling module.  ### */ 
/* ####################################################################### */ 



/*---------------------------------------------------------------------------
 * h3wErrInit()
 *     Initializes the HCI 3-wire error handling module.  This function must 
 *     be invoked before any of the other HCI 3-wire error handling functions 
 *     will work correctly.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire error handling module context structure.  
 */
void h3wErrInit(Hci3WErrStruct *cP)
{
    cP->msbErr = 0;
    cP->lsbErr = 0;
}



/*---------------------------------------------------------------------------
 * h3wErrHasNone()
 *     Determines whether an error has been set.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire error handling module context structure.  
 *
 * Returns:
 *     This function returns TRUE if no errors have been set for the given 
 *     error context, or FALSE otherwise.  
 */
BOOL h3wErrHasNone(Hci3WErrStruct *cP)
{
    BOOL retVal;

    if (0 == h3wErrGet(cP)) {
        retVal = TRUE;
    } else {
        retVal = FALSE;
    }
    return(retVal);
}



/*---------------------------------------------------------------------------
 * h3wErrGet()
 *     This function determines the numeric value corresponding to the error 
 *     state of the given error context.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire error handling module context structure.  
 *
 * Returns:
 *     This function returns zero if no errors have been set for the given 
 *     error context.  Otherwise, it returns a non-zero numeric encoding of 
 *     the error value.  
 */
U32 h3wErrGet(Hci3WErrStruct *cP)
{
    return((cP->msbErr << 8) | cP->lsbErr);
}



/*---------------------------------------------------------------------------
 * h3wErrSet()
 *     Sets an error for the given error context.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire error handling module context structure.  
 *     errSrc - An enumeration that must correspond to the location where the 
 *         error was detected.  
 *     error - An enumeration that must correspond to the error condition.  
 */
void h3wErrSet(Hci3WErrStruct *cP, ERR_SRC_ENUM errSrc, ERR_ENUM error)
{
    switch(errSrc) {
    case ES_DAT_DEHCI:
        cP->msbErr = 0x01;
        break;

    case ES_DAT_DEHCIBYTES:
        cP->msbErr = 0x02;
        break;

    case ES_DAT_ENHCI:
        cP->msbErr = 0x03;
        break;

    case ES_DAT_RECEIVE:
        cP->msbErr = 0x04;
        break;

    case ES_DAT_SEND:
        cP->msbErr = 0x05;
        break;

    case ES_H3W_CLOCKMULT:
        cP->msbErr = 0x06;
        break;

    case ES_H3W_DATAFREE:
        cP->msbErr = 0x07;
        break;

    case ES_H3W_DATAINIT:
        cP->msbErr = 0x08;
        break;

    case ES_H3W_GETCONFIG:
        cP->msbErr = 0x09;
        break;

    case ES_H3W_GETCONFIGE:
        cP->msbErr = 0x0A;
        break;

    case ES_H3W_GETPLD:
        cP->msbErr = 0x0B;
        break;

    case ES_H3W_HANDLEACTIVERX:
        cP->msbErr = 0x0C;
        break;

    case ES_H3W_INIT:
        cP->msbErr = 0x0D;
        break;

    case ES_H3W_RECEIVE:
        cP->msbErr = 0x0E;
        break;

    case ES_H3W_RECEIVE_E:
        cP->msbErr = 0x0F;
        break;

    case ES_H3W_SEND_E:
        cP->msbErr = 0x10;
        break;

    case ES_H3W_SENDREL:
        cP->msbErr = 0x11;
        break;

    case ES_H3W_SENDUNREL:
        cP->msbErr = 0x12;
        break;

    case ES_H3W_SETPLD:
        cP->msbErr = 0x13;
        break;

    case ES_H3W_SETSPEED:
        cP->msbErr = 0x14;
        break;

    case ES_H3W_SLEEP:
        cP->msbErr = 0x15;
        break;

    case ES_H3W_UARTTOBPS:
        cP->msbErr = 0x16;
        break;

    case ES_H3W_WAKEUP:
        cP->msbErr = 0x17;
        break;

    case ES_HDR_GETHDR:
        cP->msbErr = 0x18;
        break;

    case ES_HDR_SETHDR:
        cP->msbErr = 0x19;
        break;

    case ES_MEM_GETBUF:
        cP->msbErr = 0x1A;
        break;

    case ES_SLP_DESLIP:
        cP->msbErr = 0x1B;
        break;

    case ES_SLP_SEND:
        cP->msbErr = 0x1C;
        break;

    case ES_SLP_UARTFREE:
        cP->msbErr = 0x1D;
        break;

    case ES_SLP_UARTINIT:
        cP->msbErr = 0x1E;
        break;

    case ES_SLP_UARTINITIALIZED:
        cP->msbErr = 0x1F;
        break;

    case ES_UTL_BPSENCODE:
        cP->msbErr = 0x20;
        break;

    case ES_UTL_SEQITOE:
        cP->msbErr = 0x21;
        break;

    case ES_UTL_SETPLD:
        cP->msbErr = 0x22;
        break;

    default:
        cP->msbErr = 0xFF;
    }
    Assert(0x22 >= cP->msbErr);
    switch(error) {
    case ER_ALREADY_INIT:
        cP->lsbErr = 0x01;
        break;

    case ER_BAD_HDR_CS:
        cP->lsbErr = 0x02;
        break;

    case ER_BAD_PKT_RELIABILITY:
        cP->lsbErr = 0x03;
        break;

    case ER_BAD_PKT_ROLE:
        cP->lsbErr = 0x04;
        break;

    case ER_BPS_UNREPRESENTABLE:
        cP->lsbErr = 0x05;
        break;

    case ER_BUF_TOO_SMALL:
        cP->lsbErr = 0x06;
        break;

    case ER_BUSY:
        cP->lsbErr = 0x07;
        break;

    case ER_CRC_ERROR:
        cP->lsbErr = 0x08;
        break;

    case ER_INSUFFICIENT_MEMORY:
        cP->lsbErr = 0x09;
        break;

    case ER_LINK_PROHIBITS_CRCS:
        cP->lsbErr = 0x0A;
        break;

    case ER_NOT_ACTIVE:
        cP->lsbErr = 0x0B;
        break;

    case ER_NOT_AWAKE:
        cP->lsbErr = 0x0C;
        break;

    case ER_NOT_INIT:
        cP->lsbErr = 0x0D;
        break;

    case ER_OUT_OF_RANGE:
        cP->lsbErr = 0x0E;
        break;

    case ER_OUT_OF_SEQUENCE:
        cP->lsbErr = 0x0F;
        break;

    case ER_PAYLOAD_INI:
        cP->lsbErr = 0x10;
        break;

    case ER_PAYLOAD_SIZE_MISMATCH:
        cP->lsbErr = 0x11;
        break;

    case ER_PAYLOAD_TOO_LARGE:
        cP->lsbErr = 0x12;
        break;

    case ER_PAYLOAD_TOO_SMALL:
        cP->lsbErr = 0x13;
        break;

    case ER_SENT_TOO_FEW:
        cP->lsbErr = 0x14;
        break;

    case ER_SENT_TOO_MANY:
        cP->lsbErr = 0x15;
        break;

    case ER_SLIDING_WINDOW_FULL:
        cP->lsbErr = 0x16;
        break;

    case ER_TRUNCATED_HCI_MSG:
        cP->lsbErr = 0x17;
        break;

    case ER_UART_FAILURE:
        cP->lsbErr = 0x18;
        break;

    case ER_UART_SPEED:
        cP->lsbErr = 0x19;
        break;

    case ER_UNKNOWN_PKT_TYPE:
        cP->lsbErr = 0x1A;
        break;

    case ER_XOFF:
        cP->lsbErr = 0x1B;
        break;

    default:
        cP->lsbErr = 0xFF;
    }
    Assert(0x1B >= cP->lsbErr);
}



/*---------------------------------------------------------------------------
 * h3wErrReportUnsolicited()
 *     This function generates reports for errors detected by code that was 
 *     not directly solicited by the upper layers.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire error handling module context structure.  
 *     errOnly - This parameter must be TRUE if only errors are to be 
 *         reported, or FALSE if both errors and non-error conditions are to 
 *         be reported.  
 *     operationStr - This parameter must point to a short string describing 
 *         the non-solicited operation.  
 */
void h3wErrReportUnsolicited(Hci3WErrStruct *cP, 
                             BOOL            errOnly, 
                             char           *operationStr)
{
    U32 reportError;

    reportError = h3wErrGet(cP);
    if (0 == reportError) {
        if (TRUE != errOnly) {
            Report(("HCI3W:  A%s operation was successful.\n", operationStr));
        }
    } else {
        Report(("HCI3W:  A%s operation failed with error %d.\n", 
                operationStr, 
                reportError));
    }
}



/*---------------------------------------------------------------------------
 * h3wErrTerm()
 *     This function destroys a given error handling module, cleaning up any 
 *     allocated resources.  
 * 
 * Parameters:
 *     cP - Pointer to an HCI 3-wire error handling module context structure.  
 */
void h3wErrTerm(Hci3WErrStruct *cP)
{
}



/****************************************************************************
 *
 * Private Functions
 *
 ****************************************************************************/

