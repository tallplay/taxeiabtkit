/****************************************************************************
 *
 * File:
 *     $Workfile:uarttran.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:76$
 *
 * Description:
 *     Code for UART transport driver.
 * 
 * Copyright 1999-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions,
 * Inc.  This work contains confidential and proprietary information of
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret,
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "osapi.h"
#include "uart.h"
#include "sys/uarttran.h"
#include "sys/evmxp.h"
#include <string.h>

#ifdef DEMO
    #undef Report
    #define Report(S)
#endif

/* Local functions */
static void uartEventHandler(U8 event);
static void uartTransmitData(void);
static void uartReadData(void);
static void uartCommandWatchdog(EvmTimer *timer);
static void UARTTRAN_SendData(HciPacket *hciPacket);
static void UARTTRAN_BufferAvailable(HciBufFlags buffType);
static void UARTTRAN_SetSpeed(U16 speed);

/****************************************************************************
 *
 *  RAM Data
 *
 ****************************************************************************/

#if (XA_CONTEXT_PTR == XA_ENABLED)

/* If context pointers are used create a pointer called uaxprt 
 */
UATRAN_Context uaxprtTemp;
UATRAN_Context *uaxprt = &uaxprtTemp;

#elif (XA_CONTEXT_PTR == XA_DISABLED)

/* If context pointers are NOT used create a static object.
 */
UATRAN_Context uaxprt;

#endif

/*---------------------------------------------------------------------------
 * UARTTRAN_Init() 
 *
 *     Initialize the UART transport and hardware drivers
 */
BtStatus UARTTRAN_Init(TranCallback tranCallback)
{
    /* Clear the transport's context memory */
#if (XA_CONTEXT_PTR == XA_ENABLED)
    OS_MemSet((U8 *)uaxprt, 0, sizeof(UATRAN_Context));

#elif (XA_CONTEXT_PTR == XA_DISABLED)
    OS_MemSet((U8 *)&uaxprt, 0, sizeof(UATRAN_Context));
#endif

    uaxprt.callback = tranCallback;

    /* Initialize RX/TX queues */
    InitializeListHead(&(uaxprt.txQueue));
    InitializeListHead(&(uaxprt.txEnroute));

    /* Initialize rx state */
    uaxprt.maxReadLen = 1;
    uaxprt.buffType = 0;
    uaxprt.ptr = &(uaxprt.buffType);
    uaxprt.rxState = RXS_GET_TYPE;

    /* Initialize tx state */
    uaxprt.txState = TXS_IDLE;

    /* Initialize command watchdog timer */
    uaxprt.watchdog.func = uartCommandWatchdog;
    uaxprt.watchdog.context = 0;
    uaxprt.unackedCommand = FALSE;

    uaxprt.tranEntry.sendData = UARTTRAN_SendData;
    uaxprt.tranEntry.buffAvail = UARTTRAN_BufferAvailable;
    uaxprt.tranEntry.setSpeed = UARTTRAN_SetSpeed;
    HCI_RegisterTransport(&uaxprt.tranEntry);

    /* Init the serial interface */
    return UART_Init(uartEventHandler);
}


/*---------------------------------------------------------------------------
 * UARTTRAN_Shutdown() 
 *
 *     Shut down the UART transport and hardware drivers
 */
BtStatus UARTTRAN_Shutdown(void)
{
    if (uaxprt.watchdog.context == (void *)1) {
        EVM_CancelTimer(&uaxprt.watchdog);
    }

    return UART_Shutdown();
}


/*---------------------------------------------------------------------------
 * UARTTRAN_SendData() 
 *      Send a packet to the BT host via a UART interface.
 *      This function is called from HCI_Process, part of the EVM_Process or
 *      stack thread.
 *      
 * Requires:
 *     
 * Parameters:
 *
 * Returns:
 *      BT_STATUS_PENDING
 */
static void UARTTRAN_SendData(HciPacket *hciPacket)
{
    Report(("UATRAN: SendData(%lx)\n", (U32)hciPacket));

    /* Check the type of the packet */
    if ((hciPacket->flags & HCI_BUFTYPE_COMMAND) &&
        (LEtoHost16(hciPacket->header) != HCC_HOST_NUM_COMPLETED_PACKETS)) {
        /* If it's a command, start the watchdog timer. */
        OS_StopHardware();
        uaxprt.unackedCommand = TRUE;
        uaxprt.watchdog.context = (void *)1;
        OS_ResumeHardware();
        EVM_StartTimer(&uaxprt.watchdog, UTP_UART_EVENT_WAIT_TIME);
    }

    /* Because we're manipulating sensitive data structures, we must
     * request that the hardware thread/interrupt does not execute.
     */
    OS_StopHardware();
    InsertTailList(&uaxprt.txQueue, (ListEntry *)&hciPacket->node);

    if (uaxprt.txState == TXS_IDLE) {
        /* It's safe to do this during StopHardware because it
         * only sends one byte (HCI packet type)
         */
        uartTransmitData();
    }
    OS_ResumeHardware();
}

/*---------------------------------------------------------------------------
 * UARTTRAN_BufferAvailable() 
 *
 *     A receive buffer is avialable
 */
static void UARTTRAN_BufferAvailable(HciBufFlags buffType)
{
    UNUSED_PARAMETER(buffType);
    if (uaxprt.rxState == RXS_GET_BUFFER) {
        /* Restart the read */
        Report(("UATRAN: Buffer Now Available\n"));
        uartReadData();
    }
}

/*---------------------------------------------------------------------------
 * UARTTRAN_SetSpeed() 
 *
 *     Set the UART baud rate
 */
static void UARTTRAN_SetSpeed(U16 speed)
{
    UART_SetSpeed(speed);
}


/*---------------------------------------------------------------------------
 * uartEventHandler()
 *
 *     Called from UART hardware driver to indicate rx/tx events.
 */
static void uartEventHandler(U8 event)
{
    switch (event) {
    case UE_WRITE_COMPLETE:
        uartTransmitData();
        break;
    case UE_DATA_TO_READ:
        uartReadData();
        break;
    case UE_INIT_COMPLETE:
        uaxprt.callback(TRAN_INIT_STATUS, BT_STATUS_SUCCESS);
        break;
    case UE_SHUTDOWN_COMPLETE:
        uaxprt.callback(TRAN_SHUTDOWN_STATUS, BT_STATUS_SUCCESS);
        break;
    default:
        /* Invalid event received */
        Assert(FALSE);
        break;
    }        
}

/*---------------------------------------------------------------------------
 * uartCommandWatchdog()
 *     This function is called UART_EVENT_WAIT_TIME seconds after the last 
 *     command issued.
 */
static void uartCommandWatchdog(EvmTimer *timer)
{
    UNUSED_PARAMETER(timer);

    if (uaxprt.unackedCommand) {
        Report(("UATRAN: HCI Command was not acknowledged with an event\n"));
        HCI_TransportError();
        uaxprt.unackedCommand = FALSE;
        uaxprt.watchdog.context = 0;
    }
}

/*---------------------------------------------------------------------------
 * uartTransmitData()
 *
 *      Transmits data through the UART port.
 *      This function may be called when an HCI packet is sent down from
 *      the stack. It is also called on the controller context when
 *      it's ready to transmit more data.
 */
U8 BUF[256];
static void uartTransmitData(void)
{
    int        bytesWritten;

    /* 
     * Do not exit this function unless one of the following is true:
     *      a. data has been written
     *      b. the state is IDLE
     */

TopOfFunction:

//tallplay add for reduce physical traffic begin
switch (uaxprt.txState)
{
    U16 len;
    U8 idx;
    
    case TXS_IDLE:      /* Transmit state machine is waiting for a packet */
        /* Get a packet off the transmit queue */
        if (IsListEmpty(&uaxprt.txQueue)) {
            uaxprt.txPacket = 0;
        } else {
            uaxprt.txPacket = (HciPacket *)RemoveHeadList(&uaxprt.txQueue);

            if (uaxprt.txPacket->flags & HCI_BUFTYPE_COMMAND) {
                uaxprt.txType = 1;
            } else if (uaxprt.txPacket->flags & HCI_BUFTYPE_ACL_DATA) {
                uaxprt.txType = 2;
#if NUM_SCO_CONNS != 0
            } else if (uaxprt.txPacket->flags & HCI_BUFTYPE_SCO_DATA) {
                uaxprt.txType = 3;
#endif /* NUM_SCO_CONNS != 0 */
            } else {
                Assert(0);
            }

            idx = 0;
            
            BUF[0] = uaxprt.txType;
            idx += 1;
            
            len = ((uaxprt.txType == 1) || (uaxprt.txType == 3)) ? 3 : 4;
            memcpy(BUF+idx, uaxprt.txPacket->header, len);
            idx += len;

            if(uaxprt.txPacket->fCount)
            {
                len = uaxprt.txPacket->f[0].len;
                memcpy(BUF+idx,  uaxprt.txPacket->f[0].buffer, len);
                idx += len;
            }

            Assert(idx < 256);
            uaxprt.txFrag = 1;
            uaxprt.txState = TXS_DATA;
            uaxprt.txBp = BUF;
            uaxprt.txLen = idx;
        }           

        break;


    case TXS_DATA:      /* In the process of writing the data buffers */
       /* Write data */
        if (uaxprt.txLen == 0) {
            if (uaxprt.txFrag < uaxprt.txPacket->fCount) {
                uaxprt.txBp  = uaxprt.txPacket->f[uaxprt.txFrag].buffer;
                uaxprt.txLen = uaxprt.txPacket->f[uaxprt.txFrag].len;
                uaxprt.txFrag++;
            } else {
                /* Done sending entire packet */
                HCI_PacketSent(uaxprt.txPacket);
                uaxprt.txState = TXS_IDLE;
                goto TopOfFunction;
            }
        }

        break;


    default:
        Assert(0);
        break;

    }
    if (uaxprt.txState != TXS_IDLE) {
        bytesWritten = UART_Write(uaxprt.txBp, uaxprt.txLen);

        /* If no bytes were written, there was some kind of error */
        if (bytesWritten <= 0) {
            Report(("UATRAN: UART could not write data\n"));
            HCI_TransportError();
        }

        uaxprt.txLen -= bytesWritten;
        uaxprt.txBp  += bytesWritten;
    }
return;
//tallplay add for reduce physical traffic end

    switch (uaxprt.txState) {
    
    case TXS_IDLE:      /* Transmit state machine is waiting for a packet */
        /* Get a packet off the transmit queue */
        if (IsListEmpty(&uaxprt.txQueue)) {
            uaxprt.txPacket = 0;
        } else {
            uaxprt.txPacket = (HciPacket *)RemoveHeadList(&uaxprt.txQueue);
            uaxprt.txState  = TXS_TYPE;
            uaxprt.txBp     = &uaxprt.txType;
            uaxprt.txLen    = sizeof(uaxprt.txType);

            if (uaxprt.txPacket->flags & HCI_BUFTYPE_COMMAND) {
                uaxprt.txType = 1;
            } else if (uaxprt.txPacket->flags & HCI_BUFTYPE_ACL_DATA) {
                uaxprt.txType = 2;
#if NUM_SCO_CONNS != 0
            } else if (uaxprt.txPacket->flags & HCI_BUFTYPE_SCO_DATA) {
                uaxprt.txType = 3;
#endif /* NUM_SCO_CONNS != 0 */
            } else {
                Assert(0);
            }
        }

        break;


    case TXS_TYPE:      /* In the process of writing out packet type */
        if (uaxprt.txLen == 0) {
            uaxprt.txState = TXS_HEADER;
            uaxprt.txBp    = uaxprt.txPacket->header;
            if ((uaxprt.txType == 1) || (uaxprt.txType == 3)) {
                uaxprt.txLen = 3;
            } else {
                uaxprt.txLen = 4;
            }
        }
        break;


    case TXS_HEADER:    /* In the process of writing the header */
        /* Write header */
        if (uaxprt.txLen == 0) {
            uaxprt.txState   = TXS_DATA;
            uaxprt.txFrag    = 0;
        } else {
            break;
        }
        /* Fall into next state when header has been written */


    case TXS_DATA:      /* In the process of writing the data buffers */
        /* Write data */
        if (uaxprt.txLen == 0) {
            if (uaxprt.txFrag < uaxprt.txPacket->fCount) {
                uaxprt.txBp  = uaxprt.txPacket->f[uaxprt.txFrag].buffer;
                uaxprt.txLen = uaxprt.txPacket->f[uaxprt.txFrag].len;
                uaxprt.txFrag++;
            } else {
                /* Done sending entire packet */
                HCI_PacketSent(uaxprt.txPacket);
                uaxprt.txState = TXS_IDLE;
                goto TopOfFunction;
            }
        }

        break;


    default:
        Assert(0);
        break;

    } /* switch (uaxprt.txState) */


    /* Write outstanding bytes to the UART driver */
    if (uaxprt.txState != TXS_IDLE) {
        bytesWritten = UART_Write(uaxprt.txBp, uaxprt.txLen);

        /* If no bytes were written, there was some kind of error */
        if (bytesWritten <= 0) {
            Report(("UATRAN: UART could not write data\n"));
            HCI_TransportError();
        }

        uaxprt.txLen -= bytesWritten;
        uaxprt.txBp  += bytesWritten;
    }
}


/*---------------------------------------------------------------------------
 * uartReadData() 
 *      Called from lower layer when data is available for reading.
 *      
 * Requires:
 *     
 * Parameters:
 *
 * Returns:
 */
static void uartReadData(void)
{
    U16 readLen = 0; /* Length of data we most recently read from UART */
    BtStatus status;
    HciHandle hciConnHandle;
    U16 len;

    do {
        if (uaxprt.rxState != RXS_GET_BUFFER) {

            /* Read available data */
            readLen = UART_Read(uaxprt.ptr, uaxprt.maxReadLen);
            Assert(readLen >= 0);

            uaxprt.ptr += readLen;

            /* Report(("UATRAN: Bytes read = %d\n", readLen)); */

            /* See if enough data is available */
            if (readLen != uaxprt.maxReadLen) {
                uaxprt.maxReadLen -= readLen;
                return;
            }
        }

        switch (uaxprt.rxState) {
        
        case RXS_GET_TYPE:

            /* Get the type of buffer */
            if (uaxprt.buffType == IND_EVENT) { /* Event */
                uaxprt.headerLen = 2;
#if NUM_SCO_CONNS != 0
            } else if (uaxprt.buffType == IND_SCO_DATA) {
                uaxprt.headerLen = 3;
#endif /* NUM_SCO_CONNS != 0 */
            } else if (uaxprt.buffType == IND_ACL_DATA) {
                uaxprt.headerLen = 4;
            } else {
                HCI_TransportError();
            } 

            /* Set up for the next state */
            uaxprt.ptr = uaxprt.header;
            uaxprt.maxReadLen = uaxprt.headerLen;
            uaxprt.rxState = RXS_GET_HEADER;
            break;

        case RXS_GET_HEADER:

            /* Header info has been read */         
            uaxprt.rxState = RXS_GET_BUFFER;

            /* Drop through to next state */

        case RXS_GET_BUFFER:

            /* Get an available buffer */
            switch (uaxprt.buffType) {
            
            case IND_ACL_DATA:  /* ACL */
                len = (U16)(LEtoHost16(&(uaxprt.header[2])) + uaxprt.headerLen);
                hciConnHandle = LEtoHost16(&(uaxprt.header[0]));
                status = HCI_GetAclBuffer(&uaxprt.rxBuffHandle, 
                                          hciConnHandle, len);
                if (status == BT_STATUS_SUCCESS) {
                    /* Got an ACL buffer */
                    uaxprt.ptr = HCI_GetRxPtr(uaxprt.rxBuffHandle);
                    uaxprt.maxReadLen = LEtoHost16(&(uaxprt.header[2]));

                    /* Report(("UATRAN: ACL data length = %d\n" ,
                       uaxprt.maxReadLen)); */

                } else if (status == BT_STATUS_NO_RESOURCES) {
                    /* No receive buffer available */
                    /* Report(("UATRAN: No ACL receiver buffer available\n")); */
                    return;
                } else {
                    HCI_TransportError();
                    return;
                }
                break;
#if NUM_SCO_CONNS != 0
            case IND_SCO_DATA:  /* SCO */
                len = (U16)(uaxprt.header[2] + uaxprt.headerLen);
                hciConnHandle = LEtoHost16(&(uaxprt.header[0]));
                status = HCI_GetScoBuffer(&uaxprt.rxBuffHandle, 
                                          hciConnHandle, len);
                if (status == BT_STATUS_SUCCESS) {
                    /* Got an SCO buffer */
                    uaxprt.ptr = HCI_GetRxPtr(uaxprt.rxBuffHandle);
                    Assert(uaxprt.ptr);
                    uaxprt.maxReadLen = uaxprt.header[2];

                    /* Report(("UATRAN: SCO data length = %d\n" ,
                       uaxprt.maxReadLen)); */

                } else if (status == BT_STATUS_NO_RESOURCES) {
                    /* No receive buffer available */
                    /* Report(("UATRAN: No SCO receiver buffer available\n")); */
                    return;
                } else {
                    HCI_TransportError();
                    return;
                }
                break;
#endif /* NUM_SCO_CONNS != 0 */
            case IND_EVENT:  /* Event */
                len = (U16)(uaxprt.header[1] + uaxprt.headerLen);
                status = HCI_GetEventBuffer(&uaxprt.rxBuffHandle, len);
                if (status == BT_STATUS_SUCCESS) {
                    /* Got an Event buffer */
                    uaxprt.ptr = HCI_GetRxPtr(uaxprt.rxBuffHandle);
                    uaxprt.maxReadLen = uaxprt.header[1];

                    /* Report(("UATRAN: Event parameter length = %d\n",
                     * uaxprt.maxReadLen));
                     */

                } else if (status == BT_STATUS_NO_RESOURCES) {
                    /* No receive buffer available */

                    /* Report(("UATRAN: No Event receiver buffer available\n"));
                     */
                    return;
                } else {
                    HCI_TransportError();
                    return;
                }
                break;
            default:
                /* Invalid framing */
                HCI_TransportError();
                return;
            }

            OS_MemCopy(uaxprt.ptr, uaxprt.header, uaxprt.headerLen);

            /* Set up for next state */
            uaxprt.ptr += uaxprt.headerLen;
            uaxprt.rxState = RXS_GET_DATA;
            readLen = 1; /* Force loop to re-execute */
            break;

        case RXS_GET_DATA:
            /* Pass it up */

            /* If this is an event then consider all commands ack'ed */
            if (uaxprt.rxBuffHandle->flags & HCI_BUFTYPE_EVENT) {
                uaxprt.unackedCommand = FALSE;  
            }

            /* Report(("UATRAN: Passing up %d bytes\n", uaxprt.rxBuffHandle->len));*/
            HCI_RxBufferInd(uaxprt.rxBuffHandle, 
                            BT_STATUS_SUCCESS);

            /* Reset to first state */
            uaxprt.buffType = 0;
            uaxprt.ptr = &(uaxprt.buffType);
            uaxprt.maxReadLen = 1;
            uaxprt.rxState = RXS_GET_TYPE;
            break;
        }   
    } while (readLen);
}
