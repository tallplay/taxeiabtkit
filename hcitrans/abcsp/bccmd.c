/****************************************************************************
 *
 * File:
 *     $Workfile:bccmd.c$ for iAnywhere Blue SDK, Version 2.2
 *     $Revision:8$
 *
 * Description: This file contains code to send and receive data from the CSR 
 * BCSP chip interface.
 *             
 * Copyright 2006-2007 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any means, 
 * or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions,  
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "config.h"
#if BT_EXPOSE_BCCMD == XA_ENABLED
#include "sys/abcsphci.h"
#include "sys/xa_abcsp.h"
#include "abcsp.h"
#include "sys/bccmd.h"

/*---------------------------------------------------------------------------
 *            bccmdSend()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  This function sends a BCCMD command string to a CSR chip using 
 *            the ABCSP interface.
 *
 * Parameters:  (See header file)
 *
 * Return:    (See header file)
 *
 */
BCCMD_RESULT bccmdSend(char *buf, int size)
{
    BCCMD_RESULT   retVal;
    ITERATION_ENUM copyState;
    MSG_PUMP_ENUM  msgsToPump;
    unsigned       sentResult;

    if (MAX_BCCMD < size) {
        retVal = BCCMD_BUFF_TOO_LARGE;
    } else {
        if (BU_EMPTY == ABCSP(bccmdTXMsg.use)) {
            ABCSP(bccmdTXMsg.use) = BU_FILLING;
            Assert(0 == ABCSP(bccmdTXMsg.wrNdx));
            do {
                copyState = IE_DONE;
                if (size > ABCSP(bccmdTXMsg.wrNdx)) {
                    ABCSP(bccmdTXBuf.data[ABCSP(bccmdTXMsg.wrNdx)]) 
                            = *(buf + ABCSP(bccmdTXMsg.wrNdx));
                    ABCSP(bccmdTXMsg.wrNdx) += 1;
                    copyState = IE_ITERATING;
                }
            } while(IE_ITERATING == copyState);
            ABCSP(bccmdTXMsg.use) = BU_FULL;
            OS_StopHardware();
            sentResult 
                  = abcsp_sendmsg((ABCSP_TXMSG *)(&ABCSP(bccmdTXMsg)),
                                  BCSP_CHAN_BCCMD, 
                                  RELIABLE_CHANNEL);
            if (1 == sentResult) {
                do {
                    abcsp_pumptxmsgs();
                    if (TRUE == ABCSP(pumpTx)) {
                        msgsToPump = MP_MORE;
                        ABCSP(pumpTx) = FALSE;
                    } else {
                        msgsToPump = MP_NO_MORE;
                    }
                } while(MP_MORE == msgsToPump);
                retVal = BCCMD_OK;
            } else {
                retVal = BCCMD_NOT_SENDING;
            }
            OS_ResumeHardware();
        } else {
            retVal = BCCMD_PREV_UNFINISHED;
        }
    }
    return(retVal);
}

/*---------------------------------------------------------------------------
 *            bccmdGet()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  This function receives a BCCMD reply string from a CSR chip 
 *            using the ABCSP interface.
 *
 * Parameters:  (See header file)
 *
 * Return:    (See header file)
 *
 */
BCCMD_RESULT bccmdGet(char *buf, int size, int *bytesReadP)
{
    BCCMD_RESULT   retVal;
    ITERATION_ENUM copyState;

    *bytesReadP = 0;
    switch(ABCSP(bccmdRXMsg.use)) {
    case BU_EMPTY:
        retVal = BCCMD_NO_DATA_RECEIVED;
        break;
    case BU_FULL:
        *bytesReadP = 0;
        do {
            copyState = IE_DONE;
            if (ABCSP(bccmdRXMsg.wrNdx) > ABCSP(bccmdRXMsg.rdNdx)) {
                if (size > *bytesReadP) {
                    *(buf + *bytesReadP) 
                        = ABCSP(bccmdRXBuf.data[ABCSP(bccmdRXMsg.rdNdx)]);
                    *bytesReadP = *bytesReadP + 1;
                    ABCSP(bccmdRXMsg.rdNdx) = ABCSP(bccmdRXMsg.rdNdx) + 1;
                    copyState = IE_ITERATING;
                } else {
                    retVal = BCCMD_BUFF_TOO_SMALL;
                }
            } else {
                if (BL_NO_LOSS == ABCSP(bccmdRXMsg.lossState)) {
                    retVal = BCCMD_OK;
                } else {
                    retVal = BCCMD_DATA_LOST;
                }
            }
        } while(IE_ITERATING == copyState);
        ABCSP(bccmdRXMsg.use) = BU_EMPTY;
        ABCSP(bccmdRXMsg.lossState) = BL_NO_LOSS;
        ABCSP(bccmdRXMsg.rdNdx) = 0;
        ABCSP(bccmdRXMsg.wrNdx) = 0;
        break;
    case BU_FILLING:
    default:
        retVal = BCCMD_RXMSG_FILLING;
    }
    return(retVal);
}

#endif /* BT_EXPOSE_BCCMD == XA_ENABLED */ 
