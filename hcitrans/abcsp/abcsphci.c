/****************************************************************************
 *
 * File:
 *     $Workfile:abcsphci.c$ for iAnywhere Blue SDK, Version 2.2
 *     $Revision:34$
 *
 * Description:
 *     Code for ABCSP transport driver.
 * 
 * Copyright 1999-2005 Extended Systems, Inc.
 * Portions copyright 2005-2007 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions,
 * Inc.  This work contains confidential and proprietary information of
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret,
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "osapi.h"
#include "uart.h"
#include "sys/abcsphci.h"
#include "sys/evmxp.h"
#include "abcsp.h"
#include "sys/xa_abcsp.h"

#ifdef DEMO
#undef Report
#define Report(S)
#endif

#define BUFTYPE_MASK   (HCI_BUFTYPE_COMMAND|HCI_BUFTYPE_EVENT|HCI_BUFTYPE_ACL_DATA|HCI_BUFTYPE_SCO_DATA)

/* Local functions */
static void abcspEventHandler(U8 event);
static void abcspTransmitData(void);
static void abcspReadData(void);
static void abcspWriteComplete(void);
static void ABCSP_SendData(HciPacket *hciPacket);
static void ABCSP_BufferAvailable(HciBufFlags buffType);
static void ABCSP_SetSpeed(U16 speed);


/****************************************************************************
 *
 *  RAM Data
 *
 ****************************************************************************/

#if (XA_CONTEXT_PTR == XA_ENABLED)
/* If context pointers are used create a pointer called xprt 
 */
ABCSP_Context xprtTemp;
ABCSP_Context *xprt = &xprtTemp;

#elif (XA_CONTEXT_PTR == XA_DISABLED)
/* If context pointers are NOT used create a static object.
 */
ABCSP_Context xprt;

#endif

/*---------------------------------------------------------------------------
 * ABCSP_Init() 
 *
 *     Initialize the ABCSP transport and UART hardware drivers.
 */
BtStatus ABCSP_Init(TranCallback tranCallback)
{
    /* Clear the transport's context memory */
#if (XA_CONTEXT_PTR == XA_ENABLED)
    OS_MemSet((U8 *)xprt, 0, sizeof(ABCSP_Context));
#elif (XA_CONTEXT_PTR == XA_DISABLED)
    OS_MemSet((U8 *)&xprt, 0, sizeof(ABCSP_Context));
#endif

    ABCSP(callback) = tranCallback;

    /* BCSP limits this #define to the range 1 - 7. If this changes
     * the size of ABCSP(txMsgInuse) will need to be increased.
     */
    Assert(ABCSP_TXWINSIZE+1 <= 8);

    /* Initialize RX/TX queues */
    InitializeListHead(&(ABCSP(txQueue)));
#if XA_DEBUG == XA_ENABLED
    InitializeListHead(&(ABCSP(txEnroute)));
#endif

    ABCSP(tranEntry).sendData = ABCSP_SendData;
    ABCSP(tranEntry).buffAvail = ABCSP_BufferAvailable;
    ABCSP(tranEntry).setSpeed = ABCSP_SetSpeed;
#if BT_EXPOSE_BCCMD == XA_ENABLED
    ABCSP(bccmdRXMsg.use) = BU_EMPTY;
    ABCSP(bccmdRXMsg.lossState) = BL_NO_LOSS;
    ABCSP(bccmdRXMsg.rdNdx) = 0;
    ABCSP(bccmdRXMsg.wrNdx) = 0;
    ABCSP(bccmdTXMsg.use) = BU_EMPTY;
    ABCSP(bccmdTXMsg.lossState) = BL_NO_LOSS;
    ABCSP(bccmdTXMsg.rdNdx) = 0;
    ABCSP(bccmdTXMsg.wrNdx) = 0;
#endif
    HCI_RegisterTransport(&ABCSP(tranEntry));

    /* Init the serial interface */
    return UART_Init(abcspEventHandler);
}


/*---------------------------------------------------------------------------
 * ABCSP_Shutdown() 
 *
 *     Deinitialize the UART transport and hardware drivers
 */
BtStatus ABCSP_Shutdown(void)
{
    if (ABCSP(timers.tconf).context == (void *)1)
        EVM_CancelTimer(&ABCSP(timers.tconf));

    if (ABCSP(timers.tshy).context == (void *)1)
        EVM_CancelTimer(&ABCSP(timers.tshy));

    if (ABCSP(timers.bcsp).context == (void *)1)
        EVM_CancelTimer(&ABCSP(timers.bcsp));

    if (ABCSP(timers.pumpRx).context == (void *)1)
        EVM_CancelTimer(&ABCSP(timers.pumpRx));

    return UART_Shutdown();
}



/*---------------------------------------------------------------------------
 * ABCSP_SendData() 
 *      Send a packet to the BT host via a UART interface.
 *      This function is called from HCI_Process, part of the EVM_Process or
 *      stack thread.
 *      
 * Requires:
 *     
 * Parameters:
 *
 * Returns:
 *      BT_STATUS_PENDING
 */
void ABCSP_SendData(HciPacket *hciPacket)
{
    /* Because we're manipulating sensitive data structures, we must
     * request that the hardware thread/interrupt does not execute.
     */
    OS_StopHardware();
    InsertTailList(&ABCSP(txQueue), (ListEntry *)&hciPacket->node);
    Assert(ABCSP(pumpTx) == FALSE);

    abcspTransmitData();

    /* Call abcsp_pumptxmsgs() now before we return if requested. */
    while (ABCSP(pumpTx) == TRUE) {
        ABCSP(pumpTx) = FALSE;
        abcsp_pumptxmsgs();
    }

    OS_ResumeHardware();
}


/*---------------------------------------------------------------------------
 * ABCSP_BufferAvailable() 
 *
 *     A receive buffer is avialable
 */
static void ABCSP_BufferAvailable(HciBufFlags buffType)
{
    BOOL oldPump;
    if (ABCSP(rxState) == 1) { 
        oldPump = ABCSP(pumpTx);
        ABCSP(pumpTx) = FALSE;
        abcspEventHandler(UE_DATA_TO_READ);
        ABCSP(pumpTx) = oldPump;
    }
}

/*---------------------------------------------------------------------------
 * ABCSP_SetSpeed() 
 *
 *     Set the UART baud rate
 */
void ABCSP_SetSpeed(U16 speed)
{
        UART_SetSpeed(speed);
}


/*---------------------------------------------------------------------------
 * abcspEventHandler()
 *
 *     Called from UART hardware driver to indicate Rx/Tx done events.
 */
 U32 rcheck = 0;
static void abcspEventHandler(U8 event)
{
    if(rcheck > 5)
    {
  //      DPrintf("cow recursive = %x", rcheck);
//        __asm("break 100");
    }
    rcheck++;
    Assert(ABCSP(pumpTx) == FALSE);

    switch (event) {
    case UE_WRITE_COMPLETE:
        abcspWriteComplete();
        break;

    case UE_DATA_TO_READ:
        abcspReadData();
        abcspTransmitData();
        break;

    case UE_INIT_COMPLETE:
        /* Initialize the ABCSP layer */ 
        abcsp_init();

        ABCSP(callback)(TRAN_INIT_STATUS, BT_STATUS_SUCCESS);
        break;

    case UE_SHUTDOWN_COMPLETE:
        ABCSP(callback)(TRAN_SHUTDOWN_STATUS, BT_STATUS_SUCCESS);
        break;
    default:
        /* Invalid event received */
        Assert(FALSE);
        break;
    }        

    /* Call abcsp_pumptxmsgs() now before we return if requested. */
    while (ABCSP(pumpTx) == TRUE) {
        ABCSP(pumpTx) = FALSE;
        abcsp_pumptxmsgs();
    }
    rcheck--;
}


/*---------------------------------------------------------------------------
 * abcspWriteComplete()
 *
 *     Called by the UART when a write has completed. Update the write offset
 *     to reflect data written. We only allow one write to be outstanding
 *     at a time.
 */
static void abcspWriteComplete(void)
{
    ABCSP(txUartWriteOffset) = 0;

    if (abcsp_pumptxmsgs() == 0)
        abcspTransmitData();
}


/*---------------------------------------------------------------------------
 * abcspTransmitData()
 *
 *      Transmits data through the UART port.
 *      This function may be called when an HCI packet is sent down from
 *      the stack. It is also called on the controller context when
 *      it's ready to transmit more data.
 */
void abcspTransmitData(void)
{
    HciPacket   *tx;
    unsigned     channel;

    if (ABCSP(abcspInit) < 2) {
        return;
    }
    
    while (!IsListEmpty(&ABCSP(txQueue))) 
    {
        tx = (HciPacket *)GetHeadList(&ABCSP(txQueue));

        switch (tx->flags & BUFTYPE_MASK) {
        case HCI_BUFTYPE_COMMAND:
            channel = BCSP_CHAN_HCI;
            break;
        case HCI_BUFTYPE_ACL_DATA:
            channel = BCSP_CHAN_ACL;
            break;
#if NUM_SCO_CONNS != 0
        case HCI_BUFTYPE_SCO_DATA:
            channel = BCSP_CHAN_SCO;
            break;
#endif /* NUM_SCO_CONNS != 0 */
        default:
            Assert(0);
            break;
        }

        if (abcsp_sendmsg(tx, channel, TRUE) == 0) 
        {
            /* BCSP is not accepting transmit packets */
            break;
        }

        /* Packet has been accepted by BCSP, move to enroute queue. */
        RemoveHeadList(&ABCSP(txQueue));
#if XA_DEBUG == XA_ENABLED
        InsertTailList(&ABCSP(txEnroute), &tx->node);
#endif
    }

    abcsp_pumptxmsgs();
}


/*---------------------------------------------------------------------------
 * readPump()
 *
 *     Called by a quick timer to retry delivering received UART data
 *     to the ABCSP layer.
 */
void readPump(EvmTimer *Timer)
{
    OS_StopHardware();

    ABCSP(timers.pumpRx).context = 0;

    abcspEventHandler(UE_DATA_TO_READ);

    OS_ResumeHardware();
}


/*---------------------------------------------------------------------------
 * abcspReadData() 
 *
 *      Called from the UART when data is available for reading.
 */
void abcspReadData(void)
{
    U16     len, avail;
    U8     *buff = ABCSP(rxUartBuff) + ABCSP(rxUartWriteOffset);

    avail = UART_RX_BUFF_SIZE - ABCSP(rxUartWriteOffset);

    len = UART_Read(buff, avail);
    Assert(len <= avail);
    
    /* Discard all receives until the abcsp driver is initialized. */
    if (ABCSP(abcspInit) == 0) {
        return;
    }

    ABCSP(rxUartWriteOffset) += len;

    if (ABCSP(rxUartWriteOffset) == 0) {
        return;
    }

    avail = ABCSP(rxUartWriteOffset) - ABCSP(rxUartReadOffset);

    /* Pass received data to ABCSP for processing */
    len = abcsp_uart_deliverbytes((char *)(ABCSP(rxUartBuff)+ABCSP(rxUartReadOffset)),
                                   avail);

    ABCSP(rxUartReadOffset) += len;

    if (ABCSP(rxUartReadOffset) == ABCSP(rxUartWriteOffset)) {
        /* Emptied UART receive buffer */
        ABCSP(rxUartReadOffset) = ABCSP(rxUartWriteOffset) = 0;
    } else {
        /* We have unread data, schedule this function to retry
         * abcsp_uart_deliverbytes() to deliver remaining data. 
         */
        if (ABCSP(timers.pumpRx).context == 0) {
            ABCSP(timers.pumpRx).func = readPump;
            ABCSP(timers.pumpRx).context = (void *)1;
            TRAN_StartTimer(&ABCSP(timers.pumpRx), 1);
        }
    }
}


