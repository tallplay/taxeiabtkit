/****************************************************************************
FILE
        abcsp_test.h  -  mappings to wire abcsp to test environment

CONTAINS

DESCRIPTION
    This file holds rough test code mappings, to be used where ABCSP_TEST
    is #defined.

MODIFICATION HISTORY
        1.1   6:jul:01   cjo    Created.
*/

#ifndef __ABCSP_TEST_H__
#define __ABCSP_TEST_H__
 
#ifdef RCS_STRINGS
static const char abcsp_test_h_id[]
  = "$Id:abcsp_test.h,v 1.2, 2005-12-12 18:37:34Z, Glade Diviney$";
#endif

#ifdef ABCSP_TEST

#include "abcsp.h"
#include "config_event.h"
#include "config_panic.h"
#include "config_malloc.h"
#include "config_rxmsg.h"
#include "config_timer.h"
#include "config_txmsg.h"
#include "config_le.h"


extern void test_abcsp_event(unsigned e);
extern void test_abcsp_req_pumptxmsgs(void);
extern void test_abcsp_panic(unsigned e);
extern ABCSP_RXMSG *test_abcsp_rxmsg_create(unsigned chan, unsigned len);
extern char *test_abcsp_rxmsg_getbuf(ABCSP_RXMSG *m, unsigned *n);
extern void test_abcsp_rxmsg_write(ABCSP_RXMSG *m, char *buf, unsigned n);
extern void test_abcsp_rxmsg_complete(ABCSP_RXMSG *m);
extern void test_abcsp_delivermsg(ABCSP_RXMSG *m, unsigned chan, unsigned rel);
extern void test_abcsp_rxmsg_destroy(ABCSP_RXMSG *m);
extern void test_abcsp_cancel_bcsp_timer(void);
extern void test_abcsp_cancel_tshy_timer(void);
extern void test_abcsp_cancel_tconf_timer(void);
extern void test_abcsp_start_bcsp_timer(void);
extern void test_abcsp_start_tshy_timer(void);
extern void test_abcsp_start_tconf_timer(void);
extern void test_abcsp_txmsg_init_read(ABCSP_TXMSG *msg);
extern unsigned test_abcsp_txmsg_length(ABCSP_TXMSG *msg);
extern char *test_abcsp_txmsg_getbuf(ABCSP_TXMSG *msg, unsigned *buflen);
extern void test_abcsp_txmsg_taken(ABCSP_TXMSG *msg, unsigned ntaken);
extern void test_abcsp_txmsg_done(ABCSP_TXMSG *msg);
extern char *test_abcsp_uart_gettxbuf(unsigned *bufsiz);
extern void test_abcsp_uart_sendbytes(char *buf, unsigned n);
extern void test_abcsp_txmsg_done(ABCSP_TXMSG *msg);

#endif /* ABCSP_TEST */


#endif  /* __ABCSP_TEST_H__ */
