/****************************************************************************
FILE
        config_panic.h  -  wire to an panic-reporting function

CONTAINS
        ABCSP_PANIC  -  report an panic from the abcsp code

DESCRIPTION
    If the abcsp code detects something disastrously wrong, typically
    something that is supposed to be "impossible", it can call
    ABCSP_PANIC().  The single argument reports the apparent disastrous
    occurrence.  This file defines the panic function.

    It is presumed external code will not call any of the abcsp code
    again until it has cleaned up the mess.

MODIFICATION HISTORY
        1.1   6:jul:01   cjo    Created.
*/

#ifndef __CONFIG_PANIC_H__
#define __CONFIG_PANIC_H__
 
#ifdef RCS_STRINGS
static const char config_panic_h_id[]
  = "$Id:config_panic.h,v 1.2, 2005-12-12 18:37:35Z, Glade Diviney$";
#endif


/****************************************************************************
NAME
        ABCSP_PANIC  -  report an panic from the abcsp code

SYNOPSIS
        void ABCSP_PANIC(unsigned e);

FUNCTION
    Reports the occurrence of the panic "e".   Values for "e" are given
    in abcsp_panics.h.

NOTE
    Unlike ABCSP_EVENT(), it is not acceptable to #define this to be
    nothing.
*/

#ifdef ABCSP_TEST
#define ABCSP_PANIC(n)           test_abcsp_panic(n)
#endif /* ABCSP_TEST */


#endif  /* __CONFIG_PANIC_H__ */
