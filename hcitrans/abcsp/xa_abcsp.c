/***************************************************************************
 *
 * File:
 *     xa_abcsp.c
 *
 * Description:
 *      HCI Transport - ABCSP integration functions.
 *      This file provides the interface between the ABCSP layer and the
 *      HCI transport on the top and the UART transport on the bottom.
 *
 * Copyright 1999-2005 Extended Systems, Inc.
 * Portions copyright 2005-2007 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "osapi.h"
#include "uart.h"
#include "sys/abcsphci.h"
#include "sys/evmxp.h"

#include "abcsp.h"
#include "abcsp_events.h"
#include "sys/xa_abcsp.h"

#ifdef DEMO
#undef Report
#define Report(S)
#endif

static void xa_timerfire(EvmTimer *Timer);
#if XA_DEBUG == XA_ENABLED
static const char *pBcspEvent(unsigned e);
#endif /* XA_DEBUG == XA_ENABLED */

/*---------------------------------------------------------------------------
 * xa_req_pumptxmsgs()
 *
 *     Request external code call abcsp_pumptxmsgs.
 */
void xa_req_pumptxmsgs(void)
{
    /* Setting this flag will cause abcsp_pumptxmsgs() to be called
     * before this thread of execution returns from the driver.
     */
    ABCSP(pumpTx) = TRUE;
}


/*---------------------------------------------------------------------------
 * xa_rxmsg_create()
 *
 *     Create a abcsp receive message.
 */
ABCSP_RXMSG *xa_rxmsg_create(unsigned chan, unsigned len)
{
    ABCSP_RXMSG *retVal;

#if BT_EXPOSE_BCCMD == XA_ENABLED
    if (BCSP_CHAN_BCCMD == chan) {
        if (BU_EMPTY == ABCSP(bccmdRXMsg.use)) {
            ABCSP(bccmdRXMsg.lossState) = BL_NO_LOSS;
        } else {
            ABCSP(bccmdRXMsg.lossState) = BL_DATA_LOST;
        }
        ABCSP(bccmdRXMsg.use) = BU_FILLING;
        ABCSP(bccmdRXMsg.rdNdx) = 0;
        ABCSP(bccmdRXMsg.wrNdx) = 0;
        retVal = (ABCSP_RXMSG *)(&ABCSP(bccmdRXMsg));
    } else {
#endif
        ABCSP(rxChannel) = chan;
        ABCSP(rxBuffSize) = len;
        ABCSP(rxWriteOffset) = 0;

        switch (ABCSP(rxChannel)) {
        case BCSP_CHAN_HCI:
            ABCSP(rxState) = 1;     /* Query HCI for receive buffer */
            break;

        case BCSP_CHAN_ACL:
        case BCSP_CHAN_SCO:
            ABCSP(rxState) = 0;     /* Stage HCI ConnHandle before buffer query */
            break;
    
        default:
            /* Don't know why but ABCSP asks us to create msgs for their internal
             * channel, but we deny the request (their always 0 length anyway).
             */
            return 0;
            break;
        }

        /* We don't know the rxmsg value yet so just return 1. */
        retVal = (ABCSP_RXMSG *)1;
#if BT_EXPOSE_BCCMD == XA_ENABLED
    }
#endif
    return(retVal);
}    


/*---------------------------------------------------------------------------
 * xa_rxmsg_getbuf()
 *
 *     Obtain storage to write to a message.
 */
char *xa_rxmsg_getbuf(ABCSP_RXMSG *m, unsigned *len)
{
    U16             connHndl;
    BtStatus        status;
    ABCSP_RXMSG    *buff = 0;
#if BT_EXPOSE_BCCMD == XA_DISABLED
    UNUSED_PARAMETER(m);
#endif
    *len = 0;

#if BT_EXPOSE_BCCMD == XA_ENABLED
    if (m == &ABCSP(bccmdRXMsg)) {
        buff = (ABCSP_RXMSG *)(&ABCSP(bccmdRXBuf.data[0]));
        *len = MAX_BCCMD;
    } else {
#endif
        switch (ABCSP(rxState)) {
        case 0:
            Assert(ABCSP(rxWriteOffset) < 2);

            /* Initial receive for ACL & SCO Data. We need to know the HCI
             * connection handle before we query for a receive buffer. So 
             * we provide an internal 2-byte buffer to receive the handle
             */
            *len = 2 - ABCSP(rxWriteOffset);
            buff = (ABCSP_RXMSG *)(ABCSP(rxStage) + ABCSP(rxWriteOffset));
            break;

        case 1:
            connHndl = LEtoHost16(ABCSP(rxStage));

            switch (ABCSP(rxChannel)) {
            case BCSP_CHAN_HCI:
                status = HCI_GetEventBuffer(&ABCSP(rxBuff), ABCSP(rxBuffSize));
                break;
            case BCSP_CHAN_ACL:
                status = HCI_GetAclBuffer(&ABCSP(rxBuff), connHndl, ABCSP(rxBuffSize));
                break;
            case BCSP_CHAN_SCO:
                status = HCI_GetScoBuffer(&ABCSP(rxBuff), connHndl, ABCSP(rxBuffSize));
                break;
            default:
                Assert(0);
                break;
            }


            if (status != BT_STATUS_SUCCESS) {
                break;
            }
            ABCSP(rxState)++;

            /* Copy staged conn handle bytes into RX buffer */
            if (ABCSP(rxChannel) != BCSP_CHAN_HCI)
                OS_MemCopy(HCI_GetRxPtr(ABCSP(rxBuff)), ABCSP(rxStage), 2);

            /* Drop into next case to set buffer and length return values */

        case 2:
            *len = ABCSP(rxBuffSize) - ABCSP(rxWriteOffset);
            buff = HCI_GetRxPtr(ABCSP(rxBuff)) + ABCSP(rxWriteOffset);
            break;
        }
#if BT_EXPOSE_BCCMD == XA_ENABLED
    }
#endif
    return (char *)buff;
}


/*---------------------------------------------------------------------------
 * xa_rxmsg_write()
 *
 *     Write to a higher-level message.
 */
void xa_rxmsg_write(ABCSP_RXMSG *m, char *buf, unsigned n)
{
#if BT_EXPOSE_BCCMD == XA_ENABLED
    if (m == (ABCSP_RXMSG *)(&ABCSP(bccmdRXMsg))) {
        Assert(MAX_BCCMD - (int)n > ABCSP(bccmdRXMsg.wrNdx));
        ABCSP(bccmdRXMsg.wrNdx) += n;
    } 
    else 
    {
#else
        UNUSED_PARAMETER(m);
#endif

    ABCSP(rxWriteOffset) += n;

    if ((ABCSP(rxState) == 0) && (ABCSP(rxWriteOffset) == 2))
        ABCSP(rxState)++;
#if BT_EXPOSE_BCCMD == XA_ENABLED
    }
#endif
}


/*---------------------------------------------------------------------------
 * xa_rxmsg_complete()
 *
 *     Finish a higher-level message.
 */
void xa_rxmsg_complete(ABCSP_RXMSG *m)
{
#if BT_EXPOSE_BCCMD == XA_ENABLED
    if (m == &ABCSP(bccmdRXMsg)) {
        ABCSP(bccmdRXMsg.use) = BU_FULL;
    } else {
#else
    UNUSED_PARAMETER(m);
#endif
         /* Nothing to do, xa_delivermsg() will do the work. */
         Assert(ABCSP(rxWriteOffset) == ABCSP(rxBuffSize));
#if BT_EXPOSE_BCCMD == XA_ENABLED
    }
#endif
}

/* NOTE: The CSR radio's BCSP transport can be configered (via
 * PSKEY_HOSTIO_PROTOCOL_INFO5) to use a smaller-than-default
 * "max_tx_payload_size", limiting the size of incoming messages.
 * If this value is smaller than the size of an event delivered
 * by the radio (such as a remote name query) the event will
 * be segmented into multiple BCSP messages.
 *
 * The code in xa_delivermsg was written to address this problem by
 * detecting an Event packet's internal size and reassembling BCSP messages
 * into the original event. This code is unnecessary if max_tx_payload
 * size is set above the maximum HCI Event size, as it should be.
 *
 * If the CSR radio is configured normally, this code is not necessary.
 * Otherwise, define the symbol below.
 */
#undef BCSP_SEGMENT_EVENT_FIX

#ifdef BCSP_SEGMENT_EVENT_FIX
static RxBuffHandle  rxIncomplete = 0;
static U16           rxIncompletePos = 0;
#endif /* BCSP_SEGMENT_EVENT_FIX */

/*---------------------------------------------------------------------------
 * xa_delivermsg()
 *
 *     Pass abcsp message to higher level code.
 */
void xa_delivermsg(ABCSP_RXMSG *m, unsigned chan, unsigned rel)
{
#ifdef BCSP_SEGMENT_EVENT_FIX
    U16 length;
#endif /* BCSP_SEGMENT_EVENT_FIX */
    UNUSED_PARAMETER(m);
    UNUSED_PARAMETER(rel);
#if BT_EXPOSE_BCCMD == XA_ENABLED
    if (chan == BCSP_CHAN_BCCMD) {
        Assert(BU_FULL == ABCSP(bccmdRXMsg.use));
    } else {
#else
    UNUSED_PARAMETER(chan);
#endif
        Assert(ABCSP(rxChannel) == chan);
        Assert(ABCSP(rxBuff));

#ifdef BCSP_SEGMENT_EVENT_FIX
        /* Detect and reassemble segmented event packets */
        if (chan == BCSP_CHAN_HCI) 
        {
            /* Is there an outstanding incomplete event packet? */
            if (rxIncomplete) 
            {
                /* Tack new event data onto the end of the packet */
                Assert(ABCSP(rxBuff)->len + rxIncompletePos 
                      <= rxIncomplete->len);

                OS_MemCopy(rxIncomplete->buffer + rxIncompletePos,
                           ABCSP(rxBuff)->buffer,
                           ABCSP(rxBuff)->len);
                rxIncompletePos += ABCSP(rxBuff)->len;

                /* If the packet is complete plan to hand it up */
                if (rxIncompletePos == rxIncomplete->len) {
                    /* Return the partial packet */
                    HCI_RxBufferInd(ABCSP(rxBuff), BT_STATUS_FAILED);

                    /* Hand off rxIncomplete as the packet to be delivered */
                    ABCSP(rxBuff) = rxIncomplete;
                    rxIncomplete = 0;
                } else {
                    /* Otherwise discard rxBuff: there's more data coming */
                    HCI_RxBufferInd(ABCSP(rxBuff), BT_STATUS_FAILED);
                    ABCSP(rxBuff) = 0;
                }
            } 
            else 
            {
                /* Check this event packet to see if it is incomplete */
                Assert(ABCSP(rxBuff)->len >= 2);
                length = ABCSP(rxBuff)->buffer[1] + 2;

                /* If the length of the received buffer is less than the
                 * advertised size, we must create a buffer to receive the
                 * entire event.
                 */
                if (ABCSP(rxBuff)->len < length)
                {
                    /* Re-allocate its size to the correct length */
                    HCI_SetBufferLen(ABCSP(rxBuff), length);

                    /* Kidnap this buffer */
                    rxIncomplete = ABCSP(rxBuff);
                    rxIncompletePos = ABCSP(rxBuff)->len;
                    ABCSP(rxBuff) = 0;
                }
            }
        }
#endif /* BCSP_SEGMENT_EVENT_FIX */

        /* If we still have a buffer, provide it */
        if (ABCSP(rxBuff)) {
            HCI_RxBufferInd(ABCSP(rxBuff), BT_STATUS_SUCCESS);
            ABCSP(rxBuff) = 0;
        }
#if BT_EXPOSE_BCCMD == XA_ENABLED
    }
#endif
}


/*---------------------------------------------------------------------------
 * xa_rxmsg_destroy()
 *
 *     Destroy a abcsp receive message.
 */
void xa_rxmsg_destroy(ABCSP_RXMSG *m)
{
#if BT_EXPOSE_BCCMD == XA_ENABLED
    if (m == &ABCSP(bccmdRXMsg)) {
        ABCSP(bccmdRXMsg.use) = BU_EMPTY;
        ABCSP(bccmdRXMsg.lossState) = BL_NO_LOSS;
        ABCSP(bccmdRXMsg.rdNdx) = 0;
        ABCSP(bccmdRXMsg.wrNdx) = 0;
    } else {
#else
    UNUSED_PARAMETER(m);
#endif
        if (ABCSP(rxBuff) == 0) {
            return;
        }

        HCI_RxBufferInd(ABCSP(rxBuff), BT_STATUS_FAILED);

        ABCSP(rxBuff) = 0;
#if BT_EXPOSE_BCCMD == XA_ENABLED
    }
#endif
}

/*---------------------------------------------------------------------------
 * xa_start_bcsp_timer()
 *
 *     Start the bcsp ack timeout timer.
 */
void xa_start_bcsp_timer(void)
{
    ABCSP(timers.bcsp).func = xa_timerfire;
    ABCSP(timers.bcsp).context = (void *)1;

    TRAN_StartTimer(&ABCSP(timers.bcsp), TIMER_BCSP_TIMEOUT);
}

/*---------------------------------------------------------------------------
 * xa_start_tshy_timer()
 *
 *     Start the bcsp-le tshy timer.
 */
void xa_start_tshy_timer(void)
{
    ABCSP(timers.tshy).func = xa_timerfire;
    ABCSP(timers.tshy).context = (void *)1;

    TRAN_StartTimer(&ABCSP(timers.tshy), TIMER_TSHY_TIMEOUT);
}

/*---------------------------------------------------------------------------
 * xa_start_tconf_timer()
 *
 *     Start the bcsp-le tconf timer.
 */
void xa_start_tconf_timer(void)
{
    ABCSP(timers.tconf).func = xa_timerfire;
    ABCSP(timers.tconf).context = (void *)1;

    TRAN_StartTimer(&ABCSP(timers.tconf), TIMER_TCONF_TIMEOUT);
}

/*---------------------------------------------------------------------------
 * xa_cancel_bcsp_timer()
 *
 *     Cancel the bcsp ack timeout timer.
 */
void xa_cancel_bcsp_timer(void)
{
    if (ABCSP(timers.bcsp).context == (void *)1) {
        TRAN_CancelTimer(&ABCSP(timers.bcsp));
        ABCSP(timers.bcsp).context = 0;
    }
}

/*---------------------------------------------------------------------------
 * xa_cancel_tshy_timer()
 *
 *     Cancel the bcsp-le tshy timer.
 */
void xa_cancel_tshy_timer(void)
{
    if (ABCSP(timers.tshy).context == (void *)1) {
        TRAN_CancelTimer(&ABCSP(timers.tshy));
        ABCSP(timers.tshy).context = 0 ;
    }
}

/*---------------------------------------------------------------------------
 * xa_cancel_tconf_timer()
 *
 *     Cancel the bcsp-le tconf timer.
 */
void xa_cancel_tconf_timer(void)
{
    if (ABCSP(timers.tconf).context == (void *)1) {
        TRAN_CancelTimer(&ABCSP(timers.tconf));
        ABCSP(timers.tconf).context = 0 ;
    }
}

/*---------------------------------------------------------------------------
 * xa_txmsg_init_read()
 *
 *     Initialise reading a bcsp transmit message.
 */
void xa_txmsg_init_read(ABCSP_TXMSG *msg)
{
#if BT_EXPOSE_BCCMD == XA_ENABLED
    if (msg == (ABCSP_TXMSG *)(&ABCSP(bccmdTXMsg))) {

        /* If ABCSP(bccmdTXMsg.rdNdx) is not equal to 
         * zero, then the message was sent but was not 
         * acknowledged, so we are about to send it again.
         */
        ABCSP(bccmdTXMsg.rdNdx) = 0;

    } else {
#else
    UNUSED_PARAMETER(msg);
#endif
        ABCSP(txFrag) = 0;
        ABCSP(txReadOffset) = 0;
#if BT_EXPOSE_BCCMD == XA_ENABLED
    }
#endif
}

/*---------------------------------------------------------------------------
 * xa_txmsg_length()
 *
 *     How long is a transmit message.
 */
unsigned xa_txmsg_length(ABCSP_TXMSG *msg)
{
    HciPacket  *tx;
    unsigned    len, i;

#if BT_EXPOSE_BCCMD == XA_ENABLED
    if (msg == (ABCSP_TXMSG *)(&ABCSP(bccmdTXMsg))) {
        Assert(ABCSP(bccmdTXMsg.rdNdx) <= ABCSP(bccmdTXMsg.wrNdx));
        len = ABCSP(bccmdTXMsg.wrNdx) - ABCSP(bccmdTXMsg.rdNdx);
    } else {
#endif
        tx = (HciPacket *)msg;
        /* Start with HCI header length */
        if (tx->flags & HCI_BUFTYPE_ACL_DATA)
            len = 4;
        else
            len = 3;   /* HCI_COMMAND & SCO_DATA */

        /* Add in length of all fragments */
        for (i = 0; i < tx->fCount; i++)
            len += tx->f[i].len;
#if BT_EXPOSE_BCCMD == XA_ENABLED
    }
#endif
    return len;
}


/*---------------------------------------------------------------------------
 * xa_txmsg_getbuf()
 *
 *     Obtain buffer for uart output.
 */
const char *xa_txmsg_getbuf(ABCSP_TXMSG *msg, unsigned *buflen)
{
    HciPacket   *tx;
    const char  *buff;

#if BT_EXPOSE_BCCMD == XA_ENABLED
    if (msg == (ABCSP_TXMSG *)(&ABCSP(bccmdTXMsg))) {
        Assert(ABCSP(bccmdTXMsg.rdNdx) <= ABCSP(bccmdTXMsg.wrNdx));
        if (ABCSP(bccmdTXMsg.rdNdx) < ABCSP(bccmdTXMsg.wrNdx)) {
            buff = &ABCSP(bccmdTXBuf.data[ABCSP(bccmdTXMsg.rdNdx)]);
            *buflen = ABCSP(bccmdTXMsg.wrNdx) - ABCSP(bccmdTXMsg.rdNdx);
        } else {
            /* Done transmitting this packet. */
            return 0;
        }
    } else {
#endif
        tx = (HciPacket *)msg;
        if (ABCSP(txFrag) > tx->fCount)
        {
            /* Done transmitting this packet. */
            return 0;
        }

        if (ABCSP(txFrag) == 0) 
        {
            if (tx->flags & HCI_BUFTYPE_ACL_DATA)
                *buflen = 4;
            else 
                *buflen = 3;   /* HCI_COMMAND & SCO_DATA */
            buff = (const char *)tx->header;
        } 
        else 
        {
            *buflen = tx->f[ABCSP(txFrag)-1].len;
            buff = (const char *)tx->f[ABCSP(txFrag)-1].buffer;
        }
        Assert(*buflen > ABCSP(txReadOffset));

        *buflen -= ABCSP(txReadOffset);
        buff += ABCSP(txReadOffset);
#if BT_EXPOSE_BCCMD == XA_ENABLED
    }
#endif
    return buff;
}

/*---------------------------------------------------------------------------
 * xa_txmsg_taken()
 *
 *     Tell message how many bytes have been read.
 */
void xa_txmsg_taken(ABCSP_TXMSG *msg, unsigned ntaken)
{
    HciPacket  *tx;
    unsigned    len;

#if BT_EXPOSE_BCCMD == XA_ENABLED
    if (msg == &ABCSP(bccmdTXMsg)) {
        ABCSP(bccmdTXMsg.rdNdx) += ntaken;
        Assert(ABCSP(bccmdTXMsg.rdNdx) <= ABCSP(bccmdTXMsg.wrNdx));
    } else {
#endif
        tx = (HciPacket *)msg;
        ABCSP(txReadOffset) += ntaken;

        if (ABCSP(txFrag) == 0) {
            if (tx->flags & HCI_BUFTYPE_ACL_DATA)
                len = 4;
            else
                len = 3;
        }
        else
            len = tx->f[ABCSP(txFrag)-1].len;

        /* If BCSP has consumed the entire fragment, advance the index. */
        if (ABCSP(txReadOffset) == len) {
            ABCSP(txReadOffset) = 0;
            ABCSP(txFrag)++;
        }
#if BT_EXPOSE_BCCMD == XA_ENABLED
    }
#endif
}

/*---------------------------------------------------------------------------
 * xa_txmsg_done()
 *
 *     Signal that message has been delivered.
 */
void xa_txmsg_done(ABCSP_TXMSG *msg)
{
#if BT_EXPOSE_BCCMD == XA_ENABLED
    if (msg == &ABCSP(bccmdTXMsg)) {
        ABCSP(bccmdTXMsg.use) = BU_EMPTY;
        ABCSP(bccmdTXMsg.lossState) = BL_NO_LOSS;
        ABCSP(bccmdTXMsg.rdNdx) = 0;
        ABCSP(bccmdTXMsg.wrNdx) = 0;
    } else {
#endif
        /* If ABCSP is initialized and running return the packet to the HCI.
         * Otherwise, the driver is purging itself during a reset call to 
         * abcsp_init(). In this case, the HCI has already reposessed these
         * buffers when we reported the transport error.
         */
        if (ABCSP(abcspInit) == 2) {
#if XA_DEBUG == XA_ENABLED
            Assert(IsNodeOnList(&ABCSP(txEnroute), &((HciPacket *)msg)->node));
            RemoveEntryList(&((HciPacket *)msg)->node);
#endif
            HCI_PacketSent((HciPacket *)msg);
        }
#if BT_EXPOSE_BCCMD == XA_ENABLED
    }
#endif
}

/*---------------------------------------------------------------------------
 * xa_uart_gettxbuf()
 *
 *     Access raw message bytes in a message. We only provide a message if
 *     there is no transmit in progress.
 */
char *xa_uart_gettxbuf(unsigned *bufsiz)
{
    if (ABCSP(txUartWriteOffset) == 0) {
        *bufsiz = UART_TX_BUFF_SIZE;
        return (char *)ABCSP(txUartBuff);
    }

    *bufsiz = 0;
    return 0;
}

/*---------------------------------------------------------------------------
 * xa_uart_sendbytes()
 *
 *     Pass a block of bytes to the output uart.
 */
void xa_uart_sendbytes(char *buf, unsigned n)
{
    Assert(ABCSP(txUartWriteOffset) == 0);

    ABCSP(txUartWriteOffset) = n;

    UART_Write(ABCSP(txUartBuff), ABCSP(txUartWriteOffset));
}

/*---------------------------------------------------------------------------
 * xa_event()
 *
 *     Report an event from the abcsp code.
 */
void xa_event(unsigned e)
{
    Report(("ABCSP: event() %s.\n", pBcspEvent(e)));

    /* This function handles mandatory events. */
    switch (e) {
    case ABCSP_EVT_START:
        ABCSP(abcspInit) = 0;
        break;

    case ABCSP_EVT_INITED:
        ABCSP(abcspInit) = 1;
        break;

    case ABCSP_EVT_LE_CONF:
        ABCSP(abcspInit) = 2;
        break;

    case ABCSP_EVT_LE_SYNC_LOST:
        HCI_TransportError();
        ABCSP(abcspInit) = 0;
        break;
    }
}


/*---------------------------------------------------------------------------
 * xa_panic()
 *
 *     Report a panic from the abcsp code.
 */
void xa_panic(unsigned int e)
{
    Report(("ABCSP: panic() %d.\n", e));
    Assert(0);
}


/*---------------------------------------------------------------------------
 * xa_malloc()
 *
 *     Allocate a block of memory.
 */
void *xa_malloc(unsigned n)
{
    unsigned i;

    Assert(n <= sizeof(TXMSG));

    for (i = 0; i < ABCSP_TXWINSIZE+1; i++) {
        if ((ABCSP(txMsgInuse) & (1 << i)) == 0) {
            /* Set inuse flag and return TXMSG pointer. */
            ABCSP(txMsgInuse) |= (1 << i);
            return ABCSP(txMsgPool)+i;
        }
    }

    return 0;
}

/*---------------------------------------------------------------------------
 * xa_zmalloc()
 *
 *     Allocate a block of zeroed memory.
 */
void *xa_zmalloc(unsigned n)
{
    void *mem;

    if ((mem = xa_malloc(n)) != 0)
        OS_MemSet(mem, 0, n);

    return mem;
}

/*---------------------------------------------------------------------------
 * xa_free()
 *
 *     Liberate a block of memory.
 */
void xa_free(void *m)
{
    TXMSG      *txm = (TXMSG *)m;
    unsigned    i;

    /* Calculate index of returned TXMSG. */
    i = txm - ABCSP(txMsgPool);

    Assert(i <= ABCSP_TXWINSIZE);
    Assert((ABCSP(abcspInit) > 1) ? (((1 << i) & ABCSP(txMsgInuse)) != 0) : 1);

    ABCSP(txMsgInuse) &= ~(1 << i);
}


/*---------------------------------------------------------------------------
 * xa_timerfire()
 *
 *     Centralized timer fire routine for all BCSP timers and transmit pump.
 */
static void xa_timerfire(EvmTimer *Timer) 
{
    OS_StopHardware();

    Assert(Timer->context == (void *)1);
    Assert(ABCSP(pumpTx) == FALSE);

    Timer->context = 0;

    if (Timer == &ABCSP(timers.bcsp)) {
        abcsp_bcsp_timed_event();
    } 
    else if (Timer == &ABCSP(timers.tconf)) {
        abcsp_tconf_timed_event();
    } 
    else if (Timer == &ABCSP(timers.tshy)) {
        abcsp_tshy_timed_event();
    } 
    else Assert(0);

    /* Call abcsp_pumptxmsgs() now before we return if requested. */
    while (ABCSP(pumpTx) == TRUE) {
        ABCSP(pumpTx) = FALSE;
        abcsp_pumptxmsgs();
    }

    OS_ResumeHardware();
}

#if XA_DEBUG == XA_ENABLED
/*---------------------------------------------------------------------------
 * pBcspEvent()
 *
 *     Converts BCSP event type to a string.
 */
static const char *pBcspEvent(unsigned e)
{
    switch (e) {
        /* Initialisation of the abcsp library has started. */
    case ABCSP_EVT_START: 
        return "INIT START";

        /* Initialisation of the abcsp library has been completed. */
    case ABCSP_EVT_INITED: 
        return "INITIALIZED";

        /* The BCSP Link Establishment engine has established sync with its peer
        and the choke has been removed locally.  Messages will now be accepted
        by abcsp_sendmsg() and abcsp_uart_deliverbytes(). */
    case ABCSP_EVT_LE_SYNC: 
        return "LE_SYNC";

        /* The BCSP Link Establishment engine has received confirmation from the peer
        that it's in its "garrulous" state, so allowing generation of the sync-lost
        event below. */
    case ABCSP_EVT_LE_CONF:
        return "LE_CONF";

        /* The BCSP Link Establishment engine has detected that the peer BCSP-LE
        engine has restarted.  This presumably means that the peer BCSP stack
        (or system) has been restarted.  A common local response would be to
        restart (abcsp_init()) the local BCSP stack. */
    case ABCSP_EVT_LE_SYNC_LOST:
        return "LE_SYNC_LOST";

        /* An attempt has been made to use an uninitialised block of code.  This
        should only mean that a call has been made to the abcsp library before
        abcsp_init() has been called. */
    case ABCSP_EVT_UNINITED: 
        return "UNINITED";

        /* The rx SLIP engine has established sync on the received byte stream. */
    case ABCSP_EVT_SLIP_SYNC: 
        return "SLIP_SYNC";

        /* The rx SLIP engine has lost sync on the received byte stream. */
    case ABCSP_EVT_SLIP_SYNC_LOST: 
        return "SLIP_SYNC_LOST";

        /* A message received by the BCSP stack has been discarded because the local
        choke is applied.  This is a reasonable occurrence where the peer BCSP stack
        has removed its choke (shortly) before the local choke is removed. */
    case ABCSP_EVT_RX_CHOKE_DISCARD: 
        return "RX_CHOKE_DISCARD";

        /* A message submitted to abcsp_sendmsg() has been rejected because the
        local choke is applied. */
    case ABCSP_EVT_TX_CHOKE_DISCARD: 
        return "TX_CHOKE_DISCARD";

        /* A message submitted to abcsp_sendmsg() has been rejected because the
        BCSP transmit window is full. */
    case ABCSP_EVT_TX_WINDOW_FULL_DISCARD: 
        return "TX_WINDOW_FULL_DISCARD";

        /* The header of a received message has declared its payload to be
        longer than the limit set by the #define ABCSP_RXMSG_MAX_PAYLOAD_LEN
        and the message is being discarded.  (See the #define's description
        in config_rxmsg.h.) */
    case ABCSP_EVT_OVERSIZE_DISCARD: 
        return "OVERSIZE_DISCARD";

        /* A received reliable message has been rejected because its sequence
        number is not that expected by the local BCSP stack. */
    case ABCSP_EVT_MISSEQ_DISCARD: 
        return "MISSEQ_DISCARD";

        /* A received message's header checksum has failed and the message is
        being discarded. */
    case ABCSP_EVT_CHECKSUM: 
        return "CHECKSUM Failed";

        /* A received message held fewer bytes than its header declared. */
    case ABCSP_EVT_SHORT_PAYLOAD: 
        return "SHORT_PAYLOAD";

        /* A received message held more bytes than its header declared. */
    case ABCSP_EVT_OVERRUN: 
        return "OVERRUN";

        /* A received message is being discarded because its CRC check failed. */
    case ABCSP_EVT_CRC_FAIL: 
        return "CRC_FAIL";

        /* A call to ABCSP_MALLOC() or ABCSP_ZMALLOC() has failed. */
    case ABCSP_EVT_MALLOC: 
        return "MALLOC Failed";
    }

    return "Unspecified";
}
#endif /* XA_DEBUG == XA_ENABLED */

