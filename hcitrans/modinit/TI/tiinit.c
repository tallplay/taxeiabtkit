/***************************************************************************
 *
 * File:
 *     $Workfile:tiinit.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:3$
 *
 * Description:
 *     This file contains the implementation of the TI BT init script API
 *
 * Created:
 *     August, 2005 
 *
 * Copyright 2004-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/
/*******************************************************************************\
*
*   FILE NAME:      tiinit.c 
*
*   DESCRIPTION:    This file contains the implementation of BT init script API
*
*   AUTHOR:         E. Kuperman
*
\*******************************************************************************/

#include "stdio.h"
#include "TI/tiinit.h"

#ifndef MAKEWORD
#define MAKEWORD(a, b)      ((unsigned short)(((unsigned char)(a)) | ((unsigned short)((unsigned char)(b))) << 8))
#endif

#define TI_MANUFACTURER_ID    13

//////////////////////////////////////////////////////////////////////////
// Init Script specific
//////////////////////////////////////////////////////////////////////////
const char* cis_GetFilenameFromVersionInformation(const unsigned char* pCommandCompleteParams)
{
    static char s_szInitFileName[50];

    // Check for TI's id
    unsigned short wManufacturerID = MAKEWORD(pCommandCompleteParams[8], pCommandCompleteParams[9]);

    if (TI_MANUFACTURER_ID == wManufacturerID)
    {
        unsigned short wVersion = MAKEWORD(pCommandCompleteParams[10], pCommandCompleteParams[11]);
        
        unsigned short wProjectType =  (wVersion & 0x7C00) >> 10;
        unsigned short wMinorVersion = (wVersion & 0x007F);
        unsigned short wMajorVersion = (wVersion & 0x0380) >> 7;

        if (0 != (wVersion & 0x8000))
        {
            wMajorVersion |= 0x0008;
        }
        
        sprintf(
            s_szInitFileName, 
            "TIInit_%d.%d.%d.bts", 
            (int)wProjectType,
            (int)wMajorVersion,
            (int)wMinorVersion);

        return &s_szInitFileName[0];
    }
    else
    {
        return NULL;
    }
}

typedef struct tagCHeader 
{
    unsigned long  m_nMagicNumber;
    unsigned long  m_nVersion;
    unsigned char m_nFuture[24];
} CHeader;


// The value 0x42535442 stands for (in ASCII) BTSB
// which is Bluetooth Script Binary
#define FILE_HEADER_MAGIC    0x42535442


BTS* bts_LoadScript(const char* szFilename, unsigned long* pnVersion)
{
    BTS* pBTS = NULL;
    FILE* pFile = fopen(szFilename, "rb");

    if (NULL != pFile)
    {
        // Read header
        CHeader header;

        // Read header
        if (1 == fread(&header, sizeof(header), 1, pFile))
        {
            // Check magic number for correctness
            if (header.m_nMagicNumber == FILE_HEADER_MAGIC)
            {
                // If user wants the version number
                if (NULL != pnVersion)
                {
                    *pnVersion = header.m_nVersion;
                }

                pBTS = (BTS*)pFile;
            }
        }

        // If failed reading the file, close it
        if (NULL == pBTS)
        {
            fclose(pFile);
        }
    }

    return pBTS;
}

unsigned long bts_GetNextAction(const BTS* pBluetoothScript, unsigned char* pActionBuffer, unsigned long nMaxSize, unsigned short* pnType)
{
    unsigned long nTotalReturned = 0;
    FILE* pFile = (FILE*)pBluetoothScript;

    if (NULL != pFile)
    {
        // Each Action has the following:
        // UINT16 type of this action
        // UINT16 size of rest
        // BYTE[] action buffer (for HCI, includes the type byte e.g. 1 for hci command)

        unsigned char abActionHeader[4];

        if (1 == fread(&abActionHeader[0], sizeof(abActionHeader), 1, pFile))
        {
            unsigned short nType = *(unsigned short*)&abActionHeader[0];
            unsigned short nSize = *(unsigned short*)&abActionHeader[2];

            if (nSize <= nMaxSize)
            {
                int nReadSize = fread(pActionBuffer, sizeof(pActionBuffer[0]), nSize, pFile);

                if (nReadSize == nSize)
                {
                    *pnType = nType;
                    nTotalReturned = (unsigned long)nSize;
                }
            }
        }
    }

    return nTotalReturned;
}

void bts_UnloadScript(BTS* pBluetoothScript)
{
    FILE* pFile = (FILE*)pBluetoothScript;

    if (NULL != pFile)
    {
        fclose(pFile);
    }
}

