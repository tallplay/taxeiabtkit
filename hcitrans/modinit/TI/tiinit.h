/***************************************************************************
 *
 * File:
 *     $Workfile:tiinit.h$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:2$
 *
 * Description:
 *     This file contains function prototypes, constant and type definitions 
 *     for tiinit.c module
 *
 * Created:
 *     August, 2005 
 *
 * Copyright 2004-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/
/*******************************************************************************\
*
*   FILE NAME:      tiinit.c
*
*   DESCRIPTION:    This file contains function prototypes, constant and type
*                    definitions for    bt_script.c module
*
*   AUTHOR:         E. Kuperman
*
\*******************************************************************************/

#ifndef BT_SCRIPT_H
#define BT_SCRIPT_H

#ifdef __cplusplus
extern "C" {
#endif

//////////////////////////////////////////////////////////////////////////
// Specific interface for Common Init Script
//////////////////////////////////////////////////////////////////////////
const char* cis_GetFilenameFromVersionInformation(const unsigned char* pCommandCompleteParams);

//////////////////////////////////////////////////////////////////////////
// Define the interface of Bluetooth Script
//////////////////////////////////////////////////////////////////////////

typedef void BTS;


#define ACTION_SEND_COMMAND                1    // Send out raw data (as is)
#define ACTION_WAIT_EVENT                2    // Wait for data
#define ACTION_SERIAL_PORT_PARAMETERS    3    
#define ACTION_DELAY                    4    
#define ACTION_RUN_SCRIPT                5    
#define ACTION_REMARKS                    6

//////////////////////////////////////////////////////////////////////////
// Structure for ACTION_SEND_COMMAND
//////////////////////////////////////////////////////////////////////////
typedef struct tagCActionCommand
{
    unsigned char m_abPacket[1]; // Data to send
} CActionCommand;

//////////////////////////////////////////////////////////////////////////
// Structure for ACTION_WAIT_EVENT
//////////////////////////////////////////////////////////////////////////
typedef struct tagCActionWaitEvent
{
    unsigned long m_nTimeToWait; // in milliseconds
    unsigned long m_nSizeToWait;
    unsigned char m_abPacket[1]; // Data to wait for
} CActionWaitEvent;


//////////////////////////////////////////////////////////////////////////
// Structure for ACTION_SERIAL_PORT_PARAMETERS
//////////////////////////////////////////////////////////////////////////
typedef struct tagCActionSerialPortParameters
{
    unsigned long m_nBaudRate;
    unsigned long m_nFlowControl;
} CActionSerialPortParameters;

// Flow Control Type
#define FCT_NONE        0
#define FCT_HARDWARE    1

#define DONT_CHANGE        0xFFFFFFFF    // For both baud rate and flow control


//////////////////////////////////////////////////////////////////////////
// Structure for ACTION_DELAY
//////////////////////////////////////////////////////////////////////////
typedef struct tagCActionDelay
{
    unsigned long m_nDuration; // in milliseconds
} CActionDelay;

//////////////////////////////////////////////////////////////////////////
// Structure for ACTION_RUN_SCRIPT
//////////////////////////////////////////////////////////////////////////
typedef struct tagCActionRunScript
{
    char m_szFilename[1];
} CActionRunScript;

//////////////////////////////////////////////////////////////////////////
// Structure for ACTION_REMARKS
//////////////////////////////////////////////////////////////////////////
typedef struct tagCActionRemarks
{
    char m_szRemarks[1];
} CActionRemarks;


//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

BTS*            bts_LoadScript(const char* szFilename, unsigned long* pnVersion);
unsigned long    bts_GetNextAction(const BTS* pBluetoothScript, unsigned char* pActionBuffer, unsigned long nMaxBuffer, unsigned short* pnType);
void            bts_UnloadScript(BTS* pBluetoothScript);

#ifdef __cplusplus
};
#endif


#endif // BT_SCRIPT_H
