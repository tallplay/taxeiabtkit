#include "parseopts.h"
#include "hcitrans.h"
#include "btalloc.h"
#include "sys/debug.h"
//#include "TI/tiinit.h"

/****************************************************************************
 *
 * General Constants
 *
 ****************************************************************************/

/*---------------------------------------------------------------------------
 * These constants define the various states used globally by the
 * initialization code.
 */
#define RADIO_STATE_DOWN      0
#define RADIO_STATE_READY     1
#define RADIO_STATE_INIT      3
#define RADIO_STATE_SHUTDOWN  4

/****************************************************************************
 *
 * Global Variables
 *
 ****************************************************************************/

/*---------------------------------------------------------------------------
 * Variables used globally by the initialization/shutdown code
 */
static EvmTimer       timer;          /* Timer for postponing events to a
                                       * different context.
                                       */

static HciCommand     hciCommand;     /* Structure for sending commands.  Only
                                       * one command can be sent at a time.
                                       */

static RadioCallback  radioCallback;  /* Pointer to the callback function that
                                       * must be called when initialization or
                                       * deinit happen asynchronously.
                                       */

static U8             radioState;     /* Current state of initialization or
                                       * shutdown.
                                       */
#if 0
static BTS*           pInitScript = 0; /* Pointer to initialization script file.
                                           */
#endif
static HciCommandType wOpCode;          /* Opcode of HCI command sent from init
                                       * script.
                                       */
static U8 BtAddr[6] = {0x00, 0x00, 0x00, 0xE1, 0x80, 0x00};

/* Internal function prototypes for the Default initialization/shutdown */
static BtStatus RADIO_Init_Default(void);
void RADIO_EventHandler_Default(U8 Event, HciCallbackParms *Parms);
#if 0 //tallplay mark
/* Internal function prototypes for TI initialization/shutdown */
static BtStatus TI_Init(void);
void TI_EventHandler(U8 Event, HciCallbackParms *Parms);
static BtStatus TI_ContinueInitScript(void);

/* Internal function prototypes for Infineon initialization/shutdown */
static BtStatus INF_Init(void);
void INF_EventHandler(U8 Event, HciCallbackParms *Parms);
static BtStatus INF_Shutdown(void);
static BtStatus radioSetInfineonBaudRate(U16 speed);
#endif

/* Internal function prototypes for STMicro/Ericsson initialization/shutdown */
static BtStatus ERI_Init(void);
void ERI_EventHandler(U8 Event, HciCallbackParms *Parms);
static BtStatus ERI_Shutdown(void);
static void radioEricssonSpeedChangeHandler(EvmTimer *timer);
static BtStatus radioSetEricssonBaudRate(U16 speed);
static BtStatus radioSetEricssonAddress(U8 addr[6]);
#if 0 //tallplay mark
/* Internal function prototypes for Broadcom initialization/shutdown */
static BtStatus BC_Init(void);
void BC_EventHandler(U8 Event, HciCallbackParms *Parms);
static BtStatus BC_Shutdown(void);
static void radioBroadcomSpeedChangeHandler(EvmTimer *timer);
static BtStatus radioSetBroadcomBaudRate(U16 speed);

/* Internal function prototypes for Motorola initialization/shutdown */
static BtStatus MOT_Init(void);
void MOT_EventHandler(U8 Event, HciCallbackParms *Parms);
static BtStatus MOT_Shutdown(void);
static BtStatus radioSetMotorolaBaudRate(U16 speed);
static void radioConfigMotorolaScoEncode(EvmTimer *timer);
static void radioConfigMotorolaScoDecode(void);
#endif
extern int sprintf(char *, const char *, ...);
/****************************************************************************
 *
 * General Radio Module Initialization/Shutdown
 *
 ****************************************************************************/

/*---------------------------------------------------------------------------
 *            RADIO_Init()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Called by the Radio Manager to begin Radio Module
 *            initialization.  Radio module initialization is accomplished
 *            by sending vendor specific or other standard HCI commands.  This
 *            function is called after the transports and the HCI are
 *            initialized.
 *
 *            HCI commands are sent one at a time to the radio module.  Once
 *            a command has completed (COMMAND_COMPLETE or the equivalent is
 *            received), a new command can be sent.  Standard HCI commands and
 *            events are documented in the Bluetooth specification.  Vendor
 *            specific commands and events are defined by the module manufacturer.
 *
 *            The "Callback" paramter contains a pointer to a callback function
 *            for reporting initialization progress (see RadioCallback).  If no
 *            initialization is necessary for the Radio Module, or if
 *            intialization is completed during the call to RADIO_Init, RADIO_Init
 *            must return BT_STATUS_SUCCESS, and the callback function must not
 *            be called.  If initialization is required and begins successfully,
 *            RADIO_Init must return BT_STATUS_PENDING, and the callback
 *            function must be called when initializaiton is complete.  If
 *            initialization cannot be started or failed during the call to
 *            RADIO_Init, then RADIO_Init must return BT_STATUS_FAILED.
 *
 * Returns:   <See hcitrans.h>
 */
void RADIO_SetBDAddr(U8 addr[6])
{
    U8 i;

    for(i = 0; i < 6 ; i++)
        BtAddr[i] = addr[i];
}

BtStatus RADIO_Init(RadioCallback Callback)
{
    BtStatus status;
    radioCallback = Callback;
    radioState = RADIO_STATE_INIT;

    switch(getRadioType())
    {
#if 0 //tallplay mark

        case RADIO_TYPE_TI_UART:
            status = TI_Init();
            break;

        case RADIO_TYPE_INFINEON_UART:
            status = INF_Init();
            break;
#endif

        case RADIO_TYPE_ST_MICRO_UART:
        case RADIO_TYPE_ERICSSON_UART:
            status = ERI_Init();
            break;
#if 0 //tallplay mark

        case RADIO_TYPE_BROADCOM_UART:
            status = BC_Init();
            break;

        case RADIO_TYPE_MOTOROLA_UART:
            status = MOT_Init();
            break;
#endif

        default:
            status = RADIO_Init_Default();
            break;
    }

    return status;
}


/*---------------------------------------------------------------------------
 *            RADIO_Shutdown()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Called by the Radio Manager to begin shutdown of the Radio Module.
 *            Radio module shutdown is accomplished by sending vendor specific
 *            or other standard HCI commands.  This function is called before
 *            the transports and the HCI are shut down.
 *
 * Returns:   <See hcitrans.h>
 */
BtStatus RADIO_Shutdown(void)
{
    BtStatus status = BT_STATUS_PENDING;
    radioState = RADIO_STATE_SHUTDOWN;

    switch(getRadioType())
    {
#if 0

        case RADIO_TYPE_INFINEON_UART:
            status = INF_Shutdown();
            break;
#endif

        case RADIO_TYPE_ST_MICRO_UART:
        case RADIO_TYPE_ERICSSON_UART:
            status = ERI_Shutdown();
            break;
#if 0

        case RADIO_TYPE_BROADCOM_UART:
            status = BC_Shutdown();
            break;

        case RADIO_TYPE_MOTOROLA_UART:
            status = MOT_Shutdown();
            break;
#endif

        default:
            status = BT_STATUS_SUCCESS;
            break;
    }

    return status;
}

/*---------------------------------------------------------------------------
 *            RADIO_EventHandler()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Called by the Radio Manager indicate HCI events to the Radio
 *            Module initialization or shutdown code.  These events are
 *            generated in response to commands sent from the initialization
 *            or shutdown code.  The commands and events used conform to the
 *            HCI Api interface defined in the HCI_SendCommand function (see
 *            sys/hci.h).
 *
 * Returns:   <See hcitrans.h>
 */
void RADIO_EventHandler(U8 Event, HciCallbackParms *Parms)
{
    switch(getRadioType())
    {
#if 0 //tallplay

        case RADIO_TYPE_TI_UART:
            TI_EventHandler(Event, Parms);
            break;

        case RADIO_TYPE_INFINEON_UART:
            INF_EventHandler(Event, Parms);
            break;
#endif

        case RADIO_TYPE_ST_MICRO_UART:
        case RADIO_TYPE_ERICSSON_UART:
            ERI_EventHandler(Event, Parms);
            break;

#if 0 //tallplay

        case RADIO_TYPE_BROADCOM_UART:
            BC_EventHandler(Event, Parms);
            break;

        case RADIO_TYPE_MOTOROLA_UART:
            MOT_EventHandler(Event, Parms);
            break;
#endif

        default:
            RADIO_EventHandler_Default(Event, Parms);
    }
}

/*---------------------------------------------------------------------------
 *            RADIO_Init_Default()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Default radio module initialzation.
 *
 * Returns:   <See RADIO_Init in hcitrans.h>
 */
static BtStatus RADIO_Init_Default(void)
{
    wOpCode = HCC_RESET;
    return HCI_Reset();
}

/*---------------------------------------------------------------------------
 *            RADIO_EventHandler_Default()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handles HCI events for radios.
 *
 * Returns:   <See RADIO_EventHandler in hcitrans.h>
 */
void RADIO_EventHandler_Default(U8 Event, HciCallbackParms *Parms)
{
    BtStatus   status;
    RadioEvent radioEvent;

    if((Event == HCI_CONTROLLER_EVENT) &&
            (LEtoHost16(&(Parms->ptr.hciEvent->parms[1])) == HCC_RESET))
    {
        radioEvent = RADIO_INIT_STATUS;

        if(Parms->status == BT_STATUS_SUCCESS)
        {
            radioState = RADIO_STATE_READY;
            status = BT_STATUS_SUCCESS;
        }
        else
        {
            radioState = RADIO_STATE_INIT;
            status = BT_STATUS_FAILED;
        }

        /* Deliver event to Radio Manager */
        radioCallback(radioEvent, status);
        return;
    }
}

#if 0

/****************************************************************************
 *
 * TI Radio Module Initialization/Shutdown
 *
 ****************************************************************************/
/*---------------------------------------------------------------------------
 *            TI_Init()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initializes the TI radio module.
 *
 * Returns:   <See RADIO_Init in hcitrans.h>
 */
static BtStatus TI_Init(void)
{
    wOpCode = HCC_RESET;
    return HCI_Reset();
}
/*---------------------------------------------------------------------------
 *            TI_EventHandler()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handles HCI events for TI radios.
 *
 * Returns:   <See RADIO_EventHandler in hcitrans.h>
 */
void TI_EventHandler(U8 Event, HciCallbackParms *Parms)
{
    U8 *cmdParms;

    if((Event == HCI_CONTROLLER_EVENT) &&
            (LEtoHost16(&(Parms->ptr.hciEvent->parms[1])) == HCC_RESET))
    {
        if(Parms->status == BT_STATUS_SUCCESS)
        {
            /* Send HCI command Read Local Version to build an init script */
            wOpCode = HCC_READ_LOCAL_VERSION;
            HCI_SendRadioCommand(wOpCode, 0, &hciCommand);
        }
        else
        {
            radioState = RADIO_STATE_INIT;
            /* Deliver event to Radio Manager */
            radioCallback(RADIO_INIT_STATUS, BT_STATUS_FAILED);
        }

        return;
    }

    cmdParms = Parms->ptr.hciEvent->parms;

    if((Event == HCI_CONTROLLER_EVENT) &&
            ((Parms->ptr.hciEvent->event == HCE_COMMAND_COMPLETE) &&
             (LEtoHost16(&cmdParms[1]) == wOpCode)))
    {
        BtStatus   status = Parms->status;
        RadioEvent radioEvent = RADIO_DEFAULT_STATUS;

        /* Initialization starts with reading local version in order to get a filename */
        /* of the init script */
        /* assumes status for the version command is SUCCESS */
        if(wOpCode == HCC_READ_LOCAL_VERSION)
        {
            char szTIDirFileName[100];
            /* Local version is used for building an init script filename */
            const char *szInitScriptName = cis_GetFilenameFromVersionInformation(cmdParms);
            /* init script should be located in C:\hcitrans\modinit\TI\ as part of the
               default code path. */
            sprintf(szTIDirFileName, "../../../hcitrans/modinit/TI/%s", szInitScriptName);

            /* Try to load the script from the current directory */
            if((pInitScript = bts_LoadScript(szTIDirFileName, 0)) == 0)
            {
                status = BT_STATUS_FAILED;
                radioEvent = RADIO_SHUTDOWN_STATUS;
                radioState = RADIO_STATE_SHUTDOWN;
                Assert(0);
            }
        }

        /* Proceed with init script until ACTION_WAIT_EVENT */
        if((status != BT_STATUS_FAILED) && (cmdParms[3] == HC_STATUS_SUCCESS))
        {
            if((status = TI_ContinueInitScript()) != BT_STATUS_PENDING)
            {
                if(radioState == RADIO_STATE_INIT)
                {
                    if(status == HC_STATUS_SUCCESS)
                    {
                        radioState = RADIO_STATE_READY;
                    }

                    /* Close init script file */
                    bts_UnloadScript(pInitScript);
                    pInitScript = 0;
                    radioEvent = RADIO_INIT_STATUS;
                }
                else
                {
                    radioEvent = RADIO_SHUTDOWN_STATUS;
                }
            }
        }
        else
        {
            /* Failure of last command - unload script if exists.*/
            bts_UnloadScript(pInitScript);
            pInitScript = 0;
        }

        /* Deliver event to Radio Manager */
        radioCallback(radioEvent, status);
    }
}
/*---------------------------------------------------------------------------
 *            TI_ContinueInitScript()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Runs a part of initialization script until ACTION_WAIT_EVENT is
 *              read from it. Then, the function will be called next time until
 *              the next ACTION_WAIT_EVENT, and so repeated until the end of the
 *              script.
 *
 * Returns:   BtStatus
 */
BtStatus TI_ContinueInitScript(void)
{
    S8 abBuffer[300];
    U32 nSize;
    U16 wType = 0;
    BtStatus status = BT_STATUS_PENDING;
    BOOL bCmdExecuted = FALSE;
    CActionSerialPortParameters* pParams;
    U16 uartSpeed;

    if(0 != pInitScript)
    {
        do
        {
            /* Process the init script */
            nSize = bts_GetNextAction(pInitScript, &abBuffer[0], sizeof(abBuffer), &wType);

            if(nSize > 0)
            {
                switch(wType)
                {
                        /* We have an HCI command to execute here */
                    case ACTION_SEND_COMMAND:
                    {
                        U8 parmLen = abBuffer[3];
                        /* Skip HCI packet type (1 byte), command opcode (2 bytes), */
                        /* and parameters length (1 byte) */
                        OS_MemCopy(hciCommand.parms, abBuffer + 4, parmLen);
                        wOpCode = (HciCommandType)(LEtoHost16(&abBuffer[1]));
                        status = HCI_SendRadioCommand(wOpCode, parmLen, &hciCommand);
                    }
                    break;

                    case ACTION_SERIAL_PORT_PARAMETERS:
                        /* We got this action after TI Vendor Specific command */
                        /* HCI_VS_Update_Uart_HCI_Baudrate was sent to Host Controller. */
                        /* Now, we need to update UART speed */
                        pParams = (CActionSerialPortParameters*)&abBuffer[0];
                        uartSpeed = (U16)((pParams->m_nBaudRate) / 1000);
                        HCI_SetSpeed(uartSpeed);
                        break;

                    case ACTION_DELAY:
                    case ACTION_RUN_SCRIPT:
                    case ACTION_REMARKS:
                        break;

                    case ACTION_WAIT_EVENT:
                        status = BT_STATUS_PENDING;
                        bCmdExecuted = TRUE;
                        break;

                    default:
                        break;
                }
            }
            else
            {
                status = BT_STATUS_SUCCESS;
            }
        }
        while((bCmdExecuted == FALSE) && (nSize > 0) && (status != BT_STATUS_FAILED));
    }
    else
    {
        status = BT_STATUS_FAILED;
    }

    return status;
}

/****************************************************************************
 *
 * Infineon Radio Module Initialization/Shutdown
 *
 ****************************************************************************/

/* Infineon configure for audio */
#define HCC_INF_SET_SCO                      0xFC02
#define HCC_INF_SET_PCM                      0xFC04
#define HCC_INF_SET_PCM_CR                   0xFC26
#define HCC_INF_READ_VERSION                 0xFC05

/* Infineon baud rate command */
#define HCC_INF_BAUD_RATE                    0xFC06

/* Infineon default baud rate - 115200 */
#define INF_DEFAULT_BAUD                     115

/* Infineon specific global variable */
static U8 infBaudCount;

/*---------------------------------------------------------------------------
 *            INF_Init()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initializes the Infineon radio module.
 *
 * Returns:   <See RADIO_Init in hcitrans.h>
 */
BtStatus INF_Init(void)
{
    wOpCode = HCC_RESET;
    return HCI_Reset();
}

/*---------------------------------------------------------------------------
 *            INF_Shutdown()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Shut down the Infineon radio module.
 *
 * Returns:   <See RADIO_Shutdown in hcitrans.h>
 */
BtStatus INF_Shutdown(void)
{
    infBaudCount = 0;
    /* Infineon radio, set baud rate back to default (115.2) */
    return HCI_SendRadioCommand(HCC_INF_READ_VERSION, 0, &hciCommand);
}

/*---------------------------------------------------------------------------
 *            INF_EventHandler()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handles HCI events for Infineon radios.
 *
 * Returns:   <See RADIO_EventHandler in hcitrans.h>
 */
void INF_EventHandler(U8 Event, HciCallbackParms *Parms)
{
    RadioEvent radioEvent;
    BtStatus   status = Parms->status;
    U8        *cmdParms;

    if((Event == HCI_CONTROLLER_EVENT) &&
            (LEtoHost16(&(Parms->ptr.hciEvent->parms[1])) == HCC_RESET))
    {
        if(Parms->status == BT_STATUS_SUCCESS)
        {
            /* Infineon radio, get the version */
            wOpCode = HCC_INF_READ_VERSION;
            HCI_SendRadioCommand(wOpCode, 0, &hciCommand);
        }
        else
        {
            radioState = RADIO_STATE_INIT;
            /* Deliver event to Radio Manager */
            radioCallback(RADIO_INIT_STATUS, BT_STATUS_FAILED);
        }

        return;
    }

    cmdParms = Parms->ptr.hciEvent->parms;

    if((Event == HCI_CONTROLLER_EVENT) &&
            (Parms->ptr.hciEvent->event == HCE_COMMAND_COMPLETE))
    {
        switch(LEtoHost16(&cmdParms[1]))
        {
            case HCC_INF_READ_VERSION:
                radioEvent = (radioState == RADIO_STATE_INIT) ?
                             RADIO_INIT_STATUS : RADIO_SHUTDOWN_STATUS;
                Report(("RADIO: HCI_Infineon_Read_SW_Version command complete status = %s\n",
                        pBT_Status(cmdParms[3])));

                if(status == BT_STATUS_SUCCESS)
                {
                    if(LEtoHost16(&cmdParms[1]) == 0x5724)
                    {
                        /* LM 5724 or greater */
                        if(radioState == RADIO_STATE_INIT)
                        {
                            infBaudCount = 0;
                            status = radioSetInfineonBaudRate(getSpeedOption());

                            if(status != BT_STATUS_PENDING)
                            {
                                /* Call failed */
                                radioCallback(radioEvent, status);
                            }
                        }
                        else
                        {
                            radioSetInfineonBaudRate(INF_DEFAULT_BAUD);
                        }
                    }
                    else
                    {
                        /* 1.2 or greater Radio */
#if NUM_SCO_CONNS > 0
                        /* Continue initialization */
                        if((status == BT_STATUS_SUCCESS) &&
                                (radioState == RADIO_STATE_INIT))
                        {
#if HCI_ALLOW_PRESCAN == XA_ENABLED
                            /* Initializes INFINEON prescan to handle infineon specific behaviors */
#if PRESCAN_INFINEON_FIXES == XA_ENABLED
                            (void)INFINEON_Init();
#endif
#endif
                            hciCommand.parms[0] = 0x01;
                            HCI_SendRadioCommand(HCC_INF_SET_PCM_CR, 1, &hciCommand);
                        }
                        else
                        {
                            /* Report status */
                            radioCallback(radioEvent, status);
                        }

#else
                        /* Report status */
                        radioCallback(radioEvent, status);
#endif
                    }
                }
                else
                {
                    /* Command failed */
                    radioCallback(radioEvent, status);
                }

                break;

            case HCC_INF_BAUD_RATE:
                radioEvent = (radioState == RADIO_STATE_INIT) ?
                             RADIO_INIT_STATUS : RADIO_SHUTDOWN_STATUS;
                Report(("RADIO: HCC_Inf_Baud_Rate command complete status = %s\n",
                        pBT_Status(cmdParms[3])));
                infBaudCount++;

                if(infBaudCount <= 1)
                {
                    /* First command complete received */
                    if(status == BT_STATUS_SUCCESS)
                    {
                        /* Set the uart speed */
                        if(radioState == RADIO_STATE_INIT)
                        {
                            Report(("RADIO: setting UART rate to %d\n",
                                    getSpeedOption()));
                            HCI_SetSpeed(getSpeedOption());
                        }
                        else
                        {
                            Report(("RADIO: setting UART rate to %d\n",
                                    INF_DEFAULT_BAUD));
                            HCI_SetSpeed(INF_DEFAULT_BAUD);
                        }
                    }
                    else
                    {
                        /* Command failed */
                        radioCallback(radioEvent, status);
                    }
                }
                else
                {
#if NUM_SCO_CONNS > 0

                    /* Continue initialization */
                    if((status == BT_STATUS_SUCCESS) &&
                            (radioState == RADIO_STATE_INIT))
                    {
                        hciCommand.parms[0] = 0x01;
                        HCI_SendRadioCommand(HCC_INF_SET_PCM, 1, &hciCommand);
                    }
                    else
                    {
                        /* Report status */
                        radioCallback(radioEvent, status);
                    }

#else
                    /* Report status */
                    radioCallback(radioEvent, status);
#endif
                }

                break;
#if NUM_SCO_CONNS > 0

            case HCC_INF_SET_PCM_CR:
            case HCC_INF_SET_PCM:
                Report(("RADIO: HCC_Inf_Set_Pcm command complete status = %s\n",
                        pBT_Status(cmdParms[3])));
                hciCommand.parms[0] = 0x01;
                HCI_SendRadioCommand(HCC_INF_SET_SCO, 1, &hciCommand);
                break;

            case HCC_INF_SET_SCO:
                Report(("RADIO: HCC_Inf_Set_Sco command complete status = %s\n",
                        pBT_Status(cmdParms[3])));
                StoreLE16(&hciCommand.parms[0],
                          (BSAS_IN_CODING_ULAW | BSAS_IN_DATA_TWOS |
                           BSAS_IN_SAMPLE_16BIT | BSAS_CVSD));
                HCI_SendRadioCommand(HCC_WRITE_VOICE_SETTING, 2, &hciCommand);
                break;

            case HCC_WRITE_VOICE_SETTING:
                Report(("RADIO: HCC_Write_Voice_Setting command complete status = %s\n",
                        pBT_Status(cmdParms[3])));

                if(radioState == RADIO_STATE_INIT)
                {
                    radioCallback(RADIO_INIT_STATUS, status);
                }
                else
                {
                    radioCallback(RADIO_SHUTDOWN_STATUS, status);
                }

                break;
#endif
        }
    }
}

/*---------------------------------------------------------------------------
 *            radioSetInfineonBaudRate()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Send the HCI command to change baud rate for Infineon radios.
 */
static BtStatus radioSetInfineonBaudRate(U16 speed)
{
    BtStatus status = BT_STATUS_PENDING;
    Report(("RADIO: Setting Infineon baud rate to %d\n", speed));

    switch(speed)
    {
        case 9216:  /* Was entered as 821.6 on command line */
            StoreBE16(hciCommand.parms, 0x000D);
            break;

        case 4608:  /* Was entered as 460.8 on command line */
            StoreBE16(hciCommand.parms, 0x001B);
            break;

        case 2304:  /* Was entered as 230.4 on command line */
            StoreBE16(hciCommand.parms, 0x0037);
            break;

        case 1152:  /* Was entered as 115.2 on command line */
        case 115:   /* Was entered as 115 on command line */
            StoreBE16(hciCommand.parms, 0x0070);
            break;

        case 576:   /* Was entered as 57.6 on command line */
        case 57:    /* Was entered as 57 on command line */
            StoreBE16(hciCommand.parms, 0x00E1);
            break;

        case 384:   /* Was entered as 38.4 on command line */
        case 38:    /* Was entered as 38 on command line */
            StoreBE16(hciCommand.parms, 0x01A8);
            break;

        case 96:   /* Was entered as 9.6 on command line */
        case 9:    /* Was entered as 9 on command line */
            StoreBE16(hciCommand.parms, 0x03A8);
            break;

        default:
            /* Unsupported */
            status = BT_STATUS_FAILED;
            Report(("RADIO: Unsupported UART baud rate (%d)\n", speed));
            break;
    }

    if(status == BT_STATUS_PENDING)
    {
        HCI_SendRadioCommand(HCC_INF_BAUD_RATE, 2, &hciCommand);
    }

    return status;
}
#endif

/****************************************************************************
 *
 * Ericsson Radio Module Initialization/Shutdown
 *
 ****************************************************************************/

/* Ericsson baud rate command */
#define HCC_ERI_BAUD_RATE                    0xFC09


/* Ericsson default baud rate - 57600 */
#define ERI_DEFAULT_BAUD                     57

/* ST Micro default baud rate - 57600 */
#define ST_DEFAULT_BAUD                      115

/* Ericsson specific global variable */
static U16 eriSpeed;

/*---------------------------------------------------------------------------
 *            ERI_Init()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initializes the Ericsson radio module.
 *
 * Returns:   <See RADIO_Init in hcitrans.h>
 */
static BtStatus ERI_Init(void)
{
    wOpCode = HCC_RESET;
    return HCI_Reset();
}

/*---------------------------------------------------------------------------
 *            ERI_Shutdown()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Shut down the Ericsson radio module.
 *
 * Returns:   <See RADIO_Shutdown in hcitrans.h>
 */
static BtStatus ERI_Shutdown(void)
{
    /* Ericsson radio, set baud rate back to default (57.6) */
    if(getRadioType() == RADIO_TYPE_ERICSSON_UART)
    {
        eriSpeed = ERI_DEFAULT_BAUD;
    }
    else
    {
        eriSpeed = ST_DEFAULT_BAUD;
    }

    return radioSetEricssonBaudRate(eriSpeed);
}

void ERI_EventHandler(U8 Event, HciCallbackParms *Parms)
{
    BtStatus   status = Parms->status;
    U8        *cmdParms = Parms->ptr.hciEvent->parms;
    static BOOL afteraddress = FALSE;

    //HCC_RESET->HCC_ERI_SETBAUD->HCC_ERI_ADDRESS->HCC_RESET

    if((Event == HCI_CONTROLLER_EVENT))
    {
        if((LEtoHost16(&cmdParms[1]) == HCC_RESET))
        {
            if(afteraddress)
            {
                radioState = RADIO_STATE_READY;
                radioCallback(RADIO_INIT_STATUS, status);
                afteraddress = FALSE; //every hci_reset happen, we will set address again.
            }
            else
            {
                if(Parms->status == BT_STATUS_SUCCESS)
                {
                     (void)HciSendCommand(HCC_READ_LOCAL_VERSION, 0, &hciCommand, TRUE);
                }
                else
                {
                    radioState = RADIO_STATE_INIT;
                    /* Deliver event to Radio Manager */
                    radioCallback(RADIO_INIT_STATUS, BT_STATUS_FAILED);
                }
            }

            return;
        }

        if((LEtoHost16(&cmdParms[1]) == HCC_READ_LOCAL_VERSION))
        {
            /* Ericsson radio, set baud rate */
            if(LEtoHost16(&cmdParms[8]) == 0x0030) //ST micro
            {
                eriSpeed = getSpeedOption();
                radioSetEricssonBaudRate(eriSpeed);
            }
            else //mtk
            {
                setRadioType(RADIO_TYPE_MTK_6622_UART);
                radioState = RADIO_STATE_READY;
                radioCallback(RADIO_INIT_STATUS, status);
                afteraddress = FALSE;
            }
            return;
        }
        
/////////////////////////////////////ST part begin///////////////////////////////////////
        if((LEtoHost16(&cmdParms[1]) == HCC_ERI_BAUD_RATE))
        {
            Report(("RADIO: HCC_Eri_Baud_Rate command complete status = %s\n",
                    pBT_Status(cmdParms[3])));

            if(radioState == RADIO_STATE_INIT)
            {
                //radioCallback(RADIO_INIT_STATUS, status);
                radioSetEricssonAddress(BtAddr);
                afteraddress = TRUE;
            }
            else //we only issue set baud in shutdown
            {
                radioCallback(RADIO_SHUTDOWN_STATUS, status);
            }

            return;
        }

        if((LEtoHost16(&(Parms->ptr.hciEvent->parms[1])) == HCC_ERI_ADDRESS))
        {
            if(radioState == RADIO_STATE_INIT)  //after set baud
            {
                (void)HciSendCommand(HCC_RESET, 0, &hciCommand, TRUE);
            }

            return;
        }
/////////////////////////////////////ST part end///////////////////////////////////////

    }


    if(Event == HCI_COMMAND_HANDLED)
    {
        /* A command was successfully sent to the radio module */
        if((LEtoHost16(Parms->ptr.cmd->header) == HCC_ERI_BAUD_RATE) &&
                (Parms->status == BT_STATUS_SUCCESS))
        {
            /* Speed change command was sent successfully.
             * Change the UART speed
             */
            timer.context = (void *)eriSpeed;
            timer.func = radioEricssonSpeedChangeHandler;
            EVM_StartTimer(&timer, 10);
        }
    }
}

/*---------------------------------------------------------------------------
 *            radioEricssonSpeedChangeHander()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Changes the UART speed on the local host.
 */
static void radioEricssonSpeedChangeHandler(EvmTimer *timer)
{
    UNUSED_PARAMETER(timer);
    Report(("RADIO: Changing UART baud rate to %d\n", (U16)timer->context));
    /* Change the baud rate */
    HCI_SetSpeed((U16)timer->context);
}

/*---------------------------------------------------------------------------
 *            radioSetEricssonBaudRate()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Send the HCI command to change baud rate for Ericsson radios.
 */
static BtStatus radioSetEricssonBaudRate(U16 speed)
{
    BtStatus status = BT_STATUS_PENDING;
    Report(("RADIO: Setting Ericsson baud rate to %d\n", speed));

    switch(speed)
    {
        case 4608:  /* Was entered as 460.8 on command line */
            hciCommand.parms[0] = 0x00;
            break;

        case 2304:  /* Was entered as 230.4 on command line */
            hciCommand.parms[0] = 0x01;
            break;

        case 1152:  /* Was entered as 115.2 on command line */
        case 115:   /* Was entered as 115 on command line */
            hciCommand.parms[0] = 0x02;
            break;

        case 576:   /* Was entered as 57.6 on command line */
        case 57:    /* Was entered as 57 on command line */
            hciCommand.parms[0] = 0x03;
            break;

        case 384:   /* Was entered as 38.4 on command line */
        case 38:    /* Was entered as 38 on command line */
            hciCommand.parms[0] = 0x12;
            break;

        case 96:   /* Was entered as 9.6 on command line */
        case 9:    /* Was entered as 9 on command line */
            hciCommand.parms[0] = 0x14;
            break;

        default:
            /* Unsupported */
            status = BT_STATUS_FAILED;
            Report(("RADIO: Unsupported UART baud rate (%d)\n", speed));
            break;
    }

    if(status == BT_STATUS_PENDING)
    {
        HCI_SendRadioCommand(HCC_ERI_BAUD_RATE, 1, &hciCommand);
    }

    return status;
}

static BtStatus radioSetEricssonAddress(U8 addr[])
{
    U8 i;
    BtStatus status = BT_STATUS_PENDING;
    hciCommand.parms[0] = 0xfe;
    hciCommand.parms[1] = 0x06;

    for(i = 0; i < 6 ; i++)
        hciCommand.parms[2 + i] = addr[i];

    if(status == BT_STATUS_PENDING)
    {
        HCI_SendRadioCommand(HCC_ERI_ADDRESS, 8, &hciCommand);
    }

    return status;
}
#if 0
/****************************************************************************
 *
 * Broadcom Radio Module Initialization/Shutdown
 *
 ****************************************************************************/

/* Broadcom baud rate command */
#define HCC_BC_BAUD_RATE                    0xFC18

/* Broadcom default baud rate - 115200 */
#define BC_DEFAULT_BAUD                    115

/* Broadcom specific global variable */
static U16 bcSpeed;

/*---------------------------------------------------------------------------
 *            BC_Init()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initializes the Broadcom radio module.
 *
 * Returns:   <See RADIO_Init in hcitrans.h>
 */
static BtStatus BC_Init(void)
{
    wOpCode = HCC_RESET;
    return HCI_Reset();
}

/*---------------------------------------------------------------------------
 *            BC_Shutdown()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Shut down the Broadcom radio module.
 *
 * Returns:   <See RADIO_Shutdown in hcitrans.h>
 */
static BtStatus BC_Shutdown(void)
{
    /* Broadcom radio, set baud rate back to default (115.2) */
    bcSpeed = BC_DEFAULT_BAUD;
    return radioSetBroadcomBaudRate(bcSpeed);
}

/*---------------------------------------------------------------------------
 *            BC_EventHandler()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handles HCI events for Broadcom radios.
 *
 * Returns:   <See RADIO_EventHandler in hcitrans.h>
 */
void BC_EventHandler(U8 Event, HciCallbackParms *Parms)
{
    BtStatus   status = Parms->status;
    U8        *cmdParms;

    if((Event == HCI_CONTROLLER_EVENT) &&
            (LEtoHost16(&(Parms->ptr.hciEvent->parms[1])) == HCC_RESET))
    {
        if(Parms->status == BT_STATUS_SUCCESS)
        {
            /* Broadcom radio, set baud rate */
            bcSpeed = getSpeedOption();
            radioSetBroadcomBaudRate(bcSpeed);
        }
        else
        {
            radioState = RADIO_STATE_INIT;
            /* Deliver event to Radio Manager */
            radioCallback(RADIO_INIT_STATUS, BT_STATUS_FAILED);
        }

        return;
    }

    cmdParms = Parms->ptr.hciEvent->parms;

    if(Event == HCI_CONTROLLER_EVENT)
    {
        /* This is a HCI Event */
        if((Parms->ptr.hciEvent->event == HCE_COMMAND_COMPLETE) &&
                (LEtoHost16(&cmdParms[1]) == HCC_BC_BAUD_RATE))
        {
            Report(("RADIO: HCC_BC_Baud_Rate command complete status = %s\n",
                    pBT_Status(cmdParms[3])));

            if(status == BT_STATUS_SUCCESS)
            {
                /* Change the baud rate */
                timer.context = (void *)bcSpeed;
                timer.func = radioBroadcomSpeedChangeHandler;
                EVM_StartTimer(&timer, 100);
            }
            else
            {
                /* Speed change failed, tell Radio Manager */
                if(radioState == RADIO_STATE_INIT)
                {
                    radioCallback(RADIO_INIT_STATUS, status);
                }
                else
                {
                    radioCallback(RADIO_SHUTDOWN_STATUS, status);
                }
            }
        }
    }
}

/*---------------------------------------------------------------------------
 *            radioBroadcomSpeedChangeHander()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Changes the UART speed on the local host.
 */
static void radioBroadcomSpeedChangeHandler(EvmTimer *timer)
{
    UNUSED_PARAMETER(timer);
    Report(("RADIO: Changing UART baud rate to %d\n", (U16)timer->context));
    /* Change the baud rate */
    HCI_SetSpeed((U16)timer->context);

    /* Let Radio Manager know that init completed successfully */
    if(radioState == RADIO_STATE_INIT)
    {
        radioCallback(RADIO_INIT_STATUS, BT_STATUS_SUCCESS);
    }
    else
    {
        radioCallback(RADIO_SHUTDOWN_STATUS, BT_STATUS_SUCCESS);
    }
}

/*---------------------------------------------------------------------------
 *            radioSetBroadcomBaudRate()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Send the HCI command to change baud rate for Broadcom radios.
 */
static BtStatus radioSetBroadcomBaudRate(U16 speed)
{
    BtStatus status = BT_STATUS_PENDING;
    Report(("RADIO: Setting Broadcom baud rate to %d\n", speed));

    switch(speed)
    {
        case 9216:  /* Was entered as 921.6 on command line */
            StoreBE16(hciCommand.parms, 0x55ff);
            break;

        case 4608:  /* Was entered as 460.8 on command line */
            StoreBE16(hciCommand.parms, 0x22fd);
            break;

        case 2304:  /* Was entered as 230.4 on command line */
            StoreBE16(hciCommand.parms, 0x44fa);
            break;

        case 1152:  /* Was entered as 115.2 on command line */
        case 115:   /* Was entered as 115 on command line */
            StoreBE16(hciCommand.parms, 0x00f3);
            break;

        case 576:   /* Was entered as 57.6 on command line */
        case 57:    /* Was entered as 57 on command line */
            StoreBE16(hciCommand.parms, 0x00e6);
            break;

        case 384:   /* Was entered as 38.4 on command line */
        case 38:    /* Was entered as 38 on command line */
            StoreBE16(hciCommand.parms, 0x0199);
            break;

        case 96:   /* Was entered as 9.6 on command line */
        case 9:    /* Was entered as 9 on command line */
            StoreBE16(hciCommand.parms, 0x2264);
            break;

        default:
            /* Unsupported */
            status = BT_STATUS_FAILED;
            Report(("RADIO: Unsupported UART baud rate (%d)\n", speed));
            break;
    }

    if(status == BT_STATUS_PENDING)
    {
        HCI_SendRadioCommand(HCC_BC_BAUD_RATE, 2, &hciCommand);
    }

    return status;
}

/****************************************************************************
 *
 * Motorola Radio Module Initialization/Shutdown
 *
 ****************************************************************************/

/* Motorola baud rate command */
#define HCC_MOT_BAUD_RATE                    0xFD01

/* Motorola default baud rate - 9600 */
#define MOT_DEFAULT_BAUD                     9

/* Motorola configure audio CODEC */
#define HCC_WRITE_SCO_INTERPOLATION_DEFAULT  0xFC0A

/*---------------------------------------------------------------------------
 * Initialized variables that define module specific initialization data.
 */

static U8 motScoInterpolationEncode[] =
{
    '\x01',         /* direction - encode */
    '\x01', '\x00', /* buffer action point 1 */
    '\xD7', '\x7C', /* interplation speed 1 */
    '\x02', '\x00', /* buffer action point 2 */
    '\xE8', '\x7C', /* interplation speed 2 */
    '\x03', '\x00', /* buffer action point 3 */
    '\x00', '\x7D', /* interplation speed 3 */
    '\x06', '\x00', /* buffer action point 4 */
    '\x18', '\x7D', /* interplation speed 4 */
    '\x20', '\x00', /* buffer action point 5 */
    '\x28', '\x7D'  /* interplation speed 5 */
};

static U8 motScoInterpolationDecode[] =
{
    '\x02',         /* direction - encode */
    '\x01', '\x00', /* buffer action point 1 */
    '\xe7', '\x82', /* interplation speed 1 */
    '\x02', '\x00', /* buffer action point 2 */
    '\xfd', '\x82', /* interplation speed 2 */
    '\x03', '\x00', /* buffer action point 3 */
    '\x12', '\x83', /* interplation speed 3 */
    '\x06', '\x00', /* buffer action point 4 */
    '\x28', '\x83', /* interplation speed 4 */
    '\x20', '\x00', /* buffer action point 5 */
    '\x3d', '\x83'  /* interplation speed 5 */
};

/*---------------------------------------------------------------------------
 *            MOT_Init()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initializes the Motorola radio module.
 *
 * Returns:   <See RADIO_Init in hcitrans.h>
 */
static BtStatus MOT_Init(void)
{
    wOpCode = HCC_RESET;
    return HCI_Reset();
}

/*---------------------------------------------------------------------------
 *            MOT_Shutdown()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Shut down the Motorola radio module.
 *
 * Returns:   <See RADIO_Shutdown in hcitrans.h>
 */
static BtStatus MOT_Shutdown(void)
{
    /* Motorola radio, set baud rate back to default (9600) */
    return radioSetMotorolaBaudRate(MOT_DEFAULT_BAUD);
}

/*---------------------------------------------------------------------------
 *            MOT_EventHandler()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handles HCI events for Motorola radios.
 *
 * Returns:   <See RADIO_EventHandler in hcitrans.h>
 */
void MOT_EventHandler(U8 Event, HciCallbackParms *Parms)
{
    BtStatus     status = Parms->status;
    U8          *cmdParms;
    static BOOL  motEncode = TRUE;

    if((Event == HCI_CONTROLLER_EVENT) &&
            (LEtoHost16(&(Parms->ptr.hciEvent->parms[1])) == HCC_RESET))
    {
        if(Parms->status == BT_STATUS_SUCCESS)
        {
            /* MOTOROLA radio, set baud rate */
            radioSetMotorolaBaudRate(getSpeedOption());
        }
        else
        {
            radioState = RADIO_STATE_INIT;
            /* Deliver event to Radio Manager */
            radioCallback(RADIO_INIT_STATUS, BT_STATUS_FAILED);
        }

        return;
    }

    cmdParms = Parms->ptr.hciEvent->parms;

    if((Event == HCI_CONTROLLER_EVENT) &&
            (Parms->ptr.hciEvent->event == HCE_COMMAND_COMPLETE))
    {
        if(LEtoHost16(&cmdParms[1]) == HCC_MOT_BAUD_RATE)
        {
            Report(("RADIO: HCC_Mot_Baud_Rate command complete status = %s\n",
                    pBT_Status(cmdParms[3])));

            if(status == BT_STATUS_SUCCESS)
            {
                /* Change the host port baud rate */
                if(radioState == RADIO_STATE_INIT)
                {
                    Report(("RADIO: setting UART rate to %d\n",
                            getSpeedOption()));
                    HCI_SetSpeed(getSpeedOption());
                }
                else
                {
                    Report(("RADIO: setting UART rate to %d\n",
                            MOT_DEFAULT_BAUD));
                    HCI_SetSpeed(MOT_DEFAULT_BAUD);
                }
            }

            if(radioState == RADIO_STATE_INIT)
            {
                if(status == BT_STATUS_SUCCESS)
                {
                    /* We are on our way up. Delay before radio is ready.
                     * some radios seem to require this.
                     */
                    timer.func = radioConfigMotorolaScoEncode;
                    EVM_StartTimer(&timer, 10);
                }
                else
                {
                    radioCallback(RADIO_INIT_STATUS, status);
                }
            }
            else
            {
                radioCallback(RADIO_SHUTDOWN_STATUS, status);
            }
        }
        else if(LEtoHost16(&cmdParms[1]) == HCC_WRITE_SCO_INTERPOLATION_DEFAULT)
        {
            Report(("RADIO: HCC_Write_Sco_Interpolation_Default command " \
                    "complete status = %s\n", pBT_Status(cmdParms[3])));

            if(status == BT_STATUS_SUCCESS)
            {
                if(motEncode == TRUE)
                {
                    /* Just finish encode direction, now do decode. */
                    motEncode = FALSE;
                    radioConfigMotorolaScoDecode();
                }
                else
                {
                    motEncode = TRUE;
                    radioCallback(RADIO_INIT_STATUS, status);
                }
            }
            else
            {
                radioCallback(RADIO_INIT_STATUS, status);
            }
        }
    }
}

/*---------------------------------------------------------------------------
 *            radioSetMotorolaBaudRate()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Send the HCI command to change baud rate for Motorola radios.
 */
static BtStatus radioSetMotorolaBaudRate(U16 speed)
{
    BtStatus status = BT_STATUS_PENDING;
    Report(("RADIO: Setting Motorola baud rate to %d\n", speed));

    switch(speed)
    {
        case 18432:
            hciCommand.parms[0] = 0x0D;
            break;

        case 9216:
            hciCommand.parms[0] = 0x0C;
            break;

        case 4608:
            hciCommand.parms[0] = 0x0B;
            break;

        case 2304:
            hciCommand.parms[0] = 0x0A;
            break;

        case 115:
            hciCommand.parms[0] = 0x09;
            break;

        case 57:
            hciCommand.parms[0] = 0x08;
            break;

        case 38:
            hciCommand.parms[0] = 0x07;
            break;

        case 19:
            hciCommand.parms[0] = 0x05;
            break;

        case 9:
            hciCommand.parms[0] = 0x03;
            break;

        default:
            /* Unsupported */
            status = BT_STATUS_FAILED;
            Report(("RADIO: Unsupported UART baud rate (%d)\n", speed));
            break;
    }

    if(status == BT_STATUS_PENDING)
    {
        HCI_SendRadioCommand(HCC_MOT_BAUD_RATE, 1, &hciCommand);
    }

    return status;
}

/*---------------------------------------------------------------------------
 *            radioConfigMotorolaScoEncode()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Configure SCO encode for Motorola radios.
 */
static void radioConfigMotorolaScoEncode(EvmTimer *timer)
{
    UNUSED_PARAMETER(timer);
    Report(("RADIO: sending HCC_Write_Sco_Interpolation_Default for encode.\n"));
    OS_MemCopy(hciCommand.parms, motScoInterpolationEncode,
               sizeof(motScoInterpolationEncode));
    HCI_SendRadioCommand(HCC_WRITE_SCO_INTERPOLATION_DEFAULT,
                         sizeof(motScoInterpolationEncode), &hciCommand);
}


/*---------------------------------------------------------------------------
 *            radioConfigMotorolaScoDecode()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Configure SCO decode for Motorola radios.
 */
static void radioConfigMotorolaScoDecode(void)
{
    Report(("RADIO: sending HCC_Write_Sco_Interpolation_Default for deccode.\n"));
    OS_MemCopy(hciCommand.parms, motScoInterpolationDecode,
               sizeof(motScoInterpolationDecode));
    HCI_SendRadioCommand(HCC_WRITE_SCO_INTERPOLATION_DEFAULT,
                         sizeof(motScoInterpolationDecode), &hciCommand);
}

#endif

