/****************************************************************************
 *
 * File:
 *     $Workfile:transprt.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:17$
 *
 * Description:
 *     Multiple radio support transport driver.
 * 
 * Copyright 2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions,
 * Inc.  This work contains confidential and proprietary information of
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret,
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "parseopts.h"
//#include "sys/usbtrans.h"
#include "sys/uarttran.h"
//#include "sys/abcsphci.h"

/*---------------------------------------------------------------------------
 *            TRAN_Init()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Called by the HCI driver to initialize the transport.  This 
 *            initialization can happen synchronously or asynchronously.
 *
 * Returns:   <See hcitrans.h>
 */
 /*
BtStatus TRAN_Init(TranCallback tranCallback)
{
    BtStatus status;

    Report(("TRAN:  Init started\n"));

    status = UARTTRAN_Init(tranCallback);

    return status;
}
*/

/*---------------------------------------------------------------------------
 *            TRAN_Shutdown()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Called by the HCI driver to shutdown the transport.  This 
 *            shutdown can happen synchronously or asynchronously.
 *
 * Returns:   <See hcitrans.h>
 */
 /*
BtStatus TRAN_Shutdown(void)
{
    BtStatus status;

    status = UARTTRAN_Shutdown();
    return status;
}*/

