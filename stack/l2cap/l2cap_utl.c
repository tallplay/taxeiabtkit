/***************************************************************************
 *
 * File:
 *     $Workfile:l2cap_utl.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:86$
 *
 * Description:
 *     This file contains utility functions used by L2CAP.
 *
 * Created:
 *     July 15, 1999
 *
 * Copyright 1999-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "btalloc.h"
#include "btrom.h"
#include "sys/l2cap_i.h"
#include "sys/l2capxp.h"
#include "sys/mei.h"

#if L2CAP_FLOW_CONTROL == XA_ENABLED
static const U8 L2CAP_BasicFEC[] = {4,9,0,0,0,0,0,0,0,0,0};
#endif /* L2CAP_FLOW_CONTROL == XA_ENABLED */

/****************************************************************************
 *
 * Internal Function prototypes
 *
 ****************************************************************************/
static void L2CAP_TimerFire(EvmTimer *Timer);
static L2CAP_Channel *AllocChannel(const L2capPsm *Protocol);
#if L2CAP_FLOW_CONTROL == XA_ENABLED
static U16 L2Cap_StartCrc(U16 RemoteCid, U16 Length);
#endif

/*---------------------------------------------------------------------------
 *            L2CAP_Init()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Called when the stack is initializing to init the L2CAP module.
 *
 * Return:    BT_STATUS_SUCCESS
 */
BtStatus L2CAP_Init(void)
{
    I16    i;

    Report(("L2CAP: Initializing for %d protocols, for %d groups, for %d devices, with %d channels.\n",
            L2CAP_NUM_PROTOCOLS, L2CAP_NUM_GROUPS, NUM_BT_DEVICES, L2CAP_NUM_CHANNELS));

    Assert(BT_PACKET_HEADER_LEN >= 14);

    /* Init protocol slots */
    for (i = 0; i < L2CAP_NUM_PROTOCOLS; i++)
        L2C(protocols)[i] = 0;

    /* Init channel pool */
    for (i = 0; i < L2CAP_NUM_CHANNELS; i++) {
        L2C(channels)[i].state = L2C_CLOSED;
        L2C(channels)[i].localCid = L2CID_INVALID;
    }

#if L2CAP_NUM_GROUPS > 0
    /* Init groups */
    for (i = 0; i < L2CAP_NUM_GROUPS; i++) {
        L2C(groups)[i] = 0;
    }
#endif

    /* Init Signal Packet Pool */
    InitializeListHead(&L2C(signalPackets));
    for (i = 0; i < L2CAP_NUM_SIGNAL_PACKETS + L2CAP_SIGNAL_DEPTH; i++) {
        InsertTailList(&L2C(signalPackets), &L2C(_sigPacket)[i].packet.node);
    }

    /* Init Broadcast 'Link'. It only sends Piconet type broadcasts.
     */
    L2C(broadcastLink).state = BRXS_BROADCAST;
    L2C(broadcastLink).hciHandle = PICONET_BC_HANDLE;
    InitializeListHead(&L2C(broadcastLink).txQueue);

    L2C(nextIdent) = 1;
    L2C(flags) = L2CCF_BROADCASTS_ENABLED;

    return BT_STATUS_SUCCESS;
}

void L2CAP_Deinit(void)
{
#if XA_DEBUG == XA_ENABLED
    I16   i;

    /* Verify that no packets are outstanding */
    for (i = 0; i < L2CAP_NUM_SIGNAL_PACKETS + L2CAP_SIGNAL_DEPTH; i++) {
        Assert(L2Cap_GetSysPkt(TRUE));
    }
#endif

    return;
}


/*
 * L2Cap_GetValidChannel
 */
L2CAP_Channel *L2Cap_GetValidChannel(U16 Cid)
{
    L2CAP_Channel  *ch;

    /* Check that the CID is in range */
    if (Cid < BASE_DYNAMIC_CID || Cid > LAST_DYNAMIC_CID )
        return 0;

    ch = &L2C(channels)[CID_TO_INDEX(Cid)];

    /* Check that the channel is in use */
    if ((ch->localCid == L2CID_INVALID) || (ch->state == L2C_CLOSED))
        return 0;

    Assert(ch->localCid == Cid);
    return ch;
}


#if L2CAP_NUM_GROUPS > 0
/*
 * L2Cap_GetValidGroup
 */
L2capGroup *L2Cap_GetValidGroup(U16 Cid)
{
    L2capGroup  *group;

    /* Check that the CID is in range */
    if (Cid < BASE_GROUP_CID || Cid > LAST_GROUP_CID )
        return 0;

    group = L2C(groups)[GROUP_CID_TO_INDEX(Cid)];

    /* Check that the Group is registered */
    if ((group == 0) || (group->cid == L2CID_INVALID))
        return 0;

    Assert(group->cid == Cid);
    return group;
}
#endif

#if (XA_ERROR_CHECK == XA_ENABLED) || (XA_DEBUG == XA_ENABLED)
/*
 * L2Cap_IsValidProtocol
 *
 */
BOOL L2Cap_IsValidProtocol(const L2capPsm *Protocol)
{
    I8    p;

    for (p = 0; p < L2CAP_NUM_PROTOCOLS; p++) {
        if (L2C(protocols)[p] == Protocol)
            return TRUE;
    }

    return FALSE;
}
#endif /* XA_ERROR_CHECK == XA_ENABLED */

/*
 * L2Cap_FindChannelByLocalIdent
 *
 * Locate a channel based on its local signalling identifier value.
 */
L2CAP_Channel *L2Cap_FindChannelByLocalIdent(U8 Ident)
{
    I16    i;

    for (i = 0; i < L2CAP_NUM_CHANNELS; i++) {
        if (L2C(channels)[i].localCid == L2CID_INVALID)
            continue;

        if (L2C(channels)[i].localIdent == Ident) {
            return L2C(channels)+i;
        }
    }
    return 0;
}

/*
 * L2Cap_CheckForDupilicateRequest
 *
 * Compares the current request to the last request received from the
 * given device. Returns TRUE if this request is the same as the last.
 */
L2CAP_Channel *L2Cap_CheckForDupilicateRequest(U8 Ident, BtRemoteDevice *Link)
{
    I16    i;

    for (i = 0; i < L2CAP_NUM_CHANNELS; i++) {

        if (L2C(channels)[i].localCid == L2CID_INVALID)
            continue;

        if ((L2C(channels)[i].remoteIdent == Ident) &&
            (L2C(channels)[i].link == Link)) {
            return L2C(channels)+i;
        }
    }
    return 0;

}

/*
 * L2Cap_AllocLinkChannel
 *
 * Allocates a channel and associates it with a link.
 */
L2CAP_Channel *L2Cap_AllocLinkChannel(const L2capPsm *Psm, BtRemoteDevice *Link, BtStatus *Error)
{
    L2CAP_Channel  *new_channel;

    if (Link && Link->state == BDS_CONNECTED) {

        new_channel = AllocChannel(Psm);
        if (new_channel) {

            Link->refCount++;
            new_channel->link = Link;

            *Error = BT_STATUS_SUCCESS;
            return new_channel;
        } 
        else *Error = BT_STATUS_NO_RESOURCES;
    } 
    else *Error = BT_STATUS_NO_CONNECTION;

    return 0;
}


/*
 * L2Cap_AllocPsmChannel
 *
 * Allocates a channel for the given PSM.
 * Search the list of registered protocols looking for a matching PSM 
 */
L2CAP_Channel *L2Cap_AllocPsmChannel(U16 Psm, U16 *Reason)
{
    I8    p;

    for (p = 0; p < L2CAP_NUM_PROTOCOLS; p++) {

        if (L2C(protocols)[p] && (L2C(protocols)[p]->psm == Psm)) {
            /* Found the PSM, now find a free channel. If the Alloc fails,
             * we already set the appropriate error code (which should be
             * ignored if were successful).
             */
            *Reason = L2CONN_REJECT_NO_RESOURCES;
            return AllocChannel(L2C(protocols)[p]);
        }
    }

    *Reason = L2CONN_REJECT_PSM_NOT_SUPPORTED;
    return 0;
}

/*
 * AllocChannel
 *
 * Find a free channel and return it.
 */
static L2CAP_Channel *AllocChannel(const L2capPsm *Protocol)
{
    I16    i;

    for (i = 0; i < L2CAP_NUM_CHANNELS; i++) {

        if (L2C(channels)[i].localCid == L2CID_INVALID) {

            Assert(L2C(channels)[i].state == L2C_CLOSED);

            OS_MemSet((U8 *)(L2C(channels) + i), 0, (U32)sizeof(L2CAP_Channel));

            L2C(channels)[i].flags = L2CHF_AUTO_CONFIG;
            L2C(channels)[i].psmInfo = Protocol;
            L2C(channels)[i].localCid = BASE_DYNAMIC_CID + (U16)i;
            L2C(channels)[i].remoteCid = L2CID_INVALID;
            L2C(channels)[i].outMtu = L2CAP_DEFAULT_MTU;

            if (Protocol) {
                Report(("L2CAP: AllocChannel() localCid %04x, PSM %04x\n", 
                        L2C(channels)[i].localCid, Protocol->psm));

                L2C(channels)[i].localMtu = Protocol->localMtu;
            }
            XASTAT_PeakInc(l2cap_num_channels, 1);
            return &L2C(channels)[i];
        }
    }
    return 0;
}

/*
 * L2Cap_NotifyUserAndCloseChannel
 */
void L2Cap_NotifyUserAndCloseChannel(L2CAP_Channel *Channel, U16 Status)
{
    Channel->state = L2C_CLOSING;

    L2Cap_FlushChannelTransmits(Channel);

#if XA_DEBUG == XA_ENABLED
    /* Ensure that no channel owned packets are on the transmit queue. */
    {
        BtPacket *packet = (BtPacket *)GetHeadList(&Channel->link->txQueue);

        while (packet != (BtPacket *)&Channel->link->txQueue) {
            Assert(packet->llpContext != Channel->localCid);
            packet = (BtPacket *)GetNextNode(&packet->node);
        }
    }
#endif /* XA_DEBUG == XA_ENABLED */

    if (Channel->inUse > 0) {
        /* Defer L2EVENT_DISCONNECTED indication until after packets complete. */
        Channel->result = Status;
        return;
    }

    Channel->state = L2C_CLOSED;

    if ((Channel->flags & L2CHF_NOT_VISIBLE) == 0)
        L2Cap_NotifyPsm(Channel, L2EVENT_DISCONNECTED, Status);

    L2Cap_FreeChannel(Channel);
}

/*
 * L2Cap_FreeChannel
 *
 * Return an allocated channel to the free pool.
 */
void L2Cap_FreeChannel(L2CAP_Channel *Channel)
{
    BtRemoteDevice *link;

    Report(("L2CAP: L2Cap_FreeChannel(%04x), PSM %04x, %d packets outstanding%s.\n",
            Channel->localCid, Channel->psmInfo->psm, Channel->inUse, 
            (Channel->flags & L2CHF_SEC_PENDING ? ", Security Pndg": "") ));

    Channel->state = L2C_CLOSED;

#if L2CAP_FLOW_CONTROL == XA_ENABLED
    if ((Channel->flags & (L2CHF_RTX_ACTIVE|L2CHF_ERTX_ACTIVE)) || 
        (Channel->flags2 & (L2CHF2_RETRANS_ACTIVE|L2CHF2_MONITOR_ACTIVE))) {
#else
    if ((Channel->flags & (L2CHF_RTX_ACTIVE|L2CHF_ERTX_ACTIVE))) {
#endif
        EVM_CancelTimer(&Channel->timer);
    }

    /* If we have a security request outstanding we will cleanup on
     * the access response callback.
     */
    if (Channel->flags & L2CHF_SEC_PENDING) {
        return;
    }

    Channel->localCid = L2CID_INVALID;

    XASTAT_PeakDec(l2cap_num_channels, 1);

    if (0 != (link = Channel->link)) {
        Assert((signed)link->refCount > 0);
        link->refCount--;

        Channel->link = 0;
        /* If everyone is done with the link then disconnect it. */
        if (IsListEmpty(&(link->handlers)) && (link->refCount == 0) && 
            link->discFlag) {

            MeDisconnectLink(link);
        }
    }
}


/*
 * Signalling Packet Pool functions
 */
BtPacket *L2Cap_GetSysPkt(BOOL Priority)
{
    SignalPacket *pkt;
    I8            i = 0;

    if (IsListEmpty(&L2C(signalPackets))) {
        return 0;
    }

    if (Priority == FALSE) {
        pkt = (SignalPacket *)GetHeadList(&L2C(signalPackets));
        while (pkt != (SignalPacket *)&L2C(signalPackets)) {
            if (++i > L2CAP_SIGNAL_DEPTH) {
                break;
            }
            pkt = (SignalPacket *)GetNextNode(&pkt->packet.node);
        }

        if (i <= L2CAP_SIGNAL_DEPTH) {
            return 0;
        }
    }
    pkt = (SignalPacket *)RemoveHeadList(&L2C(signalPackets));

    pkt->packet.data = pkt->workspace;
    pkt->packet.dataLen = pkt->packet.tailLen = pkt->packet.headerLen = 0;
    pkt->packet.flags = BTP_FLAG_LSYSTEM;

    XASTAT_PeakInc(l2cap_num_signal_packets, 1);
    return &pkt->packet;
}


/*
 * L2Cap_GetMtu()
 */
U16 L2Cap_GetMtu(U8 *Options, U16 OptionsLen)
{
    while (OptionsLen) {

        if ((*Options & ~CONFIG_OPTION_HINT) == L2CFG_OPTION_MTU) {
            return LEtoHost16(Options+2);
        }

        OptionsLen -= Options[1]+2;
        Options += Options[1]+2;

        Assert(OptionsLen < 0x8000);
    }

    return 0;
}


#if L2CAP_FLOW_CONTROL == XA_ENABLED
/*
 * L2Cap_InitFlowControlSettings()
 */
void L2Cap_InitFlowControlSettings(L2CAP_Channel *Channel)
{
    U8  linkMode = (Channel->psmInfo->inLinkMode & (Channel->link->linkModes|L2MODE_BASIC));

    Assert(linkMode != L2MODE_UNKNOWN);

    /* Select channels initial link mode (to be negotiated). */
    if (linkMode & L2MODE_RETRANSMISSION)
        Channel->inLinkMode = LINK_MODE_RETRANSMISSION;
    else if (linkMode & L2MODE_FLOW_CONTROL)
        Channel->inLinkMode = LINK_MODE_FLOW_CONTROL;
    else Channel->inLinkMode = LINK_MODE_BASIC;

    if (linkMode != L2MODE_BASIC) {
        Channel->outMpu = L2CAP_MIN_MTU;
        Channel->localMpu = Channel->psmInfo->localMtu;
        Channel->transmitMax = 3; /* DEFAULT */

        /* Calculate available buffer space in MPU segments (plus 1). */
        Channel->rxWindow = (U8)(Channel->psmInfo->inBufferSize / Channel->localMpu);
        Assert((Channel->rxWindow > 0) && (Channel->rxWindow <= 32));

        Report(("L2CAP: AllocChannel() MTU %d, MPU %d, rxWindow %d\n", 
                Channel->localMtu, Channel->localMpu, Channel->rxWindow));
    }
}

/*
 * L2Cap_SetFlowControlOptions()
 */
U16 L2Cap_SetFlowControlOptions(L2CAP_Channel *Channel, const U8 *Options, U16 OptionsLen)
{
    U16 result = L2CFG_RESULT_ACCEPTED;

    while (OptionsLen) {

        if ((*Options & ~CONFIG_OPTION_HINT) == L2CFG_OPTION_FLOW_CONTROL) {
            /* Setup config response option values in case of failure. */
            OS_MemCopy(Channel->workspace, Options, 11);
            Channel->wsLen = 11;

            Channel->flags2 |= L2CHF2_FOUND_FC_OPTION;  /* Note that the options were received */

            if (Options[1] != 9) {
                /* Malformed option */
                return L2CFG_RESULT_REJECTED;
            }

            /* Convert protocol enumeration to a flag for comparison with API link modes. */
            if (Channel->psmInfo->outLinkMode & (1 << Options[2])) {
                /* Set the accepted link operating mode */
                Channel->outLinkMode = Options[2];
            } else {
                /* Link mode not acceptable, tell them what we would accept. */
                if (Channel->psmInfo->outLinkMode & L2MODE_RETRANSMISSION)
                    Channel->workspace[2] = LINK_MODE_RETRANSMISSION;
                else if (Channel->psmInfo->outLinkMode & L2MODE_FLOW_CONTROL)
                    Channel->workspace[2] = LINK_MODE_FLOW_CONTROL;
                else Channel->workspace[2] = LINK_MODE_BASIC;

                result = L2CFG_RESULT_UNACCEPTABLE;
            }

            /* Check the parms based on the link mode that we are/may be sending back. */
            if (Channel->workspace[2] != LINK_MODE_BASIC) {

                Channel->txWindow = Options[3];
                if ((Channel->txWindow < 1) || (Channel->txWindow > 32)) {
                    /* Invalid parameter value. */
                    Channel->workspace[3] = 1;
                    result = L2CFG_RESULT_UNACCEPTABLE;
                }

                if ((Channel->transmitMax = Options[4]) < 1) {
                    /* Invalid parameter value. */
                    Channel->workspace[4] = 1;
                    result = L2CFG_RESULT_UNACCEPTABLE;
                }

                Channel->outMpu = LEtoHost16(Options+9);
                if (Channel->outMpu == 0) {
                    /* Invalid parameter value, suggest 4 bytes. */
                    Channel->workspace[9] = 4;
                    result = L2CFG_RESULT_UNACCEPTABLE;
                }

                Channel->retransTimeout = LEtoHost16(Options+5);
                Channel->monitorTimeout = LEtoHost16(Options+7);
            }

            if (result == L2CFG_RESULT_ACCEPTED)
                Channel->wsLen = 0; /* Erase failure response */

            return result;
        }

        OptionsLen -= Options[1]+2;
        Options += Options[1]+2;

        Assert(OptionsLen < 0x8000);
    }

    if (((Channel->flags2 & L2CHF2_FOUND_FC_OPTION) == 0) &&
        ((Channel->flags & L2CHF_MORE_CONFIG) == 0)) {
        /* We did not recieve any Flow Control options, and this is the
         * last (or only) configuration request. Run a basic (the default)
         * Flow Control option through the parser to see if it is supported.
         */
        return L2Cap_SetFlowControlOptions(Channel, L2CAP_BasicFEC, sizeof(L2CAP_BasicFEC));
    }

    return L2CFG_RESULT_ACCEPTED;
}

/*---------------------------------------------------------------------------
 *            L2Cap_GetNextSegment()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  
 *            
 *
 * Return:    
 *            
 */
BOOL L2Cap_GetNextSegment(BtPacket *Segment, BtPacket *Sdu)
{
    I16             dlen = 0, hlen = 0, tlen = 0, i = 0;
    I16             dataLen, headerLen, tailLen, pduLen;
    L2CAP_Channel  *channel;

    Assert(Sdu && Segment);

    channel = &L2C(channels)[CID_TO_INDEX(Sdu->llpContext)];

    if (Sdu->segNum == 0) {
        /* Initialize the SDU for transmission. */
        Sdu->segStart = channel->sendSeq;
        i = (Sdu->dataLen + Sdu->headerLen + Sdu->tailLen);
        Sdu->segNum = (U8)((i / channel->outMpu) + ((i % channel->outMpu) ? 1 : 0));

        /* Bump the segNum for zero length SDU's */
        if (Sdu->segNum == 0)
            Sdu->segNum = 1;

        Assert((Sdu->segNum > 0) && (Sdu->segNum < 0x3f));
    }

    if (Sdu->segNum <= ((channel->sendSeq - Sdu->segStart) & 0x3f)) {
        /* All segments of SDU have been sent. */
        return FALSE;
    }
    Report(("L2CAP: GetNextSegment(%02x) SDU %x. Segment %d of %d: N(S) = %d.\n", channel->localCid, 
            Sdu, ((channel->sendSeq - Sdu->segStart) & 0x3f) + 1, Sdu->segNum, channel->sendSeq));

    *Segment = *Sdu;

    dataLen = Sdu->dataLen;
    headerLen = Sdu->headerLen;
    tailLen = Sdu->tailLen;

    for (i = 0; i <= (U8)((channel->sendSeq - Sdu->segStart) & 0x3f); i++) {
        if ((dataLen + headerLen + tailLen) < channel->outMpu)
            pduLen = (dataLen + headerLen + tailLen);
        else pduLen = channel->outMpu;

        hlen = min(pduLen, headerLen);
        dlen = min(pduLen-hlen, dataLen);
        tlen = min(pduLen-hlen-dlen, tailLen);

        if (i < (U8)((channel->sendSeq - Sdu->segStart) & 0x3f)) {
            /* skip over sent data in SDU (note: not executed on last pass) */
            headerLen -= hlen;
            dataLen -= dlen;
            tailLen -= tlen;
        }
    }
    Assert(i <= Sdu->segNum);

    /* Update segment pointers to skip over sent SDU data and 
     * set segment field lengths to a MPU sized buffer.
     */
    Segment->headerLen = (U8)hlen;
    Segment->data += (Sdu->dataLen - dataLen);
    Segment->dataLen = (U16)dlen;
    Segment->tail += (Sdu->tailLen - tailLen);
    Segment->tailLen = (U16)tlen;

    /* Cache the total SDU length in the segment ulpContext field. */
    Segment->ulpContext = (void *)(Sdu->dataLen + Sdu->headerLen + Sdu->tailLen);

    Assert((Segment->headerLen + Segment->dataLen + Segment->tailLen) <= channel->outMpu);
    return TRUE;
}


/*---------------------------------------------------------------------------
 *            L2Cap_BuildIFrameHeader()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  
 *            
 *
 * Return:    
 *            
 */
void L2Cap_BuildIFrameHeader(BtPacket *Segment)
{
    L2CAP_Channel  *channel;
    I8              offset;
    U8              sar;        /* SAR field shifted to bits 16:15 */
    U16             fcs;

    channel = &L2C(channels)[CID_TO_INDEX(Segment->llpContext)];

    offset = BT_PACKET_HEADER_LEN - Segment->headerLen - 2;

    /* Append the I-Frame header to the packet. */ 
    if (Segment->segStart == channel->sendSeq) {
        if (Segment->segNum > 1) {
            /* First segment in SDU, insert the SDU length. */
            Assert(Segment->ulpContext > 0);

            sar = 0x40;     /* Start of L2CAP SDU */

            Segment->header[offset+0] = (U8)((U32)(Segment->ulpContext));
            Segment->header[offset+1] = (U8)((U32)(Segment->ulpContext) >> 8);
            Segment->headerLen += 2;
            offset -= 2;
        }
        else sar = 0x00;     /* Unsegmented L2CAP SDU */

    } else {

        /* Continuation or last segment in SDU */
        if (((Segment->segStart + Segment->segNum - 1) & 0x3f) == channel->sendSeq)
            sar = 0x80;     /* End of L2CAP SDU */
        else sar = 0xC0;    /* Continuation of L2CAP SDU */
    }
    
    /* Add the Control fields: N(S), N(R) (aka V(B)), SAR. */
    Segment->header[offset+0] = (U8)((channel->sendSeq << 1));
    Segment->header[offset+1] = (U8)((channel->buffSeq) | sar);
    Segment->headerLen += 2;

    if (channel->flags2 & L2CHF2_NO_RETRANS_IN) {
        Assert(channel->inLinkMode == LINK_MODE_RETRANSMISSION);
        /* The application has requested that retransmissions be suspended. */
        Segment->header[offset+0] |= 0x80;
    }

    /* Increment the send sequence number. */
    channel->sendSeq = (channel->sendSeq + 1) & 0x3f;

    /* We're sending an updated N(R) so clear the Send-RR flag. */
    channel->flags2 &= ~L2CHF2_SEND_RR;

    /* Append FCS to I-Frame */
    fcs = L2Cap_StartCrc(channel->remoteCid, (U16)(Segment->headerLen + Segment->dataLen + Segment->tailLen + 2));
    fcs = L2Cap_ComputeCrc(Segment->header + offset, Segment->headerLen, fcs);
    fcs = L2Cap_ComputeCrc(Segment->data, Segment->dataLen, fcs);
    fcs = L2Cap_ComputeCrc(Segment->tail, Segment->tailLen, fcs);

    StoreLE16(Segment->fcs, fcs);
    Segment->flags |= BTP_FLAG_FCS;

    Assert((Segment->headerLen + Segment->dataLen + Segment->tailLen + 2) <= channel->outMpu + (sar == 0x40 ? 6 : 4));
}


/*---------------------------------------------------------------------------
 *            L2Cap_SendSFrame()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  
 *            
 */
void L2Cap_SendSFrame(L2CAP_Channel *Channel)
{
    BtPacket   *frame;
    BtStatus    status;

    Channel->flags &= ~L2CHF_SEND_REQUEST;

    if (0 != (frame = L2Cap_GetSysPkt(FALSE))) {
        /* Put the empty frame on the TX Queue. The L2CAP_HciTxReady() 
         * function will fill in the S-Frame information.
         */
        status = L2Cap_Send(Channel, frame, Channel->remoteCid);
        if (status == BT_STATUS_PENDING) {
            if (Channel->flags2 & L2CHF2_SEND_REJECT)
                Channel->flags2 |= L2CHF2_REJECT_ACTIVE;
            return;
        }

        /* Send failed, return the packet. */
        ReturnSysPkt(frame);
    }       

    Channel->flags |= L2CHF_SEND_REQUEST;
    L2C(flags) |= L2CCF_NEED_SYSPACKET;
}


/*---------------------------------------------------------------------------
 *            L2Cap_BuildSFrame()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  
 *            
 *
 * Return:    
 *            
 */
BtPacket *L2Cap_BuildSFrame(L2CAP_Channel *Channel, BtPacket *Packet)
{
    U16     fcs = 0;
    U8      type, nr;

    /* Need to send an RR or a Reject. We only need to send one,
     * but if both are flagged, the the Reject takes precidence. 
     */
    if (Channel->flags2 & L2CHF2_SEND_REJECT) {
        type = 0x05;            /* Reject */
        nr  = Channel->recvSeq;
    }
    else if (Channel->flags2 & L2CHF2_SEND_RR) {
        type = 0x01;            /* RR */
        nr  = Channel->buffSeq;
    } 
    else {
        /* N(R) was already sent in an I-Frame. Return the system packet. */
        Channel->inUse--;
        ReturnSysPkt(Packet);
        return 0;
    }

    Channel->flags2 &= ~(L2CHF2_SEND_REJECT|L2CHF2_SEND_RR);

    if (Channel->flags2 & L2CHF2_NO_RETRANS_IN) {
        Assert(Channel->inLinkMode == LINK_MODE_RETRANSMISSION);
        /* The application has requested that retransmissions be suspended. */
        type |= 0x80;   /* Set Retransmission disable bit */
    }

    /* Build the S-Frame */
    Packet->header[BT_PACKET_HEADER_LEN - 4] = type;
    Packet->header[BT_PACKET_HEADER_LEN - 3] = nr;

    /* Append FCS to S-Frame */
    fcs = L2Cap_StartCrc(Channel->remoteCid, 4);
    fcs = L2Cap_ComputeCrc(Packet->header + BT_PACKET_HEADER_LEN - 4, 2, fcs);

    Packet->header[BT_PACKET_HEADER_LEN - 2] = (U8)(fcs);
    Packet->header[BT_PACKET_HEADER_LEN - 1] = (U8)(fcs >> 8);
    Packet->headerLen = 4;

    Report(("L2CAP: BuildSFrame(%04x) Sent %s Packet: N(R) = %d.\n", Channel->localCid,
            ((type & 0x0d) == 1 ? "RR":"Reject"), nr));

    return Packet;
}


/*---------------------------------------------------------------------------
 *            L2Cap_ProcessNr()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  
 *            
 *
 * Return:    
 *            
 */
BOOL L2Cap_ProcessNr(L2CAP_Channel *Channel, U8 Control)
{
    U8  acknum, unacked;

    acknum = (U8)(((Control & 0x3f) - Channel->ackSeq) & 0x3f);
    unacked = (U8)((Channel->sendSeq - Channel->ackSeq) & 0x3f);

    Report(("L2CAP: ProcessNr(%02x) acknum = %d, unacked = %d.\n", Channel->localCid, acknum, unacked));

    /* N(R) is valid if: mod64(N(R) - V(A)) <= mod64(V(S) - V(A)) */
    if (acknum <= unacked) {
        /* Valid N(R), update V(A). */
        Channel->ackSeq = (U8)(Control & 0x3f);

        if (acknum > 0) {
            L2Cap_StopTimer(Channel);

            /* If packets are queued, request to send data. */
            if (Channel->inUse > 0) {
                /* Return any packets acknowledged by the N(R) we just received. */
                L2Cap_ReturnSentPackets(Channel, BT_STATUS_SUCCESS);
                L2Cap_IssueHciRequestToSend(Channel->link);
            }

            if (Channel->state == L2C_OPEN) {
                if ((acknum < unacked) && !(Channel->flags2 & L2CHF2_NO_RETRANS_OUT))
                    L2Cap_StartRetransmitTimer(Channel);
                else L2Cap_StartMonitorTimer(Channel);
            }
        }

        return TRUE;
    }
    
    /* Invalid N(R): If we're retransmitting, and the value is within the window
     * it could be for a segment we've queue'd for retransmit so let it slide. 
     */
    if ((Channel->transmitCount > 0) && (acknum <= Channel->txWindow)) {
        /* Note we could accept this into Channel->ackSeq to reduce the
         * retransmissions, but we'd need to know that it acks data we did
         * indeed send. And we don't know what the old sendseq was before 
         * the timeout.
         */
        return TRUE;
    }

    /* Invalid N(R): In flow control only mode, we automatically advance the
     * ackSeq on timer expiration. This could cause us to receive an apparently
     * bad Nr if the packet is very delayed. Just ignore it.
     */
    if (Channel->inLinkMode == LINK_MODE_FLOW_CONTROL) {
        return TRUE;
    }

    /* Invalid N(R): Close the channel! */
    return FALSE;
}


/*---------------------------------------------------------------------------
 *            L2Cap_ProcessNs()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  
 *            
 */
BOOL L2Cap_ProcessNs(L2CAP_Channel *Channel, U8 Control)
{
    U8  ns;

    ns = (U8)((Control >> 1) & 0x3f);

    if (ns == Channel->recvSeq) {
        /* N(S) is in-sequence, process the frame. V(R) is updated later. */
        Channel->flags2 &= ~L2CHF2_REJECT_ACTIVE;
        return TRUE;
    }

    /* IMHO: A valid N(S) is within rxWindow of V(R)-1 either greater or less than.
     * This is more robust than is specified. So
     *    V(R) - rxWindow <= N(S) < V(R) + rxWindow
     */
    if (((ns - Channel->recvSeq) & 0x3f) < Channel->rxWindow) {
        /* N(S) is valid but it's out-of-sequence. Send a reject if we haven't already. */
        if ((Channel->inLinkMode == LINK_MODE_RETRANSMISSION) &&
            ((Channel->flags2 & L2CHF2_REJECT_ACTIVE) == 0)) {

            /* Send a REJECT Packet */
            Channel->flags2 |= L2CHF2_SEND_REJECT;
            L2Cap_SendSFrame(Channel);
        }
        /*  Allow processing of the frame's control fields. */
        return TRUE;
    }

    if (((Channel->recvSeq - ns) & 0x3f) <= Channel->rxWindow) {
        /* N(S) is valid but it's a duplicate. Allow processing 
         * of the frame's control fields. 
         */
        return TRUE;
    }

    /* N(S) is invalid, don't process frame. */
    return FALSE;
}


/*---------------------------------------------------------------------------
 *            L2Cap_ProcessRbit()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  
 *            
 *
 * Return:    
 *            
 */
void L2Cap_ProcessRbit(L2CAP_Channel *Channel, U8 Control)
{
    /* Check for retransmission disable flag changes. */
    if (Control & 0x80) {
        if ((Channel->flags2 & L2CHF2_NO_RETRANS_OUT) == 0) {
            /* The remote peer just disabled retransmissiions. 
             * Stop the retransmission timer and start the monitor timer.
             */
            Channel->flags2 |= L2CHF2_NO_RETRANS_OUT;

            if (Channel->flags2 & L2CHF2_RETRANS_ACTIVE)
                L2Cap_StopTimer(Channel);

            L2Cap_StartMonitorTimer(Channel);
        }
    }
    else if (Channel->flags2 & L2CHF2_NO_RETRANS_OUT) {
        /* Remote side just enabled retransmissions. */
        Channel->flags2 &= ~L2CHF2_NO_RETRANS_OUT;

        /* Stop Monitor timer and request to send now that we're unblocked. */
        L2Cap_StopTimer(Channel);
        L2Cap_IssueHciRequestToSend(Channel->link);

        /* If segments in transit, start the retransmit timer. */
        if (Channel->sendSeq != Channel->ackSeq)
            L2Cap_StartRetransmitTimer(Channel);

    }
}


/*---------------------------------------------------------------------------
 *            L2Cap_ReturnSentPackets()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  
 *            
 *
 * Return:    
 *            
 */
void L2Cap_ReturnSentPackets(L2CAP_Channel *Channel, BtStatus Status)
{
    BtPacket       *packet, *next;

    packet = (BtPacket *)GetHeadList(&Channel->link->txQueue);
    while (packet != (BtPacket *)&Channel->link->txQueue) {
        next = (BtPacket *)GetNextNode(&packet->node);

        if (packet->llpContext == Channel->localCid) {
            if ((packet->remoteCid != 0x0001) && (packet->segNum > 0) &&
                ((packet->flags & BTP_FLAG_LSYSTEM) == 0)) {
                
                /* It's an BT1.2 connection oriented data packet. */
                if (packet->segNum <= ((Channel->ackSeq - packet->segStart) & 0x3f)) {
                    /* All segments have been acknowledged, return it to the application. */
                    Assert(((Channel->ackSeq - packet->segStart) & 0x3f) < 64);
                    RemoveEntryList(&packet->node);
                    L2CAP_HciTxDone(packet, Status, Channel->link->hciHandle);
                }
                else break;
            }
        }
        packet = next;
    }
}
#endif /* L2CAP_FLOW_CONTROL == XA_ENABLED */


/*---------------------------------------------------------------------------
 *            L2Cap_FlushChannelTransmits()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  
 *            
 *
 * Return:    
 *            
 */
void L2Cap_FlushChannelTransmits(L2CAP_Channel *Channel)
{
    BtPacket    *packet, *next;

    packet = (BtPacket *)GetHeadList(&Channel->link->txQueue);
    while (packet != (BtPacket *)&Channel->link->txQueue) {
        next = (BtPacket *)GetNextNode(&packet->node);

        if (packet->llpContext == Channel->localCid) {
            RemoveEntryList(&packet->node);
            L2CAP_HciTxDone(packet, BT_STATUS_NO_CONNECTION, Channel->link->hciHandle);
        }
        packet = next;
    }
}


/*
 * L2Cap_NotifyPsm()
 */
void L2Cap_NotifyPsm(L2CAP_Channel *Channel, L2capEventType Event, U16 Status)
{
    if (Channel->flags & L2CHF_NOT_VISIBLE) {
        /* The Upper layer protocol or applicaiton does not know of this
         * channel so consume all events.
         */
        Report(("L2CAP: NotifyPsm() Consumed notification event %x.\n", Event));
        return;
    }

    L2C(callbackParms).event = Event;
    L2C(callbackParms).status = Status;

    Channel->psmInfo->callback(Channel->localCid, &L2C(callbackParms));
    L2C(callbackParms).event = 0;
}


/*
 * ValidateOptions()
 */
static BOOL ValidateOptions(const U8 *Options, U16 OptionsLen, U16 *Len)
{
    *Len = 0;

    while (OptionsLen) {
        Report(("L2CAP: ValidateOptions() Option %d, Len %d\n", *Options, *(Options+1)));
        *Len += Options[1]+2; 

        /* Validate Options 1 by 1 ?? */

        OptionsLen -= Options[1]+2;
        Options += Options[1]+2;

        if (OptionsLen >= 0x8000) {
            /* Invalid Options encoding */
            return FALSE;
        }
    }

    /* Options encoding is valid */
    return TRUE;
}

BOOL L2Cap_ValidateCommand(const U8 *RxData, U16 RxDataLen)
{
    U16   len, cmdLen;

    while (RxDataLen) {

        /* First make sure the claimed PDU size is all there. */
        len = LEtoHost16(RxData+2) + SIGNAL_HEADER_LEN;

        if (RxDataLen < len) {
            return FALSE;
        }
        cmdLen = 0;

        /* Now, Calculate the required PDU size. */
        switch (RxData[0]) {
        case LLC_COMMAND_REJ:
            switch (LEtoHost16(RxData+4)) { /* Reason */
            case 0:
                cmdLen = 6;
                break;
            case 1:
                cmdLen = 8;
                break;
            case 2:
                cmdLen = 10;
                break;
            default:
                cmdLen = len;
            }
            break;

        case LLC_CONN_REQ:
            cmdLen = 8;
            break;

        case LLC_CONN_RSP:
            cmdLen = 12;
            break;

        case LLC_CONFIG_REQ:
            if ((len > 8) && 
                (!ValidateOptions(RxData+8, (U16)(len-8), &cmdLen))) {
                return FALSE;
            }
            cmdLen += 8;
            break;

        case LLC_CONFIG_RSP:
            if ((len > 10)  && 
                (!ValidateOptions(RxData+10, (U16)(len-10), &cmdLen))) {
                return FALSE;
            }
            cmdLen += 10;
            break;

        case LLC_DISC_REQ:
            cmdLen = 8;
            break;

        case LLC_DISC_RSP:
            cmdLen = 8;
            break;

        case LLC_ECHO_REQ:
            cmdLen = len;
            break;

        case LLC_ECHO_RSP:
            cmdLen = len;
            break;

        case LLC_INFO_REQ:
            if ((LEtoHost16(RxData+4) == L2INFO_CONNECTIONLESS_MTU) ||
                (LEtoHost16(RxData+4) == L2INFO_EXTENDED_FEATURES))
                cmdLen = 6;
            else cmdLen = len; /* Unknown INFO_TYPE */
            break;

        case LLC_INFO_RSP:
            if (LEtoHost16(RxData+4) == L2INFO_CONNECTIONLESS_MTU) {
                if (LEtoHost16(RxData+6) == 0) /* result == success */
                    cmdLen = 10;
                else cmdLen = 8;
            } 
            else if (LEtoHost16(RxData+4) == L2INFO_EXTENDED_FEATURES) {
                if (LEtoHost16(RxData+6) == 0) /* result == success */
                    cmdLen = 12;
                else cmdLen = 8;
            }
            else cmdLen = len; /* Unknown INFO_TYPE */
            break;

        default:
            /* We do not recognize this command so we cannot verify it */
            return TRUE;

        }

        /* Finally, make sure the PDU size is what it should be. */
        if (cmdLen != len)
            return FALSE;

        RxDataLen -= len;
        RxData += len;
    }

    return TRUE;
}


/*
 * L2Cap_IssueHciRequestToSend()
 */
void L2Cap_IssueHciRequestToSend(BtRemoteDevice *Link)
{
    if (!IsListEmpty(&Link->txQueue) && (Link->state == BDS_CONNECTED) && 
        Link->okToSend && (Link->mode != BLM_HOLD_MODE)) {
        /* Request to send */
        AssertEval(HCI_RequestToSend(Link->hciHandle) == BT_STATUS_SUCCESS);
        return;
    }
    Report(("L2Cap_IssueHciRequestToSend() HCI_Request to send called!\n"));
}


#if L2CAP_FLOW_CONTROL == XA_ENABLED
/*
 * L2Cap_ComputeCrc()
 *
 *     Notice that one of L2Cap_InitCrc() or L2Cap_StartCrc() must be
 *     used to generate the starting CRC parameter to this function.
 */
U16 L2Cap_ComputeCrc(const U8 *Buffer, U16 Length, U16 Crc)
{
    U16   tmp;

    while (Length--) {
        /* Compute checksum of lower four bits */
        tmp = L2CrcTab[Crc & 0xF];
        Crc = (Crc >> 4) & 0x0FFF;
        Crc = Crc ^ tmp ^ L2CrcTab[*Buffer & 0xF];

        /* Now compute checksum of upper four bits */
        tmp = L2CrcTab[Crc & 0xF];
        Crc = (Crc >> 4) & 0x0FFF;
        Crc = Crc ^ tmp ^ L2CrcTab[(*Buffer >> 4) & 0xF];

        Buffer++;
    }
 
    return Crc;
}


#if 0
/*
 * L2Cap_InitCrc()
 */
U16 L2Cap_InitCrc(U16 Init)
{
    U16 i;
    U16 crc = 0;

    /* Reflects the lower 15 bits of initial CRC value */
    for (i = 0; i <= 15; i++) {
        if (Init & (1 << i))
            crc |= 1 << (15 - i);
    }

    return crc;
}
#endif

/*
 * L2Cap_StartCrc()
 */
static U16 L2Cap_StartCrc(U16 RemoteCid, U16 Length)
{
    U8      hdr[4];

    hdr[0] = (U8)(Length);
    hdr[1] = (U8)(Length >> 8);
    hdr[2] = (U8)(RemoteCid);
    hdr[3] = (U8)(RemoteCid >> 8);

    return L2Cap_ComputeCrc(hdr, 4, L2Cap_InitCrc(0));
}
#endif /* L2CAP_FLOW_CONTROL == XA_ENABLED */


/*
 * RTX & ERTX Timer functions
 */

/*
 * Called by some timer management function when the timer expires.
 */
void L2CAP_TimerFire(EvmTimer *Timer)
{
    L2CAP_Channel *channel;
    I8             expired;

    channel = ContainingRecord(Timer, L2CAP_Channel, timer);

    if (channel->flags & L2CHF_RTX_ACTIVE) {
        expired = LLC_RTX_EXPIRED;
        XASTAT_CounterInc(l2cap_rtx_timeout, 1);
    }
    else if (channel->flags & L2CHF_ERTX_ACTIVE) {
        expired = LLC_ERTX_EXPIRED;
        XASTAT_CounterInc(l2cap_ertx_timeout, 1);
    }
#if L2CAP_FLOW_CONTROL == XA_ENABLED
    else if (channel->flags2 & L2CHF2_RETRANS_ACTIVE) {
        expired = LLC_RETRANS_EXPIRED;
        XASTAT_CounterInc(l2cap_retrans_timeout, 1);
    }
    else if (channel->flags2 & L2CHF2_MONITOR_ACTIVE) {
        expired = LLC_MONITOR_EXPIRED;
        XASTAT_CounterInc(l2cap_monitor_timeout, 1);
    }
#endif /* L2CAP_FLOW_CONTROL == XA_ENABLED */
    else return;   /* Timer must have been cancelled (really late) */

    /* Check for active link */
    if (ME_GetCurrentMode(channel->link) != BLM_ACTIVE_MODE) {
        /* The link is in hold, park or sniff. Defer the timeout for 1 second. */
        EVM_StartTimer(Timer, 1000);
        return;
    }

    /* Clear timer flags */
    channel->flags &= ~(L2CHF_RTX_ACTIVE|L2CHF_ERTX_ACTIVE);
#if L2CAP_FLOW_CONTROL == XA_ENABLED
    channel->flags2 &= ~(L2CHF2_RETRANS_ACTIVE|L2CHF2_MONITOR_ACTIVE);
#endif
    Report(("L2CAP: Timer Expired(%04x)\n", channel->localCid));

    L2Cap_StateMachine(channel, expired);
}

/*
 * L2Cap_StartRtx
 */
void L2Cap_StartRtx(L2CAP_Channel *Channel)
{
    Assert((Channel->flags & (L2CHF_RTX_ACTIVE|L2CHF_ERTX_ACTIVE)) == 0);

/*  Report(("Starting RTX(%04x)\n", Channel->localCid)); */

    Channel->timer.func = L2CAP_TimerFire;
    Channel->flags |= L2CHF_RTX_ACTIVE;

    /* Timeout value is in milliseconds */
    EVM_StartTimer(&Channel->timer, (TimeT) (L2CAP_RTX_TIMEOUT * 1000));
}


/*
 * L2Cap_StartErtx
 */
void L2Cap_StartErtx(L2CAP_Channel *Channel)
{
    Assert((Channel->flags & (L2CHF_RTX_ACTIVE|L2CHF_ERTX_ACTIVE)) == 0);

    Channel->timer.func = L2CAP_TimerFire;
    Channel->flags |= L2CHF_ERTX_ACTIVE;

    /* Timeout value is in milliseconds. Special case for the W4_CONN_RSP 
     * timer because we're tracking a timeout on the the remote device.
     */
    if (Channel->state == L2C_W4_CONN_RSP)
        EVM_StartTimer(&Channel->timer, (TimeT) (300 * 1000));  /* Maximum E-RTX */
    else EVM_StartTimer(&Channel->timer, (TimeT) (L2CAP_ERTX_TIMEOUT * 1000));
}


#if L2CAP_FLOW_CONTROL == XA_ENABLED
/*
 * L2Cap_StartRetransmitTimer()
 */
void L2Cap_StartRetransmitTimer(L2CAP_Channel *Channel)
{
    Assert((Channel->flags & (L2CHF_RTX_ACTIVE|L2CHF_ERTX_ACTIVE)) == 0);
    Assert(Channel->state == L2C_OPEN);

    if (Channel->flags2 & L2CHF2_MONITOR_ACTIVE) {
        /* Stop the monitor timer before starting the retransmit timer. */
        L2Cap_StopTimer(Channel);
    }

    if ((Channel->flags2 & L2CHF2_RETRANS_ACTIVE) == 0) {
        Report(("L2Cap_StartRetransmitTimer(%04x) transmitCount = %d\n", Channel->localCid, Channel->transmitCount));

        Channel->timer.func = L2CAP_TimerFire;
        Channel->flags2 |= L2CHF2_RETRANS_ACTIVE;
        
        /* Timeout value is in milliseconds */
        EVM_StartTimer(&Channel->timer, max(Channel->retransTimeout, MINIMUM_MONITOR_TIMEOUT));
    }
}


/*
 * L2Cap_StartMonitorTimer()
 */
void L2Cap_StartMonitorTimer(L2CAP_Channel *Channel)
{
    Assert((Channel->flags & (L2CHF_RTX_ACTIVE|L2CHF_ERTX_ACTIVE)) == 0);
    Assert(Channel->state == L2C_OPEN);

    if ((Channel->flags2 & L2CHF2_MONITOR_ACTIVE) == 0) {
        Report(("L2Cap_StartMonitorTimer(%04x)\n", Channel->localCid));

        Channel->timer.func = L2CAP_TimerFire;
        Channel->flags2 |= L2CHF2_MONITOR_ACTIVE;

        /* Timeout value is in milliseconds */
        EVM_StartTimer(&Channel->timer, max(Channel->monitorTimeout, MINIMUM_RETRANS_TIMEOUT));
    }
}
#endif /* L2CAP_FLOW_CONTROL == XA_ENABLED */


/*
 * L2Cap_StopTimer
 * 
 * Stops whatever timer is running.
 */
void L2Cap_StopTimer(L2CAP_Channel *Channel)
{
    Report(("L2Cap_StopTimer(%04x)\n", Channel->localCid));

#if L2CAP_FLOW_CONTROL == XA_ENABLED
    Assert((Channel->flags & (L2CHF_RTX_ACTIVE|L2CHF_ERTX_ACTIVE)) ||
           (Channel->flags2 & (L2CHF2_RETRANS_ACTIVE|L2CHF2_MONITOR_ACTIVE)));
#else
    Assert((Channel->flags & (L2CHF_RTX_ACTIVE|L2CHF_ERTX_ACTIVE)));
#endif

    Channel->flags &= ~(L2CHF_RTX_ACTIVE|L2CHF_ERTX_ACTIVE);
#if L2CAP_FLOW_CONTROL == XA_ENABLED
    Channel->flags2 &= ~(L2CHF2_RETRANS_ACTIVE|L2CHF2_MONITOR_ACTIVE);
#endif

    EVM_CancelTimer(&Channel->timer);
}

