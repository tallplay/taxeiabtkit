/***************************************************************************
 *
 * File:
 *     $Workfile:l2cap.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:80$
 *
 * Description:
 *     This file contains the functions that comprise the
 *     L2CAP upper layer API. This includes group management,
 *     PSM registration and channel control functions.
 *
 * Created:
 *     July 15, 1999
 *
 * Copyright 1999-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "btalloc.h"
#include "sys/l2cap_i.h"
#include "sys/mei.h"

/****************************************************************************
 *
 * Internal Function prototypes
 *
 ****************************************************************************/
#if L2CAP_FLOW_CONTROL == XA_ENABLED
static BtStatus L2Cap_SendFeaturesReq(L2CAP_Channel *Channel, BtPacket *Request);
#endif

/****************************************************************************
 *
 * L2CAP Upper Layer API
 *
 ****************************************************************************/

/*---------------------------------------------------------------------------
 *            L2CAP_RegisterPsm()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Used to register a protocol/service multiplexor with the L2CAP
 *            protocol. The callback function registered here receives all
 *            event notifications from L2CAP pertaining to the registered
 *            protocol/service PSM value.
 *
 * Return:    BT_STATUS_SUCCESS  - The protocol/service was successfully
 *                                 registered with L2CAP.
 *            BT_STATUS_FAILED   - The 'Psm' parameter was invalid.
 *
 *            BT_STATUS_NO_RESOUR- The max number of PSM's has been reached.
 *
 *            BT_STATUS_IN_USE   - The Protocol struct is already registered.
 *                                 
 */
BtStatus L2CAP_RegisterPsm(L2capPsm *Protocol)
{
    BtStatus    status;
    U16         p;

    OS_LockStack();   

#if XA_ERROR_CHECK == XA_ENABLED
    if (!Protocol || !((Protocol->psm == BT_CLIENT_ONLY_PSM) || (Protocol->psm & 0x0001))) {
        status = BT_STATUS_FAILED;
        goto Exit;
    }
    if (L2Cap_IsValidProtocol(Protocol)) {
        status = BT_STATUS_IN_USE;
        goto Exit;
    }
#if L2CAP_FLOW_CONTROL == XA_ENABLED
    if (((Protocol->inLinkMode & (L2MODE_RETRANSMISSION|L2MODE_FLOW_CONTROL|L2MODE_BASIC)) == 0) ||
        ((Protocol->outLinkMode & (L2MODE_RETRANSMISSION|L2MODE_FLOW_CONTROL|L2MODE_BASIC)) == 0)) {
        status = BT_STATUS_INVALID_PARM;
        goto Exit;
    }
    if ((Protocol->inTransmitMax < 1) && (Protocol->inLinkMode != L2MODE_BASIC)) {
        status = BT_STATUS_INVALID_PARM;
        goto Exit;
    }
    if (Protocol->inLinkMode != L2MODE_BASIC) {
        if ((Protocol->inBufferSize < Protocol->localMtu) || 
            (Protocol->localMtu > L2CAP_MAXIMUM_MTU_V12)) {
            status = BT_STATUS_INVALID_PARM;
            goto Exit;
        }
    } 
    else 
#endif /* L2CAP_FLOW_CONTROL == XA_ENABLED */
    if (Protocol->localMtu > L2CAP_MTU) {
        status = BT_STATUS_FAILED;
        goto Exit;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(Protocol &&
           ((Protocol->psm == BT_CLIENT_ONLY_PSM) || (Protocol->psm & 0x0001)));
    Assert(!L2Cap_IsValidProtocol(Protocol));

#if L2CAP_FLOW_CONTROL == XA_ENABLED
    Assert(Protocol->inLinkMode & (L2MODE_RETRANSMISSION|L2MODE_FLOW_CONTROL|L2MODE_BASIC));
    Assert(Protocol->outLinkMode & (L2MODE_RETRANSMISSION|L2MODE_FLOW_CONTROL|L2MODE_BASIC));
    Assert((Protocol->inTransmitMax > 0) || (Protocol->inLinkMode == L2MODE_BASIC));
    Assert((Protocol->inLinkMode != L2MODE_BASIC) || (Protocol->localMtu <= L2CAP_MTU));
    Assert((Protocol->inLinkMode == L2MODE_BASIC) || (Protocol->inBufferSize >= Protocol->localMtu));
#else
    Assert(Protocol->localMtu <= L2CAP_MTU);
#endif

    /* Find free slot for PSM */
    for (p = 0; p < L2CAP_NUM_PROTOCOLS; p++) {
        if (L2C(protocols)[p] == 0) {
            L2C(protocols)[p] = Protocol;

            /* Assign a dynamic PSM if necessary */
            if (Protocol->psm == BT_DYNAMIC_PSM)
                Protocol->psm = (((p & 0x3F80) << 2) | ((p & 0x007F) << 1) | 0x1001);

            Report(("L2CAP: PSM Registered %04x.\n", Protocol->psm));
            XASTAT_PeakInc(l2cap_num_protocols, 1);
            status = BT_STATUS_SUCCESS;
            goto Exit;
        }
    }
    status = BT_STATUS_NO_RESOURCES;

Exit:
    OS_UnlockStack();
    return status;
}

#if L2CAP_DEREGISTER_FUNC == XA_ENABLED
/*---------------------------------------------------------------------------
 *            L2CAP_DeregisterPsm()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Used to deregister a protocol/service multiplexor registered
 *            with the L2CAP protocol. The 'Protocol' parameter is the same
 *            as the one passed to the register function.
 *
 * Return:    BT_STATUS_SUCCESS  - The protocol/service was successfully
 *                                 deregistered from L2CAP.
 *            BT_STATUS_FAILED   - The 'Protocol' parameter is invalid.
 *
 *            BT_STATUS_NOT_FOUND - The 'Protocol' is not registered with L2CAP.
 */
BtStatus L2CAP_DeregisterPsm(const L2capPsm *Protocol)
{
    I16     p, c;

#if XA_ERROR_CHECK == XA_ENABLED
    if (Protocol == 0) {
        return BT_STATUS_FAILED;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(Protocol);

    XASTAT_Set(l2cap_deregister_func, TRUE);

    OS_LockStack();   
    /* Find the protocol in the PSM table */
    for (p = 0; p < L2CAP_NUM_PROTOCOLS; p++) {
        if (L2C(protocols)[p] == Protocol) {

            /* See if any it has any active channels */
            for (c = 0; c < L2CAP_NUM_CHANNELS; c++) {
                if ((L2C(channels)[c].state != L2C_CLOSED) &&
                    (L2C(channels)[c].psmInfo == Protocol)) {
                    /* Cannot deregister a PSM with active channels */
                    OS_UnlockStack();   
                    return BT_STATUS_BUSY;
                }
            }

            L2C(protocols)[p] = 0;
            XASTAT_PeakDec(l2cap_num_protocols, 1);
            OS_UnlockStack();
            return BT_STATUS_SUCCESS;
        }
    }
    OS_UnlockStack();   
    return BT_STATUS_NOT_FOUND;
}
#endif /* L2CAP_DEREGISTER_FUNC == XA_ENABLED */


/*---------------------------------------------------------------------------
 *            L2CAP_ConnectReq()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Send an L2CAP Connect request to the specified device and PSM.
 *
 * Return:    BT_STATUS_FAILED - The request was invalid.
 *
 *            BT_STATUS_NO_CONNECTION - No ACL connection exists.
 *
 *            BT_STATUS_PENDING - The request has started successfully.
 */
BtStatus L2CAP_ConnectReq(const L2capPsm *Protocol, U16 Psm, BtRemoteDevice *Device,
                          L2capConnSettings *Settings, U16 *LocalCid)
{
    L2CAP_Channel    *new_channel;
    BtStatus          status;

#if L2CAP_FLOW_CONTROL != XA_ENABLED
    UNUSED_PARAMETER(Settings);
#endif /* L2CAP_FLOW_CONTROL != XA_ENABLED */

    OS_LockStack();

#if XA_ERROR_CHECK == XA_ENABLED
    if (!L2Cap_IsValidProtocol(Protocol)) {
        OS_UnlockStack();   
        return BT_STATUS_INVALID_PARM;
    }
#if L2CAP_FLOW_CONTROL == XA_ENABLED
    if (((Protocol->inLinkMode != L2MODE_BASIC) && (!Settings || !Settings->reasmBuffer)) ||
        ((Protocol->inLinkMode == L2MODE_BASIC) && (Settings && Settings->reasmBuffer))) {
        OS_UnlockStack();   
        return BT_STATUS_FAILED;
    }
#endif
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(L2Cap_IsValidProtocol(Protocol));
#if L2CAP_FLOW_CONTROL == XA_ENABLED
    Assert(((Protocol->inLinkMode == L2MODE_BASIC) && (!Settings || !Settings->reasmBuffer)) ||
           ((Protocol->inLinkMode != L2MODE_BASIC) && (Settings && Settings->reasmBuffer)));
#endif

    if ((new_channel = L2Cap_AllocLinkChannel(Protocol, Device, &status)) != 0) {
        /* Finish setup of channel and post transmit request */
        new_channel->state = L2C_W4_CONN_CNF;
        new_channel->remotePsm = Psm;

#if L2CAP_FLOW_CONTROL == XA_ENABLED
        if (Settings)
            new_channel->sduBuffer = Settings->reasmBuffer;
#endif

#if L2CAP_FLEXIBLE_CONFIG == XA_ENABLED
        if (Settings && Settings->autoConfigOff)
            new_channel->flags &= ~L2CHF_AUTO_CONFIG;
#endif

#if BT_SECURITY == XA_ENABLED
        /* Query the Security Manager to see if this protocol is
         * authorized to establish an outgoing connection.
         */
        new_channel->authorization.remDev = Device;
        new_channel->authorization.channel = Protocol->psm;
        new_channel->authorization.incoming = FALSE;
        new_channel->authorization.id = SEC_L2CAP_ID;

        status = SEC_AccessRequest(&new_channel->authorization);
        if (status == BT_STATUS_PENDING) {
            /* Return pending for now. We will continue when the security 
             * authorization is completed. 
             */
            new_channel->flags |= L2CHF_SEC_PENDING;
        }
        else if (status != BT_STATUS_SUCCESS) {
            status = BT_STATUS_RESTRICTED;
        }
#endif /* BT_SECURITY */

        if (status == BT_STATUS_SUCCESS) {
            /* Security okay, send the packet */
            status = L2Cap_SendConnReqInternal(new_channel);
        }

        if (status == BT_STATUS_PENDING)
            *LocalCid = new_channel->localCid;
        else L2Cap_FreeChannel(new_channel);
    }

    OS_UnlockStack();   
    return status;
}

/*---------------------------------------------------------------------------
 *            L2CAP_ConnectRsp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Send a Response to a L2CAP Connect Request.
 *
 * Return:    BT_STATUS_FAILED - The request was invalid.
 *
 *            BT_STATUS_NO_CONNECTION - No ACL connection exists.
 *
 *            BT_STATUS_PENDING - The request has started successfully.
 */
BtStatus L2CAP_ConnectRsp(U16 ChannelId, U16 ResponseCode, L2capConnSettings *Settings)
{
    L2CAP_Channel   *channel;
    BtStatus         status;

#if L2CAP_FLOW_CONTROL != XA_ENABLED
    UNUSED_PARAMETER(Settings);
#endif /* L2CAP_FLOW_CONTROL != XA_ENABLED */

    OS_LockStack();   
    channel = L2Cap_GetValidChannel(ChannelId);

#if XA_ERROR_CHECK == XA_ENABLED
    if ((channel == 0) || (channel->state != L2C_W4_CONN_RSP) ||
        (ResponseCode > L2CONN_REJECT_NO_RESOURCES)) {
        OS_UnlockStack();
        return BT_STATUS_FAILED;
    }
#if L2CAP_FLOW_CONTROL == XA_ENABLED
    if (((channel->psmInfo->inLinkMode != L2MODE_BASIC) && (!Settings || !Settings->reasmBuffer)) ||
        ((channel->psmInfo->inLinkMode == L2MODE_BASIC) && (Settings && Settings->reasmBuffer))) {
        OS_UnlockStack();   
        return BT_STATUS_FAILED;
    }
#endif
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(channel && (channel->state == L2C_W4_CONN_RSP));
    Assert(ResponseCode <= L2CONN_REJECT_NO_RESOURCES);
#if L2CAP_FLOW_CONTROL == XA_ENABLED
    Assert(((channel->psmInfo->inLinkMode == L2MODE_BASIC) && (!Settings || !Settings->reasmBuffer)) ||
           ((channel->psmInfo->inLinkMode != L2MODE_BASIC) && (Settings && Settings->reasmBuffer)));
#endif

    /* Make sure the user doesn't try send the same response twice. */
    if ((channel->flags & L2CHF_CONN_RSP_SENT) &&
        ((channel->result != L2CONN_PENDING) || (ResponseCode == L2CONN_PENDING))) {
        OS_UnlockStack();   
        return BT_STATUS_IN_PROGRESS;
    }
    channel->result = ResponseCode;
    channel->flags |= L2CHF_CONN_RSP_SENT;

#if L2CAP_FLOW_CONTROL == XA_ENABLED
    if (Settings)
        channel->sduBuffer = Settings->reasmBuffer;
#endif
#if L2CAP_FLEXIBLE_CONFIG == XA_ENABLED
    if (Settings && Settings->autoConfigOff)
        channel->flags &= ~L2CHF_AUTO_CONFIG;
#endif

    status = L2Cap_SendConnRspInternal(channel);

    Report(("L2CAP: ConnectRsp(%04x) Sent Packet.\n", ChannelId));
    OS_UnlockStack();   
    return status;
}


/*---------------------------------------------------------------------------
 *            L2CAP_DisconnectReq()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Disconect a L2CAP channel.
 *
 * Return:    BT_STATUS_FAILED - The request was invalid.
 *
 *            BT_STATUS_NO_CONNECTION - No ACL connection exists.
 *
 *            BT_STATUS_PENDING - The request has started successfully.
 */
BtStatus L2CAP_DisconnectReq(U16 ChannelId)
{
    L2CAP_Channel   *channel;
    BtStatus         status;

    OS_LockStack();
    channel = L2Cap_GetValidChannel(ChannelId);

#if XA_ERROR_CHECK == XA_ENABLED
    if ((channel == 0) || (channel->state < L2C_W4_CONN_CNF) ||
        ((channel->state == L2C_W4_CONN_RSP) && ((channel->flags & L2CHF_CONN_RSP_SENT) == 0)) || 
        (channel->remoteCid == L2CID_INVALID)) {
        status = BT_STATUS_FAILED;
        goto Done;
    }
    if (channel->state > L2C_OPEN) {
        status = BT_STATUS_IN_PROGRESS;
        goto Done;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(channel && channel->state >= L2C_W4_CONN_CNF && channel->state <= L2C_OPEN);
    Assert(channel->state != L2C_W4_CONN_RSP || (channel->flags & L2CHF_CONN_RSP_SENT));
    Assert(channel->remoteCid != L2CID_INVALID);

    /* Set the disconnect callback reason */
    L2C(callbackParms).status = L2DISC_USER_REQUEST;

    status = L2Cap_SendDiscReqInternal(channel);

#if XA_ERROR_CHECK == XA_ENABLED
Done:
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    OS_UnlockStack();
    return status;
}


/*---------------------------------------------------------------------------
 *            L2CAP_SendData()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Send Data over an L2CAP channel. Support both connection 
 *            oriented and group channels.
 *
 * Return:    BT_STATUS_FAILED - The request was invalid.
 *
 *            BT_STATUS_NO_CONNECTION - No ACL connection exists.
 *
 *            BT_STATUS_PENDING - The request has started successfully.
 */
BtStatus L2CAP_SendData(L2capChannelId ChannelId, BtPacket *Packet)
{
    L2CAP_Channel   *channel;
    BtStatus         status;
#if L2CAP_NUM_GROUPS > 0
    L2capGroup     *group;
#endif

    OS_LockStack();

#if XA_ERROR_CHECK == XA_ENABLED
#if L2CAP_NUM_GROUPS > 0
    if (!L2Cap_GetValidChannel(ChannelId) && !L2Cap_GetValidGroup(ChannelId)) {
#else
    if (!L2Cap_GetValidChannel(ChannelId)) {
#endif /* L2CAP_NUM_GROUPS > 0 */
        OS_UnlockStack();
        return BT_STATUS_FAILED;
    }

    /* Make sure no private flags are set */
    if (Packet->flags & ~(BTP_FLAG_TAIL|BTP_FLAG_INUSE)) {
        OS_UnlockStack();
        return BT_STATUS_FAILED;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

#if L2CAP_NUM_GROUPS > 0
    Assert(L2Cap_GetValidChannel(ChannelId) || L2Cap_GetValidGroup(ChannelId));
#else
    Assert(L2Cap_GetValidChannel(ChannelId));
#endif /* L2CAP_NUM_GROUPS > 0 */

    Assert((Packet->flags & ~(BTP_FLAG_TAIL|BTP_FLAG_INUSE)) == 0);

    /* Is this a Group channel? */
#if L2CAP_NUM_GROUPS > 0
    if (ChannelId >= BASE_GROUP_CID) {
        group = L2C(groups)[GROUP_CID_TO_INDEX(ChannelId)];

        /* Could check what the signalling MTU is using an InfoReq */
        Report(("L2CAP: SendData(%04x) Sent %d byte Group Data Packet.\n", ChannelId, Packet->dataLen));
        status = L2Cap_GroupSend(group, Packet);
    } else
#endif
    {       
        /* It's a connection oriented channel */

        channel = &L2C(channels)[CID_TO_INDEX(ChannelId)];

        if (channel->state != L2C_OPEN) {
            OS_UnlockStack();
            return BT_STATUS_FAILED;
        }

        XASTAT_TimerStart(l2cap_tx_timer, Packet->l2cap_timer);

        Report(("L2CAP: SendData(%04x) Sent %d byte Data Packet.\n", ChannelId, Packet->dataLen));
        status = L2Cap_Send(channel, Packet, channel->remoteCid);
    }

    OS_UnlockStack();
    return status;
}


#if L2CAP_FLEXIBLE_CONFIG == XA_ENABLED
/*---------------------------------------------------------------------------
 *            L2CAP_SendConfigReq()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Sends a Configuration request packet.
 *
 * Return:    BT_STATUS_FAILED - The request was invalid.
 *
 *            BT_STATUS_NO_CONNECTION - No ACL connection exists.
 *
 *            BT_STATUS_PENDING - The request has started successfully.
 */
BtStatus L2CAP_SendConfigReq(L2capChannelId ChannelId, BtPacket *Req,
                             L2capConfigFlags Flags)
{
    L2CAP_Channel   *channel;
    BtStatus         status;

    OS_LockStack();
    channel = L2Cap_GetValidChannel(ChannelId);

#if XA_ERROR_CHECK == XA_ENABLED
    if ((channel == 0) || (Req == 0) ||
        ((channel->flags & L2CHF_AUTO_CONFIG)) ||
        ((channel->state != L2C_OPEN) && (channel->state != L2C_CONFIG))) {
        OS_UnlockStack();
        return BT_STATUS_FAILED;
    }

    if ((channel->state == L2C_CONFIG) && 
        (channel->flags & (L2CHF_OUT_CONFIG_OK|L2CHF_CONFIG_REQ_SENT))) {
        OS_UnlockStack();
        return BT_STATUS_IN_PROGRESS;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(Req && Req->headerLen == 0 && !(Req->flags & BTP_FLAG_TAIL));
    Assert(channel && 
           (channel->state == L2C_OPEN) || (channel->state == L2C_CONFIG));

    XASTAT_Set(l2cap_flexible_config, TRUE);

    /* If were open, then were restarting the whole configuration. */
    if (channel->state == L2C_OPEN) {
        channel->state = L2C_CONFIG;
        channel->flags &= ~(L2CHF_IN_CONFIG_OK|L2CHF_OUT_CONFIG_OK);
    }

    status = L2Cap_SendConfigReqInternal(channel, Req, Flags);

    OS_UnlockStack();
    return status;
}


/*---------------------------------------------------------------------------
 *            L2CAP_SendConfigRsp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Sends a Configuration response packet.
 *
 * Return:    BT_STATUS_FAILED - The request was invalid.
 *
 *            BT_STATUS_NO_CONNECTION - No ACL connection exists.
 *
 *            BT_STATUS_IN_PROGRESS - A config response has already
 *                 been sent.
 *
 *            BT_STATUS_PENDING - The request has started successfully.
 */
BtStatus L2CAP_SendConfigRsp(L2capChannelId ChannelId, BtPacket *Resp, 
                             U16 Result, L2capConfigFlags Flags)
{
    L2CAP_Channel   *channel;
    BtStatus         status;

    OS_LockStack();
    channel = L2Cap_GetValidChannel(ChannelId);

#if XA_ERROR_CHECK == XA_ENABLED
    if ((channel == 0) || 
        (channel->flags & L2CHF_AUTO_CONFIG) ||
        (channel->state != L2C_CONFIG)) {
        OS_UnlockStack();
        return BT_STATUS_FAILED;
    }

    if (channel->flags & L2CHF_IN_CONFIG_OK) {
        OS_UnlockStack();
        return BT_STATUS_IN_PROGRESS;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(Resp->headerLen == 0 && !(Resp->flags & BTP_FLAG_TAIL));
    Assert(channel && (channel->state == L2C_CONFIG));

    XASTAT_Set(l2cap_flexible_config, TRUE);

    status = L2Cap_SendConfigRspInternal(channel, Resp, Result, Flags);
    OS_UnlockStack();
    return status;
}


/*---------------------------------------------------------------------------
 *            L2CAP_SetAutoConfigMode()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Enables/Disables Auto-Configuration mode.
 *
 * Return:    void;
 */
void L2CAP_SetAutoConfigMode(L2capChannelId ChannelId, BOOL On)
{
    L2CAP_Channel   *channel;

    OS_LockStack();

    if (0 != (channel = L2Cap_GetValidChannel(ChannelId))) {
        if (On)
            channel->flags |= L2CHF_AUTO_CONFIG;
        else channel->flags &= ~L2CHF_AUTO_CONFIG;
    }
    OS_UnlockStack();
}
#endif /* L2CAP_FLEXIBLE_CONFIG == XA_ENABLED */


#if L2CAP_FLOW_CONTROL == XA_ENABLED
/*---------------------------------------------------------------------------
 *            L2CAP_GetConnectionMode()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Returns the selected link mode for the connected channel.
 *
 */
L2capLinkMode L2CAP_GetConnectionMode(L2capChannelId ChannelId, BOOL In)
{
    L2capLinkMode   mode;

    OS_LockStack();

#if XA_ERROR_CHECK == XA_ENABLED
    if ((!L2Cap_GetValidChannel(ChannelId)) ||
        (L2C(channels)[CID_TO_INDEX(ChannelId)].state != L2C_OPEN)) {
        OS_UnlockStack();
        return 0;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(L2Cap_GetValidChannel(ChannelId) && 
           (L2C(channels)[CID_TO_INDEX(ChannelId)].state == L2C_OPEN));

    if (In)
        mode = (1 << L2C(channels)[CID_TO_INDEX(ChannelId)].inLinkMode);
    else mode = (1 << L2C(channels)[CID_TO_INDEX(ChannelId)].outLinkMode);

    OS_UnlockStack();
    return mode;
}

/*---------------------------------------------------------------------------
 *            L2CAP_AcceptSduData()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  
 *
 */
BtStatus L2CAP_AcceptSduData(L2capChannelId Cid, U16 NumBytes)
{
    L2CAP_Channel   *channel;

    OS_LockStack();

    channel = L2Cap_GetValidChannel(Cid);
#if XA_ERROR_CHECK == XA_ENABLED
    if (!channel || (NumBytes > channel->buffered) || (channel->inLinkMode == LINK_MODE_BASIC)) {
        OS_UnlockStack();
        return BT_STATUS_INVALID_PARM;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */
    Assert(channel && !(NumBytes > channel->buffered) && (channel->inLinkMode != LINK_MODE_BASIC));

    Report(("L2CAP: AcceptSduData() Accepted %d of %d bytes.\n", NumBytes, channel->buffered));

    channel->buffered -= NumBytes;

    if (L2Cap_UpdateRxWindow(channel))
        L2Cap_SendSFrame(channel);

    OS_UnlockStack();
    return BT_STATUS_SUCCESS;
}

/*---------------------------------------------------------------------------
 *            L2CAP_SetRetransDisableFlag()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Returns the selected link mode for the connected channel.
 *
 */
BtStatus L2CAP_SetRetransDisableFlag(L2capChannelId ChannelId, BOOL Disable)
{
    U16             flags;
    L2CAP_Channel  *channel = L2Cap_GetValidChannel(ChannelId);

    OS_LockStack();

#if XA_ERROR_CHECK == XA_ENABLED
    if ((channel == 0) || (channel->state != L2C_OPEN) || 
        (channel->inLinkMode != LINK_MODE_RETRANSMISSION)) {
        OS_UnlockStack();
        return BT_STATUS_FAILED;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(channel && (channel->state == L2C_OPEN) &&
           (channel->inLinkMode == LINK_MODE_RETRANSMISSION));

    /* Save current flags */
    flags = channel->flags2;

    if (Disable)
        channel->flags2 |= L2CHF2_NO_RETRANS_IN;
    else channel->flags2 &= ~L2CHF2_NO_RETRANS_IN;

    if ((flags != channel->flags2) && ((flags & L2CHF2_SEND_RR) == 0)){
        /* Send flag change to remote peer. */
        channel->flags2 |= L2CHF2_SEND_RR;
        L2Cap_SendSFrame(channel);
    }

    OS_UnlockStack();
    return BT_STATUS_SUCCESS;
}

#endif /* L2CAP_FLOW_CONTROL == XA_ENABLED */

/*---------------------------------------------------------------------------
 *            L2CAP_GetTxMtu()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Retrieves the channels transmit MTU.
 *
 * Return:    U16 - Tramsmit MTU.
 */
U16 L2CAP_GetTxMtu(U16 ChannelId)
{
    U16     mtu;

    OS_LockStack();

#if XA_ERROR_CHECK == XA_ENABLED
    if ((!L2Cap_GetValidChannel(ChannelId)) ||
        (L2C(channels)[CID_TO_INDEX(ChannelId)].state != L2C_OPEN)) {
        OS_UnlockStack();
        return 0;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(L2Cap_GetValidChannel(ChannelId) && 
           (L2C(channels)[CID_TO_INDEX(ChannelId)].state == L2C_OPEN));

    mtu = L2C(channels)[CID_TO_INDEX(ChannelId)].outMtu;

    OS_UnlockStack();
    return mtu;
}


#if L2CAP_PING_SUPPORT == XA_ENABLED
/*---------------------------------------------------------------------------
 *            L2CAP_Ping()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Sends an L2CAP Echo request packet to the specified device.
 *
 * Return:    BT_STATUS_FAILED - The request was invalid.
 *
 *            BT_STATUS_NO_CONNECTION - No ACL connection exists.
 *
 *            BT_STATUS_PENDING - The request has started successfully.
 */
BtStatus L2CAP_Ping(const L2capPsm *PingClient, BtRemoteDevice *Device, 
                    const U8 *Data, U16 DataLen)
{
    L2CAP_Channel  *new_channel;
    BtPacket       *echo;
    BtStatus        status;

    OS_LockStack();
    XASTAT_Set(l2cap_ping_support, TRUE);

    new_channel = L2Cap_AllocLinkChannel(0, Device, &status);
    if (new_channel == 0) {
        OS_UnlockStack();
        return status;
    }
    new_channel->psmInfo = PingClient;

    if ((echo = L2Cap_GetSysPkt(TRUE)) == 0) {
        L2Cap_FreeChannel(new_channel);
        OS_UnlockStack();
        return BT_STATUS_NO_RESOURCES;
    }

    new_channel->state = L2C_W4_ECHO_CNF;
    new_channel->localIdent = GetNextIdent();

    echo->data = (U8 *)Data;
    echo->dataLen = DataLen;

    /*
     * Build Echo Request Header in BtPacket Header. This allows
     * us to easily send Echo Data. We know that no one put any
     * headers in this packet so there is plenty of header space.
     */
    echo->header[BT_PACKET_HEADER_LEN-4] = LLC_ECHO_REQ;
    echo->header[BT_PACKET_HEADER_LEN-3] = new_channel->localIdent;
    echo->header[BT_PACKET_HEADER_LEN-2] = (U8)(DataLen);
    echo->header[BT_PACKET_HEADER_LEN-1] = (U8)(DataLen >> 8);
    echo->headerLen = 4;

    Report(("L2CAP: Ping(%04x) Sent Packet, Ident %d.\n", new_channel->localCid, new_channel->localIdent));
    status = L2Cap_Send(new_channel, echo, 0x0001);
    if (status == BT_STATUS_PENDING)
        L2Cap_StartRtx(new_channel);
    else {
        ReturnSysPkt(echo);
        L2Cap_FreeChannel(new_channel);
    }

    OS_UnlockStack();
    return status;
}
#endif /* L2CAP_PING_SUPPORT == XA_ENABLED */

#if L2CAP_GET_INFO_SUPPORT == XA_ENABLED
/*---------------------------------------------------------------------------
 *            L2CAP_GetInfo()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Requests the specified information from the specified device.
 *
 * Return:    BT_STATUS_FAILED - The request was invalid.
 *
 *            BT_STATUS_NO_CONNECTION - No ACL connection exists.
 *
 *            BT_STATUS_PENDING - The request has started successfully.
 */
BtStatus L2CAP_GetInfo(const L2capPsm *InfoClient, BtRemoteDevice *Device, 
                       U16 InfoType)
{
    L2CAP_Channel     *new_channel;
    BtPacket          *info;
    BtStatus           status;

    OS_LockStack();

    new_channel = L2Cap_AllocLinkChannel(0, Device, &status);
    if (new_channel == 0) {
        OS_UnlockStack();
        return status;
    }
    new_channel->psmInfo = InfoClient;

    if ((info = L2Cap_GetSysPkt(TRUE)) == 0) {
        L2Cap_FreeChannel(new_channel);
        OS_UnlockStack();
        return BT_STATUS_NO_RESOURCES;
    }

    new_channel->state = L2C_W4_INFO_CNF;
    new_channel->localIdent = GetNextIdent();

    info->header[BT_PACKET_HEADER_LEN-6] = LLC_INFO_REQ;
    info->header[BT_PACKET_HEADER_LEN-5] = new_channel->localIdent;
    info->header[BT_PACKET_HEADER_LEN-4] = 2;
    info->header[BT_PACKET_HEADER_LEN-3] = 0;
    info->header[BT_PACKET_HEADER_LEN-2] = (U8)(InfoType);
    info->header[BT_PACKET_HEADER_LEN-1] = (U8)(InfoType >> 8);
    info->headerLen = 6;

    Report(("L2CAP: GetInfo(%04x) Sent Packet, Ident %d.\n", new_channel->localCid, new_channel->localIdent));

    status = L2Cap_Send(new_channel, info, 0x0001);
    if (status == BT_STATUS_PENDING)
        L2Cap_StartRtx(new_channel);
    else {
        ReturnSysPkt(info);
        L2Cap_FreeChannel(new_channel);
    }

    OS_UnlockStack();
    return status;
}
#endif /* L2CAP_GET_INFO_SUPPORT == XA_ENABLED */


#if L2CAP_NUM_GROUPS > 0
/* **************************************************************************
 *
 * L2CAP Group Management API
 *
 * **************************************************************************/

/*---------------------------------------------------------------------------
 *            L2CAP_RegisterGroup()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Registers a Group within L2CAP.
 *
 * Return:    BT_STATUS_INVALID_PARM - A parameter was invalid.
 *
 *            BT_STATUS_SUCCESS - The group is registered.
 *
 *            BT_STATUS_NO_RESOURCES - The group table is full.
 */
BtStatus L2CAP_RegisterGroup(L2capGroup *Group, U16 *ChannelId)
{
    I8    i;

#if XA_ERROR_CHECK == XA_ENABLED
    if (Group == 0 || ChannelId == 0 || Group->psm == 0 || Group->callback == 0) {
        return BT_STATUS_INVALID_PARM;
    }
#endif /* XA_ERROR_CHECK */   

    Assert(Group && ChannelId && Group->psm && Group->callback);

    OS_LockStack();
    for (i = 0; i < L2CAP_NUM_GROUPS; i++) {
        if (L2C(groups)[i] == 0) {
            L2C(groups)[i] = Group;

            Group->inUse = 0;
            Group->cid = BASE_GROUP_CID+(U16)i;
            *ChannelId = BASE_GROUP_CID+(U16)i;

            for (i = 0; i < Group->maxMembers; i++)
                OS_MemSet((U8 *)&Group->members[i], 0, sizeof(BD_ADDR));

            XASTAT_PeakInc(l2cap_num_groups, 1);
            OS_UnlockStack();
            return BT_STATUS_SUCCESS;
        }
    }

    *ChannelId = L2CID_INVALID;

    OS_UnlockStack();
    return BT_STATUS_NO_RESOURCES;
}


/*---------------------------------------------------------------------------
 *            L2CAP_GroupDestroy()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Destroys a registered group.
 *
 * Return:    
 *            
 *
 */
BtStatus L2CAP_GroupDestroy(L2capGroup *Group)
{
    BtStatus    status = BT_STATUS_FAILED;
    I16         i;

#if XA_ERROR_CHECK == XA_ENABLED
    if (!Group) {
        return BT_STATUS_FAILED;
    }
#endif /*  XA_ERROR_CHECK == XA_ENABLED */

    OS_LockStack();
    for (i = 0; i < L2CAP_NUM_GROUPS; i++) {
        if ((L2C(groups)[i] == Group) && 
            (GROUP_CID_TO_INDEX(Group->cid) == (U16)i)) {

            if (Group->inUse) {
                status = BT_STATUS_BUSY;
                break;
            }

            L2C(groups)[i] = 0;
            Group->cid = L2CID_INVALID;

            XASTAT_PeakDec(l2cap_num_groups, 1);
            status = BT_STATUS_SUCCESS;
            break;
        }
    }

    OS_UnlockStack();
    return status;
}


/*---------------------------------------------------------------------------
 *            L2CAP_GroupAddMember()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Adds a member to a group.
 *
 * Return:    
 *            
 *
 */
BtStatus L2CAP_GroupAddMember(U16 GroupId, const BD_ADDR *NewMember)
{
    L2capGroup *group;
    I8           i;

    OS_LockStack();
    group = L2Cap_GetValidGroup(GroupId);

#if XA_ERROR_CHECK == XA_ENABLED
    if (!group || !NewMember) {
        OS_UnlockStack();
        return BT_STATUS_FAILED;
    }
#endif /* XA_ERROR_CHECK */   

    Assert(group && NewMember);

    for (i = 0; i < group->maxMembers; i++) {
        if (!IsValidBdAddr(&group->members[i])) {
            OS_MemCopy(group->members[i].addr, NewMember->addr, sizeof(BD_ADDR));
            OS_UnlockStack();
            return BT_STATUS_SUCCESS;
        }
    }
    OS_UnlockStack();
    return BT_STATUS_NO_RESOURCES;
}


/*---------------------------------------------------------------------------
 *            L2CAP_GroupRemoveMember()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Removes a member from a group.
 *
 * Return:    
 *            
 *
 */
BtStatus L2CAP_GroupRemoveMember(U16 GroupId, const BD_ADDR *Member)
{
    L2capGroup *group;
    I8           i;

    OS_LockStack();
    group = L2Cap_GetValidGroup(GroupId);

#if XA_ERROR_CHECK == XA_ENABLED
    if (!group || !Member) {
        OS_UnlockStack();
        return BT_STATUS_FAILED;
    }
#endif /* XA_ERROR_CHECK */   

    Assert(group && Member);

    for (i = 0; i < group->maxMembers; i++) {
        if (AreBdAddrsEqual(&group->members[i], Member)) {
            OS_MemSet(group->members[i].addr, 0, sizeof(BD_ADDR));
            OS_UnlockStack();
            return BT_STATUS_SUCCESS;
        }
    }

    OS_UnlockStack();
    return BT_STATUS_NOT_FOUND;
}


/*---------------------------------------------------------------------------
 *            L2CAP_GroupGetMembers()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Gets member list for a given group. Compacts group before
 *            returning list pointer.
 *
 * Return:    
 *            
 *
 */
BtStatus L2CAP_GroupGetMembers(U16 GroupId, BD_ADDR **List, U8 *Count)
{
    L2capGroup *group;
    I8           i, j;

    OS_LockStack();
    group = L2Cap_GetValidGroup(GroupId);

#if XA_ERROR_CHECK == XA_ENABLED
    if (!group || !List || !Count) {
        OS_UnlockStack();
        return BT_STATUS_FAILED;
    }
#endif /* XA_ERROR_CHECK */   

    Assert(group && List && Count);

    for (i = 0; i < group->maxMembers; i++) {
        if (IsValidBdAddr(&group->members[i])) {
            continue;
        }

        /* Found an empty slot, try to fill it */
        for (j = i+1; j < group->maxMembers; j++) {
            if (!IsValidBdAddr(&group->members[j])) {
                continue;
            }
            /* Copy this member into the open slot */
            OS_MemCopy(group->members[i].addr, group->members[j].addr, sizeof(BD_ADDR));
            OS_MemSet(group->members[j].addr, 0, sizeof(BD_ADDR));
            break;
        }

        /* If this slot is still empty, were done */
        if (!IsValidBdAddr(&group->members[i]))
            break;

    }

    *Count = (U8)i;
    *List = group->members;
    OS_UnlockStack();
    return BT_STATUS_SUCCESS;
}


/*---------------------------------------------------------------------------
 *            L2CAP_DisableCLT()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Disables connectionless traffic.
 *
 * Return:    BT_STATUS_SUCCESS - Broadcasts have been disabled.
 *            
 */
BtStatus L2CAP_DisableCLT(U16 Psm)
{
    UNUSED_PARAMETER(Psm);
    OS_LockStack();
    /* We only support general blocking (ie. Psm = 0) */
    L2C(flags) &= ~L2CCF_BROADCASTS_ENABLED;
    OS_UnlockStack();

    return BT_STATUS_SUCCESS;
}


/*---------------------------------------------------------------------------
 *            L2CAP_EnableCLT()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Enables Connectionless traffic.
 *
 * Return:    BT_STATUS_SUCCESS - Broadcasts have been disabled.
 *            
 */
BtStatus L2CAP_EnableCLT(U16 Psm)
{
    UNUSED_PARAMETER(Psm);
    OS_LockStack();
    /* We only support general unblocking (ie. Psm = 0) */
    L2C(flags) |= L2CCF_BROADCASTS_ENABLED;
    OS_UnlockStack();

    return BT_STATUS_SUCCESS;
}

#endif /* L2CAP_NUM_GROUPS > 0 */

/**************************************************************************
 ** 
 **  End of API functions.
 ** 
 **************************************************************************/
/*
 * L2Cap_Send()
 *
 * Internal Connection oriented channel Send function.
 */
BtStatus L2Cap_Send(L2CAP_Channel *Channel, BtPacket *Packet, U16 RemoteCid)
{
    I16     packetLen;

#if L2CAP_FLOW_CONTROL == XA_ENABLED
    Assert(((Channel->outLinkMode != LINK_MODE_BASIC) && (RemoteCid != 0x0001)) ?
           (BT_PACKET_HEADER_LEN >= (Packet->headerLen + 6)) :
           (BT_PACKET_HEADER_LEN >= (Packet->headerLen + 4)));
#else
    Assert(BT_PACKET_HEADER_LEN >= (Packet->headerLen + 4));
#endif

    /* If the tail is unused, zero the length for convenience. */
    if ((Packet->flags & BTP_FLAG_TAIL) == 0)
        Packet->tailLen = 0;

    packetLen = Packet->dataLen + Packet->headerLen + Packet->tailLen;

    if (RemoteCid > 0x0002 && packetLen > Channel->outMtu) {
        return BT_STATUS_FAILED;
    }

    if (Channel->link->state != BDS_CONNECTED) {
        return BT_STATUS_NO_CONNECTION;
    }

    Packet->llpContext = Channel->localCid;
    Packet->remoteCid = RemoteCid;

#if L2CAP_FLOW_CONTROL == XA_ENABLED
    Packet->segNum = 0;
#endif

    if (RemoteCid == 0x0001) {
#if L2CAP_FLEXIBLE_CONFIG == XA_DISABLED
        Assert(Packet->flags & BTP_FLAG_LSYSTEM);
#endif
        Assert(!(Packet->flags & BTP_FLAG_TAIL));

        /* An L2CAP signal packet, check for workspace overrun */
        Assert((Packet->flags & BTP_FLAG_LSYSTEM ? 
                (Packet->data != ((SignalPacket *)Packet)->workspace) || 
                (Packet->dataLen <= L2SIG_DATA_LEN) : TRUE));
    }
    Channel->inUse++;

    L2Cap_QueueTxPacket(Channel->link, Packet);

    return BT_STATUS_PENDING;
}


#if L2CAP_NUM_GROUPS > 0
/*
 * L2Cap_GroupSend()
 *
 * Internal group send function.
 */
BtStatus L2Cap_GroupSend(L2capGroup *Group, BtPacket *Packet)
{
    I8      offset = BT_PACKET_HEADER_LEN - Packet->headerLen - 2;
    I16     packetLen;

    /* The minimum connectionless MTU that can be supported is 670 bytes.
     * However, we don't know what the maximum is. So no check is made.
     */

    if (HCI_RequestToSend(L2C(broadcastLink).hciHandle) != BT_STATUS_SUCCESS) {
        return BT_STATUS_NO_CONNECTION;
    }

    /* If the tail is unused, zero the length for convenience. */
    if ((Packet->flags & BTP_FLAG_TAIL) == 0)
        Packet->tailLen = 0;

    packetLen = Packet->dataLen + Packet->headerLen + Packet->tailLen;

    Assert(BT_PACKET_HEADER_LEN >= (Packet->headerLen + 6));

    /* Add Packet Length, Connectionless CID and PSM fields */

    /* Add PSM to connectionless datagram. The lower transmit routine
     * will add the remainder of the B-frame header.
     */
    Packet->header[offset+0] = (U8)(Group->psm);
    Packet->header[offset+1] = (U8)(Group->psm >> 8);

    Packet->headerLen += 2;

    Packet->llpContext = Group->cid;
    Packet->remoteCid = 0x0002;

    Group->inUse++;

    MeSuspendTransmitters();

    InsertTailList(&(L2C(broadcastLink).txQueue), &(Packet->node));

    return BT_STATUS_PENDING;
}
#endif /* L2CAP_NUM_GROUPS > 0 */

/*
 * L2Cap_QueueTxPacket
 */
void L2Cap_QueueTxPacket(BtRemoteDevice* remDev, BtPacket *Packet)
{
    Assert(remDev->state != BDS_DISCONNECTED);

    InsertTailList(&(remDev->txQueue), &(Packet->node));

    if (remDev->state == BDS_CONNECTED) {
        if (remDev->mode != BLM_ACTIVE_MODE) {
            /* Alert the ME that we have data to send. */
            MeHandleDataReqWhileNotActive(remDev);
        }

        /* Need both checks to cover MeSuspendTransmitters logic too. */
        if ((remDev->mode != BLM_HOLD_MODE) && (remDev->okToSend)) 
            AssertEval(HCI_RequestToSend(remDev->hciHandle) == BT_STATUS_SUCCESS);
    }
}

/*
 * L2Cap_SendDiscReqInternal()
 */
BtStatus L2Cap_SendDiscReqInternal(L2CAP_Channel *Channel)
{
    BtPacket       *req;
    BtStatus        status;

    /* It may be necessary to stop the timer because we could be sending
     * a disconnect request while another request is outstanding.
     */
#if L2CAP_FLOW_CONTROL == XA_ENABLED
    if ((Channel->flags & (L2CHF_RTX_ACTIVE|L2CHF_ERTX_ACTIVE)) ||
        (Channel->flags2 & (L2CHF2_MONITOR_ACTIVE|L2CHF2_RETRANS_ACTIVE))) {
#else
    if (Channel->flags & (L2CHF_RTX_ACTIVE|L2CHF_ERTX_ACTIVE)) {
#endif
        Report(("L2CAP: Timer Halted by DisconnectReq, state %d\n", Channel->state));
        L2Cap_StopTimer(Channel);
    }

    Channel->state = L2C_W4_DISC_CNF;

    /* We need to advance the localIdent now to avoid the risk of
     * accepting an outstanding response packet after we've changed state.
     */
    Channel->localIdent = GetNextIdent();
    
    /* If we have an outstanding response to send, forget about it! Also
     * clear the SEND_REQUEST flag now (for convenience) in case it is set.
     */
    Channel->flags &= ~(L2CHF_SEND_RESPONSE|L2CHF_SEND_REQUEST);

    /* Make sure we send the latest Nr before we disconnect. */
#if L2CAP_FLOW_CONTROL == XA_ENABLED
    if ((Channel->inLinkMode != LINK_MODE_BASIC) && L2Cap_UpdateRxWindow(Channel))
        L2Cap_SendSFrame(Channel);
#endif

    if ((req = L2Cap_GetSysPkt(FALSE)) == 0) {
        /* The L2C_W4_DISC_CNF state function will send the
         * request once a system packet is available.
         */
        Channel->flags |= L2CHF_SEND_REQUEST;
        L2C(flags) |= L2CCF_NEED_SYSPACKET;
        return BT_STATUS_PENDING;
    }

    req->header[BT_PACKET_HEADER_LEN - 8] = LLC_DISC_REQ;
    req->header[BT_PACKET_HEADER_LEN - 7] = Channel->localIdent;
    req->header[BT_PACKET_HEADER_LEN - 6] = 4;
    req->header[BT_PACKET_HEADER_LEN - 5] = 0;
    req->header[BT_PACKET_HEADER_LEN - 4] = (U8)(Channel->remoteCid);
    req->header[BT_PACKET_HEADER_LEN - 3] = (U8)(Channel->remoteCid >> 8);
    req->header[BT_PACKET_HEADER_LEN - 2] = (U8)(Channel->localCid);
    req->header[BT_PACKET_HEADER_LEN - 1] = (U8)(Channel->localCid >> 8);

    req->headerLen = 8;

    Report(("L2CAP: SendDisconnectReq(%04x) Sent Packet.\n", Channel->localCid));

    status = L2Cap_Send(Channel, req, 0x0001);
    if (status == BT_STATUS_PENDING)
        L2Cap_StartRtx(Channel);
    else ReturnSysPkt(req);

    return status;
}

/*
 * L2Cap_SendDiscRspInternal()
 */
BtStatus L2Cap_SendDiscRspInternal(L2CAP_Channel *Channel)
{
    BtPacket       *rsp;
    BtStatus        status;

    /* If we have an outstanding request to send, forget about it! Also
     * clear the SEND_RESPONSE flag now (for convenience) in case it is set.
     */
    Channel->flags &= ~(L2CHF_SEND_RESPONSE|L2CHF_SEND_REQUEST);

    if ((rsp = L2Cap_GetSysPkt(FALSE)) == 0) {
        Channel->flags |= L2CHF_SEND_RESPONSE;
        L2C(flags) |= L2CCF_NEED_SYSPACKET;
        return BT_STATUS_PENDING;
    }

    rsp->header[BT_PACKET_HEADER_LEN - 8] = LLC_DISC_RSP;
    rsp->header[BT_PACKET_HEADER_LEN - 7] = Channel->remoteIdent;
    rsp->header[BT_PACKET_HEADER_LEN - 6] = 4;
    rsp->header[BT_PACKET_HEADER_LEN - 5] = 0;
    rsp->header[BT_PACKET_HEADER_LEN - 4] = (U8)(Channel->localCid);
    rsp->header[BT_PACKET_HEADER_LEN - 3] = (U8)(Channel->localCid >> 8);
    rsp->header[BT_PACKET_HEADER_LEN - 2] = (U8)(Channel->remoteCid);
    rsp->header[BT_PACKET_HEADER_LEN - 1] = (U8)(Channel->remoteCid >> 8);

    rsp->headerLen = 8;

    Report(("L2CAP: SendDisconnectRsp(%04x) Sent Packet.\n", Channel->localCid));

    status = L2Cap_Send(Channel, rsp, 0x0001);
    return status;
}

/*
 * L2Cap_SendConfigReqInternal()
 */
BtStatus L2Cap_SendConfigReqInternal(L2CAP_Channel *Channel, BtPacket *Req,
                                     L2capConfigFlags Flags)
{
    BtStatus status;

    Channel->flags |= L2CHF_CONFIG_REQ_SENT;

    Channel->localIdent = GetNextIdent();

    /* Build an L2CAP Config Request header in the header area. */
    Req->header[BT_PACKET_HEADER_LEN - 8] = LLC_CONFIG_REQ;
    Req->header[BT_PACKET_HEADER_LEN - 7] = Channel->localIdent;
    Req->header[BT_PACKET_HEADER_LEN - 6] = (U8)(Req->dataLen+4);
    Req->header[BT_PACKET_HEADER_LEN - 5] = (U8)((Req->dataLen+4) >> 8);
    Req->header[BT_PACKET_HEADER_LEN - 4] = (U8)(Channel->remoteCid);
    Req->header[BT_PACKET_HEADER_LEN - 3] = (U8)(Channel->remoteCid >> 8);
    Req->header[BT_PACKET_HEADER_LEN - 2] = (U8)(Flags);
    Req->header[BT_PACKET_HEADER_LEN - 1] = (U8)(Flags >> 8);

    Req->headerLen = 8;

    Report(("L2CAP: SendConfigReq(%04x) Sent Packet.\n", Channel->localCid));

    status = L2Cap_Send(Channel, Req, 0x0001);
    if (status == BT_STATUS_PENDING)
        L2Cap_StartRtx(Channel);

    return status; 
}

/*
 * L2Cap_SendConfigRspInternal()
 */
BtStatus L2Cap_SendConfigRspInternal(L2CAP_Channel *Channel, BtPacket *Rsp,
                                     U16 Result, L2capConfigFlags Flags)
{
    /* We could just test for this since the caller should have set it. */
    if (Channel->flags & L2CHF_MORE_CONFIG) {
        Result |= L2CFG_FLAG_MORE;
    }
    else if (Result == L2CFG_RESULT_SUCCESS) {
        /* Inbound channel configuration is complete */
        Channel->flags |= L2CHF_IN_CONFIG_OK;
    }

#if L2CAP_FLOW_CONTROL == XA_ENABLED
    if ((Rsp->dataLen > 0) && (Result == L2CFG_RESULT_SUCCESS)) {
        /* Scan the Configuration response looking for the accepted link mode. */
        L2Cap_SetFlowControlOptions(Channel, Rsp->data, Rsp->dataLen);
    }
#endif

    /* Build an L2CAP Config Response header in the header area. */
    Rsp->header[BT_PACKET_HEADER_LEN - 10] = LLC_CONFIG_RSP;
    Rsp->header[BT_PACKET_HEADER_LEN - 9] = Channel->remoteIdent;
    Rsp->header[BT_PACKET_HEADER_LEN - 8] = (U8)(Rsp->dataLen+6);
    Rsp->header[BT_PACKET_HEADER_LEN - 7] = (U8)((Rsp->dataLen+6) >> 8);
    Rsp->header[BT_PACKET_HEADER_LEN - 6] = (U8)(Channel->remoteCid);
    Rsp->header[BT_PACKET_HEADER_LEN - 5] = (U8)(Channel->remoteCid >> 8);
    Rsp->header[BT_PACKET_HEADER_LEN - 4] = (U8)(Flags);
    Rsp->header[BT_PACKET_HEADER_LEN - 3] = (U8)(Flags >> 8);
    Rsp->header[BT_PACKET_HEADER_LEN - 2] = (U8)(Result);
    Rsp->header[BT_PACKET_HEADER_LEN - 1] = (U8)(Result >> 8);
    Rsp->headerLen = 10;

    Report(("L2CAP: SendConfigRsp'%s(%04x) Sent Packet.\n", (Result ? "Neg":"Pos"), Channel->localCid));

    return L2Cap_Send(Channel, Rsp, 0x0001);
}

/*
 * L2Cap_SendConnRspInternal()
 */
BtStatus L2Cap_SendConnRspInternal(L2CAP_Channel *Channel)
{
    BtPacket *rsp;
    BtStatus  status;

    if ((rsp = L2Cap_GetSysPkt(FALSE)) == 0) {
        Channel->flags |= L2CHF_SEND_RESPONSE;
        L2C(flags) |= L2CCF_NEED_SYSPACKET;
        return BT_STATUS_PENDING;
    }

#if L2CAP_FLOW_CONTROL == XA_ENABLED
    if ((Channel->result == L2CONN_ACCEPTED) &&
        (Channel->psmInfo->inLinkMode != L2MODE_BASIC)) {
            
        if (Channel->link->linkModes == L2MODE_UNKNOWN) {
            /* Need to request the devices extended features before we can respond. */
            return L2Cap_SendFeaturesReq(Channel, rsp);
        }

        if ((Channel->psmInfo->inLinkMode & Channel->link->linkModes) == 0) {
            /* No shared link modes, drop down to fail the connect. */
            Channel->result = L2CONN_REJECT_NO_RESOURCES;   /* Not the true reason! */
        }
    }
#endif

    /* We need to split the Connect Response command into two pieces
     * because its size exceeds the individual capacity of either
     * the packet header or the signal packet workspace.
     */
    rsp->header[BT_PACKET_HEADER_LEN - 8] = LLC_CONN_RSP;
    rsp->header[BT_PACKET_HEADER_LEN - 7] = Channel->remoteIdent;
    rsp->header[BT_PACKET_HEADER_LEN - 6] = 8;
    rsp->header[BT_PACKET_HEADER_LEN - 5] = 0;
    rsp->header[BT_PACKET_HEADER_LEN - 4] = (U8)(Channel->localCid);
    rsp->header[BT_PACKET_HEADER_LEN - 3] = (U8)(Channel->localCid >> 8);
    rsp->header[BT_PACKET_HEADER_LEN - 2] = (U8)(Channel->remoteCid);
    rsp->header[BT_PACKET_HEADER_LEN - 1] = (U8)(Channel->remoteCid >> 8);
    rsp->headerLen = 8;
    
    rsp->data[0] = (U8)(Channel->result);
    rsp->data[1] = (U8)(Channel->result >> 8);
    rsp->data[2] = (U8)0;   /* Status */
    rsp->data[3] = (U8)0;   /* Status >> 8 */
    rsp->dataLen = 4;
    
    status = L2Cap_Send(Channel, rsp, 0x0001);
    if (status != BT_STATUS_PENDING)
        ReturnSysPkt(rsp);

    return status;
}

/*
 * L2Cap_SendConnReqInternal()
 */
BtStatus L2Cap_SendConnReqInternal(L2CAP_Channel *Channel)
{
    BtPacket   *req;
    BtStatus    status;

    if ((req = L2Cap_GetSysPkt(FALSE)) == 0) {
        Channel->flags |= L2CHF_SEND_REQUEST;
        L2C(flags) |= L2CCF_NEED_SYSPACKET;
        return BT_STATUS_PENDING;
    }

#if L2CAP_FLOW_CONTROL == XA_ENABLED
    if (Channel->psmInfo->inLinkMode != L2MODE_BASIC) {

        if (Channel->link->linkModes == L2MODE_UNKNOWN) {
            /* Need to request the devices extended features before we can connect. */
            return L2Cap_SendFeaturesReq(Channel, req);
        }

        if ((Channel->psmInfo->inLinkMode & Channel->link->linkModes) == 0) {
            /* No shared link modes, fail the connect */
            return BT_STATUS_NOT_SUPPORTED;
        }
    }
#endif

    Channel->localIdent = GetNextIdent();

    /* Build an L2CAP Connect Request header in the header area. */
    req->header[BT_PACKET_HEADER_LEN - 8] = LLC_CONN_REQ;
    req->header[BT_PACKET_HEADER_LEN - 7] = Channel->localIdent;
    req->header[BT_PACKET_HEADER_LEN - 6] = 4;
    req->header[BT_PACKET_HEADER_LEN - 5] = 0;
    req->header[BT_PACKET_HEADER_LEN - 4] = (U8)(Channel->remotePsm);
    req->header[BT_PACKET_HEADER_LEN - 3] = (U8)(Channel->remotePsm >> 8);
    req->header[BT_PACKET_HEADER_LEN - 2] = (U8)(Channel->localCid);
    req->header[BT_PACKET_HEADER_LEN - 1] = (U8)(Channel->localCid >> 8);
    req->headerLen = 8;
    
    Report(("L2CAP: SendConnectReq(%04x) Sent Packet.\n", Channel->localCid));

    status = L2Cap_Send(Channel, req, 0x0001);
    if (status == BT_STATUS_PENDING)
        L2Cap_StartRtx(Channel);
    else ReturnSysPkt(req);

    return status; 
}

    
#if L2CAP_FLOW_CONTROL == XA_ENABLED
/*
 * L2Cap_SendFeaturesReq()
 */
 
static BtStatus L2Cap_SendFeaturesReq(L2CAP_Channel *Channel, BtPacket *Request)
{
    BtStatus    status;

    Channel->localIdent = GetNextIdent();

    Channel->flags2 |= L2CHF2_FEATURES_REQ;

    Request->header[BT_PACKET_HEADER_LEN-6] = LLC_INFO_REQ;
    Request->header[BT_PACKET_HEADER_LEN-5] = Channel->localIdent;
    Request->header[BT_PACKET_HEADER_LEN-4] = 2;
    Request->header[BT_PACKET_HEADER_LEN-3] = 0;
    Request->header[BT_PACKET_HEADER_LEN-2] = (U8)(L2INFO_EXTENDED_FEATURES);
    Request->header[BT_PACKET_HEADER_LEN-1] = 0;
    Request->headerLen = 6;

    Report(("L2CAP: SendFeaturesReq(%04x) Sent Packet.\n", Channel->localCid));

    status = L2Cap_Send(Channel, Request, 0x0001);
    if (status == BT_STATUS_PENDING)
        L2Cap_StartRtx(Channel);
    else ReturnSysPkt(Request);

    return status;
}
#endif /* L2CAP_FLOW_CONTROL == XA_ENABLED */
