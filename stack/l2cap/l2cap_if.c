/***************************************************************************
 *
 * File:
 *     $Workfile:l2cap_if.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:72$
 *
 * Description:
 *     This file contains the lower layer functions L2CAP exports
 *     to the HCI and the control functions used by the Management
 *     entity.
 *
 * Created:
 *     July 15, 1999
 *
 * Copyright 1999-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "btalloc.h"
#include "sys/l2cap_i.h"
#include "sys/mei.h"

/****************************************************************************
 *
 * Internal Function prototypes
 *
 ****************************************************************************/
static void L2Cap_ProcessSignal(U8 *RxData, U16 RxDataLen, BtRemoteDevice *Link);
#if L2CAP_NUM_GROUPS > 0
static void L2Cap_ProcessGroupData(U8 *RxData, U16 RxDataLen, BtRemoteDevice *Link);
#endif
static void L2Cap_ProcessChannelData(U8 *RxData, U16 RxDataLen, L2CAP_Channel *Channel);
static BOOL L2Cap_HandleRxError(BtRemoteDevice *link);
#if L2CAP_FLOW_CONTROL == XA_ENABLED
static void L2Cap_ChannelReceive(L2CAP_Channel *Channel, HciBuffer *BuffDesc);
static void L2Cap_ProcessIFrame(L2CAP_Channel *Channel, HciBuffer *BuffDesc);
static void L2Cap_ProcessSFrame(L2CAP_Channel *Channel);
static BOOL VerifyAndUpdateSar(L2CAP_Channel *Channel, U8 Sar);

#if UPF_TWEAKS == XA_ENABLED
BOOL DropFrame = FALSE;
#endif
#endif


/*---------------------------------------------------------------------------
 *            L2CAP_HciReceive()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Called by HCI when data arrives on an ACL Connection.
 *
 * Return:    void
 *
 */
void L2CAP_HciReceive(const HciBuffer *BuffDesc, HciHandle HciHndl)
{
    U16             len, cid, copy;
    BtRemoteDevice *link;
    HciBuffer       hciBuffer = *BuffDesc;
    L2CAP_Channel  *channel = 0;

#if JETTEST == XA_ENABLED
    if (Tester_HciReceive) {
        (*Tester_HciReceive)(BuffDesc, HciHndl);
        return;
    }
#endif /* JETTEST == XA_ENABLED */

    link = MeMapHciToRemoteDevice((HciHandle)(HciHndl & 0x0FFF));
#if BT_SCO_HCI_DATA == XA_DISABLED
    if (link == 0) {
        /* This is possible if BT_SCO_HCI_DATA support is disabled but
         * the radio still indicates SCO packets to the HCI instead of
         * directing them to the PCM or other audio hardware.
         */
        Report(("L2CAP: Discarding data from unknown HciHandle.\n"));
        return;
    }
#endif
    Assert(link);

    Report(("L2CAP: Received %d bytes.\n", hciBuffer.len));

    if (link->rxState == BRXS_DISCONNECTING)
        return;

    if (HciHndl & FIRST_PACKET) {
        if ((link->rxState == BRXS_COPYING) || (link->rxState == BRXS_FORWARDING)) {
            /* Error! Throw away buffered data and handle possible QoS violation.
             * Since this is the start of a new packet we can accept this
             * packet if the handler doesn't disconnect the link. 
             */
            if (L2Cap_HandleRxError(link))
                return;
        }
        link->rxBufferLen = 0;
        link->rxState = BRXS_COPYING;
    }

    if ((link->rxState != BRXS_COPYING) && (link->rxState != BRXS_FORWARDING)) {
        return;
    }
    Assert(link->rxBuffer);

    /* We only need 4 bytes to proceed, but try to stage 8 bytes for BT1.2 PDU's. */
    if ((link->rxBufferLen < 8) && (link->rxState == BRXS_COPYING)) {
        copy = min((8 - link->rxBufferLen), hciBuffer.len);

        OS_MemCopy(link->rxBuffer + link->rxBufferLen + L2CAP_PRELUDE_SIZE, hciBuffer.buffer, copy);

        hciBuffer.len -= copy;
        hciBuffer.buffer += copy;
        link->rxBufferLen += copy;

        if (link->rxBufferLen < 4) {
            /* Need a minimum of 4 bytes in the receive buffer to proceed. */
            return;
        }
    }

    /* We've received enough data to pull out the PDU length and channel id. */
    len = LEtoHost16((link->rxBuffer+L2CAP_PRELUDE_SIZE));
    cid = LEtoHost16((link->rxBuffer+L2CAP_PRELUDE_SIZE+2));

    /* If this is a channel data packet, make sure the channel is valid. */
    if (cid >= BASE_DYNAMIC_CID && cid <= LAST_DYNAMIC_CID) {

        channel = L2Cap_GetValidChannel(cid);
        if ((channel == 0) || 
            ((channel->state != L2C_OPEN) && (channel->state != L2C_CONFIG))) {
            Report(("L2CAP: ChannelRx(%04x) ** DROPPED Data ** No Channel!\n", cid));
            link->rxState = BRXS_FLUSH;
            return;
        }

#if L2CAP_FLOW_CONTROL == XA_ENABLED
        if (channel->inLinkMode != LINK_MODE_BASIC) {
            /* Need to make sure the control bytes are in the stage buffer. */
            if ((link->rxBufferLen < 8) && (link->rxState != BRXS_FORWARDING)) {
                if ((len + L2CAP_HEADER_MIN) == link->rxBufferLen) {
                    /* Undersized PDU, drop link. */
                    L2Cap_HandleRxError(link);
                }
                return;
            }
            link->rxState = BRXS_FORWARDING;

            /* BT1.2 PDU's are received into a channel specific buffer. */
            if (link->rxBuffer[L2CAP_PRELUDE_SIZE + 4] & 0x01) {
                /* This is a Supervisory Frame. */
                L2Cap_ProcessSFrame(channel);
            } else {
                /* This is an Information Frame */
                L2Cap_ProcessIFrame(channel, &hciBuffer);
            }
            return;
        }

        if (channel->sduBuffer) {
            /* Not running BT1.2 but using channel specific receive buffer. */
            link->rxState = BRXS_FORWARDING;

            L2Cap_ChannelReceive(channel, &hciBuffer);
            return;
        }
#endif /* L2CAP_FLOW_CONTROL == XA_ENABLED */
    }

    Assert(link->rxState == BRXS_COPYING);

    /* BT1.1 PDU's are received into common remote device buffer. */
    if (link->rxBufferLen + hciBuffer.len > L2CAP_MTU+6) {
        /* Error! MTU violation, drop link, indicate violation to channels */
        L2Cap_HandleRxError(link);
        return;
    }

    /* If this is a broadcast packet and broadcasts are disabled, silently flush. */
    if ((cid == 0x0002) && ((L2C(flags) & L2CCF_BROADCASTS_ENABLED) == 0)) {
        link->rxState = BRXS_FLUSH;
        return;
    }

    /* Copy data into receive buffer */
    OS_MemCopy(link->rxBuffer + link->rxBufferLen + L2CAP_PRELUDE_SIZE, 
               hciBuffer.buffer, hciBuffer.len);

    link->rxBufferLen += hciBuffer.len;

    /* If we have received the entire packet, pass it along. */
    if ((len + L2CAP_HEADER_MIN) == link->rxBufferLen) {
        link->rxState = BRXS_COMPLETE;

        XASTAT_Max(l2cap_mtu, len);

        /* Setup common callback parameters */
        L2C(callbackParms).event = L2EVENT_DATA_IND;
        L2C(callbackParms).status = BT_STATUS_SUCCESS;
        L2C(callbackParms).aclLink = link;

        if (channel)
            L2Cap_ProcessChannelData(link->rxBuffer + L2CAP_HEADER_MIN + L2CAP_PRELUDE_SIZE, len, channel);
        else if (cid == 0x0001)
            L2Cap_ProcessSignal(link->rxBuffer + L2CAP_HEADER_MIN + L2CAP_PRELUDE_SIZE, len, link);
#if L2CAP_NUM_GROUPS > 0
        else if (cid == 0x0002)
            L2Cap_ProcessGroupData(link->rxBuffer + L2CAP_HEADER_MIN + L2CAP_PRELUDE_SIZE, len, link);
#endif
    } 
    else if ((len + L2CAP_HEADER_MIN) < link->rxBufferLen) {
        /* Lost some fragments, received packet is larger than header claims. */
        L2Cap_HandleRxError(link);
        return;
    }
}


/*---------------------------------------------------------------------------
 *            L2CAP_HciTxDone()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Called by HCI to indicate that the packet was sent
 *
 * Return:    void
 *            
 *
 */
void L2CAP_HciTxDone(BtPacket *Packet, BtStatus Status, HciHandle HciHndl)
{
    L2CAP_Channel   *channel;
    BtRemoteDevice  *link;
    U16              cid;
    I16              i;
#if L2CAP_NUM_GROUPS > 0
    L2capGroup      *group;
#endif

#if JETTEST == XA_ENABLED
    if (Tester_HciTxDone) {
        (*Tester_HciTxDone)(Packet, Status); 
        return;
    }
#endif /* JETTEST == XA_ENABLED */

    Assert(Packet && Packet->llpContext);

    /* Grab the Channel / Group Id */
    cid = Packet->llpContext;
    link = MeMapHciToRemoteDevice((HciHandle)(HciHndl & 0x0FFF));

    if (cid >= BASE_DYNAMIC_CID && cid <= LAST_DYNAMIC_CID) {
        /* It's a channel packet */
        channel = &L2C(channels)[CID_TO_INDEX(cid)];
        
        Assert((channel->state == L2C_CLOSED) ? 
               (Packet->flags & (BTP_FLAG_RDEV|BTP_FLAG_LSYSTEM)) : 
               (channel->link == link));

        if (((Packet->flags & BTP_FLAG_RDEV) == 0) && (channel->state != L2C_CLOSED))
            channel->inUse--;

        Report(("L2CAP: TxDone(%04x) %s%sPacket 0x%lx, inUse = %d.\n", cid, 
                (Packet->flags & BTP_FLAG_LSYSTEM ? "(L-System) " : ""), 
                (Packet->flags & BTP_FLAG_RDEV ? "(R-Dev) " : ""), Packet, channel->inUse));

        L2C(callbackParms).aclLink = link;
        L2C(callbackParms).owner.psm = channel->psmInfo;

        if (Packet->flags & BTP_FLAG_LSYSTEM) {
            /* It's a Signal Packet, run the state machine. */
            Packet->flags &= ~BTP_FLAG_INUSE;

            ReturnSysPkt(Packet);

            if ((Status == BT_STATUS_SUCCESS) && (channel->state != L2C_CLOSED))
                L2Cap_StateMachine(channel, LLC_TX_DONE);
        } 
#if L2CAP_FLOW_CONTROL == XA_ENABLED
        else if (Packet->flags & BTP_FLAG_RDEV) {
            /* It's a BtRemoteDevice packet. Return it. */
            InsertTailList(&link->txPacketList, &Packet->node);

            /* Tell the HCI that we want to send data. */
            L2Cap_IssueHciRequestToSend(link);
        }
#endif /* L2CAP_FLOW_CONTROL == XA_ENABLED */
        else {
            /* It's an application packet. Return it to them. */
            Assert( channel->state != L2C_CLOSED );

            XASTAT_TimerStop(Packet->dataLen, l2cap_tx_timer, Packet->l2cap_timer);

            /* Return packet to upper layer (application). */
            L2C(callbackParms).ptrs.packet = Packet;
            L2Cap_NotifyPsm(channel, L2EVENT_PACKET_HANDLED, (U16)Status);

#if L2CAP_FLEXIBLE_CONFIG == XA_ENABLED
            if (channel->state == L2C_CONFIG) {
                /* Now run the State machine for this ConfigReq/Rsp Packet. */
                if (Status == BT_STATUS_SUCCESS)
                    L2Cap_StateMachine(channel, LLC_TX_DONE);
            }
#endif
            return;
        }
        goto CheckForResources;
    }

    if (cid == 0x0001) {
        /* System packet used by L2CAP for a non-channel based signal. */
        Report(("L2CAP: TxDone() System Packet 0x%lx\n", Packet));

        Assert(Packet->flags & BTP_FLAG_LSYSTEM);

        Packet->flags &= ~BTP_FLAG_INUSE;

        ReturnSysPkt(Packet);

#if UPF_TWEAKS == XA_ENABLED
        {
        /* The L2CAP Echo Test requires that the echo responder send an echo 
         * request after it responds. Normally the application layer does not
         * know when an echo response was sent. This tweek tells the UPF PSM
         * (0x1231) that a echo response was sent (via event 99).
         */
            I8  p;

            if (Packet->headerLen == 4 && 
                Packet->header[BT_PACKET_HEADER_LEN-2] == 0x01 &&
                Packet->data[0] == LLC_ECHO_RSP && Packet->data[2] == 0) {
                /* Looks like the UPF Echo test response, find the UPF PSM. */
                for (p = 0; p < L2CAP_NUM_PROTOCOLS; p++) {
                    if (L2C(protocols)[p] && L2C(protocols)[p]->psm == 0x1231) {
                        /* Good the psm is here! */
                        L2C(callbackParms).event = 99;
                        L2C(callbackParms).status = BT_STATUS_SUCCESS;

                        L2C(protocols)[p]->callback(L2CID_INVALID, &L2C(callbackParms));
                        break;
                    }
                }
            }
        }
#endif /* UPF_TWEAKS == XA_ENABLED */
        goto CheckForResources;
    }

#if L2CAP_NUM_GROUPS > 0
    Assert (cid >= BASE_GROUP_CID && cid <= LAST_GROUP_CID);
    /* It is a group packet that is done. Return it to the application. */
    Report(("L2CAP: TxDone() Packet 0x%lx for group %04x\n", Packet, cid));

    group = L2C(groups)[GROUP_CID_TO_INDEX(cid)];
    Assert(group);

    group->inUse--;

    L2C(callbackParms).event = L2EVENT_PACKET_HANDLED;
    L2C(callbackParms).status = (U16)Status;
    L2C(callbackParms).ptrs.packet = Packet;
    L2C(callbackParms).owner.group = group;
    L2C(callbackParms).aclLink = 0;

    group->callback(cid, &L2C(callbackParms));

    if (IsListEmpty(&L2C(broadcastLink).txQueue))
        MeRestartTransmitters();

    return;
#endif

    /* Here we check if any Channels are waiting for system packets. */
CheckForResources:

    if (L2C(flags) & L2CCF_NEED_SYSPACKET) {
        L2C(flags) &= ~L2CCF_NEED_SYSPACKET;
    
        for (i = 0; i < L2CAP_NUM_CHANNELS; i++) {
            if ((L2C(channels)[i].state > L2C_CLOSING) &&
                (L2C(channels)[i].flags & (L2CHF_SEND_REQUEST|L2CHF_SEND_RESPONSE))) {

                if (IsListEmpty(&L2C(signalPackets))) {
                    L2C(flags) |= L2CCF_NEED_SYSPACKET;
                    break;
                }
                
                L2Cap_StateMachine(&L2C(channels)[i], LLC_SEND_PACKET);

                if (L2C(channels)[i].flags & (L2CHF_SEND_REQUEST|L2CHF_SEND_RESPONSE))
                    L2C(flags) |= L2CCF_NEED_SYSPACKET;
            }
        }
    }
}

/*---------------------------------------------------------------------------
 *            L2CAP_HciTxReady()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Called by the HCI when transmit resources are available.
 *            L2CAP returns the next packet in the transmit queue.
 *
 * Return:    transmit packet or null if the queue is empty
 *            
 */
BtPacket *L2CAP_HciTxReady(HciHandle HciHndl)
{
    BtRemoteDevice *link;
    BtPacket       *packet;
    BtPacket       *txPacket = 0;
#if L2CAP_FLOW_CONTROL == XA_ENABLED
    L2CAP_Channel  *channel;
#endif
    ListEntry      *queue;
    I16             packetLen;
    I8              offset;

    if (0 != (link = MeMapHciToRemoteDevice((HciHandle)(HciHndl & 0x0FFF)))) {
        queue = &(link->txQueue);
    } 
    else if (HciHndl == PICONET_BC_HANDLE) {
        queue = &(L2C(broadcastLink).txQueue);
    } else {
        /* This should never happen, so if were running debug, Assert.
         * In release code we'll just ignore the request, no harm done.
         */
        Assert(0);
        return 0;
    }

    for (packet = (BtPacket *)GetHeadList(queue);
         packet != (BtPacket *)queue;
         packet = (BtPacket *)GetNextNode(&packet->node)) {

        if ((packet->llpContext >= BASE_DYNAMIC_CID) && 
            (packet->llpContext <= LAST_DYNAMIC_CID)) {

#if L2CAP_FLOW_CONTROL == XA_ENABLED
            /* It's a connection oriented packet */
            channel = &L2C(channels)[CID_TO_INDEX(packet->llpContext)];

            if ((channel->outLinkMode != LINK_MODE_BASIC) && (packet->remoteCid != 0x0001)) {

                if (packet->flags & BTP_FLAG_LSYSTEM) {
                    Assert((packet->dataLen == 0) && (packet->headerLen == 0));
                    /* It's a BT 1.2 S-Frame. If an RR or REJ still needs
                     * to be sent, L2Cap_BuildSFrame() will build it.
                     */
                    txPacket = packet;
                    packet = (BtPacket *)packet->node.Blink;
                    RemoveEntryList(&txPacket->node);

                    if (0 != (txPacket = L2Cap_BuildSFrame(channel, txPacket)))
                        break;

                    continue;
                }

                /* It's an BT1.2 connection oriented data packet. */
                if (L2Cap_IsTxWindowAvailable(channel) && (channel->state == L2C_OPEN) &&
                    ((channel->flags2 & L2CHF2_NO_RETRANS_OUT) == 0)) {
                    /* We can transmit another segment, grab one. */
                    if (!IsListEmpty(&link->txPacketList)) {
                        txPacket = (BtPacket *)RemoveHeadList(&link->txPacketList);

                        if (L2Cap_GetNextSegment(txPacket, packet) == TRUE) {
                            /* TxPacket contains the segment to send. */
                            L2Cap_BuildIFrameHeader(txPacket);
                            txPacket->flags |= BTP_FLAG_RDEV;

                            L2Cap_StartRetransmitTimer(channel);
                            break;
                        }

                        /* No more segments in this packet to send. */
                        InsertTailList(&link->txPacketList, &txPacket->node);
                        txPacket = 0;
                    }
                }
                continue;
            }
#endif /* L2CAP_FLOW_CONTROL == XA_ENABLED */
            /* It's an L2CAP signalling/control packet or BT1.1 channel */
            txPacket = (BtPacket *)packet;
            RemoveEntryList(&packet->node);
            break;
        } else {

#if JETTEST == XA_ENABLED
            if (packet->llpContext == 0xFFFF) {
                /* Back door for testing purposes */
                Report(("L2CAP: Passed (TESTER) Packet %lx to HCI.\n", packet));
                RemoveEntryList(&packet->node);
                return packet;
            }
#endif /* JETTEST == XA_ENABLED */

            Assert(((packet->llpContext >= BASE_GROUP_CID) && 
                    (packet->llpContext <= LAST_GROUP_CID)) ||
                   (packet->llpContext == 0x0001));
            /* It's a connectionless packet or a signalling reject-response */
            txPacket = packet;
            RemoveEntryList(&packet->node);
            break;
        }
    }

    if (txPacket) {

        /* Append the B-Frame header and send the packet. */ 
        offset = BT_PACKET_HEADER_LEN - txPacket->headerLen - 4;

        Assert(BT_PACKET_HEADER_LEN >= (txPacket->headerLen + 4));
        Assert((txPacket->flags & BTP_FLAG_TAIL) == 0 ? txPacket->tailLen == 0 : 1);

        packetLen = txPacket->dataLen + txPacket->headerLen + txPacket->tailLen;
        if (txPacket->flags & BTP_FLAG_FCS)
            packetLen += 2;

        /* Add the Packet Length and Remote CID fields. */
        txPacket->header[offset+0] = (U8)(packetLen);
        txPacket->header[offset+1] = (U8)(packetLen >> 8);
        txPacket->header[offset+2] = (U8)(txPacket->remoteCid);
        txPacket->header[offset+3] = (U8)(txPacket->remoteCid >> 8);

        txPacket->headerLen += 4;

        Report(("L2CAP: Passed %d byte Packet %lx to HCI.\n", packetLen, txPacket));
    }

    /* Pass the packet to the HCI */
    return txPacket;
}

/*---------------------------------------------------------------------------
 *            L2CAP_LinkDisconnect()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Called by Management entity when a link goes down.
 *
 * Return:    void
 *            
 *
 */
void L2CAP_LinkDisconnect(BtRemoteDevice *Link)
{
    BtPacket  *packet;
    I16        i;

#if JETTEST == XA_ENABLED
    if (Tester_LinkDisconnect) {
        (*Tester_LinkDisconnect)(Link);
        return;
    }
#endif /* JETTEST == XA_ENABLED */

    /* First we need to flush the transmit queue for this link */
    while (!IsListEmpty(&(Link->txQueue))) {
        packet = (BtPacket *)RemoveHeadList(&(Link->txQueue));

        L2CAP_HciTxDone(packet, BT_STATUS_NO_CONNECTION, Link->hciHandle); 
    }

    /* Now, Walk list of channels to find which channels are using
     * this link. Then indicate the event to the state machine.
     */
    for (i = 0; i < L2CAP_NUM_CHANNELS; i++) {

        if (L2C(channels)[i].link == Link) {
            L2Cap_StateMachine(&L2C(channels)[i], LLC_DISCONNECT_IND);
        }
    }

    Link->rxBufferLen = 0;
    Link->rxState = BRXS_COMPLETE;
}

/*---------------------------------------------------------------------------
 *            L2Cap_ProcessSignal()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Called when a complete L2CAP signalling packet has been
 *            received.
 *            
 * Return:    void
 *
 */
static void L2Cap_ProcessSignal(U8 *RxData, U16 RxDataLen, BtRemoteDevice *Link)
{
    U16   cmdLen;

    if (L2Cap_ValidateCommand(RxData, RxDataLen) == FALSE) {
        L2Cap_HandleRxError(Link);
        return;
    }

    while (RxDataLen) {
        cmdLen = LEtoHost16(RxData+2);
        /* This condition was checked by ValidateCommand */
        Assert(RxDataLen >= (cmdLen + SIGNAL_HEADER_LEN));

        /* Pass each signalling command up individually */
        if (RxData[0] & 0x01) {
            L2Cap_ProcessRsp(RxData, cmdLen);
        } else {
            L2Cap_ProcessReq(RxData, cmdLen, Link);
        }
            
        RxDataLen -= SIGNAL_HEADER_LEN + cmdLen;
        RxData += SIGNAL_HEADER_LEN + cmdLen;
    }
}


/*---------------------------------------------------------------------------
 *            L2Cap_ProcessChannelData()
 *---------------------------------------------------------------------------
 *
 * Synopsis: A complete L2CAP Data Packet has been received. Indicate it
 *           to the application or layered protocol.
 *
 * Return:   void
 *
 */
static void L2Cap_ProcessChannelData(U8 *RxData, U16 RxDataLen, L2CAP_Channel *Channel)
{
    Report(("L2CAP: ProcessChannelData(%04x) DataLen %d.\n", Channel->localCid, RxDataLen));

    Assert(Channel && ((Channel->state == L2C_OPEN) || (Channel->state == L2C_CONFIG)));

    /* Verify protocols specific MTU */
    if (RxDataLen > Channel->psmInfo->localMtu) {
        L2Cap_HandleRxError(Channel->link);
        return;
    }
    Channel->remoteIdent = 0;

    L2C(callbackParms).ptrs.data = RxData;
    L2C(callbackParms).dataLen = RxDataLen;
    L2C(callbackParms).owner.psm = Channel->psmInfo;

    L2Cap_NotifyPsm(Channel, L2EVENT_DATA_IND, BT_STATUS_SUCCESS);
}

#if L2CAP_NUM_GROUPS > 0
/*---------------------------------------------------------------------------
 *            L2Cap_ProcessGroupData()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Complete L2CAP Connectionless Data Packet has been received,
 *            indicate it to the upper layer group PSM.
 *
 * Return:    void
 *
 */
static void L2Cap_ProcessGroupData(U8 *RxData, U16 RxDataLen, BtRemoteDevice *Link)
{
    L2capGroup *group;
    U16         psm = LEtoHost16(RxData);
    I16         i;

    UNUSED_PARAMETER(Link);

    /* Find corresponding PSM and deliver data */
    for (i = 0; i < L2CAP_NUM_GROUPS; i++) {
        if (L2C(groups)[i] && L2C(groups)[i]->psm == psm) {
            Report(("L2CAP: ProcessGroupData(%04x) DataLen %d\n", L2C(groups)[i]->cid, RxDataLen-2));

            if (0 != (group = L2Cap_GetValidGroup(L2C(groups)[i]->cid))) {
                
                L2C(callbackParms).ptrs.data = (RxData + 2);
                L2C(callbackParms).dataLen = (U16)(RxDataLen - 2);
                L2C(callbackParms).owner.group = group;

                group->callback(L2C(groups)[i]->cid, &L2C(callbackParms));
            }
            break;
        }
    }
}
#endif

#if L2CAP_FLOW_CONTROL == XA_ENABLED
/*---------------------------------------------------------------------------
 *            L2Cap_ChannelRx()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Copies incoming HCI data into channel based receive buffer.
 *            This is generally for BT1.2 channels operating in BT1.1 mode.
 *
 */
static void L2Cap_ChannelReceive(L2CAP_Channel *Channel, HciBuffer *BuffDesc)
{
    U16     len;
    I8      i;

    len = LEtoHost16(Channel->link->rxBuffer + L2CAP_PRELUDE_SIZE);  /* SDU length */

    if (Channel->sduLength + BuffDesc->len > Channel->localMtu) {
        /* Error! MTU violation, drop link, indicate violation to channels */
        L2Cap_HandleRxError(Channel->link);
        return;
    }
    
    if (Channel->link->rxBufferLen > 4) {
        Assert(Channel->sduLength == 0);

        /* Because we staged more then the SDU header length, there are
         * bytes in the stage that need to be moved to the SDU buffer.
         */
        for (i = 0; (Channel->link->rxBufferLen - i) > 4; i++)
            Channel->sduBuffer[Channel->sduLength++] = Channel->link->rxBuffer[4 + i];

        Channel->link->rxBufferLen = 4;
    }

    /* Copy data into receive buffer */
    OS_MemCopy(Channel->sduBuffer + Channel->sduLength, BuffDesc->buffer, BuffDesc->len);

    Channel->sduLength += BuffDesc->len;

    /* If we have received the entire packet, pass it along. */
    if (len == Channel->sduLength) {
        Channel->sduLength = 0;
        Channel->link->rxState = BRXS_COMPLETE;

        L2Cap_ProcessChannelData(Channel->sduBuffer, len, Channel);
    }
    else if (len < Channel->sduLength) {
        /* Lost some fragments, received packet is larger than header claims. */
        L2Cap_HandleRxError(Channel->link);
        return;
    }
}


/*---------------------------------------------------------------------------
 *            L2Cap_ProcessIFrame()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Process the received information frame.
 *
 */
void L2Cap_ProcessIFrame(L2CAP_Channel *Channel, HciBuffer *BuffDesc)
{
    U8   sar, i, tmp;
    U16  segLen, fcs;
    U8  *rxbuff = Channel->link->rxBuffer + L2CAP_PRELUDE_SIZE;

    segLen = LEtoHost16(rxbuff);
    sar = (U8)(rxbuff[5] & 0xC0);

    if (segLen > Channel->localMpu + (sar == 0x40 ? 6 : 4)) {
        L2Cap_ChannelRxError(Channel, L2ERR_INVALID_PDU);
        return;
    }

    if (Channel->segLength == 0) {
        /* Start of a new segment. */
        Assert(Channel->link->rxBufferLen == 8);

        if (sar == 0x40) {
            /* SAR-Start segment must be at least 10 bytes. 'Seglen' excludes 4 byte B-frame header. */
            if (segLen < 6) {
                L2Cap_ChannelRxError(Channel, L2ERR_INVALID_PDU);
                return;
            }
        } else {
            /* Because we staged 8 bytes and the segment header is only 6,
             * there are 2 bytes in the stage that are part of the SDU data.
             */
            if (segLen > 4) {
                Channel->sduBuffer[Channel->segStart + 0] = rxbuff[6];
                Channel->segLength++;
                
                if (segLen > 5) {
                    Channel->sduBuffer[Channel->segStart + 1] = rxbuff[7];
                    Channel->segLength++;
                }
                else rxbuff[6] = rxbuff[7];

                Channel->link->rxBufferLen -= Channel->segLength;
            }
        }
    }
    
    if ((Channel->segLength + BuffDesc->len) > segLen - (sar == 0x40 ? 6 : 4)) {
        /* Write the FCS (last 2 bytes) of the segment to rxBuffer. */
        tmp = (Channel->segLength + BuffDesc->len) - (segLen - (sar == 0x40 ? 6 : 4));
        Assert(tmp <= 2);
        BuffDesc->len -= tmp;
        for (i = 0; i < tmp; i++)
            rxbuff[Channel->link->rxBufferLen++] = BuffDesc->buffer[BuffDesc->len + i];
    }

    /* Check for receive buffer and segment length overrun. */
    if (((Channel->segStart + Channel->segLength + BuffDesc->len) > Channel->localMtu) ||
        ((Channel->segLength + BuffDesc->len) > segLen)) {
        /* Received SDU or segment is larger than header claimed. */
        L2Cap_ChannelRxError(Channel, L2ERR_INVALID_MPU);
        return;
    }

    /* Copy data into channel receive buffer */
    OS_MemCopy(Channel->sduBuffer + Channel->segStart + Channel->segLength, 
               BuffDesc->buffer, BuffDesc->len);

    Channel->segLength += BuffDesc->len;

    if ((segLen + 4) == (Channel->segLength + Channel->link->rxBufferLen)) {
        /* Received entire segment, verify FCS and process control bytes. */
        Channel->link->rxState = BRXS_COMPLETE;
        
        fcs = L2Cap_ComputeCrc(rxbuff, (U16)(Channel->link->rxBufferLen - 2), L2Cap_InitCrc(0));
        fcs = L2Cap_ComputeCrc(Channel->sduBuffer + Channel->segStart, Channel->segLength, fcs);

        if (fcs != LEtoHost16(rxbuff + Channel->link->rxBufferLen - 2)) {
            /* Invalid FCS: Flush the segment. (Send a REJ?) */
            L2Cap_ChannelRxError(Channel, L2ERR_INVALID_FCS);
            return;
        }

        if (L2Cap_ProcessNs(Channel, rxbuff[4]) == FALSE) {
            /* Invalid N(S): No action, just flush the segment. */
            L2Cap_ChannelRxError(Channel, L2ERR_INVALID_NS);
            return;
        }

        if (L2Cap_ProcessNr(Channel, rxbuff[5]) == FALSE) {
            /* Invalid N(R): Close the channel & flush the segment. */
            L2Cap_ChannelRxError(Channel, L2ERR_INVALID_NR);
            return;
        }

        if (((rxbuff[4] >> 1) & 0x3f) != Channel->recvSeq) {
            /* The frame is out of sequence or a duplicate (N(S) != V(R)), just flush the segment. */
            Channel->segLength = 0;
            return;
        }

#if UPF_TWEAKS == XA_ENABLED
        if (DropFrame && (((rxbuff[4] >> 1) & 0x3f) == 1)) {
            /* Drop first frame with N(S) = 1. For UPF testing only. */
            Channel->segLength = 0;
            DropFrame = FALSE;
            return;
        }
#endif

        /* Check for retransmission disable flag changes. */
        L2Cap_ProcessRbit(Channel, rxbuff[4]);

        if (VerifyAndUpdateSar(Channel, sar) == FALSE) {
            /* Invalid SAR order, close the channel & flush the SDU. */
            L2Cap_ChannelRxError(Channel, L2ERR_INVALID_SAR);
            return;
        }

        if ((sar == 0x40) || (sar == 0x00)) {
            /* Start of an SDU so extract the SDU length. For unsegmented
             * SDU's (sar == 0), the SDU length is the segment length, minus
             * the control and fcs fields length. 
             */
            if (sar == 0x40)
                Channel->sduLength = LEtoHost16(rxbuff + 6);
            else Channel->sduLength = segLen - 4;
            
            if (Channel->sduLength > Channel->localMtu) {
                /* SDU exceeds channel MTU */
                L2Cap_ChannelRxError(Channel, L2ERR_INVALID_PDU);
                return;
            }
        } 

        /* The frame is valid and in order, commit it to the SDU. */
        Channel->recvSeq = (U8)((Channel->recvSeq + 1) & 0x3f);
        Channel->segStart += Channel->segLength;
        Channel->segLength = 0;
        Channel->remoteIdent = 0;

        if ((sar == 0x80) || (sar == 0x00)) {
            /* Received the end of SDU. */

            if (Channel->segStart != Channel->sduLength) {
                /* The SDU length doesn't pass the consistency check. This can
                 * be caused by an out-of-sequence error while in Flow control
                 * only mode.
                 */
                L2Cap_ChannelRxError(Channel, L2ERR_INVALID_PDU);
                return;
            }

            Channel->segStart = 0;
            Channel->buffered += Channel->sduLength;
    
            Report(("L2CAP: Indicating %d byte packet to upper layer.\n", Channel->sduLength));

            /* Indicate the SDU to the application. */
            L2C(callbackParms).ptrs.data = Channel->sduBuffer;
            L2C(callbackParms).dataLen = Channel->sduLength;
            L2C(callbackParms).owner.psm = Channel->psmInfo;

            L2Cap_NotifyPsm(Channel, L2EVENT_DATA_IND, BT_STATUS_SUCCESS);

            Channel->sduLength = 0;
        }

        /* Send an RR-Acknowledgement for this segment if necessary. */
        if ((Channel->state == L2C_OPEN) &&
            (L2Cap_UpdateRxWindow(Channel) || (Channel->segStart == 0)))
            L2Cap_SendSFrame(Channel);
    }
    else if ((segLen + 4) < (Channel->segLength + Channel->link->rxBufferLen)) {
        /* Lost some fragments, received packet is larger than header claims. */
        L2Cap_HandleRxError(Channel->link);
        return;
    }
}


/*---------------------------------------------------------------------------
 *            L2Cap_ProcessSFrame()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Process the received supervisory frame.
 *
 */
static void L2Cap_ProcessSFrame(L2CAP_Channel *Channel)
{
    U16     fcs, len;
    U8     *rxbuff = Channel->link->rxBuffer + L2CAP_PRELUDE_SIZE;

    len = LEtoHost16(rxbuff);      /* Segment length */
    fcs = LEtoHost16(rxbuff + 6);

    Assert(Channel->link->rxBufferLen == 8);

    Channel->link->rxBufferLen = 6; /* Strip the FCS from the buffer */
    Channel->link->rxState = BRXS_COMPLETE;

    if (fcs != L2Cap_ComputeCrc(rxbuff, 6, L2Cap_InitCrc(0))) {
        /* Frame error, handle it. */
        L2Cap_ChannelRxError(Channel, L2ERR_INVALID_FCS);
        return;
    }

    if (L2Cap_ProcessNr(Channel, rxbuff[5]) == FALSE) {
        /* Invalid N(R): Close the channel. */
        L2Cap_ChannelRxError(Channel, L2ERR_INVALID_NR);
        return;
    }
    Assert(Channel->segLength == 0);

    Report(("L2CAP: Received %s with N(R) = %d and retransmissions %s.\n", 
            ((rxbuff[4] & 0x0D) == 1 ? "RR" : "Reject"), (rxbuff[5] & 0x3f), 
            ((rxbuff[4] & 0x80) ? "disabled" : "enabled")));

    /* Check for retransmission disable flag changes. */
    L2Cap_ProcessRbit(Channel, rxbuff[4]);

    /* All the RR processing logic is covered by the ProcessNr() and
     * ProcessRbit() functions. So just check if this is a Reject frame.
     */
    if ((rxbuff[4] & 0x0D) == 0x05) {
        /* Reject: Initiate I-frame retransmission. */
        Channel->sendSeq = Channel->ackSeq;

        L2Cap_IssueHciRequestToSend(Channel->link);
    }
}


/*---------------------------------------------------------------------------
 *            L2Cap_UpdateRxWindow()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Update the receive window position based on available receive
 *            buffer space.
 *
 * Return:    TRUE if an RR should be sent.
 *
 */
BOOL L2Cap_UpdateRxWindow(L2CAP_Channel *Channel)
{
    S32 avail, existing, i;

    Report(("L2CAP: UpdateRxWindow() MPU %d, segLength %d, buffered %d.\n", Channel->localMpu, Channel->segLength, Channel->buffered));

    /* Calculate available buffer space in MPU segments. This value can be
     * negative if peer exceeds receive window.
     */
    i = Channel->psmInfo->inBufferSize - Channel->buffered - Channel->segStart;
    avail = i / Channel->localMpu;

    /* Calculate existing window space available to sending peer. This value
     * can be negative if peer exceeds receive window.
     */
    i = (U8)((Channel->recvSeq - Channel->buffSeq) & 0x3f);
    existing = Channel->rxWindow - i;

    Assert (existing <= avail);

    Report(("L2CAP: UpdateRxWindow() existing %d, avail %d.\n", existing, avail));
    if (existing < avail) {
        /* Advance V(B) to open enough space to fill available buffer. */
        Assert ((avail - existing) <= i);
        Channel->buffSeq = (U8)((Channel->buffSeq + (avail - existing)) & 0x3f);

        Channel->flags2 |= L2CHF2_SEND_RR;
        return TRUE;
    }

    if ((existing == 0) && (Channel->segStart > 0)) {
        /* Received undersized segments, move window forward one to allow
         * end segment to be sent.
         */
        Channel->buffSeq = (U8)((Channel->buffSeq + 1) & 0x3f);

        Channel->flags2 |= L2CHF2_SEND_RR;
        return TRUE;
    }

    return FALSE;
}


/*---------------------------------------------------------------------------
 *            VerifyAndUpdateSar()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Checks the received packets SAR state against the expected
 *            SAR state. Updates the state if the SAR is acceptable.
 *
 * Return:    TRUE  - SAR is valid.
 *            FALSE - SAR is invalid.
 */
static BOOL VerifyAndUpdateSar(L2CAP_Channel *Channel, U8 Sar)
{
    switch (Channel->sar) {
    case 0x00:  /* Unsegmented */
    case 0x80:  /* End */
        if ((Sar == 0x00) || (Sar == 0x40)) {   /* Unsegmented or Start */
            Assert(Channel->segStart == 0);
            Channel->sar = Sar;
            return TRUE;
        }
        break;

    case 0x40:  /* Start */
    case 0xC0:  /* Continue */
        if ((Sar == 0xC0) || (Sar == 0x80)) {   /* Continue or End */
            Channel->sar = Sar;
            return TRUE;
        }
        break;
    }
    return FALSE;
}
#endif /* L2CAP_FLOW_CONTROL == XA_ENABLED */

/*---------------------------------------------------------------------------
 *            L2Cap_HandleRxError
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handles a L2CAP packet reassembly error by disconnecting the
 *            faulty link if it is supposed to have an infinite flush.
 *
 * Return:    TRUE  - Link Disconnected
 *            FALSE - Link not disconnected.
 */
static BOOL L2Cap_HandleRxError(BtRemoteDevice *Link)
{
    Report(("L2CAP: HandleRxError(%x) hciHandle %04x\n", Link, Link->hciHandle));

    Link->rxState = BRXS_DISCONNECTING;

    /* Restricted links have not yet been fully implemented so at this time
     * we will assume that all links have infinite flush timeout. Therefore,
     * we need to disconnect the link. Later we will handle the case where
     * non infinite flush timeout.
     */
    MeDisconnectLink(Link);
    return TRUE;
}

#if L2CAP_FLOW_CONTROL == XA_ENABLED
/*---------------------------------------------------------------------------
 *            L2Cap_ChannelRxError()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  
 *            
 *
 * Return:    
 *            
 */
void L2Cap_ChannelRxError(L2CAP_Channel *Channel, U8 Error)
{
#if XA_DEBUG == XA_ENABLED
    const char *ChannelRxErrString[] = { "", "Invalid PDU", "Invalid MPU", "Invalid NR", "Invalid NS", "Invalid FCS", "Invalid SAR", ""};

    Report(("L2CAP: ChannelRxError(%x) type %s.\n", Channel->localCid, ChannelRxErrString[Error]));
#endif

    /* Always flush the segment from the link reassembly buffer. */
    Channel->link->rxState = BRXS_FLUSH;
    Channel->segLength = 0;

    if ((Error == L2ERR_INVALID_NR) ||
        (Error == L2ERR_INVALID_PDU) ||
        (Error == L2ERR_INVALID_SAR)) {
        /* Invalid N(R) causes the channel to close. */
        /* Invalid PDU length consistency check or length exceeds MTU. */
        /* Invalid SAR field causes the channel to close. */
        L2Cap_NotifyUserAndCloseChannel(Channel, L2DISC_PROTOCOL_ERROR);
        return;
    }

    if (Error == L2ERR_INVALID_NS) {
        /* On invalid N(S) just to flush the segment, that's all. */
        return;
    }

    if (Error == L2ERR_INVALID_MPU) {
        /* Invalid MPU, could be caused by duplicate or out-of-sequence
         * MPU's so just flush the segment.
         */
        return;
    }

    if (Error == L2ERR_INVALID_FCS) {
        /* On invalid FCS, just flush the segment. */
        /* Could optionally send a REJ. */
        return;
    }
}
#endif /* L2CAP_FLOW_CONTROL == XA_ENABLED */


#if BT_SECURITY == XA_ENABLED
/*---------------------------------------------------------------------------
 *            L2Cap_SecAccessRsp
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Receives responses to SEC_AccessRequest() calls.
 *
 */
void L2Cap_SecAccessRsp(const BtEvent *Event)
{
    L2CAP_Channel   *channel;

    channel = ContainingRecord(Event->p.secToken, L2CAP_Channel, authorization);
    Assert(Event->p.secToken == &channel->authorization);
    Assert(channel->flags & L2CHF_SEC_PENDING);
    Assert((channel->state == L2C_W4_CONN_RSP) || 
           (channel->state == L2C_W4_CONN_CNF) ||
           (channel->state == L2C_CLOSED));

    channel->flags &= ~L2CHF_SEC_PENDING;

    if (Event->eType == BTEVENT_ACCESS_APPROVED) {
        L2Cap_StateMachine(channel, LLC_ACCESS_RSP);
        return;
    }

    if (Event->eType == BTEVENT_ACCESS_DENIED) {
        L2Cap_StateMachine(channel, LLC_ACCESS_REJ);
        return;
    }

    Assert(0);
}
#endif /* BT_SECURITY */

