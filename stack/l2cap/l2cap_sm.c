/***************************************************************************
 *
 * File:
 *     $Workfile:l2cap_sm.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:92$
 *
 * Description:
 *     This file contains the functions which make up the L2CAP
 *     state machine.
 *
 * Created:
 *     July 15, 1999
 *
 * Copyright 1999-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "btalloc.h"
#include "sys/l2cap_i.h"
#include "sys/debug.h"

/****************************************************************************
 *
 * Internal Function prototypes
 *
 ****************************************************************************/
static void L2CapState_CONFIG(L2CAP_Channel *Channel, I8 Event);
static void L2Cap_HandleConfigReq(L2CAP_Channel *Channel);
static void L2Cap_HandleConfigRsp(L2CAP_Channel *Channel);
static void SendAutoConfigReq(L2CAP_Channel *Channel);
static void SendAutoConfigRsp(L2CAP_Channel *Channel);

/*---------------------------------------------------------------------------
 *            L2Cap_ProcessReq()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Process or Dispatches the received L2CAP Request packet.
 *
 *            RxData points to the signalling command.
 *            RxDataLen is the length of RxData, minus the code, identifier
 *            and length fields.
 *
 * Return:    void
 *
 */
void L2Cap_ProcessReq(U8 *RxData, U16 RxDataLen, BtRemoteDevice *Source)
{
    L2CAP_Channel *channel, *new_channel;
    U16            reason;
    U8             Opcode = RxData[0];
    U8             Ident = RxData[1];
    /* These variables are used for sending error responses if necessary.
     * The shortcut macro WriteSignalHeader() has knowledge of these fields. */
    U8             ws[L2SIG_DATA_LEN+BT_PACKET_HEADER_LEN-4]; /* Response Command Workspace */
    U16            wsLen;               /* Workspace length, set by macro */
    BtPacket      *btp;                 /* Response packet pointer */

    /* Check if this request is a duplicate. Note: if this request has an
     * invalid identifier (=0), don't run the check because we'll likely get
     * a false positive result.
     */
    if (Ident && (0 != (channel = L2Cap_CheckForDupilicateRequest(Ident, Source)))) {
        Report(("L2CAP: Duplicate Request ignored, Cid %04x, Opcode %02x, Ident %02x%s\n", 
                channel->localCid, Opcode, Ident, (channel->inUse ? " inUse!":".")));
        return;
    }

    /* Store pointer to response data and its length */
    L2C(currRxData) = RxData;
    L2C(currRxDataLen) = RxDataLen;

    switch (Opcode) {

    case LLC_CONN_REQ:
        /* PSM +4 and Source CID +6 */
        new_channel = L2Cap_AllocPsmChannel(LEtoHost16(RxData+4), &reason);
        if (new_channel) {
            new_channel->remoteIdent = Ident;
            new_channel->link = Source;
            new_channel->remoteCid = LEtoHost16(RxData+6);
            Source->refCount++;

            L2Cap_StateMachine(new_channel, Opcode);
            return;
        }

        /* No resources or no Psm Send a ConnRspNeg packet */
        WriteSignalHeader(LLC_CONN_RSP, Ident, 8);
        ws[4] = RxData[5];
        ws[5] = RxData[4];
        ws[6] = RxData[6];        /* Source (requestor) CID */
        ws[7] = RxData[7];
        StoreLE16(ws+8, reason);  /* Result */
        break;

    case LLC_CONFIG_REQ:
        /* Dest CID +4, Flags +6 */
        if (0 != (channel = L2Cap_GetValidChannel(LEtoHost16(RxData+4)))) {
            channel->remoteIdent = Ident;

            L2Cap_StateMachine(channel, LLC_CONFIG_REQ);
            return;
        }

        /* Invalid Dest CID Send Command Reject Message */
        WriteSignalHeader(LLC_COMMAND_REJ, Ident, 6);
        ws[4] = 2;           /* Reason: Invalid CID */
        ws[6] = RxData[4];   /* local CID (relative to sender) */
        ws[7] = RxData[5];
        break;               /* remote CID unknown */

    case LLC_DISC_REQ:
        /* Dest CID +4, Source CID +6 */
        if (0 != (channel = L2Cap_GetValidChannel(LEtoHost16(RxData+4)))) {
            if (channel->remoteCid == LEtoHost16(RxData+6)) {
                channel->remoteIdent = Ident;

                L2Cap_StateMachine(channel, LLC_DISC_REQ);
                return;
            }
            /* Invalid Source CID, ignore the request. */
            return;
        }

        /* Invalid Dest CID Send Command Reject Message */
        WriteSignalHeader(LLC_COMMAND_REJ, Ident, 6);
        ws[4] = 2;           /* Reason: Invalid CID */
        ws[6] = RxData[4];   /* local CID (relative to sender) */
        ws[7] = RxData[5];
        ws[8] = RxData[6];   /* remote CID (relative to sender) */
        ws[9] = RxData[7];
        break;

    case LLC_ECHO_REQ:
        /* Send an echo response. We echo back up to L2SIG_DATA_LEN-4
         * bytes of echo data. 
         */
        RxDataLen = min(L2SIG_DATA_LEN-4, RxDataLen);

        WriteSignalHeader(LLC_ECHO_RSP, Ident, RxDataLen);
        OS_MemCopy(ws+4, RxData+4, RxDataLen);
        break;

    case LLC_INFO_REQ:
        /* Send an Info response */
        if (LEtoHost16(RxData+4) == L2INFO_CONNECTIONLESS_MTU) {
            WriteSignalHeader(LLC_INFO_RSP, Ident, 6);
            ws[4] = RxData[4];   /* Info Type (+4 in the request) */
            ws[5] = RxData[5];
            /* Result (6,7): Success */
            ws[8] = (U8)(L2CAP_MTU);   /* Connectionless MTU */
            ws[9] = (U8)(L2CAP_MTU >> 8);
#if L2CAP_FLOW_CONTROL == XA_ENABLED
        }
        else if (LEtoHost16(RxData+4) == L2INFO_EXTENDED_FEATURES) {
            WriteSignalHeader(LLC_INFO_RSP, Ident, 8);
            ws[4] = RxData[4];   /* Info Type (+4 in the request) */
            ws[5] = RxData[5];
            /* Result (6,7): Success */
            /* 32-bit Extended feature mask (8 - 11) */
            StoreLE32(ws+8, (L2EXTENDED_FEATURE_FLOW_CONTROL|L2EXTENDED_FEATURE_RETRANSMISSIONS));
#endif /* L2CAP_FLOW_CONTROL == XA_ENABLED */
        } else {
            WriteSignalHeader(LLC_INFO_RSP, Ident, 4);
            ws[4] = RxData[4];   /* Info Type (+4 in the request) */
            ws[5] = RxData[5];
            ws[6] = 0x01;        /* Result: Not supported */
        }
        break;

    default:
        /* Unknown request, send a Command Reject packet */
        WriteSignalHeader(LLC_COMMAND_REJ, Ident, 2);
        /* Reason: 0 = Command not understood */
        break;
    }

    /* Come here to send packet ONLY! */
    if (0 != (btp = L2Cap_GetSysPkt(TRUE))) { /* Assert */
        Assert(wsLen > 3 && wsLen <= (L2SIG_DATA_LEN + BT_PACKET_HEADER_LEN - 4));

        btp->llpContext = 0x0001;
        btp->flags |= BTP_FLAG_INUSE;
        btp->remoteCid = 0x0001;

        /* Copy last 4 bytes of workspace into data area */
        wsLen -= 4;
        OS_MemCopy(btp->data, ws + wsLen, 4);
        btp->dataLen = 4;

        /* Copy any previous bytes into header area */
        if ((btp->headerLen = (U8)wsLen) > 0)
            OS_MemCopy(btp->header + BT_PACKET_HEADER_LEN - wsLen, ws, wsLen);

        L2Cap_QueueTxPacket(Source, btp);
    } else Assert(0);

    return;
}

/*---------------------------------------------------------------------------
 *            L2Cap_ProcessRsp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  
 *
 * RxData points to data after the signalling command header has been stripped off.
 *
 * Return:    void
 *
 */
void L2Cap_ProcessRsp(U8 *RxData, U16 RxDataLen)
{
    L2CAP_Channel *channel;
    U8             Opcode = RxData[0];
    U8             Ident = RxData[1];
#if L2CAP_FLOW_CONTROL == XA_ENABLED
    U32            features;
#endif

    /*
     * Since we sent the request, we can find the channel by
     * looking for the channel with a matching localIdent.
     */
    channel = L2Cap_FindChannelByLocalIdent(Ident);
    if (!channel) {
        /* Invalid response */
        Report(("L2CAP: ProcessRsp() Received Response to unknown request (Ident %02x)\n", Ident));
        return;
    }
    if (channel->state == L2C_CLOSED) {
        /* Channel is closed (it probably timed out). Ignore responses
         * while waiting for 'inUse' resources to be returned.
         */
        return;
    }
    L2C(callbackParms).owner.psm = channel->psmInfo;

    /* Store pointer to response data and its length */
    L2C(currRxData) = RxData;
    L2C(currRxDataLen) = RxDataLen;

    /* Stop retransmit timer and clear the acknowledged identifier. */
    L2Cap_StopTimer(channel);

    channel->localIdent = 0;

    switch (Opcode) {
    
    case LLC_CONN_RSP:
        /* Dest CID +4, Src CID +6, Result +8, Status +10 */
        /* Only check SourceCID if result is not conn rejected */
        if (LEtoHost16(RxData+8) == L2CONN_PENDING)
            channel->localIdent = Ident;

        if ((LEtoHost16(RxData+8) > L2CONN_PENDING) ||
            (channel->localCid == LEtoHost16(RxData+6))) {

            L2Cap_StateMachine(channel, LLC_CONN_RSP);
            return;
        }
        break;

    case LLC_CONFIG_RSP:
        /* Src CID +4, Flags +6, Result +8 */
        if (channel->localCid == LEtoHost16(RxData+4)) {

            L2Cap_StateMachine(channel, LLC_CONFIG_RSP);
            return;
        }
        break;

    case LLC_DISC_RSP:
        /* Verify that Dst CID (+4) & Src CID's (+6) match */
        if ((channel->localCid == LEtoHost16(RxData+6)) &&
            (channel->remoteCid == LEtoHost16(RxData+4))) {

            L2Cap_StateMachine(channel, LLC_DISC_RSP);
            return;
        }
        break;

#if L2CAP_PING_SUPPORT == XA_ENABLED
    case LLC_ECHO_RSP:
        /* Indicate response to Requester */
        if (channel->state == L2C_W4_ECHO_CNF)
        {
            channel->state = L2C_CLOSED;

            L2C(callbackParms).ptrs.data = RxData+4;
            L2C(callbackParms).dataLen = RxDataLen;
            L2Cap_NotifyPsm(channel, L2EVENT_COMPLETE, BT_STATUS_SUCCESS);  /* L2CAP_PING_CNF */
        
            L2Cap_FreeChannel(channel);
        }
        return;
#endif /*  L2CAP_PING_SUPPORT == XA_ENABLED */

    case LLC_INFO_RSP:
        /* Indicate response to the requester. */
#if L2CAP_FLOW_CONTROL == XA_ENABLED
        if ((channel->state == L2C_W4_CONN_CNF) || (channel->state == L2C_W4_CONN_RSP)) {
            Assert(channel->flags2 & L2CHF2_FEATURES_REQ);

            channel->flags2 &= ~L2CHF2_FEATURES_REQ;

            /* Was the request successful? And is it sufficiently sized? */
            if ((RxData[6] == 0) && (RxData[7] == 0) && (RxDataLen == 8)) {
                /* Store the features in the BtRemoteDevice structure. */
                features = LEtoHost32(RxData + 8);

                channel->link->linkModes = L2MODE_BASIC;
                if (features & L2EXTENDED_FEATURE_FLOW_CONTROL)
                    channel->link->linkModes |= L2MODE_FLOW_CONTROL;
                if (features & L2EXTENDED_FEATURE_RETRANSMISSIONS)
                    channel->link->linkModes |= L2MODE_RETRANSMISSION;
            }
            else if (channel->link->linkModes == L2MODE_UNKNOWN)
                channel->link->linkModes = L2MODE_BASIC;

            L2Cap_StateMachine(channel, LLC_INFO_RSP);
            return;
        }
#endif /* L2CAP_FLOW_CONTROL == XA_ENABLED */

#if L2CAP_GET_INFO_SUPPORT == XA_ENABLED
        Assert (channel->state == L2C_W4_INFO_CNF);

        channel->state = L2C_CLOSED;

        L2C(callbackParms).ptrs.data = RxData+8;     /* Data */
        L2C(callbackParms).dataLen = RxDataLen - 4;  /* subtract Result & InfoType fields */

        L2Cap_NotifyPsm(channel, L2EVENT_COMPLETE, LEtoHost16(RxData+6)); /* L2CAP_INFO_CNF */

        L2Cap_FreeChannel(channel);
#endif /* L2CAP_GET_INFO_SUPPORT == XA_ENABLED */
        return;

    case LLC_COMMAND_REJ:
        /* This is bad.... We sent a request and got this blank stare back.
         * We know what channel sent the request so just feed it a negative
         * response.
         */
        L2Cap_StateMachine(channel, LLC_COMMAND_REJ);
        return;

    default:
        /*
         * Unrecognized response.
         */
        return;
    }

    /* If we get here, the response could not be correlated with a 
     * request we made. Restart Rtx() so we at least timeout the request.
     */
    Report(("L2CAP: ProcessRsp(%04x) Response CID did not match expected CID (Opcode %d).\n", channel->localCid, Opcode));
    L2Cap_StartRtx(channel);
}


/*---------------------------------------------------------------------------
 *            L2CapState_OPEN()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Processes signalling commands that occur while the channel is
 *            in the OPEN state. The only valid commands are ConfigReq and
 *            DiscReq.
 *
 */
static void L2CapState_OPEN(L2CAP_Channel *Channel, I8 Event)
{
    Report(("L2CAP: OPEN(%04x) Event %s\n", Channel->localCid, pLLC_Event(Event)));

    if (Event == LLC_CONFIG_REQ) {
        Channel->state = L2C_CONFIG;
        Channel->flags &= ~(L2CHF_IN_CONFIG_OK|L2CHF_OUT_CONFIG_OK|L2CHF_CONFIG_REQ_SENT);
#if L2CAP_FLOW_CONTROL == XA_ENABLED
        Channel->flags2 &= ~L2CHF2_FOUND_FC_OPTION;
#endif

        /* Tell the application that we're reconfiguring the channel. */
        L2Cap_NotifyPsm(Channel, L2EVENT_CONFIGURING, BT_STATUS_SUCCESS);

#if L2CAP_FLEXIBLE_CONFIG == XA_ENABLED
        if (Channel->flags & L2CHF_AUTO_CONFIG)
#endif
            SendAutoConfigReq(Channel);

        L2CapState_CONFIG(Channel, LLC_CONFIG_REQ);
        return;
    }

#if L2CAP_FLOW_CONTROL == XA_ENABLED
    if (Event == LLC_SEND_PACKET) {
        /* We are waiting for a system packet. Send the Receiver-Ready
         * or Reject packet now.
         */
        Assert((Channel->flags & (L2CHF_SEND_REQUEST|L2CHF_SEND_RESPONSE)) == L2CHF_SEND_REQUEST);

        /* Send an S-Frame: RR or Reject */
        L2Cap_SendSFrame(Channel);
        return;
    }
#endif /* L2CAP_FLOW_CONTROL == XA_ENABLED */

    /* Event == LLC_DISC_REQ: Is handled by the core State Machine code. */
    /* Event == LLC_COMMAND_REJ: Is handled by the core State Machine code. */

    Assert(Event == LLC_TX_DONE);   /* RR or REJECT S-Frame */
}

/*---------------------------------------------------------------------------
 *            L2CapState_W4_DISC_CNF()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  We've sent a disconnect request and are wating for the response.
 *            If both sides disconnect simultaneously, W4_DISC_RSP is the
 *            resulting state.
 *
 * Return:    void
 *
 */
static void L2CapState_W4_DISC_CNF(L2CAP_Channel *Channel, I8 Event)
{
    Report(("L2CAP: W4_DISC_CNF(%04x) Event %s\n", Channel->localCid, pLLC_Event(Event)));

    if (Event == LLC_SEND_PACKET) {
        /* We are waiting for a system packet, send the disconnect now. */
        Assert((Channel->flags & (L2CHF_SEND_REQUEST|L2CHF_SEND_RESPONSE)) == L2CHF_SEND_REQUEST);
        (void)L2Cap_SendDiscReqInternal(Channel);
        return;
    }

    if (Event == LLC_DISC_RSP) {
        if (L2C(callbackParms).status == L2DISC_REASON_UNKNOWN)
            L2Cap_NotifyUserAndCloseChannel(Channel, L2DISC_USER_REQUEST);
        else L2Cap_NotifyUserAndCloseChannel(Channel, L2C(callbackParms).status);
        return;
    }

    /* Event == LLC_COMMAND_REJ: Handled by the core State Machine code. */
    /* Event == LLC_DISC_REQ: Handled by the core State Machine code. */

    /* Since we roll to W4_DISC_CNF state on reception of a negative
     * config response, there is the chance that a config request is
     * waiting for us to process. Just ignore it. 
     */
    Assert(Event == LLC_CONFIG_REQ || Event == LLC_TX_DONE);
}

/*---------------------------------------------------------------------------
 *            L2CapState_W4_DISC_RSP()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  We received a Disconnect request and are waiting for our
 *            response to be transmitted so we can close the channel. If
 *            both sides disconnect simultaneously, this is the resulting state.
 *
 * Return:    void
 *
 */
static void L2CapState_W4_DISC_RSP(L2CAP_Channel *Channel, I8 Event)
{
    Report(("L2CAP: W4_DISC_RSP(%04x) Event %s\n", Channel->localCid, pLLC_Event(Event)));

    if (Event == LLC_TX_DONE) {
        if ((Channel->inUse == 0) &&
            ((Channel->flags & L2CHF_SEND_RESPONSE) == 0)) {
            /* We already indicated a DISCONNECT_IND, this event tells the app
             * that we're completely disconnected now. Notice that we need to
             * wait until all packets are sent. That way we know the disconnect
             * response has been sent.
             */
            L2Cap_NotifyUserAndCloseChannel(Channel, L2DISC_USER_REQUEST);
        }
        return;
    }

    if (Event == LLC_SEND_PACKET) {
        Assert((Channel->flags & (L2CHF_SEND_REQUEST|L2CHF_SEND_RESPONSE)) == L2CHF_SEND_RESPONSE);
        /* We are waiting for a system packet, send the disconnect response now. */
        (void)L2Cap_SendDiscRspInternal(Channel);
        return;
    }

    /* If both sides simultaneously disconnect. We can receive their
     * response while in this state. But were only waiting for our response
     * to be sent before we close the channel.
     */
    Assert(Event == LLC_DISC_RSP);
}


/*---------------------------------------------------------------------------
 *            L2CapState_CONFIG()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Sending and receiving configuration commands.
 *
 * Return:    void
 *
 */
static void L2CapState_CONFIG(L2CAP_Channel *Channel, I8 Event)
{
    U16     flags;

    Report(("L2CAP: CONFIG(%04x) Event %s\n", Channel->localCid, pLLC_Event(Event)));

    switch (Event) {
    case LLC_CONFIG_REQ:
        if ((Channel->flags & (L2CHF_OUT_CONFIG_OK|L2CHF_RTX_ACTIVE)) == 
            (L2CHF_OUT_CONFIG_OK|L2CHF_RTX_ACTIVE)) {
            /* We were timing their config request. Stop the timer now. */
            L2Cap_StopTimer(Channel);
        }
        L2Cap_HandleConfigReq(Channel);
        break;

    case LLC_CONFIG_RSP:
        L2Cap_HandleConfigRsp(Channel);
        /* Fall through to check for transition OPEN state */

    case LLC_TX_DONE:
        flags = Channel->flags & (L2CHF_IN_CONFIG_OK|L2CHF_OUT_CONFIG_OK);

        if ((flags == (L2CHF_IN_CONFIG_OK|L2CHF_OUT_CONFIG_OK)) && (Channel->inUse == 0)) {

            /* In and Out Configuration are done and there are no packets
             * outstanding. Were Connected!
             */
            Report(("L2CAP: CONFIG(%04x) State advanced to OPEN\n", Channel->localCid));
            Channel->state = L2C_OPEN;

#if L2CAP_FLOW_CONTROL == XA_ENABLED
            if ((Channel->inLinkMode != Channel->outLinkMode) &&
                ((Channel->inLinkMode == LINK_MODE_BASIC) || (Channel->outLinkMode == LINK_MODE_BASIC))) {
                /* Invalid channel configuration, disconnect. */
                L2C(callbackParms).status = L2DISC_CONFIG_UNACCEPT;
                AssertEval(L2Cap_SendDiscReqInternal(Channel) == BT_STATUS_PENDING);
                return;
            }

            if (Channel->inLinkMode != LINK_MODE_BASIC) /* Either direction can be tested */
                L2Cap_StartMonitorTimer(Channel);
#endif
            if (Channel->flags & L2CHF_CONNECTED) {
                L2Cap_NotifyPsm(Channel, L2EVENT_RECONFIGURED, BT_STATUS_SUCCESS);
            } else {
                Channel->flags |= L2CHF_CONNECTED;
                L2Cap_NotifyPsm(Channel, L2EVENT_CONNECTED, BT_STATUS_SUCCESS);
            }   
            break;
        }

        if ((flags == L2CHF_OUT_CONFIG_OK) && (Channel->inUse == 0)) {
            /* A little extra protection from unresponsive devices. We set a
             * timer while waiting for their config request.
             */
            L2Cap_StartRtx(Channel);
        }
        break;

    case LLC_SEND_PACKET:
        /* We are waiting for a system packet. Send the Configuration
         * request or response packet now. We give precidence to responses
         * to try to avoid timing out the remote's request.
         */
        if (Channel->flags & L2CHF_SEND_RESPONSE) {
            SendAutoConfigRsp(Channel);
        } else {
            Assert(Channel->flags & L2CHF_SEND_REQUEST);
            SendAutoConfigReq(Channel);
        }
        break;
    
    /* case LLC_DISC_REQ: Is handled by the core State Machine code. */
    /* case LLC_COMMAND_REJ: Is handled by the core State Machine code. */

    default:
        Assert(0);
        break;

    }
    return;   
}


/*---------------------------------------------------------------------------
 *            L2CapState_W4_CONN_CNF()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Waiting for a Connection Response packet from remote device.
 *
 * Return:    void
 *
 */
static void L2CapState_W4_CONN_CNF(L2CAP_Channel *Channel, I8 Event)
{
    BtStatus  status;

    Report(("L2CAP: W4_CONN_CNF(%04x) Event %s\n", Channel->localCid, pLLC_Event(Event)));

    if (Event == LLC_CONN_RSP) {
        switch (LEtoHost16(L2C(currRxData)+8)) { /* Result */
        case L2CONN_ACCEPTED:
            Channel->state = L2C_CONFIG;
            Channel->remoteCid = LEtoHost16(L2C(currRxData)+4);

            /* Initiate configuration process. */
            Assert(!(Channel->flags & (L2CHF_OUT_CONFIG_OK|L2CHF_CONFIG_REQ_SENT|L2CHF_IN_CONFIG_OK)));

#if L2CAP_FLOW_CONTROL == XA_ENABLED
            L2Cap_InitFlowControlSettings(Channel);
#endif

#if L2CAP_FLEXIBLE_CONFIG == XA_ENABLED
            if ((Channel->flags & L2CHF_AUTO_CONFIG) == 0) {
                L2Cap_NotifyPsm(Channel, L2EVENT_CONFIGURING, BT_STATUS_SUCCESS);
                break;
            }
#endif
            SendAutoConfigReq(Channel);
            break;

        case L2CONN_PENDING:
            Channel->remoteCid = LEtoHost16(L2C(currRxData)+4);

            L2Cap_StartErtx(Channel);
            break;

        default:
            /* Connection Rejected, set Result */
            status = LEtoHost16(L2C(currRxData)+8)|0x0040;
            goto IndicateDisc;
        }
        return;
    }

    if ((Event == LLC_SEND_PACKET) || (Event == LLC_ACCESS_RSP) || (Event == LLC_INFO_RSP)) {
        /* It's time to send the connect request. We were waiting for
         * security clearance or for a transmit packet.
         */
        Assert((Event == LLC_SEND_PACKET) ? Channel->flags & L2CHF_SEND_REQUEST : TRUE);

        Channel->flags &= ~L2CHF_SEND_REQUEST;

        status = L2Cap_SendConnReqInternal(Channel);
        if (status != BT_STATUS_PENDING) {
            if (status == BT_STATUS_NOT_SUPPORTED)
                status = L2DISC_CONFIG_UNACCEPT;
            else status = L2DISC_REASON_UNKNOWN;
            goto IndicateDisc;
        }
        return;
    }

    if (Event == LLC_ACCESS_REJ) {
        /* The PSM is NOT allowed to send the connect request. 
         * L2Cap_FreeChannel() will return the system packet.
         */
        status = L2DISC_SECURITY_BLOCK;
IndicateDisc:
        
        L2Cap_NotifyUserAndCloseChannel(Channel, (U16)status); /* aka L2CAP_CONN_REJ */
        return;
    }

    /* case LLC_COMMAND_REJ: Handled by the core State Machine code. */
}


/*---------------------------------------------------------------------------
 *            L2CapState_W4_CONN_RSP()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Received a Connect Request packet from the remote device.
 *            Waiting for the application to send a response and for the send
 *            to complete.
 *
 * Return:    void
 *
 */
static void L2CapState_W4_CONN_RSP(L2CAP_Channel *Channel, I8 Event)
{
    Report(("L2CAP: W4_CONN_RSP(%04x) Event %s\n", Channel->localCid, pLLC_Event(Event)));

    if (Event == LLC_TX_DONE) {

#if L2CAP_FLOW_CONTROL == XA_ENABLED
        if (Channel->flags2 & L2CHF2_FEATURES_REQ) {
            /* The info request has been sent, ignore this event. */
            return;
        }
#endif

        /* The Connect Response has been sent.
         * If we sent a Positive response, advance to Configuration state. 
         */
        if (Channel->result == L2CONN_ACCEPTED) {
            Channel->state = L2C_CONFIG;

#if L2CAP_FLOW_CONTROL == XA_ENABLED
            L2Cap_InitFlowControlSettings(Channel);
#endif

#if L2CAP_FLEXIBLE_CONFIG == XA_ENABLED
            if ((Channel->flags & L2CHF_AUTO_CONFIG) == 0) {
                L2Cap_NotifyPsm(Channel, L2EVENT_CONFIGURING, BT_STATUS_SUCCESS);
                return;
            }
#endif
            SendAutoConfigReq(Channel);
            return;
        }

        /* If we sent a pending response, there is nothing to do here. */
        if (Channel->result == L2CONN_PENDING) {
            return;
        }

        /* We sent a Negative Confirmation, close the channel. */
        L2Cap_NotifyUserAndCloseChannel(Channel, L2DISC_USER_REQUEST);
        return;
    }

    if (Event == LLC_ACCESS_RSP) {
        /* The Security Manager accepted our access request.
         * Deliver a connect indication to the upper layer protocol. 
         */
        Channel->flags &= ~(L2CHF_SEC_PENDING|L2CHF_NOT_VISIBLE);

        L2Cap_StopTimer(Channel);
        L2Cap_NotifyPsm(Channel, L2EVENT_CONNECT_IND, BT_STATUS_SUCCESS);
        return;
    }

    if (Event == LLC_ACCESS_REJ) {
        /* The Security Manager rejected our access request. 
         * Reject the Incoming connection request.
         */
        Channel->flags &= ~L2CHF_SEC_PENDING;
        Channel->result = L2CONN_REJECT_SECURITY_BLOCK;

        L2Cap_StopTimer(Channel);
        AssertEval(L2Cap_SendConnRspInternal(Channel) != BT_STATUS_FAILED);
        return;
    }

    if ((Event == LLC_SEND_PACKET) || (Event == LLC_INFO_RSP)) {
        Assert((Event == LLC_SEND_PACKET ? (Channel->flags & L2CHF_SEND_RESPONSE) : 1));

        Channel->flags &= ~L2CHF_SEND_RESPONSE;

        AssertEval(L2Cap_SendConnRspInternal(Channel) != BT_STATUS_FAILED);
        return;
    }

    Assert(0);
}      


/*---------------------------------------------------------------------------
 *            L2CapState_CLOSED()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Generally waiting for a Connect Request. 
 *
 * Return:    void
 *
 */
static void L2CapState_CLOSED(L2CAP_Channel *Channel, I8 Event)
{
#if BT_SECURITY == XA_ENABLED
    BtStatus    status;
#endif /* BT_SECURITY */

    Report(("L2CAP: CLOSED(%04x) Event %s\n", Channel->localCid, pLLC_Event(Event)));

    if (Event == LLC_CONN_REQ) {
        Channel->state = L2C_W4_CONN_RSP;

#if BT_SECURITY == XA_ENABLED
        /* Query the Security Manager to see if this protocol is
         * authorized to connect to us.
         */
        Channel->authorization.remDev = Channel->link;
        Channel->authorization.channel = Channel->psmInfo->psm;
        Channel->authorization.incoming = TRUE;
        Channel->authorization.id = SEC_L2CAP_ID;

        status = SEC_AccessRequest(&Channel->authorization);
        if (status == BT_STATUS_PENDING) {
            /* Send a Connect Pending response. We start E-RTX to track the
             * remote devices timeout. In case the local application doesn't
             * respond to the authorization request for a very long time.
             */
            Channel->flags |= (L2CHF_NOT_VISIBLE|L2CHF_SEC_PENDING);
            Channel->result = L2CONN_PENDING;

            L2Cap_StartErtx(Channel);

            L2Cap_SendConnRspInternal(Channel);
            return;
        }
        if (status == BT_STATUS_FAILED) {
            /* Send a Connect Reject response */
            Channel->flags |= L2CHF_NOT_VISIBLE;
            Channel->result = L2CONN_REJECT_SECURITY_BLOCK;
            AssertEval(L2Cap_SendConnRspInternal(Channel) != BT_STATUS_FAILED);
            return;
        }
#endif /* BT_SECURITY */

        /* Access Granted: Deliver connection indication to the 
         * upper layer protocol.
         */
        L2Cap_NotifyPsm(Channel, L2EVENT_CONNECT_IND, BT_STATUS_SUCCESS);
        return;
    }

    if (Event == LLC_TX_DONE) {
        /* If we're waiting to close the channel so packets on the channel
         * can complete, and that is done now, close the connection.
         */
        if (Channel->state == L2C_CLOSING) {
            if (Channel->inUse == 0) {
                /* Channel can close safely now. */
                L2Cap_NotifyUserAndCloseChannel(Channel, Channel->result);
            }
            return;
        }
    }

    Assert(0);
}

/*---------------------------------------------------------------------------
 *            L2Cap_StateMachine()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  The main entrypoint into the L2CAP state machine.
 *
 */
void L2Cap_StateMachine(L2CAP_Channel *Channel, I8 Event)
{
    U16     status;

    Assert(Channel->localCid != L2CID_INVALID);

    if (Channel->localCid == L2CID_INVALID)
        return;

    L2C(callbackParms).aclLink = Channel->link;
    L2C(callbackParms).owner.psm = Channel->psmInfo;

    /* The state machine first processes events that have general handlers.
     * Then the events are passed to state specific handlers.
     */
    switch (Event) {
    case LLC_RTX_EXPIRED:
    case LLC_ERTX_EXPIRED:
        if ((Channel->state == L2C_W4_ECHO_CNF) || (Channel->state == L2C_W4_INFO_CNF)) {
            Channel->state = L2C_CLOSED;

            L2Cap_NotifyPsm(Channel, L2EVENT_COMPLETE, BT_STATUS_TIMEOUT);
            L2Cap_FreeChannel(Channel);
            return;
        }

#if L2CAP_FLOW_CONTROL == XA_ENABLED
        if (Channel->flags2 & L2CHF2_FEATURES_REQ) {
            Assert((Channel->state == L2C_W4_CONN_CNF) || (Channel->state == L2C_W4_CONN_RSP));
            /* The Extended Features Information request timed out. Proceed 
             * with the connection establishment.
             */
            Channel->flags2 &= ~L2CHF2_FEATURES_REQ;

            if (Channel->link->linkModes == L2MODE_UNKNOWN)
                Channel->link->linkModes = L2MODE_BASIC;

            L2Cap_StateMachine(Channel, LLC_INFO_RSP);
            return;
        }
#endif

#if BT_SECURITY == XA_ENABLED
        if (Channel->flags & L2CHF_SEC_PENDING) {
            Assert(Channel->state == L2C_W4_CONN_RSP);
            /* Our internal timer on the SEC_AccessRequest() timed out. 
             * We set the timer to the maximum E-RTX to detect when the remote
             * device would time out the connection waiting for us.
             */
            Channel->flags &= ~L2CHF_SEC_PENDING;
            AssertEval(SEC_CancelAccessRequest(&Channel->authorization) == BT_STATUS_SUCCESS);
        }
#endif /* BT_SECURITY == XA_ENABLED */

        status = L2DISC_REQUEST_TIMEDOUT;
        break;

    case LLC_DISCONNECT_IND:
        status = L2DISC_LINK_DISCON;
        break;

    case LLC_COMMAND_REJ:
        status = L2DISC_REASON_UNKNOWN;
        break;

    case LLC_DISC_REQ:
        Report(("L2CAP: State Machine(%04x) Event LLC_DISC_REQ.\n", Channel->localCid));

        Assert(Channel->state > L2C_CLOSING && Channel->state < L2C_W4_DISC_RSP);

#if BT_SECURITY == XA_ENABLED
        if (Channel->flags & L2CHF_SEC_PENDING) {
            /* Cancel out outstanding SEC_AccessRequest() */
            Channel->flags &= ~L2CHF_SEC_PENDING;
            AssertEval(SEC_CancelAccessRequest(&Channel->authorization) == BT_STATUS_SUCCESS);
        }
#endif /* BT_SECURITY == XA_ENABLED */

        if (Channel->state == L2C_W4_DISC_CNF) {
            /* If both sides decide to disconnect simultaneously, this may 
             * happen. Notice that we transition to L2C_W4_DISC_RSP because
             * they may not respond to our disconnect request. And there is
             * no need to wait for a response (or RTX) before closing.
             */
            Channel->state = L2C_W4_DISC_RSP;
        } else {
            if (Channel->state != L2C_W4_DISC_RSP) {
                Channel->state = L2C_W4_DISC_RSP;

                /* Notify the protocol that the channel is disconnecting. */
                L2Cap_NotifyPsm(Channel, L2EVENT_DISCON_IND, BT_STATUS_SUCCESS);
            }
        }

#if L2CAP_FLOW_CONTROL == XA_ENABLED
        if (Channel->flags2 & (L2CHF2_MONITOR_ACTIVE|L2CHF2_RETRANS_ACTIVE)) {
            /* Cancel the Monitor and Retransmit timers. */
            L2Cap_StopTimer(Channel);
        }
#endif

        /* We expect _PENDING or _NO_CONNECTION here */
        (void)L2Cap_SendDiscRspInternal(Channel);

        /* We return here because the L2C_W4_DISC_RSP will handle closing. */
        return;

    case LLC_QOS_VIOL:
        if (Channel->state != L2C_OPEN)
            return;

        /* It is up to the application to decide what to do when a QOS
         * violation occurs. So we return instead of automatically closing
         * the channel.
         */
        L2Cap_NotifyPsm(Channel, L2EVENT_QOS_VIOLATION, BT_STATUS_SUCCESS);
        return;

#if L2CAP_FLOW_CONTROL == XA_ENABLED
    case LLC_RETRANS_EXPIRED:
        Assert((Channel->flags2 & L2CHF2_NO_RETRANS_OUT) == 0);
        Assert(Channel->state == L2C_OPEN);
        Assert(Channel->inUse > 0);

        if (Channel->outLinkMode == LINK_MODE_FLOW_CONTROL) {
            Assert(((Channel->sendSeq - Channel->ackSeq) & 0x3f) > 0);

            /* No retransmissions in this mode so we have to assume that the
             * segment was lost and advance past it. Simulate receiving an RR.
             */
            Channel->ackSeq = (U8)((Channel->ackSeq + 1) & 0x3f);

            /* Return any packets implicitly acknowledged. */
            L2Cap_ReturnSentPackets(Channel, BT_STATUS_TIMEOUT);
            L2Cap_IssueHciRequestToSend(Channel->link);

            if (Channel->state == L2C_OPEN) {
                if (((Channel->sendSeq - Channel->ackSeq) & 0x3f) > 0)
                    L2Cap_StartRetransmitTimer(Channel);
                else L2Cap_StartMonitorTimer(Channel);
            }
            return;
        }

        if (++Channel->transmitCount < Channel->transmitMax) {
            /* Initiate I-frame retransmission. */
            Channel->sendSeq = Channel->ackSeq;
            L2Cap_IssueHciRequestToSend(Channel->link);
            /* We return now, the HCI will continue the retransmit process */
            return;
        }

        /* Link is assumed lost, return packets and indicate disconnect. */
        status = L2DISC_MAX_RETRANS;
        break;

    case LLC_MONITOR_EXPIRED:
        Assert(Channel->state == L2C_OPEN);
        Assert(Channel->inLinkMode != LINK_MODE_BASIC);

        /* Send an RR the remote device and restart the monitor. */
        Channel->flags2 |= L2CHF2_SEND_RR;
        L2Cap_SendSFrame(Channel);
        L2Cap_StartMonitorTimer(Channel);
        return;
#endif /* L2CAP_FLOW_CONTROL == XA_ENABLED */

    default:
        /* 
         * The state specific handlers are dispatched here 
         */
        switch (Channel->state) {
        case L2C_CLOSED:
        case L2C_CLOSING:
            L2CapState_CLOSED(Channel, Event);
            break;

        case L2C_W4_CONN_CNF:   /* Waiting for the peer to sent a response */
            L2CapState_W4_CONN_CNF(Channel, Event);
            break;

        case L2C_W4_CONN_RSP:   /* Waiting for protocol above us to send the response */
            L2CapState_W4_CONN_RSP(Channel, Event);
            break;

        case L2C_CONFIG:
            L2CapState_CONFIG(Channel, Event);
            break;

        case L2C_OPEN:
            L2CapState_OPEN(Channel, Event);
            break;

        case L2C_W4_DISC_CNF:
            L2CapState_W4_DISC_CNF(Channel, Event);
            break;

        case L2C_W4_DISC_RSP:
            L2CapState_W4_DISC_RSP(Channel, Event);
            break;

        case L2C_W4_ECHO_CNF:
        case L2C_W4_INFO_CNF:
            /* All events are handled elsewhere. This is general failure case. */
            if (Event != LLC_TX_DONE)
                L2Cap_FreeChannel(Channel);
            break;

        default:
            Assert(0);
            break;
        }
        return;
    }
    /*
     * All the Timeout and most LLC events caught in the first switch
     * statement come here for final actions.
     */
    L2Cap_NotifyUserAndCloseChannel(Channel, status);
    return;
}


/*---------------------------------------------------------------------------
 *            L2Cap_HandleConfigReq()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Runs the Config Request received substate of the CONFIG state.
 *            Handles parsing a Configuration Request packet for the layered
 *            protocol. Called by the state machine's CONFIG state function.
 *
 */
static void L2Cap_HandleConfigReq(L2CAP_Channel *Channel)
{
    L2capConfigFlags    flags;
    U8                 *response;
    U8                 *options;
    I8                  optionLen;
    U16                 result = L2CFG_RESULT_SUCCESS;
    U16                 remote, local, outMtu;

#if L2CAP_FLOW_CONTROL == XA_ENABLED
    if ((Channel->flags & L2CHF_MORE_CONFIG) == 0) {
        /* This request is not a continuation of the current request.
         * Initialize the inbound request options.
         */
        Channel->flags2 &= ~L2CHF2_FOUND_FC_OPTION;
    }
#endif /* L2CAP_FLOW_CONTROL == XA_ENABLED */

    Channel->flags &= ~(L2CHF_IN_CONFIG_OK | L2CHF_MORE_CONFIG);
    Channel->flags |= L2CHF_CONFIG_IND_RCVD;
    Channel->wsLen = 0;
    
    flags = LEtoHost16(L2C(currRxData)+6);
    if (flags & L2CFG_FLAG_MORE)
        Channel->flags |= L2CHF_MORE_CONFIG;

    if (0 != (outMtu = L2Cap_GetMtu(L2C(currRxData)+8, (U16)(L2C(currRxDataLen)-4))))
        Channel->outMtu = outMtu;

#if L2CAP_FLOW_CONTROL == XA_ENABLED
    result = L2Cap_SetFlowControlOptions(Channel, L2C(currRxData)+8, (U16)(L2C(currRxDataLen)-4));
    /* The results are checked after the flexible config stuff. */
#endif

#if L2CAP_FLEXIBLE_CONFIG == XA_ENABLED
    if ((Channel->flags & L2CHF_AUTO_CONFIG) == 0) {
        L2C(callbackParms).ptrs.configInd.ident = L2C(currRxData)[1];
        L2C(callbackParms).ptrs.configInd.flags = flags;
        L2C(callbackParms).ptrs.configInd.options = L2C(currRxData)+8;
        L2C(callbackParms).ptrs.configInd.optionsLen = L2C(currRxDataLen)-4;

        L2Cap_NotifyPsm(Channel, L2EVENT_CONFIG_IND, BT_STATUS_SUCCESS);
        return;
    }
#endif

    /* Build Configure response packet in channel workspace. */
    options = L2C(currRxData)+8;
    optionLen = L2C(currRxDataLen)-4;
    response = Channel->workspace;

    /* Now check the results from the L2Cap_SetFlowControlOptions() call. */
#if L2CAP_FLOW_CONTROL == XA_ENABLED
    if (result != L2CFG_RESULT_SUCCESS) {
        Assert(Channel->wsLen >= 11);
    } 
    else /* Verify the MTU meets our minimum requirements. */
#endif
    if (Channel->outMtu < Channel->psmInfo->minRemoteMtu) {
        result = L2CFG_RESULT_UNACCEPTABLE;
        WriteConfigOption16(response, L2CFG_OPTION_MTU, Channel->psmInfo->minRemoteMtu);
        Channel->wsLen += 4;
    }

    /* Parse until we encounter a failure or run out of options. */
    while (optionLen && (result == L2CFG_RESULT_SUCCESS)) {
        switch ((*options & ~CONFIG_OPTION_HINT)) {
        
        case L2CFG_OPTION_MTU:
            /* We already parsed and verified the outbound MTU in the
             * beginning of this function. Nothing to do here.
             */
            break;

#if L2CAP_FLOW_CONTROL == XA_ENABLED
        case L2CFG_OPTION_FLOW_CONTROL:
            /* We already parsed and verified the inbound Flow Control and 
             * Retransmission option in the beginning of this function. 
             * Nothing to do here.
             */
            break;
#endif /* L2CAP_FLOW_CONTROL == XA_ENABLED */

        case L2CFG_OPTION_FLUSH_TIMEOUT:
            Assert(!Channel->link->restricted);

            local = L2CAP_DEFAULT_FLUSH_TIMEOUT;
            remote = LEtoHost16(options+2);
            if (remote < local) {
                result = L2CFG_RESULT_UNACCEPTABLE;
                WriteConfigOption16(response, L2CFG_OPTION_FLUSH_TIMEOUT, local);
                Channel->wsLen += 4;
            }
            break;

        case L2CFG_OPTION_QOS:
            /* We ignore their QOS because WE DON'T CARE about flow rates.
             * However, we don't have to fail their request if they simply
             * sent us the default service type BEST_EFFORT, or NO_TRAFFIC.
             * We only have to reject it if they requested GUARANTEED.
             */
            if (options[3] < L2QOS_OPTION_GUARANTEED)
                break;

            /* Drop through to indicate unsupported option */

        default:
            /* Unknown option type. If the option is not a hint we have to
             * assume failure.
             */
            if ((*options & CONFIG_OPTION_HINT) == 0) {
                /* The option is not a hint. Since we can't process it,
                 * it must be failed. 
                 */
                result = L2CFG_RESULT_UNKN_OPTIONS;
                response[0] = *options;
                response[1] = 0;
                Channel->wsLen += 2;
            }
            break;
        }

        optionLen -= options[1] + 2;
        options += options[1] + 2;
    }
    Channel->result = result;

    Assert(Channel->wsLen <= L2C_WORKSPACE_LEN);

    SendAutoConfigRsp(Channel);
}


/*---------------------------------------------------------------------------
 *            L2Cap_HandleConfigRsp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Runs the Config Response received substate of the CONFIG state.
 *            We don't negotiate our Outbound configuration. So if the response
 *            from the remote side was not success, we disconnect the channel.
 *
 */
static void L2Cap_HandleConfigRsp(L2CAP_Channel *Channel)
{
    U16         flags, result;
    BtStatus    status;

    if ((Channel->flags & L2CHF_CONFIG_REQ_SENT) == 0) {
        Report(("L2CAP: Received unsolicited Configuration Response.\n"));
        /* Set the completion status. */
        L2C(callbackParms).status = L2DISC_REASON_UNKNOWN;
        goto SendDisconnect;
    }

    Assert(!(Channel->flags & L2CHF_OUT_CONFIG_OK));

    flags = LEtoHost16(L2C(currRxData)+6);
    result = LEtoHost16(L2C(currRxData)+8);

    if (((flags & L2CFG_FLAG_MORE) == 0) && (result == L2CFG_RESULT_SUCCESS)) {
        /* Complete, successful response. */
        Channel->flags |= L2CHF_OUT_CONFIG_OK;
    }
    else Channel->flags &= ~L2CHF_CONFIG_REQ_SENT;

#if L2CAP_FLEXIBLE_CONFIG == XA_ENABLED
    if ((Channel->flags & L2CHF_AUTO_CONFIG) == 0) {
        L2C(callbackParms).ptrs.configCnf.ident = L2C(currRxData)[1];
        L2C(callbackParms).ptrs.configCnf.flags = flags;
        L2C(callbackParms).ptrs.configCnf.result = result;
        L2C(callbackParms).ptrs.configCnf.options = L2C(currRxData)+10;
        L2C(callbackParms).ptrs.configCnf.optionsLen = L2C(currRxDataLen)-6;

        L2Cap_NotifyPsm(Channel, L2EVENT_CONFIG_CNF, BT_STATUS_SUCCESS);
        return;
    }
#endif

    if (flags & L2CFG_FLAG_MORE) {
        /*
         * When we receive a Configuration Response with the MORE flag,
         * we're supposed to send back an empty Configuration Request
         * to solicit the next response;
         */
        Report(("L2CAP: Sending Lone ConfigReq to solicit more responses.\n"));
        goto SendConfigReq;
    }

    if (result != L2CFG_RESULT_SUCCESS) {

#if L2CAP_FLOW_CONTROL == XA_ENABLED
        /* The only configuration option we negotiate is Flow Control. */
        if ((L2C(currRxDataLen) > (6 + 2)) && (L2C(currRxData)[10] == L2CFG_OPTION_FLOW_CONTROL)) {
            Report(("L2CAP: Received ConfigRspNeg for Flow Control Option.\n"));

            /* Convert protocol enumeration to a flag for comparison with API link modes. */
            if (Channel->psmInfo->inLinkMode & (1 << (L2C(currRxData)[10 + 2]))) {
                /* We support the mode they suggested so use it. */
                Channel->inLinkMode = L2C(currRxData)[10 + 2];
                goto SendConfigReq;
            }

            /* We don't support their suggested mode so try one we haven't tried yet. */
            switch (Channel->inLinkMode) {
            case LINK_MODE_RETRANSMISSION:
                if ((Channel->psmInfo->inLinkMode & Channel->link->linkModes) & L2MODE_FLOW_CONTROL) {
                    Channel->inLinkMode = LINK_MODE_FLOW_CONTROL;
                    goto SendConfigReq;
                }
                /* Drop into next case */

            case LINK_MODE_FLOW_CONTROL:
                if ((Channel->psmInfo->inLinkMode & Channel->link->linkModes) & L2MODE_BASIC) {
                    Channel->inLinkMode = LINK_MODE_BASIC;
                    goto SendConfigReq;
                }
                /* Drop out of switch to disconnect code */
            }
        }
#endif /* L2CAP_FLOW_CONTROL == XA_ENABLED */

        Report(("L2CAP: Received ConfigRspNeg, Disconnecting....\n"));
        /* Set the completion status. */
        L2C(callbackParms).status = (U16)(result|0x80);

SendDisconnect:
        /* Response was negative. We don't negotiate so Disconnect the channel. */
        status = L2Cap_SendDiscReqInternal(Channel);
        AssertEval(status == BT_STATUS_PENDING);
    }
    return;

SendConfigReq:
    SendAutoConfigReq(Channel);
    return;
}


/*---------------------------------------------------------------------------
 *            SendAutoConfigRsp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Sends configuration responses for auto-configuration channels.
 */
static void SendAutoConfigRsp(L2CAP_Channel *Channel)
{
    BtPacket   *rsp;
    BtStatus    status;

    Report(("L2CAP: SendAutoConfigRsp(%04x)\n", Channel->localCid));

    if ((rsp = L2Cap_GetSysPkt(FALSE)) == 0) {
        Channel->flags |= L2CHF_SEND_RESPONSE;
        L2C(flags) |= L2CCF_NEED_SYSPACKET;
        return;
    }
    Channel->flags &= ~L2CHF_SEND_RESPONSE;

    OS_MemCopy(rsp->data, Channel->workspace, Channel->wsLen);
    rsp->dataLen = Channel->wsLen;

    status = L2Cap_SendConfigRspInternal(Channel, rsp, Channel->result, 
             (L2capConfigFlags)((Channel->flags & L2CHF_MORE_CONFIG) ? L2CFG_FLAG_MORE : 0));

    Assert((status == BT_STATUS_PENDING) || (status == BT_STATUS_NO_CONNECTION));

    if (status != BT_STATUS_PENDING)
        ReturnSysPkt(rsp);
}


/*---------------------------------------------------------------------------
 *            SendAutoConfigReq()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Sends configuration requests for auto-configuration channels.
 */
static void SendAutoConfigReq(L2CAP_Channel *Channel)
{
    BtPacket   *request;
    BtStatus    status;

    if ((request = L2Cap_GetSysPkt(FALSE)) == 0) {
        Channel->flags |= L2CHF_SEND_REQUEST;
        L2C(flags) |= L2CCF_NEED_SYSPACKET;
        return;
    }
    Channel->flags &= ~L2CHF_SEND_REQUEST;

    /* Only include options if this is the first config req we're sending. */
    if ((Channel->flags & L2CHF_MORE_CONFIG) == 0) {

        /* Include the MTU if were not using the default. */
        if (Channel->psmInfo->localMtu != L2CAP_DEFAULT_MTU) {
            WriteConfigOption16(request->data, L2CFG_OPTION_MTU, Channel->psmInfo->localMtu);
            request->dataLen += 4;
        }

#if L2CAP_FLOW_CONTROL == XA_ENABLED
        /* Include the Flow Control option if we're not basic-only */
        if ((Channel->psmInfo->inLinkMode & (Channel->link->linkModes|L2MODE_BASIC)) != L2MODE_BASIC) {
            request->data[request->dataLen + 0] = L2CFG_OPTION_FLOW_CONTROL;
            request->data[request->dataLen + 1] = 9;
            request->data[request->dataLen + 2] = Channel->inLinkMode;
            request->data[request->dataLen + 3] = Channel->rxWindow;
            request->data[request->dataLen + 4] = Channel->psmInfo->inTransmitMax;
            StoreLE16(request->data + request->dataLen + 5, Channel->psmInfo->inRetransTimeout);
            StoreLE16(request->data + request->dataLen + 7, Channel->psmInfo->inMonitorTimeout);
            /* We offer the maximum segment size possible for best throughput. */
            StoreLE16(request->data + request->dataLen + 9, Channel->psmInfo->localMtu);
            request->dataLen += 11;
        }
#endif

#if  L2CAP_QOS == XA_ENABLED
    if(Channel->psmInfo->QosEnable == 1)
    {
        request->data[request->dataLen + 0] = L2CFG_OPTION_QOS;
        request->data[request->dataLen + 1] = 22;
        request->data[request->dataLen + 2] = 0;
        request->data[request->dataLen + 3] = Channel->psmInfo->QosServiceType;
        StoreLE32(request->data +request->dataLen + 4, Channel->psmInfo->QosTokenRate);
        StoreLE32(request->data +request->dataLen + 8, Channel->psmInfo->QosTokenBucketSize);
        StoreLE32(request->data +request->dataLen + 12, Channel->psmInfo->QosPeakBandWidth);
        StoreLE32(request->data +request->dataLen + 16, Channel->psmInfo->QosLatency);
        StoreLE32(request->data +request->dataLen + 20, Channel->psmInfo->QosDelayVariation);
        request->dataLen += 24;
    }
#endif

    }

    status = L2Cap_SendConfigReqInternal(Channel, request, L2CFG_FLAG_NONE);
    Assert((status == BT_STATUS_PENDING) || (Channel->link->state > BDS_CONNECTED));

    if (status != BT_STATUS_PENDING)
        ReturnSysPkt(request);
}

