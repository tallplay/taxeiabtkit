/***************************************************************************
 *
 * File:
 *     $Workfile:btutils.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:21$
 *
 * Description:
 *     Utility functions for protocol stack components.
 *
 * Created:
 *     October 7, 1999
 *
 * Copyright 1999-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "osapi.h"
#include "bttypes.h"
#include "utils.h"

/*---------------------------------------------------------------------------
 *            bdaddr_aton()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Convert a MSB ASCII bb:cc:dd:ee:ff to a LSB BD_ADDR value. The
 *            Generic Access profile specifies MSB order at the UI level. 
 *
 * Return:    BD_ADDR
 */
BD_ADDR bdaddr_aton(const char *Addr)
{
    U8       val = 0, i = 6;
    BD_ADDR  bd_addr;
    
    while (*Addr) {
        if (*Addr >= '0' && *Addr <= '9')
            val = (U8)((val << 4) + *Addr - '0');
        else if (*Addr >= 'A' && *Addr <= 'F')
            val = (U8)((val << 4) + *Addr - 'A'+10);
        else if (*Addr >= 'a' && *Addr <= 'f')
            val = (U8)((val << 4) + *Addr - 'a'+10);
        else bd_addr.addr[--i] = val;
        
        Addr++;
    }
    
    bd_addr.addr[--i] = val;
    Assert(i == 0);
    
    return bd_addr;
}

/*---------------------------------------------------------------------------
 *            bdaddr_ntoa()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Convert a LSB BD_ADDR to MSB ASCII notation aa:bb:cc:dd:ee:ff.
 *            The Generic Access profile specifies MSB order at the UI level.
 *
 *            AddrString should point to no less than BDADDR_NTOA_SIZE bytes. 
 *            It will be null-terminated upon return.
 *
 * Return:    pointer to the converted/provided AddrString
 */
char *bdaddr_ntoa(const BD_ADDR *Addr, char *AddrString)
{
    char        *bp = AddrString;
    U8           u, l;
    I8           i = 6;
    
    while (i > 0) {
        u = (U8)(Addr->addr[i-1]/16);
        l = (U8)(Addr->addr[i-1]%16);
        
        if (u < 10)
            *bp++ = (U8)('0' + u);
        else *bp++ = (U8)('A' + u - 10);
        
        if (l < 10)
            *bp++ = (U8)('0' + l);
        else *bp++ = (U8)('A' + l - 10);
        
        *bp++ = ':';
        i--;
    }
    
    *--bp = 0;
    
    return AddrString;
}

