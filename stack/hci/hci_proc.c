/***************************************************************************
 *
 * File:
 *     $Workfile:hci_proc.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:141$
 *
 * Description:
 *     This file contains code to process rx/tx and timer events
 *     for the HCI.
 *
 * Created:
 *     September 20, 1999
 *
 * Copyright 1999-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "btalloc.h"
#include "radiomgr.h"

static BtStatus HciGetMaxPackets(U16 entry, U16 *maxPackets);
static BtStatus SendHciPackets(HciHandle hciHandle);

/*---------------------------------------------------------------------------
 *            HciTransportError()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Called when the transport driver detects an error condition.
 *            This should be called at interrupt time.
 *
 * Return:    void
 */
void HciTransportError(void)
{
    bt.hci.transportError = TRUE;
    OS_NotifyEvm();
}

/*---------------------------------------------------------------------------
 *            HciBufferInd()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Called when a buffer has been received from the Host Controller.
 *            This should be called at interrupt time.
 *
 * Return:    void
 */
void HciBufferInd(HciBuffer *buffer, BtStatus status)
{
    if (status == BT_STATUS_SUCCESS) {
        /* Data was received from the Host Controller */
        InsertTailList(&(bt.hci.rxBuffList), &(buffer->node));
        OS_NotifyEvm();
    } else {
        /* Free the buffer, it contains no data */
        RXBUFF_Free(buffer);
        bt.hci.tranEntry.buffAvail(buffer->flags);
    }
}

/*---------------------------------------------------------------------------
 *            HciPacketSent()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Called when a packet has been sent to the Host Controller.
 *            This should be called at interrupt time.
 *
 * Return:    void
 */
void HciPacketSent(HciPacket *packet)
{
    /* Put the buffer back on the available list */
    InsertTailList(&(bt.hci.sentPacketList), &(packet->node));
    if (!(packet->flags & HCI_BUFTYPE_INTERMEDIATE)) {
        OS_NotifyEvm();
    }
}

/*---------------------------------------------------------------------------
 *            HciGetMaxPackets()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Returns the maximum number of HCI packets that can be sent on
 *            the specified connection;
 *
 * Return:    BT_STATUS_SUCCESS - the Host Controller can receive more data.
 *            The maxPackets value shows how much data to send on this
 *            connection (can be 0 in some cases).
 *
 *            BT_STATUS_FAILED - the Host Controller cannot receive any more
 *            data.  maxPackets will always be 0.
 *
 */
static BtStatus HciGetMaxPackets(U16 entry, U16 *maxPackets)
{
#if HCI_SCO_ENABLED
    BtPacket *btPacket;
    HciCallbackParms cbParms;
#endif /* HCI_SCO_ENABLED */
    
    *maxPackets = 0;

    /* Determine the number of HCI packets to be sent 
     * for the current BtPacket.
     */
    if (bt.hci.con[entry].conFlags & HCI_DATA_TYPE_ACL) {
        /* ACL data */
        Assert(bt.hci.aclTxPacketLen != 0);
        *maxPackets = bt.hci.con[entry].remaining / bt.hci.aclTxPacketLen;
        if (bt.hci.con[entry].remaining % bt.hci.aclTxPacketLen) {
            /* A partial packet is left over */
            (*maxPackets)++;
        }
    } 
#if HCI_SCO_ENABLED
    else if (bt.hci.con[entry].conFlags & HCI_DATA_TYPE_SCO) {
        /* SCO data */
        if (bt.hci.scoTxPacketLen == 0) {
            /* Radio does not support SCO Data, fail the Tx request */
            btPacket = (BtPacket *)RemoveHeadList(&(bt.hci.con[entry].btPacketList));

            bt.hci.con[entry].remaining = 0;
            bt.hci.con[entry].btPacket = 0;

            cbParms.hciHandle = bt.hci.con[entry].hciHandle;
            cbParms.status = BT_STATUS_NOT_SUPPORTED;
            cbParms.ptr.packet = btPacket;
            if (bt.hci.radioEntry) {
                bt.hci.radioEntry(HCI_PACKET_HANDLED, &cbParms); 
            } else {
#if BT_STACK == XA_ENABLED            
                ME_Callback(HCI_PACKET_HANDLED, &cbParms); 
#else
                HCI_Callback(HCI_PACKET_HANDLED, &cbParms); 
#endif
            }
            goto Error_Exit;
        }

        *maxPackets = bt.hci.con[entry].remaining / bt.hci.scoTxPacketLen;
        if (bt.hci.con[entry].remaining % bt.hci.scoTxPacketLen) {
            /* A partial packet is left over */
            (*maxPackets)++;
        }
    }
#endif /* HCI_SCO_ENABLED */
    else {
        Assert(0);
        goto Error_Exit;
    }

    /* Determine the maximum number of frames that can be 
     * sent on this connection at this time.
     */
    if (bt.hci.con[entry].conFlags & HCI_DATA_TYPE_ACL) {
        /* ACL data */
        *maxPackets = min(*maxPackets, bt.hci.aclTxPacketsLeft);
    } 
#if HCI_SCO_ENABLED
    else if (bt.hci.con[entry].conFlags & HCI_DATA_TYPE_SCO) {
        /* SCO data */
#if HCI_SCO_FLOW_CONTROL == XA_ENABLED
        *maxPackets = min(*maxPackets, bt.hci.scoTxPacketsLeft);
#endif /* HCI_SCO_FLOW_CONTROL == XA_ENABLED */
    } 
#endif /* HCI_SCO_ENABLED && (HCI_SCO_FLOW_CONTROL == XA_ENABLED) */
    else {
        Assert(0);
        goto Error_Exit;
    }

#if XA_STATISTICS == XA_ENABLED
    if (*maxPackets > bt.hci.hciPacketsLeft) {
        XASTAT_CounterInc(hci_num_no_hci_packets, 
                          *maxPackets - bt.hci.hciPacketsLeft);
    }
#endif /* XA_STATISTICS == XA_ENABLED */

    *maxPackets = min(*maxPackets, bt.hci.hciPacketsLeft);

Error_Exit:

#if HCI_SCO_ENABLED && (HCI_SCO_FLOW_CONTROL == XA_ENABLED)
    if ((bt.hci.aclTxPacketsLeft + bt.hci.scoTxPacketsLeft) == 0) {
#else
    if (bt.hci.aclTxPacketsLeft == 0) {
#endif /* HCI_SCO_ENABLED && (HCI_SCO_FLOW_CONTROL == XA_ENABLED) */
        return BT_STATUS_FAILED;
    } else {
        return BT_STATUS_SUCCESS;
    }
}

/*---------------------------------------------------------------------------
 *            SendHciPackets()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Send Hci packets on the specified connection.
 *
 * Return:    BT_STATUS_SUCCESS if more data can be sent to the Host Controller.
 * 
 *            BT_STATUS_FAILED if no more data can be sent.
 */
static BtStatus SendHciPackets(HciHandle hciHandle)
{
    HciPacket *hciPacket;
    U16        maxPackets = 0;
    U16        entry;
    I16        i;
    U8         fragment;
    U16        dataLen = 0;
    BtStatus   status;
    BOOL       done;
    const U8  *ptr;
    U16        len = 0;

#if HCI_ALLOW_PRESCAN == XA_ENABLED
    BOOL               handled = FALSE;
    HciPrescanHandler *handler;
#endif /* HCI_ALLOW_PRESCAN == XA_ENABLED */

    entry = HciGetTableEntry(hciHandle);
    Assert(entry != INVALID_HCI_HANDLE);

    /* See how many packet must be sent */
    status = HciGetMaxPackets(entry, &maxPackets);

    for (i = 0; i < maxPackets; i++) {

        /* Get a transmit packet */
        if (!IsListEmpty(&(bt.hci.hciPacketPool))) {
            hciPacket = (HciPacket *)RemoveHeadList(&(bt.hci.hciPacketPool));
        } else {
            /* No transmit packets left */
            break;
        }

        /* Initialize */
        if (bt.hci.con[entry].conFlags & HCI_DATA_TYPE_ACL) {
            hciPacket->flags = HCI_BUFTYPE_ACL_DATA;
        } else {
            hciPacket->flags = HCI_BUFTYPE_SCO_DATA;
        }

        hciPacket->flags |= HCI_BUFTYPE_INTERMEDIATE;
        fragment = 0;
        dataLen = 0;

        if (bt.hci.con[entry].buffPtr == bt.hci.con[entry].btPacket->header) {
            /* This is the first packet */
            if (hciPacket->flags & HCI_BUFTYPE_ACL_DATA) {
                hciHandle |= FIRST_PACKET;
                hciHandle &= ~CONTINUING_PACKET;
            }
            bt.hci.con[entry].txState = HCI_TX_STATE_SEND_HEADER;
            ptr = bt.hci.con[entry].btPacket->header + 
                  (BT_PACKET_HEADER_LEN - bt.hci.con[entry].btPacket->headerLen);
            bt.hci.con[entry].fragRemain = bt.hci.con[entry].btPacket->headerLen;
        } else {
            /* This is a continuation packet */
            if (hciPacket->flags & HCI_BUFTYPE_ACL_DATA) {
                hciHandle |= CONTINUING_PACKET;
                hciHandle &= ~FIRST_PACKET;
            }
            ptr = bt.hci.con[entry].buffPtr;
        } 

        done = FALSE;
        while (!done) {
            
            /* Set up the transmit packet */
            switch (bt.hci.con[entry].txState) {
            case HCI_TX_STATE_SEND_HEADER:

                /* Send the BtPacket header */

                if (bt.hci.con[entry].fragRemain > 0) {
                    /* Not done sending header */
                    break;
                }

                /* Set up the next state */
                bt.hci.con[entry].txState = HCI_TX_STATE_SEND_DATA;

                if (bt.hci.con[entry].btPacket->dataLen > 0) {
                    /* There is data to send */
                    ptr = bt.hci.con[entry].btPacket->data;
                    bt.hci.con[entry].fragRemain = bt.hci.con[entry].btPacket->dataLen;
                } 

                /* Fall through to next state */

            case HCI_TX_STATE_SEND_DATA:

                /* Send the BtPacket data */

                if (bt.hci.con[entry].fragRemain > 0) {
                    /* Not done sending data */
                    break;
                }

                /* Set up the next state */
                bt.hci.con[entry].txState = HCI_TX_STATE_SEND_TAIL;

                if (bt.hci.con[entry].btPacket->flags & BTP_FLAG_TAIL) {
                    /* There is a tail that must be sent */
                    ptr = bt.hci.con[entry].btPacket->tail;
                    bt.hci.con[entry].fragRemain = bt.hci.con[entry].btPacket->tailLen;
                    
                } 
                
                /* Fall through to next state */

            case HCI_TX_STATE_SEND_TAIL:

                /* Send the BtPacket tail */

                if (bt.hci.con[entry].fragRemain > 0) {
                    /* Not done sending tail */
                    break;
                }

#if L2CAP_FLOW_CONTROL == XA_ENABLED
                /* Set up the next state */
                bt.hci.con[entry].txState = HCI_TX_STATE_SEND_FCS;

                if (bt.hci.con[entry].btPacket->flags & BTP_FLAG_FCS) {
                    /* There is a FCS that must be sent */
                    ptr = bt.hci.con[entry].btPacket->fcs;
                    bt.hci.con[entry].fragRemain = 2;
                } 
                
                /* Fall through to next state */

            case HCI_TX_STATE_SEND_FCS:

                /* Send the BtPacket L2CAP FCS */

                if (bt.hci.con[entry].fragRemain > 0) {
                    /* Not done sending FCS */
                    break;
                }
#endif /* L2CAP_FLOW_CONTROL == XA_ENABLED */

                hciPacket->flags &= ~HCI_BUFTYPE_INTERMEDIATE;
                done = TRUE;
                break;
            }

            if (!done) {
                /* Initialize the length of the current fragment */
                if (hciPacket->flags & HCI_BUFTYPE_ACL_DATA) {  
                    /* Initialize the length of the current fragment */
                    len = min(bt.hci.aclTxPacketLen - dataLen,
                              bt.hci.con[entry].fragRemain);
                }
#if HCI_SCO_ENABLED
                else if (hciPacket->flags & HCI_BUFTYPE_SCO_DATA) {
                    len = min(bt.hci.scoTxPacketLen - dataLen,
                              bt.hci.con[entry].fragRemain);
                }
#endif /* HCI_SCO_ENABLED */
#if XA_DEBUG == XA_ENABLED
                else {
                    Assert(0);
                }
#endif /* XA_DEBUG == XA_ENABLED */

                /* Set up the fragment */
                hciPacket->f[fragment].len = len;
                hciPacket->f[fragment].buffer = ptr;

                bt.hci.con[entry].fragRemain -= len;
                bt.hci.con[entry].remaining -= len;
                bt.hci.con[entry].buffPtr = ptr + len;
                dataLen += len;

                fragment++;

                if (hciPacket->flags & HCI_BUFTYPE_ACL_DATA) {  
                    Assert(dataLen <= bt.hci.aclTxPacketLen);
                    if (dataLen == bt.hci.aclTxPacketLen) {
                        hciPacket->flags &= ~HCI_BUFTYPE_INTERMEDIATE;
                        done = TRUE;
                    }
                }

#if HCI_SCO_ENABLED
                else {
                    Assert(dataLen <= bt.hci.scoTxPacketLen);
                    if (dataLen == bt.hci.scoTxPacketLen) {
                        hciPacket->flags &= ~HCI_BUFTYPE_INTERMEDIATE;
                        done = TRUE;
                    }
                }
#endif /* HCI_SCO_ENABLED */
            }
        }

        if (hciPacket->flags & HCI_BUFTYPE_ACL_DATA) {
            /* Update the connection values */
            Assert(bt.hci.aclTxPacketsLeft > 0 );
            bt.hci.aclTxPacketsLeft--;
        }
#if HCI_SCO_ENABLED && (HCI_SCO_FLOW_CONTROL == XA_ENABLED)
        else {
            /* Update the connection values */
            Assert(bt.hci.scoTxPacketsLeft > 0 );
            bt.hci.scoTxPacketsLeft--;
        }
#endif /* HCI_SCO_ENABLED && (HCI_SCO_FLOW_CONTROL == XA_ENABLED) */

        hciPacket->fCount = fragment;
        bt.hci.con[entry].packetsSent++;

        /* Set up the HCI packet header */
        StoreLE16(hciPacket->header, hciHandle);

#if HCI_SCO_ENABLED
        if (hciPacket->flags & HCI_BUFTYPE_ACL_DATA) {
            /* ACL data */
            StoreLE16(hciPacket->header + 2, dataLen);
        } else {
            /* SCO data, header length == 3,  the transport must
             * be aware of this and send only the first 3 bytes.
             */
            Assert(dataLen <= 255);
            hciPacket->header[2] = (U8)dataLen;
            hciPacket->header[3] = 0;
        }
#else /* HCI_SCO_ENABLED */
        StoreLE16(hciPacket->header + 2, dataLen);
#endif /* HCI_SCO_ENABLED */

        /* Reference count the HCI packets sent */
        bt.hci.con[entry].btPacket->hciPackets++;
        bt.hci.hciPacketsLeft--;

#if HCI_ALLOW_PRESCAN == XA_ENABLED
        /* Call all transmit prescan functions */
        handler = (HciPrescanHandler *)GetHeadList(&bt.hci.psc.prescanList);
        while ((ListEntry *)handler != &bt.hci.psc.prescanList) {
            if (handler->hciTxPrescan != 0) {
                handled = handler->hciTxPrescan(hciPacket);
                if (handled) {
                    /* The data was handled elsewhere */
                    break;
                }
            }
            handler = (HciPrescanHandler *)GetNextNode(&(handler->node));
        }

        if (!handled) {
            /* Send the packet */
            XASTAT_PeakInc(hci_num_hci_packets, 1);
            bt.hci.tranEntry.sendData(hciPacket);
        } else {
            /* Re-insert the packet */
            InsertTailList(&(bt.hci.hciPacketPool), &(hciPacket->node));
        }
#else

        XASTAT_PeakInc(hci_num_hci_packets, 1);
        bt.hci.tranEntry.sendData(hciPacket);

#endif /* HCI_ALLOW_PRESCAN == XA_ENABLED */

        if (bt.hci.con[entry].remaining == 0) {
            /* The entire packet was sent */
            bt.hci.con[entry].btPacket = 0;
        }
    }   

    if (dataLen == 0) {
        return status;
    } else {
        return BT_STATUS_SUCCESS;
    }
}

/*---------------------------------------------------------------------------
 *            HciProcessSentPackets()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Process any HCI packets that have been sent.
 *
 * Return:    void
 */
static void HciProcessSentPackets(void)
{
    HciPacket        *hciPacket;
    HciCommand       *hciCommand;
    BtPacket         *btPacket;
    HciCallbackParms  cbParms;
    U16               entry;

#if HCI_ALLOW_PRESCAN == XA_ENABLED
    BOOL               handled = FALSE;
    HciPrescanHandler *handler;
#endif /* HCI_ALLOW_PRESCAN == XA_ENABLED */

    OS_StopHardware();
    while (!IsListEmpty(&(bt.hci.sentPacketList))) {

        /* Remove the transmit packet from the list and put it back
         * on the free list.
         */
        hciPacket = (HciPacket *)RemoveHeadList(&(bt.hci.sentPacketList));
        if (!IsNodeOnList(&bt.hci.hciPacketPool, &hciPacket->node)) {
            InsertTailList(&(bt.hci.hciPacketPool), &(hciPacket->node));
        } else {
            /* Ignore this packet */
            OS_ResumeHardware();
            continue;
        }
        OS_ResumeHardware();

#if HCI_ALLOW_PRESCAN == XA_ENABLED
        /* Call all transmit TX done functions */
        handler = (HciPrescanHandler *)GetHeadList(&bt.hci.psc.prescanList);
        while ((ListEntry *)handler != &bt.hci.psc.prescanList) {
            if (handler->hciTxDone != 0) {
                handled = handler->hciTxDone(hciPacket);
                if (handled) {
                    /* The event was handled.
                     */
                    break;
                }
            }
            handler = (HciPrescanHandler *)GetNextNode(&(handler->node));
        }
#endif /* HCI_ALLOW_PRESCAN == XA_ENABLED */

        if (hciPacket->flags & (HCI_BUFTYPE_ACL_DATA | HCI_BUFTYPE_SCO_DATA)) {

#if HCI_ALLOW_PRESCAN == XA_ENABLED
            if (handled) {
                /* This event was handled in a presan */
                return;
            }
#endif /* HCI_ALLOW_PRESCAN == XA_ENABLED */

            XASTAT_PeakDec(hci_num_hci_packets, 1);
            bt.hci.hciPacketsLeft++;

            /* Get the table entry for the returned packet */
            entry = HciGetTableEntry(LEtoHost16(hciPacket->header));
            if (entry == INVALID_HCI_HANDLE) {
                /* The connection went away, ignore this event */
                Report(("HCI:  Invalid HCI handle in sent packet list\n"));
                return;
            }
            
            if (!(IsListEmpty(&(bt.hci.con[entry].btPacketList)))) {
                /* Update the number of outstanding HCI packets for this BtPacket */
                btPacket = (BtPacket *)GetHeadList(&(bt.hci.con[entry].btPacketList));
                Assert(btPacket->hciPackets > 0);
                btPacket->hciPackets--;

                if (btPacket->hciPackets == 0) {
                    /* All outstanding HCI packets have been completed.
                     * Now see if all HCI packets for this BtPacket have
                     * been sent.
                     */
                    if (!((btPacket == bt.hci.con[entry].btPacket) && (bt.hci.con[entry].remaining))) {
                        /* All the data for this transmit packet has been sent */
                        btPacket = (BtPacket *)RemoveHeadList(&(bt.hci.con[entry].btPacketList));

#if XA_STATISTICS == XA_ENABLED
                        if (bt.hci.con[entry].conFlags & HCI_DATA_TYPE_ACL) {
                            XASTAT_PeakDec(hci_num_acl_bt_packets, 1);
                        } else {
                            XASTAT_PeakDec(hci_num_sco_bt_packets, 1);
                        }
#endif /* XA_STATISTICS == XA_ENABLED */

                        /* Convert a dynamic broadcast handle to a static one */
                        if (bt.hci.con[entry].hciHandle == bt.hci.piconetBcHandle) {
                            cbParms.hciHandle = PICONET_BC_HANDLE;
                        } else if (bt.hci.con[entry].hciHandle == bt.hci.activeBcHandle) {
                            cbParms.hciHandle = ACTIVE_BC_HANDLE;
                        } else {
                            cbParms.hciHandle = bt.hci.con[entry].hciHandle;
                        }

                        XASTAT_PeakSet(hci_max_tx_packet_size, btPacket->dataLen + btPacket->headerLen);
                        XASTAT_TimerStop(btPacket->dataLen, hci_tx_timer, btPacket->hci_timer);

                        cbParms.status = BT_STATUS_SUCCESS;
                        cbParms.ptr.packet = btPacket;
                        if (bt.hci.radioEntry) {
                            bt.hci.radioEntry(HCI_PACKET_HANDLED, &cbParms); 
                        } else {
#if BT_STACK == XA_ENABLED            
                            ME_Callback(HCI_PACKET_HANDLED, &cbParms); 
#else
                            HCI_Callback(HCI_PACKET_HANDLED, &cbParms); 
#endif
                        }
                    }
                }
            }
        } else {
            /* A command was handled */
            Assert(hciPacket->flags & HCI_BUFTYPE_COMMAND);

            /* Tell upper layer */
            if (!IsListEmpty(&bt.hci.sentCommandList)) {
                hciCommand = (HciCommand *)RemoveHeadList(&bt.hci.sentCommandList);
#if HCI_ALLOW_PRESCAN == XA_ENABLED
                if (!handled) {
#endif /* HCI_ALLOW_PRESCAN == XA_ENABLED */
                    if (hciCommand != &bt.hci.cmd) {
                        cbParms.status = BT_STATUS_SUCCESS;
                        cbParms.ptr.cmd = hciCommand;
                        if (bt.hci.radioEntry) {
                            bt.hci.radioEntry(HCI_COMMAND_HANDLED, &cbParms); 
                        } else {
#if BT_STACK == XA_ENABLED            
                            /* Not an internal command, notify ME */
                            ME_Callback(HCI_COMMAND_HANDLED, &cbParms);
#else
                            HCI_Callback(HCI_COMMAND_HANDLED, &cbParms);
#endif
                        }
                    }
#if HCI_ALLOW_PRESCAN == XA_ENABLED
                }
#endif /* HCI_ALLOW_PRESCAN == XA_ENABLED */
            }
        }

        OS_StopHardware();
    }
    OS_ResumeHardware();
}

/*---------------------------------------------------------------------------
 *            HciProcessReceivedData()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Process any received ACL, SCO or event data.
 *
 * Return:    void
 */
static void HciProcessReceivedData(void)
{
    HciBuffer        *hciRxBuffer;
    HciCallbackParms  cbParms;
    U16               entry = INVALID_HCI_HANDLE;
    ListEntry         tmpRxBuffList;
    U8                headerSize = 0;
    U16               dataLen = 0;

#if HCI_ALLOW_PRESCAN == XA_ENABLED
    BOOL               handled = FALSE;
    HciPrescanHandler *handler;
#endif /* HCI_ALLOW_PRESCAN == XA_ENABLED */

    OS_StopHardware();

    /* Move all elements of the receive buffer list to a temporary
     * list head for processing.  This limits the processing of
     * receive buffers to the buffers already received.  New
     * received data will be processed next time.
     */
    MoveList(&tmpRxBuffList, &(bt.hci.rxBuffList));

    while (!IsListEmpty(&tmpRxBuffList)) {

        /* Remove the data buffer from the list */
        hciRxBuffer = (HciBuffer *)RemoveHeadList(&tmpRxBuffList);
        OS_ResumeHardware();

        /* Make sure all packets are handled first */
        HciProcessSentPackets();

#if HCI_ALLOW_PRESCAN == XA_ENABLED
        /* Call all RX prescan functions */
        handler = (HciPrescanHandler *)GetHeadList(&bt.hci.psc.prescanList);
        while ((ListEntry *)handler != &bt.hci.psc.prescanList) {
            if (handler->hciRxPrescan != 0) {
                handled = handler->hciRxPrescan(hciRxBuffer);
                if (handled) {
                    break;
                }
            }
            handler = (HciPrescanHandler *)(GetNextNode(&(handler->node)));
        }

        if (!handled) {
#endif /* HCI_ALLOW_PRESCAN == XA_ENABLED */

            if (hciRxBuffer->flags & HCI_BUFTYPE_EVENT) {            
                /* Parse and handle the event */
                HciProcessEvent(hciRxBuffer);
            } else {
                /* ACL or SCO data received */
                Assert((hciRxBuffer->flags & HCI_BUFTYPE_ACL_DATA) ||
                       (hciRxBuffer->flags & HCI_BUFTYPE_SCO_DATA));

                if (hciRxBuffer->flags & HCI_BUFTYPE_ACL_DATA) {
                  headerSize = 4;    /* ACL Header size */
                  dataLen = LEtoHost16(hciRxBuffer->buffer + 2);
                } else if (hciRxBuffer->flags & HCI_BUFTYPE_SCO_DATA) {
                  headerSize = 3;    /* SCO Header size */
                  dataLen = *(U8*)(hciRxBuffer->buffer + 2);
                }
#if XA_DEBUG == XA_ENABLED
                else {
                    Assert(0);
                }
#endif /* XA_DEBUG == XA_ENABLED */

                if (dataLen > 0) {
                    /* Adjust the pointer and length */
                    hciRxBuffer->len -= headerSize;
                    Assert(hciRxBuffer->len == dataLen);

                    /* Set up callback parms */
                    cbParms.status = BT_STATUS_SUCCESS;
                    cbParms.hciHandle = LEtoHost16(hciRxBuffer->buffer);

                    /* Skip over header */
                    hciRxBuffer->buffer += headerSize;
                    cbParms.ptr.rxBuff = hciRxBuffer;
    
                    /* Get the entry in the connection table */
                    entry = HciGetTableEntry(cbParms.hciHandle);

#if XA_DEBUG == XA_ENABLED
                    if (entry == INVALID_HCI_HANDLE) {
                        Report(("HCI: Received data for an invalid HCI handle (ignored)\n"));
                    }
#endif /* XA_DEBUG == XA_ENABLED */

                    /* Pass up the data to ME */
                    if (entry != INVALID_HCI_HANDLE) {
                        if (bt.hci.radioEntry) {
                            bt.hci.radioEntry(HCI_DATA_IND, &cbParms); 
                        } else {
#if BT_STACK == XA_ENABLED            
                            ME_Callback(HCI_DATA_IND, &cbParms);
#else
                            HCI_Callback(HCI_DATA_IND, &cbParms);
#endif
                        }
                    }

                    hciRxBuffer->buffer -= headerSize;
                }

#if HCI_HOST_FLOW_CONTROL == XA_ENABLED        
                if (hciRxBuffer->flags & HCI_BUFTYPE_ACL_DATA) {
                    /* Update the number of receive packets left */
                    if (entry != INVALID_HCI_HANDLE) {
                        Assert(bt.hci.aclRxPacketsLeft != 0);
                        bt.hci.aclRxPacketsLeft--;
                        bt.hci.flags |= HCI_FLAG_PACKETS_COMPLETE;
                        bt.hci.con[entry].packetsComplete++;
                    }
                } 
#if HCI_SCO_ENABLED && (HCI_SCO_FLOW_CONTROL == XA_ENABLED)
                else {
                    /* Update the number of receive packets left */
                    if (entry != INVALID_HCI_HANDLE) {
                        Assert(bt.hci.scoRxPacketsLeft != 0);
                        bt.hci.scoRxPacketsLeft--;
                        bt.hci.flags |= HCI_FLAG_PACKETS_COMPLETE;
                        bt.hci.con[entry].packetsComplete++;
                    }
                }
#endif /* HCI_SCO_ENABLED && (HCI_SCO_FLOW_CONTROL == XA_ENABLED) */
#endif /* HCI_HOST_FLOW_CONTROL == XA_ENABLED */

            }

#if HCI_ALLOW_PRESCAN == XA_ENABLED
        }

        /* Call all RX done functions */
        handled = FALSE;
        handler = (HciPrescanHandler *)GetHeadList(&bt.hci.psc.prescanList);
        while ((ListEntry *)handler != &bt.hci.psc.prescanList) {
            if (handler->hciRxDone != 0) {
                handled = handler->hciRxDone(hciRxBuffer);
                if (handled) {
                    /* The event was handled */
                    break;
                }
            }
            handler = (HciPrescanHandler *)GetNextNode(&(handler->node));
        }

        if (handled) return;
#endif /* HCI_ALLOW_PRESCAN == XA_ENABLED */

        /* Return the buffer to the driver */
        OS_StopHardware();
        RXBUFF_Free(hciRxBuffer);
        bt.hci.tranEntry.buffAvail(hciRxBuffer->flags);//if wait rx_buffer, feed it
    }
    OS_ResumeHardware();
}

#if HCI_HOST_FLOW_CONTROL == XA_ENABLED        
/*---------------------------------------------------------------------------
 *            HciSendCompletedPackets()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  When Controller to Host flow control is enabled, the
 *            number of packets completed must be sent to the controller.
 *
 * Return:    void
 */
static void HciSendCompletedPackets(void)
{
    U8          numHandles;
    I16         i;

    if (((bt.hci.flags & HCI_FLAG_PACKETS_COMPLETE) && 
        ((bt.hci.aclRxPacketsLeft <= (HCI_NUM_ACL_BUFFERS / 2))

#if HCI_SCO_ENABLED && (HCI_SCO_FLOW_CONTROL == XA_ENABLED)
        || (bt.hci.scoRxPacketsLeft <= (HCI_NUM_SCO_BUFFERS / 2))
#endif /* HCI_SCO_ENABLED && (HCI_SCO_FLOW_CONTROL == XA_ENABLED) */

        )) && (!IsNodeOnList(&(bt.hci.commandList), &(bt.hci.cmd.node)))  
           && (!IsNodeOnList(&(bt.hci.sentCommandList), &(bt.hci.cmd.node)))) {

        /* Set up the command */
        numHandles = 0;
        for (i = 0; i < HCI_NUM_HANDLES; i++) {
            if ((bt.hci.con[i].conFlags & HCI_ACTIVE_CON) &&
                (bt.hci.con[i].packetsComplete)) {

                /* Add an entry to the command parameters */
                StoreLE16(&(bt.hci.cmd.parms[(numHandles * 4) + 1]), 
                          bt.hci.con[i].hciHandle);
                StoreLE16(&(bt.hci.cmd.parms[(numHandles * 4) + 3]), 
                          bt.hci.con[i].packetsComplete);
                bt.hci.con[i].packetsComplete = 0;
                numHandles++;
            }
        }
        bt.hci.flags &= ~HCI_FLAG_PACKETS_COMPLETE;

        if (numHandles) {
            bt.hci.cmd.parms[0] = numHandles;
            HciSendCommand(HCC_HOST_NUM_COMPLETED_PACKETS,
                            (U8)(numHandles * 4 + 1),
                            &bt.hci.cmd, TRUE);
        }

        bt.hci.aclRxPacketsLeft = HCI_NUM_ACL_BUFFERS;

#if HCI_SCO_ENABLED && (HCI_SCO_FLOW_CONTROL == XA_ENABLED)
        bt.hci.scoRxPacketsLeft = HCI_NUM_SCO_BUFFERS;
#endif /* HCI_SCO_ENABLED && (HCI_SCO_FLOW_CONTROL == XA_ENABLED) */
    }
}
#endif /* HCI_HOST_FLOW_CONTROL */

/*---------------------------------------------------------------------------
 *            HciSendCommands()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Send any pending HCI commands.
 *
 * Return:    void
 */
static void HciSendCommands(void)
{
    HciCommand  *hciCommand;
    HciPacket   *hciPacket;
    U8           cmdsSent = 0;

#if HCI_ALLOW_PRESCAN == XA_ENABLED
    HciCallbackParms  cbParms;
    BOOL               handled = FALSE;
    HciPrescanHandler *handler;
#endif /* HCI_ALLOW_PRESCAN == XA_ENABLED */

    while (!IsListEmpty(&(bt.hci.commandList))) {
        if (bt.hci.maxCommands && 
            (cmdsSent < HCI_NUM_COMMANDS) &&
            (bt.hci.tranEntry.sendData != 0)) {
            /* Send the command */
            hciCommand = (HciCommand *)RemoveHeadList(&(bt.hci.commandList));

            /* Get a transmit packet */
            if (!IsListEmpty(&(bt.hci.hciPacketPool))) {
                hciPacket = (HciPacket *)RemoveHeadList(&(bt.hci.hciPacketPool));
            } else {
                /* No transmit packets left */
                InsertHeadList(&bt.hci.commandList, &(hciCommand->node));
                break;
            }

            if (LEtoHost16(hciCommand->header) == HCC_CREATE_CONNECTION) {
                bt.hci.conReqs++;
            }

            /* Initialize the transmit packet */
            hciPacket->flags = HCI_BUFTYPE_COMMAND;
            OS_MemCopy(hciPacket->header, hciCommand->header, 3);
            hciPacket->f[0].buffer = hciCommand->parms;
            hciPacket->f[0].len = hciCommand->header[2];
            if (hciPacket->f[0].len) {
                hciPacket->fCount = 1;
            } else {
                hciPacket->fCount = 0;
            }

#if HCI_ALLOW_PRESCAN == XA_ENABLED
            /* Call all transmit prescan functions */
            handler = (HciPrescanHandler *)GetHeadList(&bt.hci.psc.prescanList);
            while ((ListEntry *)handler != &bt.hci.psc.prescanList) {
                if (handler->hciTxPrescan != 0) {
                    handled = handler->hciTxPrescan(hciPacket);
                    if (handled) {
                        /* Event was handled */
                        break;
                    }
                }
                handler = (HciPrescanHandler *)GetNextNode(&(handler->node));
            }

            if (handled) {
                /* The prescan consumed the command */
                if (hciCommand != &bt.hci.cmd) {
                    /* Not an internal command, notify ME */
                    cbParms.status = BT_STATUS_SUCCESS;
                    cbParms.ptr.cmd = hciCommand;
                    if (bt.hci.radioEntry) {
                        bt.hci.radioEntry(HCI_COMMAND_HANDLED, &cbParms); 
                    } else {
#if BT_STACK == XA_ENABLED            
                        ME_Callback(HCI_COMMAND_HANDLED, &cbParms);
#else
                        HCI_Callback(HCI_COMMAND_HANDLED, &cbParms);
#endif
                    }
                }
                /* Return the transmit packet */
                InsertTailList(&(bt.hci.hciPacketPool), &(hciPacket->node));
                break;
            }

#endif /* HCI_ALLOW_PRESCAN == XA_ENABLED */

            bt.hci.tranEntry.sendData(hciPacket);

            cmdsSent++;

#if HCI_HOST_FLOW_CONTROL == XA_ENABLED
            if (LEtoHost16(hciCommand->header) != 
                HCC_HOST_NUM_COMPLETED_PACKETS) {
                bt.hci.maxCommands--;
            }
#else
            bt.hci.maxCommands--;
#endif /* HCI_HOST_FLOW_CONTROL == XA_ENABLED */

            /* Insert the command on the sent list, to be completed later */
            InsertTailList(&(bt.hci.sentCommandList), &(hciCommand->node));

        } else {
            /* Cannot send right now */
            break;
        }
    }
}

/*---------------------------------------------------------------------------
 *            HciCheckForTxData()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  See if any connections have data to send.  Data is 
 *            taken one packet at a time from each connection and transmitted.
 *
 * Return:    void
 */
static void HciCheckForTxData(void)
{
    HciCallbackParms  cbParms;
    I16                i;

    bt.hci.flags &=~HCI_FLAG_DATA_QUEUED;
    for (i = 0; i < HCI_NUM_HANDLES; i++) {
        if ((bt.hci.state == HCI_STATE_READY) &&
            (bt.hci.con[bt.hci.txCon].conFlags & HCI_ACTIVE_CON) && 
            (bt.hci.con[bt.hci.txCon].conFlags & HCI_DATA_REQ) &&
            (bt.hci.tranEntry.sendData != 0)) {

            /* Data is available to be sent on this (active) connection */

            if (bt.hci.con[bt.hci.txCon].remaining != 0) {
                /* There is still data to be sent in the current BtPacket,
                 * continue sending the data.
                 */
                bt.hci.flags |= HCI_FLAG_DATA_QUEUED;
                if (SendHciPackets(bt.hci.con[bt.hci.txCon].hciHandle) != BT_STATUS_SUCCESS) {
                    break;
                }
            } else if (bt.hci.con[bt.hci.txCon].btPacket == 0) {
                /* There is no outstanding data to be sent from the current BtPacket
                 * Data must be requested from an upper layer.
                 */

                Assert((bt.hci.con[bt.hci.txCon].conFlags & HCI_DATA_TYPE_ACL) ||
                       (bt.hci.con[bt.hci.txCon].conFlags & HCI_DATA_TYPE_SCO));

                /* Call for data from ME */
                cbParms.status = BT_STATUS_SUCCESS;

                if (bt.hci.con[bt.hci.txCon].hciHandle & 
                    (PICONET_BROADCAST | ACTIVE_BROADCAST)) {

                    /* Don't send broadcast data if a connect request 
                     * is outstanding.  This should avoid the broadcast handle
                     * race condition.  
                     */
                    if (bt.hci.conReqs) {
                        continue;
                    }

                    /* Convert a dynamic broadcast handle to a static one */
                    if (bt.hci.con[bt.hci.txCon].hciHandle == bt.hci.activeBcHandle) {
                        cbParms.hciHandle = ACTIVE_BC_HANDLE;
                    } else {
                        cbParms.hciHandle = PICONET_BC_HANDLE;
                    }
                } else {
                        cbParms.hciHandle = bt.hci.con[bt.hci.txCon].hciHandle;
                }

                if (bt.hci.radioEntry) {
                    bt.hci.radioEntry(HCI_SEND_IND, &cbParms); 
                } else {
#if BT_STACK == XA_ENABLED            
                    ME_Callback(HCI_SEND_IND, &cbParms);
#else
                    HCI_Callback(HCI_SEND_IND, &cbParms);
#endif
                }
                if (cbParms.ptr.packet) {

                    /* There is a new packet to send, set it up */
                    XASTAT_TimerStart(hci_tx_timer, cbParms.ptr.packet->hci_timer);
                    Assert(bt.hci.con[bt.hci.txCon].btPacket == 0);
                    bt.hci.con[bt.hci.txCon].btPacket = cbParms.ptr.packet;
                    bt.hci.con[bt.hci.txCon].btPacket->hciPackets = 0;
                    bt.hci.con[bt.hci.txCon].buffPtr = bt.hci.con[bt.hci.txCon].btPacket->header;
                    bt.hci.con[bt.hci.txCon].remaining = bt.hci.con[bt.hci.txCon].btPacket->dataLen +
                                            bt.hci.con[bt.hci.txCon].btPacket->headerLen;

                    if (bt.hci.con[bt.hci.txCon].btPacket->flags & BTP_FLAG_TAIL) {
                        bt.hci.con[bt.hci.txCon].remaining += bt.hci.con[bt.hci.txCon].btPacket->tailLen;
                    }

#if L2CAP_FLOW_CONTROL == XA_ENABLED
                    if (bt.hci.con[bt.hci.txCon].btPacket->flags & BTP_FLAG_FCS)
                        bt.hci.con[bt.hci.txCon].remaining += 2;
#endif /* L2CAP_FLOW_CONTROL == XA_ENABLED */

                    /* Insert the packet on the list of packets for this connection */

#if XA_STATISTICS == XA_ENABLED
                    if (bt.hci.con[bt.hci.txCon].conFlags & HCI_DATA_TYPE_ACL) {
                        XASTAT_PeakInc(hci_num_acl_bt_packets, 1);
                    } else {
                        XASTAT_PeakInc(hci_num_sco_bt_packets, 1);
                    }
#endif /* XA_STATISTICS == XA_ENABLED */

                    InsertTailList(&(bt.hci.con[bt.hci.txCon].btPacketList), &(cbParms.ptr.packet->node));
                }

                if (cbParms.ptr.packet) {
                    /* A packet was received, send it */
                    bt.hci.flags |= HCI_FLAG_DATA_QUEUED;
                    if (SendHciPackets(bt.hci.con[bt.hci.txCon].hciHandle) != BT_STATUS_SUCCESS) {
                        break;
                    }
                } else {
                    /* No more data to send on this connection */
                    bt.hci.con[bt.hci.txCon].conFlags &= ~HCI_DATA_REQ;
                    if (bt.hci.con[bt.hci.txCon].hciHandle & (bt.hci.flags << 12)) {
                        bt.hci.flags &= ~(HCI_FLAG_PICONET_BC | HCI_FLAG_ACTIVE_BC);
                    }
                } 
            }
        }

        /* Setup the next connection for transmit */
        bt.hci.txCon++;
        if (bt.hci.txCon >= HCI_NUM_HANDLES) {
            bt.hci.txCon = 0;
        }
    }
}

/*---------------------------------------------------------------------------
 *            HCI_Process()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Process all receive data and events.  Also, process any request
 *            to send data.
 *
 * Return:    void
 */
void HCI_Process(void)
{
    HciCallbackParms   cbParms;

#if L2CAP_NUM_GROUPS > 0
    U16 entry;
#endif /* L2CAP_NUM_GROUPS > 0 */

    if ((bt.hci.state == HCI_STATE_SHUTDOWN) ||
        (bt.hci.state == HCI_STATE_DEINIT)) {
        return;
    }
    
    /* See if there are any transport errors */
    OS_StopHardware();
    if (bt.hci.transportError) {
        OS_ResumeHardware();

        /* HCI Transport Error */
        cbParms.status = BT_STATUS_FAILED;
        if (bt.hci.state == HCI_STATE_INIT) {
            /* Report init status */
            if (bt.hci.radioEntry) {
                Report(("HCI_Process: falure\n"));
                bt.hci.radioEntry(HCI_INIT_STATUS, &cbParms); 
            } else {
                HCI_RadioInitialized(BT_STATUS_FAILED);
            }
        } else {
            /* Report the transport error */
            if (bt.hci.radioEntry) {
                bt.hci.radioEntry(HCI_TRANSPORT_ERROR, &cbParms); 
            } else {
#if BT_STACK == XA_ENABLED            
                ME_Callback(HCI_TRANSPORT_ERROR, &cbParms); 
#else
                HCI_Callback(HCI_TRANSPORT_ERROR, &cbParms); 
#endif
            }

            /* Reset the HCI */
            if (RMGR_HciReset() == BT_STATUS_FAILED) {
                HCI_RadioInitialized(BT_STATUS_FAILED);
            }
        }
        bt.hci.transportError = FALSE;
        return;
    }
    OS_ResumeHardware();

    /* See if any HCI transmit packets were returned */
    HciProcessSentPackets();

    /* See if any data has been received */
    HciProcessReceivedData();

#if HCI_HOST_FLOW_CONTROL == XA_ENABLED        
    /* Send the number of completed packets command if necessary */
    HciSendCompletedPackets();
#endif /* HCI_HOST_FLOW_CONTROL */

    /* See if any commands must be sent */
    HciSendCommands();

    /* See if any connections have data to send */
    HciCheckForTxData();

#if L2CAP_NUM_GROUPS > 0
    /* Check for a broadcast */
    if ((bt.hci.flags & HCI_FLAG_PICONET_BC) || (bt.hci.flags & HCI_FLAG_ACTIVE_BC)) {
        /* A broadcast has been requested, see if all queues are empty */
        if (!(bt.hci.flags & HCI_FLAG_DATA_QUEUED)) {
            /* No data is queued on any connection Set up the broadcast to be
             * transmitted.
             */
            if (bt.hci.flags & HCI_FLAG_PICONET_BC) {
                entry = HciGetTableEntry(bt.hci.piconetBcHandle);
                bt.hci.con[entry].conFlags |= HCI_DATA_REQ;
                Assert(entry != INVALID_HCI_HANDLE);
            } else {
                entry = HciGetTableEntry(bt.hci.activeBcHandle);
                bt.hci.con[entry].conFlags |= HCI_DATA_REQ;
                Assert(entry != INVALID_HCI_HANDLE);
            }
            OS_NotifyEvm();
        }
    }
#endif /* L2CAP_NUM_GROUPS > 0 */
}

