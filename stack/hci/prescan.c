/***************************************************************************
 *
 * File:
 *     $Workfile:prescan.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:21$
 *
 * Description:
 *     This file contains code for adding prescan modules to the HCI.
 *
 * Created:
 *     September 26, 2000
 *
 * Copyright 2000-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "btalloc.h"

#if HCI_ALLOW_PRESCAN == XA_ENABLED
#include "prescan/infineon.c"


/*---------------------------------------------------------------------------
 *            HciInitPrescan()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Used to initialize prescan modules.  During init, the prescan 
 *            module should register the prescan handler.
 *
 * Return:    void
 */
void HciInitPrescan(void)
{

    InitializeListHead(&bt.hci.psc.prescanList);

    /* Call any prescan modules that must hook the stack INIT */

}

#endif


