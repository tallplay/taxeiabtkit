/***************************************************************************
 *
 * File:
 *     $Workfile:hci_evnt.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:95$
 *
 * Description:
 *     This file contains code for sending commands and handling
 *     events from the Host Controller Interface.
 *
 * Created:
 *     July 20, 1999
 *
 * Copyright 1999-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "btalloc.h"

/* Function prototypes */                                                 
static void HciHandleCompletedPackets(U8 *parms);
static void HciHandleConnectComplete(U8 *parms);
static void HciHandleDisconnectComplete(U8 *parms);
static void HciHandleCommandComplete(U8 *parms);
static void HciHandleReadBufferComplete(U8 *parms);
#if HCI_HOST_FLOW_CONTROL == XA_ENABLED
static void HciHandleSetFctlComplete(U8 *parms);
#endif /* HCI_HOST_FLOW_CONTROL == XA_ENABLED */
static void HciHandleBuffSizeComplete(U8 *parms);
#if HCI_SCO_ENABLED && (HCI_SCO_FLOW_CONTROL == XA_ENABLED)
static void HciHandleWriteScoFcEnableComplete(U8 *parms);
#endif /* HCI_SCO_ENABLED && (HCI_SCO_FLOW_CONTROL == XA_ENABLED) */  

/*---------------------------------------------------------------------------
 *            HciProcessEvent()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Called when an event has been received from the Host Controller.
 *            Some events are processed before indicating them to the next
 *            layer.  Others are handled completely here and never passed up.
 *
 * Return:    void
 */
void HciProcessEvent(HciBuffer *hciBuffer)
{
    U8                event = hciBuffer->buffer[0];
    U8                parmLen = hciBuffer->buffer[1];
    U8               *parms = &(hciBuffer->buffer[2]);
    HciCallbackParms  cbParms;
    HciEvent          hciEvent;

    if (parmLen != (hciBuffer->len - 2)) {
        /* CSR Radio responds to HCC_HOST_NUM_COMPLETED_PACKETS with an
         * ill-formed command complete event.   They should not respond at
         * all, but to keep the stack for crashing, it will be allowed.
         */
        if (LEtoHost16(&(parms[1])) != HCC_HOST_NUM_COMPLETED_PACKETS) {
            /* Invalid length */
            HciTransportError();
        }
    }

    /* Parse the events */
    switch (event) {
    case HCE_NUM_COMPLETED_PACKETS:
        /* Process the completed packets event */
        HciHandleCompletedPackets(parms);

        /* This event is never passed up */
        return;

    case HCE_SYNC_CONNECT_COMPLETE:
    case HCE_CONNECT_COMPLETE:
        /* Process the connection completed event */
        HciHandleConnectComplete(parms);

        /* Pass the event up */
        break;

    case HCE_DISCONNECT_COMPLETE:
        /* Clean up on disconnect */
        HciHandleDisconnectComplete(parms);

        /* Pass the event up */
        break;

    case HCE_COMMAND_STATUS:
        /* Get the maximum outstanding commands value from the status event.
         * Look at the second byte.
         */
        parms++;

#if HCI_HOST_FLOW_CONTROL == XA_ENABLED
        if (LEtoHost16(&(parms[1])) == HCC_HOST_NUM_COMPLETED_PACKETS) {
            /* There should be no response to this command */
            return;
        }
#endif /* HCI_HOST_FLOW_CONTROL == XA_ENABLED */        

        /* Fall through to next case */
    case HCE_COMMAND_COMPLETE:
        if (event == HCE_COMMAND_COMPLETE) {
            Report(("HciProcessEvent: HCE_COMMAND_COMPLETE\n"));
        }
        
        /* Get the maximum outstanding commands value from the command
         * complete event (or command status event).
         */
        bt.hci.maxCommands = parms[0];

        /* If the command field, is zero, eat the event, 
         * ME didn't send it, doesn't care.
         *
         * Also, ignore a command complete for HOST_NUM_COMPLETED PACKETS.
         * CSR sends this, even though they shouldn't.
         */
        if ((LEtoHost16(&(parms[1])) == 0) ||
            (LEtoHost16(&(parms[1])) == HCC_HOST_NUM_COMPLETED_PACKETS)) {
            return;
        }

        /* Handle the command complete for init commands */
        if (bt.hci.state == HCI_STATE_INIT) {
            if (event == HCE_COMMAND_COMPLETE) {
                HciHandleCommandComplete(parms);
            } 
            if (!bt.hci.radioEntry) {
                /* No radio handler, ignore any other events */
                return;
            }
        }

        /* Pass all other events up */
        break;

    case HCE_HARDWARE_ERROR:
        //tallplay add break
        break;
    case HCE_DATA_BUFFER_OVERFLOW:
        /* Fatal error has occured in the radio */
        HciTransportError();
        break;
    }

    cbParms.status = BT_STATUS_SUCCESS;
    hciEvent.event = event;
    hciEvent.len = parmLen;

    /* Set parm pointer back to the correct place if necesary */
    if (event == HCE_COMMAND_STATUS) {
        parms--;
    }

    hciEvent.parms = parms;
    cbParms.ptr.hciEvent = &hciEvent;
    if (bt.hci.radioEntry) {
        EVM_CancelTimer(&bt.hci.retryTimer);
        bt.hci.retryTimer.func = 0;
        bt.hci.radioEntry(HCI_CONTROLLER_EVENT, &cbParms); 
    } else {
#if BT_STACK == XA_ENABLED            //tallplay
        ME_Callback(HCI_CONTROLLER_EVENT, &cbParms); 
#else
        HCI_Callback(HCI_CONTROLLER_EVENT, &cbParms); 
#endif
    }
}

/*---------------------------------------------------------------------------
 *            HciHandleCompletedPackets()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handles the completed packets event from the host controller.
 *            This event is used for flow control between the Host and the
 *            Host Controller.
 *
 * Return:    void
 */
static void HciHandleCompletedPackets(U8 *parms)
{
    U16               entry;
    U8                numHandles;
    U8               *parmTable;
    U16               packetsComplete;
    I16               i;

    /* Handle the completed packets event */
    numHandles = *(parms);
    parmTable = (U8 *)(parms + 1);

    for (i = 0; i < numHandles; i++) {
        /* Get the HCI handle and the number of completed
         * packets for that handle.
         */
        entry = HciGetTableEntry(LEtoHost16(&(parmTable[i * 4])));
        if (entry != INVALID_HCI_HANDLE) {
            packetsComplete = LEtoHost16((U8 *)(&(parmTable[i * 4])) + 2);

            if (bt.hci.con[entry].conFlags & HCI_DATA_TYPE_ACL) {
                /* Update the total number of ACL packets available. */
                bt.hci.aclTxPacketsLeft += packetsComplete;
            } 
#if HCI_SCO_ENABLED && (HCI_SCO_FLOW_CONTROL == XA_ENABLED)
            else if (bt.hci.con[entry].conFlags & HCI_DATA_TYPE_SCO) {
                /* Update the total number of SCO packets available. */
                bt.hci.scoTxPacketsLeft += packetsComplete;
            }
#endif /* HCI_SCO_ENABLED */
            else {
                Assert(0);
            }

            bt.hci.con[entry].packetsSent -= packetsComplete;
        }
    }
}

/*---------------------------------------------------------------------------
 *            HciHandleConnectComplete()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handles a connect complete event.
 *
 * Return:    void
 */
static void HciHandleConnectComplete(U8 *parms)
{
    if (bt.hci.state != HCI_STATE_READY) {
        /* Don't add connections if HCI is not initialized */
        return;
    }

    if (bt.hci.conReqs) {
        bt.hci.conReqs--;
    }

    /* Process the connect complete event */
    if (parms[0] == 0) {
        /* Connect is complete, add connection to table */
        AssertEval(HciAddConnection((HciHandle)(LEtoHost16(&(parms[1])) & 0x0FFF),
                                     parms[9]) != INVALID_HCI_HANDLE);
#if L2CAP_NUM_GROUPS > 0
            /* If this is the first connection, add the broadcast handles */
            if ((bt.hci.activeAclCons == 1) && (parms[9] == 1)) {
                /* Initialize broadcast handles */
                AssertEval(HciAddConnection(bt.hci.piconetBcHandle, 1) != 
                           INVALID_HCI_HANDLE);

                AssertEval(HciAddConnection(bt.hci.activeBcHandle, 1) !=
                           INVALID_HCI_HANDLE);
            }
#endif /* L2CAP_NUM_GROUPS > 0 */

    }
}

/*---------------------------------------------------------------------------
 *            HciHandleDisconnectComplete()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handles a disconnect.
 *
 * Return:    void
 */
static void HciHandleDisconnectComplete(U8 *parms)
{
    BtStatus  status;

    if ((bt.hci.state != HCI_STATE_READY) &&
        (bt.hci.state != HCI_STATE_DEINIT)) {
        /* Don't add connections if HCI is not initialized or
         * deinitializing (HCI_Deinit generates disconnect
         * complete events).
         */
        return;
    }

    /* Process the disconnect complete event */
    if (parms[0] == 0) {
        /* Delete the entry */
        status = HciDeleteConnection(LEtoHost16(&(parms[1])));
        if (status == BT_STATUS_FAILED) {
            /* We might have just deleted this connection just return
               the job is done! 
             */
            return;
        }

#if L2CAP_NUM_GROUPS > 0
        if (bt.hci.activeAclCons == 0) {
            /* The last link went down, delete broadcast entries */
            bt.hci.flags &= ~(HCI_FLAG_PICONET_BC | 
                            HCI_FLAG_ACTIVE_BC  | 
                            HCI_FLAG_DATA_QUEUED);
            AssertEval(HciDeleteConnection(bt.hci.piconetBcHandle) != BT_STATUS_FAILED);
            AssertEval(HciDeleteConnection(bt.hci.activeBcHandle) != BT_STATUS_FAILED);
        }
#endif /* L2CAP_NUM_GROUPS > 0 */
    }
}

/*---------------------------------------------------------------------------
 *            HciHandleCommandComplete()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handles a command complete event.
 *
 * Return:    void
 */
static void HciHandleCommandComplete(U8 *parms)
{
    Report(("HCI_EVNT:  HciHandleCommandComplete\n"));
    /* Call the appropriate handler */
    switch (LEtoHost16(&(parms[1]))) {
    case HCC_RESET:
        Report(("HCI_EVNT:  HciHandleCommandComplete: HCC_RESET\n"));
        /* No need to do anything if no error, Radio_EventHandler handles this */
        if (parms[3] != 0) {
            HciTransportError();
        }
        break;
    case HCC_READ_BUFFER_SIZE:
        Report(("HCI_EVNT:  HciHandleCommandComplete: HCC_READ_BUFFER_SIZE\n"));
        /* Handle the response to the HCC_READ_BUFFER_SIZE command */
        HciHandleReadBufferComplete(parms + 3);
        break;
#if HCI_HOST_FLOW_CONTROL == XA_ENABLED
    case HCC_SET_CTRLR_TO_HOST_FLOW_CTRL:
        Report(("HCI_EVNT:  HciHandleCommandComplete: HCC_SET_CTRLR_TO_HOST_FLOW_CTRL\n"));
        /* Handle the response to the HCC_SET_CTRLR_TO_HOST_FLOW_CTRL command */
        HciHandleSetFctlComplete(parms + 3);
        break;
#endif /* HCI_HOST_FLOW_CONTROL == XA_ENABLED */
    case HCC_HOST_BUFFER_SIZE:
        Report(("HCI_EVNT:  HciHandleCommandComplete: HCC_HOST_BUFFER_SIZE\n"));
        /* Handle the response to the HCC_HOST_BUFFER_SIZE command */
        HciHandleBuffSizeComplete(parms + 3);
        break;
#if HCI_SCO_ENABLED && (HCI_SCO_FLOW_CONTROL == XA_ENABLED)
    case HCC_WRITE_SCO_FC_ENABLE:
        Report(("HCI_EVNT:  HciHandleCommandComplete: HCC_WRITE_SCO_FC_ENABLE\n"));
        HciHandleWriteScoFcEnableComplete(parms + 3);
        break;
#endif /* HCI_SCO_ENABLED && (HCI_SCO_FLOW_CONTROL == XA_ENABLED) */
    }

    /* Ignore any other commands */
}

/*---------------------------------------------------------------------------
 *            HciHandleReadBufferComplete()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handles the response to the HCC_READ_BUFFER_SIZE command.
 *            Upon initialization, the HCI must get the size and number of
 *            ACL and SCO buffers from the host controller.  This function
 *            parses out those values and then initializes flow control
 *            with the host controller.
 *
 * Return:    void
 */
static void HciHandleReadBufferComplete(U8 *parms)
{
    if (parms[0] == 0) {
        /* Read values for the ACL and SCO buffers */
        bt.hci.aclTxPacketLen = LEtoHost16(&(parms[1]));
        bt.hci.scoTxPacketLen = (U8)parms[3];

#if HCI_USB_TX_BUFFER_SIZE > 0
        /* We are using the USB transport. */
#if XA_ERROR_CHECK == XA_ENABLED
        /* See if HCI_USB_TX_BUFFER_SIZE is optimal. */
        if (HCI_USB_TX_BUFFER_SIZE != 
            max(bt.hci.aclTxPacketLen, bt.hci.scoTxPacketLen) + 4) {
            Report(("HCI: HCI_USB_TX_BUFFER_SIZE(%d) may not be "\
                    "optimal. bt.hci.aclTxPacketLen=%d, " \
                    "bt.hci.scoTxPacketLen=%d!!!\n",
                    HCI_USB_TX_BUFFER_SIZE, bt.hci.aclTxPacketLen,
                    bt.hci.scoTxPacketLen));
        }
#endif
        if (HCI_USB_TX_BUFFER_SIZE < bt.hci.aclTxPacketLen) {
            bt.hci.aclTxPacketLen = HCI_USB_TX_BUFFER_SIZE - 4;
        }
        if (HCI_USB_TX_BUFFER_SIZE < bt.hci.scoTxPacketLen) {
            bt.hci.scoTxPacketLen = (U8)min(HCI_USB_TX_BUFFER_SIZE - 4, 0xff);
        }
#endif /* HCI_USB_TX_BUFFER_SIZE > 0 */

        bt.hci.aclTxPacketsLeft  = LEtoHost16(&(parms[4]));
        bt.hci.scoTxPacketsLeft  = LEtoHost16(&(parms[6]));

        Report(("HCI: Read Buffer Complete: aclTxPktLen=%d, " \
                "scoTxPktLen=%d, aclTxPkts=%d, scoTxPkts=%d\n",
                bt.hci.aclTxPacketLen, bt.hci.scoTxPacketLen,
                bt.hci.aclTxPacketsLeft, bt.hci.scoTxPacketsLeft));
        
#if HCI_SCO_ENABLED && (HCI_SCO_FLOW_CONTROL == XA_ENABLED)
        bt.hci.cmd.parms[0] = 1;
        HciSendCommand(HCC_WRITE_SCO_FC_ENABLE, 1, &(bt.hci.cmd), TRUE);
#else /* HCI_SCO_ENABLED && (HCI_SCO_FLOW_CONTROL == XA_ENABLED) */
        
        /* Initialize flow control */
#if HCI_HOST_FLOW_CONTROL == XA_ENABLED
        bt.hci.aclRxPacketsLeft  = HCI_NUM_ACL_BUFFERS;
#endif /* HCI_HOST_FLOW_CONTROL == XA_ENABLED */

        /* Send the Hosts' buffer sizes */
        StoreLE16(&(bt.hci.cmd.parms[0]), HCI_ACL_DATA_SIZE);
        bt.hci.cmd.parms[2] =  HCI_SCO_DATA_SIZE;
        StoreLE16(&(bt.hci.cmd.parms[3]), HCI_NUM_ACL_BUFFERS);
        StoreLE16(&(bt.hci.cmd.parms[5]), HCI_NUM_SCO_BUFFERS);
        (void)HciSendCommand(HCC_HOST_BUFFER_SIZE, 7, &(bt.hci.cmd), TRUE);
#endif /* HCI_SCO_ENABLED && (HCI_SCO_FLOW_CONTROL == XA_ENABLED) */
    } else {
        /* Init failed */
        HciTransportError();
    }
}

#if HCI_SCO_ENABLED && (HCI_SCO_FLOW_CONTROL == XA_ENABLED)
/*---------------------------------------------------------------------------
 *            HciHandleWriteScoFcEnableComplete()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handles the response to the HCC_WRITE_SCO_FLOW_CONTROL_ENABLE
 *            command.  This function determines whether the host controller
 *            will send number of completed packets events for SCO connection
 *            handles
 *
 * Return:    void
 */
static void HciHandleWriteScoFcEnableComplete(U8 *parms)
{
    if (parms[0] == 0) {

        /* Initialize flow control */
#if HCI_HOST_FLOW_CONTROL == XA_ENABLED
        bt.hci.aclRxPacketsLeft  = HCI_NUM_ACL_BUFFERS;
        bt.hci.scoRxPacketsLeft  = HCI_NUM_SCO_BUFFERS;
#endif /* HCI_HOST_FLOW_CONTROL == XA_ENABLED */

        Report(("HCI: Write SCO Flow Control Enable Complete\n"));
        /* Send the Hosts' buffer sizes */
        StoreLE16(&(bt.hci.cmd.parms[0]), HCI_ACL_DATA_SIZE);
        bt.hci.cmd.parms[2] =  HCI_SCO_DATA_SIZE;
        StoreLE16(&(bt.hci.cmd.parms[3]), HCI_NUM_ACL_BUFFERS);
        StoreLE16(&(bt.hci.cmd.parms[5]), HCI_NUM_SCO_BUFFERS);
        HciSendCommand(HCC_HOST_BUFFER_SIZE, 7, &(bt.hci.cmd), TRUE);
    } else {
        /* Init failed */
        HciTransportError();
    }
}
#endif /* HCI_SCO_ENABLED && (HCI_SCO_FLOW_CONTROL == XA_ENABLED) */

/*---------------------------------------------------------------------------
 *            HciHandleBuffSizeComplete()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handles the response to the HCC_BUFFER_SIZE command.
 *
 * Return:    void
 */
static void HciHandleBuffSizeComplete(U8 *parms)
{
#if HCI_HOST_FLOW_CONTROL == XA_ENABLED

    if (parms[0] == 0) {
        Report(("HCI: Set Buffer Size Complete\n"));
        /* Set up host controller to host flow control */
#if HCI_SCO_ENABLED && (HCI_SCO_FLOW_CONTROL == XA_ENABLED)
        bt.hci.cmd.parms[0] = 3;
#else
        bt.hci.cmd.parms[0] = 1;
#endif /* HCI_SCO_ENABLED && (HCI_SCO_FLOW_CONTROL == XA_ENABLED) */

       HciSendCommand(HCC_SET_CTRLR_TO_HOST_FLOW_CTRL, 1, &(bt.hci.cmd), TRUE);
    } else {
        /* Init failed */
        HciTransportError();
    }

#else /* HCI_HOST_FLOW_CONTROL == XA_ENABLED */
    UNUSED_PARAMETER(parms);

    /*if (bt.hci.radioEntry) { */
    EVM_CancelTimer(&bt.hci.retryTimer);

    /* Tell the HCI that initialization is complete */
    HCI_RadioInitialized(BT_STATUS_SUCCESS);
#endif /* HCI_HOST_FLOW_CONTROL == XA_ENABLED */

}

#if HCI_HOST_FLOW_CONTROL == XA_ENABLED
/*---------------------------------------------------------------------------
 *            HciHandleSetFctlComplete()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handles the response to the HCC_SET_CTRLR_TO_HOST_FLOW_CTRL 
 *            command.
 *
 * Return:    void
 */
static void HciHandleSetFctlComplete(U8 *parms)
{
    if (parms[0] == 0) {

        Report(("HCI: Set Flow Control Complete\n"));
        
        EVM_CancelTimer(&bt.hci.retryTimer);

        /* Tell the HCI that initialization is complete */
        HCI_RadioInitialized(BT_STATUS_SUCCESS);

    } else {
        /* Init failed */
        HciTransportError();
    }
}
#endif /* HCI_HOST_FLOW_CONTROL == XA_ENABLED */

