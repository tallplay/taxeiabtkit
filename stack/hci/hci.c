/***************************************************************************
 *
 * File:
 *     $Workfile:hci.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:97$
 *
 * Description:
 *     This file contains the API for the Host Controller Interface.
 *
 * Created:
 *     July 20, 1999
 *
 * Copyright 1999-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "btalloc.h"

/*---------------------------------------------------------------------------
 *            HCI_Init()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initialize the HCI queues, receive buffers, and broadcast 
 *            handles.  
 *
 * Return:    BT_STATUS_SUCCESS - HCI is initialized.
 *
 *            BT_STATUS_FAILED - failed to initialize.
 */
BtStatus HCI_Init(void)
{
    I16 i;

    OS_LockStack();

    /* Initialize the broadcast handles */
    bt.hci.piconetBcHandle = PICONET_BC_HANDLE - 2;
    bt.hci.activeBcHandle = ACTIVE_BC_HANDLE - 2;

#if HCI_ALLOW_PRESCAN == XA_ENABLED
    HciInitPrescan();
#endif /* HCI_ALLOW_PRESCAN == XA_ENABLED */

    /* Initialize HCI Command Queues */
    OS_StopHardware();
    InitializeListHead(&(bt.hci.commandList));
    InitializeListHead(&(bt.hci.deferredList));
    InitializeListHead(&(bt.hci.sentCommandList));

    /* Initialized Data Queues */
    InitializeListHead(&(bt.hci.rxBuffList));
    InitializeListHead(&(bt.hci.hciPacketPool));
    InitializeListHead(&(bt.hci.sentPacketList));
    for (i = 0; i < (HCI_NUM_PACKETS + HCI_NUM_COMMANDS); i++) { 
        InsertTailList(&(bt.hci.hciPacketPool), &(bt.hci.hciPackets[i].node));
    }
    
    /* Initialize receive buffers */
    if (RXBUFF_Init() != BT_STATUS_SUCCESS) {
        OS_ResumeHardware();
        OS_UnlockStack();
        return BT_STATUS_FAILED;
    }
    OS_ResumeHardware();

    /* Initialize Internal TX Flow State */
    bt.hci.hciPacketsLeft = HCI_NUM_PACKETS;
    bt.hci.maxCommands = 1;

    OS_UnlockStack();

    return BT_STATUS_SUCCESS;
}

/*---------------------------------------------------------------------------
 *            HciResetRetryHandler()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  After a reset command is issued, if the radio doesn't resond
 *            within 10 seconds, then an error is generated which allows
 *            the Radio Manager to retry synchronizing flow control with
 *            the Radio Module.
 *
 * Return:    void
 */
static void HciResetRetryHandler(EvmTimer *timer)
{
    UNUSED_PARAMETER(timer);

    bt.hci.retryTimer.func = 0;
    HCI_TransportError();
}

/*---------------------------------------------------------------------------
 *            HCI_Reset()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Sends the HCC_RESET command only. Called by Radio_Init.
 *
 * Return:    BT_STATUS_PENDING - HCI is initializing the radio to a known
 *                                state. The status will indicate successful 
 *                                or unsucessfull attempt.
 *                               
 */
BtStatus HCI_Reset(void)
{
    OS_LockStack();

    /* Reset the Host Controller */
    bt.hci.state = HCI_STATE_INIT;
    Report(("HCI:  Sending HCC_RESET\n"));
    (void)HciSendCommand(HCC_RESET, 0, &(bt.hci.cmd), TRUE);

    if (bt.hci.retryTimer.func != 0) {
        EVM_CancelTimer(&bt.hci.retryTimer);
    }

    bt.hci.retryTimer.func = HciResetRetryHandler;
    EVM_StartTimer(&bt.hci.retryTimer, HCI_RESET_TIMEOUT);

    OS_UnlockStack();
    return BT_STATUS_PENDING;
}
/*---------------------------------------------------------------------------
 *            HCI_Config()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initiates the sequence commands for negotiating HCI Flow 
 * Control. 
 *
 * Return:    BT_STATUS_PENDING - HCI is initializing the Host Controller.  
 *                                When the Host Controller is initialized, 
 *                                the Management Entity will be called with a 
 *                                HCI_INIT_STATUS event.  The status will 
 *                                indicate successful or unsucessful 
 *                                initialization.
 */
BtStatus HCI_Config(void)
{
    OS_LockStack();

    /* Re-Initialize Command Flow State */
    bt.hci.maxCommands = 1;

    /* Read the buffer sizes */
    (void)HciSendCommand(HCC_READ_BUFFER_SIZE, 0, &(bt.hci.cmd), TRUE);

    OS_UnlockStack();
    return BT_STATUS_PENDING;
}

/*---------------------------------------------------------------------------
 *            HCI_Deinit()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Deinitialize the HCI.  Return all resources to the upper 
 *            layers.
 *
 * Return:    void
 */
void HCI_Deinit(void)
{
    HciCallbackParms cbParms;
    HciCommand       *hciCommand;
    HciBuffer        hciBuffer;
    U8               buffer[6];
    U16              i;

    OS_LockStack();

    bt.hci.state = HCI_STATE_DEINIT;

    /* Set up disconnect complete event */
    buffer[0] = HCE_DISCONNECT_COMPLETE;
    buffer[1] = 4;
    buffer[2] = 0;
    buffer[5] = HC_STATUS_CONN_TERM_LOCAL_HOST;
    hciBuffer.buffer = buffer;
    hciBuffer.flags = HCI_BUFTYPE_EVENT;
    hciBuffer.len = 6;

    /* Return all outstanding packets */
    for (i = 0; i < HCI_NUM_HANDLES; i++) {
        if (bt.hci.con[i].conFlags & HCI_ACTIVE_CON) {

            /* Complete any packets that have not been completely sent */
            while (!IsListEmpty(&(bt.hci.con[i].btPacketList))) {
                cbParms.ptr.packet = (BtPacket *)RemoveHeadList(&(bt.hci.con[i].btPacketList));
                cbParms.hciHandle = bt.hci.con[i].hciHandle;
                cbParms.status = BT_STATUS_NO_CONNECTION;
#if BT_STACK == XA_ENABLED            
                ME_Callback(HCI_PACKET_HANDLED, &cbParms); 
#else
                HCI_Callback(HCI_PACKET_HANDLED, &cbParms); 
#endif
            }
        }
    }

#if NUM_SCO_CONNS != 0
    /* Free all active SCO connections */
    for (i = 0; i < HCI_NUM_HANDLES; i++) {
        if ((bt.hci.con[i].conFlags & HCI_ACTIVE_CON) &&
            (bt.hci.con[i].conFlags & HCI_DATA_TYPE_SCO)) {

            /* Generate the disconnect complete event */
            StoreLE16(&(buffer[3]), bt.hci.con[i].hciHandle);
            HciProcessEvent(&hciBuffer);
        }
    }
#endif /* NUM_SCO_CONNS != 0 */

    /* Free all active ACL connections */
    for (i = 0; i < HCI_NUM_HANDLES; i++) {
        if ((bt.hci.con[i].conFlags & HCI_ACTIVE_CON) &&
            (!(bt.hci.con[i].hciHandle & (PICONET_BROADCAST | 
                                        ACTIVE_BROADCAST))) &&
            (bt.hci.con[i].conFlags & HCI_DATA_TYPE_ACL)) {

            /* Generate the disconnect complete event */
            StoreLE16(&(buffer[3]), bt.hci.con[i].hciHandle);
            HciProcessEvent(&hciBuffer);
        }
    }

    /* Return any outstanding commands */
    while (!IsListEmpty(&(bt.hci.sentCommandList))) {
        hciCommand = (HciCommand *)RemoveHeadList(&bt.hci.sentCommandList);
        if (hciCommand != &bt.hci.cmd) {
            cbParms.status = BT_STATUS_NO_CONNECTION;
            cbParms.ptr.cmd = hciCommand;
#if BT_STACK == XA_ENABLED            
            ME_Callback(HCI_COMMAND_HANDLED, &cbParms);
#else
            HCI_Callback(HCI_COMMAND_HANDLED, &cbParms);
#endif
        }
    }

    while (!IsListEmpty(&(bt.hci.deferredList))) {
        hciCommand = (HciCommand *)RemoveHeadList(&bt.hci.deferredList);
        cbParms.status = BT_STATUS_NO_CONNECTION;
        cbParms.ptr.cmd = hciCommand;
#if BT_STACK == XA_ENABLED            
        ME_Callback(HCI_COMMAND_HANDLED, &cbParms);
#else
        HCI_Callback(HCI_COMMAND_HANDLED, &cbParms);
#endif
    }

    while (!IsListEmpty(&(bt.hci.commandList))) {
        hciCommand = (HciCommand *)RemoveHeadList(&bt.hci.commandList);
        if (hciCommand != &bt.hci.cmd) {
            cbParms.status = BT_STATUS_NO_CONNECTION;
            cbParms.ptr.cmd = hciCommand;
#if BT_STACK == XA_ENABLED            
            ME_Callback(HCI_COMMAND_HANDLED, &cbParms);
#else
            HCI_Callback(HCI_COMMAND_HANDLED, &cbParms);
#endif
        }
    }

    /* Throw away any received data */
    InitializeListHead(&(bt.hci.rxBuffList));

    /* Initialize Internal TX Flow State */
    bt.hci.hciPacketsLeft = HCI_NUM_PACKETS;
    bt.hci.maxCommands = 1;

    OS_UnlockStack();
}

/*---------------------------------------------------------------------------
 *            HciSendCommand()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Send a command to the Host Controller.
 *
 * Return:    BT_STATUS_PENDING
 *            BT_STATUS_INVALID_PARM
 */
BtStatus HciSendCommand(U16 opCode, U8 parmLen, HciCommand *cmd, BOOL internal)
{
    BtStatus status = BT_STATUS_PENDING;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, cmd != 0);

    OS_LockStack();

    StoreLE16(cmd->header, opCode);
    cmd->header[2] = parmLen;

    if ((bt.hci.state == HCI_STATE_INIT) && (!internal)) {
        /* Defer this command until initialization is complete */
        InsertTailList(&(bt.hci.deferredList), &(cmd->node));
    } else if ((bt.hci.state == HCI_STATE_READY) || (internal)) {
        /* Queue the command for transmission */
        InsertTailList(&(bt.hci.commandList), &(cmd->node));
        OS_NotifyEvm();
    } else {
        status = BT_STATUS_FAILED;
    }

    OS_UnlockStack();

    return status;
}

#if HCI_ALLOW_PRESCAN == XA_ENABLED
/*---------------------------------------------------------------------------
 *            HCI_RegisterPrescan()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Register a Prescan handler.
 *
 * Return:    BT_STATUS_SUCCESS
 *            BT_STATUS_INVALID_PARM
 *            BT_STATUS_IN_USE
 */
BtStatus HCI_RegisterPrescan(HciPrescanHandler *Handler, HciPrescanFlags Flags)
{
    BtStatus status = BT_STATUS_IN_USE;

    OS_LockStack();

    /* Parm check */
    CheckLockedParm(BT_STATUS_INVALID_PARM, Handler != 0);
    CheckLockedParm(BT_STATUS_INVALID_PARM, 
        (Handler->hciTxPrescan != 0) ||
        (Handler->hciTxDone != 0)    ||
        (Handler->hciRxPrescan != 0) ||
        (Handler->hciRxDone != 0));

    if (!IsNodeOnList(&bt.hci.psc.prescanList, &(Handler->node))) {
        if (!(Flags & PRESCAN_FLAG_FIRST)) {
            InsertTailList(&bt.hci.psc.prescanList, &(Handler->node));
        } else {
            InsertHeadList(&bt.hci.psc.prescanList, &(Handler->node));
        }
        status = BT_STATUS_SUCCESS;
    }

    OS_UnlockStack();
    return status;
}

/*---------------------------------------------------------------------------
 *            HCI_UnregisterPrescan()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Unregister a Prescan handler.
 *
 * Return:    BT_STATUS_SUCCESS
 *            BT_STATUS_INVALID_PARM
 *            BT_STATUS_FAILED
 */
BtStatus HCI_UnregisterPrescan(HciPrescanHandler *Handler)
{
    OS_LockStack();

    /* Parm check */
    CheckLockedParm(BT_STATUS_INVALID_PARM, Handler != 0);
    CheckLockedParm(BT_STATUS_INVALID_PARM, 
        IsNodeOnList(&bt.hci.psc.prescanList, &(Handler->node)));

    RemoveEntryList(&(Handler->node));

    OS_UnlockStack();
    return BT_STATUS_SUCCESS;
}
#endif /* HCI_ALLOW_PRESCAN == XA_ENABLED */

/*---------------------------------------------------------------------------
 *            HCI_RegisterTran()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Register the transport interface.
 *
 * Return:    BT_STATUS_SUCCESS
 *            BT_STATUS_INVALID_PARM
 */
BtStatus HCI_RegisterTransport(TranEntry *tranEntry)
{
    OS_LockStack();

    if (tranEntry != 0) {

        /* Parm check */
        CheckLockedParm(BT_STATUS_INVALID_PARM, (tranEntry->sendData != 0));

        /* Register the entry points */
        bt.hci.tranEntry.sendData = tranEntry->sendData;
        bt.hci.tranEntry.buffAvail = tranEntry->buffAvail;
        bt.hci.tranEntry.setSpeed = tranEntry->setSpeed;
    } else {
        /* Deregister the entry points */
        bt.hci.tranEntry.sendData = 0;
        bt.hci.tranEntry.buffAvail = 0;
        bt.hci.tranEntry.setSpeed = 0;
    }
    
    OS_UnlockStack();
    return BT_STATUS_SUCCESS;
}

/*---------------------------------------------------------------------------
 *            HCI_RegisterRadioHandler()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Register a radio handler for radio module initialization.
 *
 * Return:    BT_STATUS_SUCCESS
 *            BT_STATUS_FAILED
 */
void HCI_RegisterRadioHandler(RadioHandlerCallback radioHandlerCallback)
{
    OS_LockStack();

    /* Register the radio handler callback */
    bt.hci.radioEntry = radioHandlerCallback;

    OS_UnlockStack();

    return;
}

/*---------------------------------------------------------------------------
 *            QueueDeferredCommands()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Queues any deferred commands for transmission.
 *
 * Return:    void
 */
static void QueueDeferredCommands(void)
{
    HciCommand *cmd;

    while (!IsListEmpty(&bt.hci.deferredList)) {
        cmd = (HciCommand *)RemoveHeadList(&bt.hci.deferredList);
        InsertTailList(&bt.hci.commandList, &(cmd->node));
    }
}

/*---------------------------------------------------------------------------
 *            HCI_RadioInitialized()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Tells the HCI that radio initialization has completed. 
 *
 * Return:    void
 */
void HCI_RadioInitialized(BtStatus Status)
{
    HciCallbackParms  cbParms;

    OS_LockStack();

    /* Tell ME that initialization is complete */
    if (bt.hci.state == HCI_STATE_INIT) {
        if (Status == BT_STATUS_SUCCESS) {
            QueueDeferredCommands();
            bt.hci.state = HCI_STATE_READY;
        }
        cbParms.status = Status;
#if BT_STACK == XA_ENABLED            
        ME_Callback(HCI_INIT_STATUS, &cbParms); 
#else
        HCI_Callback(HCI_INIT_STATUS, &cbParms); 
#endif
    }

    OS_UnlockStack();
}

/*---------------------------------------------------------------------------
 *            HCI_RadioDeinitialized()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Tells the HCI that radio deinitialization has completed. 
 *
 * Return:    void
 */
void HCI_RadioDeinitialized(void)
{
    HciCallbackParms  cbParms;

    OS_LockStack();

    if (bt.hci.state != HCI_STATE_INIT) {
        /* Generate the deinit complete event */
        bt.hci.state = HCI_STATE_SHUTDOWN;
        cbParms.status = BT_STATUS_SUCCESS;
#if BT_STACK == XA_ENABLED            
        ME_Callback(HCI_DEINIT_STATUS, &cbParms);
#else
        HCI_Callback(HCI_DEINIT_STATUS, &cbParms);
#endif
    } else {
        bt.hci.state = HCI_STATE_SHUTDOWN;
    }

    OS_UnlockStack();
}

/*---------------------------------------------------------------------------
 *            HCI_SetSpeed()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Called by the radio handler to set the speed of the transport.
 *
 * Returns:   void
 */
void HCI_SetSpeed(U16 Speed)
{
    OS_LockStack();

    if (bt.hci.tranEntry.setSpeed != 0) {
        bt.hci.tranEntry.setSpeed(Speed);
    }

    OS_UnlockStack();
}


/*---------------------------------------------------------------------------
 *            HCI_RequestToSend()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Allows upper layers to notify HCI that it has data available to
 *            send.  When the HCI gets the thread and is able to send,
 *            it will call ME with an HCI_SEND_IND event.  Must only be called
 *            when the stack is already locked.
 *
 * Return:    BT_STATUS_FAILED - The connection handle is unknown.
 *
 *            BT_STATUS_SUCESS - The request was recognized.  ME will
 *                               be called to request data.
 */
BtStatus HCI_RequestToSend(HciHandle hciHandle)
{
    U16 entry;

    /* If this is a broadcast, convert the static broadcast handle 
     * to the dynamic one.
     */
    if (hciHandle == ACTIVE_BC_HANDLE) {
        hciHandle = bt.hci.activeBcHandle;
    } else if (hciHandle == PICONET_BC_HANDLE) {
        hciHandle = bt.hci.piconetBcHandle;
    }

    /* Search the connection table for the specified handle */
    entry = HciGetTableEntry(hciHandle);
    if (entry == INVALID_HCI_HANDLE) {
        return BT_STATUS_FAILED;
    }

#if L2CAP_NUM_GROUPS > 0
    if ((hciHandle == bt.hci.activeBcHandle) || (hciHandle == bt.hci.piconetBcHandle)) {
        /* A broadcast was requested, set a flag and wait for
         * all outstanding data to be sent before sending the 
         * broadcast.
         */
        bt.hci.flags |= (U8)(hciHandle >> 12);
    } else {
#endif /* L2CAP_NUM_GROUPS > 0 */


        /* Send was requested */
        bt.hci.con[entry].conFlags |= HCI_DATA_REQ;

#if L2CAP_NUM_GROUPS > 0
    }
#endif /* L2CAP_NUM_GROUPS > 0 */

    OS_NotifyEvm();
    return BT_STATUS_SUCCESS;
}


