/***************************************************************************
 *
 * File:
 *     $Workfile:infineon.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:21$
 *
 * Description:
 *     This file contains code for INFINEON radio support.
 *
 * Created:
 *     June 12, 2001
 *
 * Copyright 2000-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "btalloc.h"

#define PSC(a) (HCC(psc).a)

#if HCI_ALLOW_PRESCAN == XA_ENABLED
#if PRESCAN_INFINEON_FIXES == XA_ENABLED

/*---------------------------------------------------------------------------
 *            INF_TxHandler()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Transmit prescan handler for INFINEON support.  
 *
 * Return:    TRUE - the buffer was handled by the presan.
 *            FALSE - the buffer was not handled by the prescan.
 */
BOOL INF_TxHandler(HciPacket *Packet)
{
    if (Packet->flags & HCI_BUFTYPE_COMMAND) {
        /* An HCI command is being sent */
        if (LEtoHost16(Packet->header) == HCC_ACCEPT_SYNC_CON_REQ) {
            /* Accept Sync Con Request.  Set a flag to look for command
             * status event with HCC_ACCEPT_CON_REQ command type.
             */
            PSC(acceptSyncSent) = TRUE;
        }
    }

    return FALSE;
}

/*---------------------------------------------------------------------------
 *            INF_RxHandler()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Receive prescan handler for INFINEON support.  
 *
 * Return:    TRUE - the buffer was handled by the presan.
 *            FALSE - the buffer was not handled by the prescan.
 */
BOOL INF_RxHandler(HciBuffer *Buffer) 
{
    if (Buffer->flags & HCI_BUFTYPE_EVENT) {
        if (Buffer->buffer[0] == HCE_COMMAND_STATUS) {
          if (LEtoHost16(Buffer->buffer + 4) == HCC_ACCEPT_CON_REQ) {
            /* Fixup the command type */
            if (PSC(acceptSyncSent)) {
                StoreLE16(Buffer->buffer + 4, HCC_ACCEPT_SYNC_CON_REQ);
                PSC(acceptSyncSent) = FALSE;
            }
          }
        } else if ((Buffer->buffer[0] == HCE_COMMAND_COMPLETE) &&
            (LEtoHost16(Buffer->buffer + 3) == HCC_READ_LOCAL_VERSION)) {
            /* Fixup the HCI version */
            if (Buffer->buffer[9] == 2) {
                Buffer->buffer[6] = 2;
            }
        }
    }

    return FALSE;
}

/*---------------------------------------------------------------------------
 *            INFINEON_Init()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initialized prescan support for INFINEON radios.  
 *
 * Return:    void
 */
BOOL INFINEON_Init(void)
{
    PSC(infHandler).hciTxPrescan = INF_TxHandler;
    PSC(infHandler).hciTxDone = 0;
    PSC(infHandler).hciRxPrescan = INF_RxHandler;
    PSC(infHandler).hciRxDone = 0;
    HCI_RegisterPrescan(&PSC(infHandler), PRESCAN_FLAG_FIRST);

    return TRUE;
}

#endif /* PRESCAN_INFINEON_FIXES == XA_ENABLED */
#endif /* HCI_ALLOW_PRESCAN == XA_ENABLED */

