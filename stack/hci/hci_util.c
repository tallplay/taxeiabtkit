/***************************************************************************
 *
 * File:
 *     $Workfile:hci_util.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:48$
 *
 * Description:
 *     This file contains utility functions for the Host Controller 
 *     Interface.
 *
 * Created:
 *     September 20, 1999
 *
 * Copyright 1999-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "btalloc.h"

/*---------------------------------------------------------------------------
 *            HciGetTableEntry()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Convert a Host Controller handle to an HCI handle
 *
 * Return:    A valid HCI handle or INVALID_HCI_HANDLE.
 */
U16 HciGetTableEntry(HciHandle hciHandle)
{
    U16 i;

    /* Search the connection table for the Host Handle and
     * return the table entry.
     */
    for (i = 0; i < HCI_NUM_HANDLES; i++) {
        if (bt.hci.con[i].conFlags & HCI_ACTIVE_CON) {
            if ((hciHandle & 0x0FFF) == (bt.hci.con[i].hciHandle & 0x0FFF)) {
                return(i);
            }
        }
    }
    return INVALID_HCI_HANDLE;
}

/*---------------------------------------------------------------------------
 *            HciSetNewBcHandle()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Searches for a new (unused) handle to use for broadcasts.
 *
 * Return:    void
 */
void HciSetNewBcHandle(HciHandle *hciHandle)
{
    HciHandle handle = *hciHandle & 0x0FFF;
    BOOL found = TRUE;

    Assert(handle != INVALID_HCI_HANDLE);

    while (found) {
        /* Try a different handle */
        handle--;
        if (handle > 0x0EFF) {
            handle = 0x0EFF;
        }

        if (HciGetTableEntry(handle) == INVALID_HCI_HANDLE) {
            found = FALSE;
        } 
    }

    /* Assign the new handle */
    *hciHandle = handle & (*hciHandle & 0xF000);
}

/*---------------------------------------------------------------------------
 *            HciAddConnection()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Add an entry to the connection table.
 *
 * Return:   
 */
U16 HciAddConnection(HciHandle hciHandle, U8 linkType)
{
    U16 i;

    /* If the handle is the same as either of broadcast handles, 
     * a new broadcast handle must be chosen.
     */
    if (hciHandle == bt.hci.piconetBcHandle) {
        i = HciGetTableEntry(hciHandle);
        if (i != INVALID_HCI_HANDLE) {
            HciSetNewBcHandle(&bt.hci.piconetBcHandle);
            bt.hci.con[i].hciHandle = bt.hci.piconetBcHandle;
        }
    } else if (hciHandle == bt.hci.activeBcHandle) {
        i = HciGetTableEntry(hciHandle);
        if (i != INVALID_HCI_HANDLE) {
            HciSetNewBcHandle(&bt.hci.activeBcHandle);
            bt.hci.con[i].hciHandle = bt.hci.activeBcHandle;
        }
    }

    /* Add the entry */
    if (HciGetTableEntry(hciHandle) == INVALID_HCI_HANDLE) {
        /* Search for an open entry */
        for (i = 0; i < HCI_NUM_HANDLES; i++) {
            if (!(bt.hci.con[i].conFlags & HCI_ACTIVE_CON)) {
                bt.hci.con[i].hciHandle = hciHandle;
                InitializeListHead(&(bt.hci.con[i].btPacketList));
                bt.hci.con[i].conFlags = HCI_ACTIVE_CON;

                if (linkType == 1) {
                    /* ACL data */
#if L2CAP_NUM_GROUPS > 0
                    if (!(bt.hci.con[i].hciHandle & 
                          (PICONET_BROADCAST | ACTIVE_BROADCAST))) {
                        /* Not a broadcast handle */
#endif /* L2CAP_NUM_GROUPS > 0 */

                        bt.hci.activeAclCons++;

#if L2CAP_NUM_GROUPS > 0
                    }
#endif /* L2CAP_NUM_GROUPS > 0 */

                    bt.hci.con[i].conFlags |= HCI_DATA_TYPE_ACL;
                }

#if NUM_SCO_CONNS != 0
                else if ((linkType == 0) || (linkType == 2)) {
                    /* SCO data */
                    bt.hci.con[i].conFlags |= HCI_DATA_TYPE_SCO;
                }
#endif /* NUM_SCO_CONNS */

#if XA_DEBUG == XA_ENABLED
                else {
                    Assert(0);
                }
#endif /* XA_DEBUG == XA_ENABLED */

                return i;
            }
        }
    }
    return INVALID_HCI_HANDLE;
}

/*---------------------------------------------------------------------------
 *            HciDeleteConnection()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Delete an entry to the connection table.
 *
 * Return:    
 */
BtStatus HciDeleteConnection(HciHandle hciHandle)
{
    HciCallbackParms  cbParms;
    U16               i;

    i = HciGetTableEntry(hciHandle);

    if (i != INVALID_HCI_HANDLE) {

        if (bt.hci.con[i].conFlags & HCI_DATA_TYPE_ACL) {
            /* ACL data */

#if L2CAP_NUM_GROUPS > 0
            if (!(bt.hci.con[i].hciHandle & 
                  (PICONET_BROADCAST | ACTIVE_BROADCAST))) {
                /* Not a broadcast handle */
#endif /* L2CAP_NUM_GROUPS > 0 */

                bt.hci.activeAclCons--;

#if L2CAP_NUM_GROUPS > 0
            }
#endif /* L2CAP_NUM_GROUPS > 0 */

            bt.hci.aclTxPacketsLeft += bt.hci.con[i].packetsSent;
        }

#if NUM_SCO_CONNS != 0
        else if (bt.hci.con[i].conFlags & HCI_DATA_TYPE_SCO) {
            /* SCO data */
            bt.hci.scoTxPacketsLeft += bt.hci.con[i].packetsSent;
        }
#endif /* NUM_SCO_CONNS != 0 */

#if XA_DEBUG == XA_ENABLED
        else {
            Assert(0);
        }
#endif /* XA_DEBUG == XA_ENABLED */

        /* Complete any packets that have not been completely sent */
        while (!IsListEmpty(&(bt.hci.con[i].btPacketList))) {
            cbParms.hciHandle = bt.hci.con[i].hciHandle;
            cbParms.status = BT_STATUS_NO_CONNECTION;
            cbParms.ptr.packet = (BtPacket *)RemoveHeadList(&(bt.hci.con[i].btPacketList));
#if BT_STACK == XA_ENABLED            
            ME_Callback(HCI_PACKET_HANDLED, &cbParms); 
#else
            HCI_Callback(HCI_PACKET_HANDLED, &cbParms); 
#endif
        }

#if HCI_HOST_FLOW_CONTROL == XA_ENABLED

        if (bt.hci.con[i].conFlags & HCI_DATA_TYPE_ACL) {
            bt.hci.aclRxPacketsLeft += bt.hci.con[i].packetsComplete;
            Assert(bt.hci.aclRxPacketsLeft <= HCI_NUM_ACL_BUFFERS);

        }
#if HCI_SCO_ENABLED && (HCI_SCO_FLOW_CONTROL == XA_ENABLED)
        else if (bt.hci.con[i].conFlags & HCI_DATA_TYPE_SCO) {
            bt.hci.scoRxPacketsLeft += bt.hci.con[i].packetsComplete;
            Assert(bt.hci.scoRxPacketsLeft <= HCI_NUM_SCO_BUFFERS);
        }
#endif /* HCI_SCO_ENABLED && (HCI_SCO_FLOW_CONTROL == XA_ENABLED) */

        bt.hci.con[i].packetsComplete = 0;

#endif /* HCI_HOST_FLOW_CONTROL == XA_ENABLED */

        /* Reset the connection state */
        bt.hci.con[i].conFlags = 0;
        bt.hci.con[i].packetsSent = 0;
        bt.hci.con[i].btPacket = 0;
        bt.hci.con[i].buffPtr = 0;
        bt.hci.con[i].remaining = 0;
    } else {
        return BT_STATUS_FAILED;
    }
    return BT_STATUS_SUCCESS;
}

