/***************************************************************************
 *
 * File:
 *     $Workfile:medev.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:24$
 *
 * Description:
 *     This is file contains the functionality for the Bluetooth Device 
 *     Selection manager. This API provides a simplified set of functions
 *     for managing non-connected devices known to the local device.
 *
 * Copyright 1999-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "btalloc.h"
#include "ddb.h"

#if NUM_KNOWN_DEVICES > 0

#define AllocEntryList(_freelist, _structtype) \
            (_structtype *)(!IsListEmpty(_freelist) ? RemoveHeadList(_freelist) : 0)

#define FreeEntryList(_freelist, _node) \
            InsertTailList(_freelist, _node)

/****************************************************************************
 *
 * Functions
 *
 ****************************************************************************/

/*---------------------------------------------------------------------------
 *            DS_AddDevice()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Add device to known devices list, if device already exists
 *            failure is returned.
 *
 * Return:    BtStatus
 */
BtStatus DS_AddDevice(const BtDeviceContext *Template, BtDeviceContext **NewDevice)
{   
    BtStatus         status;
    BtDeviceContext *newDevice;
    BtEvent          event;
#if BT_SECURITY == XA_ENABLED
    BtDeviceRecord   rec;
#endif
    OS_LockStack();

    CheckParmExit(BT_STATUS_INVALID_PARM, (Template != 0));

    if (DS_FindDevice(&Template->addr)) {
        /* Device already exists on list. */
        status = BT_STATUS_FAILED;
        goto exit;
    }

    /* Didn't find an existing match, allocate a new device context. */
    if ((newDevice = AllocEntryList(&(bt.me.btDeviceFreeList), BtDeviceContext)) == 0) {
        status = BT_STATUS_NO_RESOURCES;
        goto exit;
    }

    /* Allocated a new entry, copy in template and add new device to the list. */
    *newDevice = *Template;
    InsertHeadList(&(bt.me.deviceList), &newDevice->node);

    newDevice->link = 0;
    InitializeListEntry(&newDevice->active);

#if BT_SECURITY == XA_ENABLED
    if (SEC_FindDeviceRecord(&Template->addr, &rec) == BT_STATUS_SUCCESS) {
        newDevice->state = BDS_PAIRED;
        if (rec.trusted)
            newDevice->state |= BDS_TRUSTED;
    }
#endif

    /* Notify the Blue Manager Monitor */
    if ((bt.me.dsMonitor)) { 
        event.eType = BTEVENT_DEVICE_ADDED;
        event.handler = (bt.me.dsMonitor);
        event.p.device = newDevice;
        (bt.me.dsMonitor)->callback(&event);
    }
    status = BT_STATUS_SUCCESS;

    if (NewDevice)
        *NewDevice = newDevice;
exit:
    OS_UnlockStack();
    return status;
}

/*---------------------------------------------------------------------------
 *            DS_DeleteDevice()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Removes device from known devices list. This function will
 *            not remove devices from in the device security database.
 *
 * Return:    BtStatus
 */
BtStatus DS_DeleteDevice(const BD_ADDR *Addr)
{
    BtDeviceContext *bdc;
    BtStatus         status;
    BtEvent          event;

    OS_LockStack();

    CheckParmExit(BT_STATUS_INVALID_PARM, (Addr != 0));

    status = BT_STATUS_NOT_FOUND;

    if (0 != (bdc = DS_FindDevice(Addr))) {
        /* Found matching device, remove it. */
        if ((bt.me.dsMonitor)) { 
            event.eType = BTEVENT_DEVICE_DELETED;
            event.handler = (bt.me.dsMonitor);
            event.p.device = bdc;
            (bt.me.dsMonitor)->callback(&event);
        }
        RemoveEntryList(&bdc->node);
        FreeEntryList(&(bt.me.btDeviceFreeList), &bdc->node);
        status = BT_STATUS_SUCCESS;
    }

Check_Parm_Exit
    OS_UnlockStack();
    return status;
}


/*---------------------------------------------------------------------------
 *            DS_EnumDeviceList()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Enumerates all devices on the known device list.
 *
 * Return:    BtStatus
 */
BtStatus DS_EnumDeviceList(BtDeviceContext **LastDevice)
{
    BtStatus    status = BT_STATUS_SUCCESS;

    OS_LockStack();

    CheckParmExit(BT_STATUS_INVALID_PARM, LastDevice != 0);
    
    /* "LastDevice" points to the last entry returned or zero on first call. */
    if (*LastDevice == 0) {
        /* First call initialize "LastDevice" to first item on list. */
        *LastDevice = (BtDeviceContext *)GetHeadList(&(bt.me.deviceList));
    } else {
        /* Subsequent calls, make sure "LastDevice" is still on list. */
        if (!IsNodeOnList(&(bt.me.deviceList), &(*LastDevice)->node)) {
            status = BT_STATUS_FAILED;
            goto exit;
        }

        /* Update "LastDevice" to point to next item on list. */
        *LastDevice = (BtDeviceContext *)GetNextNode(&(*LastDevice)->node);
    }

    /* "LastDevice" now points to the current entry being returned. */
    if (*(ListEntry **)LastDevice == &(bt.me.deviceList)) {
        /* Done walking list. Zero out the device ptr for safety. */
        *LastDevice = 0;
        status = BT_STATUS_NOT_FOUND;
    }

exit:
    OS_UnlockStack();
    return status;
}


/*---------------------------------------------------------------------------
 *            DS_FindDevice()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Find the device in the device list with a matching Bluetooth 
 *            address.
 *
 * Return:    BtDeviceContext
 */
BtDeviceContext *DS_FindDevice(const BD_ADDR *Addr)
{   
    ListEntry   *node;

    OS_LockStack();

    if (Addr) {
        node = GetHeadList(&(bt.me.deviceList));
        while (node != &(bt.me.deviceList)) {
            if (OS_MemCmp(((BtDeviceContext *)node)->addr.addr, 6, Addr->addr, 6) == TRUE) {
                goto exit;
            }
            node = GetNextNode(node);
        }
    }
    node = 0;

exit:
    OS_UnlockStack();
    return (BtDeviceContext *)node;
}


/*---------------------------------------------------------------------------
 *            DS_EnumDevicesByQuality()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Enumerates all device on the known device list in sorted order
 *            based on the device quality setting. The device which best
 *            matches the device quality is returned first, followed by the
 *            next best match, and ending with the least matching device.
 *
 * Return:    BtStatus
 */
BtStatus DS_EnumDevicesByQuality(BtDeviceContext **LastDevice, 
                                 const BtDeviceQuality *Qual)
{
    BtDeviceContext *bdc, *keep;
    U8               points, high, i;
    ListEntry        activeList;
    BtStatus         status = 0;

    OS_LockStack();

    CheckParmExit(BT_STATUS_INVALID_PARM, (LastDevice != 0) && (Qual != 0));
    CheckParmExit(BT_STATUS_FAILED, (*LastDevice ? IsNodeOnList(&(bt.me.deviceList), &(*LastDevice)->node): 1));

    InitializeListHead(&activeList);

    while (TRUE) {
        high = 0;
        keep = bdc = 0;

        while (DS_EnumDeviceList(&bdc) == BT_STATUS_SUCCESS) {
            points = 6;     /* No matches receives 0 points */

            if (Qual->mask & BDQM_SERVICE_CLASS) {
                /* Score service class higher then device class. */
                if (bdc->classOfDevice & Qual->serviceClass)
                    points += 2;
                else points--;
            }

            if (Qual->mask & BDQM_DEVICE_CLASS) {
                if ((bdc->classOfDevice & COD_MAJOR_MASK) == (Qual->deviceClass & COD_MAJOR_MASK))
                    points++;
                else points--;
            }

#if DS_NUM_SERVICES > 0
            if (Qual->mask & BDQM_SERVICE_UUID) {
                /* Score Service UUID highest because it indicates that an
                 * SDP Query would very likely match.
                 */
                points--;   /* Assume no match */

                for (i = 0; i < DS_NUM_SERVICES; i++) {
                    if (bdc->services[i] == Qual->serviceUuid) {
                        points += 5;
                        break;
                    }
                }
            }
#endif /* DS_NUM_SERVICES > 0 */

            if (Qual->mask & BDQM_DEVICE_STATUS) {
                /* Can earn up to 3 points here. */
                for (i = 0; i < 3; i++) {
                    if (Qual->status & (1 << i)) {
                        if (bdc->state & (1 << i))
                            points++;
                        else points--;
                    }
                }
            }
                    
            if ((points > high) && !IsNodeOnList(&activeList, &bdc->active)) {
                high = points;
                keep = bdc;
            }
        }

        if (keep == 0) {
            /* Done walking list. Zero out the device ptr for extra safety. */
            *LastDevice = 0;
            status = BT_STATUS_NOT_FOUND;
            break;
        }

        if ((*LastDevice == 0) ||
            (GetTailList(&activeList) == &(*LastDevice)->active)) {
            /* First call, initialize "LastDevice" to first item on list. */
            /* Subsequent calls, "LastDevice" points to next item on sorted list. */
            *LastDevice = keep;
            status = BT_STATUS_SUCCESS;
            break;
        }

        InsertTailList(&activeList, &keep->active);
    }

Check_Parm_Exit
    OS_UnlockStack();
    return status;
}


/*---------------------------------------------------------------------------
 *            DS_SelectDevice()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Select a device from the known device list which best matches
 *            the device quality criteria. If registered, the device selection
 *            monitor is responsible for choosing the final result.
 *
 * Return:    BtStatus
 */
BtStatus DS_SelectDevice(BtSelectDeviceToken *token)
{
    BtStatus    status;
    BtEvent     event;

    OS_LockStack();

    CheckParmExit(BT_STATUS_INVALID_PARM, (token != 0) && (token->callback != 0));

    token->result = 0;

    /* Load automated choice into token result. */
    status = DS_EnumDevicesByQuality(&token->result, &token->quality);
    if ((status != BT_STATUS_SUCCESS) && (status != BT_STATUS_NOT_FOUND)) {
        goto exit;
    }

    if ((bt.me.dsMonitor)) {
        token->state = 1;

        Report(("MeDev: Calling select device handler.\n"));
        event.eType = BTEVENT_SELECT_DEVICE_REQ;
        event.handler = (bt.me.dsMonitor);
        event.p.select = token;
        (bt.me.dsMonitor)->callback(&event);
        token->state++;
    } else {
        /* No DS Monitor to query so final answer is known. */
        token->state = 3;
    }

    if (token->state == 3) {
        if (token->result)
            status = BT_STATUS_SUCCESS;
        else status = BT_STATUS_NOT_FOUND;
    }
    else status = BT_STATUS_PENDING;

exit:
    OS_UnlockStack();
    return status;
}

/*---------------------------------------------------------------------------
 *            DS_SelectDeviceResult()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  This function is called by the device selection monitor to
 *            complete a BTEVENT_SELECT_DEVICE_REQ event. The monitor uses
 *            this function to provide the results of the select device
 *            request back to the caller. This function is only used by the
 *            monitor handler.
 *
 * Return:    BtStatus
 */
BtStatus DS_SelectDeviceResult(BtSelectDeviceToken *token, BtDeviceContext *device, 
                               BtErrorCode reason)
{
    BtStatus    status = BT_STATUS_SUCCESS;
    BtEvent     event;

    OS_LockStack();

    CheckParmExit(BT_STATUS_INVALID_PARM, (token != 0) && (token->state < 3));
    CheckParmExit(BT_STATUS_INVALID_PARM, (device ? IsNodeOnList(&(bt.me.deviceList), &device->node):1));

    /* If moniter returns "no error" and no device, preserve the default. */
    if (device == 0) {
        if ((reason == BEC_NO_ERROR) && (token->result == 0))
            reason = BEC_NOT_FOUND;
        else device = token->result;
    }
    token->result = device;

    if (++token->state == 3) {
        /* This function is responsible for completing the request. */
        event.eType = BTEVENT_DEVICE_SELECTED;
        event.errCode = reason;
        event.p.select = token;
        token->callback(&event);
    }

Check_Parm_Exit
    OS_UnlockStack();
    return status;
}


/*---------------------------------------------------------------------------
 *            DS_RegisterMonitorHandler()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Register a handler for selecting the device to establish a
 *            connection to.
 *
 * Return:    Pointer to old handler. 
 */
BtHandler *DS_RegisterMonitorHandler(BtHandler *handler)
{
    BtHandler *tmpHandler;

    Assert(handler ? handler->callback != 0 : 1);

    /* Lock the stack */
    OS_LockStack();

    tmpHandler = (bt.me.dsMonitor);
    (bt.me.dsMonitor) = handler;

    /* Unlock the stack */
    OS_UnlockStack();
    return tmpHandler;
}


/*---------------------------------------------------------------------------
 *            MeDevInquiryStart()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Internal function to mark all devices out of range when an
 *            inquiry starts.
 */
void MeDevInquiryStart(void)
{
    BtDeviceContext *bdc = 0;

    /* Clear "In Range" status for all devices. */
    while (DS_EnumDeviceList(&bdc) == BT_STATUS_SUCCESS) {
        if (bdc->link == 0)
            bdc->state &= ~BDS_IN_RANGE;
    }
}

/*---------------------------------------------------------------------------
 *            MeDevEventHandler()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  An internal function which monitors all ME Events before they
 *            are passed to the registered handlers.
 */
void MeDevEventHandler(void)
{
    BtDeviceContext     device, *currDevice;
#if BT_SECURITY == XA_ENABLED
    BtDeviceRecord      rec;
    I8                  i;
#endif

    switch ((bt.me.btEvent).eType) {
    case BTEVENT_LINK_CONNECT_CNF:
    case BTEVENT_LINK_CONNECT_IND:
        if ((bt.me.btEvent).errCode == 0) {
            /* Make sure the device is in the device selector database. */
            if ((currDevice = DS_FindDevice(&(bt.me.btEvent).p.remDev->bdAddr)) == 0) {
                OS_MemSet((U8 *)&device, 0, (U32)sizeof(BtDeviceContext));
                device.addr = (bt.me.btEvent).p.remDev->bdAddr;
                device.classOfDevice = (bt.me.btEvent).p.remDev->cod;
                device.state = BDS_IN_RANGE;

                if (DS_AddDevice(&device, &currDevice) != BT_STATUS_SUCCESS)
                    break;
            }
            /* Link the connection to the device context. */
            currDevice->link = (bt.me.btEvent).p.remDev;
        }
        break;

    case BTEVENT_PIN_REQ:
    case BTEVENT_LINK_CONNECT_REQ:
        /* Make sure the device is in the device selector database. */
        if ((currDevice = DS_FindDevice(&(bt.me.btEvent).p.remDev->bdAddr)) == 0) {
            OS_MemSet((U8 *)&device, 0, (U32)sizeof(BtDeviceContext));
            device.addr = (bt.me.btEvent).p.remDev->bdAddr;
            device.classOfDevice = (bt.me.btEvent).p.remDev->cod;
            device.state = BDS_IN_RANGE;

            (void)DS_AddDevice(&device, 0);
        }
        break;

    case BTEVENT_LINK_DISCONNECT:
        /* Unlink the connection from the device context. */
        if (0 != (currDevice = DS_FindDevice(&(bt.me.btEvent).p.remDev->bdAddr))) {
            currDevice->link = 0;
        }
        break;

    case BTEVENT_INQUIRY_RESULT:
        /* If device is not in list yet, add it now. */
        if ((currDevice = DS_FindDevice(&(bt.me.btEvent).p.inqResult.bdAddr)) == 0) {
            OS_MemSet((U8 *)&device, 0, (U32)sizeof(BtDeviceContext));
            device.addr = (bt.me.btEvent).p.inqResult.bdAddr;

            if (DS_AddDevice(&device, &currDevice) != BT_STATUS_SUCCESS) {
                break;
            }
        }

        /* Mark device as in range, always update PSI & CoD */
        currDevice->state |= BDS_IN_RANGE;
        currDevice->classOfDevice = (bt.me.btEvent).p.inqResult.classOfDevice;
        currDevice->psi = (bt.me.btEvent).p.inqResult.psi;
        break;

    case BTEVENT_HCI_INITIALIZED:
#if BT_SECURITY == XA_ENABLED
        /* Add devices from the security database to the active database */
        OS_MemSet((U8 *)&device, 0, (U32)sizeof(BtDeviceContext));
        for (i = 0; SEC_EnumDeviceRecords(i, &rec) == BT_STATUS_SUCCESS; i++) {
            device.addr = rec.bdAddr;
            (void)DS_AddDevice(&device, 0);
        }
#endif
        /* Drop into next case. */
    case BTEVENT_HCI_FAILED:
    case BTEVENT_HCI_DEINITIALIZED:
    case BTEVENT_HCI_FATAL_ERROR:
    case BTEVENT_HCI_INIT_ERROR:
        /* Deliver HCI status events to the DS Monitor. But only if
         * the monitor is not also a registered global handler. 
         */
        if ((bt.me.dsMonitor) && !IsNodeOnList(&(bt.me.regList), &(bt.me.dsMonitor)->node)) {
            (bt.me.btEvent).handler = (bt.me.dsMonitor);
            (bt.me.dsMonitor)->callback(&(bt.me.btEvent));
        }
        break;
    }
}


#if SDP_CLIENT_SUPPORT == XA_ENABLED
/*---------------------------------------------------------------------------
 *            MeDevSdpServiceFound()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  An internal function which extracts the Service UUID from a
 *            successful SDP Service Query and adds it to the device context.
 */
void MeDevSdpServiceFound(const SdpQueryToken *SdpToken)
{
#if DS_NUM_SERVICES > 0
    SdpServiceClassUuid service = 0;
    const U8           *sdpQuery;
    BtDeviceContext    *device;
    U32                 i, j;
    U16                 offset;

    if ((SdpToken->parms == 0) || (SdpToken->rm == 0) ||
        (SdpToken->type != BSQT_SERVICE_SEARCH_ATTRIB_REQ)) {
        return;
    }

    if ((device = DS_FindDevice(&SdpToken->rm->bdAddr)) == 0) {
        return;
    }

    /* Pull the service class Id from the Service query */
    if ((SdpToken->parms[0] & DETD_SEQ) != DETD_SEQ) {
        /* We only process queries that are sequences of attributes. */
        return;
    }
    
    i = SDP_ParseDataElement(SdpToken->parms, &offset);
    sdpQuery = SdpToken->parms + offset;
    
    while (i > 0) {
        /* Looking for a 16-bit Service Class UUID */
        if ((j = SDP_ParseDataElement(sdpQuery, &offset)) == 2) {
            Assert(j + offset <= i);

            if ((sdpQuery[0] & DETD_MASK) == DETD_UUID) {
                if ((service = BEtoHost16(sdpQuery+offset)) >= 0x1100)
                    break;
            }
        }
        sdpQuery += (j + offset);
        i -= (j + offset);
    }

    if (service < 0x1100) {
        /* Interesting Service Class values are greater-than 0x1100. */
        return;
    }
            
    /* Add Service Class UUID to known services list for device. */ 
    for (i = j = DS_NUM_SERVICES; i > 0; i--) {
        if (device->services[i-1] == service)
            break;
        
        if (device->services[i-1] == 0)
            j = i;
    }

    if ((i == 0) && (j != DS_NUM_SERVICES)) {
        device->services[j-1] = service;
    }
#endif
}
#endif /* SDP_CLIENT_SUPPORT == XA_ENABLED */

#else

/*
 * Avoid compiler error of empty file. Calls itself to avoid to avoid
 * unreferenced static function warning.
 */
static void dummy()
{
    dummy();
}

#endif /* NUM_KNOWN_DEVICES > 0 */
