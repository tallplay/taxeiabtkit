/***************************************************************************
 *
 * File:
 *     $Workfile:mescoif.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:61$
 *
 * Description:   This is file contains the code for the Bluetooth 
 *                Management Entity interface. These are the functions
 *                called by applications and ME clients to manage and
 *                control SCO data links.
 *
 * Created:       May 19, 2000
 *
 * Copyright 2000-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "btalloc.h"
#include "ddb.h"
#if NUM_SCO_CONNS > 0
#include "mesco.h"


/****************************************************************************
 *
 * Functions
 *
 ****************************************************************************/

/*---------------------------------------------------------------------------
 * Creates an SCO (voice) connection to the remote device. An  ACL link must
 * already be established
 */
BtStatus SCO_CreateLink(BtScoConnect **scocon, BtRemoteDevice *remDevice, 
                        BtScoPacketType pt)          
{
    BtStatus        status;
    BtScoConnect   *scon;
    BtScoTxParms    scoTxParms;
#if (XA_ERROR_CHECK == XA_ENABLED) || (XA_DEBUG == XA_ENABLED)
    I16 same_cnt, different_cnt, i;
#endif

    OS_LockStack();

    CheckLockedParm( BT_STATUS_INVALID_PARM,
                     remDevice && remDevice->scoHandler && scocon &&
                     (pt != 0) );

#if (XA_ERROR_CHECK == XA_ENABLED) || (XA_DEBUG == XA_ENABLED)

    /* How many SCO connections already exist for the remote device? */
    same_cnt = 0;
    different_cnt = 0;
    /* Go through the ScoConnect array 
       count up whether the remDev's are the same.
     */
    for (i = 0; i < NUM_SCO_CONNS; i++) {
        scon = (BtScoConnect *) &(bt.me.scoTable[i]);
        if (scon->scostate == BDS_CONNECTED) {
            if (remDevice == scon->scord) {
                same_cnt++;
            }
            else {
                different_cnt++;
            }
        }
    }

    /* The following conditions indicate that there are no more SCO
     * resources available:
     *    * 3 SCO connections already connected
     *    * 2 SCO links already connected from different devices
     *    * 1 connection to this device, and one to another
     */
    CheckLockedParm( BT_STATUS_NO_RESOURCES,
                     (same_cnt < 3) &&
                     (different_cnt <= 1) &&
                     !((different_cnt == 1) && (same_cnt == 1)) );
    
#endif /* XA_ERROR_CHECK or XA_DEBUG */
    
    /* See if we can start a SCO connection.
     * First we need to get a free BtScoConnect structure
     */
    scon = ScoGetFreeConnect();
    if (scon == 0) {
        status = BT_STATUS_NO_RESOURCES;
        goto done;
    }

    /* Does this radio support Add_Sco_Connection ? */
    if (((bt.me.btVersion) >= 2) && ((bt.me.btFeatures[3]) & 0x80)) {
        /* No, send it to SetupLink with standard TX parms */
        scoTxParms.transmitBandwidth = 0x1f40;      /* 8000 */
        scoTxParms.receiveBandwidth = 0x1f40;       /* 8000 */
        scoTxParms.maxLatency = 0x0007;             /* 7ms */
        scoTxParms.voiceSetting = (bt.me.vSettings);
        scoTxParms.retransmissionEffort = 0x00;     /* No Retries */
        scoTxParms.eScoPktType = 0;
        if ((bt.me.btFeatures[1]) &0x08) scoTxParms.eScoPktType |= BESPT_HV1;
        if ((bt.me.btFeatures[1]) &0x10) scoTxParms.eScoPktType |= BESPT_HV2;
        if ((bt.me.btFeatures[1]) &0x20) scoTxParms.eScoPktType |= BESPT_HV3;

        /* Release the BtScoConnect structure */
        scon->scostate = BDS_DISCONNECTED;

        /* Call SetupLink */
        status = SCO_SetupLink(scocon, remDevice, &scoTxParms);
        goto done;
    }

    scon->scostate = BDS_OUT_CON;

    scon->scoop.opType = BOP_SCO_CONNECT;
    scon->scord = remDevice;
    scon->scopt = pt;
    scon->scoLinkType = BLT_SCO;
    *scocon = scon;

    Assert(IsEntryAvailable(&scon->scoop.node));

    /* We can't use MeAddOperation() here because we don't want to use
     * remDev */
    InsertTailList(&(bt.me.opList), &scon->scoop.node);

    /* Attempt to start the operation */
    MeStartOperation();
    status = BT_STATUS_PENDING;

done:
    OS_UnlockStack();
    return status;
}

/*---------------------------------------------------------------------------
 * Create or modify an eSCO connection.
 */
BtStatus SCO_SetupLink(BtScoConnect **scocon,
                       BtRemoteDevice *remDevice,
                       BtScoTxParms *scoTxParms)
{
    BtStatus        status;
    BtScoConnect   *scon;
    U8              j;
#if (XA_ERROR_CHECK == XA_ENABLED) || (XA_DEBUG == XA_ENABLED)
    I16 same_cnt, different_cnt, i;
#endif

    OS_LockStack();

    CheckLockedParm( BT_STATUS_INVALID_PARM,
                     remDevice && remDevice->scoHandler && scocon );

    /* See if the eSCO connection already exists. */
    for (j = 0; j < NUM_SCO_CONNS; j++) {
        if (((*scocon) == &((bt.me.scoTable)[j])) &&
            ((*scocon)->scostate != BDS_DISCONNECTED)) {
            if ((*scocon)->scostate != BDS_CONNECTED) {
                status = BT_STATUS_IN_PROGRESS;
                goto done;
            }
            if ((*scocon)->scoLinkType != BLT_ESCO) {
                status = BT_STATUS_INVALID_PARM;
                goto done;
            }
            /* We have found a device in the correct state */
            break;
        }
    }

#if (XA_ERROR_CHECK == XA_ENABLED) || (XA_DEBUG == XA_ENABLED)
    /* Do the check only for new connections. */
    if (j >= NUM_SCO_CONNS) {
        /* How many SCO connections already exist for the remote device? */
        same_cnt = 0;
        different_cnt = 0;
        /* Go through the ScoConnect array 
           count up whether the remDev's are the same.
         */
        for (i = 0; i < NUM_SCO_CONNS; i++) {
            scon = (BtScoConnect *) &(bt.me.scoTable[i]);
            if (scon->scostate == BDS_CONNECTED) {
                if (remDevice == scon->scord) {
                    same_cnt++;
                }
                else {
                    different_cnt++;
                }
            }
        }

        /* The following conditions indicate that there are no more SCO
         * resources available:
         *    * 3 SCO connections already connected
         *    * 2 SCO links already connected from different devices
         *    * 1 connection to this device, and one to another
         */
        CheckLockedParm( BT_STATUS_NO_RESOURCES,
                         (same_cnt < 3) &&
                         (different_cnt <= 1) &&
                         !((different_cnt == 1) && (same_cnt == 1)) );
    }
    
#endif /* XA_ERROR_CHECK or XA_DEBUG */
    
    /* See if we need to start a new eSCO connection. */
    if (j >= NUM_SCO_CONNS) {
         /* First we need to get a free BtScoConnect structure */
        scon = ScoGetFreeConnect();
        if (scon == 0) {
            status = BT_STATUS_NO_RESOURCES;
            goto done;
        }

        scon->scostate = BDS_OUT_CON;
        *scocon = scon;
    }
    else {
        scon = *scocon;
    }

    scon->scoop.opType = BOP_SCO_SETUP;
    scon->scord = remDevice;
    scon->scoLinkType = BLT_ESCO;
    scon->scoTxParms.transmitBandwidth = scoTxParms->transmitBandwidth;
    scon->scoTxParms.receiveBandwidth = scoTxParms->receiveBandwidth;
    scon->scoTxParms.maxLatency = scoTxParms->maxLatency;
    scon->scoTxParms.voiceSetting = scoTxParms->voiceSetting;
    scon->scoTxParms.retransmissionEffort = scoTxParms->retransmissionEffort;
    scon->scoTxParms.eScoPktType = scoTxParms->eScoPktType;

    Assert(IsEntryAvailable(&scon->scoop.node));

    /* We can't use MeAddOperation() here because we don't want to use
     * remDev */
    InsertTailList(&(bt.me.opList), &((*scocon)->scoop.node));

    /* Attempt to start the operation */
    MeStartOperation();
    status = BT_STATUS_PENDING;

done:
    OS_UnlockStack();
    return status;
}

/*---------------------------------------------------------------------------
 * Disconnect an SCO link.
 */
BtStatus SCO_DisconnectLink(BtScoConnect *scocon)
{
    CheckUnlockedParm(BT_STATUS_INVALID_PARM,
                      scocon);
    OS_LockStack();

    if (IsNodeOnList(&(bt.me.opList), &(scocon->scoop.node))) {
        if (scocon->scostate == BDS_OUT_DISC) {
            OS_UnlockStack();
            return BT_STATUS_IN_PROGRESS;
        }
    } else {
        if ((scocon->scostate == BDS_OUT_CON) ||
            (scocon->scostate == BDS_OUT_CON2) ||
            (scocon->scostate == BDS_IN_CON)) {
            OS_UnlockStack();

            /* Connection operation is in progress so we need to wait until it
             * is finished
             */
            return BT_STATUS_BUSY;
        }
    }

    if (scocon->scostate == BDS_CONNECTED) {
        if (scocon->scoop.opType == BOP_NOP) {
            /* We're connected so we need to disconnect */
            scocon->scostate = BDS_OUT_DISC;
            /* No operation is in progress so put the disconnect operation 
             * on the queue.
             */
            scocon->scoop.opType = BOP_SCO_DISCONNECT;

            Assert(!IsNodeOnList(&(bt.me.opList),&(scocon->scoop.node)));
            InsertTailList(&(bt.me.opList), &scocon->scoop.node);

            /* Attempt to start the operation */
            MeStartOperation();

            OS_UnlockStack();
            return BT_STATUS_PENDING;
        }
        return BT_STATUS_BUSY;
    }

    OS_UnlockStack();
    return BT_STATUS_SUCCESS;
}

/*---------------------------------------------------------------------------
 * Sends a command to the HCI to set the SCO audio settings
 */
BtStatus SCO_SetSettings(MeCommandToken *token) 
{
    BtStatus status;

    CheckUnlockedParm(BT_STATUS_INVALID_PARM, token && token->callback);
    
    OS_LockStack();

    /* Make sure the stack does not have an HCI initialization error */
    if ((bt.me.stackState) >= BTSS_INITIALIZE_ERR) {
        status = BT_STATUS_HCI_INIT_ERR;
    } else {
        token->op.opType = BOP_SCO_VOICESET;
        token->eType = BTEVENT_SCO_VSET_COMPLETE;
        token->flags = 0;
        MeProcessToken(token);

        status = BT_STATUS_PENDING;
    }

    OS_UnlockStack();
    return status;
}


#if BT_SCO_HCI_DATA == XA_ENABLED
/*---------------------------------------------------------------------------
 * Sends the given data on the SCO link
 */
BtStatus SCO_SendData(BtScoConnect *scocon, BtPacket *packet) 
{
    BtStatus status;

    CheckUnlockedParm( BT_STATUS_INVALID_PARM,
                       scocon && packet &&
                       ((packet->flags & ~BTP_FLAG_INUSE) == 0) );

    Report(("SCO: SendData(%04x) Sent %d byte Data Packet.\n", 
                        scocon->scoHciHandle, packet->dataLen));

    packet->headerLen = 0;

    OS_LockStack();
    status = Sco_Send(scocon, packet);
    OS_UnlockStack();
    return status;
}
#endif  /* BT_SCO_HCI_DATA == XA_ENABLED */


/*---------------------------------------------------------------------------
 * Registers the given SCO handler for the given remote device. One
 * handler can be registered for any one device. There can be up
 * to three SCO links for one device or two SCO links for different
 * devices.
 */
BtStatus SCO_RegisterHandler(BtRemoteDevice *remDev, BtHandler *handler)
{
    CheckUnlockedParm( BT_STATUS_INVALID_PARM,
                       handler &&
                       handler->callback  &&
                       remDev );
    
    OS_LockStack();

    if (remDev->scoHandler == 0) {
        remDev->scoHandler = handler;

        OS_UnlockStack();
        return BT_STATUS_SUCCESS;
    }

    OS_UnlockStack();
    return BT_STATUS_BUSY;
}


/*---------------------------------------------------------------------------
 *            SCO_UnregisterHandler()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Unregister the given SCO handler for the given remote device.
 *
 * Return:    BT_STATUS_INVALID_PARM
 *            BT_STATUS_SUCCESS
 */
BtStatus SCO_UnregisterHandler(BtRemoteDevice *remDev, BtHandler *handler)
{
    /* Lock the stack */
    OS_LockStack();

    CheckLockedParm( BT_STATUS_INVALID_PARM,
                     handler && handler->callback &&
                     remDev && remDev->scoHandler &&
                     (remDev->scoHandler == handler));
    
    remDev->scoHandler = 0;

    OS_UnlockStack();
    return BT_STATUS_SUCCESS;
}


/*---------------------------------------------------------------------------
 * Accepts the given SCO connect from the remote device.
 */
BtStatus SCO_AcceptIncomingConnection(BtScoConnect *scoCon, BtHandler *handler)
{
    OS_LockStack();

    CheckLockedParm( BT_STATUS_INVALID_PARM,
                     scoCon &&
                     (scoCon->scord->scoHandler->callback ==
                      handler->callback));
    
    Assert(IsEntryAvailable(&scoCon->scoop.node));

    scoCon->scoop.opType = BOP_SCO_ACCEPT; 
    InsertTailList(&(bt.me.opList), &scoCon->scoop.node);

    /* Attempt to start the operation */
    MeStartOperation();

    OS_UnlockStack();
    return BT_STATUS_PENDING;
}


/*---------------------------------------------------------------------------
 * Rejects the incoming SCO connection.
 */
BtStatus SCO_RejectIncomingConnection(
                BtScoConnect *scoCon, BtHandler *handler, BtErrorCode reason)
{
    OS_LockStack();

    CheckLockedParm( BT_STATUS_INVALID_PARM,
                     scoCon &&
                     (scoCon->scord->scoHandler->callback ==
                      handler->callback));
    scoCon->scoRejectReason = reason;
    scoCon->scoop.opType = BOP_SCO_REJECT; 

    Assert(IsEntryAvailable(&scoCon->scoop.node));

    /* We can't use MeAddOperation() here because we don't want to
     * use remDev
     */
    InsertTailList(&(bt.me.opList), &scoCon->scoop.node);

    /* Attempt to start the operation */
    MeStartOperation();

    OS_UnlockStack();
    return BT_STATUS_PENDING;
}
#else

/*
 * Avoid compiler error of empty file. Calls itself to avoid to avoid
 * unreferenced static function warning.
 */
static void dummy(void)
{
    dummy();
}

#endif /* NUM_SCO_CONNS */

