/***************************************************************************
 *
 * File:
 *     $Workfile:meif.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:127$
 *
 * Description:
 *     This is file contains the code for the Bluetooth 
 *     Management Entity interface. These are the functions
 *     called by applications and ME clients.
 *
 * Created:
 *     October 18, 1999
 *
 * Copyright 1999-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "btalloc.h"
#include "ddb.h"

/****************************************************************************
 *
 * Functions
 *
 ****************************************************************************/

/*---------------------------------------------------------------------------
 *            ME_RegisterGlobalHandler()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Register a handler with the Management Enity
 *
 * Return:    BT_STATUS_SUCCESS - if successful.
 *
 *            BT_STATUS_FAILED - (error check only) if handler not
 *            initialized or callback set to 0;
 */
BtStatus ME_RegisterGlobalHandler(BtHandler *handler)
{
#if XA_ERROR_CHECK == XA_ENABLED
    if ((handler == 0) || !IsEntryAvailable(&(handler->node)) || 
        (handler->callback == 0)) {
        return (BT_STATUS_FAILED);
    }

#endif /* XA_ERROR_CHECK */

    Assert(handler != 0);
    Assert(IsEntryAvailable(&(handler->node)));
    Assert(handler->callback != 0);
    
    /* Lock the stack */
    OS_LockStack();

    /* Add handler to the reg list */
    InsertTailList(&((bt.me.regList)), &(handler->node));

    /* Unlock the stack */
    OS_UnlockStack();

    /* Initialize fields of the handler */
    return BT_STATUS_SUCCESS;

}

/*---------------------------------------------------------------------------
 *            ME_UnregisterGlobalHandler()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Unregister a handler previously registered with the Management 
 *            Enity
 *
 * Return:    BT_STATUS_SUCCESS - if successful.
 *
 *            BT_STATUS_FAILED - (error check only) if handler not
 *            registered or 0.
 */
BtStatus ME_UnregisterGlobalHandler(BtHandler *handler)
{
    BtStatus status;

    /* Lock the stack */
    OS_LockStack();

#if XA_ERROR_CHECK == XA_ENABLED
    /* If the handler is 0 or not registered then fail. */
    if ((handler == 0) || (!IsNodeOnList(&((bt.me.regList)), &(handler->node)))) {
        status = BT_STATUS_FAILED;
    }
#endif /* XA_ERROR_CHECK */

    Assert(handler != 0);
    Assert(IsNodeOnList(&((bt.me.regList)), &(handler->node)));

    RemoveEntryList(&(handler->node));
    status = BT_STATUS_SUCCESS;

    /* Unlock the stack */
    OS_UnlockStack();
    return status;

}


#if XA_ERROR_CHECK == XA_ENABLED
/*---------------------------------------------------------------------------
 *            ME_SetEventMask()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Set the event mask for the handler.
 *
 * Return:    BT_STATUS_SUCCESS - if successful.
 *
 *            BT_STATUS_FAILED - (error check only) if handler not
 *            registered or 0.
 */
BtStatus ME_SetEventMask(BtHandler *handler, BtEventMask mask)
{
    BtStatus status;

    /* Lock the stack */
    OS_LockStack();

    /* If the handler is 0 or not registered then fail. */
    if ((handler == 0) || (!IsNodeOnList(&((bt.me.regList)), &(handler->node)))) {
        status = BT_STATUS_FAILED;
        goto done;
    }

    Assert(handler != 0);
    Assert(IsNodeOnList(&((bt.me.regList)), &(handler->node)));

    handler->eMask = mask;
    status = BT_STATUS_SUCCESS;

done:
    /* Unlock the stack */
    OS_UnlockStack();
    return status;

}
#endif /* XA_ERROR_CHECK */

/*---------------------------------------------------------------------------
 *            ME_Inquiry()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Start an Inquiry process.
 *
 * Return:    status of the operation. 
 */
BtStatus ME_Inquiry(BtIac lap, U8 len, U8 maxResp)
{
    BtStatus status;

    /* Lock the stack */
    OS_LockStack();

    /* Make sure the stack does not have an HCI initialization error */
    if ((bt.me.stackState) >= BTSS_INITIALIZE_ERR) {
        status = BT_STATUS_HCI_INIT_ERR;
        goto done;
    }

    /* If an Inquiry operation is already in progress then fail */
    if ((bt.me.opMask) & (BOM_INQUIRY | BOM_CANCEL_INQUIRY)) {
        status = BT_STATUS_IN_PROGRESS;
        goto done;
    }

    /* Put the Inquiry request onto the operation queue */
    Assert(IsEntryAvailable(&((bt.me.inquiryOp).op.node)));

    (bt.me.opMask) |= BOM_INQUIRY;
    (bt.me.inquiryOp).op.opType = BOP_INQUIRY;
    (bt.me.inquiryOp).lap = lap;
    (bt.me.inquiryOp).len = len;
    (bt.me.inquiryOp).max = maxResp;

    MeAddOperation(&(bt.me.inquiryOp));

    /* Attempt to start the operation */
    MeStartOperation();
    status = BT_STATUS_PENDING;

done:
    /* Unlock the stack */
    OS_UnlockStack();

    return status;
}


/*---------------------------------------------------------------------------
 *            ME_CancelInquiry()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Cancel an Inquiry process.
 *
 * Return:    status of the operation 
 */
BtStatus ME_CancelInquiry(void)
{
    BtStatus status;

    /* Lock the stack */
    OS_LockStack();

    /* Make sure the stack does not have an HCI initialization error */
    if ((bt.me.stackState) >= BTSS_INITIALIZE_ERR) {
        status = BT_STATUS_HCI_INIT_ERR;
        goto done;
    }

    /* See if we are already performing a cancel operation */
    if ((bt.me.opMask) & BOM_CANCEL_INQUIRY) {
        status = BT_STATUS_IN_PROGRESS;
        goto done;
    }

    if ((bt.me.opMask) & BOM_INQUIRY) {
        /* An inquiry operation is progress so we can cancel it. First see
         * if it is on the list
         */
        if (IsEntryAvailable(&((bt.me.inquiryOp).op.node))) {
            /* Entry is not on the list so start a cancel operation */
            (bt.me.opMask) |= BOM_CANCEL_INQUIRY;
            (bt.me.inquiryOp).op.opType = BOP_CANCEL_INQUIRY;
            MeAddOperation(&(bt.me.inquiryOp));

            /* Attempt to start the operation */
            MeStartOperation();
            status = BT_STATUS_PENDING;
        } else {
            /* Entry is on the list so remove it and report results */
            RemoveEntryList(&((bt.me.inquiryOp).op.node));
            (bt.me.btEvent).errCode = 0;
            MeReportInqCancelComplete();
            status = BT_STATUS_SUCCESS;
        }
    } else {
        /* No inquiry operation in progress so operation failed */
        status = BT_STATUS_FAILED;
    }

done:
    /* Unlock the stack */
    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            ME_SetAccessibleModeNC()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Set the accessability mode used when not connected.
 *
 * Return:    status of the operation 
 */
BtStatus ME_SetAccessibleModeNC(BtAccessibleMode mode, const BtAccessModeInfo *info)
{
    BtStatus status;
    BtAccessModeInfo info1;

    /* Setup info1 to contain the info */
    if (info == 0) {
        info1.inqInterval = BT_DEFAULT_SCAN_INTERVAL;
        info1.inqWindow = BT_DEFAULT_SCAN_WINDOW;
        info1.pageInterval = BT_DEFAULT_SCAN_INTERVAL;
        info1.pageWindow = BT_DEFAULT_SCAN_WINDOW;
    } else {
        info1 = *info;

        /* Check the values of info1 */
#if XA_ERROR_CHECK == XA_ENABLED
        status = BT_STATUS_INVALID_PARM;

        /* Verify if the caller is changing the interval or window that the
         * radio allows it.
         */
        if (((BT_DEFAULT_INQ_SCAN_INTERVAL == 0) && 
             (info1.inqInterval != BT_DEFAULT_SCAN_INTERVAL)) ||
            ((BT_DEFAULT_INQ_SCAN_WINDOW == 0) && 
             (info1.inqWindow != BT_DEFAULT_SCAN_WINDOW)) ||
            ((BT_DEFAULT_PAGE_SCAN_INTERVAL == 0) && 
             (info1.pageInterval != BT_DEFAULT_SCAN_INTERVAL)) ||
             ((BT_DEFAULT_PAGE_SCAN_WINDOW == 0) && 
             (info1.pageWindow != BT_DEFAULT_SCAN_WINDOW))) {
            goto done;
        }

        if ((MeIsScanValsLegal(info1.inqInterval, info1.inqWindow) == FALSE) ||
            (MeIsScanValsLegal(info1.pageInterval, info1.pageWindow) == FALSE)) {
            goto done;
        }

#endif
        Assert(((BT_DEFAULT_INQ_SCAN_INTERVAL == 0) && 
                (info1.inqInterval == BT_DEFAULT_SCAN_INTERVAL)) ||
               (BT_DEFAULT_INQ_SCAN_INTERVAL != 0));

        Assert(((BT_DEFAULT_INQ_SCAN_WINDOW == 0) && 
                (info1.inqWindow == BT_DEFAULT_SCAN_WINDOW)) ||
               (BT_DEFAULT_INQ_SCAN_INTERVAL != 0));

        Assert(((BT_DEFAULT_PAGE_SCAN_INTERVAL == 0) && 
                (info1.pageInterval == BT_DEFAULT_SCAN_INTERVAL)) ||
               (BT_DEFAULT_INQ_SCAN_INTERVAL != 0));

        Assert(((BT_DEFAULT_PAGE_SCAN_WINDOW == 0) && 
                (info1.pageWindow == BT_DEFAULT_SCAN_WINDOW)) ||
               (BT_DEFAULT_PAGE_SCAN_WINDOW != 0));

        Assert((MeIsScanValsLegal(info1.inqInterval, info1.inqWindow) == TRUE) &&
               (MeIsScanValsLegal(info1.pageInterval, info1.pageWindow) == TRUE));
    }

    CheckUnlockedParm(BT_STATUS_INVALID_PARM,
                      (mode == BAM_NOT_ACCESSIBLE) ||
                      (mode == BAM_GENERAL_ACCESSIBLE) ||
                      (mode == BAM_LIMITED_ACCESSIBLE) ||
                      (mode == BAM_CONNECTABLE_ONLY) ||
                      (mode == BAM_DISCOVERABLE_ONLY));

    /* Lock the stack */
    OS_LockStack();

    /* Make sure the stack does not have an HCI initialization error */
    if ((bt.me.stackState) >= BTSS_INITIALIZE_ERR) {
        status = BT_STATUS_HCI_INIT_ERR;
        goto unlock;
    }

    /* Set the COD Limited Discoverable bit appropriately */
    switch(mode) {
    case BAM_LIMITED_ACCESSIBLE:
        (bt.me.sdpServCoD) |= COD_LIMITED_DISC_MASK;
        break;
    case BAM_GENERAL_ACCESSIBLE:
        (bt.me.sdpServCoD) &= ~COD_LIMITED_DISC_MASK;
        break;
    }

    /* If the values are the same as the current values then return success */
    if ((mode == (bt.me.accModeNC)) && 
        MeIsAccessModeInfoEqual(&info1, &(bt.me.accInfoNC))) {
        status = BT_STATUS_SUCCESS;
        goto unlock;
    }

    /* If there are any connections up then save the information and 
     * return BT_STATUS_SUCCESS.
     */
    if ((bt.me.activeCons) > 0) {
        /* Connections are active so copy the info and return success */
        (bt.me.accModeNC) = mode;
        (bt.me.accInfoNC) = info1;
        status = BT_STATUS_SUCCESS;
        goto unlock;
    }

    /* If an access mode change operation is already in progress then fail  */
    if ((bt.me.opMask) & (BOM_ACCESS_MODE | BOM_ACCESS_MODE_AUTO)) {
        status = BT_STATUS_IN_PROGRESS;
        goto unlock;
    }

    /* Start up an operation */
    (bt.me.opMask) |= BOM_ACCESS_MODE;
    (bt.me.accOp).nc = TRUE;
    (bt.me.accOp).i = info1;
    (bt.me.accOp).mode = mode;

    Assert(IsEntryAvailable(&((bt.me.accOp).op.node)));
    MeAddOperation(&(bt.me.accOp));

    /* Attempt to start the operation */
    MeStartOperation();
    status = BT_STATUS_PENDING;

unlock:
    /* Unlock the stack */
    OS_UnlockStack();

#if XA_ERROR_CHECK == XA_ENABLED
done:
#endif

    return status;
}


/*---------------------------------------------------------------------------
 *            ME_GetAccessibleModeNC()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Get the accessibility mode used when not connected.
 *
 * Return:    status of the operation 
 */
BtStatus ME_GetAccessibleModeNC(BtAccessibleMode *mode, BtAccessModeInfo *info)
{
    BtStatus status;

    /* Lock the stack */
    OS_LockStack();

    /* If there are no connections then we need to check if an access mode 
     * operation is in progress
     */ 
    if ((bt.me.activeCons) == 0) {
        /* No Connections are active so see if an operation is progress */
        if ((bt.me.opMask) & BOM_ACCESS_MODE) {
            /* An operation is in progress so return an error */
            status = BT_STATUS_IN_PROGRESS;
            goto unlock;
        }
    }
    /* We can give back the results */
    if (mode != 0) {
        *mode = (bt.me.accModeNC);
    }

    if (info != 0) {
        *info = (bt.me.accInfoNC);
    }
    status = BT_STATUS_SUCCESS;

unlock:
    /* Unlock the stack */
    OS_UnlockStack();
    return status;
}

/*---------------------------------------------------------------------------
 *            ME_GetCurAccessibleMode()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Get the current accessibility mode.
 *
 * Return:    status of the operation 
 */
BtStatus ME_GetCurAccessibleMode(BtAccessibleMode *mode, BtAccessModeInfo *info)
{
    /* Lock the stack */
    OS_LockStack();

    /* We can give back the results */
    if (mode != 0) {
        *mode = (bt.me.accModeCur);
    }

    if (info != 0) {
        *info = (bt.me.accInfoCur);
    }

    /* Unlock the stack */
    OS_UnlockStack();
    return BT_STATUS_SUCCESS;
}

#if BT_ALLOW_SCAN_WHILE_CON == XA_ENABLED
/*---------------------------------------------------------------------------
 *            ME_SetAccessibleModeC()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Set the accessability mode used when connected.
 *
 * Return:    status of the operation 
 */
BtStatus ME_SetAccessibleModeC(BtAccessibleMode mode, const BtAccessModeInfo *info)
{
    BtStatus status;
    BtAccessModeInfo info1;

    /* Setup info1 to contain the info */
    if (info == 0) {
        info1.inqInterval = BT_DEFAULT_SCAN_INTERVAL;
        info1.inqWindow = BT_DEFAULT_SCAN_WINDOW;
        info1.pageInterval = BT_DEFAULT_SCAN_INTERVAL;
        info1.pageWindow = BT_DEFAULT_SCAN_WINDOW;
    } else {
        info1 = *info;

        /* Check the values of info1 */
#if XA_ERROR_CHECK == XA_ENABLED
        status = BT_STATUS_INVALID_PARM;

        /* Verify if the caller is changing the interval or window that the
         * radio allows it.
         */
        if (((BT_DEFAULT_INQ_SCAN_INTERVAL == 0) && 
             (info1.inqInterval != BT_DEFAULT_SCAN_INTERVAL)) ||
            ((BT_DEFAULT_INQ_SCAN_WINDOW == 0) && 
             (info1.inqWindow != BT_DEFAULT_SCAN_WINDOW)) ||
            ((BT_DEFAULT_PAGE_SCAN_INTERVAL == 0) && 
             (info1.pageInterval != BT_DEFAULT_SCAN_INTERVAL)) ||
             ((BT_DEFAULT_PAGE_SCAN_WINDOW == 0) && 
             (info1.pageWindow != BT_DEFAULT_SCAN_WINDOW))) {
            goto done;
        }

        if ((MeIsScanValsLegal(info1.inqInterval, info1.inqWindow) == FALSE) ||
            (MeIsScanValsLegal(info1.pageInterval, info1.pageWindow) == FALSE)) {
            goto done;
        }

#endif
        Assert(((BT_DEFAULT_INQ_SCAN_INTERVAL == 0) && 
                (info1.inqInterval == BT_DEFAULT_SCAN_INTERVAL)) ||
               (BT_DEFAULT_INQ_SCAN_INTERVAL != 0));

        Assert(((BT_DEFAULT_INQ_SCAN_WINDOW == 0) && 
                (info1.inqWindow == BT_DEFAULT_SCAN_WINDOW)) ||
               (BT_DEFAULT_INQ_SCAN_INTERVAL != 0));

        Assert(((BT_DEFAULT_PAGE_SCAN_INTERVAL == 0) && 
                (info1.pageInterval == BT_DEFAULT_SCAN_INTERVAL)) ||
               (BT_DEFAULT_INQ_SCAN_INTERVAL != 0));

        Assert(((BT_DEFAULT_PAGE_SCAN_WINDOW == 0) && 
                (info1.pageWindow == BT_DEFAULT_SCAN_WINDOW)) ||
               (BT_DEFAULT_PAGE_SCAN_WINDOW != 0));

        Assert((MeIsScanValsLegal(info1.inqInterval, info1.inqWindow) == TRUE) &&
               (MeIsScanValsLegal(info1.pageInterval, info1.pageWindow) == TRUE));

    }

    CheckUnlockedParm(BT_STATUS_INVALID_PARM,
                      (mode == BAM_NOT_ACCESSIBLE) ||
                      (mode == BAM_GENERAL_ACCESSIBLE) ||
                      (mode == BAM_LIMITED_ACCESSIBLE) ||
                      (mode == BAM_CONNECTABLE_ONLY) ||
                      (mode == BAM_DISCOVERABLE_ONLY));

    /* Lock the stack */
    OS_LockStack();

    /* Make sure the stack does not have an HCI initialization error */
    if ((bt.me.stackState) >= BTSS_INITIALIZE_ERR) {
        status = BT_STATUS_HCI_INIT_ERR;
        goto unlock;
    }

    /* Set the COD Limited Discoverable bit appropriately */
    switch(mode) {
    case BAM_LIMITED_ACCESSIBLE:
        (bt.me.sdpServCoD) |= COD_LIMITED_DISC_MASK;
        break;
    case BAM_GENERAL_ACCESSIBLE:
        (bt.me.sdpServCoD) &= ~COD_LIMITED_DISC_MASK;
        break;
    }

    /* If the values are the same as the current values then return success */
    if ((mode == (bt.me.accModeC)) && 
        MeIsAccessModeInfoEqual(&info1, &(bt.me.accInfoC))) {
        status = BT_STATUS_SUCCESS;
        goto unlock;
    }

    /* If there are no active connections then save the information and return 
     * BT_STATUS_SUCCESS
     */
    if ((bt.me.activeCons) == 0) {
        /* No Connections are active so copy the info and return success */
        (bt.me.accModeC) = mode;
        (bt.me.accInfoC) = info1;
        status = BT_STATUS_SUCCESS;
        goto unlock;
    }
    /* If an access mode change operation is already in progress then fail  */
    if ((bt.me.opMask) & (BOM_ACCESS_MODE | BOM_ACCESS_MODE_AUTO)) {
        status = BT_STATUS_IN_PROGRESS;
        goto unlock;
    }

    /* Start up an operation */
    (bt.me.opMask) |= BOM_ACCESS_MODE;
    (bt.me.accOp).nc = FALSE;
    (bt.me.accOp).i = info1;
    (bt.me.accOp).mode = mode;

    Assert(IsEntryAvailable(&((bt.me.accOp).op.node)));
    MeAddOperation(&(bt.me.accOp));

    /* Attempt to start the operation */
    MeStartOperation();
    status = BT_STATUS_PENDING;

unlock:
    /* Unlock the stack */
    OS_UnlockStack();

#if XA_ERROR_CHECK == XA_ENABLED
done:
#endif

    return status;
}


/*---------------------------------------------------------------------------
 *            ME_GetAccessibleModeC()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Get the accessibility mode used when connected.
 *
 * Return:    status of the operation 
 */
BtStatus ME_GetAccessibleModeC(BtAccessibleMode *mode, BtAccessModeInfo *info)
{
    BtStatus status;

    /* Lock the stack */
    OS_LockStack();

    /* If there are active connections then we need to check if an access 
     * mode operation is in progress
     */ 
    if ((bt.me.activeCons) > 0) {
        /* Active Connections exist so see if an operation is progress */
        if ((bt.me.opMask) & BOM_ACCESS_MODE) {
            /* An operation is in progress so return an error */
            status = BT_STATUS_IN_PROGRESS;
            goto unlock;
        }
    }
    /* We can give back the results */
    if (mode != 0) {
        *mode = (bt.me.accModeC);
    }

    if (info != 0) {
        *info = (bt.me.accInfoC);
    }
    status = BT_STATUS_SUCCESS;

unlock:
    /* Unlock the stack */
    OS_UnlockStack();
    return status;
}

#endif /* BT_ALLOW_SCAN_WHILE_CON */

/*---------------------------------------------------------------------------
 *            ME_GetRemoteDeviceName()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Get the remote device name. 
 *
 * Return:    status of the operation.
 */
BtStatus ME_GetRemoteDeviceName(MeCommandToken *token)
{
    BtStatus status;

    /* Lock the stack */
    OS_LockStack();

#if XA_ERROR_CHECK == XA_ENABLED
    status = BT_STATUS_FAILED;
    if (token == 0) {
        goto done;
    }

    if ((token->callback == 0) || IsNodeOnList(&(bt.me.opList), &(token->op.node)) ||
        IsNodeOnList(&(bt.me.cmdList), &(token->op.node))) {
        goto done;
    }
#endif /* XA_ERROR_CHECK */

    Assert(token != 0);
    Assert((token->callback != 0) && 
           !IsNodeOnList(&(bt.me.opList), &(token->op.node)) &&
           !IsNodeOnList(&(bt.me.cmdList), &(token->op.node)));

    /* Make sure the stack does not have an HCI initialization error */
    if ((bt.me.stackState) >= BTSS_INITIALIZE_ERR) {
        status = BT_STATUS_HCI_INIT_ERR;
    } else {
        token->op.opType = BOP_REM_NAME_REQ;
        token->eType = BTEVENT_NAME_RESULT;
        token->flags = MCTF_NEED_CHECK;
        token->remDev = ME_FindRemoteDevice(&token->p.name.bdAddr);
        MeProcessToken(token);
        status = BT_STATUS_PENDING;
    }

#if XA_ERROR_CHECK == XA_ENABLED
done:
#endif
    /* Unlock the stack */
    OS_UnlockStack();
    return status;
}


/*---------------------------------------------------------------------------
 *            ME_CancelGetRemoteDeviceName()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Request cancelation of a pending ME_GetRemoteDeviceName.
 *
 * Return:    status of the operation. 
 */
BtStatus ME_CancelGetRemoteDeviceName(MeCommandToken *token)
{
    BtStatus status;

    /* Lock the stack */
    OS_LockStack();

    if (IsNodeOnList(&(bt.me.opList), &(token->op.node)) &&
        (token->op.opType == BOP_REM_NAME_REQ)) {
        /* GetRemoteName hasn't started. Just remove it from the list
         * and return success.
         */
        RemoveEntryList(&token->op.node);

        status = BT_STATUS_SUCCESS;
        goto done;
    }
    else if (!IsNodeOnList(&(bt.me.cmdList), &(token->op.node))) {
        /* Get remote name operation is not in progress. Return error. */
        status = BT_STATUS_FAILED;
        goto done;
    }

    /* See if other name requests are in progress for this same address. */ 
    RemoveEntryList(&(token->op.node));
    if (MeFindMatchingToken(token, &(bt.me.cmdList)) != 0) {
        /* Yes, others are on the list. Let them continue and return success
         * to this token only.
         */
        status = BT_STATUS_SUCCESS;
        goto done;
    }
    else {
        /* This is the only one. Put the token back on the list to receive
         * the cancel event.
         */
        InsertHeadList(&(bt.me.cmdList), &(token->op.node));
    }

    token->cancelOp.opType = BOP_CANCEL_REM_NAME;
    InsertHeadList(&(bt.me.opList), &(token->cancelOp.node));

    /* Attempt to start the operation */
    MeStartOperation();
    status = BT_STATUS_PENDING;
done:
    /* Unlock the stack */
    OS_UnlockStack();
    return status;
}


/*---------------------------------------------------------------------------
 *            ME_SetLocalDeviceName()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Set the local device name. 
 *
 * Return:    status of the operation.
 */
BtStatus ME_SetLocalDeviceName(const U8 *name, U8 length)
{
#if XA_ERROR_CHECK == XA_ENABLED
    if ((length > 249) || 
        ((length > 0) && ((name == 0) || (name[length - 1] != 0)))) {
        return BT_STATUS_INVALID_PARM;
    }
#endif /* XA_ERROR_CHECK */

    Assert((length <= 249) && 
          !(((length > 0) && (name == 0)) || (name[length - 1] != 0)));

    /* Lock the stack */
    OS_LockStack();

    /* Always keep the new local name MeWriteLocalName will check for
     * HCI initialization status and write it when the radio is ready.
     */
    (bt.me.localName) = name;
    (bt.me.localNameLen) = length;

    MeWriteLocalName();

    /* Unlock the stack */
    OS_UnlockStack();

    return BT_STATUS_SUCCESS;
}

/*---------------------------------------------------------------------------
 *            ME_ReadLocalBdAddr()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Get the local device BD_ADDR. 
 *
 * Return:    status of the operation.
 */
BtStatus ME_ReadLocalBdAddr(BD_ADDR *Addr)
{
    BtStatus status;

    Assert(Addr);

    /* Lock the stack */
    OS_LockStack();

    /* Make sure the stack is ready. */
    if ((bt.me.stackState) != BTSS_INITIALIZED) {
        if ((bt.me.stackState) >= BTSS_INITIALIZE_ERR)
            status = BT_STATUS_HCI_INIT_ERR;
        else status = BT_STATUS_FAILED;
    } else {
        *Addr = (bt.me.bdaddr);
        status = BT_STATUS_SUCCESS;
    }

    /* Unlock the stack */
    OS_UnlockStack();
    return status;
}

/*---------------------------------------------------------------------------
 *            ME_CreateLink()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Create a link connection to the remote device specified by 
 *            bdAddr.
 *
 * Return:    status of the operation.
 */
BtStatus ME_CreateLink(BtHandler *handler, BD_ADDR *bdAddr, BtPageScanInfo *psi, 
                       BtRemoteDevice **remDevice)
{
    BtRemoteDevice *remDev;
    BtStatus       status;

#if XA_ERROR_CHECK == XA_ENABLED
    if ((handler == 0) || (handler->callback == 0) || (remDevice == 0) ||
        (bdAddr == 0) || !IsEntryAvailable(&(handler->node))) {
        status = BT_STATUS_FAILED;
        goto done;
    }
#endif /* XA_ERROR_CHECK */

    Assert(handler != 0);
    Assert(bdAddr != 0);
    Assert(remDevice != 0);
    Assert(IsEntryAvailable(&(handler->node)));
    Assert(handler->callback != 0);

    /* Lock the stack */
    OS_LockStack();

    /* Make sure the stack does not have an HCI initialization error */
    if ((bt.me.stackState) >= BTSS_INITIALIZE_ERR) {
        status = BT_STATUS_HCI_INIT_ERR;
        goto unlock;
    }

    /* First determine if a connection already exists to this device */
    if ((remDev = ME_FindRemoteDevice(bdAddr)) != 0) {
        /* We found a connection to this device. Now determine if another
         * device can bind to it.
         */
        if (remDev->restricted) {
            status = BT_STATUS_RESTRICTED;
            goto unlock;
        }

        if (remDev->state > BDS_CONNECTED) {
            status = BT_STATUS_IN_PROGRESS;
            goto unlock;
        }
        *remDevice = remDev;

        /* We can bind to this device. Add the handler to the remote device's
         * handlers list.
         */
        InsertTailList(&(remDev->handlers), &(handler->node));
        remDev->discFlag = FALSE;

        if (remDev->state == BDS_CONNECTED) {
            status = BT_STATUS_SUCCESS;
        } else {
            /* An out going or incoming connection is in progress so return
             * status pending.
             */
            status = BT_STATUS_PENDING;
        }
        goto unlock;
    }

    /* There is no current remote device so see if we can start a connection.
     * First we need to get a free RemoteDevice structure
     */
    if ((remDev = MeGetFreeRemoteDevice()) == 0) {
        status = BT_STATUS_NO_RESOURCES;
        goto unlock;
    }
    *remDevice = remDev;

    /* We have RemoteDevice structure so put the operation on the queue */
    InsertTailList(&(remDev->handlers), &(handler->node));
    remDev->state = BDS_OUT_CON;
    (bt.me.pendCons) += 1;
    Assert((bt.me.pendCons) > 0);

    if (psi != 0) {
        remDev->parms.con.psi = *psi;
    } else {
        /* Set to default values */
        remDev->parms.con.psi.clockOffset = 0;
        remDev->parms.con.psi.psMode = 0;
        remDev->parms.con.psi.psRepMode = 1;
    }

    remDev->bdAddr = *bdAddr;
    remDev->op.opType = BOP_LINK_CONNECT;
    remDev->restricted = FALSE;
    remDev->role = BCR_MASTER;

    if ((bt.me.connectionPolicy) == BCR_MASTER)
        remDev->parms.con.allowRoleChange = 0x00;
    else remDev->parms.con.allowRoleChange = 0x01;

    Assert(IsEntryAvailable(&(remDev->op.node)));
    MeAddOperation(remDev);

    /* Attempt to start the operation */
    MeStartOperation();
    status = BT_STATUS_PENDING;

unlock:
    /* Unlock the stack */
    OS_UnlockStack();

#if XA_ERROR_CHECK == XA_ENABLED
done:
#endif
    return status;

}

/*---------------------------------------------------------------------------
 *            ME_ForceDisconnectLinkWithReason()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Disconnect the link. The link will only be disconnected when
 *            all clients have issued disconnect. 
 *
 * Return:    status of the operation. 
 */
BtStatus ME_ForceDisconnectLinkWithReason(BtHandler *handler,
                                          BtRemoteDevice *remDev,
                                          BtErrorCode reason,
                                          BOOL forceDisconnect)
{
    BtStatus status;

    /* Lock the stack */
    OS_LockStack();

#if XA_ERROR_CHECK == XA_ENABLED
    switch (reason) {
    case BEC_AUTHENTICATE_FAILURE:
    case BEC_USER_TERMINATED:
    case BEC_LOW_RESOURCES:
    case BEC_POWER_OFF:
    case BEC_UNSUPPORTED_REMOTE:
    case BEC_PAIR_UNITKEY_NO_SUPP:
        break;
    default:
        status = BT_STATUS_INVALID_PARM;
        goto done;
    }

    if ((remDev == 0) || (handler == 0 && !forceDisconnect) ||
        (handler && !IsNodeOnList(&(remDev->handlers), &(handler->node)))) { 
        status = BT_STATUS_FAILED;
        goto done;
    }
#endif /* XA_ERROR_CHECK */

    Assert((forceDisconnect == TRUE) || (reason == BEC_POWER_OFF) || (handler != 0));
    Assert(handler ? IsNodeOnList(&(remDev->handlers), &(handler->node)):1);
    Assert(remDev != 0);

    /* set the disconnect reason */
    remDev->discReason = reason;

    if (!IsNodeOnList(&(bt.me.opList), &(remDev->op.node)) &&
        ((remDev->state == BDS_OUT_CON) || (remDev->state == BDS_IN_CON))) {
        /* Connection operation is in progress so we need to wait until it
         * is finished
         */
        status = BT_STATUS_IN_PROGRESS;
        goto done;
    }

    if (handler) {
        /* Remove the handler from the list */
        RemoveEntryList(&(handler->node));
    }

    /* Now see if all items have been removed and L2CAP has no channels */
    if ((forceDisconnect == TRUE || reason == BEC_POWER_OFF) ||
        IsListEmpty(&(remDev->handlers))) {
        /* Set the discFlag */
        remDev->discFlag = TRUE;

        if (((bt.me.stackState) < BTSS_INITIALIZE_ERR) &&
            ((remDev->refCount == 0) || (reason == BEC_POWER_OFF) ||
             (forceDisconnect == TRUE))) {
            MeDisconnectLink(remDev);
        }
    }

    status = BT_STATUS_SUCCESS;
done:
    /* Unlock the stack */
    OS_UnlockStack();
    return status;

}

/*---------------------------------------------------------------------------
 *            ME_CancelCreateLink()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Request cancelation of a pending ME_CreateLink.
 *
 * Return:    status of the operation. 
 */
BtStatus ME_CancelCreateLink(BtHandler* handler, BtRemoteDevice* remDev)
{
    BtStatus status;

    /* Lock the stack */
    OS_LockStack();

    if (IsNodeOnList(&(bt.me.opList), &(remDev->op.node)) &&
        (remDev->op.opType == BOP_LINK_CONNECT)) {
        /* CreateLink hasn't started. Just remove it from the list
         * and return success.
         */
        RemoveEntryList(&remDev->op.node);

        /* Remove the handler from the list */
        RemoveEntryList(&(handler->node));

        status = BT_STATUS_SUCCESS;
        goto done;
    }
    else if (remDev->state != BDS_OUT_CON) {
        /* Connection operation is not in progress. Return error. */
        status = BT_STATUS_FAILED;
        goto done;
    }

    /* Remove the handler from the list */
    RemoveEntryList(&(handler->node));

    if (!IsListEmpty(&(remDev->handlers))) {
        /* This is not the only pending connect. Handler is removed,
         * return success.
         */
        status = BT_STATUS_SUCCESS;
        goto done;
    }

    remDev->cancelOp.opType = BOP_CANCEL_CONNECT;
    InsertHeadList(&(bt.me.opList), &(remDev->cancelOp.node));

    /* Attempt to start the operation */
    MeStartOperation();
    status = BT_STATUS_SUCCESS;
done:
    /* Unlock the stack */
    OS_UnlockStack();
    return status;
}

/*---------------------------------------------------------------------------
 *            ME_RegisterAcceptHandler()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Register a handler for dealing with incoming connections. 
 *
 * Return:    Pointer to old handler. 
 */
BtHandler* ME_RegisterAcceptHandler(BtHandler *handler)
{
    BtHandler* tmpHandler;

    /* Lock the stack */
    OS_LockStack();

    tmpHandler = (bt.me.acceptHandler);
    (bt.me.acceptHandler) = handler;

    /* Unlock the stack */
    OS_UnlockStack();
    return tmpHandler;
}

/*---------------------------------------------------------------------------
 *            ME_SetConnectionRole()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Set the role that will be requested for all future incoming and
 *            outgoing connections.
 *
 * Return:    The old role. 
 */
BtConnectionRole ME_SetConnectionRole(BtConnectionRole role)
{
    BtConnectionRole oldRole;

    Assert((role == BCR_MASTER) || (role == BCR_ANY));

    /* Lock the stack */
    OS_LockStack();

    oldRole = (bt.me.connectionPolicy);
    (bt.me.connectionPolicy) = role;

    /* Unlock the stack */
    OS_UnlockStack();
    return oldRole;
}

/*---------------------------------------------------------------------------
 *            ME_AcceptIncomingLink()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Accept an incoming connection. 
 *
 * Return:    status of the operation.
 */
BtStatus ME_AcceptIncomingLink(BtRemoteDevice *remDev, BtConnectionRole role)
{
    BtStatus status;

#if XA_ERROR_CHECK == XA_ENABLED
    status = BT_STATUS_INVALID_PARM;
    if ((remDev == 0) || ((role != BCR_MASTER) && (role != BCR_SLAVE))) {
        goto done;
    }
#endif /* XA_ERROR_CHECK */

    Assert(remDev != 0);
    Assert((role == BCR_MASTER) || (role == BCR_SLAVE));

    /* Lock the stack */
    OS_LockStack();

    /* Make sure the stack does not have an HCI initialization error */
    if ((bt.me.stackState) >= BTSS_INITIALIZE_ERR) {
        status = BT_STATUS_HCI_INIT_ERR;
        goto unlock;
    }

    /* Determine if the remDev is in a state for dealing
     * with incoming connections
     */
    if ((remDev->state != BDS_IN_CON) ||
        (remDev->op.opType != BOP_NOP)) {
        status = BT_STATUS_FAILED;
    } else {
        /* Start the operation */
        remDev->op.opType = BOP_LINK_ACCEPT;
        remDev->restricted = FALSE;
        remDev->parms.acceptRole = role;
        remDev->role = role;

        /* Add the operation to the queue */
        Assert(IsEntryAvailable(&(remDev->op.node)));
        MeAddOperation(remDev);

        /* Attempt to start the operation */
        MeStartOperation();
        status = BT_STATUS_PENDING;
    }

unlock:
    /* Unlock the stack */
    OS_UnlockStack();

#if XA_ERROR_CHECK == XA_ENABLED
done:
#endif
    return status;
}

/*---------------------------------------------------------------------------
 *            ME_RejectIncomingLink()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Reject an incoming connection. 
 *
 * Return:    status of the operation.
 */
BtStatus ME_RejectIncomingLink(BtRemoteDevice *remDev, BtErrorCode reason)
{
    BtStatus status;

#if XA_ERROR_CHECK == XA_ENABLED
    status = BT_STATUS_INVALID_PARM;
    if ((remDev == 0) || ((reason != BEC_LIMITED_RESOURCE) && 
        (reason != BEC_SECURITY_ERROR) && reason != BEC_PERSONAL_DEVICE)) {
        goto done;
    }
#endif /* XA_ERROR_CHECK */

    Assert(remDev != 0);
    Assert((reason == BEC_LIMITED_RESOURCE) || (reason == BEC_SECURITY_ERROR) ||
           (reason == BEC_PERSONAL_DEVICE));

    /* Lock the stack */
    OS_LockStack();

    /* Make sure the stack does not have an HCI initialization error */
    if ((bt.me.stackState) >= BTSS_INITIALIZE_ERR) {
        status = BT_STATUS_HCI_INIT_ERR;
        goto unlock;
    }

    /* Determine if the remDev is in a state for dealing
     * with incoming connections
     */
    if ((remDev->state != BDS_IN_CON) ||
        (remDev->op.opType != BOP_NOP)) {
        /* Not in the correct state for incoming connections */
        status = BT_STATUS_FAILED;
    } else {
        /* Start the operation */
        remDev->op.opType = BOP_LINK_REJECT;
        remDev->parms.rejectReason = reason;

        /* Add the operation to the queue */
        Assert(IsEntryAvailable(&(remDev->op.node)));
        MeAddOperation(remDev);

        /* Attempt to start the operation */
        MeStartOperation();
        status = BT_STATUS_PENDING;
    }
unlock:
    /* Unlock the stack */
    OS_UnlockStack();

#if XA_ERROR_CHECK == XA_ENABLED
done:
#endif
    return status;
}

#if BT_SECURITY == XA_ENABLED
/*---------------------------------------------------------------------------
 *            SEC_RegisterPairingHandler()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Register a handler for dealing with pairing. 
 *
 * Return:    Pointer to old handler. 
 */
BtHandler* SEC_RegisterPairingHandler(BtHandler *handler)
{
    BtHandler* tmpHandler;

    /* Lock the stack */
    OS_LockStack();

    tmpHandler = (bt.me.pairingHandler);
    (bt.me.pairingHandler) = handler;

    /* Unlock the stack */
    OS_UnlockStack();
    return tmpHandler;
}

/*---------------------------------------------------------------------------
 *            SEC_RegisterAuthorizeHandler()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Register a handler for dealing with authorization. 
 *
 * Return:    Pointer to old handler. 
 */
BtHandler* SEC_RegisterAuthorizeHandler(BtHandler* handler)
{
    BtHandler* tmpHandler;

    /* Lock the stack */
    OS_LockStack();

    tmpHandler = (bt.me.authorizeHandler);
    (bt.me.authorizeHandler) = handler;

    /* Unlock the stack */
    OS_UnlockStack();
    return tmpHandler;
}

/*---------------------------------------------------------------------------
 *            SEC_SetPin()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Provide a pin code for the pairing process. 
 *
 * Return:    Pointer to old handler. 
 */
BtStatus SEC_SetPin(BtRemoteDevice *remDev, const U8 *pin, U8 len, BtPairingType type)
{
    BtStatus      status;
    BtSecurityOp* secOp;

#if XA_ERROR_CHECK == XA_ENABLED
    if (remDev == 0) { 
        status = BT_STATUS_INVALID_PARM;
        goto done;
    }
#endif /* XA_ERROR_CHECK */

    /* Lock the stack */
    OS_LockStack();

    /* Make sure the stack does not have an HCI initialization error */
    if ((bt.me.stackState) >= BTSS_INITIALIZE_ERR) {
        status = BT_STATUS_HCI_INIT_ERR;
        goto unlockStack;
    }


    /* Verify that the remote device is in a state to accept a pin */
    if ((remDev->state == BDS_DISCONNECTED) ||
        (remDev->state > BDS_CONNECTED) || 
        ((remDev->authState != BAS_WAITING_FOR_PIN) &&
        (remDev->authState != BAS_WAITING_FOR_PIN_R))) {
        status = BT_STATUS_FAILED;
        goto unlockStack;   
    }

    /* Get a security op */
    secOp = SecBtGetFreeSecurityOp();

    if (secOp == 0) {
        /* We couldn't get a security operation token so fail */
        status = BT_STATUS_NO_RESOURCES;
        goto unlockStack;
    }

    /* Copy the BD_ADDR */
    secOp->bdAddr = remDev->bdAddr;

    /* Verify the parameters */
    if (pin != 0) {
        if ((len == 0) || (len > 16)) {
            status = BT_STATUS_INVALID_PARM;
            goto unlockStack;
        }
        /* We have a pin so copy into the security op */
        secOp->remDev = remDev;
        secOp->x.pLen = len;
        OS_MemCopy(secOp->pin, pin, len);
        remDev->pairingType = type;
    } else {
        /* Send a negative reply by setting secOp->remDev to 0. */
        secOp->remDev = 0;
        remDev->authState = BAS_NOT_AUTHENTICATED;
    }

    /* Add the operation to the queue */
    secOp->op.opType = BOP_PIN_CODE_RSP;
    MeAddOperation(secOp);

    /* Attempt to start the operation */
    MeStartOperation();
    status = BT_STATUS_PENDING;

unlockStack:
    /* Unlock the stack */
    OS_UnlockStack();

#if XA_ERROR_CHECK == XA_ENABLED
done:
#endif
    return status;
}

/*---------------------------------------------------------------------------
 *            SEC_AuthenticateLink
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Authenticate an existing link.
 *
 * Return:    Pointer to old handler. 
 */
BtStatus SEC_AuthenticateLink(BtHandler *handler, BtRemoteDevice *remDev)
{
    BtStatus status;

#if XA_ERROR_CHECK == XA_ENABLED
    if (remDev == 0) { 
        status = BT_STATUS_INVALID_PARM;
        goto done;
    }
#endif /* XA_ERROR_CHECK */

    /* Lock the stack */
    OS_LockStack();

    /* Make sure the stack does not have an HCI initialization error */
    if ((bt.me.stackState) >= BTSS_INITIALIZE_ERR) {
        status = BT_STATUS_HCI_INIT_ERR;
        goto unlock;
    }

    remDev->authHandler = handler;
    status = SecAddAuthenticateOpToQueue(remDev);
unlock:
    /* Unlock the stack */
    OS_UnlockStack();

#if XA_ERROR_CHECK == XA_ENABLED
done:
#endif
    return status;
}

/*---------------------------------------------------------------------------
 *            SEC_Register()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Register a security record.
 *
 * Return:    status of operation. 
 */
BtStatus SEC_Register(BtSecurityRecord *sr)
{
    BtStatus status;
    BtSecurityRecord *tsr;

    /* Lock the stack */
    OS_LockStack();

#if XA_ERROR_CHECK == XA_ENABLED
    status = BT_STATUS_INVALID_PARM;
    if (sr == 0) { 
        goto done;
    }

    if (IsNodeOnList(&((bt.me.secList)), &(sr->node))) {
        status = BT_STATUS_FAILED;
        goto done;
    }

    if (sr->level & (BSL_AUTHORIZATION_IN | BSL_ENCRYPTION_IN)) {
        if ((sr->level & BSL_AUTHENTICATION_IN) == 0) {
            goto done;
        }
    }

    if (sr->level & (BSL_AUTHORIZATION_OUT | BSL_ENCRYPTION_OUT)) {
        if ((sr->level & BSL_AUTHENTICATION_OUT) == 0) {
            goto done;
        }
    }

#endif /* XA_ERROR_CHECK */
    Assert(sr != 0);
    Assert(!IsNodeOnList(&((bt.me.secList)), &(sr->node)));
           
#if XA_DEBUG == XA_ENABLED
    if (sr->level & (BSL_AUTHORIZATION_IN | BSL_ENCRYPTION_IN)) {
        if ((sr->level & BSL_AUTHENTICATION_IN) == 0) {
            Assert(0);
        }
    }

    if (sr->level & (BSL_AUTHORIZATION_OUT | BSL_ENCRYPTION_OUT)) {
        if ((sr->level & BSL_AUTHENTICATION_OUT) == 0) {
            Assert(0);
        }
    }
#endif /* XA_DEBUG */

    /* Make sure the stack does not have an HCI initialization error */
    if ((bt.me.stackState) >= BTSS_INITIALIZE_ERR) {
        status = BT_STATUS_HCI_INIT_ERR;
        goto done;
    }

    /* Make sure we don't already have a record that contains the protocol ID
     * and channel
     */
    tsr = (BtSecurityRecord*) (bt.me.secList).Flink;
    while ((ListEntry*)tsr != &(bt.me.secList)) {
        if ((tsr->id == sr->id) && (tsr->channel == sr->channel)) {
            /* We have found a matching record */
            status = BT_STATUS_IN_USE;
            goto done;
        }
        /* Advance to the next record */
        tsr = (BtSecurityRecord*) tsr->node.Flink;
    }

    /* Add the item to the list */
    InsertTailList(&((bt.me.secList)), &(sr->node));
    status = BT_STATUS_SUCCESS;

done:
    /* Unlock the stack */
    OS_UnlockStack();
    return status;
}


/*---------------------------------------------------------------------------
 *            SEC_Unregister()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Unregister a security record.
 *
 * Return:    status of operation. 
 */
BtStatus SEC_Unregister(BtSecurityRecord *sr)
{
    BtStatus status;

    /* Lock the stack */
    OS_LockStack();

    if ((sr != 0) && IsNodeOnList(&((bt.me.secList)), &(sr->node))) {
        RemoveEntryList(&(sr->node));
        status = BT_STATUS_SUCCESS;
    } else {
        status = BT_STATUS_FAILED;
    }

    /* Unlock the stack */
    OS_UnlockStack();
    return status;
}


/*---------------------------------------------------------------------------
 *            SEC_AccessRequest()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Request accesss from the security manager.
 *
 * Return:    status of operation. 
 */
BtStatus SEC_AccessRequest(BtSecurityToken *token)
{
    BtStatus          status;
    BtSecurityRecord* sr;

#if XA_ERROR_CHECK == XA_ENABLED
    if ((token == 0) || (token->remDev == 0)) {
        status = BT_STATUS_INVALID_PARM;
        goto done;
    }
#endif /* XA_ERROR_CHECK */

    Assert(token != 0);
    Assert(token->remDev != 0);

    /* Lock the stack */
    OS_LockStack();

    /* Make sure the stack does not have an HCI initialization error */
    if ((bt.me.stackState) >= BTSS_INITIALIZE_ERR) {
        status = BT_STATUS_HCI_INIT_ERR;
        goto unlock;
    }

    /* Search the security records for a record that contains the protocol ID
     * and channel
     */
    sr = (BtSecurityRecord*) (bt.me.secList).Flink;
    while ((ListEntry*)sr != &(bt.me.secList)) {
        if ((sr->id == token->id) && (sr->channel == token->channel)) {
            /* We have found a matching record */
            break;
        }
        /* Advance to the next record */
        sr = (BtSecurityRecord*) sr->node.Flink;
    }

    if ((ListEntry*)sr != &(bt.me.secList)) {
        /* We have a record so store the appropriate information into
         * the token.
         */
        token->record = sr;
        token->level = sr->level;
    } else {
        /* There is no security record for this protocol/channel.*/
#if BT_DEFAULT_SECURITY == XA_ENABLED
        token->level = BSL_DEFAULT;
#else
        token->level = BSL_NO_SECURITY;
#endif
    }

    token->result = BTEVENT_ACCESS_DENIED;
    status = SecProcessSecurityToken(token);

    if (status == BT_STATUS_PENDING) {

        /* Set the result to 0. We do this to
         * prevent a re-entrant call into the token's callback
         */
        token->result = 0;

        /* Add the token to the queue and start any operations
         * (especially the one that SecProcessSecurityToken added)
         */
        InsertTailList(&(token->remDev->secTokens), &(token->node));
        MeStartOperation();

        /* Did the operation complete? If so, the operation will
         * have stored a different value into the token's result:
         */
        if (token->result == BTEVENT_ACCESS_APPROVED) {
            status = BT_STATUS_SUCCESS;
        } else if (token->result == BTEVENT_ACCESS_DENIED) {
            status = BT_STATUS_FAILED;
        } else {
            /* Any other value is invalid */
            Assert(token->result == 0);

#if BT_SECURITY_TIMEOUT != 0
            /* Initiate watchdog timer. */
            token->timer.context = (void *)token;
            token->timer.func = SecAccessTimeoutHandler;
            EVM_StartTimer(&token->timer, (BT_SECURITY_TIMEOUT) * 1000);
#endif /* BT_SECURITY_TIMEOUT != 0 */
        }

        /* Replace the token's result for normal callback behavior */
        token->result = BTEVENT_ACCESS_DENIED;
    }
unlock:
    /* Unlock the stack */
    OS_UnlockStack();

#if XA_ERROR_CHECK == XA_ENABLED
done:
#endif
    return status;
}


/*---------------------------------------------------------------------------
 *            SEC_AuthorizeServiceB()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Authorize the link in response to a BTEVENT_AUTHORIZATION_REQ event.
 *
 * Return:    status of operation. 
 */
BtStatus SEC_AuthorizeServiceB(BtRemoteDevice *remDev,
                               BOOL isAuthorized,
                               BOOL futureTrust,
                               BOOL authService)
{
    BtStatus status;
    BtDeviceRecord record;

#if XA_ERROR_CHECK == XA_ENABLED
    if (remDev == 0) {
        status = BT_STATUS_INVALID_PARM;
        goto done;
    }
#endif /* XA_ERROR_CHECK */

    /* Lock the stack */
    OS_LockStack();

    /* Make sure the stack does not have an HCI initialization error */
    if ((bt.me.stackState) >= BTSS_INITIALIZE_ERR) {
        status = BT_STATUS_HCI_INIT_ERR;
        goto unlock;
    }

    if ((remDev->state == BDS_CONNECTED) && 
        (remDev->authorizeState == BAS_START_AUTHORIZE)) {
        Assert(remDev->authState == BAS_AUTHENTICATED);

        /* Assume the operation is successful */
        status = BT_STATUS_SUCCESS;

        /* See if we need to update the device database. */
        if (futureTrust == TRUE) {
            status = BT_STATUS_DEVICE_NOT_FOUND;

            /* Set the trust flag in the device database */
            if (SEC_FindDeviceRecord(&(remDev->bdAddr), &record) == BT_STATUS_SUCCESS) {
                record.trusted = TRUE;
                if (SEC_AddDeviceRecord(&record) == BT_STATUS_SUCCESS) {
                    status = BT_STATUS_SUCCESS;
                }
            }
        }

        /* Set the authorization */
        if (isAuthorized == TRUE) {
            if (authService == TRUE) {
                remDev->authorizeState = BAS_AUTHORIZED_SERVICE;
            }
            else {
                remDev->authorizeState = BAS_AUTHORIZED;
            }
        } else {
            remDev->authorizeState = BAS_NOT_AUTHORIZED;
        }

        remDev->secAccessState |= BAS_AUTHORIZE_COMPLETE;
        SecProcessAllSecurityTokens(remDev);
    } else {
        /* Remote device is not in the proper state to receive this event */
        status = BT_STATUS_FAILED;
    }
unlock:
    /* Unlock the stack */
    OS_UnlockStack();

#if XA_ERROR_CHECK == XA_ENABLED
done:
#endif
    return status;
}

/*---------------------------------------------------------------------------
 *            SEC_CancelAccessRequest()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Cancel Access Request token.
 *
 * Return:    status of operation. 
 */
BtStatus SEC_CancelAccessRequest(BtSecurityToken *token)
{
    BtStatus        status = BT_STATUS_FAILED;
    BtSecurityOp   *secOp;

    OS_LockStack();

    if (token->node.Flink && token->node.Blink) {
        RemoveEntryList(&token->node);

#if BT_SECURITY_TIMEOUT != 0
        /* Cancel the watchdog timer if it is in operation. */
        EVM_CancelTimer(&token->timer);
#endif /* BT_SECURITY_TIMEOUT != 0 */        
        
        if (token->remDev->authState == BAS_WAITING_FOR_PIN ||
            token->remDev->authState == BAS_WAITING_FOR_PIN_R) {

            Report(("MeSec: Adding cancel authentication operation to queue\n"));

            /* Setup a security op to send the rejection */
            secOp = SecBtGetFreeSecurityOp();
            Assert(secOp != 0);

            /* Set the remDev to 0 indicating a negative response */
            secOp->remDev = 0;

            /* Copy in the BD_ADDR */
            secOp->bdAddr = token->remDev->bdAddr;
            secOp->op.opType = BOP_PIN_CODE_RSP;
        
            Assert(IsEntryAvailable(&(secOp->op.node)));
            MeAddOperationFront(secOp);

            /* Attempt to start the operation */
            MeStartOperation();
        }
        else if (token->remDev->authorizeState == BAS_START_AUTHORIZE) {

            Report(("MeSec: Adding cancel authorize operation to queue\n"));

            /* Setup a security op to send the rejection */
            secOp = SecBtGetFreeSecurityOp();
            Assert(secOp != 0);

            secOp->remDev = token->remDev;
            secOp->op.opType = BOP_CANCEL_AUTHORIZE;

            token->remDev->authorizeState = BAS_NOT_AUTHORIZED;
            token->remDev->secRec = token->record;

            Assert(IsEntryAvailable(&(secOp->op.node)));
            MeAddOperationFront(secOp);

            MeStartOperation();
        }

        status = BT_STATUS_SUCCESS;
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            SEC_SetLinkEncryption()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Set link encryption.
 *
 * Return:    status of operation. 
 */
BtStatus SEC_SetLinkEncryption(BtHandler *handler, BtRemoteDevice *remDev,
                               BtEncryptMode mode)
{
    BtStatus      status;
    BtSecurityOp* secOp;

#if XA_ERROR_CHECK == XA_ENABLED
    if ((remDev == 0) ||  
        ((mode != BECM_ENCRYPT_DISABLE) && (mode != BECM_ENCRYPT_ENABLE)))  {
        status = BT_STATUS_INVALID_PARM;
        goto done;
    }
#endif /* XA_ERROR_CHECK */

    Assert((remDev != 0) && 
           ((mode == BECM_ENCRYPT_DISABLE) || (mode == BECM_ENCRYPT_ENABLE)));

    /* Lock the stack */
    OS_LockStack();

    /* Make sure the stack does not have an HCI initialization error */
    if ((bt.me.stackState) >= BTSS_INITIALIZE_ERR) {
        status = BT_STATUS_HCI_INIT_ERR;
        goto unlock;
    }

    /* See if the link is in the proper state for setting encryption */
    if ((remDev->state != BDS_CONNECTED) || 
        (remDev->encryptState == BES_START_ENCRYPT) ||
        (remDev->encryptState == BES_END_ENCRYPT)) {
        status = BT_STATUS_FAILED;
        goto unlock;
    }

    /* Now see if the desired state is already set */
    if (((mode == BECM_ENCRYPT_DISABLE) && 
         (remDev->encryptState == BES_NOT_ENCRYPTED)) ||
        ((mode == BECM_ENCRYPT_ENABLE) && 
         (remDev->encryptState == BES_ENCRYPTED))){
        status = BT_STATUS_SUCCESS;
        goto unlock;
    }

    /* If we are enabling encryption then make sure that the link is
     * authenticated
     */
    if ((mode == BECM_ENCRYPT_ENABLE) && 
        (remDev->authState != BAS_AUTHENTICATED)) {

        status = BT_STATUS_FAILED;
        goto unlock;
    }

    /* If we are disabling encryption then make sure that the link is
     * authenticated
     */
    if ((mode == BECM_ENCRYPT_DISABLE) && 
        (remDev->authState != BAS_AUTHENTICATED)) {

        status = BT_STATUS_FAILED;
        goto unlock;
    }

    /* Time to setup an encryption operation */
    secOp = SecBtGetFreeSecurityOp();

    if (secOp == 0) {
        status = BT_STATUS_NO_RESOURCES;
    } else {
        secOp->remDev = remDev;

        /* Add the encryption operation to the queue */
        secOp->op.opType = BOP_ENCRYPTION;
        secOp->x.eMode = mode;
        remDev->encryptHandler = handler;

        /* Set the encryption state */
        if (mode == BECM_ENCRYPT_DISABLE) {
            remDev->encryptState = BES_END_ENCRYPT;
        } else {
            remDev->encryptState = BES_START_ENCRYPT;
        }

        Assert(IsEntryAvailable(&(secOp->op.node)));
        MeAddOperation(secOp);

        /* Attempt to start the operation */
        MeStartOperation();
        status = BT_STATUS_PENDING;
    }
unlock:
    /* Unlock the stack */
    OS_UnlockStack();

#if XA_ERROR_CHECK == XA_ENABLED
done:
#endif
    return status;
}


/*---------------------------------------------------------------------------
 *            SEC_EnableSecurityMode3()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Enable security mode 3.
 *
 * Return:    status of operation. 
 */
BtStatus SEC_EnableSecurityMode3(BtHandler *handler, BOOL encryption)
{
    BtStatus      status;

    /* Lock the stack */
    OS_LockStack();

    /* Make sure the stack does not have an HCI initialization error */
    if ((bt.me.stackState) >= BTSS_INITIALIZE_ERR) {
        status = BT_STATUS_HCI_INIT_ERR;
        goto done;
    }

    /* See if we are already in security level 3 */
    if ((bt.me.secModeState) == BSMS_LEVEL3) {
        status = BT_STATUS_SUCCESS;
        if (encryption != (bt.me.secModeEncrypt)) {
            status = SecSetEncryptMode(handler, encryption);
        }
        goto done;
    }

    /* See if we are in a state to switch to security level 3 */
    if ((bt.me.secModeState) != BSMS_LEVEL2) {
        status = BT_STATUS_FAILED;
        goto done;
    }

    /* Set the mode */
    status = SecSetSecurityMode(handler, BSM_SEC_LEVEL_3, encryption);

done:
    /* Unlock the stack */
    OS_UnlockStack();
    return status;
}


/*---------------------------------------------------------------------------
 *            SEC_DisableSecurityMode3()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Enable security mode 3.
 *
 * Return:    status of operation. 
 */
BtStatus SEC_DisableSecurityMode3(BtHandler *handler)
{
    BtStatus      status;

    /* Lock the stack */
    OS_LockStack();

    /* Make sure the stack does not have an HCI initialization error */
    if ((bt.me.stackState) >= BTSS_INITIALIZE_ERR) {
        status = BT_STATUS_HCI_INIT_ERR;
        goto done;
    }

    /* See if we are already in security level 2 */
    if ((bt.me.secModeState) == BSMS_LEVEL2) {
        status = BT_STATUS_SUCCESS;
        goto done;
    }

    /* See if we are in a state to switch to security level 2 */
    if ((bt.me.secModeState) != BSMS_LEVEL3) {
        status = BT_STATUS_FAILED;
        goto done;
    }

    /* Set the mode */
    status = SecSetSecurityMode(handler, BSM_SEC_LEVEL_2, (bt.me.secModeEncrypt));

done:
    /* Unlock the stack */
    OS_UnlockStack();
    return status;
}

/*---------------------------------------------------------------------------
 *            SEC_DeleteDeviceRecord()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Delete the record with the given bdAddr from the device
 *            database. 
 *
 * Return:    status of operation. 
 */
BtStatus SEC_DeleteDeviceRecord(const BD_ADDR *bdAddr)
{
    BtStatus status;
#if NUM_KNOWN_DEVICES > 0
    BtDeviceContext *device;
#endif

#if XA_ERROR_CHECK == XA_ENABLED
    if (bdAddr == 0) {
        status = BT_STATUS_INVALID_PARM;
        goto done;
    }
#endif /* XA_ERROR_CHECK */

    Assert(bdAddr != 0);

    /* Lock the stack */
    OS_LockStack();

    status = DDB_DeleteRecord(bdAddr);

#if NUM_KNOWN_DEVICES > 0
    if ((status == BT_STATUS_SUCCESS) && (0!=(device = DS_FindDevice(bdAddr))))
        device->state &= ~(BDS_TRUSTED|BDS_PAIRED);
#endif

    /* Unlock the stack */
    OS_UnlockStack();
#if XA_ERROR_CHECK == XA_ENABLED
done:
#endif
    return status;
}


/*---------------------------------------------------------------------------
 *            SEC_AddDeviceRecord()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Delete the record with the given bdAddr from the device
 *            database. 
 *
 * Return:    status of operation. 
 */
BtStatus SEC_AddDeviceRecord(const BtDeviceRecord* record)
{
    BtStatus status;
#if NUM_KNOWN_DEVICES > 0
    BtDeviceContext *device;
#endif

#if XA_ERROR_CHECK == XA_ENABLED
    if (record == 0) {
        status = BT_STATUS_INVALID_PARM;
        goto done;
    }
#endif /* XA_ERROR_CHECK */

    Assert(record != 0);

    /* Lock the stack */
    OS_LockStack();

    status = DDB_AddRecord(record);

#if NUM_KNOWN_DEVICES > 0
    if ((status == BT_STATUS_SUCCESS) && (0!=(device = DS_FindDevice(&record->bdAddr)))) {
        device->state |= BDS_PAIRED;
        if (record->trusted)
            device->state |= BDS_TRUSTED;
        else device->state &= ~BDS_TRUSTED;
    }
#endif

    /* Unlock the stack */
    OS_UnlockStack();
#if XA_ERROR_CHECK == XA_ENABLED
done:
#endif
    return status;
}

/*---------------------------------------------------------------------------
 *            SEC_FindDeviceRecord()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Find the record with the given bdAddr in the device
 *            database. 
 *
 * Return:    status of operation. 
 */
BtStatus SEC_FindDeviceRecord(const BD_ADDR *bdAddr, BtDeviceRecord *record)
{
    BtStatus status;

#if XA_ERROR_CHECK == XA_ENABLED
    if ((bdAddr == 0) || (record == 0)) {
        status = BT_STATUS_INVALID_PARM;
        goto done;
    }
#endif /* XA_ERROR_CHECK */

    Assert(bdAddr != 0);
    Assert(record != 0);

    /* Lock the stack */
    OS_LockStack();

    status = DDB_FindRecord(bdAddr, record);

    /* Unlock the stack */
    OS_UnlockStack();
#if XA_ERROR_CHECK == XA_ENABLED
done:
#endif
    return status;
}


/*---------------------------------------------------------------------------
 *            SEC_EnumDeviceRecords()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Enerate record in the device database. 
 *
 * Return:    status of operation. 
 */
BtStatus SEC_EnumDeviceRecords(I16 i, BtDeviceRecord *record)
{
    BtStatus status;

#if XA_ERROR_CHECK == XA_ENABLED
    if (record == 0) {
        status = BT_STATUS_INVALID_PARM;
        goto done;
    }
#endif /* XA_ERROR_CHECK */

    Assert(record != 0);

    /* Lock the stack */
    OS_LockStack();

    status = DDB_EnumDeviceRecords(i, record);

    /* Unlock the stack */
    OS_UnlockStack();
#if XA_ERROR_CHECK == XA_ENABLED
done:
#endif
    return status;
}
#endif /* BT_SECURITY */


/*---------------------------------------------------------------------------
 *            ME_Hold()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Put the remote device into hold mode. 
 *
 * Return:    status of operation. 
 */
BtStatus ME_Hold(BtRemoteDevice *remDev, U16 max, U16 min)
{
    BtStatus status;

#if XA_ERROR_CHECK == XA_ENABLED
    if ((max == 0) || (min == 0) || (min > max)) {
        return BT_STATUS_INVALID_PARM;
    }
#endif /* XA_ERROR_CHECK */

    Assert((max != 0) && (min != 0) && (max >= min));

    /* Lock the stack */
    OS_LockStack();

    status = MeCheckModeChange(remDev, BOP_HOLD_MODE);
    if (status == BT_STATUS_SUCCESS) {
        /* Start the operation */
        remDev->op.opType = BOP_HOLD_MODE;
        remDev->parms.mode.max = max;
        remDev->parms.mode.min = min;

        Assert(IsEntryAvailable(&(remDev->op.node)));
        MeAddOperation(remDev);

        /* Attempt to start the operation */
        MeStartOperation();
        status = BT_STATUS_PENDING;
    }

    /* Unlock the stack */
    OS_UnlockStack();

    return status;
}
#if 0
BtStatus ME_FlowSpec(BtRemoteDevice *remDev, U8 flow_direction, U8 flow_service_type,
U32 flow_token_rate, U32 flow_token_size, U32 flow_token_bandwidth, U32 flow_token_latency)
{
    BtStatus status;

    /* Lock the stack */
    OS_LockStack();

        /* Start the operation */
        remDev->op.opType = BOP_FLOW_SPEC;
        remDev->parms.flowspec.flow_direction = flow_direction;
        remDev->parms.flowspec.flow_service_type = flow_service_type;
        remDev->parms.flowspec.flow_token_rate = flow_token_rate;
        remDev->parms.flowspec.flow_token_size = flow_token_size;
        remDev->parms.flowspec.flow_token_bandwidth = flow_token_bandwidth;
        remDev->parms.flowspec.flow_token_latency = flow_token_latency;            

        Assert(IsEntryAvailable(&(remDev->op.node)));
        MeAddOperation(remDev);

        /* Attempt to start the operation */
        MeStartOperation();
        status = BT_STATUS_PENDING;

    /* Unlock the stack */
    OS_UnlockStack();

    return status;
}
#endif
/*---------------------------------------------------------------------------
 *            ME_StartSniff()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Put the remote device into sniff mode. 
 *
 * Return:    status of operation. 
 */
BtStatus ME_StartSniff(BtRemoteDevice *remDev, const BtSniffInfo *info)
{
    BtStatus status;

#if XA_ERROR_CHECK == XA_ENABLED
    if ((info->maxInterval == 0) || (info->minInterval == 0) || 
        (info->minInterval > info->maxInterval) || (info->attempt == 0)) {
        return BT_STATUS_INVALID_PARM;
    }
#endif /* XA_ERROR_CHECK */

    Assert((info->maxInterval != 0) && (info->minInterval != 0));
    Assert(info->attempt != 0);
    Assert(info->maxInterval >= info->minInterval);

    /* Lock the stack */
    OS_LockStack();

    status = MeCheckModeChange(remDev, BOP_SNIFF_MODE);
    if (status == BT_STATUS_SUCCESS) {
        /* Start the operation */
        remDev->op.opType = BOP_SNIFF_MODE;
        remDev->parms.sniff = *info;

        Assert(IsEntryAvailable(&(remDev->op.node)));
        MeAddOperation(remDev);

        /* Attempt to start the operation */
        MeStartOperation();
        status = BT_STATUS_PENDING;
    }

    /* Unlock the stack */
    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            ME_StopSniff()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Take the link out of sniff mode. 
 *
 * Return:    status of operation. 
 */
BtStatus ME_StopSniff(BtRemoteDevice *remDev)
{
    BtStatus status;

    /* Lock the stack */
    OS_LockStack();

    status = MeCheckModeChange(remDev, BOP_EXIT_SNIFF_MODE);
    if (status == BT_STATUS_SUCCESS) {
        /* Start the operation */
        remDev->op.opType = BOP_EXIT_SNIFF_MODE;

        Assert(IsEntryAvailable(&(remDev->op.node)));
        MeAddOperation(remDev);

        /* Attempt to start the operation */
        MeStartOperation();
        status = BT_STATUS_PENDING;
    }

    /* Unlock the stack */
    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            ME_StartPark()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Put the link into park mode. 
 *
 * Return:    status of operation. 
 */
BtStatus ME_StartPark(BtRemoteDevice *remDev, U16 max, U16 min)
{
    BtStatus status;

#if XA_ERROR_CHECK == XA_ENABLED
    if ((max == 0) || (min == 0) || (min > max)) {
        return BT_STATUS_INVALID_PARM;
    }
#endif /* XA_ERROR_CHECK */

    Assert((max != 0) && (min != 0) && (max >= min));

    /* Lock the stack */
    OS_LockStack();

    status = MeCheckModeChange(remDev, BOP_PARK_MODE);
    if (status == BT_STATUS_SUCCESS) {
        /* Start the operation */
        remDev->op.opType = BOP_PARK_MODE;
        remDev->parms.mode.max = max;
        remDev->parms.mode.min = min;

        Assert(IsEntryAvailable(&(remDev->op.node)));
        MeAddOperation(remDev);

        /* Attempt to start the operation */
        MeStartOperation();
        status = BT_STATUS_PENDING;
    }

    /* Unlock the stack */
    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            ME_StopPark()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Take the link out of park mode. 
 *
 * Return:    status of operation. 
 */
BtStatus ME_StopPark(BtRemoteDevice *remDev)
{
    BtStatus status;

    /* Lock the stack */
    OS_LockStack();

    status = MeCheckModeChange(remDev, BOP_EXIT_PARK_MODE);
    if (status == BT_STATUS_SUCCESS) {
        /* Start the operation */
        remDev->op.opType = BOP_EXIT_PARK_MODE;

        Assert(IsEntryAvailable(&(remDev->op.node)));
        MeAddOperation(remDev);

        /* Attempt to start the operation */
        MeStartOperation();
        status = BT_STATUS_PENDING;
    }

    /* Unlock the stack */
    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            ME_StartParkAll()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Put all links into park mode. 
 *
 * Return:    status of operation. 
 */
BtStatus ME_StartParkAll(U16 max, U16 min, U8 *num)
{
    U8              i, n = 0;
    BtStatus        status = BT_STATUS_FAILED;

    Assert(num);

    /* Lock the stack */
    OS_LockStack();

    /* Make sure the stack does not have an HCI initialization error */
    if ((bt.me.stackState) >= BTSS_INITIALIZE_ERR) {
        status = BT_STATUS_HCI_INIT_ERR;
        goto unlock;
    }

    /* Go through the RemoteDevice array looking for connected devices. */
    for (i = 0; i < NUM_BT_DEVICES; i++) {
        if ((bt.me.devTable)[i].state == BDS_CONNECTED) {
            /* Let the main function do all the error checking. */
            status = ME_StartPark((bt.me.devTable)+i, max, min);
            if (status == BT_STATUS_PENDING)
                n++;
        }
    }

unlock:
    /* Unlock the stack */
    OS_UnlockStack();

    if ((*num = n) > 0) {
        return BT_STATUS_PENDING;
    }
    return status;
}

/*---------------------------------------------------------------------------
 *            ME_StopParkAll()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Take all links out of park mode. 
 *
 * Return:    status of operation. 
 */
BtStatus ME_StopParkAll(U8 *num)
{
    U8              i, n = 0;
    BtStatus        status = BT_STATUS_FAILED;

    Assert(num);

    /* Lock the stack */
    OS_LockStack();

    /* Make sure the stack does not have an HCI initialization error */
    if ((bt.me.stackState) >= BTSS_INITIALIZE_ERR) {
        status = BT_STATUS_HCI_INIT_ERR;
        goto unlock;
    }

    /* Go through the RemoteDevice array looking for connected devices. */
    for (i = 0; i < NUM_BT_DEVICES; i++) {
        if (((bt.me.devTable)[i].state == BDS_CONNECTED) &&
            ((bt.me.devTable)[i].mode == BLM_PARK_MODE)) {
            /* Let the main function do all the error checking. */
            status = ME_StopPark((bt.me.devTable)+i);
            if (status == BT_STATUS_PENDING)
                n++;
            else if (status != BT_STATUS_IN_PROGRESS)
                break;
        }
    }

unlock:
    /* Unlock the stack */
    OS_UnlockStack();

    if ((*num = n) > 0) {
        return BT_STATUS_PENDING;
    }
    return status;
}


/*---------------------------------------------------------------------------
 *            ME_SwitchRole()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Switch the role this device is performing. 
 *
 * Return:    status of operation. 
 */
BtStatus ME_SwitchRole(BtRemoteDevice *remDev)
{
    BtStatus status;

    /* Lock the stack */
    OS_LockStack();

    status = MeCheckModeChange(remDev, BOP_SWITCH_ROLE);
    if (status == BT_STATUS_SUCCESS) {
        /* Start the operation */
        remDev->op.opType = BOP_SWITCH_ROLE;

        Assert(IsEntryAvailable(&(remDev->op.node)));
        MeAddOperation(remDev);

        /* Attempt to start the operation */
        MeStartOperation();
        status = BT_STATUS_PENDING;
    }

    /* Unlock the stack */
    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *             ME_SetClassOfDevice()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Set the class of device. 
 *
 * Return:    status of operation. 
 */
BtStatus ME_SetClassOfDevice(BtClassOfDevice classOfDevice)
{
    BtClassOfDevice cod;

    /* Lock the stack */
    OS_LockStack();
    
    /* Always attempt to set the Class of Device
     * MeWriteClassOfDevice will check for HCI initialization error
     */
    cod = ((bt.me.classOfDevice) | (bt.me.sdpServCoD));
    (bt.me.classOfDevice) = classOfDevice;

    if (cod != ((bt.me.classOfDevice) | (bt.me.sdpServCoD))) {
        MeWriteClassOfDevice();
    }

    /* Unlock the stack */
    OS_UnlockStack();

    return BT_STATUS_SUCCESS;
}

/*---------------------------------------------------------------------------
 *            ME_GetChannelClassification()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Get the current classification of channels stored in ME. 
 *
 * Return:    status of operation. 
 */
BtStatus ME_GetChannelClassification(BtChannelClass *channelClass)
{
    /* Lock the stack */
    OS_LockStack();

    OS_MemCopy(channelClass->map, (bt.me.channelClass.map), 10);

    /* Unlock the stack */
    OS_UnlockStack();

    return BT_STATUS_SUCCESS;
}

/*---------------------------------------------------------------------------
 *            ME_SetChannelClassification()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Set the classification of channels for AFH. 
 *
 * Return:    status of operation. 
 */
BtStatus ME_SetChannelClassification(BtChannelClass *channelClass, BOOL autoMode)
{
    BtStatus status = BT_STATUS_IN_PROGRESS;

    /* Lock the stack */
    OS_LockStack();

    if (((bt.me.stackState) < BTSS_INITIALIZE_ERR) && 
        ((bt.me.channelOp).opType == BOP_NOP)) {

        OS_MemCopy((bt.me.channelClass.map), channelClass->map, 10);
        (bt.me.channelOp).opType = BOP_CHANNEL_CLASS;
        Assert(IsEntryAvailable(&((bt.me.channelOp).node)));
        InsertTailList(&(bt.me.opList), &((bt.me.channelOp).node));

        if (((bt.me.autoMode) == 0 && autoMode == TRUE) ||
            ((bt.me.autoMode) == 1 && autoMode == FALSE) ||
            ((bt.me.autoMode) == 0xff)) {
            (bt.me.autoMode) = (U8)((autoMode == TRUE) ? 1 : 0);
            /* Schedule the operation */
            (bt.me.autoMode) |= 0x80;
        }
        else {
            /* Make sure the operation is not scheduled */
            (bt.me.autoMode) &= 0x7f;
        }

        /* Attempt to start the operation, it may not be in progress. */
        MeStartOperation();
        status = BT_STATUS_SUCCESS;
    }

    /* Unlock the stack */
    OS_UnlockStack();

    return status;
}


/*---------------------------------------------------------------------------
 *            ME_MarkWiFiChannel()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Translate WiFi channel to BT channels for AFH. 
 *
 * Return:    status of operation. 
 */
BtStatus ME_MarkWiFiChannel(BtChannelClass *channelClass, U8 wifiChannel,
                           BOOL state, BOOL initialize)
{
    U8 i, count;

    if (wifiChannel < 1 || wifiChannel > 14) {
        return BT_STATUS_INVALID_PARM;
    }

    /* Calculate the 1st bit and count to be set in the map */
    i = (U8)((wifiChannel-1) * 5);
    count = (U8)min(79 - i, 25);
    return ME_MarkAfhChannels(channelClass, i, count, state, initialize);
}

/*---------------------------------------------------------------------------
 *            ME_MarkAfhChannels()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Mark a range of BT channels for AFH. 
 *
 * Return:    status of operation. 
 */
BtStatus ME_MarkAfhChannels(BtChannelClass *channelClass, U8 begin, U8 count,
                            BOOL state, BOOL initialize)
{
    U8 *map = channelClass->map;
    U8  bit;

    if (begin > 78 || (begin + count) > 79) {
        return BT_STATUS_INVALID_PARM;
    }

    if (initialize) {
        OS_MemSet(map, 0xff, 10);
    }

    /* Initialize only? */
    if (count == 0) {
        /* Yes */
        goto done;
    }

    /* Adjust count if too long */
    count = (U8)min(count, (79 - begin));

    /* Set the first bits in the map */
    map = channelClass->map + (begin / 8);
    begin %= 8;
    if (begin != 0) {
        bit = 8;
        if (count < (8 - begin)) {
            bit = (U8)(8 - begin);
        }
        *map = (U8)MeSetBits(*map, bit, min((U8)(8 - begin), count), state);
        map++;
        if (count <= (8 - begin)) {
            /* The bits were all in this byte */
            goto done;
        }
        count = (U8)(count - (8 - begin));
    }

    /* Set the middle bits of the map */
    if (count > 8) {
        OS_MemSet(map, (U8)(state ? 0xff : 0x00), count/8);
    }

    /* Set the last bits in the map */
    map += (count / 8);
    begin = (U8)(count % 8);
    if (begin != 0) {
        *map = (U8)MeSetBits(*map, begin, begin, state);
    }

done:
    /* Make sure the MSB is 0 */
    channelClass->map[9] &= 0x7f;

    return BT_STATUS_SUCCESS;
}


/*---------------------------------------------------------------------------
 *           ME_SendHciCommandSync()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Send an HCI command synchronously. 
 *
 * Return:    status of the operation.
 */
BtStatus ME_SendHciCommandSync(MeCommandToken *token)
{
    token->flags = 0;
    return ME_SendHciCommand(token);
}


/*---------------------------------------------------------------------------
 *           ME_SendHciCommandAsync()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Send an HCI command asynchronously. 
 *
 * Return:    status of the operation.
 */
BtStatus ME_SendHciCommandAsync(MeCommandToken *token)
{
    token->flags = MCTF_ASYNC;
    return ME_SendHciCommand(token);
}


/*---------------------------------------------------------------------------
 *           ME_SendHciCommand()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Send an HCI command synchronously. 
 *
 * Return:    status of the operation.
 */
BtStatus ME_SendHciCommand(MeCommandToken *token)
{
    BtStatus status;

    /* Lock the stack */
    OS_LockStack();

#if XA_ERROR_CHECK == XA_ENABLED
    status = BT_STATUS_FAILED;
    if (token == 0) {
        goto done;
    }

    if ((token->callback == 0) || IsNodeOnList(&(bt.me.opList), &(token->op.node)) ||
        IsNodeOnList(&(bt.me.cmdList), &(token->op.node)) ||
        IsNodeOnList(&(bt.me.asyncList), &(token->op.node))) {
        goto done;
    }

#endif /* XA_ERROR_CHECK */

    Assert(token != 0);
    Assert((token->callback != 0) && 
           !IsNodeOnList(&(bt.me.opList), &(token->op.node)) &&
           !IsNodeOnList(&(bt.me.cmdList), &(token->op.node)));

    /* Make sure the stack does not have an HCI initialization error */
    if ((bt.me.stackState) >= BTSS_INITIALIZE_ERR) {
        status = BT_STATUS_HCI_INIT_ERR;
        goto done;
    }

    /* Check the length of the parameters */
    if ((token->p.general.in.parmLen > HCI_CMD_PARM_LEN) ||
        ((token->p.general.in.parmLen > 0) &&
        (token->p.general.in.parms == 0))) {
        status = BT_STATUS_INVALID_PARM;
        goto done;
    }

    if (MeIsValidGeneralToken(token) == FALSE) {
        status = BT_STATUS_INVALID_TYPE;
        goto done;
    }

    MeCheckRemDevToken(token);
    token->op.opType = BOP_GEN_COMMAND;
    token->eType = BTEVENT_COMMAND_COMPLETE;
    status = BT_STATUS_PENDING;

    /* If this is an async command and a command with the same event
     * already exists then put this one on the async queue.
     */
    if (token->flags & MCTF_ASYNC) {
        if ((MeFindMatchingToken(token, &(bt.me.opList)) != 0) ||
            (MeFindMatchingToken(token, &(bt.me.cmdList)) != 0)) {
            InsertTailList(&(bt.me.asyncList), &(token->op.node));
            goto done;
        }
    }
    /* Start up an operation */
    MeAddOperation(token);
    MeStartOperation();

done:
    /* Unlock the stack */
    OS_UnlockStack();
    return status;
}


/*---------------------------------------------------------------------------
 *            ME_GetHciConnectionHandle()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Get the HCI connection handle. 
 *
 * Return:    The handle. 
 */
U16 ME_GetHciConnectionHandle(BtRemoteDevice *rm)
{
    U16 handle;

    Assert(rm != 0);

    /* Lock the stack */
    OS_LockStack();
    
    handle = rm->hciHandle;

    /* Unlock the stack */
    OS_UnlockStack();

    return handle;
}

/*---------------------------------------------------------------------------
 *            ME_GetBdAddr()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Get the BdAddr. 
 *
 * Return:    void 
 */
void ME_GetBdAddr(BtRemoteDevice *rm, BD_ADDR *bdAddr)
{
    Assert(rm != 0);
    Assert(bdAddr != 0);

    /* Lock the stack */
    OS_LockStack();
    
    *bdAddr = rm->bdAddr;

    /* Unlock the stack */
    OS_UnlockStack();
}


/*---------------------------------------------------------------------------
 *            ME_SetDefaultLinkPolicy()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Set the default link policy setting. 
 *
 * Return:    void 
 */
BtStatus ME_SetDefaultLinkPolicy(BtLinkPolicy inACL, BtLinkPolicy outACL)
{
    BtLinkPolicy    saveInAcl;

#if XA_ERROR_CHECK == XA_ENABLED
    if ((inACL & BLP_MASK) || (outACL & BLP_MASK)) {
        return BT_STATUS_FAILED;
    }
#endif

    Assert(!(inACL & BLP_MASK) && !(outACL & BLP_MASK));

    /* Lock the stack */
    OS_LockStack();

    saveInAcl = (bt.me.inAclPolicy);
    (bt.me.inAclPolicy)  = inACL;
    (bt.me.outAclPolicy) = outACL;

    if (((bt.me.btVersion) >= 2) && (saveInAcl != inACL)) {
         MeWriteDefaultLinkPolicy();
    }

    /* Unlock the stack */
    OS_UnlockStack();

    return BT_STATUS_SUCCESS;
}

/*---------------------------------------------------------------------------
 *            ME_GetStackInitState()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Gets the stack/radio initialization state.
 *
 * Return:    BtStackState
 */
BtStackState ME_GetStackInitState(void)
{
    return (bt.me.stackState);
}

/*---------------------------------------------------------------------------
 *            ME_GetBtVersion()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Gets the Bluetooth version of the local radio.
 *
 * Return:    Version number
 */
U8 ME_GetBtVersion(void)
{
    return (bt.me.btVersion);
}

/*---------------------------------------------------------------------------
 *            ME_GetBtFeatures()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Gets the value of the specified byte of the Bluetooth features
 *            bitmask from the local radio.
 *
 * Return:
 *     Value of the specified byte of the BT features bitmask.
 */
U8 ME_GetBtFeatures(U8 byte)
{
    if (byte > 7) return 0;

    return (bt.me.btFeatures)[byte];
}

/*---------------------------------------------------------------------------
 *            ME_HasPendingTask()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Determines if operations are pending.
 *
 * Return:    Pending HCI event associated with the current HCI command
 *            or 0 for none.
 */
U16 ME_HasPendingTask(void)
{
    if ((bt.me.curTask) == 0) {
        goto done;
    }

    switch ((bt.me.curTask)) {
    case HCC_INQUIRY_CANCEL:
    case HCC_START_PERIODIC_INQ_MODE:
    case HCC_EXIT_PERIODIC_INQ_MODE:
    case HCC_LINK_KEY_REQ_REPL:
    case HCC_LINK_KEY_REQ_NEG_REPL:
    case HCC_PIN_CODE_REQ_REPL:
    case HCC_PIN_CODE_REQ_NEG_REPL:
    case HCC_ROLE_DISCOVERY:
    case HCC_READ_LINK_POLICY:
    case HCC_WRITE_LINK_POLICY:
    case HCC_SET_EVENT_MASK:
    case HCC_RESET:
    case HCC_EVENT_FILTER:
    case HCC_FLUSH:
    case HCC_READ_PIN_TYPE:
    case HCC_WRITE_PIN_TYPE:
    case HCC_CREATE_NEW_UNIT_KEY:
    case HCC_READ_STORED_LINK_KEY:
    case HCC_WRITE_STORED_LINK_KEY:
    case HCC_DEL_STORED_LINK_KEY:
    case HCC_CHNG_LOCAL_NAME:
    case HCC_READ_LOCAL_NAME:
    case HCC_READ_CONN_ACCEPT_TIMEOUT:
    case HCC_WRITE_CONN_ACCEPT_TIMEOUT:
    case HCC_READ_PAGE_TIMEOUT:
    case HCC_WRITE_PAGE_TIMEOUT:
    case HCC_READ_SCAN_ENABLE:
    case HCC_WRITE_SCAN_ENABLE:
    case HCC_READ_PAGE_SCAN_ACTIVITY:
    case HCC_WRITE_PAGE_SCAN_ACTIVITY:
    case HCC_READ_INQ_SCAN_ACTIVITY:
    case HCC_WRITE_INQ_SCAN_ACTIVITY:
    case HCC_READ_AUTH_ENABLE:
    case HCC_WRITE_AUTH_ENABLE:
    case HCC_READ_ENCRYPT_MODE:
    case HCC_WRITE_ENCRYPT_MODE:
    case HCC_READ_CLASS_OF_DEVICE:
    case HCC_WRITE_CLASS_OF_DEVICE:
    case HCC_READ_VOICE_SETTING:
    case HCC_WRITE_VOICE_SETTING:
    case HCC_READ_AUTO_FLUSH_TIMEOUT:
    case HCC_WRITE_AUTO_FLUSH_TIMEOUT:
    case HCC_READ_NUM_BC_RETRANSMIT:
    case HCC_WRITE_NUM_BC_RETRANSMIT:
    case HCC_READ_HOLD_MODE_ACTIVITY:
    case HCC_WRITE_HOLD_MODE_ACTIVITY:
    case HCC_READ_XMIT_POWER_LEVEL:
    case HCC_READ_SCO_FC_ENABLE:
    case HCC_WRITE_SCO_FC_ENABLE:
    case HCC_SET_CTRLR_TO_HOST_FLOW_CTRL:
    case HCC_HOST_BUFFER_SIZE:
    case HCC_READ_LINK_SUPERV_TIMEOUT:
    case HCC_WRITE_LINK_SUPERV_TIMEOUT:
    case HCC_READ_NUM_IAC:
    case HCC_READ_CURRENT_IAC_LAP:
    case HCC_WRITE_CURRENT_IAC_LAP:
    case HCC_READ_PAGE_SCAN_PERIOD_MODE:
    case HCC_WRITE_PAGE_SCAN_PERIOD_MODE:
    case HCC_READ_PAGE_SCAN_MODE:
    case HCC_WRITE_PAGE_SCAN_MODE:
    case HCC_READ_LOCAL_VERSION:
    case HCC_READ_LOCAL_FEATURES:
    case HCC_READ_BUFFER_SIZE:
    case HCC_READ_COUNTRY_CODE:
    case HCC_READ_BD_ADDR:
    case HCC_READ_FAILED_CONTACT_COUNT:
    case HCC_RESET_FAILED_CONTACT_COUNT:
    case HCC_GET_LINK_QUALITY:
    case HCC_READ_RSSI:
    case HCC_READ_LOOPBACK_MODE:
    case HCC_WRITE_LOOPBACK_MODE:
    case HCC_ENABLE_DUT:
        return HCE_COMMAND_COMPLETE;

    case HCC_INQUIRY:
        return HCE_INQUIRY_COMPLETE;

    case HCC_CREATE_CONNECTION:
    case HCC_ADD_SCO_CONNECTION:
    case HCC_ACCEPT_CON_REQ:
    case HCC_REJECT_CON_REQ:
        return HCE_CONNECT_COMPLETE;

    case HCC_DISCONNECT:
        return HCE_DISCONNECT_COMPLETE;

    case HCC_CHNG_CONN_PACKET_TYPE:
        return HCE_CONN_PACKET_TYPE_CHNG;

    case HCC_AUTH_REQ:
        return HCE_AUTH_COMPLETE;

    case HCC_SET_CONN_ENCRYPT:
        return HCE_ENCRYPT_CHNG;

    case HCC_CHNG_CONN_LINK_KEY:
        return HCE_CHNG_CONN_LINK_KEY_COMPLETE;

    case HCC_MASTER_LINK_KEY:
        return HCE_MASTER_LINK_KEY_COMPLETE;

    case HCC_REM_NAME_REQ:
        return HCE_REMOTE_NAME_REQ_COMPLETE;

    case HCC_READ_REMOTE_FEATURES:
        return HCE_READ_REMOTE_FEATURES_COMPLETE;

    case HCC_READ_REMOTE_VERSION:
        return HCE_READ_REMOTE_VERSION_COMPLETE;

    case HCC_READ_CLOCK_OFFSET:
        return HCE_READ_CLOCK_OFFSET_COMPLETE;

    case HCC_HOLD_MODE:
    case HCC_SNIFF_MODE:
    case HCC_EXIT_SNIFF_MODE:
    case HCC_PARK_MODE:
    case HCC_EXIT_PARK_MODE:
        return HCE_MODE_CHNG;

    case HCC_QOS_SETUP:
        return HCE_QOS_SETUP_COMPLETE;

    case HCC_SWITCH_ROLE:
        return HCE_ROLE_CHANGE;

    case HCC_HOST_NUM_COMPLETED_PACKETS:
    default:
        return 0xFF00;
    }

done:
    return 0;
}

/*---------------------------------------------------------------------------
 *            ME_RadioShutdownTime()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Gracefully shuts down the radio module, including the
 *            transports and the HCI.
 *
 * Returns:
 *     BT_STATUS_SUCCESS - The radio is down.  The application will be notified
 *         during the call that the shutdown was complete with 
 *         BTEVENT_HCI_DEINITIALIZED.
 *     BT_STATUS_FAILED - The radio is already down.
 *     BT_STATUS_PENDING - The radio will be shut down.  The application will 
 *        be notified when shutdown is complete with BTEVENT_HCI_DEINITIALIZED.
 */
BtStatus ME_RadioShutdownTime(TimeT time)
{
    U16 event;

    /* Stop operation processing. */
    (bt.me.stackState) = BTSS_DEINITIALIZE;

    /* Check for pending operations. */
    if ((time > 0) && (event = ME_HasPendingTask()) != 0) {
        Report(("RADIO: Waiting for event %x\n", event));
        (bt.me.pendingEvent) = event;
        (bt.me.timer).context = (void *)(time/50);
        (bt.me.timer).func = MePendingCommandHandler;
        EVM_StartTimer(&(bt.me.timer), 50);
        return BT_STATUS_PENDING;
    }

    /* Now we are ready to take down HCI and transports. */
    return RMGR_RadioShutdown();
}
