/***************************************************************************
 *
 * File:
 *     $Workfile:mesec.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:74$
 *
 * Description:
 *     This is file contains the code for the Bluetooth Security
 *     Manager portion of the Management Entity.
 *
 * Created:
 *     January 31, 2000
 *
 * Copyright 1999-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "btalloc.h"
#include "sys/hci.h"
#include "utils.h"
#include "sys/l2capxp.h"
#include "sys/sdpi.h"
#include "sys/rfcexp.h"
#include "ddb.h"

/****************************************************************************
 *
 * Macros
 *
 ****************************************************************************/

/* Macro for accessing a RemoteDevice structure given the curOp */

#define RDEV ((BtRemoteDevice*)(bt.me.curOp))
#define SECOP ((BtSecurityOp*)(bt.me.curOp))

/****************************************************************************
 *
 * Function prototypes
 *
 ****************************************************************************/

static void HandleSecurityCommandComplete(U8 event, U8  len, U8* parm);
static void StartLinkKeyRspTask(void);
static void StartPinCodeRspTask(void);
static void StartAuthenticateTask(void);
static void StartEncryptionTask(void);
static void StartWriteAuthenticateEnableTask(void);
static void StartWriteEncryptModeTask(void);
#if BT_SECURITY == XA_DISABLED
static void StartDenyPinCodeRspTask(void);
#endif

#if BT_SECURITY == XA_ENABLED
/****************************************************************************
 *
 * Functions
 *
 ****************************************************************************/

/*---------------------------------------------------------------------------
 *            SecHandleAuthenticateOp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle the authenticate operation.
 *
 * Return:    void
 */
void SecHandleAuthenticateOp(BtOpEvent event)
{
    switch (event) {
    case BOE_START:
        StartAuthenticateTask();
        return;

    case BOE_TASK_END:
        /* The operation is done so clear the op type */
        SECOP->op.opType = BOP_NOP;

        /* This is the connection status event */
        if ((bt.me.btEvent).errCode != 0) {
            SecReportAuthenticateResult(SECOP->remDev);
        } else {
            /* We are in the process of authentication. We expect to get
             * a request for a link key so set the authState accordingly 
             */
            SECOP->remDev->authState = BAS_WAITING_KEY_REQ;
        }
        break;

    default:
        Assert(0);
        break;
    }

    /* Start another operation if one exists */
    MeOperationEnd();

}

/*---------------------------------------------------------------------------
 *            SecHandleLinkKeyRspOp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle the Link key response operation.
 *
 * Return:    void
 */
void SecHandleLinkKeyRspOp(BtOpEvent event)
{
    switch (event) {
    case BOE_START:
        StartLinkKeyRspTask();
        return;

    case BOE_TASK_END:
        if ((bt.me.btEvent).errCode != 0) {
            /* Clear authentication state */
            SECOP->remDev->authState = BAS_NOT_AUTHENTICATED;
        }

        /* The operation is done so clear the op type */
        SECOP->op.opType = BOP_NOP;
        break;

    default:
        Assert(0);
        break;
    }

    /* Start another operation if one exists */
    MeOperationEnd();

}

/*---------------------------------------------------------------------------
 *            SecHandlePinCodeRspOp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle the Link key response operation.
 *
 * Return:    void
 */
void SecHandlePinCodeRspOp(BtOpEvent event)
{
    switch (event) {
    case BOE_START:
        StartPinCodeRspTask();
        return;

    case BOE_TASK_END:
        /* The operation is done so clear the op type */
        SECOP->op.opType = BOP_NOP;
        break;

    default:
        Assert(0);
        break;
    }

    /* Start another operation if one exists */
    MeOperationEnd();

}

/*---------------------------------------------------------------------------
 *            SecHandleAuthorizeOp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle the Authorize request operation.
 *
 * Return:    void
 */
void SecHandleAuthorizeOp(BtOpEvent event)
{
    SECOP->op.opType = BOP_NOP;

    switch (event) {
    case BOE_START:
        (bt.me.btEvent).errCode = BEC_NO_ERROR;
        break;

    case BOE_TASK_ERR:
        (bt.me.btEvent).errCode = BEC_REQUEST_CANCELLED;
        break;

    case BOE_TASK_END:
        goto done;

    default:
        Assert(0);
    }

    /* Call the authorize handler if it exists */
    if ((bt.me.authorizeHandler) != 0) {
        Report(("MeSec: Calling authorization handler\n"));
        SecHandleAuthorizeRequest(SECOP->remDev);
    } else {
        /* No handler so were done. Process all tokens */
        Report(("MeSec: No authorization handler\n"));
        SECOP->remDev->secAccessState |= BAS_AUTHORIZE_COMPLETE;
        SECOP->remDev->authorizeState = BAS_NOT_AUTHORIZED;
        SecProcessAllSecurityTokens(SECOP->remDev);
    }

done:
    /* Start another operation if one exists */
    MeOperationEnd();

}

/*---------------------------------------------------------------------------
 *            SecHandleAuthorizeRequest()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Indicate the Authorize request operation.
 *
 * Return:    void
 */
void SecHandleAuthorizeRequest(BtRemoteDevice *remDev)
{
    if ((bt.me.btEvent).errCode != BEC_NO_ERROR) {
        /* Got some error during authorization, restart. */
        remDev->secAccessState |= BAS_AUTH_COMPLETE;
        remDev->authorizeState = BAS_NOT_AUTHORIZED;
    }

    if ((bt.me.authorizeHandler) != 0) {
        (bt.me.btEvent).eType = BTEVENT_AUTHORIZATION_REQ;
        (bt.me.btEvent).p.remDev = remDev;
        (bt.me.btEvent).handler = (bt.me.authorizeHandler);
        (bt.me.authorizeHandler)->callback(&(bt.me.btEvent));
    }
}

/*---------------------------------------------------------------------------
 *            SecHandleEncryptOp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle the Encryption request operation.
 *
 * Return:    void
 */
void SecHandleEncryptOp(BtOpEvent event)
{
    BtRemoteDevice *remDev;

    switch (event) {
    case BOE_START:
        StartEncryptionTask();
        return;

    case BOE_TASK_END:
        /* The operation is done so clear the op type */
        SECOP->op.opType = BOP_NOP;
        remDev = SECOP->remDev;

        /* Start another operation if one exists */
        MeOperationEnd();

        /* This is the connection status event */
        if ((bt.me.btEvent).errCode != 0) {
            SecReportEncryptionResult(remDev, 0x00);
        } 
        break;

    default:
        Assert(0);
        break;
    }
}


/*---------------------------------------------------------------------------
 *            SecHandleSecurityModeOp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle the authenticate operation.
 *
 * Return:    void
 */
void SecHandleSecurityModeOp(BtOpEvent event)
{
    switch (event) {
    case BOE_START:
        /* Check if this is an encryption only operation */
        if (SECOP->x.secMode.mode == BSM_SEC_ENCRYPT) {
            /* It is, so set the encryption mode only */
            SECOP->x.secMode.mode = BSM_SEC_LEVEL_3;
            (bt.me.secModeState) = BSMS_START_LEVEL3;
            (bt.me.opState) = 1;
            StartWriteEncryptModeTask();
            return;
        }
        (bt.me.opState) = 0;
        StartWriteAuthenticateEnableTask();
        return;

    case BOE_TASK_END:
        if ((bt.me.opState) == 0) {
            /* Authentication enable is complete. See if it was successful */
            if ((bt.me.btEvent).errCode == 0) {
                /* It was successful so see if we need to set encryption mode */
                if (SECOP->x.secMode.encrypt == TRUE) {
                    /* Set the encryption mode */
                    (bt.me.opState) = 1;
                    StartWriteEncryptModeTask();
                    return;
                }
            }
        }
        /* If we get here then we need to report the results. First deal with the result
         * of setting the encryption state
         */
        if ((bt.me.opState) == 1) {
            /* We just finished the encryption mode task. If its was successful
             * then set the global flag. Make sure that the errCode field is set
             * to indication success
             */
            if ((bt.me.btEvent).errCode != 0) {
                /* Failed to set the encryption mode */
                Report(("MecSec: Error setting encryption mode. Err = %d\n", 
                       (bt.me.btEvent).errCode));
                (bt.me.btEvent).errCode = 0;
            } else {
                /* Encrypt mode operation was successful so set the flag appropriately */
                if ((SECOP->x.secMode.mode == BSM_SEC_LEVEL_3) && (SECOP->x.secMode.encrypt == TRUE)) {
                    (bt.me.secModeEncrypt) = TRUE;
                } else {
                    (bt.me.secModeEncrypt) = FALSE;
                }
            }
        }

        /* Report the results */
        SECOP->op.opType = BOP_NOP;
        SecReportSecurityModeResult();
        break;

    default:
        Assert(0);
        break;
    }

    /* Start another operation if one exists */
    MeOperationEnd();

}

/*---------------------------------------------------------------------------
 *            StartAuthenticateTask()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Start the link accept task.
 *
 * Return:    void
 */
static void StartAuthenticateTask(void)
{
    (bt.me.taskHandler) = MeHandleConnectionStatusOnly;
    (bt.me.taskEvent) = HCE_COMMAND_STATUS;

    /* Store the connection handle */
    StoreLE16((bt.me.hciGenCmd).parms, SECOP->remDev->hciHandle);

    Assert(IsEntryAvailable(&((bt.me.hciGenCmd).node)));
    Report(("MeSec: Starting Authentication_Requested command\n"));
    AssertEval(MeSendHciCommand(HCC_AUTH_REQ, 2) == BT_STATUS_PENDING);
}

/*---------------------------------------------------------------------------
 *            StartLinkKeyRspTask()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Start the link key response task.
 *
 * Return:    void
 */
static void StartLinkKeyRspTask(void)
{
    BtDeviceRecord record;

    /* Set the task handler */
    (bt.me.taskHandler) = HandleSecurityCommandComplete;
    (bt.me.taskEvent) = HCE_COMMAND_COMPLETE;

    /* Put in the BD_ADDR portion of the response */ 
    Assert(IsEntryAvailable(&((bt.me.hciGenCmd).node)));
    OS_MemCopy((bt.me.hciGenCmd).parms, SECOP->bdAddr.addr, 6);

    if (SECOP->remDev != 0) {
        /* We have a remote device so see if a link key exists */
        if (SEC_FindDeviceRecord(&(SECOP->bdAddr), &record) == BT_STATUS_SUCCESS) {
            /* Found a record, set the authentication state and check authorization. */
            SECOP->remDev->authState = BAS_AUTHENTICATED;
            SECOP->remDev->pairingType = BPT_SAVE_NOT_TRUSTED;
            /* Set the trusted flag based on the stored record */
            if (record.trusted) {
                SECOP->remDev->authorizeState = BAS_AUTHORIZED;
                SECOP->remDev->pairingType = BPT_SAVE_TRUSTED;
            }
            /* The link key was found so return it to the Host controller */
            OS_MemCopy((bt.me.hciGenCmd).parms+6, record.linkKey, 16);
            Report(("MeSec: Key exists - sending Link Key Request Reply command\n"));
            AssertEval(MeSendHciCommand(HCC_LINK_KEY_REQ_REPL, 22) == BT_STATUS_PENDING);
            return;
        } else {
            /* We need a pin code so respond in the negative */
            Report(("MeSec: No link key - need a pin code\n"));
        }
    } 

    /* We need to respond in the negative */
    Report(("MeSec: Sending Link Key Request Negative Reply command\n"));
    AssertEval(MeSendHciCommand(HCC_LINK_KEY_REQ_NEG_REPL, 6) == BT_STATUS_PENDING);
}

/*---------------------------------------------------------------------------
 *            StartPinCodeRspTask()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Start the Pin Code response task.
 *
 * Return:    void
 */
static void StartPinCodeRspTask(void)
{
    /* Set the task handler */
    (bt.me.taskHandler) = HandleSecurityCommandComplete;
    (bt.me.taskEvent) = HCE_COMMAND_COMPLETE;

    /* Put in the BD_ADDR portion of the response */ 
    Assert(IsEntryAvailable(&((bt.me.hciGenCmd).node)));
    OS_MemCopy((bt.me.hciGenCmd).parms, SECOP->bdAddr.addr, 6);

    if (SECOP->remDev != 0) {
        /* We have a remote device so send the pin code */
        Report(("MeSec: Sending Pin Code Request Reply command\n"));
        Assert(SECOP->x.pLen > 0);

        Assert((SECOP->remDev->authState == BAS_WAITING_FOR_PIN) ||
               (SECOP->remDev->authState == BAS_WAITING_FOR_PIN_R));

        /* Set the state accordingly */
        if (SECOP->remDev->authState == BAS_WAITING_FOR_PIN) {
            SECOP->remDev->authState = BAS_WAITING_FOR_KEY;
        } else {
            SECOP->remDev->authState = BAS_WAITING_FOR_KEY_R;
        }

        (bt.me.hciGenCmd).parms[6] = SECOP->x.pLen;
        OS_MemCopy((bt.me.hciGenCmd).parms+7,SECOP->pin, SECOP->x.pLen);
        AssertEval(MeSendHciCommand(HCC_PIN_CODE_REQ_REPL, 23) == BT_STATUS_PENDING);
    } else {
        /* We need to respond in the negative */
        Report(("MeSec: Sending Pin Code Request Negative Reply command\n"));
        AssertEval(MeSendHciCommand(HCC_PIN_CODE_REQ_NEG_REPL, 6) == BT_STATUS_PENDING);
    }
}


/*---------------------------------------------------------------------------
 *            StartEncryptionTask()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Start the encryption task.
 *
 * Return:    void
 */
static void StartEncryptionTask(void)
{
    (bt.me.taskHandler) = MeHandleConnectionStatusOnly;
    (bt.me.taskEvent) = HCE_COMMAND_STATUS;

    /* Store the connection handle */
    StoreLE16((bt.me.hciGenCmd).parms, SECOP->remDev->hciHandle);
    (bt.me.hciGenCmd).parms[2] = SECOP->x.eMode;

    Assert(IsEntryAvailable(&((bt.me.hciGenCmd).node)));
    Report(("MeSec: Starting Set Connection Encryption command\n"));
    AssertEval(MeSendHciCommand(HCC_SET_CONN_ENCRYPT, 3) == BT_STATUS_PENDING);
}

/*---------------------------------------------------------------------------
 *            StartWriteAuthenticateEnableTask()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Start the write authenticat enable task.
 *
 * Return:    void
 */
static void StartWriteAuthenticateEnableTask(void)
{
    (bt.me.taskHandler) = HandleSecurityCommandComplete;
    (bt.me.taskEvent) = HCE_COMMAND_COMPLETE;

    /* Set the proper mode */
    if (SECOP->x.secMode.mode == BSM_SEC_LEVEL_3) {
        Report(("MeSec: Enabling Authentication for all connections\n"));
        (bt.me.hciGenCmd).parms[0] = 0x01;
    } else {
        Report(("MeSec: Disabling Authentication for all connections\n"));
        (bt.me.hciGenCmd).parms[0] = 0x00;
    }
    Assert(IsEntryAvailable(&((bt.me.hciGenCmd).node)));
    Report(("MeSec: Sending Authentication enable command\n"));
    AssertEval(MeSendHciCommand(HCC_WRITE_AUTH_ENABLE, 1) == BT_STATUS_PENDING);
}

/*---------------------------------------------------------------------------
 *            StartWriteEncryptModeTask()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Start the write encryption mode task.
 *
 * Return:    void
 */
static void StartWriteEncryptModeTask(void)
{
    (bt.me.taskHandler) = HandleSecurityCommandComplete;
    (bt.me.taskEvent) = HCE_COMMAND_COMPLETE;
    (bt.me.opState) = 1;

    /* Set the proper mode */
    if ((SECOP->x.secMode.mode == BSM_SEC_LEVEL_3) && (SECOP->x.secMode.encrypt == TRUE)) {
        Report(("MeSec: Enabling Encryption for all connections\n"));
        (bt.me.hciGenCmd).parms[0] = 0x01;
    } else {
        Report(("MeSec: Disabling Encryption for all connections\n"));
        (bt.me.hciGenCmd).parms[0] = 0x00;
    }
    Assert(IsEntryAvailable(&((bt.me.hciGenCmd).node)));
    Report(("MeSec: Sending encyrption mode command\n"));
    AssertEval(MeSendHciCommand(HCC_WRITE_ENCRYPT_MODE, 1) == BT_STATUS_PENDING);
}

/*---------------------------------------------------------------------------
 *            SecHandleLinkKeyReq()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle a link key request event 
 *
 * Return:    void
 */
void SecHandleLinkKeyReq(U8 len, U8* parm)
{
    BtRemoteDevice* remDev;
    BtSecurityOp*   secOp;

    UNUSED_PARAMETER(len);

    /* Get a security op to use */
    secOp = SecBtGetFreeSecurityOp();
    Assert(secOp != 0);

    /* Save the BD_ADDR */
    OS_MemCopy(secOp->bdAddr.addr, parm, 6);
    secOp->op.opType = BOP_LINK_KEY_RSP;
    MeAddOperationFront(secOp);

    /* Find the remote device associated with this request. */
    remDev = ME_FindRemoteDeviceP(parm);

    /* If we are in the process of performing security or already secure
     * then something is wrong so we should reply in the negative.
     * Setting secOp->remDev to 0 is the way to send a negative response.
     */
    secOp->remDev = 0;
    if (remDev != 0) {
        if (remDev->authState == BAS_WAITING_KEY_REQ) {
            /* We got the key request as part of our authentication so
             * change to next state
             */
            remDev->authState = BAS_SENDING_KEY;
        } else {
            /* We got the key request because of the remote device's 
             * authentication. We don't want to change states because
             * we don't consider the link to be in the process of authentication.
             */
            Report(("MeSec: remote device performing authentication\n"));
        }
        Report(("MeSec: Creating operation for link key response\n"));
        secOp->remDev = remDev;
    } else {
        Report(("MeSec: Link key req for invalid BD_ADDR - link key neg rsp\n"));
    }

    /* Attempt to start the operation */
    MeStartOperation();
}

/*---------------------------------------------------------------------------
 *            SecHandlePinCodeReq()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle a pin code request event 
 *
 * Return:    void
 */
void SecHandlePinCodeReq(U8 len, U8* parm)
{
    BtRemoteDevice* remDev;
    BtSecurityOp*   secOp;

    UNUSED_PARAMETER(len);

    Report(("MeSec: Received Pin Code Request Event\n"));

    /* Find the RemoteDevice that corresponds to the BD_ADDR. If we cannot
     * find one then respond in the negative
     */
    Assert(len >= 6);
    remDev = ME_FindRemoteDeviceP(parm);
    if (remDev != 0) {
        /* We need to call the pairing handler to get a Pin code.
         * If there is no pairing handler then respond in the negative */
        if ((bt.me.pairingHandler) != 0) {
            if (remDev->authState == BAS_WAITING_FOR_PIN_R) {
                /* Pairing is already in progress, continue waiting. */
                Report(("MeSec: ignoring second PIN request\n"));
                return;
            }

            Assert((remDev->authState == BAS_NOT_AUTHENTICATED) ||
                   (remDev->authState == BAS_WAITING_FOR_KEY_R) ||
                   (remDev->authState == BAS_AUTHENTICATED) ||
                   (remDev->authState == BAS_SENDING_KEY));

            if (remDev->authState == BAS_SENDING_KEY) { 
                /* Local request for authentication */
                remDev->authState = BAS_WAITING_FOR_PIN;
            } else {
                /* A remote device is doing pairing. We need to keep track
                 * of this state just it case it fails and we do not get
                 * an event indicated failure
                 */
                Report(("MeSec: Remote device performing pairing\n"));
                remDev->authState = BAS_WAITING_FOR_PIN_R;
            }

            /* Get the pin code */
            Report(("MeSec: Calling pairing handler for a pin code\n"));
            (bt.me.btEvent).eType = BTEVENT_PIN_REQ;
            (bt.me.btEvent).p.remDev = remDev;
            (bt.me.btEvent).handler = (bt.me.pairingHandler);
            MeDevEventHandler();
            (bt.me.pairingHandler)->callback(&(bt.me.btEvent));
            /* Start a disconnect that may be waiting */
            MeStartOperation();
            return;
        } else {
            /* Pairing is not allowed so set state to NOT_SECURE */
            Report(("MeSec: No pairing handler\n"));
            remDev->authState = BAS_NOT_AUTHENTICATED;
        }
    } else {
        Report(("MeSec: Remote device does not exist for pairing\n"));
    }

    /* Setup a security op to send the rejection */
    secOp = SecBtGetFreeSecurityOp();
    Assert(secOp != 0);

    /* Set the remDev to 0 indicating a negative response */
    secOp->remDev = 0;

    /* Copy in the BD_ADDR */
    OS_MemCopy(secOp->bdAddr.addr, parm, 6);

    secOp->op.opType = BOP_PIN_CODE_RSP;
    MeAddOperationFront(secOp);

    /* Attempt to start the operation */
    MeStartOperation();
}


/*---------------------------------------------------------------------------
 *            SecHandleAuthenticateComplete()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Start the link accept task.
 *
 * Return:    void
 */
void SecHandleAuthenticateComplete(U8  len, U8* parm)
{
    BtRemoteDevice* remDev;

    UNUSED_PARAMETER(len);

    /* Locate the remote device */
    remDev = MeMapHciToRemoteDevice((HciHandle) (LEtoHost16(parm+1) & 0x0fff));
    if (remDev != 0) {
        Report(("MeSec: Received Authenticate Complete event. err = %d\n", parm[0]));
        (bt.me.btEvent).errCode = parm[0];
        SecReportAuthenticateResult(remDev);
    } else {
        Report(("MeSec: Received Authenticate complete event - no remote device\n"));
    }

}

/*---------------------------------------------------------------------------
 *            SecHandleReturnLinkKeys()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle the HCI Inquiry_Result event. 
 *
 * Return:    void
 */
void SecHandleReturnLinkKeys(U8 len, U8* parm)
{
    U16             i;
    MeCommandToken *token;
    U8              tParms[3];

    UNUSED_PARAMETER(len);

    Assert(len > 1);
    Assert(len >= (U8) (parm[0] * 22));

    /* Returned one or more Link Keys. Report to clients */
    Report(("MeSec: num link keys = %d, bdaddr %x %x %x %x %x %x\n",
            parm[0], parm[1], parm[2], parm[3], parm[4], parm[5], parm[6]));

    OS_MemSet(tParms, 0, 3);
    for (i = 0; i < (U16)(parm[0]*22); i+=22) {
        /* Get the parameters */
        OS_MemCopy((bt.me.btEvent).p.bdLinkKey.bdAddr.addr,parm+1+i,6);
        OS_MemCopy((bt.me.btEvent).p.bdLinkKey.linkKey, parm+7+i, 16);

        /* Report this one to the clients */
        (bt.me.btEvent).errCode = 0;
        (bt.me.btEvent).eType = BTEVENT_RETURN_LINK_KEYS;

        /* Pass in parms of 0 to force match on future COMMAND_COMPLETE */
        token = MeFindGeneralEvent(HCE_COMMAND_COMPLETE, tParms);
        Assert(IsNodeOnList(&(bt.me.cmdList), &(token->op.node)));
        token->callback(&(bt.me.btEvent));
    }
}

/*---------------------------------------------------------------------------
 *            SecHandleLinkKeyNotify()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle a link key notify event. 
 *
 * Return:    void
 */
void SecHandleLinkKeyNotify(U8 len, U8* parm)
{
    BtRemoteDevice* remDev;
    BtDeviceRecord  record;
    BtStatus        status;

    Report(("MeSec: Link key notify event\n"));

    /* Find the remote device associate with this link key */
    Assert(len >= 22);
    remDev = ME_FindRemoteDeviceP(parm);
    if (remDev != 0) {
        /* Set the security state to authenticated */
        remDev->authState = BAS_AUTHENTICATED;
        status = BT_STATUS_SUCCESS;

        /* See if we need to store the link key. */
        if (remDev->pairingType != BPT_NOT_SAVED) {
            record.bdAddr = remDev->bdAddr;
            OS_MemCopy(record.linkKey, parm+6, 16);
            if (len > 22) {
                record.keyType = parm[22];
            }
            if (remDev->pairingType == BPT_SAVE_TRUSTED) {
                record.trusted = TRUE;
                remDev->authorizeState = BAS_AUTHORIZED;
                Report(("MeSec: Link key is saved. Device trusted\n"));
            } else {
                record.trusted = FALSE;
                remDev->authorizeState = BAS_NOT_AUTHORIZED;
                Report(("MeSec: Link key is saved. Device not trusted\n"));
            }
            /* Store the link key. */
            status = SEC_AddDeviceRecord(&record);
        } else {
            Report(("MeSec: Link is not saved\n"));
        }
        
        /* If we have a pairing handler then report the results */
        if ((bt.me.pairingHandler) != 0) {
            Report(("MeSec: Calling pairing handler indicating pairing complete\n"));
            (bt.me.btEvent).eType = BTEVENT_PAIRING_COMPLETE;
            (bt.me.btEvent).p.remDev = remDev;
            (bt.me.btEvent).handler = (bt.me.pairingHandler);

            /* Set the errCode appropriately */
            if (status == BT_STATUS_SUCCESS) {
                (bt.me.btEvent).errCode = BEC_NO_ERROR;
            } else {
                Report(("MeSec: Error saving link key\n"));
                (bt.me.btEvent).errCode = BEC_STORE_LINK_KEY_ERR;
            }

            /* Call the pairing handler */
            (bt.me.pairingHandler)->callback(&(bt.me.btEvent));
        } else {
            Report(("MeSec: No pairing handler to report pairing complete\n"));
        }
    } else {
        Report(("MeSec: No remote device so link key not saved\n"));
    }
}


/*---------------------------------------------------------------------------
 *            SecHandleEncryptChange()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle an encryption change event. 
 *
 * Return:    void
 */
void SecHandleEncryptChange(U8 len, U8* parm)
{
    BtRemoteDevice* remDev;

    UNUSED_PARAMETER(len);

    Assert(len >= 3);

    /* Locate the remote device */
    remDev = MeMapHciToRemoteDevice((HciHandle) (LEtoHost16(parm+1) & 0x0fff));
    if (remDev != 0) {
        Report(("MeSec: Received encryption change err = %d\n", parm[0]));
        (bt.me.btEvent).errCode = parm[0];
        SecReportEncryptionResult(remDev, parm[3]);
    } else {
        Report(("MeSec: Received encryption change event - no remote device\n"));
    }
}

/*---------------------------------------------------------------------------
 *            SecReportAuthenticateResult()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  
 *
 * Return:    void
 */
void SecReportAuthenticateResult(BtRemoteDevice* remDev)
{
    BtErrorCode errCode;
    U8          authState = BAS_NOT_AUTHENTICATED;

    /* Set the security state based on the result of the operation */
    if ((bt.me.btEvent).errCode == 0) {
        remDev->authState = BAS_AUTHENTICATED;
    } else {
        authState = remDev->authState;
        remDev->authState = BAS_NOT_AUTHENTICATED;
    }

    /* Process any security access tokens */
    remDev->secAccessState |= BAS_AUTH_COMPLETE;
    SecProcessAllSecurityTokens(remDev);

    if (((bt.me.pairingHandler) != 0) &&
        (authState == BAS_WAITING_FOR_PIN || authState == BAS_WAITING_FOR_PIN_R)) {
        /* Cancel the pin code request */
        Report(("MeSec: Canceling pin code request\n"));
        errCode = (bt.me.btEvent).errCode;
        (bt.me.btEvent).errCode = BEC_REQUEST_CANCELLED;
        (bt.me.btEvent).eType = BTEVENT_PIN_REQ;
        (bt.me.btEvent).p.remDev = remDev;
        (bt.me.btEvent).handler = (bt.me.pairingHandler);
        (bt.me.pairingHandler)->callback(&(bt.me.btEvent));
        (bt.me.btEvent).errCode = errCode;
    }

    /* Report the results to the handler */
    remDev->op.opType = BOP_NOP;
    (bt.me.btEvent).eType = BTEVENT_AUTHENTICATE_CNF;
    (bt.me.btEvent).p.remDev = remDev;

    if (remDev->authHandler != 0) {
        remDev->authHandler->callback(&((bt.me.btEvent)));
    }


    if ((bt.me.btEvent).errCode == 0) {
        /* Report the event to all bound handlers and all registered 
         * global handlers 
         */
        (bt.me.btEvent).eType = BTEVENT_AUTHENTICATED;
        MeCallLinkHandlers(remDev);
        MeReportResults(BEM_AUTHENTICATED);
    }
}

/*---------------------------------------------------------------------------
 *            SecReportEncryptionResult()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  
 *
 * Return:    void
 */
void SecReportEncryptionResult(BtRemoteDevice* remDev, U8 mode)
{
    /* Set the security state based on the result of the operation */
    if ((bt.me.btEvent).errCode == 0) {
        /* The encrypt operation was successful so set the state based on
         * the mode. We do this because we could get a mode change from the
         * other side.
         */
        if (mode == 0x00) {
            /* Mode indicated disabled */
            remDev->encryptState = BES_NOT_ENCRYPTED;
        } else {
            /* Mode indicated enabled */
            remDev->encryptState = BES_ENCRYPTED;
        }
    } else {
        /* The encrypt operation failed. So mode is meaningless. We will
         * use the previous state to determine the new state
         */
        if (remDev->encryptState == BES_START_ENCRYPT) {
            remDev->encryptState = BES_NOT_ENCRYPTED;
        } else {
            Assert(remDev->encryptState == BES_END_ENCRYPT);
            remDev->encryptState = BES_ENCRYPTED;
        }
    }

    /* Process any security access tokens. */
    remDev->secAccessState |= BAS_ENCRYPT_COMPLETE;
    SecProcessAllSecurityTokens(remDev);

    /* Report the results to the handler */
    remDev->op.opType = BOP_NOP;

    if (remDev->encryptHandler != 0) {
        (bt.me.btEvent).p.remDev = remDev;
        (bt.me.btEvent).eType = BTEVENT_ENCRYPT_COMPLETE;
        remDev->encryptHandler->callback(&((bt.me.btEvent)));
    }

    /* Call the global handlers if the operations was successful */
    if ((bt.me.btEvent).errCode == 0) {
        /* Report the event to all bound handlers and all registered 
         * global handlers 
         */
        (bt.me.btEvent).eType = BTEVENT_ENCRYPTION_CHANGE;
        (bt.me.btEvent).p.encrypt.remDev = remDev;
        (bt.me.btEvent).p.encrypt.mode = mode;

        MeCallLinkHandlers(remDev);
        MeReportResults(BEM_ENCRYPTION_CHANGE);
    }
}

/*---------------------------------------------------------------------------
 *            SecReportSecurityModeResult()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Report the results of changing security mode
 *
 * Return:    void
 */
void SecReportSecurityModeResult(void)
{
    BtHandler* handler;

    if ((bt.me.btEvent).errCode == 0) {
        /* The operation was successful so set the security mode state
         * properly.
         */
        if ((bt.me.secModeState) == BSMS_START_LEVEL3) {
            (bt.me.secModeState) = BSMS_LEVEL3;
            (bt.me.btEvent).p.secMode.mode = BSM_SEC_LEVEL_3;
        } else {
            Assert((bt.me.secModeState) == BSMS_END_LEVEL3);
            (bt.me.secModeState) = BSMS_LEVEL2;
            (bt.me.btEvent).p.secMode.mode = BSM_SEC_LEVEL_2;
        }
    } else {
        /* The operation ended in error so go back to our original
         * setting.
         */
        if ((bt.me.secModeState) == BSMS_START_LEVEL3) {
            (bt.me.secModeState) = BSMS_LEVEL2;
            (bt.me.btEvent).p.secMode.mode = BSM_SEC_LEVEL_2;
        } else {
            Assert((bt.me.secModeState) == BSMS_END_LEVEL3);
            (bt.me.secModeState) = BSMS_LEVEL3;
            (bt.me.btEvent).p.secMode.mode = BSM_SEC_LEVEL_3;
        }
    }

    /* Set the encryption mode flag */
    (bt.me.btEvent).p.secMode.encrypt = (bt.me.secModeEncrypt);

    /* Call the handler. Make sure it is 0 for future use */
    if ((bt.me.secModeHandler) != 0) {
        handler = (bt.me.secModeHandler);
        (bt.me.secModeHandler) = 0;
        (bt.me.btEvent).handler = handler;
        (bt.me.btEvent).eType = BTEVENT_SECURITY3_COMPLETE;
        handler->callback(&(bt.me.btEvent));
    }

    if ((bt.me.btEvent).errCode == 0) {
        /* Call the global handlers */
        (bt.me.btEvent).eType = BTEVENT_SECURITY_CHANGE;
        MeReportResults(BEM_SECURITY_CHANGE);
    }
}

/*---------------------------------------------------------------------------
 *            SecAddAuthenticateOpToQueue()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Add the authenticate operation to the operation queue.
 *
 * Return:    status of the operation
 */
BtStatus SecAddAuthenticateOpToQueue(BtRemoteDevice* remDev)
{
    BtSecurityOp* secOp;
    BtStatus      status;

    if (remDev->state != BDS_CONNECTED ||
        remDev->encryptState != BES_NOT_ENCRYPTED) {
        status = BT_STATUS_FAILED;
    } else {
        /* Get a security op */
        secOp = SecBtGetFreeSecurityOp();

        if (secOp == 0) {
            status = BT_STATUS_NO_RESOURCES;
        } else {
            secOp->remDev = remDev;

            /* Add the authentication operation to the queue */
            secOp->op.opType = BOP_AUTHENTICATE;

            /* We are not in the process of authenticating then
             * set the state appropriately. There are situations where
             * will run authentication event if we appear to be in the middle
             * of one.
             */
            if (remDev->authState == BAS_NOT_AUTHENTICATED) {
                remDev->authState = BAS_START_AUTHENTICATE;
            }

            Assert(IsEntryAvailable(&(secOp->op.node)));
            MeAddOperation(secOp);

            /* Attempt to start the operation */
            MeStartOperation();
            status = BT_STATUS_PENDING;
        }
    }
    return status;
}


/*---------------------------------------------------------------------------
 *            SecProcessAllSecurityTokens()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Process all the security tokens for the given remote device.
 *
 * Return:    status of the operation
 */
void SecProcessAllSecurityTokens(BtRemoteDevice* remDev)
{
    BtSecurityToken* token;
    BtSecurityToken* tmp;
    BtEventType      eType;

    token = (BtSecurityToken*) remDev->secTokens.Flink;
    while ((ListEntry*)token != &(remDev->secTokens)) {
        /* Point to the next token */
        tmp = (BtSecurityToken*)token->node.Flink;

        /* Process the token */
        Report(("MeSec: SecProcessAllSecurityTokens: processing a token\n"));
        switch (SecProcessSecurityToken(token)) {
        case BT_STATUS_SUCCESS:
            /* This token is complete and access is granted. Call the protocol 
             * callback to let it know.
             */
            eType = BTEVENT_ACCESS_APPROVED;
            goto reportit;

        case BT_STATUS_FAILED:
            /* This token is complete and access is denied. Call the protocol
             * callback to let it know.
             */
            eType = BTEVENT_ACCESS_DENIED;
reportit:
            /* Remove the token from the list */
            Report(("MeSec: SecProcessAllSecurityTokens: removing token\n"));
            RemoveEntryList(&(token->node));

#if BT_SECURITY_TIMEOUT != 0
            /* Cancel the watchdog timer. */
            EVM_CancelTimer(&token->timer);
#endif /* BT_SECURITY_TIMEOUT != 0 */        
            
            /* Call the protocol */
            (bt.me.btEvent).eType = eType;
            (bt.me.btEvent).p.secToken = token;

            if (token->result == 0) {
                token->result = eType;
            } else {
                token->id(&((bt.me.btEvent)));
            }
            break;

        case BT_STATUS_PENDING:
            /* This token is not finished yet so keep it on the list. Attempt 
             * to start any pending operations.
             */
            MeStartOperation();
            break;

        default:
            Assert(0);
        }
        /* Advance to the next token */
        token = tmp;
    }

    /* There are no more tokens on the list so the access request
     * is complete. Clear the security access state.
     */
    if (IsListEmpty(&(remDev->secTokens))) {
        Report(("MeSec: Security token list is empty\n"));
        remDev->secAccessState = BAS_NO_ACCESS;
    } else {
        Report(("MeSec: Security token list is not empty\n"));
    }
}

/*---------------------------------------------------------------------------
 *            SecProcessSecurityToken()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Process the security token.
 *
 * Return:    status of the operation
 */
BtStatus SecProcessSecurityToken(BtSecurityToken* token)
{
    BtStatus        status;
    BtSecurityLevel mask;
    BtSecurityOp*   secOp;
    BtOpType        opType;

    /* Assume failure */
    status = BT_STATUS_FAILED;

    /* Set the mask to the starting point  */
    if (token->incoming) {
        mask = BSL_AUTHENTICATION_IN;
    } else {
        mask = BSL_AUTHENTICATION_OUT;
    }

    /* Check if authentication is needed */
    if ((token->level & mask) &&
        (token->remDev->encryptState == BES_NOT_ENCRYPTED)) {
        /* Authentication is required. Check to see if we need to start
         * authentication.
         */
        Report(("MeSec: Access Request needs authentication\n"));
        switch (token->remDev->authState) {
        case BAS_WAITING_FOR_PIN_R :
            /* This is the case where the remote device attempted to pair
             * with us. We are waiting for the user to enter a pin code.
             * This operation could end successfully or result in a limbo
             * state. We will perform an authenticate operation by falling
             * thru the case statement.
             */
        case BAS_WAITING_FOR_KEY_R:
            /* This is the case where the remote device attempted to pair
             * with us. We have sent the pin code but have not gotten back
             * a link key. It could be coming or it may not (limbo state) 
             * so we will perform an authenticate operation
             */
        case BAS_NOT_AUTHENTICATED:
            /* If we have already tried an authenticate operation and it
             * failed then we are done otherwise start an authentication 
             * operation. 
             */
            if ((token->remDev->secAccessState & BAS_AUTH_COMPLETE) == 0) {
                /* We can attempt to authenticate */
                Report(("MeSec: Adding authenticate operation to queue\n"));
                token->remDev->authHandler = 0;
                status = SecAddAuthenticateOpToQueue(token->remDev);
                if (status != BT_STATUS_PENDING) {
                    status = BT_STATUS_FAILED;
                }
            }
            goto done;

        case BAS_AUTHENTICATED:
            /* We are authenticated so go on to the next step */
            break;

        default:
            /* We are in the middle of authentication so wait until it
             * finishes
             */
            goto pending;
        }
    }

    /* We are authenticated so see if we need authorization */
    mask <<= 1;
    if (token->level & mask) {
        /* We need to be authorized so see if we already are */
        Report(("MeSec: Access request needs authorization\n"));
        switch (token->remDev->authorizeState) {
        case BAS_NOT_AUTHORIZED:
            /* We are not authorized so if we have not already atempted
             * authorization then try it
             */
            if (((bt.me.authorizeHandler) != 0) &&
                ((token->remDev->secAccessState & BAS_AUTHORIZE_COMPLETE) == 0)) {
                /* We need to request authorization from the user. Schedule
                 * a request for authorization.
                 */
                Report(("MeSec: Adding authorize operation to queue\n"));
                opType = BOP_AUTHORIZE;
                token->remDev->authorizeState = BAS_START_AUTHORIZE;
                token->remDev->secRec = token->record;
                goto addOp;
            } else {
                Report(("MeSec: No authorization handle or previous attempt failed\n"));
            }
            /* Authorization failed so we are done */
            goto done;

        case BAS_START_AUTHORIZE:
            /* Authorization in progress so wait until it finishes */
            goto pending;

        case BAS_AUTHORIZED:
        case BAS_AUTHORIZED_SERVICE:
            /* Already authorized so go to next step */
            break;
        }
    }

    /* We are authenticated and if needed, authorized. See if we need encyrption */
    mask <<= 1;
    if (token->level & mask) {
        /* We need to have encryption so see if we already have it */
        Report(("MeSec: Access request needs encryption\n"));
        switch(token->remDev->encryptState) {
        case BES_NOT_ENCRYPTED:
            /* The link is not encrypted. If we have already attempted then
             * fail otherwise start it.
             */
            if (((token->remDev->secAccessState & BAS_ENCRYPT_COMPLETE) == 0)) {
                /* We need to start encryption operation. */
                Report(("MeSec: Adding encryption operation to queue\n"));
                opType = BOP_ENCRYPTION;
                token->remDev->encryptState = BES_START_ENCRYPT;
                token->remDev->encryptHandler = 0;
                 goto addOp;
            }
            /* Encryption failed so we are done */
            goto done;

        case BES_START_ENCRYPT:
        case BES_END_ENCRYPT:
            /* Encryption in progress so wait until it finishes */
            goto pending;

        case BES_ENCRYPTED:
            /* Encryption in place so go to next step */
            break;
        }
    }

    /* If we make to here then access is allowed */
    Report(("MeSec: Access is granted\n"));
    status = BT_STATUS_SUCCESS;
    if (token->remDev->authorizeState == BAS_AUTHORIZED_SERVICE) {
        token->remDev->authorizeState = BAS_NOT_AUTHORIZED;
    }
    goto done;

addOp:
    /* Get a security operation token */
    secOp = SecBtGetFreeSecurityOp();
    if (secOp == 0) {
        goto done;
    }
    secOp->op.opType = opType;
    secOp->remDev = token->remDev;

    /* If this is encryption set the mode */
    if (opType == BOP_ENCRYPTION) {
        secOp->x.eMode = BECM_ENCRYPT_ENABLE;
    }

    Assert(IsEntryAvailable(&(secOp->op.node)));
    if (token->remDev->secAccessState == BAS_NO_ACCESS) {
        /* This is the first call so put at end of queue */
        MeAddOperation(secOp);
    } else {
        /* We are now in the middle of the process so put in front */
        MeAddOperationFront(secOp);
    }

pending:
    status = BT_STATUS_PENDING;
done:
       return status;
}

/*---------------------------------------------------------------------------
 *            SecAccessTimeoutHandler()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handles the timeout of a security token.
 *
 */
void SecAccessTimeoutHandler(EvmTimer *timer)
{
    BtSecurityToken *token = (BtSecurityToken *)timer->context;

    Report(("MeSec: Timeout on security access request\n"));

    /* Cancel the original access request */
    (void)SEC_CancelAccessRequest(token);

    /* Notify the caller of the cancellation */
    (bt.me.btEvent).eType = BTEVENT_ACCESS_DENIED;
    (bt.me.btEvent).p.secToken = token;
    token->id(&((bt.me.btEvent)));
}

/*---------------------------------------------------------------------------
 *            SecSetSecurityMode()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Set security mode.
 *
 * Return:    status of operation. 
 */
BtStatus SecSetSecurityMode(BtHandler* handler, BtSecurityMode mode, BOOL encrypt)
{
    BtStatus      status;
    BtSecurityOp* secOp;

#if XA_ERROR_CHECK == XA_ENABLED
    if (handler == 0) {
        status = BT_STATUS_INVALID_PARM;
        goto done;
    }
#endif /* XA_ERROR_CHECK */

    Assert(handler != 0);
    Assert((mode == BSM_SEC_LEVEL_3) || (mode == BSM_SEC_LEVEL_2));

    Assert(((mode == BSM_SEC_LEVEL_3) && ((bt.me.secModeState) == BSMS_LEVEL2)) ||
           ((mode == BSM_SEC_LEVEL_2) && ((bt.me.secModeState) == BSMS_LEVEL3)));

    /* Get a security op and start the process */
    secOp = SecBtGetFreeSecurityOp();
    if (secOp == 0) {
        status = BT_STATUS_NO_RESOURCES;
    } else {
        secOp->op.opType = BOP_SECURITY_MODE;
        secOp->x.secMode.mode = mode;
        secOp->x.secMode.encrypt = encrypt;
        (bt.me.secModeHandler) = handler;

        /* Set the encryption state */
        if (mode == BSM_SEC_LEVEL_3) {
            (bt.me.secModeState) = BSMS_START_LEVEL3;
        } else {
            (bt.me.secModeState) = BSMS_END_LEVEL3;
        }

        Assert(IsEntryAvailable(&(secOp->op.node)));
        MeAddOperation(secOp);

        /* Attempt to start the operation */
        MeStartOperation();
        status = BT_STATUS_PENDING;
    }

#if XA_ERROR_CHECK == XA_ENABLED
done:
#endif
    return status;
}

/*---------------------------------------------------------------------------
 *            SecEnableEncryptMode()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Set security mode.
 *
 * Return:    status of operation. 
 */
BtStatus SecSetEncryptMode(BtHandler* handler, BOOL encryption)
{
    BtStatus      status;
    BtSecurityOp* secOp;

#if XA_ERROR_CHECK == XA_ENABLED
    if (handler == 0) {
        status = BT_STATUS_INVALID_PARM;
        goto done;
    }
#endif /* XA_ERROR_CHECK */

    Assert(handler != 0);
    Assert((bt.me.secModeState) == BSMS_LEVEL3);

    /* Get a security op and start the process */
    secOp = SecBtGetFreeSecurityOp();
    if (secOp == 0) {
        status = BT_STATUS_NO_RESOURCES;
    } else {
        secOp->op.opType = BOP_SECURITY_MODE;
        secOp->x.secMode.mode = BSM_SEC_ENCRYPT;
        secOp->x.secMode.encrypt = encryption;
        (bt.me.secModeHandler) = handler;

        Assert(IsEntryAvailable(&(secOp->op.node)));
        MeAddOperation(secOp);

        /* Attempt to start the operation */
        MeStartOperation();
        status = BT_STATUS_PENDING;
    }

#if XA_ERROR_CHECK == XA_ENABLED
done:
#endif
    return status;
}
#endif /* BT_SECURITY == XA_ENABLED*/

/*---------------------------------------------------------------------------
 *            SecBtGetFreeSecurityOp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Return a pointer to a free security op structure
 *
 * Return:    void
 */
BtSecurityOp* SecBtGetFreeSecurityOp(void)
{
    I8 i;

    for (i = 0; i < NUM_SEC_OPS; i++) {
        if ((bt.me.secOpTable)[i].op.opType == BOP_NOP) {
            return &((bt.me.secOpTable)[i]);
        }
    }
    return 0;
}

        
/*---------------------------------------------------------------------------
 *            HandleSecurityCommandComplete()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle the command complete event for security tasks.. 
 *
 * Return:    void
 */
static void HandleSecurityCommandComplete(U8 event, U8  len, U8* parm)
{
    U16 opcode = 0;

    UNUSED_PARAMETER(len);

    switch (event) {
    case HCE_COMMAND_COMPLETE:
        /* Save the status in case it is an error */
        opcode = LEtoHost16((parm+1));
        (bt.me.btEvent).errCode = parm[3];
        Report(("MeSec: Security Command complete event received. Err = %x\n", (bt.me.btEvent).errCode));
        break;

    case HCE_COMMAND_STATUS:
        /* Save the status in case it is an error */
        opcode = LEtoHost16((parm+2));
        (bt.me.btEvent).errCode = parm[0];
        Report(("MeSec: Security Command status event received. Err = %x\n", (bt.me.btEvent).errCode));
        Assert(parm[0] != 0);
        break;

    default:
        Assert(0);
    }

    /* Make sure the opcode matches. If not ignore the response */
    if (opcode == (bt.me.curTask)) {
        /* Clear the task */
        (bt.me.curTask) = 0;
        /* Call the operation handler */
        (bt.me.opHandler)(BOE_TASK_END);
    } else {
        /* Ignore this command complete event */
        Report(("MeSec: HandleSecurityCommandComplete opcode mismatch"
                "opcode = %x, curTask = %x\n",
                LEtoHost16((parm+1)), (bt.me.curTask)));
    }
}


#if BT_SECURITY == XA_DISABLED
void SecHandleDenyPinCodeReq(U8 len, U8 *parm)
{
    BtRemoteDevice* remDev;
    BtSecurityOp*   secOp;

    UNUSED_PARAMETER(len);
    Assert(len >= 6);

    remDev = ME_FindRemoteDeviceP(parm);
    if (remDev != 0) {
        /* Pairing is not allowed so set state to NOT_SECURE */
        Report(("MeSec: No pairing handler\n"));
        remDev->authState = BAS_NOT_AUTHENTICATED;
    } else {
        Report(("MeSec: Remote device does not exist for pairing\n"));
    }

    /* Setup a security op to send the rejection */
    secOp = SecBtGetFreeSecurityOp();
    Assert(secOp != 0);

    /* Set the remDev to 0 indicating a negative response */
    secOp->remDev = 0;

    /* Copy in the BD_ADDR */
    OS_MemCopy(secOp->bdAddr.addr, parm, 6);

    secOp->op.opType = BOP_PIN_CODE_RSP;
    MeAddOperationFront(secOp);

    /* Attempt to start the operation */
    MeStartOperation();
}

/*---------------------------------------------------------------------------
 *            SecHandleDenyPinCodeRspOp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle the Link key response operation.
 *
 * Return:    void
 */
void SecHandleDenyPinCodeRspOp(BtOpEvent event)
{
    switch (event) {
    case BOE_START:
        StartDenyPinCodeRspTask();
        return;

    case BOE_TASK_END:
        /* The operation is done so clear the op type */
        SECOP->op.opType = BOP_NOP;
        break;

    default:
        Assert(0);
        break;
    }

    /* Start another operation if one exists */
    MeOperationEnd();

}

static void StartDenyPinCodeRspTask(void)
{
    /* Set the task handler */
    (bt.me.taskHandler) = HandleSecurityCommandComplete;
    (bt.me.taskEvent) = HCE_COMMAND_COMPLETE;

    /* Put in the BD_ADDR portion of the response */ 
    Assert(IsEntryAvailable(&((bt.me.hciGenCmd).node)));
    OS_MemCopy((bt.me.hciGenCmd).parms, SECOP->bdAddr.addr, 6);

    /* We need to respond in the negative */
    Report(("MeSec: Sending Pin Code Request Negative Reply command\n"));
    AssertEval(MeSendHciCommand(HCC_PIN_CODE_REQ_NEG_REPL, 6) == BT_STATUS_PENDING);
}


#endif /* BT_SECURITY == XA_DISABLED */


