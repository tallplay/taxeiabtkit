/***************************************************************************
 *
 * File:
 *     $Workfile:mesco.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:73$
 *
 * Description:   These are the functions called internally by ME clients 
 *                to manage and control SCO data links.
 *
 * Created:       March 28, 2000
 *
 * Copyright 2000-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "btalloc.h"
#include "ddb.h"
#if NUM_SCO_CONNS > 0
#include "mesco.h"

static void ScoFailPendingCommands(BtScoConnect *scocon);
static BOOL ScoFailCommand(BtScoConnect *scocon, MeCommandToken *token);

/****************************************************************************
 *
 * Functions
 *
 ****************************************************************************/

/*---------------------------------------------------------------------------
 *            StartScoLinkConnectTask()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Start the link connect task.
 *
 * Return:    void
 */
static void StartScoLinkConnectTask(void)
{
    BtRemoteDevice *remDev;
    BtScoConnect *scocon;

    scocon = (BtScoConnect*) (bt.me.curOp);
    remDev = scocon->scord;

    (bt.me.taskHandler) = MeHandleConnectionStatusOnly;
    (bt.me.taskEvent) = HCE_COMMAND_STATUS;

    Report(("MeSco: Starting SCO link to ACL link %x\n", remDev->hciHandle));
    /* ACL Connection handle */
    StoreLE16((bt.me.hciGenCmd).parms, remDev->hciHandle);
    (bt.me.hciGenCmd).parms[2] = (U8) scocon->scopt;
    (bt.me.hciGenCmd).parms[3] = (U8) (scocon->scopt >> 8);

    Assert(IsEntryAvailable(&((bt.me.hciGenCmd).node)));
    AssertEval(MeSendHciCommand(HCC_ADD_SCO_CONNECTION, 4) == BT_STATUS_PENDING);
}

/*---------------------------------------------------------------------------
 *            StartScoLinkSetupTask()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Start the BT v1.2 SCO link setup task.
 *
 * Return:    void
 */
static void StartScoLinkSetupTask(void)
{
    BtRemoteDevice *remDev;
    BtScoConnect *scocon;

    scocon = (BtScoConnect*) (bt.me.curOp);
    remDev = scocon->scord;

    (bt.me.taskHandler) = MeHandleConnectionStatusOnly;
    (bt.me.taskEvent) = HCE_COMMAND_STATUS;

    if (scocon->scostate != BDS_CONNECTED) {
        Report(("MeSco: Starting eSCO link to ACL link %x\n", remDev->hciHandle));
        /* ACL Connection handle */
        StoreLE16((bt.me.hciGenCmd).parms, remDev->hciHandle);
    }
    else {
        Report(("MeSco: Re-configuring eSCO link %x\n", scocon->scoHciHandle));
        /* SCO Connection handle for reconfig */
        StoreLE16((bt.me.hciGenCmd).parms, scocon->scoHciHandle);
    }
    StoreLE32((bt.me.hciGenCmd).parms+2, scocon->scoTxParms.transmitBandwidth);
    StoreLE32((bt.me.hciGenCmd).parms+6, scocon->scoTxParms.receiveBandwidth);
    StoreLE16((bt.me.hciGenCmd).parms+10, scocon->scoTxParms.maxLatency);
    StoreLE16((bt.me.hciGenCmd).parms+12, scocon->scoTxParms.voiceSetting);
    (bt.me.hciGenCmd).parms[14] = scocon->scoTxParms.retransmissionEffort;
    StoreLE16((bt.me.hciGenCmd).parms+15, scocon->scoTxParms.eScoPktType);

    Assert(IsEntryAvailable(&((bt.me.hciGenCmd).node)));
    AssertEval(MeSendHciCommand(HCC_SETUP_SYNC_CONNECTION, 17) == BT_STATUS_PENDING);
}

/*---------------------------------------------------------------------------
 *            StartScoLinkAcceptTask()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Start the SCO link accept task.
 *
 * Return:    void
 */
static void StartScoLinkAcceptTask(void)
{
    BtRemoteDevice *remDev;
    BtScoConnect   *scocon;
    U8              len;
    HciCommandType  command;

    scocon = (BtScoConnect*) (bt.me.curOp);
    remDev = scocon->scord;

    (bt.me.taskHandler) = MeHandleConnectionStatusOnly;
    (bt.me.taskEvent) = HCE_COMMAND_STATUS;

    OS_MemCopy((bt.me.hciGenCmd).parms, remDev->bdAddr.addr, 6); /* BD_ADDR */
    if (((bt.me.btVersion) >= 2) && ((bt.me.btFeatures[3]) & 0x80)) {
        /* This is a BT v1.2 eSCO accept. */
        StoreLE32((bt.me.hciGenCmd).parms+6, scocon->scoTxParms.transmitBandwidth);
        StoreLE32((bt.me.hciGenCmd).parms+10, scocon->scoTxParms.receiveBandwidth);
        StoreLE16((bt.me.hciGenCmd).parms+14, scocon->scoTxParms.maxLatency);
        StoreLE16((bt.me.hciGenCmd).parms+16, scocon->scoTxParms.voiceSetting);
        (bt.me.hciGenCmd).parms[18] = scocon->scoTxParms.retransmissionEffort;
        StoreLE16((bt.me.hciGenCmd).parms+19, scocon->scoTxParms.eScoPktType);
        command = HCC_ACCEPT_SYNC_CON_REQ;
        len = 21;
    }
    else {
        (bt.me.hciGenCmd).parms[6] = 0;        /* Not used for SCO links */
        command = HCC_ACCEPT_CON_REQ;
        len = 7;
    }

    Assert(IsEntryAvailable(&((bt.me.hciGenCmd).node)));
    Report(("MeSco: Starting SCO Link Accept command\n"));
    AssertEval(MeSendHciCommand(command, len) == BT_STATUS_PENDING);
}

/*---------------------------------------------------------------------------
 *            StartScoLinkRejectTask()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Start the SCO link reject task.
 *
 * Return:    void
 */
static void StartScoLinkRejectTask(void)
{
    BtRemoteDevice *remDev;
    BtScoConnect   *scocon;
    HciCommandType  command;

    scocon = (BtScoConnect*) (bt.me.curOp);
    remDev = scocon->scord;

    (bt.me.taskHandler) = MeHandleConnectionStatusOnly;
    (bt.me.taskEvent) = HCE_COMMAND_STATUS;

    OS_MemCopy((bt.me.hciGenCmd).parms, remDev->bdAddr.addr, 6); /* BD_ADDR */

    /* Most often HC_STATUS_HOST_REJ_NO_RESOURCES */
    (bt.me.hciGenCmd).parms[6] = scocon->scoRejectReason;

    if (((bt.me.btVersion) >= 2) && ((bt.me.btFeatures[3]) & 0x80)) {
        /* This is a BT v1.2 eSCO reject. */
        command = HCC_REJECT_SYNC_CON_REQ;
    }
    else {
        command = HCC_REJECT_CON_REQ;
    }

    Assert(IsEntryAvailable(&((bt.me.hciGenCmd).node)));
    Report(("MeSco: Starting SCO Link Reject command\n"));
    AssertEval(MeSendHciCommand(command, 7) == BT_STATUS_PENDING);
}

/*---------------------------------------------------------------------------
 *            StartScoVoiceSettings()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Start the SCO voice settings command.
 *
 * Return:    void
 */
static void StartScoVoiceSettings(void)
{
    MeCommandToken *token;

    token = (MeCommandToken *)(bt.me.curOp);

    (bt.me.taskHandler) = MeHandleCompleteTask;
    (bt.me.taskEvent) = HCE_COMMAND_COMPLETE;

    (bt.me.hciGenCmd).parms[0] = (U8)token->p.vsettings;
    (bt.me.hciGenCmd).parms[1] = (U8)(token->p.vsettings >> 8);

    Assert(IsEntryAvailable(&((bt.me.hciGenCmd).node)));
    Report(("MeSco: Starting SCO Voice Settings command\n"));
    AssertEval(MeSendHciCommand(HCC_WRITE_VOICE_SETTING, 2) == BT_STATUS_PENDING);
}

/*---------------------------------------------------------------------------
 *            StartScoLinkDisconnectTask()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Start the link disconnect task.
 *
 * Return:    void
 */
static void StartScoLinkDisconnectTask(void)
{
    BtScoConnect *scocon;

    scocon = (BtScoConnect*) (bt.me.curOp);

    (bt.me.taskHandler) = MeHandleConnectionStatusOnly;
    (bt.me.taskEvent) = HCE_COMMAND_STATUS;

    Report(("MeSco: Starting disconnect to SCO hciHandle %x\n", 
            scocon->scoHciHandle));
    /* Local SCO Connection handle */
    StoreLE16((bt.me.hciGenCmd).parms, scocon->scoHciHandle); 
    (bt.me.hciGenCmd).parms[2] = BEC_USER_TERMINATED;    /* Reason */

    Assert(IsEntryAvailable(&((bt.me.hciGenCmd).node)));
    AssertEval(MeSendHciCommand(HCC_DISCONNECT, 3) == BT_STATUS_PENDING);
}

/*---------------------------------------------------------------------------
 *            ScoHandleConnectComplete()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle completion of a SCO link connection 
 *            (incoming and outgoing). 
 *
 * Return:    void
 */
void ScoHandleConnectComplete(U8 len, U8* parm)
{
    BtScoConnect *scocon=0;
    I8 i;

    UNUSED_PARAMETER(len);

    Report(("MeSco: SCO Connection complete event\n"));

    /* SCO link.  Locate the pending BtScoConnect that matches the 
     * complete event and is pending.
     */
    for (i = 0; i < NUM_SCO_CONNS; i++) {
        scocon = &((bt.me.scoTable)[i]);
        /* Hunt for an outstanding connection */
        if (((bt.me.scoTable)[i].scostate == BDS_OUT_CON2) ||
            ((bt.me.scoTable)[i].scostate == BDS_IN_CON)) { 
                break;
        }
    }

    if (i < NUM_SCO_CONNS) {
        (bt.me.btEvent).errCode = parm[0];
        scocon->scoHciHandle = LEtoHost16(parm+1);
        Report(("MeSco: SCO Connection Complete hciHandle = %x\n", 
                scocon->scoHciHandle));

        /* Report the results of the link up */
        if (scocon->scostate == BDS_OUT_CON2) {
            ScoReportScoLinkUp(scocon, BTEVENT_SCO_CONNECT_CNF);
        } else {
            ScoReportScoLinkUp(scocon, BTEVENT_SCO_CONNECT_IND);
        }
    } else {
        /* Probably the response to an internal reject so ignore it */
        Report(("MeSco: SCO connection complete internal reject\n"));
    }
}

/*---------------------------------------------------------------------------
 *            ScoHandleConnChanged()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle completion of a SCO link connection 
 *            (incoming and outgoing). 
 *
 * Return:    void
 */
void ScoHandleConnChanged(U8 len, U8* parm)
{
    BtScoConnect *scocon=0;
    BtRemoteDevice *remDev;
    I8 i;

    UNUSED_PARAMETER(len);

    Report(("MeSco: eSCO Connection Changed event\n"));

    /* Locate the pending SCO connection that matches the change event. */
    for (i = 0; i < NUM_SCO_CONNS; i++) {
        scocon = &((bt.me.scoTable)[i]);
        if (scocon->scostate == BDS_CONNECTED) {
            /* We have found a device in the correct state */
            if (scocon->scoHciHandle == LEtoHost16(parm+1)) {
                scocon->scoop.opType = BOP_NOP;
                break;
            }
        }
    }

    if (i < NUM_SCO_CONNS) {
        (bt.me.btEvent).errCode = parm[0];
        scocon->scoHciHandle = LEtoHost16(parm+1);
        Report(("MeSco: eSCO Connection Changed hciHandle = %x\n", 
                scocon->scoHciHandle));

        /* Report the results */
        remDev = scocon->scord;
        if (remDev->scoHandler != 0) {
            Assert(len >= 9);
            (bt.me.btEvent).p.scoConnect.remDev = remDev;
            scocon->scoRxParms.scoTransmissionInterval = parm[3];
            scocon->scoRxParms.scoRetransmissionWindow = parm[4];
            scocon->scoRxParms.scoRxPacketLen = LEtoHost16(parm+5);        
            scocon->scoRxParms.scoTxPacketLen = LEtoHost16(parm+7);        
            (bt.me.btEvent).p.scoConnect.scoCon = scocon;
            (bt.me.btEvent).p.scoConnect.scoHandle = scocon->scoHciHandle;
            (bt.me.btEvent).p.scoConnect.scoLinkType = BLT_ESCO;
            (bt.me.btEvent).p.scoConnect.scoTxParms = &scocon->scoTxParms;
            (bt.me.btEvent).p.scoConnect.scoRxParms = &scocon->scoRxParms;
            (bt.me.btEvent).eType = BTEVENT_SCO_CONN_CHNG;

            remDev->scoHandler->callback(&(bt.me.btEvent));
        }

    } else {
        /* Got a connection changed event for an unknown handle. */
        Report(("MeSco: eSCO connection changed unknown hciHandle = %x\n",
            LEtoHost16(parm+1)));
    }
}

/*---------------------------------------------------------------------------
 *            ScoHandleLinkConnectOp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle the link connection operation.
 *
 * Return:    void
 */
void ScoHandleLinkConnectOp(BtOpEvent event)
{
    BtScoConnect *scocon;

    if (event == BOE_START) {
        StartScoLinkConnectTask();
        return;
    }

    if (event == BOE_TASK_END) {
        scocon = (BtScoConnect*) (bt.me.curOp);
        if ((bt.me.btEvent).errCode != 0) {
            /* We got an error so report it */
            ScoReportScoLinkUp(scocon, BTEVENT_SCO_CONNECT_CNF);
        } else {
            /* After COMMAND_STATUS, promote to _CON2 if necessary. */
            if (scocon->scostate == BDS_OUT_CON) {
                scocon->scostate = BDS_OUT_CON2;
            }
        }
    }

    /* Start another operation if one exists */
    MeOperationEnd();
}

/*---------------------------------------------------------------------------
 *            ScoHandleLinkSetupOp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle the BT v1.2 SCO link setup operation.
 *
 * Return:    void
 */
void ScoHandleLinkSetupOp(BtOpEvent event)
{
    BtScoConnect *scocon;

    if (event == BOE_START) {
        StartScoLinkSetupTask();
        return;
    }

    if (event == BOE_TASK_END) {
        scocon = (BtScoConnect*) (bt.me.curOp);
        if ((bt.me.btEvent).errCode != 0) {
            /* We got an error so report it */
            ScoReportScoLinkUp(scocon, BTEVENT_SCO_CONNECT_CNF);
        } else {
            /* After COMMAND_STATUS, promote to _CON2 if necessary. */
            if (scocon->scostate == BDS_OUT_CON) {
                scocon->scostate = BDS_OUT_CON2;
            }
        }
    }

    /* Start another operation if one exists */
    MeOperationEnd();
}

/*---------------------------------------------------------------------------
 *            ScoHandleLinkDisconnectOp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle the link disconnect operation.
 *
 * Return:    void
 */
void ScoHandleLinkDisconnectOp(BtOpEvent event)
{
    BtScoConnect *scocon;

    switch (event) {
    case BOE_START:
        StartScoLinkDisconnectTask();
        return;

    case BOE_TASK_END:
        Report(("MeSco: ScoHandleLinkDisconnectOp task End errcode = %x\n", 
                    (bt.me.btEvent).errCode));
        /* The operation is done so clear the op type */
        scocon = (BtScoConnect*) (bt.me.curOp);
        scocon->scoop.opType = BOP_NOP;

        if ((bt.me.btEvent).errCode != 0) {
            /* This link could already be disconnected by the other side. 
               In this case we will get an error indicating no connection.
               Go ahead and remove the connection without checking the return
               code. When connections are unexpectedly dropped, they can 
               still exist in our data structures and no disconnect event
               will be received.
             */
            U8 scratch[sizeof(HciHandle) + sizeof(BtErrorCode) + 1];

            Assert((bt.me.btEvent).errCode == BEC_NO_CONNECTION);
            scocon = (BtScoConnect*) (bt.me.curOp);
            (void)HciDeleteConnection(scocon->scoHciHandle);
            OS_MemCopy(scratch + 1, (const U8*)&scocon->scoHciHandle,
                       (U32)sizeof(HciHandle));
            scratch[3] = BEC_NO_CONNECTION;
            (void)ScoHandleDisconnectComplete((U8)sizeof(scratch), scratch);
        }
        break;

    default:
        Assert(0);
    }

    /* Start another operation if one exists */
    MeOperationEnd();
}

/*---------------------------------------------------------------------------
 *            ScoHandleLinkAcceptOp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle the link accept operation.
 *
 * Return:    void
 */
void ScoHandleLinkAcceptOp(BtOpEvent event)
{
    BtScoConnect *scocon;

    switch (event) {
    case BOE_START:
        StartScoLinkAcceptTask();
        return;

    case BOE_TASK_END:
        /* The operation is done so clear the op type */
        scocon = (BtScoConnect*) (bt.me.curOp);
        scocon->scoop.opType = BOP_NOP;

        if ((bt.me.btEvent).errCode != 0) {
            /* We received an error so report it */
            Report(("MeSco:LinkAcceptError = %x\n", (bt.me.btEvent).errCode));
            ScoReportScoLinkUp(scocon, BTEVENT_SCO_CONNECT_IND);
        }
        break;

    default:
        Assert(0);
    }

    /* Start another operation if one exists */
    MeOperationEnd();
}

/*---------------------------------------------------------------------------
 *            ScoHandleLinkRejectOp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle the link accept operation.
 *
 * Return:    void
 */
void ScoHandleLinkRejectOp(BtOpEvent event)
{
    BtScoConnect *scocon;

    switch (event) {
    case BOE_START:
        StartScoLinkRejectTask();
        return;

    case BOE_TASK_END:
        /* The operation is done so clear the op type */
        scocon = (BtScoConnect*) (bt.me.curOp);
        scocon->scoop.opType = BOP_NOP;

        if ((bt.me.btEvent).errCode != 0) {
            /* We received an error so report it */
            Report(("MeSco: LinkRejectError = %x\n", (bt.me.btEvent).errCode)); 
            ScoReportScoLinkUp(scocon, BTEVENT_SCO_CONNECT_IND);
        }
        break;

    default:
        Assert(0);
    }

    /* Start another operation if one exists */
    MeOperationEnd();

}

/*---------------------------------------------------------------------------
 *            ScoHandleVoiceSettingsOp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle the SCO Voice Settings operation.
 *
 * Return:    void
 */
void ScoHandleVoiceSettingsOp(BtOpEvent event)
{
    MeCommandToken *token;

    if (event == BOE_START) {
        StartScoVoiceSettings();
        return;
    }

    if ((event == BOE_TASK_END) || (event == BOE_TASK_ERR)) {
        /* The operation is done so clear the op type */
        token = (MeCommandToken *) (bt.me.curOp);
        token->op.opType = BOP_NOP;

        if ((bt.me.btEvent).errCode == 0) {
            /* Save the new voice settings */
            (bt.me.vSettings) = token->p.vsettings;
        }

        /* We received an error so report it */
        Report(("MeSco: VoiceSettings done. Status %x\n", (bt.me.btEvent).errCode)); 
        (bt.me.btEvent).eType = BTEVENT_SCO_VSET_COMPLETE;
        token->callback(&(bt.me.btEvent));
    }

    /* Start another operation if one exists */
    MeOperationEnd();
} 


/*---------------------------------------------------------------------------
 *            ScoHandleLinkConnectReq()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle an incoming link connection. 
 *
 * Return:    TRUE if SCO handle it otherwise FALSE.
 */
BOOL ScoHandleLinkConnectReq(U8 len, U8* parm)
{
    I8 i;
    BtRemoteDevice *rdev=0;
    BtScoConnect *scocon=0;

    UNUSED_PARAMETER(len);

    /* Locate the RemoteDevice that matches the requested event.
     */
    for (i = 0; i < NUM_BT_DEVICES; i++) {
        rdev = &((bt.me.devTable)[i]);
        if (rdev->state == BDS_CONNECTED) {
            if (StrnCmp(rdev->bdAddr.addr,6,parm,6)) {
                break;
            }
        }
    }

    if (i < NUM_BT_DEVICES) {
        /* If there is no accept handler then reject it. */
        /* Call the scoHandler to accept/reject connection */
        if (rdev->scoHandler != 0) {
            /* First get a free BtScoConnect structure */
            scocon = ScoGetFreeConnect();

            if (scocon == 0) {
                /* No BtScoConnect structure, see if outgoing connect is in progress */
                for (i = 0; i < NUM_SCO_CONNS; i++) {
                    scocon = (BtScoConnect *) &(bt.me.scoTable[i]);
                    if (scocon->scostate == BDS_OUT_CON2) {
                        if (rdev == scocon->scord) {
                            /* Found an outgoing SCO connection to same device,
                             * handle race condition by indicating the incoming
                             * connection to the app. The app should accept, as
                             * the outgoing connection will fail.
                             */
                            Report(("MeSco: Detected race condition.\n"));
                            /* Remove the outgoing SCO connect operation */
                            if ((bt.me.curOp)->opType == BOP_SCO_CONNECT) {

                                (bt.me.taskHandler) = 0;
                                (bt.me.curOp) = 0;

                                /* Tell the app the outgoing failed. */
                                (bt.me.btEvent).p.scoConnect.remDev = rdev;
                                (bt.me.btEvent).p.scoConnect.scoHandle = scocon->scoHciHandle;
                                (bt.me.btEvent).p.scoConnect.scoCon = scocon;
                                (bt.me.btEvent).eType = BTEVENT_SCO_CONNECT_CNF; 
                                (bt.me.btEvent).errCode = BEC_REQUEST_CANCELLED; 
                                rdev->scoHandler->callback(&(bt.me.btEvent));
                            }
                            break;
                        }
                    }
                }
                if (i >= NUM_SCO_CONNS) {
                    goto reject;
                }
            }

            Report(("MeSco: Received SCO Link Connect Request\n"));
            /* Store the proper values */
            scocon->scord = rdev;
            scocon->scostate = BDS_IN_CON;
            scocon->scoLinkType = parm[9];

            (bt.me.btEvent).p.scoConnect.remDev = rdev;
            (bt.me.btEvent).p.scoConnect.scoHandle = scocon->scoHciHandle;

            scocon->scoRxParms.scoTransmissionInterval = parm[3];
            scocon->scoRxParms.scoRetransmissionWindow = parm[4];
            scocon->scoRxParms.scoRxPacketLen = LEtoHost16(parm+5);
            scocon->scoRxParms.scoTxPacketLen = LEtoHost16(parm+7);
            (bt.me.btEvent).p.scoConnect.scoRxParms = &scocon->scoRxParms;

            /* Initialize to TX parms that accept most anything.
             * App may change them.
             */
            scocon->scoTxParms.transmitBandwidth = 0x1f40;
            scocon->scoTxParms.receiveBandwidth = 0x1f40;
            scocon->scoTxParms.voiceSetting = (bt.me.vSettings);
            scocon->scoTxParms.eScoPktType = 0;

            if (scocon->scoLinkType == BLT_ESCO) {
                scocon->scoTxParms.maxLatency = 0xFFFF;
                scocon->scoTxParms.retransmissionEffort = 0xFF;
                if ((bt.me.btFeatures[1]) &0x08) scocon->scoTxParms.eScoPktType |= BESPT_HV1;
                if ((bt.me.btFeatures[1]) &0x10) scocon->scoTxParms.eScoPktType |= BESPT_HV2;
                if ((bt.me.btFeatures[1]) &0x20) scocon->scoTxParms.eScoPktType |= BESPT_HV3;
                if ((bt.me.btFeatures[3]) &0x80) scocon->scoTxParms.eScoPktType |= BESPT_EV3;
                if ((bt.me.btFeatures[4]) &0x01) scocon->scoTxParms.eScoPktType |= BESPT_EV4;
                if ((bt.me.btFeatures[4]) &0x20) scocon->scoTxParms.eScoPktType |= BESPT_EV5;
            } else {
                scocon->scoTxParms.maxLatency = 0x0007;
                scocon->scoTxParms.retransmissionEffort = 0x00;
                if ((bt.me.btFeatures[1]) &0x08) scocon->scoTxParms.eScoPktType |= BESPT_HV1;
                if ((bt.me.btFeatures[1]) &0x10) scocon->scoTxParms.eScoPktType |= BESPT_HV2;
                if ((bt.me.btFeatures[1]) &0x20) scocon->scoTxParms.eScoPktType |= BESPT_HV3;
            }

            (bt.me.btEvent).p.scoConnect.scoTxParms = &scocon->scoTxParms;
            (bt.me.btEvent).p.scoConnect.scoCon = scocon;
            (bt.me.btEvent).eType = BTEVENT_SCO_CONNECT_REQ; 
            (bt.me.btEvent).p.scoConnect.scoLinkType = parm[9];
            rdev->scoHandler->callback(&(bt.me.btEvent));
            return TRUE;
        } 
    }

reject:
    /* We need to reject the request so return FALSE */
    Report(("MeSco: Received SCO Link Connect Request (Reject)\n"));
    return FALSE;
}

/*---------------------------------------------------------------------------
 *            ScoHandleDisconnectComplete()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle the Disconnect Complete event. 
 *
 * Return:    TRUE - SCO handled it otherwise FALSE.
 */
BOOL ScoHandleDisconnectComplete(U8 len, U8* parm)
{
    BtScoConnect *scocon=0;
    I8 i;

    UNUSED_PARAMETER(len);

    /* Locate the pending SCO connection that matches the complete event and
     * is pending.
     */
    for (i = 0; i < NUM_SCO_CONNS; i++) {
        scocon = &((bt.me.scoTable)[i]);
        if ((scocon->scostate == BDS_OUT_DISC) || 
            (scocon->scostate == BDS_CONNECTED)) {
            /* We have found a device in the correct state */
            if (scocon->scoHciHandle == LEtoHost16(parm+1)) {
                break;
            }
        }
    }

    /* If we can't find match then it is probably an ACL link */
    if (i < NUM_SCO_CONNS) {
       Report(("MeSco: SCO Disconnect complete event for hciHandle = %d\n", 
               scocon->scoHciHandle));

        /* Make sure the state is in pending disconnect when failing commands */
        scocon->scostate = BDS_OUT_DISC;

        /* Clear any pending operations */
        ScoFailPendingCommands(scocon);

        /* Now report to all global handlers */
        (bt.me.btEvent).errCode = parm[3]; /* Pass along the reason error code */
        (bt.me.btEvent).p.scoConnect.remDev = scocon->scord;
        (bt.me.btEvent).p.scoConnect.scoHandle = scocon->scoHciHandle;
        (bt.me.btEvent).p.scoConnect.scoCon = scocon;
        (bt.me.btEvent).eType = BTEVENT_SCO_DISCONNECT;
        (bt.me.btEvent).p.scoConnect.scoLinkType = scocon->scoLinkType;

        MeReportResults(BEM_SCO_DISCONNECT);

        /* The registered handler may need to reconnect, so
         * clear the handle and state.
         */
        scocon->scoHciHandle = 0x852;
        scocon->scostate = BDS_DISCONNECTED;

        /* The application might be terminating so lets not require
           the scoHandler here.
         */
        if (scocon->scord->scoHandler != 0) {
            /* Tell the SCO handler that the link has been disconnected */
            scocon->scord->scoHandler->callback(&(bt.me.btEvent));
        }
        return TRUE;
    }

    return FALSE;
}


/*---------------------------------------------------------------------------
 *            ScoFailPendingCommands()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Fail commands associated with this SCO link.
 *
 * Return:    void
 */
static void ScoFailPendingCommands(BtScoConnect *scocon)
{
    BtOperation    *op, *nextOp, *saveCurOp;

    saveCurOp = 0;
    /* First, check the current operation */
    if ((bt.me.curOp) != 0 && (bt.me.curOp)->opType == BOP_GEN_COMMAND) {
        if (ScoFailCommand(scocon, (MeCommandToken *)(bt.me.curOp)) == TRUE) {
            saveCurOp = (bt.me.curOp);
            if (IsNodeOnList(&(bt.me.cmdList), &saveCurOp->node)) {
                /* This is an async command */
                RemoveEntryList(&saveCurOp->node);
            }
            MeOperationEnd();
        }
    }

    /* Check the operations queue */
    op = (BtOperation *)GetHeadList(&(bt.me.opList));
    do {
        nextOp = (BtOperation *)GetNextNode(&op->node);

        if (op->opType == BOP_GEN_COMMAND) {
            if (ScoFailCommand(scocon, (MeCommandToken *)op) == TRUE) {
                RemoveEntryList(&op->node);
            }
        }
        op = nextOp;

    } while((BtOperation *)GetHeadList(&(bt.me.opList)) != op);

    /* Check the command queue */
    op = (BtOperation *)GetHeadList(&(bt.me.cmdList));
    do {
        nextOp = (BtOperation *)GetNextNode(&op->node);

        if (op != saveCurOp && op->opType == BOP_GEN_COMMAND) {
            /* This is not the current remDev operation so check it */
            if (ScoFailCommand(scocon, (MeCommandToken *)op) == TRUE) {
                RemoveEntryList(&op->node);
            }
        }
        op = nextOp;

    } while((BtOperation *)GetHeadList(&(bt.me.cmdList)) != op);

    /* Check the async queue */
    op = (BtOperation *)GetHeadList(&(bt.me.asyncList));
    do {
        nextOp = (BtOperation *)GetNextNode(&op->node);

        if (op != saveCurOp && op->opType == BOP_GEN_COMMAND) {
            /* This is not the current operation so check it */
            if (ScoFailCommand(scocon, (MeCommandToken *)op) == TRUE) {
                RemoveEntryList(&op->node);
            }
        }
        op = nextOp;

    } while((BtOperation *)GetHeadList(&(bt.me.asyncList)) != op);
}

/*---------------------------------------------------------------------------
 *            ScoFailCommand()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Fail the associated SCO command.
 *
 * Return:    void
 */
static BOOL ScoFailCommand(BtScoConnect *scocon, MeCommandToken *token)
{
    if (token->p.general.in.hciCommand == HCC_CHNG_CONN_PACKET_TYPE &&
        scocon == ScoMapHciToConnect(
        (HciHandle)(LEtoHost16(token->p.general.in.parms) & 0x0fff))) {
        token->p.general.out.status = BT_STATUS_FAILED;
        (bt.me.btEvent).errCode = BEC_NO_CONNECTION;
        (bt.me.btEvent).p.meToken = token;
        (bt.me.btEvent).eType = token->eType;
        token->callback(&(bt.me.btEvent));
        return TRUE;
    }

    return FALSE;
}


/*---------------------------------------------------------------------------
 *            ScoHandleDisconnectAcl()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle the Disconnect of the ACL link. Disconnect all SCO
 *            connections attached to the ACL link. 
 *
 * Return:    void
 */
void ScoHandleDisconnectAcl(BtRemoteDevice *remDev)
{
    BtScoConnect *scon;
    I8 i;

    (bt.me.btEvent).eType = BTEVENT_SCO_DISCONNECT;

    /* GO through SCO's and disconnect all the SCO 
     * connections attached to this remote device.
     */
    for (i = 0; i < NUM_SCO_CONNS; i++) {
        scon = (BtScoConnect *) &(bt.me.scoTable[i]);
        if (remDev == scon->scord) {

            switch (scon->scostate) {
            case BDS_CONNECTED:
                /* Found a SCO connection, notify handlers. */
                scon->scostate = BDS_OUT_DISC;
                (bt.me.btEvent).p.scoConnect.remDev = remDev;
                (bt.me.btEvent).p.scoConnect.scoHandle = scon->scoHciHandle;
                (bt.me.btEvent).p.scoConnect.scoCon = scon;
                (bt.me.btEvent).p.scoConnect.scoLinkType = scon->scoLinkType;

                /* Disconnect didn't happen normaly , so
                 * delete the HCI handle directly.
                 */
                (void)HciDeleteConnection(scon->scoHciHandle);
                scon->scoHciHandle = 0x852;
                scon->scostate = BDS_DISCONNECTED;

                if (remDev->scoHandler != 0) {
                    /* Tell the SCO handler that the link has been disconnected */
                    remDev->scoHandler->callback(&(bt.me.btEvent));
                }

                /* Now report to all global handlers */
                MeReportResults(BEM_SCO_DISCONNECT);
                break;

            case BDS_OUT_CON:
                /* If in outgoing connection mode, indicate terminated
                 * connect CNF to app.
                 */
                (bt.me.btEvent).errCode = BEC_LOCAL_TERMINATED;
                ScoReportScoLinkUp(scon, BTEVENT_SCO_CONNECT_CNF);
                /* No break! Fall through */

            default:
                /* Make sure the state is in pending disconnect */
                scon->scostate = BDS_OUT_DISC;

                /* Clear out any pending operations */
                ScoFailPendingCommands(scon);

                /* Indicate SCO event is disconnected */
                scon->scostate = BDS_DISCONNECTED;
            }
            scon->scord = 0;
        }
    }

    /* Automactically unregister SCO handler. The posted disconnects will
       happen after the RemDev is offically terminated so we must unregister
       it here. This will prevent the SCO Handler from being called after the
       remDev is invalid.
     */
    remDev->scoHandler = 0;
}

/*---------------------------------------------------------------------------
 *            ScoGetFreeConnect()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Return a pointer to a free remote device structure or 0 if
 *            one is not found. 
 *
 * Return:    BtRemoteDevice*
 */
BtScoConnect *ScoGetFreeConnect(void)
{
    I8 i;

    /* Go through the ScoConnect array looking for the first free entry. */
    for (i = 0; i < NUM_SCO_CONNS; i++) {
        if ((bt.me.scoTable)[i].scostate == BDS_DISCONNECTED) {
            (bt.me.scoTable)[i].scopt = BSPT_HV3;
            (bt.me.scoTable)[i].scoHciHandle = 0x852;
            return &((bt.me.scoTable)[i]);
        }
    }
    return 0;
}

/*---------------------------------------------------------------------------
 *            ScoReportScoLinkUp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Report results of an outgoing SCO link coming up.
 *
 * Return:    void
 */
void ScoReportScoLinkUp(BtScoConnect *scocon, BtEventType evnt)
{
    BtEventMask mask;
    BtOpType saveOp;
    BtRemoteDevice *remDev;

    saveOp = scocon->scoop.opType;
    scocon->scoop.opType = BOP_NOP;

    /* If we were rejecting the link then success does not mean the link
     * is up. It actually means the link is down. We will set the errorCode
     * so the link does not come up.
     */
    if (saveOp == BOP_SCO_REJECT) {
        Report(("MeSco: Setting error for link reject. Old error = 0x%x\n", (bt.me.btEvent).errCode));
        (bt.me.btEvent).errCode = BEC_LOCAL_TERMINATED;
    }

    if ((bt.me.btEvent).errCode) {
        scocon->scostate = BDS_DISCONNECTED;
    } else {
        /* Operation succeeded. Put the proper value into the Event and
         * report the event to all bound handlers.
         */
        scocon->scostate = BDS_CONNECTED;
    }
    remDev = scocon->scord;
    (bt.me.btEvent).p.scoConnect.remDev = remDev;
    (bt.me.btEvent).p.scoConnect.scoHandle = scocon->scoHciHandle;
    (bt.me.btEvent).p.scoConnect.scoCon = scocon;
    (bt.me.btEvent).p.scoConnect.scoLinkType = scocon->scoLinkType;
    (bt.me.btEvent).eType = evnt;

    if (remDev->scoHandler != 0) {
        /* The SCO connect operation is complete so report results to 
           the SCO handler. 
         */
        remDev->scoHandler->callback(&(bt.me.btEvent));
    }

    if (evnt == BTEVENT_SCO_CONNECT_IND) {
        mask = BEM_SCO_CONNECT_IND;
    }
    else {
        mask = BEM_SCO_CONNECT_CNF;
    }
    MeReportResults(mask);
}

/*---------------------------------------------------------------------------
 *            ScoMapHciToConnect(HciHandle hcihandle)
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Return whether the given HciHandle is a currently connected
 *            SCO link.
 *
 * Return:    BtRemoteDevice*
 */
BtScoConnect *ScoMapHciToConnect(HciHandle hcihandle)
{
    I8 i;

    /* Go through the ScoConnect array looking for the first free entry. */
    for (i = 0; i < NUM_SCO_CONNS; i++) {
        if ((bt.me.scoTable)[i].scostate != BDS_DISCONNECTED) {
            if ((bt.me.scoTable)[i].scoHciHandle == hcihandle) {
                return &(bt.me.scoTable[i]);
            }
        }
    }
    return 0;
}
#else

/*
 * Avoid compiler error of empty file. Calls itself to avoid to avoid
 * unreferenced static function warning.
 */
static void dummy(void)
{
    dummy();
}

#endif /* NUM_SCO_CONNS */

