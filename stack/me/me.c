/***************************************************************************
 *
 * File:
 *     $Workfile:me.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:187$
 *
 * Description:
 *     Source code for the Bluetooth Management Entity.
 *
 * Created:
 *     October 18, 1999
 *
 * Copyright 1999-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *  
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "btalloc.h"
#include "sys/hci.h"
#include "utils.h"
#include "sys/l2capxp.h"
#include "sys/sdpi.h"
#if RFCOMM_PROTOCOL == XA_ENABLED
#include "sys/rfcexp.h"
#endif /* RFCOMM_PROTOCOL == XA_ENABLED */
#include "ddb.h"

/****************************************************************************
 *
 * Macros
 *
 ****************************************************************************/

/* Macro for accessing a Command token structure given the curOp */

#define METOKEN ((MeCommandToken*)(bt.me.curOp))

/****************************************************************************
 *
 * Functions
 *
 ****************************************************************************/
void MeRetryHciInit(void);
void MeProcessInitStatus(HciCallbackParms *parms);
void MeFailPendingCommands(void);
void MeFailCommand(BtOpType opType, void* parm);
void MeInitRadio(BtOpEvent event);
static void MeFailInProgressCommands(void);
static void HandleCompleteTaskRadioParms(U8 event, U8  len, U8* parm);
static void HandleWriteCoDOp(BtOpEvent event);
static void HandleWriteDefLinkPolicyOp(BtOpEvent event);
static void HandleChannelClass(BtOpEvent event);
static void HandleGenCmdOp(BtOpEvent event);
static void HandleGeneralTask(U8 event, U8  len, U8* parm);
static void ReportInquiryComplete(void);
static void HandleCancelInqOp(BtOpEvent event);
static BOOL IsMeCommandEqual(MeCommandToken* token1, MeCommandToken* token2);
static void HandleLocalNameChangeOp(BtOpEvent event);
static void HandleInquiryOp(BtOpEvent event);
static void HandleInquiryComplete(U8 len, U8* parm);
static void HandleInquiryResult(U8 len, U8* parm, BOOL rssi);
static void StartInquiryTask(void);
static void ReportCommandSent(const HciCommand *cmd);

/*---------------------------------------------------------------------------
 *            ME_Init(void)
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initialize the Management Entity. This includes initializing
 *            the HCI, L2CAP and SDP. 
 *
 * Return:    BT_STATUS_SUCCESS if success otherwise BT_STATUS_FAILED.
 */
BOOL ME_Init(void) 
{
#if (BT_SCO_HCI_DATA == XA_ENABLED) || (NUM_KNOWN_DEVICES > 0)
    I8 i;
#endif

    /* Initialize some lists */
    InitializeListHead(&((bt.me.regList)));
    InitializeListHead(&((bt.me.opList)));
    InitializeListHead(&((bt.me.secList)));
    InitializeListHead(&((bt.me.cmdList)));
    InitializeListHead(&((bt.me.asyncList)));
    InitializeListHead(&((bt.me.pendList)));

#if BT_SCO_HCI_DATA == XA_ENABLED
    /* Initialize the SCO Tx queues. */
    for (i = 0; i < NUM_SCO_CONNS; i++) {
        InitializeListHead(&(bt.me.scoTable[i].scoTxQueue));
    }
#endif

#if NUM_KNOWN_DEVICES > 0
    InitializeListHead(&((bt.me.deviceList)));
    InitializeListHead(&((bt.me.btDeviceFreeList)));
    for (i = 0; i < NUM_KNOWN_DEVICES; i++) {
        InsertTailList(&((bt.me.btDeviceFreeList)), &((bt.me.btDevices)[i].node));
    }
#endif

    (bt.me.accOp).op.opType = BOP_ACCESS_MODE;
    (bt.me.accUpOp).op.opType = BOP_ACCESS_MODE;
    (bt.me.accDownOp).op.opType = BOP_ACCESS_MODE;
    (bt.me.rejectOp).op.opType = BOP_LINK_IN_REJECT;

    (bt.me.connectionPolicy) = BCR_ANY;
    (bt.me.autoMode) = 0xff;
#if NUM_SCO_CONNS > 0
    (bt.me.vSettings) = BSAS_DEFAULT;
#endif /* NUM_SCO_CONNS > 0 */

    /* Reset the Bluetooth Host Controller, Link Manager and the
     * radio module.
     */
    if (L2CAP_Init() != BT_STATUS_SUCCESS) {
        goto failed;
    }

    if (SDP_Init() == BT_STATUS_FAILED) {
        goto failed;
    }

#if RFCOMM_PROTOCOL == XA_ENABLED
    if (RF_Init() == BT_STATUS_FAILED) {
        goto failed;
    }
#endif /* RFCOMM_PROTOCOL == XA_ENABLED */

    return BT_STATUS_SUCCESS;

failed:
/*    Assert(0); */
    Report(("ME_Init(): failed.\n"));
    return BT_STATUS_FAILED;

}

/*---------------------------------------------------------------------------
 *            ME_Deinit(void)
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Deinitialize the Management Entity. This includes deinitializing
 *            the HCI, L2CAP and SDP and RFCOMM. 
 *
 * Return:    void
 */
void ME_Deinit(void) 
{
    (bt.me.stackState) = BTSS_DEINITIALIZE;

    L2CAP_Deinit(); 
    SDP_Deinit();
#if RFCOMM_PROTOCOL == XA_ENABLED
    RF_Deinit();
#endif /* RFCOMM_PROTOCOL == XA_ENABLED */
}

/*---------------------------------------------------------------------------
 *            MeStartOperation()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  If possible start the next operation on the queue. 
 *
 * Return:    void
 */
void MeStartOperation(void)
{
    if (((bt.me.stackState) == BTSS_INITIALIZED) && ((bt.me.curOp) == 0)) {
        /* delay the command if hciGenCmd is in use */
        if (!IsEntryAvailable(&((bt.me.hciGenCmd).node))) {
            (bt.me.opMask) |= BOM_HCICOMMAND_BUSY;
            return;
        }
        /* There is no active operation so start the next one on the queue */
        if (!IsListEmpty(&(bt.me.opList))) {
            (bt.me.curOp) = (BtOperation*)RemoveHeadList(&(bt.me.opList));
            InitializeListEntry(&((bt.me.curOp)->node));

            switch ((bt.me.curOp)->opType) {
            case BOP_INQUIRY:
                /* Startup the Inquiry process */
                (bt.me.opHandler) = HandleInquiryOp;
                break;

            case BOP_CANCEL_INQUIRY:
                /* Start a cancel inquiry process */
                (bt.me.opHandler) = HandleCancelInqOp;
                break;

            case BOP_ACCESS_MODE:
                (bt.me.opHandler) = MeHandleAccessModeOp;
                break;

            case BOP_LINK_CONNECT:
                /* Start a link connection to a remote device */
                (bt.me.opHandler) = MeHandleLinkConnectOp;
                break;

            case BOP_LINK_ACCEPT:
                /* Accept an incoming link connection from a remote device */
                (bt.me.opHandler) = MeHandleLinkAcceptOp;
                break;

            case BOP_LINK_REJECT:
                /* Reject an incoming link connection from a remote device */
                (bt.me.opHandler) = MeHandleLinkRejectOp;
                break;

            case BOP_LINK_DISCONNECT:
                /* Disconnect a link */
                (bt.me.opHandler) = MeHandleLinkDisconnectOp;
                break;

            case BOP_PIN_CODE_RSP:
#if BT_SECURITY == XA_DISABLED
                (bt.me.opHandler) = SecHandleDenyPinCodeRspOp;
                break;
#else
                (bt.me.opHandler) = SecHandlePinCodeRspOp;
                break;

            case BOP_AUTHENTICATE:
                (bt.me.opHandler) = SecHandleAuthenticateOp;
                break;

            case BOP_LINK_KEY_RSP:
                (bt.me.opHandler) = SecHandleLinkKeyRspOp;
                break;

            case BOP_AUTHORIZE:
                (bt.me.opHandler) = SecHandleAuthorizeOp;
                break;

            case BOP_ENCRYPTION:
                (bt.me.opHandler) = SecHandleEncryptOp;
                break;

            case BOP_SECURITY_MODE:
                (bt.me.opHandler) = SecHandleSecurityModeOp;
                break;

            case BOP_CANCEL_AUTHORIZE:
                (bt.me.opHandler) = SecHandleAuthorizeOp;
                (bt.me.opHandler)(BOE_TASK_ERR);
                return;
#endif /* BT_SECURITY */

            case BOP_REM_NAME_REQ:
                (bt.me.opHandler) = MeHandleRemNameReqOp;
                break;

            case BOP_LOCAL_NAME_CHG:
                (bt.me.opHandler) = HandleLocalNameChangeOp;
                break;
#if 0
            case BOP_FLOW_SPEC:
                (bt.me.opHandler) = MeHandleFlowSpecOp;
                break;
#endif
            case BOP_HOLD_MODE:
                (bt.me.opHandler) = MeHandleHoldModeOp;
                break;

            case BOP_SNIFF_MODE:
                (bt.me.opHandler) = MeHandleSniffModeOp;
                break;

            case BOP_EXIT_SNIFF_MODE:
                (bt.me.opHandler) = MeHandleExitSniffModeOp;
                break;

            case BOP_PARK_MODE:
                (bt.me.opHandler) = MeHandleParkModeOp;
                break;

            case BOP_EXIT_PARK_MODE:
                (bt.me.opHandler) = MeHandleExitParkModeOp;
                break;

            case BOP_SWITCH_ROLE:
                (bt.me.opHandler) = MeHandleSwitchRoleOp;
                break;

            case BOP_WRITE_COD:
                (bt.me.opHandler) = HandleWriteCoDOp;
                break;

            case BOP_WRITE_DEF_LNK_POL:
                (bt.me.opHandler) = HandleWriteDefLinkPolicyOp;
                break;

            case BOP_GEN_COMMAND:
                (bt.me.opHandler) = HandleGenCmdOp;
                break;

            case BOP_LINK_IN_REJECT:
                /* Internally reject a link */
                (bt.me.opHandler) = MeHandleLinkRejectOp;
                break;

            case BOP_WRITE_LINK_POLICY:
                (bt.me.opHandler) = MeHandleWriteLinkPolicyOp;
                break;

            case BOP_CANCEL_CONNECT:
                (bt.me.opHandler) = MeHandleCancelConnectOp;
                break;

            case BOP_CANCEL_REM_NAME:
                (bt.me.opHandler) = MeHandleCancelRemNameOp;
                break;

            case BOP_CHANNEL_CLASS:
                (bt.me.opHandler) = HandleChannelClass;
                break;

            case BOP_DISCOVER_ROLE:
                (bt.me.opHandler) = MeHandleDiscoverRole;
                break;

#if NUM_SCO_CONNS > 0
            case BOP_SCO_CONNECT:
                /* Start a SCO link connection to a remote device */
                (bt.me.opHandler) = ScoHandleLinkConnectOp;
                break;

            case BOP_SCO_SETUP:
                /* Start a SCO link connection to a remote device */
                (bt.me.opHandler) = ScoHandleLinkSetupOp;
                break;

            case BOP_SCO_ACCEPT:
                (bt.me.opHandler) = ScoHandleLinkAcceptOp;
                break;

            case BOP_SCO_REJECT:
                (bt.me.opHandler) = ScoHandleLinkRejectOp;
                break;

            case BOP_SCO_DISCONNECT:
                /* Disconnect a link */
                (bt.me.opHandler) = ScoHandleLinkDisconnectOp;
                break;

            case BOP_SCO_VOICESET:
                /* SCO Voice Settings */
                (bt.me.opHandler) = ScoHandleVoiceSettingsOp;
                break;
#endif /* NUM_SCO_CONNS */

            default:
                Assert(0);
            }

            /* Call the handler */
            (bt.me.opHandler)(BOE_START);
        }
    }
}

/*---------------------------------------------------------------------------
 *            MeWriteClassOfDevice()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  If one is not already started then start a Write class of
 *            device operation. 
 *
 * Return:    void
 */
void MeWriteClassOfDevice(void)
{
    if (((bt.me.stackState) < BTSS_INITIALIZE_ERR) && 
        ((bt.me.codOp).opType == BOP_NOP)) {

        (bt.me.codOp).opType = BOP_WRITE_COD;
        Assert(IsEntryAvailable(&((bt.me.codOp).node)));
        InsertTailList(&(bt.me.opList), &((bt.me.codOp).node));

        /* Attempt to start the operation, it may not be in progress. */
        MeStartOperation();
    }
}

/*---------------------------------------------------------------------------
 *            MeWriteLocalName()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  If one is not already started then start a Local Name Change
 *            operation. 
 *
 * Return:    void
 */
void MeWriteLocalName(void)
{
    if (((bt.me.stackState) < BTSS_INITIALIZE_ERR) && 
        ((bt.me.nameOp).opType == BOP_NOP)) {

        (bt.me.nameOp).opType = BOP_LOCAL_NAME_CHG;
        Assert(IsEntryAvailable(&((bt.me.nameOp).node)));
        InsertTailList(&(bt.me.opList), &((bt.me.nameOp).node));

        /* Attempt to start the operation, it may not be in progress. */
        MeStartOperation();
    }
}

/*---------------------------------------------------------------------------
 *            MeWriteDefaultLinkPolicy()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  If one is not already started then start a Write Default Link
 *            policy operation. 
 *
 * Return:    void
 */
void MeWriteDefaultLinkPolicy(void)
{
    if (((bt.me.stackState) < BTSS_INITIALIZE_ERR) && 
        ((bt.me.policyOp).opType == BOP_NOP)) {

        (bt.me.policyOp).opType = BOP_WRITE_DEF_LNK_POL;
        Assert(IsEntryAvailable(&((bt.me.policyOp).node)));
        InsertTailList(&(bt.me.opList), &((bt.me.policyOp).node));

        /* Attempt to start the operation, it may not be in progress. */
        MeStartOperation();
    }
}

/*---------------------------------------------------------------------------
 *            HandleInquiryOp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Start an Inquiry process. 
 *
 * Return:    void
 */
static void HandleInquiryOp(BtOpEvent event)
{
    switch (event) {
    case BOE_START:
        /* Start the inquiry task */
        (bt.me.taskHandler) = MeHandleConnectionStatusOnly;
        (bt.me.taskEvent) = HCE_COMMAND_STATUS;
        StartInquiryTask();
        MeDevInquiryStart();
        return;

    case BOE_TASK_END:
        /* The operation is done check for errors */ 
        if ((bt.me.btEvent).errCode != 0) {
            /* We got an error so report it */
            Report(("ME: Inquiry operation completed with error = %x\n", (bt.me.btEvent).errCode));
            ReportInquiryComplete();
        }
        break;

    default:
        Assert(0);
    }

    /* Start another operation if one exists */
    MeOperationEnd();
}

/*---------------------------------------------------------------------------
 *            HandleCancelInqOp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Start a Cancel Inquiry process. 
 *
 * Return:    void
 */
static void HandleCancelInqOp(BtOpEvent event)
{
    switch (event) {
    case BOE_START:
        /* Start the task */
        (bt.me.taskHandler) = MeHandleCompleteTask;
        (bt.me.taskEvent) = HCE_COMMAND_COMPLETE;
        Assert(IsEntryAvailable(&((bt.me.hciGenCmd).node)));
        AssertEval(MeSendHciCommand(HCC_INQUIRY_CANCEL, 0) == BT_STATUS_PENDING);
        return;

    case BOE_TASK_END:
    case BOE_TASK_ERR:
        /* The operation is done check for errors */ 
        Report(("ME: Inquiry cancel operation completed error = %x\n", (bt.me.btEvent).errCode));
        MeReportInqCancelComplete();
        break;

    default:
        Assert(0);
    }

    /* Start another operation if one exists */
    MeOperationEnd();
}

/*---------------------------------------------------------------------------
 *            HandleWriteCoDOp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Start a Write Class of Device operation. 
 *
 * Return:    void
 */
static void HandleWriteCoDOp(BtOpEvent event)
{
    switch (event) {
    case BOE_START:
        /* Start the task */
        (bt.me.taskHandler) = MeHandleCompleteTask;
        (bt.me.taskEvent) = HCE_COMMAND_COMPLETE;
        Assert(IsEntryAvailable(&((bt.me.hciGenCmd).node)));
        (bt.me.writeCoD) = (bt.me.classOfDevice) | (bt.me.sdpServCoD);
        StoreLE32((bt.me.hciGenCmd).parms, (bt.me.writeCoD)); 

        AssertEval(MeSendHciCommand(HCC_WRITE_CLASS_OF_DEVICE, 3) == BT_STATUS_PENDING);
        return;

    case BOE_TASK_END:
    case BOE_TASK_ERR:
        (bt.me.codOp).opType = BOP_NOP;
        if ((bt.me.writeCoD) != ((bt.me.classOfDevice) | (bt.me.sdpServCoD))) {
            MeWriteClassOfDevice();
        }
        /* Operation done. When we put in error event then report any errors */
        Report(("ME: Write Class of Device operation completed error = %x\n", (bt.me.btEvent).errCode));
        break;

    default:
        Assert(0);
    }

    /* Start another operation if one exists */
    MeOperationEnd();
}

/*---------------------------------------------------------------------------
 *            HandleLocalNameChangeOp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Start an Inquiry process. 
 *
 * Return:    void
 */
static void HandleLocalNameChangeOp(BtOpEvent event)
{
    switch (event) {
    case BOE_START:
        (bt.me.taskHandler) = MeHandleCompleteTask;
        (bt.me.taskEvent) = HCE_COMMAND_COMPLETE;

        if ((bt.me.localNameLen) > 0)
            OS_MemCopy((bt.me.hciGenCmd).parms, (bt.me.localName), (bt.me.localNameLen) > 248 ? 248 : (bt.me.localNameLen));
        else (bt.me.hciGenCmd).parms[0] = 0;

        Assert(IsEntryAvailable(&((bt.me.hciGenCmd).node)));
        Report(("MeCon: Starting Local name change command, len = %d\n", (bt.me.localNameLen)));
        AssertEval(MeSendHciCommand(HCC_CHNG_LOCAL_NAME, 248) == BT_STATUS_PENDING);
        return;

    case BOE_TASK_END:
    case BOE_TASK_ERR:
        (bt.me.nameOp).opType = BOP_NOP;
        if (((((bt.me.localNameLen) > 0) &&
             (OS_MemCmp((bt.me.hciGenCmd).parms, (bt.me.localNameLen), (bt.me.localName), (bt.me.localNameLen)) == FALSE)) ||
             (((bt.me.localNameLen) == 0) && ((bt.me.hciGenCmd).parms[0] != 0)))) {
            MeWriteLocalName();
        }
        /* The operation is done check */ 
        Report(("ME: Local Name Change operation completed. Status = 0x%x\n", (bt.me.btEvent).errCode));
        break;

    default:
        Assert(0);
    }

    /* Start another operation if one exists */
    MeOperationEnd();
}


/*---------------------------------------------------------------------------
 *            HandleWriteDefLinkPolicyOp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Start a Write Class of Device operation. 
 *
 * Return:    void
 */
static void HandleWriteDefLinkPolicyOp(BtOpEvent event)
{
    switch (event) {
    case BOE_START:
        /* Start the task */
        (bt.me.taskHandler) = MeHandleCompleteTask;
        (bt.me.taskEvent) = HCE_COMMAND_COMPLETE;
        Assert(IsEntryAvailable(&((bt.me.hciGenCmd).node)));
        StoreLE16((bt.me.hciGenCmd).parms, (bt.me.inAclPolicy)); 

        AssertEval(MeSendHciCommand(HCC_WRITE_DEFAULT_LINK_POLICY, 2) == BT_STATUS_PENDING);
        return;

    case BOE_TASK_END:
    case BOE_TASK_ERR:
        (bt.me.policyOp).opType = BOP_NOP;
        /* Operation done. */
        Report(("ME: Write Default Link Policy operation completed error = 0x%x\n", (bt.me.btEvent).errCode));
        break;

    default:
        Assert(0);
    }

    /* Start another operation if one exists */
    MeOperationEnd();
}


/*---------------------------------------------------------------------------
 *            HandleChannelClass()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Start a Set Channel Classification operation. 
 *
 * Return:    void
 */
static void HandleChannelClass(BtOpEvent event)
{
    switch (event) {
    case BOE_START:
        /* Start the task */
        (bt.me.taskHandler) = MeHandleCompleteTask;
        (bt.me.taskEvent) = HCE_COMMAND_COMPLETE;
        Assert(IsEntryAvailable(&((bt.me.hciGenCmd).node)));
        OS_MemCopy((bt.me.hciGenCmd).parms, (bt.me.channelClass.map), 10);

        AssertEval(MeSendHciCommand(HCC_SET_AFH_HOST_CHAN_CLASS, 10) == BT_STATUS_PENDING);
        return;

    case BOE_TASK_END:
    case BOE_TASK_ERR:
        if ((bt.me.autoMode) & 0x80) {
            /* Clear the autoMode operation start bit */
            (bt.me.autoMode) &= 0x7f;

            if ((bt.me.autoMode) == 0) {
                (bt.me.hciGenCmd).parms[0] = 0;
            }
            else {
                (bt.me.hciGenCmd).parms[0] = 1;
            }
            AssertEval(MeSendHciCommand(HCC_WRITE_AFH_CHAN_ASSESS_MODE, 1) == BT_STATUS_PENDING);
            return;
        }

        (bt.me.channelOp).opType = BOP_NOP;
        /* Operation done. */
        Report(("ME: Set Channel Classification operation completed error = 0x%x\n", (bt.me.btEvent).errCode));
        break;

    default:
        Assert(0);
    }

    /* Start another operation if one exists */
    MeOperationEnd();
}


/*---------------------------------------------------------------------------
 *            HandleGenCmdOp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Perform a general HCI command. 
 *
 * Return:    void
 */
static void HandleGenCmdOp(BtOpEvent event)
{
    BtOpType opType;

    /* Save the op type (it may change when processing token) */
    opType = (bt.me.curOp)->opType;

    switch (event) {
    case BOE_START:
        /* Put the command on the command queue */
        InsertHeadList(&(bt.me.cmdList), &(METOKEN->op.node));
        (bt.me.taskHandler) = HandleGeneralTask;
        (bt.me.taskEvent) = METOKEN->p.general.in.event;
        Assert(IsEntryAvailable(&((bt.me.hciGenCmd).node)));
        Report(("MeCon: Starting General command\n"));
        OS_MemCopy((bt.me.hciGenCmd).parms, METOKEN->p.general.in.parms, 
                       METOKEN->p.general.in.parmLen); 
        AssertEval(MeSendHciCommand(METOKEN->p.general.in.hciCommand, 
                         METOKEN->p.general.in.parmLen) == BT_STATUS_PENDING);
        return;

    case BOE_CONT:
        /* This event occurs when a general command has received its command status
         * and is waiting for the next event. If it is an async event then start the
         * the next operation
         */
        if (!(METOKEN->flags & MCTF_ASYNC)) {
            return;
        }
        break;

    case BOE_TASK_END:
        /* Task is complete so report it. */
        Report(("ME: General Command completed. Status = 0x%x\n", (bt.me.btEvent).errCode));
        MeProcessGenToken(METOKEN);
        break;

    default:
        Assert(0);
    }

    /* Make sure this event belongs to the current operation
     * (it could have come in asynchronously)
     */
    if (opType == BOP_GEN_COMMAND) {
        /* Start another operation if one exists */
        MeOperationEnd();
    }
}

/*---------------------------------------------------------------------------
 *            StartInquiryTask()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Start the Iqnuiry Task. 
 *
 * Return:    void
 */
static void StartInquiryTask(void)
{
    Assert((bt.me.curOp) == (BtOperation*)(&((bt.me.inquiryOp))));

    (bt.me.hciGenCmd).parms[0] = (U8)(bt.me.inquiryOp).lap;
    (bt.me.hciGenCmd).parms[1] = (U8)(((bt.me.inquiryOp.lap)) >> 8);
    (bt.me.hciGenCmd).parms[2] = (U8)(((bt.me.inquiryOp.lap)) >> 16);

    /* Load duration of inquiry */
    (bt.me.hciGenCmd).parms[3] = (bt.me.inquiryOp).len;

    /* Load max number of responses */
    (bt.me.hciGenCmd).parms[4] = (bt.me.inquiryOp).max; 

    Assert(IsEntryAvailable(&((bt.me.hciGenCmd).node)));
    AssertEval(MeSendHciCommand(HCC_INQUIRY, 5) == BT_STATUS_PENDING);
}

/*---------------------------------------------------------------------------
 *            MeHandleCompleteTask()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle HCI events for the Complete task. 
 *
 * Return:    void
 */
void MeHandleCompleteTask(U8 event, U8  len, U8* parm)
{
    U16 opcode = 0;

    UNUSED_PARAMETER(len);

    switch (event) {
    case HCE_COMMAND_COMPLETE:
        /* Save the status in case it is an error */
        opcode = LEtoHost16((parm+1));
        (bt.me.btEvent).errCode = parm[3];
        break;

    case HCE_COMMAND_STATUS:
        /* Save the status in case it is an error */
        opcode = LEtoHost16((parm+2));
        (bt.me.btEvent).errCode = parm[0];
        break;

    default:
        Assert(0);
    }

    /* Make sure the opcode matches. If not ignore the response */
    if (opcode == (bt.me.curTask)) {
        /* Clear the task if this is the end. */
        if ((event != HCE_COMMAND_STATUS) ||
            ((event == HCE_COMMAND_STATUS) && (parm[0] != 0))) {
            (bt.me.curTask) = 0;
        }

        /* Call the operation handler */
        if ((bt.me.btEvent).errCode == 0) {
            (bt.me.opHandler)(BOE_TASK_END);
        } else {
            (bt.me.opHandler)(BOE_TASK_ERR);
        }

    } else {
        Report(("ME: MeHandleCompleteTask opcode mismatch opcode = %x, curTask = %x\n",
            opcode, (bt.me.curTask)));
    }
}

/*---------------------------------------------------------------------------
 *            MeHandleConnectionStatusOnly()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle the tasks where the only event from HCI is
 *            the connection status event. 
 *
 * Return:    void
 */
void MeHandleConnectionStatusOnly(U8 event, U8  len, U8* parm)
{
    UNUSED_PARAMETER(len);
    UNUSED_PARAMETER(event);

    Report(("ME: Received Status Only event\n"));
    Assert(event == HCE_COMMAND_STATUS);

    /* Make sure the opcode matches. If not ignore the response */
    if ((bt.me.curTask) == LEtoHost16((parm+2))) {
        /* Task is considered complete on status event */
        (bt.me.curTask) = 0;
        (bt.me.btEvent).errCode = parm[0];
        (bt.me.opHandler)(BOE_TASK_END);
    } else {
        Report(("ME: MeHandleConnectionStatusOnly opcode mismatch"
                "opcode = %x, curTask = %x\n",
            LEtoHost16((parm+2)), (bt.me.curTask)));
    }
}


/*---------------------------------------------------------------------------
 *            HandleGeneralTask()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  The task handler for General commands. 
 *
 * Return:    void
 */
static void HandleGeneralTask(U8 event, U8  len, U8* parm)
{
    BtOpEvent opEvent;

    Report(("Receive event for general command. Event = 0x%x\n", event));

    /* Ignore command complete and command status events that do not
     * match the current task
     */
    if (((event == HCE_COMMAND_STATUS) && 
         ((bt.me.curTask) != LEtoHost16((parm+2)))) ||
        ((event == HCE_COMMAND_COMPLETE) &&
         ((bt.me.curTask) != LEtoHost16((parm+1))))) {
        /* Ignore this event */
        Report(("ME: HandleGeneralTask opcode mismatch\n"));
        return;
    }

    /* Clear the task if this is the end. */
    if ((event != HCE_COMMAND_STATUS) ||
        ((event == HCE_COMMAND_STATUS) && (parm[0] != 0))) {
        (bt.me.curTask) = 0;
    }

    /* Copy the info into the token in case it is needed */
    METOKEN->p.general.out.status = BT_STATUS_SUCCESS;
    METOKEN->p.general.out.event = event;
    METOKEN->p.general.out.parmLen = len;
    METOKEN->p.general.out.parms = parm;

    opEvent = BOE_TASK_END;
    if (METOKEN->flags & MCTF_ASYNC) {
        /* This is an async command so we expect command status. If we
         * get a Command status event with an error or some other event
         * then we have problems so stop the command and report.
         */
        if ((event == HCE_COMMAND_STATUS) && (parm[0] == 0)) {
            opEvent = BOE_CONT;
        } 
    } else {
        /* This is a sync event. If we get a legal command status and it is not
         * the event we are looking for then continue otherwise stop the command
         * and report.
         */
        if ((event == HCE_COMMAND_STATUS) && (parm[0] == 0) && 
            (METOKEN->p.general.in.event != event)) {
            opEvent = BOE_CONT;
        }
    }

    (bt.me.btEvent).errCode = BEC_NO_ERROR;
    if (event == HCE_COMMAND_STATUS) {
        /* Set the error. */
        (bt.me.btEvent).errCode = parm[0];
    }
    (bt.me.opHandler)(opEvent);
}

/*---------------------------------------------------------------------------
 *            HandleInquiryResult()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle the HCI Inquiry_Result event. 
 *
 * Return:    void
 */
static void HandleInquiryResult(U8 len, U8* parm, BOOL rssi)
{
    U16          i;
    U8*          p;
    UNUSED_PARAMETER(len);

    Assert(len > 1);
    Assert(len >= (U8) (parm[0] * 14));

    /* Found one or more devices. Report to clients */
    Report(("ME: Inquiry result num resp = %d, bdaddr %x %x %x %x %x %x\n",
            parm[0], parm[1], parm[2], parm[3], parm[4], parm[5], parm[6]));
    for (i = 0; i < (U16)(parm[0]*14); i+=14) {
        /* Get the parameters */
        OS_MemCopy((bt.me.btEvent).p.inqResult.bdAddr.addr,parm+1+i,6);
        (bt.me.btEvent).p.inqResult.psi.psRepMode = parm[7+i];
        (bt.me.btEvent).p.inqResult.psPeriodMode = parm[8+i];
        p = parm + 9 + i;
        /* PS Mode is no longer provided (parm[9+i]) */
        (bt.me.btEvent).p.inqResult.psi.psMode = 0;

        /* Reserved parameter not present in RSSI responses */
        if (!rssi) {
            p++;
        }

        (bt.me.btEvent).p.inqResult.classOfDevice = 
            (U32)(p[0]) | ((U32)(p[1])<<8) | ((U32)(p[2])<<16);
        p += 3;

        (bt.me.btEvent).p.inqResult.psi.clockOffset = LEtoHost16(p);
        p += 2;
        Report(("ME:Inquiry result clockOffset = %x\n",
            (bt.me.btEvent).p.inqResult.psi.clockOffset));

        (bt.me.btEvent).p.inqResult.rssi = (rssi ? (S8)(*p++) : BT_INVALID_RSSI);

        /* Report this one to the clients */
        (bt.me.btEvent).errCode = 0;
        (bt.me.btEvent).eType = BTEVENT_INQUIRY_RESULT;
        MeReportResults(BEM_INQUIRY_RESULT);
    }
}

/*---------------------------------------------------------------------------
 *            HandleInquiryComplete()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle the HCI Inquiry_Complete event. 
 *
 * Return:    void
 */
static void HandleInquiryComplete(U8 len, U8* parm)
{
    UNUSED_PARAMETER(len);

    /* The Inquiry operation completed. Store the status and number of
     * responses in btEvent. Then tell all registered handlers.
     */
    Report(("ME: Received Inquiry complete event\n"));
    Assert(len >= 1);
    (bt.me.btEvent).errCode = parm[0];
    ReportInquiryComplete();
}

/*---------------------------------------------------------------------------
 *            MeReportResults()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Call the callback of all the handlers that have the mask set. 
 *
 * Return:    void
 */
void MeReportResults(BtEventMask mask)
{
    ListEntry*  entry;
    BtHandler*  handler;
#if XA_DEBUG == XA_ENABLED
    BtEvent     event;

    (bt.me.btEvent).handler = 0;
    event = (bt.me.btEvent);
#endif

    MeDevEventHandler();

    Assert(IsListCircular(&(bt.me.regList)));
    entry = (bt.me.regList).Flink;
    while(entry != &(bt.me.regList)) {
        /* Point to the next entry before calling the callback in case the
         * callback removes the current handler
         */
        handler = (BtHandler*)entry;
        entry = entry->Flink;
        
        if (handler->eMask & mask) {
            /* Call the callback */
            (bt.me.btEvent).handler = handler;
            handler->callback(&(bt.me.btEvent));
        }
#if XA_DEBUG == XA_ENABLED
        /* If this assert fires, it means that the current handler modified a
         * portion of the BtEvent structure. This is very bad and should be fixed.
         */
        (bt.me.btEvent).handler = 0;
        Assert(OS_MemCmp((U8 *)&event, (U16)sizeof(BtEvent), (U8 *)&(bt.me.btEvent), (U16)sizeof(BtEvent)) == TRUE);
#endif
        Assert(IsListCircular(entry));
    }
    Assert(IsListCircular(&(bt.me.regList)));

    /* Start a disconnect that may be waiting */
    MeStartOperation();
}

/*---------------------------------------------------------------------------
 *            MeReportNMResults()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Call the callback of all the global handlers with a Non Maskable
 *            event. 
 *
 * Return:    void
 */
void MeReportNMResults(void)
{
    ListEntry*  entry;
    BtHandler*  handler;
#if XA_DEBUG == XA_ENABLED
    BtEvent     event;

    (bt.me.btEvent).handler = 0;
    event = (bt.me.btEvent);
#endif

    MeDevEventHandler();

    Assert(IsListCircular(&(bt.me.regList)));
    entry = (bt.me.regList).Flink;
    while(entry != &(bt.me.regList)) {
        /* Point to the next entry before calling the callback in case the
         * callback removes the current handler
         */
        handler = (BtHandler*)entry;
        entry = entry->Flink;
        
        /* Call the callback */
        (bt.me.btEvent).handler = handler;
        handler->callback(&(bt.me.btEvent));

#if XA_DEBUG == XA_ENABLED
        /* If this assert fires, it means that the current handler modified a
         * portion of the BtEvent structure. This is very bad and should be fixed.
         */
        (bt.me.btEvent).handler = 0;
        Assert(OS_MemCmp((U8 *)&event, (U16)sizeof(BtEvent), (U8 *)&(bt.me.btEvent), (U16)sizeof(BtEvent)) == TRUE);
#endif

        Assert(IsListCircular(entry));
    }
    Assert(IsListCircular(&(bt.me.regList)));

    /* Start a disconnect that may be waiting */
    MeStartOperation();
}

/*---------------------------------------------------------------------------
 *            ReportCommandSent()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  
 *            
 *
 * Return:    void
 */
static void ReportCommandSent(const HciCommand *cmd)
{
    ListEntry*  entry;
    BtHandler*  handler;
    BtEvent     event;

    event.eType = BTEVENT_HCI_COMMAND_SENT;
    event.p.hciCmd.type = LEtoHost16(cmd->header);
    event.p.hciCmd.length = cmd->header[2];
    event.p.hciCmd.parms = cmd->parms;

    Assert(IsListCircular(&(bt.me.regList)));
    entry = (bt.me.regList).Flink;
    while(entry != &(bt.me.regList)) {
        /* Point to the next entry before calling the callback in case the
         * callback removes the current handler
         */
        handler = (BtHandler*)entry;
        entry = entry->Flink;
        
        /* Call the callback */
        event.handler = handler;
        handler->callback(&event);

        Assert(IsListCircular(entry));
    }
    Assert(IsListCircular(&(bt.me.regList)));
}

/*---------------------------------------------------------------------------
 *            Me_Callback()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  This function is called by the HCI layer to report all events
 *            and status. 
 *
 * Return:    void
 */
void ME_Callback(U8 event, HciCallbackParms *parms)
{
#if JETTEST == XA_ENABLED
    if (Tester_MeReceive) {
        (*Tester_MeReceive)(event, parms);
        return;
    }
#endif /* JETTEST == XA_ENABLED */

    switch (event) {
    case HCI_DATA_IND:
        /* We received data so see who it is for.  */
#if BT_SCO_HCI_DATA == XA_ENABLED
        if (ScoMapHciToConnect(parms->hciHandle) != 0) {
            /* Data is SCO data so have SCO deal with it */
            ScoDataCallback(event, parms);
            return;
        }
#endif /* BT_SCO_HCI_DATA == XA_ENABLED */

        /* Send data to L2CAP */
        L2CAP_HciReceive(parms->ptr.rxBuff, parms->hciHandle); 
        break;

    case HCI_PACKET_HANDLED:
        /* A packet is returned from HCI see where it goes */
#if BT_SCO_HCI_DATA == XA_ENABLED
        if (ScoMapHciToConnect(parms->hciHandle) != 0) {
            /* The packet belongs to SCO */
            ScoDataCallback(event, parms);
            return;
        }
#endif /* BT_SCO_HCI_DATA == XA_ENABLED */

        /* Give the packet to L2CAP */
        L2CAP_HciTxDone(parms->ptr.packet, parms->status, parms->hciHandle); 
        break;

    case HCI_COMMAND_HANDLED:
        (bt.me.commandPending) = FALSE;
        /* The command buffer is free */
        Report(("ME: HCI Command sent\n"));
        InitializeListEntry(&(parms->ptr.cmd->node));
        if (parms->status == BT_STATUS_SUCCESS)
            ReportCommandSent(parms->ptr.cmd);

        /* start operation queue, if it is waiting for the command buffer */
        if ((bt.me.opMask) & BOM_HCICOMMAND_BUSY) {
            (bt.me.opMask) &= ~BOM_HCICOMMAND_BUSY;
            MeStartOperation();
        }
        /* finish init radio task, if it is waiting */
        if ((bt.me.opMask) & BOM_INITRADIO_BUSY) {
            (bt.me.opMask) &= ~BOM_INITRADIO_BUSY;
            MeInitRadio(BOE_TASK_END);
        }
        break;

    case HCI_SEND_IND:
        /* Request to send data */
#if BT_SCO_HCI_DATA == XA_ENABLED
        if (ScoMapHciToConnect(parms->hciHandle) != 0) {
            ScoDataCallback(event, parms);
            return;
        }
#endif /* BT_SCO_HCI_DATA == XA_ENABLED */

        /* Tell L2CAP it can send data */
        parms->ptr.packet = L2CAP_HciTxReady(parms->hciHandle); 
        break;

    case HCI_CONTROLLER_EVENT:
        /* Handle the event. */
        (void)MeHandleEvents(parms->ptr.hciEvent->event, 
                             parms->ptr.hciEvent->len, 
                             parms->ptr.hciEvent->parms);
        break;

    case HCI_INIT_STATUS:
        /* HCI initializaiton is complete */
        Report(("ME: HCI Init complete status: %d\n", parms->status));
        MeProcessInitStatus(parms);
        break;

    case HCI_DEINIT_STATUS:
        /* Set state to ensure no commands are queued. */
        (bt.me.stackState) = BTSS_INITIALIZE_ERR;

        /* HCI deintialization is complete. All ACL and SCO connections
         * should have been disconnected by HCI. Return any existing commands.
         * and attempt to initialize HCI 
         */
        Report(("ME: HCI deinit complete status: %d\n", parms->status));

        (bt.me.curTask) = 0;
        MeFailPendingCommands();
        
        (bt.me.stackState) = BTSS_NOT_INITIALIZED;
        (bt.me.btEvent).eType = BTEVENT_HCI_DEINITIALIZED;
        MeReportNMResults();

#if BT_SECURITY == XA_ENABLED
        (void)DDB_Close();
#endif
        break;

    case HCI_TRANSPORT_ERROR:
        Report(("ME: HCI transport error\n"));
        (bt.me.btEvent).eType = BTEVENT_HCI_FATAL_ERROR;
        MeReportNMResults();
        HCI_Deinit();
        break;


    }
}


/*---------------------------------------------------------------------------
 *            MeHandleEvents()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle events from the HCI.
 *
 * Return:    TRUE if handled FALSE otherwise
 */
BOOL MeHandleEvents(U8 event, U8  len, U8* parm)
{
    MeCommandToken *token;
    U16             opcode;
    BOOL            useTaskHandler;

    switch (event) {

    case HCE_INQUIRY_RESULT_WITH_RSSI:
        HandleInquiryResult(len, parm, TRUE);
        break;

    case HCE_INQUIRY_RESULT:
        HandleInquiryResult(len, parm, FALSE);
        break;

    case HCE_INQUIRY_COMPLETE:
        HandleInquiryComplete(len, parm);
        break;

    case HCE_CONNECT_REQUEST:  /* Incoming connection */
        MeHandleLinkConnectReq(len, parm);
        break;

    case HCE_CONNECT_COMPLETE:
        MeHandleConnectComplete(len,parm);
        break;

    case HCE_DISCONNECT_COMPLETE:
        MeHandleDisconnectComplete(len,parm);
        break;

#if NUM_SCO_CONNS > 0
    case HCE_SYNC_CONNECT_COMPLETE:
        ScoHandleConnectComplete(len, parm);
        break;

    case HCE_SYNC_CONN_CHANGED:
        ScoHandleConnChanged(len, parm);
        break;
#endif /* NUM_SCO_CONNS > 0 */

    case HCE_PIN_CODE_REQ:
#if BT_SECURITY == XA_DISABLED
        SecHandleDenyPinCodeReq(len,parm);
        break;
#else /* BT_SECURITY == XA_ENABLED */
        SecHandlePinCodeReq(len,parm);
        break;

    case HCE_LINK_KEY_REQ:
        SecHandleLinkKeyReq(len,parm);
        break;

    case HCE_AUTH_COMPLETE:
        SecHandleAuthenticateComplete(len,parm);
        break;

    case HCE_RETURN_LINK_KEYS:
        SecHandleReturnLinkKeys(len,parm);
        break;

    case HCE_LINK_KEY_NOTIFY:
        SecHandleLinkKeyNotify(len,parm);
        break;

    case HCE_ENCRYPT_CHNG:
        SecHandleEncryptChange(len,parm);
        break;
#endif /* BT_SECURITY */

    case HCE_REMOTE_NAME_REQ_COMPLETE:
        MeHandleRemNameReqComplete(len,parm);
        break;

    case HCE_ROLE_CHANGE:
        MeHandleRoleChange(len, parm);
        break;

    case HCE_MODE_CHNG:
        MeHandleModeChange(len,parm);
        break;

    case HCE_MAX_SLOTS_CHNG:
        Report(("ME: MeHandleEvents - MaxSlotsChange event\n"));
        break;

    case HCE_QOS_SETUP_COMPLETE:
    case HCE_PAGE_SCAN_REPETITION_MODE:
        /* Ignore these events */
        break;

    default:
        /* First, check to see if a handler exists */
        useTaskHandler = FALSE;
        if ((bt.me.taskHandler) != 0) {
            /* Handler exists. We need to make sure it is for
             * this event. This may be an event from an 
             * asynchronous command or an unexpected event.
             */

            /* Check if event has opcode */
            switch (event) {
            case HCE_COMMAND_COMPLETE:
                opcode = LEtoHost16((parm+1));
                break;

            case HCE_COMMAND_STATUS:
                opcode = LEtoHost16((parm+2));
                break;

            default:
                opcode = 0;
                break;
            }

            /* Use the taskHandler if the opcode matches OR
             * if no opcode, the taskEvent matches.
             */
            if ((opcode && opcode == (bt.me.curTask)) ||
                (!opcode && event == (bt.me.taskEvent))) {
                /* use the taskHandler */
                useTaskHandler = TRUE;
            }
        }


        /* Use the taskHandler if it matched above. */
        if (useTaskHandler == TRUE) {
            /* Call the task handler */
            (bt.me.taskHandler)(event, len, parm);
        } else {
            /* Handler does not exist so see if there is a general command
             * waiting for the event
             */
            token = MeFindGeneralEvent(event, parm);
            if (token != 0) {
                /* We have general command looking for this event. */
                token->p.general.out.status = BT_STATUS_SUCCESS;
                token->p.general.out.event = event;
                token->p.general.out.parmLen = len;
                token->p.general.out.parms = parm;
                MeProcessGenToken(token);
            } else {
                /* There is no one to handle this event */
                Report(("ME: HCI Handle event with no task handler, event = 0x%x\n",event));
        
                /* Not handled so return FALSE */
                return FALSE;
            }
        }
    }

    /* Handled so return TRUE */
    return TRUE;
}


/*---------------------------------------------------------------------------
 *            MeOperationEnd()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  End the current operation and start a new one if one
 *            exists.
 *
 * Return:    void
 */
void MeOperationEnd(void)
{
    (bt.me.taskHandler) = 0;
    (bt.me.curOp) = 0;
    MeStartOperation();
}

/*---------------------------------------------------------------------------
 *            void MeProcessToken()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Add the token to the appropriate queue.  
 *
 * Return:    void
 */
void MeProcessToken(MeCommandToken *token)
{
    /* If an operation is in progress then put this token on the command queue */
    if (token->flags & MCTF_NEED_CHECK) {
        if ((MeFindMatchingToken(token, &(bt.me.opList)) != 0) ||
            (MeFindMatchingToken(token, &(bt.me.cmdList)) != 0)) {
            InsertTailList(&(bt.me.cmdList), &(token->op.node));
            return;
        }
    }

    /* Start up an operation */
    MeAddOperation(token);

    /* Attempt to start the operation */
    MeStartOperation();
}


/*---------------------------------------------------------------------------
 *            void MeProcessGenToken()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Process the general token.  
 *
 * Return:    void
 */
void MeProcessGenToken(MeCommandToken *token)
{
    MeCommandToken *next;

    /* Remove this token from the cmd list and see if a match exists on
     * the asyncList
     */
    Assert(IsNodeOnList(&(bt.me.cmdList), &(token->op.node)));
    RemoveEntryList(&(token->op.node));

    next = MeFindMatchingToken(token, &(bt.me.asyncList));
    if (next != 0) {
        /* Found another async token to run so put it on the op queue */
        Assert(IsNodeOnList(&(bt.me.asyncList), &(next->op.node)));
        RemoveEntryList(&(next->op.node));
        MeAddOperation(next);
        MeStartOperation();
    }

    /* Report the results of this token */
    (bt.me.btEvent).p.meToken = token;
    (bt.me.btEvent).eType = token->eType;
    token->callback(&(bt.me.btEvent));
    /* Start a disconnect that may be waiting */
    MeStartOperation();
}

/*---------------------------------------------------------------------------
 *            void MeFindMatchingToken()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  If a token already exists on the list that is making the same 
 *            request then return a pointer to it. Otherwise return 0..  
 *
 * Return:    MeCommandToken* or 0
 */
MeCommandToken* MeFindMatchingToken(MeCommandToken* token, ListEntry* list)
{
    MeCommandToken *curToken;

    /* Check the operation queue */
    curToken = (MeCommandToken*)list->Flink;
    while (curToken != (MeCommandToken*)list) {
        if (IsMeCommandEqual(token, curToken)) {
            /* The commands are the same */
            return curToken;
        }
        curToken = (MeCommandToken*) (curToken->op.node.Flink);
    }

    return 0;
}

/*---------------------------------------------------------------------------
 *            IsMeCommandEqual()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Determine if one token is performing the same command as
 *            another. 
 *
 * Return:    TRUE - if commands are the same otherwise FALSE.
 */
static BOOL IsMeCommandEqual(MeCommandToken* token1, MeCommandToken* token2)
{
    if ((token1->op.opType == token2->op.opType) ||
        (token1->op.opType == BOP_CANCEL_REM_NAME &&
        token2->op.opType == BOP_REM_NAME_REQ)) {
        /* We found a token performing the same operation. Now see if
         * it has the same input parameters (if necessary)
         */
        switch (token1->op.opType) {
        case BOP_REM_NAME_REQ:
        case BOP_CANCEL_REM_NAME:
            /* Remote Name request */
            return (OS_MemCmp(token1->p.name.bdAddr.addr, 6, 
                          token2->p.name.bdAddr.addr, 6));

        case BOP_GEN_COMMAND:
            if ((token1->flags & MCTF_ASYNC) && (token2->flags & MCTF_ASYNC) &&
                (token1->p.general.in.event == token2->p.general.in.event)) {
                return TRUE;
            }
            break;

        default:
            /* Only the command needs to match */
            return TRUE;
        }
    }
    return FALSE;
}

/*---------------------------------------------------------------------------
 *            void MeFindGeneralEvent()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Find the token on the cmdList that is looking for the specified
 *            event.   
 *
 * Return:    MeCommandToken* or 0
 */
MeCommandToken* MeFindGeneralEvent(BtOpEvent event, U8 *parm)
{
    U16             opcode;
    MeCommandToken *curToken;

    /* Check if event has opcode */
    switch (event) {
    case HCE_COMMAND_COMPLETE:
        opcode = LEtoHost16((parm+1));
        break;

    case HCE_COMMAND_STATUS:
        opcode = LEtoHost16((parm+2));
        break;

    default:
        opcode = 0;
        break;
    }

    /* Check the operation queue */
    curToken = (MeCommandToken*)((bt.me.cmdList).Flink);
    while (curToken != (MeCommandToken*)&((bt.me.cmdList))) {
        if (curToken->p.general.in.event == event) {
            if (!opcode || (opcode == curToken->p.general.in.hciCommand)) {
                return curToken;;
            }
        }
        curToken = (MeCommandToken*) (curToken->op.node.Flink);
    }

    return 0;
}

/*---------------------------------------------------------------------------
 *            void ReportInquiryComplete()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Report results of the inquiry complete. 
 *
 * Return:    void
 */
static void ReportInquiryComplete(void)
{
    if (!((bt.me.opMask) & BOM_CANCEL_INQUIRY)) {
        /* A cancel is not in progress so report result clear flag. */
        Report(("ME: Reporting inquiry complete event\n"));
        (bt.me.opMask) &= ~BOM_INQUIRY;
    } else {
        /* A cancel is in progress so wait until it returns to clear flag*/
        Report(("ME: Inquiry complete event - cancel in progress\n"));
    }
    (bt.me.btEvent).eType = BTEVENT_INQUIRY_COMPLETE;
    MeReportResults(BEM_INQUIRY_COMPLETE);
}


/*---------------------------------------------------------------------------
 *            void MeReportInqCancelComplete()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Report results of the inquiry cancel complete. 
 *
 * Return:    void
 */
void MeReportInqCancelComplete(void)
{
    (bt.me.opMask) &= ~(BOM_CANCEL_INQUIRY | BOM_INQUIRY);
    (bt.me.btEvent).eType = BTEVENT_INQUIRY_CANCELED;
    MeReportResults(BEM_INQUIRY_CANCELED);
}

/*---------------------------------------------------------------------------
 *            MeReportMeCommandComplete()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Report results of a ME commands.  
 *
 * Return:    void
 */
void MeReportMeCommandComplete(MeCommandToken* token)
{
    MeCommandToken* tmp;

    /* If this command is of the type that all tokens receive the
     * results from this one invocation. Then find all the matches.
     */
    (bt.me.btEvent).eType = token->eType;
    if (token->flags & MCTF_NEED_CHECK) {
        /* Find all matching tokens and report the results */
        while ((tmp = MeFindMatchingToken(token, &(bt.me.cmdList))) != 0) {
            /* Remove the token from the list */
            RemoveEntryList(&(tmp->op.node));

            /* Copy the parameter information in the token */
            if (tmp != token) {
                tmp->p = token->p;
            }

            /* Call the callback */
            (bt.me.btEvent).p.meToken = tmp;
            tmp->callback(&(bt.me.btEvent));
        }
    } else {
        /* Handle only this command */
        if (IsNodeOnList(&(bt.me.cmdList), &(token->op.node))) {
            RemoveEntryList(&(token->op.node));
        }
        (bt.me.btEvent).p.meToken = token;
        token->callback(&(bt.me.btEvent));
    }
    /* Start a disconnect that may be waiting */
    MeStartOperation();
}


/*---------------------------------------------------------------------------
 *            MeIsValidGeneralToken()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Determine if the token is valid for a general command. The
 *            hciCommand and event fields are checked.   
 *
 * Return:    TRUE if token is valid otherwise FALSE.
 */
BOOL MeIsValidGeneralToken(MeCommandToken* token)
{
    /* First check the hciCommand */
    switch (token->p.general.in.hciCommand) {
    case HCC_INQUIRY:                       /*  0x0401 */
    case HCC_INQUIRY_CANCEL:                /*  0x0402 */
      /* Enabled:
       * HCC_START_PERIODIC_INQ_MODE:           0x0403
       * HCC_EXIT_PERIODIC_INQ_MODE:            0x0404
       */
    case HCC_CREATE_CONNECTION:             /*  0x0405 */
    case HCC_DISCONNECT:                    /*  0x0406 */
    case HCC_ADD_SCO_CONNECTION:            /*  0x0407 Not in 1.2 */
      /* Enabled:
       * HCC_CREATE_CONNECTION_CANCEL:          0x0408 1.2
       */
    case HCC_ACCEPT_CON_REQ:                /*  0x0409 */
    case HCC_REJECT_CON_REQ:                /*  0x040A */
    case HCC_LINK_KEY_REQ_REPL:             /*  0x040B */
    case HCC_LINK_KEY_REQ_NEG_REPL:         /*  0x040C */
    case HCC_PIN_CODE_REQ_REPL:             /*  0x040D */
    case HCC_PIN_CODE_REQ_NEG_REPL:         /*  0x040E */
      /* Enabled:
       * HCC_CHNG_CONN_PACKET_TYPE              0x040F
       */
    case HCC_AUTH_REQ:                      /*  0x0411 */
    case HCC_SET_CONN_ENCRYPT:              /*  0x0413 */
      /* Enabled:
       * HCC_CHNG_CONN_LINK_KEY:                0x0415
       */
    case HCC_MASTER_LINK_KEY:               /*  0x0417 */
    case HCC_REM_NAME_REQ:                  /*  0x0419 */
      /* Enabled:
       * HCC_REM_NAME_REQ_CANCEL:               0x041A 1.2 
       * HCC_READ_REMOTE_FEATURES               0x041B
       * HCC_READ_REMOTE_EXT_FEATURES           0x041C 1.2
       * HCC_READ_REMOTE_VERSION                0x041D
       * HCC_READ_CLOCK_OFFSET                  0x041F
       * HCC_READ_LMP_HANDLE                    0x0420 1.2
       * HCC_EXCHANGE_FIXED_INFO                0x0421 1.2
       * HCC_EXCHANGE_ALIAS_INFO                0x0422 1.2
       * HCC_PRIVATE_PAIRING_REQ_REPL           0x0423 1.2
       * HCC_PRIVATE_PAIRING_REQ_NEG_REPL       0x0424 1.2
       * HCC_GENERATED_ALIAS                    0x0425 1.2
       * HCC_ALIAS_ADDRESS_REQ_REPL             0x0426 1.2
       * HCC_ALIAS_ADDRESS_REQ_NEG_REPL         0x0427 1.2
       */
    case HCC_SETUP_SYNC_CONNECTION:         /*  0x0428 1.2 */
    case HCC_ACCEPT_SYNC_CON_REQ:           /*  0x0429 1.2 */
    case HCC_REJECT_SYNC_CON_REQ:           /*  0x042A 1.2 */

    case HCC_HOLD_MODE:                     /*  0x0801 */
    case HCC_SNIFF_MODE:                    /*  0x0803 */
    case HCC_EXIT_SNIFF_MODE:               /*  0x0804 */
    case HCC_PARK_MODE:                     /*  0x0805 */
    case HCC_EXIT_PARK_MODE:                /*  0x0806 */
    case HCC_QOS_SETUP:                     /*  0x0807 */
      /* Enabled:
       * HCC_ROLE_DISCOVERY                     0x0809
       * HCC_SWITCH_ROLE                        0x080B
       * HCC_READ_LINK_POLICY                   0x080C
       * HCC_WRITE_LINK_POLICY                  0x080D
       * HCC_READ_DEFAULT_LINK_POLICY           0x080E 1.2
       */

    case HCC_WRITE_DEFAULT_LINK_POLICY:     /*  0x080F 1.2 */
      /* Enabled:
       * HCC_FLOW_SPECIFICATION                 0x0810 1.2
       */

    case HCC_SET_EVENT_MASK:                /*  0x0C01 */
    case HCC_RESET:                         /*  0x0C03 */
    case HCC_EVENT_FILTER:                  /*  0x0C05 */
    case HCC_FLUSH:                         /*  0x0C08 */
    case HCC_READ_PIN_TYPE:                 /*  0x0C09 */
    case HCC_WRITE_PIN_TYPE:                /*  0x0C0A */
    case HCC_CREATE_NEW_UNIT_KEY:           /*  0x0C0B */
      /* Enabled:
       * HCC_READ_STORED_LINK_KEY:              0x0C0D
       * HCC_WRITE_STORED_LINK_KEY:             0x0C11
       * HCC_DEL_STORED_LINK_KEY:               0x0C12
       */
    case HCC_CHNG_LOCAL_NAME:               /*  0x0C13 */
      /* Enabled:
       * HCC_READ_LOCAL_NAME                    0x0C14
       * HCC_READ_CONN_ACCEPT_TIMEOUT:          0x0C15
       */
    case HCC_WRITE_CONN_ACCEPT_TIMEOUT:     /*  0x0C16 */
      /* Enabled:
       * HCC_READ_PAGE_TIMEOUT                  0x0C17
       * HCC_WRITE_PAGE_TIMEOUT                 0x0C18
       * HCC_READ_SCAN_ENABLE:                  0x0C19
       */
    case HCC_WRITE_SCAN_ENABLE:             /*  0x0C1A */
      /* Enabled:
       * HCC_READ_PAGE_SCAN_ACTIVITY:           0x0C1B
       */
    case HCC_WRITE_PAGE_SCAN_ACTIVITY:      /*  0x0C1C */
      /* Enabled:
       * HCC_READ_INQ_SCAN_ACTIVITY:            0x0C1D
       */
    case HCC_WRITE_INQ_SCAN_ACTIVITY:       /*  0x0C1E */
      /* Enabled:
       * HCC_READ_AUTH_ENABLE:                  0x0C1F
       */
    case HCC_WRITE_AUTH_ENABLE:             /*  0x0C20 */
      /* Enabled:
       * HCC_READ_ENCRYPT_MODE:                 0x0C21
       */
    case HCC_WRITE_ENCRYPT_MODE:            /*  0x0C22 */
      /* Enabled:
       * HCC_READ_CLASS_OF_DEVICE:              0x0C23
       */
    case HCC_WRITE_CLASS_OF_DEVICE:         /*  0x0C24 */
      /* Enabled:
       * HCC_READ_VOICE_SETTING:                0x0C25
       */
    case HCC_WRITE_VOICE_SETTING:           /*  0x0C26 */
      /* Enabled:
       * HCC_READ_AUTO_FLUSH_TIMEOUT:           0x0C27
       */
    case HCC_WRITE_AUTO_FLUSH_TIMEOUT:      /*  0x0C28 */
      /* Enabled:
       * HCC_READ_NUM_BC_RETRANSMIT:            0x0C29
       */
    case HCC_WRITE_NUM_BC_RETRANSMIT:       /*  0x0C2A */
      /* Enabled:
       * HCC_READ_HOLD_MODE_ACTIVITY:           0x0C2B
       */
    case HCC_WRITE_HOLD_MODE_ACTIVITY:      /*  0x0C2C */
      /* Enabled:
       * HCC_READ_XMIT_POWER_LEVEL:             0x0C2D
       * HCC_READ_SCO_FC_ENABLE:                0x0C2E
       */
    case HCC_WRITE_SCO_FC_ENABLE:           /*  0x0C2F */
    case HCC_SET_CTRLR_TO_HOST_FLOW_CTRL:   /*  0x0C31 */
    case HCC_HOST_BUFFER_SIZE:              /*  0x0C33 */
    case HCC_HOST_NUM_COMPLETED_PACKETS:    /*  0x0C35 */
      /* Enabled:
       * HCC_READ_LINK_SUPERV_TIMEOUT           0x0C36
       *
       * Caution: The Link Supervision Timeout value is
       * negotiated at the Link Manager level based on
       * Hold, Sniff & Park values. Setting this value
       * from the application is an advisory default
       * setting.
       * HCC_WRITE_LINK_SUPERV_TIMEOUT:         0x0C37
       *
       * HCC_READ_NUM_IAC:                      0x0C38
       * HCC_READ_CURRENT_IAC_LAP:              0x0C39
       */
    case HCC_WRITE_CURRENT_IAC_LAP:         /*  0x0C3A */
      /* Enabled:
       * HCC_READ_PAGE_SCAN_PERIOD_MODE:        0x0C3B
       */
    case HCC_WRITE_PAGE_SCAN_PERIOD_MODE:   /*  0x0C3C */
      /* Enabled:
       * HCC_READ_PAGE_SCAN_MODE:               0x0C3D Not in 1.2
       */
    case HCC_WRITE_PAGE_SCAN_MODE:          /*  0x0C3E Not in 1.2 */
      /* Enabled:
       * HCC_SET_AFH_HOST_CHAN_CLASS            0x0C3F 1.2
       * HCC_READ_INQ_SCAN_TYPE                 0x0C42 1.2
       * HCC_WRITE_INQ_SCAN_TYPE                0x0C43 1.2
       * HCC_READ_INQ_MODE                      0x0C44 1.2
       * HCC_WRITE_INQ_MODE                     0x0C45 1.2
       * HCC_READ_PAGE_SCAN_TYPE                0x0C46 1.2
       * HCC_WRITE_PAGE_SCAN_TYPE               0x0C47 1.2
       * HCC_READ_AFH_CHAN_ASSESS_MODE          0x0C48 1.2
       * HCC_WRITE_AFH_CHAN_ASSESS_MODE         0x0C49 1.2
       * HCC_READ_ANONYMITY_MODE                0x0C4A 1.2
       * HCC_WRITE_ANONYMITY_MODE               0x0C4B 1.2
       * HCC_READ_ALIAS_AUTH_ENABLE             0x0C4C 1.2
       * HCC_WRITE_ALIAS_AUTH_ENABLE            0x0C4D 1.2
       * HCC_READ_ANON_ADDR_CHNG_PARM           0x0C4E 1.2
       * HCC_WRITE_ANON_ADDR_CHNG_PARM          0x0C4F 1.2
       * HCC_RESET_FIXED_ADDR_COUNTER           0x0C50 1.2

       * HCC_READ_LOCAL_VERSION                 0x1001
       * HCC_READ_LOCAL_COMMANDS                0x1002 1.2
       * HCC_READ_LOCAL_FEATURES                0x1003
       * HCC_READ_LOCAL_EXT_FEATURES            0x1004 1.2
       * HCC_READ_BUFFER_SIZE                   0x1005
       * HCC_READ_COUNTRY_CODE                  0x1007 Not in 1.2   
       * HCC_READ_BD_ADDR                       0x1009

       * HCC_READ_FAILED_CONTACT_COUNT          0x1401
       * HCC_RESET_FAILED_CONTACT_COUNT         0x1402
       * HCC_GET_LINK_QUALITY                   0x1403
       * HCC_READ_RSSI                          0x1405
       * HCC_READ_AFH_CHANNEL_MAP               0x1406 1.2
       * HCC_READ_CLOCK                         0x1407 1.2

       * HCC_READ_LOOPBACK_MODE                 0x1801
       * HCC_WRITE_LOOPBACK_MODE                0x1802
       * HCC_ENABLE_DUT                         0x1803
       */
        return FALSE;
   }

    /* Next check the event */
    switch (token->p.general.in.event) {
    case HCE_INQUIRY_COMPLETE:              /*  0x01 */
    case HCE_INQUIRY_RESULT:                /*  0x02 */
    case HCE_CONNECT_COMPLETE:              /*  0x03 */
    case HCE_CONNECT_REQUEST:               /*  0x04 */
    case HCE_DISCONNECT_COMPLETE:           /*  0x05 */
    case HCE_AUTH_COMPLETE:                 /*  0x06 */
    case HCE_REMOTE_NAME_REQ_COMPLETE:      /*  0x07 */
    case HCE_ENCRYPT_CHNG:                  /*  0x08 */
      /* Enabled:
       * HCE_CHNG_CONN_LINK_KEY_COMPLETE:       0x09
       */
    case HCE_MASTER_LINK_KEY_COMPLETE:      /*  0x0A */
      /* Enabled:
       * HCE_READ_REMOTE_FEATURES_COMPLETE      0x0B
       * HCE_READ_REMOTE_VERSION_COMPLETE       0x0C
       */
    case HCE_QOS_SETUP_COMPLETE:            /*  0x0D */
      /* Enabled:
       * HCE_COMMAND_COMPLETE                   0x0E
       * HCE_COMMAND_STATUS                     0x0F
       */
    case HCE_HARDWARE_ERROR:                /*  0x10 */
    case HCE_FLUSH_OCCURRED:                /*  0x11 */
    case HCE_ROLE_CHANGE:                   /*  0x12 */
    case HCE_NUM_COMPLETED_PACKETS:         /*  0x13 */
    case HCE_MODE_CHNG:                     /*  0x14 */
    case HCE_RETURN_LINK_KEYS:              /*  0x15 */
    case HCE_PIN_CODE_REQ:                  /*  0x16 */
    case HCE_LINK_KEY_REQ:                  /*  0x17 */
    case HCE_LINK_KEY_NOTIFY:               /*  0x18 */
    case HCE_LOOPBACK_COMMAND:              /*  0x19 */
    case HCE_DATA_BUFFER_OVERFLOW:          /*  0x1A */
    case HCE_MAX_SLOTS_CHNG:                /*  0x1B */
      /* Enabled:
       * HCE_READ_CLOCK_OFFSET_COMPLETE         0x1C
       * HCE_CONN_PACKET_TYPE_CHNG:             0x1D
       */
    case HCE_QOS_VIOLATION:                 /*  0x1E */
    case HCE_PAGE_SCAN_MODE_CHANGE:         /*  0x1F Not in 1.2 */
    case HCE_PAGE_SCAN_REPETITION_MODE:     /*  0x20 */
      /* Enabled:
       * HCE_FLOW_SPECIFICATION_COMPLETE        0x21 1.2
       * HCE_INQUIRY_RESULT_WITH_RSSI           0x22 1.2
       * HCE_READ_REMOTE_EXT_FEAT_COMPLETE      0x23 1.2
       * HCE_FIXED_ADDRESS                      0x24 1.2
       * HCE_ALIAS_ADDRESS                      0x25 1.2
       * HCE_GENERATE_ALIAS_REQ                 0x26 1.2
       * HCE_ACTIVE_ADDRESS                     0x27 1.2
       * HCE_ALLOW_PRIVATE_PAIRING              0x28 1.2
       * HCE_ALIAS_ADDRESS_REQ                  0x29 1.2
       * HCE_ALIAS_NOT_RECOGNIZED               0x2A 1.2
       * HCE_FIXED_ADDRESS_ATTEMPT              0x2B 1.2
       */
    case HCE_SYNC_CONNECT_COMPLETE:         /*  0x2C 1.2 */
    case HCE_SYNC_CONN_CHANGED:             /*  0x2D 1.2 */
      /* Enabled:
       * HCE_BLUETOOTH_LOGO                     0xFE
       * HCE_VENDOR_SPECIFIC                    0xFF
       */
        return FALSE;
    }

    return TRUE;
}

/*---------------------------------------------------------------------------
 *            MeCheckRemDevToken()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Determine if the token references an existing remDev. If so,
 *            token->remDev is set.
 *
 * Return:    void.
 */
void MeCheckRemDevToken(MeCommandToken* token)
{
    /* Default to no remDev. */
    token->remDev = 0;

    switch (token->p.general.in.hciCommand) {
    /* Identify the remDev for these commands
     * through the passed in connection handle.
     */
    case HCC_CHNG_CONN_PACKET_TYPE:         /* 0x040F */
    case HCC_READ_REMOTE_FEATURES:          /* 0x041B */
    case HCC_READ_REMOTE_VERSION:           /* 0x041D */
    case HCC_READ_CLOCK_OFFSET:             /* 0x041F */
    case HCC_ROLE_DISCOVERY:                /* 0x0809 */
    case HCC_READ_LINK_POLICY:              /* 0x080C */
    case HCC_WRITE_LINK_POLICY:             /* 0x080D */
    case HCC_READ_AUTO_FLUSH_TIMEOUT:       /* 0x0C27 */
    case HCC_READ_XMIT_POWER_LEVEL:         /* 0x0C2D */
    case HCC_READ_LINK_SUPERV_TIMEOUT:      /* 0x0C36 */
    case HCC_READ_FAILED_CONTACT_COUNT:     /* 0x1401 */
    case HCC_RESET_FAILED_CONTACT_COUNT:    /* 0x1402 */
    case HCC_GET_LINK_QUALITY:              /* 0x1403 */
    case HCC_READ_RSSI:                     /* 0x1405 */
    case HCC_READ_REMOTE_EXT_FEATURES:      /* 0x041C 1.2 */
    case HCC_READ_LMP_HANDLE:               /* 0x0420 1.2 */
    case HCC_EXCHANGE_FIXED_INFO:           /* 0x0421 1.2 */
    case HCC_EXCHANGE_ALIAS_INFO:           /* 0x0422 1.2 */
    case HCC_PRIVATE_PAIRING_REQ_REPL:      /* 0x0423 1.2 */
    case HCC_PRIVATE_PAIRING_REQ_NEG_REPL:  /* 0x0424 1.2 */
    case HCC_GENERATED_ALIAS:               /* 0x0425 1.2 */
    case HCC_SETUP_SYNC_CONNECTION:        /* 0x0428 1.2 */
    case HCC_FLOW_SPECIFICATION:            /* 0x0810 1.2 */
    case HCC_READ_AFH_CHANNEL_MAP:          /* 0x1406 1.2 */
    case HCC_READ_CLOCK:                    /* 0x1407 1.2 */
        if (token->p.general.in.parms == 0) {
            return;
        }
        token->remDev = MeMapHciToRemoteDevice(
            (HciHandle)(LEtoHost16(token->p.general.in.parms) & 0x0fff));
        break;

    /* Identify the remDev for these commands
     * through the passed in BD_ADDR.
     */
    case HCC_SWITCH_ROLE:                   /* 0x080B */
    case HCC_CREATE_CONNECTION_CANCEL:      /* 0x0408 1.2 */
    case HCC_REM_NAME_REQ_CANCEL:           /* 0x041A 1.2 */
    case HCC_ALIAS_ADDRESS_REQ_REPL:        /* 0x0426 1.2 */
    case HCC_ALIAS_ADDRESS_REQ_NEG_REPL:    /* 0x0427 1.2 */
    case HCC_ACCEPT_SYNC_CON_REQ:          /* 0x0429 1.2 */
    case HCC_REJECT_SYNC_CON_REQ:          /* 0x042A 1.2 */
        if (token->p.general.in.parms == 0) {
            return;
        }
        token->remDev = ME_FindRemoteDeviceP(token->p.general.in.parms);
        break;

    default:
        /* Ignore others */
        break;
    }
}

/*---------------------------------------------------------------------------
 *            MeProcessInitStatus()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Process the HCI_INIT_STATUS event.   
 *
 * Return:    void
 */
void MeProcessInitStatus(HciCallbackParms *parms)
{
    Assert(((bt.me.stackState) == BTSS_NOT_INITIALIZED) ||
           ((bt.me.stackState) == BTSS_INITIALIZE_ERR));

    if (parms->status == BT_STATUS_FAILED) {
        (bt.me.stackState) = BTSS_NOT_INITIALIZED;
        (bt.me.btEvent).eType = BTEVENT_HCI_FAILED;
        MeReportNMResults();
    } else if (parms->status == BT_STATUS_HCI_INIT_ERR) {
        (bt.me.stackState) = BTSS_INITIALIZE_ERR;
        (bt.me.btEvent).eType = BTEVENT_HCI_INIT_ERROR;
        MeReportNMResults();
    } else {
        /* HCI has initialized successfully so get local radio parameters */
        MeInitRadio(BOE_START);
    }
}

/*---------------------------------------------------------------------------
 *            MeFailPendingCommands()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Fail all pending ME commands.   
 *
 * Return:    void
 */
void MeFailPendingCommands(void)
{
    I8 i;
    BtRemoteDevice* remDev;
    BtOperation* saveCurOp;

    saveCurOp = 0;
    /* First fail the current operation */
    if ((bt.me.curOp) != 0) {
        MeFailCommand((bt.me.curOp)->opType, (void*)(bt.me.curOp));
        saveCurOp = (bt.me.curOp);
    }

    /* Go through the operations queue and fail all operations */
    while (!IsListEmpty(&(bt.me.opList))) {
        (bt.me.curOp) = (BtOperation*)RemoveHeadList(&(bt.me.opList));
        InitializeListEntry(&((bt.me.curOp)->node));
        MeFailCommand((bt.me.curOp)->opType, (void *)(bt.me.curOp));
    }

    /* Clear the command queue */
    while (!IsListEmpty(&(bt.me.cmdList))) {
        (bt.me.curOp) = (BtOperation*)RemoveHeadList(&(bt.me.cmdList));

        if ((bt.me.curOp) != saveCurOp) {
            /* This is not the current operation so fail it */
            InitializeListEntry(&((bt.me.curOp)->node));
            MeFailCommand((bt.me.curOp)->opType, (void *)(bt.me.curOp));
        }
    }

    /* Clear the async queue */
    while (!IsListEmpty(&(bt.me.asyncList))) {
        (bt.me.curOp) = (BtOperation*)RemoveHeadList(&(bt.me.asyncList));
        if ((bt.me.curOp) != saveCurOp) {
            /* This is not the current operation so fail it */
            InitializeListEntry(&((bt.me.curOp)->node));
            MeFailCommand((bt.me.curOp)->opType, (void *)(bt.me.curOp));
        }
    }

    /* Clear all BtRemoteDevice structures */
    for (i = 0; (remDev = MeEnumerateRemoteDevices(i)) != 0; i++) {
        if (remDev->op.opType != BOP_NOP) {
            if (&(remDev->op) != saveCurOp) {
                /* This is not the current operation so fail it */
                MeFailCommand(remDev->op.opType, (void*)remDev);

                /* Verify no pending security tokens */
                Assert(IsListEmpty(&(remDev->secTokens)));
            }
        }
    }

    /* Fail any commands cleared from queues but still in progress */
    MeFailInProgressCommands();

    if (!IsEntryAvailable(&((bt.me.hciGenCmd).node))) {
        (bt.me.opMask) &= ~BOM_HCICOMMAND_BUSY;
        InitializeListEntry(&((bt.me.hciGenCmd).node));
    }

    /* Reset the ME state back to the initial state. */
    (bt.me.curOp) = 0;
    (bt.me.opMask) = 0;

    (bt.me.secModeState) = 0;
    (bt.me.secModeEncrypt) = 0;

    (bt.me.pendCons) = 0;
    (bt.me.activeCons) = 0;
    (bt.me.inHoldCons) = 0;

    (bt.me.opState) = 0;
    (bt.me.taskState) = 0;

    (bt.me.isLiac) = FALSE;   
    (bt.me.accModeCur) = 0;

    OS_MemSet((U8*)&((bt.me.accInfoCur)), 0, (U16)sizeof(BtAccessModeInfo));

    /* Non connected accessible info */
    (bt.me.accModeNC) = 0;
    OS_MemSet((U8*)&((bt.me.accInfoNC)), 0, (U16)sizeof(BtAccessModeInfo));

#if BT_ALLOW_SCAN_WHILE_CON == XA_ENABLED
    /* Connected accessible info */
    (bt.me.accModeC) = 0;
    OS_MemSet((U8*)&((bt.me.accInfoC)), 0, (U16)sizeof(BtAccessModeInfo));
#endif /* BT_ALLOW_SCAN_WHILE_CON */

#if XA_DEBUG == XA_ENABLED
    /* Verify that all BtRemoteDevice structures are free */
    for (i = 0; (remDev = MeEnumerateRemoteDevices(i)) != 0; i++) {
        Assert(remDev->op.opType == BOP_NOP);
        Assert(remDev->state == BDS_DISCONNECTED);
    }

    /* Verify all security tokens are free */
    for (i = 0; i < NUM_SEC_OPS; i++) {
        Assert((bt.me.secOpTable)[i].op.opType == BOP_NOP);
    }

#endif

}

/*---------------------------------------------------------------------------
 *            MeFailCommand()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Fail the command pointed to by the operation. For most commands
 *            a event is not generated but for those involving memory given
 *            by the application an event is generated.
 *
 * Return:    void
 */
void MeFailCommand(BtOpType opType, void* parm)
{
    (bt.me.btEvent).errCode = BEC_HARDWARE_FAILURE;

    switch (opType) {
    case BOP_INQUIRY:
        ReportInquiryComplete();
        break;

    case BOP_CANCEL_INQUIRY:
        MeReportInqCancelComplete();
        break;

    case BOP_REM_NAME_REQ:  
        /* MeFailPendingCommands() already removed the token from the list.  
         * Clear the MCTF_NEED_CHECK flag so the callback gets called directly.
         */
        ((MeCommandToken*)parm)->flags &= ~MCTF_NEED_CHECK;  
        MeReportMeCommandComplete((MeCommandToken*)parm);  
        break;
    
    case BOP_GEN_COMMAND:
        /* Remove the token from the command list prior to callback */
        Assert(IsNodeOnList(&(bt.me.cmdList), &(((MeCommandToken*)parm)->op.node)));
        RemoveEntryList(&(((MeCommandToken*)parm)->op.node));

        /* Indicate the failure */
        ((MeCommandToken*)parm)->p.general.out.status = BT_STATUS_FAILED;
        (bt.me.btEvent).p.meToken = (MeCommandToken*)parm;
        (bt.me.btEvent).eType = ((MeCommandToken*)parm)->eType;
        ((MeCommandToken*)parm)->callback(&(bt.me.btEvent));
        break;

    case BOP_LINK_CONNECT:
        MeReportLinkUp((BtRemoteDevice*)parm, BTEVENT_LINK_CONNECT_CNF);
        break;

    case BOP_LINK_ACCEPT:
    case BOP_LINK_REJECT:
        MeReportLinkUp((BtRemoteDevice*)parm, BTEVENT_LINK_CONNECT_IND);
        break;

    case BOP_ACCESS_MODE:
        MeHandleAccessModeOp(BOE_TASK_ERR);
        break;

#if BT_SECURITY == XA_ENABLED
    case BOP_ENCRYPTION:
        ((BtSecurityOp *)parm)->op.opType = BOP_NOP;
        SecReportEncryptionResult(((BtSecurityOp *)parm)->remDev, 0);
        break;

    case BOP_SECURITY_MODE:
        ((BtSecurityOp *)parm)->op.opType = BOP_NOP;
        SecReportSecurityModeResult();
        break;

    case BOP_AUTHENTICATE:
        ((BtSecurityOp *)parm)->op.opType = BOP_NOP;
        SecReportAuthenticateResult(((BtSecurityOp *)parm)->remDev);
        break;
#endif /* BT_SECURITY */

    /* The following commands do not require that an event be generated */
    case BOP_LINK_KEY_RSP:
    case BOP_PIN_CODE_RSP:
    case BOP_AUTHORIZE:
        /* Return the security op to the pool. */
        ((BtSecurityOp*)parm)->op.opType = BOP_NOP;
        break;

    case BOP_WRITE_COD:
        /* Free the Write COD structure */
        (bt.me.codOp).opType = BOP_NOP;
        break;

    case BOP_LOCAL_NAME_CHG:
        /* Free the Name Op structure */
        (bt.me.localName) = 0;
        (bt.me.localNameLen) = 0;
        (bt.me.nameOp).opType = BOP_NOP;
        break;

    case BOP_LINK_IN_REJECT:
        /* Internal reject. Nothing to do here */
        break;

    case BOP_SCO_DISCONNECT:
        /* SCO already reported the disconnect. Nothing else to do. */
        break;

    case BOP_SCO_VOICESET:
        ((MeCommandToken*)parm)->op.opType = BOP_NOP;
        (bt.me.btEvent).eType = BTEVENT_SCO_VSET_COMPLETE;
        ((MeCommandToken*)parm)->callback(&(bt.me.btEvent));
        break;

    /* The following commands should not exist because they would be handled
     * by an ACL disconnect
     */
    case BOP_FLOW_SPEC:
    case BOP_LINK_DISCONNECT:
    case BOP_HOLD_MODE:
    case BOP_SNIFF_MODE:
    case BOP_EXIT_SNIFF_MODE:
    case BOP_PARK_MODE:
    case BOP_EXIT_PARK_MODE:
    case BOP_SWITCH_ROLE:
#if NUM_SCO_CONNS > 0
    /* None of these should exist because all SCO should be
     * disconnected
     */
    case BOP_SCO_CONNECT:
    case BOP_SCO_ACCEPT:
    case BOP_SCO_REJECT:
#endif /* NUM_SCO_CONNS */
        Assert(0);
    }


}

/*---------------------------------------------------------------------------
 *            MeFailInProgressCommands()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Fail commands in progress indicated by ME(opmask).
 *
 * Return:    void
 */
void MeFailInProgressCommands(void)
{
    /* Check if an inquiry is in progress */
    if ((bt.me.opMask) & BOM_INQUIRY) {
        /* Complete it */
        ReportInquiryComplete();
    }

    /* Check if an inquiry cancel is in progress */
    if ((bt.me.opMask) & BOM_CANCEL_INQUIRY) {
        /* Complete it */
        MeReportInqCancelComplete();
    }
}

/*---------------------------------------------------------------------------
 *            MeSendHciCommand()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Send an HCI command. We can only send one HCI command at a time
 *            so they all use the same HciCommand structure.
 *
 * Return:    void
 */
BtStatus MeSendHciCommand(HciCommandType opCode, U8 parmLen)
{
    Assert((bt.me.commandPending) == FALSE);
    (bt.me.commandPending) = TRUE;
    (bt.me.curTask) = opCode;
    return(HCI_SendCommand(opCode, parmLen, &(bt.me.hciGenCmd)));
}

/*---------------------------------------------------------------------------
 *            MeInitRadio()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Write and read radio values. When done initialization is over. 
 *
 * Return:    void
 */
void MeInitRadio(BtOpEvent event)
{
    BtOperation busyOp;

    const BtAccessModeInfo info = { BT_DEFAULT_INQ_SCAN_INTERVAL, 
                                    BT_DEFAULT_INQ_SCAN_WINDOW,
                                    BT_DEFAULT_PAGE_SCAN_INTERVAL, 
                                    BT_DEFAULT_PAGE_SCAN_WINDOW };
    switch (event) {
    case BOE_START:
        /* Start the first operation */
        (bt.me.taskHandler) = HandleCompleteTaskRadioParms;
        (bt.me.taskEvent) = HCE_COMMAND_COMPLETE;
        (bt.me.taskState) = 0;

        /* The radio's access mode is unknown. */
#if BT_ALLOW_SCAN_WHILE_CON == XA_ENABLED
        (bt.me.accModeC) = 0xff;
#endif
        (bt.me.accModeNC) = 0xff;

        /* Write page timeout (default is 5.12 seconds) */
        Assert(IsEntryAvailable(&((bt.me.hciGenCmd).node)));
        StoreLE16((bt.me.hciGenCmd).parms, BT_DEFAULT_PAGE_TIMEOUT);
        AssertEval(MeSendHciCommand(HCC_WRITE_PAGE_TIMEOUT, 2) == BT_STATUS_PENDING);
        return;

    case BOE_TASK_END:
        (bt.me.taskState)++;
        switch ((bt.me.taskState)) {
        case 1:
            /* Perform next operation */
            Assert(IsEntryAvailable(&((bt.me.hciGenCmd).node)));
            AssertEval(MeSendHciCommand(HCC_READ_PAGE_TIMEOUT, 0) == BT_STATUS_PENDING);
            break;

        case 2:
            /* Get the Bluetooth version info */
            Assert(IsEntryAvailable(&((bt.me.hciGenCmd).node)));
            AssertEval(MeSendHciCommand(HCC_READ_LOCAL_VERSION, 0) == BT_STATUS_PENDING);
            break;
 
        case 3:
            /* Get the Bluetooth features info */
            Assert(IsEntryAvailable(&((bt.me.hciGenCmd).node)));
            AssertEval(MeSendHciCommand(HCC_READ_LOCAL_FEATURES, 0) == BT_STATUS_PENDING);
            break;
 
        case 4:
            Assert(IsEntryAvailable(&((bt.me.hciGenCmd).node)));
            Report(("MeCon: Automatic Read BD_ADDR command.\n"));
            AssertEval(MeSendHciCommand(HCC_READ_BD_ADDR, 0) == BT_STATUS_PENDING);
            break;

        case 5:
            (bt.me.stackState) = BTSS_INITIALIZED;

#if BT_SECURITY == XA_ENABLED
            /* Open the device database */
            if (DDB_Open(&(bt.me.bdaddr)) != BT_STATUS_SUCCESS)
                Report(("ME: DDB_Open failed.\n"));
#endif

            /* We're done so stack is initialized. Report it to all 
             * handlers 
             */
            Assert((bt.me.curOp) == 0);
            InitializeListEntry(&(busyOp.node));
            busyOp.opType = BOP_BUSY;
            (bt.me.curOp) = &busyOp;
            (bt.me.btEvent).errCode = BT_STATUS_SUCCESS;
            (bt.me.btEvent).eType = BTEVENT_HCI_INITIALIZED;
            MeReportNMResults();
            (bt.me.curOp) = 0;

            /* Update the class of device */
            MeWriteClassOfDevice();

            /* Update the local name, if its known. */
            if ((bt.me.localNameLen) > 0)
                MeWriteLocalName();

            /* Set the default Accessibility modes. Force the stack to 
             * apply them since we cannot assume default radio accessibility.
             */
            (bt.me.accModeCur) = (U8)(~BT_DEFAULT_ACCESS_MODE_NC);
#if BT_ALLOW_SCAN_WHILE_CON == XA_ENABLED
            (void)ME_SetAccessibleModeC(BT_DEFAULT_ACCESS_MODE_C, &info);
#endif
            (void)ME_SetAccessibleModeNC(BT_DEFAULT_ACCESS_MODE_NC, &info);

            /* Attempt to start the operation, it may not be in progress. */
            /* Move this forward to any future version of MeInitRadio */
            MeStartOperation();
            break;

        default:
            Assert(0);
        }
        break;

    default:
        Assert(0);
    }
}

/*---------------------------------------------------------------------------
 *            HandleCompleteTaskRadioParms()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle HCI events for the Inquiry task. 
 *
 * Return:    void
 */
static void HandleCompleteTaskRadioParms(U8 event, U8  len, U8* parm)
{
    BtErrorCode errCode = 0;

    UNUSED_PARAMETER(len);

    switch (event) {
    case HCE_COMMAND_COMPLETE:
        /* Save the status in case it is an error */
        errCode= parm[3];
        break;

    case HCE_COMMAND_STATUS:
        /* Save the status in case it is an error */
        errCode = parm[0];
        break;

    default:
        Assert(0);
    }

    /* Process the result */
    if (errCode == 0) {
        /* Result is successful so store the radio parameter */
        switch ((bt.me.taskState)) {
        case 0:
            /* We just wrote the page timeout value */
            Report(("ME: RadioParms wrote page timeout\n"));
            break;

        case 1:
            /* Save Page timeout */
            Report(("ME: RadioParms storing page timeout = 0x%x\n", LEtoHost16((parm+4))));
            (bt.me.radioPageTimeout) = LEtoHost16((parm+4));
            break;

        case 2:
            /* We now have the Bluetooth version info */
            Report(("ME: Received Bluetooth version info.\n"));
            if (parm[4] == 3 || parm[7] == 3) {
                (bt.me.btVersion) = 3;
            } 
            else if (parm[4] == 2 || parm[7] == 2) {
                (bt.me.btVersion) = 2;
            }
            else if (parm[4] >= 1 && parm[7] >= 1) {
                (bt.me.btVersion) = 1;
            }
            else {
                (bt.me.btVersion) = 0;
            }
            break;

        case 3:
            /* We now have the Bluetooth features info */
            Report(("ME: Received Bluetooth features info.\n"));
            OS_MemCopy((bt.me.btFeatures), parm+4, 8);
            break;

        case 4:
            Report(("ME: RadioParms storing BD_ADDR.\n"));
            OS_MemCopy((bt.me.bdaddr).addr, parm+4, 6);
            break;

        default:
            Assert(0);
        }
    } else {
        Report(("ME: RadioParms error opcode = 0x%x, errCode = 0x%x\n",
            (bt.me.curTask), errCode));
    }

    /* can't end init radio task until we get the buffer back */
    if (!IsEntryAvailable(&((bt.me.hciGenCmd).node))) {
        (bt.me.opMask) |= BOM_INITRADIO_BUSY;
        return;
    }
    MeInitRadio(BOE_TASK_END);
}

/*---------------------------------------------------------------------------
 *            MePendingCommandHandler()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Checks for pending command.
 */
void MePendingCommandHandler(EvmTimer *timer)
{
    U32 count;

    Report(("ME: Waiting for pending commands\n"));
 
    /* If the event hasn't arrived, continue waiting */
    count = (U32)timer->context;
    if (count > 0) {
        if (ME_HasPendingTask() != 0) {
            count--;
            timer->context = (void *)count;
            timer->func = MePendingCommandHandler;
            EVM_StartTimer(timer, 50);
            return;
        }
    }

    (void)ME_RadioShutdownTime(0);
}
