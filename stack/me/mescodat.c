/***************************************************************************
 *
 * File:
 *     $Workfile:mescodat.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:29$
 *
 * Description:   This is file contains the code to Tx and Rx SCO data.
 *                These are the internal MESCO functions called by 
 *                MESCO to send and receive SCO data. 
 *
 * Created:       May 23, 2000
 *
 * Copyright 2000-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "btalloc.h"
#include "ddb.h"
#if BT_SCO_HCI_DATA == XA_ENABLED
#if NUM_SCO_CONNS > 0
#include "mesco.h"

/*---------------------------------------------------------------------------
 *            SCO_Init()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initialize SCO.
 *
 * Return:    void
 *            
 *
 */
void SCO_Init(void)
{
    I8 i;

    /* Initialize the SCO connection table. */
    for (i = 0; i < NUM_SCO_CONNS; i++) {
        InitializeListHead(&(bt.me.scoTable[i].scoTxQueue));
    }
}


/*---------------------------------------------------------------------------
 *            Sco_Send()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Internal SCO connection Send function.
 *
 * Return:    
 *            
 */
BtStatus Sco_Send(BtScoConnect *scocon, BtPacket *Packet)
{
#if XA_ERROR_CHECK == XA_ENABLED
    if (Packet->dataLen > 255) {
        return BT_STATUS_FAILED;
    }
    
    if (scocon->scostate != BDS_CONNECTED) {
        return BT_STATUS_NO_CONNECTION;
    }
#else
    Assert(scocon->scostate != BDS_DISCONNECTED);
#endif

    Packet->headerLen = 0;

    InsertTailList(&scocon->scoTxQueue, &(Packet->node));

    HCI_RequestToSend(scocon->scoHciHandle);

    return BT_STATUS_PENDING;
}

/*---------------------------------------------------------------------------
 *            ScoDataCallback()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  This function is called by the HCI layer to report all events
 *            and status. 
 *
 * Return:    void
 */
void ScoDataCallback(U8 event, HciCallbackParms *parms)
{
    BtScoConnect *scocon;
    
    switch (event) {
    case HCI_DATA_IND:      
        /* called by HCI when data arrives on an SCO Connection. */
        if (parms->ptr.rxBuff->len < 5)
            return;

        scocon = ScoMapHciToConnect(parms->hciHandle);
        Assert(scocon);

        /* Now report to SCO handler */
        (bt.me.btEvent).p.scoDataInd.remDev = scocon->scord;
        (bt.me.btEvent).p.scoDataInd.scoHandle = parms->hciHandle;
        (bt.me.btEvent).p.scoDataInd.scoCon = scocon;
        (bt.me.btEvent).p.scoDataInd.ptr = parms->ptr.rxBuff->buffer;
        (bt.me.btEvent).p.scoDataInd.len = (U8) parms->ptr.rxBuff->len;

        (bt.me.btEvent).eType = BTEVENT_SCO_DATA_IND;
        scocon->scord->scoHandler->callback(&(bt.me.btEvent));
        break;

    case HCI_PACKET_HANDLED:
        /* Called by HCI to indicate that the SCO packet was sent. */
        Assert(parms->ptr.packet);

        scocon = ScoMapHciToConnect(parms->hciHandle);
        Assert(scocon);

        if (scocon->scord->scoHandler != 0) {
            /* Return packet to upper layer */
            /* Now report to SCO handler */
            (bt.me.btEvent).p.scoPacketHandled.remDev = scocon->scord;
            (bt.me.btEvent).p.scoPacketHandled.scoCon = scocon;
            (bt.me.btEvent).p.scoPacketHandled.scoPacket = parms->ptr.packet;
            (bt.me.btEvent).eType = BTEVENT_SCO_DATA_CNF;
            (bt.me.btEvent).errCode = (BtErrorCode) parms->status;
            scocon->scord->scoHandler->callback(&(bt.me.btEvent));
        }
        break;

    case HCI_SEND_IND:
         /* Called by the HCI when SCO transmit resources are available. */
        scocon = ScoMapHciToConnect(parms->hciHandle);
        Assert(scocon);

        if (IsListEmpty(&scocon->scoTxQueue)) {
            parms->ptr.packet = 0;
        } else {
            parms->ptr.packet = (BtPacket *) RemoveHeadList(&scocon->scoTxQueue);
        }
        break;
    }
}

#endif /* NUM_SCO_CONNS */
#endif /* BT_SCO_HCI_DATA == XA_ENABLED */
