/***************************************************************************
 *
 * File: 
 *     $Workfile:mecon.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:141$
 *
 * Description:
 *     This is file contains the code for managing ACL connections. 
 *
 * Created:
 *     October 18, 1999
 *
 * Copyright 1999-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "btalloc.h"
#include "sys/hci.h"
#include "utils.h"
#include "l2cap.h"

/****************************************************************************
 *
 * Macros
 *
 ****************************************************************************/

/* Macro for accessing a RemoteDevice structure given the curOp */

#define RDEV ((BtRemoteDevice*)(bt.me.curOp))
#define REJECT ((BtConRejectOp*)(bt.me.curOp))
#define METOKEN ((MeCommandToken*)(bt.me.curOp))

/****************************************************************************
 *
 * Functions
 *
 ****************************************************************************/
void MeHandleAclConComplete(BtRemoteDevice *remDev);
static void StartHoldModeTask(void);
#if 0
static void StartFlowSpecTask(void);
#endif
static void StartSniffModeTask(void);
static void StartExitSniffModeTask(void);
static void StartParkModeTask(void);
static void StartExitParkModeTask(void);
static void StartSwitchRoleTask(void);
static void ReportModeChange(BtRemoteDevice* remDev);
static void ReportRoleChange(BtRemoteDevice* remDev);
static void StartRemNameReqTask(void);
static void StartLinkRejectTask(U8* bdAddr, U8 rejectReason);
static void StartLinkDisconnectTask(void);
static void CallAndRemoveHandlers(BtRemoteDevice* remDev);
static void StartLinkAcceptTask(void);
static void StartLinkConnectTask(void);
static void MeFreeRemoteDevice(BtRemoteDevice *remDev);
static void MeFailPendingRemDevCommands(BtRemoteDevice *remDev);
static BOOL MeFailRemDevCommand(BtRemoteDevice *remDev, BtOpType opType, void* parm);

/* Memory Manager function prototypes and macros. These macros may be
 * replaced with dynamic functions as part of the porting effort if desired.
 */
U8  *MM_AllocBuffer(U32 Size, void *Context);
void MM_FreeBuffer(U8 *Buffer);

#define MM_AllocBuffer(_Size, _IdxPtr)  \
        (Assert(*(I8 *)(_IdxPtr) < NUM_BT_DEVICES), (bt.me.rxBuffTable)[*(I8 *)(_IdxPtr)])
#define MM_FreeBuffer(_Buff)


/*---------------------------------------------------------------------------
 *            MeHandleLinkConnectOp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle the link connection operation.
 *
 * Return:    void
 */
void MeHandleLinkConnectOp(BtOpEvent event)
{
    switch (event) {
    case BOE_START:
        /* For now do not check if connections already exist. In the future
         * we may need to put existing connections on hold but for now we 
         * will not
         */
        StartLinkConnectTask();
        return;

    case BOE_TASK_END:
        if ((bt.me.btEvent).errCode != 0) {
            /* We got an error so report it */
            MeReportLinkUp(RDEV, BTEVENT_LINK_CONNECT_CNF);
        }
        break;

    default:
        Assert(0);
        break;
    }

    /* Start another operation if one exists */
    MeOperationEnd();

}


/*---------------------------------------------------------------------------
 *            MeHandleLinkAcceptOp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle the link accept operation.
 *
 * Return:    void
 */
void MeHandleLinkAcceptOp(BtOpEvent event)
{
    switch (event) {
    case BOE_START:
        StartLinkAcceptTask();
        return;

    case BOE_TASK_END:
        if ((bt.me.btEvent).errCode != 0) {
            /* We received an error so report it */
            Report(("MeCon:LinkAcceptError = %x\n", (bt.me.btEvent).errCode));
            MeReportLinkUp(RDEV, BTEVENT_LINK_CONNECT_IND);
        }
        break;

    default:
        Assert(0);
    }

    /* Start another operation if one exists */
    MeOperationEnd();

}


/*---------------------------------------------------------------------------
 *            MeHandleLinkRejectOp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle the link reject operation.
 *
 * Return:    void
 */
void MeHandleLinkRejectOp(BtOpEvent event)
{
    switch (event) {
    case BOE_START:
        if ((bt.me.curOp)->opType == BOP_LINK_IN_REJECT) {
            StartLinkRejectTask(REJECT->bdAddr.addr, REJECT->rejectReason);
        } else {
            StartLinkRejectTask(RDEV->bdAddr.addr, RDEV->parms.rejectReason);
        }
        return;

    case BOE_TASK_END:
        if ((bt.me.btEvent).errCode != 0) {
            if ((bt.me.curOp)->opType == BOP_LINK_REJECT) {
                /* We received an error so report it */
                Report(("MeCon:LinkRejectError = %x\n", (bt.me.btEvent).errCode));
                MeReportLinkUp(RDEV, BTEVENT_LINK_CONNECT_IND);
            } else {
                Report(("MeCon: Internal link reject = %x\n", (bt.me.btEvent).errCode));
            }
        } 
        break;

    default:
        Assert(0);
    }

    /* Start another operation if one exists */
    MeOperationEnd();

}


/*---------------------------------------------------------------------------
 *            MeHandleLinkDisconnectOp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle the link disconnect operation.
 *
 * Return:    void
 */
void MeHandleLinkDisconnectOp(BtOpEvent event)
{
    switch (event) {
    case BOE_START:
        StartLinkDisconnectTask();
        return;

    case BOE_TASK_END:
        Report(("MeCon: MeHandleLinkDisconnectOp task End errcode = %x\n", (bt.me.btEvent).errCode));
        /* If we are disconnecting, advance the state of the disconnect to indicate
         * status received. We're still waiting for disconnect complete.
         */
        if (RDEV->state == BDS_OUT_DISC) {
            RDEV->state = BDS_OUT_DISC2;
        }

        /* If we haven't started another op, clear the op type */
        if (RDEV->op.opType == BOP_LINK_DISCONNECT) {
            RDEV->op.opType = BOP_NOP;
        }

        if ((bt.me.btEvent).errCode != 0) {
            /* This link could already be disconnected by the other side. In this case
             * we will get an error indicating no connection.
             */
            if ((bt.me.btEvent).errCode != BEC_NO_CONNECTION) {
                /* We got some other error and must notify the global handlers. Depending on
                 * the error, they may need to force a disconnect to clear the link.
                 */

                (bt.me.btEvent).eType = BTEVENT_LINK_DISCONNECT;
                (bt.me.btEvent).p.remDev = RDEV;
                MeReportResults(BEM_LINK_DISCONNECT);
            }
        }
        break;

    default:
        Assert(0);
    }

    /* Start another operation if one exists */
    MeOperationEnd();

}


/*---------------------------------------------------------------------------
 *            MeHandleRemNameReqOp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Start an Inquiry process. 
 *
 * Return:    void
 */
void MeHandleRemNameReqOp(BtOpEvent event)
{
    switch (event) {
    case BOE_START:
        /* Put the remote name event on the command queue */
        InsertHeadList(&(bt.me.cmdList), &(METOKEN->op.node));

        /* Start the inquiry task */
        StartRemNameReqTask();
        return;

    case BOE_TASK_END:
        if ((bt.me.btEvent).errCode != 0) {
            /* We got an error so report it */
            Report(("ME: Remote Name Request operation error. Status = %x\n", (bt.me.btEvent).errCode));
            MeReportMeCommandComplete(METOKEN);
        }
        break;

    default:
        Assert(0);
    }

    /* Start another operation if one exists */
    MeOperationEnd();
}


/*---------------------------------------------------------------------------
 *            MeHandleHoldModeOp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle the hold mode operation.
 *
 * Return:    void
 */
void MeHandleHoldModeOp(BtOpEvent event)
{
    switch (event) {
    case BOE_START:
        StartHoldModeTask();
        return;

    case BOE_TASK_END:
        if ((bt.me.btEvent).errCode != 0) {
            /* We got an error so report it */
            Report(("ME: Hold mode operation error. Status = %x\n", (bt.me.btEvent).errCode));
            (bt.me.btEvent).p.modeChange.curMode = RDEV->mode;
            ReportModeChange(RDEV);
        }
        break;

    default:
        Assert(0);
        break;
    }

    /* Start another operation if one exists */
    MeOperationEnd();
}
#if 0
void MeHandleFlowSpecOp(BtOpEvent event)
{
    switch (event) {
    case BOE_START:
        StartFlowSpecTask();
        return;

    case BOE_TASK_END:
        if ((bt.me.btEvent).errCode != 0) {
            /* We got an error so report it */
            Report(("ME: Flow Spec operation error. Status = %x\n", (bt.me.btEvent).errCode));
            //ReportFlowSpecChange(RDEV);
        }
        break;

    default:
        Assert(0);
        break;
    }

    /* Start another operation if one exists */
    MeOperationEnd();
}
#endif

/*---------------------------------------------------------------------------
 *            MeHandleSniffModeOp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle the sniff mode operation.
 *
 * Return:    void
 */
void MeHandleSniffModeOp(BtOpEvent event)
{
    switch (event) {
    case BOE_START:
        StartSniffModeTask();
        return;

    case BOE_TASK_END:
        if ((bt.me.btEvent).errCode != 0) {
            /* We got an error so report it */
            Report(("ME: Sniff mode operation error. Status = %x\n", (bt.me.btEvent).errCode));
            (bt.me.btEvent).p.modeChange.curMode = RDEV->mode;
            ReportModeChange(RDEV);
        }
        break;

    default:
        Assert(0);
        break;
    }

    /* Start another operation if one exists */
    MeOperationEnd();

}


/*---------------------------------------------------------------------------
 *            MeHandleExitSniffModeOp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle the exit sniff mode operation.
 *
 * Return:    void
 */
void MeHandleExitSniffModeOp(BtOpEvent event)
{
    switch (event) {
    case BOE_START:
        StartExitSniffModeTask();
        return;

    case BOE_TASK_END:
        if ((bt.me.btEvent).errCode != 0) {
            /* We got an error so report it */
            Report(("ME: Exit Sniff mode operation error. Status = %x\n", (bt.me.btEvent).errCode));
            (bt.me.btEvent).p.modeChange.curMode = RDEV->mode;
            ReportModeChange(RDEV);
        }
        break;

    default:
        Assert(0);
        break;
    }

    /* Start another operation if one exists */
    MeOperationEnd();

}


/*---------------------------------------------------------------------------
 *            MeHandleParkModeOp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle the park mode operation.
 *
 * Return:    void
 */
void MeHandleParkModeOp(BtOpEvent event)
{
    switch (event) {
    case BOE_START:
        StartParkModeTask();
        return;

    case BOE_TASK_END:
        if ((bt.me.btEvent).errCode != 0) {
            /* We got an error so report it */
            Report(("ME: Park mode operation error. Status = %x\n", (bt.me.btEvent).errCode));
            (bt.me.btEvent).p.modeChange.curMode = RDEV->mode;
            ReportModeChange(RDEV);
        }
        break;

    default:
        Assert(0);
        break;
    }

    /* Start another operation if one exists */
    MeOperationEnd();

}


/*---------------------------------------------------------------------------
 *            MeHandleExitParkModeOp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle the exit park mode operation.
 *
 * Return:    void
 */
void MeHandleExitParkModeOp(BtOpEvent event)
{
    switch (event) {
    case BOE_START:
        StartExitParkModeTask();
        return;

    case BOE_TASK_END:
        if ((bt.me.btEvent).errCode != 0) {
            /* We got an error so report it */
            Report(("ME: Exit Park mode operation error. Status = %x\n", (bt.me.btEvent).errCode));
            (bt.me.btEvent).p.modeChange.curMode = RDEV->mode;
            ReportModeChange(RDEV);
        }
        break;

    default:
        Assert(0);
        break;
    }

    /* Start another operation if one exists */
    MeOperationEnd();

}


/*---------------------------------------------------------------------------
 *            MeHandleSwitchRoleOp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle the switch role operation.
 *
 * Return:    void
 */
void MeHandleSwitchRoleOp(BtOpEvent event)
{
    switch (event) {
    case BOE_START:
        StartSwitchRoleTask();
        return;

    case BOE_TASK_END:
        if ((bt.me.btEvent).errCode != 0) {
            /* We got an error so report it */
            Report(("ME: Switch Role operation error. Status = %x\n", (bt.me.btEvent).errCode));
            (bt.me.btEvent).p.roleChange.newRole = RDEV->role;
            ReportRoleChange(RDEV);
        }
        break;

    default:
        Assert(0);
        break;
    }

    /* Start another operation if one exists */
    MeOperationEnd();

}


/*---------------------------------------------------------------------------
 *            MeHandleWriteLinkPolicyOp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle the write link policy operation.
 *
 * Return:    void
 */
void MeHandleWriteLinkPolicyOp(BtOpEvent event)
{
    BtRemoteDevice* remDev;

    /* Get a pointer to the remote device */
    remDev = ContainingRecord(RDEV, BtRemoteDevice, policyOp);

    switch (event) {
    case BOE_START:
        (bt.me.taskHandler) = MeHandleCompleteTask;
        (bt.me.taskEvent) = HCE_COMMAND_COMPLETE;

        Report(("MeCon: Write Link policy to hciHandle %x\n", remDev->hciHandle));
        StoreLE16((bt.me.hciGenCmd).parms, remDev->hciHandle);      /* Connection handle */
        StoreLE16((bt.me.hciGenCmd).parms+2, remDev->parms.policy); /* Policy */
        Assert(IsEntryAvailable(&((bt.me.hciGenCmd).node)));
        AssertEval(MeSendHciCommand(HCC_WRITE_LINK_POLICY, 4) == BT_STATUS_PENDING);
        return;

    case BOE_TASK_END:
    case BOE_TASK_ERR:
        /* For now ignore the result of the link policy */
        Report(("ME: Write Link Policy Status = %x\n", (bt.me.btEvent).errCode));
        break;

    default:
        Assert(0);
        break;
    }

    /* Start another operation if one exists */
    MeOperationEnd();

}


/*---------------------------------------------------------------------------
 *            MeHandleCancelConnectOp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle the Cancel Connect operation.
 *
 * Return:    void
 */
void MeHandleCancelConnectOp(BtOpEvent event)
{
    BtRemoteDevice *remDev;

    /* Get a pointer to the remote device */
    remDev = ContainingRecord(RDEV, BtRemoteDevice, cancelOp);

    switch (event) {
    case BOE_START:
        (bt.me.taskHandler) = MeHandleCompleteTask;
        (bt.me.taskEvent) = HCE_COMMAND_COMPLETE;

        OS_MemCopy((bt.me.hciGenCmd).parms, remDev->bdAddr.addr, 6); /* BD_ADDR */

        Assert(IsEntryAvailable(&((bt.me.hciGenCmd).node)));
        Report(("MeCon: Starting Create Link Cancel command\n"));
        AssertEval(MeSendHciCommand(HCC_CREATE_CONNECTION_CANCEL, 6) == BT_STATUS_PENDING);
        return;

    case BOE_TASK_END:
    case BOE_TASK_ERR:
        Report(("ME: Create Link Cancel operation complete. Status = %x\n", (bt.me.btEvent).errCode));
        (bt.me.btEvent).eType = BTEVENT_LINK_CREATE_CANCEL;
        (bt.me.btEvent).p.remDev = remDev;
        MeReportNMResults();
        break;

    default:
        Assert(0);
        break;
    }

    /* Start another operation if one exists */
    MeOperationEnd();
}


/*---------------------------------------------------------------------------
 *            MeHandleCancelRemNameOp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle the Cancel Connect operation.
 *
 * Return:    void
 */
void MeHandleCancelRemNameOp(BtOpEvent event)
{
    MeCommandToken *cancelToken;
    MeCommandToken  token;

    /* Get a pointer to the token */
    cancelToken = ContainingRecord(METOKEN, MeCommandToken, cancelOp);

    switch (event) {
    case BOE_START:
        (bt.me.taskHandler) = MeHandleCompleteTask;
        (bt.me.taskEvent) = HCE_COMMAND_COMPLETE;

        OS_MemCopy((bt.me.hciGenCmd).parms, cancelToken->p.name.bdAddr.addr, 6); /* BD_ADDR */

        Assert(IsEntryAvailable(&((bt.me.hciGenCmd).node)));
        Report(("MeCon: Starting Create Link Cancel command\n"));
        AssertEval(MeSendHciCommand(HCC_REM_NAME_REQ_CANCEL, 6) == BT_STATUS_PENDING);
        return;

    case BOE_TASK_END:
    case BOE_TASK_ERR:
        Report(("ME: Get Remote Device Name Cancel operation complete. Status = %x\n",
                (bt.me.btEvent).errCode));
        /* Call all the callbacks with the results. We do this by setting up
         * a token and finding all that match
         */
        token.flags = MCTF_NEED_CHECK;
        token.eType = BTEVENT_NAME_RESULT;
        if ((bt.me.btEvent).errCode == 0) {
            (bt.me.btEvent).errCode = BEC_REQUEST_CANCELLED;
        }
        OS_MemCopy(token.p.name.bdAddr.addr, cancelToken->p.name.bdAddr.addr, 6);
        token.op.opType = BOP_CANCEL_REM_NAME;
        MeReportMeCommandComplete(&token);
        break;

    default:
        Assert(0);
        break;
    }

    /* Start another operation if one exists */
    MeOperationEnd();

}


/*---------------------------------------------------------------------------
 *            StartLinkConnectTask()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Start the link connect task.
 *
 * Return:    void
 */
static void StartLinkConnectTask(void)
{
    (bt.me.taskHandler) = MeHandleConnectionStatusOnly;
    (bt.me.taskEvent) = HCE_COMMAND_STATUS;

    OS_MemCopy((bt.me.hciGenCmd).parms, RDEV->bdAddr.addr, 6); /* BD_ADDR */
    StoreLE16((bt.me.hciGenCmd).parms+6, (U16)BAPT_STANDARD);         /* Packet_Type */
    (bt.me.hciGenCmd).parms[8] = RDEV->parms.con.psi.psRepMode;  /* Page_Scan_Rep_Mode */
    (bt.me.hciGenCmd).parms[9] = RDEV->parms.con.psi.psMode;     /* Page_Scan_Mode */
    StoreLE16((bt.me.hciGenCmd).parms+10, RDEV->parms.con.psi.clockOffset);/* Clock offset */
    (bt.me.hciGenCmd).parms[12] = RDEV->parms.con.allowRoleChange;         /* allowRoleChange */

    Assert(IsEntryAvailable(&((bt.me.hciGenCmd).node)));
    Report(("MeCon: Starting Link Connect command clock offset = 0x%x allowRoleChange = %d\n", 
        RDEV->parms.con.psi.clockOffset, RDEV->parms.con.allowRoleChange));
    AssertEval(MeSendHciCommand(HCC_CREATE_CONNECTION, 13) == BT_STATUS_PENDING);

}


/*---------------------------------------------------------------------------
 *            StartLinkAcceptTask()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Start the link accept task.
 *
 * Return:    void
 */
static void StartLinkAcceptTask(void)
{
    (bt.me.taskHandler) = MeHandleConnectionStatusOnly;
    (bt.me.taskEvent) = HCE_COMMAND_STATUS;

    OS_MemCopy((bt.me.hciGenCmd).parms, RDEV->bdAddr.addr, 6); /* BD_ADDR */
    (bt.me.hciGenCmd).parms[6] = RDEV->parms.acceptRole;

    Assert(IsEntryAvailable(&((bt.me.hciGenCmd).node)));
    Report(("MeCon: Starting Link Accept command. Accept Role = %d\n",RDEV->parms.acceptRole ));
    AssertEval(MeSendHciCommand(HCC_ACCEPT_CON_REQ, 7) == BT_STATUS_PENDING);
}


/*---------------------------------------------------------------------------
 *            StartLinkRejectTask()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Start the link accept task.
 *
 * Return:    void
 */
static void StartLinkRejectTask(U8* bdAddr, U8 rejectReason)
{
    (bt.me.taskHandler) = MeHandleConnectionStatusOnly;
    (bt.me.taskEvent) = HCE_COMMAND_STATUS;

    OS_MemCopy((bt.me.hciGenCmd).parms, bdAddr, 6); /* BD_ADDR */
    (bt.me.hciGenCmd).parms[6] = rejectReason;

    Assert(IsEntryAvailable(&((bt.me.hciGenCmd).node)));
    Report(("MeCon: Starting Link Reject command\n"));
    AssertEval(MeSendHciCommand(HCC_REJECT_CON_REQ, 7) == BT_STATUS_PENDING);
}


/*---------------------------------------------------------------------------
 *            StartLinkDisconnectTask()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Start the link disconnect task.
 *
 * Return:    void
 */
static void StartLinkDisconnectTask(void)
{
    (bt.me.taskHandler) = MeHandleConnectionStatusOnly;
    (bt.me.taskEvent) = HCE_COMMAND_STATUS;

    Report(("MeCon: Starting disconnect to hciHandle %x\n", RDEV->hciHandle));
    StoreLE16((bt.me.hciGenCmd).parms, RDEV->hciHandle);  /* Connection handle */
    if (RDEV->discReason != 0) {
        (bt.me.hciGenCmd).parms[2] = RDEV->discReason;    /* Provided Reason */
    }
    else {
        (bt.me.hciGenCmd).parms[2] = BEC_USER_TERMINATED; /* Default Reason */
    }

    Assert(IsEntryAvailable(&((bt.me.hciGenCmd).node)));
    AssertEval(MeSendHciCommand(HCC_DISCONNECT, 3) == BT_STATUS_PENDING);
}

/*---------------------------------------------------------------------------
 *            StartRemNameReqTask()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Start the remote name request task.
 *
 * Return:    void
 */
static void StartRemNameReqTask(void)
{
    (bt.me.taskHandler) = MeHandleConnectionStatusOnly;
    (bt.me.taskEvent) = HCE_COMMAND_STATUS;

    OS_MemCopy((bt.me.hciGenCmd).parms, METOKEN->p.name.bdAddr.addr, 6); /* BD_ADDR */
    (bt.me.hciGenCmd).parms[6] = METOKEN->p.name.io.in.psi.psRepMode;         /* Page_Scan_Rep_Mode */
    (bt.me.hciGenCmd).parms[7] = METOKEN->p.name.io.in.psi.psMode;            /* Page_Scan_Mode */
    StoreLE16((bt.me.hciGenCmd).parms+8, METOKEN->p.name.io.in.psi.clockOffset);

    Assert(IsEntryAvailable(&((bt.me.hciGenCmd).node)));
    Report(("MeCon: Starting Remote name request command\n"));
    AssertEval(MeSendHciCommand(HCC_REM_NAME_REQ, 10) == BT_STATUS_PENDING);

}


/*---------------------------------------------------------------------------
 *            StartHoldModeTask()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Start the hold mode task.
 *
 * Return:    void
 */
static void StartHoldModeTask(void)
{
    (bt.me.taskHandler) = MeHandleConnectionStatusOnly;
    (bt.me.taskEvent) = HCE_COMMAND_STATUS;

    Report(("MeCon: Hold mode to hciHandle %x\n", RDEV->hciHandle));
    StoreLE16((bt.me.hciGenCmd).parms, RDEV->hciHandle); /* Connection handle */
    StoreLE16((bt.me.hciGenCmd).parms+2, RDEV->parms.mode.max);
    StoreLE16((bt.me.hciGenCmd).parms+4, RDEV->parms.mode.min);

    Assert(IsEntryAvailable(&((bt.me.hciGenCmd).node)));
    AssertEval(MeSendHciCommand(HCC_HOLD_MODE, 6) == BT_STATUS_PENDING);
}

#if 0
static void StartFlowSpecTask(void)
{
    (bt.me.taskHandler) = MeHandleConnectionStatusOnly;
    (bt.me.taskEvent) = HCE_COMMAND_STATUS;

    Report(("MeCon: Flow spec to hciHandle %x\n", RDEV->hciHandle));

    StoreLE16((bt.me.hciGenCmd).parms, RDEV->hciHandle); /* Connection handle */
    (bt.me.hciGenCmd).parms[2] = 0;
    (bt.me.hciGenCmd).parms[3] = RDEV->parms.flowspec.flow_direction;
    (bt.me.hciGenCmd).parms[4] = RDEV->parms.flowspec.flow_service_type;
    StoreLE32((bt.me.hciGenCmd).parms+5, RDEV->parms.flowspec.flow_token_rate);
    StoreLE32((bt.me.hciGenCmd).parms+9, RDEV->parms.flowspec.flow_token_size);
    StoreLE32((bt.me.hciGenCmd).parms+13, RDEV->parms.flowspec.flow_token_bandwidth);
    StoreLE32((bt.me.hciGenCmd).parms+17, RDEV->parms.flowspec.flow_token_latency);


    Assert(IsEntryAvailable(&((bt.me.hciGenCmd).node)));
    AssertEval(MeSendHciCommand(HCC_FLOW_SPECIFICATION, 21) == BT_STATUS_PENDING);
}
#endif

/*---------------------------------------------------------------------------
 *            StartSniffModeTask()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Start the sniff mode task.
 *
 * Return:    void
 */
static void StartSniffModeTask(void)
{
    (bt.me.taskHandler) = MeHandleConnectionStatusOnly;
    (bt.me.taskEvent) = HCE_COMMAND_STATUS;

    Report(("MeCon: Sniff mode to hciHandle %x\n", RDEV->hciHandle));
    StoreLE16((bt.me.hciGenCmd).parms, RDEV->hciHandle); /* Connection handle */
    StoreLE16((bt.me.hciGenCmd).parms+2, RDEV->parms.sniff.maxInterval);
    StoreLE16((bt.me.hciGenCmd).parms+4, RDEV->parms.sniff.minInterval);
    StoreLE16((bt.me.hciGenCmd).parms+6, RDEV->parms.sniff.attempt);
    StoreLE16((bt.me.hciGenCmd).parms+8, RDEV->parms.sniff.timeout);

    Assert(IsEntryAvailable(&((bt.me.hciGenCmd).node)));
    AssertEval(MeSendHciCommand(HCC_SNIFF_MODE, 10) == BT_STATUS_PENDING);
}


/*---------------------------------------------------------------------------
 *            StartExitSniffModeTask()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Start the exit sniff mode task.
 *
 * Return:    void
 */
static void StartExitSniffModeTask(void)
{
    (bt.me.taskHandler) = MeHandleConnectionStatusOnly;
    (bt.me.taskEvent) = HCE_COMMAND_STATUS;

    Report(("MeCon: Exit Sniff mode to hciHandle %x\n", RDEV->hciHandle));
    StoreLE16((bt.me.hciGenCmd).parms, RDEV->hciHandle); /* Connection handle */

    Assert(IsEntryAvailable(&((bt.me.hciGenCmd).node)));
    AssertEval(MeSendHciCommand(HCC_EXIT_SNIFF_MODE, 2) == BT_STATUS_PENDING);
}


/*---------------------------------------------------------------------------
 *            StartParkModeTask()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Start the Park mode task.
 *
 * Return:    void
 */
static void StartParkModeTask(void)
{
    (bt.me.taskHandler) = MeHandleConnectionStatusOnly;
    (bt.me.taskEvent) = HCE_COMMAND_STATUS;

    Report(("MeCon: Park mode to hciHandle %x\n", RDEV->hciHandle));
    StoreLE16((bt.me.hciGenCmd).parms, RDEV->hciHandle); /* Connection handle */
    StoreLE16((bt.me.hciGenCmd).parms+2, RDEV->parms.mode.max);
    StoreLE16((bt.me.hciGenCmd).parms+4, RDEV->parms.mode.min);

    Assert(IsEntryAvailable(&((bt.me.hciGenCmd).node)));
    AssertEval(MeSendHciCommand(HCC_PARK_MODE, 6) == BT_STATUS_PENDING);
}


/*---------------------------------------------------------------------------
 *            StartExitParkModeTask()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Start the exit park mode task.
 *
 * Return:    void
 */
static void StartExitParkModeTask(void)
{
    (bt.me.taskHandler) = MeHandleConnectionStatusOnly;
    (bt.me.taskEvent) = HCE_COMMAND_STATUS;

    Report(("MeCon: Exit Park mode to hciHandle %x\n", RDEV->hciHandle));
    StoreLE16((bt.me.hciGenCmd).parms, RDEV->hciHandle); /* Connection handle */

    Assert(IsEntryAvailable(&((bt.me.hciGenCmd).node)));
    AssertEval(MeSendHciCommand(HCC_EXIT_PARK_MODE, 2) == BT_STATUS_PENDING);
}


/*---------------------------------------------------------------------------
 *            StartSwitchRoleTask()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Start the switch role task.
 *
 * Return:    void
 */
static void StartSwitchRoleTask(void)
{
    (bt.me.taskHandler) = MeHandleConnectionStatusOnly;
    (bt.me.taskEvent) = HCE_COMMAND_STATUS;

    Report(("MeCon: Switch role to hciHandle %x\n", RDEV->hciHandle));
    OS_MemCopy((bt.me.hciGenCmd).parms, RDEV->bdAddr.addr, 6); /* BD_ADDR */

    if (RDEV->role == BCR_MASTER) {
        /* Currently master so make slave */
        (bt.me.hciGenCmd).parms[6] = 0x01;
    } else {
        /* Currently slave so make master */
        (bt.me.hciGenCmd).parms[6] = 0x00;
    }
 
    Assert(IsEntryAvailable(&((bt.me.hciGenCmd).node)));
    AssertEval(MeSendHciCommand(HCC_SWITCH_ROLE, 7) == BT_STATUS_PENDING);
}


/*---------------------------------------------------------------------------
 *            ME_FindRemoteDeviceP()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Return a pointer to the RemoteDevice structure which corresponds
 *            to the given bdAddr. If not found return 0. 
 *
 * Return:    BtRemoteDevice*
 *
 */
BtRemoteDevice* ME_FindRemoteDeviceP(U8* bdAddr)
{
    I8 i;

    /* Go through the RemoteDevice array looking for non free entries
     * and determine if one matches the bdAddr
     */
    for (i = 0; i < NUM_BT_DEVICES; i++) {
        if ((bt.me.devTable)[i].state != BDS_DISCONNECTED) {
            /* We have found an active remote device */
            if (OS_MemCmp((bt.me.devTable)[i].bdAddr.addr,6,bdAddr,6)) {
                return &((bt.me.devTable)[i]);
            }
        }
    }
    return 0;
}

/*---------------------------------------------------------------------------
 *            MeGetFreeRemoteDevice()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Return a pointer to a free remote device structure or 0 if
 *            one is not found. 
 *
 * Return:    BtRemoteDevice*
 */
BtRemoteDevice* MeGetFreeRemoteDevice(void)
{
    I8              i;
#if L2CAP_FLOW_CONTROL == XA_ENABLED
    I8              x;
#endif
    BtRemoteDevice *rDev;

    /* Go through the RemoteDevice array looking for the first free entry. */
    for (i = 0; i < NUM_BT_DEVICES; i++) {
        if ((bt.me.devTable)[i].state == BDS_DISCONNECTED) {
            Assert((bt.me.devTable)[i].sdpServInfo == 0);

            rDev = &(bt.me.devTable)[i];
            OS_MemSet((U8*)rDev, 0, (U16)sizeof(BtRemoteDevice));

            /* Initialize the RemoteDevice structure */
            InitializeListHead(&(rDev->handlers));
            InitializeListHead(&(rDev->txQueue));
            InitializeListHead(&(rDev->queryList));
            InitializeListHead(&(rDev->secTokens));
            InitializeListEntry(&(rDev->op.node));

#if L2CAP_FLOW_CONTROL == XA_ENABLED
            InitializeListHead(&(rDev->txPacketList));
            for (x = 0; x < 2; x++) {
                InsertTailList(&(rDev->txPacketList), &(rDev->txPackets[x].node));
            }
#endif
            Assert(rDev->rxBuffer == 0);
            rDev->rxBuffer = MM_AllocBuffer(L2CAP_MTU+6, (void *)&i);
            if (rDev->rxBuffer == 0) {
                return 0;
            }

            return rDev;
        }
    }
    return 0;
}

/*---------------------------------------------------------------------------
 *            MeFreeRemoteDevice()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Return a remote device structure to the pool.
 *
 */
static void MeFreeRemoteDevice(BtRemoteDevice *remDev)
{
    remDev->state = BDS_DISCONNECTED;

    MM_FreeBuffer(remDev->rxBuffer);

#if XA_DEBUG == XA_ENABLED
    remDev->rxBuffer = 0;
#endif
}

/*---------------------------------------------------------------------------
 *            MeMapHciToRemoteDevice()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Return a pointer to the BtRemoteDevice structure that matches
 *            the given hci handle. If no structure is found then return 0.
 *            The remote device must be active.
 *
 * Return:    void
 */
BtRemoteDevice* MeMapHciToRemoteDevice(HciHandle hciHandle)
{ 
    I8 i;

    /* Go through the RemoteDevice array looking for a match. */
    for (i = 0; i < NUM_BT_DEVICES; i++) {
        if ((bt.me.devTable)[i].state != BDS_DISCONNECTED) {
            if ((bt.me.devTable)[i].hciHandle == hciHandle) {
                return &((bt.me.devTable)[i]);
            }
        }
    }
    return 0;
}


/*---------------------------------------------------------------------------
 *            MeCallLinkHandlers()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Call the callback of all the handlers bound to the remote device. 
 *
 * Return:    void
 */
void MeCallLinkHandlers(BtRemoteDevice* remDev)
{
    ListEntry*  entry;
    BtHandler*  handler;

    Assert(IsListCircular(&(remDev->handlers)));
    entry = remDev->handlers.Flink;
    while(entry != &(remDev->handlers)) {
        /* Point to the next entry before calling the callback in case the
         * callback removes the current handler
         */
        handler = (BtHandler*)entry;
        entry = entry->Flink;
        (bt.me.btEvent).handler = handler;
        handler->callback(&(bt.me.btEvent));
        Assert(IsListCircular(entry));
    }
    Assert(IsListCircular(&(remDev->handlers)));
}


/*---------------------------------------------------------------------------
 *            MeHandleLinkConnectReq()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle an incoming link connection. 
 *
 * Return:    void
 */
void MeHandleLinkConnectReq(U8 len, U8* parm)
{
    U8             *p;
    BtRemoteDevice *remDev;

    UNUSED_PARAMETER(len);

    /* Verify that the reject entry is available and copy the in the
     * the BdAddr just in case
     */
    Assert((bt.me.curOp) != &((bt.me.rejectOp).op));
    Assert(IsEntryAvailable(&((bt.me.rejectOp).op.node)));
    OS_MemCopy((bt.me.rejectOp).bdAddr.addr, parm, 6);

    /* Determine the type of link */
    if (parm[9] == BLT_SCO || parm[9] == BLT_ESCO) {
        /* This is an SCO connection request */
        Report(("MeCon: Received SCO Connect Request\n"));

#if NUM_SCO_CONNS > 0
        /* Deal with the SCO link request */
        if (ScoHandleLinkConnectReq(len, parm) == TRUE) {
            return;
        }
#endif /* NUM_SCO_CONNS */
    } else {
        /* This is a request for an ACL link. We cannot have more than one
         * ACL link per device. We assume that the radio will not allow this
         */
        Report(("MeCon: Received ACL Connect Request\n"));

        /* First get a free remote device structure */
        if ((remDev = MeGetFreeRemoteDevice()) != 0) {
            /* Store the proper values */
            OS_MemCopy(remDev->bdAddr.addr, parm, 6);
            p = parm+6;
            remDev->cod = (U32)(p[0]) | ((U32)(p[1])<<8) | ((U32)(p[2])<<16);
            remDev->state = BDS_IN_CON;
            (bt.me.pendCons) += 1;
            Assert((bt.me.pendCons) > 0);
            Report(("MeCon: COD = 0x%x\n", remDev->cod));

            /* If we have an accept handler then call it. Otherwise accept
             * the connection automatically
             */
            if ((bt.me.acceptHandler) != 0) {
                /* Call the accept handler */
                (bt.me.btEvent).p.remDev = remDev;
                (bt.me.btEvent).eType = BTEVENT_LINK_CONNECT_REQ;
                MeDevEventHandler();
                (bt.me.acceptHandler)->callback(&(bt.me.btEvent));
                return;
            } else {
                /* Accept the connection automatcially */
                remDev->op.opType = BOP_LINK_ACCEPT;
                remDev->restricted = FALSE;
                if ((bt.me.connectionPolicy) == BCR_MASTER)
                    remDev->parms.acceptRole = remDev->role = BCR_MASTER;
                else remDev->parms.acceptRole = remDev->role = BCR_SLAVE;
            }
            /* Add the operation to the queue */
            Assert(IsEntryAvailable(&(remDev->op.node)));
            MeAddOperationFront(remDev);
            goto startOp;
        } 
    } 

    /** MAY need to fix this by having a reject pool or simply not responding
     ** and letting the connection timeout. Don't know what the local radio 
     ** will do
     */
    /* Reject the operation */
    (bt.me.rejectOp).rejectReason = HC_STATUS_HOST_REJ_NO_RESOURCES;
    Assert(IsEntryAvailable(&((bt.me.rejectOp).op.node)));
    MeAddOperationFront(&((bt.me.rejectOp)));

startOp:
    /* Attempt to start the operation */
    MeStartOperation();

}


/*---------------------------------------------------------------------------
 *            MeHandleConnectComplete()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle completion of a link connection (incoming and outgoing). 
 *
 * Return:    void
 */
void MeHandleConnectComplete(U8 len, U8* parm)
{
    BtRemoteDevice* remDev = 0;
    I8 i;

    UNUSED_PARAMETER(len);

    Report(("MeCon: Connection complete event. Status = 0x%x\n", parm[0]));

    /* Determine the type of connection */
    if (parm[9] == BLT_SCO || parm[9] == BLT_ESCO) {
        Report(("MeCon: SCO Connect complete event.\n"));

        /* If SCO is supported then have the SCO code handle it. Otherwise
         * we will ignore it since we should have rejected it anyway.
         */
#if NUM_SCO_CONNS > 0
        /* Pass it to SCO code */
        ScoHandleConnectComplete(len, parm);
#endif /* NUM_SCO_CONNS */
        return;
    }

    /* This is for an ACL link */
    Report(("MeCon: ACL Connect complete event.\n"));

    /* Locate the pending RemoteDevice that matches the complete event and
     * is pending.
     */
    for (i = 0; i < NUM_BT_DEVICES; i++) {
        remDev = &((bt.me.devTable)[i]);
        if ((remDev->state == BDS_OUT_CON) || (remDev->state == BDS_IN_CON)) {
            /* We have found a pending remote device */
            if (StrnCmp(remDev->bdAddr.addr,6,parm+3,6)) {
                break;
            }
            /* Ericsson Workaround!!! Handles incorrect returned BdAddr case */
            if ((bt.me.pendCons) == 1) {
                break;
            }
        }
    }

    if (i < NUM_BT_DEVICES) {
        /* We have found a device */
        Assert((bt.me.pendCons) > 0);
        (bt.me.pendCons) -= 1;

        (bt.me.btEvent).errCode = parm[0];
        remDev->hciHandle = LEtoHost16(parm+1);
        if (parm[10] != 0) {
            remDev->encryptState = BES_ENCRYPTED;
        }
        else {
            remDev->encryptState = BES_NOT_ENCRYPTED;
        }
        Report(("MeCon: ACL Connect Complete hciHandle = %x\n", remDev->hciHandle));
        MeHandleAclConComplete(remDev);
    } else {
        /* We do not have remote device so this must be an internal reject */
        Report(("MeCon: ACL Connect Complete internal reject\n"));
    }

}


/*---------------------------------------------------------------------------
 *            MeHandleDisconnectComplete()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle the Disconnect Complete event. 
 *
 * Return:    void
 */
void MeHandleDisconnectComplete(U8 len, U8* parm)
{
    BtRemoteDevice* remDev = 0;
    I8 i;

    UNUSED_PARAMETER(len);

    Report(("MeCon: Disconnect complete event. Status = 0x%x\n", parm[0]));

#if NUM_SCO_CONNS > 0
    /* Determine if this handle is for SCO. If so call SCO to handle it */
    if (ScoHandleDisconnectComplete(len, parm) == TRUE) {
        return;
    }
#endif /* NUM_SCO_CONNS */

    /* Locate the pending RemoteDevice that matches the complete event and
     * is pending.
     */
    for (i = 0; i < NUM_BT_DEVICES; i++) {
        remDev = &((bt.me.devTable)[i]);
        if ((remDev->state >= BDS_OUT_DISC) || (remDev->state == BDS_CONNECTED)) {
            /* We have found a device in the correct state */
            if (remDev->hciHandle == LEtoHost16(parm+1)) {
                break;
            }
        }
    }

    /* It is possible that we tried to connect at the same time as the remote device.
     * If the remote devices got in first then we should not find a device in the
     * the correct state. In that case we ignore this event.
     */
    if (i < NUM_BT_DEVICES) {
        /* We found the device to disconnect. The status should always be 0 in
         * this case.
         */

        if (parm[0] != 0 && parm[0] != BEC_NO_CONNECTION) {
            /* The disconnect failed. Try to requeue. */
            if (remDev->op.opType == BOP_NOP) {
                /* No operation is in progress so put the disconnect operation 
                 * on the queue.
                 */
                remDev->op.opType = BOP_LINK_DISCONNECT;

                Assert(!IsNodeOnList(&(bt.me.opList),&(remDev->op.node)));

                /* Queue the operation, but don't start it.
                 * Let the next event complete start it.
                 */
                MeAddOperation(remDev);
            }
            else {
                /* We can't requeue, so return error to app */
                (bt.me.btEvent).eType = BTEVENT_LINK_DISCONNECT;
                (bt.me.btEvent).errCode = parm[0]; 
                (bt.me.btEvent).p.remDev = remDev;
                MeReportResults(BEM_LINK_DISCONNECT);
            }
            return;
        }

        Report(("MeCon: Disconnect complete event for hciHandle = %d\n", remDev->hciHandle));

        /* Make sure the state is in the pending disconnect. */
        remDev->state = BDS_OUT_DISC;

        Assert((bt.me.activeCons) > 0);
        (bt.me.activeCons) -= 1;

        /* If this is the last connection going down then put a change access mode 
         * operation on the queue
         */
        if (((bt.me.stackState) == BTSS_INITIALIZED) &&
            ((bt.me.activeCons) == 0) &&
            (HCI_GetState() != HCI_STATE_DEINIT)) {
            MeChangeAccessibleMode(FALSE);
            MeStartOperation();
        }

        /* Put the reason code into the error code */
        (bt.me.btEvent).errCode = parm[3]; 

        /* Clear any pending operations */
        MeFailPendingRemDevCommands(remDev);

        /* Make sure state is not authorized */
        remDev->authorizeState = BAS_NOT_AUTHORIZED;
        remDev->authState = BAS_NOT_AUTHENTICATED;

#if BT_SECURITY == XA_ENABLED
        /* Fail all security access requests */
        SecProcessAllSecurityTokens(remDev);
#endif /* BT_SECURITY */

        /* Clear the operation type */
        remDev->op.opType = BOP_NOP;

        /* Make sure this op is not still curOp */
        if (&(remDev->op) == (bt.me.curOp)) {
            MeOperationEnd();
        }

#if NUM_SCO_CONNS > 0
        /* Tell SCO that the ACL link is down thus, all its SCO connections
         * are also down.
         */
        ScoHandleDisconnectAcl(remDev);
#endif /* NUM_SCO_CONNS */

        /* Tell L2CAP that the link has gone down */
        L2CAP_LinkDisconnect(remDev);

        /* Verify that SDP has cleaned up the remote device */
        Assert(remDev->sdpServInfo == 0);

        if ((bt.me.activeCons) == 0) {
           /* Tell L2CAP that broadcast link is down */
           L2CAP_LinkDisconnect(&L2C(broadcastLink));
        }

        /* Tell all the bound handlers that the link has been disconnected */
        (bt.me.btEvent).errCode = parm[3]; 
        (bt.me.btEvent).eType = BTEVENT_LINK_DISCONNECT;
        (bt.me.btEvent).p.remDev = remDev;
        CallAndRemoveHandlers(remDev);

        /* Now report to all global handlers */
        MeReportResults(BEM_LINK_DISCONNECT);

        /* Set the state to disconnected */
        MeFreeRemoteDevice(remDev);
    }
}

/*---------------------------------------------------------------------------
 *            MeFailPendingRemDevCommands()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Fail all pending ME commands for remote device.   
 *
 * Return:    void
 */
void MeFailPendingRemDevCommands(BtRemoteDevice *remDev)
{
    BtOperation    *op, *nextOp, *saveCurOp;

    saveCurOp = 0;
    /* First, fail the current remDev operation, if it's not disconnect. */
    if ((bt.me.curOp) != 0 && (bt.me.curOp)->opType != BOP_LINK_DISCONNECT) {
        if (MeFailRemDevCommand(remDev, (bt.me.curOp)->opType, (void*)(bt.me.curOp)) == TRUE) {
            saveCurOp = (bt.me.curOp);
            if (IsNodeOnList(&(bt.me.cmdList), &saveCurOp->node)) {
                /* This is an async command */
                RemoveEntryList(&saveCurOp->node);
            }
            MeOperationEnd();
        }
    }

    /* Go through the operations queue and fail operations for remDev */
    op = (BtOperation *)GetHeadList(&(bt.me.opList));
    do {
        nextOp = (BtOperation *)GetNextNode(&op->node);

        if (MeFailRemDevCommand(remDev, op->opType, (void *)op) == TRUE) {
            RemoveEntryList(&op->node);
        }
        op = nextOp;

    } while((BtOperation *)GetHeadList(&(bt.me.opList)) != op);

    /* Clear remDev from the command queue */
    op = (BtOperation *)GetHeadList(&(bt.me.cmdList));
    do {
        nextOp = (BtOperation *)GetNextNode(&op->node);

        if (op != saveCurOp) {
            /* This is not the current remDev operation so check it */
            if (MeFailRemDevCommand(remDev, op->opType, (void *)op) == TRUE) {
                RemoveEntryList(&op->node);
            }
        }
        op = nextOp;

    } while((BtOperation *)GetHeadList(&(bt.me.cmdList)) != op);

    /* Clear remDev from the async queue */
    op = (BtOperation *)GetHeadList(&(bt.me.asyncList));
    do {
        nextOp = (BtOperation *)GetNextNode(&op->node);

        if (op != saveCurOp) {
            /* This is not the current operation so check it */
            if (MeFailRemDevCommand(remDev, op->opType, (void *)op) == TRUE) {
                RemoveEntryList(&op->node);
            }
        }
        op = nextOp;

    } while((BtOperation *)GetHeadList(&(bt.me.asyncList)) != op);

#if BT_SECURITY == XA_ENABLED
    /* Fail Authenticate op when between status and complete events. */
    if (remDev->authState > BAS_NOT_AUTHENTICATED &&
        remDev->authState < BAS_AUTHENTICATED) {
        (bt.me.btEvent).errCode = BEC_NO_CONNECTION;
        SecReportAuthenticateResult(remDev);
    }

    /* Fail Authorize op when between status and complete events. */
    if (remDev->authorizeState > BAS_NOT_AUTHORIZED &&
        remDev->authorizeState < BAS_AUTHORIZED) {
        (bt.me.btEvent).errCode = BEC_NO_CONNECTION;
        SecHandleAuthorizeRequest(remDev);
    }

    /* Fail Encrypt op when between status and change events. */
    if (remDev->encryptState == BES_END_ENCRYPT ||
        remDev->encryptState == BES_START_ENCRYPT) {
        (bt.me.btEvent).errCode = BEC_NO_CONNECTION;
        SecReportEncryptionResult(remDev, 0);
    }
#endif /* BT_SECURITY */
}

/*---------------------------------------------------------------------------
 *            MeFailRemDevCommand()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Fail the command pointed to by the operation if it belongs
 *            to remDev.
 *
 * Return:    TRUE if the command was failed.
 */
BOOL MeFailRemDevCommand(BtRemoteDevice *remDev, BtOpType opType, void* parm)
{
    MeCommandToken *token = (MeCommandToken *)parm;
    BtRemoteDevice *tremDev = (BtRemoteDevice *)parm;
#if BT_SECURITY == XA_ENABLED
    BtSecurityOp   *secop = (BtSecurityOp *)parm;
#endif /* BT_SECURITY == XA_ENABLED */
#if NUM_SCO_CONNS > 0
    BtScoConnect   *scocon = (BtScoConnect *)parm;
#endif

    (bt.me.btEvent).errCode = BEC_NO_CONNECTION;

    switch (opType) {
    case BOP_REM_NAME_REQ:
        if (token->remDev == remDev) {
            MeReportMeCommandComplete(token);
            /* MeReportMeCommandComplete removed the token from
             * the list so, return FALSE here to prevent trying to
             * remove it again.
             */
            return FALSE;
        }
        break;

    case BOP_WRITE_LINK_POLICY:
        if (&remDev->policyOp == (BtOperation *)parm) {
            return TRUE;
        }
        break;

    case BOP_DISCOVER_ROLE:
        if (&remDev->roleOp == (BtOperation *)parm) {
            /* Role discovery is done at connection, no need to indicate it
             * if the connection is gone. 
             */
            return TRUE;
        }
        break;

    case BOP_GEN_COMMAND:
        if (token->remDev == remDev) {
            token->p.general.out.status = BT_STATUS_FAILED;
            (bt.me.btEvent).p.meToken = token;
            (bt.me.btEvent).eType = token->eType;
            token->callback(&(bt.me.btEvent));
            return TRUE;
        }
        break;

    case BOP_LINK_CONNECT:
        if (tremDev == remDev) {
            MeReportLinkUp(tremDev, BTEVENT_LINK_CONNECT_CNF);
            return TRUE;
        }
        break;

    case BOP_LINK_ACCEPT:
    case BOP_LINK_REJECT:
        if (tremDev == remDev) {
            MeReportLinkUp(tremDev, BTEVENT_LINK_CONNECT_IND);
            return TRUE;
        }
        break;

    case BOP_LINK_DISCONNECT:
        if (tremDev == remDev) {
            /* We received the Disconnect_Complete from the remDev
             * before we were able to start ours. Remove our
             * Disconnect from the list.
             */
            return TRUE;
        }
        break;

#if BT_SECURITY == XA_ENABLED
    case BOP_AUTHENTICATE:
        if (secop->remDev == remDev) {
            SecReportAuthenticateResult(remDev);
            secop->op.opType = BOP_NOP;
            return TRUE;
        }
        break;

    case BOP_ENCRYPTION:
        if (secop->remDev == remDev) {
            remDev->encryptState = BES_START_ENCRYPT;
            SecReportEncryptionResult(remDev, 0);
            secop->op.opType = BOP_NOP;
            return TRUE;
        }
        break;
#endif /* BT_SECURITY */

    case BOP_HOLD_MODE:
    case BOP_SNIFF_MODE:
    case BOP_EXIT_SNIFF_MODE:
    case BOP_PARK_MODE:
    case BOP_EXIT_PARK_MODE:
        if (tremDev == remDev) {
            tremDev->op.opType = BOP_LINK_DISCONNECT;
            (bt.me.btEvent).p.modeChange.curMode = BLM_ACTIVE_MODE;
            tremDev->mode = BLM_ACTIVE_MODE;

            ReportModeChange(tremDev);
            return TRUE;
        }
        break;

    case BOP_SWITCH_ROLE:
        if (tremDev == remDev) {
            tremDev->op.opType = BOP_LINK_DISCONNECT;
            ReportRoleChange(tremDev);
            return TRUE;
        }
        break;

#if NUM_SCO_CONNS > 0
    case BOP_SCO_CONNECT:
        if (scocon->scord == remDev) {
            ScoReportScoLinkUp(scocon, BTEVENT_SCO_CONNECT_CNF);
            return TRUE;
        }
        break;

    case BOP_SCO_ACCEPT:
    case BOP_SCO_REJECT:
        if (scocon->scord == remDev) {
            ScoReportScoLinkUp(scocon, BTEVENT_SCO_CONNECT_IND);
            return TRUE;
        }
        break;
#endif /* NUM_SCO_CONNS */

    default:
        /* Ignore others. */
        break;
    }

    return FALSE;
}

/*---------------------------------------------------------------------------
 *            MeHandleRemNameReqComplete()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle the Remote Name Request Complete event. 
 *
 * Return:    void
 */
void MeHandleRemNameReqComplete(U8 len, U8* parm)
{
    I8               i;
    MeCommandToken token;

    UNUSED_PARAMETER(len);

    Report(("MeCon: Remote name request complete status = 0x%x\n", (bt.me.btEvent.errCode)));

    /* Get the error code */
    (bt.me.btEvent).errCode = parm[0];

    if (parm[0] == 0) {
        /* No errors so find the length of the name and add a 0 at
         * the end if needed
         */
        for (i = 0; i < BT_MAX_REM_DEV_NAME; i++) {
            if (*(parm+i+7) == 0) {
                break;
            }
        }

        if (i == BT_MAX_REM_DEV_NAME) {
            /* Add the null termination */
            *(parm+i+7) = 0;
        }

        /* Add the output parameters to the token */
        token.p.name.io.out.len = (U8)i;
        token.p.name.io.out.name = parm + 7;
    }

    /* Call all the callbacks with the results. We do this by setting up
     * a token and finding all that match
     */
    token.op.opType = BOP_REM_NAME_REQ;
    token.flags = MCTF_NEED_CHECK;
    token.eType = BTEVENT_NAME_RESULT;
    OS_MemCopy(token.p.name.bdAddr.addr, parm+1, 6);
    MeReportMeCommandComplete(&token);
}


/*---------------------------------------------------------------------------
 *            MeHandleModeChange()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle a mode change event. 
 *
 * Return:    void
 */
void MeHandleModeChange(U8 len, U8* parm)
{
    BtRemoteDevice* remDev;

    UNUSED_PARAMETER(len);

    Report(("MeCon: Mode Change event\n"));

    /* Locate the remote device */
    remDev = MeMapHciToRemoteDevice((HciHandle) (LEtoHost16(parm+1) & 0x0fff));
    if (remDev != 0) {
        (bt.me.btEvent).errCode = parm[0];
        (bt.me.btEvent).p.modeChange.curMode = parm[3];
        remDev->mode = parm[3];

        if (parm[0] == 0) {
            (bt.me.btEvent).p. modeChange.interval = LEtoHost16(parm+4);
        }

        /* Set okToSend according to mode */
        if (parm[3] == BLM_ACTIVE_MODE) {
            remDev->okToSend = TRUE;
            AssertEval(HCI_RequestToSend(remDev->hciHandle) == BT_STATUS_SUCCESS);
        } else if (parm[3] == BLM_HOLD_MODE || parm[3] == BLM_PARK_MODE) {
            remDev->okToSend = FALSE;
        }

        /* Report the results */
        ReportModeChange(remDev);
    }

}


/*---------------------------------------------------------------------------
 *            MeHandleRoleChange()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle a role change event. 
 *
 * Return:    void
 */
void MeHandleRoleChange(U8 len, U8* parm)
{
    BtRemoteDevice* remDev;

    UNUSED_PARAMETER(len);

    Report(("MeCon: Role Change event\n"));

    /* Locate the remote device */
    remDev = ME_FindRemoteDeviceP(parm+1);
    if (remDev != 0) {
        (bt.me.btEvent).errCode = parm[0];
        (bt.me.btEvent).p.roleChange.newRole = parm[7];
        remDev->role = parm[7];

        /* Report the results */
        ReportRoleChange(remDev);
    }

}


/*---------------------------------------------------------------------------
 *            MeHandleDiscoverRoleTask()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle HCI events for the Role Discovery Task. 
 *
 * Return:    void
 */
static void MeHandleDiscoverRoleTask(U8 event, U8  len, U8 *parm)
{
    U16 opcode = 0;
    BtRemoteDevice *remDev;

    UNUSED_PARAMETER(event);
    UNUSED_PARAMETER(len);

    /* Get a pointer to the remote device */
    remDev = ContainingRecord((bt.me.curOp), BtRemoteDevice, roleOp);

    /* Save the status in case it is an error */
    opcode = LEtoHost16((parm+1));
    (bt.me.btEvent).errCode = parm[3];

    /* Make sure the opcode matches. If not ignore the response */
    if (opcode == (bt.me.curTask)) {
        /* Clear the task if this is the end. */
        (bt.me.curTask) = 0;

        /* Call the operation handler */
        if ((bt.me.btEvent).errCode == 0) {
            remDev->role = parm[6];
            (bt.me.opHandler)(BOE_TASK_END);
        } else {
            (bt.me.opHandler)(BOE_TASK_ERR);
        }

    } else {
        Report(("ME: MeHandleDiscoverRoleTask opcode mismatch opcode = %x, curTask = %x\n",
            opcode, (bt.me.curTask)));
    }
}

/*---------------------------------------------------------------------------
 *            MeHandleDiscoverRole()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Discover the current role. 
 *
 * Return:    void
 */
void MeHandleDiscoverRole(BtOpEvent event)
{
    BtRemoteDevice *remDev;

    /* Get a pointer to the remote device */
    remDev = ContainingRecord((bt.me.curOp), BtRemoteDevice, roleOp);

    switch (event) {
    case BOE_START:
        /* Start the task */
        (bt.me.taskHandler) = MeHandleDiscoverRoleTask;
        (bt.me.taskEvent) = HCE_COMMAND_COMPLETE;
        Assert(IsEntryAvailable(&((bt.me.hciGenCmd).node)));
        StoreLE16((bt.me.hciGenCmd).parms, remDev->hciHandle);

        AssertEval(MeSendHciCommand(HCC_ROLE_DISCOVERY, 2) == BT_STATUS_PENDING);
        return;

    case BOE_TASK_ERR:
        /* The command failed. Assume we are what we expect. */
        remDev->role = (remDev->role == BCR_PSLAVE ? BCR_SLAVE : BCR_MASTER);
        /* drop through */

    case BOE_TASK_END:
        (bt.me.channelOp).opType = BOP_NOP;

        /* Report the results */
        (bt.me.btEvent).p.roleChange.newRole = remDev->role;
        ReportRoleChange(remDev);

        /* Operation done. */
        Report(("ME: Discover Role operation completed error = 0x%x\n", (bt.me.btEvent).errCode));
        break;

    default:
        Assert(0);
    }

    /* Start another operation if one exists */
    MeOperationEnd();
}


/*---------------------------------------------------------------------------
 *            MeDisconnectLink()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Add a disconnect operation to queue. 
 *
 * Return:    void
 */
void MeDisconnectLink(BtRemoteDevice* remDev)
{
    /* See if this link is already in the process of being disconnected
     * or coming up
     */
    if (IsNodeOnList(&((bt.me.opList)), &(remDev->op.node))) {
        /* Item is on the list. If it is for a connect operation then simply 
         * remove it from the list. If it already being disconnected then 
         * we are done. 
         */
        Assert((remDev->op.opType != BOP_LINK_ACCEPT) &&
               (remDev->op.opType != BOP_LINK_REJECT));

        if ((remDev->op.opType >= BOP_HOLD_MODE) &&
            (remDev->op.opType <= BOP_SWITCH_ROLE)) {
            /* We have a mode/role change operation on the queue. Remove it
             * and disconnect the link.
             */
            remDev->op.opType = BOP_NOP;
            RemoveEntryList(&(remDev->op.node));
        } else {
            if (remDev->op.opType == BOP_LINK_CONNECT) {
                /* Remove the operation from the list and make it free. There 
                 * should be no handlers since it could not have gotten this 
                 * far if there was.
                 */
                Assert((IsListEmpty(&(remDev->handlers)) && (remDev->refCount == 0)));
                RemoveEntryList(&(remDev->op.node));
   
                Assert((bt.me.pendCons) > 0);
                (bt.me.pendCons) -= 1;

                MeFreeRemoteDevice(remDev);
            }
            return;
        }
    }
     

    /* If the connection is in the process of being disconnected then do nothing otherwise
     * it must be active so we need to put the disconnect operation on the queue.
     */ 
    Assert(remDev->state >= BDS_CONNECTED);
    if (remDev->state == BDS_CONNECTED) {

        /* Were connected so we need to disconnect */
        remDev->state = BDS_OUT_DISC;
        remDev->okToSend = FALSE;

        /* If we are performing a mode change operation then we have to wait
         * until it is done before we can actually disconnect. In this case
         * we will set the state and wait for the mode change operation to
         * occur then we will disconnect.
         */
        if (remDev->op.opType == BOP_NOP) {
            /* No operation is in progress so put the disconnect operation 
             * on the queue.
             */
            remDev->op.opType = BOP_LINK_DISCONNECT;

            Assert(!IsNodeOnList(&(bt.me.opList),&(remDev->op.node)));
            MeAddOperation(remDev);
            MeStartOperation();
        }
    }
}


/*---------------------------------------------------------------------------
 *            MeReportLinkUp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Report results of an ACL link coming up. 
 *
 * Return:    void
 */
void MeReportLinkUp(BtRemoteDevice* remDev, BtEventType eType)
{
    BtEventMask mask;
    BtOpType    saveOp;

    mask = BEM_LINK_CONNECT_CNF;

    saveOp = remDev->op.opType;
    remDev->op.opType = BOP_NOP;

    /* If an auto-role-switch is necessary, this is a good place to put it. */

    /* If we have an accept handler and the operation is reject or accept
     * then call the accept handler with the results.
     */
    if (((bt.me.acceptHandler) != 0) &&
        ((saveOp == BOP_LINK_ACCEPT) || (saveOp == BOP_LINK_REJECT))) {
        (bt.me.btEvent).p.remDev = remDev;
        (bt.me.btEvent).handler = (bt.me.acceptHandler);

        if (saveOp == BOP_LINK_ACCEPT) {
            (bt.me.btEvent).eType = BTEVENT_LINK_ACCEPT_RSP;
        } else {
            Assert(saveOp == BOP_LINK_REJECT);
            (bt.me.btEvent).eType = BTEVENT_LINK_REJECT_RSP;
        }
        (bt.me.acceptHandler)->callback(&(bt.me.btEvent));
    }

    /* If we were rejecting the link then success does not mean the link
     * is up. It actually means the link is down. We will set the errorCode
     * so the link does not come up.
     */
    if (saveOp == BOP_LINK_REJECT) {
        Report(("MeCon: Setting error for link reject. Old error = 0x%x\n", (bt.me.btEvent).errCode));
        (bt.me.btEvent).errCode = BEC_LOCAL_TERMINATED;
    }

    /* We need to call the bound handlers. Any connection inbound or
     * outbound will result in the same event being passed to the bound
     * handlers. Setup the things common to either success or failure.
     */
    (bt.me.btEvent).eType = BTEVENT_LINK_CONNECT_CNF;
    (bt.me.btEvent).p.remDev = remDev;

    if ((bt.me.btEvent).errCode == 0) {
        /* Operation succeeded. Put the proper value into the Event and
         * report the event to all bound handlers.
         */
        remDev->okToSend = TRUE;
        remDev->state = BDS_CONNECTED;

        /* If we are in security mode 3 then set authState. The encryptState
         * is set in MeHandleConnectComplete.
         */
        if ((bt.me.secModeState) == BSMS_LEVEL3) {
            remDev->authState = BAS_AUTHENTICATED;
        }

        /* Update activeCons and send event to all bound handlers */
        (bt.me.activeCons) += 1;
        Assert((bt.me.activeCons) > 0);
        MeCallLinkHandlers(remDev);
    } else {
        /* Operation failed. Report the error. */
        remDev->state = BDS_OUT_DISC;
        CallAndRemoveHandlers(remDev);
    }

    /* The connect operation is complete so report results to all global handlers */
    (bt.me.btEvent).eType = eType;

    if (eType == BTEVENT_LINK_CONNECT_IND) {
        mask = BEM_LINK_CONNECT_IND;
    }
    MeReportResults(mask);

    /* If an error occured set the state to disconnected */
    if ((bt.me.btEvent).errCode != 0) {
        MeFreeRemoteDevice(remDev);
    }
}


/*---------------------------------------------------------------------------
 *            ReportModeChange()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Report results of a mode change. 
 *
 * Return:    void
 */
static void ReportModeChange(BtRemoteDevice* remDev)
{
    if ((remDev->state == BDS_OUT_DISC) && 
        (remDev->op.opType != BOP_LINK_DISCONNECT)) {
        /* A disconnect request occured while trying to enter a mode change
         * so we need to disconnect. Add a disconnect operation to the queue.
         */
        remDev->op.opType = BOP_LINK_DISCONNECT;
        Assert(!IsNodeOnList(&(bt.me.opList),&(remDev->op.node)));
        MeAddOperation(remDev);
        MeStartOperation();
    } else {
        /* Only reset the opType if the requested operation is a
         * mode change operation.
         */
        if ((remDev->op.opType >= BOP_HOLD_MODE) &&
            (remDev->op.opType <= BOP_EXIT_PARK_MODE)) {
             remDev->op.opType = BOP_NOP;
        }
    }

    /* We need to call the bound handlers. */
    (bt.me.btEvent).eType = BTEVENT_MODE_CHANGE;
    (bt.me.btEvent).p.modeChange.remDev = remDev;

    /* Report to bound handler */
    MeCallLinkHandlers(remDev);

    /* Report to global handlers */
    MeReportResults(BEM_MODE_CHANGE);
}


/*---------------------------------------------------------------------------
 *            ReportRoleChange()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Report results of a role change. 
 *
 * Return:    void
 */
static void ReportRoleChange(BtRemoteDevice* remDev)
{
    if ((remDev->state == BDS_OUT_DISC) && 
        (remDev->op.opType != BOP_LINK_DISCONNECT)) {
        /* A disconnect request occured while trying to enter a mode change
         * so we need to disconnect. Add a disconnect operation to the queue.
         */
        remDev->op.opType = BOP_LINK_DISCONNECT;
        Assert(!IsNodeOnList(&(bt.me.opList),&(remDev->op.node)));
        MeAddOperation(remDev);
        MeStartOperation();
    } else {
        /* Only clear the opType if we were performing a role switch */
        if (remDev->op.opType == BOP_SWITCH_ROLE) {
            remDev->op.opType = BOP_NOP;
        }
    }

    /* Do not report a role switch if we are in the middle of connecting */
    if (remDev->state >= BDS_CONNECTED) {
        /* We need to call the bound handlers. */
        (bt.me.btEvent).eType = BTEVENT_ROLE_CHANGE;
        (bt.me.btEvent).p.roleChange.remDev = remDev;

        /* Report to bound handler */
        MeCallLinkHandlers(remDev);

        /* Report to global handlers */
        MeReportResults(BEM_ROLE_CHANGE);
    }
}

/*---------------------------------------------------------------------------
 *            CallAndRemoveHandlers()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Call the handlers bound to a BtRemoteDevice and remove
 *            them from the BtRemoteDevice's handler list.
 *
 * Return:    void
 */
static void CallAndRemoveHandlers(BtRemoteDevice* remDev)
{
    BtHandler*      handler;

    while(!IsListEmpty(&(remDev->handlers))) {
        handler = (BtHandler*) RemoveHeadList(&(remDev->handlers));
        (bt.me.btEvent).handler = handler;

        InitializeListEntry(&(handler->node));
        handler->callback(&(bt.me.btEvent));
    }
    Assert(IsListCircular(&(remDev->handlers)));
}

#if L2CAP_NUM_GROUPS > 0
/*---------------------------------------------------------------------------
 *            MeSuspendTransmitters()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Suspends transmission on all links. Used when a broadcast packet
 *            is queue'd for transmit. 
 *
 * Return:    void
 */
void MeSuspendTransmitters(void)
{
    I8 i;

    for (i = 0; i < NUM_BT_DEVICES; i++) {
        if ((bt.me.devTable)[i].state == BDS_CONNECTED) {
            (bt.me.devTable)[i].okToSend = FALSE;
        }
    }
}


/*---------------------------------------------------------------------------
 *            MeRestartTransmitters()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Resumes transmission on all links. Used after the broadcast 
 *            packet has been transmitted. 
 *
 * Return:    void
 */
void MeRestartTransmitters(void)
{
    I8 i;

    for (i = 0; i < NUM_BT_DEVICES; i++) {
        if ((bt.me.devTable)[i].state == BDS_CONNECTED) {
            (bt.me.devTable)[i].okToSend = TRUE;

            if (!IsListEmpty(&((bt.me.devTable)[i].txQueue))) {
                AssertEval(HCI_RequestToSend((bt.me.devTable)[i].hciHandle) == BT_STATUS_SUCCESS);
            }
        }
    }
}
#endif

/*---------------------------------------------------------------------------
 *            MeEnumerateRemoteDevices()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Return a pointer to a remote device structure based on an 
 *            interator. 0 is returned if the iterator exceeds the number of 
 *            remote device structures.
 *
 * Return:    void
 */
BtRemoteDevice* MeEnumerateRemoteDevices(I8 i)
{
    if (i < NUM_BT_DEVICES) {
        return &((bt.me.devTable)[i]);
    } else {
        return 0;
    }
}


/*---------------------------------------------------------------------------
 *            MeHandleAclConComplete()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle an ACL connection complete event. 
 *
 * Return:    void
 */
void MeHandleAclConComplete(BtRemoteDevice *remDev)
{
    BtEventType eType;

    /* Determine the proper event to send.  */
    Assert((remDev->state == BDS_IN_CON) ||
           (remDev->op.opType == BOP_LINK_CONNECT) ||
           (remDev->op.opType == BOP_LINK_ACCEPT)  ||
           (remDev->op.opType == BOP_LINK_REJECT)  ||
           (remDev->op.opType == BOP_LINK_IN_REJECT));

    if (remDev->op.opType == BOP_LINK_CONNECT) {
        eType = BTEVENT_LINK_CONNECT_CNF;
        remDev->parms.policy = (bt.me.outAclPolicy);
    } else {
        eType = BTEVENT_LINK_CONNECT_IND;
        remDev->parms.policy = (bt.me.inAclPolicy);
    }

    /* If a link has come up then we need to queue a policy command and an
     * accessibility command.
     */
    if (((bt.me.btEvent).errCode == 0) &&  
        (remDev->op.opType != BOP_LINK_REJECT)) {

        /* Queue the accessibility command */
        if ((bt.me.activeCons) == 0) {
            MeChangeAccessibleMode(TRUE);
        }

        /* Queue the policy command */
        remDev->policyOp.opType = BOP_WRITE_LINK_POLICY;
        Assert(IsEntryAvailable(&(remDev->policyOp.node)));
        InsertHeadList(&(bt.me.opList), &(remDev->policyOp.node));

        if (remDev->op.opType == BOP_LINK_ACCEPT) {
            /* Queue a role discovery */
            remDev->roleOp.opType = BOP_DISCOVER_ROLE;
            Assert(IsEntryAvailable(&(remDev->roleOp.node)));
            InsertHeadList(&(bt.me.opList), &(remDev->roleOp.node));
            remDev->role = (remDev->role == BCR_SLAVE ? BCR_PSLAVE : BCR_PMASTER);
        }

        /* Attempt to start the operation */
        MeStartOperation();
    }
    else if (IsNodeOnList(&(bt.me.opList), &(remDev->op.node)) == TRUE) {
        /* remove aborted operation from opList (should this be
         * limited to accept/reject operations only?)
         */
        RemoveEntryList(&remDev->op.node);
    }
    
#if BT_SECURITY == XA_ENABLED
    if (remDev->authState == BAS_WAITING_FOR_PIN ||
        remDev->authState == BAS_WAITING_FOR_PIN_R) {
        /* Cancel the PIN request */
        SecReportAuthenticateResult(remDev);
    }
#endif /* BT_SECURITY == XA_ENABLED */

    /* Report the results of the link up */
    MeReportLinkUp(remDev, eType);
}

/*---------------------------------------------------------------------------
 *            MeCheckModeChange()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Common error checking for mode change API functions.
 *
 * Return:    BtStatus
 */
BtStatus MeCheckModeChange(BtRemoteDevice *remDev, BtOpType bop)
{
    BtLinkMode  preMode;

#if XA_ERROR_CHECK == XA_ENABLED
    if (remDev == 0) {
        return BT_STATUS_INVALID_PARM;
    }
#endif
    Assert(remDev);

    /* Make sure the stack does not have an HCI initialization error */
    if ((bt.me.stackState) >= BTSS_INITIALIZE_ERR) {
        return BT_STATUS_HCI_INIT_ERR;
    }

    if (bop == BOP_EXIT_PARK_MODE)
        preMode = BLM_PARK_MODE;
    else if (bop == BOP_EXIT_SNIFF_MODE)
        preMode = BLM_SNIFF_MODE;
    else preMode = BLM_ACTIVE_MODE;

    /* Must be connected and in the correct mode for the operation. */
    if ((remDev->state != BDS_CONNECTED) || (remDev->mode != preMode)) {
        /* Not in the right state */
        return BT_STATUS_FAILED;
    }
    
    /* The link must not in the process of performing an operation. */
    if (remDev->op.opType != BOP_NOP) {
        return BT_STATUS_IN_PROGRESS;
    }

    return BT_STATUS_SUCCESS;
}


/*---------------------------------------------------------------------------
 *            MeHandleDataReqWhileNotActive()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Deliver data request while not active event to global handlers.
 *
 * Return:    void
 */
void MeHandleDataReqWhileNotActive(BtRemoteDevice *remDev)
{
    /* Backup global event to avoid clobbering possible event in progress. */
    BtEvent     event = (bt.me.btEvent);

    (bt.me.btEvent).eType = BTEVENT_ACL_DATA_NOT_ACTIVE;
    (bt.me.btEvent).p.remDev = remDev;

    MeReportResults(BEM_ALL_EVENTS);

    (bt.me.btEvent) = event;
}

