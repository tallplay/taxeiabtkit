/***************************************************************************
 *
 * File:
 *     $Workfile:meacc.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:41$
 *
 * Description:
 *     This is file contains the code for the accessibility 
 *     functions of Bluetooth Management Entity. This includes
 *     connectability, discoverability, and pageablility.
 *
 * Created:
 *     October 18, 1999
 *
 * Copyright 1999-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "btalloc.h"
#include "sys/hci.h"
#include "utils.h"

/****************************************************************************
 *
 * Functions
 *
 ****************************************************************************/
static BOOL StartWriteIacLap(BtAccessibleMode newMode);
static BOOL StartWriteScanActivity(U16 opcode, BtAccessModeInfo* newInfo);
static void StartWriteScanEnable(BtAccessibleMode mode);

#define ACCOP ((BtScanOp*)(bt.me.curOp))

/*---------------------------------------------------------------------------
 *            MeHandleAccessModeOp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle the change access mode operation.
 *
 * Return:    void
 */
void MeHandleAccessModeOp(BtOpEvent event)
{
    BtAccessModeInfo*  curInfo;
    BtAccessibleMode*  curMode;

    if (ACCOP->nc == TRUE) {
        curMode = &(bt.me.accModeNC);
        curInfo = &((bt.me.accInfoNC));
    } else {
#if BT_ALLOW_SCAN_WHILE_CON == XA_ENABLED
        curMode = &(bt.me.accModeC);
        curInfo = &((bt.me.accInfoC));
#else
        curInfo = 0;
        curMode = 0;
#endif /* BT_ALLOW_SCAN_WHILE_CON */
    }

    switch (event) {
    case BOE_START:
        /* All tasks return using Command Complete Event so set the task
         * handler appropriately. Also assume errCode = success, in case
         * we don't send any commands.
         */
        (bt.me.taskHandler) = MeHandleCompleteTask;
        (bt.me.taskEvent) = HCE_COMMAND_COMPLETE;
        (bt.me.opState) = 0;
        (bt.me.btEvent).errCode = BEC_NO_ERROR;

        /* Fall through to the next case */

    case BOE_CONT:
        /* Determine if page scan activity needs to change. */
        if (StartWriteScanActivity(HCC_WRITE_PAGE_SCAN_ACTIVITY, 
                                   &(ACCOP->i)) == TRUE) {
            (bt.me.opState) = 1;
            return;
        }

        /* Determine if the inquiry scan activity needs to change */
        if (StartWriteScanActivity(HCC_WRITE_INQ_SCAN_ACTIVITY, 
                                   &(ACCOP->i)) == TRUE) {
            (bt.me.opState) = 2;
            return;
        }

        /* Determine if we need to perform the Write IAC task */
        if (StartWriteIacLap(ACCOP->mode) == TRUE) {
            (bt.me.opState) = 3;
            return;
        }

        /* Determine if we need to perform write scan enable */
        if (((bt.me.accModeCur) & 0x03) != (ACCOP->mode & 0x03)) {
            /* We need to write scan enable */
            StartWriteScanEnable(ACCOP->mode);
            (bt.me.opState) = 4;
            return;
        } 
        else if ((bt.me.accModeCur) != ACCOP->mode) {
            /* The scan enable is the same but the mode is different. So
             * just copy the mode and report
             */
            (bt.me.accModeCur) = ACCOP->mode;

            if (curMode != 0) {
                *curMode = ACCOP->mode;
            }
        }
        break;

    case BOE_TASK_END:
        switch ((bt.me.opState)) {
        case 1:   /* WRITE_PAGE_SCAN_ACTIVITY command */
            /* The operation is complete so update things and do the
             * next step
             */
            Report(("MeAcc: PageScanActivity complete\n"));
            (bt.me.accInfoCur).pageInterval = ACCOP->i.pageInterval;
            (bt.me.accInfoCur).pageWindow = ACCOP->i.pageWindow;

            if (curInfo != 0) {
                curInfo->pageInterval = ACCOP->i.pageInterval;
                curInfo->pageWindow = ACCOP->i.pageWindow;
            }
            MeHandleAccessModeOp(BOE_CONT);
            return;

        case 2:   /* WRITE_INQ_SCAN_ACTIVITY command */
            /* The operation is complete so update things and do the
             * next step
             */
            Report(("MeAcc: InqScanActivity complete\n"));
            (bt.me.accInfoCur).inqInterval = ACCOP->i.inqInterval;
            (bt.me.accInfoCur).inqWindow = ACCOP->i.inqWindow;

            if (curInfo != 0) {
                curInfo->inqInterval = ACCOP->i.inqInterval;
                curInfo->inqWindow = ACCOP->i.inqWindow;
            }
            MeHandleAccessModeOp(BOE_CONT);
            return;

        case 3:   /* WRITE_CURRENT_IAC_LAP command */
            Report(("MeAcc: Write IAC Lap complete\n"));
            if ((ACCOP->mode & 0x10) == 0x10) {
                (bt.me.isLiac) = TRUE;
            } else {
                (bt.me.isLiac) = FALSE;
            }
            MeHandleAccessModeOp(BOE_CONT);
            return;

        case 4:   /* WRITE_SCAN_ENABLE command */
            /* Final task complete so report results */
            Report(("MeAcc: Write Scan Enable complete\n"));
            (bt.me.accModeCur) = ACCOP->mode;

            if (curMode != 0) {
                *curMode = ACCOP->mode;
            }
            break;
        }
        break;

    case BOE_TASK_ERR:
        /* The operation failed so report it to the clients */
        Report(("MeAcc: The SetAccessibleMode Operation failed\n"));
        break;

    default:
        Assert(0);
        break;
    }

    /* Clean up so another SetDiscoverable operation can be launched */
    if (ACCOP == &((bt.me.accOp))) {
        (bt.me.opMask) &= ~BOM_ACCESS_MODE;
    } else {
        (bt.me.opMask) &= ~BOM_ACCESS_MODE_AUTO;
    }

    /* Report the results to the clients */
    (bt.me.btEvent).eType = BTEVENT_ACCESSIBLE_CHANGE;
    (bt.me.btEvent).p.aMode = ACCOP->mode;
    MeReportResults(BEM_ACCESSIBLE_CHANGE);

    /* Start another operation if one exists */
    MeOperationEnd();

}


/*---------------------------------------------------------------------------
 *            StartWriteScanEnable()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Start a write scan enable function. 
 *
 * Return:    void
 */
static void StartWriteScanEnable(BtAccessibleMode mode)
{
    /* Send the command */
    Assert(IsEntryAvailable(&((bt.me.hciGenCmd).node)));
    (bt.me.hciGenCmd).parms[0] = mode & 0x03;
    Report(("MeAcc: Starting Write_Scan_Enable\n"));
    AssertEval(MeSendHciCommand(HCC_WRITE_SCAN_ENABLE, 1) == BT_STATUS_PENDING);
}

/*---------------------------------------------------------------------------
 *            StartWriteScanActivity()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  If needed start an Write Scan Activity command. This function
 *            works for either inquiry scan or page scan. 
 *
 * Return:    TRUE or FALSE
 */
static BOOL StartWriteScanActivity(U16 opcode, BtAccessModeInfo* newInfo)
{
    U16 curWindow;
    U16 curInterval;
    U16 newWindow;
    U16 newInterval;
    U16 defaultWindow;
    U16 defaultInterval;

    /* Setup the correct values */
    if (opcode == HCC_WRITE_PAGE_SCAN_ACTIVITY) {
        /* Writing page scan activity */
        curWindow = (bt.me.accInfoCur).pageWindow;
        curInterval = (bt.me.accInfoCur).pageInterval;
        newWindow = newInfo->pageWindow;
        newInterval = newInfo->pageInterval;
        defaultWindow = BT_DEFAULT_PAGE_SCAN_WINDOW;
        defaultInterval = BT_DEFAULT_PAGE_SCAN_INTERVAL;
    } else {
        /* Writing inquiry scan activity */
        curWindow = (bt.me.accInfoCur).inqWindow;
        curInterval = (bt.me.accInfoCur).inqInterval;
        newWindow = newInfo->inqWindow;
        newInterval = newInfo->inqInterval;
        defaultWindow = BT_DEFAULT_INQ_SCAN_WINDOW;
        defaultInterval = BT_DEFAULT_INQ_SCAN_INTERVAL;
    }

    if ((newInterval != curInterval) || (newWindow != curWindow)) {
        /* We need to change the scan activity. If setting back to defaults
         * get the correct value.
         */
        if (newInterval == BT_DEFAULT_SCAN_INTERVAL) {
            newInterval = defaultInterval;
            Assert(defaultInterval != 0);
        }

        if (newWindow == BT_DEFAULT_SCAN_WINDOW) {
            newWindow = defaultWindow;
            Assert(defaultWindow != 0);
        }

        /* Build the command and send it */
        StoreLE16((bt.me.hciGenCmd).parms, newInterval);
        StoreLE16((bt.me.hciGenCmd).parms+2, newWindow);
        Assert(IsEntryAvailable(&((bt.me.hciGenCmd).node)));
        Report(("MeAcc: Starting Write Scan Activity command\n"));
        Report(("MeAcc: Write Scan op = %d, int = %d, win = %d\n", 
            opcode, newInterval, newWindow));
        AssertEval(MeSendHciCommand(opcode, 4) == BT_STATUS_PENDING);
        return TRUE;
    } 
    return FALSE;
}

/*---------------------------------------------------------------------------
 *            StartWriteIacLap()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  If needed start a Write IAC LAP command. 
 *
 * Return:    TRUE or FALSE
 */
static BOOL StartWriteIacLap(BtAccessibleMode newMode)
{
    U8 num;

    /* First determine if a IAC change is needed */
    num = 0;
    if ((newMode != BAM_NOT_ACCESSIBLE) && 
        (newMode != BAM_CONNECTABLE_ONLY)) {
        if (((bt.me.isLiac) == TRUE) && ((newMode & 0x10) == 0)) {
            /* Changing from LIAC to GIAC so write one */
            num = 1;
        }

        if (((bt.me.isLiac) == FALSE) && ((newMode & 0x10) == 0x10)) {
            /* Changing from GIAC to LIAC to so write 2 */
            num = 2;
        }
    }

    if (num > 0) {
        /* Load up the GIAC */
        (bt.me.hciGenCmd).parms[0] = num;
        (bt.me.hciGenCmd).parms[1] = 0x33;
        (bt.me.hciGenCmd).parms[2] = 0x8B;
        (bt.me.hciGenCmd).parms[3] = 0x9E;

        if (num == 2) {
            /* Load up the LIAC */
            (bt.me.hciGenCmd).parms[4] = 0x00;
            (bt.me.hciGenCmd).parms[5] = 0x8B;
            (bt.me.hciGenCmd).parms[6] = 0x9E;
        }

        Assert(IsEntryAvailable(&((bt.me.hciGenCmd).node)));
        Report(("MeAcc: Starting Write_Current_IAC_LAP\n"));
        AssertEval(
            MeSendHciCommand(HCC_WRITE_CURRENT_IAC_LAP, (U8)(1+(3*num)))
            == BT_STATUS_PENDING);
        return TRUE;
    }
    return FALSE;
}


/*---------------------------------------------------------------------------
 *            MeIsAccessModeInfoEqual()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Return TRUE if the two accessModeInfo structures are equivalent.
 *            Otherwise return FALSE.
 *
 * Return:    TRUE or FALSE
 */
BOOL MeIsAccessModeInfoEqual(BtAccessModeInfo* info1, BtAccessModeInfo* info2)
{
    Assert((info1 != 0) && (info2 != 0));

    if ((info1->inqInterval  != info2->inqInterval) || 
        (info1->inqWindow    != info2->inqWindow) ||
        (info1->pageInterval != info2->pageInterval) || 
        (info1->pageWindow   != info2->pageWindow)) {
        return FALSE;
    }
    return TRUE;
}

/*---------------------------------------------------------------------------
 *            MeChangeAccessibleMode()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Change the accessible mode. 
 *
 * Return:    void
 */
void MeChangeAccessibleMode(BOOL firstCon)
{
    BtAccessModeInfo info1;
    BtAccessibleMode mode1;
    BOOL             nc1;
    BtScanOp        *accOp;

    /* Intialize info1 */
    info1.inqInterval = BT_DEFAULT_SCAN_INTERVAL;
    info1.inqWindow = BT_DEFAULT_SCAN_WINDOW;
    info1.pageInterval = BT_DEFAULT_SCAN_INTERVAL;
    info1.pageWindow = BT_DEFAULT_SCAN_WINDOW;

    if (firstCon == TRUE) {
        /* First connection is up so change the accessible mode to the
         * proper setting. If there is an accessible operation waiting it
         * must be the NC version. So we can simply save the info and
         * use the op structure
         */
        accOp = &((bt.me.accUpOp));
        if ((bt.me.opMask) & BOM_ACCESS_MODE) {
            /* Operation is in progress save the info*/
            if (IsNodeOnList(&(bt.me.opList), (ListEntry*) (&((bt.me.accOp).op.node)))) {
                Report(("MeAcc: Auto access mode switch on first connect - existing op on queue\n"));
                Assert((bt.me.accOp).nc == TRUE);
                (bt.me.accInfoNC) = (bt.me.accOp).i;
                (bt.me.accModeNC) = (bt.me.accOp).mode;
                RemoveEntryList(&((bt.me.accOp).op.node));
                (bt.me.opMask) &= ~BOM_ACCESS_MODE;
            } else {
                Report(("MeAcc: Auto access mode switch on first connect - existing op executing\n"));
            }
        } else {
            /* No operation in progress */
            Report(("MeAcc: Auto access mode switch on first connect - no op on queue\n"));
        }

        /* See if the down op is on the queue. If it is then remove it. */
        if (IsNodeOnList(&(bt.me.opList), (ListEntry*) (&((bt.me.accDownOp).op.node)))) {
            Report(("MeAcc: Auto access mode switch on first connect - down op on queue\n"));
            RemoveEntryList(&((bt.me.accDownOp).op.node));
        }
            
#if BT_ALLOW_SCAN_WHILE_CON == XA_ENABLED
        /* Use the C settings */
        mode1 = (bt.me.accModeC);
        info1 = (bt.me.accInfoC);
#else
        /* No C settings so make non accessible */
        mode1 = BAM_NOT_ACCESSIBLE;

#endif /* BT_ALLOW_SCAN_WHILE_CON */
        /* We are performing the connected operation */
        nc1 = FALSE;
    } else {
        /* Last connection is being disconnected so change to the proper setting. */
        accOp = &((bt.me.accDownOp));

#if BT_ALLOW_SCAN_WHILE_CON == XA_ENABLED
        /* We are disconnecting the last item. See if an existing operation is on the
         * queue.
         */
        if ((bt.me.opMask) & BOM_ACCESS_MODE) {
            /* Operation is in progress save the info*/
            if (IsNodeOnList(&(bt.me.opList), (ListEntry*) (&((bt.me.accOp).op.node)))) {
                Report(("MeAcc: Auto access mode switch on last disconnect - existing op on queue\n"));
                Assert((bt.me.accOp).nc == FALSE);
                (bt.me.accInfoC) = (bt.me.accOp).i;
                (bt.me.accModeC) = (bt.me.accOp).mode;
                RemoveEntryList(&((bt.me.accOp).op.node));
                (bt.me.opMask) &= ~BOM_ACCESS_MODE;
            } else {
                Report(("MeAcc: Auto access mode switch on last disconnet - existing op executing\n"));
            }
        } else {
            /* No operation in progress */
            Report(("MeAcc: Auto access mode switch on last disconnect - no op on queue\n"));
        }
#else
        Report(("MeAcc: Auto access mode switch on last disconnect - no op on queue\n"));
#endif /* BT_ALLOW_SCAN_WHILE_CON */

        /* See if the up op is on the queue. If it is then remove it. */
        if (IsNodeOnList(&(bt.me.opList), (ListEntry*) (&((bt.me.accUpOp).op.node)))) {
            Report(("MeAcc: Auto access mode switch on first connect - up op on queue\n"));
            RemoveEntryList(&((bt.me.accUpOp).op.node));
        }

        /* Use the NC settings */
        mode1 = (bt.me.accModeNC);
        info1 = (bt.me.accInfoNC);
        nc1 = TRUE;
    }

    /* Start up an operation */
    (bt.me.opMask) |= BOM_ACCESS_MODE_AUTO;
    accOp->nc = nc1;
    accOp->i = info1;
    accOp->mode = mode1;

    Assert(IsEntryAvailable(&(accOp->op.node)));
    Assert((void*)(bt.me.curOp) != (void*)accOp);
    MeAddOperationFront(accOp);
}

#if XA_ERROR_CHECK == XA_ENABLED || XA_DEBUG == XA_ENABLED
/*---------------------------------------------------------------------------
 *            MeIsScanValsLegal()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Return TRUE if the scan values are legal otherwise return FALSE.
 *
 * Return:    TRUE or FALSE
 */
BOOL MeIsScanValsLegal(U16 interval, U16 window)
{
    if (((interval == BT_DEFAULT_SCAN_INTERVAL) &&
         (window == BT_DEFAULT_SCAN_WINDOW)) ||
        ((window >= 0x0012) && (window <= 0x1000) &&
         (interval >= 0x0012) && (interval <= 0x1000))) {
        return TRUE;
    }
    return FALSE;
}

#endif /* XA_ERROR_CHECK || XA_DEBUG */
