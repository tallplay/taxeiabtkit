/***************************************************************************
 *
 * File:
 *     $Workfile:rxbuff.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:30$
 *
 * Description:
 *     This file contains receive buffers management routines for the HCI 
 *     transport.
 *
 * Copyright 1999-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "btalloc.h"

/*---------------------------------------------------------------------------
 *            RXBUFF_Init()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initializes receive buffer memory and lists.  This should be
 *            called during stack initialization.
 *
 * Return:    void
 */
BtStatus RXBUFF_Init(void)
{
    U8 *ptr;
    I8 i;

    /* Initialize RX/TX queues */
#if NUM_SCO_CONNS != 0
    InitializeListHead(&(bt.rxbf.rxSCOFreeList));
#endif /* NUM_SCO_CONNS != 0 */
    InitializeListHead(&(bt.rxbf.rxACLFreeList));
    InitializeListHead(&(bt.rxbf.rxEvtFreeList));

#if NUM_SCO_CONNS != 0
    /* Init SCO RX buffers */
    ptr = bt.rxbf.rxSCOBuffer;
    for (i = 0; i < TRANS_NUM_SCO_BUFFERS; i++) {
        bt.rxbf.rxSCOBuffers[i].flags |= HCI_BUFTYPE_SCO_DATA;
        bt.rxbf.rxSCOBuffers[i].buffer = ptr;
        InsertTailList(&(bt.rxbf.rxSCOFreeList), &(bt.rxbf.rxSCOBuffers[i].node));
        ptr += TRANS_MAX_SCO_BUFF_SIZE;
    }
#endif /* NUM_SCO_CONNS != 0 */

    /* Init ACL RX buffers */
    ptr = bt.rxbf.rxACLBuffer;
    for (i = 0; i < TRANS_NUM_ACL_BUFFERS; i++) {
        bt.rxbf.rxACLBuffers[i].flags |= HCI_BUFTYPE_ACL_DATA;
        bt.rxbf.rxACLBuffers[i].buffer = ptr;
        InsertTailList(&(bt.rxbf.rxACLFreeList), &(bt.rxbf.rxACLBuffers[i].node));
        ptr += TRANS_MAX_ACL_BUFF_SIZE;
    }

    /* Init Event RX buffers */
    ptr = bt.rxbf.rxEvtBuffer;
    for (i = 0; i < HCI_NUM_EVENTS; i++) {
        bt.rxbf.rxEvtBuffers[i].flags |= HCI_BUFTYPE_EVENT;
        bt.rxbf.rxEvtBuffers[i].buffer = ptr;
        InsertTailList(&(bt.rxbf.rxEvtFreeList), &(bt.rxbf.rxEvtBuffers[i].node));
        ptr += TRANS_EVENT_BUFF_SIZE;
    }

    return BT_STATUS_SUCCESS;
}


/*---------------------------------------------------------------------------
 *            RXBUFF_Alloc()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Allocates a receive buffer and prepares it to receive data.
 *            This function is intended to be called at interrupt time or
 *            with interrupts disabled (OS_StopHardware()).
 *
 * Return:    BT_STATUS_SUCCESS - the buffer was allocated successfully.
 *            BT_STATUS_NO_RESOURCES - a buffer is not available
 *            BT_STATUS_INVALID_PARM - one of the parameters contained an 
 *                invalid value.
 */
BtStatus RXBUFF_Alloc(RxBuffHandle *rxBuffHandle, HciConnHandle hciConnHandle, 
                      U16 len, U8 type)
{
    BtStatus    status = BT_STATUS_NO_RESOURCES;
    HciBuffer  *hciBuffer = 0;

    UNUSED_PARAMETER(hciConnHandle);

    switch (type) {
    case HCI_BUFTYPE_ACL_DATA:

        if (len > TRANS_MAX_ACL_BUFF_SIZE) {
            status = BT_STATUS_INVALID_PARM;
            break;
        }

        if (!IsListEmpty(&(bt.rxbf.rxACLFreeList))) {
            XASTAT_PeakInc(hci_num_acl_buffers, 1);
            XASTAT_PeakSet(hci_acl_buff_size, len);
            hciBuffer = (HciBuffer *)RemoveHeadList(&(bt.rxbf.rxACLFreeList));
            hciBuffer->flags |= HCI_BUFTYPE_ACL_DATA;
            status = BT_STATUS_SUCCESS;
        }
#if XA_STATISTICS == XA_ENABLED
        else {
            XASTAT_CounterInc(hci_num_no_acl_rxbuf, 1);
        }
#endif
        break;

#if NUM_SCO_CONNS != 0
    case HCI_BUFTYPE_SCO_DATA:

        if (len > TRANS_MAX_SCO_BUFF_SIZE) {
            status = BT_STATUS_INVALID_PARM;
            break;
        }

        if (!IsListEmpty(&(bt.rxbf.rxSCOFreeList))) {
            XASTAT_PeakInc(hci_num_sco_buffers, 1);
            XASTAT_PeakSet(hci_sco_buff_size, len);
            hciBuffer = (HciBuffer *)RemoveHeadList(&(bt.rxbf.rxSCOFreeList));
            hciBuffer->flags |= HCI_BUFTYPE_SCO_DATA;
            status = BT_STATUS_SUCCESS;
        }
#if XA_STATISTICS == XA_ENABLED
        else {
            XASTAT_CounterInc(hci_num_no_sco_rxbuf, 1);
        }
#endif
        break;

#endif /* NUM_SCO_CONNS != 0 */
    case HCI_BUFTYPE_EVENT:

        if (len > TRANS_EVENT_BUFF_SIZE) {
            status = BT_STATUS_INVALID_PARM;
            break;
        }

        if (!IsListEmpty(&(bt.rxbf.rxEvtFreeList))) {
            XASTAT_PeakInc(hci_num_event_buffers, 1);
            hciBuffer = (HciBuffer *)RemoveHeadList(&(bt.rxbf.rxEvtFreeList));
            hciBuffer->flags |= HCI_BUFTYPE_EVENT;
            status = BT_STATUS_SUCCESS;
        }
        break;

    default:
        status = BT_STATUS_INVALID_PARM;
        break;
    }

    if (status == BT_STATUS_SUCCESS) {
        XASTAT_TimerStart(hci_rx_timer, hciBuffer->hci_timer);
        hciBuffer->len = len;
        *rxBuffHandle = hciBuffer;
    }

    return status;
}

/*---------------------------------------------------------------------------
 *            RXBUFF_Free()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Frees a buffer.  The buffer must have been allocated previously
 *            by a call to RXBUFF_Alloc().  This function is intended to be 
 *            called at interrupt time or with interrupts disabled 
 *            (OS_StopHardware()).
 *
 * Return:    void
 */
void RXBUFF_Free(RxBuffHandle rxBuffHandle)
{
    HciBuffer *hciBuffer = rxBuffHandle;

    XASTAT_TimerStop(hciBuffer->len, hci_rx_timer, hciBuffer->hci_timer);

#if NUM_SCO_CONNS != 0
    if (hciBuffer->flags & HCI_BUFTYPE_SCO_DATA) {
        if (!IsNodeOnList(&bt.rxbf.rxSCOFreeList, &(hciBuffer->node))) {
            XASTAT_PeakDec(hci_num_sco_buffers, 1);
            InsertTailList(&(bt.rxbf.rxSCOFreeList), &(hciBuffer->node));
        }
    } else 
#endif /* NUM_SCO_CONNS != 0 */
    if (hciBuffer->flags & HCI_BUFTYPE_ACL_DATA) {
        if (!IsNodeOnList(&bt.rxbf.rxACLFreeList, &(hciBuffer->node))) {
            XASTAT_PeakDec(hci_num_acl_buffers, 1);
            InsertTailList(&(bt.rxbf.rxACLFreeList), &(hciBuffer->node));
        }
    } else if (hciBuffer->flags & HCI_BUFTYPE_EVENT) {
        if (!IsNodeOnList(&bt.rxbf.rxEvtFreeList, &(hciBuffer->node))) {
            XASTAT_PeakDec(hci_num_event_buffers, 1);
            InsertTailList(&(bt.rxbf.rxEvtFreeList), &(hciBuffer->node));
        }
    }
#if XA_DEBUG == XA_ENABLED
    else {
        Report(("RXBUFF:  Invalid buffer type on free\n"));
        Assert(0);
    }
#endif /* XA_DEBUG == XA_ENABLED */
}

/*---------------------------------------------------------------------------
 *            RXBUFF_SetLength()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Adjust the length of the HCI receive buffer.  
 *
 * Return:    BT_STATUS_INVALID_PARM
 *            BT_STATUS_SUCCESS
 */
BtStatus RXBUFF_SetLength(RxBuffHandle rxBuffHandle, U16 len)
{
    HciBuffer *hciBuffer;

    if (rxBuffHandle->flags & HCI_BUFTYPE_ACL_DATA) {
        if (len > TRANS_MAX_ACL_BUFF_SIZE) {
            return BT_STATUS_INVALID_PARM;
        }
        XASTAT_PeakSet(hci_acl_buff_size, len);
    } else if (rxBuffHandle->flags & HCI_BUFTYPE_SCO_DATA) {
        if (len > TRANS_MAX_SCO_BUFF_SIZE) {
            return BT_STATUS_INVALID_PARM;
        }
        XASTAT_PeakSet(hci_sco_buff_size, len);
    } else {
        /* Event buffer */
        if (len > TRANS_EVENT_BUFF_SIZE) {
            return BT_STATUS_INVALID_PARM;
        }
    }

    hciBuffer = rxBuffHandle;
    hciBuffer->len = len;
    return BT_STATUS_SUCCESS;
}

/*---------------------------------------------------------------------------
 *            RXBUFF_Deinit()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Deinitializes receive buffer memory and lists.  This should be
 *            called during stack initialization.
 *
 * Return:    void
 */
void RXBUFF_Deinit(void)
{
    /* If receive buffers are allocated dynamically, then can be freed here */
    return;
}

