/***************************************************************************
 *
 * File:
 *     $Workfile:utils.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:34$
 *
 * Description:
 *     This file contains utility function code for stacks
 *     and add-in components. Including functions for list 
 *     manipulation. These functions may be mapped to OS function 
 *     calls in some instances.
 *
 * Created:
 *     June 20, 1995
 *
 * Copyright 1999-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "osapi.h"
#include "utils.h"

#if XA_USE_ENDIAN_MACROS == XA_DISABLED
/*---------------------------------------------------------------------------
 *            BEtoHost16()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Retrieve a 16-bit number from the given buffer. The number
 *            is in Big-Endian format.
 *
 * Return:    16-bit number.
 */
U16 BEtoHost16(const U8* ptr)
{
    return (U16)( ((U16) *ptr << 8) | ((U16) *(ptr+1)) ); 
}

/*---------------------------------------------------------------------------
 *            BEtoHost32()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Retrieve a 32-bit number from the given buffer. The number
 *            is in Big-Endian format.
 *
 * Return:    32-bit number.
 */
U32 BEtoHost32(const U8* ptr) 
{
    return (U32)( ((U32) *ptr << 24)   | \
                  ((U32) *(ptr+1) << 16) | \
                  ((U32) *(ptr+2) << 8)  | \
                  ((U32) *(ptr+3)) );
}

/*---------------------------------------------------------------------------
 *            LEtoHost16()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Retrieve a 16-bit number from the given buffer. The number
 *            is in Little-Endian format.
 *
 * Return:    16-bit number.
 */
U16 LEtoHost16(const U8* ptr)
{
    return (U16)( ((U16) *(ptr+1) << 8) | ((U16) *ptr) ); 
}

/*---------------------------------------------------------------------------
 *            LEtoHost32()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Retrieve a 32-bit number from the given buffer. The number
 *            is in Little-Endian format.
 *
 * Return:    32-bit number.
 */
U32 LEtoHost32(const U8* ptr) 
{
    return (U32)( ((U32) *(ptr+3) << 24)   | \
                  ((U32) *(ptr+2) << 16) | \
                  ((U32) *(ptr+1) << 8)  | \
                  ((U32) *(ptr)) );
}

/*---------------------------------------------------------------------------
 *            StoreLE16()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Store 16 bit value into a buffer in Little Endian format.
 *
 * Return:    void
 */
void StoreLE16(U8 *buff, U16 le_value) 
{
   buff[1] = (U8)(le_value>>8);
   buff[0] = (U8)le_value;
}

/*---------------------------------------------------------------------------
 *            StoreLE32()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Store 32 bit value into a buffer in Little Endian format.
 *
 * Return:    void
 */
void StoreLE32(U8 *buff, U32 le_value)
{
   buff[3] = (U8) (le_value>>24);
   buff[2] = (U8) (le_value>>16);
   buff[1] = (U8) (le_value>>8);
   buff[0] = (U8) le_value;
}

/*---------------------------------------------------------------------------
 *            StoreBE16()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Store 16 bit value into a buffer in Big Endian format.
 *
 * Return:    void
 */
void StoreBE16(U8 *buff, U16 be_value) 
{
   buff[0] = (U8)(be_value>>8);
   buff[1] = (U8)be_value;
}

/*---------------------------------------------------------------------------
 *            StoreBE32()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Store 32 bit value into a buffer in Big Endian format.
 *
 * Return:    void
 */
void StoreBE32(U8 *buff, U32 be_value)
{
   buff[0] = (U8) (be_value>>24);
   buff[1] = (U8) (be_value>>16);
   buff[2] = (U8) (be_value>>8);
   buff[3] = (U8) be_value;
}

#endif /* XA_USE_ENDIAN_MACROS == XA_DISABLED */

/*---------------------------------------------------------------------------
 *            _InsertTailList()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Insert an entry at the tail of the list specified by head.
 *
 * Return:    void
 */
void _InsertTailList(ListEntry* head, ListEntry* entry)
{
  entry->Flink = head;
  entry->Blink = head->Blink;
  head->Blink->Flink = entry;
  head->Blink = entry;
  Assert(IsNodeConnected(entry));

}

/*---------------------------------------------------------------------------
 *            _InsertHeadList()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Insert an entry at the head of the list specified by head.
 *
 * Return:    void
 */
void _InsertHeadList(ListEntry* head, ListEntry* entry)
{
  entry->Flink = head->Flink;
  entry->Blink = head;
  head->Flink->Blink = entry;
  head->Flink = entry;
  Assert(IsNodeConnected(entry));

}

/*---------------------------------------------------------------------------
 *            _RemoveHeadList()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Remove the first entry on the list specified by head.
 *
 * Return:    void
 */
ListEntry* _RemoveHeadList(ListEntry* head)
{
  ListEntry* first;

  first = head->Flink;
  first->Flink->Blink = head;
  head->Flink = first->Flink;
  Assert(IsListCircular(head));
  return(first);

}

/*---------------------------------------------------------------------------
 *           RemoveEntryList()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Remove the given entry from the list.
 *
 * Return:    void
 *
 */
void RemoveEntryList(ListEntry* entry)
{
  Assert(IsListCircular(entry));
  entry->Blink->Flink = entry->Flink;
  entry->Flink->Blink = entry->Blink;
  Assert(IsListCircular(entry->Blink));
  InitializeListEntry(entry);

}

/*---------------------------------------------------------------------------
 *            IsNodeOnList()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Determine if an entry is on the list specified by head.
 *
 * Return:    TRUE - the entry is on the list.
 *            FALSE - the entry is not on the list.
 */
BOOL IsNodeOnList(ListEntry* head, ListEntry* node)
{
  ListEntry* tmpNode;

  Assert(IsListCircular(head));
  tmpNode = GetHeadList(head);

  while (tmpNode != head)
  {
    if (tmpNode == node)
      return(TRUE);

    tmpNode = tmpNode->Flink;
  }
  return(FALSE);

}

/*---------------------------------------------------------------------------
 *            MoveList()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Moves a list to a new list head.  The source list is left empty.
 *
 * Return:    none
 */
void MoveList(ListEntry* dest, ListEntry* src)
{
    Assert(IsListCircular(src));
    dest->Flink = src->Flink;
    dest->Blink = src->Blink;
    src->Flink->Blink = dest;
    src->Blink->Flink = dest;
    InitializeListHead(src);
}

#if XA_DEBUG == XA_ENABLED
/*---------------------------------------------------------------------------
 *            IsListCircular()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Determine if the list is circular. This is only used for
 *            debugging purposes.
 *
 * Return:    TRUE - the list is circular
 *            FALSE - the list is not circular.
 */
BOOL IsListCircular(ListEntry* list)
{
  ListEntry* tmp = list;

  if (!IsNodeConnected(list))
    return(FALSE);

  for (tmp = tmp->Flink; tmp != list; tmp = tmp->Flink)
    if (!IsNodeConnected(tmp))
      return(FALSE);

  return(TRUE);

}

/*---------------------------------------------------------------------------
 *            VerifyMacros()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Determine if the ContainingRecord() macro is functioning
 *            properly.
 */
void VerifyMacros(void)
{
    struct MacroTests {
        U8      f1;
        U16     f2[3];
        U8      f3;
        U32     f4;
    } mt;

    /* The following Asserts verify the ContainingRecord() macro.
     * If an assert fails, there is a problem with your compilers handling
     * of the macro. A macro compatible with your compiler should be placed
     * in the overide.h file to override the default version found in config.h.
     */
    Assert(ContainingRecord(&mt.f3, struct MacroTests, f3) == &mt);
    Assert(ContainingRecord(&mt.f4, struct MacroTests, f3) != &mt);
}
#endif /* XA_DEBUG */

