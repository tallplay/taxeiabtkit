/***************************************************************************
 *
 * File:
 *     $Workfile:eventmgr.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:68$
 *
 * Description:
 *     This file contains code for the protocol stack event manager.
 *
 * Created:
 *     July 20, 1999
 *
 * Copyright 1999-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "eventmgr.h"
#include "utils.h"

#include "radiomgr.h"
#if BT_STACK == XA_ENABLED
#include "sys/mexp.h"
#endif

#if BT_STACK == XA_DISABLED
/* If the Bluetooth stack is not enabled, this module is responsible
 * for its own memory.
 */
ListEntry          timerList;
#define BTC(e) e
#include "sys/hci.h"
#else /* BT_STACK disabled */
#include "btalloc.h"
#endif /* BT_STACK disabled */

/* Creates "extern BOOL MODULE_Init();" prototypes from XA_LOAD_LIST. */
#define XA_MODULE(a) extern BOOL a##_Init(void);
XA_LOAD_LIST

#undef XA_MODULE
/* Creates "MODULE_Init() &&" conditional from XA_LOAD_LIST (used in EVM_Init()). */
#define XA_MODULE(a) a##_Init() &&

/***************************************************************************
 *
 * Local functions
 *
 ***************************************************************************/

/* In non-multitasking systems, the OS timer is not necessary */
#if XA_MULTITASKING == XA_ENABLED                
static void ResetOsTimer(TimeT curTime);
#else
#define ResetOsTimer(a) (void)0
#endif

#if 0 //tallplay
#if BT_STACK == XA_DISABLED
#define OS_StopHardware()       (void)0
#define OS_ResumeHardware()     (void)0
#endif
#endif
/***************************************************************************
 *
 * Local variables
 *
 ***************************************************************************/

/*---------------------------------------------------------------------------
 *           EVM_Init()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initialize the Event Manager. 
 *
 * Return:    void
 */
BOOL EVM_Init(void)
{
#if XA_DEBUG == XA_ENABLED
    /* This is a debug-only test to verify that macros are compiled correctly. */
    VerifyMacros();
#endif /* XA_DEBUG == XA_ENABLED */
        
    /* Intialize the Bluetooth memory */
    if (!BtInitMemory()) {
        return FALSE;
    }

    /* Initialize the timer list */
    InitializeListHead(&BTC(timerList));

    /* Initialize the Radio Manager */
    if (RMGR_RadioInit() == BT_STATUS_FAILED) {
        return FALSE;
    }
#if BT_STACK == XA_ENABLED

    /* Initialize the Bluetooth stack */
    if (ME_Init() == BT_STATUS_FAILED) {
        return FALSE;
    }
#endif
 
  
    /* XA_LOAD_LIST evaluates to series of module specific Init() function calls. */
    if ((XA_LOAD_LIST TRUE) != TRUE) {
        return FALSE;
    }

    /* ... other initialization here ... */

    return TRUE;

}

/*---------------------------------------------------------------------------
 *           EVM_Deinit()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Deinitialize the Event Manager. 
 *
 * Return:    void
 */
void EVM_Deinit(void)
{
    OS_LockStack();

    /* Deinit the radio manager */
    RMGR_RadioDeinit();

#if BT_STACK == XA_ENABLED
        /* Deinitialize the stack */
        ME_Deinit();
#endif /* BT_STACK == XA_ENABLED */

    OS_UnlockStack();
}

/*---------------------------------------------------------------------------
 *            EVM_CancelTimer()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Cancel an Event Manager timer.
 */
 U32 timecount = 0;
void EvmCancelTimer(EvmTimer* timer, BOOL stopHardware)
{
    EvmTimer   *curTimer;
    ListEntry  *node;

    if (stopHardware) {
        OS_StopHardware();
    }

    /* Look for the timer in our active timer list and remove it. */ 
    if (!IsListEmpty(&BTC(timerList))) {
        
        node = GetHeadList(&BTC(timerList));        
        while (node != &BTC(timerList)) { 
            curTimer = (EvmTimer*) node;
            node = node->Flink;
            
            if (curTimer == timer) {
                timecount--;
                //OS_Report("cancel%2d, (%d, %d)\n", timecount, timer->startTime, timer->time);
                RemoveEntryList(&curTimer->node);

                /* Recalculates the correct OS timer period */
                ResetOsTimer(OS_GetSystemTime());
                break;
            }
        }
    }

    /* If the timer was not found in our active list, simply assume it was
     * already cancelled.
     */
    if (stopHardware) {
        OS_ResumeHardware();
    }
}



#if XA_MULTITASKING == XA_ENABLED

/*---------------------------------------------------------------------------
 *            TimerFired()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Called by the OS layer to indicate that the system timer fired.
 */
static void TimerFired(void)
{
    /* The EVM_Process thread looks for specific expired timers */
    OS_NotifyEvm();
}

/*---------------------------------------------------------------------------
 *            ResetOsTimer()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Resets the actual OS timer based on the shortest remaining
 *            timer.
 */
static void ResetOsTimer(TimeT curTime)
{
    EvmTimer   *timer;
    ListEntry  *node;
    TimeT       timeElapsed;
    TimeT       minWait = (TimeT)(-1);

    /* Look for timers scheduled in the future */
    if (!IsListEmpty(&BTC(timerList))) {
        
        node = GetHeadList(&BTC(timerList));        
        while (node != &BTC(timerList)) {            
            timer = (EvmTimer*) node;
            node = node->Flink;
            timeElapsed = curTime - timer->startTime;

            /* If the timer must elapse sooner than minWait, adjust minWait */
            if (timeElapsed < timer->time) {                
                minWait = min(minWait, timer->time - timeElapsed); 
            }
        }
    }

    /* Are any timers in the future? */
    if (minWait < (TimeT)(-1))
    {
        OS_StartTimer(minWait,TimerFired);
    }
    else
    {
        OS_CancelTimer();
    }
}

#endif /* XA_MULTITASKING */


/*---------------------------------------------------------------------------
 *           EVM_StartTimer()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Start an Event Manager timer.
 *
 */
void EvmStartTimer(EvmTimer* timer, TimeT ticks, BOOL stopHardware)
{
    Assert(timer->func != 0);

    if (stopHardware) {
        OS_StopHardware();
    }
    
    /* Configure the timer's internals */
    timer->startTime = OS_GetSystemTime();
    timer->time = ticks;

    /* If the timer is already on the list, don't add it again. */
    if (!IsNodeOnList(&BTC(timerList), &timer->node)) {
        timecount++;
        //OS_Report("start%2d, (%d, %d)\n", timecount, timer->startTime, timer->time);
        InsertHeadList(&BTC(timerList), &timer->node);        
    }

    /* Reset the time amount for the OS timer */
    ResetOsTimer(timer->startTime);

    if (stopHardware) {
        OS_ResumeHardware();
    }
}


/*---------------------------------------------------------------------------
 *           CheckTimers()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Look for expired timers and call their notify functions
 *
 */
static void CheckTimers(void)
{
    EvmTimer   *timer;
    ListEntry  *node;
    TimeT       curTime;
    TimeT       timeElapsed;
    BOOL        timerFired;
    
    OS_StopHardware();

    timerFired = FALSE;

    /* See if we have any expired timers */
    if (!IsListEmpty(&BTC(timerList))) {
        
        curTime = OS_GetSystemTime();

        /* Look through all active timers */
        node = GetHeadList(&BTC(timerList));
        while (node != &BTC(timerList)) {            
            timer = (EvmTimer*) node;            
            node = node->Flink;
            timeElapsed = curTime - timer->startTime;

            /* If its time is complete, remove the timer and fire it. */
            if (timeElapsed >= timer->time) {
                timecount--;
                //OS_Report("check%2d, (%d, %d)\n", timecount, timer->startTime, timer->time);
                RemoveEntryList(&timer->node);
                OS_ResumeHardware();

                timer->func(timer);
                timerFired = TRUE;

                OS_StopHardware();

                /* Start looking back at the beginning of the list
                 * This is necessary because the contents of the list
                 * might have been modified during timer->func.
                 */
                node = GetHeadList(&BTC(timerList));
                curTime = OS_GetSystemTime();
            }
        }

        /* If any timer fired, we need to reset the OS timer and
         * re-schedule the stack task.
         */
        if (timerFired) {
            OS_NotifyEvm();
            ResetOsTimer(curTime);
        }
    }

    OS_ResumeHardware();
}


/*---------------------------------------------------------------------------
 *           EVM_Process()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Process all outstanding events.
 *
 * Return:    void
 */
void EVM_Process(void)
{
    
    /* Do not permit stack API calls to occur until we're done */
    OS_LockStack();

    /* Let HCI process any events */
    HCI_Process();

    /* Look for timers that might have fired */
    CheckTimers();
    
    /* Permit stack API calls again */
    OS_UnlockStack();
}

