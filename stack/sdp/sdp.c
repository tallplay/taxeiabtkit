/***************************************************************************
 *
 * File:
 *     $Workfile:sdp.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:18$
 *
 * Description:
 *     Source code for the Bluetooth Service Discovery Protocol.
 *
 * Created:
 *     October 18, 1999
 *
 * Copyright 1999-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "btalloc.h"
#include "sys/sdpi.h"

/****************************************************************************
 *
 * Functions
 *
 ****************************************************************************/

/*---------------------------------------------------------------------------
 *            SDP_Init(void)
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initialize SDP. 
 *
 * Return:    BT_STATUS_SUCCESS if success otherwise BT_STATUS_FAILED.
 */
BtStatus SDP_Init(void)
{
    BtStatus status = BT_STATUS_FAILED;

#if SDP_SERVER_SUPPORT == XA_ENABLED
    status = SdpInitServer();
    if (status != BT_STATUS_SUCCESS)
        return status;
#endif /* SDP_SERVER_SUPPORT == XA_ENABLED */

#if SDP_CLIENT_SUPPORT == XA_ENABLED
    status = SdpInitClient();
    if (status != BT_STATUS_SUCCESS)
        return status;
#endif /* SDP_CLIENT_SUPPORT == XA_ENABLED */

    return status;
}

/*---------------------------------------------------------------------------
 *            SDP_Deinit(void)
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Deinitialize SDP. 
 *
 * Return:    void 
 */
void SDP_Deinit(void)
{
#if SDP_SERVER_SUPPORT == XA_ENABLED
    SdpDeinitServer();
#endif /* SDP_SERVER_SUPPORT == XA_ENABLED */

#if SDP_CLIENT_SUPPORT == XA_ENABLED
    SdpDeinitClient();
#endif /* SDP_CLIENT_SUPPORT == XA_ENABLED */
}
