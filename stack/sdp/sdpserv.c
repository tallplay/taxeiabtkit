/***************************************************************************
 *
 * File:
 *     $Workfile:sdpserv.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:89$
 *
 * Description:
 *     Source code for the Bluetooth Service Discovery Protocol Server.
 *
 * Created:
 *     October 18, 1999
 *
 * Copyright 1999-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "btalloc.h"
#include "btrom.h"
#include "sys/sdpi.h"
#include "sys/mei.h"
#include "sys/debug.h"

#if SDP_SERVER_SUPPORT == XA_ENABLED
/****************************************************************************
 *
 * Function prototypes
 *
 ****************************************************************************/
void SdpServL2CapCallback(U16 ChannelId, L2capCallbackParms *Info);
void SdpServParseQuery(BtRemoteDevice* remDev, I16 len, U8* data);
BOOL SdpFindUuidInRecord(SdpRecord* record, U8* uuid);
BOOL SdpFindUuidInAttrib(const U8* aVal, U8* uuid, U16 uuidLen, U16 level);
U16 SdpServMarkMatches(BtRemoteDevice* remDev, U8* searchPat);
BtRemoteDevice* FindRemDevWithSdpServChannel(U16 channel);
BtPacket* SdpServGetPacket(void);
void SdpServSendError(BtRemoteDevice* remDev, BtPacket* packet, SdpErrorCode errCode);
void SdpServHandleServiceSearch(BtRemoteDevice* remDev, BtPacket* packet, 
                                U8* data, I16 len);
void SdpServHandleAttribReq(BtRemoteDevice* remDev, BtPacket* packet, 
                            U8* data, I16 len);

void SdpServHandleServiceSearchAttribReq(BtRemoteDevice* remDev, 
                                         BtPacket* packet, 
                                         U8* data, I16 len);
U8 SdpServGetNextContState(void);
SdpRecord* SdpServFindSdpRecord(U32 handle);
SdpRecord* SdpServFindMarkedRecord(U16 mask);
U32 SdpServMarkAttribs(BtRemoteDevice* remDev, SdpRecord* record, U8* list);
U16 SdpMarkAttribute(BtRemoteDevice* remDev, SdpRecord* record, U16 id);
BOOL SdpVerifyParmLength(BtRemoteDevice* remDev, BtPacket* packet, 
                                U8* data, I16 len, I16 len2);
U16 SdpStoreAttribData(BtRemoteDevice* remDev, BtPacket* packet, U16 offset, U16 final);
void SdpServFinishUpPacket(BtRemoteDevice* remDev, BtPacket* packet, I16 offset, I16 hOffset);

BOOL SdpServVerifyRecord(SdpRecord* record);
BtStatus SdpUpdateServiceDatabaseState(void);
void SdpHandleMarkedRecord(SdpRecord* record);
SdpServerInfo* SdpServGetInfo(void);

/****************************************************************************
 *
 * Functions
 *
 ****************************************************************************/

/*---------------------------------------------------------------------------
 *            SdpInitServer()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initialize SDP server. 
 *
 * Return:    BT_STATUS_SUCCESS if success otherwise BT_STATUS_FAILED.
 */
BtStatus SdpInitServer(void)
{
    I8  i;
    U16 mask;

    SDPS(SdpServPsm).callback = SdpServL2CapCallback;
    SDPS(SdpServPsm).psm = BT_PSM_SDP;
    SDPS(SdpServPsm).localMtu = SDP_SERVER_LOCAL_MTU;
    SDPS(SdpServPsm).minRemoteMtu = SDP_SERVER_MIN_REMOTE_MTU;
#if L2CAP_FLOW_CONTROL == XA_ENABLED
    SDPS(SdpServPsm).inLinkMode = SDPS(SdpServPsm).outLinkMode = L2MODE_BASIC;
#endif /* L2CAP_FLOW_CONTROL == XA_ENABLED */

    if (L2CAP_RegisterPsm((L2capPsm*)(&SDPS(SdpServPsm))) != BT_STATUS_SUCCESS) {
        return BT_STATUS_FAILED;
    }
    
    /* Initialize the list of records */
    InitializeListHead(&SDPS(records));

    /* Initialize the sdpRecMask of each SdpServiceInfo structure based
     * on the number of SDP clients. Also initializes the transmit packet.
     */
    mask = 1;
    for (i = 0; i < SDP_ACTIVE_CLIENTS; i++) {
        SDPS(serverInfos)[i].recMask = mask;
        mask <<= 1;

        /* Init the transmit packet */
        SDPS(serverInfos)[i].txPacket.data = SDPS(serverInfos)[i].txBuff;
        SDPS(serverInfos)[i].txPacketBusy = FALSE;
    }

    /* Initialize the record 0 attributes and record 0 itself. */
    SDPS(record0Attribs)[0].id = AID_SERVICE_CLASS_ID_LIST;
    SDPS(record0Attribs)[0].len = SDP_ZERO_SERV_CLASS_ID_SIZE;
    SDPS(record0Attribs)[0].value = ZeroServClassIdVal;

    SDPS(record0Attribs)[1].id = AID_VERSION_NUMBER_LIST;
    SDPS(record0Attribs)[1].len = SDP_ZERO_VER_NUMBER_SIZE;
    SDPS(record0Attribs)[1].value = ZeroVersionNumberList;

    /* Initialize the ServiceDatabaseState value of the 
     * SDP Server.
     */
    SDPS(ZeroServiceDatabaseState[0]) = DETD_UINT + DESD_4BYTES;
    /* Use OS_GetSystemTime() to prevent rollover problems */
    StoreBE32(SDPS(ZeroServiceDatabaseState+1), OS_GetSystemTime());

    SDPS(record0Attribs)[2].id = AID_SERVICE_DATABASE_STATE;
    SDPS(record0Attribs)[2].len = SDP_ZERO_SERVICE_DATABASE_STATE_SIZE;
    SDPS(record0Attribs)[2].value = SDPS(ZeroServiceDatabaseState);

    SDPS(record0).attribs = SDPS(record0Attribs); 
    SDPS(record0).num = 3; 

    /* Set SDP server's service record handle to zero. Should already be zero. 
     * This code is here for readability. 
     */
    SDPS(record0).handle = SDP_SERV_OWN_HANDLE;

    /* SDP is unlocked and can accept queries */
    SDPS(sdpLocked) = FALSE;

    /* Add an Sevice Discovery entry (Record Zero) to the SDP server */
    InsertTailList(&(SDPS(records)), &(SDPS(record0).node));

    /* Initialize the first SDP record */
    SDPS(nextHandle) = SDP_SERV_FIRST_HANDLE;

    return BT_STATUS_SUCCESS;
}

/*---------------------------------------------------------------------------
 *            SdpDeinitServer()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Deinitialize SDP client. 
 *
 * Return:    void 
 */
void SdpDeinitServer(void)
{
#if L2CAP_DEREGISTER_FUNC == XA_ENABLED
    L2CAP_DeregisterPsm((L2capPsm*)(&SDPS(SdpServPsm)));
#endif /* L2CAP_DEREGISTER_FUNC == XA_ENABLED */

    Report(("SDP Server:  Deinitialized.\n"));
}


/*---------------------------------------------------------------------------
 *            SDP_AddRecord()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Add a record to the SDP database. 
 *
 * Return:    BT_STATUS_SUCCESS if success, BT_STATUS_FAILED if record
 *            already exists, and BT_STATUS_INVALID_PARM if the new
 *            record is formed incorrectly.
 */
BtStatus SDP_AddRecord(SdpRecord* record)
{
    U8              i;
    BtStatus        status = BT_STATUS_FAILED;

#if XA_ERROR_CHECK == XA_ENABLED
    if ((!record) ||  (!SdpServVerifyRecord(record))) {
        return BT_STATUS_INVALID_PARM;
    }

#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(record && SdpServVerifyRecord(record));

    /* Lock the stack */
    OS_LockStack();

    if (IsNodeOnList(&SDPS(records), &(record->node))) {
        goto Done;
    }

    /* Insert the record at the end of the list */
    InsertTailList(&(SDPS(records)), &(record->node));
    record->classOfDevice &= COD_SERVICE_MASK;
    record->handle = SDPS(nextHandle);
    SDPS(nextHandle) += 1;
    /* Set the initial Service Record State for this record */
    record->recordState = OS_GetSystemTime();
    /* Clear all of the internal flags */
    record->flags = 0;
    record->handleFlag = 0;
    record->stateFlag = 0;

    /* Go through the attributes and clear the flags */
    for (i = 0; i < record->num; i++) {
        record->attribs[i].flags = 0;
    }

    /* Update the SDP class of device value and if it has changed
     * Write the class of device to the radio 
     */
    SdpServUpdateClassOfDevice();

    /* Update the SDP Server's ServiceDatabaseState */
    status = SdpUpdateServiceDatabaseState();

Done:
    OS_UnlockStack();
    return status;
}


/*---------------------------------------------------------------------------
 *            SDP_RemoveRecord()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Remove a record from the SDP database. 
 *
 * Return:    BT_STATUS_SUCCESS if success, BT_STATUS_FAILED if failure, 
 *            and BT_STATUS_INVALID_PARM if the parameters are invalid.
 */
BtStatus SDP_RemoveRecord(SdpRecord* record)
{
    BtStatus        status = BT_STATUS_FAILED;

#if XA_ERROR_CHECK == XA_ENABLED
    if (!record) {
        return BT_STATUS_INVALID_PARM;
    }

#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(record);

    /* Lock the stack */
    OS_LockStack();

    if (!IsNodeOnList(&SDPS(records), &(record->node))) {
        goto Done;
    }

    /* Remove the SDP record */
    RemoveEntryList(&(record->node));
    /* Clear out any continuation state info for any in-progress queries */
    SdpHandleMarkedRecord(record);

    /* Update the SDP class of device value and if it has changed
     * Write the class of device to the radio 
     */
    SdpServUpdateClassOfDevice();    

    /* Update the SDP Server's ServiceDatabaseState */
    status = SdpUpdateServiceDatabaseState();

Done:
    /* Unlock the stack */
    OS_UnlockStack();
    return status;
}

/*---------------------------------------------------------------------------
 *            SDP_LockRecord()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  If no current SDP Queries are in progress and if the SDP record
 *            to update is a valid entry, lock SDP for record updating.  SDP 
 *            will refuse any SDP queries via an SDP error response until the 
 *            record has been updated (signaled by SDP_UnlockRecord()).  In 
 *            order to guarantee safe SDP record updates, this function should 
 *            be called prior to altering any SDP record.  SDP can only be 
 *            locked one time, thus preventing simultaneous record updated by 
 *            multiple tasks.
 *
 * Return:    BT_STATUS_SUCCESS if success, BT_STATUS_FAILED if failure, 
 *            and BT_STATUS_INVALID_PARM if the parameters are invalid.
 */
BtStatus SDP_LockRecord(SdpRecord *record)
{
    I8              i;
    BtStatus        status = BT_STATUS_FAILED;

#if XA_ERROR_CHECK == XA_ENABLED
    if ((!record) || (!SdpServVerifyRecord(record))) {
        /* Can't lock a badly formed record  */
        return BT_STATUS_INVALID_PARM;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(record && SdpServVerifyRecord(record));

    /* Lock the stack */
    OS_LockStack();

    if (!IsNodeOnList(&SDPS(records), &(record->node))) {
        /* Can't lock a non-existent record */
        goto Done;
    } 

    /* Check if an SDP Query is in progress (ie txPacketBusy is TRUE for
     * any of the serverInfos structures 
     */
    for (i = 0; i < SDP_ACTIVE_CLIENTS; i++) {
        if (SDPS(serverInfos)[i].txPacketBusy == TRUE) {
            goto Done;
        }
    }
    
    /* SDP can only be locked by one task */
    if (SDPS(sdpLocked)) {
        goto Done;
    }

    /* SDP is locked and cannot accept queries */
    SDPS(sdpLocked) = TRUE;
    status = BT_STATUS_SUCCESS;

Done:
    /* Unlock the stack */
    OS_UnlockStack();
    return status;
}

/*---------------------------------------------------------------------------
 *            SDP_UnlockRecord()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Unlocks SDP from record updating, and once again allows SDP 
 *            queries to be handled normally.  Once this function is called, 
 *            it is no longer valid to alter any SDP record. If the unlocked 
 *            record has changed, its internal fields are updated 
 *            appropriately. SDP must be locked prior to calling this 
 *            function.
 *
 * Return:    BT_STATUS_SUCCESS if success, BT_STATUS_FAILED if failure, 
 *            and BT_STATUS_INVALID_PARM if the parameters are invalid.
 */
BtStatus SDP_UnlockRecord(SdpRecord *record, BOOL updated)
{
    I8              i;
    BtStatus        status = BT_STATUS_FAILED;

#if XA_ERROR_CHECK == XA_ENABLED
    if ((!record) || (!SdpServVerifyRecord(record))) {
        /* Can't unlock a badly formed record  */
        return BT_STATUS_INVALID_PARM;
    }
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(record && SdpServVerifyRecord(record));

    /* Lock the stack */
    OS_LockStack();

    if (!IsNodeOnList(&SDPS(records), &(record->node))) {
        /* Can't unlock a non-existent record */
        goto Done;
    } 

    /* SDP must already be locked */
    if (!SDPS(sdpLocked)) {
        goto Done;
    }

    /* SDP record has been changed */
    if (updated) {
        /* Clear out any continuation state info for any old 
         * in-progress queries 
         */
        SdpHandleMarkedRecord(record);

        /* Update the Service Record State for this record */
        record->recordState = OS_GetSystemTime();
        record->classOfDevice &= COD_SERVICE_MASK;
        /* Clear all of the internal flags */
        record->flags = 0;
        record->handleFlag = 0;
        record->stateFlag = 0;
        /* Keep the existing service record handle */

        /* Go through the attributes and clear the flags */
        for (i = 0; i < record->num; i++) {
            record->attribs[i].flags = 0;
        }

        /* Update the SDP class of device value and if it has changed
         * Write the class of device to the radio 
         */
        SdpServUpdateClassOfDevice();
    }

    /* SDP is unlocked and can accept queries */
    SDPS(sdpLocked) = FALSE;
    status = BT_STATUS_SUCCESS;

Done:
    /* Unlock the stack */
    OS_UnlockStack();
    return status;
}

/*---------------------------------------------------------------------------
 *            SdpServL2CapCallback()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle the callbacks from L2CAP 
 *
 * Return:    void 
 */
void SdpServL2CapCallback(U16 channelId, L2capCallbackParms *info)
{
    BtRemoteDevice *remDev;
    U16             respCode;
    BtStatus        status;
    BtPacket       *packet;

    remDev = info->aclLink;

    switch (info->event) {
    case L2EVENT_CONNECT_IND:
        if ((SDPS(numClients) < SDP_NUM_CLIENTS) && (remDev->sdpServInfo == 0)) {
            /* Accept the connection */
            respCode = L2CONN_ACCEPTED;

            /* Assign an SdpServerInfo structure to the remote device */
            remDev->sdpServInfo = SdpServGetInfo();
            Assert(remDev->sdpServInfo != 0);
            SDPS(numClients) += 1;
            Assert(SDPS(numClients) > 0);

            remDev->sdpServInfo->channel = channelId;
            Report(("SDP Server: Incoming L2CAP connection accepted\n"));
        } else {
            respCode = L2CONN_REJECT_NO_RESOURCES;
            Report(("SDP Server: Incoming L2CAP connection rejected\n"));
        }
        (void)L2CAP_ConnectRsp(channelId, respCode, 0);
        break;

    case L2EVENT_CONNECTED:
        /* The connection is up. Find the remote device */ 
        Assert(remDev != 0);
        Assert(remDev->sdpServInfo != 0);
        Assert(SDPS(numClients) > 0);

        SnifferRegisterEndpoint(SPI_L2CAP, &channelId, remDev, SPI_SDP);

        Report(("SDP Server: Connection complete\n"));
        break;

    case L2EVENT_DISCONNECTED:
        Report(("SDP Server: Connection disconnected\n"));
        remDev = SdpFindRemDevWithSdpChannel(channelId, BST_SDP_SERVER);
        if (remDev != 0) {
            Assert(SDPS(numClients) > 0);
            SDPS(numClients) -= 1;

            /* Free the SdpServerInfo record */
            Assert(remDev->sdpServInfo != 0);
            remDev->sdpServInfo->busy = FALSE;
            remDev->sdpServInfo->txPacketBusy = FALSE;
            remDev->sdpServInfo = 0;
        }
        break;

    case L2EVENT_DATA_IND:
        /* We have received data from an SDP client */
        Report(("SDP Server: received data\n"));
        Assert(remDev != 0);
        Assert(remDev->sdpServInfo != 0);

        if ((remDev->sdpServInfo->txPacketBusy == FALSE) && (!SDPS(sdpLocked))) {
            /* Only parse the query if SDP is unlocked and the remote
             * device's transmit packet is free.
             */
            SdpServParseQuery(remDev, (I16)info->dataLen, info->ptrs.data);
        } 
        else if ((SDPS(sdpLocked)) && (remDev->sdpServInfo->txPacketBusy == FALSE)) {
            /* SDP is locked due to a record update.  Reject this SDP Query. 
             * The remote device will have to try again later.
             */ 
            
            /* Get a packet for the error response */
            Assert(remDev->sdpServInfo->txPacketBusy == FALSE);
            packet = &(remDev->sdpServInfo->txPacket);
            remDev->sdpServInfo->txPacketBusy = TRUE;
            SdpServSendError(remDev, packet,BSEC_OUT_OF_RESOURCES );
        }
        else {
            /* Packet is still in use so the SDP client has violated the protocol
             * we need to disconnect.
             */
            status = L2CAP_DisconnectReq(channelId);
            AssertEval((status == BT_STATUS_PENDING) || (status == BT_STATUS_BUSY));
        }
        break;

    case L2EVENT_PACKET_HANDLED:
        Report(("SDP Server: packet returned\n"));
        Assert(remDev->sdpServInfo != 0);
        remDev->sdpServInfo->txPacketBusy = FALSE;
        break;
    }

}

/*---------------------------------------------------------------------------
 *            SdpServParseQuery()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Parse the SDP query 
 *
 * Return:    void 
 */
void SdpServParseQuery(BtRemoteDevice* remDev, I16 len, U8* data)
{
    U16       transID;
    BtPacket* packet;
    I8        offset;

    offset = BT_PACKET_HEADER_LEN - 5;

    /* Get a packet for the response */
    Assert(remDev->sdpServInfo->txPacketBusy == FALSE);
    packet = &(remDev->sdpServInfo->txPacket);
    remDev->sdpServInfo->txPacketBusy = TRUE;

    /* Get the trans ID. If it does not exist then set it to 0. */
    if (len < 3) {
        transID = 0;
    } else {
        transID = BEtoHost16(data+1);
    }

    /* Insert the transID into the response */
    StoreBE16(packet->header+offset+1, transID);

    /* Verify that the packet contains the minimal amount of data */
    if (len < SDP_QUERY_MIN_LEN) {
        Report(("SDP Server: query len is short\n"));
        SdpServSendError(remDev, packet,BSEC_BAD_PDU_SIZE);
        return;
    }

    switch (data[0]) {
    case BSQT_SERVICE_SEARCH_REQ:
        SdpServHandleServiceSearch(remDev, packet, data, len);
        break;

    case BSQT_ATTRIB_REQ:
        SdpServHandleAttribReq(remDev, packet, data, len);
        break;

    case BSQT_SERVICE_SEARCH_ATTRIB_REQ:
        SdpServHandleServiceSearchAttribReq(remDev, packet, data, len);
        break;

    default:
        /* Not a valid query so return BSEC_BAD_SYNTAX error */ 
        SdpServSendError(remDev, packet,BSEC_BAD_SYNTAX );
        break;
    }
}


/*---------------------------------------------------------------------------
 *            SdpServHandleServiceSearch()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Mark all records that match the search pattern and return the
 *            the number of matches. 
 *
 * Return:    void 
 */
void SdpServHandleServiceSearch(BtRemoteDevice* remDev, BtPacket* packet, 
                                U8* data, I16 len)
{
    SdpRecord*    record;
    U32             plen;
    U16             offset;
    U16             matches;
    U16             scratch;
    I8              hOffset;
    U8              contLen;

    hOffset = BT_PACKET_HEADER_LEN - 5;

    /* Verify that the Service Search pattern is legally formed */
    if (!SdpValidateQueryParms(data[0], data+5, (U16)(len - 5), 0, 0, BSQM_DONT_CARE)) {
        /* Send an error packet with reason BSEC_BAD_SYNTAX */
        Report(("SDP Server: query parameters are not valid\n"));
        SdpServSendError(remDev, packet, BSEC_BAD_SYNTAX);
        return;
    }

    /* Get the length of the continuation state */
    plen = SdpParseElement(data+5, &offset);
    plen += offset;
    contLen = data[5+plen+2];

    /* Verify the length of the parameters.  */
    if (!SdpVerifyParmLength(remDev, packet, data, len, (I16)(plen+3+contLen))) {
        Report(("SDP Server: HandleServSearch - bad parameter len\n"));
        return;
    }

    /* Put in the response opcode */
    packet->header[hOffset] = BSQR_SERVICE_SEARCH_RESP;

    /* If the continuation state is 0 then this is a new query. We will
     * throw out any old continuation states and start fresh
     */
    if (contLen == 0) {
        /* First mark all records that meet the service search pattern */
        Report(("SDP Server: Service Search rcv continuation state = 0\n"));
        remDev->sdpServInfo->totalMatches = SdpServMarkMatches(remDev, data+5);
        remDev->sdpServInfo->totalMatches = 
            min(remDev->sdpServInfo->totalMatches,BEtoHost16(data+5+plen));
        remDev->sdpServInfo->matchesSent = 0;
        remDev->sdpServInfo->contState = 0;
    } else {
        /* We have a continuation state so see if it is valid */
        if ((contLen != 1) || (data[5+plen+3] != remDev->sdpServInfo->contState)) {
            Report(("SDP Server: service search rcv bad continuation state\n"));
            SdpServSendError(remDev, packet, BSEC_BAD_CONTINUATION);
            return;
        }
        Assert(remDev->sdpServInfo->totalMatches > 0);
        Report(("SDP Server: Service Search rcv cont state size = %d val = %d\n",
            contLen, remDev->sdpServInfo->contState));
    }
    /* Insert the Total number of matches into the response packet */
    StoreBE16(packet->data, remDev->sdpServInfo->totalMatches);
    scratch = 0;
    offset = 4;

    if (remDev->sdpServInfo->totalMatches > 0) {
        /* We have some matches so create the response. First determine how
         * many matches we can send in the first packet. We determine this
         * by starting with the maximum size we are allowed to send then
         * subtracting off the 5 bytes for the header, subracting off 4 bytes 
         * for the total record count and the current record count. Finally, 
         * we subract off 1 for the continuation state. We divide the result 
         * by 4 to determine the number of Service Record handles we can return.
         * The maximum size we can send is the minimum of the L2CAP MTU size
         * and our self imposed size.
         */
        Report(("SDP Server: Match found\n"));
        scratch = min(L2CAP_GetTxMtu(remDev->sdpServInfo->channel),SDP_SERVER_SEND_SIZE);
        scratch -= 10;
        matches = scratch/4;
        if (matches < (remDev->sdpServInfo->totalMatches - remDev->sdpServInfo->matchesSent)) {
            /* We do not fit in a single packet so determine the amount
             * that will fit and get a continuation state
             */
            matches = (scratch - 1)/4;
            remDev->sdpServInfo->contState = SdpServGetNextContState();
        } else {
            /* We do fit into a single packet so continuation state is 0 */
            matches = remDev->sdpServInfo->totalMatches - remDev->sdpServInfo->matchesSent;
            remDev->sdpServInfo->contState = 0;
        }

        /* Update the matches sent */
        remDev->sdpServInfo->matchesSent += matches;

        /* Now go through the records and store the values into the
         * packet. Unmark the matches.
         */
        record = (SdpRecord*) SDPS(records).Flink;
        scratch = matches; 
        while ((ListEntry*)record != &SDPS(records)) {
            if ((record->flags & remDev->sdpServInfo->recMask) &&
                (matches > 0)) {
                StoreBE32(packet->data+offset, record->handle);
                offset += 4;
                matches--;

                /* Unmark the record */
                record->flags &= ~remDev->sdpServInfo->recMask;
            }

            if (remDev->sdpServInfo->contState == 0) {
                /* This is the last or only packet so unmark the rest as well */
                record->flags &= ~remDev->sdpServInfo->recMask;
            }

            /* Advance to the next record */
            record = (SdpRecord*) record->node.Flink;
        }
        Assert(matches == 0);
    } else {
        /* We have no matches so return 0 found */
        Report(("SDP Server: Match not found\n"));
    }
    /* Put the number of current matches into the packet */
    StoreBE16(packet->data+2, scratch);

    /* Finish sending the packet */
    SdpServFinishUpPacket(remDev, packet, (I16)offset, (I16)hOffset);

}


/*---------------------------------------------------------------------------
 *            SdpServHandleAttribReq()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle an attribute value request. 
 *
 * Return:    void 
 */
void SdpServHandleAttribReq(BtRemoteDevice* remDev, BtPacket* packet, 
                            U8* data, I16 len)
{
    U32            plen;
    U16            offset;
    U16            scratch;
    I8             hOffset;
    U8             contLen;

    hOffset = BT_PACKET_HEADER_LEN - 5;

    /* Verify that the Attribute request is legally formed */
    if (!SdpValidateQueryParms(data[0], data+5, (U16)(len - 5), 0, 0, BSQM_DONT_CARE)) {
        /* Send an error packet with reason BSEC_BAD_SYNTAX */
        Report(("SDP Server: query parameters are not valid\n"));
        SdpServSendError(remDev, packet, BSEC_BAD_SYNTAX);
        return;
    }

    /* Get the length of the continuation state */
    plen = SdpParseElement(data+(5+6), &offset);
    plen += offset;
    /* Add the header, service record handle, max attr. byte count,
     * and attribute id list length to get to the continuation state.
     */
    contLen = data[plen+(5+6)];

    /* Verify the length of the parameters.  */
    if (!SdpVerifyParmLength(remDev, packet, data, len, (I16)(plen+7+contLen))) {
        Report(("SDP Server: HandleServAttrib - bad parameter len\n"));
        return;
    }

    /* Determine if the service record handle is valid */
    remDev->sdpServInfo->sdpRecord = SdpServFindSdpRecord(BEtoHost32(data+5));
    if (remDev->sdpServInfo->sdpRecord == 0) {
        Report(("SDP Server: Invalid record handle\n"));
        SdpServSendError(remDev, packet, BSEC_BAD_HANDLE);
        return;
    }

    /* Put in the response opcode */
    packet->header[hOffset] = BSQR_ATTRIB_RESP;

    /* If the continuation state is 0 then this is a new query. We will
     * throw out any old continuation states and start fresh
     */
    if (contLen == 0) {
        /* First mark all the attributes in the record that should be returned
         * and get the total size. We need to add three for the header.
         */
        Report(("SDP Server: Attrib Search rcv continuation state = 0\n"));
        remDev->sdpServInfo->totalAttribSize = 
            SdpServMarkAttribs(remDev, remDev->sdpServInfo->sdpRecord, data+(5+6)) + 3;
        remDev->sdpServInfo->attribState = 0;
        remDev->sdpServInfo->contState = 0;
    } else {
        /* We have a continuation state so see if it is valid */
        if ((contLen != 1) || (data[5+plen+7] != remDev->sdpServInfo->contState)) {
            Report(("SDP Server: Attrib search rcv bad continuation state\n"));
            SdpServSendError(remDev, packet, BSEC_BAD_CONTINUATION);
            return;
        }
        Report(("SDP Server: Attrib Search rcv cont state size = %d val = %d\n",contLen, 
            remDev->sdpServInfo->contState));
    }

    /* Determine the amount of attribute ID list data we can send in one L2CAP packet. This
     * is the largest packet we can send minus 5 bytes for the header, 2 bytes for the
     * the attribute list byte count and 2 bytes for the continuation state
     */

    /* Get the L2CAP MTU size - largest packet we can send */
    scratch = min(L2CAP_GetTxMtu(remDev->sdpServInfo->channel),SDP_SERVER_SEND_SIZE);

    /* Subtract header, attribute list byte count, and continuation state bytes */
    scratch -= (5 + 4);

    /* Now see if the MaxAttributeByteCount requires us to be smaller */
    scratch = min(scratch, BEtoHost16(data+(5+4)));
    
    /* Scratch now contains the total amount of space available for attribute data.
     * Determine if we need a continuation state or not.
     */
    if (remDev->sdpServInfo->totalAttribSize > (U32)scratch) {
        /* We need at least another packet to send the data so get a continuation state */
        remDev->sdpServInfo->contState = SdpServGetNextContState();
    } else {
        /* This fits into one packet so continuation state is 0 */
        scratch = (U16)remDev->sdpServInfo->totalAttribSize;
        remDev->sdpServInfo->contState = 0;
    }

    /* Store the attribute list byte count. Advance offset past the attrib 
     * list byte count 
     */
    StoreBE16(packet->data, scratch);
    offset = 2;

    /* Store the attribute data. This is a state machine. */
    offset = SdpStoreAttribData(remDev, packet, offset, (U16)(scratch+2));

    /* Update the total attribute size and send the packet */
    remDev->sdpServInfo->totalAttribSize -= (U32)scratch;
    SdpServFinishUpPacket(remDev, packet, (I16)offset, (I16)hOffset);
        
}

/*---------------------------------------------------------------------------
 *            SdpServHandleServiceSearchAttribReq()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle a service search attribute value request.
 *
 * Return:    void 
 */
void SdpServHandleServiceSearchAttribReq(BtRemoteDevice* remDev, 
                                         BtPacket* packet, 
                                         U8* data, I16 len)
{
    SdpRecord*     record;
    U32            slen, alen;
    U16            soffset, aoffset;
    U16            scratch;
    I8             hOffset;
    U8             contLen;
    I8             extraTotalAttribSize;

    hOffset = BT_PACKET_HEADER_LEN - 5;

    /* Verify that the Service search attribute request is legally formed */
    if (!SdpValidateQueryParms(data[0], data+5, (U16)(len - 5), 0, 0, BSQM_DONT_CARE)) {
        /* Send an error packet with reason BSEC_BAD_SYNTAX */
        Report(("SDP Server: SerSearAttrReq - query parameters are not valid\n"));
        SdpServSendError(remDev, packet, BSEC_BAD_SYNTAX);
        return;
    }

    /* First get the length of the service search pattern */
    slen = SdpParseElement(data+5, &soffset);
    slen += soffset;

    /* Now get the length of the attribute ID list */
    alen = SdpParseElement(data+5+slen+2, &aoffset);
    alen += aoffset;

    /* Now get the length of the continuation state */
    contLen = data[5+slen+2+alen];

    /* Verify the length of the parameters.  */
    if (!SdpVerifyParmLength(remDev, packet, data, len, (I16)(slen+alen+3+contLen))) {
        Report(("SDP Server: HandleServSearchAttrib - bad parameter len\n"));
        return;
    }

    /* Put in the response opcode */
    packet->header[hOffset] = BSQR_SERVICE_SEARCH_ATTRIB_RESP;

    /* If the continuation state is 0 then this is a new query. We will
     * throw out any old continuation states and start fresh
     */
    aoffset = 0;
    if (contLen == 0) {
        Report(("SDP Server: Service Search rcv coninuation state = 0\n"));
        /* First mark all records that meet the service search pattern */
        SdpServMarkMatches(remDev, data+5);

        /* Determine the total size of the attribute ID list. */
        remDev->sdpServInfo->totalServAttribSize = 3;
        record = (SdpRecord*) SDPS(records).Flink;
        while ((ListEntry*)record != &SDPS(records)) {
            if (record->flags & remDev->sdpServInfo->recMask) {
                /* This is a marked record so mark the attributes */
                remDev->sdpServInfo->totalServAttribSize += 
                    SdpServMarkAttribs(remDev, record, data+(5+2+slen)) + 3;
            }
            /* Advance to the next record */
            record = (SdpRecord*) record->node.Flink;
        }
        /* Put in the header for the attribute ID list */
        packet->data[2] = DETD_SEQ + DESD_ADD_16BITS;
        StoreBE16(packet->data+3, (U16)(remDev->sdpServInfo->totalServAttribSize - 3));
        aoffset = 3;

        remDev->sdpServInfo->contState = 0;
        remDev->sdpServInfo->attribState = 6;
        extraTotalAttribSize = 3;
    } else {
        /* We have a continuation state so see if it is valid */
        if ((contLen != 1) || (data[5+slen+alen+3] != remDev->sdpServInfo->contState)) {
            Report(("SDP Server: HandleServSearchAttrib - rcv bad continuation state\n"));
            SdpServSendError(remDev, packet, BSEC_BAD_CONTINUATION);
            return;
        }
        Report(("SDP Server: HandleServSearchAttrib - rcv cont state size = %d val = %d\n",
            contLen, remDev->sdpServInfo->contState));
        extraTotalAttribSize = 0;
    }

    /* Determine the amount of attribute ID list data we can send in one L2CAP packet. This
     * is the largest packet we can send minus 5 bytes for the header, 2 bytes for the
     * the attribute list byte count and 2 bytes for the continuation state
     */

    /* Get the L2CAP MTU size - largest packet we can send */
    scratch = min(L2CAP_GetTxMtu(remDev->sdpServInfo->channel),SDP_SERVER_SEND_SIZE);

    /* Subtract header, attribute list byte count, and continuation state bytes */
    scratch -= (5 + 4);

    /* Now see if the MaxAttributeByteCount requires us to be smaller */
    scratch = min(scratch, BEtoHost16(data+(5+slen)));
    
    /* Scratch now contains the total amount of space available for attribute ID list
     * data in a packet. Determine if we need a continuation state or not.
     */
    if (remDev->sdpServInfo->totalServAttribSize > (U32)scratch) {
        /* We need at least another packet to send the data so get a continuation state */
        remDev->sdpServInfo->contState = SdpServGetNextContState();
    } else {
        /* This fits into one packet so continuation state is 0 */
        scratch = (U16)remDev->sdpServInfo->totalServAttribSize;
        remDev->sdpServInfo->contState = 0;
    }

    /* Store the attribute lists byte count. Advance offset past the attrib 
     * list byte count 
     */
    StoreBE16(packet->data, scratch);
    aoffset += 2;

    /* Adjust scratch to account for attrib lists byte count */
    scratch += 2;

    /* Adjust totalServAttribSize for the initial list header */
    remDev->sdpServInfo->totalServAttribSize -= extraTotalAttribSize; 

    /* Now load in the attribute ID list info */
    while (aoffset < scratch) {
        if (remDev->sdpServInfo->attribState < 6) {
            /* Storing the attribute data */
            soffset = (U16)min(remDev->sdpServInfo->totalAttribSize, (U32)(scratch - aoffset));
            aoffset = SdpStoreAttribData(remDev, packet, aoffset, (U16)(soffset+aoffset));
            remDev->sdpServInfo->totalAttribSize -= (U32)soffset;
            remDev->sdpServInfo->totalServAttribSize -= (U32)soffset;

            /* Determine the next state */
            if (remDev->sdpServInfo->totalAttribSize == 0) {
                /* This record is done so goto the next record */
                remDev->sdpServInfo->attribState = 6;
            }
        } else {
            Assert(remDev->sdpServInfo->attribState == 6);
            /* Get the next record */
            remDev->sdpServInfo->sdpRecord = 
                SdpServFindMarkedRecord(remDev->sdpServInfo->recMask);
            Assert(remDev->sdpServInfo->sdpRecord != 0);

            /* Mark the attributes and determine the amount of data to store */
            remDev->sdpServInfo->totalAttribSize = 
                SdpServMarkAttribs(remDev, remDev->sdpServInfo->sdpRecord, data+(5+2+slen)) + 3;
            remDev->sdpServInfo->totalAttribSize = min(remDev->sdpServInfo->totalAttribSize, 
                                          remDev->sdpServInfo->totalServAttribSize);
            remDev->sdpServInfo->attribState = 0;

            /* Clear the record flag */
            remDev->sdpServInfo->sdpRecord->flags &= ~remDev->sdpServInfo->recMask;
        }
    }
    Assert(aoffset == scratch);

    /* Finish sending the packet */
    SdpServFinishUpPacket(remDev, packet, (I16)aoffset, (I16)hOffset);

}


/*---------------------------------------------------------------------------
 *            SdpServSendError()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Mark all records that match the search pattern and return the
 *            the number of matches. 
 *
 * Return:    void 
 */
void SdpServSendError(BtRemoteDevice* remDev, BtPacket* packet, SdpErrorCode errCode)
{
    I8        hOffset;
    BtStatus  status;

    hOffset = BT_PACKET_HEADER_LEN - 5;

    /* Put in the PDU ID and the parameter size. The parameter size
     * is 2 for the error code
     */
    packet->header[hOffset] = BSQR_ERROR_RESP;
    StoreBE16(packet->header+hOffset+3, 2); 

    /* Put in the error code */
    StoreBE16(packet->data, errCode);

    /* Set the sizes */
    packet->headerLen = 5;
    packet->dataLen = 2;

    /* Call L2CAP to send the data */
    status = L2CAP_SendData(remDev->sdpServInfo->channel, packet);
    Assert(status == BT_STATUS_PENDING);
    if (status != BT_STATUS_PENDING) {
        /* The send failed.  Clear the txPacketBusy flag, since the 
         * packet will never complete.
         */
        remDev->sdpServInfo->txPacketBusy = FALSE;
        Report(("SDP Server: L2CAP_SendData call failed. error = %x\n", status));
    }
    Report(("SDP Server: Sending an error. error = %x\n", errCode));
}
        
/*---------------------------------------------------------------------------
 *            SdpServMarkMatches()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Mark all records that match the service search pattern and 
 *            return the number of matches. 
 *
 * Return:    Number of records that match the service search pattern. 
 */
U16 SdpServMarkMatches(BtRemoteDevice* remDev, U8* searchPat)
{
    SdpRecord* record;
    U32          len;
    U8*          p;
    U8*          p2;
    U16          offset;
    U16          matches;

    /* Mark every record in the data base that has all the UUIDs of the
     * service search pattern
     */
    matches = 0;
    record = (SdpRecord*) SDPS(records).Flink;
    while ((ListEntry*)record != &SDPS(records)) {
        /* Make sure the record flag is clean. This is needed in case
         * we aborting a continuation query
         */
        record->flags &= ~remDev->sdpServInfo->recMask;
 
        /* For each UUID in the service search pattern determine
         * if it exists in the attributes of the record
         */
        len = SdpParseElement(searchPat, &offset);
        p = searchPat+offset;
        p2 = p + len;
        while (p < p2) {
            if (!SdpFindUuidInRecord(record, p)) {
                /* This UUID is not in the record so break out of the
                 * the loop.
                 */
                break;
            }

            /* Advance to the next UUID */
            p += SdpParseElement(p, &offset);
            p += offset;
        }
        Assert(p <= p2);

        if (p == p2) {
            /* This record contains all UUIDs so mark it */
            record->flags |= remDev->sdpServInfo->recMask;
            matches++;
        }
        /* Advance to the next record */
        record = (SdpRecord*) record->node.Flink;
    }
    return matches;
}

/*---------------------------------------------------------------------------
 *            SdpFindUuidInRecord()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Determine if the UUID exists in the record 
 *
 * Return:    TRUE if UUID exist. FALSE otherwise.
 */
BOOL SdpFindUuidInRecord(SdpRecord* record, U8* uuid)
{
    U32 len;
    U16 offset;
    U8 i;

    len = SdpParseElement(uuid, &offset);
    Assert((len == 2) || (len == 4) || (len == 16));

    for (i = 0; i < record->num; i++) {
        if (SdpFindUuidInAttrib(record->attribs[i].value, uuid+offset,(U16)len,0)) {
            return TRUE;
        }
    }
    return FALSE;
}

/*---------------------------------------------------------------------------
 *            SdpFindUuidInAttrib()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Determine if the UUID exists in the attribute value 
 *
 * Return:    TRUE if UUID exist. FALSE otherwise.
 */
BOOL SdpFindUuidInAttrib(const U8* aVal, U8* uuid, U16 uuidLen, U16 level)
{
    U32 len, i;
    U16 offset;
    const U8* p;

    if (level > SDP_SERVER_MAX_LEVEL) {
        Report(("SdpFindUUidInAttrib: max recursion level exceeded"));
        return FALSE;
    }

    switch (aVal[0] & DETD_MASK) {
    case DETD_UUID:
        /* The UUID in the database should be in its smallest form as should
         * the query one. So for now just compare them.
         */
        len = SdpParseElement(aVal, &offset);
        return (SdpUuidCmp((const U8*)(aVal+1),(U16)len,uuid,uuidLen));

    case DETD_SEQ:
    case DETD_ALT:
        /* Go through the sequence and look for UUID entries. First
         * determine the size of the sequence.
         */
        len = SdpParseElement(aVal, &offset);
        p = (aVal+offset);
        i = 0;
        while (i < len) {
            /* Go through the sequence to see if any UUIDs match. Use
             * this function recursively on each element
             */
            if (SdpFindUuidInAttrib(p+i,uuid,uuidLen, (U16)(level+1))) {
                return TRUE;
            }

            /* Advance to the next element */
            i += SdpParseElement(p+i,&offset);
            i += (U32)offset;
        }
        Assert(i == len);
        break;
    }
    return FALSE;
}

/*---------------------------------------------------------------------------
 *            SdpServMarkAttribs()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Mark all records that match the search pattern and return the
 *            the number of matches. 
 *
 * Return:    Total length of the marked attributes
 */
U32 SdpServMarkAttribs(BtRemoteDevice* remDev, SdpRecord* record, U8* list)
{
    U32          len, i, j, begin, last;
    U16          offset;
    U32          totalLen;

    /* First clear out any existing marks */
    record->handleFlag &= ~remDev->sdpServInfo->recMask;
    record->stateFlag &= ~remDev->sdpServInfo->recMask;
    for (i = 0; i < (U32)record->num; i++) {
        record->attribs[i].flags &= ~remDev->sdpServInfo->recMask;
    }

    len = SdpParseElement(list, &offset);
    i = 0;
    totalLen = 0;

    /* Go through the attribute ID list and mark every attribute
     * that needs to be returned
     */
    while (i < len) {
        switch (list[offset+i] & DESD_MASK) {
        case DESD_2BYTES:
            /* Single attribute */
            j = (U32)BEtoHost16((list+offset+i+1));
            totalLen += (U32)SdpMarkAttribute(remDev, record, (U16)j);
            i += 3;
            break;

        case DESD_4BYTES:
            /* Range of attributes */
            begin = (U32)BEtoHost16((list+offset+i+1));
            last = (U32)BEtoHost16((list+offset+i+3));
            /* Mark Service Record Handle is the range */
            if (begin == 0) 
                totalLen += (U32)SdpMarkAttribute(remDev, record, 0);
            /* Mark Service Record State is in the range */
            if ((2 >= begin) && (2 <= last)) 
                totalLen += (U32)SdpMarkAttribute(remDev, record, 2);

            /* Mark all attributes within the range that exist in the record */
            for (j = 0; j < record->num; j++) {
                if ((record->attribs[j].id >= begin) && 
                    (record->attribs[j].id <= last)) {
                    totalLen += (U32)SdpMarkAttribute(remDev, record, 
                                     record->attribs[j].id);
                }
            }
            i += 5;
            break;

        /* Only 2 byte and 4 byte attribute ID values can exist.
         * VerifyAttributeIdList() guarantees this.
         */

        }
    }
    Assert( i == len);
    return totalLen;
}

/*---------------------------------------------------------------------------
 *            SdpServGetNextContState()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Return the next valid continuation state. 
 *
 * Return:    U8
 */
U8 SdpServGetNextContState(void)
{
    SDPS(contState) += 1;
    if (SDPS(contState) == 0) {
        SDPS(contState) = 1;
    }
    return SDPS(contState);
}

/*---------------------------------------------------------------------------
 *            SdpServFindSdpRecord()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Return a pointer to the record with the given record number or 0. 
 *
 * Return:    SdpRecord*
 */
SdpRecord* SdpServFindSdpRecord(U32 handle)
{
    SdpRecord* record;

    record = (SdpRecord*) SDPS(records).Flink;
    while ((ListEntry*)record != &SDPS(records)) {
        if (record->handle == handle) {
            return record;
        }
        /* Advance to the next record */
        record = (SdpRecord*) record->node.Flink;
    }
    return 0;

}

/*---------------------------------------------------------------------------
 *            SdpServFindMarkedAttribute()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Return a pointer to the first marked attribute. 
 *
 * Return:    SdpAttribute*
 */
SdpAttribute* SdpServFindMarkedAttribute(SdpRecord* record, U16 mask)
{
    U8 i;

    for (i = 0; i < record->num; i++) {
        if (record->attribs[i].flags & mask) {
            return &(record->attribs[i]);
        }
    }
    return 0;
}

/*---------------------------------------------------------------------------
 *            SdpServFindMarkedRecord()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Return a pointer to the first marked attribute. 
 *
 * Return:    SdpAttribute*
 */
SdpRecord* SdpServFindMarkedRecord(U16 mask)
{
    SdpRecord* record;

    record = (SdpRecord*) SDPS(records).Flink;
    while ((ListEntry*)record != &SDPS(records)) {
        if (record->flags & mask) {
            return record;
        }
        /* Advance to the next record */
        record = (SdpRecord*) record->node.Flink;
    }
    return 0;

}

/*---------------------------------------------------------------------------
 *            SdpMarkAttribute()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Mark the given attribute in the given record if it exists.
 *            
 * Return:    The total size of the attribute including the header and 
 *            attribute ID.
 */
U16 SdpMarkAttribute(BtRemoteDevice* remDev, SdpRecord* record, U16 id)
{
    U16 offset;
    U8  i;
    U16 size;

    Assert(remDev->sdpServInfo != 0);

    size = 0;
    for (i = 0; i < record->num; i++) {
        if ((id == 0) && !(record->handleFlag & remDev->sdpServInfo->recMask)) {
            Report(("SDP Server: Attribute ID = 0 marked\n"));
            record->handleFlag |= remDev->sdpServInfo->recMask;
            /* Header and Attribute ID + Header and Service Record Handle */
            size = 3 + 5;
            break;
        }
        else if ((id == 2) && !(record->stateFlag & remDev->sdpServInfo->recMask)) {
            Report(("SDP Server: Attribute ID = 2 marked\n"));
            record->stateFlag |= remDev->sdpServInfo->recMask;
            /* Header and Attribute ID + Header and Service Record State */
            size = 3 + 5;
            break;
        } 
        else if ((record->attribs[i].id == id) && 
                   !(record->attribs[i].flags & remDev->sdpServInfo->recMask)) {
            Report(("SDP Server: Attribute ID = %d marked\n",id));
            record->attribs[i].flags |= remDev->sdpServInfo->recMask;
            /* Get the length of the attribute */
            size = (U16)SdpParseElement(record->attribs[i].value, &offset);
            /* Header offset + Attribute ID and Header */
            size += offset + 3;
            break;
        } 
    }
    return size;
}


/*---------------------------------------------------------------------------
 *            SdpVerifyParmLength()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Verify the length of the parameters. 
 *
 * Return:    TRUE if valid otherwise FALSE
 */
BOOL SdpVerifyParmLength(BtRemoteDevice* remDev, BtPacket* packet, 
                                U8* data, I16 len, I16 len2)
{
    /* Verify the length of the parameters.  */
    if (((U32)(len - 5) < len2) || 
        (BEtoHost16(data+3) < len2) ||
        (BEtoHost16(data+3) < (len - 5)) ||
        (BEtoHost16(data+3) > (len - 5)) ||
        (BEtoHost16(data+3) > len2)) {
        Report(("SDP Server: query parmeters len is bad\n"));
        SdpServSendError(remDev, packet, BSEC_BAD_PDU_SIZE);
        return FALSE;
    }

    return TRUE;
}
    

/*---------------------------------------------------------------------------
 *            SdpStoreAttribData()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Store the marked attribute data into the packet. 
 *
 * Return:    Return the new offset
 */
U16 SdpStoreAttribData(
        BtRemoteDevice* remDev, BtPacket* packet, U16 offset, U16 final)
{
    U16 len;

    /* Load in attribute info */
    while (offset < final) {
        switch (remDev->sdpServInfo->attribState) {
        case 0:
            /* Create the the attibute list header in the scratch */
            remDev->sdpServInfo->attribScratch[0] = DETD_SEQ + DESD_ADD_16BITS;
            StoreBE16(remDev->sdpServInfo->attribScratch+1, 
                      (U16)(remDev->sdpServInfo->totalAttribSize - 3));
            remDev->sdpServInfo->scratchOff = 0;
            remDev->sdpServInfo->scratchLeft = 3;
            remDev->sdpServInfo->attribState = 2;
            break;

        case 1:
            /* Create the attribute ID and value header in the scratch area */
            remDev->sdpServInfo->attribScratch[0] = DETD_UINT + DESD_2BYTES;
            remDev->sdpServInfo->scratchOff = 0;

            if (remDev->sdpServInfo->sdpRecord->handleFlag & remDev->sdpServInfo->recMask) {
                /* We need to send the record handle attribute  so put
                 * the attibute ID and the value into the scratch area.
                 */
                Report(("SDP Server: StoreAttrib - storing ID 0 into scratch\n"));
                StoreBE16(remDev->sdpServInfo->attribScratch+1, 0);
                remDev->sdpServInfo->attribScratch[3] = DETD_UINT + DESD_4BYTES;
                StoreBE32(remDev->sdpServInfo->attribScratch+4, 
                          remDev->sdpServInfo->sdpRecord->handle);
                remDev->sdpServInfo->scratchLeft = 8;
                remDev->sdpServInfo->attribState = 3;
                remDev->sdpServInfo->sdpRecord->handleFlag &= ~remDev->sdpServInfo->recMask;
            } 
            else if (remDev->sdpServInfo->sdpRecord->attribs[0].flags &
                     remDev->sdpServInfo->recMask) {
                /* We need to send the service class Id list attribute */
                Report(("SDP Server: StoreAttrib - storing ID 1 into scratch\n"));
                remDev->sdpServInfo->sdpAttrib = 
                    SdpServFindMarkedAttribute(remDev->sdpServInfo->sdpRecord, 
                                               remDev->sdpServInfo->recMask);
                /* Make sure we have the first attribute */
                Assert(remDev->sdpServInfo->sdpAttrib == 
                       &(remDev->sdpServInfo->sdpRecord->attribs[0]));

                /* Store the attribute ID in the scratch area */
                StoreBE16(remDev->sdpServInfo->attribScratch+1, 
                          remDev->sdpServInfo->sdpAttrib->id);
                /* This had better be the service class Id list */
                Assert(remDev->sdpServInfo->sdpAttrib->id == 0x0001);

                remDev->sdpServInfo->scratchLeft = 3;
                remDev->sdpServInfo->attribState = 4;

                /* Clear the flag for this attribute */
                remDev->sdpServInfo->sdpRecord->attribs[0].flags &= 
                    ~remDev->sdpServInfo->recMask;

            }
            else if (remDev->sdpServInfo->sdpRecord->stateFlag & remDev->sdpServInfo->recMask) {
                /* We need to send the service record state attribute  so put
                 * the attibute ID and the value into the scratch area.
                 */
                Report(("SDP Server: StoreAttrib - storing ID 2 into scratch\n"));
                StoreBE16(remDev->sdpServInfo->attribScratch+1, 2);
                remDev->sdpServInfo->attribScratch[3] = DETD_UINT + DESD_4BYTES;
                StoreBE32(remDev->sdpServInfo->attribScratch+4, 
                          remDev->sdpServInfo->sdpRecord->recordState);
                remDev->sdpServInfo->scratchLeft = 8;
                remDev->sdpServInfo->attribState = 3;
                remDev->sdpServInfo->sdpRecord->stateFlag &= ~remDev->sdpServInfo->recMask;
            } 
            else {
                /* Find the next marked attribute */
                Report(("SDP Server: Attrib Search storing header into scratch\n"));
                remDev->sdpServInfo->sdpAttrib = 
                    SdpServFindMarkedAttribute(remDev->sdpServInfo->sdpRecord, 
                                               remDev->sdpServInfo->recMask);
                Assert(remDev->sdpServInfo->sdpAttrib != 0);

                /* Store the attribute ID in the scratch area */
                StoreBE16(remDev->sdpServInfo->attribScratch+1, 
                          remDev->sdpServInfo->sdpAttrib->id);
                remDev->sdpServInfo->scratchLeft = 3;
                remDev->sdpServInfo->attribState = 4;
            }
            break;

        case 2:
        case 3:
        case 4:
            /* Copy the scratch into the packet */
            Report(("SDP Server: Copying the scratch into a packet\n"));
            len = min(remDev->sdpServInfo->scratchLeft, final - offset);
            OS_MemCopy(packet->data+offset, 
                    remDev->sdpServInfo->attribScratch + 
                    remDev->sdpServInfo->scratchOff, (U16) len);
            offset += len;
            remDev->sdpServInfo->scratchLeft -= len;
            remDev->sdpServInfo->scratchOff +=  len;

            if (remDev->sdpServInfo->scratchLeft == 0) {
                /* All of the scratch has been stored so set the next state */
                if (remDev->sdpServInfo->attribState < 4) {
                    /* We just stored a whole attribute or the list header
                     * so get next attribute 
                     */
                    remDev->sdpServInfo->attribState = 1;
                } else {
                    /* Now we need to send the value portion */
                    remDev->sdpServInfo->attribState = 5;
                    remDev->sdpServInfo->curAttribTotal = 0;
                }
            }
            break;

        case 5:
            /* Copy the attribute value */
            if (remDev->sdpServInfo->curAttribTotal == 0) {
                /* This is the start so get the length of the value */
                remDev->sdpServInfo->curAttribTotal = 
                    (U16)SdpParseElement(remDev->sdpServInfo->sdpAttrib->value, 
                                         &(remDev->sdpServInfo->curAttribOff));
                remDev->sdpServInfo->curAttribTotal += remDev->sdpServInfo->curAttribOff;
                remDev->sdpServInfo->curAttribOff = 0;
            }

            /* Copy as much of the value as we can */
            len = min(remDev->sdpServInfo->curAttribTotal, final - offset);
            OS_MemCopy(packet->data+offset,
                remDev->sdpServInfo->sdpAttrib->value+ 
                remDev->sdpServInfo->curAttribOff, (U16)len);
            offset += len;
            remDev->sdpServInfo->curAttribTotal -= len;
            remDev->sdpServInfo->curAttribOff += len;

            if (remDev->sdpServInfo->curAttribTotal == 0) {
                /* We have copied the whole attribute so clear the attribute flag and
                 * get the next attribute 
                 */
                remDev->sdpServInfo->sdpAttrib->flags &= 
                    ~remDev->sdpServInfo->recMask;
                remDev->sdpServInfo->attribState = 1;
            }
            break;
        }
    }
    Assert(offset == final);
    return offset;
}


/*---------------------------------------------------------------------------
 *            SdpServFinishUpPacket()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Finish up sending the server packet. 
 *
 * Return:    void
 */
void SdpServFinishUpPacket(BtRemoteDevice* remDev, BtPacket* packet, I16 offset, I16 hOffset)
{
    BtStatus    status;

    /* Put in the continuation state */
    if (remDev->sdpServInfo->contState != 0) {
        packet->data[offset] = 1;
        offset++;
        packet->data[offset] = remDev->sdpServInfo->contState;
        Report(("SDP Server: Sending cont state = %d\n", remDev->sdpServInfo->contState));
    } else {
        packet->data[offset] = 0;
        Report(("SDP Server: Sending cont state 0\n"));
    }

    /* Store the parameter size */
    StoreBE16((packet->header+hOffset+3), (U16)(offset+1));

    /* Update the sizes */
    packet->headerLen = 5;
    packet->dataLen = (U16)(offset+1);
                
    /* Call L2CAP to send the data */
    status = L2CAP_SendData(remDev->sdpServInfo->channel, packet);
    
    if (status != BT_STATUS_PENDING) {
        /* The send failed.  Clear the txPacketBusy flag, since the 
         * packet will never complete.
         */
        remDev->sdpServInfo->txPacketBusy = FALSE;
        Report(("SDP Server: L2CAP_SendData call failed. error = %x\n", status));
    }
}

/*---------------------------------------------------------------------------
 *            SdpServHandleMarkedRecord()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  If this record or its attributes are marked then it may be
 *            part of an ongoing query where the result did not fit in
 *            a single L2CAP packet. This means we should set the 
 *            continuation state of all connections whose mark appears in the
 *            record to 0. This will cause future queries involving this record
 *            fail gracefully. 
 *
 * Return:    void
 */
void SdpHandleMarkedRecord(SdpRecord* record)
{
    I8              i;
    U16             mask;
    BtRemoteDevice* remDev;

    /* Go through the table of remote devices if their mark appears in the record
     * or its attributes then set its continuation state to 0
     */
    for (i = 0; (remDev = MeEnumerateRemoteDevices(i)) != 0; i++) {
        Assert(remDev != 0);

        if (remDev->sdpServInfo != 0) {
            mask = remDev->sdpServInfo->recMask;

            if ((record->flags & mask) || (record->handleFlag & mask) ||
                (record->stateFlag & mask) || 
                (SdpServFindMarkedAttribute(record, mask) != 0)) {

                /* The record is marked so set the contuation state to 0 */
                Report(("SdpServer: Removed record is marked setting cont state to 0 for remdev %d\n",i));
                remDev->sdpServInfo->contState = 0;
            }
        } 
    }
}


/*---------------------------------------------------------------------------
 *            SdpServGetInfo()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Get a free SdpServerInfo structure. Return the pointer if one
 *            is found otherwise return 0. 
 *
 * Return:    SdpServerInfo*
 */
SdpServerInfo* SdpServGetInfo(void)
{
    I8             i;
    SdpServerInfo  *info;
    BtRemoteDevice *remDev;
    BtRemoteDevice *saveRemDev;

    info = 0;
    for (i = 0; i < SDP_ACTIVE_CLIENTS; i++) {
        if (SDPS(serverInfos)[i].busy == FALSE) {
            info = &(SDPS(serverInfos)[i]);
            Assert(info->txPacketBusy == FALSE);
            info->txPacketBusy = FALSE;
            goto done;
        }
    }

    /* There are no free ones so we need to find a parked device
     * and take his.
     */
    saveRemDev = 0;
    for (i = 0; (remDev = MeEnumerateRemoteDevices(i)) != 0; i++) {
        if ((remDev->sdpServInfo != 0) && (remDev->state == BDS_CONNECTED) &&
            (remDev->mode == BLM_PARK_MODE)) {
            /* We have candidate. The one we want will have its contState 
             * set to 0. If we can't find one of those then we will take
             * any we can find.
             */
            if (remDev->sdpServInfo->contState == 0) {
                info = remDev->sdpServInfo;
                remDev->sdpServInfo = 0;
                goto done;
            } else {
                /* This is not exactly what we want but we will save it just
                 * in case we can't find something better
                 */
                saveRemDev = remDev;
            }
        }
    }
    /* See if we have to use the last resort */
    if (saveRemDev != 0) {
        info = saveRemDev->sdpServInfo;
        saveRemDev->sdpServInfo = 0;
    }

done:
    if (info != 0) {
        info->busy = TRUE;
        info->contState = 0;
    }
    return info;
     
}

/*---------------------------------------------------------------------------
 *            SdpServUpdateClassOfDevice()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Update the class of device field and if a change
 *            has occured then write the value out.
 *
 * Return:    SdpRecord*
 */
void SdpServUpdateClassOfDevice(void)
{
    SdpRecord* record;
    BtClassOfDevice saveCod;

    saveCod = (bt.me.sdpServCoD);

    /* Preserve the Limited Discoverable state */
    (bt.me.sdpServCoD) = saveCod & COD_LIMITED_DISC_MASK;
    record = (SdpRecord*) SDPS(records).Flink;
    while ((ListEntry*)record != &SDPS(records)) {
        (bt.me.sdpServCoD) |= record->classOfDevice;
        /* Advance to the next record */
        record = (SdpRecord*) record->node.Flink;
    }
    if ((bt.me.sdpServCoD) != saveCod) {
        MeWriteClassOfDevice();
    }

}

#if XA_ERROR_CHECK == XA_ENABLED || XA_DEBUG == XA_ENABLED

/*---------------------------------------------------------------------------
 *            SdpServVerifyRecord()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Verify that the SDP record is properly formed. Verify that
 *            there are no duplicate Attribute IDs and that the attributes
 *            are in ascending order. Verify the values are properly formed.
 *
 * Return:    TRUE if valid otherwise FALSE.
 */
BOOL SdpServVerifyRecord(SdpRecord* record)
{
    U8    i;
    U16   lastId;

    /* There must be at least one attribute */
    if (record->num == 0) {
        return FALSE;
    }

    /* Since the ID *must* be in order and the AID_SERVICE_CLASS_ID_LIST
       must be in the record it must be in the first slot. Bug 884 
     */
    if (record->attribs[0].id != AID_SERVICE_CLASS_ID_LIST) {
        return FALSE;
    }

    /* Check each attribute */
    lastId = 0;
    for (i = 0; i < record->num; i++) {
        if (record->attribs[i].id == 2) {
            /* SDP Server automatically handles Service Record 
             * State attribute (0x0002)
             */
            return FALSE;
        }
        if (record->attribs[i].id <= lastId) {
            /* Found a duplicate attribute, out of order attribute ID,
             * or the Service Record Handle attribute (0x0000)*/
            return FALSE;
        }
        lastId = record->attribs[i].id;

        if (!SdpVerifyDataElement(record->attribs[i].value,
                                  record->attribs[i].len,
                                  SDP_SERVER_MAX_LEVEL)) {
            return FALSE;
        }
    }
    return TRUE;
}
#endif /* XA_ERROR_CHECK || XA_DEBUG */

/*---------------------------------------------------------------------------
 *            SdpUpdateServiceDatabaseState()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Updates the SDP Server's Service Database State attribute.  
 *            Modification of this attribute indicates that an SDP record
 *            has been added or removed.
 *
 * Return:    BT_STATUS_SUCCESS if success otherwise BT_STATUS_FAILED.
 */
BtStatus SdpUpdateServiceDatabaseState(void)
{
    BtStatus        status;

    if (!IsNodeOnList(&SDPS(records), &(SDPS(record0).node))) {
        status = BT_STATUS_FAILED;
    } else {
        RemoveEntryList(&(SDPS(record0).node));
 
        /* Store a new value into the ServiceDatabaseState
         * Use OS_GetSystemTime() to prevent rollover problems 
         */
        StoreBE32(SDPS(ZeroServiceDatabaseState+1), OS_GetSystemTime());
        
        /* Add back the Sevice Discovery entry (Record Zero) to the 
         * SDP server 
         */
        InsertTailList(&(SDPS(records)), &(SDPS(record0).node)); 
        status = BT_STATUS_SUCCESS;
    }
    return status;
}
#endif /* SDP_SERVER_SUPPORT == XA_ENABLED */
