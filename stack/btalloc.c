/***************************************************************************
 *
 * File:
 *     $Workfile:btalloc.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:23$
 *
 * Description:
 *     Contains the allocation for all the memory used in the
 *     the Bluetooth stack (both ROM and RAM).
 *
 * Created:
 *     October 19, 1999
 *
 * Copyright 1999-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "btalloc.h"


/****************************************************************************
 *
 * RAM Data
 *
 ****************************************************************************/

/* The stack context
 */
#if XA_CONTEXT_PTR == XA_DISABLED
/* The stack context is a global structure */
BtContext bt;
#else /* XA_CONTEXT_PTR == XA_DISABLED */
/* The stack context is accessed through a pointer. Normally in this case
 * the stack context would be dynamically allocated. For testing purposes
 * the pointer points to a global structure.
 */
BtContext  btTmp;
BtContext *bt = &btTmp;
#endif /* XA_CONTEXT_PTR */

/*---------------------------------------------------------------------------
 *            BtInitMemory()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Intialize the memory used by the Bluetooth stack. This may
 *            include allocating the memory.
 *
 * Return:    TRUE if success otherwise FALSE. 
 */
BOOL BtInitMemory(void)
{
    U8* ptr; 

    /* Fill memory with 0. */
#if XA_CONTEXT_PTR == XA_ENABLED
    ptr = (U8*)bt;
#else
    ptr = (U8*)&bt;
#endif /* XA_CONTEXT_PTR */

    OS_MemSet(ptr, 0, (U32)sizeof(BtContext));

    return TRUE;
}

