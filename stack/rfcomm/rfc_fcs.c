/***************************************************************************
 *
 * File:
 *     $Workfile:rfc_fcs.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:27$
 *
 * Description:
 *     This file contains code to generate and check FCS.
 *
 * Created:
 *     Sept 13, 1999
 *
 * Copyright 1999-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "btrom.h"

#if RFCOMM_PROTOCOL == XA_ENABLED

#include "sys/rfc.h"

/*---------------------------------------------------------------------------
 *            RfCalcFCS()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Calculate the Frame Check Sequence.
 *
 * Return:    
 */
U8 RfCalcFCS(U8 *Buffer, U8 len)
{
    U8 FCS=0xFF;

    while (len--) {
        FCS = crctable[FCS ^ *Buffer++];
    }

    FCS = 0xFF - FCS;

    return FCS;
}

/*---------------------------------------------------------------------------
 *            RfIsValidFCS()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Test the validity of the Frame Check Sequence.
 *
 * Return:    
 */
BOOL RfIsValidFCS(U8 *Buffer, U8 len, U8 RxFCS)
{
    U8 FCS = 0xFF;

    while (len--) {
        FCS = crctable[FCS ^ *Buffer++];
    }

    FCS=crctable[FCS ^ RxFCS];

    if (FCS == 0xCF) {
        return TRUE;
    } else {
        return FALSE;
    }
}

#endif /* RFCOMM_PROTOCOL == XA_ENABLED */

