/***************************************************************************
 *
 * File:
 *     $Workfile:rfc_ctl.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:118$
 *
 * Description:
 *     This file contains the control channel code for the 
 *     RFCOMM protocol.
 *
 * Created:
 *     Jan 10, 2000
 *
 * Copyright 1999-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "btalloc.h"

#if RFCOMM_PROTOCOL == XA_ENABLED

/* Function prototypes */       
static BtStatus RfSendNSC(U8 MuxId, U8 mType);
static void RfCtlParseParms(U8 MuxId, U8 *mType, U8 **parms, U16 *length);

/*---------------------------------------------------------------------------
 *            RfCtlHandlePNReq()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handle port negotiation request.
 *
 * Return:    
 */
static void RfCtlHandlePNReq(U8 MuxId, U8 DlcId, U8 *Parms)
{
    RfChannel *channel;
    U16        maxFrameSize;
#if NUM_RF_SERVERS != 0
    U8         serverId;
#endif /* NUM_RF_SERVERS != 0 */

    /* A Port negotiation was received */
    if (DlcId == INVALID_DLC_ID) {
#if NUM_RF_SERVERS != 0
        /* This request is for a new channel */
        serverId = (U8) (Parms[0] >> 1);
        if (RfGetFreeServiceChannel(MuxId, serverId, &channel) != BT_STATUS_SUCCESS) 
#endif /* NUM_RF_SERVERS != 0 */
        {
            /* No registered service */
            AssertEval(RfSendAck(DM_F, MuxId, Parms[0]) == BT_STATUS_PENDING);  
            return;
        }
        channel->dlci = Parms[0];
        channel->muxId = MuxId;
    } else {
        /* This request is for an existing channel */
        channel = DLC(MuxId,DlcId).channel;
    }
    
    if ((channel->flags & CHNL_FLAG_USE_PENDING) ||
        !(MUX(MuxId).flags & MUX_FLAG_CREDIT_FLOW)) {
        /* This PN request has either been received before connection 
         * establishment, or this connection does not support credit flow
         * control.  In the latter case, we don't re-negotiate after the 
         * connection is up).
         */
        if (Parms[2] < 64) {
            channel->priority = Parms[2];
        } else {
            channel->priority = 0;
        }

        maxFrameSize = (U16)(Parms[4] | (Parms[5] << 8));
        if ((maxFrameSize >= RF_MIN_FRAME_SIZE) && 
            (maxFrameSize <= channel->maxFrameSize)) {
            /* The frame size is valid, accept it */ 
            channel->maxFrameSize = maxFrameSize;
        } else {
            channel->maxFrameSize = channel->maxFrameSize;
        }

        if ((Parms[1] & 0xF0) == CREDIT_FLOW_REQ) {
            /* Credit flow control has been requested.
             * If this is not the first DLC being established, and negotiation
             * has not already been successful, this must be rejected.
             */
#if UPF_TWEAKS == XA_ENABLED
            /* For testing, if CHNL_FLAG_NO_CFC is set, then credit
             * flow control is effectively disabled.
             */
            if (((MUX(MuxId).flags & MUX_FLAG_CREDIT_FLOW) ||
                 (MUX(MuxId).dlcCount < 1)) &&
                !(channel->flags & CHNL_FLAG_NO_CFC))  {
#else
            if ((MUX(MuxId).flags & MUX_FLAG_CREDIT_FLOW) ||
                 MUX(MuxId).dlcCount < 1) {
#endif /* UPF_TWEAKS == XA_ENABLED */
                /* Accept the credit flow control */
                MUX(MuxId).flags |= MUX_FLAG_CREDIT_FLOW;
                channel->txCredit = (U16)(Parms[7] & 0x07);
                goto RF_SEND_PN;
            }
        }

        /* Credit Flow Control is not in effect, 
         * set the initial flow state.
         */
        if (!(MUX(MuxId).flags & MUX_FLAG_CREDIT_FLOW)) {
            if (channel->rxCredit) {
                channel->rfSignals &= ~RF_FLOW;
            } else {
                channel->rfSignals |= RF_FLOW;
            }
        }
    }


RF_SEND_PN:

    channel->flags |= CHNL_FLAG_NEG_DONE;
    AssertEval(RfSendParmNegotiation(channel, 0) == BT_STATUS_PENDING);
}

/*---------------------------------------------------------------------------
 *            RfCtlHandlePNCnf()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Process a port negotiation response.
 *
 * Return:    
 */
static void RfCtlHandlePNCnf(U8 MuxId, U8 DlcId, U8 *Parms)
{
    if (DlcId != INVALID_DLC_ID) {
        if (DLC(MuxId,DlcId).flags & DLC_FLAG_REQ_SENT) {

            EVM_CancelTimer(&(DLC(MuxId,DlcId).channel->timer));
            DLC(MuxId,DlcId).flags &= ~DLC_FLAG_REQ_SENT;
            RfCheckForCreditToSend(DLC(MuxId,DlcId).channel);

            if (DLC(MuxId,DlcId).flags & DLC_FLAG_CREDIT_FLOW) {
                /* We requested credit flow control */
                if ((Parms[1] & 0xF0) != CREDIT_FLOW_RSP) {
                    /* The other end does not support credit based flow ctrl */
                    if ((MUX(MuxId).flags & MUX_FLAG_CREDIT_FLOW) &&
                        MUX(MuxId).dlcCount) {
                        /* Once flow control has been negotiated, it
                         * cannot be rejected.
                         */
                        Report(("RFCOMM:  Remote tried to renegotiate flow control, disconnecting L2CAP\n"));
                        RfDisconnectL2CAP(MuxId);                        
                        return;
                    }

                    DLC(MuxId,DlcId).flags &= ~DLC_FLAG_CREDIT_FLOW;

                    /* Set the initial flow state */
                    DLC(MuxId,DlcId).channel->rxCredit = DLC(MuxId,DlcId).channel->initialRxCredit;
                    if (DLC(MuxId,DlcId).channel->rxCredit) {
                        DLC(MuxId,DlcId).channel->rfSignals &= ~RF_FLOW;
                    } else {
                        DLC(MuxId,DlcId).channel->rfSignals |= RF_FLOW;
                    }
                } else {
                    /* The remote device supports credit flow control */
                    DLC(MuxId,DlcId).channel->txCredit = (U16)(Parms[7] & 0x07);
                    MUX(MuxId).flags |= MUX_FLAG_CREDIT_FLOW;
                }
            }
  
            /* Validate the negotiated parameters */
            if (((Parms[4] | (Parms[5] << 8)) <= 
                 DLC(MuxId,DlcId).channel->maxFrameSize) &&
                ((Parms[4] | (Parms[5] << 8)) >= 
                 RF_MIN_FRAME_SIZE)) {

                /* Parms are OK, advance the state */
                DLC(MuxId,DlcId).channel->maxFrameSize = (U16)(Parms[4] | (Parms[5] << 8));
                RfAdvanceNegPending(DLC(MuxId,DlcId).channel, TRUE);
            } else {
                /* Invalid frame size received.  An attempt is made here to
                 * continue with the connection, by using a frame size no larger
                 * than the default RFCOMM frame size.
                 */
                DLC(MuxId,DlcId).channel->maxFrameSize = (U16)
                    min(DLC(MuxId,DlcId).channel->maxFrameSize,
                        RF_DEFAULT_FRAMESIZE);
                RfAdvanceNegPending(DLC(MuxId,DlcId).channel, TRUE);
            }
        }
    } else {
        /* No valid DLC found */
        AssertEval(RfSendAck(DM_F, MuxId, Parms[0]) == BT_STATUS_PENDING);
    }
}

/*---------------------------------------------------------------------------
 *            RfCtlHandleMSCReq()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Process an modem status request.
 *
 * Return:    
 */
static void RfCtlHandleMSCReq(U8 MuxId, U8 DlcId, U8 *Parms, U16 Length)
{
    RfModemStatus modemStatus;
    U8 breakLen = 0;
    BtStatus status;

    /* Respond (if possible) with the same values that were received */
    status = RfSendModemStatus(DLC(MuxId,DlcId).channel, 
                                 Parms[1], breakLen, 0);
    if (status != BT_STATUS_PENDING) return;

    modemStatus.signals = (RfSignals)(Parms[1] & 0xCE);
    /* Set the remote status */
    if (!(Parms[1] & EA_BIT) && (Length > 2) && (Parms[2] & 0x02)) {
        /* A break field was received */
        modemStatus.breakLen = (U8)(Parms[2] >> 4);
    } else {
        modemStatus.breakLen = 0;
    }

    breakLen = modemStatus.breakLen;
    DLC(MuxId,DlcId).flags |= DLC_FLAG_MSC_RCVD;

    if (!(MUX(MuxId).flags & MUX_FLAG_CREDIT_FLOW)) {
        if ((modemStatus.signals & RF_FLOW))
        {
            /* Flow control is off */
            DLC(MuxId,DlcId).flags |= DLC_FLAG_FCOFF;
        } else {
            /* If flow is back on, send any data on this DLC */
            DLC(MuxId,DlcId).flags &= ~DLC_FLAG_FCOFF;
            RfMUXCheckTxQueues(MuxId);
        }
    } else {
        RfMUXCheckTxQueues(MuxId);
    }

    /* Indicate status to the application */
    RfAppCallback(DLC(MuxId,DlcId).channel,
                  BT_STATUS_SUCCESS, RFEVENT_MODEM_STATUS_IND,
                  &modemStatus);
}

/*---------------------------------------------------------------------------
 *            RfCtlHandleRPNReq()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Process a remote port negotiation request.
 *
 * Return:    
 */
static void RfCtlHandleRPNReq(U8 MuxId, U8 DlcId, U8 *Parms, U16 length)
{
    RfPortSettings  portSettings;
    RfChannel      *channel;

#if NUM_RF_SERVERS != 0
    U8              serverId;
#endif /* NUM_RF_SERVERS != 0 */

    OS_MemSet((U8 *)&portSettings, 0, sizeof(RfPortSettings));
    if (length > 1) {
        /* This is a request to set port settings */
        portSettings.baudRate = Parms[1];
        portSettings.dataFormat = Parms[2];
        portSettings.flowControl = Parms[3];
        portSettings.xonChar = Parms[4];
        portSettings.xoffChar = Parms[5];
        portSettings.parmMask = LEtoHost16(&(Parms[6]));
    } else {
        /* This is a request to get port settings */
        portSettings.baudRate = RF_BAUD_9600;
        portSettings.dataFormat = RF_DATA_BITS_8 | RF_STOP_BITS_1 | RF_PARITY_NONE;
        portSettings.parmMask = RF_PARM_BAUDRATE | RF_PARM_DATA_FORMAT;
    }

    /* Process the request */
    if (DlcId == INVALID_DLC_ID) {
        /* This request is for a new channel */
#if NUM_RF_SERVERS != 0
        serverId = (U8)(Parms[0] >> 3);
        if (RfGetFreeServiceChannel(MuxId, serverId, &channel) != BT_STATUS_SUCCESS)
#endif /* NUM_RF_SERVERS != 0 */
        {
            /* No registered service found */
            AssertEval(RfSendAck(DM_F, MuxId, (U8)(Parms[0] >> 2)) == 
                       BT_STATUS_PENDING);
            return;
        }
#if NUM_RF_SERVERS != 0
        channel->dlci = (U8)(Parms[0] >> 2);
        channel->muxId = MuxId;
        AssertEval(RfSendRemotePortNeg(channel, &portSettings, 0) ==
                   BT_STATUS_PENDING);
        return;
#endif /* NUM_RF_SERVERS != 0 */
    } else {
        /* This is an existing channel */
        channel = DLC(MuxId,DlcId).channel;
    }

    DLC(MuxId,DlcId).flags |= DLC_FLAG_REQ_INCOMING;

    if (length > 1) {
        /* A Remote Port Negotiation was received */
        if (DLC(channel->muxId,channel->dlcId).flags & DLC_FLAG_REQ_SENT) {
            /* We have a request outstanding, refuse this request */
            portSettings.parmMask = 0;
            AssertEval(RfSendRemotePortNeg(channel, &portSettings, 0) ==
                       BT_STATUS_PENDING);
            return;
        }

#if RF_SEND_CONTROL == XA_ENABLED
        RfAppCallback(channel, BT_STATUS_SUCCESS, RFEVENT_PORT_NEG_IND,
                      &portSettings);
    } else {
        /* This is a request for port settings */
        RfAppCallback(channel, BT_STATUS_SUCCESS, RFEVENT_PORT_STATUS_IND,
                      0);
#endif /* RF_SEND_CONTROL == XA_ENABLED */
    }

    if ((DLC(MuxId,DlcId).flags & DLC_FLAG_REQ_INCOMING) &&
        !(DLC(MuxId,DlcId).channel->flags & CHNL_FLAG_RSP_DELAYED)) {
        /* The app didn't respond, so respond for it (accept settings) */
        DLC(MuxId,DlcId).flags &= ~DLC_FLAG_REQ_INCOMING;
        AssertEval(RfSendRemotePortNeg(channel, &portSettings, 0) ==
                   BT_STATUS_PENDING);
    }
}

#if RF_SEND_CONTROL == XA_ENABLED
/*---------------------------------------------------------------------------
 *            RfCtlHandleRPNCnf()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Process a remote port negotiation confirmation.
 *
 * Return:    
 */
static void RfCtlHandleRPNCnf(U8 MuxId, U8 DlcId, U8 *Parms)
{
    RfPortSettings  portSettings;

    if (DlcId != INVALID_DLC_ID) {
        if (DLC(MuxId,DlcId).flags & DLC_FLAG_REQ_SENT) {

            portSettings.baudRate = Parms[1];
            portSettings.dataFormat = Parms[2];
            portSettings.flowControl = Parms[3];
            portSettings.xonChar = Parms[4];
            portSettings.xoffChar = Parms[5];
            portSettings.parmMask = LEtoHost16(&(Parms[6]));

            EVM_CancelTimer(&(DLC(MuxId,DlcId).channel->timer));
            DLC(MuxId,DlcId).flags &= ~DLC_FLAG_REQ_SENT;
            RfCheckForCreditToSend(DLC(MuxId,DlcId).channel);
            
            /* Notify app of confirmation */
            if (DLC(MuxId,DlcId).flags & DLC_FLAG_RPN_STATUS) {
                DLC(MuxId,DlcId).flags &= ~DLC_FLAG_RPN_STATUS;
                RfAppCallback(DLC(MuxId,DlcId).channel, BT_STATUS_SUCCESS, 
                              RFEVENT_PORT_STATUS_CNF, &portSettings);
            } else {
                RfAppCallback(DLC(MuxId,DlcId).channel, BT_STATUS_SUCCESS, 
                              RFEVENT_PORT_NEG_CNF, &portSettings);
            }
        }
    } else {
        /* No valid DLC found */
        AssertEval(RfSendAck(DM_F, MuxId, (U8)(Parms[0] >> 2)) == 
                   BT_STATUS_PENDING);
    }
}
#endif /* RF_SEND_CONTROL == XA_ENABLED */

/*---------------------------------------------------------------------------
 *            RfCtlHandleRLSReq()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Process a remote line status request.
 *
 * Return:    
 */
static void RfCtlHandleRLSReq(U8 MuxId, U8 DlcId, U8 *Parms)
{
#if RF_SEND_CONTROL == XA_ENABLED
    RfLineStatus lineStatus;
#else
    UNUSED_PARAMETER(DlcId);
#endif /* RF_SEND_CONTROL == XA_DISABLED */

    /* Respond with the same values that were received */
    AssertEval(RfSendLineStatus(DLC(MuxId,DlcId).channel, Parms[1], 0) == 
               BT_STATUS_PENDING);

#if RF_SEND_CONTROL == XA_ENABLED
    /* Set the Remote line Status */
    lineStatus = (RfLineStatus)(Parms[1] & 0x0E);

    /* Indicate status to the application */
    RfAppCallback(DLC(MuxId,DlcId).channel, 
                 BT_STATUS_SUCCESS, RFEVENT_LINE_STATUS_IND,
                 &lineStatus);
#endif /* RF_SEND_CONTROL == XA_ENABLED */
}

/*---------------------------------------------------------------------------
 *            RfCtlHandleAggregateFlow()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Process the FCON/FCOFF commands.
 *
 * Return:    
 */
static void RfCtlHandleAggregateFlow(U8 MuxId, U8 mType)
{
#if RF_SEND_CONTROL == XA_ENABLED || UPF_TWEAKS == XA_ENABLED
    U8 event;
    I8 i;
#endif /* RF_SEND_CONTROL == XA_ENABLED */

    if (MUX(MuxId).flags & MUX_FLAG_CREDIT_FLOW) {
        Report(("RFCOMM:  Invalid flow control command received\n"));
        AssertEval(RfSendNSC(MuxId, mType) == BT_STATUS_PENDING);
        return;
    }

    if (mType & CR_BIT) {
        /* Set the aggregate flow control */
        if ((mType & 0xFD) == FCOFF) {
            MUX(MuxId).flags |= MUX_FLAG_FCOFF;
            AssertEval(RfSendAggregateFlow(MuxId, FALSE, 0) == 
                       BT_STATUS_PENDING);
#if RF_SEND_CONTROL == XA_ENABLED || UPF_TWEAKS == XA_ENABLED
            event = RFEVENT_FLOW_OFF_IND;
#endif /* RF_SEND_CONTROL == XA_ENABLED */
        } else {
            MUX(MuxId).flags &= ~MUX_FLAG_FCOFF;
            AssertEval(RfSendAggregateFlow(MuxId, TRUE, 0) ==
                       BT_STATUS_PENDING);
            RfMUXCheckTxQueues(MuxId);
#if RF_SEND_CONTROL == XA_ENABLED || UPF_TWEAKS == XA_ENABLED
            event = RFEVENT_FLOW_ON_IND;
#endif /* RF_SEND_CONTROL == XA_ENABLED */
        }


#if RF_SEND_CONTROL == XA_ENABLED
        for (i = 0; i < NUM_RF_CHANNELS; i++) {
            if (DLC(MuxId,i).flags & DLC_FLAG_ALLOCATED) {
                RfAppCallback(DLC(MuxId,i).channel, BT_STATUS_SUCCESS, event, 0);
            }
        }
#endif /* RF_SEND_CONTROL == XA_ENABLED */
    } 

/* Added for unplugFest testing */
#if UPF_TWEAKS == XA_ENABLED
    else {
        if ((mType & 0xFD) == FCOFF) {
            event = RFEVENT_FLOW_OFF_CNF;
        } else {
            event = RFEVENT_FLOW_ON_CNF;
        }

        for (i = 0; i < NUM_RF_CHANNELS; i++) {
            if ((DLC(MuxId,i).flags & DLC_FLAG_ALLOCATED) &&
                (DLC(MuxId,i).flags |= DLC_FLAG_REQ_SENT)) {

                /* Notify the app */
                EVM_CancelTimer(&(DLC(MuxId,i).channel->timer));
                RfAppCallback(DLC(MuxId,i).channel, BT_STATUS_SUCCESS, event, 0);
            }
        }
    }
#endif /* UPF_TWEAKS == XA_ENABLED */
}

/*---------------------------------------------------------------------------
 *            RfCtlHandleTEST()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Process the TEST command.
 *
 * Return:    
 */
static void RfCtlHandleTEST(U8 MuxId, U8 *Parms, U16 Len, U8 CR)
{
#if RF_SEND_TEST == XA_ENABLED
    U8        testPattern;
#endif /* RF_SEND_TEST == XA_ENABLED */

    if (CR) {
        /* Respond to the test */
        if (!(MUX(MuxId).flags & MUX_FLAG_TEST_RECV)) {
            OS_MemCopy(&(MUX(MuxId).testRspData[2]), Parms, 
                       (U16)min(Len, RF_DEFAULT_FRAMESIZE));
            AssertEval(RfSendTest(MuxId, (U8)Len, 0) == 
                       BT_STATUS_PENDING);
        }
#if XA_DEBUG == XA_ENABLED
        else {
            Assert(0);
        }
#endif /* XA_DEBUG == XA_ENABLED */
    } 
#if RF_SEND_TEST == XA_ENABLED
    else {
        /* Validate the response */
        if (DLC(MuxId,MUX(MuxId).testDlcId).flags & DLC_FLAG_REQ_SENT) {
            EVM_CancelTimer(&(DLC(MuxId,MUX(MuxId).testDlcId).channel->timer));
            MUX(MuxId).flags &= ~MUX_FLAG_TEST_SENT;
            DLC(MuxId,MUX(MuxId).testDlcId).flags &= ~DLC_FLAG_REQ_SENT;
            RfCheckForCreditToSend(DLC(MuxId,MUX(MuxId).testDlcId).channel);

            testPattern = TEST_PATTERN;
            if (OS_MemCmp(Parms, Len, &testPattern, 1) == TRUE) {
                /* Contents compare OK */
                RfAppCallback(DLC(MuxId,MUX(MuxId).testDlcId).channel, 
                            BT_STATUS_SUCCESS, RFEVENT_TEST_CNF,
                            0);
            } else {
                /* Contents are different */
                RfAppCallback(DLC(MuxId,MUX(MuxId).testDlcId).channel, 
                            BT_STATUS_FAILED, RFEVENT_TEST_CNF,
                            0);
            }
        }
    }
#endif /* RF_SEND_TEST == XA_ENABLED */
}

/*---------------------------------------------------------------------------
 *            RfCtlHandleNSC()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Process the NSC command.
 *
 * Return:    
 */
static void RfCtlHandleNSC(U8 MuxId, U8 mType)
{
    I8      i;
    RfEvent event = 0;

    /* Find the first command sent, that matches this response */
    for (i = 0; i < NUM_RF_CHANNELS; i++) {
        if ((DLC(MuxId,i).flags & DLC_FLAG_REQ_SENT) &&
            (DLC(MuxId,i).channel->cmdSent == mType)) {
            /* Found the connection that sent the request, set up
             * the appropriate response callback
             */
            EVM_CancelTimer(&(DLC(MuxId,i).channel->timer));
            switch (mType & 0xFD) {
            case PN:
                RfAdvanceNegPending(DLC(MuxId,i).channel, FALSE);
                break;
#if RF_SEND_TEST == XA_ENABLED
            case TEST:
                event = RFEVENT_TEST_CNF;
                break;
#endif /* RF_SEND_TEST == XA_ENABLED */
#if RF_SEND_CONTROL == XA_ENABLED
            case MSC:
                event = RFEVENT_MODEM_STATUS_CNF;
                break;
            case RLS:
                event = RFEVENT_LINE_STATUS_CNF;
                break;
            case RPN:
                event = RFEVENT_PORT_NEG_CNF;
                break;
#endif /* RF_SEND_CONTROL == XA_ENABLED */
#if UPF_TWEAKS == XA_ENABLED
            case FCON:
                event = RFEVENT_FLOW_ON_CNF;
                break;
            case FCOFF:
                event = RFEVENT_FLOW_OFF_CNF;
                break;
#endif /* UPF_TWEAKS == XA_ENABLED */

#if UPF_TWEAKS == XA_ENABLED
            case INVALID_CMD:
                event = RFEVENT_INVALID_CMD_CNF;
                break;
#endif /* UPF_TWEAKS == XA_ENABLED */
            }

            if (event != 0) {
                RfAppCallback(DLC(MuxId,i).channel, BT_STATUS_FAILED, event, 0);
            }

            /* Stop looking */
            break;
        }
    }
}

/*---------------------------------------------------------------------------
 *            RfCtlHandleCnf()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Process various confirmations.
 *
 * Return:    
 */
static void RfCtlHandleCnf(U8 MuxId, U8 DlcId, RfEvent Event)
{
    if ((DLC(MuxId,DlcId).flags & DLC_FLAG_MSC_PENDING) &&
        (Event == RFEVENT_MODEM_STATUS_CNF)) {
        EVM_CancelTimer(&(DLC(MuxId,DlcId).channel->timer));
        DLC(MuxId,DlcId).flags &= ~DLC_FLAG_MSC_PENDING;
        RfCheckForCreditToSend(DLC(MuxId,DlcId).channel);
        RfMUXCheckTxQueues(MuxId);
    } else if (DLC(MuxId,DlcId).flags & DLC_FLAG_REQ_SENT) {

        EVM_CancelTimer(&(DLC(MuxId,DlcId).channel->timer));
        DLC(MuxId,DlcId).flags &= ~DLC_FLAG_REQ_SENT;
        RfCheckForCreditToSend(DLC(MuxId,DlcId).channel);

        /* Notify app of confirmation */
        RfAppCallback(DLC(MuxId,DlcId).channel, 
                      BT_STATUS_SUCCESS, Event, 0);

    }
}

/*---------------------------------------------------------------------------
 *            RfCtlParseParms()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Parser for multiplexer control channel
 *
 * Return:    
 */
static void RfCtlParseParms(U8 MuxId, U8 *mType, U8 **parms, U16 *length)
{
    U8        headerLen;

    /* Check for a valid length, the control channel cannot handle frames
     * larger than the default frame size
     */
    if (MUX(MuxId).rxLen > RF_DEFAULT_FRAMESIZE) {
        /* Invalid RX length, disconnect */
        Report(("RFCOMM:  Invalid length, disconnecting L2CAP\n"));
        RfDisconnectL2CAP(MuxId);
        return;
    }

    /* Find the first part of the data payload */
    if ((U8)MUX(MuxId).rxBuff[2] & 0x01) {
        /* 1 Byte Length */
        headerLen = 3;
    } else {
        /* 2 Byte Length */
        headerLen = 4;
    }                 

    /* Get the message type */
    *mType = MUX(MuxId).rxBuff[headerLen];
    *parms = MUX(MuxId).rxBuff + headerLen;

    /* Find the length */
    if ((*parms)[1] & 0x01) {
        /* 1 Byte Length */
        *length = (*parms)[1] >> 1;
        headerLen = 2;
    } else {
        /* 2 Byte Length */
        *length = ((*parms)[1] >> 1) | 
                  ((*parms)[2] << 7);
        headerLen = 3;
    }

    /* Set a pointer to the parameter section */
    *parms += headerLen;

    /* Validate the length of the parameters */
    if (*length > (MUX(MuxId).rxLen - headerLen)) {
        /* Invalid parm length, disconnect */
        Report(("RFCOMM:  Invalid parm length, disconnecting L2CAP\n"));
        RfDisconnectL2CAP(MuxId);
        return;
    }
}

/*---------------------------------------------------------------------------
 *            RfControlChannel()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handles multiplexer control channel commands.
 *
 * Return:    
 */
void RfControlChannel(U8 MuxId, U8 Event)
{
    U8        dlcId;
    U8        mType;
    U16       length;
    U8       *parms;
    
    if (Event == UIH) {
        
        /* Get the parm info */
        RfCtlParseParms(MuxId, &mType, &parms, &length);

        switch (mType & 0xFD) {
        case TEST:
            /* Test command */
            RfCtlHandleTEST(MuxId, parms, length, (U8)(mType & CR_BIT));
            break;

        case FCON:
        case FCOFF:
            /* Aggregate flow control */
            RfCtlHandleAggregateFlow(MuxId, mType);
            break;


        case PN:
            /* Parameter Negotiation. 
             * Get the DLC ID, it is OK to get invalid DLC ID 
             * Since an PN command may be received before the
             * channel is actually up.
             */
            (void)RfFindDlcIdByDlci(MuxId, parms[0], &dlcId);
            if (mType & CR_BIT) {
                /* Request */
                RfCtlHandlePNReq(MuxId, dlcId, parms);
            } else {
                /* Response */
                RfCtlHandlePNCnf(MuxId, dlcId, parms);
            }
            break;

        case RPN:
            /* Remote Port Negotiation.
             * Get the DLC ID, it is OK to get invalid DLC ID 
             * Since an PN command may be received before the
             * channel is actually up.
             */
            (void)RfFindDlcIdByDlci(MuxId, (U8)(parms[0] >> 2), &dlcId);
            if (mType & CR_BIT) {
                RfCtlHandleRPNReq(MuxId, dlcId, parms, length);
            } 
#if RF_SEND_CONTROL == XA_ENABLED
            else {
                RfCtlHandleRPNCnf(MuxId, dlcId, parms);
            }
#endif /* RF_SEND_CONTROL == XA_ENABLED */
            break;

        case MSC:
            /* Modem Status.
             * Get the DLC ID, may return invalid DLC ID 
             * if no channel exists.
             */
            if (RfFindDlcIdByDlci(MuxId, (U8)(parms[0] >> 2), &dlcId) == 
                BT_STATUS_SUCCESS) {

                /* Found the DLC */
                if (mType & CR_BIT) {
                    RfCtlHandleMSCReq(MuxId, dlcId, parms, length);
                } else {
                    RfCtlHandleCnf(MuxId, dlcId, RFEVENT_MODEM_STATUS_CNF);
                }
            } else {
                /* No valid DLC found */
                AssertEval(RfSendAck(DM_F, MuxId, (U8)(parms[0] >> 2)) == 
                           BT_STATUS_PENDING);
            }
            break;

        case RLS:
            /* Remote Line Status.
             * Get the DLC ID, may return invalid DLC ID 
             * if no channel exists.
             */
            if (RfFindDlcIdByDlci(MuxId, (U8)(parms[0] >> 2), &dlcId) ==
                BT_STATUS_SUCCESS) {

                /* Found a DLC */
                if (mType & CR_BIT) {
                    RfCtlHandleRLSReq(MuxId, dlcId, parms);
                } 
#if RF_SEND_CONTROL == XA_ENABLED
                else {
                    RfCtlHandleCnf(MuxId, dlcId, RFEVENT_LINE_STATUS_CNF);
                }
#endif /* RF_SEND_CONTROL == XA_ENABLED */
            } else {
                /* No valid DLC found */
                AssertEval(RfSendAck(DM_F, MuxId, (U8)(parms[0] >> 2)) ==
                           BT_STATUS_PENDING);
            }
            break;

        case NSC:
            /* Non-Supported Command.
             * A command the was sent is not supported by the 
             * remote device.
             */
            RfCtlHandleNSC(MuxId, parms[0]);
            break;

        default:
            /* The command the was received is not supported command, 
             * send an NSC.
             */
            AssertEval(RfSendNSC(MuxId, mType) == BT_STATUS_PENDING);
            break;
        }

    } else if (Event == L2EVENT_PACKET_HANDLED) {
        /* See if this is a packet handled event for a test response */
        if (MUX(MuxId).txPacket == &(MUX(MuxId).testRspPacket)) {
            MUX(MuxId).flags &= ~MUX_FLAG_TEST_RECV;
        }
    }
}

/*---------------------------------------------------------------------------
 *            RfSendParmNegotiation()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Send a parameter negotiation packet.  The values for dlci, 
 *            priority, and frameSize must have been set previously.
 *
 * Return:    
 */
BtStatus RfSendParmNegotiation(RfChannel *channel, U8 CR)
{
    BtPacket *packet;
    BtStatus  status;

    /* Set up the packet */
    if (!(IsListEmpty(&(MUX(channel->muxId).freeList)))) {
        packet = (BtPacket *)RemoveHeadList(&(MUX(channel->muxId).freeList));
    } else {
        return BT_STATUS_NO_RESOURCES;
    }

    /* Set up the PN payload */
    packet->dataLen = 10;
    OS_MemSet(packet->data, 0, 10);
    packet->data[0] = PN | CR;
    channel->cmdSent = PN | CR;
    packet->data[1] = (8 << 1) | EA_BIT;
    packet->data[2] = channel->dlci;

    /* This is a valid negotiation */
    if (CR == CR_BIT) {
        /* This is a request, check to see if credit flow 
         * must be negotiated.
         */
        if (DLC(channel->muxId,channel->dlcId).flags & DLC_FLAG_CREDIT_FLOW) {
            packet->data[3] = CREDIT_FLOW_REQ;
        }
    } else {
        /* This is a response, check to see if credit flow 
         * control was requested 
         */
        if ((channel->flags & CHNL_FLAG_USE_PENDING) &&
            (MUX(channel->muxId).flags & MUX_FLAG_CREDIT_FLOW)) {
            packet->data[3] = CREDIT_FLOW_RSP;
        }
    }

    /* Advance initial credit if necessary */
    if (packet->data[3] != 0) {
        packet->data[9] = (U8)min(channel->initialRxCredit, 7);
        channel->grantedCredit = packet->data[9];
        channel->rxCredit = (S16)(channel->initialRxCredit - channel->grantedCredit);
    }

    packet->data[4] = channel->priority;
    StoreLE16(&(packet->data[6]), channel->maxFrameSize);

    packet->ulpContext = (void *)0;
    status = RfSendDataInternal(channel->muxId, 0, packet);
    if (status != BT_STATUS_PENDING) {
        /* Send failed */
        InsertTailList(&(MUX(channel->muxId).freeList), &(packet->node));
    } else if (CR == CR_BIT) {
        EVM_StartTimer(&(channel->timer), RF_T2_TIMEOUT);
        DLC(channel->muxId,channel->dlcId).flags |= DLC_FLAG_REQ_SENT;
    }
    return status;
}

/*---------------------------------------------------------------------------
 *            RfSendModemStatus()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Send a modem status command.
 *
 * Return:    
 */
BtStatus RfSendModemStatus(RfChannel *Channel, U8 Status, U8 Break, U8 CR)
{
    BtPacket *packet;
    U8        length;
    BtStatus  status;

    if (!(IsListEmpty(&(MUX(Channel->muxId).freeList)))) {
        packet = (BtPacket *)RemoveHeadList(&(MUX(Channel->muxId).freeList));
    } else {
        return BT_STATUS_NO_RESOURCES;
    }

    if (Break) {
        length = 3;
        packet->data[3] = Status & 0xCE;
        packet->data[4] = (Break << 4) | 0x03;
    } else {
        length = 2;
        packet->data[3] = (Status & 0xCE) | EA_BIT;
    }

    packet->dataLen = length + 2;
    packet->data[0] = MSC | CR;
    Channel->cmdSent = MSC | CR;
    packet->data[1] = (length << 1) | EA_BIT;
    packet->data[2] = (Channel->dlci << 2) | CR_BIT | EA_BIT;
    packet->ulpContext = (void *)0;
    status = RfSendDataInternal(Channel->muxId, 0, packet);
    if (status != BT_STATUS_PENDING) {
        /* Send failed */
        InsertTailList(&(MUX(Channel->muxId).freeList), &(packet->node));
    } else if (CR == CR_BIT) {
        EVM_StartTimer(&(Channel->timer), RF_T2_TIMEOUT);
        DLC(Channel->muxId,Channel->dlcId).flags |= DLC_FLAG_REQ_SENT;
    }

    return status;
}

/*---------------------------------------------------------------------------
 *            RfSendLineStatus()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Send a line status command.
 *
 * Return:    
 */
BtStatus RfSendLineStatus(RfChannel *Channel, RfLineStatus Status, U8 CR)
{
    BtPacket *packet;
    U8        length = 2;
    BtStatus  status;

    if (!(IsListEmpty(&(MUX(Channel->muxId).freeList)))) {
        packet = (BtPacket *)RemoveHeadList(&(MUX(Channel->muxId).freeList));
    } else {
        return BT_STATUS_NO_RESOURCES;
    }

    packet->dataLen = length + 2;
    packet->data[0] = RLS | CR;
    Channel->cmdSent = RLS | CR;
    packet->data[1] = (length << 1) | EA_BIT;
    packet->data[2] = (Channel->dlci << 2) | CR_BIT | EA_BIT;
    packet->data[3] = (Status & 0x0E) == 0 ? (Status & 0x0E) : (Status & 0x0E) | 0x01;
    packet->ulpContext = (void *)0;
    status = RfSendDataInternal(Channel->muxId, 0, packet);
    if (status != BT_STATUS_PENDING) {
        /* Send failed */
        InsertTailList(&(MUX(Channel->muxId).freeList), &(packet->node));
    } else if (CR == CR_BIT) {
        EVM_StartTimer(&(DLC(Channel->muxId,Channel->dlcId).channel->timer), RF_T2_TIMEOUT);
        DLC(Channel->muxId,Channel->dlcId).flags |= DLC_FLAG_REQ_SENT;
    }

    return status;
}

/*---------------------------------------------------------------------------
 *            RfSendRemotePortNeg()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Send a remote port negotiation command.
 *
 * Return:    
 */
BtStatus RfSendRemotePortNeg(RfChannel *Channel, RfPortSettings *Rpn, U8 CR)
{
    BtPacket *packet;
    U8        length;
    BtStatus  status;

    if (!(IsListEmpty(&(MUX(Channel->muxId).freeList)))) {
        packet = (BtPacket *)RemoveHeadList(&(MUX(Channel->muxId).freeList));
    } else {
        return BT_STATUS_NO_RESOURCES;
    }

    if (Rpn != 0) {
        length = 8;
        packet->data[3] = Rpn->baudRate;
        packet->data[4] = Rpn->dataFormat;
        packet->data[5] = Rpn->flowControl;
        packet->data[6] = Rpn->xonChar;
        packet->data[7] = Rpn->xoffChar;
        StoreLE16(&(packet->data[8]), Rpn->parmMask);
    } else {
        length = 1;
    }

    packet->dataLen = length + 2;
    packet->data[0] = RPN | CR;
    Channel->cmdSent = RPN | CR;
    packet->data[1] = (length << 1) | EA_BIT;
    packet->data[2] = (Channel->dlci << 2) | CR_BIT | EA_BIT;
    packet->ulpContext = (void *)0;
    status = RfSendDataInternal(Channel->muxId, 0, packet);
    if (status != BT_STATUS_PENDING) {
        /* Send failed */
        InsertTailList(&(MUX(Channel->muxId).freeList), &(packet->node));
    } else if (CR == CR_BIT) {
        EVM_StartTimer(&(DLC(Channel->muxId,Channel->dlcId).channel->timer), RF_T2_TIMEOUT);
        DLC(Channel->muxId,Channel->dlcId).flags |= DLC_FLAG_REQ_SENT;
    }

    return status;
}

/*---------------------------------------------------------------------------
 *            RfSendAggregateFlow()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Send an aggregate flow (FCON/FCOFF) command.
 *
 * Return:    
 */
BtStatus RfSendAggregateFlow(U8 MuxId, BOOL Flow, U8 CR)
{
    BtPacket *packet;
    BtStatus  status;

    if (!(IsListEmpty(&(MUX(MuxId).freeList)))) {
        packet = (BtPacket *)RemoveHeadList(&(MUX(MuxId).freeList));
    } else {
        return BT_STATUS_NO_RESOURCES;
    }

    packet->dataLen = 2;
    if (Flow) {
        packet->data[0] = FCON | CR;
    } else {
        packet->data[0] = FCOFF | CR;
    }
    packet->data[1] = EA_BIT;
    packet->ulpContext = (void *)0;
    status = RfSendDataInternal(MuxId, 0, packet);
    if (status != BT_STATUS_PENDING) {
        /* Send failed */
        InsertTailList(&(MUX(MuxId).freeList), &(packet->node));
    } 
    return status;
}

/*---------------------------------------------------------------------------
 *            RfSendTest()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Send a remote port negotiation command.
 *
 * Return:    
 */
BtStatus RfSendTest(U8 MuxId, U8 len, U8 CR)
{
    BtPacket *packet;
    BtStatus  status;

#if RF_SEND_TEST == XA_ENABLED
    if (CR == CR_BIT) {
        packet = &MUX(MuxId).testCmdPacket;
    } else 
#endif /* RF_SEND_TEST == XA_ENABLED */
    {
        packet = &MUX(MuxId).testRspPacket;
    }

    packet->dataLen = len + 2;
    packet->data[0] = (U8)(TEST | CR);
    packet->data[1] = (U8)((len << 1) | EA_BIT);
    packet->ulpContext = (void *)0;
    status = RfSendDataInternal(MuxId, 0, packet);
    if (status == BT_STATUS_PENDING) {
        if (CR == CR_BIT) {
            MUX(MuxId).flags |= MUX_FLAG_TEST_SENT;
        } else {
            MUX(MuxId).flags |= MUX_FLAG_TEST_RECV;
        }
    }

    return status;
}

/*---------------------------------------------------------------------------
 *            RfSendNSC()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Send a NSC command.
 *
 * Return:    
 */
static BtStatus RfSendNSC(U8 MuxId, U8 mType)
{
    BtPacket *packet;
    BtStatus  status = BT_STATUS_NO_RESOURCES;

    if (mType & CR_BIT) {
        /* Respond to the unsupported request */
        if (!(IsListEmpty(&(MUX(MuxId).freeList)))) {
            packet = (BtPacket *)RemoveHeadList(&(MUX(MuxId).freeList));
            packet->dataLen = 3;
            packet->data[0] = NSC;
            packet->data[1] = (1 << 1) | EA_BIT;
            packet->data[2] = mType;
            packet->ulpContext = (void *)0;
            status = RfSendDataInternal(MuxId, 0, packet);
            if (status != BT_STATUS_PENDING) {
                /* Send failed */
                InsertTailList(&(MUX(MuxId).freeList), &(packet->node));
            }
        }
    }

    return status;
}

#else 

/* Some compilers complain about empty files */
const U8 rfcomm_protocol=0;

#endif /* RFCOMM_PROTOCOL == XA_ENABLED */
