/***************************************************************************
 *
 * File:
 *     $Workfile:rfcomm.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:119$
 *
 * Description:
 *     This file contains code for the rfcomm protocol.
 *
 * Created:
 *     July 20, 1999
 *
 * Copyright 1999-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "btalloc.h"

#if RFCOMM_PROTOCOL == XA_ENABLED

#if NUM_RF_SERVERS != 0
/*---------------------------------------------------------------------------
 *            RF_RegisterServerChannel()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Register a service on the specified RFCOMM channel.
 *
 * Return:    <See rfcomm.h>
 */
BtStatus RF_RegisterServerChannel(RfChannel *Channel, RfService *Service, U8 Credit)
{
    BtStatus status;

#if XA_ERROR_CHECK == XA_ENABLED
    if (Channel == 0) {
        return BT_STATUS_INVALID_PARM;
    }

    if (Channel->callback == 0) {
        return BT_STATUS_INVALID_PARM;
    }

    if (Service == 0) {
        return BT_STATUS_INVALID_PARM;
    }

    if ((Channel->maxFrameSize > RF_MAX_FRAME_SIZE) ||
        (Channel->maxFrameSize < RF_MIN_FRAME_SIZE)) {
        return BT_STATUS_INVALID_PARM;
    }

#else /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(Channel != 0);
    Assert(Channel->callback != 0);
    Assert(Service != 0);
    Assert((Channel->maxFrameSize <= RF_MAX_FRAME_SIZE) &&
           (Channel->maxFrameSize > RF_MIN_FRAME_SIZE));

#endif /* XA_ERROR_CHECK == XA_ENABLED */

    OS_LockStack();

    Channel->rxCredit = Credit;
    Channel->initialRxCredit = Credit;
    Channel->initialFrameSize = Channel->maxFrameSize;

#if UPF_TWEAKS == XA_ENABLED
    Channel->flags &= CHNL_FLAG_NO_CFC;
#else
    Channel->flags = 0;
#endif /* UPF_TWEAKS == XA_ENABLED */

    /* Get the new Server Channel */
    status =  RfAllocService(Channel, Service);
    
    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            RF_DeregisterServerChannel()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Deregister a channel from a service.  
 *
 * Return:    <See rfcomm.h>
 */
BtStatus RF_DeregisterServerChannel(RfChannel *Channel, RfService *Service)
{
    BtStatus status;

#if XA_ERROR_CHECK == XA_ENABLED
    if (Channel == 0) {
        return BT_STATUS_INVALID_PARM;
    }

    if (Channel->callback == 0) {
        return BT_STATUS_INVALID_PARM;
    }

    if (Service == 0) {
        return BT_STATUS_INVALID_PARM;
    }

    if ((Service->serviceId > NUM_RF_SERVERS) ||
        (Service->serviceId == 0)) {
        return BT_STATUS_INVALID_PARM;
    }

#else /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(Channel != 0);
    Assert(Channel->callback != 0);
    Assert(Service != 0);
    Assert((Service->serviceId < NUM_RF_SERVERS) && (Service->serviceId > 0));

#endif /* XA_ERROR_CHECK == XA_ENABLED */

    OS_LockStack();

    /* Free the server channel */
    status = RfFreeServerChannel(Channel, Service);

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            RF_DeregisterService()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Deregister a service from RFCOMM.  
 *
 * Return:    <See rfcomm.h>
 */
BtStatus RF_DeregisterService(RfService *Service)
{
    BtStatus status;

#if XA_ERROR_CHECK == XA_ENABLED
    if (Service == 0) {
        return BT_STATUS_INVALID_PARM;
    }

#else /* XA_ERROR_CHECK == XA_ENABLED */
    Assert(Service != 0);
#endif /* XA_ERROR_CHECK == XA_ENABLED */

    OS_LockStack();

    /* Free the service */
    status = RfFreeService(Service);

    OS_UnlockStack();

    return status;
}
#endif /* NUM_RF_SERVERS != 0 */

/*---------------------------------------------------------------------------
 *            RfOpenClientChannel()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Open a channel to a service on the remote device after security
 *            access has been validated.
 */
static BtStatus RfOpenClientChannel(BtRemoteDevice *RemDev, 
                                    U8              ServerId,
                                    RfChannel      *Channel)
{
    L2capChannelId l2ChannelId;
    BtStatus status;
    U8 muxId;
    U8 dlcId;
    U8 dlci;

    if (RfFindMuxIdByRemDev(RemDev, &muxId) == BT_STATUS_SUCCESS) {

        /* The multiplexer is established, create the DLCI */
        if (MUX(muxId).flags & MUX_FLAG_INITIATOR) {
            dlci = (U8) (ServerId << 1);
        } else {
            dlci = (U8) ((ServerId << 1) | 0x01);
        }

        /* An L2CAP channel already exists to the device */
        if (MUX(muxId).state == MUX_STATE_OPEN) {

            /* Allocate a new DLC */
            if (RfAllocDLC(muxId, dlci, Channel, &dlcId) == BT_STATUS_SUCCESS) {

                if (MUX(muxId).flags & MUX_FLAG_CREDIT_FLOW) {
                    /* This is not the first DLC established and credit-based flow
                     * control has been negotiated.  Keep sending the negotiation.
                     * If credit-based flow had not been negotiated, we cannot try
                     * again.
                     */
                    DLC(muxId,dlcId).flags |= DLC_FLAG_CREDIT_FLOW;
                }

                /* Send the channel negotiation */
                DLC(muxId,dlcId).channel->maxFrameSize = (U16)
                                    min(L2CAP_GetTxMtu(MUX(muxId).l2ChannelId) - 5,
                                        DLC(muxId,dlcId).channel->maxFrameSize);
                status = RfSendParmNegotiation(Channel, CR_BIT);
                if (status == BT_STATUS_PENDING) {

                    /* Negotiation request sent */
                    DLC(muxId,dlcId).channel->state = DLC_STATE_NEG_PENDING;
                    Report(("RFCOMM:  Negotiation sent successfully to DLCI %d on mux %d\n", 
                            DLC(muxId,MUX(muxId).cDlcId).channel->dlci, muxId));
                } else {

                    /* Could not send the negotiation */
                    RfFreeDLC(muxId, dlcId);

                    /* If this is the last DLC, shut down the mux */
                    if (!MUX(muxId).dlcCount) {
                        RfDisconnectL2CAP(muxId);
                    }
                }
            } else {
    
                /* No channels left, or channel is in use */
                status = BT_STATUS_NO_RESOURCES;
            }
        } else {
            /* No mux is allocated */
            status = BT_STATUS_FAILED;
        }

    } else {

        /* No connection exists yet, this side is the initator */
        dlci = (U8) (ServerId << 1);

        /* Establish a new connection to the device */
        status = L2CAP_ConnectReq(&(RFC(psm)), BT_PSM_RFCOMM, RemDev, 0, &l2ChannelId);
        if (status == BT_STATUS_PENDING) {

            /* Connection started, allocate a new mux */
            if (RfAllocMux(RemDev, l2ChannelId, &muxId) == BT_STATUS_SUCCESS) {

                /* Initialize mux */
                MUX(muxId).state = MUX_STATE_CONN_PENDING;
                MUX(muxId).flags |= MUX_FLAG_INITIATOR;

                /* Allocate a new DLC */
                if (RfAllocDLC(muxId, dlci, Channel, &dlcId) == BT_STATUS_SUCCESS) {
                    /* Initialize DLC and channel ID */
                    MUX(muxId).cDlcId = dlcId;

                    /* This is the first DLC, try to negotiate credit flow control */
                    DLC(muxId,dlcId).flags |= DLC_FLAG_CREDIT_FLOW;

#if UPF_TWEAKS == XA_ENABLED
                    if (Channel->flags & CHNL_FLAG_NO_CFC) {
                        DLC(muxId,dlcId).flags &= ~DLC_FLAG_CREDIT_FLOW;
                    }
#endif /* UPF_TWEAKS == XA_ENABLED */

                } else {
                    /* No more available channels */
                    RfFreeMux(muxId);
                    status = BT_STATUS_NO_RESOURCES;
                }
            } else {
                /* No more available MUX's */
                status = BT_STATUS_NO_RESOURCES;
            }
        } 
    } 

    return status;
}

#if (BT_SECURITY == XA_ENABLED) && (RF_SECURITY == XA_ENABLED)
/*---------------------------------------------------------------------------
 *            RfSecCallback()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Callback for security check.
 */
void RfSecCallback(const BtEvent *Event)
{
    RfChannel *channel = ContainingRecord(Event->p.secToken, RfChannel, secToken);
    BtStatus status;

    if (channel->state == DLC_STATE_SEC_PENDING) {
        if (Event->p.secToken->incoming == FALSE) {
            /* This is an outgoing connection */
            if (Event->eType == BTEVENT_ACCESS_APPROVED) {
                /* Security approved. Use dlci as ServerID for now. */
                status = RfOpenClientChannel(channel->secToken.remDev, 
                                            channel->dlci, 
                                            channel);
                if (status != BT_STATUS_PENDING) {
                    RfAppCallback(channel, status, RFEVENT_CLOSED, 0);
                }
            } else {
                /* Security failed */
                RfAppCallback(channel, BT_STATUS_RESTRICTED, RFEVENT_CLOSED, 0);
            }
        } else {
            /* This is an incoming connection */
            if (Event->eType == BTEVENT_ACCESS_APPROVED) {
                /* Security approved */
                channel->state = DLC_STATE_CONN_INCOMING;
                RfAppCallback(channel, BT_STATUS_SUCCESS, RFEVENT_OPEN_IND, 0);
            } else {
                /* Security failed */
                (void)RfSendAck(DM_F, channel->muxId, channel->dlci);
                channel->state = DLC_STATE_SEC_FAILED;
                RfFreeDLC(channel->muxId, channel->dlcId);
            }
        }
    }
}
#endif /* (BT_SECURITY == XA_ENABLED) && (RF_SECURITY == XA_ENABLED) */

/*---------------------------------------------------------------------------
 *            RF_OpenClientChannel()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Open a channel to a service on the remote device.
 *
 * Return:    <See rfcomm.h>
 */
BtStatus RF_OpenClientChannel(BtRemoteDevice *RemDev, 
                              U8              ServerId,
                              RfChannel      *Channel,
                              U8              Credit)
{
    BtStatus status = BT_STATUS_FAILED;
    U8 muxId;
    I8 i;

#if (BT_SECURITY == XA_ENABLED) && (RF_SECURITY == XA_ENABLED)
    U8 oldState;
#endif /* (BT_SECURITY == XA_ENABLED) && (RF_SECURITY == XA_ENABLED) */

#if XA_ERROR_CHECK == XA_ENABLED

    if (RemDev == 0) {
        return BT_STATUS_INVALID_PARM;
    }

    if ((ServerId == 0) || (ServerId > 30)) {
        return BT_STATUS_INVALID_PARM;
    }

    if (Channel == 0) {
        return BT_STATUS_INVALID_PARM;
    }

    if ((Channel->callback == 0) || 
        (Channel->priority > 63) ||
        (Channel->maxFrameSize > RF_MAX_FRAME_SIZE) ||
        (Channel->maxFrameSize < RF_MIN_FRAME_SIZE)) {
        return BT_STATUS_INVALID_PARM;
    }

#else /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(RemDev != 0);
    Assert((ServerId  > 0) && (ServerId <= 30));
    Assert(Channel != 0);
    Assert(Channel->callback != 0);
    Assert(Channel->priority <= 63);
    Assert(Channel->maxFrameSize <= RF_MAX_FRAME_SIZE);
    Assert(Channel->maxFrameSize >= RF_MIN_FRAME_SIZE);

#endif /* XA_ERROR_CHECK == XA_ENABLED */

    OS_LockStack();

    /* See if this channel is allocated already */
    if (RfFindMuxIdByRemDev(RemDev, &muxId) == BT_STATUS_SUCCESS) {
        for (i = 0; i < NUM_RF_CHANNELS; i++) {
            if ((DLC(muxId,i).flags & DLC_FLAG_ALLOCATED) &&
                (DLC(muxId,i).channel == Channel)) {
                OS_UnlockStack();
                return BT_STATUS_IN_USE;
            }
        }
    }

    Channel->rxCredit = Credit;
    Channel->initialRxCredit = Credit;
    Channel->initialFrameSize = Channel->maxFrameSize;
#if (BT_SECURITY == XA_ENABLED) && (RF_SECURITY == XA_ENABLED)
    /* Check security for this channel */
    oldState = Channel->state;
    Channel->state = DLC_STATE_SEC_PENDING;
    Channel->secToken.id = SEC_RFCOMM_ID;
    Channel->secToken.remDev = RemDev;
    Channel->secToken.channel = (U32)Channel;
    Channel->secToken.incoming = FALSE;
    status = SEC_AccessRequest(&(Channel->secToken));
    if (status == BT_STATUS_SUCCESS) {
        Channel->state = oldState;
        status = RfOpenClientChannel(RemDev, ServerId, Channel);
    } else if (status != BT_STATUS_PENDING) {
        Channel->state = oldState;
        status = BT_STATUS_RESTRICTED;
    } else {
        /* Temporarily use dlci to store the requested ServerID */
        Channel->dlci = ServerId;
    }

#else /* (BT_SECURITY == XA_ENABLED) && (RF_SECURITY == XA_ENABLED) */
    status = RfOpenClientChannel(RemDev, ServerId, Channel);
#endif /* (BT_SECURITY == XA_ENABLED) && (RF_SECURITY == XA_ENABLED) */

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            RF_CloseChannel()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Close the channel between two devices.
 *
 * Return:    <See rfcomm.h>
 */
BtStatus RF_CloseChannel(RfChannel *Channel)
{
    BtStatus status = BT_STATUS_FAILED;

#if XA_ERROR_CHECK == XA_ENABLED

    if (!RfIsValidChannel(Channel)) {
        return BT_STATUS_INVALID_PARM;
    }

#else /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(RfIsValidChannel(Channel));

#endif /* XA_ERROR_CHECK == XA_ENABLED */

    OS_LockStack();

    if ((DLC(Channel->muxId, Channel->dlcId).channel)) {
        if ((Channel->state == DLC_STATE_CONN_PENDING) ||
            (RfIsChannelAllocated(Channel) &&
             Channel->state == DLC_STATE_CONNECTED)) {
            /* Make sure we are clear to send DISC (even if previous
             * command un-acked) */
            MUX(Channel->muxId).flags &= ~MUX_FLAG_CMD_SENT;
            status = RfDisconnectDLC(Channel->muxId, Channel->dlcId);
        } else if (Channel->state == DLC_STATE_DISC_PENDING) {
            status = BT_STATUS_IN_PROGRESS;
        }
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            RF_RespondChannel()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Accept or reject a Channel connect request between two devices.  
 *
 * Return:    <See rfcomm.h>
 */
BtStatus RF_RespondChannel(RfChannel *Channel, BOOL flag)
{
    BtStatus status = BT_STATUS_FAILED;

#if XA_ERROR_CHECK == XA_ENABLED

    if (!RfIsValidChannel(Channel)) {
        return BT_STATUS_INVALID_PARM;
    }

#else /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(RfIsValidChannel(Channel));

#endif /* XA_ERROR_CHECK == XA_ENABLED */

    OS_LockStack();

    if (RfIsChannelAllocated(Channel) &&
            Channel->state == DLC_STATE_CONN_INCOMING) {
        if (flag) {
            status = RfSendAck(UA_F, Channel->muxId, Channel->dlci);
        } else {
            status = RfSendAck(DM_F, Channel->muxId, Channel->dlci);
        }
    } else {
        status = BT_STATUS_FAILED;
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            RF_SendData()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Send data on an RFCOMM channel.
 *
 * Return:    <See rfcomm.h>
 */
BtStatus RF_SendData(RfChannel *Channel, BtPacket *Packet)
{
    BtStatus status = BT_STATUS_FAILED;

#if XA_ERROR_CHECK == XA_ENABLED

    if (!RfIsValidChannel(Channel)) {
        return BT_STATUS_INVALID_PARM;
    }

    if (Packet == 0) {
        return BT_STATUS_INVALID_PARM;
    }

    if (MUX(Channel->muxId).flags & MUX_FLAG_CREDIT_FLOW) {
        /* Allow one less byte of data when credit flow is used */
        if (Packet->dataLen > DLC(Channel->muxId,Channel->dlcId).channel->maxFrameSize - 1) {
            return BT_STATUS_INVALID_PARM;
        } 
    } else if (Packet->dataLen > DLC(Channel->muxId,Channel->dlcId).channel->maxFrameSize) {
        return BT_STATUS_INVALID_PARM;
    }

    if ((Packet->dataLen > 0) && Packet->data == 0) {
        return BT_STATUS_INVALID_PARM;
    }

#else /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(RfIsValidChannel(Channel));
    Assert(Packet != 0);

#if XA_DEBUG == XA_ENABLED

    if (MUX(Channel->muxId).flags & MUX_FLAG_CREDIT_FLOW) {
        Assert(Packet->dataLen <= DLC(Channel->muxId,Channel->dlcId).channel->maxFrameSize - 1);
    } else {
        Assert(Packet->dataLen <= DLC(Channel->muxId,Channel->dlcId).channel->maxFrameSize);
    }

#endif

    Assert((Packet->dataLen > 0) ? (U32)(Packet->data) : TRUE);

#endif /* XA_ERROR_CHECK == XA_ENABLED */


    OS_LockStack();

    XASTAT_TimerStart(rfcomm_tx_timer, Packet->rfc_timer);

    if (RfIsChannelAllocated(Channel) && 
            Channel->state == DLC_STATE_CONNECTED) {
        Packet->ulpContext = Channel;
        
        /* Insert onto the transmit queue */
        InsertTailList(&(Channel->txQueue), &(Packet->node));
        MUX(Channel->muxId).numTxPackets++;
        status = BT_STATUS_PENDING;
        if (!(MUX(Channel->muxId).flags & MUX_FLAG_XMIT)) {
            /* Not already transmitting, send any queued packets. */
                RfMUXCheckTxQueues(Channel->muxId);
            }
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            RF_SetModemStatus()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Set the modem status.
 *
 * Return:    <See rfcomm.h>
 */
BtStatus RF_SetModemStatus(RfChannel *Channel, RfModemStatus *ModemStatus)
{
    BtStatus status = BT_STATUS_FAILED;
    RF_DLC *dlc;

#if XA_ERROR_CHECK == XA_ENABLED

    if (!RfIsValidChannel(Channel)) {
        return BT_STATUS_INVALID_PARM;
    }

    if (ModemStatus == 0) {
        return BT_STATUS_INVALID_PARM;
    }

    if (ModemStatus->breakLen > 15) {
        return BT_STATUS_INVALID_PARM;
    }

#else /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(RfIsValidChannel(Channel));
    Assert(ModemStatus != 0);
    Assert(ModemStatus->breakLen < 16);

#endif /* XA_ERROR_CHECK == XA_ENABLED */

    OS_LockStack();

    dlc = &DLC(Channel->muxId,Channel->dlcId);

    if (RfIsChannelAllocated(Channel) &&
            Channel->state == DLC_STATE_CONNECTED) {
        if (dlc->flags & DLC_FLAG_REQ_SENT) {
            /* A request has already been sent */
            status = BT_STATUS_IN_PROGRESS;
        } else {
            /* No outstanding request */
            Channel->rfSignals = (RfSignals)((ModemStatus->signals & ~RF_FLOW) | 
                                 (Channel->rfSignals & RF_FLOW));

            status = RfSendModemStatus(Channel, 
                               Channel->rfSignals, 
                               ModemStatus->breakLen, 
                               CR_BIT);
        }
    }

    OS_UnlockStack();

    return status;
}

#if RF_SEND_CONTROL == XA_ENABLED
/*---------------------------------------------------------------------------
 *            RF_SetLineStatus()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Set the line status.
 *
 * Return:    <See rfcomm.h>
 */
BtStatus RF_SetLineStatus(RfChannel *Channel, RfLineStatus Status)
{
    BtStatus status = BT_STATUS_FAILED;
    RF_DLC *dlc;

#if XA_ERROR_CHECK == XA_ENABLED

    if (!RfIsValidChannel(Channel)) {
        return BT_STATUS_INVALID_PARM;
    }

#else /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(RfIsValidChannel(Channel));

#endif /* XA_ERROR_CHECK == XA_ENABLED */

    OS_LockStack();

    dlc = &DLC(Channel->muxId,Channel->dlcId);

    if (RfIsChannelAllocated(Channel) &&
            Channel->state == DLC_STATE_CONNECTED) {
        if (dlc->flags & DLC_FLAG_REQ_SENT) {
            /* A request has already been sent */
            status = BT_STATUS_IN_PROGRESS;
        } else {
            status = RfSendLineStatus(Channel, Status, CR_BIT);
        }
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            RF_SendPortSettings()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Negotiate port settings with the remote device. 
 *
 * Return:    <See rfcomm.h>
 */
BtStatus RF_SendPortSettings(RfChannel *Channel, RfPortSettings *PortSettings)
{
    BtStatus status = BT_STATUS_FAILED;
    RF_DLC *dlc;

#if XA_ERROR_CHECK == XA_ENABLED

    if (!RfIsValidChannel(Channel)) {
        return BT_STATUS_INVALID_PARM;
    }

#else /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(RfIsValidChannel(Channel));

#endif /* XA_ERROR_CHECK == XA_ENABLED */

    OS_LockStack();

    dlc = &DLC(Channel->muxId,Channel->dlcId);

    if (RfIsChannelAllocated(Channel) &&
            Channel->state == DLC_STATE_CONNECTED) {
        if (dlc->flags & DLC_FLAG_REQ_SENT) {
            /* A request has already been sent */
            status = BT_STATUS_IN_PROGRESS;
        } else {
            if (PortSettings == 0) {
                dlc->flags |= DLC_FLAG_RPN_STATUS;
            }
            status = RfSendRemotePortNeg(Channel, PortSettings, CR_BIT);
        }
    }

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            RF_RespondPortSettings()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Accept or reject port setting received from the remote device.
 *
 * Return:    <See rfcomm.h>
 */
BtStatus RF_RespondPortSettings(RfChannel *Channel, 
                                RfPortSettings *PortSettings,
                                BOOL delayedResponse)
{
    BtStatus status = BT_STATUS_FAILED;
    RF_DLC *dlc;

#if XA_ERROR_CHECK == XA_ENABLED

    if (!RfIsValidChannel(Channel)) {
        return BT_STATUS_INVALID_PARM;
    }

#else /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(RfIsValidChannel(Channel));

#endif /* XA_ERROR_CHECK == XA_ENABLED */

    OS_LockStack();

    dlc = &DLC(Channel->muxId,Channel->dlcId);

    if (RfIsChannelAllocated(Channel) &&
            dlc->flags & DLC_FLAG_REQ_INCOMING) {
        
        /* Send the reponse */
        if (!delayedResponse) {
            if ((status = RfSendRemotePortNeg(Channel, PortSettings, 0)) ==
                BT_STATUS_PENDING) {
                DLC(Channel->muxId,Channel->dlcId).flags &= ~DLC_FLAG_REQ_INCOMING;
                status = BT_STATUS_SUCCESS;
            } else if (status == BT_STATUS_NO_RESOURCES) {
                Channel->flags |= CHNL_FLAG_NO_RESOURCE;
            }
        } else {
            Channel->flags |= CHNL_FLAG_RSP_DELAYED;
            status = BT_STATUS_SUCCESS;
        }
    }

    OS_UnlockStack();

    return status;

}
#endif /* RF_SEND_CONTROL == XA_ENABLED */

#if RF_SEND_TEST == XA_ENABLED
/*---------------------------------------------------------------------------
 *            RF_SendTest()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Tests the connection with the remote device.
 *
 * Return:    <See rfcomm.h>
 */
BtStatus RF_SendTest(RfChannel *Channel)
{
    BtStatus status = BT_STATUS_FAILED;
    RF_DLC *dlc;

#if XA_ERROR_CHECK == XA_ENABLED

    if (!RfIsValidChannel(Channel)) {
        return BT_STATUS_INVALID_PARM;
    }

#else /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(RfIsValidChannel(Channel));

#endif /* XA_ERROR_CHECK == XA_ENABLED */

    OS_LockStack();

    dlc = &DLC(Channel->muxId,Channel->dlcId);

    if (RfIsChannelAllocated(Channel) &&
            Channel->state == DLC_STATE_CONNECTED) {
        if (!(MUX(Channel->muxId).flags & MUX_FLAG_TEST_SENT) &&
            !(dlc->flags & DLC_FLAG_REQ_SENT)) {
            MUX(Channel->muxId).testCmdData[2] = TEST_PATTERN;
            Channel->cmdSent = TEST | CR_BIT;
            status = RfSendTest(Channel->muxId, 1, CR_BIT);
            if (status == BT_STATUS_PENDING) {
                EVM_StartTimer(&(Channel->timer), RF_T2_TIMEOUT);
                DLC(Channel->muxId,Channel->dlcId).flags |= DLC_FLAG_REQ_SENT;
                MUX(Channel->muxId).testDlcId = Channel->dlcId;
            }
        } else {
            status = BT_STATUS_IN_PROGRESS;
        }
    }

    OS_UnlockStack();

    return status;
}
#endif /* RF_SEND_TEST == XA_ENABLED */

/*---------------------------------------------------------------------------
 *            RF_FrameSize()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Returns the maximum size allowed for a transmit packet.
 *
 * Return:    Maxmum number of bytes for a transmit packet.
 */
U16 RF_FrameSize(RfChannel *Channel)
{
    U16 status = 0;

#if XA_ERROR_CHECK == XA_ENABLED

    if (!RfIsValidChannel(Channel)) {
        return status;
    }

#else /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(RfIsValidChannel(Channel));

#endif /* XA_ERROR_CHECK == XA_ENABLED */

    OS_LockStack();

    if (RfIsChannelAllocated(Channel) &&
            Channel->state == DLC_STATE_CONNECTED) {
        if (MUX(Channel->muxId).flags & MUX_FLAG_CREDIT_FLOW) {
            /* Report one less byte when credit flow control is on */
            status = (U16)(DLC(Channel->muxId,Channel->dlcId).channel->maxFrameSize - 1);
        } else {
            status = (U16)(DLC(Channel->muxId,Channel->dlcId).channel->maxFrameSize);
        }
    } 

    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            RF_AdvanceCredit()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Advance transmit credit to the remote device.
 *
 * Return:    <See rfcomm.h>
 */
BtStatus RF_AdvanceCredit(RfChannel *Channel, U8 Credit)
{
    BtStatus status = BT_STATUS_SUCCESS;

#if XA_ERROR_CHECK == XA_ENABLED

    if (!RfIsValidChannel(Channel)) {
        return BT_STATUS_INVALID_PARM;
    }

#else /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(RfIsValidChannel(Channel));

#endif /* XA_ERROR_CHECK == XA_ENABLED */

    OS_LockStack();
    
    if (RfIsChannelAllocated(Channel) &&
            Channel->state == DLC_STATE_CONNECTED) {
        if ((Credit + Channel->rxCredit + Channel->grantedCredit) <= 255) {
            Channel->rxCredit = (S16)(Channel->rxCredit + Credit);
            RfCheckForCreditToSend(Channel);
        } else {
            /* Too much credit advanced */
            status = BT_STATUS_FAILED;
        }
    } else {
        /* Not connected */
        status = BT_STATUS_NO_CONNECTION;
    }


    OS_UnlockStack();

    return status;
}

/*---------------------------------------------------------------------------
 *            RF_CreditFlowEnabled()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Identifies whether credit based flow control has been negotiated 
 *            for the currrent RFCOMM session or not.
 *
 * Return:    TRUE - credit flow is enabled.
 *            FALSE - credit flow is not enabled.
 */
BOOL RF_CreditFlowEnabled(RfChannel *Channel)
{
    BOOL enabled = FALSE;

#if XA_ERROR_CHECK == XA_ENABLED

    if (!RfIsValidChannel(Channel)) {
        return FALSE;
    }

#else /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(RfIsValidChannel(Channel));

#endif /* XA_ERROR_CHECK == XA_ENABLED */

    OS_LockStack();

    if (RfIsChannelAllocated(Channel)) {
        enabled = (MUX(Channel->muxId).flags & MUX_FLAG_CREDIT_FLOW) ? TRUE: FALSE;
    }

    OS_UnlockStack();

    return enabled;
}

/*---------------------------------------------------------------------------
 *            RF_RemoteDevice()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Returns the remote device associated with the specified channel.
 *
 * Return:    BtRemoteDevice - a pointer to the remote device structure used 
 *                             for the specified channel.
 */
BtRemoteDevice *RF_RemoteDevice(RfChannel *Channel)
{
    BtRemoteDevice *remDev = 0;

#if XA_ERROR_CHECK == XA_ENABLED

    if (!RfIsValidChannel(Channel)) {
        return 0;
    }

#else /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(RfIsValidChannel(Channel));

#endif /* XA_ERROR_CHECK == XA_ENABLED */

    OS_LockStack();

    if (RfIsChannelAllocated(Channel)) {
        remDev = MUX(Channel->muxId).remDev;
    }

    OS_UnlockStack();

    return remDev;
}


#if UPF_TWEAKS == XA_ENABLED
/*---------------------------------------------------------------------------
 *            RF_SendAggrateFlow()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Send the aggregate flow command.  This is provided for 
 *            Unplugfest support.  
 */
BtStatus RF_SendAggregateFlow(RfChannel *Channel, U8 state)
{
    BtStatus status;

#if XA_ERROR_CHECK == XA_ENABLED

    if (!RfIsValidChannel(Channel)) {
        OS_UnlockStack();
        return FALSE;
    }

#else /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(RfIsValidChannel(Channel));

#endif /* XA_ERROR_CHECK == XA_ENABLED */

    OS_LockStack();

    /* If credit flow control has been negotiated, this command
     * cannot be sent.
     */
    if (MUX(Channel->muxId).flags & MUX_FLAG_CREDIT_FLOW) {
        OS_UnlockStack();
        return BT_STATUS_FAILED;   
    } 

    if (state) {
        Channel->cmdSent = FCON | CR_BIT;
    } else {
        Channel->cmdSent = FCOFF | CR_BIT;
    }

    status = RfSendAggregateFlow(Channel->muxId, state, CR_BIT);
    if (status == BT_STATUS_PENDING) {
        EVM_StartTimer(&(Channel->timer), RF_T2_TIMEOUT);
        DLC(Channel->muxId,Channel->dlcId).flags |= DLC_FLAG_REQ_SENT;
    }

    OS_UnlockStack();
    return status;
}

/*---------------------------------------------------------------------------
 *            RF_SendBadCommand()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Send an unsupported commmand.  This is provided for 
 *            Unplugfest support.  
 */
BtStatus RF_SendBadCommand(RfChannel *Channel)
{
    BtPacket *packet;
    BtStatus  status;

#if XA_ERROR_CHECK == XA_ENABLED

    if (!RfIsValidChannel(Channel)) {
        return FALSE;
    }

#else /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(RfIsValidChannel(Channel));

#endif /* XA_ERROR_CHECK == XA_ENABLED */

    OS_LockStack();

    if (!(IsListEmpty(&(MUX(Channel->muxId).freeList)))) {
        packet = (BtPacket *)RemoveHeadList(&(MUX(Channel->muxId).freeList));
    } else {
        OS_UnlockStack();
        return BT_STATUS_NO_RESOURCES;
    }

    packet->dataLen = 2;
    packet->data[0] = INVALID_CMD | CR_BIT;
    Channel->cmdSent = INVALID_CMD | CR_BIT;
    packet->data[1] = EA_BIT;
    packet->ulpContext = (void *)0;
    status = RfSendDataInternal(Channel->muxId, 0, packet);
    if (status != BT_STATUS_PENDING) {
        /* Send failed */
        InsertTailList(&(MUX(Channel->muxId).freeList), &(packet->node));
    } else {
        EVM_StartTimer(&(DLC(Channel->muxId,Channel->dlcId).channel->timer), RF_T2_TIMEOUT);
        DLC(Channel->muxId,Channel->dlcId).flags |= DLC_FLAG_REQ_SENT;
    }

    OS_UnlockStack();
    return status;
}

/*---------------------------------------------------------------------------
 *            RF_SendBadMSC()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Send an MSC to an invalid dlci.  This is provided for 
 *            Unplugfest support.  
 */
BtStatus RF_SendBadMSC(RfChannel *Channel, RfModemStatus *ModemStatus)
{
    BtStatus  status;

#if XA_ERROR_CHECK == XA_ENABLED

    if (!RfIsValidChannel(Channel)) {
        OS_UnlockStack();
        return FALSE;
    }

#else /* XA_ERROR_CHECK == XA_ENABLED */

    Assert(RfIsValidChannel(Channel));

#endif /* XA_ERROR_CHECK == XA_ENABLED */

    OS_LockStack();

    Channel->dlci = 0x23;
    status = RfSendModemStatus(Channel, 
                       ModemStatus->signals, 
                       ModemStatus->breakLen, 
                       CR_BIT);
    OS_UnlockStack();
    return status;
}

/*---------------------------------------------------------------------------
 *            RF_SendPN()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Send a PN command.  This is provided for Unplugfest support.  
 */
BtStatus RF_SendPN(RfChannel *Channel)
{
    return RfSendParmNegotiation(Channel, CR_BIT);
}

#endif /* UPF_TWEAKS == XA_ENABLED */

#endif /* RFCOMM_PROTOCOL == XA_ENABLED */


