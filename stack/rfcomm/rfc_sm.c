/***************************************************************************
 *
 * File:
 *     $Workfile:rfc_sm.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:128$
 *
 * Description:
 *     This file contains the state machine for the RFCOMM protocol.
 *
 * Created:
 *     Sept 13, 1999
 * 
 * Copyright 1999-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "btalloc.h"
#include "sys/debug.h"

#if RFCOMM_PROTOCOL == XA_ENABLED

/* Function prototypes */       
static void RfCommandSent(U8 MuxId);
static void RfSendDmAck(U8 MuxId, U8 *Buffer);

/*---------------------------------------------------------------------------
 *            RfDlcStateDisconnected()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handles the disconnected state of the DLC state machine.  A
 *            SABM is the only valid event.
 *
 * Return:    void
 */
void RfDlcStateDisconnected(RfChannel *Channel, U8 Event)
{
#if (BT_SECURITY == XA_ENABLED) && (RF_SECURITY == XA_ENABLED)
    BtStatus   status;
#endif /* (BT_SECURITY == XA_ENABLED) && (RF_SECURITY == XA_ENABLED) */

    switch (Event) {
    case SABM:
        /* A connection was requested */
        Report(("RFCOMM:  SABM received for DLCI %d on mux %d\n", 
                    Channel->dlci, Channel->muxId));

#if (BT_SECURITY == XA_ENABLED) && (RF_SECURITY == XA_ENABLED)
        /* Check security for this channel */
        Channel->state = DLC_STATE_SEC_PENDING;
        Channel->secToken.id = SEC_RFCOMM_ID;
        Channel->secToken.remDev = MUX(Channel->muxId).remDev;
        Channel->secToken.channel = Channel->dlci >> 1;
        Channel->secToken.incoming = TRUE;
        status = SEC_AccessRequest(&(Channel->secToken));
        if (status == BT_STATUS_SUCCESS) {
            Channel->state = DLC_STATE_CONN_INCOMING;
            RfAppCallback(Channel, BT_STATUS_SUCCESS, RFEVENT_OPEN_IND, 0);
        } else if (status != BT_STATUS_PENDING) {
            Channel->state = DLC_STATE_DISCONNECTED;
            RfSendDmAck(Channel->muxId, MUX(Channel->muxId).rxBuff);
            RfFreeDLC(Channel->muxId, Channel->dlcId);
        }
#else /* (BT_SECURITY == XA_ENABLED) && (RF_SECURITY == XA_ENABLED) */
        Channel->state = DLC_STATE_CONN_INCOMING;
        RfAppCallback(Channel, BT_STATUS_SUCCESS, RFEVENT_OPEN_IND, 0);
#endif /* (BT_SECURITY == XA_ENABLED) && (RF_SECURITY == XA_ENABLED) */
        break;

    case UIH_F:
    case UIH:
    case DISC:
        RfSendDmAck(Channel->muxId, MUX(Channel->muxId).rxBuff);
        break;
    }
}

/*---------------------------------------------------------------------------
 *            RfDlcStateConnected()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handles the connected state of the DLC state machine. 
 *            Expecting rx data, packet handled, or disconnect request. 
 *
 * Return:    void
 */
void RfDlcStateConnected(RfChannel *Channel, U8 Event)
{
    switch (Event) {
    case DISC:
        /* Disconnect requested, notify app and send response */
        Channel->state = DLC_STATE_DISC_INCOMING;
        Report(("RFCOMM:  DISC received for DLCI %d on mux %d\n", 
                Channel->dlci, Channel->muxId));
        RfAppCallback(Channel, BT_STATUS_SUCCESS, RFEVENT_CLOSE_IND, 0);
        (void)RfSendAck(UA_F, Channel->muxId, Channel->dlci);
        break;

    case L2EVENT_PACKET_HANDLED:

        /* An application packet was handled */
        RfAppCallback(Channel, MUX(Channel->muxId).l2Status, 
                      RFEVENT_PACKET_HANDLED, 0);
        break;

    case UIH_F:
    case UIH:
        /* Data has been received for the application, check the length */
        if (MUX(Channel->muxId).rxLen <= Channel->maxFrameSize) {
            RfAppCallback(Channel, BT_STATUS_SUCCESS, RFEVENT_DATA_IND, 0);
        } else {
            /* Invalid RX length */
            Report(("RFCOMM:  Invalid receive length, disconnecting...\n"));
            if (RfDisconnectDLC(Channel->muxId, Channel->dlcId) != BT_STATUS_PENDING) {
                RfDisconnectL2CAP(Channel->muxId);
            }
        }
        break;

    case DM_F:
    case DM:
        Report(("RFCOMM:  Disconnected mode reported\n"));
        if (MUX(Channel->muxId).dlcCount == 1) {
            RfDisconnectL2CAP(Channel->muxId);
        } else {
            RfFreeDLC(Channel->muxId, Channel->dlcId);
        }
        break;

    case SABM:
        /* Unexpected (invalid) event received, tear down the MUX */
        Report(("RFCOMM:  Invalid event for DLCI %d mux %d\n", 
                Channel->dlci, Channel->muxId));
        RfDisconnectL2CAP(Channel->muxId);
        break;
    }
}

/*---------------------------------------------------------------------------
 *            RfDlcStateConnPending()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handles the connect pending state of the DLC state machine. 
 *            Expecting a UA response (accept) or a DM (reject).
 *
 * Return:    void
 */
void RfDlcStateConnPending(RfChannel *Channel, U8 Event)
{
    /* Waiting for a UA_F for the connection */
    switch (Event) {
    case UA_F:
        RfCommandSent(Channel->muxId);
        Report(("RFCOMM:  DLCI %d up on mux %d, sending MSC\n", 
                Channel->dlci, Channel->muxId));

        if (!(MUX(Channel->muxId).flags & MUX_FLAG_CREDIT_FLOW) &&
            (Channel->rxCredit == 0)) {
            Channel->rfSignals |= RF_FLOW;
        }

        if (RfSendModemStatus(Channel, Channel->rfSignals, 0, CR_BIT) == 
            BT_STATUS_PENDING) {

            DLC(Channel->muxId,Channel->dlcId).flags &= ~DLC_FLAG_REQ_SENT;

            /* MSC has been sent, now we are open */
            Channel->state = DLC_STATE_CONNECTED;
            DLC(Channel->muxId,Channel->dlcId).flags |= DLC_FLAG_MSC_PENDING;
            RfAppCallback(Channel, BT_STATUS_SUCCESS, RFEVENT_OPEN, 0);
        } else {
            /* Free the channel, because L2CAP failed to send MSC */
            RfFreeDLC(Channel->muxId, Channel->dlcId);
        }
        break;

    case DM_F:
    case DM:
        /* Connection rejected */
        Report(("RFCOMM:  Connection to DLCI %d on mux %d rejected\n", 
                Channel->dlci, Channel->muxId));
        RfCommandSent(Channel->muxId);

        /* If this is the last DLC, shut down the mux */
        if (MUX(Channel->muxId).dlcCount == 1) {
            RfDisconnectL2CAP(Channel->muxId);
        } else {
            /* Free the channel */
            RfFreeDLC(Channel->muxId, Channel->dlcId);
        }
        break;

    case SABM:
        /* Connect race condition */
        RfSendDmAck(Channel->muxId, MUX(Channel->muxId).rxBuff);
        break;

    case UIH_F:
    case DISC:
    case UIH:
        /* Unexpected (invalid) event received, tear down the MUX */
        Report(("RFCOMM:  Invalid event for DLCI %d mux %d\n", 
                Channel->dlci, Channel->muxId));
        RfDisconnectL2CAP(Channel->muxId);
        break;
    }
}

/*---------------------------------------------------------------------------
 *            RfDlcStateConnIncoming()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handles the connect incoming state of the DLC state machine.
 *            Expecting a packet handled event, which indicates that the
 *            connect accept (UA) was sent.
 *
 * Return:    void
 */
void RfDlcStateConnIncoming(RfChannel *Channel, U8 Event)
{
    if (Event == L2EVENT_PACKET_HANDLED) {
        /* See what kind of reponse was made */
        if (MUX(Channel->muxId).txPacket->header[BT_PACKET_HEADER_LEN - 2] == UA_F) {
            /* The UA_F was sent successfully */
            Report(("RFCOMM:  UA_F sent successfully to DLCI %d on mux %d\n", 
                    Channel->dlci, Channel->muxId));
    
            Report(("RFCOMM:  Sending MSC\n"));

            if (!(MUX(Channel->muxId).flags & MUX_FLAG_CREDIT_FLOW) &&
                (Channel->rxCredit == 0)) {
                Channel->rfSignals |= RF_FLOW;
            }

            if (RfSendModemStatus(Channel, Channel->rfSignals, 0, CR_BIT) ==
                BT_STATUS_PENDING) {
            
                DLC(Channel->muxId,Channel->dlcId).flags &= ~DLC_FLAG_REQ_SENT;

                /* MSC has been sent, now we are open */
                Channel->state = DLC_STATE_CONNECTED;
                DLC(Channel->muxId,Channel->dlcId).flags |= DLC_FLAG_MSC_PENDING;
                RfAppCallback(Channel, BT_STATUS_SUCCESS, RFEVENT_OPEN, 0);
            } else {
                Report(("RFCOMM:  L2CAP did not allow MSC to be sent\n"));
                RfFreeDLC(Channel->muxId, Channel->dlcId);
            }
        } else {
            /* A DM_F was sent or an error occured on the previous send */
            RfFreeDLC(Channel->muxId, Channel->dlcId);
        }
    } else if (Event == DISC) {
        /* Disconnect requested, notify app and send response */
        Channel->state = DLC_STATE_DISC_INCOMING;
        Report(("RFCOMM:  DISC received for DLCI %d on mux %d\n", 
                Channel->dlci, Channel->muxId));
        RfAppCallback(Channel, BT_STATUS_SUCCESS, RFEVENT_CLOSE_IND, 0);
        (void)RfSendAck(UA_F, Channel->muxId, Channel->dlci);
    } else {
        /* Unexpected (invalid) event received, tear down the MUX */
        Report(("RFCOMM:  Invalid event for DLCI %d mux %d\n", 
                Channel->dlci, Channel->muxId));
        RfDisconnectL2CAP(Channel->muxId);
    }
}

/*---------------------------------------------------------------------------
 *            RfDlcStateDiscPending()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handles the disconnect pending state for the DLC state machine.
 *            Expecting a UA or DM responce.  Packet handled events are
 *            passed up to the application for processing.
 *
 * Return:    void
 */
void RfDlcStateDiscPending(RfChannel *Channel, U8 Event)
{
    /* Waiting for a UA_F for the connection */
    switch (Event) {
    case DISC:
        /* Disconnect race condition */
        (void)RfSendAck(UA_F, Channel->muxId, Channel->dlci);

        /* Drop through to next case */
    case UA_F:
    case DM_F:
    case DM:
        RfCommandSent(Channel->muxId);
        Report(("RFCOMM:  DLCI %d on mux %d disconnected\n", 
                Channel->dlci, Channel->muxId));

        /* If this is the last DLC, shut down the mux */
        if (MUX(Channel->muxId).dlcCount == 1) {
            RfDisconnectL2CAP(Channel->muxId);
        } else {
            /* Free the channel */
            RfFreeDLC(Channel->muxId, Channel->dlcId);
        }
        break;

    case L2EVENT_PACKET_HANDLED:
        if (MUX(Channel->muxId).txPacket->ulpContext != (void *)0) {
            RfAppCallback(Channel, MUX(Channel->muxId).l2Status, 
                          RFEVENT_PACKET_HANDLED, 0);
        }
        break;

    case SABM:
        /* Unexpected (invalid) event received, tear down the MUX */
        Report(("RFCOMM:  Invalid event for DLCI %d mux %d\n", 
                Channel->dlci, Channel->muxId));
        RfDisconnectL2CAP(Channel->muxId);
    }
}

/*---------------------------------------------------------------------------
 *            RfDlcStateDiscIncoming()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handles the disconnect incoming state of the DLC state machine.
 *            Expecting a packet handled event to indicate that the connect
 *            response was sent.
 *
 * Return:    void
 */
void RfDlcStateDiscIncoming(RfChannel *Channel, U8 Event)
{
    /* Waiting for UA_F packet to be handled */
    if (Event == L2EVENT_PACKET_HANDLED) {
        if (MUX(Channel->muxId).txPacket->ulpContext != (void *)0) {
            /* This is a user packet, pass it up */
            RfAppCallback(Channel, MUX(Channel->muxId).l2Status, 
                          RFEVENT_PACKET_HANDLED, 0);
        } else {
            Report(("RFCOMM:  DLCI %d on mux %d disconnected\n", 
                    Channel->dlci, Channel->muxId));
            RfFreeDLC(Channel->muxId, Channel->dlcId);
        }
    } else {
        /* Unexpected (invalid) event received, tear down the MUX */
        Report(("RFCOMM:  Invalid event for DLCI %d mux %d\n", 
                Channel->dlci, Channel->muxId));
        RfDisconnectL2CAP(Channel->muxId);
    }
}

/*---------------------------------------------------------------------------
 *            RfAdvanceNegPending()
 *---------------------------------------------------------------------------
 *
 * Synopsis: Advances the negotiate pending state.  If negOK is TRUE, a
 *           SABM is sent and the state is advanced to DLC_STATE_CON_PENDING.
 *           If negOK is FALSE, the channel is cleaned up and the state
 *           is advanced DLC_STATE_DISCONNECTED.
 *
 * Return:    
 */
void RfAdvanceNegPending(RfChannel *Channel, BOOL negOK)
{
    /* Send the SABM command */
    if (negOK == TRUE) {
        if (RfSendCommand(SABM, Channel->muxId, Channel->dlcId) == 
            BT_STATUS_PENDING) {
            Report(("RFCOMM:  SABM sent successfully to DLCI %d on mux %d\n", 
                    Channel->dlci, Channel->muxId));
            Channel->state = DLC_STATE_CONN_PENDING;
        } else {
            /* Free the channel, because L2CAP failed to send SABM */
            RfFreeDLC(Channel->muxId, Channel->dlcId);
        }
    } else {
        /* Negotiation failed */

        Report(("RFCOMM:  Negotiation failed\n"));
        /* If this is the last DLC, shut down the mux */
        if (MUX(Channel->muxId).dlcCount == 1) {
            RfDisconnectL2CAP(Channel->muxId);
        } else {
            /* Free the channel */
            RfFreeDLC(Channel->muxId, Channel->dlcId);
        }
    }
}

/*---------------------------------------------------------------------------
 *            RfDlcStateNegPending()
 *---------------------------------------------------------------------------
 *
 * Synopsis: A parameter negotiation was sent.  This state waits for the
 *           negotiation to be accepted or rejected.
 *
 * Return:    
 */
void RfDlcStateNegPending(RfChannel *Channel, U8 Event)
{
    /* Anything but a positive PN response will cause the 
     * connect sequence to fail.
     */
    switch (Event) {
    case SABM:
        /* Could be a connect race condidition, let's try to be nice */
        RfSendDmAck(Channel->muxId, MUX(Channel->muxId).rxBuff);
        break;

    case UIH_F:
    case UA_F:
    case DISC:
    case UIH:
        /* Unexpected (invalid) event received, tear down the MUX */
        Report(("RFCOMM:  Invalid event for DLCI %d mux %d\n", 
                Channel->dlci, Channel->muxId));
        RfDisconnectL2CAP(Channel->muxId);
        break;
    case DM_F:
    case DM:
        /* Disconnected mode - could be a reject */
        RfAdvanceNegPending(Channel, FALSE);
    }
}

#if (BT_SECURITY == XA_ENABLED) && (RF_SECURITY == XA_ENABLED)
/*---------------------------------------------------------------------------
 *            RfDlcStateSecPending()
 *---------------------------------------------------------------------------
 *
 * Synopsis: Security is being checked on an incoming/outgoing channel.
 *
 * Return:    
 */
void RfDlcStateSecPending(RfChannel *Channel, U8 Event)
{
    UNUSED_PARAMETER(Event);

    if (Channel->secToken.incoming == TRUE) {
        if (Event == DISC) {
            /* Disconnect requested, notify app and send response */
            Report(("RFCOMM:  DISC received for DLCI %d on mux %d\n", 
                    Channel->dlci, Channel->muxId));
            (void)RfSendAck(UA_F, Channel->muxId, Channel->dlci);
            RfFreeDLC(Channel->muxId, Channel->dlcId);
        } else {
            /* We shouldn't receive any other events in this state */
            Report(("RFCOMM:  Invalid event for DLCI %d mux %d\n", 
                    Channel->dlci, Channel->muxId));
            RfDisconnectL2CAP(Channel->muxId);
        }
    } else {
        /* Race condition, the local application is already attempting
         * to connect to this channel and wait for security.
         */
        RfSendDmAck(Channel->muxId, MUX(Channel->muxId).rxBuff);
    }
}
#endif /* (BT_SECURITY == XA_ENABLED) && (RF_SECURITY == XA_ENABLED) */

/*---------------------------------------------------------------------------
 *            RfMuxStateOpen()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handles the open state of the RFCOMM multiplexer.  Events for
 *            allocated channels are passed to the DLC state machine.  If
 *            a channel is not allocated, then a SABM is the only valid event.
 *            Events for DLC 0 (control channel) are passed up to the
 *            control channel state machine (see RfMuxStateMachine()).
 *
 * Return:    void
 */
static void RfMuxStateOpen(U8 MuxId, U8 Dlci, U8 Event)
{
    U8 dlcId;
#if NUM_RF_SERVERS != 0
    RfChannel *channel;
#endif /* NUM_RF_SERVERS != 0 */

    UNUSED_PARAMETER(Event);

    if (RfFindDlcIdByDlci(MuxId, Dlci, &dlcId) != BT_STATUS_SUCCESS) {
        /* No channel has been allocated yet, only a SABM is a valid
         * event (connect request).
         */
        if (Event == SABM) {
#if NUM_RF_SERVERS != 0
            /* See if a registered service exists for the specified DLCI */
            if (RfGetFreeServiceChannel(MuxId, (U8)(Dlci >> 1), &channel) ==
                BT_STATUS_SUCCESS) {
                /* A service is registered */

                if (!(channel->flags & CHNL_FLAG_NEG_DONE)) {
                    /* No negotiation took place, use the default
                     * RFCOMM frame size.
                     */
                    channel->maxFrameSize = RF_DEFAULT_FRAMESIZE;
                }

                /* Try to allocate the channel */
                if (RfAllocDLC(MuxId, Dlci, channel, &dlcId) == BT_STATUS_FAILED) {
                    /* Cannot alloc a channel, report disconnected mode */
                    Report(("RFCOMM:  Out of resources on connect request for mux %d\n", 
                            MuxId));
                    goto error_exit;
                } else {
                    /* A channel was allocated */
                    RFC(servers)[(Dlci >> 1) - 1].inUseCnt++;
                    DLC(MuxId,dlcId).channel = channel;
                    if (MUX(MuxId).flags & MUX_FLAG_CREDIT_FLOW) {
                        /* Credit flow control has been negotiated */
                        DLC(MuxId,dlcId).flags |= DLC_FLAG_CREDIT_FLOW;
                    }

                    Report(("RFCOMM:  Allocated incoming DLCI %d on mux %d\n", 
                            Dlci, MuxId));
                }
            } else 
#endif /* NUM_RF_SERVERS != 0 */
            {
                /* No registered service exists */
                Report(("RFCOMM:  Service %d not registered on connect request for mux %d\n", 
                        Dlci >> 1, MuxId));
                goto error_exit;
            } 
        } else if ((Event == DM) || (Event == DM_F) || (Event == DISC)) {
            /* It is possible a Parm Negotiation was made, but no connect 
             * attempt will be made.
             */
#if NUM_RF_SERVERS != 0
            if (RfGetFreeServiceChannel(MuxId, (U8)(Dlci >> 1), &channel) ==
                BT_STATUS_SUCCESS) {
                /* Cancel the timer and free the resource */
                EVM_CancelTimer(&(channel->timer));
                RfFreeUsePendingChannel(channel);
            }
#endif /* NUM_RF_SERVERS != 0 */

            /* Respond with a DM_F */
            goto error_exit;
        } else if (Event == L2EVENT_PACKET_HANDLED) {
            /* Ignore this event */
            return;
        } else {
            /* Not a valid command for an unallocated channel */
            Report(("RFCOMM:  Command for unallocated channel on mux %d\n", MuxId));
            goto error_exit;
        }
    }

    /* Call the DLC state machine */
    RFC(state)[DLC(MuxId,dlcId).channel->state](DLC(MuxId,dlcId).channel, Event);
    return;

error_exit:

    /* Report disconnected mode */
    RfSendDmAck(MuxId, MUX(MuxId).rxBuff);
}

/*---------------------------------------------------------------------------
 *            RfMuxStateConnPending()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handles the connect pending state of the RFCOMM multiplexer.
 *            Expecting an UA or DM event for DLC 0.  Parameter negotiation
 *            for the DLC requested by the application is initiated here.
 *            If DLC 0 is not up yet, it is initiated here as well.
 *
 * Return:    void
 */
static void RfMuxStateConnPending(U8 MuxId, U8 Event)
{
    if (Event == L2EVENT_CONNECTED) {
        /* Bring up DLC 0 */
        Report(("RFCOMM:  Sending SABM to DLCI 0 for Mux %d\n", MuxId));
        AssertEval(RfSendCommand(SABM, MuxId, INVALID_DLC_ID) == BT_STATUS_PENDING);
    } else if (Event == UA_F) {
        /* Done initializing the multiplexer */
        RfCommandSent(MuxId);
        Report(("RFCOMM:  Mux %d up\n", MuxId));
        MUX(MuxId).state = MUX_STATE_OPEN;

        /* Send Negotiation request for the DLC */
        DLC(MuxId,MUX(MuxId).cDlcId).channel->maxFrameSize = 
                        min(L2CAP_GetTxMtu(MUX(MuxId).l2ChannelId) - 5,
                            DLC(MuxId,MUX(MuxId).cDlcId).channel->maxFrameSize);

        if (RfSendParmNegotiation(DLC(MuxId,MUX(MuxId).cDlcId).channel,
                                CR_BIT) != BT_STATUS_PENDING) {

            /* Could not sent the negotiation */
            Report(("RFCOMM:  Could not send negotiation to DLCI %d on mux %d\n", 
                    DLC(MuxId,MUX(MuxId).cDlcId).channel->dlci, MuxId));

            /* If this is the last DLC, shut down the mux */
            if (MUX(MuxId).dlcCount == 1) {
                RfDisconnectL2CAP(MuxId);
            } else {
                /* Free the channel */
                RfFreeDLC(MuxId, MUX(MuxId).cDlcId);
            }
        } else {
            /* Negotiation request sent */
            DLC(MuxId, MUX(MuxId).cDlcId).channel->state = DLC_STATE_NEG_PENDING;
            Report(("RFCOMM:  Negotiation sent successfully to DLCI %d on mux %d\n", 
                    DLC(MuxId,MUX(MuxId).cDlcId).channel->dlci, MuxId));
        }
    } else if ((Event == DM)   || (Event == DM_F) || (Event == SABM) ||
               (Event == DISC) || (Event == UIH) || (Event == UIH_F)) {
        /* Connection rejected (or invalid event) */
        Report(("RFCOMM:  Connection to DLCI 0 on mux %d rejected (or invalid event)\n", 
                MuxId));

        /* Shut down the mux */
        RfDisconnectL2CAP(MuxId);
    } 
}

/*---------------------------------------------------------------------------
 *            RfMuxStateConnIncoming()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Handles the connect incoming state of the RFCOMM multiplexer.
 *
 * Return:    void
 */
static void RfMuxStateConnIncoming(U8 MuxId, U8 Event)
{
    /* Waiting for an SABM on DLC 0 */
    if (Event == SABM) {
        /* Respond to SABM */
        Report(("RFCOMM:  Sending UA_F to DLCI 0 on Mux %d\n", MuxId));
        (void)RfSendAck(UA_F, MuxId, 0);
    } else if (Event == L2EVENT_PACKET_HANDLED) {
        /* The UA_F was sent sucessfully */
        Report(("RFCOMM:  Mux %d up\n", MuxId));
        MUX(MuxId).state = MUX_STATE_OPEN;
    } else if ((Event == UA_F) || (Event == DM_F) || (Event == DM) ||
               (Event == DISC) || (Event == UIH) || (Event == UIH_F)) {
        /* Invalid event */
        Report(("RFCOMM:  Invalid event for uninitialized mux %d\n", 
                MuxId));
        RfDisconnectL2CAP(MuxId);
    }
}

/*---------------------------------------------------------------------------
 *            RfMuxStateMachine()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  State machine for specified Mux.
 *
 * Return:    void
 */
static void RfMuxStateMachine(U8 MuxId, U8 Event)
{
    U8 dlci = 0;
    U8 dlcId;
    RfChannel *channel;
    U8 *ptr;

    if (Event == L2EVENT_DISCONNECTED) {
        Report(("RFCOMM:  L2CAP disconnect, shutting down mux\n"));
        RfFreeMux(MuxId);
        return;
    }

    /* Get the DLCI value for the event */
    if (Event == L2EVENT_PACKET_HANDLED) { 
        
        ptr = &(MUX(MuxId).txPacket->header[BT_PACKET_HEADER_LEN - 1]);

        if ((MUX(MuxId).txPacket->ulpContext == (void *)0) ||
            (MUX(MuxId).txPacket->ulpContext == (void *)1)) {
            /* Internal packet */
            if (*(ptr-1) & 0x01) {
                /* 1 byte length */
                dlci = *(ptr-2) >> 2;
            } else {
                /* 2 byte length */
                dlci = *(ptr-3) >> 2;
            }

            if (RfFindDlcIdByDlci(MuxId, dlci, &dlcId) == BT_STATUS_SUCCESS) {
                if (DLC(MuxId,dlcId).channel->flags & CHNL_FLAG_NO_RESOURCE) {
                    /* Tell the app that a resource is free */
                    DLC(MuxId,dlcId).channel->flags &= ~CHNL_FLAG_NO_RESOURCE;
                    RfAppCallback(DLC(MuxId,dlcId).channel, BT_STATUS_SUCCESS, 
                                                    RFEVENT_RESOURCE_FREE, 0);
                }
            }

            if (MUX(MuxId).txPacket->ulpContext == (void *)1) {
                /* Credit packet */
                return;
            }
        } else {
            /* User data */
            channel = (RfChannel *)(MUX(MuxId).txPacket->ulpContext);
            dlci = channel->dlci;
        }

    } else if (Event != L2EVENT_CONNECTED) {
        dlci = (MUX(MuxId).rxBuff[0] & 0xFC) >> 2;
    }

    /* Process the event for all other states */
    switch (MUX(MuxId).state) {
    case MUX_STATE_OPEN:
        if (dlci == 0) {
            switch (Event) {
            case DISC:
                /* Multiplexer is being shut down */
                Report(("RFCOMM:  DISC (shutdown) for mux %d\n", MuxId)); 
                (void)RfSendAck(UA_F, MuxId, dlci);
                break;
            case DM:
            case DM_F:
            case SABM:
                Report(("RFCOMM:  Received SABM or DM for channel 0 on connected session\n"));
                RfDisconnectL2CAP(MuxId);
                break;
            default:
                /* This event is for the mutliplexer control channel */
                RfControlChannel(MuxId, Event);
                break;
            }
        } else {
            /* This event is for an open DLC */
            RfMuxStateOpen(MuxId, dlci, Event);
        } 
        break;
    case MUX_STATE_CONN_PENDING:
        /* Waiting for a UA_F response to DLC 0 */
        if (dlci == 0) {
            RfMuxStateConnPending(MuxId, Event);
        } else {
            /* Shut down the mux */
            Report(("RFCOMM:  Invalid event for uninitialized mux %d\n", 
                    MuxId));
            RfDisconnectL2CAP(MuxId);
        }
        break;
    case MUX_STATE_CONN_INCOMING:
        if (dlci == 0) {
            RfMuxStateConnIncoming(MuxId, Event);
        } else {
            /* Shut down the mux */
            Report(("RFCOMM:  Invalid event for uninitialized mux %d\n", 
                    MuxId));
            RfDisconnectL2CAP(MuxId);
        }
        break;

    case MUX_STATE_CLOSED:
        if ((Event == UA_F) || (Event == DM_F) || (Event == DM) ||
            (Event == DISC) || (Event == UIH)  || (Event == SABM) || 
            (Event == UIH_F)) {
            /* This mux is not initialized - Report disconnected mode */
            RfSendDmAck(MuxId, MUX(MuxId).rxBuff);
        }
        break;
    default:
        Assert(0);
        break;
    }
}

/*---------------------------------------------------------------------------
 *            RfL2Callback()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Callback routine for L2CAP connections.
 *
 * Return:    void
 */
void RfL2Callback(L2capChannelId L2ChannelId, L2capCallbackParms *info)
{
    BOOL validFrame = TRUE;
    U8  lenSize;
    U8 muxId;

    switch (info->event) {
    case L2EVENT_CONNECT_IND:
        /* Request from a new machine.  A new multiplexer must be added */
        if (RfAllocMux(info->aclLink, L2ChannelId, &muxId) != BT_STATUS_SUCCESS) {
            /* Unable to allocate mux, reject the L2CAP connect indication */
            AssertEval(L2CAP_ConnectRsp(L2ChannelId, L2CONN_REJECT_NO_RESOURCES, 0) == 
                       BT_STATUS_PENDING);
            Report(("RFCOMM:  Sending connect reject on Mux %d\n", muxId));
        } else {
            /* Accept the connection */
            AssertEval(L2CAP_ConnectRsp(L2ChannelId, L2CONN_ACCEPTED, 0) == 
                       BT_STATUS_PENDING);
            Report(("RFCOMM:  Incoming connection, initializing Mux %d\n", muxId));
            MUX(muxId).state = MUX_STATE_CONN_INCOMING;
        }
        break;
    case L2EVENT_DISCONNECTED:
        Report(("RFCOMM:  L2CAP Disconnected Event\n"));
    case L2EVENT_CONNECTED:
#if XA_SNIFFER == XA_ENABLED
        if (info->event == L2EVENT_CONNECTED)
            SnifferRegisterEndpoint(SPI_L2CAP, &L2ChannelId, info->aclLink, SPI_RFCOMM);
#endif
    case L2EVENT_PACKET_HANDLED:
        if (RfFindMuxIdByRemDev(info->aclLink, &muxId) == BT_STATUS_SUCCESS) {
            /* Found the multiplexer */
            if (info->event == L2EVENT_PACKET_HANDLED) {
                MUX(muxId).l2Status = (BtStatus)info->status;
                MUX(muxId).txPacket = info->ptrs.packet;
                if ((info->ptrs.packet->ulpContext == (void *)0) ||
                    (info->ptrs.packet->ulpContext == (void *)1)) {
                    if (
#if RF_SEND_TEST == XA_ENABLED
                        (info->ptrs.packet != &(MUX(muxId).testCmdPacket)) &&
#endif /* RF_SEND_TEST == XA_ENABLED */
                        (info->ptrs.packet != &(MUX(muxId).testRspPacket))) {
                        InsertTailList(&(MUX(muxId).freeList), 
                                       &(info->ptrs.packet->node));
                    }
                }
            }

            RfMuxStateMachine(muxId, info->event);
        } else if (info->event == L2EVENT_CONNECTED) {
            /* Connected, but no MUX exists */
            AssertEval(L2CAP_DisconnectReq(L2ChannelId) == BT_STATUS_PENDING);
        }
        break;
    case L2EVENT_DATA_IND:
        /* Handle incoming data */
        if (RfFindMuxIdByRemDev(info->aclLink, &muxId) == BT_STATUS_SUCCESS) {

            /* Found an existing Mux */
            MUX(muxId).rxBuff = info->ptrs.data;

            /* Determine size of the length field. */
            if ((U8)MUX(muxId).rxBuff[2] & 0x01) {
                /* 1 Byte Length */
                lenSize = 1;
                MUX(muxId).rxLen = MUX(muxId).rxBuff[2] >> 1;
            } else {
                /* 2 Byte Length */
                lenSize = 2;
                MUX(muxId).rxLen = (MUX(muxId).rxBuff[2] >> 1) | (MUX(muxId).rxBuff[3] << 7);
            }

            /* Check FCS */
            if ((MUX(muxId).rxBuff[1] == UIH) || (MUX(muxId).rxBuff[1] == UIH_F)) {
                if (MUX(muxId).rxBuff[1] == UIH_F) {
                    /* UIH_F has a credit bytes. Here lenSize is increased to later
                     * verify data length. 
                     */
                    lenSize++;
                }

                /* Calculate FCS on address and control bytes. */
                validFrame = RfIsValidFCS(MUX(muxId).rxBuff,
                                        2,
                                        MUX(muxId).rxBuff[info->dataLen - 1]);
            } else {
                /* Calculate FCS on address, control, and length bytes. */
                validFrame = RfIsValidFCS(MUX(muxId).rxBuff,
                                        (U8)(lenSize + 2),
                                        MUX(muxId).rxBuff[info->dataLen - 1]);
            }

#if XA_DEBUG == XA_ENABLED
            if (!validFrame) {
                /* Shut down the multiplexer, and tear down L2CAP channel */
                Report(("RFCOMM:  Invalid FCS, shutting down Mux %d\n", muxId));
            }
#endif /* XA_DEBUG == XA_ENABLED */

            /* Check length */
            if (validFrame) {
                if (info->dataLen != (MUX(muxId).rxLen + lenSize + 3)) {
                    validFrame = FALSE;
                    Report(("RFCOMM:  Invalid length in RX data, shutting down Mux %d\n", muxId));
                }
            }

            if (!validFrame) {
                RfDisconnectL2CAP(muxId);
            } else {
                /* Valid frame received, Call the Mux state machine */
                RfMuxStateMachine(muxId, MUX(muxId).rxBuff[1]);
            }
        } else {
            /* Connected, but no MUX exists */
            AssertEval(L2CAP_DisconnectReq(L2ChannelId) == BT_STATUS_PENDING);
        }
        break;
    }
}

/*---------------------------------------------------------------------------
 *            RfSendCmdRsp()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Sets up a command or response for transmistion and transmits.
 *
 * Return:    
 */
static BtStatus RfSendCmdRsp(U8 CmdRsp, U8 MuxId, BtPacket *Packet)
{
    BtStatus  status = BT_STATUS_PENDING;

    /* Initialize the transmit packet */
    Packet->header[BT_PACKET_HEADER_LEN - 2] = CmdRsp;
    Packet->header[BT_PACKET_HEADER_LEN - 1] = 0x01;
    Packet->headerLen = 3;
    Packet->flags = BTP_FLAG_TAIL;
    Packet->header[0] = RfCalcFCS(Packet->header + BT_PACKET_HEADER_LEN - 3, 3);
    Packet->tail = Packet->header;
    Packet->tailLen = 1;
    Packet->dataLen = 0;

    if ((CmdRsp == SABM) || (CmdRsp == DISC)) {
        /* This is a command */
        if (MUX(MuxId).flags & MUX_FLAG_CMD_SENT) {
            /* Command outstanding, queue the command for later */
            InsertTailList(&(MUX(MuxId).cmdQueue), &(Packet->node));
        } else {
            status = L2CAP_SendData(MUX(MuxId).l2ChannelId, Packet);
            if (status == BT_STATUS_PENDING) {
                /* Command was sent */
                if ((CmdRsp == SABM) && 
                    ((Packet->header[BT_PACKET_HEADER_LEN - 3] >> 2) != 0)) {
                    EVM_StartTimer(&(MUX(MuxId).timer), RF_CONNECT_TIMEOUT);
                } else {
                    EVM_StartTimer(&(MUX(MuxId).timer), RF_T1_TIMEOUT);
                }
                MUX(MuxId).flags |= MUX_FLAG_CMD_SENT;
            }
        }
    } else {
        /* This is a response */
        status = L2CAP_SendData(MUX(MuxId).l2ChannelId, Packet);
    }

    return status;
}

/*---------------------------------------------------------------------------
 *            RfSendCommand()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Routine for sending a command.
 *
 * Return:    
 */
BtStatus RfSendCommand(U8 Command, U8 MuxId, U8 DlcId)
{
    BtPacket *packet;
    U8        dlci;
    BtStatus  status;

    /* Get a transmit packet */
    if (!(IsListEmpty(&(MUX(MuxId).freeList)))) {
        packet = (BtPacket *)RemoveHeadList(&(MUX(MuxId).freeList));
    } else {
        return BT_STATUS_NO_RESOURCES;
    }

    if (DlcId == INVALID_DLC_ID) {
        dlci = 0;
    } else {
        dlci = DLC(MuxId,DlcId).channel->dlci;
    }

    if (MUX(MuxId).flags & MUX_FLAG_INITIATOR) {
        /* Command */
        packet->header[BT_PACKET_HEADER_LEN - 3] = 
            ((dlci << 2) | EA_BIT) | CR_BIT;
    } else {
        /* Response */
        packet->header[BT_PACKET_HEADER_LEN - 3] = 
            ((dlci << 2) | EA_BIT) & ~CR_BIT;
    }

    packet->ulpContext = (void *)0;
    status = RfSendCmdRsp(Command, MuxId, packet);
    if (status != BT_STATUS_PENDING) {
        /* Send failed */
        InsertTailList(&(MUX(MuxId).freeList), &(packet->node));
    }

    return status;
}


/*---------------------------------------------------------------------------
 *            RfCommandSent()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Called when an acknowledgement has been received.
 *
 * Return:    
 */
static void RfCommandSent(U8 MuxId)
{
    BtStatus  status = BT_STATUS_SUCCESS;
    BtPacket *packet;

    MUX(MuxId).flags &= ~MUX_FLAG_CMD_SENT;
    EVM_CancelTimer(&(MUX(MuxId).timer));

    if (!IsListEmpty(&(MUX(MuxId).cmdQueue))) {
        packet = (BtPacket *)RemoveHeadList(&(MUX(MuxId).cmdQueue));
        status = L2CAP_SendData(MUX(MuxId).l2ChannelId, packet);
        if (status == BT_STATUS_PENDING) {
            if (packet->header[BT_PACKET_HEADER_LEN - 2] == SABM) {
                EVM_StartTimer(&(MUX(MuxId).timer), RF_CONNECT_TIMEOUT);
            } else {
                EVM_StartTimer(&(MUX(MuxId).timer), RF_T1_TIMEOUT);
            }
            MUX(MuxId).flags |= MUX_FLAG_CMD_SENT;
        }
    }
}

/*---------------------------------------------------------------------------
 *            RfSendAck()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Routine for sending an acknowledgement.
 *
 * Return:    
 */
BtStatus RfSendAck(U8 Response, U8 MuxId, U8 Dlci)
{
    BtPacket *packet;
    BtStatus  status;

    /* Get a transmit packet */
    if (!IsListEmpty(&(MUX(MuxId).freeList))) {
        packet = (BtPacket *)RemoveHeadList(&(MUX(MuxId).freeList));
    } else {
        return BT_STATUS_NO_RESOURCES;
    }

    if (MUX(MuxId).flags & MUX_FLAG_INITIATOR) {
        packet->header[BT_PACKET_HEADER_LEN - 3] = 
            ((Dlci << 2) | EA_BIT) & ~CR_BIT;
    } else {
        packet->header[BT_PACKET_HEADER_LEN - 3] = 
            ((Dlci << 2) | EA_BIT) | CR_BIT;
    }

    packet->ulpContext = (void *)0;
    status = RfSendCmdRsp(Response, MuxId, packet);
    if (status != BT_STATUS_PENDING) {
        /* Send failed */
        InsertTailList(&(MUX(MuxId).freeList), &(packet->node));
    }

    return status;
}

/*---------------------------------------------------------------------------
 *            RfSendDmAck()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Routine for sending a DM response.
 *
 * Return:    
 */
static void RfSendDmAck(U8 MuxId, U8 *Buffer)
{
    if (Buffer[1] != DM_F) {
        Report(("RFCOMM:  Sending DM_F to DLCI %d on Mux %d\n", (Buffer[0] & 0xFC) >> 2, MuxId));
        (void)RfSendAck(DM_F, MuxId, (U8)(Buffer[0] >> 2));
    }
}

/*---------------------------------------------------------------------------
 *            RfSendDataInternal()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Send a UIH data packet.
 *
 * Return:    
 */
BtStatus RfSendDataInternal(U8 MuxId, U8 Dlci, BtPacket *Packet)
{
    U8 offset;
    U8 hLen;
    U8 dlcId;
    RfChannel *channel = (void *)0;

    offset = Packet->dataLen > 127 ? 4 : 3;

    /* See if credit flow control is being used and adjust header len */
    if (RfFindDlcIdByDlci(MuxId, Dlci, &dlcId) == BT_STATUS_SUCCESS) {
        channel = DLC(MuxId,dlcId).channel;
        if ((MUX(MuxId).flags & MUX_FLAG_CREDIT_FLOW) &&
            (channel->rxCredit)) {
            offset++;
        }
    }

    hLen = offset;

    /* Create the address */
    if (MUX(MuxId).flags & MUX_FLAG_INITIATOR) {
        Packet->header[BT_PACKET_HEADER_LEN - offset--] = 
        ((Dlci << 2) | EA_BIT) | CR_BIT;
    } else {
        Packet->header[BT_PACKET_HEADER_LEN - offset--] = 
        ((Dlci << 2) | EA_BIT) & ~CR_BIT;
    }

    /* Set the UIH message type
     * Check to see if credit flow control is enabled 
     * and there is credit to grant.
     */
    if ((dlcId != INVALID_DLC_ID) &&
        (MUX(MuxId).flags & MUX_FLAG_CREDIT_FLOW) && (channel->rxCredit)) {
        Packet->header[BT_PACKET_HEADER_LEN - offset--] = UIH_F;
    } else {
        Packet->header[BT_PACKET_HEADER_LEN - offset--] = UIH;
    }

    /* Calculate the length using ISO/IEC 13239:1997 */
    if (Packet->dataLen > 127) {
        Packet->header[BT_PACKET_HEADER_LEN - offset--] = ((Packet->dataLen & 0x007F) << 1);
        Packet->header[BT_PACKET_HEADER_LEN - offset--] = ((Packet->dataLen & 0x7F80) >> 7);
    } else {
        Packet->header[BT_PACKET_HEADER_LEN - offset--] = ((Packet->dataLen << 1) | 0x01);
    }

    Packet->headerLen = hLen;

    /* Add the credit field */
    if ((dlcId != INVALID_DLC_ID) &&
        (MUX(MuxId).flags & MUX_FLAG_CREDIT_FLOW) && (channel->rxCredit)) {
        Packet->header[BT_PACKET_HEADER_LEN - offset--] = (U8)channel->rxCredit;
        channel->grantedCredit += channel->rxCredit;
        channel->rxCredit = 0;
    }

    Packet->flags &= BTP_FLAG_INUSE;
    Packet->flags |= BTP_FLAG_TAIL;
    Packet->header[0] = RfCalcFCS(Packet->header + BT_PACKET_HEADER_LEN - hLen, 2);
    Packet->tail = Packet->header;
    Packet->tailLen = 1;

    return L2CAP_SendData(MUX(MuxId).l2ChannelId, Packet);
}


#endif /* RFCOMM_PROTOCOL == XA_ENABLED */


