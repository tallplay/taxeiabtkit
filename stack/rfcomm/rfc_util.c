/***************************************************************************
 *
 * File:
 *     $Workfile:rfc_util.c$ for iAnywhere Blue SDK, Version 2.1.1
 *     $Revision:123$
 *
 * Description:
 *     Utility functions for the RFCOMM protocol.
 *
 * Created:
 *     Sept 13, 1999
 *
 * Copyright 1999-2005 Extended Systems, Inc.
 * Portions copyright 2005 iAnywhere Solutions, Inc.
 * All rights reserved. All unpublished rights reserved.
 *
 * Unpublished Confidential Information of iAnywhere Solutions, Inc.  
 * Do Not Disclose.
 *
 * No part of this work may be used or reproduced in any form or by any 
 * means, or stored in a database or retrieval system, without prior written 
 * permission of iAnywhere Solutions, Inc.
 * 
 * Use of this work is governed by a license granted by iAnywhere Solutions, 
 * Inc.  This work contains confidential and proprietary information of 
 * iAnywhere Solutions, Inc. which is protected by copyright, trade secret, 
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

#include "btalloc.h"
#include "sys/rfcexp.h"

#if RFCOMM_PROTOCOL == XA_ENABLED

/* Function prototypes */       
static void RfFreeAllDLCs(U8 MuxId);
static void RfServiceTimeoutHandler(EvmTimer *timer);

/*---------------------------------------------------------------------------
 *            RF_Init()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Initialize RFCOMM.  Register with L2CAP, initialize state and
 *            other variables.
 *
 * Return:    
 */
BtStatus RF_Init(void)
{
#if NUM_RF_SERVERS != 0
    I8 i;
#endif /* NUM_RF_SERVERS != 0 */

    /* Register RFCOMM with L2CAP */
    RFC(psm).callback = RfL2Callback;
    RFC(psm).psm = BT_PSM_RFCOMM;
    RFC(psm).localMtu = RF_MAX_FRAME_SIZE + 5;
    RFC(psm).minRemoteMtu = RF_MIN_FRAME_SIZE + 5;
#if L2CAP_FLOW_CONTROL == XA_ENABLED
    RFC(psm).inLinkMode = RFC(psm).outLinkMode = L2MODE_BASIC;
#endif /* L2CAP_FLOW_CONTROL == XA_ENABLED */

    if (L2CAP_RegisterPsm(&(RFC(psm))) != BT_STATUS_SUCCESS) {
        /* Unable to register */
        Report(("RFCOMM:  Unable to register with L2CAP.\n"));
        return BT_STATUS_FAILED;
    }

#if NUM_RF_SERVERS != 0
    for (i = 0; i < NUM_RF_SERVERS; i++) {
        InitializeListHead(&(RFC(servers)[i].channelList));
    }
#endif /* NUM_RF_SERVERS != 0 */

    /* Load the state table */
    RFC(state)[0] = RfDlcStateDisconnected;
    RFC(state)[1] = RfDlcStateConnected;
    RFC(state)[2] = RfDlcStateConnPending;
    RFC(state)[3] = RfDlcStateConnIncoming;
    RFC(state)[4] = RfDlcStateDiscPending;
    RFC(state)[5] = RfDlcStateDiscIncoming;
    RFC(state)[6] = RfDlcStateNegPending;

#if (BT_SECURITY == XA_ENABLED) && (RF_SECURITY == XA_ENABLED)
    RFC(state)[7] = RfDlcStateSecPending;
#endif

    Report(("RFCOMM:  Initialized.\n"));
    return BT_STATUS_SUCCESS;
}

/*---------------------------------------------------------------------------
 *            RF_Deinit()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Deinitialize RFCOMM.  Deregister with L2CAP.
 *            
 *
 * Return:    
 */
void RF_Deinit(void)
{
#if L2CAP_DEREGISTER_FUNC == XA_ENABLED
    L2CAP_DeregisterPsm(&(RFC(psm)));
    Report(("RFCOMM:  Deinitialized.\n"));
#endif /* L2CAP_DEREGISTER_FUNC == XA_ENABLED */
}

#if (XA_ERROR_CHECK == XA_ENABLED) || (XA_DEBUG == XA_ENABLED)
/*---------------------------------------------------------------------------
 *            RfIsValidChannel()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Validate the channel structure.
 *
 * Return:    
 */
BOOL RfIsValidChannel(RfChannel *Channel)
{
    if (Channel == 0) {
        return FALSE;
    }

    if (Channel->dlcId >= NUM_RF_CHANNELS) {
        return FALSE;
    }

    if (Channel->muxId >= NUM_BT_DEVICES) {
        return FALSE;
    }

    return TRUE;
}

#endif

/*---------------------------------------------------------------------------
 *            RfIsChannelAllocated()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  See if a channel is in the connected state.
 *
 * Return:    
 */
BOOL RfIsChannelAllocated(RfChannel *Channel)
{
    if (DLC(Channel->muxId,Channel->dlcId).channel != Channel) {
        return FALSE;
    }

    if (!(DLC(Channel->muxId,Channel->dlcId).flags & DLC_FLAG_ALLOCATED)) {
        return FALSE;
    }

    return TRUE;
}

#if NUM_RF_SERVERS != 0

/*---------------------------------------------------------------------------
 *            RfFreeUsePendingChannel()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Free a service resource that was requested, but never connected.
 *
 * Return:    
 */
void RfFreeUsePendingChannel(RfChannel *channel)
{
    Assert(channel->flags & CHNL_FLAG_USE_PENDING);
    Report(("RFCOMM:  A channel was requested, but not used\n"));
    channel->maxFrameSize = channel->initialFrameSize;
    channel->rxCredit = channel->initialRxCredit;

#if UPF_TWEAKS == XA_ENABLED
    channel->flags &= CHNL_FLAG_NO_CFC;
#else
    channel->flags = 0;
#endif /* UPF_TWEAKS == XA_ENABLED */
}

/*---------------------------------------------------------------------------
 *            RfServiceTimeoutHandler()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Timeout handler for PN and RPN requests for a service before
 *            a connection is established.  If a subsequent SABM is not 
 *            received withing a reasonable amount of time, then this timer
 *            will fire and free up the service resource.
 *
 * Return:    
 */
static void RfServiceTimeoutHandler(EvmTimer *timer)
{
    U8         serviceId = (U16)((I32)timer->context) & 0x00FF;
    U8         muxId = (U16)((I32)timer->context) >> 8;
    RfChannel *channel;

    if (RfGetFreeServiceChannel(muxId, serviceId, &channel) == BT_STATUS_SUCCESS) {
        /* This channel was requested, but then never used.  A PN
         * or RPN request was received, but no SABM.
         */

        /* Check for active link */
        if ((ME_GetCurrentMode(MUX(muxId).remDev) == BLM_HOLD_MODE) ||
            (ME_GetCurrentMode(MUX(muxId).remDev) == BLM_PARK_MODE)) {
            /* The link is in hold, park or sniff, restart the timer. */
            EVM_StartTimer(timer, SRV_CHNL_PENDING_TIMEOUT);
            return;
        }

        RfFreeUsePendingChannel(channel);
    }
    
}
#endif

#if NUM_RF_SERVERS != 0
/*---------------------------------------------------------------------------
 *            RfAllocService()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Find an open server slot and assign it.
 *            If no open slot exists, then an error is returned and the channel
 *            number is set to 0 (1 - 30 is valid).
 *
 * Return:    
 */
BtStatus RfAllocService(RfChannel *Channel, RfService *Service)
{
    BtStatus status = BT_STATUS_FAILED;
    I8 i;
    I8 slot = 0xfe;

    if (Service->serviceId == 0) {
        /* Look for a new service */
        for (i = NUM_RF_SERVERS; i > 0; i--) {
            /* Search for an open slot */
            if (!(RFC(servers)[i - 1].flags & SRV_FLAG_ALLOCATED)) {
                /* Free slot */
                slot = i - 1;
                Service->serviceId = (U8)i;
                status = BT_STATUS_SUCCESS;
            } else {
                /* Make sure the channel is not already registered */
                if (IsNodeOnList(&RFC(servers[i - 1].channelList), &(Channel->node))) {
                    status = BT_STATUS_IN_USE;
                    break;
                }
            }
        }
    } else if (Service->serviceId <= NUM_RF_SERVERS)  {
        /* Adding a channel to the service */
        slot = Service->serviceId - 1;
        if (RFC(servers)[slot].service == Service) {
            status = BT_STATUS_SUCCESS;
        } 
    } else {
        status = BT_STATUS_FAILED;
    }

    if (status == BT_STATUS_SUCCESS) {
        /* Add the channel to the list of channels for the service */
        if (!IsNodeOnList(&(RFC(servers)[slot].channelList), &(Channel->node))) {
            InsertTailList(&(RFC(servers[slot].channelList)), &(Channel->node));
            RFC(servers)[slot].flags |= SRV_FLAG_ALLOCATED;
            RFC(servers)[slot].service = Service;
            Report(("RFCOMM:  Registered channel on service %d\n", slot + 1));
        } else {
            status = BT_STATUS_FAILED;
        }
    }

#if XA_DEBUG == XA_ENABLED
    if (status != BT_STATUS_SUCCESS) {
        Report(("RFCOMM:  Failed to registered channel on service %d\n", slot + 1));
    }
#endif /* XA_DEBUG == XA_ENABLED */

    return status;
}

/*---------------------------------------------------------------------------
 *            RfFreeServerChannel()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Free a service.
 *
 * Return:    
 */
BtStatus RfFreeServerChannel(RfChannel *Channel, RfService *Service)
{
    BtStatus status = BT_STATUS_FAILED;
    U8       slot = Service->serviceId - 1;

    /* Free the server channel */
    if (IsNodeOnList(&(RFC(servers)[slot].channelList), &(Channel->node))) {
        if (!(Channel->flags & CHNL_FLAG_IN_USE)) {
            /* The channel is not in use, remove it from the service */
            Report(("RFCOMM:  Deregistered channel from service %d\n", Service->serviceId));
            RemoveEntryList(&(Channel->node));
            if (Channel->flags & CHNL_FLAG_USE_PENDING) {
                EVM_CancelTimer(&(Channel->timer));
            }

            if (IsListEmpty(&(RFC(servers)[slot].channelList))) {
                /* This is the last channel register on this service, 
                 * free the service ID 
                 */
                RFC(servers)[slot].flags = 0;
            }

            status = BT_STATUS_SUCCESS;
        } else {
            /* The channel is still in use */
            status = BT_STATUS_BUSY;
        }
    }
        
    return status;
}

/*---------------------------------------------------------------------------
 *            RfFreeService()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Frees all channels from a service.
 *
 * Return:    
 */
BtStatus RfFreeService(RfService *Service)
{
    BtStatus   status = BT_STATUS_FAILED;
    U8         slot = Service->serviceId - 1;
    RfChannel *channel;

    if (RFC(servers)[slot].inUseCnt == 0) {
        /* Free the service (no channels are in use) */
        if (RFC(servers)[slot].service == Service) {
            if (RFC(servers)[slot].flags & SRV_FLAG_ALLOCATED) {
                /* Free all channels from the service */
                while (!IsListEmpty(&(RFC(servers)[slot].channelList))) {
                    channel = (RfChannel *)GetHeadList(&(RFC(servers)[slot].channelList));
                    AssertEval(RfFreeServerChannel(channel, Service) == BT_STATUS_SUCCESS);
                }
            }

            /* Free the service */
            RFC(servers)[slot].flags = 0;
            RFC(servers)[slot].service = 0;
            Report(("RFCOMM:  Deregistered all channels from service %d\n", Service->serviceId));
            status = BT_STATUS_SUCCESS;
        }
    } else {
        status = BT_STATUS_BUSY;
    }

    return status;
}

/*---------------------------------------------------------------------------
 *            RfGetFreeServiceChannel()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Get an unused channel from the specified service.
 *
 * Return:    
 */
BtStatus RfGetFreeServiceChannel(U8 MuxId, U8 ServiceId, RfChannel **Channel)
{
    RfChannel *entry;
    BtStatus   status = BT_STATUS_FAILED;

    if ((ServiceId > 0) &&
        ((ServiceId - 1) < NUM_RF_SERVERS) && 
        RFC(servers)[ServiceId - 1].flags & SRV_FLAG_ALLOCATED) {

        Assert(!IsListEmpty(&(RFC(servers)[ServiceId - 1].channelList)));
        entry = (RfChannel *)GetHeadList(&(RFC(servers)[ServiceId - 1].channelList));

        /* Walk the list */
        while ((ListEntry *)entry != &(RFC(servers)[ServiceId - 1].channelList)) {
            if (!(entry->flags & CHNL_FLAG_IN_USE)) {

                *Channel = entry;

                if ((*Channel)->flags & CHNL_FLAG_USE_PENDING) {
                    if ((*Channel)->muxId == MuxId) {
                        /* This channel has been requested once already */
                        Report(("RFCOMM:  Service channel requested previously\n"));
                        status = BT_STATUS_SUCCESS;
                        break;
                    }
                } else {
                    /* The channel is free and has not be requested previously */
                    Report(("RFCOMM:  Service channel requested for the first time\n"));
                    (*Channel)->flags |= CHNL_FLAG_USE_PENDING;
                    (*Channel)->muxId = MuxId;
                    (*Channel)->timer.func = RfServiceTimeoutHandler;
                    (*Channel)->timer.context = (void *)(MuxId << 8 | ServiceId);
                    EVM_StartTimer(&(*Channel)->timer, SRV_CHNL_PENDING_TIMEOUT);
                    status = BT_STATUS_SUCCESS;
                    break;
                }

            }
            entry = (RfChannel *)GetNextNode(&(entry->node));
        }
    }

    return status;
}
#endif /* NUM_RF_SERVERS != 0 */

/*---------------------------------------------------------------------------
 *            RfAllocMux()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Allocate a multiplexer for the specified Remote Device.  
 *
 * Return:    
 */
BtStatus RfAllocMux(BtRemoteDevice *RemDev, L2capChannelId L2ChannelId, U8 *MuxId)
{
    BtStatus status = BT_STATUS_FAILED;
    I8 i;
    I8 slot = 0;
    U8 *ptr;

    for (i = NUM_BT_DEVICES; i > 0; i--) {
        /* Search for an open slot */
        if (MUX(i-1).flags & MUX_FLAG_ALLOCATED) {
            /* Slot is occupied */
            if (RemDev == MUX(i-1).remDev) {
                /* Error, already allocated */
                slot = i - 1;
                status = BT_STATUS_IN_USE;
                break;
            }
        } else {
            /* Free slot */
            slot = i - 1;
            status = BT_STATUS_SUCCESS;
        }
    }

    if (status == BT_STATUS_SUCCESS) {
        /* Initialize this mux */
        ptr = MUX(slot).reqBuff;
        InitializeListHead(&(MUX(slot).freeList));
        for (i = 0; i < (NUM_RF_CHANNELS + 3); i++) {
            /* Initialize Tx packets for control channel */
            MUX(slot).packets[i].data = ptr;
            ptr += MAX_CMD_RSP_SIZE;
            InsertTailList(&(MUX(slot).freeList), &(MUX(slot).packets[i].node));
        }

#if RF_SEND_TEST == XA_ENABLED
        MUX(slot).testCmdPacket.data = MUX(slot).testCmdData;
#endif /* RF_SEND_TEST == XA_ENABLED */
        MUX(slot).testRspPacket.data = MUX(slot).testRspData;

        InitializeListHead(&(MUX(slot).cmdQueue));

        MUX(slot).remDev = RemDev;
        MUX(slot).l2ChannelId = L2ChannelId;
        MUX(slot).flags = MUX_FLAG_ALLOCATED;
        MUX(slot).state = MUX_STATE_CLOSED;
        MUX(slot).timer.func = RfTimeoutHandler;
        MUX(slot).timer.context = (void *)((slot << 8) | INVALID_DLC_ID);
        MUX(slot).packetsInTransit = 0;
        InitializeListHead(&(MUX(slot).priorityList));
    }

    *MuxId = (U8)(slot);

    return status;
}

/*---------------------------------------------------------------------------
 *            RfFreeMux()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Free the multiplexer specified by MuxId.
 *
 * Return:    
 */
void RfFreeMux(U8 MuxId)
{
    if (MUX(MuxId).flags & MUX_FLAG_ALLOCATED) {

        if (MUX(MuxId).flags & MUX_FLAG_CMD_SENT) {
            /* Command outstanding, cancel the timer */
            EVM_CancelTimer(&(MUX(MuxId).timer));
        }

        /* Free the multiplexer */
        MUX(MuxId).state = MUX_STATE_CLOSED;
        MUX(MuxId).flags = 0;
        Report(("RFCOMM:  Mux %d freed\n", MuxId));
        
        /* Free all DLCs */
        if (MUX(MuxId).dlcCount) {
            RfFreeAllDLCs(MuxId);
        }
    }
}

/*---------------------------------------------------------------------------
 *            FindMuxIdByRemDev()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Find the multiplexer associated with the Remote Device.
 *
 * Return:    
 */
BtStatus RfFindMuxIdByRemDev(BtRemoteDevice *RemDev, U8 *MuxId)
{
    I8 i;
    BtStatus status = BT_STATUS_FAILED;

    for (i = 0; i < NUM_BT_DEVICES; i++) {
        /* Search for an allocated mux */
        if (MUX(i).flags & MUX_FLAG_ALLOCATED) {
            if (MUX(i).remDev == RemDev) {
                *MuxId = (U8)i;
                status = BT_STATUS_SUCCESS;
                break;
            }
        }
    }
    return status;
}

/*---------------------------------------------------------------------------
 *            RfAllocDLC()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Allocate a DLC in the designated multiplexer.
 *
 * Return:    
 */
BtStatus RfAllocDLC(U8 MuxId, U8 Dlci, RfChannel *Channel, U8 *DlcId)
{
    I8 i;
    I8 slot = 0;
    BtStatus status = BT_STATUS_FAILED;
    RF_DLC *ptr;

    for (i = NUM_RF_CHANNELS; i > 0; i--) {
        /* Search for an open slot */
        if (!(DLC(MuxId,i-1).flags & DLC_FLAG_ALLOCATED)) {
            /* Free slot */
            slot = i - 1;
            status = BT_STATUS_SUCCESS;
        } else {
            if (DLC(MuxId,i-1).channel->dlci == Dlci) {
                /* This channel is already in use */
                status = BT_STATUS_IN_USE;
                break;
            }
        }
    }

    if (status == BT_STATUS_SUCCESS) {
        /* Initialize the structure */
        if (Channel->flags & CHNL_FLAG_USE_PENDING) {
            EVM_CancelTimer(&(Channel->timer));
            Channel->flags &= ~CHNL_FLAG_USE_PENDING;
        }

        *DlcId = (U8)slot;
        MUX(MuxId).dlcCount++;
        DLC(MuxId,slot).channel = Channel;
        DLC(MuxId,slot).flags |= DLC_FLAG_ALLOCATED;
        Channel->muxId = MuxId;
        Channel->dlcId = (U8)slot;
        Channel->dlci  = Dlci;
        Channel->timer.func = RfTimeoutHandler;
        Channel->timer.context = (void *)(MuxId << 8 | slot);
        Channel->state = DLC_STATE_DISCONNECTED;

#if UPF_TWEAKS == XA_ENABLED
        Channel->flags &= CHNL_FLAG_NO_CFC;
        Channel->flags |= CHNL_FLAG_IN_USE;
#else
        Channel->flags = CHNL_FLAG_IN_USE;
#endif /* UPF_TWEAKS == XA_ENABLED */

        Channel->rfSignals = RF_RTR | RF_RTC | RF_DV;

        InitializeListHead(&(Channel->txQueue));

        /* Find a spot on the priority list */
        if (!IsListEmpty(&(MUX(MuxId).priorityList))) {
            ptr = (RF_DLC *)GetHeadList(&(MUX(MuxId).priorityList));
            while (GetNextNode(&(ptr->node)) != &(MUX(MuxId).priorityList)) {
                if (Channel->priority > ptr->channel->priority) {
                    ptr = (RF_DLC *)GetNextNode(&(ptr->node));
                } else {
                    /* Found the right spot in the priority list */
                    break;
                }
            }
        } else {
            ptr = (RF_DLC *)(&(MUX(MuxId).priorityList));
        }
    
        /* Insert into the priority list */
        InsertHeadList((ListEntry *)ptr, &(DLC(MuxId,slot).node));
        Report(("RFCOMM: Allocated DLCI %02X on Mux %02X\n", Dlci, MuxId));
    }

    return status;
}

/*---------------------------------------------------------------------------
 *            RfFreeDLC()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Free the DLC in the specified multiplexer.
 *
 * Return:    
 */
void RfFreeDLC(U8 MuxId, U8 DlcId)
{
    RfChannel *channel;
    BOOL       do_callback = TRUE;

#if NUM_RF_SERVERS != 0
    RF_SERVER *server;
#endif /* NUM_RF_SERVERS != 0 */

    Assert(DLC(MuxId,DlcId).flags & DLC_FLAG_ALLOCATED);
    Assert(MUX(MuxId).dlcCount);

    channel = DLC(MuxId,DlcId).channel;

    if ((DLC(MuxId,DlcId).flags & DLC_FLAG_REQ_SENT) || 
        (DLC(MuxId,DlcId).flags & DLC_FLAG_MSC_PENDING)) {
        /* A timer is running if a request has been sent */
        EVM_CancelTimer(&(DLC(MuxId,DlcId).channel->timer));
    }

#if (BT_SECURITY == XA_ENABLED) && (RF_SECURITY == XA_ENABLED)
    if (DLC(MuxId,DlcId).channel->state == DLC_STATE_SEC_PENDING) {
        /* Cancel the security access request */
        do_callback = FALSE;
        AssertEval(SEC_CancelAccessRequest(&(channel->secToken)) ==
                   BT_STATUS_SUCCESS);
    } else if (DLC(MuxId,DlcId).channel->state == DLC_STATE_SEC_FAILED) {
        do_callback = FALSE;
    }
#endif /* (BT_SECURITY == XA_ENABLED) && (RF_SECURITY == XA_ENABLED) */

    DLC(MuxId,DlcId).channel->state = DLC_STATE_DISCONNECTED;

    /* Free the DLC */
    MUX(MuxId).dlcCount--;
    DLC(MuxId,DlcId).flags = 0;

#if UPF_TWEAKS == XA_ENABLED
    DLC(MuxId,DlcId).channel->flags &= CHNL_FLAG_NO_CFC;
#else
    DLC(MuxId,DlcId).channel->flags = 0;
#endif /* UPF_TWEAKS == XA_ENABLED */

    /* Restore the initial credit */
    DLC(MuxId,DlcId).channel->rxCredit = DLC(MuxId,DlcId).channel->initialRxCredit;

#if NUM_RF_SERVERS != 0
    /* See if this DLC was bound to a local server */
    if (((DLC(MuxId,DlcId).channel->dlci >> 1) - 1) < NUM_RF_SERVERS) {
        server = &(RFC(servers)[(DLC(MuxId,DlcId).channel->dlci >> 1) - 1]);
        if (IsNodeOnList(&(server->channelList), &(DLC(MuxId,DlcId).channel->node))) {
            DLC(MuxId,DlcId).channel->maxFrameSize = 
                DLC(MuxId,DlcId).channel->initialFrameSize;
            server->inUseCnt--;
        }
    }
#endif /* NUM_RF_SERVERS != 0 */

    /* Return all packets */
    while (!IsListEmpty(&(DLC(MuxId,DlcId).channel->txQueue))) {
        MUX(MuxId).txPacket = (BtPacket *)RemoveHeadList(&(DLC(MuxId,DlcId).channel->txQueue));
        RfAppCallback(DLC(MuxId,DlcId).channel, BT_STATUS_NO_CONNECTION, RFEVENT_PACKET_HANDLED, 0);
    }

    /* Remove the DLC from the priority list */
    RemoveEntryList(&(DLC(MuxId,DlcId).node));

    DLC(MuxId,DlcId).channel = 0;

    if (do_callback) {
        RfAppCallback(channel, BT_STATUS_SUCCESS, RFEVENT_CLOSED, 0);
    }
}

/*---------------------------------------------------------------------------
 *            RfFreeAllDLCs()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Free all the DLCs in the specified multiplexer.
 *
 * Return:    
 */
static void RfFreeAllDLCs(U8 MuxId)
{
    I8 i;

    for (i = 0; i < NUM_RF_CHANNELS; i++) {
        if (DLC(MuxId,i).flags & DLC_FLAG_ALLOCATED) {
            RfFreeDLC(MuxId, (U8)i);
        }
    }
}

/*---------------------------------------------------------------------------
 *            RfFindDlcIdByDlci()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Find the DLC in the multiplexer by the Dlci value.
 *
 * Return:    
 */
BtStatus RfFindDlcIdByDlci(U8 MuxId, U8 Dlci, U8 *DlcId)
{
    I8 i;
    BtStatus status = BT_STATUS_FAILED;

    for (i = 0; i < NUM_RF_CHANNELS; i++) {
        /* Search for an allocate channel with the correct DLC ID */
        if (DLC(MuxId,i).flags & DLC_FLAG_ALLOCATED) {
            if (DLC(MuxId,i).channel->dlci == Dlci) {
                *DlcId = (U8)i;
                status = BT_STATUS_SUCCESS;
                break;
            }
        }
    }

    if (status == BT_STATUS_FAILED) {
       *DlcId = INVALID_DLC_ID;
    }

    return status;
}

/*---------------------------------------------------------------------------
 *            RfDisconnectDLC()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Disconnect Data Link Channel.
 *
 * Return:    void
 */
BtStatus RfDisconnectDLC(U8 MuxId, U8 DlcId)
{
    BtStatus status;

    if ((status = RfSendCommand(DISC, MuxId, DlcId)) == BT_STATUS_PENDING) {
        DLC(MuxId,DlcId).channel->state = DLC_STATE_DISC_PENDING;
    }
    return status;
}

/*---------------------------------------------------------------------------
 *            RfDisconnectL2CAP()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Disconnect L2CAP.
 *
 * Return:    void
 */
void RfDisconnectL2CAP(U8 MuxId)
{
    if (L2CAP_DisconnectReq(MUX(MuxId).l2ChannelId) != BT_STATUS_PENDING) {
        /* There is some problem below, just abort */
        Report(("RFCOMM:  Could not disconnect from L2CAP, shutting down mux\n"));
        RfFreeMux(MuxId);
    }
}

/*---------------------------------------------------------------------------
 *            RfTimeoutHandler()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Timer handler for RFCOMM's acknowledgement/response timer.
 *
 * Return:    void
 */
void RfTimeoutHandler(EvmTimer *timer)
{
    U8  dlcId = (U16)((I32)timer->context) & 0x00FF;
    U8  muxId = (U16)((I32)timer->context) >> 8;

    /* Check for active link */
    if ((ME_GetCurrentMode(MUX(muxId).remDev) == BLM_HOLD_MODE) ||
        (ME_GetCurrentMode(MUX(muxId).remDev) == BLM_PARK_MODE)) {
        /* The link is in hold or park mode, restart the timer. */
        EVM_StartTimer(timer, RF_T1_TIMEOUT);
        return;
    }

    if (dlcId != INVALID_DLC_ID) {
        /* A request was sent, but no response was received */
        DLC(muxId,dlcId).flags &= ~DLC_FLAG_REQ_SENT;
    } else {
        MUX(muxId).flags &= ~MUX_FLAG_CMD_SENT;
    }

    /* Assuming a link loss, shutting down multiplexer */
    Report(("RFCOMM:  Link loss timer fired, shutting down mux\n"));

    if (DLC(muxId, MUX(muxId).cDlcId).channel->state == DLC_STATE_CONN_PENDING) {
        RfDisconnectDLC(muxId,MUX(muxId).cDlcId);
    } else {
        RfDisconnectL2CAP(muxId);
    }
}

/*---------------------------------------------------------------------------
 *            RfAppCallback()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Call the application with the specified event.
 *
 * Return:    void
 */
void RfAppCallback(RfChannel *Channel, BtStatus status, U8 Event, void *data)
{
    RfCallbackParms   parms;

#if RF_SEND_CONTROL == XA_DISABLED
    UNUSED_PARAMETER(data);
#endif /* RF_SEND_CONTROL == XA_ENABLED */
    
    switch (Event) {
    case RFEVENT_PACKET_HANDLED:

        XASTAT_TimerStop(MUX(Channel->muxId).txPacket->dataLen, rfcomm_tx_timer, 
                         MUX(Channel->muxId).txPacket->rfc_timer);

        /* An application packet has been sent */
        if (MUX(Channel->muxId).packetsInTransit) {
            (MUX(Channel->muxId).packetsInTransit)--;
        }
        parms.ptrs.packet = MUX(Channel->muxId).txPacket;
        RfMUXCheckTxQueues(Channel->muxId);
        break;
    case RFEVENT_DATA_IND:
        /* Data has been received for the application */
        if ((U8)MUX(Channel->muxId).rxBuff[2] & 0x01) {
            /* 1 Byte Length */
            parms.ptrs.data = MUX(Channel->muxId).rxBuff + 3;
            parms.dataLen = MUX(Channel->muxId).rxBuff[2] >> 1;
        } else {
            /* 2 Byte Length */
            parms.ptrs.data = MUX(Channel->muxId).rxBuff + 4;
            parms.dataLen = (MUX(Channel->muxId).rxBuff[2] >> 1) | 
                            (MUX(Channel->muxId).rxBuff[3] << 7);
        }

        if (MUX(Channel->muxId).flags & MUX_FLAG_CREDIT_FLOW) {
            /* Credit flow control is in effect */
            if (MUX(Channel->muxId).rxBuff[1] == UIH_F) {
                /* Check for an overflow */
                if ((Channel->txCredit + parms.ptrs.data[0]) < Channel->txCredit) {
                    /* Overflow of transmit credit */
                    Report(("RFCOMM:  txCredit overflow\n"));
                    if (RfDisconnectDLC(Channel->muxId, Channel->dlcId) != BT_STATUS_PENDING) {
                        RfDisconnectL2CAP(Channel->muxId);
                    }
                    return;
                } else {
                    /* Get the credit amount */
                    Channel->txCredit += parms.ptrs.data[0];
                    parms.ptrs.data++;
                    RfMUXCheckTxQueues(Channel->muxId);
                }
            }

            if (parms.dataLen > 0 ) {
                /* Data was received */
                if (Channel->grantedCredit == 0) {
                    Report(("RFCOMM:  Data received from device with no credit\n"));
                    if (RfDisconnectDLC(Channel->muxId, Channel->dlcId) != BT_STATUS_PENDING) {
                        RfDisconnectL2CAP(Channel->muxId);
                    }
                    return;
                }
                Channel->grantedCredit--;
            }
        } else {
            /* MSC flow control is in effect */
            if (parms.dataLen > 0) {
                Channel->rxCredit--;
            }
        }

        if (parms.dataLen == 0 ) {
            /* 0 length data, ignore it */
            return;
        }
        break;
    case RFEVENT_MODEM_STATUS_IND:
        parms.ptrs.modemStatus = (RfModemStatus *)data;
        parms.dataLen = 2;
        break;

#if RF_SEND_CONTROL == XA_ENABLED
    case RFEVENT_LINE_STATUS_IND:
        parms.ptrs.lineStatus = (RfLineStatus *)data;
        parms.dataLen = 1;
        break;
    case RFEVENT_PORT_NEG_IND:
    case RFEVENT_PORT_NEG_CNF:
    case RFEVENT_PORT_STATUS_CNF:
        if (data) {
            parms.ptrs.portSettings = (RfPortSettings *)data;
            parms.dataLen = (U16)sizeof(RfPortSettings);
        } else {
            parms.dataLen = 0;
        }
        break;
        
#endif /* RF_SEND_CONTROL == XA_DISABLED */
        
    case RFEVENT_OPEN_IND:
    case RFEVENT_OPEN:
    case RFEVENT_CLOSE_IND:
    case RFEVENT_CLOSED:
        parms.ptrs.remDev = MUX(Channel->muxId).remDev;
        parms.dataLen = (U16)sizeof(BtRemoteDevice);
        break;
        
    default:
        parms.ptrs.data = 0;
        parms.dataLen = 0;
        break;
    }

    parms.status = status;
    parms.event = Event;
    Channel->callback(Channel, &parms);

    if (Event == RFEVENT_DATA_IND) {
        RfCheckForCreditToSend(Channel);
    }
}

/*---------------------------------------------------------------------------
 *            RfCheckForCreditToSend()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Checks to see if credit must be sent to the remote device.  To
 *            keep from sending an empty UIH frame every time credit is 
 *            advanced, check to see if the amount of outstanding credits
 *            is larger than the granted credits.
 *
 * Return:    
 */
void RfCheckForCreditToSend(RfChannel *Channel) 
{
    BtStatus  status = BT_STATUS_SUCCESS;
    BtPacket *packet;
    BOOL      reqSent;

    if ((Channel->state != DLC_STATE_CONNECTED) ||
        !(DLC(Channel->muxId, Channel->dlcId).flags & DLC_FLAG_MSC_RCVD)) {
        return;
    }

    /* Send credit if necessary */
    if (MUX(Channel->muxId).flags & MUX_FLAG_CREDIT_FLOW) {
        /* Credit flow control is enabled */
        if ((Channel->rxCredit > 0) && (Channel->rxCredit >= Channel->grantedCredit)) {
            /* There is credit to be granted */
            if (IsListEmpty(&(Channel->txQueue)) || (Channel->txCredit == 0)) {
                /* Credit must be sent in it's own 0 length packet */
                if (!(IsListEmpty(&(MUX(Channel->muxId).freeList)))) {
                    packet = (BtPacket *)RemoveHeadList(&(MUX(Channel->muxId).freeList));
                    packet->dataLen = 0;
                    packet->ulpContext = (void *)1;
                    status = RfSendDataInternal(Channel->muxId, Channel->dlci, packet);
                    if (status != BT_STATUS_PENDING) {
                        /* Send failed */
                        InsertTailList(&(MUX(Channel->muxId).freeList), &(packet->node));
                    }
                }
            }
        }
    } else {
        /* Credit flow was not negotiated */
        if (!(DLC(Channel->muxId,Channel->dlcId).flags & DLC_FLAG_MSC_PENDING)) {
            /* A request is not already outstanding */
            reqSent = DLC(Channel->muxId,Channel->dlcId).flags & DLC_FLAG_REQ_SENT;
            if (Channel->rxCredit <= 0) {
                /* Out of credit, send an MSC to stop the flow */
                if (!(Channel->rfSignals & RF_FLOW)) {
                    Channel->rfSignals |= RF_FLOW;
                    status = RfSendModemStatus(Channel, Channel->rfSignals, 0, CR_BIT);
                    if (status == BT_STATUS_PENDING) {
                        if (!reqSent) {
                            DLC(Channel->muxId,Channel->dlcId).flags &= ~DLC_FLAG_REQ_SENT;
                        }
                        DLC(Channel->muxId,Channel->dlcId).flags |= DLC_FLAG_MSC_PENDING;
                    }
                }
            } else {
                /* Credit available, send an MSC to start the flow */
                if (Channel->rfSignals & RF_FLOW) {
                    Channel->rfSignals &= ~RF_FLOW;
                    status = RfSendModemStatus(Channel, Channel->rfSignals, 0, CR_BIT);
                    if (status == BT_STATUS_PENDING) {
                        if (!reqSent) {
                            DLC(Channel->muxId,Channel->dlcId).flags &= ~DLC_FLAG_REQ_SENT;
                        }
                        DLC(Channel->muxId,Channel->dlcId).flags |= DLC_FLAG_MSC_PENDING;
                    }
                }
            }
        }
    }
}

/*---------------------------------------------------------------------------
 *            RfIsMuxFlowOn()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Checks to see if MUX flow control is on or off.
 *
 * Return:    
 */
static BOOL RfIsMuxFlowOn(U8 MuxId)
{
    if (MUX(MuxId).flags & MUX_FLAG_CREDIT_FLOW) {
        return TRUE;
    } else {
        if (MUX(MuxId).flags & MUX_FLAG_FCOFF) {
            return FALSE;
        } else {
            return TRUE;
        }
    }
}

/*---------------------------------------------------------------------------
 *            RfIsDlcFlowOn()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Checks to see if DLC flow control is on or off.
 *
 * Return:    
 */
static BOOL RfIsDlcFlowOn(U8 MuxId, U8 DlcId) 
{
    if (DLC(MuxId,DlcId).flags & DLC_FLAG_MSC_RCVD) {

        if (MUX(MuxId).flags & MUX_FLAG_CREDIT_FLOW) {
            if (DLC(MuxId,DlcId).channel->txCredit) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            if (DLC(MuxId,DlcId).flags & DLC_FLAG_FCOFF) {
                return FALSE;
            } else {
                return TRUE;
            }
        }

    } else {
        return FALSE;
    }
}

/*---------------------------------------------------------------------------
 *            RfMUXCheckNextTxQueue()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Sends data on the next queue in the priority list with data to 
 *            send and flow control on.
 *
 * Return:    
 */
void RfMUXCheckNextTxQueue(U8 MuxId)
{
    BtPacket *packet;
    RF_DLC *startNode = MUX(MuxId).cDlc;

    while (IsListEmpty(&(MUX(MuxId).cDlc->channel->txQueue)) ||
           !RfIsDlcFlowOn(MuxId, MUX(MuxId).cDlc->channel->dlcId)) {
        /* No data to send on the current DLC */
        MUX(MuxId).cDlc = (RF_DLC *)GetNextNode(&(MUX(MuxId).cDlc->node));
        if (MUX(MuxId).cDlc == (RF_DLC *)&(MUX(MuxId).priorityList)) {
            /* At the end of the list, go to the beginning */
            MUX(MuxId).cDlc = (RF_DLC *)GetNextNode(&(MUX(MuxId).cDlc->node));
        }

        if (MUX(MuxId).cDlc == startNode) {
            /* We've gone completely through the list once */
            MUX(MuxId).flags &= ~MUX_FLAG_XMIT;
            return;
        }
    }

    /* A packet is available and flow is on, send packet from the TX queue */
    if ((MUX(MuxId).cDlc->channel->state == DLC_STATE_CONNECTED) &&
        !(MUX(MuxId).cDlc->flags & DLC_FLAG_MSC_PENDING)) {
        packet = (BtPacket *)RemoveHeadList(&(MUX(MuxId).cDlc->channel->txQueue));
        if (RfSendDataInternal(MuxId, MUX(MuxId).cDlc->channel->dlci, packet) == BT_STATUS_PENDING) {
            (MUX(MuxId).packetsInTransit)++;
            if (MUX(MuxId).flags & MUX_FLAG_CREDIT_FLOW) {
                /* Decrement the flow control */
                Assert(MUX(MuxId).cDlc->channel->txCredit != 0);
                if (packet->dataLen > 0) {
                    MUX(MuxId).cDlc->channel->txCredit--;
                }
            }
        } else {
            /* Return to queue */
            InsertHeadList(&(MUX(MuxId).cDlc->channel->txQueue), &(packet->node));
            MUX(MuxId).flags &= ~MUX_FLAG_XMIT;
            return;
        }

        MUX(MuxId).numTxPackets--;
    } else {
        /* A packet could not be sent */
        MUX(MuxId).flags &= ~MUX_FLAG_XMIT;
    }
}

/*---------------------------------------------------------------------------
 *            RfMUXCheckTxQueues()
 *---------------------------------------------------------------------------
 *
 * Synopsis:  Sends data on the first queue in the priority list with data to 
 *            send and flow control on.
 *
 * Return:    
 */
void RfMUXCheckTxQueues(U8 MuxId) 
{
    if (RfIsMuxFlowOn(MuxId)) {
        if (!(MUX(MuxId).flags & MUX_FLAG_XMIT)) {
            /* Not already transmitting */
            MUX(MuxId).flags |= MUX_FLAG_XMIT;
            Assert(!IsListEmpty(&(MUX(MuxId).priorityList)));
            MUX(MuxId).cDlc = (RF_DLC *)GetHeadList(&(MUX(MuxId).priorityList));
        }
                
        while ((MUX(MuxId).packetsInTransit < RF_MAX_PACKETS_IN_TRANSIT) &&
               (MUX(MuxId).flags & MUX_FLAG_XMIT)) {
            RfMUXCheckNextTxQueue(MuxId);
        }
    }
}

#endif /* RFCOMM_PROTOCOL == XA_ENABLED */

